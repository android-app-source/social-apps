.class public final Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1448996
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1448997
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1448893
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1448894
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 1448967
    if-nez p1, :cond_0

    .line 1448968
    :goto_0
    return v0

    .line 1448969
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1448970
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1448971
    :sswitch_0
    const-class v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->c(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v1

    invoke-static {v1}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v1

    .line 1448972
    invoke-virtual {p3, v1}, LX/186;->c(Ljava/util/List;)I

    move-result v1

    .line 1448973
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1448974
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1448975
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1448976
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1448977
    const v2, -0x10c4f9b7

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1448978
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1448979
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1448980
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1448981
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1448982
    const v2, -0x7485b11b

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1448983
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1448984
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1448985
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1448986
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1448987
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1448988
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1448989
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1448990
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1448991
    :sswitch_4
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1448992
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1448993
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1448994
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1448995
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7f6bef82 -> :sswitch_4
        -0x7485b11b -> :sswitch_3
        -0x6b59ddfb -> :sswitch_0
        -0x43dda7e0 -> :sswitch_1
        -0x10c4f9b7 -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1448958
    if-nez p0, :cond_0

    move v0, v1

    .line 1448959
    :goto_0
    return v0

    .line 1448960
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 1448961
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 1448962
    :goto_1
    if-ge v1, v2, :cond_2

    .line 1448963
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 1448964
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1448965
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 1448966
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 1448951
    const/4 v7, 0x0

    .line 1448952
    const/4 v1, 0x0

    .line 1448953
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 1448954
    invoke-static {v2, v3, v0}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1448955
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 1448956
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1448957
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1448950
    new-instance v0, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1448943
    sparse-switch p2, :sswitch_data_0

    .line 1448944
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1448945
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1448946
    const v1, -0x10c4f9b7

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1448947
    :goto_0
    :sswitch_1
    return-void

    .line 1448948
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1448949
    const v1, -0x7485b11b

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x7f6bef82 -> :sswitch_1
        -0x7485b11b -> :sswitch_1
        -0x6b59ddfb -> :sswitch_1
        -0x43dda7e0 -> :sswitch_0
        -0x10c4f9b7 -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(LX/15i;IILX/3Sb;I)V
    .locals 2

    .prologue
    .line 1448935
    const/4 v0, 0x0

    .line 1448936
    if-eqz p3, :cond_0

    .line 1448937
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1448938
    invoke-static {p3}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v1

    .line 1448939
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1448940
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    .line 1448941
    :cond_0
    invoke-virtual {p0, p1, p2, v0}, LX/15i;->a(II[B)V

    .line 1448942
    return-void
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1448929
    if-eqz p1, :cond_0

    .line 1448930
    invoke-static {p0, p1, p2}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 1448931
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$DraculaImplementation;

    .line 1448932
    if-eq v0, v1, :cond_0

    .line 1448933
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1448934
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1448928
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1448926
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1448927
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1448921
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1448922
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1448923
    :cond_0
    iput-object p1, p0, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1448924
    iput p2, p0, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$DraculaImplementation;->b:I

    .line 1448925
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1448920
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1448919
    new-instance v0, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1448916
    iget v0, p0, LX/1vt;->c:I

    .line 1448917
    move v0, v0

    .line 1448918
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1448913
    iget v0, p0, LX/1vt;->c:I

    .line 1448914
    move v0, v0

    .line 1448915
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1448910
    iget v0, p0, LX/1vt;->b:I

    .line 1448911
    move v0, v0

    .line 1448912
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1448907
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1448908
    move-object v0, v0

    .line 1448909
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1448898
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1448899
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1448900
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1448901
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1448902
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1448903
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1448904
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1448905
    invoke-static {v3, v9, v2}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1448906
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1448895
    iget v0, p0, LX/1vt;->c:I

    .line 1448896
    move v0, v0

    .line 1448897
    return v0
.end method
