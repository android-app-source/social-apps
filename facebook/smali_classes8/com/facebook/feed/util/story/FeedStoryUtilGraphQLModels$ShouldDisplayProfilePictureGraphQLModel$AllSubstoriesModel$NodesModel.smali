.class public final Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel$AllSubstoriesModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/6XG;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x5ac77b64
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel$AllSubstoriesModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel$AllSubstoriesModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel$AttachmentsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1449207
    const-class v0, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel$AllSubstoriesModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1449206
    const-class v0, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel$AllSubstoriesModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1449202
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1449203
    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel$AttachmentsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1449208
    iput-object p1, p0, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel$AllSubstoriesModel$NodesModel;->e:Ljava/util/List;

    .line 1449209
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1449210
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1449211
    if-eqz v0, :cond_0

    .line 1449212
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/util/List;)V

    .line 1449213
    :cond_0
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1449230
    iget-object v0, p0, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel$AllSubstoriesModel$NodesModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel$AllSubstoriesModel$NodesModel;->f:Ljava/lang/String;

    .line 1449231
    iget-object v0, p0, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel$AllSubstoriesModel$NodesModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1449214
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1449215
    invoke-virtual {p0}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel$AllSubstoriesModel$NodesModel;->b()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1449216
    invoke-direct {p0}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel$AllSubstoriesModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1449217
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1449218
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1449219
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1449220
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1449221
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1449222
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1449223
    invoke-virtual {p0}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel$AllSubstoriesModel$NodesModel;->b()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1449224
    invoke-virtual {p0}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel$AllSubstoriesModel$NodesModel;->b()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1449225
    if-eqz v1, :cond_0

    .line 1449226
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel$AllSubstoriesModel$NodesModel;

    .line 1449227
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel$AllSubstoriesModel$NodesModel;->e:Ljava/util/List;

    .line 1449228
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1449229
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1449204
    new-instance v0, LX/9AD;

    invoke-direct {v0, p1}, LX/9AD;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1449205
    invoke-direct {p0}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel$AllSubstoriesModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1449189
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1449190
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1449191
    const-string v0, "attachments"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1449192
    check-cast p2, Ljava/util/List;

    invoke-direct {p0, p2}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel$AllSubstoriesModel$NodesModel;->a(Ljava/util/List;)V

    .line 1449193
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1449194
    return-void
.end method

.method public final b()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel$AttachmentsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1449195
    iget-object v0, p0, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel$AllSubstoriesModel$NodesModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel$AttachmentsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel$AllSubstoriesModel$NodesModel;->e:Ljava/util/List;

    .line 1449196
    iget-object v0, p0, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel$AllSubstoriesModel$NodesModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1449197
    new-instance v0, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel$AllSubstoriesModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel$AllSubstoriesModel$NodesModel;-><init>()V

    .line 1449198
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1449199
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1449200
    const v0, -0x79789b95

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1449201
    const v0, 0x4c808d5

    return v0
.end method
