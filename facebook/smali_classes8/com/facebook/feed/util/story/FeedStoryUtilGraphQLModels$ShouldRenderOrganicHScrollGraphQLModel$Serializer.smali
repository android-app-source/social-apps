.class public final Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldRenderOrganicHScrollGraphQLModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldRenderOrganicHScrollGraphQLModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1449805
    const-class v0, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldRenderOrganicHScrollGraphQLModel;

    new-instance v1, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldRenderOrganicHScrollGraphQLModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldRenderOrganicHScrollGraphQLModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1449806
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1449807
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldRenderOrganicHScrollGraphQLModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1449808
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1449809
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x1

    .line 1449810
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1449811
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1449812
    if-eqz v2, :cond_0

    .line 1449813
    const-string v3, "all_substories"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1449814
    invoke-static {v1, v2, p1, p2}, LX/9AV;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1449815
    :cond_0
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1449816
    if-eqz v2, :cond_1

    .line 1449817
    const-string v2, "substories_grouping_reasons"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1449818
    invoke-virtual {v1, v0, p0}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1449819
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1449820
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1449821
    check-cast p1, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldRenderOrganicHScrollGraphQLModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldRenderOrganicHScrollGraphQLModel$Serializer;->a(Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldRenderOrganicHScrollGraphQLModel;LX/0nX;LX/0my;)V

    return-void
.end method
