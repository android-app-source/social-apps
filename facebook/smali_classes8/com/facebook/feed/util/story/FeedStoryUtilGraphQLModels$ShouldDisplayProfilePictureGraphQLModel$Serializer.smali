.class public final Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1449450
    const-class v0, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel;

    new-instance v1, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1449451
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1449452
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1449453
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1449454
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1449455
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1449456
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1449457
    if-eqz v2, :cond_1

    .line 1449458
    const-string v3, "actors"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1449459
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1449460
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result p0

    if-ge v3, p0, :cond_0

    .line 1449461
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result p0

    invoke-static {v1, p0, p1, p2}, LX/9AM;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1449462
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1449463
    :cond_0
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1449464
    :cond_1
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1449465
    if-eqz v2, :cond_2

    .line 1449466
    const-string v3, "all_substories"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1449467
    invoke-static {v1, v2, p1, p2}, LX/9AO;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1449468
    :cond_2
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1449469
    if-eqz v2, :cond_3

    .line 1449470
    const-string v3, "attached_story"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1449471
    invoke-static {v1, v2, p1}, LX/9AP;->a(LX/15i;ILX/0nX;)V

    .line 1449472
    :cond_3
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1449473
    if-eqz v2, :cond_4

    .line 1449474
    const-string v3, "attachments"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1449475
    invoke-static {v1, v2, p1, p2}, LX/9AL;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1449476
    :cond_4
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1449477
    if-eqz v2, :cond_5

    .line 1449478
    const-string v3, "feedback"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1449479
    invoke-static {v1, v2, p1}, LX/9AQ;->a(LX/15i;ILX/0nX;)V

    .line 1449480
    :cond_5
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1449481
    if-eqz v2, :cond_6

    .line 1449482
    const-string v3, "has_comprehensive_title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1449483
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1449484
    :cond_6
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1449485
    if-eqz v2, :cond_7

    .line 1449486
    const-string v3, "message"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1449487
    invoke-static {v1, v2, p1}, LX/9AR;->a(LX/15i;ILX/0nX;)V

    .line 1449488
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1449489
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1449490
    check-cast p1, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel$Serializer;->a(Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel;LX/0nX;LX/0my;)V

    return-void
.end method
