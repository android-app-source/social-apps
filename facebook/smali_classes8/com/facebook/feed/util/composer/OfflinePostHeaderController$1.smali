.class public final Lcom/facebook/feed/util/composer/OfflinePostHeaderController$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:LX/9A1;


# direct methods
.method public constructor <init>(LX/9A1;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 0

    .prologue
    .line 1447981
    iput-object p1, p0, Lcom/facebook/feed/util/composer/OfflinePostHeaderController$1;->b:LX/9A1;

    iput-object p2, p0, Lcom/facebook/feed/util/composer/OfflinePostHeaderController$1;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 1447982
    iget-object v0, p0, Lcom/facebook/feed/util/composer/OfflinePostHeaderController$1;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/9A1;->h(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    .line 1447983
    iget-object v1, p0, Lcom/facebook/feed/util/composer/OfflinePostHeaderController$1;->b:LX/9A1;

    iget-object v1, v1, LX/9A1;->h:LX/0qn;

    iget-object v2, p0, Lcom/facebook/feed/util/composer/OfflinePostHeaderController$1;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1, v2}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v1

    .line 1447984
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->DELETED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v1, v2, :cond_0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->SUCCESS:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v1, v2, :cond_0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-ne v1, v2, :cond_1

    .line 1447985
    :cond_0
    iget-object v1, p0, Lcom/facebook/feed/util/composer/OfflinePostHeaderController$1;->b:LX/9A1;

    invoke-static {v1, v0}, LX/9A1;->b(LX/9A1;Ljava/lang/String;)V

    .line 1447986
    :goto_0
    return-void

    .line 1447987
    :cond_1
    iget-object v1, p0, Lcom/facebook/feed/util/composer/OfflinePostHeaderController$1;->b:LX/9A1;

    iget-object v1, v1, LX/9A1;->c:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1447988
    iget-object v0, p0, Lcom/facebook/feed/util/composer/OfflinePostHeaderController$1;->b:LX/9A1;

    iget-object v1, p0, Lcom/facebook/feed/util/composer/OfflinePostHeaderController$1;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0, v1}, LX/9A1;->g(LX/9A1;Lcom/facebook/graphql/model/GraphQLStory;)V

    goto :goto_0
.end method
