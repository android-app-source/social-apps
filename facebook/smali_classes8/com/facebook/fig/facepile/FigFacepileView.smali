.class public final Lcom/facebook/fig/facepile/FigFacepileView;
.super LX/6WN;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final c:[I


# instance fields
.field public a:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0hL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Z

.field private e:Z

.field private f:LX/9Iy;

.field private g:I

.field private h:LX/6WV;

.field private i:LX/6WU;

.field private j:I

.field private k:I

.field private l:I

.field private m:Z

.field private n:Landroid/graphics/drawable/Drawable;

.field private final o:Landroid/graphics/Paint;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1464114
    const/4 v0, 0x3

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f0e0269

    aput v2, v0, v1

    const/4 v1, 0x1

    const v2, 0x7f0e026a

    aput v2, v0, v1

    const/4 v1, 0x2

    const v2, 0x7f0e026b

    aput v2, v0, v1

    sput-object v0, Lcom/facebook/fig/facepile/FigFacepileView;->c:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1464109
    invoke-direct {p0, p1}, LX/6WN;-><init>(Landroid/content/Context;)V

    .line 1464110
    const/4 v0, 0x1

    iput v0, p0, Lcom/facebook/fig/facepile/FigFacepileView;->g:I

    .line 1464111
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/fig/facepile/FigFacepileView;->o:Landroid/graphics/Paint;

    .line 1464112
    invoke-direct {p0, p1}, Lcom/facebook/fig/facepile/FigFacepileView;->a(Landroid/content/Context;)V

    .line 1464113
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1464103
    invoke-direct {p0, p1, p2}, LX/6WN;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1464104
    const/4 v0, 0x1

    iput v0, p0, Lcom/facebook/fig/facepile/FigFacepileView;->g:I

    .line 1464105
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/fig/facepile/FigFacepileView;->o:Landroid/graphics/Paint;

    .line 1464106
    invoke-direct {p0, p1}, Lcom/facebook/fig/facepile/FigFacepileView;->a(Landroid/content/Context;)V

    .line 1464107
    invoke-direct {p0, p2}, Lcom/facebook/fig/facepile/FigFacepileView;->a(Landroid/util/AttributeSet;)V

    .line 1464108
    return-void
.end method

.method private a()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1464095
    invoke-virtual {p0}, LX/6WN;->getNumDraweeControllers()I

    move-result v2

    move v0, v1

    .line 1464096
    :goto_0
    if-ge v0, v2, :cond_1

    .line 1464097
    iget-object v3, p0, Lcom/facebook/fig/facepile/FigFacepileView;->f:LX/9Iy;

    invoke-virtual {v3, v0}, LX/9Iy;->a(I)LX/9Ix;

    move-result-object v3

    .line 1464098
    iget-object v4, v3, LX/9Ix;->b:Landroid/graphics/drawable/Drawable;

    move-object v3, v4

    .line 1464099
    if-eqz v3, :cond_0

    .line 1464100
    iget v4, p0, Lcom/facebook/fig/facepile/FigFacepileView;->l:I

    add-int/lit8 v4, v4, -0x1

    iget v5, p0, Lcom/facebook/fig/facepile/FigFacepileView;->l:I

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v3, v1, v1, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1464101
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1464102
    :cond_1
    return-void
.end method

.method private a(I[I)V
    .locals 7
    .param p1    # I
        .annotation build Landroid/support/annotation/StyleRes;
        .end annotation
    .end param
    .param p2    # [I
        .annotation build Landroid/support/annotation/StyleableRes;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1464052
    invoke-virtual {p0}, Lcom/facebook/fig/facepile/FigFacepileView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 1464053
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v3

    move v0, v1

    .line 1464054
    :goto_0
    if-ge v0, v3, :cond_c

    .line 1464055
    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v4

    .line 1464056
    const/16 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 1464057
    invoke-virtual {v2, v4, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    .line 1464058
    iget-object v5, p0, LX/6WN;->d:Landroid/graphics/Paint;

    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 1464059
    invoke-virtual {p0}, LX/6WN;->invalidate()V

    .line 1464060
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1464061
    :cond_1
    const/16 v5, 0x2

    if-ne v4, v5, :cond_2

    .line 1464062
    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v4

    .line 1464063
    iget-object v5, p0, LX/6WN;->d:Landroid/graphics/Paint;

    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1464064
    invoke-virtual {p0}, LX/6WN;->invalidate()V

    .line 1464065
    goto :goto_1

    .line 1464066
    :cond_2
    const/16 v5, 0x3

    if-ne v4, v5, :cond_3

    .line 1464067
    iget-object v5, p0, Lcom/facebook/fig/facepile/FigFacepileView;->o:Landroid/graphics/Paint;

    invoke-virtual {v2, v4, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_1

    .line 1464068
    :cond_3
    const/16 v5, 0x4

    if-ne v4, v5, :cond_4

    .line 1464069
    invoke-virtual {v2, v4, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v5

    .line 1464070
    iget-object v6, p0, Lcom/facebook/fig/facepile/FigFacepileView;->h:LX/6WV;

    invoke-virtual {v6, v5}, LX/6WU;->e(I)V

    .line 1464071
    iget-object v5, p0, Lcom/facebook/fig/facepile/FigFacepileView;->n:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v4, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    invoke-static {v5, v4}, LX/1d5;->a(Landroid/graphics/drawable/Drawable;I)V

    goto :goto_1

    .line 1464072
    :cond_4
    const/16 v5, 0x5

    if-ne v4, v5, :cond_5

    .line 1464073
    iget-object v5, p0, Lcom/facebook/fig/facepile/FigFacepileView;->h:LX/6WV;

    invoke-virtual {v2, v4, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    invoke-virtual {v5, v4}, LX/6WU;->d(I)V

    goto :goto_1

    .line 1464074
    :cond_5
    const/16 v5, 0x6

    if-ne v4, v5, :cond_6

    .line 1464075
    invoke-virtual {v2, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 1464076
    iput-object v4, p0, LX/6WN;->e:Landroid/graphics/drawable/Drawable;

    .line 1464077
    goto :goto_1

    .line 1464078
    :cond_6
    const/16 v5, 0x7

    if-ne v4, v5, :cond_7

    .line 1464079
    invoke-virtual {v2, v4, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    iput v4, p0, Lcom/facebook/fig/facepile/FigFacepileView;->j:I

    goto :goto_1

    .line 1464080
    :cond_7
    const/16 v5, 0x8

    if-ne v4, v5, :cond_8

    .line 1464081
    invoke-virtual {v2, v4, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    iput v4, p0, Lcom/facebook/fig/facepile/FigFacepileView;->k:I

    goto :goto_1

    .line 1464082
    :cond_8
    const/16 v5, 0x0

    if-ne v4, v5, :cond_9

    .line 1464083
    invoke-virtual {v2, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-static {v4}, LX/1d5;->c(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iput-object v4, p0, Lcom/facebook/fig/facepile/FigFacepileView;->n:Landroid/graphics/drawable/Drawable;

    .line 1464084
    iget-object v4, p0, Lcom/facebook/fig/facepile/FigFacepileView;->n:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    .line 1464085
    iget-object v5, p0, Lcom/facebook/fig/facepile/FigFacepileView;->n:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    .line 1464086
    iget-object v6, p0, Lcom/facebook/fig/facepile/FigFacepileView;->n:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, v1, v1, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto/16 :goto_1

    .line 1464087
    :cond_9
    const/16 v5, 0xa

    if-ne v4, v5, :cond_a

    .line 1464088
    iget-object v5, p0, Lcom/facebook/fig/facepile/FigFacepileView;->i:LX/6WU;

    invoke-virtual {v2, v4, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    invoke-virtual {v5, v4}, LX/6WU;->d(I)V

    goto/16 :goto_1

    .line 1464089
    :cond_a
    const/16 v5, 0x9

    if-ne v4, v5, :cond_b

    .line 1464090
    iget-object v5, p0, Lcom/facebook/fig/facepile/FigFacepileView;->i:LX/6WU;

    invoke-virtual {v2, v4, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    invoke-virtual {v5, v4}, LX/6WU;->e(I)V

    goto/16 :goto_1

    .line 1464091
    :cond_b
    const/16 v5, 0xb

    if-ne v4, v5, :cond_0

    .line 1464092
    invoke-virtual {v2, v4, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    iput v4, p0, Lcom/facebook/fig/facepile/FigFacepileView;->l:I

    goto/16 :goto_1

    .line 1464093
    :cond_c
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 1464094
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1464042
    const-class v0, Lcom/facebook/fig/facepile/FigFacepileView;

    invoke-static {v0, p0}, Lcom/facebook/fig/facepile/FigFacepileView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1464043
    new-instance v0, LX/6WV;

    invoke-direct {v0, p1}, LX/6WV;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/fig/facepile/FigFacepileView;->h:LX/6WV;

    .line 1464044
    new-instance v0, LX/6WU;

    invoke-direct {v0}, LX/6WU;-><init>()V

    iput-object v0, p0, Lcom/facebook/fig/facepile/FigFacepileView;->i:LX/6WU;

    .line 1464045
    iget-object v0, p0, Lcom/facebook/fig/facepile/FigFacepileView;->i:LX/6WU;

    .line 1464046
    iget-object p1, v0, LX/6WU;->d:LX/1nq;

    invoke-virtual {p1, v1}, LX/1nq;->f(I)LX/1nq;

    .line 1464047
    const/4 p1, 0x1

    iput-boolean p1, v0, LX/6WU;->a:Z

    .line 1464048
    if-eqz v1, :cond_0

    iget v0, p0, LX/6WN;->f:I

    or-int/lit8 v0, v0, 0x1

    :goto_0
    iput v0, p0, LX/6WN;->f:I

    .line 1464049
    iget-object v0, p0, Lcom/facebook/fig/facepile/FigFacepileView;->a:LX/1Ad;

    const-class v1, Lcom/facebook/fig/facepile/FigFacepileView;

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    .line 1464050
    return-void

    .line 1464051
    :cond_0
    iget v0, p0, LX/6WN;->f:I

    and-int/lit8 v0, v0, -0x2

    goto :goto_0
.end method

.method private a(Landroid/graphics/Canvas;Z)V
    .locals 4

    .prologue
    .line 1464032
    invoke-virtual {p0}, LX/6WN;->getNumDraweeControllers()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, LX/6WN;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 1464033
    iget-object v1, p0, Lcom/facebook/fig/facepile/FigFacepileView;->o:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1464034
    if-eqz p2, :cond_0

    .line 1464035
    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/fig/facepile/FigFacepileView;->n:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    shr-int/lit8 v2, v2, 0x1

    sub-int/2addr v1, v2

    .line 1464036
    invoke-virtual {v0}, Landroid/graphics/Rect;->centerY()I

    move-result v0

    iget-object v2, p0, Lcom/facebook/fig/facepile/FigFacepileView;->n:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    shr-int/lit8 v2, v2, 0x1

    sub-int/2addr v0, v2

    .line 1464037
    int-to-float v2, v1

    int-to-float v3, v0

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1464038
    iget-object v2, p0, Lcom/facebook/fig/facepile/FigFacepileView;->n:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1464039
    neg-int v1, v1

    int-to-float v1, v1

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1464040
    :goto_0
    return-void

    .line 1464041
    :cond_0
    iget-object v0, p0, Lcom/facebook/fig/facepile/FigFacepileView;->h:LX/6WV;

    invoke-virtual {v0, p1}, LX/6WU;->a(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method private a(Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 1464027
    invoke-virtual {p0}, Lcom/facebook/fig/facepile/FigFacepileView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/03r;->FigFacepileView:[I

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1464028
    const/16 v1, 0x0

    iget v2, p0, Lcom/facebook/fig/facepile/FigFacepileView;->k:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    .line 1464029
    invoke-virtual {p0, v1}, Lcom/facebook/fig/facepile/FigFacepileView;->setFaceSize(I)V

    .line 1464030
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1464031
    return-void
.end method

.method private static a(Lcom/facebook/fig/facepile/FigFacepileView;LX/1Ad;LX/0hL;)V
    .locals 0

    .prologue
    .line 1464026
    iput-object p1, p0, Lcom/facebook/fig/facepile/FigFacepileView;->a:LX/1Ad;

    iput-object p2, p0, Lcom/facebook/fig/facepile/FigFacepileView;->b:LX/0hL;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/fig/facepile/FigFacepileView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/fig/facepile/FigFacepileView;

    invoke-static {v1}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-static {v1}, LX/0hL;->a(LX/0QB;)LX/0hL;

    move-result-object v1

    check-cast v1, LX/0hL;

    invoke-static {p0, v0, v1}, Lcom/facebook/fig/facepile/FigFacepileView;->a(Lcom/facebook/fig/facepile/FigFacepileView;LX/1Ad;LX/0hL;)V

    return-void
.end method

.method private b(Landroid/graphics/Canvas;Z)V
    .locals 7

    .prologue
    .line 1463883
    if-eqz p2, :cond_1

    invoke-virtual {p0}, LX/6WN;->getNumDraweeControllers()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 1463884
    :goto_0
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v0, :cond_2

    .line 1463885
    iget-object v2, p0, Lcom/facebook/fig/facepile/FigFacepileView;->f:LX/9Iy;

    invoke-virtual {v2, v1}, LX/9Iy;->a(I)LX/9Ix;

    move-result-object v2

    .line 1463886
    iget-object v3, v2, LX/9Ix;->b:Landroid/graphics/drawable/Drawable;

    move-object v2, v3

    .line 1463887
    if-eqz v2, :cond_0

    .line 1463888
    invoke-virtual {p0, v1}, LX/6WN;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v3

    .line 1463889
    iget v4, v3, Landroid/graphics/Rect;->right:I

    iget v5, p0, Lcom/facebook/fig/facepile/FigFacepileView;->j:I

    add-int/2addr v4, v5

    iget v5, p0, Lcom/facebook/fig/facepile/FigFacepileView;->l:I

    sub-int/2addr v4, v5

    add-int/lit8 v4, v4, -0x1

    .line 1463890
    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    iget v5, p0, Lcom/facebook/fig/facepile/FigFacepileView;->j:I

    add-int/2addr v3, v5

    iget v5, p0, Lcom/facebook/fig/facepile/FigFacepileView;->l:I

    sub-int/2addr v3, v5

    add-int/lit8 v3, v3, -0x1

    .line 1463891
    int-to-float v5, v4

    int-to-float v6, v3

    invoke-virtual {p1, v5, v6}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1463892
    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1463893
    neg-int v2, v4

    int-to-float v2, v2

    neg-int v3, v3

    int-to-float v3, v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1463894
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1463895
    :cond_1
    invoke-virtual {p0}, LX/6WN;->getNumDraweeControllers()I

    move-result v0

    goto :goto_0

    .line 1463896
    :cond_2
    return-void
.end method

.method private d(I)V
    .locals 4

    .prologue
    .line 1464017
    invoke-virtual {p0, p1}, LX/6WN;->setCapacity(I)V

    .line 1464018
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 1464019
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_0

    .line 1464020
    iget-object v2, p0, Lcom/facebook/fig/facepile/FigFacepileView;->f:LX/9Iy;

    invoke-virtual {v2, v0}, LX/9Iy;->a(I)LX/9Ix;

    move-result-object v2

    .line 1464021
    iget-object v3, v2, LX/9Ix;->a:Ljava/lang/String;

    move-object v2, v3

    .line 1464022
    iget-object v3, p0, Lcom/facebook/fig/facepile/FigFacepileView;->a:LX/1Ad;

    invoke-virtual {v3, v2}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v2

    invoke-virtual {v2}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1464023
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1464024
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/6WN;->setDraweeControllers(LX/0Px;)V

    .line 1464025
    return-void
.end method


# virtual methods
.method public final c(I)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1463997
    invoke-virtual {p0}, Lcom/facebook/fig/facepile/FigFacepileView;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1463998
    :cond_0
    :goto_0
    return-object v0

    .line 1463999
    :cond_1
    iget-object v1, p0, Lcom/facebook/fig/facepile/FigFacepileView;->h:LX/6WV;

    .line 1464000
    iget v2, v1, LX/6WV;->b:I

    move v1, v2

    .line 1464001
    if-lez v1, :cond_2

    invoke-virtual {p0}, LX/6WN;->getNumDraweeControllers()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne p1, v1, :cond_2

    .line 1464002
    invoke-virtual {p0}, Lcom/facebook/fig/facepile/FigFacepileView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08014e

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/facebook/fig/facepile/FigFacepileView;->h:LX/6WV;

    .line 1464003
    iget v5, v3, LX/6WV;->b:I

    move v3, v5

    .line 1464004
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1464005
    :cond_2
    invoke-virtual {p0}, LX/6WN;->getNumDraweeControllers()I

    move-result v1

    if-ne p1, v1, :cond_3

    .line 1464006
    iget-object v0, p0, Lcom/facebook/fig/facepile/FigFacepileView;->f:LX/9Iy;

    .line 1464007
    iget-object v1, v0, LX/9Iy;->b:Ljava/lang/CharSequence;

    move-object v0, v1

    .line 1464008
    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 1464009
    :cond_3
    iget-object v1, p0, Lcom/facebook/fig/facepile/FigFacepileView;->f:LX/9Iy;

    invoke-virtual {v1, p1}, LX/9Iy;->a(I)LX/9Ix;

    move-result-object v2

    .line 1464010
    iget-object v1, v2, LX/9Ix;->c:Ljava/lang/String;

    move-object v1, v1

    .line 1464011
    iget-object v5, v2, LX/9Ix;->d:Ljava/lang/String;

    move-object v5, v5

    .line 1464012
    iget v2, p0, Lcom/facebook/fig/facepile/FigFacepileView;->l:I

    if-eqz v2, :cond_4

    move v2, v3

    .line 1464013
    :goto_1
    if-eqz v1, :cond_0

    .line 1464014
    if-eqz v2, :cond_5

    if-eqz v5, :cond_5

    invoke-virtual {p0}, Lcom/facebook/fig/facepile/FigFacepileView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f08014d

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v1, v6, v4

    aput-object v5, v6, v3

    invoke-virtual {v0, v2, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    move v2, v4

    .line 1464015
    goto :goto_1

    :cond_5
    move-object v0, v1

    .line 1464016
    goto :goto_0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/16 v0, 0x2c

    const v1, 0x4666723b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1463991
    invoke-super {p0}, LX/6WN;->onAttachedToWindow()V

    .line 1463992
    iput-boolean v2, p0, Lcom/facebook/fig/facepile/FigFacepileView;->m:Z

    .line 1463993
    iget-boolean v1, p0, Lcom/facebook/fig/facepile/FigFacepileView;->e:Z

    if-nez v1, :cond_0

    .line 1463994
    iput-boolean v2, p0, Lcom/facebook/fig/facepile/FigFacepileView;->e:Z

    .line 1463995
    sget-object v1, Lcom/facebook/fig/facepile/FigFacepileView;->c:[I

    iget v2, p0, Lcom/facebook/fig/facepile/FigFacepileView;->g:I

    aget v1, v1, v2

    sget-object v2, LX/03r;->FigFacepileViewAttrs:[I

    invoke-direct {p0, v1, v2}, Lcom/facebook/fig/facepile/FigFacepileView;->a(I[I)V

    .line 1463996
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x16d954fe

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 1463977
    iget-object v0, p0, Lcom/facebook/fig/facepile/FigFacepileView;->f:LX/9Iy;

    if-eqz v0, :cond_2

    .line 1463978
    iget-object v0, p0, Lcom/facebook/fig/facepile/FigFacepileView;->f:LX/9Iy;

    invoke-virtual {v0}, LX/9Iy;->c()Z

    move-result v1

    .line 1463979
    iget-object v0, p0, Lcom/facebook/fig/facepile/FigFacepileView;->h:LX/6WV;

    .line 1463980
    iget v2, v0, LX/6WV;->b:I

    move v0, v2

    .line 1463981
    if-lez v0, :cond_3

    const/4 v0, 0x1

    .line 1463982
    :goto_0
    invoke-super {p0, p1}, LX/6WN;->onDraw(Landroid/graphics/Canvas;)V

    .line 1463983
    if-eqz v0, :cond_0

    .line 1463984
    invoke-direct {p0, p1, v1}, Lcom/facebook/fig/facepile/FigFacepileView;->a(Landroid/graphics/Canvas;Z)V

    .line 1463985
    :cond_0
    iget v2, p0, Lcom/facebook/fig/facepile/FigFacepileView;->l:I

    if-eqz v2, :cond_1

    .line 1463986
    invoke-direct {p0, p1, v0}, Lcom/facebook/fig/facepile/FigFacepileView;->b(Landroid/graphics/Canvas;Z)V

    .line 1463987
    :cond_1
    if-eqz v1, :cond_2

    .line 1463988
    iget-object v0, p0, Lcom/facebook/fig/facepile/FigFacepileView;->i:LX/6WU;

    invoke-virtual {v0, p1}, LX/6WU;->a(Landroid/graphics/Canvas;)V

    .line 1463989
    :cond_2
    return-void

    .line 1463990
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onLayout(ZIIII)V
    .locals 12

    .prologue
    .line 1463941
    iget-object v0, p0, Lcom/facebook/fig/facepile/FigFacepileView;->f:LX/9Iy;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    iget-boolean v0, p0, Lcom/facebook/fig/facepile/FigFacepileView;->d:Z

    if-nez v0, :cond_1

    .line 1463942
    :cond_0
    :goto_0
    return-void

    .line 1463943
    :cond_1
    iget v0, p0, Lcom/facebook/fig/facepile/FigFacepileView;->k:I

    iget v1, p0, Lcom/facebook/fig/facepile/FigFacepileView;->j:I

    add-int v2, v0, v1

    .line 1463944
    invoke-virtual {p0}, Lcom/facebook/fig/facepile/FigFacepileView;->getPaddingRight()I

    move-result v0

    .line 1463945
    invoke-virtual {p0}, Lcom/facebook/fig/facepile/FigFacepileView;->getPaddingLeft()I

    move-result v1

    .line 1463946
    sub-int v3, p4, p2

    sub-int/2addr v3, v1

    sub-int/2addr v3, v0

    add-int/lit8 v3, v3, 0x1

    .line 1463947
    iget v4, p0, Lcom/facebook/fig/facepile/FigFacepileView;->j:I

    add-int/2addr v3, v4

    div-int/2addr v3, v2

    .line 1463948
    iget-object v4, p0, Lcom/facebook/fig/facepile/FigFacepileView;->f:LX/9Iy;

    invoke-virtual {v4}, LX/9Iy;->a()I

    move-result v4

    invoke-static {v4, v3}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 1463949
    invoke-virtual {p0}, Lcom/facebook/fig/facepile/FigFacepileView;->getPaddingTop()I

    move-result v5

    .line 1463950
    iget v6, p0, Lcom/facebook/fig/facepile/FigFacepileView;->k:I

    add-int/2addr v6, v5

    .line 1463951
    iget-boolean v7, p0, Lcom/facebook/fig/facepile/FigFacepileView;->d:Z

    if-nez v7, :cond_2

    invoke-virtual {p0}, LX/6WN;->getNumDraweeControllers()I

    move-result v7

    iget-object v8, p0, Lcom/facebook/fig/facepile/FigFacepileView;->f:LX/9Iy;

    invoke-virtual {v8}, LX/9Iy;->a()I

    move-result v8

    if-eq v7, v8, :cond_3

    .line 1463952
    :cond_2
    invoke-direct {p0, v4}, Lcom/facebook/fig/facepile/FigFacepileView;->d(I)V

    .line 1463953
    iget-object v7, p0, Lcom/facebook/fig/facepile/FigFacepileView;->f:LX/9Iy;

    invoke-virtual {v7}, LX/9Iy;->c()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1463954
    iget v7, p0, Lcom/facebook/fig/facepile/FigFacepileView;->j:I

    mul-int/lit8 v7, v7, 0x2

    add-int/2addr v7, v6

    .line 1463955
    iget-object v8, p0, Lcom/facebook/fig/facepile/FigFacepileView;->i:LX/6WU;

    invoke-virtual {v8}, LX/6WU;->c()I

    move-result v8

    add-int/2addr v8, v7

    .line 1463956
    iget-object v9, p0, Lcom/facebook/fig/facepile/FigFacepileView;->i:LX/6WU;

    add-int v10, p2, v1

    sub-int v11, p4, v0

    invoke-virtual {v9, v10, v7, v11, v8}, LX/6WU;->a(IIII)V

    .line 1463957
    :cond_3
    iget-object v7, p0, Lcom/facebook/fig/facepile/FigFacepileView;->b:LX/0hL;

    invoke-virtual {v7}, LX/0hL;->a()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1463958
    sub-int v1, p4, v0

    .line 1463959
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v4, :cond_5

    .line 1463960
    invoke-virtual {p0, v0}, LX/6WN;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    iget v8, p0, Lcom/facebook/fig/facepile/FigFacepileView;->k:I

    sub-int v8, v1, v8

    invoke-virtual {v7, v8, v5, v1, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1463961
    sub-int/2addr v1, v2

    .line 1463962
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1463963
    :cond_4
    iget-object v0, p0, Lcom/facebook/fig/facepile/FigFacepileView;->i:LX/6WU;

    const/4 v7, 0x1

    invoke-virtual {v0, v7}, LX/6WU;->a(Z)V

    .line 1463964
    add-int/2addr v1, p2

    .line 1463965
    const/4 v0, 0x0

    :goto_2
    if-ge v0, v4, :cond_5

    .line 1463966
    invoke-virtual {p0, v0}, LX/6WN;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    iget v8, p0, Lcom/facebook/fig/facepile/FigFacepileView;->k:I

    add-int/2addr v8, v1

    invoke-virtual {v7, v1, v5, v8, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1463967
    add-int/2addr v1, v2

    .line 1463968
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1463969
    :cond_5
    iget-object v0, p0, Lcom/facebook/fig/facepile/FigFacepileView;->f:LX/9Iy;

    invoke-virtual {v0}, LX/9Iy;->a()I

    move-result v0

    sub-int/2addr v0, v3

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget-object v1, p0, Lcom/facebook/fig/facepile/FigFacepileView;->f:LX/9Iy;

    invoke-virtual {v1}, LX/9Iy;->d()I

    move-result v1

    add-int/2addr v0, v1

    .line 1463970
    iget-object v1, p0, Lcom/facebook/fig/facepile/FigFacepileView;->h:LX/6WV;

    invoke-virtual {v1, v0}, LX/6WV;->a(I)V

    .line 1463971
    if-lez v0, :cond_6

    .line 1463972
    iget-object v0, p0, Lcom/facebook/fig/facepile/FigFacepileView;->h:LX/6WV;

    invoke-virtual {p0}, LX/6WN;->getNumDraweeControllers()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, LX/6WN;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6WU;->a(Landroid/graphics/Rect;)V

    .line 1463973
    :cond_6
    iget-boolean v0, p0, Lcom/facebook/fig/facepile/FigFacepileView;->d:Z

    if-eqz v0, :cond_7

    iget v0, p0, Lcom/facebook/fig/facepile/FigFacepileView;->l:I

    if-eqz v0, :cond_7

    .line 1463974
    invoke-direct {p0}, Lcom/facebook/fig/facepile/FigFacepileView;->a()V

    .line 1463975
    :cond_7
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/fig/facepile/FigFacepileView;->d:Z

    .line 1463976
    new-instance v0, LX/9Iz;

    iget-object v1, p0, Lcom/facebook/fig/facepile/FigFacepileView;->i:LX/6WU;

    invoke-virtual {p0}, LX/6WN;->getNumDraweeControllers()I

    move-result v2

    invoke-direct {v0, p0, v1, v2}, LX/9Iz;-><init>(Lcom/facebook/fig/facepile/FigFacepileView;LX/6WU;I)V

    invoke-virtual {p0, v0}, LX/6WN;->setAccessibilityHelper(LX/6WT;)V

    goto/16 :goto_0
.end method

.method public final onMeasure(II)V
    .locals 6

    .prologue
    .line 1463922
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 1463923
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 1463924
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 1463925
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 1463926
    if-nez v1, :cond_0

    .line 1463927
    invoke-virtual {p0}, Lcom/facebook/fig/facepile/FigFacepileView;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/fig/facepile/FigFacepileView;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    .line 1463928
    iget-object v1, p0, Lcom/facebook/fig/facepile/FigFacepileView;->f:LX/9Iy;

    invoke-virtual {v1}, LX/9Iy;->a()I

    move-result v1

    iget v4, p0, Lcom/facebook/fig/facepile/FigFacepileView;->k:I

    iget v5, p0, Lcom/facebook/fig/facepile/FigFacepileView;->j:I

    add-int/2addr v4, v5

    mul-int/2addr v1, v4

    add-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/fig/facepile/FigFacepileView;->j:I

    sub-int/2addr v0, v1

    .line 1463929
    :cond_0
    iget-boolean v1, p0, Lcom/facebook/fig/facepile/FigFacepileView;->d:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/fig/facepile/FigFacepileView;->f:LX/9Iy;

    invoke-virtual {v1}, LX/9Iy;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1463930
    iget-object v1, p0, Lcom/facebook/fig/facepile/FigFacepileView;->i:LX/6WU;

    iget-object v4, p0, Lcom/facebook/fig/facepile/FigFacepileView;->f:LX/9Iy;

    .line 1463931
    iget-object v5, v4, LX/9Iy;->b:Ljava/lang/CharSequence;

    move-object v4, v5

    .line 1463932
    invoke-virtual {v1, v4}, LX/6WU;->a(Ljava/lang/CharSequence;)V

    .line 1463933
    :cond_1
    iget-object v1, p0, Lcom/facebook/fig/facepile/FigFacepileView;->i:LX/6WU;

    invoke-virtual {v1}, LX/6WU;->d()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/facebook/fig/facepile/FigFacepileView;->i:LX/6WU;

    invoke-virtual {v1}, LX/6WU;->c()I

    move-result v1

    iget v4, p0, Lcom/facebook/fig/facepile/FigFacepileView;->j:I

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v1, v4

    .line 1463934
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/fig/facepile/FigFacepileView;->getPaddingTop()I

    move-result v4

    iget v5, p0, Lcom/facebook/fig/facepile/FigFacepileView;->k:I

    add-int/2addr v4, v5

    add-int/2addr v1, v4

    invoke-virtual {p0}, Lcom/facebook/fig/facepile/FigFacepileView;->getPaddingBottom()I

    move-result v4

    add-int/2addr v1, v4

    .line 1463935
    const/high16 v4, -0x80000000

    if-ne v3, v4, :cond_4

    .line 1463936
    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1463937
    :cond_2
    :goto_1
    invoke-virtual {p0, v0, v1}, Lcom/facebook/fig/facepile/FigFacepileView;->setMeasuredDimension(II)V

    .line 1463938
    return-void

    .line 1463939
    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .line 1463940
    :cond_4
    if-eqz v3, :cond_2

    move v1, v2

    goto :goto_1
.end method

.method public final setFaceSize(I)V
    .locals 2

    .prologue
    .line 1463916
    iget-boolean v0, p0, Lcom/facebook/fig/facepile/FigFacepileView;->m:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Can\'t change size after view is in use"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1463917
    iput p1, p0, Lcom/facebook/fig/facepile/FigFacepileView;->g:I

    .line 1463918
    invoke-virtual {p0}, Lcom/facebook/fig/facepile/FigFacepileView;->invalidate()V

    .line 1463919
    invoke-virtual {p0}, Lcom/facebook/fig/facepile/FigFacepileView;->requestLayout()V

    .line 1463920
    return-void

    .line 1463921
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final setModel(LX/9Iy;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 1463902
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1463903
    iget-object v0, p0, Lcom/facebook/fig/facepile/FigFacepileView;->f:LX/9Iy;

    if-eq p1, v0, :cond_0

    .line 1463904
    iput-object p1, p0, Lcom/facebook/fig/facepile/FigFacepileView;->f:LX/9Iy;

    .line 1463905
    iput-boolean v5, p0, Lcom/facebook/fig/facepile/FigFacepileView;->d:Z

    .line 1463906
    invoke-virtual {p0}, Lcom/facebook/fig/facepile/FigFacepileView;->invalidate()V

    .line 1463907
    invoke-virtual {p0}, Lcom/facebook/fig/facepile/FigFacepileView;->requestLayout()V

    .line 1463908
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/fig/facepile/FigFacepileView;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/fig/facepile/FigFacepileView;->f:LX/9Iy;

    .line 1463909
    iget-object v1, v0, LX/9Iy;->b:Ljava/lang/CharSequence;

    move-object v0, v1

    .line 1463910
    if-eqz v0, :cond_1

    .line 1463911
    invoke-virtual {p0}, Lcom/facebook/fig/facepile/FigFacepileView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08014f

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/facebook/fig/facepile/FigFacepileView;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v4

    aput-object v4, v2, v3

    iget-object v3, p0, Lcom/facebook/fig/facepile/FigFacepileView;->f:LX/9Iy;

    .line 1463912
    iget-object v4, v3, LX/9Iy;->b:Ljava/lang/CharSequence;

    move-object v3, v4

    .line 1463913
    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1463914
    invoke-virtual {p0, v0}, Lcom/facebook/fig/facepile/FigFacepileView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1463915
    :cond_1
    return-void
.end method

.method public final setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 1463897
    iget-object v0, p0, LX/6WN;->g:LX/6WT;

    move-object v0, v0

    .line 1463898
    const/4 v1, 0x1

    .line 1463899
    iput-boolean v1, v0, LX/6WT;->d:Z

    .line 1463900
    invoke-super {p0, p1}, LX/6WN;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1463901
    return-void
.end method
