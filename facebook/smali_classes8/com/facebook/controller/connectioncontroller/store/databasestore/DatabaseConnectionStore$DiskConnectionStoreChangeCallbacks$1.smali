.class public final Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$DiskConnectionStoreChangeCallbacks$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/2nf;

.field public final synthetic b:LX/95l;


# direct methods
.method public constructor <init>(LX/95l;LX/2nf;)V
    .locals 0

    .prologue
    .line 1437484
    iput-object p1, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$DiskConnectionStoreChangeCallbacks$1;->b:LX/95l;

    iput-object p2, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$DiskConnectionStoreChangeCallbacks$1;->a:LX/2nf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mStateChangeLock"
    .end annotation

    .prologue
    const/4 v2, 0x0

    const v6, 0x920009

    .line 1437466
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$DiskConnectionStoreChangeCallbacks$1;->b:LX/95l;

    iget-object v0, v0, LX/95l;->a:LX/95n;

    iget-object v0, v0, LX/95n;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v7

    .line 1437467
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$DiskConnectionStoreChangeCallbacks$1;->b:LX/95l;

    iget-object v0, v0, LX/95l;->a:LX/95n;

    iget-object v0, v0, LX/95n;->m:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v6, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 1437468
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$DiskConnectionStoreChangeCallbacks$1;->b:LX/95l;

    iget-object v0, v0, LX/95l;->a:LX/95n;

    iget-object v0, v0, LX/95n;->m:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v1, "DatabaseConnectionStore"

    invoke-interface {v0, v6, v7, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 1437469
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$DiskConnectionStoreChangeCallbacks$1;->b:LX/95l;

    iget-object v0, v0, LX/95l;->a:LX/95n;

    iget-object v0, v0, LX/95n;->m:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/16 v1, 0xbd

    invoke-interface {v0, v6, v7, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 1437470
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$DiskConnectionStoreChangeCallbacks$1;->b:LX/95l;

    iget-object v0, v0, LX/95l;->a:LX/95n;

    .line 1437471
    iget-boolean v1, v0, LX/95n;->w:Z

    move v0, v1

    .line 1437472
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$DiskConnectionStoreChangeCallbacks$1;->b:LX/95l;

    iget-object v0, v0, LX/95l;->a:LX/95n;

    iget-object v0, v0, LX/95n;->y:LX/95j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$DiskConnectionStoreChangeCallbacks$1;->b:LX/95l;

    iget-object v0, v0, LX/95l;->a:LX/95n;

    iget-object v0, v0, LX/95n;->y:LX/95j;

    invoke-virtual {v0}, LX/95j;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1437473
    :cond_0
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$DiskConnectionStoreChangeCallbacks$1;->b:LX/95l;

    iget-object v0, v0, LX/95l;->a:LX/95n;

    iget-object v0, v0, LX/95n;->m:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v1, 0x4

    invoke-interface {v0, v6, v7, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 1437474
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$DiskConnectionStoreChangeCallbacks$1;->a:LX/2nf;

    invoke-interface {v0}, LX/2nf;->close()V

    .line 1437475
    :goto_0
    return-void

    .line 1437476
    :cond_1
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$DiskConnectionStoreChangeCallbacks$1;->b:LX/95l;

    iget-object v0, v0, LX/95l;->a:LX/95n;

    iget-object v1, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$DiskConnectionStoreChangeCallbacks$1;->a:LX/2nf;

    invoke-static {v0, v1}, LX/95n;->a$redex0(LX/95n;LX/2nf;)LX/95j;

    move-result-object v0

    .line 1437477
    iget-object v1, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$DiskConnectionStoreChangeCallbacks$1;->b:LX/95l;

    iget-object v1, v1, LX/95l;->a:LX/95n;

    iget-object v1, v1, LX/95n;->y:LX/95j;

    .line 1437478
    iget-object v3, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$DiskConnectionStoreChangeCallbacks$1;->a:LX/2nf;

    invoke-interface {v3}, LX/2nf;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    .line 1437479
    const-string v3, "DELETED_ROW_IDS"

    invoke-virtual {v4, v3}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v3

    const-string v5, "CHANGED_ROW_IDS"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v5

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, LX/95j;->a(LX/95j;LX/2nj;[J[J[J)LX/0Px;

    move-result-object v4

    .line 1437480
    iget-object v2, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$DiskConnectionStoreChangeCallbacks$1;->a:LX/2nf;

    invoke-interface {v2}, LX/2nf;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$DiskConnectionStoreChangeCallbacks$1;->b:LX/95l;

    iget-object v3, v3, LX/95l;->a:LX/95n;

    iget-object v3, v3, LX/95n;->p:LX/03V;

    iget-object v5, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$DiskConnectionStoreChangeCallbacks$1;->b:LX/95l;

    iget-object v5, v5, LX/95l;->a:LX/95n;

    iget-object v5, v5, LX/95n;->n:LX/1BA;

    invoke-static {v1, v0, v2, v3, v5}, LX/95n;->b(LX/95j;LX/95j;Landroid/os/Bundle;LX/03V;LX/1BA;)Z

    .line 1437481
    iget-object v2, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$DiskConnectionStoreChangeCallbacks$1;->b:LX/95l;

    iget-object v2, v2, LX/95l;->a:LX/95n;

    invoke-static {v2, v0}, LX/95n;->c(LX/95n;LX/95j;)LX/95j;

    .line 1437482
    iget-object v2, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$DiskConnectionStoreChangeCallbacks$1;->b:LX/95l;

    iget-object v2, v2, LX/95l;->a:LX/95n;

    iget-object v2, v2, LX/95n;->m:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v3, 0x2

    invoke-interface {v2, v6, v7, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 1437483
    iget-object v2, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$DiskConnectionStoreChangeCallbacks$1;->b:LX/95l;

    iget-object v8, v2, LX/95l;->a:LX/95n;

    new-instance v2, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$DiskConnectionStoreChangeCallbacks$1$1;

    move-object v3, p0

    move-object v5, v1

    move-object v6, v0

    invoke-direct/range {v2 .. v7}, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$DiskConnectionStoreChangeCallbacks$1$1;-><init>(Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$DiskConnectionStoreChangeCallbacks$1;LX/0Px;LX/95j;LX/95j;I)V

    invoke-static {v8, v2}, LX/95n;->a$redex0(LX/95n;Ljava/lang/Runnable;)V

    goto :goto_0
.end method
