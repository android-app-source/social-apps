.class public final Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$InitializeRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TEdge:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field private final a:LX/95n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/95n",
            "<TTEdge;>;"
        }
    .end annotation
.end field

.field private final b:LX/2kI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2kI",
            "<TTEdge;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private final d:LX/15j;

.field private final e:LX/03V;

.field private final f:LX/2k1;

.field private final g:I


# direct methods
.method public constructor <init>(LX/95n;LX/2kI;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/15j;LX/03V;LX/2k1;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/95n",
            "<TTEdge;>;",
            "LX/2kI",
            "<TTEdge;>;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/15j;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/2k1;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 1437494
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1437495
    iput-object p1, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$InitializeRunnable;->a:LX/95n;

    .line 1437496
    iput-object p2, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$InitializeRunnable;->b:LX/2kI;

    .line 1437497
    iput-object p3, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$InitializeRunnable;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 1437498
    iput-object p4, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$InitializeRunnable;->d:LX/15j;

    .line 1437499
    iput-object p5, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$InitializeRunnable;->e:LX/03V;

    .line 1437500
    iput-object p6, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$InitializeRunnable;->f:LX/2k1;

    .line 1437501
    iput p7, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$InitializeRunnable;->g:I

    .line 1437502
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 14
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1437503
    iget-object v1, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$InitializeRunnable;->a:LX/95n;

    iget-object v1, v1, LX/95n;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 1437504
    :try_start_0
    iget-object v1, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$InitializeRunnable;->a:LX/95n;

    iget-boolean v1, v1, LX/95n;->x:Z

    if-eqz v1, :cond_0

    .line 1437505
    iget-object v1, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$InitializeRunnable;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x920002

    iget v3, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$InitializeRunnable;->g:I

    const/16 v4, 0x69

    invoke-interface {v1, v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1437506
    iget-object v1, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$InitializeRunnable;->a:LX/95n;

    iget-object v1, v1, LX/95n;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 1437507
    :goto_0
    return-void

    .line 1437508
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$InitializeRunnable;->d:LX/15j;

    invoke-virtual {v1}, LX/15j;->b()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1437509
    iget-object v1, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$InitializeRunnable;->e:LX/03V;

    const-string v2, "ConnectionController"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "FlatBuffer corruption detected for session: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$InitializeRunnable;->a:LX/95n;

    .line 1437510
    iget-object v0, v5, LX/95n;->b:Ljava/lang/String;

    move-object v5, v0

    .line 1437511
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1437512
    iget-object v2, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$InitializeRunnable;->f:LX/2k1;

    monitor-enter v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1437513
    :try_start_2
    iget-object v1, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$InitializeRunnable;->f:LX/2k1;

    iget-object v4, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$InitializeRunnable;->a:LX/95n;

    invoke-virtual {v4}, LX/95n;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, LX/2k1;->b(Ljava/lang/String;)V

    .line 1437514
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1437515
    :try_start_3
    iget-object v1, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$InitializeRunnable;->d:LX/15j;

    invoke-virtual {v1}, LX/15j;->c()V

    move-object v2, v3

    .line 1437516
    :cond_1
    :goto_1
    if-eqz v2, :cond_2

    invoke-virtual {v2}, LX/95j;->c()I

    move-result v1

    if-nez v1, :cond_7

    .line 1437517
    :cond_2
    iget-object v1, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$InitializeRunnable;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x920002

    iget v4, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$InitializeRunnable;->g:I

    const/16 v5, 0x21

    invoke-interface {v1, v3, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 1437518
    if-eqz v2, :cond_3

    .line 1437519
    invoke-static {v2}, LX/95n;->d(LX/95j;)V

    .line 1437520
    :cond_3
    iget-object v1, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$InitializeRunnable;->a:LX/95n;

    iget v2, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$InitializeRunnable;->g:I

    invoke-static {v1, v2}, LX/95n;->a$redex0(LX/95n;I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1437521
    :goto_2
    iget-object v1, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$InitializeRunnable;->a:LX/95n;

    iget-object v1, v1, LX/95n;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    .line 1437522
    :catchall_0
    move-exception v1

    :try_start_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1437523
    :catchall_1
    move-exception v1

    iget-object v2, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$InitializeRunnable;->a:LX/95n;

    iget-object v2, v2, LX/95n;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v1

    .line 1437524
    :cond_4
    :try_start_6
    iget-object v2, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$InitializeRunnable;->f:LX/2k1;

    monitor-enter v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1437525
    :try_start_7
    iget-object v1, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$InitializeRunnable;->f:LX/2k1;

    iget-object v4, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$InitializeRunnable;->a:LX/95n;

    .line 1437526
    iget-object v0, v4, LX/95n;->z:LX/2kb;

    move-object v4, v0

    .line 1437527
    invoke-interface {v1, v4}, LX/2k1;->a(LX/2kb;)LX/2nf;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2nf;

    .line 1437528
    iget-object v4, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$InitializeRunnable;->a:LX/95n;

    const/4 v5, 0x1

    .line 1437529
    iput-boolean v5, v4, LX/95n;->u:Z

    .line 1437530
    monitor-exit v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1437531
    :try_start_8
    iget-object v2, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$InitializeRunnable;->a:LX/95n;

    invoke-static {v2, v1}, LX/95n;->a$redex0(LX/95n;LX/2nf;)LX/95j;

    move-result-object v2

    .line 1437532
    iget-object v1, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$InitializeRunnable;->a:LX/95n;

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1437533
    iget-wide v8, v1, LX/95n;->d:J

    const-wide/16 v10, -0x1

    cmp-long v8, v8, v10

    if-nez v8, :cond_8

    .line 1437534
    :cond_5
    :goto_3
    move v1, v6

    .line 1437535
    if-eqz v1, :cond_6

    .line 1437536
    invoke-static {v2}, LX/95n;->d(LX/95j;)V

    .line 1437537
    iget-object v1, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$InitializeRunnable;->a:LX/95n;

    const/4 v2, 0x1

    .line 1437538
    iput-boolean v2, v1, LX/95n;->w:Z
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 1437539
    move-object v2, v3

    goto :goto_1

    .line 1437540
    :catchall_2
    move-exception v1

    :try_start_9
    monitor-exit v2
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    :try_start_a
    throw v1

    .line 1437541
    :cond_6
    instance-of v1, v2, LX/95k;

    if-eqz v1, :cond_1

    .line 1437542
    move-object v0, v2

    check-cast v0, LX/95k;

    move-object v1, v0

    .line 1437543
    invoke-virtual {v1}, LX/95k;->e()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1437544
    iget-object v2, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$InitializeRunnable;->a:LX/95n;

    .line 1437545
    iget v0, v2, LX/95n;->g:I

    move v2, v0

    .line 1437546
    invoke-virtual {v1, v2}, LX/95k;->b(I)LX/95k;

    move-result-object v2

    goto/16 :goto_1

    .line 1437547
    :cond_7
    iget-object v1, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$InitializeRunnable;->a:LX/95n;

    invoke-static {v1, v2}, LX/95n;->c(LX/95n;LX/95j;)LX/95j;

    move-result-object v1

    .line 1437548
    iget-object v2, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$InitializeRunnable;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x920002

    iget v4, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$InitializeRunnable;->g:I

    const/4 v5, 0x2

    invoke-interface {v2, v3, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 1437549
    iget-object v2, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$InitializeRunnable;->a:LX/95n;

    iget v3, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$InitializeRunnable;->g:I

    invoke-static {v2, v3}, LX/95n;->a$redex0(LX/95n;I)V

    .line 1437550
    invoke-static {v1}, LX/95n;->d(LX/95j;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto/16 :goto_2

    .line 1437551
    :cond_8
    iget-wide v8, v1, LX/95n;->d:J

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-nez v8, :cond_9

    move v6, v7

    .line 1437552
    goto :goto_3

    .line 1437553
    :cond_9
    invoke-virtual {v2}, LX/95j;->i()J

    move-result-wide v8

    iget-wide v10, v1, LX/95n;->d:J

    const-wide/16 v12, 0x3e8

    mul-long/2addr v10, v12

    add-long/2addr v8, v10

    .line 1437554
    iget-object v10, v1, LX/95n;->o:LX/0SG;

    invoke-interface {v10}, LX/0SG;->a()J

    move-result-wide v10

    cmp-long v8, v8, v10

    if-gez v8, :cond_5

    move v6, v7

    goto :goto_3
.end method
