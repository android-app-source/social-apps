.class public final Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0Rl;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/95n;


# direct methods
.method public constructor <init>(LX/95n;LX/0Rl;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1437422
    iput-object p1, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$2;->c:LX/95n;

    iput-object p2, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$2;->a:LX/0Rl;

    iput-object p3, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$2;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 1437423
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$2;->c:LX/95n;

    iget-object v0, v0, LX/95n;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 1437424
    :try_start_0
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$2;->c:LX/95n;

    iget-object v0, v0, LX/95n;->z:LX/2kb;

    if-nez v0, :cond_0

    .line 1437425
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$2;->c:LX/95n;

    iget-object v0, v0, LX/95n;->p:LX/03V;

    const-string v1, "ConnectionController"

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "removeEdgeByTagAndPredicate called on closed session"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1437426
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$2;->c:LX/95n;

    iget-object v0, v0, LX/95n;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 1437427
    :goto_0
    return-void

    .line 1437428
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$2;->c:LX/95n;

    iget-object v1, v0, LX/95n;->k:LX/2k1;

    monitor-enter v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1437429
    :try_start_2
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$2;->c:LX/95n;

    invoke-static {v0}, LX/95n;->k(LX/95n;)V

    .line 1437430
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$2;->c:LX/95n;

    iget-object v0, v0, LX/95n;->k:LX/2k1;

    iget-object v2, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$2;->c:LX/95n;

    iget-object v2, v2, LX/95n;->z:LX/2kb;

    iget-object v3, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$2;->a:LX/0Rl;

    iget-object v4, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$2;->b:Ljava/lang/String;

    invoke-interface {v0, v2, v3, v4}, LX/2k1;->a(LX/2kb;LX/0Rl;Ljava/lang/String;)V

    .line 1437431
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1437432
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$2;->c:LX/95n;

    iget-object v0, v0, LX/95n;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    .line 1437433
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1437434
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$2;->c:LX/95n;

    iget-object v1, v1, LX/95n;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method
