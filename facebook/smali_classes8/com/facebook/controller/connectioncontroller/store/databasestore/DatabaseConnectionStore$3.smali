.class public final Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/95n;


# direct methods
.method public constructor <init>(LX/95n;)V
    .locals 0

    .prologue
    .line 1437445
    iput-object p1, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$3;->a:LX/95n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    .line 1437435
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$3;->a:LX/95n;

    iget-object v0, v0, LX/95n;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 1437436
    :try_start_0
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$3;->a:LX/95n;

    invoke-static {v0}, LX/95n;->f$redex0(LX/95n;)V

    .line 1437437
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$3;->a:LX/95n;

    iget-object v1, v0, LX/95n;->k:LX/2k1;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1437438
    :try_start_1
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$3;->a:LX/95n;

    iget-object v0, v0, LX/95n;->t:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 1437439
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$3;->a:LX/95n;

    iget-object v0, v0, LX/95n;->k:LX/2k1;

    iget-object v2, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$3;->a:LX/95n;

    iget-object v2, v2, LX/95n;->c:Ljava/lang/String;

    invoke-interface {v0, v2}, LX/2k1;->b(Ljava/lang/String;)V

    .line 1437440
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1437441
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$3;->a:LX/95n;

    iget-object v0, v0, LX/95n;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 1437442
    return-void

    .line 1437443
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1437444
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$3;->a:LX/95n;

    iget-object v1, v1, LX/95n;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method
