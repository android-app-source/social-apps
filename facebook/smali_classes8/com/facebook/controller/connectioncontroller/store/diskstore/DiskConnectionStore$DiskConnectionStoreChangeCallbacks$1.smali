.class public final Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$DiskConnectionStoreChangeCallbacks$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/2nf;

.field public final synthetic c:LX/2kQ;


# direct methods
.method public constructor <init>(LX/2kQ;ILX/2nf;)V
    .locals 0

    .prologue
    .line 1437863
    iput-object p1, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$DiskConnectionStoreChangeCallbacks$1;->c:LX/2kQ;

    iput p2, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$DiskConnectionStoreChangeCallbacks$1;->a:I

    iput-object p3, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$DiskConnectionStoreChangeCallbacks$1;->b:LX/2nf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 1437834
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$DiskConnectionStoreChangeCallbacks$1;->c:LX/2kQ;

    iget-object v0, v0, LX/2kQ;->a:LX/2jx;

    iget-object v0, v0, LX/2jx;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x920009

    iget v2, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$DiskConnectionStoreChangeCallbacks$1;->a:I

    const/16 v3, 0xbe

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 1437835
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$DiskConnectionStoreChangeCallbacks$1;->c:LX/2kQ;

    iget-object v1, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$DiskConnectionStoreChangeCallbacks$1;->b:LX/2nf;

    iget v2, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$DiskConnectionStoreChangeCallbacks$1;->a:I

    const p0, 0x920009

    .line 1437836
    iget-object v3, v0, LX/2kQ;->a:LX/2jx;

    iget-object v3, v3, LX/2jx;->i:LX/0Sh;

    invoke-virtual {v3}, LX/0Sh;->a()V

    .line 1437837
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1437838
    iget-object v3, v0, LX/2kQ;->a:LX/2jx;

    .line 1437839
    iget-boolean v4, v3, LX/2jx;->w:Z

    move v3, v4

    .line 1437840
    if-nez v3, :cond_0

    iget-object v3, v0, LX/2kQ;->a:LX/2jx;

    iget-object v3, v3, LX/2jx;->y:LX/2kM;

    instance-of v3, v3, LX/2nh;

    if-eqz v3, :cond_0

    iget-object v3, v0, LX/2kQ;->a:LX/2jx;

    iget-object v3, v3, LX/2jx;->y:LX/2kM;

    check-cast v3, LX/2nh;

    invoke-virtual {v3}, LX/2nh;->h()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1437841
    :cond_0
    iget-object v3, v0, LX/2kQ;->a:LX/2jx;

    iget-object v3, v3, LX/2jx;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v4, 0x4

    invoke-interface {v3, p0, v2, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 1437842
    invoke-interface {v1}, LX/2nf;->close()V

    .line 1437843
    :goto_0
    return-void

    .line 1437844
    :cond_1
    iget-object v3, v0, LX/2kQ;->b:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1437845
    iget-object v3, v0, LX/2kQ;->a:LX/2jx;

    iget-object v3, v3, LX/2jx;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/16 v4, 0xb0

    invoke-interface {v3, p0, v2, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 1437846
    invoke-interface {v1}, LX/2nf;->close()V

    goto :goto_0

    .line 1437847
    :cond_2
    iget-object v3, v0, LX/2kQ;->b:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/util/SparseArray;

    .line 1437848
    iget-object v4, v0, LX/2kQ;->a:LX/2jx;

    const/4 v5, 0x0

    invoke-static {v4, v1, v5}, LX/2jx;->a$redex0(LX/2jx;LX/2nf;Ljava/util/ArrayList;)LX/2nh;

    move-result-object v4

    .line 1437849
    iget-object v5, v0, LX/2kQ;->a:LX/2jx;

    invoke-static {v5, v4}, LX/2jx;->a$redex0(LX/2jx;LX/2kM;)LX/2kM;

    move-result-object v5

    .line 1437850
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1437851
    invoke-interface {v1}, LX/2nf;->getExtras()Landroid/os/Bundle;

    move-result-object v7

    iget-object v8, v0, LX/2kQ;->a:LX/2jx;

    iget-object v8, v8, LX/2jx;->q:LX/03V;

    iget-object v9, v0, LX/2kQ;->a:LX/2jx;

    iget-object v9, v9, LX/2jx;->o:LX/1BA;

    invoke-static {v5, v4, v7, v8, v9}, LX/2jx;->b(LX/2kM;LX/2nh;Landroid/os/Bundle;LX/03V;LX/1BA;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1437852
    invoke-interface {v1}, LX/2nf;->getExtras()Landroid/os/Bundle;

    move-result-object v7

    .line 1437853
    iget-object v8, v0, LX/2kQ;->a:LX/2jx;

    invoke-static {v8}, LX/2jx;->k(LX/2jx;)LX/2nn;

    move-result-object v8

    invoke-static {v5, v7, v8}, LX/2jx;->b(LX/2kM;Landroid/os/Bundle;LX/2nn;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1437854
    iget-object v8, v0, LX/2kQ;->a:LX/2jx;

    invoke-static {v8}, LX/2jx;->k(LX/2jx;)LX/2nn;

    move-result-object v8

    invoke-static {v4, v7, v8, v3}, LX/2jx;->b(LX/2nh;Landroid/os/Bundle;LX/2nn;Landroid/util/SparseArray;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1437855
    :goto_1
    iget-object v3, v0, LX/2kQ;->a:LX/2jx;

    iget-object v3, v3, LX/2jx;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v7, 0x2

    invoke-interface {v3, p0, v2, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 1437856
    iget-object v3, v0, LX/2kQ;->a:LX/2jx;

    invoke-static {v3}, LX/2jx;->j(LX/2jx;)V

    .line 1437857
    iget-object v3, v0, LX/2kQ;->a:LX/2jx;

    iget-object v3, v3, LX/2jx;->s:LX/2kI;

    invoke-static {v6}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v6

    sget-object v7, LX/3Cb;->DISK:LX/3Cb;

    invoke-virtual {v3, v6, v7, v5, v4}, LX/2kI;->a(LX/0Px;LX/3Cb;LX/2kM;LX/2kM;)V

    .line 1437858
    iget-object v3, v0, LX/2kQ;->a:LX/2jx;

    iget-object v3, v3, LX/2jx;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/16 v4, 0xbf

    invoke-interface {v3, p0, v2, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 1437859
    iget-object v3, v0, LX/2kQ;->a:LX/2jx;

    invoke-static {v3, v5}, LX/2jx;->b$redex0(LX/2jx;LX/2kM;)V

    goto/16 :goto_0

    .line 1437860
    :cond_3
    invoke-interface {v1}, LX/2nf;->getExtras()Landroid/os/Bundle;

    move-result-object v7

    .line 1437861
    iget-object v8, v0, LX/2kQ;->a:LX/2jx;

    invoke-static {v8}, LX/2jx;->k(LX/2jx;)LX/2nn;

    move-result-object v8

    invoke-static {v5, v7, v8}, LX/2jx;->b(LX/2kM;Landroid/os/Bundle;LX/2nn;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1437862
    iget-object v8, v0, LX/2kQ;->a:LX/2jx;

    invoke-static {v8}, LX/2jx;->k(LX/2jx;)LX/2nn;

    move-result-object v8

    invoke-static {v4, v7, v8, v3}, LX/2jx;->b(LX/2nh;Landroid/os/Bundle;LX/2nn;Landroid/util/SparseArray;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method
