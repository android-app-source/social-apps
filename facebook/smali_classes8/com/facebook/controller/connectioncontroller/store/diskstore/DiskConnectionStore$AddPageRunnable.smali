.class public final Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TEdge:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field private final a:LX/2jx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2jx",
            "<TTEdge;>;"
        }
    .end annotation
.end field

.field private final b:LX/2kI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2kI",
            "<TTEdge;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private final d:LX/1BA;

.field private final e:LX/03V;

.field private final f:LX/2nf;

.field private final g:I

.field private final h:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<TTEdge;>;"
        }
    .end annotation
.end field

.field private final i:LX/2nj;

.field private final j:LX/3DP;

.field private final k:LX/3Cb;

.field private final l:Z


# direct methods
.method public constructor <init>(LX/2jx;LX/2kI;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/1BA;LX/03V;LX/2nf;ILandroid/util/SparseArray;LX/2nj;LX/3DP;LX/3Cb;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2jx",
            "<TTEdge;>;",
            "LX/2kI",
            "<TTEdge;>;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "Lcom/facebook/localstats/LocalStatsLogger;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/2nf;",
            "I",
            "Landroid/util/SparseArray",
            "<TTEdge;>;",
            "LX/2nj;",
            "LX/3DP;",
            "LX/3Cb;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 1437781
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1437782
    iput-object p1, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;->a:LX/2jx;

    .line 1437783
    iput-object p2, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;->b:LX/2kI;

    .line 1437784
    iput-object p3, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 1437785
    iput-object p4, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;->d:LX/1BA;

    .line 1437786
    iput-object p5, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;->e:LX/03V;

    .line 1437787
    iput-object p6, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;->f:LX/2nf;

    .line 1437788
    iput p7, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;->g:I

    .line 1437789
    iput-object p8, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;->h:Landroid/util/SparseArray;

    .line 1437790
    iput-object p9, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;->i:LX/2nj;

    .line 1437791
    iput-object p10, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;->j:LX/3DP;

    .line 1437792
    iput-object p11, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;->k:LX/3Cb;

    .line 1437793
    iput-boolean p12, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;->l:Z

    .line 1437794
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 12
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    const v6, 0x920001

    .line 1437795
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;->g:I

    const/16 v2, 0xbe

    invoke-interface {v0, v6, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 1437796
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;->a:LX/2jx;

    iget-object v1, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;->f:LX/2nf;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/2jx;->a$redex0(LX/2jx;LX/2nf;Ljava/util/ArrayList;)LX/2nh;

    move-result-object v1

    .line 1437797
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;->a:LX/2jx;

    invoke-static {v0, v1}, LX/2jx;->a$redex0(LX/2jx;LX/2kM;)LX/2kM;

    move-result-object v2

    .line 1437798
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;->f:LX/2nf;

    invoke-interface {v0}, LX/2nf;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1437799
    iget-object v3, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;->e:LX/03V;

    iget-object v4, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;->d:LX/1BA;

    invoke-static {v2, v1, v0, v3, v4}, LX/2jx;->b(LX/2kM;LX/2nh;Landroid/os/Bundle;LX/03V;LX/1BA;)Z

    .line 1437800
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1437801
    iget-object v4, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;->a:LX/2jx;

    invoke-static {v4}, LX/2jx;->k(LX/2jx;)LX/2nn;

    move-result-object v4

    invoke-static {v2, v0, v4}, LX/2jx;->b(LX/2kM;Landroid/os/Bundle;LX/2nn;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1437802
    iget-boolean v0, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;->l:Z

    if-eqz v0, :cond_0

    .line 1437803
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;->a:LX/2jx;

    .line 1437804
    invoke-static {v0}, LX/2jx;->k(LX/2jx;)LX/2nn;

    move-result-object v4

    .line 1437805
    if-eqz v4, :cond_0

    .line 1437806
    iget-boolean v0, v4, LX/2nn;->b:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1437807
    iget-object v0, v4, LX/2nn;->a:LX/2ng;

    iget-object v0, v0, LX/2ng;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1437808
    :cond_0
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;->h:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 1437809
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;->a:LX/2jx;

    invoke-static {v0}, LX/2jx;->k(LX/2jx;)LX/2nn;

    move-result-object v0

    check-cast v0, LX/2nn;

    iget-object v4, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;->h:Landroid/util/SparseArray;

    .line 1437810
    if-nez v0, :cond_3

    .line 1437811
    :goto_1
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;->h:Landroid/util/SparseArray;

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    iget-object v4, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;->h:Landroid/util/SparseArray;

    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v4

    invoke-static {v0, v4}, LX/3CY;->a(II)LX/3CY;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1437812
    :cond_1
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;->a:LX/2jx;

    invoke-static {v0}, LX/2jx;->j(LX/2jx;)V

    .line 1437813
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v4, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;->g:I

    const/4 v5, 0x2

    invoke-interface {v0, v6, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 1437814
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;->b:LX/2kI;

    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;->k:LX/3Cb;

    invoke-virtual {v0, v3, v4, v2, v1}, LX/2kI;->a(LX/0Px;LX/3Cb;LX/2kM;LX/2kM;)V

    .line 1437815
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;->b:LX/2kI;

    iget-object v1, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;->i:LX/2nj;

    iget-object v3, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;->j:LX/3DP;

    invoke-virtual {v0, v1, v3}, LX/2kI;->a(LX/2nj;LX/3DP;)V

    .line 1437816
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;->g:I

    const/16 v3, 0xbf

    invoke-interface {v0, v6, v1, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 1437817
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;->a:LX/2jx;

    invoke-static {v0, v2}, LX/2jx;->b$redex0(LX/2jx;LX/2kM;)V

    .line 1437818
    return-void

    .line 1437819
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 1437820
    :cond_3
    const/4 v5, 0x0

    :goto_2
    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v7

    if-ge v5, v7, :cond_5

    .line 1437821
    invoke-virtual {v4, v5}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v7

    .line 1437822
    invoke-virtual {v4, v5}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v8

    .line 1437823
    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 1437824
    iget-boolean v9, v0, LX/2nn;->b:Z

    if-nez v9, :cond_6

    move v9, v10

    :goto_3
    invoke-static {v9}, LX/0PB;->checkState(Z)V

    .line 1437825
    iget-object v9, v0, LX/2nn;->a:LX/2ng;

    iget-object v9, v9, LX/2ng;->c:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-gt v7, v9, :cond_7

    .line 1437826
    if-eqz v8, :cond_4

    move v11, v10

    :cond_4
    invoke-static {v11}, LX/0PB;->checkState(Z)V

    move-object v9, v8

    .line 1437827
    check-cast v9, Lcom/facebook/flatbuffers/MutableFlattenable;

    invoke-interface {v9}, Lcom/facebook/flatbuffers/MutableFlattenable;->q_()LX/15i;

    move-result-object v9

    invoke-static {v9}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/15i;

    .line 1437828
    invoke-virtual {v9}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v9

    instance-of v9, v9, Ljava/nio/MappedByteBuffer;

    invoke-static {v9}, LX/0PB;->checkArgument(Z)V

    .line 1437829
    iget-object v9, v0, LX/2nn;->a:LX/2ng;

    iget-object v9, v9, LX/2ng;->c:Ljava/util/ArrayList;

    invoke-virtual {v9, v7, v8}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1437830
    :goto_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 1437831
    :cond_5
    invoke-virtual {v0}, LX/2nn;->e()V

    goto/16 :goto_1

    :cond_6
    move v9, v11

    .line 1437832
    goto :goto_3

    .line 1437833
    :cond_7
    goto :goto_4
.end method
