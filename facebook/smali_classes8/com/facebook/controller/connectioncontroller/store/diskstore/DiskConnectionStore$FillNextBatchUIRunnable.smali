.class public final Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchUIRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TEdge:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field private final a:LX/2jx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2jx",
            "<TTEdge;>;"
        }
    .end annotation
.end field

.field private final b:LX/2kI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2kI",
            "<TTEdge;>;"
        }
    .end annotation
.end field

.field private final c:LX/2nj;

.field private final d:LX/3DP;

.field private final e:I

.field private final f:Z

.field private final g:LX/2ng;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2ng",
            "<TTEdge;>;"
        }
    .end annotation
.end field

.field private final h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<TTEdge;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2jx;LX/2kI;LX/2nj;LX/3DP;IZLX/2ng;Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2jx",
            "<TTEdge;>;",
            "LX/2kI",
            "<TTEdge;>;",
            "LX/2nj;",
            "LX/3DP;",
            "IZ",
            "LX/2ng",
            "<TTEdge;>;",
            "Ljava/util/ArrayList",
            "<TTEdge;>;)V"
        }
    .end annotation

    .prologue
    .line 1437864
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1437865
    iput-object p1, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchUIRunnable;->a:LX/2jx;

    .line 1437866
    iput-object p3, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchUIRunnable;->c:LX/2nj;

    .line 1437867
    iput-object p4, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchUIRunnable;->d:LX/3DP;

    .line 1437868
    iput p5, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchUIRunnable;->e:I

    .line 1437869
    iput-boolean p6, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchUIRunnable;->f:Z

    .line 1437870
    iput-object p7, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchUIRunnable;->g:LX/2ng;

    .line 1437871
    iput-object p8, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchUIRunnable;->h:Ljava/util/ArrayList;

    .line 1437872
    iput-object p2, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchUIRunnable;->b:LX/2kI;

    .line 1437873
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 1437874
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchUIRunnable;->h:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchUIRunnable;->g:LX/2ng;

    iget-object v1, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchUIRunnable;->a:LX/2jx;

    .line 1437875
    iget-object v2, v1, LX/2jx;->y:LX/2kM;

    move-object v1, v2

    .line 1437876
    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchUIRunnable;->g:LX/2ng;

    invoke-virtual {v0}, LX/2ng;->h()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1437877
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchUIRunnable;->a:LX/2jx;

    iget-object v1, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchUIRunnable;->g:LX/2ng;

    invoke-virtual {v1}, LX/2ng;->e()LX/2nf;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchUIRunnable;->h:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2}, LX/2jx;->a$redex0(LX/2jx;LX/2nf;Ljava/util/ArrayList;)LX/2nh;

    move-result-object v0

    check-cast v0, LX/2ng;

    .line 1437878
    iget-object v1, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchUIRunnable;->a:LX/2jx;

    invoke-static {v1, v0}, LX/2jx;->a$redex0(LX/2jx;LX/2kM;)LX/2kM;

    .line 1437879
    iget-object v1, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchUIRunnable;->a:LX/2jx;

    invoke-static {v1}, LX/2jx;->j(LX/2jx;)V

    .line 1437880
    iget-object v1, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchUIRunnable;->b:LX/2kI;

    iget-object v2, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchUIRunnable;->g:LX/2ng;

    invoke-virtual {v2}, LX/2ng;->c()I

    move-result v2

    invoke-virtual {v0}, LX/2ng;->c()I

    move-result v3

    iget-object v4, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchUIRunnable;->g:LX/2ng;

    invoke-virtual {v4}, LX/2ng;->c()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-static {v2, v3}, LX/3CY;->a(II)LX/3CY;

    move-result-object v2

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    sget-object v3, LX/3Cb;->DISK:LX/3Cb;

    iget-object v4, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchUIRunnable;->g:LX/2ng;

    invoke-virtual {v1, v2, v3, v4, v0}, LX/2kI;->a(LX/0Px;LX/3Cb;LX/2kM;LX/2kM;)V

    .line 1437881
    :cond_0
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchUIRunnable;->b:LX/2kI;

    iget-object v1, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchUIRunnable;->c:LX/2nj;

    iget-object v2, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchUIRunnable;->d:LX/3DP;

    invoke-virtual {v0, v1, v2}, LX/2kI;->a(LX/2nj;LX/3DP;)V

    .line 1437882
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchUIRunnable;->a:LX/2jx;

    .line 1437883
    iput-boolean v5, v0, LX/2jx;->v:Z

    .line 1437884
    :goto_0
    return-void

    .line 1437885
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchUIRunnable;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchUIRunnable;->a:LX/2jx;

    .line 1437886
    iget-object v1, v0, LX/2jx;->y:LX/2kM;

    move-object v0, v1

    .line 1437887
    instance-of v0, v0, LX/2ng;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchUIRunnable;->a:LX/2jx;

    .line 1437888
    iget-object v1, v0, LX/2jx;->y:LX/2kM;

    move-object v0, v1

    .line 1437889
    invoke-interface {v0}, LX/2kM;->b()LX/2nj;

    move-result-object v0

    .line 1437890
    iget-object v1, v0, LX/2nj;->b:Ljava/lang/String;

    move-object v0, v1

    .line 1437891
    invoke-static {v0}, LX/3DQ;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1437892
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchUIRunnable;->a:LX/2jx;

    iget-object v1, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchUIRunnable;->c:LX/2nj;

    iget-object v2, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchUIRunnable;->d:LX/3DP;

    iget v3, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchUIRunnable;->e:I

    invoke-static {v0, v1, v2, v3, v5}, LX/2jx;->a$redex0(LX/2jx;LX/2nj;LX/3DP;IZ)V

    goto :goto_0
.end method
