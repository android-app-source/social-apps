.class public final Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchWorkerRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TEdge:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field private final a:LX/2jx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2jx",
            "<TTEdge;>;"
        }
    .end annotation
.end field

.field private final b:LX/2kI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2kI",
            "<TTEdge;>;"
        }
    .end annotation
.end field

.field private final c:LX/2nj;

.field private final d:LX/3DP;

.field private final e:I

.field private final f:Z


# direct methods
.method public constructor <init>(LX/2jx;LX/2kI;LX/2nj;LX/3DP;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2jx",
            "<TTEdge;>;",
            "LX/2kI",
            "<TTEdge;>;",
            "LX/2nj;",
            "LX/3DP;",
            "IZ)V"
        }
    .end annotation

    .prologue
    .line 1437893
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1437894
    iput-object p1, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchWorkerRunnable;->a:LX/2jx;

    .line 1437895
    iput-object p3, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchWorkerRunnable;->c:LX/2nj;

    .line 1437896
    iput-object p4, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchWorkerRunnable;->d:LX/3DP;

    .line 1437897
    iput p5, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchWorkerRunnable;->e:I

    .line 1437898
    iput-boolean p6, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchWorkerRunnable;->f:Z

    .line 1437899
    iput-object p2, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchWorkerRunnable;->b:LX/2kI;

    .line 1437900
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    .line 1437901
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchWorkerRunnable;->a:LX/2jx;

    .line 1437902
    iget-object v1, v0, LX/2jx;->y:LX/2kM;

    move-object v7, v1

    .line 1437903
    check-cast v7, LX/2ng;

    .line 1437904
    iget-object v0, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchWorkerRunnable;->a:LX/2jx;

    iget v1, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchWorkerRunnable;->e:I

    invoke-static {v0, v7, v1}, LX/2jx;->a$redex0(LX/2jx;LX/2ng;I)Ljava/util/ArrayList;

    move-result-object v8

    .line 1437905
    iget-object v9, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchWorkerRunnable;->a:LX/2jx;

    new-instance v0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchUIRunnable;

    iget-object v1, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchWorkerRunnable;->a:LX/2jx;

    iget-object v2, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchWorkerRunnable;->b:LX/2kI;

    iget-object v3, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchWorkerRunnable;->c:LX/2nj;

    iget-object v4, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchWorkerRunnable;->d:LX/3DP;

    iget v5, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchWorkerRunnable;->e:I

    iget-boolean v6, p0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchWorkerRunnable;->f:Z

    invoke-direct/range {v0 .. v8}, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchUIRunnable;-><init>(LX/2jx;LX/2kI;LX/2nj;LX/3DP;IZLX/2ng;Ljava/util/ArrayList;)V

    invoke-static {v9, v0}, LX/2jx;->a$redex0(LX/2jx;Ljava/lang/Runnable;)V

    .line 1437906
    return-void
.end method
