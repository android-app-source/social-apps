.class public Lcom/facebook/adspayments/analytics/PaymentsLogEvent;
.super Lcom/facebook/adspayments/analytics/BasePaymentsLogEvent;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/adspayments/analytics/BasePaymentsLogEvent",
        "<",
        "Lcom/facebook/adspayments/analytics/PaymentsLogEvent;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)V
    .locals 2

    .prologue
    .line 1644774
    invoke-direct {p0, p1, p2}, Lcom/facebook/adspayments/analytics/BasePaymentsLogEvent;-><init>(Ljava/lang/String;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)V

    .line 1644775
    const-string v0, "flow_name"

    .line 1644776
    iget-object v1, p2, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1644777
    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1644778
    const-string v0, "flow_type"

    .line 1644779
    iget-object v1, p2, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->e:LX/6xj;

    move-object v1, v1

    .line 1644780
    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1644781
    const-string v0, "flow_step"

    .line 1644782
    iget-object v1, p0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1644783
    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1644784
    const-string v0, "external_reference_id"

    .line 1644785
    iget-object v1, p2, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1644786
    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1644787
    const-string v0, "item_type"

    .line 1644788
    iget-object v1, p2, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->b:LX/6xg;

    move-object v1, v1

    .line 1644789
    invoke-virtual {v1}, LX/6xg;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1644790
    const-string v0, "external_reference_id"

    .line 1644791
    iget-object v1, p2, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->f:Ljava/lang/String;

    move-object v1, v1

    .line 1644792
    invoke-virtual {p0, v0, v1}, Lcom/facebook/adspayments/analytics/BasePaymentsLogEvent;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1644793
    instance-of v0, p2, Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;

    if-eqz v0, :cond_0

    .line 1644794
    check-cast p2, Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;

    .line 1644795
    iget-object v0, p2, Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;->c:LX/ADX;

    move-object v0, v0

    .line 1644796
    const-string v1, "is_nux"

    invoke-virtual {v0}, LX/ADX;->isNUX()Z

    move-result p1

    invoke-virtual {p0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1644797
    const-string v1, "is_pux"

    invoke-virtual {v0}, LX/ADX;->isPUX()Z

    move-result p1

    invoke-virtual {p0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1644798
    const-string v1, "is_prepay_account"

    invoke-virtual {v0}, LX/ADX;->isLockedIntoPrepay()Z

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1644799
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/paymentmethods/model/PaymentOption;)Lcom/facebook/adspayments/analytics/PaymentsLogEvent;
    .locals 2
    .param p1    # Lcom/facebook/payments/paymentmethods/model/PaymentOption;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1644768
    if-nez p1, :cond_0

    .line 1644769
    :goto_0
    return-object p0

    .line 1644770
    :cond_0
    instance-of v0, p1, Lcom/facebook/payments/paymentmethods/model/CreditCard;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 1644771
    check-cast v0, Lcom/facebook/payments/paymentmethods/model/CreditCard;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/model/CreditCard;->g()Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->getHumanReadableName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/analytics/PaymentsLogEvent;->n(Ljava/lang/String;)Lcom/facebook/adspayments/analytics/PaymentsLogEvent;

    .line 1644772
    :cond_1
    const-string v1, "payment_method_type"

    invoke-interface {p1}, Lcom/facebook/payments/paymentmethods/model/PaymentOption;->e()LX/6zP;

    move-result-object v0

    invoke-interface {v0}, LX/6LU;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1644773
    invoke-interface {p1}, Lcom/facebook/payments/paymentmethods/model/PaymentOption;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/adspayments/analytics/PaymentsLogEvent;->o(Ljava/lang/String;)Lcom/facebook/adspayments/analytics/PaymentsLogEvent;

    move-result-object p0

    goto :goto_0
.end method

.method public final synthetic a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 1
    .param p1    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1644765
    invoke-super {p0, p1}, Lcom/facebook/adspayments/analytics/BasePaymentsLogEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1644766
    move-object v0, p0

    .line 1644767
    return-object v0
.end method

.method public final b(Ljava/util/Map;)Lcom/facebook/adspayments/analytics/PaymentsLogEvent;
    .locals 0
    .param p1    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)",
            "Lcom/facebook/adspayments/analytics/PaymentsLogEvent;"
        }
    .end annotation

    .prologue
    .line 1644763
    invoke-super {p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1644764
    return-object p0
.end method

.method public final n(Ljava/lang/String;)Lcom/facebook/adspayments/analytics/PaymentsLogEvent;
    .locals 1

    .prologue
    .line 1644800
    const-string v0, "card_issuer"

    invoke-virtual {p0, v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1644801
    return-object p0
.end method

.method public final o(Ljava/lang/String;)Lcom/facebook/adspayments/analytics/PaymentsLogEvent;
    .locals 1

    .prologue
    .line 1644761
    const-string v0, "credential_id"

    invoke-virtual {p0, v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1644762
    return-object p0
.end method

.method public final q(Ljava/lang/String;)Lcom/facebook/adspayments/analytics/PaymentsLogEvent;
    .locals 1

    .prologue
    .line 1644759
    const-string v0, "payment_id"

    invoke-virtual {p0, v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1644760
    return-object p0
.end method

.method public final u()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1644758
    const-string v0, "payments_flow"

    return-object v0
.end method
