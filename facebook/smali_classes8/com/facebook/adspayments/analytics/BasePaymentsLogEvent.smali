.class public abstract Lcom/facebook/adspayments/analytics/BasePaymentsLogEvent;
.super Lcom/facebook/analytics/logger/HoneyClientEvent;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/facebook/adspayments/analytics/BasePaymentsLogEvent;",
        ">",
        "Lcom/facebook/analytics/logger/HoneyClientEvent;"
    }
.end annotation


# instance fields
.field public c:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

.field private d:Ljava/lang/Throwable;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)V
    .locals 6

    .prologue
    .line 1644680
    invoke-direct {p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1644681
    iput-object p2, p0, Lcom/facebook/adspayments/analytics/BasePaymentsLogEvent;->c:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    .line 1644682
    invoke-virtual {p0}, Lcom/facebook/adspayments/analytics/BasePaymentsLogEvent;->u()Ljava/lang/String;

    move-result-object v0

    .line 1644683
    iput-object v0, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1644684
    const-string v0, "flow_context_id"

    .line 1644685
    iget-wide v4, p2, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->c:J

    move-wide v2, v4

    .line 1644686
    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1644687
    const-string v0, "payment_account_id"

    .line 1644688
    iget-object v1, p2, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1644689
    invoke-virtual {p0, v0, v1}, Lcom/facebook/adspayments/analytics/BasePaymentsLogEvent;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1644690
    return-void
.end method

.method public static a(Lcom/facebook/adspayments/analytics/BasePaymentsLogEvent;LX/2Oo;)V
    .locals 3

    .prologue
    .line 1644691
    invoke-virtual {p1}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v0

    .line 1644692
    const-string v1, "error_code"

    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1644693
    const-string v1, "error_message"

    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1644694
    const-string v0, "exception_domain"

    const-string v1, "FBAPIErrorDomain"

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1644695
    return-void
.end method

.method public static d(Ljava/lang/Throwable;)[Ljava/lang/String;
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1644696
    invoke-static {p0}, LX/1Bz;->getStackTraceAsString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;)Lcom/facebook/adspayments/analytics/BasePaymentsLogEvent;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 1644697
    iget-object v0, p0, Lcom/facebook/adspayments/analytics/BasePaymentsLogEvent;->e:Ljava/lang/String;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1644698
    iput-object p1, p0, Lcom/facebook/adspayments/analytics/BasePaymentsLogEvent;->d:Ljava/lang/Throwable;

    .line 1644699
    const-string v0, "error_stacktrace"

    .line 1644700
    new-instance v2, LX/162;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v1}, LX/162;-><init>(LX/0mC;)V

    .line 1644701
    invoke-static {p1}, Lcom/facebook/adspayments/analytics/BasePaymentsLogEvent;->d(Ljava/lang/Throwable;)[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    .line 1644702
    invoke-virtual {v2, v5}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 1644703
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1644704
    :cond_0
    move-object v1, v2

    .line 1644705
    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1644706
    const-class v0, LX/6u0;

    invoke-static {p1, v0}, LX/23D;->a(Ljava/lang/Throwable;Ljava/lang/Class;)Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, LX/6u0;

    .line 1644707
    if-eqz v0, :cond_2

    .line 1644708
    const-class v1, LX/2Oo;

    invoke-static {v0, v1}, LX/23D;->a(Ljava/lang/Throwable;Ljava/lang/Class;)Ljava/lang/Throwable;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oo;

    move-object v1, v1

    .line 1644709
    invoke-static {p0, v1}, Lcom/facebook/adspayments/analytics/BasePaymentsLogEvent;->a(Lcom/facebook/adspayments/analytics/BasePaymentsLogEvent;LX/2Oo;)V

    .line 1644710
    const-string v1, "error_message"

    invoke-virtual {v0}, LX/6u0;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1644711
    const-string v1, "call"

    .line 1644712
    iget-object v2, v0, LX/6u0;->mApiMethod:LX/6sU;

    move-object v0, v2

    .line 1644713
    invoke-virtual {v0}, LX/6sU;->d()Ljava/lang/String;

    move-result-object v2

    move-object v0, v2

    .line 1644714
    invoke-virtual {p0, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1644715
    :goto_2
    move-object v0, p0

    .line 1644716
    return-object v0

    .line 1644717
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1644718
    :cond_2
    const-class v0, LX/2Oo;

    invoke-static {p1, v0}, LX/23D;->a(Ljava/lang/Throwable;Ljava/lang/Class;)Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, LX/2Oo;

    .line 1644719
    if-eqz v0, :cond_3

    .line 1644720
    invoke-static {p0, v0}, Lcom/facebook/adspayments/analytics/BasePaymentsLogEvent;->a(Lcom/facebook/adspayments/analytics/BasePaymentsLogEvent;LX/2Oo;)V

    goto :goto_2

    .line 1644721
    :cond_3
    const-string v0, "error_message"

    invoke-static {p1}, LX/1Bz;->getRootCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1644722
    const-class v0, Lcom/facebook/fbservice/service/ServiceException;

    invoke-static {p1, v0}, LX/23D;->a(Ljava/lang/Throwable;Ljava/lang/Class;)Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbservice/service/ServiceException;

    .line 1644723
    if-eqz v0, :cond_4

    .line 1644724
    const-string v1, "error_code"

    .line 1644725
    iget-object v2, v0, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v2

    .line 1644726
    invoke-virtual {v0}, LX/1nY;->getAsInt()I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1644727
    const-string v0, "exception_domain"

    const-string v1, "FBServiceErrorDomain"

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_2

    .line 1644728
    :cond_4
    const-string v0, "error_code"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1644729
    const-string v0, "exception_domain"

    const-string v1, "FBAdsPaymentsDomain"

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_2
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1644730
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1644731
    invoke-virtual {p0, p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1644732
    :cond_0
    return-void
.end method

.method public final m(Ljava/lang/String;)Lcom/facebook/adspayments/analytics/BasePaymentsLogEvent;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1644733
    iget-object v0, p0, Lcom/facebook/adspayments/analytics/BasePaymentsLogEvent;->d:Ljava/lang/Throwable;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1644734
    iput-object p1, p0, Lcom/facebook/adspayments/analytics/BasePaymentsLogEvent;->e:Ljava/lang/String;

    .line 1644735
    const-string v0, "error_code"

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1644736
    const-string v0, "error_message"

    invoke-virtual {p0, v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1644737
    move-object v0, p0

    .line 1644738
    return-object v0

    :cond_0
    move v0, v1

    .line 1644739
    goto :goto_0
.end method

.method public abstract u()Ljava/lang/String;
.end method
