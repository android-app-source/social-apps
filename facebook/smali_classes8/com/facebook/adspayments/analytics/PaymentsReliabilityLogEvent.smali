.class public abstract Lcom/facebook/adspayments/analytics/PaymentsReliabilityLogEvent;
.super Lcom/facebook/adspayments/analytics/BasePaymentsLogEvent;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/adspayments/analytics/BasePaymentsLogEvent",
        "<",
        "Lcom/facebook/adspayments/analytics/PaymentsReliabilityLogEvent;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)V
    .locals 2

    .prologue
    .line 1644863
    const-string v0, "mobile_payments_reliability"

    invoke-direct {p0, v0, p1}, Lcom/facebook/adspayments/analytics/BasePaymentsLogEvent;-><init>(Ljava/lang/String;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)V

    .line 1644864
    const-string v0, "flow"

    invoke-direct {p0}, Lcom/facebook/adspayments/analytics/PaymentsReliabilityLogEvent;->w()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1644865
    const-string v0, "event_type"

    invoke-virtual {p0}, Lcom/facebook/adspayments/analytics/PaymentsReliabilityLogEvent;->v()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1644866
    return-void
.end method

.method private w()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1644856
    const-string v0, "android_mobile_payments_%s"

    .line 1644857
    iget-object v1, p0, Lcom/facebook/adspayments/analytics/BasePaymentsLogEvent;->c:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    move-object v1, v1

    .line 1644858
    iget-object p0, v1, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->b:LX/6xg;

    move-object v1, p0

    .line 1644859
    invoke-virtual {v1}, LX/6xg;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final n(Ljava/lang/String;)Lcom/facebook/adspayments/analytics/PaymentsReliabilityLogEvent;
    .locals 1

    .prologue
    .line 1644861
    const-string v0, "step"

    invoke-virtual {p0, v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1644862
    return-object p0
.end method

.method public final u()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1644860
    const-string v0, "payments_reliability"

    return-object v0
.end method

.method public abstract v()Ljava/lang/String;
.end method
