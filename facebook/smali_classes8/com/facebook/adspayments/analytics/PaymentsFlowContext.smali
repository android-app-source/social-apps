.class public Lcom/facebook/adspayments/analytics/PaymentsFlowContext;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/adspayments/analytics/PaymentsFlowContext;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/6xg;

.field public final c:J

.field public final d:Ljava/lang/String;

.field public final e:LX/6xj;

.field public final f:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1644646
    new-instance v0, LX/ADU;

    invoke-direct {v0}, LX/ADU;-><init>()V

    sput-object v0, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1644599
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1644600
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->a:Ljava/lang/String;

    .line 1644601
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->d:Ljava/lang/String;

    .line 1644602
    const-class v0, LX/6xg;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6xg;

    iput-object v0, p0, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->b:LX/6xg;

    .line 1644603
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->c:J

    .line 1644604
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->f:Ljava/lang/String;

    .line 1644605
    const-class v0, LX/6xj;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6xj;

    iput-object v0, p0, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->e:LX/6xj;

    .line 1644606
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;LX/6xg;LX/6xj;JLjava/lang/String;)V
    .locals 3
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1644633
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1644634
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->a:Ljava/lang/String;

    .line 1644635
    iput-object p2, p0, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->d:Ljava/lang/String;

    .line 1644636
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6xg;

    iput-object v0, p0, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->b:LX/6xg;

    .line 1644637
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6xj;

    iput-object v0, p0, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->e:LX/6xj;

    .line 1644638
    const-wide/16 v0, 0x0

    cmp-long v0, p5, v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Flow context id should be passed."

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1644639
    iput-wide p5, p0, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->c:J

    .line 1644640
    if-eqz p7, :cond_2

    :goto_1
    iput-object p7, p0, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->f:Ljava/lang/String;

    .line 1644641
    iget-object v0, p0, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->b:LX/6xg;

    sget-object v1, LX/6xg;->INVOICE:LX/6xg;

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->d:Ljava/lang/String;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1644642
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invoice payment (Ads) only works with valid account id"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1644643
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1644644
    :cond_2
    iget-wide v0, p0, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->c:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p7

    goto :goto_1

    .line 1644645
    :cond_3
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;LX/6xj;)V
    .locals 9

    .prologue
    .line 1644631
    sget-object v4, LX/6xg;->INVOICE:LX/6xg;

    new-instance v0, LX/3Ed;

    invoke-direct {v0}, LX/3Ed;-><init>()V

    invoke-virtual {v0}, LX/3Ed;->a()J

    move-result-wide v6

    const/4 v8, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, p3

    invoke-direct/range {v1 .. v8}, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;-><init>(Ljava/lang/String;Ljava/lang/String;LX/6xg;LX/6xj;JLjava/lang/String;)V

    .line 1644632
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1644630
    const/4 v0, 0x0

    return v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1644629
    iget-object v0, p0, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final h()J
    .locals 2

    .prologue
    .line 1644628
    iget-wide v0, p0, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->c:J

    return-wide v0
.end method

.method public final k()LX/0zA;
    .locals 6

    .prologue
    .line 1644615
    invoke-static {p0}, LX/0Qh;->toStringHelper(Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "flowName"

    .line 1644616
    iget-object v2, p0, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1644617
    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "accountId"

    .line 1644618
    iget-object v2, p0, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->d:Ljava/lang/String;

    move-object v2, v2

    .line 1644619
    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "paymentItemType"

    .line 1644620
    iget-object v2, p0, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->b:LX/6xg;

    move-object v2, v2

    .line 1644621
    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "flowContextId"

    .line 1644622
    iget-wide v4, p0, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->c:J

    move-wide v2, v4

    .line 1644623
    invoke-virtual {v0, v1, v2, v3}, LX/0zA;->add(Ljava/lang/String;J)LX/0zA;

    move-result-object v0

    const-string v1, "externalReferenceId"

    .line 1644624
    iget-object v2, p0, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->f:Ljava/lang/String;

    move-object v2, v2

    .line 1644625
    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "paymentsFlowType"

    .line 1644626
    iget-object v2, p0, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->e:LX/6xj;

    move-object v2, v2

    .line 1644627
    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1644614
    invoke-virtual {p0}, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->k()LX/0zA;

    move-result-object v0

    invoke-virtual {v0}, LX/0zA;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1644607
    iget-object v0, p0, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1644608
    iget-object v0, p0, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1644609
    iget-object v0, p0, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->b:LX/6xg;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1644610
    iget-wide v0, p0, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1644611
    iget-object v0, p0, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1644612
    iget-object v0, p0, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->e:LX/6xj;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1644613
    return-void
.end method
