.class public Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;
.super Lcom/facebook/adspayments/analytics/PaymentsFlowContext;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/payments/currency/CurrencyAmount;

.field public final b:Z

.field public final c:LX/ADX;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1644667
    new-instance v0, LX/ADS;

    invoke-direct {v0}, LX/ADS;-><init>()V

    sput-object v0, Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1644662
    invoke-direct {p0, p1}, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;-><init>(Landroid/os/Parcel;)V

    .line 1644663
    const-class v0, Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-static {p1, v0}, LX/46R;->d(Landroid/os/Parcel;Ljava/lang/Class;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/currency/CurrencyAmount;

    iput-object v0, p0, Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1644664
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;->b:Z

    .line 1644665
    const-class v0, LX/ADX;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ADX;

    iput-object v0, p0, Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;->c:LX/ADX;

    .line 1644666
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;LX/6xj;Lcom/facebook/payments/currency/CurrencyAmount;ZLX/ADX;)V
    .locals 0

    .prologue
    .line 1644668
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;-><init>(Ljava/lang/String;Ljava/lang/String;LX/6xj;)V

    .line 1644669
    iput-object p4, p0, Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1644670
    iput-boolean p5, p0, Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;->b:Z

    .line 1644671
    iput-object p6, p0, Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;->c:LX/ADX;

    .line 1644672
    return-void
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1644659
    iget-object v0, p0, Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    move-object v0, v0

    .line 1644660
    iget-object p0, v0, Lcom/facebook/payments/currency/CurrencyAmount;->b:Ljava/lang/String;

    move-object v0, p0

    .line 1644661
    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1644647
    invoke-virtual {p0}, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->k()LX/0zA;

    move-result-object v0

    const-string v1, "selectedBudget"

    .line 1644648
    iget-object v2, p0, Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    move-object v2, v2

    .line 1644649
    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "dailyBudget"

    .line 1644650
    iget-boolean v2, p0, Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;->b:Z

    move v2, v2

    .line 1644651
    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Z)LX/0zA;

    move-result-object v0

    const-string v1, "storedBalanceStatus"

    .line 1644652
    iget-object v2, p0, Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;->c:LX/ADX;

    move-object v2, v2

    .line 1644653
    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    invoke-virtual {v0}, LX/0zA;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1644654
    invoke-super {p0, p1, p2}, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1644655
    iget-object v0, p0, Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1644656
    iget-boolean v0, p0, Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;->b:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1644657
    iget-object v0, p0, Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;->c:LX/ADX;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1644658
    return-void
.end method
