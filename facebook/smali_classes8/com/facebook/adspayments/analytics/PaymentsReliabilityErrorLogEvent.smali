.class public Lcom/facebook/adspayments/analytics/PaymentsReliabilityErrorLogEvent;
.super Lcom/facebook/adspayments/analytics/PaymentsReliabilityLogEvent;
.source ""


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)V
    .locals 0

    .prologue
    .line 1644867
    invoke-direct {p0, p2}, Lcom/facebook/adspayments/analytics/PaymentsReliabilityLogEvent;-><init>(Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)V

    .line 1644868
    invoke-virtual {p0, p1}, Lcom/facebook/adspayments/analytics/BasePaymentsLogEvent;->m(Ljava/lang/String;)Lcom/facebook/adspayments/analytics/BasePaymentsLogEvent;

    .line 1644869
    return-void
.end method

.method public constructor <init>(Ljava/lang/Throwable;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)V
    .locals 0

    .prologue
    .line 1644870
    invoke-direct {p0, p2}, Lcom/facebook/adspayments/analytics/PaymentsReliabilityLogEvent;-><init>(Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)V

    .line 1644871
    invoke-virtual {p0, p1}, Lcom/facebook/adspayments/analytics/BasePaymentsLogEvent;->a(Ljava/lang/Throwable;)Lcom/facebook/adspayments/analytics/BasePaymentsLogEvent;

    .line 1644872
    return-void
.end method


# virtual methods
.method public final v()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1644873
    const-string v0, "process_error"

    return-object v0
.end method
