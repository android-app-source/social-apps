.class public final Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;


# direct methods
.method public constructor <init>(Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;)V
    .locals 0

    .prologue
    .line 1417225
    iput-object p1, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable$1;->a:Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 1417226
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable$1;->a:Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    iget-object v0, v0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    iget v0, v0, LX/8vj;->aP:I

    .line 1417227
    iget-object v1, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable$1;->a:Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    iget-object v1, v1, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    iget-object v1, v1, LX/8vj;->au:Landroid/view/VelocityTracker;

    .line 1417228
    iget-object v2, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable$1;->a:Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    iget-object v2, v2, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->b:LX/8vs;

    .line 1417229
    if-eqz v1, :cond_0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_1

    .line 1417230
    :cond_0
    :goto_0
    return-void

    .line 1417231
    :cond_1
    const/16 v3, 0x3e8

    iget-object v4, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable$1;->a:Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    iget-object v4, v4, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    iget v4, v4, LX/8vj;->aN:I

    int-to-float v4, v4

    invoke-virtual {v1, v3, v4}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 1417232
    invoke-virtual {v1, v0}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v0

    neg-float v0, v0

    .line 1417233
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget-object v3, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable$1;->a:Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    iget-object v3, v3, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    iget v3, v3, LX/8vj;->aM:I

    int-to-float v3, v3

    cmpl-float v1, v1, v3

    if-ltz v1, :cond_2

    const/4 v1, 0x0

    .line 1417234
    iget-object v3, v2, LX/8vs;->b:LX/8vr;

    iget v3, v3, LX/8vr;->c:I

    iget-object v4, v2, LX/8vs;->b:LX/8vr;

    iget v4, v4, LX/8vr;->a:I

    sub-int/2addr v3, v4

    .line 1417235
    iget-object v4, v2, LX/8vs;->c:LX/8vr;

    iget v4, v4, LX/8vr;->c:I

    iget-object v5, v2, LX/8vs;->c:LX/8vr;

    iget v5, v5, LX/8vr;->a:I

    sub-int/2addr v4, v5

    .line 1417236
    invoke-virtual {v2}, LX/8vs;->a()Z

    move-result v5

    if-nez v5, :cond_3

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v5

    int-to-float v3, v3

    invoke-static {v3}, Ljava/lang/Math;->signum(F)F

    move-result v3

    cmpl-float v3, v5, v3

    if-nez v3, :cond_3

    invoke-static {v1}, Ljava/lang/Math;->signum(F)F

    move-result v3

    int-to-float v4, v4

    invoke-static {v4}, Ljava/lang/Math;->signum(F)F

    move-result v4

    cmpl-float v3, v3, v4

    if-nez v3, :cond_3

    const/4 v3, 0x1

    :goto_1
    move v0, v3

    .line 1417237
    if-eqz v0, :cond_2

    .line 1417238
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable$1;->a:Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    iget-object v0, v0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    const-wide/16 v2, 0x28

    invoke-virtual {v0, p0, v2, v3}, LX/8vj;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 1417239
    :cond_2
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable$1;->a:Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    invoke-virtual {v0}, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->b()V

    .line 1417240
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable$1;->a:Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    iget-object v0, v0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    const/4 v1, 0x3

    iput v1, v0, LX/8vj;->F:I

    .line 1417241
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable$1;->a:Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    iget-object v0, v0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/8vj;->a(I)V

    goto :goto_0

    :cond_3
    const/4 v3, 0x0

    goto :goto_1
.end method
