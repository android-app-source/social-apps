.class public final Lit/sephiroth/android/library/widget/AbsHListView$CheckForKeyLongPress;
.super LX/8vb;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/8vj;


# direct methods
.method public constructor <init>(LX/8vj;)V
    .locals 1

    .prologue
    .line 1417169
    iput-object p1, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForKeyLongPress;->a:LX/8vj;

    invoke-direct {p0, p1}, LX/8vb;-><init>(LX/8vj;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1417170
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForKeyLongPress;->a:LX/8vj;

    invoke-virtual {v0}, LX/8vj;->isPressed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForKeyLongPress;->a:LX/8vj;

    iget v0, v0, LX/8vi;->am:I

    if-ltz v0, :cond_0

    .line 1417171
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForKeyLongPress;->a:LX/8vj;

    iget v0, v0, LX/8vi;->am:I

    iget-object v2, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForKeyLongPress;->a:LX/8vj;

    iget v2, v2, LX/8vi;->V:I

    sub-int/2addr v0, v2

    .line 1417172
    iget-object v2, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForKeyLongPress;->a:LX/8vj;

    invoke-virtual {v2, v0}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1417173
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForKeyLongPress;->a:LX/8vj;

    iget-boolean v0, v0, LX/8vi;->aj:Z

    if-nez v0, :cond_1

    .line 1417174
    invoke-virtual {p0}, LX/8vb;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1417175
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForKeyLongPress;->a:LX/8vj;

    iget-object v3, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForKeyLongPress;->a:LX/8vj;

    iget v3, v3, LX/8vi;->am:I

    iget-object v4, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForKeyLongPress;->a:LX/8vj;

    iget-wide v4, v4, LX/8vi;->an:J

    invoke-virtual {v0, v2, v3, v4, v5}, LX/8vj;->b(Landroid/view/View;IJ)Z

    move-result v0

    .line 1417176
    :goto_0
    if-eqz v0, :cond_0

    .line 1417177
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForKeyLongPress;->a:LX/8vj;

    invoke-virtual {v0, v1}, LX/8vj;->setPressed(Z)V

    .line 1417178
    invoke-virtual {v2, v1}, Landroid/view/View;->setPressed(Z)V

    .line 1417179
    :cond_0
    :goto_1
    return-void

    .line 1417180
    :cond_1
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForKeyLongPress;->a:LX/8vj;

    invoke-virtual {v0, v1}, LX/8vj;->setPressed(Z)V

    .line 1417181
    if-eqz v2, :cond_0

    invoke-virtual {v2, v1}, Landroid/view/View;->setPressed(Z)V

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0
.end method
