.class public final Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/8vj;

.field public final b:LX/8vs;

.field public c:I

.field private final d:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(LX/8vj;)V
    .locals 2

    .prologue
    .line 1417349
    iput-object p1, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1417350
    new-instance v0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable$1;

    invoke-direct {v0, p0}, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable$1;-><init>(Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;)V

    iput-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->d:Ljava/lang/Runnable;

    .line 1417351
    new-instance v0, LX/8vs;

    invoke-virtual {p1}, LX/8vj;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/8vs;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->b:LX/8vs;

    .line 1417352
    return-void
.end method

.method private c(I)V
    .locals 7

    .prologue
    .line 1417328
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->b:LX/8vs;

    iget-object v1, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    invoke-virtual {v1}, LX/8vj;->getScrollX()I

    move-result v1

    const/4 v2, 0x0

    iget-object v3, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    iget v3, v3, LX/8vj;->R:I

    .line 1417329
    iget-object v4, v0, LX/8vs;->b:LX/8vr;

    .line 1417330
    iget v5, v4, LX/8vr;->n:I

    if-nez v5, :cond_0

    .line 1417331
    iput v3, v4, LX/8vr;->l:I

    .line 1417332
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v5

    iput-wide v5, v4, LX/8vr;->g:J

    .line 1417333
    iget v5, v4, LX/8vr;->e:F

    float-to-int v5, v5

    invoke-static {v4, v1, v2, v2, v5}, LX/8vr;->a(LX/8vr;IIII)V

    .line 1417334
    :cond_0
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    invoke-virtual {v0}, LX/8vj;->getOverScrollMode()I

    move-result v0

    .line 1417335
    if-eqz v0, :cond_1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    .line 1417336
    invoke-static {v0}, LX/8vj;->r(LX/8vj;)Z

    move-result v1

    move v0, v1

    .line 1417337
    if-nez v0, :cond_4

    .line 1417338
    :cond_1
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    const/4 v1, 0x6

    iput v1, v0, LX/8vj;->F:I

    .line 1417339
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->b:LX/8vs;

    invoke-virtual {v0}, LX/8vs;->c()F

    move-result v0

    float-to-int v0, v0

    .line 1417340
    if-lez p1, :cond_3

    .line 1417341
    iget-object v1, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    iget-object v1, v1, LX/8vj;->aQ:LX/8vn;

    invoke-virtual {v1, v0}, LX/8vn;->a(I)V

    .line 1417342
    :cond_2
    :goto_0
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    invoke-virtual {v0}, LX/8vj;->invalidate()V

    .line 1417343
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    iget-object v0, v0, LX/8vj;->a:LX/8vS;

    invoke-virtual {v0, p0}, LX/8vS;->a(Ljava/lang/Runnable;)V

    .line 1417344
    return-void

    .line 1417345
    :cond_3
    iget-object v1, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    iget-object v1, v1, LX/8vj;->aR:LX/8vn;

    invoke-virtual {v1, v0}, LX/8vn;->a(I)V

    goto :goto_0

    .line 1417346
    :cond_4
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    const/4 v1, -0x1

    iput v1, v0, LX/8vj;->F:I

    .line 1417347
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    iget-object v0, v0, LX/8vj;->I:Lit/sephiroth/android/library/widget/AbsHListView$PositionScroller;

    if-eqz v0, :cond_2

    .line 1417348
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    iget-object v0, v0, LX/8vj;->I:Lit/sephiroth/android/library/widget/AbsHListView$PositionScroller;

    invoke-virtual {v0}, Lit/sephiroth/android/library/widget/AbsHListView$PositionScroller;->a()V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 1417316
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->b:LX/8vs;

    iget-object v1, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    invoke-virtual {v1}, LX/8vj;->getScrollX()I

    move-result v1

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    const/4 v7, 0x1

    .line 1417317
    iput v7, v0, LX/8vs;->a:I

    .line 1417318
    iget-object v8, v0, LX/8vs;->b:LX/8vr;

    invoke-virtual {v8, v1, v3, v4}, LX/8vr;->b(III)Z

    move-result v8

    .line 1417319
    iget-object v9, v0, LX/8vs;->c:LX/8vr;

    invoke-virtual {v9, v2, v5, v6}, LX/8vr;->b(III)Z

    move-result v9

    .line 1417320
    if-nez v8, :cond_0

    if-eqz v9, :cond_2

    :cond_0
    :goto_0
    move v0, v7

    .line 1417321
    if-eqz v0, :cond_1

    .line 1417322
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    const/4 v1, 0x6

    iput v1, v0, LX/8vj;->F:I

    .line 1417323
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    invoke-virtual {v0}, LX/8vj;->invalidate()V

    .line 1417324
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    iget-object v0, v0, LX/8vj;->a:LX/8vS;

    invoke-virtual {v0, p0}, LX/8vS;->a(Ljava/lang/Runnable;)V

    .line 1417325
    :goto_1
    return-void

    .line 1417326
    :cond_1
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    const/4 v1, -0x1

    iput v1, v0, LX/8vj;->F:I

    .line 1417327
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    invoke-virtual {v0, v2}, LX/8vj;->a(I)V

    goto :goto_1

    :cond_2
    const/4 v7, 0x0

    goto :goto_0
.end method

.method public final a(I)V
    .locals 9

    .prologue
    const v6, 0x7fffffff

    const/4 v2, 0x0

    .line 1417353
    if-gez p1, :cond_0

    move v1, v6

    .line 1417354
    :goto_0
    iput v1, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->c:I

    .line 1417355
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->b:LX/8vs;

    const/4 v3, 0x0

    .line 1417356
    iput-object v3, v0, LX/8vs;->d:Landroid/view/animation/Interpolator;

    .line 1417357
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->b:LX/8vs;

    move v3, p1

    move v4, v2

    move v5, v2

    move v7, v2

    move v8, v6

    invoke-virtual/range {v0 .. v8}, LX/8vs;->a(IIIIIIII)V

    .line 1417358
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    const/4 v1, 0x4

    iput v1, v0, LX/8vj;->F:I

    .line 1417359
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    iget-object v0, v0, LX/8vj;->a:LX/8vS;

    invoke-virtual {v0, p0}, LX/8vS;->a(Ljava/lang/Runnable;)V

    .line 1417360
    return-void

    :cond_0
    move v1, v2

    .line 1417361
    goto :goto_0
.end method

.method public final b()V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 1417249
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    const/4 v2, -0x1

    iput v2, v0, LX/8vj;->F:I

    .line 1417250
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    invoke-virtual {v0, p0}, LX/8vj;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1417251
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    iget-object v2, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->d:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, LX/8vj;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1417252
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    invoke-virtual {v0, v1}, LX/8vj;->a(I)V

    .line 1417253
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    invoke-static {v0}, LX/8vj;->B(LX/8vj;)V

    .line 1417254
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->b:LX/8vs;

    invoke-virtual {v0}, LX/8vs;->e()V

    .line 1417255
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    invoke-static/range {v0 .. v9}, LX/8vj;->a(LX/8vj;IIIIIIIIZ)Z

    .line 1417256
    return-void
.end method

.method public final b(I)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 1417242
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->b:LX/8vs;

    const/4 v1, 0x0

    .line 1417243
    iput-object v1, v0, LX/8vs;->d:Landroid/view/animation/Interpolator;

    .line 1417244
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->b:LX/8vs;

    iget-object v1, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    invoke-virtual {v1}, LX/8vj;->getScrollX()I

    move-result v1

    const/high16 v5, -0x80000000

    const v6, 0x7fffffff

    iget-object v3, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    invoke-virtual {v3}, LX/8vj;->getWidth()I

    move-result v9

    move v3, p1

    move v4, v2

    move v7, v2

    move v8, v2

    move v10, v2

    invoke-virtual/range {v0 .. v10}, LX/8vs;->a(IIIIIIIIII)V

    .line 1417245
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    const/4 v1, 0x6

    iput v1, v0, LX/8vj;->F:I

    .line 1417246
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    invoke-virtual {v0}, LX/8vj;->invalidate()V

    .line 1417247
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    iget-object v0, v0, LX/8vj;->a:LX/8vS;

    invoke-virtual {v0, p0}, LX/8vS;->a(Ljava/lang/Runnable;)V

    .line 1417248
    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 1417314
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    iget-object v1, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->d:Ljava/lang/Runnable;

    const-wide/16 v2, 0x28

    invoke-virtual {v0, v1, v2, v3}, LX/8vj;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1417315
    return-void
.end method

.method public final run()V
    .locals 13

    .prologue
    const/4 v11, 0x1

    const/4 v2, 0x0

    .line 1417257
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    iget v0, v0, LX/8vj;->F:I

    packed-switch v0, :pswitch_data_0

    .line 1417258
    :pswitch_0
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->b()V

    .line 1417259
    :cond_0
    :goto_0
    return-void

    .line 1417260
    :pswitch_1
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->b:LX/8vs;

    invoke-virtual {v0}, LX/8vs;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1417261
    :pswitch_2
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    iget-boolean v0, v0, LX/8vi;->aj:Z

    if-eqz v0, :cond_1

    .line 1417262
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    invoke-virtual {v0}, LX/8vj;->d()V

    .line 1417263
    :cond_1
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    iget v0, v0, LX/8vi;->ao:I

    if-eqz v0, :cond_2

    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    invoke-virtual {v0}, LX/8vj;->getChildCount()I

    move-result v0

    if-nez v0, :cond_3

    .line 1417264
    :cond_2
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->b()V

    goto :goto_0

    .line 1417265
    :cond_3
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->b:LX/8vs;

    .line 1417266
    invoke-virtual {v0}, LX/8vs;->d()Z

    move-result v12

    .line 1417267
    invoke-virtual {v0}, LX/8vs;->b()I

    move-result v1

    .line 1417268
    iget v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->c:I

    sub-int/2addr v0, v1

    .line 1417269
    if-lez v0, :cond_5

    .line 1417270
    iget-object v3, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    iget-object v4, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    iget v4, v4, LX/8vi;->V:I

    iput v4, v3, LX/8vj;->A:I

    .line 1417271
    iget-object v3, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    invoke-virtual {v3, v2}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1417272
    iget-object v4, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v3

    iput v3, v4, LX/8vj;->B:I

    .line 1417273
    iget-object v3, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    invoke-virtual {v3}, LX/8vj;->getWidth()I

    move-result v3

    iget-object v4, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    invoke-virtual {v4}, LX/8vj;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    iget-object v4, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    invoke-virtual {v4}, LX/8vj;->getPaddingLeft()I

    move-result v4

    sub-int/2addr v3, v4

    add-int/lit8 v3, v3, -0x1

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    move v10, v0

    .line 1417274
    :goto_1
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    iget-object v3, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    iget v3, v3, LX/8vj;->A:I

    iget-object v4, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    iget v4, v4, LX/8vi;->V:I

    sub-int/2addr v3, v4

    invoke-virtual {v0, v3}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1417275
    if-eqz v3, :cond_11

    .line 1417276
    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v0

    .line 1417277
    :goto_2
    iget-object v4, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    invoke-virtual {v4, v10, v10}, LX/8vj;->a(II)Z

    move-result v4

    .line 1417278
    if-eqz v4, :cond_6

    if-eqz v10, :cond_6

    .line 1417279
    :goto_3
    if-eqz v11, :cond_7

    .line 1417280
    if-eqz v3, :cond_4

    .line 1417281
    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v1

    sub-int v0, v1, v0

    sub-int v0, v10, v0

    neg-int v1, v0

    .line 1417282
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    iget-object v3, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    invoke-virtual {v3}, LX/8vj;->getScrollX()I

    move-result v3

    iget-object v4, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    iget v7, v4, LX/8vj;->R:I

    move v4, v2

    move v5, v2

    move v6, v2

    move v8, v2

    move v9, v2

    invoke-static/range {v0 .. v9}, LX/8vj;->b(LX/8vj;IIIIIIIIZ)Z

    .line 1417283
    :cond_4
    if-eqz v12, :cond_0

    .line 1417284
    invoke-direct {p0, v10}, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->c(I)V

    goto/16 :goto_0

    .line 1417285
    :cond_5
    iget-object v3, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    invoke-virtual {v3}, LX/8vj;->getChildCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    .line 1417286
    iget-object v4, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    iget-object v5, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    iget v5, v5, LX/8vi;->V:I

    add-int/2addr v5, v3

    iput v5, v4, LX/8vj;->A:I

    .line 1417287
    iget-object v4, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    invoke-virtual {v4, v3}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1417288
    iget-object v4, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v3

    iput v3, v4, LX/8vj;->B:I

    .line 1417289
    iget-object v3, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    invoke-virtual {v3}, LX/8vj;->getWidth()I

    move-result v3

    iget-object v4, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    invoke-virtual {v4}, LX/8vj;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    iget-object v4, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    invoke-virtual {v4}, LX/8vj;->getPaddingLeft()I

    move-result v4

    sub-int/2addr v3, v4

    add-int/lit8 v3, v3, -0x1

    neg-int v3, v3

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v10, v0

    goto/16 :goto_1

    :cond_6
    move v11, v2

    .line 1417290
    goto :goto_3

    .line 1417291
    :cond_7
    if-eqz v12, :cond_9

    if-nez v11, :cond_9

    .line 1417292
    if-eqz v4, :cond_8

    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    invoke-virtual {v0}, LX/8vj;->invalidate()V

    .line 1417293
    :cond_8
    iput v1, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->c:I

    .line 1417294
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    iget-object v0, v0, LX/8vj;->a:LX/8vS;

    invoke-virtual {v0, p0}, LX/8vS;->a(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 1417295
    :cond_9
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->b()V

    goto/16 :goto_0

    .line 1417296
    :pswitch_3
    iget-object v10, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->b:LX/8vs;

    .line 1417297
    invoke-virtual {v10}, LX/8vs;->d()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1417298
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    invoke-virtual {v0}, LX/8vj;->getScrollX()I

    move-result v3

    .line 1417299
    invoke-virtual {v10}, LX/8vs;->b()I

    move-result v12

    .line 1417300
    sub-int v1, v12, v3

    .line 1417301
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    iget-object v4, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    iget v7, v4, LX/8vj;->R:I

    move v4, v2

    move v5, v2

    move v6, v2

    move v8, v2

    move v9, v2

    invoke-static/range {v0 .. v9}, LX/8vj;->c(LX/8vj;IIIIIIIIZ)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1417302
    if-gtz v3, :cond_d

    if-lez v12, :cond_d

    move v0, v11

    .line 1417303
    :goto_4
    if-ltz v3, :cond_a

    if-gez v12, :cond_a

    move v2, v11

    .line 1417304
    :cond_a
    if-nez v0, :cond_b

    if-eqz v2, :cond_e

    .line 1417305
    :cond_b
    invoke-virtual {v10}, LX/8vs;->c()F

    move-result v0

    float-to-int v0, v0

    .line 1417306
    if-eqz v2, :cond_c

    neg-int v0, v0

    .line 1417307
    :cond_c
    invoke-virtual {v10}, LX/8vs;->e()V

    .line 1417308
    invoke-virtual {p0, v0}, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a(I)V

    goto/16 :goto_0

    :cond_d
    move v0, v2

    .line 1417309
    goto :goto_4

    .line 1417310
    :cond_e
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a()V

    goto/16 :goto_0

    .line 1417311
    :cond_f
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    invoke-virtual {v0}, LX/8vj;->invalidate()V

    .line 1417312
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    iget-object v0, v0, LX/8vj;->a:LX/8vS;

    invoke-virtual {v0, p0}, LX/8vS;->a(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 1417313
    :cond_10
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->b()V

    goto/16 :goto_0

    :cond_11
    move v0, v2

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
