.class public Lit/sephiroth/android/library/widget/HListView;
.super LX/8vj;
.source ""


# annotations
.annotation runtime Landroid/widget/RemoteViews$RemoteView;
.end annotation


# instance fields
.field private aA:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/8vp;",
            ">;"
        }
    .end annotation
.end field

.field private aB:Z

.field private aC:Z

.field private aD:Z

.field private aE:Z

.field private aF:Z

.field private aG:Z

.field private final aH:Landroid/graphics/Rect;

.field private aI:Landroid/graphics/Paint;

.field private final aJ:LX/8vo;

.field private aK:Lit/sephiroth/android/library/widget/HListView$FocusSelector;

.field public au:Landroid/graphics/drawable/Drawable;

.field public av:I

.field public aw:I

.field public ax:Landroid/graphics/drawable/Drawable;

.field public ay:Landroid/graphics/drawable/Drawable;

.field private az:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/8vp;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1421042
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lit/sephiroth/android/library/widget/HListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1421043
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1420744
    const v0, 0x7f010762

    invoke-direct {p0, p1, p2, v0}, Lit/sephiroth/android/library/widget/HListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1420745
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 10

    .prologue
    const/4 v0, -0x1

    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1420711
    invoke-direct {p0, p1, p2, p3}, LX/8vj;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1420712
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lit/sephiroth/android/library/widget/HListView;->az:Ljava/util/ArrayList;

    .line 1420713
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lit/sephiroth/android/library/widget/HListView;->aA:Ljava/util/ArrayList;

    .line 1420714
    iput-boolean v1, p0, Lit/sephiroth/android/library/widget/HListView;->aF:Z

    .line 1420715
    iput-boolean v2, p0, Lit/sephiroth/android/library/widget/HListView;->aG:Z

    .line 1420716
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    iput-object v4, p0, Lit/sephiroth/android/library/widget/HListView;->aH:Landroid/graphics/Rect;

    .line 1420717
    new-instance v4, LX/8vo;

    invoke-direct {v4}, LX/8vo;-><init>()V

    iput-object v4, p0, Lit/sephiroth/android/library/widget/HListView;->aJ:LX/8vo;

    .line 1420718
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    .line 1420719
    sget-object v5, LX/KCg;->HListView:[I

    invoke-virtual {v4, p2, v5, p3, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v8

    .line 1420720
    if-eqz v8, :cond_5

    .line 1420721
    const/16 v3, 0x0

    invoke-virtual {v8, v3}, Landroid/content/res/TypedArray;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v7

    .line 1420722
    const/16 v3, 0x1

    invoke-virtual {v8, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    .line 1420723
    const/16 v3, 0x5

    invoke-virtual {v8, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 1420724
    const/16 v3, 0x6

    invoke-virtual {v8, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 1420725
    const/16 v3, 0x2

    invoke-virtual {v8, v3, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    .line 1420726
    const/16 v2, 0x3

    invoke-virtual {v8, v2, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    .line 1420727
    const/16 v9, 0x4

    invoke-virtual {v8, v9, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    .line 1420728
    const/16 v9, 0x7

    invoke-virtual {v8, v9, v0}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    .line 1420729
    invoke-virtual {v8}, Landroid/content/res/TypedArray;->recycle()V

    .line 1420730
    :goto_0
    if-eqz v7, :cond_0

    .line 1420731
    new-instance v8, Landroid/widget/ArrayAdapter;

    const v9, 0x1090003

    invoke-direct {v8, p1, v9, v7}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {p0, v8}, LX/8vj;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1420732
    :cond_0
    if-eqz v6, :cond_1

    .line 1420733
    invoke-virtual {p0, v6}, Lit/sephiroth/android/library/widget/HListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 1420734
    :cond_1
    if-eqz v5, :cond_2

    .line 1420735
    invoke-virtual {p0, v5}, Lit/sephiroth/android/library/widget/HListView;->setOverscrollHeader(Landroid/graphics/drawable/Drawable;)V

    .line 1420736
    :cond_2
    if-eqz v4, :cond_3

    .line 1420737
    invoke-virtual {p0, v4}, Lit/sephiroth/android/library/widget/HListView;->setOverscrollFooter(Landroid/graphics/drawable/Drawable;)V

    .line 1420738
    :cond_3
    if-eqz v3, :cond_4

    .line 1420739
    invoke-virtual {p0, v3}, Lit/sephiroth/android/library/widget/HListView;->setDividerWidth(I)V

    .line 1420740
    :cond_4
    iput-boolean v2, p0, Lit/sephiroth/android/library/widget/HListView;->aD:Z

    .line 1420741
    iput-boolean v1, p0, Lit/sephiroth/android/library/widget/HListView;->aE:Z

    .line 1420742
    iput v0, p0, Lit/sephiroth/android/library/widget/HListView;->aw:I

    .line 1420743
    return-void

    :cond_5
    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    move v3, v2

    move v2, v1

    goto :goto_0
.end method

.method private a(IIIII)I
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v8, -0x1

    .line 1420687
    iget-object v4, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    .line 1420688
    if-nez v4, :cond_1

    .line 1420689
    iget-object v0, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int v2, v0, v1

    .line 1420690
    :cond_0
    :goto_0
    return v2

    .line 1420691
    :cond_1
    iget-object v0, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    .line 1420692
    iget v1, p0, Lit/sephiroth/android/library/widget/HListView;->av:I

    if-lez v1, :cond_5

    iget-object v1, p0, Lit/sephiroth/android/library/widget/HListView;->au:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_5

    iget v1, p0, Lit/sephiroth/android/library/widget/HListView;->av:I

    move v3, v1

    .line 1420693
    :goto_1
    if-ne p3, v8, :cond_2

    invoke-interface {v4}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    add-int/lit8 p3, v1, -0x1

    .line 1420694
    :cond_2
    iget-object v4, p0, LX/8vj;->p:LX/8vf;

    .line 1420695
    const/4 v1, 0x1

    move v5, v1

    .line 1420696
    iget-object v6, p0, LX/8vj;->P:[Z

    .line 1420697
    :goto_2
    if-gt p2, p3, :cond_7

    .line 1420698
    invoke-virtual {p0, p2, v6}, LX/8vj;->a(I[Z)Landroid/view/View;

    move-result-object v7

    .line 1420699
    invoke-direct {p0, v7, p2, p1}, Lit/sephiroth/android/library/widget/HListView;->a(Landroid/view/View;II)V

    .line 1420700
    if-lez p2, :cond_9

    .line 1420701
    add-int/2addr v0, v3

    move v1, v0

    .line 1420702
    :goto_3
    if-eqz v5, :cond_3

    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/8vc;

    iget v0, v0, LX/8vc;->a:I

    invoke-static {v0}, LX/8vf;->b(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1420703
    invoke-virtual {v4, v7, v8}, LX/8vf;->a(Landroid/view/View;I)V

    .line 1420704
    :cond_3
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    add-int/2addr v1, v0

    .line 1420705
    if-lt v1, p4, :cond_6

    .line 1420706
    if-ltz p5, :cond_4

    if-le p2, p5, :cond_4

    if-lez v2, :cond_4

    if-ne v1, p4, :cond_0

    :cond_4
    move v2, p4

    goto :goto_0

    :cond_5
    move v3, v2

    .line 1420707
    goto :goto_1

    .line 1420708
    :cond_6
    if-ltz p5, :cond_8

    if-lt p2, p5, :cond_8

    move v0, v1

    .line 1420709
    :goto_4
    add-int/lit8 p2, p2, 0x1

    move v2, v0

    move v0, v1

    goto :goto_2

    :cond_7
    move v2, v0

    .line 1420710
    goto :goto_0

    :cond_8
    move v0, v2

    goto :goto_4

    :cond_9
    move v1, v0

    goto :goto_3
.end method

.method private a(ILandroid/view/View;I)I
    .locals 3

    .prologue
    .line 1420673
    const/4 v0, 0x0

    .line 1420674
    iget-object v1, p0, Lit/sephiroth/android/library/widget/HListView;->aH:Landroid/graphics/Rect;

    invoke-virtual {p2, v1}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 1420675
    iget-object v1, p0, Lit/sephiroth/android/library/widget/HListView;->aH:Landroid/graphics/Rect;

    invoke-virtual {p0, p2, v1}, Lit/sephiroth/android/library/widget/HListView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 1420676
    const/16 v1, 0x21

    if-ne p1, v1, :cond_1

    .line 1420677
    iget-object v1, p0, Lit/sephiroth/android/library/widget/HListView;->aH:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    if-ge v1, v2, :cond_0

    .line 1420678
    iget-object v0, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lit/sephiroth/android/library/widget/HListView;->aH:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    .line 1420679
    if-lez p3, :cond_0

    .line 1420680
    invoke-direct {p0}, Lit/sephiroth/android/library/widget/HListView;->getArrowScrollPreviewLength()I

    move-result v1

    add-int/2addr v0, v1

    .line 1420681
    :cond_0
    :goto_0
    return v0

    .line 1420682
    :cond_1
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getWidth()I

    move-result v1

    iget-object v2, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v1, v2

    .line 1420683
    iget-object v2, p0, Lit/sephiroth/android/library/widget/HListView;->aH:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    if-le v2, v1, :cond_0

    .line 1420684
    iget-object v0, p0, Lit/sephiroth/android/library/widget/HListView;->aH:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v1

    .line 1420685
    iget v1, p0, LX/8vi;->ao:I

    add-int/lit8 v1, v1, -0x1

    if-ge p3, v1, :cond_0

    .line 1420686
    invoke-direct {p0}, Lit/sephiroth/android/library/widget/HListView;->getArrowScrollPreviewLength()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method private a(III)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v3, 0x1

    .line 1420653
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getHorizontalFadingEdgeLength()I

    move-result v0

    .line 1420654
    iget v1, p0, LX/8vi;->am:I

    .line 1420655
    invoke-static {p2, v0, v1}, Lit/sephiroth/android/library/widget/HListView;->c(III)I

    move-result v6

    .line 1420656
    invoke-direct {p0, p3, v0, v1}, Lit/sephiroth/android/library/widget/HListView;->b(III)I

    move-result v7

    .line 1420657
    iget-object v0, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v4, v0, Landroid/graphics/Rect;->top:I

    move-object v0, p0

    move v2, p1

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lit/sephiroth/android/library/widget/HListView;->a(IIZIZ)Landroid/view/View;

    move-result-object v0

    .line 1420658
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v2

    if-le v2, v7, :cond_1

    .line 1420659
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v2

    sub-int/2addr v2, v6

    .line 1420660
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v3

    sub-int/2addr v3, v7

    .line 1420661
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 1420662
    neg-int v2, v2

    invoke-virtual {v0, v2}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 1420663
    :cond_0
    :goto_0
    invoke-direct {p0, v0, v1}, Lit/sephiroth/android/library/widget/HListView;->a(Landroid/view/View;I)V

    .line 1420664
    iget-boolean v1, p0, LX/8vj;->K:Z

    if-nez v1, :cond_2

    .line 1420665
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getChildCount()I

    move-result v1

    invoke-direct {p0, v1}, Lit/sephiroth/android/library/widget/HListView;->f(I)V

    .line 1420666
    :goto_1
    return-object v0

    .line 1420667
    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v2

    if-ge v2, v6, :cond_0

    .line 1420668
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v2

    sub-int v2, v6, v2

    .line 1420669
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v3

    sub-int v3, v7, v3

    .line 1420670
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 1420671
    invoke-virtual {v0, v2}, Landroid/view/View;->offsetLeftAndRight(I)V

    goto :goto_0

    .line 1420672
    :cond_2
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getChildCount()I

    move-result v1

    invoke-direct {p0, v1}, Lit/sephiroth/android/library/widget/HListView;->g(I)V

    goto :goto_1
.end method

.method private a(IIZIZ)Landroid/view/View;
    .locals 8

    .prologue
    .line 1420640
    iget-boolean v0, p0, LX/8vi;->aj:Z

    if-nez v0, :cond_0

    .line 1420641
    iget-object v0, p0, LX/8vj;->p:LX/8vf;

    const/4 v2, 0x0

    .line 1420642
    iget v1, v0, LX/8vf;->c:I

    sub-int v3, p1, v1

    .line 1420643
    iget-object v4, v0, LX/8vf;->d:[Landroid/view/View;

    .line 1420644
    if-ltz v3, :cond_1

    array-length v1, v4

    if-ge v3, v1, :cond_1

    .line 1420645
    aget-object v1, v4, v3

    .line 1420646
    aput-object v2, v4, v3

    .line 1420647
    :goto_0
    move-object v1, v1

    .line 1420648
    if-eqz v1, :cond_0

    .line 1420649
    const/4 v7, 0x1

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v7}, Lit/sephiroth/android/library/widget/HListView;->a(Landroid/view/View;IIZIZZ)V

    .line 1420650
    :goto_1
    return-object v1

    .line 1420651
    :cond_0
    iget-object v0, p0, LX/8vj;->P:[Z

    invoke-virtual {p0, p1, v0}, LX/8vj;->a(I[Z)Landroid/view/View;

    move-result-object v1

    .line 1420652
    iget-object v0, p0, LX/8vj;->P:[Z

    const/4 v2, 0x0

    aget-boolean v7, v0, v2

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v7}, Lit/sephiroth/android/library/widget/HListView;->a(Landroid/view/View;IIZIZZ)V

    goto :goto_1

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method private a(Landroid/view/View;Landroid/view/View;III)Landroid/view/View;
    .locals 11

    .prologue
    .line 1420597
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getHorizontalFadingEdgeLength()I

    move-result v0

    .line 1420598
    iget v6, p0, LX/8vi;->am:I

    .line 1420599
    invoke-static {p4, v0, v6}, Lit/sephiroth/android/library/widget/HListView;->c(III)I

    move-result v7

    .line 1420600
    invoke-direct {p0, p4, v0, v6}, Lit/sephiroth/android/library/widget/HListView;->b(III)I

    move-result v8

    .line 1420601
    if-lez p3, :cond_2

    .line 1420602
    add-int/lit8 v1, v6, -0x1

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v2

    const/4 v3, 0x1

    iget-object v0, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v4, v0, Landroid/graphics/Rect;->top:I

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lit/sephiroth/android/library/widget/HListView;->a(IIZIZ)Landroid/view/View;

    move-result-object v9

    .line 1420603
    iget v10, p0, Lit/sephiroth/android/library/widget/HListView;->av:I

    .line 1420604
    invoke-virtual {v9}, Landroid/view/View;->getRight()I

    move-result v0

    add-int v2, v0, v10

    const/4 v3, 0x1

    iget-object v0, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v4, v0, Landroid/graphics/Rect;->top:I

    const/4 v5, 0x1

    move-object v0, p0

    move v1, v6

    invoke-direct/range {v0 .. v5}, Lit/sephiroth/android/library/widget/HListView;->a(IIZIZ)Landroid/view/View;

    move-result-object v0

    .line 1420605
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v1

    if-le v1, v8, :cond_0

    .line 1420606
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    sub-int/2addr v1, v7

    .line 1420607
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v2

    sub-int/2addr v2, v8

    .line 1420608
    sub-int v3, p5, p4

    div-int/lit8 v3, v3, 0x2

    .line 1420609
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1420610
    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1420611
    neg-int v2, v1

    invoke-virtual {v9, v2}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 1420612
    neg-int v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 1420613
    :cond_0
    iget-boolean v1, p0, LX/8vj;->K:Z

    if-nez v1, :cond_1

    .line 1420614
    iget v1, p0, LX/8vi;->am:I

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v2

    sub-int/2addr v2, v10

    invoke-direct {p0, v1, v2}, Lit/sephiroth/android/library/widget/HListView;->d(II)Landroid/view/View;

    .line 1420615
    invoke-direct {p0}, Lit/sephiroth/android/library/widget/HListView;->p()V

    .line 1420616
    iget v1, p0, LX/8vi;->am:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v2

    add-int/2addr v2, v10

    invoke-direct {p0, v1, v2}, Lit/sephiroth/android/library/widget/HListView;->c(II)Landroid/view/View;

    .line 1420617
    :goto_0
    return-object v0

    .line 1420618
    :cond_1
    iget v1, p0, LX/8vi;->am:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v2

    add-int/2addr v2, v10

    invoke-direct {p0, v1, v2}, Lit/sephiroth/android/library/widget/HListView;->c(II)Landroid/view/View;

    .line 1420619
    invoke-direct {p0}, Lit/sephiroth/android/library/widget/HListView;->p()V

    .line 1420620
    iget v1, p0, LX/8vi;->am:I

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v2

    sub-int/2addr v2, v10

    invoke-direct {p0, v1, v2}, Lit/sephiroth/android/library/widget/HListView;->d(II)Landroid/view/View;

    goto :goto_0

    .line 1420621
    :cond_2
    if-gez p3, :cond_5

    .line 1420622
    if-eqz p2, :cond_4

    .line 1420623
    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v2

    const/4 v3, 0x1

    iget-object v0, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v4, v0, Landroid/graphics/Rect;->top:I

    const/4 v5, 0x1

    move-object v0, p0

    move v1, v6

    invoke-direct/range {v0 .. v5}, Lit/sephiroth/android/library/widget/HListView;->a(IIZIZ)Landroid/view/View;

    move-result-object v0

    .line 1420624
    :goto_1
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    if-ge v1, v7, :cond_3

    .line 1420625
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    sub-int v1, v7, v1

    .line 1420626
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v2

    sub-int v2, v8, v2

    .line 1420627
    sub-int v3, p5, p4

    div-int/lit8 v3, v3, 0x2

    .line 1420628
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1420629
    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1420630
    invoke-virtual {v0, v1}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 1420631
    :cond_3
    invoke-direct {p0, v0, v6}, Lit/sephiroth/android/library/widget/HListView;->a(Landroid/view/View;I)V

    goto :goto_0

    .line 1420632
    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v2

    const/4 v3, 0x0

    iget-object v0, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v4, v0, Landroid/graphics/Rect;->top:I

    const/4 v5, 0x1

    move-object v0, p0

    move v1, v6

    invoke-direct/range {v0 .. v5}, Lit/sephiroth/android/library/widget/HListView;->a(IIZIZ)Landroid/view/View;

    move-result-object v0

    goto :goto_1

    .line 1420633
    :cond_5
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v2

    .line 1420634
    const/4 v3, 0x1

    iget-object v0, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v4, v0, Landroid/graphics/Rect;->top:I

    const/4 v5, 0x1

    move-object v0, p0

    move v1, v6

    invoke-direct/range {v0 .. v5}, Lit/sephiroth/android/library/widget/HListView;->a(IIZIZ)Landroid/view/View;

    move-result-object v0

    .line 1420635
    if-ge v2, p4, :cond_6

    .line 1420636
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v1

    .line 1420637
    add-int/lit8 v2, p4, 0x14

    if-ge v1, v2, :cond_6

    .line 1420638
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    sub-int v1, p4, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 1420639
    :cond_6
    invoke-direct {p0, v0, v6}, Lit/sephiroth/android/library/widget/HListView;->a(Landroid/view/View;I)V

    goto/16 :goto_0
.end method

.method private a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 1420593
    iget-object v0, p0, Lit/sephiroth/android/library/widget/HListView;->au:Landroid/graphics/drawable/Drawable;

    .line 1420594
    invoke-virtual {v0, p2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1420595
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1420596
    return-void
.end method

.method private static a(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;Landroid/graphics/Rect;)V
    .locals 3

    .prologue
    .line 1420583
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getMinimumWidth()I

    move-result v0

    .line 1420584
    invoke-virtual {p0}, Landroid/graphics/Canvas;->save()I

    .line 1420585
    invoke-virtual {p0, p2}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 1420586
    iget v1, p2, Landroid/graphics/Rect;->right:I

    iget v2, p2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v2

    .line 1420587
    if-ge v1, v0, :cond_0

    .line 1420588
    iget v1, p2, Landroid/graphics/Rect;->right:I

    sub-int v0, v1, v0

    iput v0, p2, Landroid/graphics/Rect;->left:I

    .line 1420589
    :cond_0
    invoke-virtual {p1, p2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1420590
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1420591
    invoke-virtual {p0}, Landroid/graphics/Canvas;->restore()V

    .line 1420592
    return-void
.end method

.method private a(Landroid/view/View;I)V
    .locals 3

    .prologue
    .line 1420574
    iget v0, p0, Lit/sephiroth/android/library/widget/HListView;->av:I

    .line 1420575
    iget-boolean v1, p0, LX/8vj;->K:Z

    if-nez v1, :cond_0

    .line 1420576
    add-int/lit8 v1, p2, -0x1

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v2

    sub-int/2addr v2, v0

    invoke-direct {p0, v1, v2}, Lit/sephiroth/android/library/widget/HListView;->d(II)Landroid/view/View;

    .line 1420577
    invoke-direct {p0}, Lit/sephiroth/android/library/widget/HListView;->p()V

    .line 1420578
    add-int/lit8 v1, p2, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v2

    add-int/2addr v0, v2

    invoke-direct {p0, v1, v0}, Lit/sephiroth/android/library/widget/HListView;->c(II)Landroid/view/View;

    .line 1420579
    :goto_0
    return-void

    .line 1420580
    :cond_0
    add-int/lit8 v1, p2, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v2

    add-int/2addr v2, v0

    invoke-direct {p0, v1, v2}, Lit/sephiroth/android/library/widget/HListView;->c(II)Landroid/view/View;

    .line 1420581
    invoke-direct {p0}, Lit/sephiroth/android/library/widget/HListView;->p()V

    .line 1420582
    add-int/lit8 v1, p2, -0x1

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v2

    sub-int v0, v2, v0

    invoke-direct {p0, v1, v0}, Lit/sephiroth/android/library/widget/HListView;->d(II)Landroid/view/View;

    goto :goto_0
.end method

.method private a(Landroid/view/View;II)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1420293
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/8vc;

    .line 1420294
    if-nez v0, :cond_0

    .line 1420295
    invoke-virtual {p0}, LX/8vj;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/8vc;

    .line 1420296
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1420297
    :cond_0
    iget-object v1, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    invoke-interface {v1, p2}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v1

    iput v1, v0, LX/8vc;->a:I

    .line 1420298
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/8vc;->c:Z

    .line 1420299
    iget-object v1, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, v2

    iget v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {p3, v1, v2}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v1

    .line 1420300
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1420301
    if-lez v0, :cond_1

    .line 1420302
    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 1420303
    :goto_0
    invoke-virtual {p1, v0, v1}, Landroid/view/View;->measure(II)V

    .line 1420304
    return-void

    .line 1420305
    :cond_1
    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_0
.end method

.method private a(Landroid/view/View;IIZ)V
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1420515
    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    .line 1420516
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "newSelectedPosition needs to be valid"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1420517
    :cond_0
    iget v0, p0, LX/8vi;->am:I

    iget v1, p0, LX/8vi;->V:I

    sub-int v1, v0, v1

    .line 1420518
    iget v0, p0, LX/8vi;->V:I

    sub-int v2, p3, v0

    .line 1420519
    const/16 v0, 0x21

    if-ne p2, v0, :cond_3

    .line 1420520
    invoke-virtual {p0, v2}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    move v3, v4

    .line 1420521
    :goto_0
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getChildCount()I

    move-result v7

    .line 1420522
    if-eqz v0, :cond_1

    .line 1420523
    if-nez p4, :cond_4

    if-eqz v3, :cond_4

    move v6, v4

    :goto_1
    invoke-virtual {v0, v6}, Landroid/view/View;->setSelected(Z)V

    .line 1420524
    invoke-direct {p0, v0, v2, v7}, Lit/sephiroth/android/library/widget/HListView;->b(Landroid/view/View;II)V

    .line 1420525
    :cond_1
    if-eqz p1, :cond_2

    .line 1420526
    if-nez p4, :cond_5

    if-nez v3, :cond_5

    :goto_2
    invoke-virtual {p1, v4}, Landroid/view/View;->setSelected(Z)V

    .line 1420527
    invoke-direct {p0, p1, v1, v7}, Lit/sephiroth/android/library/widget/HListView;->b(Landroid/view/View;II)V

    .line 1420528
    :cond_2
    return-void

    .line 1420529
    :cond_3
    invoke-virtual {p0, v2}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    move v3, v5

    move-object v8, p1

    move-object p1, v0

    move-object v0, v8

    move v9, v1

    move v1, v2

    move v2, v9

    goto :goto_0

    :cond_4
    move v6, v5

    .line 1420530
    goto :goto_1

    :cond_5
    move v4, v5

    .line 1420531
    goto :goto_2
.end method

.method private a(Landroid/view/View;IIZIZZ)V
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 1420460
    if-eqz p6, :cond_8

    invoke-virtual {p0}, LX/8vj;->f()Z

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    move v1, v0

    .line 1420461
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-eq v1, v0, :cond_9

    const/4 v0, 0x1

    move v2, v0

    .line 1420462
    :goto_1
    iget v0, p0, LX/8vj;->F:I

    .line 1420463
    if-lez v0, :cond_a

    const/4 v3, 0x3

    if-ge v0, v3, :cond_a

    iget v0, p0, LX/8vj;->A:I

    if-ne v0, p2, :cond_a

    const/4 v0, 0x1

    move v3, v0

    .line 1420464
    :goto_2
    invoke-virtual {p1}, Landroid/view/View;->isPressed()Z

    move-result v0

    if-eq v3, v0, :cond_b

    const/4 v0, 0x1

    move v6, v0

    .line 1420465
    :goto_3
    if-eqz p7, :cond_0

    if-nez v2, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->isLayoutRequested()Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_0
    const/4 v0, 0x1

    move v4, v0

    .line 1420466
    :goto_4
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/8vc;

    .line 1420467
    if-nez v0, :cond_16

    .line 1420468
    invoke-virtual {p0}, LX/8vj;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/8vc;

    move-object v5, v0

    .line 1420469
    :goto_5
    iget-object v0, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    invoke-interface {v0, p2}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v0

    iput v0, v5, LX/8vc;->a:I

    .line 1420470
    if-eqz p7, :cond_1

    iget-boolean v0, v5, LX/8vc;->c:Z

    if-eqz v0, :cond_2

    :cond_1
    iget-boolean v0, v5, LX/8vc;->b:Z

    if-eqz v0, :cond_e

    iget v0, v5, LX/8vc;->a:I

    const/4 v7, -0x2

    if-ne v0, v7, :cond_e

    .line 1420471
    :cond_2
    if-eqz p4, :cond_d

    const/4 v0, -0x1

    :goto_6
    invoke-virtual {p0, p1, v0, v5}, Lit/sephiroth/android/library/widget/HListView;->attachViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 1420472
    :goto_7
    if-eqz v2, :cond_3

    .line 1420473
    invoke-virtual {p1, v1}, Landroid/view/View;->setSelected(Z)V

    .line 1420474
    :cond_3
    if-eqz v6, :cond_4

    .line 1420475
    invoke-virtual {p1, v3}, Landroid/view/View;->setPressed(Z)V

    .line 1420476
    :cond_4
    iget v0, p0, LX/8vj;->b:I

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/8vj;->f:Landroid/util/SparseBooleanArray;

    if-eqz v0, :cond_5

    .line 1420477
    instance-of v0, p1, Landroid/widget/Checkable;

    if-eqz v0, :cond_11

    move-object v0, p1

    .line 1420478
    check-cast v0, Landroid/widget/Checkable;

    iget-object v1, p0, LX/8vj;->f:Landroid/util/SparseBooleanArray;

    const/4 v2, 0x0

    invoke-virtual {v1, p2, v2}, Landroid/util/SparseBooleanArray;->get(IZ)Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/widget/Checkable;->setChecked(Z)V

    .line 1420479
    :cond_5
    :goto_8
    if-eqz v4, :cond_13

    .line 1420480
    iget v0, p0, LX/8vj;->v:I

    iget-object v1, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, v2

    iget v2, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v0, v1, v2}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v1

    .line 1420481
    iget v0, v5, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1420482
    if-lez v0, :cond_12

    .line 1420483
    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 1420484
    :goto_9
    invoke-virtual {p1, v0, v1}, Landroid/view/View;->measure(II)V

    .line 1420485
    :goto_a
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    .line 1420486
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    .line 1420487
    if-eqz p4, :cond_14

    .line 1420488
    :goto_b
    if-eqz v4, :cond_15

    .line 1420489
    add-int/2addr v1, p5

    .line 1420490
    add-int/2addr v0, p3

    .line 1420491
    invoke-virtual {p1, p3, p5, v0, v1}, Landroid/view/View;->layout(IIII)V

    .line 1420492
    :goto_c
    iget-boolean v0, p0, LX/8vj;->y:Z

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Landroid/view/View;->isDrawingCacheEnabled()Z

    move-result v0

    if-nez v0, :cond_6

    .line 1420493
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 1420494
    :cond_6
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_7

    .line 1420495
    if-eqz p7, :cond_7

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/8vc;

    iget v0, v0, LX/8vc;->d:I

    if-eq v0, p2, :cond_7

    .line 1420496
    invoke-virtual {p1}, Landroid/view/View;->jumpDrawablesToCurrentState()V

    .line 1420497
    :cond_7
    return-void

    .line 1420498
    :cond_8
    const/4 v0, 0x0

    move v1, v0

    goto/16 :goto_0

    .line 1420499
    :cond_9
    const/4 v0, 0x0

    move v2, v0

    goto/16 :goto_1

    .line 1420500
    :cond_a
    const/4 v0, 0x0

    move v3, v0

    goto/16 :goto_2

    .line 1420501
    :cond_b
    const/4 v0, 0x0

    move v6, v0

    goto/16 :goto_3

    .line 1420502
    :cond_c
    const/4 v0, 0x0

    move v4, v0

    goto/16 :goto_4

    .line 1420503
    :cond_d
    const/4 v0, 0x0

    goto/16 :goto_6

    .line 1420504
    :cond_e
    const/4 v0, 0x0

    iput-boolean v0, v5, LX/8vc;->c:Z

    .line 1420505
    iget v0, v5, LX/8vc;->a:I

    const/4 v7, -0x2

    if-ne v0, v7, :cond_f

    .line 1420506
    const/4 v0, 0x1

    iput-boolean v0, v5, LX/8vc;->b:Z

    .line 1420507
    :cond_f
    if-eqz p4, :cond_10

    const/4 v0, -0x1

    :goto_d
    const/4 v7, 0x1

    invoke-virtual {p0, p1, v0, v5, v7}, Lit/sephiroth/android/library/widget/HListView;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    goto/16 :goto_7

    :cond_10
    const/4 v0, 0x0

    goto :goto_d

    .line 1420508
    :cond_11
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_5

    .line 1420509
    iget-object v0, p0, LX/8vj;->f:Landroid/util/SparseBooleanArray;

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Landroid/util/SparseBooleanArray;->get(IZ)Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setActivated(Z)V

    goto/16 :goto_8

    .line 1420510
    :cond_12
    const/4 v0, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_9

    .line 1420511
    :cond_13
    invoke-virtual {p0, p1}, Lit/sephiroth/android/library/widget/HListView;->cleanupLayoutState(Landroid/view/View;)V

    goto :goto_a

    .line 1420512
    :cond_14
    sub-int/2addr p3, v0

    goto :goto_b

    .line 1420513
    :cond_15
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    sub-int v0, p3, v0

    invoke-virtual {p1, v0}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 1420514
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v0

    sub-int v0, p5, v0

    invoke-virtual {p1, v0}, Landroid/view/View;->offsetTopAndBottom(I)V

    goto/16 :goto_c

    :cond_16
    move-object v5, v0

    goto/16 :goto_5
.end method

.method private a(Landroid/view/View;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 1420450
    iget-object v0, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    instance-of v0, v0, LX/8vq;

    if-nez v0, :cond_0

    .line 1420451
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot add header view to list -- setAdapter has already been called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1420452
    :cond_0
    new-instance v0, LX/8vp;

    invoke-direct {v0}, LX/8vp;-><init>()V

    .line 1420453
    iput-object p1, v0, LX/8vp;->a:Landroid/view/View;

    .line 1420454
    iput-object p2, v0, LX/8vp;->b:Ljava/lang/Object;

    .line 1420455
    iput-boolean p3, v0, LX/8vp;->c:Z

    .line 1420456
    iget-object v1, p0, Lit/sephiroth/android/library/widget/HListView;->az:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1420457
    iget-object v0, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/8vj;->i:LX/8va;

    if-eqz v0, :cond_1

    .line 1420458
    iget-object v0, p0, LX/8vj;->i:LX/8va;

    invoke-virtual {v0}, LX/8vZ;->onChanged()V

    .line 1420459
    :cond_1
    return-void
.end method

.method private static a(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "LX/8vp;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1420441
    if-eqz p0, :cond_1

    .line 1420442
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    .line 1420443
    :goto_0
    if-ge v1, v3, :cond_1

    .line 1420444
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8vp;

    iget-object v0, v0, LX/8vp;->a:Landroid/view/View;

    .line 1420445
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/8vc;

    .line 1420446
    if-eqz v0, :cond_0

    .line 1420447
    iput-boolean v2, v0, LX/8vc;->b:Z

    .line 1420448
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1420449
    :cond_1
    return-void
.end method

.method private a(IILandroid/view/KeyEvent;)Z
    .locals 12
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v7, 0x2

    const/16 v6, 0x82

    const/16 v5, 0x21

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1420364
    iget-object v0, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/8vj;->S:Z

    if-nez v0, :cond_1

    .line 1420365
    :cond_0
    :goto_0
    return v1

    .line 1420366
    :cond_1
    iget-boolean v0, p0, LX/8vi;->aj:Z

    if-eqz v0, :cond_2

    .line 1420367
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->d()V

    .line 1420368
    :cond_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v0, v3, :cond_0

    .line 1420369
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v4

    .line 1420370
    if-eq v4, v2, :cond_3

    .line 1420371
    sparse-switch p1, :sswitch_data_0

    :cond_3
    move v0, v1

    .line 1420372
    :cond_4
    :goto_1
    if-eqz v0, :cond_1c

    move v1, v2

    .line 1420373
    goto :goto_0

    .line 1420374
    :sswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1420375
    invoke-virtual {p0}, LX/8vj;->j()Z

    move-result v0

    .line 1420376
    if-nez v0, :cond_4

    move v3, p2

    .line 1420377
    :goto_2
    add-int/lit8 p2, v3, -0x1

    if-lez v3, :cond_4

    .line 1420378
    invoke-direct {p0, v5}, Lit/sephiroth/android/library/widget/HListView;->k(I)Z

    move-result v3

    if-eqz v3, :cond_4

    move v0, v2

    move v3, p2

    .line 1420379
    goto :goto_2

    .line 1420380
    :cond_5
    invoke-virtual {p3, v7}, Landroid/view/KeyEvent;->hasModifiers(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1420381
    invoke-virtual {p0}, LX/8vj;->j()Z

    move-result v0

    if-nez v0, :cond_6

    invoke-direct {p0, v5}, Lit/sephiroth/android/library/widget/HListView;->i(I)Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_6
    move v0, v2

    goto :goto_1

    :cond_7
    move v0, v1

    goto :goto_1

    .line 1420382
    :sswitch_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1420383
    invoke-virtual {p0}, LX/8vj;->j()Z

    move-result v0

    .line 1420384
    if-nez v0, :cond_4

    move v3, p2

    .line 1420385
    :goto_3
    add-int/lit8 p2, v3, -0x1

    if-lez v3, :cond_4

    .line 1420386
    invoke-direct {p0, v6}, Lit/sephiroth/android/library/widget/HListView;->k(I)Z

    move-result v3

    if-eqz v3, :cond_4

    move v0, v2

    move v3, p2

    .line 1420387
    goto :goto_3

    .line 1420388
    :cond_8
    invoke-virtual {p3, v7}, Landroid/view/KeyEvent;->hasModifiers(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1420389
    invoke-virtual {p0}, LX/8vj;->j()Z

    move-result v0

    if-nez v0, :cond_9

    invoke-direct {p0, v6}, Lit/sephiroth/android/library/widget/HListView;->i(I)Z

    move-result v0

    if-eqz v0, :cond_a

    :cond_9
    move v0, v2

    goto :goto_1

    :cond_a
    move v0, v1

    goto :goto_1

    .line 1420390
    :sswitch_2
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1420391
    const/16 v0, 0x11

    invoke-direct {p0, v0}, Lit/sephiroth/android/library/widget/HListView;->j(I)Z

    move-result v0

    goto :goto_1

    .line 1420392
    :sswitch_3
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1420393
    const/16 v0, 0x42

    invoke-direct {p0, v0}, Lit/sephiroth/android/library/widget/HListView;->j(I)Z

    move-result v0

    goto :goto_1

    .line 1420394
    :sswitch_4
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1420395
    invoke-virtual {p0}, LX/8vj;->j()Z

    move-result v0

    .line 1420396
    if-nez v0, :cond_4

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v3

    if-nez v3, :cond_4

    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getChildCount()I

    move-result v3

    if-lez v3, :cond_4

    .line 1420397
    const/4 v11, 0x1

    .line 1420398
    invoke-virtual {p0}, LX/8vj;->isEnabled()Z

    move-result v8

    if-eqz v8, :cond_b

    invoke-virtual {p0}, LX/8vj;->isClickable()Z

    move-result v8

    if-nez v8, :cond_1d

    .line 1420399
    :cond_b
    :goto_4
    move v0, v2

    .line 1420400
    goto/16 :goto_1

    .line 1420401
    :sswitch_5
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1420402
    invoke-virtual {p0}, LX/8vj;->j()Z

    move-result v0

    if-nez v0, :cond_c

    invoke-direct {p0, v6}, Lit/sephiroth/android/library/widget/HListView;->h(I)Z

    :cond_c
    :goto_5
    move v0, v2

    .line 1420403
    goto/16 :goto_1

    .line 1420404
    :cond_d
    invoke-virtual {p3, v2}, Landroid/view/KeyEvent;->hasModifiers(I)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1420405
    invoke-virtual {p0}, LX/8vj;->j()Z

    move-result v0

    if-nez v0, :cond_c

    invoke-direct {p0, v5}, Lit/sephiroth/android/library/widget/HListView;->h(I)Z

    goto :goto_5

    .line 1420406
    :sswitch_6
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1420407
    invoke-virtual {p0}, LX/8vj;->j()Z

    move-result v0

    if-nez v0, :cond_e

    invoke-direct {p0, v5}, Lit/sephiroth/android/library/widget/HListView;->h(I)Z

    move-result v0

    if-eqz v0, :cond_f

    :cond_e
    move v0, v2

    goto/16 :goto_1

    :cond_f
    move v0, v1

    goto/16 :goto_1

    .line 1420408
    :cond_10
    invoke-virtual {p3, v7}, Landroid/view/KeyEvent;->hasModifiers(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1420409
    invoke-virtual {p0}, LX/8vj;->j()Z

    move-result v0

    if-nez v0, :cond_11

    invoke-direct {p0, v5}, Lit/sephiroth/android/library/widget/HListView;->i(I)Z

    move-result v0

    if-eqz v0, :cond_12

    :cond_11
    move v0, v2

    goto/16 :goto_1

    :cond_12
    move v0, v1

    goto/16 :goto_1

    .line 1420410
    :sswitch_7
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 1420411
    invoke-virtual {p0}, LX/8vj;->j()Z

    move-result v0

    if-nez v0, :cond_13

    invoke-direct {p0, v6}, Lit/sephiroth/android/library/widget/HListView;->h(I)Z

    move-result v0

    if-eqz v0, :cond_14

    :cond_13
    move v0, v2

    goto/16 :goto_1

    :cond_14
    move v0, v1

    goto/16 :goto_1

    .line 1420412
    :cond_15
    invoke-virtual {p3, v7}, Landroid/view/KeyEvent;->hasModifiers(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1420413
    invoke-virtual {p0}, LX/8vj;->j()Z

    move-result v0

    if-nez v0, :cond_16

    invoke-direct {p0, v6}, Lit/sephiroth/android/library/widget/HListView;->i(I)Z

    move-result v0

    if-eqz v0, :cond_17

    :cond_16
    move v0, v2

    goto/16 :goto_1

    :cond_17
    move v0, v1

    goto/16 :goto_1

    .line 1420414
    :sswitch_8
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1420415
    invoke-virtual {p0}, LX/8vj;->j()Z

    move-result v0

    if-nez v0, :cond_18

    invoke-direct {p0, v5}, Lit/sephiroth/android/library/widget/HListView;->i(I)Z

    move-result v0

    if-eqz v0, :cond_19

    :cond_18
    move v0, v2

    goto/16 :goto_1

    :cond_19
    move v0, v1

    goto/16 :goto_1

    .line 1420416
    :sswitch_9
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1420417
    invoke-virtual {p0}, LX/8vj;->j()Z

    move-result v0

    if-nez v0, :cond_1a

    invoke-direct {p0, v6}, Lit/sephiroth/android/library/widget/HListView;->i(I)Z

    move-result v0

    if-eqz v0, :cond_1b

    :cond_1a
    move v0, v2

    goto/16 :goto_1

    :cond_1b
    move v0, v1

    goto/16 :goto_1

    .line 1420418
    :cond_1c
    packed-switch v4, :pswitch_data_0

    goto/16 :goto_0

    .line 1420419
    :pswitch_0
    invoke-super {p0, p1, p3}, LX/8vj;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto/16 :goto_0

    .line 1420420
    :pswitch_1
    invoke-super {p0, p1, p3}, LX/8vj;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto/16 :goto_0

    .line 1420421
    :pswitch_2
    invoke-super {p0, p1, p2, p3}, LX/8vj;->onKeyMultiple(IILandroid/view/KeyEvent;)Z

    move-result v1

    goto/16 :goto_0

    .line 1420422
    :cond_1d
    iget-object v8, p0, LX/8vj;->m:Landroid/graphics/drawable/Drawable;

    .line 1420423
    iget-object v9, p0, LX/8vj;->o:Landroid/graphics/Rect;

    .line 1420424
    if-eqz v8, :cond_b

    invoke-virtual {p0}, LX/8vj;->isFocused()Z

    move-result v10

    if-nez v10, :cond_1e

    invoke-static {p0}, LX/8vj;->u(LX/8vj;)Z

    move-result v10

    if-eqz v10, :cond_b

    :cond_1e
    invoke-virtual {v9}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_b

    .line 1420425
    iget v9, p0, LX/8vi;->am:I

    iget v10, p0, LX/8vi;->V:I

    sub-int/2addr v9, v10

    invoke-virtual {p0, v9}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    .line 1420426
    if-eqz v9, :cond_1f

    .line 1420427
    invoke-virtual {v9}, Landroid/view/View;->hasFocusable()Z

    move-result v10

    if-nez v10, :cond_b

    .line 1420428
    invoke-virtual {v9, v11}, Landroid/view/View;->setPressed(Z)V

    .line 1420429
    :cond_1f
    invoke-virtual {p0, v11}, LX/8vj;->setPressed(Z)V

    .line 1420430
    invoke-virtual {p0}, LX/8vj;->isLongClickable()Z

    move-result v9

    .line 1420431
    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v8

    .line 1420432
    if-eqz v8, :cond_20

    instance-of v10, v8, Landroid/graphics/drawable/TransitionDrawable;

    if-eqz v10, :cond_20

    .line 1420433
    if-eqz v9, :cond_22

    .line 1420434
    check-cast v8, Landroid/graphics/drawable/TransitionDrawable;

    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v10

    invoke-virtual {v8, v10}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 1420435
    :cond_20
    :goto_6
    if-eqz v9, :cond_b

    iget-boolean v8, p0, LX/8vi;->aj:Z

    if-nez v8, :cond_b

    .line 1420436
    iget-object v8, p0, LX/8vj;->aD:Lit/sephiroth/android/library/widget/AbsHListView$CheckForKeyLongPress;

    if-nez v8, :cond_21

    .line 1420437
    new-instance v8, Lit/sephiroth/android/library/widget/AbsHListView$CheckForKeyLongPress;

    invoke-direct {v8, p0}, Lit/sephiroth/android/library/widget/AbsHListView$CheckForKeyLongPress;-><init>(LX/8vj;)V

    iput-object v8, p0, LX/8vj;->aD:Lit/sephiroth/android/library/widget/AbsHListView$CheckForKeyLongPress;

    .line 1420438
    :cond_21
    iget-object v8, p0, LX/8vj;->aD:Lit/sephiroth/android/library/widget/AbsHListView$CheckForKeyLongPress;

    invoke-virtual {v8}, LX/8vb;->a()V

    .line 1420439
    iget-object v8, p0, LX/8vj;->aD:Lit/sephiroth/android/library/widget/AbsHListView$CheckForKeyLongPress;

    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v9

    int-to-long v10, v9

    invoke-virtual {p0, v8, v10, v11}, LX/8vj;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_4

    .line 1420440
    :cond_22
    check-cast v8, Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v8}, Landroid/graphics/drawable/TransitionDrawable;->resetTransition()V

    goto :goto_6

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_1
        0x15 -> :sswitch_2
        0x16 -> :sswitch_3
        0x17 -> :sswitch_4
        0x3e -> :sswitch_5
        0x42 -> :sswitch_4
        0x5c -> :sswitch_6
        0x5d -> :sswitch_7
        0x7a -> :sswitch_8
        0x7b -> :sswitch_9
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(Landroid/view/View;Landroid/view/View;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1420360
    if-ne p1, p2, :cond_0

    move v0, v1

    .line 1420361
    :goto_0
    return v0

    .line 1420362
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 1420363
    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_1

    check-cast v0, Landroid/view/View;

    invoke-direct {p0, v0, p2}, Lit/sephiroth/android/library/widget/HListView;->a(Landroid/view/View;Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(III)I
    .locals 1

    .prologue
    .line 1420357
    iget v0, p0, LX/8vi;->ao:I

    add-int/lit8 v0, v0, -0x1

    if-eq p3, v0, :cond_0

    .line 1420358
    sub-int/2addr p1, p2

    .line 1420359
    :cond_0
    return p1
.end method

.method private b(Landroid/view/View;I)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 1420352
    add-int/lit8 v2, p2, -0x1

    .line 1420353
    iget-object v0, p0, LX/8vj;->P:[Z

    invoke-virtual {p0, v2, v0}, LX/8vj;->a(I[Z)Landroid/view/View;

    move-result-object v1

    .line 1420354
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    iget v3, p0, Lit/sephiroth/android/library/widget/HListView;->av:I

    sub-int v3, v0, v3

    .line 1420355
    iget-object v0, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v5, v0, Landroid/graphics/Rect;->top:I

    iget-object v0, p0, LX/8vj;->P:[Z

    aget-boolean v7, v0, v4

    move-object v0, p0

    move v6, v4

    invoke-direct/range {v0 .. v7}, Lit/sephiroth/android/library/widget/HListView;->a(Landroid/view/View;IIZIZZ)V

    .line 1420356
    return-object v1
.end method

.method private static b(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;Landroid/graphics/Rect;)V
    .locals 3

    .prologue
    .line 1420342
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getMinimumWidth()I

    move-result v0

    .line 1420343
    invoke-virtual {p0}, Landroid/graphics/Canvas;->save()I

    .line 1420344
    invoke-virtual {p0, p2}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 1420345
    iget v1, p2, Landroid/graphics/Rect;->right:I

    iget v2, p2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v2

    .line 1420346
    if-ge v1, v0, :cond_0

    .line 1420347
    iget v1, p2, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v1

    iput v0, p2, Landroid/graphics/Rect;->right:I

    .line 1420348
    :cond_0
    invoke-virtual {p1, p2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1420349
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1420350
    invoke-virtual {p0}, Landroid/graphics/Canvas;->restore()V

    .line 1420351
    return-void
.end method

.method private b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1420340
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lit/sephiroth/android/library/widget/HListView;->a(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 1420341
    return-void
.end method

.method private b(Landroid/view/View;II)V
    .locals 3

    .prologue
    .line 1420331
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    .line 1420332
    invoke-direct {p0, p1}, Lit/sephiroth/android/library/widget/HListView;->d(Landroid/view/View;)V

    .line 1420333
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    if-eq v1, v0, :cond_0

    .line 1420334
    invoke-direct {p0, p1}, Lit/sephiroth/android/library/widget/HListView;->e(Landroid/view/View;)V

    .line 1420335
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr v1, v0

    .line 1420336
    add-int/lit8 v0, p2, 0x1

    :goto_0
    if-ge v0, p3, :cond_0

    .line 1420337
    invoke-virtual {p0, v0}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 1420338
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1420339
    :cond_0
    return-void
.end method

.method private b(IIIII)[I
    .locals 10

    .prologue
    .line 1420306
    iget-object v2, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    .line 1420307
    if-nez v2, :cond_0

    .line 1420308
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v1, 0x0

    iget-object v2, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    add-int/2addr v2, v3

    aput v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v2, v3

    aput v2, v0, v1

    .line 1420309
    :goto_0
    return-object v0

    .line 1420310
    :cond_0
    iget-object v0, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int v4, v0, v1

    .line 1420311
    iget-object v0, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    add-int v5, v0, v1

    .line 1420312
    iget v0, p0, Lit/sephiroth/android/library/widget/HListView;->av:I

    if-lez v0, :cond_3

    iget-object v0, p0, Lit/sephiroth/android/library/widget/HListView;->au:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_3

    iget v0, p0, Lit/sephiroth/android/library/widget/HListView;->av:I

    move v3, v0

    .line 1420313
    :goto_1
    const/4 v1, 0x0

    .line 1420314
    const/4 v0, 0x0

    .line 1420315
    const/4 v6, -0x1

    if-ne p3, v6, :cond_1

    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    add-int/lit8 p3, v2, -0x1

    .line 1420316
    :cond_1
    iget-object v6, p0, LX/8vj;->p:LX/8vf;

    .line 1420317
    const/4 v2, 0x1

    move v7, v2

    .line 1420318
    iget-object v8, p0, LX/8vj;->P:[Z

    move v2, v1

    move v1, v0

    .line 1420319
    :goto_2
    if-gt p2, p3, :cond_4

    .line 1420320
    invoke-virtual {p0, p2, v8}, LX/8vj;->a(I[Z)Landroid/view/View;

    move-result-object v9

    .line 1420321
    invoke-direct {p0, v9, p2, p1}, Lit/sephiroth/android/library/widget/HListView;->a(Landroid/view/View;II)V

    .line 1420322
    if-eqz v7, :cond_2

    invoke-virtual {v9}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/8vc;

    iget v0, v0, LX/8vc;->a:I

    invoke-static {v0}, LX/8vf;->b(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1420323
    const/4 v0, -0x1

    invoke-virtual {v6, v9, v0}, LX/8vf;->a(Landroid/view/View;I)V

    .line 1420324
    :cond_2
    invoke-virtual {v9}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    add-int/2addr v0, v3

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 1420325
    invoke-virtual {v9}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1420326
    add-int/lit8 p2, p2, 0x1

    move v1, v0

    goto :goto_2

    .line 1420327
    :cond_3
    const/4 v0, 0x0

    move v3, v0

    goto :goto_1

    .line 1420328
    :cond_4
    add-int/2addr v2, v4

    .line 1420329
    add-int/2addr v1, v5

    .line 1420330
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v3, 0x0

    invoke-static {v2, p4}, Ljava/lang/Math;->min(II)I

    move-result v2

    aput v2, v0, v3

    const/4 v2, 0x1

    invoke-static {v1, p5}, Ljava/lang/Math;->min(II)I

    move-result v1

    aput v1, v0, v2

    goto :goto_0
.end method

.method private static c(III)I
    .locals 0

    .prologue
    .line 1421021
    if-lez p2, :cond_0

    .line 1421022
    add-int/2addr p0, p1

    .line 1421023
    :cond_0
    return p0
.end method

.method private c(II)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v3, 0x1

    .line 1420762
    const/4 v6, 0x0

    .line 1420763
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getRight()I

    move-result v0

    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getLeft()I

    move-result v1

    sub-int v7, v0, v1

    move v2, p2

    move v1, p1

    .line 1420764
    :goto_0
    if-ge v2, v7, :cond_1

    iget v0, p0, LX/8vi;->ao:I

    if-ge v1, v0, :cond_1

    .line 1420765
    iget v0, p0, LX/8vi;->am:I

    if-ne v1, v0, :cond_0

    move v5, v3

    .line 1420766
    :goto_1
    iget-object v0, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v4, v0, Landroid/graphics/Rect;->top:I

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lit/sephiroth/android/library/widget/HListView;->a(IIZIZ)Landroid/view/View;

    move-result-object v0

    .line 1420767
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v2

    iget v4, p0, Lit/sephiroth/android/library/widget/HListView;->av:I

    add-int/2addr v2, v4

    .line 1420768
    if-eqz v5, :cond_2

    .line 1420769
    :goto_2
    add-int/lit8 v1, v1, 0x1

    move-object v6, v0

    .line 1420770
    goto :goto_0

    .line 1420771
    :cond_0
    const/4 v5, 0x0

    goto :goto_1

    .line 1420772
    :cond_1
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getChildCount()I

    .line 1420773
    return-object v6

    :cond_2
    move-object v0, v6

    goto :goto_2
.end method

.method private c(Landroid/view/View;I)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 1421024
    add-int/lit8 v2, p2, 0x1

    .line 1421025
    iget-object v0, p0, LX/8vj;->P:[Z

    invoke-virtual {p0, v2, v0}, LX/8vj;->a(I[Z)Landroid/view/View;

    move-result-object v1

    .line 1421026
    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v0

    iget v3, p0, Lit/sephiroth/android/library/widget/HListView;->av:I

    add-int/2addr v3, v0

    .line 1421027
    const/4 v4, 0x1

    iget-object v0, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v5, v0, Landroid/graphics/Rect;->top:I

    iget-object v0, p0, LX/8vj;->P:[Z

    aget-boolean v7, v0, v6

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lit/sephiroth/android/library/widget/HListView;->a(Landroid/view/View;IIZIZZ)V

    .line 1421028
    return-object v1
.end method

.method private c(Landroid/view/View;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1421029
    iget-object v4, p0, Lit/sephiroth/android/library/widget/HListView;->az:Ljava/util/ArrayList;

    .line 1421030
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v2

    .line 1421031
    :goto_0
    if-ge v3, v5, :cond_1

    .line 1421032
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8vp;

    iget-object v0, v0, LX/8vp;->a:Landroid/view/View;

    if-ne p1, v0, :cond_0

    move v0, v1

    .line 1421033
    :goto_1
    return v0

    .line 1421034
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 1421035
    :cond_1
    iget-object v4, p0, Lit/sephiroth/android/library/widget/HListView;->aA:Ljava/util/ArrayList;

    .line 1421036
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v2

    .line 1421037
    :goto_2
    if-ge v3, v5, :cond_3

    .line 1421038
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8vp;

    iget-object v0, v0, LX/8vp;->a:Landroid/view/View;

    if-ne p1, v0, :cond_2

    move v0, v1

    .line 1421039
    goto :goto_1

    .line 1421040
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_3
    move v0, v2

    .line 1421041
    goto :goto_1
.end method

.method private d(II)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 1420105
    const/4 v6, 0x0

    move v2, p2

    move v1, p1

    .line 1420106
    :goto_0
    if-lez v2, :cond_1

    if-ltz v1, :cond_1

    .line 1420107
    iget v0, p0, LX/8vi;->am:I

    if-ne v1, v0, :cond_0

    const/4 v5, 0x1

    .line 1420108
    :goto_1
    iget-object v0, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v4, v0, Landroid/graphics/Rect;->top:I

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lit/sephiroth/android/library/widget/HListView;->a(IIZIZ)Landroid/view/View;

    move-result-object v0

    .line 1420109
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v2

    iget v4, p0, Lit/sephiroth/android/library/widget/HListView;->av:I

    sub-int/2addr v2, v4

    .line 1420110
    if-eqz v5, :cond_2

    .line 1420111
    :goto_2
    add-int/lit8 v1, v1, -0x1

    move-object v6, v0

    .line 1420112
    goto :goto_0

    :cond_0
    move v5, v3

    .line 1420113
    goto :goto_1

    .line 1420114
    :cond_1
    add-int/lit8 v0, v1, 0x1

    iput v0, p0, Lit/sephiroth/android/library/widget/HListView;->V:I

    .line 1420115
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getChildCount()I

    .line 1420116
    return-object v6

    :cond_2
    move-object v0, v6

    goto :goto_2
.end method

.method private d(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1421044
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1421045
    if-nez v0, :cond_0

    .line 1421046
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x2

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 1421047
    :cond_0
    iget v1, p0, LX/8vj;->v:I

    iget-object v2, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v2, v3

    iget v3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v1, v2, v3}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v1

    .line 1421048
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1421049
    if-lez v0, :cond_1

    .line 1421050
    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 1421051
    :goto_0
    invoke-virtual {p1, v0, v1}, Landroid/view/View;->measure(II)V

    .line 1421052
    return-void

    .line 1421053
    :cond_1
    invoke-static {v4, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_0
.end method

.method private e(I)Landroid/view/View;
    .locals 2

    .prologue
    .line 1421054
    iget v0, p0, LX/8vi;->V:I

    iget v1, p0, LX/8vi;->am:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lit/sephiroth/android/library/widget/HListView;->V:I

    .line 1421055
    iget v0, p0, LX/8vi;->V:I

    iget v1, p0, LX/8vi;->ao:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lit/sephiroth/android/library/widget/HListView;->V:I

    .line 1421056
    iget v0, p0, LX/8vi;->V:I

    if-gez v0, :cond_0

    .line 1421057
    const/4 v0, 0x0

    iput v0, p0, Lit/sephiroth/android/library/widget/HListView;->V:I

    .line 1421058
    :cond_0
    iget v0, p0, LX/8vi;->V:I

    invoke-direct {p0, v0, p1}, Lit/sephiroth/android/library/widget/HListView;->c(II)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private e(II)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v3, 0x1

    .line 1421059
    sub-int v6, p2, p1

    .line 1421060
    invoke-virtual {p0}, LX/8vj;->i()I

    move-result v1

    .line 1421061
    iget-object v0, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v4, v0, Landroid/graphics/Rect;->top:I

    move-object v0, p0

    move v2, p1

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lit/sephiroth/android/library/widget/HListView;->a(IIZIZ)Landroid/view/View;

    move-result-object v0

    .line 1421062
    iput v1, p0, Lit/sephiroth/android/library/widget/HListView;->V:I

    .line 1421063
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    .line 1421064
    if-gt v2, v6, :cond_0

    .line 1421065
    sub-int v2, v6, v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v2}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 1421066
    :cond_0
    invoke-direct {p0, v0, v1}, Lit/sephiroth/android/library/widget/HListView;->a(Landroid/view/View;I)V

    .line 1421067
    iget-boolean v1, p0, LX/8vj;->K:Z

    if-nez v1, :cond_1

    .line 1421068
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getChildCount()I

    move-result v1

    invoke-direct {p0, v1}, Lit/sephiroth/android/library/widget/HListView;->f(I)V

    .line 1421069
    :goto_0
    return-object v0

    .line 1421070
    :cond_1
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getChildCount()I

    move-result v1

    invoke-direct {p0, v1}, Lit/sephiroth/android/library/widget/HListView;->g(I)V

    goto :goto_0
.end method

.method private e(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 1421071
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    .line 1421072
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    .line 1421073
    iget-object v2, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    .line 1421074
    add-int/2addr v1, v2

    .line 1421075
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v3

    .line 1421076
    add-int/2addr v0, v3

    .line 1421077
    invoke-virtual {p1, v3, v2, v0, v1}, Landroid/view/View;->layout(IIII)V

    .line 1421078
    return-void
.end method

.method private f(Landroid/view/View;)I
    .locals 3

    .prologue
    .line 1421079
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getChildCount()I

    move-result v1

    .line 1421080
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 1421081
    invoke-virtual {p0, v0}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1421082
    invoke-direct {p0, p1, v2}, Lit/sephiroth/android/library/widget/HListView;->a(Landroid/view/View;Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1421083
    iget v1, p0, LX/8vi;->V:I

    add-int/2addr v0, v1

    return v0

    .line 1421084
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1421085
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "newFocus is not a child of any of the children of the list!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private f(II)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 1420998
    iget v0, p0, LX/8vi;->am:I

    if-ne p1, v0, :cond_1

    move v5, v3

    .line 1420999
    :goto_0
    iget-object v0, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v4, v0, Landroid/graphics/Rect;->top:I

    move-object v0, p0

    move v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Lit/sephiroth/android/library/widget/HListView;->a(IIZIZ)Landroid/view/View;

    move-result-object v0

    .line 1421000
    iput p1, p0, Lit/sephiroth/android/library/widget/HListView;->V:I

    .line 1421001
    iget v3, p0, Lit/sephiroth/android/library/widget/HListView;->av:I

    .line 1421002
    iget-boolean v1, p0, LX/8vj;->K:Z

    if-nez v1, :cond_2

    .line 1421003
    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v2

    sub-int/2addr v2, v3

    invoke-direct {p0, v1, v2}, Lit/sephiroth/android/library/widget/HListView;->d(II)Landroid/view/View;

    move-result-object v1

    .line 1421004
    invoke-direct {p0}, Lit/sephiroth/android/library/widget/HListView;->p()V

    .line 1421005
    add-int/lit8 v2, p1, 0x1

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {p0, v2, v3}, Lit/sephiroth/android/library/widget/HListView;->c(II)Landroid/view/View;

    move-result-object v2

    .line 1421006
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getChildCount()I

    move-result v3

    .line 1421007
    if-lez v3, :cond_0

    .line 1421008
    invoke-direct {p0, v3}, Lit/sephiroth/android/library/widget/HListView;->f(I)V

    .line 1421009
    :cond_0
    :goto_1
    if-eqz v5, :cond_3

    .line 1421010
    :goto_2
    return-object v0

    .line 1421011
    :cond_1
    const/4 v5, 0x0

    goto :goto_0

    .line 1421012
    :cond_2
    add-int/lit8 v1, p1, 0x1

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v2

    add-int/2addr v2, v3

    invoke-direct {p0, v1, v2}, Lit/sephiroth/android/library/widget/HListView;->c(II)Landroid/view/View;

    move-result-object v2

    .line 1421013
    invoke-direct {p0}, Lit/sephiroth/android/library/widget/HListView;->p()V

    .line 1421014
    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v4

    sub-int v3, v4, v3

    invoke-direct {p0, v1, v3}, Lit/sephiroth/android/library/widget/HListView;->d(II)Landroid/view/View;

    move-result-object v1

    .line 1421015
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getChildCount()I

    move-result v3

    .line 1421016
    if-lez v3, :cond_0

    .line 1421017
    invoke-direct {p0, v3}, Lit/sephiroth/android/library/widget/HListView;->g(I)V

    goto :goto_1

    .line 1421018
    :cond_3
    if-eqz v1, :cond_4

    move-object v0, v1

    .line 1421019
    goto :goto_2

    :cond_4
    move-object v0, v2

    .line 1421020
    goto :goto_2
.end method

.method private f(I)V
    .locals 4

    .prologue
    .line 1421086
    iget v0, p0, LX/8vi;->V:I

    add-int/2addr v0, p1

    add-int/lit8 v0, v0, -0x1

    .line 1421087
    iget v1, p0, LX/8vi;->ao:I

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_2

    if-lez p1, :cond_2

    .line 1421088
    add-int/lit8 v0, p1, -0x1

    invoke-virtual {p0, v0}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1421089
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    .line 1421090
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getRight()I

    move-result v1

    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getLeft()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v1, v2

    .line 1421091
    sub-int v0, v1, v0

    .line 1421092
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1421093
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    .line 1421094
    if-lez v0, :cond_2

    iget v3, p0, LX/8vi;->V:I

    if-gtz v3, :cond_0

    iget-object v3, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    if-ge v2, v3, :cond_2

    .line 1421095
    :cond_0
    iget v3, p0, LX/8vi;->V:I

    if-nez v3, :cond_1

    .line 1421096
    iget-object v3, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    sub-int v2, v3, v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1421097
    :cond_1
    invoke-virtual {p0, v0}, LX/8vj;->b(I)V

    .line 1421098
    iget v0, p0, LX/8vi;->V:I

    if-lez v0, :cond_2

    .line 1421099
    iget v0, p0, LX/8vi;->V:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    iget v2, p0, Lit/sephiroth/android/library/widget/HListView;->av:I

    sub-int/2addr v1, v2

    invoke-direct {p0, v0, v1}, Lit/sephiroth/android/library/widget/HListView;->d(II)Landroid/view/View;

    .line 1421100
    invoke-direct {p0}, Lit/sephiroth/android/library/widget/HListView;->p()V

    .line 1421101
    :cond_2
    return-void
.end method

.method private g(II)I
    .locals 7

    .prologue
    const/4 v6, -0x1

    const/4 v2, 0x0

    .line 1420966
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getWidth()I

    move-result v0

    iget-object v1, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    sub-int v1, v0, v1

    .line 1420967
    iget-object v0, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 1420968
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getChildCount()I

    move-result v4

    .line 1420969
    const/16 v0, 0x82

    if-ne p1, v0, :cond_5

    .line 1420970
    add-int/lit8 v0, v4, -0x1

    .line 1420971
    if-eq p2, v6, :cond_0

    .line 1420972
    iget v0, p0, LX/8vi;->V:I

    sub-int v0, p2, v0

    .line 1420973
    :cond_0
    iget v3, p0, LX/8vi;->V:I

    add-int/2addr v3, v0

    .line 1420974
    invoke-virtual {p0, v0}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 1420975
    iget v0, p0, LX/8vi;->ao:I

    add-int/lit8 v0, v0, -0x1

    if-ge v3, v0, :cond_a

    .line 1420976
    invoke-direct {p0}, Lit/sephiroth/android/library/widget/HListView;->getArrowScrollPreviewLength()I

    move-result v0

    sub-int v0, v1, v0

    .line 1420977
    :goto_0
    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    move-result v3

    if-gt v3, v0, :cond_2

    .line 1420978
    :cond_1
    :goto_1
    return v2

    .line 1420979
    :cond_2
    if-eq p2, v6, :cond_3

    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v3

    sub-int v3, v0, v3

    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getMaxScrollAmount()I

    move-result v6

    if-ge v3, v6, :cond_1

    .line 1420980
    :cond_3
    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    move-result v2

    sub-int v0, v2, v0

    .line 1420981
    iget v2, p0, LX/8vi;->V:I

    add-int/2addr v2, v4

    iget v3, p0, LX/8vi;->ao:I

    if-ne v2, v3, :cond_4

    .line 1420982
    add-int/lit8 v2, v4, -0x1

    invoke-virtual {p0, v2}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v2

    sub-int v1, v2, v1

    .line 1420983
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1420984
    :cond_4
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getMaxScrollAmount()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    goto :goto_1

    .line 1420985
    :cond_5
    if-eq p2, v6, :cond_9

    .line 1420986
    iget v0, p0, LX/8vi;->V:I

    sub-int v0, p2, v0

    .line 1420987
    :goto_2
    iget v1, p0, LX/8vi;->V:I

    add-int/2addr v1, v0

    .line 1420988
    invoke-virtual {p0, v0}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 1420989
    if-lez v1, :cond_8

    .line 1420990
    invoke-direct {p0}, Lit/sephiroth/android/library/widget/HListView;->getArrowScrollPreviewLength()I

    move-result v0

    add-int/2addr v0, v3

    .line 1420991
    :goto_3
    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v1

    if-ge v1, v0, :cond_1

    .line 1420992
    if-eq p2, v6, :cond_6

    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v1

    sub-int/2addr v1, v0

    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getMaxScrollAmount()I

    move-result v5

    if-ge v1, v5, :cond_1

    .line 1420993
    :cond_6
    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v1

    sub-int/2addr v0, v1

    .line 1420994
    iget v1, p0, LX/8vi;->V:I

    if-nez v1, :cond_7

    .line 1420995
    invoke-virtual {p0, v2}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    sub-int v1, v3, v1

    .line 1420996
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1420997
    :cond_7
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getMaxScrollAmount()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    goto :goto_1

    :cond_8
    move v0, v3

    goto :goto_3

    :cond_9
    move v0, v2

    goto :goto_2

    :cond_a
    move v0, v1

    goto/16 :goto_0
.end method

.method private g(Landroid/view/View;)I
    .locals 4

    .prologue
    .line 1420957
    const/4 v0, 0x0

    .line 1420958
    iget-object v1, p0, Lit/sephiroth/android/library/widget/HListView;->aH:Landroid/graphics/Rect;

    invoke-virtual {p1, v1}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 1420959
    iget-object v1, p0, Lit/sephiroth/android/library/widget/HListView;->aH:Landroid/graphics/Rect;

    invoke-virtual {p0, p1, v1}, Lit/sephiroth/android/library/widget/HListView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 1420960
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getRight()I

    move-result v1

    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getLeft()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v1, v2

    .line 1420961
    iget-object v2, p0, Lit/sephiroth/android/library/widget/HListView;->aH:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iget-object v3, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    if-ge v2, v3, :cond_1

    .line 1420962
    iget-object v0, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lit/sephiroth/android/library/widget/HListView;->aH:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v1

    .line 1420963
    :cond_0
    :goto_0
    return v0

    .line 1420964
    :cond_1
    iget-object v2, p0, Lit/sephiroth/android/library/widget/HListView;->aH:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    if-le v2, v1, :cond_0

    .line 1420965
    iget-object v0, p0, Lit/sephiroth/android/library/widget/HListView;->aH:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method private g(I)V
    .locals 6

    .prologue
    .line 1420937
    iget v0, p0, LX/8vi;->V:I

    if-nez v0, :cond_2

    if-lez p1, :cond_2

    .line 1420938
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1420939
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    .line 1420940
    iget-object v1, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    .line 1420941
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getRight()I

    move-result v2

    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getLeft()I

    move-result v3

    sub-int/2addr v2, v3

    iget-object v3, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    sub-int/2addr v2, v3

    .line 1420942
    sub-int/2addr v0, v1

    .line 1420943
    add-int/lit8 v1, p1, -0x1

    invoke-virtual {p0, v1}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1420944
    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v3

    .line 1420945
    iget v4, p0, LX/8vi;->V:I

    add-int/2addr v4, p1

    add-int/lit8 v4, v4, -0x1

    .line 1420946
    if-lez v0, :cond_2

    .line 1420947
    iget v5, p0, LX/8vi;->ao:I

    add-int/lit8 v5, v5, -0x1

    if-lt v4, v5, :cond_0

    if-le v3, v2, :cond_3

    .line 1420948
    :cond_0
    iget v5, p0, LX/8vi;->ao:I

    add-int/lit8 v5, v5, -0x1

    if-ne v4, v5, :cond_1

    .line 1420949
    sub-int v2, v3, v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1420950
    :cond_1
    neg-int v0, v0

    invoke-virtual {p0, v0}, LX/8vj;->b(I)V

    .line 1420951
    iget v0, p0, LX/8vi;->ao:I

    add-int/lit8 v0, v0, -0x1

    if-ge v4, v0, :cond_2

    .line 1420952
    add-int/lit8 v0, v4, 0x1

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v1

    iget v2, p0, Lit/sephiroth/android/library/widget/HListView;->av:I

    add-int/2addr v1, v2

    invoke-direct {p0, v0, v1}, Lit/sephiroth/android/library/widget/HListView;->c(II)Landroid/view/View;

    .line 1420953
    invoke-direct {p0}, Lit/sephiroth/android/library/widget/HListView;->p()V

    .line 1420954
    :cond_2
    :goto_0
    return-void

    .line 1420955
    :cond_3
    iget v0, p0, LX/8vi;->ao:I

    add-int/lit8 v0, v0, -0x1

    if-ne v4, v0, :cond_2

    .line 1420956
    invoke-direct {p0}, Lit/sephiroth/android/library/widget/HListView;->p()V

    goto :goto_0
.end method

.method private getArrowScrollPreviewLength()I
    .locals 2

    .prologue
    .line 1420936
    const/4 v0, 0x2

    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getHorizontalFadingEdgeLength()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method private h(I)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1420916
    const/4 v2, -0x1

    .line 1420917
    const/16 v3, 0x21

    if-ne p1, v3, :cond_3

    iget v2, p0, LX/8vi;->am:I

    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getChildCount()I

    move-result v3

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, -0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    move v3, v2

    move v2, v1

    .line 1420918
    :goto_0
    if-ltz v3, :cond_4

    .line 1420919
    invoke-virtual {p0, v3, v2}, Lit/sephiroth/android/library/widget/HListView;->a(IZ)I

    move-result v3

    .line 1420920
    if-ltz v3, :cond_4

    .line 1420921
    const/4 v1, 0x4

    iput v1, p0, Lit/sephiroth/android/library/widget/HListView;->h:I

    .line 1420922
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getHorizontalFadingEdgeLength()I

    move-result v4

    add-int/2addr v1, v4

    iput v1, p0, Lit/sephiroth/android/library/widget/HListView;->W:I

    .line 1420923
    if-eqz v2, :cond_0

    iget v1, p0, LX/8vi;->ao:I

    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getChildCount()I

    move-result v4

    sub-int/2addr v1, v4

    if-le v3, v1, :cond_0

    .line 1420924
    const/4 v1, 0x3

    iput v1, p0, Lit/sephiroth/android/library/widget/HListView;->h:I

    .line 1420925
    :cond_0
    if-nez v2, :cond_1

    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getChildCount()I

    move-result v1

    if-ge v3, v1, :cond_1

    .line 1420926
    iput v0, p0, Lit/sephiroth/android/library/widget/HListView;->h:I

    .line 1420927
    :cond_1
    invoke-virtual {p0, v3}, LX/8vj;->setSelectionInt(I)V

    .line 1420928
    invoke-virtual {p0}, LX/8vj;->b()V

    .line 1420929
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->awakenScrollBars()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1420930
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->invalidate()V

    .line 1420931
    :cond_2
    :goto_1
    return v0

    .line 1420932
    :cond_3
    const/16 v3, 0x82

    if-ne p1, v3, :cond_5

    .line 1420933
    iget v2, p0, LX/8vi;->ao:I

    add-int/lit8 v2, v2, -0x1

    iget v3, p0, LX/8vi;->am:I

    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getChildCount()I

    move-result v4

    add-int/2addr v3, v4

    add-int/lit8 v3, v3, -0x1

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    move v3, v2

    move v2, v0

    .line 1420934
    goto :goto_0

    :cond_4
    move v0, v1

    .line 1420935
    goto :goto_1

    :cond_5
    move v3, v2

    move v2, v1

    goto :goto_0
.end method

.method private i(I)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1420898
    const/16 v2, 0x21

    if-ne p1, v2, :cond_2

    .line 1420899
    iget v2, p0, LX/8vi;->am:I

    if-eqz v2, :cond_3

    .line 1420900
    invoke-virtual {p0, v1, v0}, Lit/sephiroth/android/library/widget/HListView;->a(IZ)I

    move-result v1

    .line 1420901
    if-ltz v1, :cond_0

    .line 1420902
    iput v0, p0, Lit/sephiroth/android/library/widget/HListView;->h:I

    .line 1420903
    invoke-virtual {p0, v1}, LX/8vj;->setSelectionInt(I)V

    .line 1420904
    invoke-virtual {p0}, LX/8vj;->b()V

    .line 1420905
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->awakenScrollBars()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1420906
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->awakenScrollBars()Z

    .line 1420907
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->invalidate()V

    .line 1420908
    :cond_1
    return v0

    .line 1420909
    :cond_2
    const/16 v2, 0x82

    if-ne p1, v2, :cond_3

    .line 1420910
    iget v2, p0, LX/8vi;->am:I

    iget v3, p0, LX/8vi;->ao:I

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_3

    .line 1420911
    iget v1, p0, LX/8vi;->ao:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1, v0}, Lit/sephiroth/android/library/widget/HListView;->a(IZ)I

    move-result v1

    .line 1420912
    if-ltz v1, :cond_0

    .line 1420913
    const/4 v2, 0x3

    iput v2, p0, Lit/sephiroth/android/library/widget/HListView;->h:I

    .line 1420914
    invoke-virtual {p0, v1}, LX/8vj;->setSelectionInt(I)V

    .line 1420915
    invoke-virtual {p0}, LX/8vj;->b()V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method private j(I)Z
    .locals 3

    .prologue
    .line 1420879
    const/16 v0, 0x11

    if-eq p1, v0, :cond_0

    const/16 v0, 0x42

    if-eq p1, v0, :cond_0

    .line 1420880
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "direction must be one of {View.FOCUS_LEFT, View.FOCUS_RIGHT}"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1420881
    :cond_0
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getChildCount()I

    move-result v0

    .line 1420882
    iget-boolean v1, p0, Lit/sephiroth/android/library/widget/HListView;->aG:Z

    if-eqz v1, :cond_2

    if-lez v0, :cond_2

    iget v0, p0, LX/8vi;->am:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    .line 1420883
    invoke-virtual {p0}, LX/8vi;->getSelectedView()Landroid/view/View;

    move-result-object v0

    .line 1420884
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->hasFocus()Z

    move-result v1

    if-eqz v1, :cond_2

    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_2

    .line 1420885
    invoke-virtual {v0}, Landroid/view/View;->findFocus()Landroid/view/View;

    move-result-object v1

    .line 1420886
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v2

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v2, v0, v1, p1}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 1420887
    if-eqz v0, :cond_1

    .line 1420888
    iget-object v2, p0, Lit/sephiroth/android/library/widget/HListView;->aH:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/view/View;->getFocusedRect(Landroid/graphics/Rect;)V

    .line 1420889
    iget-object v2, p0, Lit/sephiroth/android/library/widget/HListView;->aH:Landroid/graphics/Rect;

    invoke-virtual {p0, v1, v2}, Lit/sephiroth/android/library/widget/HListView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 1420890
    iget-object v2, p0, Lit/sephiroth/android/library/widget/HListView;->aH:Landroid/graphics/Rect;

    invoke-virtual {p0, v0, v2}, Lit/sephiroth/android/library/widget/HListView;->offsetRectIntoDescendantCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 1420891
    iget-object v2, p0, Lit/sephiroth/android/library/widget/HListView;->aH:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, v2}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1420892
    const/4 v0, 0x1

    .line 1420893
    :goto_0
    return v0

    .line 1420894
    :cond_1
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v2

    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getRootView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v2, v0, v1, p1}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 1420895
    if-eqz v0, :cond_2

    .line 1420896
    invoke-direct {p0, v0, p0}, Lit/sephiroth/android/library/widget/HListView;->a(Landroid/view/View;Landroid/view/View;)Z

    move-result v0

    goto :goto_0

    .line 1420897
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private k(I)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1420874
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lit/sephiroth/android/library/widget/HListView;->af:Z

    .line 1420875
    invoke-direct {p0, p1}, Lit/sephiroth/android/library/widget/HListView;->l(I)Z

    move-result v0

    .line 1420876
    if-eqz v0, :cond_0

    .line 1420877
    invoke-static {p1}, Landroid/view/SoundEffectConstants;->getContantForFocusDirection(I)I

    move-result v1

    invoke-virtual {p0, v1}, Lit/sephiroth/android/library/widget/HListView;->playSoundEffect(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1420878
    :cond_0
    iput-boolean v2, p0, Lit/sephiroth/android/library/widget/HListView;->af:Z

    return v0

    :catchall_0
    move-exception v0

    iput-boolean v2, p0, Lit/sephiroth/android/library/widget/HListView;->af:Z

    throw v0
.end method

.method private l(I)Z
    .locals 11

    .prologue
    const/4 v4, 0x0

    const/4 v9, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1420832
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getChildCount()I

    move-result v0

    if-gtz v0, :cond_1

    .line 1420833
    :cond_0
    :goto_0
    return v2

    .line 1420834
    :cond_1
    invoke-virtual {p0}, LX/8vi;->getSelectedView()Landroid/view/View;

    move-result-object v6

    .line 1420835
    iget v5, p0, LX/8vi;->am:I

    .line 1420836
    invoke-direct {p0, p1}, Lit/sephiroth/android/library/widget/HListView;->m(I)I

    move-result v7

    .line 1420837
    invoke-direct {p0, p1, v7}, Lit/sephiroth/android/library/widget/HListView;->g(II)I

    move-result v3

    .line 1420838
    iget-boolean v0, p0, Lit/sephiroth/android/library/widget/HListView;->aG:Z

    if-eqz v0, :cond_9

    invoke-direct {p0, p1}, Lit/sephiroth/android/library/widget/HListView;->n(I)LX/8vo;

    move-result-object v0

    move-object v8, v0

    .line 1420839
    :goto_1
    if-eqz v8, :cond_2

    .line 1420840
    iget v0, v8, LX/8vo;->a:I

    move v7, v0

    .line 1420841
    iget v0, v8, LX/8vo;->b:I

    move v3, v0

    .line 1420842
    :cond_2
    if-eqz v8, :cond_a

    move v0, v1

    .line 1420843
    :goto_2
    if-eq v7, v9, :cond_e

    .line 1420844
    if-eqz v8, :cond_b

    move v0, v1

    :goto_3
    invoke-direct {p0, v6, p1, v7, v0}, Lit/sephiroth/android/library/widget/HListView;->a(Landroid/view/View;IIZ)V

    .line 1420845
    invoke-virtual {p0, v7}, LX/8vi;->setSelectedPositionInt(I)V

    .line 1420846
    invoke-virtual {p0, v7}, LX/8vi;->setNextSelectedPositionInt(I)V

    .line 1420847
    invoke-virtual {p0}, LX/8vi;->getSelectedView()Landroid/view/View;

    move-result-object v5

    .line 1420848
    iget-boolean v0, p0, Lit/sephiroth/android/library/widget/HListView;->aG:Z

    if-eqz v0, :cond_3

    if-nez v8, :cond_3

    .line 1420849
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getFocusedChild()Landroid/view/View;

    move-result-object v0

    .line 1420850
    if-eqz v0, :cond_3

    .line 1420851
    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    .line 1420852
    :cond_3
    invoke-virtual {p0}, LX/8vi;->m()V

    move v0, v1

    move v6, v7

    .line 1420853
    :goto_4
    if-lez v3, :cond_4

    .line 1420854
    const/16 v0, 0x21

    if-ne p1, v0, :cond_c

    move v0, v3

    :goto_5
    invoke-direct {p0, v0}, Lit/sephiroth/android/library/widget/HListView;->o(I)V

    move v0, v1

    .line 1420855
    :cond_4
    iget-boolean v3, p0, Lit/sephiroth/android/library/widget/HListView;->aG:Z

    if-eqz v3, :cond_6

    if-nez v8, :cond_6

    if-eqz v5, :cond_6

    invoke-virtual {v5}, Landroid/view/View;->hasFocus()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1420856
    invoke-virtual {v5}, Landroid/view/View;->findFocus()Landroid/view/View;

    move-result-object v3

    .line 1420857
    invoke-direct {p0, v3, p0}, Lit/sephiroth/android/library/widget/HListView;->a(Landroid/view/View;Landroid/view/View;)Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-direct {p0, v3}, Lit/sephiroth/android/library/widget/HListView;->g(Landroid/view/View;)I

    move-result v8

    if-lez v8, :cond_6

    .line 1420858
    :cond_5
    invoke-virtual {v3}, Landroid/view/View;->clearFocus()V

    .line 1420859
    :cond_6
    if-ne v7, v9, :cond_d

    if-eqz v5, :cond_d

    invoke-direct {p0, v5, p0}, Lit/sephiroth/android/library/widget/HListView;->a(Landroid/view/View;Landroid/view/View;)Z

    move-result v3

    if-nez v3, :cond_d

    .line 1420860
    invoke-virtual {p0}, LX/8vj;->h()V

    .line 1420861
    iput v9, p0, Lit/sephiroth/android/library/widget/HListView;->M:I

    move-object v3, v4

    .line 1420862
    :goto_6
    if-eqz v0, :cond_0

    .line 1420863
    if-eqz v3, :cond_7

    .line 1420864
    invoke-virtual {p0, v6, v3}, LX/8vj;->a(ILandroid/view/View;)V

    .line 1420865
    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v0

    iput v0, p0, Lit/sephiroth/android/library/widget/HListView;->J:I

    .line 1420866
    :cond_7
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->awakenScrollBars()Z

    move-result v0

    if-nez v0, :cond_8

    .line 1420867
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->invalidate()V

    .line 1420868
    :cond_8
    invoke-virtual {p0}, LX/8vj;->b()V

    move v2, v1

    .line 1420869
    goto/16 :goto_0

    :cond_9
    move-object v8, v4

    .line 1420870
    goto/16 :goto_1

    :cond_a
    move v0, v2

    .line 1420871
    goto/16 :goto_2

    :cond_b
    move v0, v2

    .line 1420872
    goto :goto_3

    .line 1420873
    :cond_c
    neg-int v0, v3

    goto :goto_5

    :cond_d
    move-object v3, v5

    goto :goto_6

    :cond_e
    move v10, v5

    move-object v5, v6

    move v6, v10

    goto :goto_4
.end method

.method private m(I)I
    .locals 6

    .prologue
    const/4 v3, -0x1

    .line 1420809
    iget v1, p0, LX/8vi;->V:I

    .line 1420810
    const/16 v0, 0x82

    if-ne p1, v0, :cond_5

    .line 1420811
    iget v0, p0, LX/8vi;->am:I

    if-eq v0, v3, :cond_1

    iget v0, p0, LX/8vi;->am:I

    add-int/lit8 v0, v0, 0x1

    .line 1420812
    :goto_0
    iget-object v2, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    if-lt v0, v2, :cond_2

    move v0, v3

    .line 1420813
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v0, v1

    .line 1420814
    goto :goto_0

    .line 1420815
    :cond_2
    if-ge v0, v1, :cond_3

    move v0, v1

    .line 1420816
    :cond_3
    invoke-virtual {p0}, LX/8vi;->getLastVisiblePosition()I

    move-result v2

    .line 1420817
    iget-object v4, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    move-object v4, v4

    .line 1420818
    :goto_2
    if-gt v0, v2, :cond_a

    .line 1420819
    invoke-interface {v4, v0}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v5

    if-eqz v5, :cond_4

    sub-int v5, v0, v1

    invoke-virtual {p0, v5}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v5

    if-eqz v5, :cond_0

    .line 1420820
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1420821
    :cond_5
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getChildCount()I

    move-result v0

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    .line 1420822
    iget v2, p0, LX/8vi;->am:I

    if-eq v2, v3, :cond_7

    iget v2, p0, LX/8vi;->am:I

    add-int/lit8 v2, v2, -0x1

    .line 1420823
    :goto_3
    if-ltz v2, :cond_6

    iget-object v4, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    invoke-interface {v4}, Landroid/widget/ListAdapter;->getCount()I

    move-result v4

    if-lt v2, v4, :cond_8

    :cond_6
    move v0, v3

    .line 1420824
    goto :goto_1

    .line 1420825
    :cond_7
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getChildCount()I

    move-result v2

    add-int/2addr v2, v1

    add-int/lit8 v2, v2, -0x1

    goto :goto_3

    .line 1420826
    :cond_8
    if-le v2, v0, :cond_b

    .line 1420827
    :goto_4
    iget-object v2, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    move-object v2, v2

    .line 1420828
    :goto_5
    if-lt v0, v1, :cond_a

    .line 1420829
    invoke-interface {v2, v0}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v4

    if-eqz v4, :cond_9

    sub-int v4, v0, v1

    invoke-virtual {p0, v4}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-eqz v4, :cond_0

    .line 1420830
    :cond_9
    add-int/lit8 v0, v0, -0x1

    goto :goto_5

    :cond_a
    move v0, v3

    .line 1420831
    goto :goto_1

    :cond_b
    move v0, v2

    goto :goto_4
.end method

.method private n(I)LX/8vo;
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/16 v7, 0x82

    const/4 v0, 0x1

    const/4 v6, -0x1

    const/4 v1, 0x0

    .line 1420774
    invoke-virtual {p0}, LX/8vi;->getSelectedView()Landroid/view/View;

    move-result-object v3

    .line 1420775
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Landroid/view/View;->hasFocus()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1420776
    invoke-virtual {v3}, Landroid/view/View;->findFocus()Landroid/view/View;

    move-result-object v0

    .line 1420777
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v1

    invoke-virtual {v1, p0, v0, p1}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 1420778
    :goto_0
    if-eqz v0, :cond_c

    .line 1420779
    invoke-direct {p0, v0}, Lit/sephiroth/android/library/widget/HListView;->f(Landroid/view/View;)I

    move-result v1

    .line 1420780
    iget v3, p0, LX/8vi;->am:I

    if-eq v3, v6, :cond_a

    iget v3, p0, LX/8vi;->am:I

    if-eq v1, v3, :cond_a

    .line 1420781
    invoke-direct {p0, p1}, Lit/sephiroth/android/library/widget/HListView;->m(I)I

    move-result v3

    .line 1420782
    if-eq v3, v6, :cond_a

    if-ne p1, v7, :cond_0

    if-lt v3, v1, :cond_1

    :cond_0
    const/16 v4, 0x21

    if-ne p1, v4, :cond_a

    if-le v3, v1, :cond_a

    :cond_1
    move-object v0, v2

    .line 1420783
    :goto_1
    return-object v0

    .line 1420784
    :cond_2
    if-ne p1, v7, :cond_6

    .line 1420785
    iget v4, p0, LX/8vi;->V:I

    if-lez v4, :cond_4

    .line 1420786
    :goto_2
    iget-object v4, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    if-eqz v0, :cond_5

    invoke-direct {p0}, Lit/sephiroth/android/library/widget/HListView;->getArrowScrollPreviewLength()I

    move-result v0

    :goto_3
    add-int/2addr v0, v4

    .line 1420787
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v4

    if-le v4, v0, :cond_3

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v0

    .line 1420788
    :cond_3
    iget-object v3, p0, Lit/sephiroth/android/library/widget/HListView;->aH:Landroid/graphics/Rect;

    invoke-virtual {v3, v0, v1, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 1420789
    :goto_4
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v0

    iget-object v1, p0, Lit/sephiroth/android/library/widget/HListView;->aH:Landroid/graphics/Rect;

    invoke-virtual {v0, p0, v1, p1}, Landroid/view/FocusFinder;->findNextFocusFromRect(Landroid/view/ViewGroup;Landroid/graphics/Rect;I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_4
    move v0, v1

    .line 1420790
    goto :goto_2

    :cond_5
    move v0, v1

    .line 1420791
    goto :goto_3

    .line 1420792
    :cond_6
    iget v4, p0, LX/8vi;->V:I

    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getChildCount()I

    move-result v5

    add-int/2addr v4, v5

    add-int/lit8 v4, v4, -0x1

    iget v5, p0, LX/8vi;->ao:I

    if-ge v4, v5, :cond_8

    .line 1420793
    :goto_5
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getWidth()I

    move-result v4

    iget-object v5, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    sub-int/2addr v4, v5

    if-eqz v0, :cond_9

    invoke-direct {p0}, Lit/sephiroth/android/library/widget/HListView;->getArrowScrollPreviewLength()I

    move-result v0

    :goto_6
    sub-int v0, v4, v0

    .line 1420794
    if-eqz v3, :cond_7

    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v4

    if-ge v4, v0, :cond_7

    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v0

    .line 1420795
    :cond_7
    iget-object v3, p0, Lit/sephiroth/android/library/widget/HListView;->aH:Landroid/graphics/Rect;

    invoke-virtual {v3, v0, v1, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_4

    :cond_8
    move v0, v1

    .line 1420796
    goto :goto_5

    :cond_9
    move v0, v1

    .line 1420797
    goto :goto_6

    .line 1420798
    :cond_a
    invoke-direct {p0, p1, v0, v1}, Lit/sephiroth/android/library/widget/HListView;->a(ILandroid/view/View;I)I

    move-result v3

    .line 1420799
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getMaxScrollAmount()I

    move-result v4

    .line 1420800
    if-ge v3, v4, :cond_b

    .line 1420801
    invoke-virtual {v0, p1}, Landroid/view/View;->requestFocus(I)Z

    .line 1420802
    iget-object v0, p0, Lit/sephiroth/android/library/widget/HListView;->aJ:LX/8vo;

    invoke-virtual {v0, v1, v3}, LX/8vo;->a(II)V

    .line 1420803
    iget-object v0, p0, Lit/sephiroth/android/library/widget/HListView;->aJ:LX/8vo;

    goto :goto_1

    .line 1420804
    :cond_b
    invoke-direct {p0, v0}, Lit/sephiroth/android/library/widget/HListView;->g(Landroid/view/View;)I

    move-result v3

    if-ge v3, v4, :cond_c

    .line 1420805
    invoke-virtual {v0, p1}, Landroid/view/View;->requestFocus(I)Z

    .line 1420806
    iget-object v0, p0, Lit/sephiroth/android/library/widget/HListView;->aJ:LX/8vo;

    invoke-virtual {v0, v1, v4}, LX/8vo;->a(II)V

    .line 1420807
    iget-object v0, p0, Lit/sephiroth/android/library/widget/HListView;->aJ:LX/8vo;

    goto/16 :goto_1

    :cond_c
    move-object v0, v2

    .line 1420808
    goto/16 :goto_1
.end method

.method private o(I)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1420532
    invoke-virtual {p0, p1}, LX/8vj;->b(I)V

    .line 1420533
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getWidth()I

    move-result v0

    iget-object v1, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    sub-int v3, v0, v1

    .line 1420534
    iget-object v0, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 1420535
    iget-object v4, p0, LX/8vj;->p:LX/8vf;

    .line 1420536
    if-gez p1, :cond_3

    .line 1420537
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getChildCount()I

    move-result v1

    .line 1420538
    add-int/lit8 v0, v1, -0x1

    invoke-virtual {p0, v0}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1420539
    :goto_0
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v5

    if-ge v5, v3, :cond_0

    .line 1420540
    iget v5, p0, LX/8vi;->V:I

    add-int/2addr v5, v1

    add-int/lit8 v5, v5, -0x1

    .line 1420541
    iget v6, p0, LX/8vi;->ao:I

    add-int/lit8 v6, v6, -0x1

    if-ge v5, v6, :cond_0

    .line 1420542
    invoke-direct {p0, v0, v5}, Lit/sephiroth/android/library/widget/HListView;->c(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 1420543
    add-int/lit8 v1, v1, 0x1

    .line 1420544
    goto :goto_0

    .line 1420545
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v1

    if-ge v1, v3, :cond_1

    .line 1420546
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    sub-int v0, v3, v0

    invoke-virtual {p0, v0}, LX/8vj;->b(I)V

    .line 1420547
    :cond_1
    invoke-virtual {p0, v7}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    .line 1420548
    :goto_1
    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v0

    if-ge v0, v2, :cond_7

    .line 1420549
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/8vc;

    .line 1420550
    iget v0, v0, LX/8vc;->a:I

    invoke-static {v0}, LX/8vf;->b(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1420551
    invoke-virtual {p0, v1}, Lit/sephiroth/android/library/widget/HListView;->detachViewFromParent(Landroid/view/View;)V

    .line 1420552
    iget v0, p0, LX/8vi;->V:I

    invoke-virtual {v4, v1, v0}, LX/8vf;->a(Landroid/view/View;I)V

    .line 1420553
    :goto_2
    invoke-virtual {p0, v7}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1420554
    iget v1, p0, LX/8vi;->V:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lit/sephiroth/android/library/widget/HListView;->V:I

    move-object v1, v0

    .line 1420555
    goto :goto_1

    .line 1420556
    :cond_2
    invoke-virtual {p0, v1}, Lit/sephiroth/android/library/widget/HListView;->removeViewInLayout(Landroid/view/View;)V

    goto :goto_2

    .line 1420557
    :cond_3
    invoke-virtual {p0, v7}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1420558
    :goto_3
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    if-le v1, v2, :cond_4

    iget v1, p0, LX/8vi;->V:I

    if-lez v1, :cond_4

    .line 1420559
    iget v1, p0, LX/8vi;->V:I

    invoke-direct {p0, v0, v1}, Lit/sephiroth/android/library/widget/HListView;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 1420560
    iget v1, p0, LX/8vi;->V:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lit/sephiroth/android/library/widget/HListView;->V:I

    goto :goto_3

    .line 1420561
    :cond_4
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    if-le v1, v2, :cond_5

    .line 1420562
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    sub-int v0, v2, v0

    invoke-virtual {p0, v0}, LX/8vj;->b(I)V

    .line 1420563
    :cond_5
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getChildCount()I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    .line 1420564
    invoke-virtual {p0, v1}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    move v2, v1

    move-object v1, v0

    .line 1420565
    :goto_4
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v0

    if-le v0, v3, :cond_7

    .line 1420566
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/8vc;

    .line 1420567
    iget v0, v0, LX/8vc;->a:I

    invoke-static {v0}, LX/8vf;->b(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1420568
    invoke-virtual {p0, v1}, Lit/sephiroth/android/library/widget/HListView;->detachViewFromParent(Landroid/view/View;)V

    .line 1420569
    iget v0, p0, LX/8vi;->V:I

    add-int/2addr v0, v2

    invoke-virtual {v4, v1, v0}, LX/8vf;->a(Landroid/view/View;I)V

    .line 1420570
    :goto_5
    add-int/lit8 v1, v2, -0x1

    invoke-virtual {p0, v1}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    move v2, v1

    move-object v1, v0

    .line 1420571
    goto :goto_4

    .line 1420572
    :cond_6
    invoke-virtual {p0, v1}, Lit/sephiroth/android/library/widget/HListView;->removeViewInLayout(Landroid/view/View;)V

    goto :goto_5

    .line 1420573
    :cond_7
    return-void
.end method

.method private p()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1420746
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getChildCount()I

    move-result v2

    .line 1420747
    if-lez v2, :cond_2

    .line 1420748
    iget-boolean v1, p0, LX/8vj;->K:Z

    if-nez v1, :cond_3

    .line 1420749
    invoke-virtual {p0, v0}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1420750
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    iget-object v2, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v2

    .line 1420751
    iget v2, p0, LX/8vi;->V:I

    if-eqz v2, :cond_0

    .line 1420752
    iget v2, p0, Lit/sephiroth/android/library/widget/HListView;->av:I

    sub-int/2addr v1, v2

    .line 1420753
    :cond_0
    if-gez v1, :cond_5

    .line 1420754
    :cond_1
    :goto_0
    if-eqz v0, :cond_2

    .line 1420755
    neg-int v0, v0

    invoke-virtual {p0, v0}, LX/8vj;->b(I)V

    .line 1420756
    :cond_2
    return-void

    .line 1420757
    :cond_3
    add-int/lit8 v1, v2, -0x1

    invoke-virtual {p0, v1}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1420758
    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v1

    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getWidth()I

    move-result v3

    iget-object v4, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    sub-int/2addr v3, v4

    sub-int/2addr v1, v3

    .line 1420759
    iget v3, p0, LX/8vi;->V:I

    add-int/2addr v2, v3

    iget v3, p0, LX/8vi;->ao:I

    if-ge v2, v3, :cond_4

    .line 1420760
    iget v2, p0, Lit/sephiroth/android/library/widget/HListView;->av:I

    add-int/2addr v1, v2

    .line 1420761
    :cond_4
    if-gtz v1, :cond_1

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method private q()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1419743
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getScrollX()I

    move-result v1

    iget-object v2, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v2

    .line 1419744
    iget v2, p0, LX/8vi;->V:I

    if-gtz v2, :cond_0

    invoke-virtual {p0, v0}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v2

    if-le v2, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method private r()Z
    .locals 4

    .prologue
    .line 1420070
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getChildCount()I

    move-result v0

    .line 1420071
    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p0, v1}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v1

    .line 1420072
    iget v2, p0, LX/8vi;->V:I

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, -0x1

    .line 1420073
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getScrollX()I

    move-result v2

    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    iget-object v3, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    sub-int/2addr v2, v3

    .line 1420074
    iget v3, p0, LX/8vi;->ao:I

    add-int/lit8 v3, v3, -0x1

    if-lt v0, v3, :cond_0

    if-ge v1, v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(IZ)I
    .locals 4

    .prologue
    const/4 v0, -0x1

    .line 1420054
    iget-object v1, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    .line 1420055
    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->isInTouchMode()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    move p1, v0

    .line 1420056
    :cond_1
    :goto_0
    return p1

    .line 1420057
    :cond_2
    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    .line 1420058
    iget-boolean v3, p0, Lit/sephiroth/android/library/widget/HListView;->aF:Z

    if-nez v3, :cond_6

    .line 1420059
    if-eqz p2, :cond_3

    .line 1420060
    const/4 v3, 0x0

    invoke-static {v3, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 1420061
    :goto_1
    if-ge p1, v2, :cond_4

    invoke-interface {v1, p1}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v3

    if-nez v3, :cond_4

    .line 1420062
    add-int/lit8 p1, p1, 0x1

    goto :goto_1

    .line 1420063
    :cond_3
    add-int/lit8 v3, v2, -0x1

    invoke-static {p1, v3}, Ljava/lang/Math;->min(II)I

    move-result p1

    .line 1420064
    :goto_2
    if-ltz p1, :cond_4

    invoke-interface {v1, p1}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v3

    if-nez v3, :cond_4

    .line 1420065
    add-int/lit8 p1, p1, -0x1

    goto :goto_2

    .line 1420066
    :cond_4
    if-ltz p1, :cond_5

    if-lt p1, v2, :cond_1

    :cond_5
    move p1, v0

    .line 1420067
    goto :goto_0

    .line 1420068
    :cond_6
    if-ltz p1, :cond_7

    if-lt p1, v2, :cond_1

    :cond_7
    move p1, v0

    .line 1420069
    goto :goto_0
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1420044
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getChildCount()I

    move-result v1

    .line 1420045
    if-eqz p1, :cond_1

    .line 1420046
    if-lez v1, :cond_0

    add-int/lit8 v0, v1, -0x1

    invoke-virtual {p0, v0}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    iget v2, p0, Lit/sephiroth/android/library/widget/HListView;->av:I

    add-int/2addr v0, v2

    .line 1420047
    :cond_0
    iget v2, p0, LX/8vi;->V:I

    add-int/2addr v1, v2

    invoke-direct {p0, v1, v0}, Lit/sephiroth/android/library/widget/HListView;->c(II)Landroid/view/View;

    .line 1420048
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getChildCount()I

    move-result v0

    invoke-direct {p0, v0}, Lit/sephiroth/android/library/widget/HListView;->f(I)V

    .line 1420049
    :goto_0
    return-void

    .line 1420050
    :cond_1
    if-lez v1, :cond_2

    invoke-virtual {p0, v0}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    iget v1, p0, Lit/sephiroth/android/library/widget/HListView;->av:I

    sub-int/2addr v0, v1

    .line 1420051
    :goto_1
    iget v1, p0, LX/8vi;->V:I

    add-int/lit8 v1, v1, -0x1

    invoke-direct {p0, v1, v0}, Lit/sephiroth/android/library/widget/HListView;->d(II)Landroid/view/View;

    .line 1420052
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getChildCount()I

    move-result v0

    invoke-direct {p0, v0}, Lit/sephiroth/android/library/widget/HListView;->g(I)V

    goto :goto_0

    .line 1420053
    :cond_2
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getWidth()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    goto :goto_1
.end method

.method public final b(II)V
    .locals 2

    .prologue
    .line 1420028
    iget-object v0, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    if-nez v0, :cond_1

    .line 1420029
    :cond_0
    :goto_0
    return-void

    .line 1420030
    :cond_1
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->isInTouchMode()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1420031
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lit/sephiroth/android/library/widget/HListView;->a(IZ)I

    move-result p1

    .line 1420032
    if-ltz p1, :cond_2

    .line 1420033
    invoke-virtual {p0, p1}, LX/8vi;->setNextSelectedPositionInt(I)V

    .line 1420034
    :cond_2
    :goto_1
    if-ltz p1, :cond_0

    .line 1420035
    const/4 v0, 0x4

    iput v0, p0, Lit/sephiroth/android/library/widget/HListView;->h:I

    .line 1420036
    iget-object v0, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, p2

    iput v0, p0, Lit/sephiroth/android/library/widget/HListView;->W:I

    .line 1420037
    iget-boolean v0, p0, LX/8vi;->ad:Z

    if-eqz v0, :cond_3

    .line 1420038
    iput p1, p0, Lit/sephiroth/android/library/widget/HListView;->aa:I

    .line 1420039
    iget-object v0, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v0

    iput-wide v0, p0, Lit/sephiroth/android/library/widget/HListView;->ab:J

    .line 1420040
    :cond_3
    iget-object v0, p0, LX/8vj;->I:Lit/sephiroth/android/library/widget/AbsHListView$PositionScroller;

    if-eqz v0, :cond_4

    .line 1420041
    iget-object v0, p0, LX/8vj;->I:Lit/sephiroth/android/library/widget/AbsHListView$PositionScroller;

    invoke-virtual {v0}, Lit/sephiroth/android/library/widget/AbsHListView$PositionScroller;->a()V

    .line 1420042
    :cond_4
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->requestLayout()V

    goto :goto_0

    .line 1420043
    :cond_5
    iput p1, p0, Lit/sephiroth/android/library/widget/HListView;->M:I

    goto :goto_1
.end method

.method public final c(I)I
    .locals 3

    .prologue
    .line 1420013
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getChildCount()I

    move-result v1

    .line 1420014
    if-lez v1, :cond_3

    .line 1420015
    iget-boolean v0, p0, LX/8vj;->K:Z

    if-nez v0, :cond_1

    .line 1420016
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_3

    .line 1420017
    invoke-virtual {p0, v0}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1420018
    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v2

    if-gt p1, v2, :cond_0

    .line 1420019
    iget v1, p0, LX/8vi;->V:I

    add-int/2addr v0, v1

    .line 1420020
    :goto_1
    return v0

    .line 1420021
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1420022
    :cond_1
    add-int/lit8 v0, v1, -0x1

    :goto_2
    if-ltz v0, :cond_3

    .line 1420023
    invoke-virtual {p0, v0}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1420024
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    if-lt p1, v1, :cond_2

    .line 1420025
    iget v1, p0, LX/8vi;->V:I

    add-int/2addr v0, v1

    goto :goto_1

    .line 1420026
    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 1420027
    :cond_3
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1420008
    iget-object v0, p0, Lit/sephiroth/android/library/widget/HListView;->az:Ljava/util/ArrayList;

    invoke-static {v0}, Lit/sephiroth/android/library/widget/HListView;->a(Ljava/util/ArrayList;)V

    .line 1420009
    iget-object v0, p0, Lit/sephiroth/android/library/widget/HListView;->aA:Ljava/util/ArrayList;

    invoke-static {v0}, Lit/sephiroth/android/library/widget/HListView;->a(Ljava/util/ArrayList;)V

    .line 1420010
    invoke-super {p0}, LX/8vj;->c()V

    .line 1420011
    const/4 v0, 0x0

    iput v0, p0, Lit/sephiroth/android/library/widget/HListView;->h:I

    .line 1420012
    return-void
.end method

.method public final canAnimate()Z
    .locals 1

    .prologue
    .line 1420007
    invoke-super {p0}, LX/8vj;->canAnimate()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, LX/8vi;->ao:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 17

    .prologue
    .line 1419878
    move-object/from16 v0, p0

    iget-boolean v10, v0, LX/8vi;->at:Z

    .line 1419879
    if-nez v10, :cond_0

    .line 1419880
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lit/sephiroth/android/library/widget/HListView;->at:Z

    .line 1419881
    :try_start_0
    invoke-super/range {p0 .. p0}, LX/8vj;->d()V

    .line 1419882
    invoke-virtual/range {p0 .. p0}, Lit/sephiroth/android/library/widget/HListView;->invalidate()V

    .line 1419883
    move-object/from16 v0, p0

    iget-object v1, v0, LX/8vj;->j:Landroid/widget/ListAdapter;

    if-nez v1, :cond_1

    .line 1419884
    invoke-virtual/range {p0 .. p0}, Lit/sephiroth/android/library/widget/HListView;->c()V

    .line 1419885
    invoke-virtual/range {p0 .. p0}, LX/8vj;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1419886
    if-nez v10, :cond_0

    .line 1419887
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lit/sephiroth/android/library/widget/HListView;->at:Z

    .line 1419888
    :cond_0
    :goto_0
    return-void

    .line 1419889
    :cond_1
    :try_start_1
    move-object/from16 v0, p0

    iget-object v1, v0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v5, v1, Landroid/graphics/Rect;->left:I

    .line 1419890
    invoke-virtual/range {p0 .. p0}, Lit/sephiroth/android/library/widget/HListView;->getRight()I

    move-result v1

    invoke-virtual/range {p0 .. p0}, Lit/sephiroth/android/library/widget/HListView;->getLeft()I

    move-result v2

    sub-int/2addr v1, v2

    move-object/from16 v0, p0

    iget-object v2, v0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    sub-int v6, v1, v2

    .line 1419891
    invoke-virtual/range {p0 .. p0}, Lit/sephiroth/android/library/widget/HListView;->getChildCount()I

    move-result v11

    .line 1419892
    const/4 v4, 0x0

    .line 1419893
    const/4 v2, 0x0

    .line 1419894
    const/4 v1, 0x0

    .line 1419895
    const/4 v3, 0x0

    .line 1419896
    const/4 v7, 0x0

    .line 1419897
    move-object/from16 v0, p0

    iget v8, v0, LX/8vj;->h:I

    packed-switch v8, :pswitch_data_0

    .line 1419898
    move-object/from16 v0, p0

    iget v1, v0, LX/8vi;->am:I

    move-object/from16 v0, p0

    iget v3, v0, LX/8vi;->V:I

    sub-int v3, v1, v3

    .line 1419899
    if-ltz v3, :cond_2

    if-ge v3, v11, :cond_2

    .line 1419900
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1419901
    :cond_2
    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1419902
    move-object/from16 v0, p0

    iget v8, v0, LX/8vi;->ak:I

    if-ltz v8, :cond_3

    .line 1419903
    move-object/from16 v0, p0

    iget v4, v0, LX/8vi;->ak:I

    move-object/from16 v0, p0

    iget v8, v0, LX/8vi;->am:I

    sub-int/2addr v4, v8

    .line 1419904
    :cond_3
    add-int/2addr v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    move-object v9, v1

    .line 1419905
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v12, v0, LX/8vi;->aj:Z

    .line 1419906
    if-eqz v12, :cond_4

    .line 1419907
    invoke-virtual/range {p0 .. p0}, LX/8vj;->k()V

    .line 1419908
    :cond_4
    move-object/from16 v0, p0

    iget v1, v0, LX/8vi;->ao:I

    if-nez v1, :cond_5

    .line 1419909
    invoke-virtual/range {p0 .. p0}, Lit/sephiroth/android/library/widget/HListView;->c()V

    .line 1419910
    invoke-virtual/range {p0 .. p0}, LX/8vj;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1419911
    if-nez v10, :cond_0

    .line 1419912
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lit/sephiroth/android/library/widget/HListView;->at:Z

    goto :goto_0

    .line 1419913
    :pswitch_0
    :try_start_2
    move-object/from16 v0, p0

    iget v8, v0, LX/8vi;->ak:I

    move-object/from16 v0, p0

    iget v9, v0, LX/8vi;->V:I

    sub-int/2addr v8, v9

    .line 1419914
    if-ltz v8, :cond_22

    if-ge v8, v11, :cond_22

    .line 1419915
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    move-object v9, v1

    goto :goto_1

    :pswitch_1
    move-object v9, v1

    .line 1419916
    goto :goto_1

    .line 1419917
    :cond_5
    move-object/from16 v0, p0

    iget v1, v0, LX/8vi;->ao:I

    move-object/from16 v0, p0

    iget-object v8, v0, LX/8vj;->j:Landroid/widget/ListAdapter;

    invoke-interface {v8}, Landroid/widget/ListAdapter;->getCount()I

    move-result v8

    if-eq v1, v8, :cond_7

    .line 1419918
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The content of the adapter has changed but ListView did not receive a notification. Make sure the content of your adapter is not modified from a background thread, but only from the UI thread. [in ListView("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lit/sephiroth/android/library/widget/HListView;->getId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") with Adapter("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, LX/8vj;->j:Landroid/widget/ListAdapter;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1419919
    :catchall_0
    move-exception v1

    if-nez v10, :cond_6

    .line 1419920
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lit/sephiroth/android/library/widget/HListView;->at:Z

    :cond_6
    throw v1

    .line 1419921
    :cond_7
    :try_start_3
    move-object/from16 v0, p0

    iget v1, v0, LX/8vi;->ak:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, LX/8vi;->setSelectedPositionInt(I)V

    .line 1419922
    move-object/from16 v0, p0

    iget v13, v0, LX/8vi;->V:I

    .line 1419923
    move-object/from16 v0, p0

    iget-object v14, v0, LX/8vj;->p:LX/8vf;

    .line 1419924
    const/4 v1, 0x0

    .line 1419925
    if-eqz v12, :cond_8

    .line 1419926
    const/4 v8, 0x0

    :goto_2
    if-ge v8, v11, :cond_9

    .line 1419927
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v15

    add-int v16, v13, v8

    invoke-virtual/range {v14 .. v16}, LX/8vf;->a(Landroid/view/View;I)V

    .line 1419928
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 1419929
    :cond_8
    invoke-virtual {v14, v11, v13}, LX/8vf;->a(II)V

    .line 1419930
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lit/sephiroth/android/library/widget/HListView;->getFocusedChild()Landroid/view/View;

    move-result-object v8

    .line 1419931
    if-eqz v8, :cond_21

    .line 1419932
    if-eqz v12, :cond_a

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lit/sephiroth/android/library/widget/HListView;->c(Landroid/view/View;)Z

    move-result v12

    if-eqz v12, :cond_c

    .line 1419933
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lit/sephiroth/android/library/widget/HListView;->findFocus()Landroid/view/View;

    move-result-object v7

    .line 1419934
    if-eqz v7, :cond_b

    .line 1419935
    invoke-virtual {v7}, Landroid/view/View;->onStartTemporaryDetach()V

    :cond_b
    move-object v1, v8

    .line 1419936
    :cond_c
    invoke-virtual/range {p0 .. p0}, Lit/sephiroth/android/library/widget/HListView;->requestFocus()Z

    move-object v8, v7

    move-object v7, v1

    .line 1419937
    :goto_3
    invoke-virtual/range {p0 .. p0}, Lit/sephiroth/android/library/widget/HListView;->detachAllViewsFromParent()V

    .line 1419938
    invoke-virtual {v14}, LX/8vf;->d()V

    .line 1419939
    move-object/from16 v0, p0

    iget v1, v0, LX/8vj;->h:I

    packed-switch v1, :pswitch_data_1

    .line 1419940
    if-nez v11, :cond_16

    .line 1419941
    move-object/from16 v0, p0

    iget-boolean v1, v0, LX/8vj;->K:Z

    if-nez v1, :cond_15

    .line 1419942
    const/4 v1, 0x0

    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Lit/sephiroth/android/library/widget/HListView;->a(IZ)I

    move-result v1

    .line 1419943
    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, LX/8vi;->setSelectedPositionInt(I)V

    .line 1419944
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lit/sephiroth/android/library/widget/HListView;->e(I)Landroid/view/View;

    move-result-object v1

    move-object v2, v1

    .line 1419945
    :goto_4
    invoke-virtual {v14}, LX/8vf;->e()V

    .line 1419946
    if-eqz v2, :cond_1e

    .line 1419947
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lit/sephiroth/android/library/widget/HListView;->aG:Z

    if-eqz v1, :cond_1d

    invoke-virtual/range {p0 .. p0}, Lit/sephiroth/android/library/widget/HListView;->hasFocus()Z

    move-result v1

    if-eqz v1, :cond_1d

    invoke-virtual {v2}, Landroid/view/View;->hasFocus()Z

    move-result v1

    if-nez v1, :cond_1d

    .line 1419948
    if-ne v2, v7, :cond_d

    if-eqz v8, :cond_d

    invoke-virtual {v8}, Landroid/view/View;->requestFocus()Z

    move-result v1

    if-nez v1, :cond_e

    :cond_d
    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    move-result v1

    if-eqz v1, :cond_1b

    :cond_e
    const/4 v1, 0x1

    .line 1419949
    :goto_5
    if-nez v1, :cond_1c

    .line 1419950
    invoke-virtual/range {p0 .. p0}, Lit/sephiroth/android/library/widget/HListView;->getFocusedChild()Landroid/view/View;

    move-result-object v1

    .line 1419951
    if-eqz v1, :cond_f

    .line 1419952
    invoke-virtual {v1}, Landroid/view/View;->clearFocus()V

    .line 1419953
    :cond_f
    const/4 v1, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, LX/8vj;->a(ILandroid/view/View;)V

    .line 1419954
    :goto_6
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lit/sephiroth/android/library/widget/HListView;->J:I

    .line 1419955
    :cond_10
    :goto_7
    if-eqz v8, :cond_11

    invoke-virtual {v8}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    if-eqz v1, :cond_11

    .line 1419956
    invoke-virtual {v8}, Landroid/view/View;->onFinishTemporaryDetach()V

    .line 1419957
    :cond_11
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput v1, v0, Lit/sephiroth/android/library/widget/HListView;->h:I

    .line 1419958
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lit/sephiroth/android/library/widget/HListView;->aj:Z

    .line 1419959
    move-object/from16 v0, p0

    iget-object v1, v0, LX/8vj;->O:Ljava/lang/Runnable;

    if-eqz v1, :cond_12

    .line 1419960
    move-object/from16 v0, p0

    iget-object v1, v0, LX/8vj;->O:Ljava/lang/Runnable;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lit/sephiroth/android/library/widget/HListView;->post(Ljava/lang/Runnable;)Z

    .line 1419961
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lit/sephiroth/android/library/widget/HListView;->O:Ljava/lang/Runnable;

    .line 1419962
    :cond_12
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lit/sephiroth/android/library/widget/HListView;->ad:Z

    .line 1419963
    move-object/from16 v0, p0

    iget v1, v0, LX/8vi;->am:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, LX/8vi;->setNextSelectedPositionInt(I)V

    .line 1419964
    invoke-virtual/range {p0 .. p0}, LX/8vj;->e()V

    .line 1419965
    move-object/from16 v0, p0

    iget v1, v0, LX/8vi;->ao:I

    if-lez v1, :cond_13

    .line 1419966
    invoke-virtual/range {p0 .. p0}, LX/8vi;->m()V

    .line 1419967
    :cond_13
    invoke-virtual/range {p0 .. p0}, LX/8vj;->b()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1419968
    if-nez v10, :cond_0

    .line 1419969
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lit/sephiroth/android/library/widget/HListView;->at:Z

    goto/16 :goto_0

    .line 1419970
    :pswitch_2
    if-eqz v3, :cond_14

    .line 1419971
    :try_start_4
    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v1

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v5, v6}, Lit/sephiroth/android/library/widget/HListView;->a(III)Landroid/view/View;

    move-result-object v1

    move-object v2, v1

    goto/16 :goto_4

    .line 1419972
    :cond_14
    move-object/from16 v0, p0

    invoke-direct {v0, v5, v6}, Lit/sephiroth/android/library/widget/HListView;->e(II)Landroid/view/View;

    move-result-object v1

    move-object v2, v1

    .line 1419973
    goto/16 :goto_4

    .line 1419974
    :pswitch_3
    move-object/from16 v0, p0

    iget v1, v0, LX/8vi;->aa:I

    move-object/from16 v0, p0

    iget v2, v0, LX/8vi;->W:I

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2}, Lit/sephiroth/android/library/widget/HListView;->f(II)Landroid/view/View;

    move-result-object v1

    move-object v2, v1

    .line 1419975
    goto/16 :goto_4

    .line 1419976
    :pswitch_4
    move-object/from16 v0, p0

    iget v1, v0, LX/8vi;->ao:I

    add-int/lit8 v1, v1, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v6}, Lit/sephiroth/android/library/widget/HListView;->d(II)Landroid/view/View;

    move-result-object v1

    .line 1419977
    invoke-direct/range {p0 .. p0}, Lit/sephiroth/android/library/widget/HListView;->p()V

    move-object v2, v1

    .line 1419978
    goto/16 :goto_4

    .line 1419979
    :pswitch_5
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput v1, v0, Lit/sephiroth/android/library/widget/HListView;->V:I

    .line 1419980
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lit/sephiroth/android/library/widget/HListView;->e(I)Landroid/view/View;

    move-result-object v1

    .line 1419981
    invoke-direct/range {p0 .. p0}, Lit/sephiroth/android/library/widget/HListView;->p()V

    move-object v2, v1

    .line 1419982
    goto/16 :goto_4

    .line 1419983
    :pswitch_6
    invoke-virtual/range {p0 .. p0}, LX/8vj;->i()I

    move-result v1

    move-object/from16 v0, p0

    iget v2, v0, LX/8vi;->W:I

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2}, Lit/sephiroth/android/library/widget/HListView;->f(II)Landroid/view/View;

    move-result-object v1

    move-object v2, v1

    .line 1419984
    goto/16 :goto_4

    :pswitch_7
    move-object/from16 v1, p0

    .line 1419985
    invoke-direct/range {v1 .. v6}, Lit/sephiroth/android/library/widget/HListView;->a(Landroid/view/View;Landroid/view/View;III)Landroid/view/View;

    move-result-object v1

    move-object v2, v1

    .line 1419986
    goto/16 :goto_4

    .line 1419987
    :cond_15
    move-object/from16 v0, p0

    iget v1, v0, LX/8vi;->ao:I

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Lit/sephiroth/android/library/widget/HListView;->a(IZ)I

    move-result v1

    .line 1419988
    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, LX/8vi;->setSelectedPositionInt(I)V

    .line 1419989
    move-object/from16 v0, p0

    iget v1, v0, LX/8vi;->ao:I

    add-int/lit8 v1, v1, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v6}, Lit/sephiroth/android/library/widget/HListView;->d(II)Landroid/view/View;

    move-result-object v1

    move-object v2, v1

    .line 1419990
    goto/16 :goto_4

    .line 1419991
    :cond_16
    move-object/from16 v0, p0

    iget v1, v0, LX/8vi;->am:I

    if-ltz v1, :cond_18

    move-object/from16 v0, p0

    iget v1, v0, LX/8vi;->am:I

    move-object/from16 v0, p0

    iget v3, v0, LX/8vi;->ao:I

    if-ge v1, v3, :cond_18

    .line 1419992
    move-object/from16 v0, p0

    iget v1, v0, LX/8vi;->am:I

    if-nez v2, :cond_17

    :goto_8
    move-object/from16 v0, p0

    invoke-direct {v0, v1, v5}, Lit/sephiroth/android/library/widget/HListView;->f(II)Landroid/view/View;

    move-result-object v1

    move-object v2, v1

    goto/16 :goto_4

    :cond_17
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v5

    goto :goto_8

    .line 1419993
    :cond_18
    move-object/from16 v0, p0

    iget v1, v0, LX/8vi;->V:I

    move-object/from16 v0, p0

    iget v2, v0, LX/8vi;->ao:I

    if-ge v1, v2, :cond_1a

    .line 1419994
    move-object/from16 v0, p0

    iget v1, v0, LX/8vi;->V:I

    if-nez v9, :cond_19

    :goto_9
    move-object/from16 v0, p0

    invoke-direct {v0, v1, v5}, Lit/sephiroth/android/library/widget/HListView;->f(II)Landroid/view/View;

    move-result-object v1

    move-object v2, v1

    goto/16 :goto_4

    :cond_19
    invoke-virtual {v9}, Landroid/view/View;->getLeft()I

    move-result v5

    goto :goto_9

    .line 1419995
    :cond_1a
    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v5}, Lit/sephiroth/android/library/widget/HListView;->f(II)Landroid/view/View;

    move-result-object v1

    move-object v2, v1

    goto/16 :goto_4

    .line 1419996
    :cond_1b
    const/4 v1, 0x0

    goto/16 :goto_5

    .line 1419997
    :cond_1c
    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Landroid/view/View;->setSelected(Z)V

    .line 1419998
    move-object/from16 v0, p0

    iget-object v1, v0, LX/8vj;->o:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->setEmpty()V

    goto/16 :goto_6

    .line 1419999
    :cond_1d
    const/4 v1, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, LX/8vj;->a(ILandroid/view/View;)V

    goto/16 :goto_6

    .line 1420000
    :cond_1e
    move-object/from16 v0, p0

    iget v1, v0, LX/8vj;->F:I

    if-lez v1, :cond_20

    move-object/from16 v0, p0

    iget v1, v0, LX/8vj;->F:I

    const/4 v2, 0x3

    if-ge v1, v2, :cond_20

    .line 1420001
    move-object/from16 v0, p0

    iget v1, v0, LX/8vj;->A:I

    move-object/from16 v0, p0

    iget v2, v0, LX/8vi;->V:I

    sub-int/2addr v1, v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1420002
    if-eqz v1, :cond_1f

    move-object/from16 v0, p0

    iget v2, v0, LX/8vj;->A:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v1}, LX/8vj;->a(ILandroid/view/View;)V

    .line 1420003
    :cond_1f
    :goto_a
    invoke-virtual/range {p0 .. p0}, Lit/sephiroth/android/library/widget/HListView;->hasFocus()Z

    move-result v1

    if-eqz v1, :cond_10

    if-eqz v8, :cond_10

    .line 1420004
    invoke-virtual {v8}, Landroid/view/View;->requestFocus()Z

    goto/16 :goto_7

    .line 1420005
    :cond_20
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput v1, v0, Lit/sephiroth/android/library/widget/HListView;->J:I

    .line 1420006
    move-object/from16 v0, p0

    iget-object v1, v0, LX/8vj;->o:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->setEmpty()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_a

    :cond_21
    move-object v8, v7

    move-object v7, v1

    goto/16 :goto_3

    :cond_22
    move-object v9, v1

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_2
        :pswitch_4
        :pswitch_6
        :pswitch_3
        :pswitch_7
    .end packed-switch
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 24

    .prologue
    .line 1419783
    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/8vj;->y:Z

    if-eqz v2, :cond_0

    .line 1419784
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lit/sephiroth/android/library/widget/HListView;->z:Z

    .line 1419785
    :cond_0
    move-object/from16 v0, p0

    iget v8, v0, Lit/sephiroth/android/library/widget/HListView;->av:I

    .line 1419786
    move-object/from16 v0, p0

    iget-object v9, v0, Lit/sephiroth/android/library/widget/HListView;->ax:Landroid/graphics/drawable/Drawable;

    .line 1419787
    move-object/from16 v0, p0

    iget-object v10, v0, Lit/sephiroth/android/library/widget/HListView;->ay:Landroid/graphics/drawable/Drawable;

    .line 1419788
    if-eqz v9, :cond_9

    const/4 v2, 0x1

    move v7, v2

    .line 1419789
    :goto_0
    if-eqz v10, :cond_a

    const/4 v2, 0x1

    move v6, v2

    .line 1419790
    :goto_1
    if-lez v8, :cond_b

    move-object/from16 v0, p0

    iget-object v2, v0, Lit/sephiroth/android/library/widget/HListView;->au:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_b

    const/4 v2, 0x1

    move v5, v2

    .line 1419791
    :goto_2
    if-nez v5, :cond_1

    if-nez v7, :cond_1

    if-eqz v6, :cond_10

    .line 1419792
    :cond_1
    move-object/from16 v0, p0

    iget-object v11, v0, Lit/sephiroth/android/library/widget/HListView;->aH:Landroid/graphics/Rect;

    .line 1419793
    invoke-virtual/range {p0 .. p0}, Lit/sephiroth/android/library/widget/HListView;->getPaddingTop()I

    move-result v2

    iput v2, v11, Landroid/graphics/Rect;->top:I

    .line 1419794
    invoke-virtual/range {p0 .. p0}, Lit/sephiroth/android/library/widget/HListView;->getBottom()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lit/sephiroth/android/library/widget/HListView;->getTop()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual/range {p0 .. p0}, Lit/sephiroth/android/library/widget/HListView;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, v11, Landroid/graphics/Rect;->bottom:I

    .line 1419795
    invoke-virtual/range {p0 .. p0}, Lit/sephiroth/android/library/widget/HListView;->getChildCount()I

    move-result v12

    .line 1419796
    move-object/from16 v0, p0

    iget-object v2, v0, Lit/sephiroth/android/library/widget/HListView;->az:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v13

    .line 1419797
    move-object/from16 v0, p0

    iget v14, v0, LX/8vi;->ao:I

    .line 1419798
    move-object/from16 v0, p0

    iget-object v2, v0, Lit/sephiroth/android/library/widget/HListView;->aA:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    sub-int v2, v14, v2

    add-int/lit8 v15, v2, -0x1

    .line 1419799
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lit/sephiroth/android/library/widget/HListView;->aD:Z

    move/from16 v16, v0

    .line 1419800
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lit/sephiroth/android/library/widget/HListView;->aE:Z

    move/from16 v17, v0

    .line 1419801
    move-object/from16 v0, p0

    iget v0, v0, LX/8vi;->V:I

    move/from16 v18, v0

    .line 1419802
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lit/sephiroth/android/library/widget/HListView;->aF:Z

    move/from16 v19, v0

    .line 1419803
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8vj;->j:Landroid/widget/ListAdapter;

    move-object/from16 v20, v0

    .line 1419804
    invoke-virtual/range {p0 .. p0}, Lit/sephiroth/android/library/widget/HListView;->isOpaque()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-super/range {p0 .. p0}, LX/8vj;->isOpaque()Z

    move-result v2

    if-nez v2, :cond_c

    const/4 v2, 0x1

    move v4, v2

    .line 1419805
    :goto_3
    if-eqz v4, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lit/sephiroth/android/library/widget/HListView;->aI:Landroid/graphics/Paint;

    if-nez v2, :cond_2

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lit/sephiroth/android/library/widget/HListView;->aB:Z

    if-eqz v2, :cond_2

    .line 1419806
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lit/sephiroth/android/library/widget/HListView;->aI:Landroid/graphics/Paint;

    .line 1419807
    move-object/from16 v0, p0

    iget-object v2, v0, Lit/sephiroth/android/library/widget/HListView;->aI:Landroid/graphics/Paint;

    invoke-virtual/range {p0 .. p0}, LX/8vj;->getCacheColorHint()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 1419808
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lit/sephiroth/android/library/widget/HListView;->aI:Landroid/graphics/Paint;

    move-object/from16 v21, v0

    .line 1419809
    invoke-virtual/range {p0 .. p0}, Lit/sephiroth/android/library/widget/HListView;->getRight()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lit/sephiroth/android/library/widget/HListView;->getLeft()I

    move-result v3

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x0

    invoke-virtual/range {p0 .. p0}, Lit/sephiroth/android/library/widget/HListView;->getScrollX()I

    move-result v3

    add-int v22, v2, v3

    .line 1419810
    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/8vj;->K:Z

    if-nez v2, :cond_11

    .line 1419811
    const/4 v3, 0x0

    .line 1419812
    invoke-virtual/range {p0 .. p0}, Lit/sephiroth/android/library/widget/HListView;->getScrollX()I

    move-result v2

    .line 1419813
    if-lez v12, :cond_3

    if-gez v2, :cond_3

    .line 1419814
    if-eqz v7, :cond_d

    .line 1419815
    const/4 v7, 0x0

    iput v7, v11, Landroid/graphics/Rect;->right:I

    .line 1419816
    iput v2, v11, Landroid/graphics/Rect;->left:I

    .line 1419817
    move-object/from16 v0, p1

    invoke-static {v0, v9, v11}, Lit/sephiroth/android/library/widget/HListView;->a(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;Landroid/graphics/Rect;)V

    .line 1419818
    :cond_3
    :goto_4
    const/4 v2, 0x0

    move/from16 v23, v2

    move v2, v3

    move/from16 v3, v23

    :goto_5
    if-ge v3, v12, :cond_f

    .line 1419819
    if-nez v16, :cond_4

    add-int v7, v18, v3

    if-lt v7, v13, :cond_8

    :cond_4
    if-nez v17, :cond_5

    add-int v7, v18, v3

    if-ge v7, v15, :cond_8

    .line 1419820
    :cond_5
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1419821
    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v2

    .line 1419822
    if-eqz v5, :cond_8

    move/from16 v0, v22

    if-ge v2, v0, :cond_8

    if-eqz v6, :cond_6

    add-int/lit8 v7, v12, -0x1

    if-eq v3, v7, :cond_8

    .line 1419823
    :cond_6
    if-nez v19, :cond_7

    add-int v7, v18, v3

    move-object/from16 v0, v20

    invoke-interface {v0, v7}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v7

    if-eqz v7, :cond_e

    add-int/lit8 v7, v12, -0x1

    if-eq v3, v7, :cond_7

    add-int v7, v18, v3

    add-int/lit8 v7, v7, 0x1

    move-object/from16 v0, v20

    invoke-interface {v0, v7}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v7

    if-eqz v7, :cond_e

    .line 1419824
    :cond_7
    iput v2, v11, Landroid/graphics/Rect;->left:I

    .line 1419825
    add-int v7, v2, v8

    iput v7, v11, Landroid/graphics/Rect;->right:I

    .line 1419826
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v11}, Lit/sephiroth/android/library/widget/HListView;->a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    .line 1419827
    :cond_8
    :goto_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 1419828
    :cond_9
    const/4 v2, 0x0

    move v7, v2

    goto/16 :goto_0

    .line 1419829
    :cond_a
    const/4 v2, 0x0

    move v6, v2

    goto/16 :goto_1

    .line 1419830
    :cond_b
    const/4 v2, 0x0

    move v5, v2

    goto/16 :goto_2

    .line 1419831
    :cond_c
    const/4 v2, 0x0

    move v4, v2

    goto/16 :goto_3

    .line 1419832
    :cond_d
    if-eqz v5, :cond_3

    .line 1419833
    const/4 v2, 0x0

    iput v2, v11, Landroid/graphics/Rect;->right:I

    .line 1419834
    neg-int v2, v8

    iput v2, v11, Landroid/graphics/Rect;->left:I

    .line 1419835
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v11}, Lit/sephiroth/android/library/widget/HListView;->a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    goto :goto_4

    .line 1419836
    :cond_e
    if-eqz v4, :cond_8

    .line 1419837
    iput v2, v11, Landroid/graphics/Rect;->left:I

    .line 1419838
    add-int v7, v2, v8

    iput v7, v11, Landroid/graphics/Rect;->right:I

    .line 1419839
    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v11, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_6

    .line 1419840
    :cond_f
    invoke-virtual/range {p0 .. p0}, Lit/sephiroth/android/library/widget/HListView;->getRight()I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Lit/sephiroth/android/library/widget/HListView;->getScrollX()I

    move-result v4

    add-int/2addr v3, v4

    .line 1419841
    if-eqz v6, :cond_10

    add-int v4, v18, v12

    if-ne v4, v14, :cond_10

    if-le v3, v2, :cond_10

    .line 1419842
    iput v2, v11, Landroid/graphics/Rect;->left:I

    .line 1419843
    iput v3, v11, Landroid/graphics/Rect;->right:I

    .line 1419844
    move-object/from16 v0, p1

    invoke-static {v0, v10, v11}, Lit/sephiroth/android/library/widget/HListView;->b(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;Landroid/graphics/Rect;)V

    .line 1419845
    :cond_10
    :goto_7
    invoke-super/range {p0 .. p1}, LX/8vj;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 1419846
    return-void

    .line 1419847
    :cond_11
    invoke-virtual/range {p0 .. p0}, Lit/sephiroth/android/library/widget/HListView;->getScrollX()I

    move-result v3

    .line 1419848
    if-lez v12, :cond_12

    if-eqz v7, :cond_12

    .line 1419849
    iput v3, v11, Landroid/graphics/Rect;->left:I

    .line 1419850
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v2

    iput v2, v11, Landroid/graphics/Rect;->right:I

    .line 1419851
    move-object/from16 v0, p1

    invoke-static {v0, v9, v11}, Lit/sephiroth/android/library/widget/HListView;->a(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;Landroid/graphics/Rect;)V

    .line 1419852
    :cond_12
    if-eqz v7, :cond_17

    const/4 v2, 0x1

    .line 1419853
    :goto_8
    if-ge v2, v12, :cond_19

    .line 1419854
    if-nez v16, :cond_13

    add-int v7, v18, v2

    if-lt v7, v13, :cond_16

    :cond_13
    if-nez v17, :cond_14

    add-int v7, v18, v2

    if-ge v7, v15, :cond_16

    .line 1419855
    :cond_14
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 1419856
    invoke-virtual {v7}, Landroid/view/View;->getLeft()I

    move-result v7

    .line 1419857
    if-lez v7, :cond_16

    .line 1419858
    if-nez v19, :cond_15

    add-int v9, v18, v2

    move-object/from16 v0, v20

    invoke-interface {v0, v9}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v9

    if-eqz v9, :cond_18

    add-int/lit8 v9, v12, -0x1

    if-eq v2, v9, :cond_15

    add-int v9, v18, v2

    add-int/lit8 v9, v9, 0x1

    move-object/from16 v0, v20

    invoke-interface {v0, v9}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v9

    if-eqz v9, :cond_18

    .line 1419859
    :cond_15
    sub-int v9, v7, v8

    iput v9, v11, Landroid/graphics/Rect;->left:I

    .line 1419860
    iput v7, v11, Landroid/graphics/Rect;->right:I

    .line 1419861
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v11}, Lit/sephiroth/android/library/widget/HListView;->a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    .line 1419862
    :cond_16
    :goto_9
    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    .line 1419863
    :cond_17
    const/4 v2, 0x0

    goto :goto_8

    .line 1419864
    :cond_18
    if-eqz v4, :cond_16

    .line 1419865
    sub-int v9, v7, v8

    iput v9, v11, Landroid/graphics/Rect;->left:I

    .line 1419866
    iput v7, v11, Landroid/graphics/Rect;->right:I

    .line 1419867
    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v11, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_9

    .line 1419868
    :cond_19
    if-lez v12, :cond_10

    if-lez v3, :cond_10

    .line 1419869
    if-eqz v6, :cond_1a

    .line 1419870
    invoke-virtual/range {p0 .. p0}, Lit/sephiroth/android/library/widget/HListView;->getRight()I

    move-result v2

    .line 1419871
    iput v2, v11, Landroid/graphics/Rect;->left:I

    .line 1419872
    add-int/2addr v2, v3

    iput v2, v11, Landroid/graphics/Rect;->right:I

    .line 1419873
    move-object/from16 v0, p1

    invoke-static {v0, v10, v11}, Lit/sephiroth/android/library/widget/HListView;->b(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;Landroid/graphics/Rect;)V

    goto/16 :goto_7

    .line 1419874
    :cond_1a
    if-eqz v5, :cond_10

    .line 1419875
    move/from16 v0, v22

    iput v0, v11, Landroid/graphics/Rect;->left:I

    .line 1419876
    add-int v2, v22, v8

    iput v2, v11, Landroid/graphics/Rect;->right:I

    .line 1419877
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v11}, Lit/sephiroth/android/library/widget/HListView;->a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    goto/16 :goto_7
.end method

.method public final dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 1419777
    invoke-super {p0, p1}, LX/8vj;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    .line 1419778
    if-nez v0, :cond_0

    .line 1419779
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getFocusedChild()Landroid/view/View;

    move-result-object v1

    .line 1419780
    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    .line 1419781
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    invoke-virtual {p0, v0, p1}, Lit/sephiroth/android/library/widget/HListView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 1419782
    :cond_0
    return v0
.end method

.method public final drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 3

    .prologue
    .line 1419773
    invoke-super {p0, p1, p2, p3, p4}, LX/8vj;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v0

    .line 1419774
    iget-boolean v1, p0, LX/8vj;->z:Z

    if-eqz v1, :cond_0

    .line 1419775
    const/4 v1, 0x0

    iput-boolean v1, p0, Lit/sephiroth/android/library/widget/HListView;->z:Z

    .line 1419776
    :cond_0
    return v0
.end method

.method public bridge synthetic getAdapter()Landroid/widget/Adapter;
    .locals 1

    .prologue
    .line 1419771
    iget-object v0, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    move-object v0, v0

    .line 1419772
    return-object v0
.end method

.method public getAdapter()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 1419770
    iget-object v0, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method public getCheckItemIds()[J
    .locals 10
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1419753
    iget-object v0, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1419754
    invoke-virtual {p0}, LX/8vj;->getCheckedItemIds()[J

    move-result-object v0

    .line 1419755
    :goto_0
    return-object v0

    .line 1419756
    :cond_0
    iget v0, p0, LX/8vj;->b:I

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/8vj;->f:Landroid/util/SparseBooleanArray;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_3

    .line 1419757
    iget-object v5, p0, LX/8vj;->f:Landroid/util/SparseBooleanArray;

    .line 1419758
    invoke-virtual {v5}, Landroid/util/SparseBooleanArray;->size()I

    move-result v6

    .line 1419759
    new-array v2, v6, [J

    .line 1419760
    iget-object v7, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    move v3, v4

    move v1, v4

    .line 1419761
    :goto_1
    if-ge v3, v6, :cond_1

    .line 1419762
    invoke-virtual {v5, v3}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1419763
    add-int/lit8 v0, v1, 0x1

    invoke-virtual {v5, v3}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v8

    invoke-interface {v7, v8}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v8

    aput-wide v8, v2, v1

    .line 1419764
    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    .line 1419765
    :cond_1
    if-ne v1, v6, :cond_2

    move-object v0, v2

    .line 1419766
    goto :goto_0

    .line 1419767
    :cond_2
    new-array v0, v1, [J

    .line 1419768
    invoke-static {v2, v4, v0, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    .line 1419769
    :cond_3
    new-array v0, v4, [J

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public getDivider()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 1419752
    iget-object v0, p0, Lit/sephiroth/android/library/widget/HListView;->au:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getDividerWidth()I
    .locals 1

    .prologue
    .line 1419751
    iget v0, p0, Lit/sephiroth/android/library/widget/HListView;->av:I

    return v0
.end method

.method public getFooterViewsCount()I
    .locals 1

    .prologue
    .line 1419750
    iget-object v0, p0, Lit/sephiroth/android/library/widget/HListView;->aA:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getHeaderViewsCount()I
    .locals 1

    .prologue
    .line 1419749
    iget-object v0, p0, Lit/sephiroth/android/library/widget/HListView;->az:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItemsCanFocus()Z
    .locals 1

    .prologue
    .line 1419748
    iget-boolean v0, p0, Lit/sephiroth/android/library/widget/HListView;->aG:Z

    return v0
.end method

.method public getMaxScrollAmount()I
    .locals 3

    .prologue
    .line 1419747
    const v0, 0x3ea8f5c3    # 0.33f

    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getRight()I

    move-result v1

    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getLeft()I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public getOverscrollFooter()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 1419746
    iget-object v0, p0, Lit/sephiroth/android/library/widget/HListView;->ay:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getOverscrollHeader()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 1419745
    iget-object v0, p0, Lit/sephiroth/android/library/widget/HListView;->ax:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final isOpaque()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1419729
    iget-boolean v0, p0, LX/8vj;->z:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lit/sephiroth/android/library/widget/HListView;->aB:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lit/sephiroth/android/library/widget/HListView;->aC:Z

    if-nez v0, :cond_1

    :cond_0
    invoke-super {p0}, LX/8vj;->isOpaque()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    const/4 v2, 0x1

    .line 1419730
    :goto_0
    if-eqz v2, :cond_8

    .line 1419731
    iget-object v0, p0, LX/8vj;->u:Landroid/graphics/Rect;

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    .line 1419732
    :goto_1
    invoke-virtual {p0, v1}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1419733
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v3

    if-le v3, v0, :cond_5

    :cond_2
    move v0, v1

    .line 1419734
    :goto_2
    return v0

    :cond_3
    move v2, v1

    .line 1419735
    goto :goto_0

    .line 1419736
    :cond_4
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getPaddingLeft()I

    move-result v0

    goto :goto_1

    .line 1419737
    :cond_5
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getWidth()I

    move-result v3

    iget-object v0, p0, LX/8vj;->u:Landroid/graphics/Rect;

    if-eqz v0, :cond_7

    iget-object v0, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    :goto_3
    sub-int v0, v3, v0

    .line 1419738
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getChildCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p0, v3}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1419739
    if-eqz v3, :cond_6

    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v3

    if-ge v3, v0, :cond_8

    :cond_6
    move v0, v1

    .line 1419740
    goto :goto_2

    .line 1419741
    :cond_7
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getPaddingRight()I

    move-result v0

    goto :goto_3

    :cond_8
    move v0, v2

    .line 1419742
    goto :goto_2
.end method

.method public final onFinishInflate()V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2c

    const v2, -0x79456738

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1420169
    invoke-super {p0}, LX/8vj;->onFinishInflate()V

    .line 1420170
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getChildCount()I

    move-result v2

    .line 1420171
    if-lez v2, :cond_1

    .line 1420172
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 1420173
    invoke-virtual {p0, v0}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-direct {p0, v3}, Lit/sephiroth/android/library/widget/HListView;->b(Landroid/view/View;)V

    .line 1420174
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1420175
    :cond_0
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->removeAllViews()V

    .line 1420176
    :cond_1
    const v0, -0x3267ce43    # -3.1917456E8f

    invoke-static {v0, v1}, LX/02F;->g(II)V

    return-void
.end method

.method public final onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2c

    const v3, 0x409d29b2

    invoke-static {v0, v1, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v5

    .line 1420268
    invoke-super {p0, p1, p2, p3}, LX/8vj;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 1420269
    iget-object v6, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    .line 1420270
    const/4 v4, -0x1

    .line 1420271
    if-eqz v6, :cond_1

    if-eqz p1, :cond_1

    if-eqz p3, :cond_1

    .line 1420272
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getScrollX()I

    move-result v0

    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getScrollY()I

    move-result v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    .line 1420273
    invoke-interface {v6}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getChildCount()I

    move-result v1

    iget v3, p0, LX/8vi;->V:I

    add-int/2addr v1, v3

    if-ge v0, v1, :cond_0

    .line 1420274
    iput v2, p0, Lit/sephiroth/android/library/widget/HListView;->h:I

    .line 1420275
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->d()V

    .line 1420276
    :cond_0
    iget-object v7, p0, Lit/sephiroth/android/library/widget/HListView;->aH:Landroid/graphics/Rect;

    .line 1420277
    const v1, 0x7fffffff

    .line 1420278
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getChildCount()I

    move-result v8

    .line 1420279
    iget v9, p0, LX/8vi;->V:I

    move v3, v2

    .line 1420280
    :goto_0
    if-ge v3, v8, :cond_1

    .line 1420281
    add-int v0, v9, v3

    invoke-interface {v6, v0}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1420282
    invoke-virtual {p0, v3}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    .line 1420283
    invoke-virtual {v10, v7}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 1420284
    invoke-virtual {p0, v10, v7}, Lit/sephiroth/android/library/widget/HListView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 1420285
    invoke-static {p3, v7, p2}, LX/8vj;->a(Landroid/graphics/Rect;Landroid/graphics/Rect;I)I

    move-result v0

    .line 1420286
    if-ge v0, v1, :cond_3

    .line 1420287
    invoke-virtual {v10}, Landroid/view/View;->getLeft()I

    move-result v1

    move v2, v3

    .line 1420288
    :goto_1
    add-int/lit8 v3, v3, 0x1

    move v4, v2

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 1420289
    :cond_1
    if-ltz v4, :cond_2

    .line 1420290
    iget v0, p0, LX/8vi;->V:I

    add-int/2addr v0, v4

    invoke-virtual {p0, v0, v2}, Lit/sephiroth/android/library/widget/HListView;->b(II)V

    .line 1420291
    :goto_2
    const v0, 0x49865334    # 1100390.5f

    invoke-static {v0, v5}, LX/02F;->g(II)V

    return-void

    .line 1420292
    :cond_2
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->requestLayout()V

    goto :goto_2

    :cond_3
    move v0, v1

    move v1, v2

    move v2, v4

    goto :goto_1
.end method

.method public final onGlobalLayout()V
    .locals 0

    .prologue
    .line 1420267
    return-void
.end method

.method public final onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 1420264
    invoke-super {p0, p1}, LX/8vj;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1420265
    const-class v0, Lit/sephiroth/android/library/widget/HListView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 1420266
    return-void
.end method

.method public final onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 1420261
    invoke-super {p0, p1}, LX/8vj;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 1420262
    const-class v0, Lit/sephiroth/android/library/widget/HListView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 1420263
    return-void
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 1420260
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p2}, Lit/sephiroth/android/library/widget/HListView;->a(IILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final onKeyMultiple(IILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 1420259
    invoke-direct {p0, p1, p2, p3}, Lit/sephiroth/android/library/widget/HListView;->a(IILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 1420258
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p2}, Lit/sephiroth/android/library/widget/HListView;->a(IILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final onMeasure(II)V
    .locals 13
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/16 v12, 0xb

    const/high16 v11, -0x80000000

    const/4 v10, -0x1

    const/4 v6, 0x0

    .line 1420226
    invoke-super {p0, p1, p2}, LX/8vj;->onMeasure(II)V

    .line 1420227
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v9

    .line 1420228
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v7

    .line 1420229
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 1420230
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    .line 1420231
    iget-object v0, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    if-nez v0, :cond_4

    move v0, v6

    :goto_0
    iput v0, p0, Lit/sephiroth/android/library/widget/HListView;->ao:I

    .line 1420232
    iget v0, p0, LX/8vi;->ao:I

    if-lez v0, :cond_9

    if-eqz v9, :cond_0

    if-nez v7, :cond_9

    .line 1420233
    :cond_0
    iget-object v0, p0, LX/8vj;->P:[Z

    invoke-virtual {p0, v6, v0}, LX/8vj;->a(I[Z)Landroid/view/View;

    move-result-object v8

    .line 1420234
    invoke-direct {p0, v8, v6, p2}, Lit/sephiroth/android/library/widget/HListView;->a(Landroid/view/View;II)V

    .line 1420235
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    .line 1420236
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 1420237
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v12, :cond_8

    .line 1420238
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredState()I

    move-result v0

    invoke-static {v6, v0}, Lit/sephiroth/android/library/widget/HListView;->combineMeasuredStates(II)I

    move-result v0

    move v1, v0

    .line 1420239
    :goto_1
    const/4 v0, 0x1

    move v0, v0

    .line 1420240
    if-eqz v0, :cond_1

    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/8vc;

    iget v0, v0, LX/8vc;->a:I

    invoke-static {v0}, LX/8vf;->b(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1420241
    iget-object v0, p0, LX/8vj;->p:LX/8vf;

    invoke-virtual {v0, v8, v10}, LX/8vf;->a(Landroid/view/View;I)V

    :cond_1
    move v0, v2

    move v8, v3

    .line 1420242
    :goto_2
    if-nez v7, :cond_5

    .line 1420243
    iget-object v1, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    invoke-virtual {p0}, LX/8vj;->getHorizontalScrollbarHeight()I

    move-result v1

    add-int v5, v0, v1

    move v7, v5

    .line 1420244
    :goto_3
    if-nez v9, :cond_2

    .line 1420245
    iget-object v0, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    add-int/2addr v0, v8

    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getHorizontalFadingEdgeLength()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    add-int v4, v0, v1

    .line 1420246
    :cond_2
    if-ne v9, v11, :cond_3

    move-object v0, p0

    move v1, p2

    move v2, v6

    move v3, v10

    move v5, v10

    .line 1420247
    invoke-direct/range {v0 .. v5}, Lit/sephiroth/android/library/widget/HListView;->a(IIIII)I

    move-result v4

    .line 1420248
    :cond_3
    invoke-virtual {p0, v4, v7}, Lit/sephiroth/android/library/widget/HListView;->setMeasuredDimension(II)V

    .line 1420249
    iput p2, p0, Lit/sephiroth/android/library/widget/HListView;->v:I

    .line 1420250
    return-void

    .line 1420251
    :cond_4
    iget-object v0, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    goto :goto_0

    .line 1420252
    :cond_5
    if-ne v7, v11, :cond_6

    iget v0, p0, LX/8vi;->ao:I

    if-lez v0, :cond_6

    iget v0, p0, Lit/sephiroth/android/library/widget/HListView;->aw:I

    if-ltz v0, :cond_6

    .line 1420253
    iget v2, p0, Lit/sephiroth/android/library/widget/HListView;->aw:I

    iget v3, p0, Lit/sephiroth/android/library/widget/HListView;->aw:I

    move-object v0, p0

    move v1, p2

    invoke-direct/range {v0 .. v5}, Lit/sephiroth/android/library/widget/HListView;->b(IIIII)[I

    move-result-object v0

    .line 1420254
    const/4 v1, 0x1

    aget v5, v0, v1

    move v7, v5

    .line 1420255
    goto :goto_3

    .line 1420256
    :cond_6
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v12, :cond_7

    .line 1420257
    const/high16 v0, -0x1000000

    and-int/2addr v0, v1

    or-int/2addr v5, v0

    move v7, v5

    goto :goto_3

    :cond_7
    move v7, v5

    goto :goto_3

    :cond_8
    move v1, v6

    goto :goto_1

    :cond_9
    move v1, v6

    move v0, v6

    move v8, v6

    goto :goto_2
.end method

.method public final onSizeChanged(IIII)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/16 v0, 0x2c

    const v1, -0x289b77f3

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1420214
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getChildCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 1420215
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getFocusedChild()Landroid/view/View;

    move-result-object v1

    .line 1420216
    if-eqz v1, :cond_1

    .line 1420217
    iget v2, p0, LX/8vi;->V:I

    invoke-virtual {p0, v1}, Lit/sephiroth/android/library/widget/HListView;->indexOfChild(Landroid/view/View;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1420218
    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v3

    .line 1420219
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getPaddingLeft()I

    move-result v4

    sub-int v4, p1, v4

    sub-int/2addr v3, v4

    invoke-static {v5, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 1420220
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    sub-int/2addr v1, v3

    .line 1420221
    iget-object v3, p0, Lit/sephiroth/android/library/widget/HListView;->aK:Lit/sephiroth/android/library/widget/HListView$FocusSelector;

    if-nez v3, :cond_0

    .line 1420222
    new-instance v3, Lit/sephiroth/android/library/widget/HListView$FocusSelector;

    invoke-direct {v3, p0}, Lit/sephiroth/android/library/widget/HListView$FocusSelector;-><init>(Lit/sephiroth/android/library/widget/HListView;)V

    iput-object v3, p0, Lit/sephiroth/android/library/widget/HListView;->aK:Lit/sephiroth/android/library/widget/HListView$FocusSelector;

    .line 1420223
    :cond_0
    iget-object v3, p0, Lit/sephiroth/android/library/widget/HListView;->aK:Lit/sephiroth/android/library/widget/HListView$FocusSelector;

    invoke-virtual {v3, v2, v1}, Lit/sephiroth/android/library/widget/HListView$FocusSelector;->a(II)Lit/sephiroth/android/library/widget/HListView$FocusSelector;

    move-result-object v1

    invoke-virtual {p0, v1}, Lit/sephiroth/android/library/widget/HListView;->post(Ljava/lang/Runnable;)Z

    .line 1420224
    :cond_1
    invoke-super {p0, p1, p2, p3, p4}, LX/8vj;->onSizeChanged(IIII)V

    .line 1420225
    const/16 v1, 0x2d

    const v2, -0x1a3e69d9

    invoke-static {v6, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1420178
    iget v3, p2, Landroid/graphics/Rect;->left:I

    .line 1420179
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v1

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    .line 1420180
    invoke-virtual {p1}, Landroid/view/View;->getScrollX()I

    move-result v0

    neg-int v0, v0

    invoke-virtual {p1}, Landroid/view/View;->getScrollY()I

    move-result v1

    neg-int v1, v1

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    .line 1420181
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getWidth()I

    move-result v4

    .line 1420182
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getScrollX()I

    move-result v0

    .line 1420183
    add-int v1, v0, v4

    .line 1420184
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getHorizontalFadingEdgeLength()I

    move-result v5

    .line 1420185
    invoke-direct {p0}, Lit/sephiroth/android/library/widget/HListView;->q()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1420186
    iget v6, p0, LX/8vi;->am:I

    if-gtz v6, :cond_0

    if-le v3, v5, :cond_1

    .line 1420187
    :cond_0
    add-int/2addr v0, v5

    .line 1420188
    :cond_1
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getChildCount()I

    move-result v3

    .line 1420189
    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p0, v3}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v3

    .line 1420190
    invoke-direct {p0}, Lit/sephiroth/android/library/widget/HListView;->r()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1420191
    iget v6, p0, LX/8vi;->am:I

    iget v7, p0, LX/8vi;->ao:I

    add-int/lit8 v7, v7, -0x1

    if-lt v6, v7, :cond_2

    iget v6, p2, Landroid/graphics/Rect;->right:I

    sub-int v7, v3, v5

    if-ge v6, v7, :cond_3

    .line 1420192
    :cond_2
    sub-int/2addr v1, v5

    .line 1420193
    :cond_3
    iget v5, p2, Landroid/graphics/Rect;->right:I

    if-le v5, v1, :cond_6

    iget v5, p2, Landroid/graphics/Rect;->left:I

    if-le v5, v0, :cond_6

    .line 1420194
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v5

    if-le v5, v4, :cond_5

    .line 1420195
    iget v4, p2, Landroid/graphics/Rect;->left:I

    sub-int v0, v4, v0

    add-int/lit8 v0, v0, 0x0

    .line 1420196
    :goto_0
    sub-int v1, v3, v1

    .line 1420197
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    move v1, v0

    .line 1420198
    :goto_1
    if-eqz v1, :cond_8

    const/4 v0, 0x1

    .line 1420199
    :goto_2
    if-eqz v0, :cond_4

    .line 1420200
    neg-int v1, v1

    invoke-direct {p0, v1}, Lit/sephiroth/android/library/widget/HListView;->o(I)V

    .line 1420201
    const/4 v1, -0x1

    invoke-virtual {p0, v1, p1}, LX/8vj;->a(ILandroid/view/View;)V

    .line 1420202
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v1

    iput v1, p0, Lit/sephiroth/android/library/widget/HListView;->J:I

    .line 1420203
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->invalidate()V

    .line 1420204
    :cond_4
    return v0

    .line 1420205
    :cond_5
    iget v0, p2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    goto :goto_0

    .line 1420206
    :cond_6
    iget v3, p2, Landroid/graphics/Rect;->left:I

    if-ge v3, v0, :cond_9

    iget v3, p2, Landroid/graphics/Rect;->right:I

    if-ge v3, v1, :cond_9

    .line 1420207
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v3

    if-le v3, v4, :cond_7

    .line 1420208
    iget v3, p2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v1, v3

    rsub-int/lit8 v1, v1, 0x0

    .line 1420209
    :goto_3
    invoke-virtual {p0, v2}, Lit/sephiroth/android/library/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v3

    .line 1420210
    sub-int v0, v3, v0

    .line 1420211
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v1, v0

    goto :goto_1

    .line 1420212
    :cond_7
    iget v1, p2, Landroid/graphics/Rect;->left:I

    sub-int v1, v0, v1

    rsub-int/lit8 v1, v1, 0x0

    goto :goto_3

    :cond_8
    move v0, v2

    .line 1420213
    goto :goto_2

    :cond_9
    move v1, v2

    goto :goto_1
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0

    .prologue
    .line 1420177
    check-cast p1, Landroid/widget/ListAdapter;

    invoke-virtual {p0, p1}, LX/8vj;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1420075
    iget-object v0, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8vj;->i:LX/8va;

    if-eqz v0, :cond_0

    .line 1420076
    iget-object v0, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    iget-object v1, p0, LX/8vj;->i:LX/8va;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 1420077
    :cond_0
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->c()V

    .line 1420078
    iget-object v0, p0, LX/8vj;->p:LX/8vf;

    invoke-virtual {v0}, LX/8vf;->b()V

    .line 1420079
    iget-object v0, p0, Lit/sephiroth/android/library/widget/HListView;->az:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_1

    iget-object v0, p0, Lit/sephiroth/android/library/widget/HListView;->aA:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 1420080
    :cond_1
    new-instance v0, LX/8vq;

    iget-object v1, p0, Lit/sephiroth/android/library/widget/HListView;->az:Ljava/util/ArrayList;

    iget-object v2, p0, Lit/sephiroth/android/library/widget/HListView;->aA:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2, p1}, LX/8vq;-><init>(Ljava/util/ArrayList;Ljava/util/ArrayList;Landroid/widget/ListAdapter;)V

    iput-object v0, p0, Lit/sephiroth/android/library/widget/HListView;->j:Landroid/widget/ListAdapter;

    .line 1420081
    :goto_0
    const/4 v0, -0x1

    iput v0, p0, Lit/sephiroth/android/library/widget/HListView;->ar:I

    .line 1420082
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lit/sephiroth/android/library/widget/HListView;->as:J

    .line 1420083
    invoke-super {p0, p1}, LX/8vj;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1420084
    iget-object v0, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_5

    .line 1420085
    iget-object v0, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->areAllItemsEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lit/sephiroth/android/library/widget/HListView;->aF:Z

    .line 1420086
    iget v0, p0, LX/8vi;->ao:I

    iput v0, p0, Lit/sephiroth/android/library/widget/HListView;->ap:I

    .line 1420087
    iget-object v0, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    iput v0, p0, Lit/sephiroth/android/library/widget/HListView;->ao:I

    .line 1420088
    invoke-virtual {p0}, LX/8vi;->l()V

    .line 1420089
    new-instance v0, LX/8va;

    invoke-direct {v0, p0}, LX/8va;-><init>(LX/8vj;)V

    iput-object v0, p0, Lit/sephiroth/android/library/widget/HListView;->i:LX/8va;

    .line 1420090
    iget-object v0, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    iget-object v1, p0, LX/8vj;->i:LX/8va;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 1420091
    iget-object v0, p0, LX/8vj;->p:LX/8vf;

    iget-object v1, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getViewTypeCount()I

    move-result v1

    invoke-virtual {v0, v1}, LX/8vf;->a(I)V

    .line 1420092
    iget-boolean v0, p0, LX/8vj;->K:Z

    if-eqz v0, :cond_4

    .line 1420093
    iget v0, p0, LX/8vi;->ao:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0, v3}, Lit/sephiroth/android/library/widget/HListView;->a(IZ)I

    move-result v0

    .line 1420094
    :goto_1
    invoke-virtual {p0, v0}, LX/8vi;->setSelectedPositionInt(I)V

    .line 1420095
    invoke-virtual {p0, v0}, LX/8vi;->setNextSelectedPositionInt(I)V

    .line 1420096
    iget v0, p0, LX/8vi;->ao:I

    if-nez v0, :cond_2

    .line 1420097
    invoke-virtual {p0}, LX/8vi;->m()V

    .line 1420098
    :cond_2
    :goto_2
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->requestLayout()V

    .line 1420099
    return-void

    .line 1420100
    :cond_3
    iput-object p1, p0, Lit/sephiroth/android/library/widget/HListView;->j:Landroid/widget/ListAdapter;

    goto :goto_0

    .line 1420101
    :cond_4
    invoke-virtual {p0, v3, v4}, Lit/sephiroth/android/library/widget/HListView;->a(IZ)I

    move-result v0

    goto :goto_1

    .line 1420102
    :cond_5
    iput-boolean v4, p0, Lit/sephiroth/android/library/widget/HListView;->aF:Z

    .line 1420103
    invoke-virtual {p0}, LX/8vi;->l()V

    .line 1420104
    invoke-virtual {p0}, LX/8vi;->m()V

    goto :goto_2
.end method

.method public setCacheColorHint(I)V
    .locals 2

    .prologue
    .line 1420160
    ushr-int/lit8 v0, p1, 0x18

    const/16 v1, 0xff

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    .line 1420161
    :goto_0
    iput-boolean v0, p0, Lit/sephiroth/android/library/widget/HListView;->aB:Z

    .line 1420162
    if-eqz v0, :cond_1

    .line 1420163
    iget-object v0, p0, Lit/sephiroth/android/library/widget/HListView;->aI:Landroid/graphics/Paint;

    if-nez v0, :cond_0

    .line 1420164
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lit/sephiroth/android/library/widget/HListView;->aI:Landroid/graphics/Paint;

    .line 1420165
    :cond_0
    iget-object v0, p0, Lit/sephiroth/android/library/widget/HListView;->aI:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1420166
    :cond_1
    invoke-super {p0, p1}, LX/8vj;->setCacheColorHint(I)V

    .line 1420167
    return-void

    .line 1420168
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDivider(Landroid/graphics/drawable/Drawable;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1420152
    if-eqz p1, :cond_2

    .line 1420153
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iput v1, p0, Lit/sephiroth/android/library/widget/HListView;->av:I

    .line 1420154
    :goto_0
    iput-object p1, p0, Lit/sephiroth/android/library/widget/HListView;->au:Landroid/graphics/drawable/Drawable;

    .line 1420155
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    iput-boolean v0, p0, Lit/sephiroth/android/library/widget/HListView;->aC:Z

    .line 1420156
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->requestLayout()V

    .line 1420157
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->invalidate()V

    .line 1420158
    return-void

    .line 1420159
    :cond_2
    iput v0, p0, Lit/sephiroth/android/library/widget/HListView;->av:I

    goto :goto_0
.end method

.method public setDividerWidth(I)V
    .locals 0

    .prologue
    .line 1420148
    iput p1, p0, Lit/sephiroth/android/library/widget/HListView;->av:I

    .line 1420149
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->requestLayout()V

    .line 1420150
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->invalidate()V

    .line 1420151
    return-void
.end method

.method public setFooterDividersEnabled(Z)V
    .locals 0

    .prologue
    .line 1420145
    iput-boolean p1, p0, Lit/sephiroth/android/library/widget/HListView;->aE:Z

    .line 1420146
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->invalidate()V

    .line 1420147
    return-void
.end method

.method public setHeaderDividersEnabled(Z)V
    .locals 0

    .prologue
    .line 1420142
    iput-boolean p1, p0, Lit/sephiroth/android/library/widget/HListView;->aD:Z

    .line 1420143
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->invalidate()V

    .line 1420144
    return-void
.end method

.method public setItemsCanFocus(Z)V
    .locals 1

    .prologue
    .line 1420138
    iput-boolean p1, p0, Lit/sephiroth/android/library/widget/HListView;->aG:Z

    .line 1420139
    if-nez p1, :cond_0

    .line 1420140
    const/high16 v0, 0x60000

    invoke-virtual {p0, v0}, Lit/sephiroth/android/library/widget/HListView;->setDescendantFocusability(I)V

    .line 1420141
    :cond_0
    return-void
.end method

.method public setOverscrollFooter(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 1420135
    iput-object p1, p0, Lit/sephiroth/android/library/widget/HListView;->ay:Landroid/graphics/drawable/Drawable;

    .line 1420136
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->invalidate()V

    .line 1420137
    return-void
.end method

.method public setOverscrollHeader(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 1420131
    iput-object p1, p0, Lit/sephiroth/android/library/widget/HListView;->ax:Landroid/graphics/drawable/Drawable;

    .line 1420132
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->getScrollX()I

    move-result v0

    if-gez v0, :cond_0

    .line 1420133
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->invalidate()V

    .line 1420134
    :cond_0
    return-void
.end method

.method public setSelection(I)V
    .locals 1

    .prologue
    .line 1420129
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lit/sephiroth/android/library/widget/HListView;->b(II)V

    .line 1420130
    return-void
.end method

.method public setSelectionInt(I)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 1420117
    invoke-virtual {p0, p1}, LX/8vi;->setNextSelectedPositionInt(I)V

    .line 1420118
    const/4 v1, 0x0

    .line 1420119
    iget v2, p0, LX/8vi;->am:I

    .line 1420120
    if-ltz v2, :cond_4

    .line 1420121
    add-int/lit8 v3, v2, -0x1

    if-ne p1, v3, :cond_3

    .line 1420122
    :cond_0
    :goto_0
    iget-object v1, p0, LX/8vj;->I:Lit/sephiroth/android/library/widget/AbsHListView$PositionScroller;

    if-eqz v1, :cond_1

    .line 1420123
    iget-object v1, p0, LX/8vj;->I:Lit/sephiroth/android/library/widget/AbsHListView$PositionScroller;

    invoke-virtual {v1}, Lit/sephiroth/android/library/widget/AbsHListView$PositionScroller;->a()V

    .line 1420124
    :cond_1
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->d()V

    .line 1420125
    if-eqz v0, :cond_2

    .line 1420126
    invoke-virtual {p0}, Lit/sephiroth/android/library/widget/HListView;->awakenScrollBars()Z

    .line 1420127
    :cond_2
    return-void

    .line 1420128
    :cond_3
    add-int/lit8 v2, v2, 0x1

    if-eq p1, v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method
