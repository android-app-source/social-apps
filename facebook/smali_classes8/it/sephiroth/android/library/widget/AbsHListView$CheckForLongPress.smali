.class public final Lit/sephiroth/android/library/widget/AbsHListView$CheckForLongPress;
.super LX/8vb;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/8vj;


# direct methods
.method public constructor <init>(LX/8vj;)V
    .locals 1

    .prologue
    .line 1417182
    iput-object p1, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForLongPress;->a:LX/8vj;

    invoke-direct {p0, p1}, LX/8vb;-><init>(LX/8vj;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1417183
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForLongPress;->a:LX/8vj;

    iget v0, v0, LX/8vj;->A:I

    .line 1417184
    iget-object v2, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForLongPress;->a:LX/8vj;

    iget-object v3, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForLongPress;->a:LX/8vj;

    iget v3, v3, LX/8vi;->V:I

    sub-int/2addr v0, v3

    invoke-virtual {v2, v0}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1417185
    if-eqz v2, :cond_0

    .line 1417186
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForLongPress;->a:LX/8vj;

    iget v0, v0, LX/8vj;->A:I

    .line 1417187
    iget-object v3, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForLongPress;->a:LX/8vj;

    iget-object v3, v3, LX/8vj;->j:Landroid/widget/ListAdapter;

    iget-object v4, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForLongPress;->a:LX/8vj;

    iget v4, v4, LX/8vj;->A:I

    invoke-interface {v3, v4}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    .line 1417188
    invoke-virtual {p0}, LX/8vb;->b()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForLongPress;->a:LX/8vj;

    iget-boolean v3, v3, LX/8vi;->aj:Z

    if-nez v3, :cond_2

    .line 1417189
    iget-object v3, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForLongPress;->a:LX/8vj;

    invoke-virtual {v3, v2, v0, v4, v5}, LX/8vj;->b(Landroid/view/View;IJ)Z

    move-result v0

    .line 1417190
    :goto_0
    if-eqz v0, :cond_1

    .line 1417191
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForLongPress;->a:LX/8vj;

    const/4 v3, -0x1

    iput v3, v0, LX/8vj;->F:I

    .line 1417192
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForLongPress;->a:LX/8vj;

    invoke-virtual {v0, v1}, LX/8vj;->setPressed(Z)V

    .line 1417193
    invoke-virtual {v2, v1}, Landroid/view/View;->setPressed(Z)V

    .line 1417194
    :cond_0
    :goto_1
    return-void

    .line 1417195
    :cond_1
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForLongPress;->a:LX/8vj;

    const/4 v1, 0x2

    iput v1, v0, LX/8vj;->F:I

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0
.end method
