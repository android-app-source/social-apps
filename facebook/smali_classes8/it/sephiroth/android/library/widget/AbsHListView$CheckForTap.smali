.class public final Lit/sephiroth/android/library/widget/AbsHListView$CheckForTap;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/8vj;


# direct methods
.method public constructor <init>(LX/8vj;)V
    .locals 0

    .prologue
    .line 1417196
    iput-object p1, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForTap;->a:LX/8vj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1417197
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForTap;->a:LX/8vj;

    iget v0, v0, LX/8vj;->F:I

    if-nez v0, :cond_2

    .line 1417198
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForTap;->a:LX/8vj;

    iput v3, v0, LX/8vj;->F:I

    .line 1417199
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForTap;->a:LX/8vj;

    iget-object v1, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForTap;->a:LX/8vj;

    iget v1, v1, LX/8vj;->A:I

    iget-object v2, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForTap;->a:LX/8vj;

    iget v2, v2, LX/8vi;->V:I

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1417200
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->hasFocusable()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1417201
    iget-object v1, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForTap;->a:LX/8vj;

    iput v4, v1, LX/8vj;->h:I

    .line 1417202
    iget-object v1, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForTap;->a:LX/8vj;

    iget-boolean v1, v1, LX/8vi;->aj:Z

    if-nez v1, :cond_5

    .line 1417203
    invoke-virtual {v0, v3}, Landroid/view/View;->setPressed(Z)V

    .line 1417204
    iget-object v1, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForTap;->a:LX/8vj;

    invoke-virtual {v1, v3}, LX/8vj;->setPressed(Z)V

    .line 1417205
    iget-object v1, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForTap;->a:LX/8vj;

    invoke-virtual {v1}, LX/8vj;->d()V

    .line 1417206
    iget-object v1, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForTap;->a:LX/8vj;

    iget-object v2, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForTap;->a:LX/8vj;

    iget v2, v2, LX/8vj;->A:I

    invoke-virtual {v1, v2, v0}, LX/8vj;->a(ILandroid/view/View;)V

    .line 1417207
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForTap;->a:LX/8vj;

    invoke-virtual {v0}, LX/8vj;->refreshDrawableState()V

    .line 1417208
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v1

    .line 1417209
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForTap;->a:LX/8vj;

    invoke-virtual {v0}, LX/8vj;->isLongClickable()Z

    move-result v2

    .line 1417210
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForTap;->a:LX/8vj;

    iget-object v0, v0, LX/8vj;->m:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 1417211
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForTap;->a:LX/8vj;

    iget-object v0, v0, LX/8vj;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1417212
    if-eqz v0, :cond_0

    instance-of v3, v0, Landroid/graphics/drawable/TransitionDrawable;

    if-eqz v3, :cond_0

    .line 1417213
    if-eqz v2, :cond_3

    .line 1417214
    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 1417215
    :cond_0
    :goto_0
    if-eqz v2, :cond_4

    .line 1417216
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForTap;->a:LX/8vj;

    iget-object v0, v0, LX/8vj;->aB:Lit/sephiroth/android/library/widget/AbsHListView$CheckForLongPress;

    if-nez v0, :cond_1

    .line 1417217
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForTap;->a:LX/8vj;

    new-instance v2, Lit/sephiroth/android/library/widget/AbsHListView$CheckForLongPress;

    iget-object v3, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForTap;->a:LX/8vj;

    invoke-direct {v2, v3}, Lit/sephiroth/android/library/widget/AbsHListView$CheckForLongPress;-><init>(LX/8vj;)V

    .line 1417218
    iput-object v2, v0, LX/8vj;->aB:Lit/sephiroth/android/library/widget/AbsHListView$CheckForLongPress;

    .line 1417219
    :cond_1
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForTap;->a:LX/8vj;

    iget-object v0, v0, LX/8vj;->aB:Lit/sephiroth/android/library/widget/AbsHListView$CheckForLongPress;

    invoke-virtual {v0}, LX/8vb;->a()V

    .line 1417220
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForTap;->a:LX/8vj;

    iget-object v2, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForTap;->a:LX/8vj;

    iget-object v2, v2, LX/8vj;->aB:Lit/sephiroth/android/library/widget/AbsHListView$CheckForLongPress;

    int-to-long v4, v1

    invoke-virtual {v0, v2, v4, v5}, LX/8vj;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1417221
    :cond_2
    :goto_1
    return-void

    .line 1417222
    :cond_3
    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/TransitionDrawable;->resetTransition()V

    goto :goto_0

    .line 1417223
    :cond_4
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForTap;->a:LX/8vj;

    iput v5, v0, LX/8vj;->F:I

    goto :goto_1

    .line 1417224
    :cond_5
    iget-object v0, p0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForTap;->a:LX/8vj;

    iput v5, v0, LX/8vj;->F:I

    goto :goto_1
.end method
