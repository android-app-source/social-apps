.class public final LX/9kl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1532155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1532156
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 1532157
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 1532158
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 1532159
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v5, 0x1

    .line 1532160
    :goto_0
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v6

    .line 1532161
    invoke-virtual {p1, v6}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 1532162
    new-instance v0, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;

    invoke-static {v6}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;-><init>(Ljava/lang/String;JLjava/lang/String;ZLX/0Px;)V

    return-object v0

    .line 1532163
    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1532164
    new-array v0, p1, [Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;

    return-object v0
.end method
