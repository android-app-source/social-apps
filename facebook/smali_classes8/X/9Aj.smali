.class public LX/9Aj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/9Aj;


# instance fields
.field public final a:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1450417
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1450418
    iput-object p1, p0, LX/9Aj;->a:LX/0Uh;

    .line 1450419
    return-void
.end method

.method public static a(LX/0QB;)LX/9Aj;
    .locals 4

    .prologue
    .line 1450420
    sget-object v0, LX/9Aj;->b:LX/9Aj;

    if-nez v0, :cond_1

    .line 1450421
    const-class v1, LX/9Aj;

    monitor-enter v1

    .line 1450422
    :try_start_0
    sget-object v0, LX/9Aj;->b:LX/9Aj;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1450423
    if-eqz v2, :cond_0

    .line 1450424
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1450425
    new-instance p0, LX/9Aj;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-direct {p0, v3}, LX/9Aj;-><init>(LX/0Uh;)V

    .line 1450426
    move-object v0, p0

    .line 1450427
    sput-object v0, LX/9Aj;->b:LX/9Aj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1450428
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1450429
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1450430
    :cond_1
    sget-object v0, LX/9Aj;->b:LX/9Aj;

    return-object v0

    .line 1450431
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1450432
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
