.class public LX/9iC;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1527402
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1527403
    return-void
.end method

.method public static final a(Ljava/util/ArrayList;Z)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Parcelable;",
            ">;Z)",
            "LX/0Px",
            "<",
            "LX/5kD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1527364
    if-nez p0, :cond_0

    .line 1527365
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1527366
    :goto_0
    return-object v0

    .line 1527367
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1527368
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    .line 1527369
    check-cast v0, Lcom/facebook/photos/mediagallery/util/MediaGalleryDataCore;

    .line 1527370
    new-instance v4, LX/5kN;

    invoke-direct {v4}, LX/5kN;-><init>()V

    iget-object v5, v0, Lcom/facebook/photos/mediagallery/util/MediaGalleryDataCore;->a:Ljava/lang/String;

    .line 1527371
    iput-object v5, v4, LX/5kN;->D:Ljava/lang/String;

    .line 1527372
    move-object v4, v4

    .line 1527373
    new-instance v5, LX/4aM;

    invoke-direct {v5}, LX/4aM;-><init>()V

    iget-object v6, v0, Lcom/facebook/photos/mediagallery/util/MediaGalleryDataCore;->b:Ljava/lang/String;

    .line 1527374
    iput-object v6, v5, LX/4aM;->b:Ljava/lang/String;

    .line 1527375
    move-object v5, v5

    .line 1527376
    iget v6, v0, Lcom/facebook/photos/mediagallery/util/MediaGalleryDataCore;->c:I

    .line 1527377
    iput v6, v5, LX/4aM;->c:I

    .line 1527378
    move-object v5, v5

    .line 1527379
    iget v6, v0, Lcom/facebook/photos/mediagallery/util/MediaGalleryDataCore;->d:I

    .line 1527380
    iput v6, v5, LX/4aM;->a:I

    .line 1527381
    move-object v5, v5

    .line 1527382
    invoke-virtual {v5}, LX/4aM;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v5

    .line 1527383
    iput-object v5, v4, LX/5kN;->F:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1527384
    move-object v5, v4

    .line 1527385
    if-eqz p1, :cond_2

    iget-object v4, v0, Lcom/facebook/photos/mediagallery/util/MediaGalleryDataCore;->e:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    invoke-static {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;)Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    move-result-object v4

    .line 1527386
    :goto_2
    iput-object v4, v5, LX/5kN;->x:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    .line 1527387
    move-object v4, v5

    .line 1527388
    invoke-virtual {v4}, LX/5kN;->a()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;

    move-result-object v4

    move-object v0, v4

    .line 1527389
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1527390
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1527391
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    goto :goto_2
.end method

.method public static final a(LX/0Px;Z)Ljava/util/ArrayList;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/5kD;",
            ">;Z)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1527392
    if-nez p0, :cond_0

    .line 1527393
    const/4 v0, 0x0

    .line 1527394
    :goto_0
    return-object v0

    .line 1527395
    :cond_0
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 1527396
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_1

    invoke-virtual {p0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5kD;

    .line 1527397
    invoke-interface {v0}, LX/5kD;->aj_()LX/1Fb;

    move-result-object v8

    .line 1527398
    new-instance v4, Lcom/facebook/photos/mediagallery/util/MediaGalleryDataCore;

    invoke-interface {v0}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v8}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v8}, LX/1Fb;->c()I

    move-result v7

    invoke-interface {v8}, LX/1Fb;->a()I

    move-result v8

    if-eqz p1, :cond_2

    invoke-interface {v0}, LX/5kD;->G()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    move-result-object v9

    :goto_2
    invoke-direct/range {v4 .. v9}, Lcom/facebook/photos/mediagallery/util/MediaGalleryDataCore;-><init>(Ljava/lang/String;Ljava/lang/String;IILcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;)V

    move-object v0, v4

    .line 1527399
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1527400
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 1527401
    goto :goto_0

    :cond_2
    const/4 v9, 0x0

    goto :goto_2
.end method
