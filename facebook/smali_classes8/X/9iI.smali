.class public final LX/9iI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9iH;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;)V
    .locals 0

    .prologue
    .line 1527557
    iput-object p1, p0, LX/9iI;->a:Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1527558
    iget-object v0, p0, LX/9iI;->a:Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;

    const/4 v1, 0x1

    .line 1527559
    iput-boolean v1, v0, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->g:Z

    .line 1527560
    return-void
.end method

.method public final a(ILX/9iQ;)V
    .locals 12

    .prologue
    .line 1527561
    iget-object v0, p0, LX/9iI;->a:Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;

    invoke-virtual {v0}, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->d()V

    .line 1527562
    if-eqz p2, :cond_0

    .line 1527563
    iget-object v0, p2, LX/9iQ;->b:LX/74w;

    move-object v0, v0

    .line 1527564
    if-nez v0, :cond_1

    .line 1527565
    :cond_0
    :goto_0
    return-void

    .line 1527566
    :cond_1
    iget-object v0, p2, LX/9iQ;->b:LX/74w;

    move-object v0, v0

    .line 1527567
    iget-wide v10, v0, LX/74w;->a:J

    move-wide v8, v10

    .line 1527568
    iget-object v0, p0, LX/9iI;->a:Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;

    iget-wide v0, v0, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->k:J

    cmp-long v0, v8, v0

    if-eqz v0, :cond_0

    .line 1527569
    iget-object v0, p0, LX/9iI;->a:Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;

    .line 1527570
    invoke-static {v0}, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->e(Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;)V

    .line 1527571
    iget-object v0, p0, LX/9iI;->a:Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/745;

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, LX/9iI;->a:Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;

    iget-boolean v3, v3, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->g:Z

    if-eqz v3, :cond_3

    sget-object v3, LX/74P;->SWIPE:LX/74P;

    :goto_1
    iget-object v4, p0, LX/9iI;->a:Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;

    iget-object v4, v4, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->j:Ljava/lang/String;

    sget-object v5, LX/74N;->FULL_SCREEN_GALLERY:LX/74N;

    iget-object v6, p0, LX/9iI;->a:Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;

    iget-boolean v6, v6, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->g:Z

    if-eqz v6, :cond_4

    sget-object v6, LX/74S;->FULLSCREEN_GALLERY:LX/74S;

    .line 1527572
    :goto_2
    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v0, v7}, LX/745;->a(Ljava/lang/String;)LX/745;

    .line 1527573
    invoke-static {v5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/74N;

    invoke-virtual {v0, v7}, LX/745;->a(LX/74N;)LX/745;

    .line 1527574
    invoke-static {v6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/74S;

    invoke-virtual {v0, v7}, LX/745;->a(LX/74S;)LX/745;

    .line 1527575
    invoke-static {v0}, LX/745;->d(LX/745;)Ljava/util/HashMap;

    move-result-object v10

    .line 1527576
    const-string v7, "content_id"

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    invoke-virtual {v10, v7, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1527577
    const-string v11, "action"

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/74P;

    iget-object v7, v7, LX/74P;->value:Ljava/lang/String;

    invoke-virtual {v10, v11, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1527578
    if-eqz v2, :cond_2

    .line 1527579
    const-string v7, "owner_id"

    invoke-virtual {v10, v7, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1527580
    :cond_2
    sget-object v7, LX/74R;->ANDROID_PHOTOS_CONSUMPTION:LX/74R;

    invoke-static {v0, v7, v10, v1}, LX/745;->a(LX/745;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1527581
    iget-object v0, p0, LX/9iI;->a:Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;

    .line 1527582
    iput-wide v8, v0, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->k:J

    .line 1527583
    goto :goto_0

    .line 1527584
    :cond_3
    sget-object v3, LX/74P;->CLICK:LX/74P;

    goto :goto_1

    :cond_4
    iget-object v6, p0, LX/9iI;->a:Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;

    iget-object v6, v6, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->e:LX/74S;

    if-eqz v6, :cond_5

    iget-object v6, p0, LX/9iI;->a:Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;

    iget-object v6, v6, Lcom/facebook/photos/photogallery/LaunchableGalleryFragment;->e:LX/74S;

    goto :goto_2

    :cond_5
    sget-object v6, LX/74S;->UNKNOWN:LX/74S;

    goto :goto_2
.end method
