.class public LX/AOt;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/9Er;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/widget/text/BetterTextView;

.field public c:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

.field public d:Lcom/facebook/widget/text/BetterTextView;

.field public e:Lcom/facebook/widget/text/BetterTextView;

.field public f:Lcom/facebook/widget/text/BetterTextView;

.field public g:Landroid/view/View;

.field public h:LX/AOi;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1669702
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1669703
    const-class v0, LX/AOt;

    invoke-static {v0, p0}, LX/AOt;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1669704
    const v0, 0x7f0311a6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1669705
    const v0, 0x7f0d296b

    invoke-virtual {p0, v0}, LX/AOt;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/AOt;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 1669706
    const v0, 0x7f0d296d

    invoke-virtual {p0, v0}, LX/AOt;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iput-object v0, p0, LX/AOt;->c:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 1669707
    iget-object v0, p0, LX/AOt;->c:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    const p1, 0x7f0d296e

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/AOt;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 1669708
    iget-object v0, p0, LX/AOt;->c:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    const p1, 0x7f0d296f

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/AOt;->e:Lcom/facebook/widget/text/BetterTextView;

    .line 1669709
    iget-object v0, p0, LX/AOt;->c:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    const p1, 0x7f0d2970

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/AOt;->f:Lcom/facebook/widget/text/BetterTextView;

    .line 1669710
    const v0, 0x7f0d296c

    invoke-virtual {p0, v0}, LX/AOt;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/AOt;->g:Landroid/view/View;

    .line 1669711
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/AOt;

    invoke-static {p0}, LX/9Er;->b(LX/0QB;)LX/9Er;

    move-result-object p0

    check-cast p0, LX/9Er;

    iput-object p0, p1, LX/AOt;->a:LX/9Er;

    return-void
.end method
