.class public final LX/ARM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AQ4;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/AQ4",
        "<",
        "Landroid/content/Intent;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/ARS;


# direct methods
.method public constructor <init>(LX/ARS;)V
    .locals 0

    .prologue
    .line 1672904
    iput-object p1, p0, LX/ARM;->a:LX/ARS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1672905
    iget-object v0, p0, LX/ARM;->a:LX/ARS;

    iget-object v0, v0, LX/ARS;->d:LX/BJ7;

    iget-object v1, p0, LX/ARM;->a:LX/ARS;

    .line 1672906
    invoke-virtual {v1}, LX/AQ9;->R()LX/B5j;

    move-result-object v2

    move-object v1, v2

    .line 1672907
    invoke-virtual {v1}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getStoryId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/ARM;->a:LX/ARS;

    .line 1672908
    invoke-virtual {v2}, LX/AQ9;->R()LX/B5j;

    move-result-object v3

    move-object v2, v3

    .line 1672909
    invoke-virtual {v2}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v2

    invoke-interface {v2}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v2

    .line 1672910
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v4, LX/BJ6;->DRAFT_COMPOSER_POST_CLICKED:LX/BJ6;

    iget-object v4, v4, LX/BJ6;->name:Ljava/lang/String;

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v4, "story_id"

    invoke-virtual {v3, v4, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "composer_session_id"

    invoke-virtual {v3, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-static {v0, v3}, LX/BJ7;->a(LX/BJ7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1672911
    iget-object v0, p0, LX/ARM;->a:LX/ARS;

    iget-object v0, v0, LX/ARS;->a:LX/AR2;

    iget-object v1, p0, LX/ARM;->a:LX/ARS;

    .line 1672912
    invoke-virtual {v1}, LX/AQ9;->R()LX/B5j;

    move-result-object v2

    move-object v1, v2

    .line 1672913
    iget-object v2, p0, LX/ARM;->a:LX/ARS;

    .line 1672914
    iget-object v3, v2, LX/AQ9;->q:LX/ARN;

    move-object v2, v3

    .line 1672915
    invoke-virtual {v0, v1, v2}, LX/AR2;->a(Ljava/lang/Object;LX/ARN;)LX/AR1;

    move-result-object v0

    .line 1672916
    iget-object v1, p0, LX/ARM;->a:LX/ARS;

    iget-object v1, v1, LX/ARS;->b:LX/AR7;

    iget-object v2, p0, LX/ARM;->a:LX/ARS;

    .line 1672917
    invoke-virtual {v2}, LX/AQ9;->R()LX/B5j;

    move-result-object v3

    move-object v2, v3

    .line 1672918
    invoke-virtual {v1, v2, v0}, LX/AR7;->a(LX/0il;LX/AR1;)LX/AR6;

    move-result-object v0

    .line 1672919
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/AR6;->a(Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method
