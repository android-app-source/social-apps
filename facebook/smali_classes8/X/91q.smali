.class public final LX/91q;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/91v;


# direct methods
.method public constructor <init>(LX/91v;)V
    .locals 0

    .prologue
    .line 1431480
    iput-object p1, p0, LX/91q;->a:LX/91v;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1431481
    new-instance v0, Lcom/facebook/composer/minutiae/feelings/MinutiaeFeelingsController$1$1;

    invoke-direct {v0, p0}, Lcom/facebook/composer/minutiae/feelings/MinutiaeFeelingsController$1$1;-><init>(LX/91q;)V

    .line 1431482
    iget-object v1, p0, LX/91q;->a:LX/91v;

    const/4 v2, 0x0

    .line 1431483
    iput-boolean v2, v1, LX/91v;->o:Z

    .line 1431484
    iget-object v1, p0, LX/91q;->a:LX/91v;

    const/4 v2, 0x1

    .line 1431485
    iput-boolean v2, v1, LX/91v;->m:Z

    .line 1431486
    iget-object v1, p0, LX/91q;->a:LX/91v;

    invoke-static {v1}, LX/91v;->g(LX/91v;)V

    .line 1431487
    iget-object v1, p0, LX/91q;->a:LX/91v;

    iget-object v1, v1, LX/91v;->a:LX/91k;

    .line 1431488
    iget-boolean v2, v1, LX/91k;->g:Z

    if-eqz v2, :cond_0

    .line 1431489
    :goto_0
    iget-object v0, p0, LX/91q;->a:LX/91v;

    iget-object v0, v0, LX/91v;->f:LX/91D;

    iget-object v1, p0, LX/91q;->a:LX/91v;

    iget-object v1, v1, LX/91v;->d:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1431490
    iget-object v2, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v1, v2

    .line 1431491
    iget-object v2, p0, LX/91q;->a:LX/91v;

    .line 1431492
    iget-object p0, v2, LX/91v;->g:LX/5LG;

    move-object v2, p0

    .line 1431493
    invoke-interface {v2}, LX/5LG;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, LX/91D;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1431494
    return-void

    .line 1431495
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, v1, LX/91k;->g:Z

    .line 1431496
    iput-object v0, v1, LX/91k;->h:Ljava/lang/Runnable;

    .line 1431497
    iget-object v2, v1, LX/91k;->a:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    invoke-virtual {v1, v2}, LX/3mY;->u_(I)V

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 1431498
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1431499
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1431500
    check-cast v0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;

    .line 1431501
    iget-object v1, p0, LX/91q;->a:LX/91v;

    invoke-static {v1, v0}, LX/91v;->a$redex0(LX/91v;Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;)V

    .line 1431502
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;->o()Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;

    move-result-object v0

    .line 1431503
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1431504
    :cond_0
    :goto_0
    return-void

    .line 1431505
    :cond_1
    iget-object v1, p0, LX/91q;->a:LX/91v;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;->b()LX/0ut;

    move-result-object v0

    const/4 v4, 0x0

    .line 1431506
    iput-object v0, v1, LX/91v;->h:LX/0ut;

    .line 1431507
    iget-object v5, v1, LX/91v;->a:LX/91k;

    .line 1431508
    invoke-virtual {v5}, LX/91k;->e()I

    move-result p1

    .line 1431509
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 1431510
    iget-object p0, v5, LX/91k;->a:LX/0Px;

    move-object p0, p0

    .line 1431511
    invoke-virtual {v6, p0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v6

    invoke-virtual {v6, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v6

    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    iput-object v6, v5, LX/91k;->a:LX/0Px;

    .line 1431512
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v6, 0x0

    move p0, v6

    :goto_1
    if-ge p0, v3, :cond_2

    invoke-virtual {v2, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    .line 1431513
    iget-object v0, v5, LX/91k;->b:Ljava/util/Map;

    invoke-interface {v0, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1431514
    add-int/lit8 v6, p0, 0x1

    move p0, v6

    goto :goto_1

    .line 1431515
    :cond_2
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v6

    invoke-virtual {v5, p1, v6}, LX/3mY;->a(II)V

    .line 1431516
    const/4 v5, 0x0

    iput-boolean v5, v1, LX/91v;->o:Z

    .line 1431517
    iget-boolean v5, v1, LX/91v;->n:Z

    if-eqz v5, :cond_3

    .line 1431518
    invoke-static {v1}, LX/91v;->g(LX/91v;)V

    .line 1431519
    :cond_3
    iget-object v5, v1, LX/91v;->e:LX/92B;

    .line 1431520
    const v6, 0xc5000b

    const-string p0, "minutiae_feelings_selector_time_to_scroll_load"

    invoke-virtual {v5, v6, p0}, LX/92A;->b(ILjava/lang/String;)V

    .line 1431521
    goto :goto_0
.end method
