.class public LX/AHu;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Ljava/util/concurrent/Executor;

.field public final c:LX/0tX;

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0gK;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1656672
    const-class v0, LX/AHu;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AHu;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;LX/0tX;LX/0Or;LX/0gK;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            "LX/0tX;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0gK;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1656673
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1656674
    iput-object p1, p0, LX/AHu;->b:Ljava/util/concurrent/Executor;

    .line 1656675
    iput-object p2, p0, LX/AHu;->c:LX/0tX;

    .line 1656676
    iput-object p3, p0, LX/AHu;->d:LX/0Or;

    .line 1656677
    iput-object p4, p0, LX/AHu;->e:LX/0gK;

    .line 1656678
    return-void
.end method

.method public static b(LX/0QB;)LX/AHu;
    .locals 5

    .prologue
    .line 1656679
    new-instance v3, LX/AHu;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    const/16 v2, 0x15e7

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p0}, LX/0gK;->b(LX/0QB;)LX/0gK;

    move-result-object v2

    check-cast v2, LX/0gK;

    invoke-direct {v3, v0, v1, v4, v2}, LX/AHu;-><init>(Ljava/util/concurrent/Executor;LX/0tX;LX/0Or;LX/0gK;)V

    .line 1656680
    return-object v3
.end method

.method public static d(LX/AHu;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1656681
    new-instance v0, LX/AHw;

    invoke-direct {v0}, LX/AHw;-><init>()V

    move-object v1, v0

    .line 1656682
    new-instance v2, LX/2rE;

    invoke-direct {v2}, LX/2rE;-><init>()V

    iget-object v0, p0, LX/AHu;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1656683
    const-string v3, "actor_id"

    invoke-virtual {v2, v3, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1656684
    move-object v0, v2

    .line 1656685
    const-string v2, "thread_id"

    invoke-virtual {v0, v2, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1656686
    move-object v0, v0

    .line 1656687
    iget-object v2, v1, LX/0gW;->h:Ljava/lang/String;

    move-object v2, v2

    .line 1656688
    const-string v3, "client_mutation_id"

    invoke-virtual {v0, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1656689
    move-object v0, v0

    .line 1656690
    const-string v2, "0"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1656691
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 1656692
    iget-object v1, p0, LX/AHu;->c:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v0, v0

    .line 1656693
    new-instance v1, LX/AHt;

    invoke-direct {v1, p0, p1}, LX/AHt;-><init>(LX/AHu;Ljava/lang/String;)V

    iget-object v2, p0, LX/AHu;->b:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1656694
    return-void
.end method
