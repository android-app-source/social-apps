.class public LX/AQh;
.super LX/1Cv;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/3GL;

.field public final c:LX/AQu;

.field private final d:Landroid/view/View$OnClickListener;

.field private final e:Landroid/view/View$OnClickListener;

.field public f:LX/0Px;


# direct methods
.method public constructor <init>(LX/3GL;Landroid/content/Context;LX/AQu;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p2    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/AQu;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Landroid/view/View$OnClickListener;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Landroid/view/View$OnClickListener;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1671769
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 1671770
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1671771
    iput-object v0, p0, LX/AQh;->f:LX/0Px;

    .line 1671772
    iput-object p1, p0, LX/AQh;->b:LX/3GL;

    .line 1671773
    iput-object p2, p0, LX/AQh;->a:Landroid/content/Context;

    .line 1671774
    iput-object p3, p0, LX/AQh;->c:LX/AQu;

    .line 1671775
    iput-object p4, p0, LX/AQh;->d:Landroid/view/View$OnClickListener;

    .line 1671776
    iput-object p5, p0, LX/AQh;->e:Landroid/view/View$OnClickListener;

    .line 1671777
    return-void
.end method

.method private a(I)I
    .locals 1

    .prologue
    .line 1671768
    iget-object v0, p0, LX/AQh;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1671701
    const v0, 0x7f0b0b70

    invoke-direct {p0, v0}, LX/AQh;->a(I)I

    move-result v1

    .line 1671702
    if-nez p1, :cond_0

    .line 1671703
    new-instance v0, Lcom/facebook/fbui/widget/header/SectionHeaderView;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/facebook/fbui/widget/header/SectionHeaderView;-><init>(Landroid/content/Context;)V

    .line 1671704
    invoke-virtual {v0, v1, v3, v1, v3}, Lcom/facebook/fbui/widget/header/SectionHeaderView;->setPadding(IIII)V

    .line 1671705
    :goto_0
    return-object v0

    .line 1671706
    :cond_0
    new-instance v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;-><init>(Landroid/content/Context;)V

    .line 1671707
    const v2, 0x7f0b0b6f

    invoke-direct {p0, v2}, LX/AQh;->a(I)I

    move-result v2

    .line 1671708
    const v3, 0x7f0207fb

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/contentview/ContentView;->setBackgroundResource(I)V

    .line 1671709
    const v3, 0x7f0e013e

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleTextAppearance(I)V

    .line 1671710
    invoke-virtual {v0, v2, v1, v2, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setPadding(IIII)V

    .line 1671711
    const v1, 0x7f0b0b6d

    invoke-direct {p0, v1}, LX/AQh;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailPadding(I)V

    goto :goto_0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1671717
    if-nez p4, :cond_1

    .line 1671718
    check-cast p3, Lcom/facebook/fbui/widget/header/SectionHeaderView;

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p3, p2}, Lcom/facebook/fbui/widget/header/SectionHeaderView;->setTitleText(Ljava/lang/String;)V

    .line 1671719
    :cond_0
    :goto_0
    return-void

    .line 1671720
    :cond_1
    check-cast p3, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 1671721
    instance-of v0, p2, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;

    if-eqz v0, :cond_4

    .line 1671722
    check-cast p2, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;

    .line 1671723
    invoke-virtual {p2}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 1671724
    const-string v0, ""

    invoke-virtual {p3, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 1671725
    const v0, 0x7f0e013f

    invoke-virtual {p3, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleTextAppearance(I)V

    .line 1671726
    const v0, 0x7f0d00d0

    invoke-virtual {p3, v0, p2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTag(ILjava/lang/Object;)V

    .line 1671727
    const v3, 0x7f0d00cf

    iget-object v0, p0, LX/AQh;->c:LX/AQu;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/AQh;->c:LX/AQu;

    .line 1671728
    iget-object p1, v0, LX/AQu;->d:Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;

    if-eqz p1, :cond_7

    iget-object p1, v0, LX/AQu;->d:Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;

    invoke-virtual {p1}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object p1

    if-eqz p1, :cond_7

    if-eqz p2, :cond_7

    iget-object p1, v0, LX/AQu;->d:Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;

    invoke-virtual {p1}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object p1

    invoke-virtual {p2}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object p4

    invoke-virtual {p1, p4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_7

    const/4 p1, 0x1

    :goto_1
    move v0, p1

    .line 1671729
    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p3, v3, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTag(ILjava/lang/Object;)V

    .line 1671730
    iget-object v0, p0, LX/AQh;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {p3, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1671731
    invoke-virtual {p3, v2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setShowAuxView(Z)V

    .line 1671732
    invoke-virtual {p2}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    .line 1671733
    invoke-virtual {p3, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 1671734
    if-nez v0, :cond_3

    .line 1671735
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 1671736
    goto :goto_2

    .line 1671737
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 1671738
    const v0, 0x7f0b0b6e

    invoke-direct {p0, v0}, LX/AQh;->a(I)I

    move-result v0

    invoke-virtual {p3, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailSize(I)V

    goto :goto_0

    .line 1671739
    :cond_4
    instance-of v0, p2, LX/AQs;

    if-eqz v0, :cond_0

    .line 1671740
    check-cast p2, LX/AQs;

    .line 1671741
    iget-object v0, p2, LX/AQs;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1671742
    invoke-virtual {p3, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 1671743
    const/4 p1, 0x3

    const/4 v0, 0x0

    .line 1671744
    iget-object v3, p2, LX/AQs;->b:LX/0Px;

    move-object v3, v3

    .line 1671745
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    if-ge v4, p1, :cond_8

    .line 1671746
    const-string v0, ""

    .line 1671747
    :goto_3
    move-object v0, v0

    .line 1671748
    invoke-virtual {p3, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 1671749
    const v0, 0x7f0e013c

    invoke-virtual {p3, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleTextAppearance(I)V

    .line 1671750
    const v0, 0x7f0d00d0

    invoke-virtual {p3, v0, p2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTag(ILjava/lang/Object;)V

    .line 1671751
    iget-object v0, p0, LX/AQh;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {p3, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1671752
    iget-object v0, p3, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    move-object v0, v0

    .line 1671753
    if-nez v0, :cond_5

    .line 1671754
    invoke-virtual {p5}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1671755
    const v3, 0x7f03032d

    invoke-virtual {v0, v3, p3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1671756
    iget-object v0, p3, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    move-object v0, v0

    .line 1671757
    :cond_5
    if-eqz v0, :cond_6

    .line 1671758
    check-cast v0, Landroid/widget/ImageView;

    const v3, 0x7f0207eb

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1671759
    :cond_6
    invoke-virtual {p3, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setShowAuxView(Z)V

    .line 1671760
    invoke-virtual {p3, v2}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 1671761
    sget-object v0, LX/6VF;->MEDIUM:LX/6VF;

    invoke-virtual {p3, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    goto/16 :goto_0

    :cond_7
    const/4 p1, 0x0

    goto/16 :goto_1

    .line 1671762
    :cond_8
    invoke-virtual {v3, v0, p1}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v4

    .line 1671763
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object p1

    .line 1671764
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result p4

    move v3, v0

    :goto_4
    if-ge v3, p4, :cond_9

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;

    .line 1671765
    invoke-virtual {v0}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1671766
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_4

    .line 1671767
    :cond_9
    iget-object v0, p0, LX/AQh;->b:LX/3GL;

    invoke-virtual {v0, p1}, LX/3GL;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 1671716
    iget-object v0, p0, LX/AQh;->f:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1671778
    iget-object v0, p0, LX/AQh;->f:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1671715
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1671712
    invoke-virtual {p0, p1}, LX/AQh;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1671713
    const/4 v0, 0x0

    .line 1671714
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 1671700
    const/4 v0, 0x2

    return v0
.end method

.method public final notifyDataSetChanged()V
    .locals 4

    .prologue
    .line 1671683
    iget-object v0, p0, LX/AQh;->c:LX/AQu;

    if-nez v0, :cond_0

    .line 1671684
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1671685
    iput-object v0, p0, LX/AQh;->f:LX/0Px;

    .line 1671686
    :goto_0
    invoke-super {p0}, LX/1Cv;->notifyDataSetChanged()V

    .line 1671687
    return-void

    .line 1671688
    :cond_0
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    .line 1671689
    iget-object v1, p0, LX/AQh;->a:Landroid/content/Context;

    const v2, 0x7f0812bd

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1671690
    iget-object v1, p0, LX/AQh;->c:LX/AQu;

    .line 1671691
    iget-object v2, v1, LX/AQu;->f:Ljava/lang/String;

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1671692
    iget-object v2, v1, LX/AQu;->b:LX/0Px;

    .line 1671693
    :goto_1
    move-object v1, v2

    .line 1671694
    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1671695
    iget-object v1, p0, LX/AQh;->a:Landroid/content/Context;

    const v2, 0x7f0812be

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1671696
    iget-object v1, p0, LX/AQh;->c:LX/AQu;

    .line 1671697
    iget-object v2, v1, LX/AQu;->c:LX/0Px;

    move-object v1, v2

    .line 1671698
    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1671699
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/AQh;->f:LX/0Px;

    goto :goto_0

    :cond_1
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    iget-object v3, v1, LX/AQu;->g:Ljava/util/List;

    invoke-virtual {v2, v3}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v2

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    goto :goto_1
.end method
