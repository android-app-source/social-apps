.class public LX/A8U;
.super LX/1Cv;
.source ""


# instance fields
.field private final a:LX/A8S;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/A8S",
            "<",
            "LX/90i;",
            ">;"
        }
    .end annotation
.end field


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1627731
    iget-object v0, p0, LX/A8U;->a:LX/A8S;

    invoke-virtual {v0, p1}, LX/A8S;->a(I)LX/A8R;

    move-result-object v1

    .line 1627732
    iget-object v0, v1, LX/A8R;->b:LX/90h;

    check-cast v0, LX/90i;

    iget v1, v1, LX/A8R;->a:I

    invoke-virtual {v0, v1, p2}, LX/1Cv;->a(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 6

    .prologue
    .line 1627733
    iget-object v0, p0, LX/A8U;->a:LX/A8S;

    invoke-virtual {v0, p4}, LX/A8S;->a(I)LX/A8R;

    move-result-object v1

    .line 1627734
    iget-object v0, v1, LX/A8R;->b:LX/90h;

    check-cast v0, LX/90i;

    iget v4, v1, LX/A8R;->a:I

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LX/1Cv;->a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V

    .line 1627735
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 1627736
    iget-object v0, p0, LX/A8U;->a:LX/A8S;

    invoke-virtual {v0}, LX/A8S;->a()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1627737
    iget-object v0, p0, LX/A8U;->a:LX/A8S;

    .line 1627738
    invoke-static {v0}, LX/A8S;->d(LX/A8S;)V

    .line 1627739
    invoke-virtual {v0, p1}, LX/A8S;->b(I)LX/A8P;

    move-result-object v1

    .line 1627740
    iget-object p0, v1, LX/A8P;->b:LX/90h;

    iget v1, v1, LX/A8P;->a:I

    invoke-interface {p0, v1}, LX/90h;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    .line 1627741
    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1627742
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1627743
    iget-object v0, p0, LX/A8U;->a:LX/A8S;

    invoke-virtual {v0, p1}, LX/A8S;->c(I)I

    move-result v0

    return v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 1627744
    iget-object v0, p0, LX/A8U;->a:LX/A8S;

    .line 1627745
    invoke-static {v0}, LX/A8S;->d(LX/A8S;)V

    .line 1627746
    iget p0, v0, LX/A8S;->f:I

    add-int/lit8 p0, p0, 0x1

    move v0, p0

    .line 1627747
    return v0
.end method
