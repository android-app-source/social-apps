.class public LX/9Vx;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:J

.field public c:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Z


# direct methods
.method public constructor <init>(LX/9Vx;)V
    .locals 4

    .prologue
    .line 1500445
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1500446
    iget-object v0, p1, LX/9Vx;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1500447
    iput-object v0, p0, LX/9Vx;->a:Ljava/lang/String;

    .line 1500448
    iget-wide v2, p1, LX/9Vx;->b:J

    move-wide v0, v2

    .line 1500449
    iput-wide v0, p0, LX/9Vx;->b:J

    .line 1500450
    iget-object v0, p1, LX/9Vx;->c:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-object v0, v0

    .line 1500451
    iput-object v0, p0, LX/9Vx;->c:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 1500452
    iget-object v0, p1, LX/9Vx;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1500453
    iput-object v0, p0, LX/9Vx;->d:Ljava/lang/String;

    .line 1500454
    iget-object v0, p1, LX/9Vx;->e:Ljava/lang/String;

    move-object v0, v0

    .line 1500455
    iput-object v0, p0, LX/9Vx;->e:Ljava/lang/String;

    .line 1500456
    iget-object v0, p1, LX/9Vx;->f:Ljava/lang/String;

    move-object v0, v0

    .line 1500457
    iput-object v0, p0, LX/9Vx;->f:Ljava/lang/String;

    .line 1500458
    iget-boolean v0, p1, LX/9Vx;->g:Z

    move v0, v0

    .line 1500459
    iput-boolean v0, p0, LX/9Vx;->g:Z

    .line 1500460
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JLcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 1500461
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1500462
    iput-object p1, p0, LX/9Vx;->a:Ljava/lang/String;

    .line 1500463
    iput-wide p2, p0, LX/9Vx;->b:J

    .line 1500464
    iput-object p4, p0, LX/9Vx;->c:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 1500465
    iput-object p5, p0, LX/9Vx;->d:Ljava/lang/String;

    .line 1500466
    iput-object p6, p0, LX/9Vx;->e:Ljava/lang/String;

    .line 1500467
    iput-object p7, p0, LX/9Vx;->f:Ljava/lang/String;

    .line 1500468
    iput-boolean p8, p0, LX/9Vx;->g:Z

    .line 1500469
    return-void
.end method
