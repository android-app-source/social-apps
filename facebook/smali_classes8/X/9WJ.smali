.class public final enum LX/9WJ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9WJ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9WJ;

.field public static final enum DATA_ERROR:LX/9WJ;

.field public static final enum NETWORK_ERROR:LX/9WJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1501040
    new-instance v0, LX/9WJ;

    const-string v1, "NETWORK_ERROR"

    invoke-direct {v0, v1, v2}, LX/9WJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9WJ;->NETWORK_ERROR:LX/9WJ;

    .line 1501041
    new-instance v0, LX/9WJ;

    const-string v1, "DATA_ERROR"

    invoke-direct {v0, v1, v3}, LX/9WJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9WJ;->DATA_ERROR:LX/9WJ;

    .line 1501042
    const/4 v0, 0x2

    new-array v0, v0, [LX/9WJ;

    sget-object v1, LX/9WJ;->NETWORK_ERROR:LX/9WJ;

    aput-object v1, v0, v2

    sget-object v1, LX/9WJ;->DATA_ERROR:LX/9WJ;

    aput-object v1, v0, v3

    sput-object v0, LX/9WJ;->$VALUES:[LX/9WJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1501043
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9WJ;
    .locals 1

    .prologue
    .line 1501044
    const-class v0, LX/9WJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9WJ;

    return-object v0
.end method

.method public static values()[LX/9WJ;
    .locals 1

    .prologue
    .line 1501045
    sget-object v0, LX/9WJ;->$VALUES:[LX/9WJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9WJ;

    return-object v0
.end method
