.class public LX/9FO;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/9FO;


# instance fields
.field public final a:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1458525
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1458526
    iput-object p1, p0, LX/9FO;->a:Lcom/facebook/content/SecureContextHelper;

    .line 1458527
    return-void
.end method

.method public static a(LX/0QB;)LX/9FO;
    .locals 4

    .prologue
    .line 1458528
    sget-object v0, LX/9FO;->b:LX/9FO;

    if-nez v0, :cond_1

    .line 1458529
    const-class v1, LX/9FO;

    monitor-enter v1

    .line 1458530
    :try_start_0
    sget-object v0, LX/9FO;->b:LX/9FO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1458531
    if-eqz v2, :cond_0

    .line 1458532
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1458533
    new-instance p0, LX/9FO;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {p0, v3}, LX/9FO;-><init>(Lcom/facebook/content/SecureContextHelper;)V

    .line 1458534
    move-object v0, p0

    .line 1458535
    sput-object v0, LX/9FO;->b:LX/9FO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1458536
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1458537
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1458538
    :cond_1
    sget-object v0, LX/9FO;->b:LX/9FO;

    return-object v0

    .line 1458539
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1458540
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(IILandroid/content/Intent;)Lcom/facebook/ipc/media/MediaItem;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1458541
    const/4 v1, -0x1

    if-ne p1, v1, :cond_0

    const/16 v1, 0x3ba

    if-eq p0, v1, :cond_1

    .line 1458542
    :cond_0
    :goto_0
    return-object v0

    .line 1458543
    :cond_1
    const-string v1, "extra_media_items"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 1458544
    invoke-static {v1, v0}, LX/0Ph;->b(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    goto :goto_0
.end method
