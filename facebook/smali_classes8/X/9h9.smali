.class public LX/9h9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/9h9;


# instance fields
.field private final a:LX/0se;

.field private final b:Landroid/content/res/Resources;

.field private final c:LX/0y3;

.field private final d:LX/0tG;

.field private final e:LX/0tI;

.field public final f:LX/0ad;

.field public g:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(LX/0se;Landroid/content/res/Resources;LX/0y3;LX/0tG;LX/0tI;LX/0ad;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1526128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1526129
    const/4 v0, 0x0

    iput-object v0, p0, LX/9h9;->g:Ljava/lang/Boolean;

    .line 1526130
    iput-object p1, p0, LX/9h9;->a:LX/0se;

    .line 1526131
    iput-object p2, p0, LX/9h9;->b:Landroid/content/res/Resources;

    .line 1526132
    iput-object p3, p0, LX/9h9;->c:LX/0y3;

    .line 1526133
    iput-object p4, p0, LX/9h9;->d:LX/0tG;

    .line 1526134
    iput-object p5, p0, LX/9h9;->e:LX/0tI;

    .line 1526135
    iput-object p6, p0, LX/9h9;->f:LX/0ad;

    .line 1526136
    return-void
.end method

.method public static a(LX/0QB;)LX/9h9;
    .locals 10

    .prologue
    .line 1526137
    sget-object v0, LX/9h9;->h:LX/9h9;

    if-nez v0, :cond_1

    .line 1526138
    const-class v1, LX/9h9;

    monitor-enter v1

    .line 1526139
    :try_start_0
    sget-object v0, LX/9h9;->h:LX/9h9;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1526140
    if-eqz v2, :cond_0

    .line 1526141
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1526142
    new-instance v3, LX/9h9;

    invoke-static {v0}, LX/0se;->a(LX/0QB;)LX/0se;

    move-result-object v4

    check-cast v4, LX/0se;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0y3;->a(LX/0QB;)LX/0y3;

    move-result-object v6

    check-cast v6, LX/0y3;

    invoke-static {v0}, LX/0tG;->a(LX/0QB;)LX/0tG;

    move-result-object v7

    check-cast v7, LX/0tG;

    invoke-static {v0}, LX/0tI;->a(LX/0QB;)LX/0tI;

    move-result-object v8

    check-cast v8, LX/0tI;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-direct/range {v3 .. v9}, LX/9h9;-><init>(LX/0se;Landroid/content/res/Resources;LX/0y3;LX/0tG;LX/0tI;LX/0ad;)V

    .line 1526143
    move-object v0, v3

    .line 1526144
    sput-object v0, LX/9h9;->h:LX/9h9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1526145
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1526146
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1526147
    :cond_1
    sget-object v0, LX/9h9;->h:LX/9h9;

    return-object v0

    .line 1526148
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1526149
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0gW;)LX/0gW;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 1526150
    const-string v1, "icon_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v1

    const-string v2, "include_replies_in_total_count"

    .line 1526151
    iget-object v3, p0, LX/9h9;->g:Ljava/lang/Boolean;

    if-nez v3, :cond_0

    .line 1526152
    iget-object v3, p0, LX/9h9;->f:LX/0ad;

    sget-short v4, LX/0wg;->i:S

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, p0, LX/9h9;->g:Ljava/lang/Boolean;

    .line 1526153
    :cond_0
    iget-object v3, p0, LX/9h9;->g:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    move v3, v3

    .line 1526154
    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "fetch_heisman_cta"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 1526155
    iget-object v1, p0, LX/9h9;->c:LX/0y3;

    invoke-virtual {v1}, LX/0y3;->b()LX/1rv;

    move-result-object v1

    iget-object v1, v1, LX/1rv;->a:LX/0yG;

    sget-object v2, LX/0yG;->OKAY:LX/0yG;

    if-ne v1, v2, :cond_1

    .line 1526156
    :goto_0
    const-string v1, "can_fetch_suggestion"

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1526157
    iget-object v0, p0, LX/9h9;->b:Landroid/content/res/Resources;

    const v1, 0x7f0b1880

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1526158
    const-string v1, "location_suggestion_profile_image_size"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1526159
    iget-object v0, p0, LX/9h9;->d:LX/0tG;

    invoke-virtual {v0, p1}, LX/0tG;->a(LX/0gW;)V

    .line 1526160
    iget-object v0, p0, LX/9h9;->e:LX/0tI;

    invoke-virtual {v0, p1}, LX/0tI;->a(LX/0gW;)V

    .line 1526161
    iget-object v0, p0, LX/9h9;->a:LX/0se;

    .line 1526162
    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, LX/0se;->b(LX/0se;LX/0gW;LX/0wF;)LX/0gW;

    move-result-object v1

    move-object v0, v1

    .line 1526163
    return-object v0

    .line 1526164
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
