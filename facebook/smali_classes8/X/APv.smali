.class public LX/APv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0j3;",
        ":",
        "LX/0j4;",
        ":",
        "LX/0j6;",
        ":",
        "LX/0jF;",
        "PluginData::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginGetters$ProvidesPluginPublishButtonTextGetter;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0in",
        "<TPluginData;>;>",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field public final b:Landroid/content/res/Resources;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0ad;


# direct methods
.method public constructor <init>(LX/0il;Landroid/content/res/Resources;LX/0Ot;LX/0ad;)V
    .locals 2
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "Landroid/content/res/Resources;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1670566
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1670567
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/APv;->a:Ljava/lang/ref/WeakReference;

    .line 1670568
    iput-object p2, p0, LX/APv;->b:Landroid/content/res/Resources;

    .line 1670569
    iput-object p3, p0, LX/APv;->c:LX/0Ot;

    .line 1670570
    iput-object p4, p0, LX/APv;->d:LX/0ad;

    .line 1670571
    return-void
.end method
