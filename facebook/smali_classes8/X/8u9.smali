.class public LX/8u9;
.super LX/8u8;
.source ""


# instance fields
.field private d:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1414530
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-direct {p0, v0}, LX/8u9;-><init>(Landroid/util/DisplayMetrics;)V

    .line 1414531
    return-void
.end method

.method private constructor <init>(Landroid/util/DisplayMetrics;)V
    .locals 2

    .prologue
    .line 1414532
    invoke-direct {p0, p1}, LX/8u8;-><init>(Landroid/util/DisplayMetrics;)V

    .line 1414533
    const/4 v0, 0x2

    const/high16 v1, 0x40800000    # 4.0f

    invoke-static {v0, v1, p1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    iput v0, p0, LX/8u9;->d:F

    .line 1414534
    return-void
.end method


# virtual methods
.method public final drawLeadingMargin(Landroid/graphics/Canvas;Landroid/graphics/Paint;IIIIILjava/lang/CharSequence;IIZLandroid/text/Layout;)V
    .locals 6

    .prologue
    .line 1414535
    invoke-virtual {p0, p2}, LX/8u8;->a(Landroid/graphics/Paint;)V

    .line 1414536
    int-to-float v1, p3

    int-to-float v2, p5

    int-to-float v0, p3

    iget v3, p0, LX/8u9;->d:F

    add-float/2addr v3, v0

    int-to-float v4, p7

    iget-object v5, p0, LX/8u8;->c:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1414537
    return-void
.end method
