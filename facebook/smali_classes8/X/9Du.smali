.class public LX/9Du;
.super LX/9Bh;
.source ""


# instance fields
.field public a:LX/9D1;

.field public b:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/feedback/ui/TypingDotsView;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/view/View$OnClickListener;

.field public d:LX/0if;


# direct methods
.method public constructor <init>(LX/0wW;LX/4mV;LX/0if;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1456280
    invoke-direct {p0, p1, p2}, LX/9Bh;-><init>(LX/0wW;LX/4mV;)V

    .line 1456281
    iput-object p3, p0, LX/9Du;->d:LX/0if;

    .line 1456282
    return-void
.end method

.method public static a(LX/0QB;)LX/9Du;
    .locals 4

    .prologue
    .line 1456283
    new-instance v3, LX/9Du;

    invoke-static {p0}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v0

    check-cast v0, LX/0wW;

    invoke-static {p0}, LX/4mV;->a(LX/0QB;)LX/4mV;

    move-result-object v1

    check-cast v1, LX/4mV;

    invoke-static {p0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v2

    check-cast v2, LX/0if;

    invoke-direct {v3, v0, v1, v2}, LX/9Du;-><init>(LX/0wW;LX/4mV;LX/0if;)V

    .line 1456284
    move-object v0, v3

    .line 1456285
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/ViewStub;LX/9D1;)V
    .locals 2

    .prologue
    .line 1456286
    new-instance v0, LX/0zw;

    invoke-direct {v0, p1}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v0, p0, LX/9Du;->b:LX/0zw;

    .line 1456287
    iput-object p2, p0, LX/9Du;->a:LX/9D1;

    .line 1456288
    iget-object v0, p0, LX/9Du;->a:LX/9D1;

    if-nez v0, :cond_0

    .line 1456289
    :goto_0
    return-void

    .line 1456290
    :cond_0
    iget-object v0, p0, LX/9Du;->a:LX/9D1;

    new-instance v1, LX/9Ds;

    invoke-direct {v1, p0}, LX/9Ds;-><init>(LX/9Du;)V

    invoke-virtual {v0, v1}, LX/9D1;->a(LX/0fx;)V

    .line 1456291
    new-instance v0, LX/9Dt;

    invoke-direct {v0, p0}, LX/9Dt;-><init>(LX/9Du;)V

    iput-object v0, p0, LX/9Du;->c:Landroid/view/View$OnClickListener;

    goto :goto_0
.end method

.method public final a(I)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1456292
    iget-object v0, p0, LX/9Du;->b:LX/0zw;

    if-nez v0, :cond_0

    move v0, v1

    .line 1456293
    :goto_0
    return v0

    .line 1456294
    :cond_0
    iget-object v0, p0, LX/9Du;->b:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedback/ui/TypingDotsView;

    iget-object v2, p0, LX/9Du;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Lcom/facebook/feedback/ui/TypingDotsView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1456295
    if-lez p1, :cond_1

    .line 1456296
    invoke-virtual {p0}, LX/9Bh;->a()Z

    move-result v0

    goto :goto_0

    .line 1456297
    :cond_1
    invoke-virtual {p0}, LX/9Bh;->b()V

    move v0, v1

    .line 1456298
    goto :goto_0
.end method

.method public final f()LX/0zw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0zw",
            "<+",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1456299
    iget-object v0, p0, LX/9Du;->b:LX/0zw;

    return-object v0
.end method
