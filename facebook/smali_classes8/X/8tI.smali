.class public final enum LX/8tI;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8tI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8tI;

.field public static final enum ERROR:LX/8tI;

.field public static final enum INIT:LX/8tI;

.field public static final enum PAUSED:LX/8tI;

.field public static final enum PLAYING:LX/8tI;

.field public static final enum PREPARING:LX/8tI;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1412781
    new-instance v0, LX/8tI;

    const-string v1, "INIT"

    invoke-direct {v0, v1, v2}, LX/8tI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8tI;->INIT:LX/8tI;

    .line 1412782
    new-instance v0, LX/8tI;

    const-string v1, "PREPARING"

    invoke-direct {v0, v1, v3}, LX/8tI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8tI;->PREPARING:LX/8tI;

    .line 1412783
    new-instance v0, LX/8tI;

    const-string v1, "PAUSED"

    invoke-direct {v0, v1, v4}, LX/8tI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8tI;->PAUSED:LX/8tI;

    .line 1412784
    new-instance v0, LX/8tI;

    const-string v1, "PLAYING"

    invoke-direct {v0, v1, v5}, LX/8tI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8tI;->PLAYING:LX/8tI;

    .line 1412785
    new-instance v0, LX/8tI;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v6}, LX/8tI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8tI;->ERROR:LX/8tI;

    .line 1412786
    const/4 v0, 0x5

    new-array v0, v0, [LX/8tI;

    sget-object v1, LX/8tI;->INIT:LX/8tI;

    aput-object v1, v0, v2

    sget-object v1, LX/8tI;->PREPARING:LX/8tI;

    aput-object v1, v0, v3

    sget-object v1, LX/8tI;->PAUSED:LX/8tI;

    aput-object v1, v0, v4

    sget-object v1, LX/8tI;->PLAYING:LX/8tI;

    aput-object v1, v0, v5

    sget-object v1, LX/8tI;->ERROR:LX/8tI;

    aput-object v1, v0, v6

    sput-object v0, LX/8tI;->$VALUES:[LX/8tI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1412787
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8tI;
    .locals 1

    .prologue
    .line 1412788
    const-class v0, LX/8tI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8tI;

    return-object v0
.end method

.method public static values()[LX/8tI;
    .locals 1

    .prologue
    .line 1412789
    sget-object v0, LX/8tI;->$VALUES:[LX/8tI;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8tI;

    return-object v0
.end method
