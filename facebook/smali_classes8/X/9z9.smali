.class public LX/9z9;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Lcom/facebook/search/protocol/FetchGraphSearchResultDataParams;",
        "Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/9z9;


# instance fields
.field private final b:LX/0rq;

.field private final c:LX/8iF;

.field private final d:LX/0ad;


# direct methods
.method public constructor <init>(LX/0sO;LX/0rq;LX/0ad;LX/8iF;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1599916
    invoke-direct {p0, p1}, LX/0ro;-><init>(LX/0sO;)V

    .line 1599917
    iput-object p2, p0, LX/9z9;->b:LX/0rq;

    .line 1599918
    iput-object p3, p0, LX/9z9;->d:LX/0ad;

    .line 1599919
    iput-object p4, p0, LX/9z9;->c:LX/8iF;

    .line 1599920
    return-void
.end method

.method public static a(LX/0QB;)LX/9z9;
    .locals 7

    .prologue
    .line 1599903
    sget-object v0, LX/9z9;->e:LX/9z9;

    if-nez v0, :cond_1

    .line 1599904
    const-class v1, LX/9z9;

    monitor-enter v1

    .line 1599905
    :try_start_0
    sget-object v0, LX/9z9;->e:LX/9z9;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1599906
    if-eqz v2, :cond_0

    .line 1599907
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1599908
    new-instance p0, LX/9z9;

    invoke-static {v0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v3

    check-cast v3, LX/0sO;

    invoke-static {v0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v4

    check-cast v4, LX/0rq;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static {v0}, LX/8iF;->a(LX/0QB;)LX/8iF;

    move-result-object v6

    check-cast v6, LX/8iF;

    invoke-direct {p0, v3, v4, v5, v6}, LX/9z9;-><init>(LX/0sO;LX/0rq;LX/0ad;LX/8iF;)V

    .line 1599909
    move-object v0, p0

    .line 1599910
    sput-object v0, LX/9z9;->e:LX/9z9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1599911
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1599912
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1599913
    :cond_1
    sget-object v0, LX/9z9;->e:LX/9z9;

    return-object v0

    .line 1599914
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1599915
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1599882
    iget-object v0, p0, LX/0ro;->a:LX/0sO;

    const-class v1, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    const-string v2, "graphQLGraphSearchQuery"

    invoke-virtual {v0, p3, v1, v2}, LX/0sO;->a(LX/15w;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    .line 1599883
    return-object v0
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 1599902
    const/4 v0, 0x1

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 6

    .prologue
    .line 1599884
    check-cast p1, Lcom/facebook/search/protocol/FetchGraphSearchResultDataParams;

    const/4 v0, 0x0

    .line 1599885
    new-instance v1, LX/A14;

    invoke-direct {v1}, LX/A14;-><init>()V

    move-object v1, v1

    .line 1599886
    iget-object v2, p0, LX/9z9;->c:LX/8iF;

    invoke-virtual {v2}, LX/8iF;->b()I

    move-result v2

    .line 1599887
    const-string v3, "query"

    .line 1599888
    iget-object v4, p1, Lcom/facebook/search/protocol/FetchGraphSearchResultDataParams;->a:Ljava/lang/String;

    move-object v4, v4

    .line 1599889
    invoke-virtual {v1, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v4, "count"

    .line 1599890
    iget v5, p1, Lcom/facebook/search/protocol/FetchGraphSearchResultDataParams;->b:I

    move v5, v5

    .line 1599891
    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v4, "profile_image_size"

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "image_size"

    iget-object v4, p0, LX/9z9;->b:LX/0rq;

    invoke-virtual {v4}, LX/0rq;->f()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "media_type"

    iget-object v4, p0, LX/9z9;->b:LX/0rq;

    invoke-virtual {v4}, LX/0rq;->a()LX/0wF;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v2

    const-string v3, "facepile_image_size"

    iget-object v4, p0, LX/9z9;->c:LX/8iF;

    .line 1599892
    iget v5, v4, LX/8iF;->f:I

    move v4, v5

    .line 1599893
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "facepile_count"

    const/16 v4, 0x9

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "page_cta_enabled"

    iget-object v4, p0, LX/9z9;->d:LX/0ad;

    sget-short v5, LX/100;->bq:S

    invoke-interface {v4, v5, v0}, LX/0ad;->a(SZ)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, LX/9z9;->d:LX/0ad;

    sget-short v5, LX/100;->br:S

    invoke-interface {v4, v5, v0}, LX/0ad;->a(SZ)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v0

    const-string v2, "tsid"

    .line 1599894
    iget-object v3, p1, Lcom/facebook/search/protocol/FetchGraphSearchResultDataParams;->d:Ljava/lang/String;

    move-object v3, v3

    .line 1599895
    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1599896
    iget-object v0, p1, Lcom/facebook/search/protocol/FetchGraphSearchResultDataParams;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1599897
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1599898
    const-string v0, "after"

    .line 1599899
    iget-object v2, p1, Lcom/facebook/search/protocol/FetchGraphSearchResultDataParams;->c:Ljava/lang/String;

    move-object v2, v2

    .line 1599900
    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1599901
    :cond_2
    return-object v1
.end method
