.class public LX/8si;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1411747
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1411748
    return-void
.end method

.method public static a(Landroid/content/Context;ZZ)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1411749
    if-eqz p1, :cond_1

    sget-object v0, LX/8AB;->MEDIA_COMMENT:LX/8AB;

    .line 1411750
    :goto_0
    new-instance v1, LX/8AA;

    invoke-direct {v1, v0}, LX/8AA;-><init>(LX/8AB;)V

    sget-object v0, LX/8A9;->NONE:LX/8A9;

    invoke-virtual {v1, v0}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->i()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->l()LX/8AA;

    move-result-object v0

    .line 1411751
    if-nez p1, :cond_2

    .line 1411752
    invoke-virtual {v0}, LX/8AA;->j()LX/8AA;

    .line 1411753
    :cond_0
    :goto_1
    invoke-static {p0, v0}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    .line 1411754
    :cond_1
    sget-object v0, LX/8AB;->PHOTO_COMMENT:LX/8AB;

    goto :goto_0

    .line 1411755
    :cond_2
    if-eqz p2, :cond_0

    .line 1411756
    invoke-virtual {v0}, LX/8AA;->s()LX/8AA;

    goto :goto_1
.end method
