.class public final enum LX/9lQ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9lQ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9lQ;

.field public static final enum FRIEND:LX/9lQ;

.field public static final enum SELF:LX/9lQ;

.field public static final enum SUBSCRIBED_TO:LX/9lQ;

.field public static final enum UNDEFINED:LX/9lQ;

.field public static final enum UNKNOWN_RELATIONSHIP:LX/9lQ;


# instance fields
.field private mType:I


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1532836
    new-instance v0, LX/9lQ;

    const-string v1, "UNDEFINED"

    invoke-direct {v0, v1, v2, v2}, LX/9lQ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/9lQ;->UNDEFINED:LX/9lQ;

    .line 1532837
    new-instance v0, LX/9lQ;

    const-string v1, "SELF"

    invoke-direct {v0, v1, v3, v3}, LX/9lQ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/9lQ;->SELF:LX/9lQ;

    .line 1532838
    new-instance v0, LX/9lQ;

    const-string v1, "FRIEND"

    invoke-direct {v0, v1, v4, v4}, LX/9lQ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/9lQ;->FRIEND:LX/9lQ;

    .line 1532839
    new-instance v0, LX/9lQ;

    const-string v1, "SUBSCRIBED_TO"

    invoke-direct {v0, v1, v5, v5}, LX/9lQ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/9lQ;->SUBSCRIBED_TO:LX/9lQ;

    .line 1532840
    new-instance v0, LX/9lQ;

    const-string v1, "UNKNOWN_RELATIONSHIP"

    invoke-direct {v0, v1, v6, v6}, LX/9lQ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/9lQ;->UNKNOWN_RELATIONSHIP:LX/9lQ;

    .line 1532841
    const/4 v0, 0x5

    new-array v0, v0, [LX/9lQ;

    sget-object v1, LX/9lQ;->UNDEFINED:LX/9lQ;

    aput-object v1, v0, v2

    sget-object v1, LX/9lQ;->SELF:LX/9lQ;

    aput-object v1, v0, v3

    sget-object v1, LX/9lQ;->FRIEND:LX/9lQ;

    aput-object v1, v0, v4

    sget-object v1, LX/9lQ;->SUBSCRIBED_TO:LX/9lQ;

    aput-object v1, v0, v5

    sget-object v1, LX/9lQ;->UNKNOWN_RELATIONSHIP:LX/9lQ;

    aput-object v1, v0, v6

    sput-object v0, LX/9lQ;->$VALUES:[LX/9lQ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 1532853
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1532854
    iput p3, p0, LX/9lQ;->mType:I

    .line 1532855
    return-void
.end method

.method public static getRelationshipType(ZLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)LX/9lQ;
    .locals 1

    .prologue
    .line 1532845
    if-eqz p0, :cond_0

    .line 1532846
    sget-object v0, LX/9lQ;->SELF:LX/9lQ;

    .line 1532847
    :goto_0
    return-object v0

    .line 1532848
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0, p1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1532849
    sget-object v0, LX/9lQ;->FRIEND:LX/9lQ;

    goto :goto_0

    .line 1532850
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-ne p2, v0, :cond_2

    .line 1532851
    sget-object v0, LX/9lQ;->SUBSCRIBED_TO:LX/9lQ;

    goto :goto_0

    .line 1532852
    :cond_2
    sget-object v0, LX/9lQ;->UNKNOWN_RELATIONSHIP:LX/9lQ;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/9lQ;
    .locals 1

    .prologue
    .line 1532844
    const-class v0, LX/9lQ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9lQ;

    return-object v0
.end method

.method public static values()[LX/9lQ;
    .locals 1

    .prologue
    .line 1532843
    sget-object v0, LX/9lQ;->$VALUES:[LX/9lQ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9lQ;

    return-object v0
.end method


# virtual methods
.method public final getValue()I
    .locals 1

    .prologue
    .line 1532842
    iget v0, p0, LX/9lQ;->mType:I

    return v0
.end method
