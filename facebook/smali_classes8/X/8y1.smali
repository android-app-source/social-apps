.class public final LX/8y1;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightEditFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0TF;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/8y2;


# direct methods
.method public constructor <init>(LX/8y2;LX/0TF;Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1425050
    iput-object p1, p0, LX/8y1;->d:LX/8y2;

    iput-object p2, p0, LX/8y1;->a:LX/0TF;

    iput-object p3, p0, LX/8y1;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object p4, p0, LX/8y1;->c:Ljava/lang/String;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1425051
    iget-object v0, p0, LX/8y1;->a:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 1425052
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 11

    .prologue
    .line 1425053
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1425054
    iget-object v0, p0, LX/8y1;->a:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 1425055
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1425056
    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightEditFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightEditFieldsModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserCreatedRecommendationDetailsModel;

    move-result-object v0

    invoke-static {v0}, LX/5I9;->a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserCreatedRecommendationDetailsModel;)Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;

    move-result-object v0

    .line 1425057
    iget-object v1, p0, LX/8y1;->d:LX/8y2;

    iget-object v1, v1, LX/8y2;->c:LX/20j;

    iget-object v2, p0, LX/8y1;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    iget-object v3, p0, LX/8y1;->c:Ljava/lang/String;

    const/4 v5, 0x0

    .line 1425058
    invoke-static {v2}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v7

    .line 1425059
    invoke-virtual {v7}, LX/0Px;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    move-object v4, v5

    .line 1425060
    :goto_0
    move-object v0, v4

    .line 1425061
    if-eqz v0, :cond_0

    .line 1425062
    iget-object v1, p0, LX/8y1;->d:LX/8y2;

    iget-object v1, v1, LX/8y2;->b:LX/1K9;

    new-instance v2, LX/8q4;

    iget-object v3, v0, LX/6PS;->b:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v0, v0, LX/6PS;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, LX/8q4;-><init>(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/1K9;->a(LX/1KJ;)V

    .line 1425063
    :cond_0
    return-void

    .line 1425064
    :cond_1
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    const/4 v4, 0x0

    move v6, v4

    :goto_1
    if-ge v6, v8, :cond_4

    invoke-virtual {v7, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1425065
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 1425066
    :goto_2
    if-nez v4, :cond_3

    move-object v4, v5

    .line 1425067
    goto :goto_0

    .line 1425068
    :cond_2
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_1

    .line 1425069
    :cond_3
    invoke-static {v4, v0}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v5

    .line 1425070
    invoke-static {v7, v4, v5}, LX/20j;->a(LX/0Px;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;)LX/0Px;

    move-result-object v4

    .line 1425071
    invoke-static {v2}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/3dM;

    move-result-object v6

    iget-object v7, v1, LX/20j;->b:LX/0SG;

    invoke-interface {v7}, LX/0SG;->a()J

    move-result-wide v8

    .line 1425072
    iput-wide v8, v6, LX/3dM;->v:J

    .line 1425073
    move-object v6, v6

    .line 1425074
    invoke-static {v2}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v7

    invoke-static {v2}, LX/16z;->g(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v8

    invoke-static {v6, v4, v7, v8}, LX/20j;->a(LX/3dM;Ljava/util/List;ILcom/facebook/graphql/model/GraphQLPageInfo;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v6

    .line 1425075
    new-instance v4, LX/6PS;

    invoke-direct {v4, v6, v5}, LX/6PS;-><init>(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;)V

    goto :goto_0

    :cond_4
    move-object v4, v5

    goto :goto_2
.end method
