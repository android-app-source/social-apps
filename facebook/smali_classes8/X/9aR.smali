.class public LX/9aR;
.super Landroid/view/View;
.source ""


# instance fields
.field public a:LX/9aQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:LX/9Aq;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1513738
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1513739
    const-class p1, LX/9aR;

    invoke-static {p1, p0}, LX/9aR;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1513740
    iget-object p1, p0, LX/9aR;->a:LX/9aQ;

    invoke-virtual {p0, p1}, LX/9aR;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1513741
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/9aR;

    new-instance v0, LX/9aQ;

    const-class v2, Landroid/content/Context;

    invoke-interface {v1, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {v1}, LX/0wX;->a(LX/0QB;)LX/0wX;

    move-result-object v3

    check-cast v3, LX/0wY;

    invoke-static {v1}, LX/8iw;->b(LX/0QB;)LX/8iw;

    move-result-object v4

    check-cast v4, LX/3RX;

    invoke-static {v1}, LX/3RZ;->b(LX/0QB;)LX/3RZ;

    move-result-object p0

    check-cast p0, LX/3RZ;

    invoke-direct {v0, v2, v3, v4, p0}, LX/9aQ;-><init>(Landroid/content/Context;LX/0wY;LX/3RX;LX/3RZ;)V

    move-object v1, v0

    check-cast v1, LX/9aQ;

    iput-object v1, p1, LX/9aR;->a:LX/9aQ;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1513742
    iget-object v0, p0, LX/9aR;->a:LX/9aQ;

    .line 1513743
    iget-object v1, v0, LX/9aQ;->a:LX/0wY;

    iget-object p0, v0, LX/9aQ;->m:LX/0wa;

    invoke-interface {v1, p0}, LX/0wY;->b(LX/0wa;)V

    .line 1513744
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/9aQ;->l:Z

    .line 1513745
    iget-object v1, v0, LX/9aQ;->d:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->clear()V

    .line 1513746
    iget-object v1, v0, LX/9aQ;->e:Ljava/util/Deque;

    invoke-interface {v1}, Ljava/util/Deque;->clear()V

    .line 1513747
    iget-object v1, v0, LX/9aQ;->f:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->clear()V

    .line 1513748
    invoke-virtual {v0}, LX/9aQ;->invalidateSelf()V

    .line 1513749
    return-void
.end method

.method public final a(LX/9aS;)V
    .locals 5

    .prologue
    .line 1513750
    iget-object v0, p0, LX/9aR;->a:LX/9aQ;

    .line 1513751
    invoke-virtual {v0}, LX/9aQ;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1513752
    iget-object v1, v0, LX/9aQ;->a:LX/0wY;

    iget-object v2, v0, LX/9aQ;->m:LX/0wa;

    invoke-interface {v1, v2}, LX/0wY;->a(LX/0wa;)V

    .line 1513753
    :cond_0
    iget-object v1, v0, LX/9aQ;->d:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1513754
    iget-wide v1, v0, LX/9aQ;->h:J

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v1

    iput-wide v1, v0, LX/9aQ;->h:J

    .line 1513755
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/9aQ;->l:Z

    .line 1513756
    iget-object v1, v0, LX/9aQ;->d:Ljava/util/Queue;

    invoke-interface {v1, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 1513757
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x2

    const v2, 0x252cfe62

    invoke-static {v1, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1513758
    iget-object v2, p0, LX/9aR;->a:LX/9aQ;

    .line 1513759
    iget-object v3, v2, LX/9aQ;->i:Landroid/view/GestureDetector;

    invoke-virtual {v3, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    .line 1513760
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 1513761
    :goto_0
    :pswitch_0
    move v2, v3

    .line 1513762
    if-nez v2, :cond_0

    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_1
    const v2, 0x1a99aae1

    invoke-static {v2, v1}, LX/02F;->a(II)V

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1513763
    :pswitch_1
    const/4 v3, 0x0

    iput-object v3, v2, LX/9aQ;->k:LX/9aL;

    .line 1513764
    const/4 v3, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onWindowVisibilityChanged(I)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x63609d5d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1513765
    invoke-super {p0, p1}, Landroid/view/View;->onWindowVisibilityChanged(I)V

    .line 1513766
    const/16 v1, 0x8

    if-ne p1, v1, :cond_0

    .line 1513767
    iget-object v1, p0, LX/9aR;->a:LX/9aQ;

    invoke-virtual {v1}, LX/9aQ;->a()Z

    move-result v1

    move v1, v1

    .line 1513768
    if-eqz v1, :cond_0

    iget-object v1, p0, LX/9aR;->b:LX/9Aq;

    if-eqz v1, :cond_0

    .line 1513769
    invoke-virtual {p0}, LX/9aR;->a()V

    .line 1513770
    iget-object v1, p0, LX/9aR;->b:LX/9Aq;

    invoke-interface {v1}, LX/9Aq;->a()V

    .line 1513771
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x20aa6ee7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setListener(LX/9Aq;)V
    .locals 1

    .prologue
    .line 1513772
    iput-object p1, p0, LX/9aR;->b:LX/9Aq;

    .line 1513773
    iget-object v0, p0, LX/9aR;->a:LX/9aQ;

    .line 1513774
    iput-object p1, v0, LX/9aQ;->j:LX/9Aq;

    .line 1513775
    return-void
.end method
