.class public final LX/9JO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field public final a:Ljava/nio/channels/FileChannel;


# direct methods
.method public constructor <init>(Ljava/nio/channels/FileChannel;)V
    .locals 0

    .prologue
    .line 1464679
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1464680
    iput-object p1, p0, LX/9JO;->a:Ljava/nio/channels/FileChannel;

    .line 1464681
    return-void
.end method


# virtual methods
.method public final a(Ljava/nio/ByteBuffer;)V
    .locals 1

    .prologue
    .line 1464682
    iget-object v0, p0, LX/9JO;->a:Ljava/nio/channels/FileChannel;

    invoke-virtual {v0, p1}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 1464683
    return-void
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 1464684
    iget-object v0, p0, LX/9JO;->a:Ljava/nio/channels/FileChannel;

    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->close()V

    .line 1464685
    return-void
.end method
