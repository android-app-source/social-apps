.class public LX/9ac;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/9af;

.field private b:LX/0gc;

.field public c:LX/9fy;

.field private d:LX/1Ck;

.field public e:Lcom/facebook/auth/viewercontext/ViewerContext;

.field public f:LX/9bY;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/9af;LX/9fy;LX/1Ck;Lcom/facebook/auth/viewercontext/ViewerContext;LX/9bY;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1513877
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1513878
    iput-object p2, p0, LX/9ac;->a:LX/9af;

    .line 1513879
    iput-object p3, p0, LX/9ac;->c:LX/9fy;

    .line 1513880
    iput-object p4, p0, LX/9ac;->d:LX/1Ck;

    .line 1513881
    check-cast p1, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    iput-object v0, p0, LX/9ac;->b:LX/0gc;

    .line 1513882
    iput-object p5, p0, LX/9ac;->e:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1513883
    iput-object p6, p0, LX/9ac;->f:LX/9bY;

    .line 1513884
    return-void
.end method

.method public static a$redex0(LX/9ac;Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 1513885
    iget-object v0, p0, LX/9ac;->a:LX/9af;

    .line 1513886
    sget-object p0, LX/9ae;->ALBUM_CREATOR_CANCELLED:LX/9ae;

    invoke-static {p0}, LX/9af;->a(LX/9ae;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-static {v0, p0}, LX/9af;->a(LX/9af;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1513887
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/app/Activity;->setResult(I)V

    .line 1513888
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 1513889
    return-void
.end method


# virtual methods
.method public final a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Lcom/facebook/composer/privacy/model/ComposerPrivacyData;LX/9ab;Lcom/facebook/ipc/composer/intent/ComposerTargetData;)V
    .locals 12
    .param p7    # Lcom/facebook/ipc/composer/intent/ComposerTargetData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1513890
    const v0, 0x7f082453

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(IZZ)Landroid/support/v4/app/DialogFragment;

    move-result-object v0

    .line 1513891
    iget-object v1, p0, LX/9ac;->b:LX/0gc;

    const-string v2, "progress_dialog"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1513892
    new-instance v9, LX/9aZ;

    invoke-direct {v9, p0, v0, p1, p2}, LX/9aZ;-><init>(LX/9ac;Landroid/support/v4/app/DialogFragment;Landroid/app/Activity;Ljava/lang/String;)V

    .line 1513893
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f082451

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1513894
    :goto_0
    if-nez p4, :cond_1

    const-string v6, ""

    .line 1513895
    :goto_1
    invoke-virtual/range {p5 .. p5}, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->a()Ljava/lang/String;

    move-result-object v7

    .line 1513896
    iget-object v10, p0, LX/9ac;->d:LX/1Ck;

    const/16 v0, 0xa

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    new-instance v0, LX/9aa;

    move-object v1, p0

    move-object/from16 v2, p6

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, LX/9aa;-><init>(LX/9ac;LX/9ab;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerTargetData;)V

    invoke-virtual {v10, v11, v0, v9}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1513897
    return-void

    :cond_0
    move-object v3, p2

    .line 1513898
    goto :goto_0

    .line 1513899
    :cond_1
    invoke-virtual/range {p4 .. p4}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v6

    goto :goto_1
.end method

.method public final a(Landroid/app/Activity;Z)V
    .locals 5

    .prologue
    .line 1513900
    if-nez p2, :cond_0

    .line 1513901
    invoke-static {p0, p1}, LX/9ac;->a$redex0(LX/9ac;Landroid/app/Activity;)V

    .line 1513902
    :goto_0
    return-void

    .line 1513903
    :cond_0
    const v0, 0x7f082452

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1513904
    iget-object v1, p0, LX/9ac;->a:LX/9af;

    sget-object v2, LX/9ad;->CANCELLATION:LX/9ad;

    .line 1513905
    sget-object v3, LX/9ae;->ALBUM_CREATOR_DIALOG_SHOWN:LX/9ae;

    invoke-static {v3}, LX/9af;->a(LX/9ae;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "message"

    invoke-virtual {v2}, LX/9ad;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v3, v4, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-static {v1, v3}, LX/9af;->a(LX/9af;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1513906
    new-instance v1, LX/0ju;

    invoke-direct {v1, p1}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v2, 0x7f080017

    invoke-virtual {p1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    const v1, 0x7f080017

    new-instance v2, LX/9aY;

    invoke-direct {v2, p0}, LX/9aY;-><init>(LX/9ac;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f081436

    new-instance v2, LX/9aX;

    invoke-direct {v2, p0, p1}, LX/9aX;-><init>(LX/9ac;Landroid/app/Activity;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    goto :goto_0
.end method
