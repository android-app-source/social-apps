.class public LX/AJS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/7gT;

.field public final b:LX/AJK;

.field public final c:Landroid/app/Activity;

.field public final d:LX/0he;

.field public final e:LX/0hg;

.field public f:Ljava/lang/String;

.field public g:Z


# direct methods
.method public constructor <init>(Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;Landroid/app/Activity;LX/7gT;LX/AJK;LX/0he;LX/0hg;)V
    .locals 0
    .param p1    # Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/app/Activity;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1661119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1661120
    iput-object p3, p0, LX/AJS;->a:LX/7gT;

    .line 1661121
    iput-object p4, p0, LX/AJS;->b:LX/AJK;

    .line 1661122
    iput-object p2, p0, LX/AJS;->c:Landroid/app/Activity;

    .line 1661123
    iput-object p5, p0, LX/AJS;->d:LX/0he;

    .line 1661124
    iput-object p6, p0, LX/AJS;->e:LX/0hg;

    .line 1661125
    invoke-virtual {p1}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->getSessionId()Ljava/lang/String;

    move-result-object p2

    .line 1661126
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p3

    if-eqz p3, :cond_0

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object p3

    invoke-virtual {p3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p2

    :cond_0
    iput-object p2, p0, LX/AJS;->f:Ljava/lang/String;

    .line 1661127
    invoke-virtual {p1}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->isVideo()Z

    move-result p2

    iput-boolean p2, p0, LX/AJS;->g:Z

    .line 1661128
    iget-object p2, p0, LX/AJS;->b:LX/AJK;

    iget-object p3, p0, LX/AJS;->f:Ljava/lang/String;

    .line 1661129
    iput-object p3, p2, LX/AJK;->b:Ljava/lang/String;

    .line 1661130
    iget-object p2, p0, LX/AJS;->b:LX/AJK;

    invoke-virtual {p1}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->getPromptId()Ljava/lang/String;

    move-result-object p3

    .line 1661131
    iput-object p3, p2, LX/AJK;->c:Ljava/lang/String;

    .line 1661132
    iget-object p2, p0, LX/AJS;->a:LX/7gT;

    iget-object p3, p0, LX/AJS;->f:Ljava/lang/String;

    .line 1661133
    iput-object p3, p2, LX/7gT;->f:Ljava/lang/String;

    .line 1661134
    iget-object p2, p0, LX/AJS;->a:LX/7gT;

    invoke-virtual {p1}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->getInspirationGroupSessionId()Ljava/lang/String;

    move-result-object p3

    .line 1661135
    iput-object p3, p2, LX/7gT;->d:Ljava/lang/String;

    .line 1661136
    iget-object p2, p0, LX/AJS;->a:LX/7gT;

    invoke-virtual {p1}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->getPromptId()Ljava/lang/String;

    move-result-object p3

    .line 1661137
    iput-object p3, p2, LX/7gT;->e:Ljava/lang/String;

    .line 1661138
    iget-object p3, p0, LX/AJS;->a:LX/7gT;

    iget-boolean p2, p0, LX/AJS;->g:Z

    if-eqz p2, :cond_1

    const-string p2, "video"

    .line 1661139
    :goto_0
    iput-object p2, p3, LX/7gT;->g:Ljava/lang/String;

    .line 1661140
    iget-object p2, p0, LX/AJS;->a:LX/7gT;

    invoke-virtual {p1}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->getMediaContentId()Ljava/lang/String;

    move-result-object p3

    .line 1661141
    iput-object p3, p2, LX/7gT;->h:Ljava/lang/String;

    .line 1661142
    iget-object p2, p0, LX/AJS;->e:LX/0hg;

    invoke-virtual {p1}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->getInspirationGroupSessionId()Ljava/lang/String;

    move-result-object p3

    .line 1661143
    iput-object p3, p2, LX/0hg;->d:Ljava/lang/String;

    .line 1661144
    iget-object p2, p0, LX/AJS;->e:LX/0hg;

    invoke-virtual {p1}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->getPromptId()Ljava/lang/String;

    move-result-object p3

    .line 1661145
    iput-object p3, p2, LX/0hg;->b:Ljava/lang/String;

    .line 1661146
    iget-object p2, p0, LX/AJS;->e:LX/0hg;

    iget-object p3, p0, LX/AJS;->f:Ljava/lang/String;

    .line 1661147
    iput-object p3, p2, LX/0hg;->c:Ljava/lang/String;

    .line 1661148
    return-void

    .line 1661149
    :cond_1
    const-string p2, "photo"

    goto :goto_0
.end method

.method public static a(LX/AJS;LX/0Px;ZZ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/model/AudienceControlData;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    .line 1661150
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1661151
    iget-object v0, p0, LX/AJS;->a:LX/7gT;

    sget-object v1, LX/7gR;->SEND_DIRECT:LX/7gR;

    invoke-virtual {v0, v1, p1}, LX/7gT;->a(LX/7gR;LX/0Px;)V

    .line 1661152
    if-nez p2, :cond_0

    if-nez p3, :cond_0

    .line 1661153
    iget-object v0, p0, LX/AJS;->e:LX/0hg;

    .line 1661154
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/0hg;->g:Z

    .line 1661155
    const v1, 0xbc0003

    invoke-static {v0}, LX/0hg;->h(LX/0hg;)Ljava/util/Map;

    move-result-object p1

    invoke-static {v0, v1, p1}, LX/0hg;->a(LX/0hg;ILjava/util/Map;)V

    .line 1661156
    :cond_0
    if-eqz p2, :cond_1

    .line 1661157
    iget-object v0, p0, LX/AJS;->a:LX/7gT;

    sget-object v1, LX/7gR;->SEND_STORY:LX/7gR;

    invoke-virtual {v0, v1}, LX/7gT;->a(LX/7gR;)V

    .line 1661158
    :cond_1
    if-eqz p3, :cond_2

    .line 1661159
    iget-object v0, p0, LX/AJS;->a:LX/7gT;

    sget-object v1, LX/7gR;->SEND_NEWS_FEED:LX/7gR;

    invoke-virtual {v0, v1}, LX/7gT;->a(LX/7gR;)V

    .line 1661160
    iget-object v0, p0, LX/AJS;->e:LX/0hg;

    .line 1661161
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/0hg;->f:Z

    .line 1661162
    const v1, 0xbc0002

    invoke-static {v0}, LX/0hg;->h(LX/0hg;)Ljava/util/Map;

    move-result-object p0

    invoke-static {v0, v1, p0}, LX/0hg;->a(LX/0hg;ILjava/util/Map;)V

    .line 1661163
    :cond_2
    return-void
.end method

.method public static b(LX/0Px;Lcom/facebook/privacy/model/SelectablePrivacyData;ZZ)Lcom/facebook/audience/model/SharesheetSelectedAudience;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/model/AudienceControlData;",
            ">;",
            "Lcom/facebook/privacy/model/SelectablePrivacyData;",
            "ZZ)",
            "Lcom/facebook/audience/model/SharesheetSelectedAudience;"
        }
    .end annotation

    .prologue
    .line 1661164
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1661165
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/model/AudienceControlData;

    .line 1661166
    invoke-virtual {v0}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->a(J)LX/5Rc;

    move-result-object v4

    invoke-virtual {v0}, Lcom/facebook/audience/model/AudienceControlData;->getName()Ljava/lang/String;

    move-result-object v5

    .line 1661167
    iput-object v5, v4, LX/5Rc;->b:Ljava/lang/String;

    .line 1661168
    move-object v4, v4

    .line 1661169
    invoke-virtual {v0}, Lcom/facebook/audience/model/AudienceControlData;->getProfileUri()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Lcom/facebook/audience/model/AudienceControlData;->getProfileUri()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1661170
    :goto_1
    iput-object v0, v4, LX/5Rc;->c:Ljava/lang/String;

    .line 1661171
    move-object v0, v4

    .line 1661172
    invoke-virtual {v0}, LX/5Rc;->a()Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1661173
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1661174
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 1661175
    :cond_1
    invoke-static {}, Lcom/facebook/audience/model/SharesheetSelectedAudience;->newBuilder()LX/7h4;

    move-result-object v0

    .line 1661176
    iput-boolean p2, v0, LX/7h4;->c:Z

    .line 1661177
    move-object v0, v0

    .line 1661178
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1661179
    iput-object v1, v0, LX/7h4;->a:LX/0Px;

    .line 1661180
    move-object v0, v0

    .line 1661181
    if-eqz p3, :cond_2

    .line 1661182
    iput-object p1, v0, LX/7h4;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1661183
    :cond_2
    invoke-virtual {v0}, LX/7h4;->a()Lcom/facebook/audience/model/SharesheetSelectedAudience;

    move-result-object v0

    return-object v0
.end method
