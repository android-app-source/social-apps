.class public final LX/A7k;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field public final synthetic a:Landroid/animation/AnimatorSet;

.field public final synthetic b:LX/A7l;

.field public final synthetic c:LX/A7m;


# direct methods
.method public constructor <init>(LX/A7m;Landroid/animation/AnimatorSet;LX/A7l;)V
    .locals 0

    .prologue
    .line 1626717
    iput-object p1, p0, LX/A7k;->c:LX/A7m;

    iput-object p2, p0, LX/A7k;->a:Landroid/animation/AnimatorSet;

    iput-object p3, p0, LX/A7k;->b:LX/A7l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 1626718
    return-void
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 1626719
    return-void
.end method

.method public final onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 4

    .prologue
    .line 1626720
    iget-object v0, p0, LX/A7k;->a:Landroid/animation/AnimatorSet;

    iget-object v1, p0, LX/A7k;->b:LX/A7l;

    iget v1, v1, LX/A7l;->f:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setStartDelay(J)V

    .line 1626721
    return-void
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 1626722
    return-void
.end method
