.class public final LX/APo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field public final synthetic a:LX/0il;

.field public final synthetic b:LX/APp;


# direct methods
.method public constructor <init>(LX/APp;LX/0il;)V
    .locals 0

    .prologue
    .line 1670449
    iput-object p1, p0, LX/APo;->b:LX/APp;

    iput-object p2, p0, LX/APo;->a:LX/0il;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2

    .prologue
    .line 1670450
    iget-object v0, p0, LX/APo;->a:LX/0il;

    check-cast v0, LX/0im;

    invoke-interface {v0}, LX/0im;->c()LX/0jJ;

    move-result-object v0

    sget-object v1, LX/APp;->a:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    .line 1670451
    iget-object v1, v0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v1}, LX/0Sh;->a()V

    .line 1670452
    iget-object v1, v0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->isFeedOnlyPost()Z

    move-result v1

    if-eq v1, p2, :cond_1

    .line 1670453
    iget-object v1, v0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v1, :cond_0

    .line 1670454
    iget-object v1, v0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v1

    iput-object v1, v0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 1670455
    :cond_0
    iget-object v1, v0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v1, p2}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setIsFeedOnlyPost(Z)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 1670456
    iget-object v1, v0, LX/0jL;->a:LX/0cA;

    sget-object p0, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v1, p0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1670457
    :cond_1
    move-object v0, v0

    .line 1670458
    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 1670459
    return-void
.end method
