.class public LX/8iF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile i:LX/8iF;


# instance fields
.field public final a:Landroid/content/res/Resources;

.field public final b:Landroid/content/Context;

.field private final c:LX/0rq;

.field public final d:LX/0ad;

.field public e:I

.field public f:I

.field public g:I

.field public h:I


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Landroid/content/Context;LX/0rq;LX/0ad;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1391335
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1391336
    iput v0, p0, LX/8iF;->e:I

    .line 1391337
    iput v0, p0, LX/8iF;->f:I

    .line 1391338
    iput v0, p0, LX/8iF;->g:I

    .line 1391339
    iput v0, p0, LX/8iF;->h:I

    .line 1391340
    iput-object p1, p0, LX/8iF;->a:Landroid/content/res/Resources;

    .line 1391341
    iput-object p2, p0, LX/8iF;->b:Landroid/content/Context;

    .line 1391342
    iput-object p3, p0, LX/8iF;->c:LX/0rq;

    .line 1391343
    iput-object p4, p0, LX/8iF;->d:LX/0ad;

    .line 1391344
    iget-object v0, p0, LX/8iF;->d:LX/0ad;

    sget-object v1, LX/0c0;->Cached:LX/0c0;

    sget-object v2, LX/0c1;->Off:LX/0c1;

    sget-short p1, LX/100;->X:S

    const/4 p2, 0x0

    invoke-interface {v0, v1, v2, p1, p2}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8iF;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b14af

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1391345
    :goto_0
    iput v0, p0, LX/8iF;->g:I

    .line 1391346
    iget-object v0, p0, LX/8iF;->b:Landroid/content/Context;

    iget-object v1, p0, LX/8iF;->d:LX/0ad;

    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-object p1, LX/0c1;->Off:LX/0c1;

    sget p2, LX/100;->V:I

    iget-object p3, p0, LX/8iF;->a:Landroid/content/res/Resources;

    const p4, 0x7f0b14b0

    invoke-virtual {p3, p4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p3

    float-to-int p3, p3

    invoke-interface {v1, v2, p1, p2, p3}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v1

    int-to-float v1, v1

    invoke-static {v0, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, LX/8iF;->h:I

    .line 1391347
    iget-object v0, p0, LX/8iF;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b14ad

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1391348
    iget-object v1, p0, LX/8iF;->a:Landroid/content/res/Resources;

    const v2, 0x7f0b0099

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, LX/8iF;->e:I

    .line 1391349
    iget-object v1, p0, LX/8iF;->a:Landroid/content/res/Resources;

    const v2, 0x7f0b009b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    div-int/lit8 v1, v1, 0x3

    mul-int/lit8 v0, v0, 0x2

    sub-int v0, v1, v0

    iput v0, p0, LX/8iF;->f:I

    .line 1391350
    return-void

    .line 1391351
    :cond_0
    iget-object v0, p0, LX/8iF;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b009a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/8iF;
    .locals 7

    .prologue
    .line 1391359
    sget-object v0, LX/8iF;->i:LX/8iF;

    if-nez v0, :cond_1

    .line 1391360
    const-class v1, LX/8iF;

    monitor-enter v1

    .line 1391361
    :try_start_0
    sget-object v0, LX/8iF;->i:LX/8iF;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1391362
    if-eqz v2, :cond_0

    .line 1391363
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1391364
    new-instance p0, LX/8iF;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v5

    check-cast v5, LX/0rq;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-direct {p0, v3, v4, v5, v6}, LX/8iF;-><init>(Landroid/content/res/Resources;Landroid/content/Context;LX/0rq;LX/0ad;)V

    .line 1391365
    move-object v0, p0

    .line 1391366
    sput-object v0, LX/8iF;->i:LX/8iF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1391367
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1391368
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1391369
    :cond_1
    sget-object v0, LX/8iF;->i:LX/8iF;

    return-object v0

    .line 1391370
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1391371
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1391358
    iget v0, p0, LX/8iF;->g:I

    return v0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 1391357
    iget-object v0, p0, LX/8iF;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b14ae

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 1391372
    iget v0, p0, LX/8iF;->e:I

    return v0
.end method

.method public final e()F
    .locals 4

    .prologue
    .line 1391354
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 1391355
    iget-object v1, p0, LX/8iF;->a:Landroid/content/res/Resources;

    const v2, 0x7f0b14ac

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    .line 1391356
    invoke-virtual {v0}, Landroid/util/TypedValue;->getFloat()F

    move-result v0

    return v0
.end method

.method public final f()Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 1391353
    iget-object v0, p0, LX/8iF;->c:LX/0rq;

    invoke-virtual {v0}, LX/0rq;->f()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, LX/8iF;->e()F

    move-result v1

    div-float/2addr v0, v1

    float-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 1391352
    iget v0, p0, LX/8iF;->h:I

    return v0
.end method
