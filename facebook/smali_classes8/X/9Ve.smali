.class public LX/9Ve;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;

.field public final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1499517
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1499518
    new-instance v0, LX/026;

    invoke-direct {v0}, LX/026;-><init>()V

    iput-object v0, p0, LX/9Ve;->b:Ljava/util/Map;

    .line 1499519
    iput-object p1, p0, LX/9Ve;->a:LX/0Zb;

    .line 1499520
    return-void
.end method

.method public static a(LX/9Ve;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1499521
    const-string v0, "graphql_token"

    invoke-virtual {p1, v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1499522
    iget-object v0, p0, LX/9Ve;->b:Ljava/util/Map;

    invoke-virtual {p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1499523
    iget-object v0, p0, LX/9Ve;->a:LX/0Zb;

    invoke-interface {v0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1499524
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1499525
    iget-object v0, p0, LX/9Ve;->a:LX/0Zb;

    const-string v1, "negativefeedback_start_flow"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 1499526
    iget-object v1, p0, LX/9Ve;->b:Ljava/util/Map;

    const-string v2, "negative_feedback_location"

    invoke-interface {v1, v2, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1499527
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1499528
    const-string v1, "graphql_token"

    invoke-virtual {v0, v1, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1499529
    iget-object v1, p0, LX/9Ve;->b:Ljava/util/Map;

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/util/Map;)LX/0oG;

    .line 1499530
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 1499531
    :cond_0
    return-void
.end method
