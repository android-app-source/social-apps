.class public LX/APY;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0io;",
        ":",
        "LX/0j0;",
        ":",
        "LX/0j2;",
        ":",
        "LX/0j8;",
        ":",
        "LX/0iq;",
        ":",
        "LX/0j5;",
        ":",
        "LX/0jB;",
        ":",
        "LX/0jD;",
        ":",
        "LX/0j6;",
        ":",
        "LX/0ip;",
        "Services::",
        "LX/0il",
        "<TModelData;>;>",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field public final b:LX/0ad;

.field private final c:LX/1RW;

.field public final d:LX/ATy;

.field public e:Z

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:I

.field public k:I


# direct methods
.method public constructor <init>(LX/0ad;LX/0il;LX/1RW;LX/ATy;)V
    .locals 2
    .param p2    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ad;",
            "TServices;",
            "LX/1RW;",
            "LX/ATy;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1670218
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1670219
    iput-object p1, p0, LX/APY;->b:LX/0ad;

    .line 1670220
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/APY;->a:Ljava/lang/ref/WeakReference;

    .line 1670221
    iput-object p3, p0, LX/APY;->c:LX/1RW;

    .line 1670222
    iput-object p4, p0, LX/APY;->d:LX/ATy;

    .line 1670223
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/APY;->e:Z

    .line 1670224
    return-void
.end method

.method public static a$redex0(LX/APY;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1670225
    iget-object v0, p0, LX/APY;->c:LX/1RW;

    const-string v2, "inapp"

    iget-object v3, p0, LX/APY;->i:Ljava/lang/String;

    iget v4, p0, LX/APY;->j:I

    iget v5, p0, LX/APY;->k:I

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/1RW;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1670226
    return-void
.end method


# virtual methods
.method public final a(LX/8K5;)V
    .locals 12

    .prologue
    .line 1670227
    iget-object v0, p0, LX/APY;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, LX/0il;

    .line 1670228
    iget-object v0, p0, LX/APY;->d:LX/ATy;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0io;

    check-cast v2, LX/0j0;

    invoke-interface {v2}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0io;

    check-cast v3, LX/0j2;

    invoke-interface {v3}, LX/0j2;->getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0io;

    check-cast v4, LX/0jD;

    invoke-interface {v4}, LX/0jD;->getTaggedUsers()LX/0Px;

    move-result-object v4

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0io;

    check-cast v5, LX/0ip;

    invoke-interface {v5}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v5

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0io;

    check-cast v6, LX/0iq;

    invoke-interface {v6}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v6

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0io;

    check-cast v7, LX/0j6;

    invoke-interface {v7}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v7

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/0io;

    check-cast v8, LX/0j8;

    invoke-interface {v8}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v8

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/0io;

    check-cast v9, LX/0jB;

    invoke-interface {v9}, LX/0jB;->getReferencedStickerData()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v9

    new-instance v11, LX/ATv;

    invoke-direct {v11}, LX/ATv;-><init>()V

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/0io;

    check-cast v10, LX/0j5;

    invoke-interface {v10}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v10

    .line 1670229
    iput-object v10, v11, LX/ATv;->a:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    .line 1670230
    move-object v10, v11

    .line 1670231
    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    .line 1670232
    iput-object v1, v10, LX/ATv;->b:LX/0Px;

    .line 1670233
    move-object v1, v10

    .line 1670234
    invoke-virtual {v1}, LX/ATv;->a()LX/ATw;

    move-result-object v10

    move-object v1, p1

    invoke-virtual/range {v0 .. v10}, LX/ATy;->a(LX/8K5;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLTextWithEntities;LX/0Px;Lcom/facebook/composer/minutiae/model/MinutiaeObject;Lcom/facebook/composer/privacy/model/ComposerPrivacyData;Lcom/facebook/ipc/composer/intent/ComposerTargetData;Lcom/facebook/ipc/composer/model/ComposerLocationInfo;Lcom/facebook/ipc/composer/model/ComposerStickerData;LX/ATw;)V

    .line 1670235
    return-void
.end method
