.class public final LX/A14;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1608685
    const-class v1, Lcom/facebook/search/protocol/feedstory/FetchGraphSearchResultDataGraphQLModels$FBGraphSearchQueryModel;

    const v0, 0x4f3b66cd

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FBGraphSearchQuery"

    const-string v6, "68c9c0aeccad377846e2d165d6c72a99"

    const-string v7, "graph_search_query"

    const-string v8, "10155244967006729"

    const-string v9, "10155263677951729"

    .line 1608686
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1608687
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1608688
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1608689
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1608690
    sparse-switch v0, :sswitch_data_0

    .line 1608691
    :goto_0
    return-object p1

    .line 1608692
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1608693
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1608694
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1608695
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1608696
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1608697
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 1608698
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 1608699
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 1608700
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 1608701
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 1608702
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 1608703
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 1608704
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 1608705
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x6a24640d -> :sswitch_d
        -0x6326fdb3 -> :sswitch_7
        -0x55ff6f9b -> :sswitch_8
        -0x41a91745 -> :sswitch_b
        -0x32ef5c05 -> :sswitch_5
        -0x1b87b280 -> :sswitch_6
        0x58705dc -> :sswitch_1
        0x5a7510f -> :sswitch_2
        0x66f18c8 -> :sswitch_c
        0xa1fa812 -> :sswitch_0
        0x1fbc4ddf -> :sswitch_3
        0x24991595 -> :sswitch_4
        0x31140662 -> :sswitch_a
        0x73a026b5 -> :sswitch_9
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1608706
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 1608707
    :goto_1
    return v0

    .line 1608708
    :pswitch_0
    const-string v2, "10"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 1608709
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x61f
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
