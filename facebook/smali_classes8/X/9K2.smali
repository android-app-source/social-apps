.class public final LX/9K2;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 1466760
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1466761
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1466762
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1466763
    const/4 v2, 0x0

    .line 1466764
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 1466765
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1466766
    :goto_1
    move v1, v2

    .line 1466767
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1466768
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 1466769
    :cond_1
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1466770
    :cond_2
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1466771
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1466772
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1466773
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 1466774
    const-string v4, "node"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1466775
    const/4 v3, 0x0

    .line 1466776
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_8

    .line 1466777
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1466778
    :goto_3
    move v1, v3

    .line 1466779
    goto :goto_2

    .line 1466780
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1466781
    invoke-virtual {p1, v2, v1}, LX/186;->b(II)V

    .line 1466782
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_1

    :cond_4
    move v1, v2

    goto :goto_2

    .line 1466783
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1466784
    :cond_6
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_7

    .line 1466785
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1466786
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1466787
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 1466788
    const-string v5, "marketplace_cross_post_setting"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1466789
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1466790
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v6, :cond_11

    .line 1466791
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1466792
    :goto_5
    move v1, v4

    .line 1466793
    goto :goto_4

    .line 1466794
    :cond_7
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1466795
    invoke-virtual {p1, v3, v1}, LX/186;->b(II)V

    .line 1466796
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_3

    :cond_8
    move v1, v3

    goto :goto_4

    .line 1466797
    :cond_9
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_d

    .line 1466798
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1466799
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1466800
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_9

    if-eqz v11, :cond_9

    .line 1466801
    const-string v12, "is_enabled"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 1466802
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v7

    move v10, v7

    move v7, v5

    goto :goto_6

    .line 1466803
    :cond_a
    const-string v12, "should_show_intercept"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_b

    .line 1466804
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v6

    move v9, v6

    move v6, v5

    goto :goto_6

    .line 1466805
    :cond_b
    const-string v12, "should_show_nux"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_c

    .line 1466806
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v1

    move v8, v1

    move v1, v5

    goto :goto_6

    .line 1466807
    :cond_c
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_6

    .line 1466808
    :cond_d
    const/4 v11, 0x3

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1466809
    if-eqz v7, :cond_e

    .line 1466810
    invoke-virtual {p1, v4, v10}, LX/186;->a(IZ)V

    .line 1466811
    :cond_e
    if-eqz v6, :cond_f

    .line 1466812
    invoke-virtual {p1, v5, v9}, LX/186;->a(IZ)V

    .line 1466813
    :cond_f
    if-eqz v1, :cond_10

    .line 1466814
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v8}, LX/186;->a(IZ)V

    .line 1466815
    :cond_10
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto :goto_5

    :cond_11
    move v1, v4

    move v6, v4

    move v7, v4

    move v8, v4

    move v9, v4

    move v10, v4

    goto :goto_6
.end method
