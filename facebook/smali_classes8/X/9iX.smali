.class public final enum LX/9iX;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9iX;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9iX;

.field public static final enum IMAGE:LX/9iX;

.field public static final enum VIDEO:LX/9iX;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1527954
    new-instance v0, LX/9iX;

    const-string v1, "IMAGE"

    invoke-direct {v0, v1, v2}, LX/9iX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9iX;->IMAGE:LX/9iX;

    .line 1527955
    new-instance v0, LX/9iX;

    const-string v1, "VIDEO"

    invoke-direct {v0, v1, v3}, LX/9iX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9iX;->VIDEO:LX/9iX;

    .line 1527956
    const/4 v0, 0x2

    new-array v0, v0, [LX/9iX;

    sget-object v1, LX/9iX;->IMAGE:LX/9iX;

    aput-object v1, v0, v2

    sget-object v1, LX/9iX;->VIDEO:LX/9iX;

    aput-object v1, v0, v3

    sput-object v0, LX/9iX;->$VALUES:[LX/9iX;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1527957
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9iX;
    .locals 1

    .prologue
    .line 1527953
    const-class v0, LX/9iX;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9iX;

    return-object v0
.end method

.method public static values()[LX/9iX;
    .locals 1

    .prologue
    .line 1527952
    sget-object v0, LX/9iX;->$VALUES:[LX/9iX;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9iX;

    return-object v0
.end method
