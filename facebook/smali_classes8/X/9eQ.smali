.class public LX/9eQ;
.super Landroid/widget/FrameLayout;
.source ""


# instance fields
.field public a:LX/9hP;

.field public b:Landroid/graphics/drawable/Drawable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:LX/9eP;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1519597
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 1519598
    new-instance v0, LX/9eP;

    invoke-direct {v0, p0}, LX/9eP;-><init>(LX/9eQ;)V

    iput-object v0, p0, LX/9eQ;->c:LX/9eP;

    .line 1519599
    invoke-virtual {p0}, LX/9eQ;->a()V

    .line 1519600
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1519601
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1519602
    new-instance v0, LX/9eP;

    invoke-direct {v0, p0}, LX/9eP;-><init>(LX/9eQ;)V

    iput-object v0, p0, LX/9eQ;->c:LX/9eP;

    .line 1519603
    invoke-virtual {p0}, LX/9eQ;->a()V

    .line 1519604
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1519605
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1519606
    new-instance v0, LX/9eP;

    invoke-direct {v0, p0}, LX/9eP;-><init>(LX/9eQ;)V

    iput-object v0, p0, LX/9eQ;->c:LX/9eP;

    .line 1519607
    invoke-virtual {p0}, LX/9eQ;->a()V

    .line 1519608
    return-void
.end method


# virtual methods
.method public final a(LX/9hP;)LX/9hP;
    .locals 1

    .prologue
    .line 1519609
    iget-object v0, p0, LX/9eQ;->a:LX/9hP;

    .line 1519610
    iput-object p1, p0, LX/9eQ;->a:LX/9hP;

    .line 1519611
    invoke-virtual {p0, v0}, LX/9eQ;->b(LX/9hP;)V

    .line 1519612
    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 1519613
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/9eQ;->setWillNotDraw(Z)V

    .line 1519614
    return-void
.end method

.method public a(LX/9hP;LX/9hP;)V
    .locals 0

    .prologue
    .line 1519615
    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 1519616
    return-void
.end method

.method public b(LX/9hP;)V
    .locals 0
    .param p1    # LX/9hP;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1519617
    return-void
.end method

.method public setDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1519618
    invoke-virtual {p0, p1}, LX/9eQ;->a(Landroid/graphics/drawable/Drawable;)V

    .line 1519619
    iget-object v0, p0, LX/9eQ;->b:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_0

    .line 1519620
    :goto_0
    return-void

    .line 1519621
    :cond_0
    iget-object v0, p0, LX/9eQ;->b:Landroid/graphics/drawable/Drawable;

    instance-of v0, v0, Ljava/io/Closeable;

    if-eqz v0, :cond_1

    .line 1519622
    iget-object v0, p0, LX/9eQ;->b:Landroid/graphics/drawable/Drawable;

    check-cast v0, Ljava/io/Closeable;

    invoke-static {v0}, LX/1pX;->a(Ljava/io/Closeable;)V

    .line 1519623
    :cond_1
    iput-object p1, p0, LX/9eQ;->b:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method
