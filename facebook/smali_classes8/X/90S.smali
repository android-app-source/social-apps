.class public final LX/90S;
.super LX/3sJ;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;)V
    .locals 0

    .prologue
    .line 1428861
    iput-object p1, p0, LX/90S;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;

    invoke-direct {p0}, LX/3sJ;-><init>()V

    return-void
.end method


# virtual methods
.method public final B_(I)V
    .locals 3

    .prologue
    .line 1428862
    sget-object v0, LX/93G;->STICKERS_TAB:LX/93G;

    invoke-virtual {v0}, LX/93G;->ordinal()I

    move-result v0

    if-ne p1, v0, :cond_0

    iget-object v0, p0, LX/90S;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;->p:Landroid/view/inputmethod/InputMethodManager;

    if-eqz v0, :cond_0

    .line 1428863
    iget-object v0, p0, LX/90S;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;->p:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, LX/90S;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;

    iget-object v1, v1, Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;->q:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1428864
    :cond_0
    invoke-static {}, LX/93G;->values()[LX/93G;

    move-result-object v0

    aget-object v0, v0, p1

    .line 1428865
    iget-object v1, p0, LX/90S;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;

    iget-object v1, v1, Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;->r:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    iget-object v2, p0, LX/90S;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;

    invoke-virtual {v2}, Lcom/facebook/composer/minutiae/activity/MinutiaeTabbedPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v0, v0, LX/93G;->mTitleBarResource:I

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(Ljava/lang/String;)V

    .line 1428866
    return-void
.end method
