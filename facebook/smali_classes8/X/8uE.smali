.class public LX/8uE;
.super LX/8uC;
.source ""


# instance fields
.field private final d:F

.field private final e:I

.field private final f:I


# direct methods
.method public constructor <init>(IIII)V
    .locals 6
    .param p4    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 1414589
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, LX/8uE;-><init>(IIIILandroid/util/DisplayMetrics;)V

    .line 1414590
    return-void
.end method

.method private constructor <init>(IIIILandroid/util/DisplayMetrics;)V
    .locals 2
    .param p4    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 1414591
    add-int/lit8 v0, p1, 0x1

    invoke-direct {p0, v0, p5}, LX/8uC;-><init>(ILandroid/util/DisplayMetrics;)V

    .line 1414592
    iput p2, p0, LX/8uE;->e:I

    .line 1414593
    iput p3, p0, LX/8uE;->f:I

    .line 1414594
    iput p4, p0, LX/8uE;->b:I

    .line 1414595
    const/4 v0, 0x2

    const/high16 v1, 0x41b80000    # 23.0f

    invoke-static {v0, v1, p5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    iput v0, p0, LX/8uE;->d:F

    .line 1414596
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1414597
    iget v0, p0, LX/8uE;->e:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 1414598
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, LX/8uE;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1414599
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, LX/8uE;->f:I

    add-int/lit8 v1, v1, 0x60

    int-to-char v1, v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final drawLeadingMargin(Landroid/graphics/Canvas;Landroid/graphics/Paint;IIIIILjava/lang/CharSequence;IIZLandroid/text/Layout;)V
    .locals 5

    .prologue
    .line 1414600
    check-cast p8, Landroid/text/Spanned;

    invoke-interface {p8, p0}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v0

    if-eq v0, p9, :cond_0

    .line 1414601
    :goto_0
    return-void

    .line 1414602
    :cond_0
    invoke-virtual {p0, p2}, LX/8u8;->a(Landroid/graphics/Paint;)V

    .line 1414603
    iget v0, p0, LX/8uE;->f:I

    const/16 v1, 0xa

    if-ge v0, v1, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, LX/8uE;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    int-to-float v1, p3

    int-to-float v2, p4

    iget v3, p0, LX/8u8;->a:F

    iget v4, p0, LX/8uE;->d:F

    sub-float/2addr v3, v4

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    int-to-float v2, p6

    iget-object v3, p0, LX/8u8;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, LX/8uE;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
