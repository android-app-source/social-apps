.class public LX/A70;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "LX/A70;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:D


# direct methods
.method public constructor <init>(Ljava/lang/String;D)V
    .locals 0

    .prologue
    .line 1625329
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1625330
    iput-object p1, p0, LX/A70;->a:Ljava/lang/String;

    .line 1625331
    iput-wide p2, p0, LX/A70;->b:D

    .line 1625332
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1625333
    iget-object v0, p0, LX/A70;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/Double;
    .locals 2

    .prologue
    .line 1625334
    iget-wide v0, p0, LX/A70;->b:D

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method public final compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 1625335
    check-cast p1, LX/A70;

    .line 1625336
    invoke-virtual {p0}, LX/A70;->b()Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {p1}, LX/A70;->b()Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Double;->compareTo(Ljava/lang/Double;)I

    move-result v0

    return v0
.end method
