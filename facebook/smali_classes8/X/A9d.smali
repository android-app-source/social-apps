.class public final LX/A9d;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 1635340
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_3

    .line 1635341
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1635342
    :goto_0
    return v1

    .line 1635343
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1635344
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_2

    .line 1635345
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1635346
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1635347
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 1635348
    const-string v3, "link_data"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1635349
    const/4 v2, 0x0

    .line 1635350
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_d

    .line 1635351
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1635352
    :goto_2
    move v0, v2

    .line 1635353
    goto :goto_1

    .line 1635354
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1635355
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1635356
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    .line 1635357
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1635358
    :cond_5
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_c

    .line 1635359
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1635360
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1635361
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_5

    if-eqz v9, :cond_5

    .line 1635362
    const-string v10, "call_to_action"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 1635363
    const/4 v9, 0x0

    .line 1635364
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v8, v10, :cond_11

    .line 1635365
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1635366
    :goto_4
    move v8, v9

    .line 1635367
    goto :goto_3

    .line 1635368
    :cond_6
    const-string v10, "description"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 1635369
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_3

    .line 1635370
    :cond_7
    const-string v10, "image_hash"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 1635371
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_3

    .line 1635372
    :cond_8
    const-string v10, "link"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 1635373
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_3

    .line 1635374
    :cond_9
    const-string v10, "message"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 1635375
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_3

    .line 1635376
    :cond_a
    const-string v10, "name"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 1635377
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_3

    .line 1635378
    :cond_b
    const-string v10, "picture"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 1635379
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_3

    .line 1635380
    :cond_c
    const/4 v9, 0x7

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1635381
    invoke-virtual {p1, v2, v8}, LX/186;->b(II)V

    .line 1635382
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v7}, LX/186;->b(II)V

    .line 1635383
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v6}, LX/186;->b(II)V

    .line 1635384
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v5}, LX/186;->b(II)V

    .line 1635385
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v4}, LX/186;->b(II)V

    .line 1635386
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v3}, LX/186;->b(II)V

    .line 1635387
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1635388
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_2

    :cond_d
    move v0, v2

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    goto/16 :goto_3

    .line 1635389
    :cond_e
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1635390
    :cond_f
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_10

    .line 1635391
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1635392
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1635393
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_f

    if-eqz v10, :cond_f

    .line 1635394
    const-string v11, "call_to_action_type"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_e

    .line 1635395
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_5

    .line 1635396
    :cond_10
    const/4 v10, 0x1

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1635397
    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 1635398
    invoke-virtual {p1}, LX/186;->d()I

    move-result v9

    goto/16 :goto_4

    :cond_11
    move v8, v9

    goto :goto_5
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1635399
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1635400
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1635401
    if-eqz v0, :cond_8

    .line 1635402
    const-string v1, "link_data"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635403
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1635404
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1635405
    if-eqz v1, :cond_1

    .line 1635406
    const-string p1, "call_to_action"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635407
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1635408
    const/4 p1, 0x0

    invoke-virtual {p0, v1, p1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p1

    .line 1635409
    if-eqz p1, :cond_0

    .line 1635410
    const-string p3, "call_to_action_type"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635411
    invoke-virtual {p2, p1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1635412
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1635413
    :cond_1
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1635414
    if-eqz v1, :cond_2

    .line 1635415
    const-string p1, "description"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635416
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1635417
    :cond_2
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1635418
    if-eqz v1, :cond_3

    .line 1635419
    const-string p1, "image_hash"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635420
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1635421
    :cond_3
    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1635422
    if-eqz v1, :cond_4

    .line 1635423
    const-string p1, "link"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635424
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1635425
    :cond_4
    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1635426
    if-eqz v1, :cond_5

    .line 1635427
    const-string p1, "message"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635428
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1635429
    :cond_5
    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1635430
    if-eqz v1, :cond_6

    .line 1635431
    const-string p1, "name"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635432
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1635433
    :cond_6
    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1635434
    if-eqz v1, :cond_7

    .line 1635435
    const-string p1, "picture"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635436
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1635437
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1635438
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1635439
    return-void
.end method
