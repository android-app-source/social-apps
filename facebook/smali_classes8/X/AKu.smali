.class public LX/AKu;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/resources/ui/FbFrameLayout;

.field public final b:Landroid/view/View;

.field private final c:Landroid/support/v4/view/ViewPager;

.field private final d:LX/3rW;

.field private e:F

.field private f:F

.field private g:F

.field private h:F

.field private i:F

.field public j:LX/AKt;

.field public k:Z

.field private final l:Landroid/view/GestureDetector$SimpleOnGestureListener;


# direct methods
.method public constructor <init>(Lcom/facebook/resources/ui/FbFrameLayout;Landroid/view/View;Landroid/support/v4/view/ViewPager;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1664370
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1664371
    new-instance v0, LX/AKo;

    invoke-direct {v0, p0}, LX/AKo;-><init>(LX/AKu;)V

    iput-object v0, p0, LX/AKu;->l:Landroid/view/GestureDetector$SimpleOnGestureListener;

    .line 1664372
    iput-object p1, p0, LX/AKu;->a:Lcom/facebook/resources/ui/FbFrameLayout;

    .line 1664373
    iput-object p2, p0, LX/AKu;->b:Landroid/view/View;

    .line 1664374
    iput-object p3, p0, LX/AKu;->c:Landroid/support/v4/view/ViewPager;

    .line 1664375
    new-instance v0, LX/3rW;

    iget-object v1, p0, LX/AKu;->a:Lcom/facebook/resources/ui/FbFrameLayout;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbFrameLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/AKu;->l:Landroid/view/GestureDetector$SimpleOnGestureListener;

    invoke-direct {v0, v1, v2}, LX/3rW;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, LX/AKu;->d:LX/3rW;

    .line 1664376
    iget-object v0, p0, LX/AKu;->d:LX/3rW;

    invoke-virtual {v0, v3}, LX/3rW;->a(Z)V

    .line 1664377
    invoke-virtual {p0, v3}, LX/AKu;->a(Z)V

    .line 1664378
    return-void
.end method

.method public static b$redex0(LX/AKu;)V
    .locals 3

    .prologue
    .line 1664379
    iget-object v0, p0, LX/AKu;->a:Lcom/facebook/resources/ui/FbFrameLayout;

    invoke-static {v0}, LX/7hT;->a(Landroid/view/View;)LX/7hT;

    move-result-object v0

    invoke-virtual {v0}, LX/7hT;->e()LX/7hT;

    move-result-object v0

    iget-object v1, p0, LX/AKu;->a:Lcom/facebook/resources/ui/FbFrameLayout;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbFrameLayout;->getY()F

    move-result v1

    iget-object v2, p0, LX/AKu;->a:Lcom/facebook/resources/ui/FbFrameLayout;

    invoke-virtual {v2}, Lcom/facebook/resources/ui/FbFrameLayout;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, LX/7hT;->b(FF)LX/7hT;

    move-result-object v0

    new-instance v1, LX/AKq;

    invoke-direct {v1, p0}, LX/AKq;-><init>(LX/AKu;)V

    .line 1664380
    iput-object v1, v0, LX/7hT;->d:LX/7hP;

    .line 1664381
    move-object v0, v0

    .line 1664382
    new-instance v1, LX/AKp;

    invoke-direct {v1, p0}, LX/AKp;-><init>(LX/AKu;)V

    .line 1664383
    iput-object v1, v0, LX/7hT;->e:LX/7hO;

    .line 1664384
    move-object v0, v0

    .line 1664385
    invoke-virtual {v0}, LX/7hT;->d()LX/7hT;

    .line 1664386
    return-void
.end method

.method public static d$redex0(LX/AKu;)V
    .locals 3

    .prologue
    .line 1664338
    iget-object v0, p0, LX/AKu;->a:Lcom/facebook/resources/ui/FbFrameLayout;

    invoke-static {v0}, LX/7hT;->a(Landroid/view/View;)LX/7hT;

    move-result-object v0

    invoke-virtual {v0}, LX/7hT;->e()LX/7hT;

    move-result-object v0

    iget-object v1, p0, LX/AKu;->a:Lcom/facebook/resources/ui/FbFrameLayout;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbFrameLayout;->getY()F

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/7hT;->b(FF)LX/7hT;

    move-result-object v0

    new-instance v1, LX/AKs;

    invoke-direct {v1, p0}, LX/AKs;-><init>(LX/AKu;)V

    .line 1664339
    iput-object v1, v0, LX/7hT;->d:LX/7hP;

    .line 1664340
    move-object v0, v0

    .line 1664341
    new-instance v1, LX/AKr;

    invoke-direct {v1, p0}, LX/AKr;-><init>(LX/AKu;)V

    .line 1664342
    iput-object v1, v0, LX/7hT;->e:LX/7hO;

    .line 1664343
    move-object v0, v0

    .line 1664344
    invoke-virtual {v0}, LX/7hT;->d()LX/7hT;

    .line 1664345
    return-void
.end method

.method public static f(LX/AKu;)V
    .locals 4

    .prologue
    .line 1664368
    iget-object v0, p0, LX/AKu;->b:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    iget-object v2, p0, LX/AKu;->a:Lcom/facebook/resources/ui/FbFrameLayout;

    invoke-virtual {v2}, Lcom/facebook/resources/ui/FbFrameLayout;->getY()F

    move-result v2

    iget-object v3, p0, LX/AKu;->a:Lcom/facebook/resources/ui/FbFrameLayout;

    invoke-virtual {v3}, Lcom/facebook/resources/ui/FbFrameLayout;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 1664369
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MotionEvent;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1664349
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 1664350
    iget-object v1, p0, LX/AKu;->d:LX/3rW;

    invoke-virtual {v1, p1}, LX/3rW;->a(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1664351
    :cond_0
    :goto_0
    return-void

    .line 1664352
    :cond_1
    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    if-ne v0, v3, :cond_4

    .line 1664353
    :cond_2
    iget-object v0, p0, LX/AKu;->a:Lcom/facebook/resources/ui/FbFrameLayout;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbFrameLayout;->getY()F

    move-result v0

    iget-object v1, p0, LX/AKu;->a:Lcom/facebook/resources/ui/FbFrameLayout;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbFrameLayout;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x4

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    .line 1664354
    invoke-static {p0}, LX/AKu;->b$redex0(LX/AKu;)V

    .line 1664355
    :goto_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/AKu;->a(Z)V

    goto :goto_0

    .line 1664356
    :cond_3
    invoke-static {p0}, LX/AKu;->d$redex0(LX/AKu;)V

    goto :goto_1

    .line 1664357
    :cond_4
    if-nez v0, :cond_5

    .line 1664358
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iput v0, p0, LX/AKu;->f:F

    .line 1664359
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iput v0, p0, LX/AKu;->h:F

    .line 1664360
    iget-object v0, p0, LX/AKu;->a:Lcom/facebook/resources/ui/FbFrameLayout;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbFrameLayout;->getY()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    sub-float/2addr v0, v1

    iput v0, p0, LX/AKu;->e:F

    goto :goto_0

    .line 1664361
    :cond_5
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1664362
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iput v0, p0, LX/AKu;->g:F

    .line 1664363
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iput v0, p0, LX/AKu;->i:F

    .line 1664364
    iget v0, p0, LX/AKu;->h:F

    iget v1, p0, LX/AKu;->i:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, LX/AKu;->f:F

    iget v2, p0, LX/AKu;->g:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 1664365
    iget-object v0, p0, LX/AKu;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v3}, Landroid/support/v4/view/ViewPager;->requestDisallowInterceptTouchEvent(Z)V

    .line 1664366
    iget-object v0, p0, LX/AKu;->a:Lcom/facebook/resources/ui/FbFrameLayout;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    iget v2, p0, LX/AKu;->e:F

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbFrameLayout;->setY(F)V

    .line 1664367
    invoke-static {p0}, LX/AKu;->f(LX/AKu;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 1664346
    iput-boolean p1, p0, LX/AKu;->k:Z

    .line 1664347
    iget-object v0, p0, LX/AKu;->c:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->requestDisallowInterceptTouchEvent(Z)V

    .line 1664348
    return-void
.end method
