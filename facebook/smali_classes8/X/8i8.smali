.class public LX/8i8;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1391216
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1391217
    return-void
.end method

.method public static a(Ljava/util/List;Ljava/util/List;)Z
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1391218
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    .line 1391219
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, p0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move v4, v2

    .line 1391220
    :goto_0
    if-ge v4, v5, :cond_3

    .line 1391221
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1391222
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1391223
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .line 1391224
    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1391225
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1391226
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 1391227
    add-int/lit8 v8, v5, -0x1

    if-ne v4, v8, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1391228
    invoke-interface {v7}, Ljava/util/Iterator;->remove()V

    move v0, v3

    .line 1391229
    :goto_1
    if-nez v0, :cond_2

    move v0, v2

    .line 1391230
    :goto_2
    return v0

    .line 1391231
    :cond_1
    add-int/lit8 v8, v5, -0x1

    if-ge v4, v8, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1391232
    invoke-interface {v7}, Ljava/util/Iterator;->remove()V

    move v0, v3

    .line 1391233
    goto :goto_1

    .line 1391234
    :cond_2
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_3
    move v0, v3

    .line 1391235
    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_1
.end method
