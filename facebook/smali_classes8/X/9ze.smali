.class public final LX/9ze;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 1602194
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_b

    .line 1602195
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1602196
    :goto_0
    return v1

    .line 1602197
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1602198
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_a

    .line 1602199
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1602200
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1602201
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 1602202
    const-string v11, "about_information"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 1602203
    invoke-static {p0, p1}, LX/9zc;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 1602204
    :cond_2
    const-string v11, "dense_node_story"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1602205
    invoke-static {p0, p1}, LX/2aD;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1602206
    :cond_3
    const-string v11, "external_uri"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 1602207
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 1602208
    :cond_4
    const-string v11, "logging_unit_id"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 1602209
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 1602210
    :cond_5
    const-string v11, "metadata"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 1602211
    invoke-static {p0, p1}, LX/9zd;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1602212
    :cond_6
    const-string v11, "node"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 1602213
    invoke-static {p0, p1}, LX/2bR;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1602214
    :cond_7
    const-string v11, "node_story"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 1602215
    invoke-static {p0, p1}, LX/2aD;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1602216
    :cond_8
    const-string v11, "result_decoration"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 1602217
    invoke-static {p0, p1}, LX/9zb;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 1602218
    :cond_9
    const-string v11, "result_role"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 1602219
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    goto/16 :goto_1

    .line 1602220
    :cond_a
    const/16 v10, 0x9

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1602221
    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 1602222
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 1602223
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 1602224
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 1602225
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1602226
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1602227
    const/4 v1, 0x6

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1602228
    const/4 v1, 0x7

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1602229
    const/16 v1, 0x8

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1602230
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_b
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 1602155
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1602156
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1602157
    if-eqz v0, :cond_0

    .line 1602158
    const-string v1, "about_information"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1602159
    invoke-static {p0, v0, p2}, LX/9zc;->a(LX/15i;ILX/0nX;)V

    .line 1602160
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1602161
    if-eqz v0, :cond_1

    .line 1602162
    const-string v1, "dense_node_story"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1602163
    invoke-static {p0, v0, p2, p3}, LX/2aD;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1602164
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1602165
    if-eqz v0, :cond_2

    .line 1602166
    const-string v1, "external_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1602167
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1602168
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1602169
    if-eqz v0, :cond_3

    .line 1602170
    const-string v1, "logging_unit_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1602171
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1602172
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1602173
    if-eqz v0, :cond_4

    .line 1602174
    const-string v1, "metadata"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1602175
    invoke-static {p0, v0, p2, p3}, LX/9zd;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1602176
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1602177
    if-eqz v0, :cond_5

    .line 1602178
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1602179
    invoke-static {p0, v0, p2, p3}, LX/2bR;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1602180
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1602181
    if-eqz v0, :cond_6

    .line 1602182
    const-string v1, "node_story"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1602183
    invoke-static {p0, v0, p2, p3}, LX/2aD;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1602184
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1602185
    if-eqz v0, :cond_7

    .line 1602186
    const-string v1, "result_decoration"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1602187
    invoke-static {p0, v0, p2, p3}, LX/9zb;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1602188
    :cond_7
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1602189
    if-eqz v0, :cond_8

    .line 1602190
    const-string v0, "result_role"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1602191
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1602192
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1602193
    return-void
.end method
