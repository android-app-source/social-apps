.class public LX/8tn;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/4Ba;
.implements LX/39D;


# static fields
.field public static final a:LX/0wT;


# instance fields
.field public A:LX/31M;

.field public B:LX/31M;

.field public C:D

.field public D:D

.field public b:LX/1wz;

.field public c:LX/0wc;

.field public d:LX/1nJ;

.field public e:LX/0hB;

.field public f:LX/0wd;

.field public g:LX/0wd;

.field public h:LX/8tk;

.field public i:LX/8tl;

.field public j:LX/8qU;

.field public k:LX/0xi;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Landroid/graphics/drawable/Drawable;

.field public m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field

.field public n:Landroid/view/ViewGroup;

.field public o:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Landroid/view/ViewGroup;",
            ">;"
        }
    .end annotation
.end field

.field public p:Landroid/view/ViewGroup;

.field public q:F

.field public r:F

.field public s:LX/31M;

.field public t:LX/31M;

.field public u:LX/8tm;

.field public v:Z

.field public w:Z

.field public x:Z

.field public y:I

.field public z:I


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1413903
    const-wide/high16 v0, 0x4014000000000000L    # 5.0

    const-wide/high16 v2, 0x4018000000000000L    # 6.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->b(DD)LX/0wT;

    move-result-object v0

    sput-object v0, LX/8tn;->a:LX/0wT;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1413942
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1413943
    iput-boolean v0, p0, LX/8tn;->v:Z

    .line 1413944
    iput-boolean v0, p0, LX/8tn;->w:Z

    .line 1413945
    iput-boolean v0, p0, LX/8tn;->x:Z

    .line 1413946
    sget-object v0, LX/31M;->DOWN:LX/31M;

    invoke-virtual {v0}, LX/31M;->flag()I

    move-result v0

    sget-object v1, LX/31M;->UP:LX/31M;

    invoke-virtual {v1}, LX/31M;->flag()I

    move-result v1

    or-int/2addr v0, v1

    iput v0, p0, LX/8tn;->y:I

    .line 1413947
    iget v0, p0, LX/8tn;->y:I

    iput v0, p0, LX/8tn;->z:I

    .line 1413948
    sget-object v0, LX/31M;->UP:LX/31M;

    iput-object v0, p0, LX/8tn;->A:LX/31M;

    .line 1413949
    sget-object v0, LX/31M;->DOWN:LX/31M;

    iput-object v0, p0, LX/8tn;->B:LX/31M;

    .line 1413950
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    iput-wide v0, p0, LX/8tn;->C:D

    .line 1413951
    const-wide/high16 v0, 0x3fd0000000000000L    # 0.25

    iput-wide v0, p0, LX/8tn;->D:D

    .line 1413952
    const-class v0, LX/8tn;

    invoke-static {v0, p0}, LX/8tn;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1413953
    invoke-virtual {p0, p2}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1413954
    const v0, 0x7f0d0807

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/8tn;->n:Landroid/view/ViewGroup;

    .line 1413955
    const v0, 0x7f0d1605

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->b(I)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/8tn;->o:LX/0am;

    .line 1413956
    const v0, 0x7f0d0d5c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/8tn;->p:Landroid/view/ViewGroup;

    .line 1413957
    sget-object v0, LX/8tm;->AT_REST:LX/8tm;

    iput-object v0, p0, LX/8tn;->u:LX/8tm;

    .line 1413958
    iget-object v0, p0, LX/8tn;->b:LX/1wz;

    .line 1413959
    iput-object p0, v0, LX/1wz;->q:LX/4Ba;

    .line 1413960
    iget-object v0, p0, LX/8tn;->b:LX/1wz;

    .line 1413961
    iput-object p0, v0, LX/1wz;->r:LX/39D;

    .line 1413962
    iget-object v0, p0, LX/8tn;->b:LX/1wz;

    iget v1, p0, LX/8tn;->y:I

    .line 1413963
    iput v1, v0, LX/1wz;->p:I

    .line 1413964
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, LX/8tn;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const p1, 0x7f0a04a3

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, LX/8tn;->l:Landroid/graphics/drawable/Drawable;

    .line 1413965
    iget-object v0, p0, LX/8tn;->l:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1413966
    iget-object v0, p0, LX/8tn;->l:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, LX/8tn;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1413967
    return-void
.end method

.method public static a(LX/8tn;LX/31M;D)V
    .locals 4

    .prologue
    .line 1413930
    iget-object v0, p0, LX/8tn;->u:LX/8tm;

    sget-object v1, LX/8tm;->DISMISSING:LX/8tm;

    if-ne v0, v1, :cond_0

    .line 1413931
    :goto_0
    return-void

    .line 1413932
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/8tn;->t:LX/31M;

    .line 1413933
    iget-object v1, p0, LX/8tn;->p:Landroid/view/ViewGroup;

    invoke-static {v1}, LX/0wc;->a(Landroid/view/View;)V

    .line 1413934
    iget-object v0, p0, LX/8tn;->j:LX/8qU;

    invoke-virtual {v0}, LX/8qU;->c()V

    .line 1413935
    invoke-virtual {p1}, LX/31M;->isYAxis()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/8tn;->e:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->d()I

    move-result v0

    int-to-double v0, v0

    .line 1413936
    :goto_1
    sget-object v2, LX/31M;->UP:LX/31M;

    if-eq p1, v2, :cond_1

    sget-object v2, LX/31M;->LEFT:LX/31M;

    if-ne p1, v2, :cond_2

    .line 1413937
    :cond_1
    neg-double v0, v0

    .line 1413938
    :cond_2
    iget-object v2, p0, LX/8tn;->f:LX/0wd;

    invoke-virtual {v2, v0, v1}, LX/0wd;->b(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, LX/0wd;->c(D)LX/0wd;

    move-result-object v0

    const/4 v1, 0x1

    .line 1413939
    iput-boolean v1, v0, LX/0wd;->c:Z

    .line 1413940
    sget-object v0, LX/8tm;->DISMISSING:LX/8tm;

    iput-object v0, p0, LX/8tn;->u:LX/8tm;

    goto :goto_0

    .line 1413941
    :cond_3
    iget-object v0, p0, LX/8tn;->e:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    move-result v0

    int-to-double v0, v0

    goto :goto_1
.end method

.method public static a(Landroid/view/View;LX/31M;I)V
    .locals 1

    .prologue
    .line 1413926
    invoke-virtual {p1}, LX/31M;->isYAxis()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1413927
    int-to-float v0, p2

    invoke-virtual {p0, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 1413928
    :goto_0
    return-void

    .line 1413929
    :cond_0
    int-to-float v0, p2

    invoke-virtual {p0, v0}, Landroid/view/View;->setTranslationX(F)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v7

    move-object v1, p1

    check-cast v1, LX/8tn;

    invoke-static {v7}, LX/1wx;->b(LX/0QB;)LX/1wz;

    move-result-object v2

    check-cast v2, LX/1wz;

    invoke-static {v7}, LX/0wc;->a(LX/0QB;)LX/0wc;

    move-result-object v3

    check-cast v3, LX/0wc;

    const/16 v4, 0x97

    invoke-static {v7, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {v7}, LX/1nJ;->a(LX/0QB;)LX/1nJ;

    move-result-object v5

    check-cast v5, LX/1nJ;

    invoke-static {v7}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v6

    check-cast v6, LX/0wW;

    invoke-static {v7}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v7

    check-cast v7, LX/0hB;

    const-wide/high16 v10, 0x4024000000000000L    # 10.0

    iput-object v3, v1, LX/8tn;->c:LX/0wc;

    iput-object v2, v1, LX/8tn;->b:LX/1wz;

    iput-object v4, v1, LX/8tn;->m:LX/0Ot;

    iput-object v5, v1, LX/8tn;->d:LX/1nJ;

    invoke-virtual {v6}, LX/0wW;->a()LX/0wd;

    move-result-object v8

    sget-object v9, LX/8tn;->a:LX/0wT;

    invoke-virtual {v8, v9}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v8

    iput-wide v10, v8, LX/0wd;->l:D

    move-object v8, v8

    iput-wide v10, v8, LX/0wd;->k:D

    move-object v8, v8

    iput-object v8, v1, LX/8tn;->f:LX/0wd;

    new-instance v8, LX/8tk;

    invoke-direct {v8, v1}, LX/8tk;-><init>(LX/8tn;)V

    iput-object v8, v1, LX/8tn;->h:LX/8tk;

    invoke-virtual {v6}, LX/0wW;->a()LX/0wd;

    move-result-object v8

    sget-object v9, LX/8tn;->a:LX/0wT;

    invoke-virtual {v8, v9}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v8

    iput-wide v10, v8, LX/0wd;->l:D

    move-object v8, v8

    iput-wide v10, v8, LX/0wd;->k:D

    move-object v8, v8

    iput-object v8, v1, LX/8tn;->g:LX/0wd;

    new-instance v8, LX/8tl;

    invoke-direct {v8, v1}, LX/8tl;-><init>(LX/8tn;)V

    iput-object v8, v1, LX/8tn;->i:LX/8tl;

    iput-object v7, v1, LX/8tn;->e:LX/0hB;

    return-void
.end method

.method private b(LX/31M;I)V
    .locals 6

    .prologue
    .line 1413914
    iget v0, p0, LX/8tn;->r:F

    .line 1413915
    iget v2, p0, LX/8tn;->z:I

    invoke-virtual {p1, v2}, LX/31M;->isSetInFlags(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1413916
    const/4 v2, 0x0

    .line 1413917
    :goto_0
    move v0, v2

    .line 1413918
    if-eqz v0, :cond_0

    .line 1413919
    iget-object v0, p0, LX/8tn;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    const-string v1, "swipe"

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 1413920
    int-to-double v0, p2

    invoke-static {p0, p1, v0, v1}, LX/8tn;->a(LX/8tn;LX/31M;D)V

    .line 1413921
    :goto_1
    return-void

    .line 1413922
    :cond_0
    int-to-double v0, p2

    invoke-static {p0, v0, v1}, LX/8tn;->c(LX/8tn;D)V

    goto :goto_1

    .line 1413923
    :cond_1
    invoke-virtual {p1}, LX/31M;->isYAxis()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1413924
    iget-object v2, p0, LX/8tn;->p:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getHeight()I

    move-result v2

    int-to-double v2, v2

    iget-wide v4, p0, LX/8tn;->D:D

    mul-double/2addr v2, v4

    double-to-int v2, v2

    invoke-static {v2, v0, p1}, LX/8ti;->b(IFLX/31M;)Z

    move-result v2

    goto :goto_0

    .line 1413925
    :cond_2
    iget-object v2, p0, LX/8tn;->p:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getWidth()I

    move-result v2

    int-to-double v2, v2

    iget-wide v4, p0, LX/8tn;->D:D

    mul-double/2addr v2, v4

    double-to-int v2, v2

    invoke-static {v2, v0, p1}, LX/8ti;->c(IFLX/31M;)Z

    move-result v2

    goto :goto_0
.end method

.method public static c(LX/8tn;D)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 1413906
    iget-object v0, p0, LX/8tn;->u:LX/8tm;

    sget-object v1, LX/8tm;->ANIMATING:LX/8tm;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/8tn;->u:LX/8tm;

    sget-object v1, LX/8tm;->DISMISSING:LX/8tm;

    if-ne v0, v1, :cond_1

    .line 1413907
    :cond_0
    :goto_0
    return-void

    .line 1413908
    :cond_1
    iget-object v0, p0, LX/8tn;->f:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->d()D

    move-result-wide v0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_2

    .line 1413909
    iget-object v0, p0, LX/8tn;->f:LX/0wd;

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LX/0wd;->c(D)LX/0wd;

    .line 1413910
    sget-object v0, LX/8tm;->ANIMATING:LX/8tm;

    iput-object v0, p0, LX/8tn;->u:LX/8tm;

    .line 1413911
    :cond_2
    iget-object v0, p0, LX/8tn;->g:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->d()D

    move-result-wide v0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    .line 1413912
    iget-object v0, p0, LX/8tn;->g:LX/0wd;

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LX/0wd;->c(D)LX/0wd;

    .line 1413913
    sget-object v0, LX/8tm;->ANIMATING:LX/8tm;

    iput-object v0, p0, LX/8tn;->u:LX/8tm;

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1413904
    iget-object v0, p0, LX/8tn;->s:LX/31M;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LX/8tn;->b(LX/31M;I)V

    .line 1413905
    return-void
.end method

.method public final a(LX/31M;I)V
    .locals 0

    .prologue
    .line 1413831
    invoke-direct {p0, p1, p2}, LX/8tn;->b(LX/31M;I)V

    .line 1413832
    return-void
.end method

.method public final a(FF)Z
    .locals 1

    .prologue
    .line 1413902
    const/4 v0, 0x1

    return v0
.end method

.method public final a(FFLX/31M;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 1413968
    iget-object v1, p0, LX/8tn;->u:LX/8tm;

    sget-object v2, LX/8tm;->REVEALING:LX/8tm;

    if-ne v1, v2, :cond_1

    .line 1413969
    :cond_0
    :goto_0
    return v0

    .line 1413970
    :cond_1
    iput v3, p0, LX/8tn;->q:F

    .line 1413971
    iput v3, p0, LX/8tn;->r:F

    .line 1413972
    iput-object p3, p0, LX/8tn;->s:LX/31M;

    .line 1413973
    iput-object p3, p0, LX/8tn;->t:LX/31M;

    .line 1413974
    sget-object v1, LX/8tm;->BEING_DRAGGED:LX/8tm;

    iput-object v1, p0, LX/8tn;->u:LX/8tm;

    .line 1413975
    iget-object v1, p0, LX/8tn;->n:Landroid/view/ViewGroup;

    invoke-static {v1, p1, p2}, LX/8ti;->a(Landroid/view/View;FF)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, LX/8tn;->j:LX/8qU;

    invoke-virtual {v1, p1, p2, p3}, LX/8qU;->a(FFLX/31M;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1413901
    return-void
.end method

.method public final b(FFLX/31M;)V
    .locals 10

    .prologue
    const-wide/16 v4, 0x0

    .line 1413867
    invoke-virtual {p3}, LX/31M;->isYAxis()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1413868
    iget v0, p0, LX/8tn;->r:F

    .line 1413869
    :goto_0
    const/high16 v2, 0x3f800000    # 1.0f

    .line 1413870
    iget v1, p0, LX/8tn;->z:I

    invoke-virtual {p3, v1}, LX/31M;->isSetInFlags(I)Z

    move-result v1

    if-nez v1, :cond_6

    .line 1413871
    invoke-virtual {p3}, LX/31M;->isYAxis()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/8tn;->p:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v1

    .line 1413872
    :goto_1
    add-float v3, v0, p2

    .line 1413873
    div-int/lit8 v6, v1, 0x10

    invoke-static {v6, v3, p3}, LX/8ti;->a(IFLX/31M;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1413874
    const v1, 0x3dcccccd    # 0.1f

    .line 1413875
    :goto_2
    mul-float/2addr v1, p2

    add-float/2addr v0, v1

    .line 1413876
    invoke-virtual {p3}, LX/31M;->isYAxis()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1413877
    iput v0, p0, LX/8tn;->r:F

    .line 1413878
    :goto_3
    move v0, v0

    .line 1413879
    iget v6, p0, LX/8tn;->z:I

    invoke-virtual {p3, v6}, LX/31M;->isSetInFlags(I)Z

    move-result v6

    if-nez v6, :cond_7

    .line 1413880
    const/4 v6, 0x0

    .line 1413881
    :goto_4
    move v1, v6

    .line 1413882
    if-eqz v1, :cond_0

    .line 1413883
    iget-object v0, p0, LX/8tn;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    const-string v1, "swipe"

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 1413884
    iget-object v0, p0, LX/8tn;->b:LX/1wz;

    invoke-virtual {v0}, LX/1wz;->c()V

    .line 1413885
    invoke-static {p0, p3, v4, v5}, LX/8tn;->a(LX/8tn;LX/31M;D)V

    .line 1413886
    :goto_5
    return-void

    .line 1413887
    :cond_0
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-lez v1, :cond_1

    .line 1413888
    iget-object v1, p0, LX/8tn;->g:LX/0wd;

    float-to-double v2, v0

    invoke-virtual {v1, v2, v3}, LX/0wd;->b(D)LX/0wd;

    move-result-object v1

    float-to-double v2, v0

    invoke-virtual {v1, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    .line 1413889
    iget-object v0, p0, LX/8tn;->f:LX/0wd;

    invoke-virtual {v0, v4, v5}, LX/0wd;->b(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    goto :goto_5

    .line 1413890
    :cond_1
    iget-object v1, p0, LX/8tn;->f:LX/0wd;

    float-to-double v2, v0

    invoke-virtual {v1, v2, v3}, LX/0wd;->b(D)LX/0wd;

    move-result-object v1

    float-to-double v2, v0

    invoke-virtual {v1, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    .line 1413891
    iget-object v0, p0, LX/8tn;->g:LX/0wd;

    invoke-virtual {v0, v4, v5}, LX/0wd;->b(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    goto :goto_5

    .line 1413892
    :cond_2
    iget v0, p0, LX/8tn;->q:F

    move p2, p1

    .line 1413893
    goto/16 :goto_0

    .line 1413894
    :cond_3
    iget-object v1, p0, LX/8tn;->p:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v1

    goto/16 :goto_1

    .line 1413895
    :cond_4
    div-int/lit8 v1, v1, 0x20

    invoke-static {v1, v3, p3}, LX/8ti;->a(IFLX/31M;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1413896
    const/high16 v1, 0x3e800000    # 0.25f

    goto/16 :goto_2

    .line 1413897
    :cond_5
    iput v0, p0, LX/8tn;->q:F

    goto :goto_3

    :cond_6
    move v1, v2

    goto/16 :goto_2

    .line 1413898
    :cond_7
    invoke-virtual {p3}, LX/31M;->isYAxis()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1413899
    iget-object v6, p0, LX/8tn;->p:Landroid/view/ViewGroup;

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getHeight()I

    move-result v6

    int-to-double v6, v6

    iget-wide v8, p0, LX/8tn;->C:D

    mul-double/2addr v6, v8

    double-to-int v6, v6

    invoke-static {v6, v0, p3}, LX/8ti;->b(IFLX/31M;)Z

    move-result v6

    goto/16 :goto_4

    .line 1413900
    :cond_8
    iget-object v6, p0, LX/8tn;->p:Landroid/view/ViewGroup;

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getWidth()I

    move-result v6

    int-to-double v6, v6

    iget-wide v8, p0, LX/8tn;->C:D

    mul-double/2addr v6, v8

    double-to-int v6, v6

    invoke-static {v6, v0, p3}, LX/8ti;->c(IFLX/31M;)Z

    move-result v6

    goto/16 :goto_4
.end method

.method public final b(FF)Z
    .locals 1

    .prologue
    .line 1413866
    const/4 v0, 0x0

    return v0
.end method

.method public final d()V
    .locals 7

    .prologue
    .line 1413856
    sget-object v0, LX/8tm;->NEEDS_REVEAL:LX/8tm;

    iput-object v0, p0, LX/8tn;->u:LX/8tm;

    .line 1413857
    iget-boolean v0, p0, LX/8tn;->v:Z

    if-eqz v0, :cond_0

    .line 1413858
    const-wide/16 v5, 0x0

    .line 1413859
    iget-object v1, p0, LX/8tn;->A:LX/31M;

    invoke-virtual {v1}, LX/31M;->isYAxis()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/8tn;->e:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->d()I

    move-result v1

    .line 1413860
    :goto_0
    iget-object v2, p0, LX/8tn;->f:LX/0wd;

    int-to-double v3, v1

    invoke-virtual {v2, v3, v4}, LX/0wd;->b(D)LX/0wd;

    move-result-object v2

    int-to-double v3, v1

    invoke-virtual {v2, v3, v4}, LX/0wd;->a(D)LX/0wd;

    move-result-object v2

    invoke-virtual {v2}, LX/0wd;->j()LX/0wd;

    .line 1413861
    iget-object v2, p0, LX/8tn;->g:LX/0wd;

    invoke-virtual {v2, v5, v6}, LX/0wd;->b(D)LX/0wd;

    move-result-object v2

    invoke-virtual {v2, v5, v6}, LX/0wd;->a(D)LX/0wd;

    move-result-object v2

    invoke-virtual {v2}, LX/0wd;->j()LX/0wd;

    .line 1413862
    iget-object v2, p0, LX/8tn;->p:Landroid/view/ViewGroup;

    iget-object v3, p0, LX/8tn;->A:LX/31M;

    invoke-static {v2, v3, v1}, LX/8tn;->a(Landroid/view/View;LX/31M;I)V

    .line 1413863
    invoke-virtual {p0}, LX/8tn;->requestLayout()V

    .line 1413864
    :cond_0
    return-void

    .line 1413865
    :cond_1
    iget-object v1, p0, LX/8tn;->e:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->c()I

    move-result v1

    goto :goto_0
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 1413846
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 1413847
    iget-object v0, p0, LX/8tn;->u:LX/8tm;

    sget-object v1, LX/8tm;->NEEDS_REVEAL:LX/8tm;

    if-ne v0, v1, :cond_0

    .line 1413848
    iget-object v3, p0, LX/8tn;->p:Landroid/view/ViewGroup;

    invoke-static {v3}, LX/0wc;->a(Landroid/view/View;)V

    .line 1413849
    const-wide/16 v4, 0x0

    invoke-static {p0, v4, v5}, LX/8tn;->c(LX/8tn;D)V

    .line 1413850
    iget-object v2, p0, LX/8tn;->j:LX/8qU;

    invoke-virtual {v2}, LX/8qU;->a()V

    .line 1413851
    sget-object v2, LX/8tm;->REVEALING:LX/8tm;

    iput-object v2, p0, LX/8tn;->u:LX/8tm;

    .line 1413852
    iget-boolean v0, p0, LX/8tn;->v:Z

    if-nez v0, :cond_0

    .line 1413853
    sget-object v0, LX/8tm;->AT_REST:LX/8tm;

    iput-object v0, p0, LX/8tn;->u:LX/8tm;

    .line 1413854
    iget-object v0, p0, LX/8tn;->d:LX/1nJ;

    invoke-virtual {v0}, LX/1nJ;->c()V

    .line 1413855
    :cond_0
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x605762a2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1413842
    invoke-super {p0}, Lcom/facebook/widget/CustomRelativeLayout;->onAttachedToWindow()V

    .line 1413843
    iget-object v1, p0, LX/8tn;->f:LX/0wd;

    iget-object v2, p0, LX/8tn;->h:LX/8tk;

    invoke-virtual {v1, v2}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 1413844
    iget-object v1, p0, LX/8tn;->g:LX/0wd;

    iget-object v2, p0, LX/8tn;->i:LX/8tl;

    invoke-virtual {v1, v2}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 1413845
    const/16 v1, 0x2d

    const v2, -0x1f8bf407

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x4ec44867

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1413837
    invoke-super {p0}, Lcom/facebook/widget/CustomRelativeLayout;->onDetachedFromWindow()V

    .line 1413838
    iget-object v1, p0, LX/8tn;->f:LX/0wd;

    iget-object v2, p0, LX/8tn;->h:LX/8tk;

    invoke-virtual {v1, v2}, LX/0wd;->b(LX/0xi;)LX/0wd;

    .line 1413839
    iget-object v1, p0, LX/8tn;->g:LX/0wd;

    iget-object v2, p0, LX/8tn;->i:LX/8tl;

    invoke-virtual {v1, v2}, LX/0wd;->b(LX/0xi;)LX/0wd;

    .line 1413840
    iget-object v1, p0, LX/8tn;->d:LX/1nJ;

    invoke-virtual {v1}, LX/1nJ;->c()V

    .line 1413841
    const/16 v1, 0x2d

    const v2, -0x3e9ef9a0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1413836
    iget-object v0, p0, LX/8tn;->b:LX/1wz;

    invoke-virtual {v0, p1}, LX/1wz;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v4, 0x2

    const v1, 0x10f4a568

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1413833
    iget-object v2, p0, LX/8tn;->u:LX/8tm;

    sget-object v3, LX/8tm;->DISMISSING:LX/8tm;

    if-eq v2, v3, :cond_0

    iget-object v2, p0, LX/8tn;->u:LX/8tm;

    sget-object v3, LX/8tm;->ANIMATING:LX/8tm;

    if-eq v2, v3, :cond_0

    iget-object v2, p0, LX/8tn;->u:LX/8tm;

    sget-object v3, LX/8tm;->NEEDS_REVEAL:LX/8tm;

    if-ne v2, v3, :cond_1

    .line 1413834
    :cond_0
    const v2, -0x463f499

    invoke-static {v4, v4, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1413835
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/8tn;->b:LX/1wz;

    invoke-virtual {v0, p1}, LX/1wz;->b(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, 0x61e5678b

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method
