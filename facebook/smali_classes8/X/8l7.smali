.class public final LX/8l7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8l6;


# instance fields
.field public final synthetic a:LX/8kl;

.field public final synthetic b:Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;LX/8kl;)V
    .locals 0

    .prologue
    .line 1397393
    iput-object p1, p0, LX/8l7;->b:Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;

    iput-object p2, p0, LX/8l7;->a:LX/8kl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1397379
    iget-object v0, p0, LX/8l7;->b:Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;

    iget-object v0, v0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->o:LX/8kq;

    .line 1397380
    iget-object v1, v0, LX/8kq;->a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    const/4 v2, 0x1

    .line 1397381
    iput-boolean v2, v1, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->G:Z

    .line 1397382
    iget-object v0, p0, LX/8l7;->b:Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;

    iget-object v0, v0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->o:LX/8kq;

    const-string v1, ""

    invoke-virtual {v0, v1}, LX/8kq;->a(Ljava/lang/String;)V

    .line 1397383
    return-void
.end method

.method public final a(Lcom/facebook/stickers/model/Sticker;)V
    .locals 2

    .prologue
    .line 1397390
    iget-object v0, p0, LX/8l7;->b:Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;

    iget-object v0, v0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->o:LX/8kq;

    if-eqz v0, :cond_0

    .line 1397391
    iget-object v0, p0, LX/8l7;->b:Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;

    iget-object v0, v0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->o:LX/8kq;

    iget-object v1, p0, LX/8l7;->a:LX/8kl;

    iget-object v1, v1, LX/8kl;->c:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, LX/8kq;->a(Lcom/facebook/stickers/model/Sticker;Ljava/lang/String;)V

    .line 1397392
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1397388
    iget-object v0, p0, LX/8l7;->b:Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;

    iget-object v0, v0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->o:LX/8kq;

    invoke-virtual {v0, p1}, LX/8kq;->a(Ljava/lang/String;)V

    .line 1397389
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1397384
    iget-object v0, p0, LX/8l7;->b:Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;

    iget-object v0, v0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->o:LX/8kq;

    .line 1397385
    iget-object v1, v0, LX/8kq;->a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    const/4 p0, 0x0

    .line 1397386
    iput-boolean p0, v1, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->G:Z

    .line 1397387
    return-void
.end method
