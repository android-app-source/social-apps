.class public LX/9aV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9aT;


# instance fields
.field private final a:Ljava/util/Random;

.field private final b:F

.field private final c:F


# direct methods
.method public constructor <init>(FF)V
    .locals 1

    .prologue
    .line 1513800
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1513801
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, LX/9aV;->a:Ljava/util/Random;

    .line 1513802
    iput p1, p0, LX/9aV;->b:F

    .line 1513803
    iput p2, p0, LX/9aV;->c:F

    .line 1513804
    return-void
.end method


# virtual methods
.method public final a()F
    .locals 7

    .prologue
    .line 1513805
    iget-object v0, p0, LX/9aV;->a:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextGaussian()D

    move-result-wide v0

    double-to-float v0, v0

    .line 1513806
    const v1, -0x3feae148    # -2.33f

    const v2, 0x40151eb8    # 2.33f

    iget v3, p0, LX/9aV;->b:F

    iget v4, p0, LX/9aV;->c:F

    .line 1513807
    invoke-static {v0, v1, v2, v3, v4}, LX/0yq;->a(FFFFF)F

    move-result v5

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v6

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result p0

    invoke-static {v5, v6, p0}, LX/0yq;->b(FFF)F

    move-result v5

    move v0, v5

    .line 1513808
    return v0
.end method
