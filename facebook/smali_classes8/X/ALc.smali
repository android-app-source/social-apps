.class public final LX/ALc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:Lcom/facebook/backstage/camera/FocusView;


# direct methods
.method public constructor <init>(Lcom/facebook/backstage/camera/FocusView;)V
    .locals 0

    .prologue
    .line 1665118
    iput-object p1, p0, LX/ALc;->a:Lcom/facebook/backstage/camera/FocusView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 3

    .prologue
    .line 1665119
    iget-object v1, p0, LX/ALc;->a:Lcom/facebook/backstage/camera/FocusView;

    iget-object v0, p0, LX/ALc;->a:Lcom/facebook/backstage/camera/FocusView;

    iget v2, v0, Lcom/facebook/backstage/camera/FocusView;->c:F

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    mul-float/2addr v0, v2

    .line 1665120
    iput v0, v1, Lcom/facebook/backstage/camera/FocusView;->g:F

    .line 1665121
    iget-object v0, p0, LX/ALc;->a:Lcom/facebook/backstage/camera/FocusView;

    iget-object v0, v0, Lcom/facebook/backstage/camera/FocusView;->a:Landroid/graphics/Paint;

    const/high16 v1, 0x437f0000    # 255.0f

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1665122
    iget-object v0, p0, LX/ALc;->a:Lcom/facebook/backstage/camera/FocusView;

    invoke-virtual {v0}, Lcom/facebook/backstage/camera/FocusView;->invalidate()V

    .line 1665123
    return-void
.end method
