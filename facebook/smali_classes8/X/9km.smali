.class public LX/9km;
.super LX/1Cv;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/1Cv;"
    }
.end annotation


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/9ks",
            "<TT;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1532203
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 1532204
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1532205
    iput-object v0, p0, LX/9km;->a:LX/0Px;

    return-void
.end method

.method private a(I)LX/9ks;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/9ks",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1532206
    iget-object v0, p0, LX/9km;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9ks;

    return-object v0
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1532188
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1532189
    const v1, 0x7f030f67

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 3

    .prologue
    .line 1532190
    move-object v0, p3

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 1532191
    check-cast p2, LX/9ks;

    .line 1532192
    iget-object v1, p2, LX/9ks;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 1532193
    iget v1, p2, LX/9ks;->e:I

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleTextAppearance(I)V

    .line 1532194
    iget-object v1, p2, LX/9ks;->d:LX/0am;

    invoke-virtual {v1}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 1532195
    const v1, 0x7f0d2528

    invoke-virtual {p3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1532196
    iget v2, p2, LX/9ks;->f:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1532197
    iget v1, p2, LX/9ks;->f:I

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 1532198
    invoke-virtual {v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->getPaddingTop()I

    move-result v1

    .line 1532199
    iget-object v2, v0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    move-object v2, v2

    .line 1532200
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    add-int/2addr v1, v2

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMinimumHeight(I)V

    .line 1532201
    return-void

    .line 1532202
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 1532187
    iget-object v0, p0, LX/9km;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1532186
    invoke-direct {p0, p1}, LX/9km;->a(I)LX/9ks;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1532185
    invoke-direct {p0, p1}, LX/9km;->a(I)LX/9ks;

    move-result-object v0

    iget-wide v0, v0, LX/9ks;->b:J

    return-wide v0
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 1532184
    const/4 v0, 0x1

    return v0
.end method
