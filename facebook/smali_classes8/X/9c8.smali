.class public final enum LX/9c8;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9c8;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9c8;

.field public static final enum APPLIED_FILTER_NAME:LX/9c8;

.field public static final enum Entry_POINT:LX/9c8;

.field public static final enum IS_PHOTO_CROPPED:LX/9c8;

.field public static final enum IS_PHOTO_DELETED:LX/9c8;

.field public static final enum IS_PHOTO_PUBLISHED:LX/9c8;

.field public static final enum IS_PHOTO_ROTATED:LX/9c8;

.field public static final enum MEDIA_ITEM_IDENTIFIER:LX/9c8;

.field public static final enum MEDIA_ITEM_URI:LX/9c8;

.field public static final enum NUMBER_OF_CROP_ENTRIES:LX/9c8;

.field public static final enum NUMBER_OF_DOODLE_ON_PHOTO:LX/9c8;

.field public static final enum NUMBER_OF_FILTER_ENTRIES:LX/9c8;

.field public static final enum NUMBER_OF_STICKERS_ENTRIES:LX/9c8;

.field public static final enum NUMBER_OF_STICKER_ON_PHOTO:LX/9c8;

.field public static final enum NUMBER_OF_SWIPES:LX/9c8;

.field public static final enum NUMBER_OF_TEXT_ENTRIES:LX/9c8;

.field public static final enum NUMBER_OF_TEXT_ON_PHOTO:LX/9c8;

.field public static final enum NUMBER_OF_UG_ENTRIES:LX/9c8;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1515983
    new-instance v0, LX/9c8;

    const-string v1, "MEDIA_ITEM_IDENTIFIER"

    const-string v2, "media_id"

    invoke-direct {v0, v1, v4, v2}, LX/9c8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c8;->MEDIA_ITEM_IDENTIFIER:LX/9c8;

    .line 1515984
    new-instance v0, LX/9c8;

    const-string v1, "NUMBER_OF_UG_ENTRIES"

    const-string v2, "gv_editing_entries"

    invoke-direct {v0, v1, v5, v2}, LX/9c8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c8;->NUMBER_OF_UG_ENTRIES:LX/9c8;

    .line 1515985
    new-instance v0, LX/9c8;

    const-string v1, "NUMBER_OF_STICKERS_ENTRIES"

    const-string v2, "sticker_flow_entries"

    invoke-direct {v0, v1, v6, v2}, LX/9c8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c8;->NUMBER_OF_STICKERS_ENTRIES:LX/9c8;

    .line 1515986
    new-instance v0, LX/9c8;

    const-string v1, "NUMBER_OF_TEXT_ENTRIES"

    const-string v2, "text_flow_entries"

    invoke-direct {v0, v1, v7, v2}, LX/9c8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c8;->NUMBER_OF_TEXT_ENTRIES:LX/9c8;

    .line 1515987
    new-instance v0, LX/9c8;

    const-string v1, "NUMBER_OF_CROP_ENTRIES"

    const-string v2, "crop_flow_entries"

    invoke-direct {v0, v1, v8, v2}, LX/9c8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c8;->NUMBER_OF_CROP_ENTRIES:LX/9c8;

    .line 1515988
    new-instance v0, LX/9c8;

    const-string v1, "NUMBER_OF_FILTER_ENTRIES"

    const/4 v2, 0x5

    const-string v3, "filter_in_gallery_entries"

    invoke-direct {v0, v1, v2, v3}, LX/9c8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c8;->NUMBER_OF_FILTER_ENTRIES:LX/9c8;

    .line 1515989
    new-instance v0, LX/9c8;

    const-string v1, "NUMBER_OF_SWIPES"

    const/4 v2, 0x6

    const-string v3, "swipe_count"

    invoke-direct {v0, v1, v2, v3}, LX/9c8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c8;->NUMBER_OF_SWIPES:LX/9c8;

    .line 1515990
    new-instance v0, LX/9c8;

    const-string v1, "NUMBER_OF_STICKER_ON_PHOTO"

    const/4 v2, 0x7

    const-string v3, "sticker_count"

    invoke-direct {v0, v1, v2, v3}, LX/9c8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c8;->NUMBER_OF_STICKER_ON_PHOTO:LX/9c8;

    .line 1515991
    new-instance v0, LX/9c8;

    const-string v1, "NUMBER_OF_TEXT_ON_PHOTO"

    const/16 v2, 0x8

    const-string v3, "text_count"

    invoke-direct {v0, v1, v2, v3}, LX/9c8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c8;->NUMBER_OF_TEXT_ON_PHOTO:LX/9c8;

    .line 1515992
    new-instance v0, LX/9c8;

    const-string v1, "NUMBER_OF_DOODLE_ON_PHOTO"

    const/16 v2, 0x9

    const-string v3, "doodle_count"

    invoke-direct {v0, v1, v2, v3}, LX/9c8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c8;->NUMBER_OF_DOODLE_ON_PHOTO:LX/9c8;

    .line 1515993
    new-instance v0, LX/9c8;

    const-string v1, "APPLIED_FILTER_NAME"

    const/16 v2, 0xa

    const-string v3, "filter_name"

    invoke-direct {v0, v1, v2, v3}, LX/9c8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c8;->APPLIED_FILTER_NAME:LX/9c8;

    .line 1515994
    new-instance v0, LX/9c8;

    const-string v1, "IS_PHOTO_CROPPED"

    const/16 v2, 0xb

    const-string v3, "is_cropped"

    invoke-direct {v0, v1, v2, v3}, LX/9c8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c8;->IS_PHOTO_CROPPED:LX/9c8;

    .line 1515995
    new-instance v0, LX/9c8;

    const-string v1, "IS_PHOTO_ROTATED"

    const/16 v2, 0xc

    const-string v3, "is_rotated"

    invoke-direct {v0, v1, v2, v3}, LX/9c8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c8;->IS_PHOTO_ROTATED:LX/9c8;

    .line 1515996
    new-instance v0, LX/9c8;

    const-string v1, "IS_PHOTO_DELETED"

    const/16 v2, 0xd

    const-string v3, "is_deleted"

    invoke-direct {v0, v1, v2, v3}, LX/9c8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c8;->IS_PHOTO_DELETED:LX/9c8;

    .line 1515997
    new-instance v0, LX/9c8;

    const-string v1, "IS_PHOTO_PUBLISHED"

    const/16 v2, 0xe

    const-string v3, "is_published"

    invoke-direct {v0, v1, v2, v3}, LX/9c8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c8;->IS_PHOTO_PUBLISHED:LX/9c8;

    .line 1515998
    new-instance v0, LX/9c8;

    const-string v1, "Entry_POINT"

    const/16 v2, 0xf

    const-string v3, "entry_point"

    invoke-direct {v0, v1, v2, v3}, LX/9c8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c8;->Entry_POINT:LX/9c8;

    .line 1515999
    new-instance v0, LX/9c8;

    const-string v1, "MEDIA_ITEM_URI"

    const/16 v2, 0x10

    const-string v3, "media_uri"

    invoke-direct {v0, v1, v2, v3}, LX/9c8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c8;->MEDIA_ITEM_URI:LX/9c8;

    .line 1516000
    const/16 v0, 0x11

    new-array v0, v0, [LX/9c8;

    sget-object v1, LX/9c8;->MEDIA_ITEM_IDENTIFIER:LX/9c8;

    aput-object v1, v0, v4

    sget-object v1, LX/9c8;->NUMBER_OF_UG_ENTRIES:LX/9c8;

    aput-object v1, v0, v5

    sget-object v1, LX/9c8;->NUMBER_OF_STICKERS_ENTRIES:LX/9c8;

    aput-object v1, v0, v6

    sget-object v1, LX/9c8;->NUMBER_OF_TEXT_ENTRIES:LX/9c8;

    aput-object v1, v0, v7

    sget-object v1, LX/9c8;->NUMBER_OF_CROP_ENTRIES:LX/9c8;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/9c8;->NUMBER_OF_FILTER_ENTRIES:LX/9c8;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/9c8;->NUMBER_OF_SWIPES:LX/9c8;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/9c8;->NUMBER_OF_STICKER_ON_PHOTO:LX/9c8;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/9c8;->NUMBER_OF_TEXT_ON_PHOTO:LX/9c8;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/9c8;->NUMBER_OF_DOODLE_ON_PHOTO:LX/9c8;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/9c8;->APPLIED_FILTER_NAME:LX/9c8;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/9c8;->IS_PHOTO_CROPPED:LX/9c8;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/9c8;->IS_PHOTO_ROTATED:LX/9c8;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/9c8;->IS_PHOTO_DELETED:LX/9c8;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/9c8;->IS_PHOTO_PUBLISHED:LX/9c8;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/9c8;->Entry_POINT:LX/9c8;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/9c8;->MEDIA_ITEM_URI:LX/9c8;

    aput-object v2, v0, v1

    sput-object v0, LX/9c8;->$VALUES:[LX/9c8;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1515979
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1515980
    iput-object p3, p0, LX/9c8;->name:Ljava/lang/String;

    .line 1515981
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9c8;
    .locals 1

    .prologue
    .line 1515982
    const-class v0, LX/9c8;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9c8;

    return-object v0
.end method

.method public static values()[LX/9c8;
    .locals 1

    .prologue
    .line 1515978
    sget-object v0, LX/9c8;->$VALUES:[LX/9c8;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9c8;

    return-object v0
.end method


# virtual methods
.method public final getParamKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1515977
    iget-object v0, p0, LX/9c8;->name:Ljava/lang/String;

    return-object v0
.end method
