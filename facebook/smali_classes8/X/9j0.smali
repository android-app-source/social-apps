.class public LX/9j0;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/8Jn;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final b:F

.field public c:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

.field public d:Z

.field public e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/9ij;",
            "LX/8Jk;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/9ij;

.field public g:LX/9il;

.field public h:LX/8Jm;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;)V
    .locals 3

    .prologue
    .line 1528962
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1528963
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/9j0;->e:Ljava/util/Map;

    .line 1528964
    iput-object p2, p0, LX/9j0;->c:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    .line 1528965
    invoke-virtual {p0}, LX/9j0;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b100d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-virtual {p0}, LX/9j0;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b100c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    add-float/2addr v0, v1

    iput v0, p0, LX/9j0;->b:F

    .line 1528966
    const-class v0, LX/9j0;

    invoke-static {v0, p0}, LX/9j0;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1528967
    iget-object v0, p0, LX/9j0;->a:LX/8Jn;

    invoke-virtual {p0}, LX/9j0;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1016

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v0, p0, v1}, LX/8Jn;->a(Landroid/view/View;F)LX/8Jm;

    move-result-object v0

    iput-object v0, p0, LX/9j0;->h:LX/8Jm;

    .line 1528968
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/9j0;

    const-class p0, LX/8Jn;

    invoke-interface {v1, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/8Jn;

    iput-object v1, p1, LX/9j0;->a:LX/8Jn;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1528954
    iget-object v0, p0, LX/9j0;->f:LX/9ij;

    if-eqz v0, :cond_1

    .line 1528955
    iget-object v0, p0, LX/9j0;->f:LX/9ij;

    .line 1528956
    iget-object v1, v0, LX/9ij;->e:Lcom/facebook/photos/base/tagging/Tag;

    move-object v0, v1

    .line 1528957
    iget-boolean v1, v0, Lcom/facebook/photos/base/tagging/Tag;->e:Z

    move v0, v1

    .line 1528958
    if-nez v0, :cond_0

    .line 1528959
    iget-object v0, p0, LX/9j0;->f:LX/9ij;

    invoke-virtual {v0}, LX/9ij;->e()V

    .line 1528960
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/9j0;->f:LX/9ij;

    .line 1528961
    :cond_1
    return-void
.end method

.method public final a(Lcom/facebook/photos/base/tagging/Tag;)V
    .locals 3

    .prologue
    .line 1528969
    iget-object v0, p0, LX/9j0;->f:LX/9ij;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9j0;->f:LX/9ij;

    .line 1528970
    iget-object v1, v0, LX/9ij;->e:Lcom/facebook/photos/base/tagging/Tag;

    move-object v0, v1

    .line 1528971
    if-ne v0, p1, :cond_0

    .line 1528972
    const/4 v0, 0x0

    iput-object v0, p0, LX/9j0;->f:LX/9ij;

    .line 1528973
    :cond_0
    iget-object v0, p0, LX/9j0;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9ij;

    .line 1528974
    iget-object v2, v0, LX/9ij;->e:Lcom/facebook/photos/base/tagging/Tag;

    move-object v2, v2

    .line 1528975
    if-ne v2, p1, :cond_1

    .line 1528976
    invoke-virtual {p0, v0}, LX/9j0;->removeView(Landroid/view/View;)V

    .line 1528977
    iget-object v0, p0, LX/9j0;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1528978
    iget-object v0, p0, LX/9j0;->h:LX/8Jm;

    iget-object v1, p0, LX/9j0;->e:Ljava/util/Map;

    invoke-virtual {v0, v1}, LX/8Jm;->a(Ljava/util/Map;)V

    .line 1528979
    :cond_2
    return-void
.end method

.method public final a(Ljava/util/List;Z)V
    .locals 6
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/base/tagging/Tag;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 1528924
    if-nez p1, :cond_0

    .line 1528925
    sget-object p1, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    .line 1528926
    :cond_0
    iput-boolean p2, p0, LX/9j0;->d:Z

    .line 1528927
    invoke-virtual {p0}, LX/9j0;->removeAllViews()V

    .line 1528928
    iget-object v0, p0, LX/9j0;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1528929
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/tagging/Tag;

    .line 1528930
    const/4 p2, -0x2

    .line 1528931
    new-instance v2, LX/9ij;

    invoke-virtual {p0}, LX/9j0;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-boolean v4, p0, LX/9j0;->d:Z

    invoke-direct {v2, v3, v0, v4}, LX/9ij;-><init>(Landroid/content/Context;Lcom/facebook/photos/base/tagging/Tag;Z)V

    .line 1528932
    new-instance v3, LX/9j2;

    invoke-virtual {p0}, LX/9j0;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, LX/9j0;->c:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    new-instance p1, LX/9iy;

    invoke-direct {p1, p0, v2, v0}, LX/9iy;-><init>(LX/9j0;LX/9ij;Lcom/facebook/photos/base/tagging/Tag;)V

    invoke-direct {v3, v4, v5, p1}, LX/9j2;-><init>(Landroid/content/Context;Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;LX/9iy;)V

    .line 1528933
    invoke-virtual {v2, v3}, LX/9ij;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1528934
    new-instance v3, LX/9iz;

    invoke-direct {v3, p0}, LX/9iz;-><init>(LX/9j0;)V

    .line 1528935
    iput-object v3, v2, LX/9ij;->j:LX/9ii;

    .line 1528936
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, p2, p2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1528937
    const/4 v4, 0x1

    iput v4, v3, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 1528938
    const/4 v4, 0x4

    invoke-virtual {v2, v4}, LX/9ij;->setVisibility(I)V

    .line 1528939
    invoke-virtual {p0, v2, v3}, LX/9j0;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1528940
    move-object v2, v2

    .line 1528941
    iget-object v3, p0, LX/9j0;->e:Ljava/util/Map;

    new-instance v4, LX/8Jk;

    .line 1528942
    iget-object v5, v0, Lcom/facebook/photos/base/tagging/Tag;->a:Lcom/facebook/photos/base/tagging/TagTarget;

    move-object v5, v5

    .line 1528943
    invoke-interface {v5}, Lcom/facebook/photos/base/tagging/TagTarget;->e()Landroid/graphics/PointF;

    move-result-object v5

    .line 1528944
    iget-object p1, v0, Lcom/facebook/photos/base/tagging/Tag;->a:Lcom/facebook/photos/base/tagging/TagTarget;

    move-object v0, p1

    .line 1528945
    invoke-interface {v0}, Lcom/facebook/photos/base/tagging/TagTarget;->d()Landroid/graphics/RectF;

    move-result-object v0

    invoke-direct {v4, v5, v0}, LX/8Jk;-><init>(Landroid/graphics/PointF;Landroid/graphics/RectF;)V

    invoke-interface {v3, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1528946
    :cond_1
    iget-object v0, p0, LX/9j0;->h:LX/8Jm;

    iget-object v1, p0, LX/9j0;->e:Ljava/util/Map;

    invoke-virtual {v0, v1}, LX/8Jm;->a(Ljava/util/Map;)V

    .line 1528947
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 1528952
    iget-object v0, p0, LX/9j0;->h:LX/8Jm;

    iget-object v1, p0, LX/9j0;->c:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    invoke-virtual {v0, v1, p1}, LX/8Jm;->a(LX/7UZ;Z)V

    .line 1528953
    return-void
.end method

.method public setFaceBoxRects(Ljava/util/List;)V
    .locals 2
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/RectF;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1528948
    if-nez p1, :cond_0

    .line 1528949
    iget-object v0, p0, LX/9j0;->h:LX/8Jm;

    sget-object v1, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/8Jm;->a(Ljava/util/Collection;)V

    .line 1528950
    :goto_0
    return-void

    .line 1528951
    :cond_0
    iget-object v0, p0, LX/9j0;->h:LX/8Jm;

    invoke-virtual {v0, p1}, LX/8Jm;->a(Ljava/util/Collection;)V

    goto :goto_0
.end method
