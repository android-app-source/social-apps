.class public final LX/AGA;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 17

    .prologue
    .line 1651420
    const/4 v12, 0x0

    .line 1651421
    const/4 v11, 0x0

    .line 1651422
    const/4 v10, 0x0

    .line 1651423
    const/4 v9, 0x0

    .line 1651424
    const/4 v8, 0x0

    .line 1651425
    const/4 v5, 0x0

    .line 1651426
    const-wide/16 v6, 0x0

    .line 1651427
    const/4 v4, 0x0

    .line 1651428
    const/4 v3, 0x0

    .line 1651429
    const/4 v2, 0x0

    .line 1651430
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v13, v14, :cond_c

    .line 1651431
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1651432
    const/4 v2, 0x0

    .line 1651433
    :goto_0
    return v2

    .line 1651434
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_9

    .line 1651435
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1651436
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1651437
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v15, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v15, :cond_0

    if-eqz v2, :cond_0

    .line 1651438
    const-string v6, "backstage_message"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1651439
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v14, v2

    goto :goto_1

    .line 1651440
    :cond_1
    const-string v6, "backstage_post_type"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1651441
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v13, v2

    goto :goto_1

    .line 1651442
    :cond_2
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1651443
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v12, v2

    goto :goto_1

    .line 1651444
    :cond_3
    const-string v6, "post_media"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1651445
    invoke-static/range {p0 .. p1}, LX/AG2;->a(LX/15w;LX/186;)I

    move-result v2

    move v11, v2

    goto :goto_1

    .line 1651446
    :cond_4
    const-string v6, "reactions"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1651447
    invoke-static/range {p0 .. p1}, LX/AG9;->a(LX/15w;LX/186;)I

    move-result v2

    move v10, v2

    goto :goto_1

    .line 1651448
    :cond_5
    const-string v6, "seen_by_users"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1651449
    invoke-static/range {p0 .. p1}, LX/AGN;->a(LX/15w;LX/186;)I

    move-result v2

    move v7, v2

    goto :goto_1

    .line 1651450
    :cond_6
    const-string v6, "time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1651451
    const/4 v2, 0x1

    .line 1651452
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 1651453
    :cond_7
    const-string v6, "timezone_offset_seconds"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1651454
    const/4 v2, 0x1

    .line 1651455
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v8, v2

    move v9, v6

    goto/16 :goto_1

    .line 1651456
    :cond_8
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1651457
    :cond_9
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1651458
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1651459
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1651460
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1651461
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1651462
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1651463
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1651464
    if-eqz v3, :cond_a

    .line 1651465
    const/4 v3, 0x6

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1651466
    :cond_a
    if-eqz v8, :cond_b

    .line 1651467
    const/4 v2, 0x7

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9, v3}, LX/186;->a(III)V

    .line 1651468
    :cond_b
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_c
    move v13, v11

    move v14, v12

    move v11, v9

    move v12, v10

    move v10, v8

    move v9, v4

    move v8, v2

    move/from16 v16, v5

    move-wide v4, v6

    move/from16 v7, v16

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 1651469
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1651470
    invoke-virtual {p0, p1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1651471
    if-eqz v0, :cond_0

    .line 1651472
    const-string v1, "backstage_message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1651473
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1651474
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1651475
    if-eqz v0, :cond_1

    .line 1651476
    const-string v1, "backstage_post_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1651477
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1651478
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1651479
    if-eqz v0, :cond_2

    .line 1651480
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1651481
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1651482
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1651483
    if-eqz v0, :cond_3

    .line 1651484
    const-string v1, "post_media"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1651485
    invoke-static {p0, v0, p2, p3}, LX/AG2;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1651486
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1651487
    if-eqz v0, :cond_4

    .line 1651488
    const-string v1, "reactions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1651489
    invoke-static {p0, v0, p2, p3}, LX/AG9;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1651490
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1651491
    if-eqz v0, :cond_5

    .line 1651492
    const-string v1, "seen_by_users"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1651493
    invoke-static {p0, v0, p2, p3}, LX/AGN;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1651494
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1651495
    cmp-long v2, v0, v4

    if-eqz v2, :cond_6

    .line 1651496
    const-string v2, "time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1651497
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1651498
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1651499
    if-eqz v0, :cond_7

    .line 1651500
    const-string v1, "timezone_offset_seconds"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1651501
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1651502
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1651503
    return-void
.end method
