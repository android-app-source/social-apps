.class public LX/9E2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;
.implements LX/21o;
.implements LX/21p;
.implements LX/21m;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public final a:Lcom/facebook/feedback/ui/CommentComposerHelper;

.field public final b:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0ad;

.field public d:LX/9FA;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:LX/9CP;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;

.field public i:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/CommentComposerHelper;LX/0ad;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1456461
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1456462
    iput-object p1, p0, LX/9E2;->a:Lcom/facebook/feedback/ui/CommentComposerHelper;

    .line 1456463
    iput-object p2, p0, LX/9E2;->c:LX/0ad;

    .line 1456464
    new-instance v0, LX/9E0;

    invoke-direct {v0, p0}, LX/9E0;-><init>(LX/9E2;)V

    iput-object v0, p0, LX/9E2;->b:LX/0QK;

    .line 1456465
    return-void
.end method

.method private a(Lcom/facebook/ipc/media/StickerItem;)V
    .locals 2

    .prologue
    .line 1456455
    iget-object v0, p0, LX/9E2;->g:LX/9CP;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1456456
    invoke-direct {p0, p1}, LX/9E2;->b(Lcom/facebook/ipc/media/StickerItem;)Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;

    move-result-object v0

    .line 1456457
    if-nez v0, :cond_1

    .line 1456458
    :goto_1
    return-void

    .line 1456459
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1456460
    :cond_1
    iget-object v1, p0, LX/9E2;->g:LX/9CP;

    invoke-virtual {v1, v0}, LX/9CP;->a(Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;)V

    goto :goto_1
.end method

.method private b(Lcom/facebook/ipc/media/StickerItem;)Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;
    .locals 10
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 1456450
    iget-object v0, p0, LX/9E2;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9E2;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;

    iget-object v1, p0, LX/9E2;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/9E2;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    .line 1456451
    iget-object v4, v3, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->n:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-virtual {v4}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getEncodedText()Ljava/lang/String;

    move-result-object v4

    move-object v3, v4

    .line 1456452
    const/4 v4, 0x1

    iget-object v6, p0, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    .line 1456453
    iget-object v7, v6, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->w:Lcom/facebook/ipc/media/MediaItem;

    move-object v6, v7

    .line 1456454
    move-object v7, p1

    move v8, v5

    move v9, v5

    invoke-direct/range {v0 .. v9}, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLcom/facebook/ipc/media/MediaItem;Lcom/facebook/ipc/media/StickerItem;ZI)V

    goto :goto_0
.end method

.method private j()V
    .locals 3

    .prologue
    .line 1456447
    iget-object v0, p0, LX/9E2;->g:LX/9CP;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    if-nez v0, :cond_1

    .line 1456448
    :cond_0
    :goto_0
    return-void

    .line 1456449
    :cond_1
    iget-object v0, p0, LX/9E2;->g:LX/9CP;

    iget-object v1, p0, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    invoke-virtual {v1}, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    invoke-virtual {v0, v1, v2, p0}, LX/9CP;->a(Landroid/content/Context;Landroid/view/View;LX/21p;)V

    goto :goto_0
.end method

.method public static k(LX/9E2;)V
    .locals 6

    .prologue
    .line 1456438
    iget-object v0, p0, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9E2;->g:LX/9CP;

    if-nez v0, :cond_1

    .line 1456439
    :cond_0
    :goto_0
    return-void

    .line 1456440
    :cond_1
    iget-object v0, p0, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    iget-object v1, p0, LX/9E2;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v1}, Lcom/facebook/feedback/ui/CommentComposerHelper;->c(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v1

    iget-object v2, p0, LX/9E2;->g:LX/9CP;

    invoke-virtual {v2, p0}, LX/9CP;->a(LX/21p;)Z

    move-result v2

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1456441
    iget-object p0, v0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->q:Lcom/facebook/feedback/ui/CommentComposerPostButton;

    if-eqz v1, :cond_4

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->i(Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;)Z

    move-result v3

    if-eqz v3, :cond_4

    move v3, v4

    :goto_1
    invoke-virtual {p0, v3}, Lcom/facebook/feedback/ui/CommentComposerPostButton;->setShowSticker(Z)V

    .line 1456442
    iget-object p0, v0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->q:Lcom/facebook/feedback/ui/CommentComposerPostButton;

    if-eqz v1, :cond_5

    if-eqz v2, :cond_5

    move v3, v4

    :goto_2
    invoke-virtual {p0, v3}, Lcom/facebook/feedback/ui/CommentComposerPostButton;->setSelected(Z)V

    .line 1456443
    iget-object v3, v0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->q:Lcom/facebook/feedback/ui/CommentComposerPostButton;

    if-nez v1, :cond_2

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->i(Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;)Z

    move-result p0

    if-nez p0, :cond_3

    :cond_2
    move v5, v4

    :cond_3
    invoke-virtual {v3, v5}, Lcom/facebook/feedback/ui/CommentComposerPostButton;->setEnabled(Z)V

    .line 1456444
    goto :goto_0

    :cond_4
    move v3, v5

    .line 1456445
    goto :goto_1

    :cond_5
    move v3, v5

    .line 1456446
    goto :goto_2
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1456434
    iget-object v0, p0, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1456435
    iget-object v0, p0, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    invoke-virtual {v0}, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->f()V

    .line 1456436
    return-void

    .line 1456437
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;Z)V
    .locals 1
    .param p1    # Lcom/facebook/graphql/model/GraphQLFeedback;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1456433
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Lcom/facebook/stickers/model/Sticker;)V
    .locals 5
    .param p1    # Lcom/facebook/stickers/model/Sticker;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1456419
    iget-object v0, p0, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/facebook/stickers/model/Sticker;->c:Landroid/net/Uri;

    if-nez v0, :cond_1

    .line 1456420
    :cond_0
    :goto_0
    return-void

    .line 1456421
    :cond_1
    iget-object v0, p0, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    invoke-virtual {v0}, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->f()V

    .line 1456422
    invoke-static {}, Lcom/facebook/ipc/media/StickerItem;->b()LX/4gH;

    move-result-object v0

    .line 1456423
    iput-object p1, v0, LX/4gH;->d:Lcom/facebook/stickers/model/Sticker;

    .line 1456424
    move-object v0, v0

    .line 1456425
    iget-object v1, p1, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1456426
    iput-wide v2, v0, LX/4gH;->c:J

    .line 1456427
    move-object v0, v0

    .line 1456428
    iget-object v1, p1, Lcom/facebook/stickers/model/Sticker;->c:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1456429
    iput-object v1, v0, LX/4gH;->b:Ljava/lang/String;

    .line 1456430
    move-object v0, v0

    .line 1456431
    invoke-virtual {v0}, LX/4gH;->a()Lcom/facebook/ipc/media/StickerItem;

    move-result-object v0

    .line 1456432
    invoke-direct {p0, v0}, LX/9E2;->a(Lcom/facebook/ipc/media/StickerItem;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;)V
    .locals 3
    .param p1    # Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1456407
    iget-object v0, p0, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9E2;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-nez v0, :cond_1

    .line 1456408
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "cannot restore pending reply if inline reply composer is already unbound"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1456409
    :cond_1
    iget-object v0, p0, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    invoke-virtual {v0}, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->f()V

    .line 1456410
    if-nez p1, :cond_2

    .line 1456411
    :goto_0
    return-void

    .line 1456412
    :cond_2
    iget-object v0, p0, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    iget-object v1, p1, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->c:Ljava/lang/String;

    .line 1456413
    iget-object v2, v0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->n:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1456414
    iget-object v0, p1, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->f:Lcom/facebook/ipc/media/MediaItem;

    if-eqz v0, :cond_3

    .line 1456415
    iget-object v0, p1, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->f:Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {p0, v0}, LX/9E2;->setMediaItem(Lcom/facebook/ipc/media/MediaItem;)V

    .line 1456416
    :cond_3
    iget-object v0, p0, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    .line 1456417
    iget-object v1, v0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->n:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    iget-object v2, v0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->n:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-virtual {v2}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->setSelection(I)V

    .line 1456418
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1456406
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 1456396
    if-eqz p1, :cond_1

    .line 1456397
    invoke-direct {p0}, LX/9E2;->j()V

    .line 1456398
    :cond_0
    :goto_0
    return-void

    .line 1456399
    :cond_1
    iget-object v0, p0, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    if-eqz v0, :cond_0

    .line 1456400
    iget-object v0, p0, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    const/4 p1, 0x1

    .line 1456401
    iget-object p0, v0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->n:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-virtual {p0, p1}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->setFocusable(Z)V

    .line 1456402
    iget-object p0, v0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->n:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-virtual {p0, p1}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->setFocusableInTouchMode(Z)V

    .line 1456403
    iget-object p0, v0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->n:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-virtual {p0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->requestFocus()Z

    .line 1456404
    iget-object p0, v0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->n:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    new-instance p1, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView$1;

    invoke-direct {p1, v0}, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView$1;-><init>(Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;)V

    invoke-virtual {p0, p1}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->post(Ljava/lang/Runnable;)Z

    .line 1456405
    goto :goto_0
.end method

.method public final a(II)Z
    .locals 1

    .prologue
    .line 1456393
    iget-object v0, p0, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    .line 1456394
    invoke-static {v0, p1, p2}, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->f(Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;II)Z

    move-result p0

    if-eqz p0, :cond_0

    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->j(Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;)Z

    move-result p0

    if-nez p0, :cond_3

    :cond_0
    const/4 p0, 0x1

    :goto_0
    move v0, p0

    .line 1456395
    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 1456391
    invoke-static {p0}, LX/9E2;->k(LX/9E2;)V

    .line 1456392
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1456466
    iget-object v0, p0, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1456467
    iget-object v0, p0, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    invoke-virtual {v0}, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    invoke-static {v0, v1}, LX/2Na;->a(Landroid/content/Context;Landroid/view/View;)V

    .line 1456468
    return-void

    .line 1456469
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1456358
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1456361
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 1456359
    invoke-static {p0}, LX/9E2;->k(LX/9E2;)V

    .line 1456360
    return-void
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 1456362
    invoke-static {p0}, LX/9E2;->k(LX/9E2;)V

    .line 1456363
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 1456364
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/9E2;->a(Lcom/facebook/ipc/media/StickerItem;)V

    .line 1456365
    return-void
.end method

.method public final g()V
    .locals 0

    .prologue
    .line 1456366
    invoke-direct {p0}, LX/9E2;->j()V

    .line 1456367
    return-void
.end method

.method public final getPendingComment()Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1456368
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/9E2;->b(Lcom/facebook/ipc/media/StickerItem;)Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;

    move-result-object v0

    return-object v0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 1456369
    iget-object v0, p0, LX/9E2;->g:LX/9CP;

    if-nez v0, :cond_0

    .line 1456370
    :goto_0
    return-void

    .line 1456371
    :cond_0
    iget-object v0, p0, LX/9E2;->g:LX/9CP;

    invoke-virtual {v0}, LX/9CP;->j()V

    goto :goto_0
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1456372
    return-void
.end method

.method public final setIsVisible(Z)V
    .locals 2

    .prologue
    .line 1456373
    iget-object v0, p0, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    if-nez v0, :cond_0

    .line 1456374
    :goto_0
    return-void

    .line 1456375
    :cond_0
    iget-object v1, p0, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    const/16 v0, 0x8

    goto :goto_1
.end method

.method public final setMediaItem(Lcom/facebook/ipc/media/MediaItem;)V
    .locals 1

    .prologue
    .line 1456376
    iget-object v0, p0, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    if-nez v0, :cond_0

    .line 1456377
    :goto_0
    return-void

    .line 1456378
    :cond_0
    iget-object v0, p0, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    invoke-virtual {v0, p1}, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->setMediaItem(Lcom/facebook/ipc/media/MediaItem;)V

    .line 1456379
    invoke-static {p0}, LX/9E2;->k(LX/9E2;)V

    goto :goto_0
.end method

.method public final setMediaPickerListener(LX/9DF;)V
    .locals 3

    .prologue
    .line 1456380
    iget-object v0, p0, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    if-nez v0, :cond_0

    .line 1456381
    :goto_0
    return-void

    .line 1456382
    :cond_0
    if-nez p1, :cond_1

    .line 1456383
    iget-object v0, p0, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->setMediaPickerListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 1456384
    :cond_1
    iget-object v0, p0, LX/9E2;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v0}, Lcom/facebook/feedback/ui/CommentComposerHelper;->d(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v0

    .line 1456385
    iget-object v1, p0, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    new-instance v2, LX/9E1;

    invoke-direct {v2, p0, p1, v0}, LX/9E1;-><init>(LX/9E2;LX/9DF;Z)V

    invoke-virtual {v1, v2}, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->setMediaPickerListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnFocusChangeListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1456386
    iget-object v0, p0, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    if-nez v0, :cond_0

    .line 1456387
    :goto_0
    return-void

    .line 1456388
    :cond_0
    iget-object v0, p0, LX/9E2;->f:Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;

    .line 1456389
    iget-object p0, v0, Lcom/facebook/feedback/ui/rows/views/InlineReplyComposerView;->n:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-virtual {p0, p1}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1456390
    goto :goto_0
.end method
