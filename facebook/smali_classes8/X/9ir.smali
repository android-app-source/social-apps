.class public final LX/9ir;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8JT;


# instance fields
.field public final synthetic a:LX/9iw;


# direct methods
.method public constructor <init>(LX/9iw;)V
    .locals 0

    .prologue
    .line 1528663
    iput-object p1, p0, LX/9ir;->a:LX/9iw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1528664
    iget-object v0, p0, LX/9ir;->a:LX/9iw;

    invoke-virtual {v0}, LX/9iw;->b()V

    .line 1528665
    return-void
.end method

.method public final a(Lcom/facebook/tagging/model/TaggingProfile;ILjava/lang/String;)V
    .locals 11

    .prologue
    const/4 v8, 0x0

    const/4 v6, 0x1

    .line 1528666
    iget-object v0, p0, LX/9ir;->a:LX/9iw;

    iget-object v2, v0, LX/9iw;->g:Lcom/facebook/photos/base/tagging/TagTarget;

    .line 1528667
    instance-of v0, v2, Lcom/facebook/photos/base/tagging/FaceBox;

    if-eqz v0, :cond_6

    .line 1528668
    iget-object v0, p0, LX/9ir;->a:LX/9iw;

    iget-object v0, v0, LX/9iw;->g:Lcom/facebook/photos/base/tagging/TagTarget;

    check-cast v0, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1528669
    iget-object v1, p0, LX/9ir;->a:LX/9iw;

    iget-object v1, v1, LX/9iw;->m:LX/9ic;

    if-eqz v1, :cond_2

    .line 1528670
    iget-object v0, p0, LX/9ir;->a:LX/9iw;

    iget-object v1, v0, LX/9iw;->m:LX/9ic;

    iget-object v0, p0, LX/9ir;->a:LX/9iw;

    iget-object v0, v0, LX/9iw;->g:Lcom/facebook/photos/base/tagging/TagTarget;

    check-cast v0, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1528671
    iget-object v2, v1, LX/9ic;->b:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 1528672
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    if-eq v3, v0, :cond_1

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/photos/base/tagging/FaceBox;

    invoke-static {v3, v0}, LX/9ic;->a(Lcom/facebook/photos/base/tagging/FaceBox;Lcom/facebook/photos/base/tagging/FaceBox;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1528673
    :cond_1
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1528674
    :goto_0
    move-object v0, v2

    .line 1528675
    if-nez v0, :cond_2

    .line 1528676
    iget-object v0, p0, LX/9ir;->a:LX/9iw;

    invoke-virtual {v0}, LX/9iw;->b()V

    .line 1528677
    :goto_1
    return-void

    .line 1528678
    :cond_2
    iput-boolean v6, v0, Lcom/facebook/photos/base/tagging/FaceBox;->f:Z

    .line 1528679
    move-object v2, v0

    .line 1528680
    :cond_3
    :goto_2
    new-instance v1, Lcom/facebook/photos/base/tagging/Tag;

    .line 1528681
    iget-object v0, p1, Lcom/facebook/tagging/model/TaggingProfile;->a:Lcom/facebook/user/model/Name;

    move-object v3, v0

    .line 1528682
    iget-wide v9, p1, Lcom/facebook/tagging/model/TaggingProfile;->b:J

    move-wide v4, v9

    .line 1528683
    iget-object v0, p1, Lcom/facebook/tagging/model/TaggingProfile;->e:LX/7Gr;

    move-object v7, v0

    .line 1528684
    invoke-direct/range {v1 .. v7}, Lcom/facebook/photos/base/tagging/Tag;-><init>(Lcom/facebook/photos/base/tagging/TagTarget;Lcom/facebook/user/model/Name;JZLX/7Gr;)V

    .line 1528685
    iget-object v0, p1, Lcom/facebook/tagging/model/TaggingProfile;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1528686
    iput-object v0, v1, Lcom/facebook/photos/base/tagging/Tag;->i:Ljava/lang/String;

    .line 1528687
    instance-of v0, v2, Lcom/facebook/photos/base/tagging/FaceBox;

    if-eqz v0, :cond_4

    move-object v0, v2

    .line 1528688
    check-cast v0, Lcom/facebook/photos/base/tagging/FaceBox;

    invoke-virtual {v0}, Lcom/facebook/photos/base/tagging/FaceBox;->i()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/photos/base/tagging/Tag;->a(Ljava/util/Map;)V

    .line 1528689
    check-cast v2, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1528690
    iput-boolean v6, v2, Lcom/facebook/photos/base/tagging/FaceBox;->f:Z

    .line 1528691
    :cond_4
    iget-object v0, p0, LX/9ir;->a:LX/9iw;

    .line 1528692
    iput-boolean v6, v0, LX/9iw;->l:Z

    .line 1528693
    iget-object v0, p0, LX/9ir;->a:LX/9iw;

    iget-object v0, v0, LX/9iw;->k:LX/BJ0;

    invoke-virtual {v0, v1}, LX/BJ0;->a(Lcom/facebook/photos/base/tagging/Tag;)V

    .line 1528694
    const/4 v0, 0x0

    .line 1528695
    iget-object v1, p0, LX/9ir;->a:LX/9iw;

    iget-object v1, v1, LX/9iw;->g:Lcom/facebook/photos/base/tagging/TagTarget;

    instance-of v1, v1, Lcom/facebook/photos/base/tagging/FaceBox;

    if-eqz v1, :cond_5

    .line 1528696
    iget-object v1, p0, LX/9ir;->a:LX/9iw;

    iget-object v0, p0, LX/9ir;->a:LX/9iw;

    iget-object v0, v0, LX/9iw;->g:Lcom/facebook/photos/base/tagging/TagTarget;

    check-cast v0, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1528697
    iget-object v2, v1, LX/9iw;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_b

    .line 1528698
    iget-object v2, v1, LX/9iw;->e:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 1528699
    add-int/lit8 v3, v2, 0x1

    iget-object v4, v1, LX/9iw;->e:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_a

    add-int/lit8 v2, v2, 0x1

    .line 1528700
    :goto_3
    iget-object v3, v1, LX/9iw;->e:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1528701
    :goto_4
    move-object v0, v2

    .line 1528702
    iget-object v1, p0, LX/9ir;->a:LX/9iw;

    iget-object v1, v1, LX/9iw;->e:Ljava/util/List;

    iget-object v2, p0, LX/9ir;->a:LX/9iw;

    iget-object v2, v2, LX/9iw;->g:Lcom/facebook/photos/base/tagging/TagTarget;

    invoke-interface {v1, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1528703
    iget-object v1, p0, LX/9ir;->a:LX/9iw;

    iget-object v1, v1, LX/9iw;->c:LX/BIz;

    invoke-virtual {v1}, LX/BIz;->a()LX/9ip;

    move-result-object v1

    invoke-virtual {v1}, LX/9ip;->m()V

    .line 1528704
    :cond_5
    if-eqz v0, :cond_7

    .line 1528705
    iget-object v1, p0, LX/9ir;->a:LX/9iw;

    invoke-static {v1, v0, v8, v6}, LX/9iw;->a$redex0(LX/9iw;Lcom/facebook/photos/base/tagging/TagTarget;ZZ)V

    goto/16 :goto_1

    .line 1528706
    :cond_6
    instance-of v0, v2, Lcom/facebook/photos/base/tagging/TagPoint;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/9ir;->a:LX/9iw;

    iget-object v0, v0, LX/9iw;->m:LX/9ic;

    if-eqz v0, :cond_3

    .line 1528707
    iget-object v0, p0, LX/9ir;->a:LX/9iw;

    iget-object v0, v0, LX/9iw;->f:LX/74x;

    instance-of v0, v0, Lcom/facebook/photos/base/photos/LocalPhoto;

    if-eqz v0, :cond_8

    .line 1528708
    iget-object v0, p0, LX/9ir;->a:LX/9iw;

    iget-object v0, v0, LX/9iw;->f:LX/74x;

    check-cast v0, Lcom/facebook/photos/base/photos/LocalPhoto;

    .line 1528709
    iget v1, v0, Lcom/facebook/photos/base/photos/LocalPhoto;->e:I

    move v0, v1

    .line 1528710
    move v1, v0

    .line 1528711
    :goto_5
    iget-object v0, p0, LX/9ir;->a:LX/9iw;

    iget-object v2, v0, LX/9iw;->m:LX/9ic;

    iget-object v0, p0, LX/9ir;->a:LX/9iw;

    iget-object v0, v0, LX/9iw;->g:Lcom/facebook/photos/base/tagging/TagTarget;

    check-cast v0, Lcom/facebook/photos/base/tagging/TagPoint;

    const/4 p2, 0x1

    const/4 v10, 0x0

    const/high16 v9, 0x3f000000    # 0.5f

    .line 1528712
    iget-object v3, v2, LX/9ic;->c:Landroid/graphics/RectF;

    if-nez v3, :cond_c

    if-nez v1, :cond_c

    .line 1528713
    :goto_6
    move-object v2, v0

    .line 1528714
    goto/16 :goto_2

    .line 1528715
    :cond_7
    iget-object v0, p0, LX/9ir;->a:LX/9iw;

    invoke-virtual {v0}, LX/9iw;->b()V

    goto/16 :goto_1

    :cond_8
    move v1, v8

    goto :goto_5

    :cond_9
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1528716
    :cond_a
    const/4 v2, 0x0

    goto :goto_3

    .line 1528717
    :cond_b
    const/4 v2, 0x0

    goto :goto_4

    .line 1528718
    :cond_c
    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    .line 1528719
    iget-object v4, v2, LX/9ic;->c:Landroid/graphics/RectF;

    sget-object v5, LX/9ic;->a:Landroid/graphics/RectF;

    sget-object v7, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v3, v4, v5, v7}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 1528720
    int-to-float v4, v1

    invoke-virtual {v3, v4, v9, v9}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 1528721
    new-instance v4, Landroid/graphics/Matrix;

    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    .line 1528722
    invoke-virtual {v3, v4}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 1528723
    const/4 v3, 0x2

    new-array v5, v3, [F

    invoke-virtual {v0}, Lcom/facebook/photos/base/tagging/TagPoint;->f()Landroid/graphics/PointF;

    move-result-object v3

    iget v3, v3, Landroid/graphics/PointF;->x:F

    aput v3, v5, v10

    invoke-virtual {v0}, Lcom/facebook/photos/base/tagging/TagPoint;->f()Landroid/graphics/PointF;

    move-result-object v3

    iget v3, v3, Landroid/graphics/PointF;->y:F

    aput v3, v5, p2

    .line 1528724
    invoke-virtual {v4, v5}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 1528725
    new-instance v3, Lcom/facebook/photos/base/tagging/TagPoint;

    new-instance v4, Landroid/graphics/PointF;

    aget v7, v5, v10

    aget v5, v5, p2

    invoke-direct {v4, v7, v5}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v0}, Lcom/facebook/photos/base/tagging/TagPoint;->n()Ljava/util/List;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/facebook/photos/base/tagging/TagPoint;-><init>(Landroid/graphics/PointF;Ljava/util/List;)V

    move-object v0, v3

    .line 1528726
    goto :goto_6
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1528727
    iget-object v0, p0, LX/9ir;->a:LX/9iw;

    iget-object v0, v0, LX/9iw;->k:LX/BJ0;

    .line 1528728
    iget-object p0, v0, LX/BJ0;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    iget-object p0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->h:LX/74G;

    .line 1528729
    sget-object v0, LX/74F;->START_TYPING:LX/74F;

    invoke-static {v0}, LX/74G;->a(LX/74F;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-static {p0, v0}, LX/74G;->a(LX/74G;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1528730
    return-void
.end method
