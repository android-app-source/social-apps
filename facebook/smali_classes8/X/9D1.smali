.class public LX/9D1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/9CC;

.field public final b:LX/9Br;

.field public c:LX/0g8;

.field private d:Lcom/facebook/graphql/model/GraphQLComment;

.field private e:I

.field public f:Z

.field public g:I

.field public h:LX/9D0;

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0fx;",
            ">;"
        }
    .end annotation
.end field

.field public j:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:LX/9Im;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/9CC;LX/9Br;)V
    .locals 2

    .prologue
    .line 1454667
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1454668
    iput-object p1, p0, LX/9D1;->a:LX/9CC;

    .line 1454669
    iput-object p2, p0, LX/9D1;->b:LX/9Br;

    .line 1454670
    new-instance v0, LX/9D0;

    invoke-direct {v0, p0}, LX/9D0;-><init>(LX/9D1;)V

    iput-object v0, p0, LX/9D1;->h:LX/9D0;

    .line 1454671
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/9D1;->i:Ljava/util/List;

    .line 1454672
    return-void
.end method

.method private a(I)Z
    .locals 2

    .prologue
    .line 1454661
    iget-object v0, p0, LX/9D1;->c:LX/0g8;

    if-eqz v0, :cond_0

    if-gez p1, :cond_1

    .line 1454662
    :cond_0
    const/4 v0, 0x0

    .line 1454663
    :goto_0
    return v0

    .line 1454664
    :cond_1
    iput p1, p0, LX/9D1;->g:I

    .line 1454665
    iget-object v0, p0, LX/9D1;->c:LX/0g8;

    new-instance v1, Lcom/facebook/feedback/ui/CommentListScrollStateController$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/feedback/ui/CommentListScrollStateController$1;-><init>(LX/9D1;I)V

    invoke-interface {v0, v1}, LX/0g8;->a(Ljava/lang/Runnable;)V

    .line 1454666
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(LX/9D1;II)Z
    .locals 1

    .prologue
    .line 1454657
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/9D1;->c:LX/0g8;

    if-eqz v0, :cond_0

    .line 1454658
    iget-object v0, p0, LX/9D1;->c:LX/0g8;

    invoke-interface {v0, p1, p2}, LX/0g8;->d(II)V

    .line 1454659
    const/4 v0, 0x1

    .line 1454660
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLComment;I)Z
    .locals 5

    .prologue
    .line 1454644
    iget-object v0, p0, LX/9D1;->c:LX/0g8;

    if-nez v0, :cond_0

    .line 1454645
    const/4 v0, 0x0

    .line 1454646
    :goto_0
    return v0

    .line 1454647
    :cond_0
    const/4 v2, -0x1

    .line 1454648
    if-nez p1, :cond_2

    move v0, v2

    .line 1454649
    :cond_1
    :goto_1
    move v0, v0

    .line 1454650
    invoke-static {p0, v0, p2}, LX/9D1;->a(LX/9D1;II)Z

    move-result v0

    goto :goto_0

    .line 1454651
    :cond_2
    iget-object v0, p0, LX/9D1;->b:LX/9Br;

    invoke-interface {v0}, LX/9Br;->a()I

    move-result v1

    move v0, v1

    .line 1454652
    :goto_2
    iget-object v3, p0, LX/9D1;->a:LX/9CC;

    invoke-virtual {v3}, LX/62C;->getCount()I

    move-result v3

    add-int/2addr v3, v1

    if-ge v0, v3, :cond_4

    .line 1454653
    iget-object v3, p0, LX/9D1;->a:LX/9CC;

    sub-int v4, v0, v1

    invoke-virtual {v3, v4}, LX/9CC;->b(I)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v3

    .line 1454654
    if-eqz v3, :cond_3

    invoke-virtual {v3, p1}, Lcom/facebook/graphql/model/GraphQLComment;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1454655
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    move v0, v2

    .line 1454656
    goto :goto_1
.end method


# virtual methods
.method public final a(LX/0fx;)V
    .locals 1

    .prologue
    .line 1454640
    iget-object v0, p0, LX/9D1;->c:LX/0g8;

    if-nez v0, :cond_0

    .line 1454641
    iget-object v0, p0, LX/9D1;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1454642
    :goto_0
    return-void

    .line 1454643
    :cond_0
    iget-object v0, p0, LX/9D1;->c:LX/0g8;

    invoke-interface {v0, p1}, LX/0g8;->b(LX/0fx;)V

    goto :goto_0
.end method

.method public final a(LX/9Im;)V
    .locals 0

    .prologue
    .line 1454673
    iput-object p1, p0, LX/9D1;->k:LX/9Im;

    .line 1454674
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1454639
    iget-object v0, p0, LX/9D1;->c:LX/0g8;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/9D1;->c:LX/0g8;

    invoke-interface {v0}, LX/0g8;->l()Z

    move-result v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLComment;)Z
    .locals 1

    .prologue
    .line 1454638
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/9D1;->a(Lcom/facebook/graphql/model/GraphQLComment;I)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 5

    .prologue
    .line 1454625
    iget-object v0, p0, LX/9D1;->c:LX/0g8;

    if-nez v0, :cond_0

    .line 1454626
    const/4 v0, 0x0

    .line 1454627
    :goto_0
    return v0

    .line 1454628
    :cond_0
    const/4 v2, -0x1

    .line 1454629
    if-nez p1, :cond_2

    move v0, v2

    .line 1454630
    :cond_1
    :goto_1
    move v0, v0

    .line 1454631
    invoke-direct {p0, v0}, LX/9D1;->a(I)Z

    move-result v0

    goto :goto_0

    .line 1454632
    :cond_2
    iget-object v0, p0, LX/9D1;->b:LX/9Br;

    invoke-interface {v0}, LX/9Br;->a()I

    move-result v1

    move v0, v1

    .line 1454633
    :goto_2
    iget-object v3, p0, LX/9D1;->a:LX/9CC;

    invoke-virtual {v3}, LX/62C;->getCount()I

    move-result v3

    add-int/2addr v3, v1

    if-ge v0, v3, :cond_4

    .line 1454634
    iget-object v3, p0, LX/9D1;->a:LX/9CC;

    sub-int v4, v0, v1

    invoke-virtual {v3, v4}, LX/9CC;->b(I)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v3

    .line 1454635
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1454636
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    move v0, v2

    .line 1454637
    goto :goto_1
.end method

.method public final d()V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 1454608
    iget-object v1, p0, LX/9D1;->c:LX/0g8;

    if-nez v1, :cond_1

    .line 1454609
    :cond_0
    :goto_0
    return-void

    .line 1454610
    :cond_1
    iget-object v1, p0, LX/9D1;->c:LX/0g8;

    invoke-interface {v1}, LX/0g8;->q()I

    move-result v2

    .line 1454611
    iget-object v1, p0, LX/9D1;->c:LX/0g8;

    invoke-interface {v1}, LX/0g8;->p()I

    move-result v1

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    .line 1454612
    iget-object v3, p0, LX/9D1;->b:LX/9Br;

    invoke-interface {v3}, LX/9Br;->a()I

    move-result v3

    .line 1454613
    iget-object v4, p0, LX/9D1;->a:LX/9CC;

    invoke-virtual {v4}, LX/62C;->getCount()I

    move-result v4

    add-int/2addr v4, v3

    add-int/lit8 v4, v4, -0x1

    .line 1454614
    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v5

    if-lt v1, v5, :cond_3

    const/4 v1, 0x1

    .line 1454615
    :goto_1
    if-eqz v1, :cond_0

    .line 1454616
    :goto_2
    iget-object v1, p0, LX/9D1;->c:LX/0g8;

    invoke-interface {v1}, LX/0g8;->p()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1454617
    add-int v1, v2, v0

    .line 1454618
    if-lt v1, v3, :cond_4

    if-gt v1, v4, :cond_4

    .line 1454619
    iget-object v5, p0, LX/9D1;->a:LX/9CC;

    sub-int v6, v1, v3

    invoke-virtual {v5, v6}, LX/9CC;->b(I)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v5

    .line 1454620
    if-eqz v5, :cond_4

    if-eq v1, v3, :cond_2

    iget-object v6, p0, LX/9D1;->a:LX/9CC;

    sub-int/2addr v1, v3

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v6, v1}, LX/9CC;->b(I)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v1

    if-eq v5, v1, :cond_4

    .line 1454621
    :cond_2
    iget-object v1, p0, LX/9D1;->c:LX/0g8;

    invoke-interface {v1, v0}, LX/0g8;->e(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    iput v0, p0, LX/9D1;->e:I

    .line 1454622
    iput-object v5, p0, LX/9D1;->d:Lcom/facebook/graphql/model/GraphQLComment;

    goto :goto_0

    :cond_3
    move v1, v0

    .line 1454623
    goto :goto_1

    .line 1454624
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 1454603
    iget-object v0, p0, LX/9D1;->c:LX/0g8;

    if-nez v0, :cond_0

    .line 1454604
    :goto_0
    return-void

    .line 1454605
    :cond_0
    iget-object v0, p0, LX/9D1;->d:Lcom/facebook/graphql/model/GraphQLComment;

    iget v1, p0, LX/9D1;->e:I

    invoke-direct {p0, v0, v1}, LX/9D1;->a(Lcom/facebook/graphql/model/GraphQLComment;I)Z

    .line 1454606
    const/4 v0, 0x0

    iput-object v0, p0, LX/9D1;->d:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1454607
    const/4 v0, 0x0

    iput v0, p0, LX/9D1;->e:I

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 1454602
    iget-object v0, p0, LX/9D1;->c:LX/0g8;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9D1;->c:LX/0g8;

    invoke-interface {v0}, LX/0g8;->s()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, LX/9D1;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
