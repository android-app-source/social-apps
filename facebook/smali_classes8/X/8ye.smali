.class public LX/8ye;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/8yc;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8yf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1426298
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/8ye;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/8yf;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1426295
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1426296
    iput-object p1, p0, LX/8ye;->b:LX/0Ot;

    .line 1426297
    return-void
.end method

.method public static a(LX/0QB;)LX/8ye;
    .locals 4

    .prologue
    .line 1426284
    const-class v1, LX/8ye;

    monitor-enter v1

    .line 1426285
    :try_start_0
    sget-object v0, LX/8ye;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1426286
    sput-object v2, LX/8ye;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1426287
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1426288
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1426289
    new-instance v3, LX/8ye;

    const/16 p0, 0x193d

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/8ye;-><init>(LX/0Ot;)V

    .line 1426290
    move-object v0, v3

    .line 1426291
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1426292
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8ye;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1426293
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1426294
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1426282
    invoke-static {}, LX/1dS;->b()V

    .line 1426283
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x1a56dce4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1426276
    iget-object v1, p0, LX/8ye;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    .line 1426277
    invoke-static {p4}, LX/1mh;->a(I)I

    move-result v1

    if-nez v1, :cond_0

    const/16 v1, 0x18

    invoke-static {v1}, LX/1mh;->b(I)I

    move-result v1

    :goto_0
    iput v1, p5, LX/1no;->b:I

    .line 1426278
    invoke-static {p3}, LX/1mh;->a(I)I

    move-result v1

    if-nez v1, :cond_1

    const/16 v1, 0x64

    invoke-static {v1}, LX/1mh;->b(I)I

    move-result v1

    :goto_1
    iput v1, p5, LX/1no;->a:I

    .line 1426279
    const/16 v1, 0x1f

    const v2, 0x45baa1ab

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1426280
    :cond_0
    invoke-static {p4}, LX/1mh;->b(I)I

    move-result v1

    goto :goto_0

    .line 1426281
    :cond_1
    invoke-static {p3}, LX/1mh;->b(I)I

    move-result v1

    goto :goto_1
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1426273
    iget-object v0, p0, LX/8ye;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 1426274
    new-instance v0, Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    const p0, 0x1010078

    invoke-direct {v0, p1, v1, p0}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    move-object v0, v0

    .line 1426275
    return-object v0
.end method

.method public final c(LX/1De;)LX/8yc;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1426299
    new-instance v1, LX/8yd;

    invoke-direct {v1, p0}, LX/8yd;-><init>(LX/8ye;)V

    .line 1426300
    sget-object v2, LX/8ye;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8yc;

    .line 1426301
    if-nez v2, :cond_0

    .line 1426302
    new-instance v2, LX/8yc;

    invoke-direct {v2}, LX/8yc;-><init>()V

    .line 1426303
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/8yc;->a$redex0(LX/8yc;LX/1De;IILX/8yd;)V

    .line 1426304
    move-object v1, v2

    .line 1426305
    move-object v0, v1

    .line 1426306
    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1426272
    const/4 v0, 0x1

    return v0
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 3

    .prologue
    .line 1426266
    check-cast p3, LX/8yd;

    .line 1426267
    iget-object v0, p0, LX/8ye;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    check-cast p2, Landroid/widget/ProgressBar;

    iget-object v0, p3, LX/8yd;->a:LX/1dc;

    iget v1, p3, LX/8yd;->b:I

    iget v2, p3, LX/8yd;->c:I

    .line 1426268
    invoke-virtual {p2, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 1426269
    invoke-virtual {p2, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 1426270
    invoke-static {p1, v0}, LX/1dc;->a(LX/1De;LX/1dc;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p2, p0}, Landroid/widget/ProgressBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1426271
    return-void
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 1426265
    sget-object v0, LX/1mv;->VIEW:LX/1mv;

    return-object v0
.end method

.method public final f(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 1426261
    check-cast p3, LX/8yd;

    .line 1426262
    iget-object v0, p0, LX/8ye;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    check-cast p2, Landroid/widget/ProgressBar;

    iget-object v0, p3, LX/8yd;->a:LX/1dc;

    .line 1426263
    invoke-virtual {p2}, Landroid/widget/ProgressBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-static {p1, p0, v0}, LX/1dc;->a(LX/1De;Ljava/lang/Object;LX/1dc;)V

    .line 1426264
    return-void
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 1426259
    const/4 v0, 0x1

    return v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 1426260
    const/16 v0, 0xf

    return v0
.end method
