.class public LX/8zT;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/91K;


# direct methods
.method public constructor <init>(LX/91K;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1427448
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1427449
    iput-object p1, p0, LX/8zT;->a:LX/91K;

    .line 1427450
    return-void
.end method

.method public static a(LX/0QB;)LX/8zT;
    .locals 4

    .prologue
    .line 1427451
    const-class v1, LX/8zT;

    monitor-enter v1

    .line 1427452
    :try_start_0
    sget-object v0, LX/8zT;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1427453
    sput-object v2, LX/8zT;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1427454
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1427455
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1427456
    new-instance p0, LX/8zT;

    invoke-static {v0}, LX/91K;->a(LX/0QB;)LX/91K;

    move-result-object v3

    check-cast v3, LX/91K;

    invoke-direct {p0, v3}, LX/8zT;-><init>(LX/91K;)V

    .line 1427457
    move-object v0, p0

    .line 1427458
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1427459
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8zT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1427460
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1427461
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
