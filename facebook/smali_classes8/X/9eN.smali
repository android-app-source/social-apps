.class public final LX/9eN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:LX/9eP;

.field private b:LX/9hP;


# direct methods
.method public constructor <init>(LX/9eP;)V
    .locals 0

    .prologue
    .line 1519536
    iput-object p1, p0, LX/9eN;->a:LX/9eP;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 5

    .prologue
    .line 1519537
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 1519538
    iget-object v1, p0, LX/9eN;->b:LX/9hP;

    if-nez v1, :cond_0

    .line 1519539
    new-instance v1, LX/9hP;

    invoke-direct {v1}, LX/9hP;-><init>()V

    iput-object v1, p0, LX/9eN;->b:LX/9hP;

    .line 1519540
    :cond_0
    iget-object v2, p0, LX/9eN;->a:LX/9eP;

    iget-object v2, v2, LX/9eP;->b:LX/9hP;

    iget-object v3, p0, LX/9eN;->a:LX/9eP;

    iget-object v3, v3, LX/9eP;->c:LX/9hP;

    iget-object v4, p0, LX/9eN;->b:LX/9hP;

    .line 1519541
    invoke-static {v2, v3, v0, v4}, LX/9eP;->a(LX/9hP;LX/9hP;FLX/9hP;)V

    .line 1519542
    iget-object v0, p0, LX/9eN;->a:LX/9eP;

    iget-object v0, v0, LX/9eP;->a:LX/9eQ;

    iget-object v1, p0, LX/9eN;->b:LX/9hP;

    invoke-virtual {v0, v1}, LX/9eQ;->a(LX/9hP;)LX/9hP;

    move-result-object v0

    iput-object v0, p0, LX/9eN;->b:LX/9hP;

    .line 1519543
    return-void
.end method
