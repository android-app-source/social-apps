.class public LX/AKe;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0tX;

.field public final d:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1664026
    const-class v0, LX/AKe;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AKe;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0tX;Ljava/util/concurrent/ExecutorService;LX/0Or;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0tX;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1664021
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1664022
    iput-object p1, p0, LX/AKe;->c:LX/0tX;

    .line 1664023
    iput-object p2, p0, LX/AKe;->d:Ljava/util/concurrent/ExecutorService;

    .line 1664024
    iput-object p3, p0, LX/AKe;->b:LX/0Or;

    .line 1664025
    return-void
.end method
