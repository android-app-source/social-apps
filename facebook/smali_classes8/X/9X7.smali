.class public final enum LX/9X7;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/9X2;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9X7;",
        ">;",
        "LX/9X2;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9X7;

.field public static final enum EDIT_COMPLETE_REPLACE_ACTION:LX/9X7;

.field public static final enum EDIT_SWITCH_DEFAULT_ACTIONS:LX/9X7;

.field public static final enum EDIT_TAP_ADD_ACTION:LX/9X7;

.field public static final enum EDIT_TAP_ADD_TABS:LX/9X7;

.field public static final enum EDIT_TAP_REORDER_TABS:LX/9X7;

.field public static final enum EDIT_TAP_REPLACE_ACTION:LX/9X7;

.field public static final enum EDIT_TAP_TAB_ACTION:LX/9X7;

.field public static final enum EDIT_TAP_TEMPLATE_MORE_INFO:LX/9X7;

.field public static final enum EDIT_TAP_TEMPLATE_ROW:LX/9X7;

.field public static final enum EDIT_USE_DEFAULT_BUTTONS:LX/9X7;

.field public static final enum EDIT_USE_DEFAULT_TAB_ORDER:LX/9X7;

.field public static final enum TEMPLATES_TAP_APPLY:LX/9X7;

.field public static final enum TEMPLATE_CANCEL_APPLICATION:LX/9X7;

.field public static final enum TEMPLATE_CONFIRM_APPLICATION:LX/9X7;


# instance fields
.field private mEventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1502326
    new-instance v0, LX/9X7;

    const-string v1, "EDIT_TAP_TEMPLATE_MORE_INFO"

    const-string v2, "edit_tap_template_more_info"

    invoke-direct {v0, v1, v4, v2}, LX/9X7;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X7;->EDIT_TAP_TEMPLATE_MORE_INFO:LX/9X7;

    .line 1502327
    new-instance v0, LX/9X7;

    const-string v1, "EDIT_SWITCH_DEFAULT_ACTIONS"

    const-string v2, "edit_switch_default_actions"

    invoke-direct {v0, v1, v5, v2}, LX/9X7;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X7;->EDIT_SWITCH_DEFAULT_ACTIONS:LX/9X7;

    .line 1502328
    new-instance v0, LX/9X7;

    const-string v1, "EDIT_TAP_ADD_ACTION"

    const-string v2, "edit_tap_add_action"

    invoke-direct {v0, v1, v6, v2}, LX/9X7;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X7;->EDIT_TAP_ADD_ACTION:LX/9X7;

    .line 1502329
    new-instance v0, LX/9X7;

    const-string v1, "EDIT_TAP_REPLACE_ACTION"

    const-string v2, "edit_tap_replace_action"

    invoke-direct {v0, v1, v7, v2}, LX/9X7;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X7;->EDIT_TAP_REPLACE_ACTION:LX/9X7;

    .line 1502330
    new-instance v0, LX/9X7;

    const-string v1, "EDIT_TAP_TAB_ACTION"

    const-string v2, "edit_tap_tab_action"

    invoke-direct {v0, v1, v8, v2}, LX/9X7;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X7;->EDIT_TAP_TAB_ACTION:LX/9X7;

    .line 1502331
    new-instance v0, LX/9X7;

    const-string v1, "EDIT_USE_DEFAULT_TAB_ORDER"

    const/4 v2, 0x5

    const-string v3, "edit_use_default_tab_order"

    invoke-direct {v0, v1, v2, v3}, LX/9X7;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X7;->EDIT_USE_DEFAULT_TAB_ORDER:LX/9X7;

    .line 1502332
    new-instance v0, LX/9X7;

    const-string v1, "EDIT_USE_DEFAULT_BUTTONS"

    const/4 v2, 0x6

    const-string v3, "edit_use_default_buttons"

    invoke-direct {v0, v1, v2, v3}, LX/9X7;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X7;->EDIT_USE_DEFAULT_BUTTONS:LX/9X7;

    .line 1502333
    new-instance v0, LX/9X7;

    const-string v1, "EDIT_TAP_REORDER_TABS"

    const/4 v2, 0x7

    const-string v3, "edit_tap_reorder_tabs"

    invoke-direct {v0, v1, v2, v3}, LX/9X7;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X7;->EDIT_TAP_REORDER_TABS:LX/9X7;

    .line 1502334
    new-instance v0, LX/9X7;

    const-string v1, "EDIT_TAP_ADD_TABS"

    const/16 v2, 0x8

    const-string v3, "edit_tap_add_tabs"

    invoke-direct {v0, v1, v2, v3}, LX/9X7;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X7;->EDIT_TAP_ADD_TABS:LX/9X7;

    .line 1502335
    new-instance v0, LX/9X7;

    const-string v1, "EDIT_TAP_TEMPLATE_ROW"

    const/16 v2, 0x9

    const-string v3, "edit_tap_template_row"

    invoke-direct {v0, v1, v2, v3}, LX/9X7;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X7;->EDIT_TAP_TEMPLATE_ROW:LX/9X7;

    .line 1502336
    new-instance v0, LX/9X7;

    const-string v1, "TEMPLATES_TAP_APPLY"

    const/16 v2, 0xa

    const-string v3, "templates_tap_apply"

    invoke-direct {v0, v1, v2, v3}, LX/9X7;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X7;->TEMPLATES_TAP_APPLY:LX/9X7;

    .line 1502337
    new-instance v0, LX/9X7;

    const-string v1, "TEMPLATE_CONFIRM_APPLICATION"

    const/16 v2, 0xb

    const-string v3, "template_confirm_application"

    invoke-direct {v0, v1, v2, v3}, LX/9X7;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X7;->TEMPLATE_CONFIRM_APPLICATION:LX/9X7;

    .line 1502338
    new-instance v0, LX/9X7;

    const-string v1, "TEMPLATE_CANCEL_APPLICATION"

    const/16 v2, 0xc

    const-string v3, "template_cancel_application"

    invoke-direct {v0, v1, v2, v3}, LX/9X7;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X7;->TEMPLATE_CANCEL_APPLICATION:LX/9X7;

    .line 1502339
    new-instance v0, LX/9X7;

    const-string v1, "EDIT_COMPLETE_REPLACE_ACTION"

    const/16 v2, 0xd

    const-string v3, "edit_complete_replace_action"

    invoke-direct {v0, v1, v2, v3}, LX/9X7;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X7;->EDIT_COMPLETE_REPLACE_ACTION:LX/9X7;

    .line 1502340
    const/16 v0, 0xe

    new-array v0, v0, [LX/9X7;

    sget-object v1, LX/9X7;->EDIT_TAP_TEMPLATE_MORE_INFO:LX/9X7;

    aput-object v1, v0, v4

    sget-object v1, LX/9X7;->EDIT_SWITCH_DEFAULT_ACTIONS:LX/9X7;

    aput-object v1, v0, v5

    sget-object v1, LX/9X7;->EDIT_TAP_ADD_ACTION:LX/9X7;

    aput-object v1, v0, v6

    sget-object v1, LX/9X7;->EDIT_TAP_REPLACE_ACTION:LX/9X7;

    aput-object v1, v0, v7

    sget-object v1, LX/9X7;->EDIT_TAP_TAB_ACTION:LX/9X7;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/9X7;->EDIT_USE_DEFAULT_TAB_ORDER:LX/9X7;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/9X7;->EDIT_USE_DEFAULT_BUTTONS:LX/9X7;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/9X7;->EDIT_TAP_REORDER_TABS:LX/9X7;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/9X7;->EDIT_TAP_ADD_TABS:LX/9X7;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/9X7;->EDIT_TAP_TEMPLATE_ROW:LX/9X7;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/9X7;->TEMPLATES_TAP_APPLY:LX/9X7;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/9X7;->TEMPLATE_CONFIRM_APPLICATION:LX/9X7;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/9X7;->TEMPLATE_CANCEL_APPLICATION:LX/9X7;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/9X7;->EDIT_COMPLETE_REPLACE_ACTION:LX/9X7;

    aput-object v2, v0, v1

    sput-object v0, LX/9X7;->$VALUES:[LX/9X7;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1502341
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1502342
    iput-object p3, p0, LX/9X7;->mEventName:Ljava/lang/String;

    .line 1502343
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9X7;
    .locals 1

    .prologue
    .line 1502344
    const-class v0, LX/9X7;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9X7;

    return-object v0
.end method

.method public static values()[LX/9X7;
    .locals 1

    .prologue
    .line 1502345
    sget-object v0, LX/9X7;->$VALUES:[LX/9X7;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9X7;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1502346
    iget-object v0, p0, LX/9X7;->mEventName:Ljava/lang/String;

    return-object v0
.end method

.method public final getType()LX/9XC;
    .locals 1

    .prologue
    .line 1502347
    sget-object v0, LX/9XC;->ADMIN_EDIT_PAGE:LX/9XC;

    return-object v0
.end method
