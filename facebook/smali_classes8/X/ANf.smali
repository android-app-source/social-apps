.class public LX/ANf;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public b:LX/6JN;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6JN;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6JN;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6JN;",
            ">;"
        }
    .end annotation
.end field

.field private f:I

.field private g:I

.field private h:Landroid/content/res/Resources;

.field private i:Lcom/facebook/fbui/glyph/GlyphButton;

.field public j:LX/6JF;

.field public k:LX/ANZ;

.field public l:LX/6Ia;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1668233
    const-class v0, LX/ANf;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/ANf;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;Lcom/facebook/fbui/glyph/GlyphButton;LX/6JF;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1668290
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1668291
    sget-object v0, LX/6JN;->OFF:LX/6JN;

    iput-object v0, p0, LX/ANf;->b:LX/6JN;

    .line 1668292
    iput v1, p0, LX/ANf;->f:I

    .line 1668293
    iput v1, p0, LX/ANf;->g:I

    .line 1668294
    sget-object v0, LX/6JF;->FRONT:LX/6JF;

    iput-object v0, p0, LX/ANf;->j:LX/6JF;

    .line 1668295
    sget-object v0, LX/ANZ;->PHOTO:LX/ANZ;

    iput-object v0, p0, LX/ANf;->k:LX/ANZ;

    .line 1668296
    iput-object p1, p0, LX/ANf;->h:Landroid/content/res/Resources;

    .line 1668297
    iput-object p2, p0, LX/ANf;->i:Lcom/facebook/fbui/glyph/GlyphButton;

    .line 1668298
    iput-object p3, p0, LX/ANf;->j:LX/6JF;

    .line 1668299
    return-void
.end method


# virtual methods
.method public final a(LX/ANZ;)V
    .locals 0

    .prologue
    .line 1668300
    iput-object p1, p0, LX/ANf;->k:LX/ANZ;

    .line 1668301
    invoke-virtual {p0}, LX/ANf;->d()V

    .line 1668302
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 1668238
    iget-object v0, p0, LX/ANf;->j:LX/6JF;

    sget-object v1, LX/6JF;->FRONT:LX/6JF;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/ANf;->l:LX/6Ia;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ANf;->l:LX/6Ia;

    invoke-interface {v0}, LX/6Ia;->c()Z

    move-result v0

    if-nez v0, :cond_7

    .line 1668239
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1668240
    :goto_0
    move-object v1, v0

    .line 1668241
    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1668242
    :cond_1
    :goto_1
    return-void

    .line 1668243
    :cond_2
    iget-object v0, p0, LX/ANf;->k:LX/ANZ;

    sget-object v2, LX/ANZ;->PHOTO:LX/ANZ;

    if-ne v0, v2, :cond_3

    .line 1668244
    iget v0, p0, LX/ANf;->f:I

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6JN;

    iput-object v0, p0, LX/ANf;->b:LX/6JN;

    .line 1668245
    iget v0, p0, LX/ANf;->f:I

    add-int/lit8 v0, v0, 0x1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    rem-int/2addr v0, v1

    iput v0, p0, LX/ANf;->f:I

    .line 1668246
    :goto_2
    iget-object v0, p0, LX/ANf;->b:LX/6JN;

    sget-object v1, LX/6JN;->OFF:LX/6JN;

    if-ne v0, v1, :cond_4

    .line 1668247
    iget-object v0, p0, LX/ANf;->i:Lcom/facebook/fbui/glyph/GlyphButton;

    iget-object v1, p0, LX/ANf;->h:Landroid/content/res/Resources;

    const v2, 0x7f02077e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 1668248
    :cond_3
    iget v0, p0, LX/ANf;->g:I

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6JN;

    iput-object v0, p0, LX/ANf;->b:LX/6JN;

    .line 1668249
    iget v0, p0, LX/ANf;->g:I

    add-int/lit8 v0, v0, 0x1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    rem-int/2addr v0, v1

    iput v0, p0, LX/ANf;->g:I

    goto :goto_2

    .line 1668250
    :cond_4
    iget-object v0, p0, LX/ANf;->b:LX/6JN;

    sget-object v1, LX/6JN;->ON:LX/6JN;

    if-eq v0, v1, :cond_5

    iget-object v0, p0, LX/ANf;->b:LX/6JN;

    sget-object v1, LX/6JN;->TORCH:LX/6JN;

    if-ne v0, v1, :cond_6

    .line 1668251
    :cond_5
    iget-object v0, p0, LX/ANf;->i:Lcom/facebook/fbui/glyph/GlyphButton;

    iget-object v1, p0, LX/ANf;->h:Landroid/content/res/Resources;

    const v2, 0x7f02077d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 1668252
    :cond_6
    iget-object v0, p0, LX/ANf;->b:LX/6JN;

    sget-object v1, LX/6JN;->AUTO:LX/6JN;

    if-ne v0, v1, :cond_1

    .line 1668253
    iget-object v0, p0, LX/ANf;->i:Lcom/facebook/fbui/glyph/GlyphButton;

    iget-object v1, p0, LX/ANf;->h:Landroid/content/res/Resources;

    const v2, 0x7f02077c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 1668254
    :cond_7
    iget-object v0, p0, LX/ANf;->k:LX/ANZ;

    sget-object v1, LX/ANZ;->IMAGE:LX/ANZ;

    if-ne v0, v1, :cond_9

    .line 1668255
    iget-object v0, p0, LX/ANf;->c:Ljava/util/List;

    if-nez v0, :cond_8

    .line 1668256
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/ANf;->c:Ljava/util/List;

    .line 1668257
    :cond_8
    iget-object v0, p0, LX/ANf;->c:Ljava/util/List;

    move-object v0, v0

    .line 1668258
    goto/16 :goto_0

    .line 1668259
    :cond_9
    iget-object v0, p0, LX/ANf;->k:LX/ANZ;

    sget-object v1, LX/ANZ;->PHOTO:LX/ANZ;

    if-ne v0, v1, :cond_a

    .line 1668260
    iget-object v0, p0, LX/ANf;->d:Ljava/util/List;

    if-eqz v0, :cond_b

    .line 1668261
    iget-object v0, p0, LX/ANf;->d:Ljava/util/List;

    .line 1668262
    :goto_3
    move-object v0, v0

    .line 1668263
    goto/16 :goto_0

    .line 1668264
    :cond_a
    iget-object v0, p0, LX/ANf;->e:Ljava/util/List;

    if-eqz v0, :cond_10

    .line 1668265
    iget-object v0, p0, LX/ANf;->e:Ljava/util/List;

    .line 1668266
    :goto_4
    move-object v0, v0

    .line 1668267
    goto/16 :goto_0

    .line 1668268
    :cond_b
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/ANf;->d:Ljava/util/List;

    .line 1668269
    iget-object v0, p0, LX/ANf;->l:LX/6Ia;

    invoke-interface {v0}, LX/6Ia;->a()LX/6IP;

    move-result-object v0

    .line 1668270
    invoke-interface {v0}, LX/6IP;->a()Ljava/util/List;

    move-result-object v0

    .line 1668271
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 1668272
    iget-object v0, p0, LX/ANf;->d:Ljava/util/List;

    goto :goto_3

    .line 1668273
    :cond_c
    sget-object v1, LX/6JN;->OFF:LX/6JN;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 1668274
    iget-object v1, p0, LX/ANf;->d:Ljava/util/List;

    sget-object v2, LX/6JN;->OFF:LX/6JN;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668275
    :cond_d
    sget-object v1, LX/6JN;->ON:LX/6JN;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 1668276
    iget-object v1, p0, LX/ANf;->d:Ljava/util/List;

    sget-object v2, LX/6JN;->ON:LX/6JN;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668277
    :cond_e
    sget-object v1, LX/6JN;->AUTO:LX/6JN;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1668278
    iget-object v0, p0, LX/ANf;->d:Ljava/util/List;

    sget-object v1, LX/6JN;->AUTO:LX/6JN;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch LX/6JJ; {:try_start_0 .. :try_end_0} :catch_0

    .line 1668279
    :cond_f
    :goto_5
    iget-object v0, p0, LX/ANf;->d:Ljava/util/List;

    goto :goto_3

    :catch_0
    goto :goto_5

    .line 1668280
    :cond_10
    :try_start_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/ANf;->e:Ljava/util/List;

    .line 1668281
    iget-object v0, p0, LX/ANf;->l:LX/6Ia;

    invoke-interface {v0}, LX/6Ia;->a()LX/6IP;

    move-result-object v0

    .line 1668282
    invoke-interface {v0}, LX/6IP;->a()Ljava/util/List;

    move-result-object v0

    .line 1668283
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_11

    .line 1668284
    iget-object v0, p0, LX/ANf;->e:Ljava/util/List;

    goto :goto_4

    .line 1668285
    :cond_11
    sget-object v1, LX/6JN;->OFF:LX/6JN;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 1668286
    iget-object v1, p0, LX/ANf;->e:Ljava/util/List;

    sget-object v2, LX/6JN;->OFF:LX/6JN;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668287
    :cond_12
    sget-object v1, LX/6JN;->TORCH:LX/6JN;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 1668288
    iget-object v0, p0, LX/ANf;->e:Ljava/util/List;

    sget-object v1, LX/6JN;->TORCH:LX/6JN;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch LX/6JJ; {:try_start_1 .. :try_end_1} :catch_1

    .line 1668289
    :cond_13
    :goto_6
    iget-object v0, p0, LX/ANf;->e:Ljava/util/List;

    goto/16 :goto_4

    :catch_1
    goto :goto_6
.end method

.method public final d()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1668234
    iput v0, p0, LX/ANf;->f:I

    .line 1668235
    iput v0, p0, LX/ANf;->g:I

    .line 1668236
    invoke-virtual {p0}, LX/ANf;->c()V

    .line 1668237
    return-void
.end method
