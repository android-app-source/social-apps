.class public final LX/9mQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:LX/9mU;


# direct methods
.method public constructor <init>(LX/9mU;)V
    .locals 0

    .prologue
    .line 1535636
    iput-object p1, p0, LX/9mQ;->a:LX/9mU;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 4

    .prologue
    .line 1535637
    iget-object v0, p0, LX/9mQ;->a:LX/9mU;

    iget-object v0, v0, LX/9mU;->j:Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1535638
    iget-object v2, v0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->q:Lcom/facebook/rapidreporting/ui/DialogStateData;

    .line 1535639
    iput-object v1, v2, Lcom/facebook/rapidreporting/ui/DialogStateData;->h:Ljava/lang/String;

    .line 1535640
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1535641
    iget-object v2, v0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->q:Lcom/facebook/rapidreporting/ui/DialogStateData;

    const-string v3, "other"

    invoke-virtual {v2, v3}, Lcom/facebook/rapidreporting/ui/DialogStateData;->a(Ljava/lang/String;)V

    .line 1535642
    :goto_0
    invoke-virtual {v0}, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->b()V

    .line 1535643
    iget-object v0, p0, LX/9mQ;->a:LX/9mU;

    iget-object v0, v0, LX/9mU;->l:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 1535644
    :goto_1
    return-void

    .line 1535645
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1535646
    iget-object v0, p0, LX/9mQ;->a:LX/9mU;

    iget-object v0, v0, LX/9mU;->l:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_1

    .line 1535647
    :cond_1
    iget-object v0, p0, LX/9mQ;->a:LX/9mU;

    iget-object v0, v0, LX/9mU;->l:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_1

    .line 1535648
    :cond_2
    iget-object v2, v0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->q:Lcom/facebook/rapidreporting/ui/DialogStateData;

    const-string v3, "other"

    invoke-virtual {v2, v3}, Lcom/facebook/rapidreporting/ui/DialogStateData;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1535649
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1535650
    return-void
.end method
