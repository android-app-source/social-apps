.class public LX/A78;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Tn;

.field public static final c:Ljava/lang/String;


# instance fields
.field public b:LX/A77;

.field private d:Landroid/content/Context;

.field public e:LX/A71;

.field public f:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public g:LX/11H;

.field public h:LX/A6X;

.field public final i:LX/0SG;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1625573
    const-class v0, LX/A78;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/A78;->c:Ljava/lang/String;

    .line 1625574
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "transliteration/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/A78;->a:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/A71;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/11H;LX/A6X;LX/A77;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1625575
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1625576
    iput-object p1, p0, LX/A78;->d:Landroid/content/Context;

    .line 1625577
    iput-object p2, p0, LX/A78;->e:LX/A71;

    .line 1625578
    iput-object p3, p0, LX/A78;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1625579
    iput-object p4, p0, LX/A78;->g:LX/11H;

    .line 1625580
    iput-object p5, p0, LX/A78;->h:LX/A6X;

    .line 1625581
    iput-object p6, p0, LX/A78;->b:LX/A77;

    .line 1625582
    iput-object p7, p0, LX/A78;->i:LX/0SG;

    .line 1625583
    return-void
.end method

.method public static a(LX/A78;Ljava/lang/String;II)I
    .locals 3

    .prologue
    .line 1625584
    iget-object v0, p0, LX/A78;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p1, p2, p3}, LX/A78;->b(Ljava/lang/String;II)LX/0Tn;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    return v0
.end method

.method public static a(LX/A78;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1625585
    :try_start_0
    iget-object v1, p0, LX/A78;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 1625586
    :try_start_1
    invoke-static {v2}, LX/A78;->a(Ljava/io/InputStream;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 1625587
    invoke-static {v2}, LX/A78;->a(Ljava/io/Closeable;)V

    .line 1625588
    :goto_0
    return-object v0

    .line 1625589
    :catch_0
    move-exception v1

    move-object v2, v0

    .line 1625590
    :goto_1
    :try_start_2
    sget-object v3, LX/A78;->c:Ljava/lang/String;

    const-string v4, "file IO exception on reading"

    invoke-static {v3, v4, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1625591
    invoke-static {v2}, LX/A78;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_2
    invoke-static {v2}, LX/A78;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_2

    .line 1625592
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method private static a(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1625593
    :try_start_0
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v1, Ljava/io/InputStreamReader;

    invoke-direct {v1, p0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 1625594
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1625595
    :goto_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1625596
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 1625597
    :catch_0
    move-exception v0

    .line 1625598
    sget-object v1, LX/A78;->c:Ljava/lang/String;

    const-string v2, "No file to load from"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1625599
    :goto_1
    const/4 v0, 0x0

    :goto_2
    return-object v0

    .line 1625600
    :cond_0
    :try_start_1
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    .line 1625601
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    .line 1625602
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto :goto_2

    .line 1625603
    :catch_1
    move-exception v0

    .line 1625604
    sget-object v1, LX/A78;->c:Ljava/lang/String;

    const-string v2, "file IO exception on reading"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private static a(Ljava/io/Closeable;)V
    .locals 3

    .prologue
    .line 1625568
    if-nez p0, :cond_0

    .line 1625569
    :goto_0
    return-void

    .line 1625570
    :cond_0
    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1625571
    :catch_0
    move-exception v0

    .line 1625572
    sget-object v1, LX/A78;->c:Ljava/lang/String;

    const-string v2, "IO Exception, couldnt close stream"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static b(Ljava/lang/String;II)LX/0Tn;
    .locals 3

    .prologue
    .line 1625605
    sget-object v0, LX/A78;->a:LX/0Tn;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public static b(LX/0QB;)LX/A78;
    .locals 8

    .prologue
    .line 1625562
    new-instance v0, LX/A78;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    .line 1625563
    new-instance v2, LX/A71;

    invoke-direct {v2}, LX/A71;-><init>()V

    .line 1625564
    move-object v2, v2

    .line 1625565
    move-object v2, v2

    .line 1625566
    check-cast v2, LX/A71;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v4

    check-cast v4, LX/11H;

    invoke-static {p0}, LX/A6X;->a(LX/0QB;)LX/A6X;

    move-result-object v5

    check-cast v5, LX/A6X;

    invoke-static {p0}, LX/A77;->a(LX/0QB;)LX/A77;

    move-result-object v6

    check-cast v6, LX/A77;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    invoke-direct/range {v0 .. v7}, LX/A78;-><init>(Landroid/content/Context;LX/A71;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/11H;LX/A6X;LX/A77;LX/0SG;)V

    .line 1625567
    return-object v0
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1625552
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 1625553
    const-string v0, "\\\\u([0-9A-Fa-f]{4})"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 1625554
    :goto_0
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1625555
    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {v2, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    const/16 v3, 0x10

    invoke-static {v0, v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    .line 1625556
    const-string v3, ""

    invoke-virtual {v2, v1, v3}, Ljava/util/regex/Matcher;->appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;

    .line 1625557
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->appendCodePoint(I)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1625558
    :catch_0
    move-exception v0

    .line 1625559
    sget-object v3, LX/A78;->c:Ljava/lang/String;

    const-string v4, "Unicode Exception"

    invoke-static {v3, v4, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1625560
    :cond_0
    invoke-virtual {v2, v1}, Ljava/util/regex/Matcher;->appendTail(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 1625561
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/A78;Ljava/lang/String;III)V
    .locals 2

    .prologue
    .line 1625550
    iget-object v0, p0, LX/A78;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    invoke-static {p1, p2, p3}, LX/A78;->b(Ljava/lang/String;II)LX/0Tn;

    move-result-object v1

    invoke-interface {v0, v1, p4}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1625551
    return-void
.end method

.method private static g(II)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1625549
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "transliteration_model_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(II)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1625524
    invoke-static {p1, p2}, LX/A78;->g(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/A78;->a(LX/A78;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;III)V
    .locals 4

    .prologue
    .line 1625534
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/A78;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-static {p2, p3}, LX/A78;->g(II)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1625535
    const/4 v2, 0x0

    .line 1625536
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1625537
    :try_start_1
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/FileOutputStream;->write([B)V

    .line 1625538
    const-string v0, "transliteration_version"

    invoke-static {p0, v0, p2, p3, p4}, LX/A78;->b(LX/A78;Ljava/lang/String;III)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1625539
    invoke-static {v1}, LX/A78;->a(Ljava/io/Closeable;)V

    .line 1625540
    :goto_0
    return-void

    .line 1625541
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 1625542
    :goto_1
    :try_start_2
    sget-object v2, LX/A78;->c:Ljava/lang/String;

    const-string v3, "File not found while storing"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1625543
    invoke-static {v1}, LX/A78;->a(Ljava/io/Closeable;)V

    goto :goto_0

    .line 1625544
    :catch_1
    move-exception v0

    move-object v1, v2

    .line 1625545
    :goto_2
    :try_start_3
    sget-object v2, LX/A78;->c:Ljava/lang/String;

    const-string v3, "IO Exception while writing to file"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1625546
    invoke-static {v1}, LX/A78;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_3
    invoke-static {v1}, LX/A78;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_3

    .line 1625547
    :catch_2
    move-exception v0

    goto :goto_2

    .line 1625548
    :catch_3
    move-exception v0

    goto :goto_1
.end method

.method public final b(II)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1625525
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, LX/A78;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-static {p1, p2}, LX/A78;->g(II)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1625526
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1625527
    :try_start_1
    invoke-static {v2}, LX/A78;->a(Ljava/io/InputStream;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 1625528
    invoke-static {v2}, LX/A78;->a(Ljava/io/Closeable;)V

    .line 1625529
    :goto_0
    return-object v0

    .line 1625530
    :catch_0
    move-exception v1

    move-object v2, v0

    .line 1625531
    :goto_1
    :try_start_2
    sget-object v3, LX/A78;->c:Ljava/lang/String;

    const-string v4, "file IO exception on reading"

    invoke-static {v3, v4, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1625532
    invoke-static {v2}, LX/A78;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_2
    invoke-static {v2}, LX/A78;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_2

    .line 1625533
    :catch_1
    move-exception v1

    goto :goto_1
.end method
