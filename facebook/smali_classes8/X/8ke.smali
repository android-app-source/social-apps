.class public final LX/8ke;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/List;

.field public final synthetic b:LX/8kh;

.field public final synthetic c:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic d:LX/8kj;


# direct methods
.method public constructor <init>(LX/8kj;Ljava/util/List;LX/8kh;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0

    .prologue
    .line 1396614
    iput-object p1, p0, LX/8ke;->d:LX/8kj;

    iput-object p2, p0, LX/8ke;->a:Ljava/util/List;

    iput-object p3, p0, LX/8ke;->b:LX/8kh;

    iput-object p4, p0, LX/8ke;->c:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1396608
    iget-object v0, p0, LX/8ke;->d:LX/8kj;

    const/4 v1, 0x0

    .line 1396609
    iput-object v1, v0, LX/8kj;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1396610
    iget-object v0, p0, LX/8ke;->c:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 1396611
    iget-object v0, p0, LX/8ke;->d:LX/8kj;

    iget-boolean v0, v0, LX/8kj;->b:Z

    if-eqz v0, :cond_0

    .line 1396612
    :goto_0
    return-void

    .line 1396613
    :cond_0
    iget-object v0, p0, LX/8ke;->d:LX/8kj;

    iget-object v0, v0, LX/8kj;->d:LX/3Mb;

    iget-object v1, p0, LX/8ke;->b:LX/8kh;

    invoke-interface {v0, v1, p1}, LX/3Mb;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1396615
    check-cast p1, Ljava/util/List;

    .line 1396616
    iget-object v0, p0, LX/8ke;->d:LX/8kj;

    const/4 v1, 0x0

    .line 1396617
    iput-object v1, v0, LX/8kj;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1396618
    iget-object v0, p0, LX/8ke;->d:LX/8kj;

    iget-boolean v0, v0, LX/8kj;->b:Z

    if-eqz v0, :cond_0

    .line 1396619
    :goto_0
    return-void

    .line 1396620
    :cond_0
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbservice/service/OperationResult;

    invoke-virtual {v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/service/FetchRecentStickersResult;

    .line 1396621
    const/4 v1, 0x1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbservice/service/OperationResult;

    invoke-virtual {v1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/stickers/service/FetchStickerPacksResult;

    .line 1396622
    iget-object v2, v1, Lcom/facebook/stickers/service/FetchStickerPacksResult;->b:LX/0am;

    move-object v2, v2

    .line 1396623
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1396624
    iget-object v2, p0, LX/8ke;->a:Ljava/util/List;

    .line 1396625
    iget-object v3, v1, Lcom/facebook/stickers/service/FetchStickerPacksResult;->b:LX/0am;

    move-object v1, v3

    .line 1396626
    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1396627
    :cond_1
    const/4 v1, 0x2

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbservice/service/OperationResult;

    invoke-virtual {v1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/stickers/service/FetchStickerPacksResult;

    .line 1396628
    iget-object v3, p0, LX/8ke;->b:LX/8kh;

    iget-object v4, p0, LX/8ke;->a:Ljava/util/List;

    .line 1396629
    invoke-static {v3, v4}, LX/8kj;->a(LX/8kh;Ljava/util/List;)LX/0Px;

    move-result-object v5

    move-object v2, v5

    .line 1396630
    iget-object v3, v1, Lcom/facebook/stickers/service/FetchStickerPacksResult;->b:LX/0am;

    move-object v1, v3

    .line 1396631
    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 1396632
    new-instance v3, LX/8ki;

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    iget-object v5, p0, LX/8ke;->a:Ljava/util/List;

    .line 1396633
    invoke-static {v5, v1}, LX/8kj;->a(Ljava/util/List;Ljava/util/List;)LX/0Px;

    move-result-object p1

    move-object v1, p1

    .line 1396634
    iget-object v4, v0, Lcom/facebook/stickers/service/FetchRecentStickersResult;->a:LX/0Px;

    move-object v0, v4

    .line 1396635
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    invoke-direct {v3, v2, v1, v0}, LX/8ki;-><init>(LX/0Px;LX/0Px;Ljava/util/List;)V

    .line 1396636
    iget-object v0, p0, LX/8ke;->d:LX/8kj;

    iget-object v0, v0, LX/8kj;->d:LX/3Mb;

    iget-object v1, p0, LX/8ke;->b:LX/8kh;

    invoke-interface {v0, v1, v3}, LX/3Mb;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1396637
    iget-object v0, p0, LX/8ke;->c:Lcom/google/common/util/concurrent/SettableFuture;

    const v1, 0x1d9ffb5c

    invoke-static {v0, v3, v1}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    goto :goto_0
.end method
