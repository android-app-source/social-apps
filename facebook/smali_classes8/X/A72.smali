.class public LX/A72;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:Ljava/lang/String;


# instance fields
.field public b:D

.field public c:Ljava/lang/String;

.field public d:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Character;",
            "LX/A72;",
            ">;"
        }
    .end annotation
.end field

.field public e:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1625463
    const-string v0, ""

    sput-object v0, LX/A72;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1625457
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1625458
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/A72;->b:D

    .line 1625459
    const/4 v0, 0x0

    iput-object v0, p0, LX/A72;->c:Ljava/lang/String;

    .line 1625460
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/A72;->d:Ljava/util/HashMap;

    .line 1625461
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/A72;->e:Z

    .line 1625462
    return-void
.end method

.method private a(Ljava/lang/String;I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1625449
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-ne p2, v0, :cond_1

    .line 1625450
    iget-boolean v0, p0, LX/A72;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/A72;->c:Ljava/lang/String;

    .line 1625451
    :goto_0
    return-object v0

    .line 1625452
    :cond_0
    sget-object v0, LX/A72;->a:Ljava/lang/String;

    goto :goto_0

    .line 1625453
    :cond_1
    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    .line 1625454
    iget-object v1, p0, LX/A72;->d:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1625455
    iget-object v1, p0, LX/A72;->d:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/A72;

    add-int/lit8 v1, p2, 0x1

    invoke-direct {v0, p1, v1}, LX/A72;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1625456
    :cond_2
    sget-object v0, LX/A72;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public static a(LX/A72;Ljava/lang/String;DI)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "DI)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1625434
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-ne p4, v0, :cond_4

    .line 1625435
    iget-wide v0, p0, LX/A72;->b:D

    cmpl-double v0, v0, p2

    if-ltz v0, :cond_3

    .line 1625436
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1625437
    iget-boolean v0, p0, LX/A72;->e:Z

    if-eqz v0, :cond_0

    .line 1625438
    iget-object v0, p0, LX/A72;->c:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1625439
    :cond_0
    iget-object v0, p0, LX/A72;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1625440
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/A72;

    .line 1625441
    iget-wide v4, v0, LX/A72;->b:D

    cmpl-double v3, v4, p2

    if-ltz v3, :cond_1

    .line 1625442
    iget-object v0, v0, LX/A72;->c:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 1625443
    :goto_1
    return-object v0

    .line 1625444
    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_1

    .line 1625445
    :cond_4
    invoke-virtual {p1, p4}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    .line 1625446
    iget-object v1, p0, LX/A72;->d:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1625447
    iget-object v1, p0, LX/A72;->d:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/A72;

    add-int/lit8 v1, p4, 0x1

    invoke-static {v0, p1, p2, p3, v1}, LX/A72;->a(LX/A72;Ljava/lang/String;DI)Ljava/util/List;

    move-result-object v0

    goto :goto_1

    .line 1625448
    :cond_5
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_1
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 1625417
    if-eqz p3, :cond_0

    .line 1625418
    int-to-double v0, p3

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    mul-double/2addr v0, v2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    int-to-double v2, v2

    div-double/2addr v0, v2

    .line 1625419
    iget-wide v2, p0, LX/A72;->b:D

    cmpl-double v2, v0, v2

    if-ltz v2, :cond_0

    .line 1625420
    iput-wide v0, p0, LX/A72;->b:D

    .line 1625421
    iput-object p2, p0, LX/A72;->c:Ljava/lang/String;

    .line 1625422
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-ne p3, v0, :cond_1

    .line 1625423
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/A72;->e:Z

    .line 1625424
    :goto_0
    return-void

    .line 1625425
    :cond_1
    invoke-virtual {p1, p3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    .line 1625426
    iget-object v1, p0, LX/A72;->d:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1625427
    iget-object v1, p0, LX/A72;->d:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/A72;

    add-int/lit8 v1, p3, 0x1

    invoke-direct {v0, p1, p2, v1}, LX/A72;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 1625428
    :cond_2
    new-instance v1, LX/A72;

    invoke-direct {v1}, LX/A72;-><init>()V

    .line 1625429
    add-int/lit8 v2, p3, 0x1

    invoke-direct {v1, p1, p2, v2}, LX/A72;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1625430
    iget-object v2, p0, LX/A72;->d:Ljava/util/HashMap;

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1625433
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/A72;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1625431
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/A72;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1625432
    return-void
.end method
