.class public final enum LX/AMd;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/AMd;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/AMd;

.field public static final enum UNZIP_TO_CACHE:LX/AMd;

.field public static final enum WRITE_TO_CACHE:LX/AMd;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1666340
    new-instance v0, LX/AMd;

    const-string v1, "WRITE_TO_CACHE"

    invoke-direct {v0, v1, v2}, LX/AMd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AMd;->WRITE_TO_CACHE:LX/AMd;

    .line 1666341
    new-instance v0, LX/AMd;

    const-string v1, "UNZIP_TO_CACHE"

    invoke-direct {v0, v1, v3}, LX/AMd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AMd;->UNZIP_TO_CACHE:LX/AMd;

    .line 1666342
    const/4 v0, 0x2

    new-array v0, v0, [LX/AMd;

    sget-object v1, LX/AMd;->WRITE_TO_CACHE:LX/AMd;

    aput-object v1, v0, v2

    sget-object v1, LX/AMd;->UNZIP_TO_CACHE:LX/AMd;

    aput-object v1, v0, v3

    sput-object v0, LX/AMd;->$VALUES:[LX/AMd;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1666343
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/AMd;
    .locals 1

    .prologue
    .line 1666344
    const-class v0, LX/AMd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/AMd;

    return-object v0
.end method

.method public static values()[LX/AMd;
    .locals 1

    .prologue
    .line 1666345
    sget-object v0, LX/AMd;->$VALUES:[LX/AMd;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/AMd;

    return-object v0
.end method
