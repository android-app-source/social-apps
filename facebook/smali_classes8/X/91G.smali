.class public LX/91G;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1430266
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1430267
    iput-object p1, p0, LX/91G;->a:LX/0Zb;

    .line 1430268
    return-void
.end method


# virtual methods
.method public final a(LX/91E;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1430269
    iget-object v0, p0, LX/91G;->a:LX/0Zb;

    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v2, LX/91F;->CONTEXTUAL_SUGGESTION_ACTION:LX/91F;

    iget-object v2, v2, LX/91F;->name:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "composer"

    .line 1430270
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1430271
    move-object v1, v1

    .line 1430272
    const-string v2, "activity_id"

    invoke-virtual {p2}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->bx_()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "place_id"

    invoke-static {p3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "suggestion_mechanism"

    invoke-virtual {p2}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "session_id"

    invoke-virtual {v1, v2, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "action"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1430273
    return-void
.end method
