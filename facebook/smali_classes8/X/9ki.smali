.class public LX/9ki;
.super LX/9kh;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/9ki;


# instance fields
.field public a:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;",
            ">;>;"
        }
    .end annotation
.end field

.field public final c:LX/0tX;

.field public final d:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1532086
    invoke-direct {p0}, LX/9kh;-><init>()V

    .line 1532087
    iput-object p1, p0, LX/9ki;->c:LX/0tX;

    .line 1532088
    iput-object p2, p0, LX/9ki;->d:LX/1Ck;

    .line 1532089
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/9ki;->a:LX/0am;

    .line 1532090
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/9ki;->b:Ljava/util/Map;

    .line 1532091
    return-void
.end method

.method public static a(LX/0QB;)LX/9ki;
    .locals 5

    .prologue
    .line 1532092
    sget-object v0, LX/9ki;->e:LX/9ki;

    if-nez v0, :cond_1

    .line 1532093
    const-class v1, LX/9ki;

    monitor-enter v1

    .line 1532094
    :try_start_0
    sget-object v0, LX/9ki;->e:LX/9ki;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1532095
    if-eqz v2, :cond_0

    .line 1532096
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1532097
    new-instance p0, LX/9ki;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-direct {p0, v3, v4}, LX/9ki;-><init>(LX/0tX;LX/1Ck;)V

    .line 1532098
    move-object v0, p0

    .line 1532099
    sput-object v0, LX/9ki;->e:LX/9ki;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1532100
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1532101
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1532102
    :cond_1
    sget-object v0, LX/9ki;->e:LX/9ki;

    return-object v0

    .line 1532103
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1532104
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static c(LX/9ki;Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;)Z
    .locals 2

    .prologue
    .line 1532105
    iget-object v0, p0, LX/9ki;->b:Ljava/util/Map;

    iget-object v1, p1, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/9ki;->a:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/9ki;->a:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
