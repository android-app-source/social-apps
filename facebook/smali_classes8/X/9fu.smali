.class public final LX/9fu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/photos/albums/protocols/MediasetQueryModels$MediasetQueryModel;",
        ">;",
        "Lcom/facebook/photos/albums/protocols/MediasetQueryInterfaces$DefaultMediaSetMediaConnection;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/9fx;


# direct methods
.method public constructor <init>(LX/9fx;)V
    .locals 0

    .prologue
    .line 1522772
    iput-object p1, p0, LX/9fu;->a:LX/9fx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1522773
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1522774
    if-eqz p1, :cond_0

    .line 1522775
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1522776
    if-nez v0, :cond_1

    .line 1522777
    :cond_0
    const/4 v0, 0x0

    .line 1522778
    :goto_0
    return-object v0

    .line 1522779
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1522780
    check-cast v0, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$MediasetQueryModel;

    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$MediasetQueryModel;->a()Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    move-result-object v0

    goto :goto_0
.end method
