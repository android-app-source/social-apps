.class public final LX/95x;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;

.field public final synthetic c:LX/961;


# direct methods
.method public constructor <init>(LX/961;ZLcom/facebook/api/ufiservices/common/TogglePageLikeParams;)V
    .locals 0

    .prologue
    .line 1438053
    iput-object p1, p0, LX/95x;->c:LX/961;

    iput-boolean p2, p0, LX/95x;->a:Z

    iput-object p3, p0, LX/95x;->b:Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1438054
    iget-boolean v0, p0, LX/95x;->a:Z

    if-eqz v0, :cond_0

    .line 1438055
    iget-object v0, p0, LX/95x;->c:LX/961;

    iget-object v0, v0, LX/961;->f:LX/25G;

    iget-object v1, p0, LX/95x;->b:Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;

    invoke-virtual {v0, v1}, LX/25G;->a(Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1438056
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/95x;->c:LX/961;

    iget-object v0, v0, LX/961;->e:LX/3iV;

    iget-object v1, p0, LX/95x;->b:Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/3iV;->a(Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method
