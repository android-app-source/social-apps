.class public final LX/95u;
.super LX/2h1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2h1",
        "<",
        "Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentHideUnhideFragmentModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1L9;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLComment;

.field public final synthetic c:LX/3iQ;


# direct methods
.method public constructor <init>(LX/3iQ;LX/1L9;Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 0

    .prologue
    .line 1438025
    iput-object p1, p0, LX/95u;->c:LX/3iQ;

    iput-object p2, p0, LX/95u;->a:LX/1L9;

    iput-object p3, p0, LX/95u;->b:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-direct {p0}, LX/2h1;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 2

    .prologue
    .line 1438026
    iget-object v0, p0, LX/95u;->a:LX/1L9;

    if-eqz v0, :cond_0

    .line 1438027
    iget-object v0, p0, LX/95u;->a:LX/1L9;

    iget-object v1, p0, LX/95u;->b:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-interface {v0, v1, p1}, LX/1L9;->a(Ljava/lang/Object;Lcom/facebook/fbservice/service/ServiceException;)V

    .line 1438028
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1438029
    check-cast p1, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentHideUnhideFragmentModel;

    .line 1438030
    iget-object v0, p0, LX/95u;->a:LX/1L9;

    if-eqz v0, :cond_1

    .line 1438031
    iget-object v0, p0, LX/95u;->b:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-static {v0}, LX/4Vu;->a(Lcom/facebook/graphql/model/GraphQLComment;)LX/4Vu;

    move-result-object v0

    .line 1438032
    if-eqz p1, :cond_0

    .line 1438033
    invoke-virtual {p1}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentHideUnhideFragmentModel;->k()Ljava/lang/String;

    move-result-object v1

    .line 1438034
    iput-object v1, v0, LX/4Vu;->D:Ljava/lang/String;

    .line 1438035
    :cond_0
    iget-object v1, p0, LX/95u;->a:LX/1L9;

    invoke-virtual {v0}, LX/4Vu;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    invoke-interface {v1, v0}, LX/1L9;->b(Ljava/lang/Object;)V

    .line 1438036
    :cond_1
    return-void
.end method
