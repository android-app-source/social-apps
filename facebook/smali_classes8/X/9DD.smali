.class public final LX/9DD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:LX/0i1;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/Runnable;

.field public final synthetic d:LX/9DG;


# direct methods
.method public constructor <init>(LX/9DG;LX/0i1;Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 1454828
    iput-object p1, p0, LX/9DD;->d:LX/9DG;

    iput-object p2, p0, LX/9DD;->a:LX/0i1;

    iput-object p3, p0, LX/9DD;->b:Ljava/lang/String;

    iput-object p4, p0, LX/9DD;->c:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 1454829
    iget-object v0, p0, LX/9DD;->a:LX/0i1;

    if-eqz v0, :cond_0

    const-string v0, "match_post_composer"

    iget-object v1, p0, LX/9DD;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1454830
    iget-object v0, p0, LX/9DD;->d:LX/9DG;

    iget-object v0, v0, LX/9DG;->q:LX/0iA;

    iget-object v1, p0, LX/9DD;->a:LX/0i1;

    invoke-interface {v1}, LX/0i1;->b()Ljava/lang/String;

    move-result-object v1

    .line 1454831
    invoke-virtual {v0}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v2

    .line 1454832
    sget-object v3, LX/6Xs;->RESET_VIEW_STATE:LX/6Xs;

    invoke-static {v2, v1, v3}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Lcom/facebook/interstitial/manager/InterstitialLogger;Ljava/lang/String;LX/6Xs;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v2, v3

    .line 1454833
    new-instance v3, LX/6Xw;

    invoke-direct {v3, v0, v1}, LX/6Xw;-><init>(LX/0iA;Ljava/lang/String;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object p2

    invoke-static {v2, v3, p2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1454834
    :cond_0
    const-string v0, "match_post_composer"

    iget-object v1, p0, LX/9DD;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1454835
    iget-object v0, p0, LX/9DD;->d:LX/9DG;

    iget-object v0, v0, LX/9DG;->L:LX/21n;

    invoke-interface {v0}, LX/21n;->a()V

    .line 1454836
    :cond_1
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 1454837
    iget-object v0, p0, LX/9DD;->c:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 1454838
    return-void
.end method
