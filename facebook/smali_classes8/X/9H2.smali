.class public LX/9H2;
.super Lcom/facebook/widget/FlowLayout;
.source ""

# interfaces
.implements LX/3Wr;
.implements LX/3Ws;


# instance fields
.field public a:LX/9HC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:LX/9HB;

.field private final c:LX/9HK;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1460800
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LX/9H2;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1460801
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 1460792
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/FlowLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1460793
    const-class v0, LX/9H2;

    invoke-static {v0, p0}, LX/9H2;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1460794
    invoke-virtual {p0}, LX/9H2;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1460795
    const v1, 0x7f030c5c

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1460796
    iget-object v0, p0, LX/9H2;->a:LX/9HC;

    invoke-virtual {v0, p0}, LX/9HC;->a(Landroid/view/View;)LX/9HB;

    move-result-object v0

    iput-object v0, p0, LX/9H2;->b:LX/9HB;

    .line 1460797
    iget-object v0, p0, LX/9H2;->b:LX/9HB;

    invoke-virtual {v0}, LX/9HB;->a()V

    .line 1460798
    new-instance v0, LX/9HK;

    invoke-direct {v0, p1}, LX/9HK;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/9H2;->c:LX/9HK;

    .line 1460799
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/9H2;

    const-class p0, LX/9HC;

    invoke-interface {v1, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/9HC;

    iput-object v1, p1, LX/9H2;->a:LX/9HC;

    return-void
.end method


# virtual methods
.method public final a(LX/9CP;)V
    .locals 1

    .prologue
    .line 1460782
    iget-object v0, p0, LX/9H2;->c:LX/9HK;

    invoke-virtual {v0, p1}, LX/9HK;->a(LX/9CP;)V

    .line 1460783
    return-void
.end method

.method public final a(Landroid/animation/ValueAnimator;)V
    .locals 1

    .prologue
    .line 1460790
    iget-object v0, p0, LX/9H2;->b:LX/9HB;

    invoke-virtual {v0, p1}, LX/9HB;->a(Landroid/animation/ValueAnimator;)V

    .line 1460791
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1460788
    iget-object v0, p0, LX/9H2;->c:LX/9HK;

    invoke-virtual {v0}, LX/9HK;->a()V

    .line 1460789
    return-void
.end method

.method public final dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1460787
    iget-object v0, p0, LX/9H2;->c:LX/9HK;

    invoke-virtual {v0, p1}, LX/9HK;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lcom/facebook/widget/FlowLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1460784
    invoke-super {p0, p1}, Lcom/facebook/widget/FlowLayout;->draw(Landroid/graphics/Canvas;)V

    .line 1460785
    iget-object v0, p0, LX/9H2;->c:LX/9HK;

    invoke-virtual {v0, p1}, LX/9HK;->a(Landroid/graphics/Canvas;)V

    .line 1460786
    return-void
.end method
