.class public LX/ACU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/adinterfaces/protocol/UploadAdImageMethod$Params;",
        "Lcom/facebook/adinterfaces/protocol/UploadAdImageMethod$Result;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private a:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1642768
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1642769
    return-void
.end method

.method public static a(LX/0QB;)LX/ACU;
    .locals 3

    .prologue
    .line 1642770
    const-class v1, LX/ACU;

    monitor-enter v1

    .line 1642771
    :try_start_0
    sget-object v0, LX/ACU;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1642772
    sput-object v2, LX/ACU;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1642773
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1642774
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1642775
    new-instance v0, LX/ACU;

    invoke-direct {v0}, LX/ACU;-><init>()V

    .line 1642776
    move-object v0, v0

    .line 1642777
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1642778
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/ACU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1642779
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1642780
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 5

    .prologue
    .line 1642781
    check-cast p1, Lcom/facebook/adinterfaces/protocol/UploadAdImageMethod$Params;

    .line 1642782
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1642783
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1642784
    new-instance v1, Ljava/io/File;

    .line 1642785
    iget-object v2, p1, Lcom/facebook/adinterfaces/protocol/UploadAdImageMethod$Params;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1642786
    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1642787
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, LX/ACU;->a:Ljava/lang/String;

    .line 1642788
    new-instance v2, LX/4ct;

    const-string v3, "image/jpeg"

    iget-object v4, p0, LX/ACU;->a:Ljava/lang/String;

    invoke-direct {v2, v1, v3, v4}, LX/4ct;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V

    .line 1642789
    new-instance v1, LX/4cQ;

    const-string v3, "source"

    invoke-direct {v1, v3, v2}, LX/4cQ;-><init>(Ljava/lang/String;LX/4cO;)V

    .line 1642790
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v2

    const-string v3, "uploadAdImage"

    .line 1642791
    iput-object v3, v2, LX/14O;->b:Ljava/lang/String;

    .line 1642792
    move-object v2, v2

    .line 1642793
    const-string v3, "POST"

    .line 1642794
    iput-object v3, v2, LX/14O;->c:Ljava/lang/String;

    .line 1642795
    move-object v2, v2

    .line 1642796
    const-string v3, "act_%s/adimages"

    .line 1642797
    iget-object v4, p1, Lcom/facebook/adinterfaces/protocol/UploadAdImageMethod$Params;->a:Ljava/lang/String;

    move-object v4, v4

    .line 1642798
    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1642799
    iput-object v3, v2, LX/14O;->d:Ljava/lang/String;

    .line 1642800
    move-object v2, v2

    .line 1642801
    iput-object v0, v2, LX/14O;->g:Ljava/util/List;

    .line 1642802
    move-object v0, v2

    .line 1642803
    sget-object v2, LX/14S;->JSON:LX/14S;

    .line 1642804
    iput-object v2, v0, LX/14O;->k:LX/14S;

    .line 1642805
    move-object v0, v0

    .line 1642806
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 1642807
    iput-object v1, v0, LX/14O;->l:Ljava/util/List;

    .line 1642808
    move-object v0, v0

    .line 1642809
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1642810
    const/4 v0, 0x0

    .line 1642811
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1642812
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v1

    const-string v2, "images"

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v1

    const-string v2, "images"

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    iget-object v2, p0, LX/ACU;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1642813
    :cond_0
    :goto_0
    return-object v0

    .line 1642814
    :cond_1
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v1

    const-string v2, "images"

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    iget-object v2, p0, LX/ACU;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 1642815
    const-string v2, "hash"

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v2, "url"

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1642816
    new-instance v0, Lcom/facebook/adinterfaces/protocol/UploadAdImageMethod$Result;

    const-string v2, "hash"

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-virtual {v2}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v2

    const-string v3, "url"

    invoke-virtual {v1, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-virtual {v1}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lcom/facebook/adinterfaces/protocol/UploadAdImageMethod$Result;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
