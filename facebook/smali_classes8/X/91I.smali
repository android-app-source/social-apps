.class public LX/91I;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Ljava/lang/String;

.field private b:I

.field private c:I

.field private d:Z

.field private e:Ljava/lang/String;

.field private f:I

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 1430315
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1430316
    iput-object v1, p0, LX/91I;->a:Ljava/lang/String;

    .line 1430317
    iput v0, p0, LX/91I;->b:I

    .line 1430318
    iput v0, p0, LX/91I;->c:I

    .line 1430319
    iput-boolean v0, p0, LX/91I;->d:Z

    .line 1430320
    iput-object v1, p0, LX/91I;->e:Ljava/lang/String;

    .line 1430321
    const/4 v0, -0x1

    iput v0, p0, LX/91I;->f:I

    .line 1430322
    iput-object p1, p0, LX/91I;->g:Ljava/lang/String;

    .line 1430323
    return-void
.end method

.method private static a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1430312
    if-eqz p2, :cond_0

    .line 1430313
    invoke-virtual {p0, p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1430314
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()LX/91I;
    .locals 1

    .prologue
    .line 1430324
    iget v0, p0, LX/91I;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/91I;->c:I

    .line 1430325
    return-object p0
.end method

.method public final a(Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;I)LX/91I;
    .locals 1

    .prologue
    .line 1430307
    invoke-virtual {p1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->e()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->e()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->e()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/91I;->e:Ljava/lang/String;

    .line 1430308
    iput p2, p0, LX/91I;->f:I

    .line 1430309
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/91I;->d:Z

    .line 1430310
    return-object p0

    .line 1430311
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)LX/91I;
    .locals 1

    .prologue
    .line 1430303
    iput-object p1, p0, LX/91I;->a:Ljava/lang/String;

    .line 1430304
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    :goto_0
    iput v0, p0, LX/91I;->b:I

    .line 1430305
    return-object p0

    .line 1430306
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 3

    .prologue
    .line 1430292
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "minutiae_session_finished"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1430293
    const-string v1, "taggable_objects"

    .line 1430294
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1430295
    const-string v1, "query"

    iget-object v2, p0, LX/91I;->a:Ljava/lang/String;

    invoke-static {v0, v1, v2}, LX/91I;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;Ljava/lang/String;)V

    .line 1430296
    const-string v1, "selected_object_id"

    iget-object v2, p0, LX/91I;->e:Ljava/lang/String;

    invoke-static {v0, v1, v2}, LX/91I;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;Ljava/lang/String;)V

    .line 1430297
    const-string v1, "composer_session_id"

    iget-object v2, p0, LX/91I;->g:Ljava/lang/String;

    invoke-static {v0, v1, v2}, LX/91I;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;Ljava/lang/String;)V

    .line 1430298
    const-string v1, "query_length"

    iget v2, p0, LX/91I;->b:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1430299
    const-string v1, "query_count"

    iget v2, p0, LX/91I;->c:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1430300
    const-string v1, "has_clicked"

    iget-boolean v2, p0, LX/91I;->d:Z

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1430301
    const-string v1, "selected_object_position"

    iget v2, p0, LX/91I;->f:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1430302
    return-object v0
.end method
