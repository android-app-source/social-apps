.class public final enum LX/8ue;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8ue;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8ue;

.field public static final enum ACTIVE_NOW:LX/8ue;

.field public static final enum BIRTHDAY:LX/8ue;

.field public static final enum BROADCASTER:LX/8ue;

.field public static final enum EVENT_REMINDER_DECLINED:LX/8ue;

.field public static final enum EVENT_REMINDER_GOING:LX/8ue;

.field public static final enum FACEBOOK:LX/8ue;

.field public static final enum MESSENGER:LX/8ue;

.field public static final enum MESSENGER_AUDIO:LX/8ue;

.field public static final enum MESSENGER_VIDEO:LX/8ue;

.field public static final enum NONE:LX/8ue;

.field public static final enum SMS:LX/8ue;

.field public static final enum TINCAN:LX/8ue;

.field public static final enum VERIFIED:LX/8ue;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1415394
    new-instance v0, LX/8ue;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, LX/8ue;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8ue;->NONE:LX/8ue;

    .line 1415395
    new-instance v0, LX/8ue;

    const-string v1, "FACEBOOK"

    invoke-direct {v0, v1, v4}, LX/8ue;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8ue;->FACEBOOK:LX/8ue;

    .line 1415396
    new-instance v0, LX/8ue;

    const-string v1, "MESSENGER"

    invoke-direct {v0, v1, v5}, LX/8ue;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8ue;->MESSENGER:LX/8ue;

    .line 1415397
    new-instance v0, LX/8ue;

    const-string v1, "BIRTHDAY"

    invoke-direct {v0, v1, v6}, LX/8ue;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8ue;->BIRTHDAY:LX/8ue;

    .line 1415398
    new-instance v0, LX/8ue;

    const-string v1, "VERIFIED"

    invoke-direct {v0, v1, v7}, LX/8ue;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8ue;->VERIFIED:LX/8ue;

    .line 1415399
    new-instance v0, LX/8ue;

    const-string v1, "SMS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/8ue;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8ue;->SMS:LX/8ue;

    .line 1415400
    new-instance v0, LX/8ue;

    const-string v1, "MESSENGER_AUDIO"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/8ue;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8ue;->MESSENGER_AUDIO:LX/8ue;

    .line 1415401
    new-instance v0, LX/8ue;

    const-string v1, "MESSENGER_VIDEO"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/8ue;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8ue;->MESSENGER_VIDEO:LX/8ue;

    .line 1415402
    new-instance v0, LX/8ue;

    const-string v1, "BROADCASTER"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/8ue;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8ue;->BROADCASTER:LX/8ue;

    .line 1415403
    new-instance v0, LX/8ue;

    const-string v1, "TINCAN"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/8ue;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8ue;->TINCAN:LX/8ue;

    .line 1415404
    new-instance v0, LX/8ue;

    const-string v1, "EVENT_REMINDER_GOING"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/8ue;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8ue;->EVENT_REMINDER_GOING:LX/8ue;

    .line 1415405
    new-instance v0, LX/8ue;

    const-string v1, "EVENT_REMINDER_DECLINED"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/8ue;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8ue;->EVENT_REMINDER_DECLINED:LX/8ue;

    .line 1415406
    new-instance v0, LX/8ue;

    const-string v1, "ACTIVE_NOW"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/8ue;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8ue;->ACTIVE_NOW:LX/8ue;

    .line 1415407
    const/16 v0, 0xd

    new-array v0, v0, [LX/8ue;

    sget-object v1, LX/8ue;->NONE:LX/8ue;

    aput-object v1, v0, v3

    sget-object v1, LX/8ue;->FACEBOOK:LX/8ue;

    aput-object v1, v0, v4

    sget-object v1, LX/8ue;->MESSENGER:LX/8ue;

    aput-object v1, v0, v5

    sget-object v1, LX/8ue;->BIRTHDAY:LX/8ue;

    aput-object v1, v0, v6

    sget-object v1, LX/8ue;->VERIFIED:LX/8ue;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/8ue;->SMS:LX/8ue;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/8ue;->MESSENGER_AUDIO:LX/8ue;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/8ue;->MESSENGER_VIDEO:LX/8ue;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/8ue;->BROADCASTER:LX/8ue;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/8ue;->TINCAN:LX/8ue;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/8ue;->EVENT_REMINDER_GOING:LX/8ue;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/8ue;->EVENT_REMINDER_DECLINED:LX/8ue;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/8ue;->ACTIVE_NOW:LX/8ue;

    aput-object v2, v0, v1

    sput-object v0, LX/8ue;->$VALUES:[LX/8ue;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1415408
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8ue;
    .locals 1

    .prologue
    .line 1415409
    const-class v0, LX/8ue;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8ue;

    return-object v0
.end method

.method public static values()[LX/8ue;
    .locals 1

    .prologue
    .line 1415410
    sget-object v0, LX/8ue;->$VALUES:[LX/8ue;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8ue;

    return-object v0
.end method
