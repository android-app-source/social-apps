.class public LX/9kq;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Landroid/widget/ImageView;

.field public b:Landroid/widget/TextView;

.field public c:Landroid/widget/TextView;

.field public d:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1532219
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1532220
    const p1, 0x7f030f66

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1532221
    const/4 p1, 0x1

    invoke-virtual {p0, p1}, LX/9kq;->setOrientation(I)V

    .line 1532222
    const p1, 0x7f0d2524

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, LX/9kq;->a:Landroid/widget/ImageView;

    .line 1532223
    const p1, 0x7f0d2525

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, LX/9kq;->b:Landroid/widget/TextView;

    .line 1532224
    const p1, 0x7f0d2526

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, LX/9kq;->c:Landroid/widget/TextView;

    .line 1532225
    const p1, 0x7f0d2527

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, LX/9kq;->d:Landroid/widget/TextView;

    .line 1532226
    return-void
.end method


# virtual methods
.method public setImage(I)V
    .locals 1

    .prologue
    .line 1532227
    iget-object v0, p0, LX/9kq;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1532228
    return-void
.end method

.method public setSectionTitle(I)V
    .locals 1

    .prologue
    .line 1532229
    iget-object v0, p0, LX/9kq;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 1532230
    return-void
.end method

.method public setSubTitle(I)V
    .locals 1

    .prologue
    .line 1532231
    iget-object v0, p0, LX/9kq;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 1532232
    return-void
.end method

.method public setTitle(I)V
    .locals 1

    .prologue
    .line 1532233
    iget-object v0, p0, LX/9kq;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 1532234
    return-void
.end method
