.class public final LX/928;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/composer/minutiae/model/MinutiaeObject;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Ve;

.field public final synthetic b:LX/929;


# direct methods
.method public constructor <init>(LX/929;LX/0Ve;)V
    .locals 0

    .prologue
    .line 1431999
    iput-object p1, p0, LX/928;->b:LX/929;

    iput-object p2, p0, LX/928;->a:LX/0Ve;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1432000
    iget-object v0, p0, LX/928;->b:LX/929;

    iget-object v0, v0, LX/929;->c:LX/03V;

    sget-object v1, LX/929;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1432001
    iget-object v0, p0, LX/928;->a:LX/0Ve;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 1432002
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1432003
    check-cast p1, Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1432004
    if-nez p1, :cond_0

    .line 1432005
    new-instance v0, Ljava/lang/Throwable;

    invoke-direct {v0}, Ljava/lang/Throwable;-><init>()V

    invoke-virtual {p0, v0}, LX/928;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    .line 1432006
    :goto_0
    return-void

    .line 1432007
    :cond_0
    iget-object v0, p0, LX/928;->a:LX/0Ve;

    invoke-interface {v0, p1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    goto :goto_0
.end method
