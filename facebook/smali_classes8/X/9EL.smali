.class public LX/9EL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/9CC;

.field public final b:LX/20j;

.field public c:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/9CC;LX/20j;)V
    .locals 2
    .param p1    # LX/9CC;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1456951
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1456952
    iput-object p1, p0, LX/9EL;->a:LX/9CC;

    .line 1456953
    iput-object p2, p0, LX/9EL;->b:LX/20j;

    .line 1456954
    new-instance v0, LX/3dM;

    invoke-direct {v0}, LX/3dM;-><init>()V

    sget-object v1, LX/16z;->f:Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    invoke-virtual {v0, v1}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;)LX/3dM;

    move-result-object v0

    invoke-virtual {v0}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 1456955
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    iput-object v0, p0, LX/9EL;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1456956
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1456957
    if-nez p1, :cond_0

    .line 1456958
    const/4 v0, 0x0

    iput-object v0, p0, LX/9EL;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1456959
    :goto_0
    iget-object v0, p0, LX/9EL;->a:LX/9CC;

    iget-object v1, p0, LX/9EL;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0, v1}, LX/9CC;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1456960
    return-void

    .line 1456961
    :cond_0
    iget-object v0, p0, LX/9EL;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v0, :cond_3

    sget-object v0, LX/16z;->f:Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-object v1, v0

    .line 1456962
    :goto_1
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1456963
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v0}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/3dM;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;)LX/3dM;

    move-result-object v0

    invoke-virtual {v0}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 1456964
    iget-object v2, p0, LX/9EL;->b:LX/20j;

    .line 1456965
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1456966
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    const/4 v3, 0x0

    .line 1456967
    invoke-static {v1}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/3dM;

    move-result-object v4

    iget-object v5, v2, LX/20j;->b:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v5

    .line 1456968
    iput-wide v5, v4, LX/3dM;->v:J

    .line 1456969
    move-object v6, v4

    .line 1456970
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 1456971
    invoke-static {v1}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v5, v3

    move v4, v3

    :goto_2
    if-ge v5, v9, :cond_2

    invoke-virtual {v8, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1456972
    invoke-static {v0, v3}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 1456973
    add-int/lit8 v3, v4, 0x1

    .line 1456974
    :goto_3
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move v4, v3

    goto :goto_2

    .line 1456975
    :cond_1
    invoke-virtual {v7, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move v3, v4

    goto :goto_3

    .line 1456976
    :cond_2
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-static {v1}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v5

    sub-int v4, v5, v4

    invoke-static {v1}, LX/16z;->g(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v5

    invoke-static {v6, v3, v4, v5}, LX/20j;->a(LX/3dM;Ljava/util/List;ILcom/facebook/graphql/model/GraphQLPageInfo;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    move-object v0, v3

    .line 1456977
    invoke-virtual {p1, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->b(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    iput-object v0, p0, LX/9EL;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    goto :goto_0

    .line 1456978
    :cond_3
    iget-object v0, p0, LX/9EL;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1456979
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1456980
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->N()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v0

    move-object v1, v0

    goto :goto_1
.end method
