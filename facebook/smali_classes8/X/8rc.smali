.class public final LX/8rc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/graphql/model/GraphQLActor;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/ufiservices/ui/ProfileListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/ufiservices/ui/ProfileListFragment;)V
    .locals 0

    .prologue
    .line 1409012
    iput-object p1, p0, LX/8rc;->a:Lcom/facebook/ufiservices/ui/ProfileListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1409019
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1409013
    check-cast p1, Ljava/util/List;

    .line 1409014
    iget-object v0, p0, LX/8rc;->a:Lcom/facebook/ufiservices/ui/ProfileListFragment;

    .line 1409015
    if-nez p1, :cond_0

    .line 1409016
    :goto_0
    return-void

    .line 1409017
    :cond_0
    iget-object v1, v0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->m:LX/55h;

    invoke-virtual {v0, p1}, Lcom/facebook/ufiservices/ui/ProfileListFragment;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    invoke-virtual {v1, p0}, LX/55h;->a(Ljava/lang/Iterable;)V

    .line 1409018
    iget-object v1, v0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->l:Landroid/widget/BaseAdapter;

    const p0, -0x72de7c50

    invoke-static {v1, p0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0
.end method
