.class public final LX/9iL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0hc;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/photogallery/PhotoGallery;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/photogallery/PhotoGallery;)V
    .locals 0

    .prologue
    .line 1527625
    iput-object p1, p0, LX/9iL;->a:Lcom/facebook/photos/photogallery/PhotoGallery;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final B_(I)V
    .locals 2

    .prologue
    .line 1527626
    iget-object v0, p0, LX/9iL;->a:Lcom/facebook/photos/photogallery/PhotoGallery;

    iget-boolean v0, v0, Lcom/facebook/photos/photogallery/PhotoGallery;->j:Z

    if-eqz v0, :cond_0

    .line 1527627
    :goto_0
    return-void

    .line 1527628
    :cond_0
    iget-object v0, p0, LX/9iL;->a:Lcom/facebook/photos/photogallery/PhotoGallery;

    iget-object v0, v0, Lcom/facebook/photos/photogallery/PhotoGallery;->f:LX/9iH;

    if-eqz v0, :cond_1

    .line 1527629
    iget-object v0, p0, LX/9iL;->a:Lcom/facebook/photos/photogallery/PhotoGallery;

    iget-object v0, v0, Lcom/facebook/photos/photogallery/PhotoGallery;->f:LX/9iH;

    iget-object v1, p0, LX/9iL;->a:Lcom/facebook/photos/photogallery/PhotoGallery;

    invoke-static {v1, p1}, Lcom/facebook/photos/photogallery/PhotoGallery;->b(Lcom/facebook/photos/photogallery/PhotoGallery;I)LX/9iQ;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/9iH;->a(ILX/9iQ;)V

    .line 1527630
    :cond_1
    iget-object v0, p0, LX/9iL;->a:Lcom/facebook/photos/photogallery/PhotoGallery;

    const/4 v1, 0x1

    .line 1527631
    iput-boolean v1, v0, Lcom/facebook/photos/photogallery/PhotoGallery;->i:Z

    .line 1527632
    goto :goto_0
.end method

.method public final a(IFI)V
    .locals 1

    .prologue
    .line 1527633
    iget-object v0, p0, LX/9iL;->a:Lcom/facebook/photos/photogallery/PhotoGallery;

    iget-boolean v0, v0, Lcom/facebook/photos/photogallery/PhotoGallery;->j:Z

    if-eqz v0, :cond_0

    .line 1527634
    :cond_0
    return-void
.end method

.method public final b(I)V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 1527635
    iget-object v0, p0, LX/9iL;->a:Lcom/facebook/photos/photogallery/PhotoGallery;

    iget-boolean v0, v0, Lcom/facebook/photos/photogallery/PhotoGallery;->j:Z

    if-eqz v0, :cond_1

    .line 1527636
    :cond_0
    :goto_0
    return-void

    .line 1527637
    :cond_1
    if-nez p1, :cond_0

    iget-object v0, p0, LX/9iL;->a:Lcom/facebook/photos/photogallery/PhotoGallery;

    iget-object v0, v0, Lcom/facebook/photos/photogallery/PhotoGallery;->g:LX/9iQ;

    iget-object v1, p0, LX/9iL;->a:Lcom/facebook/photos/photogallery/PhotoGallery;

    invoke-virtual {v1}, Lcom/facebook/photos/photogallery/PhotoGallery;->getCurrentPhotoView()LX/9iQ;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1527638
    iget-object v0, p0, LX/9iL;->a:Lcom/facebook/photos/photogallery/PhotoGallery;

    iget-object v0, v0, Lcom/facebook/photos/photogallery/PhotoGallery;->g:LX/9iQ;

    invoke-virtual {v0}, LX/9iQ;->getZoomableImageView()Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    move-result-object v0

    invoke-virtual {v0}, LX/7UQ;->getScale()F

    move-result v0

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_2

    .line 1527639
    iget-object v0, p0, LX/9iL;->a:Lcom/facebook/photos/photogallery/PhotoGallery;

    iget-object v0, v0, Lcom/facebook/photos/photogallery/PhotoGallery;->g:LX/9iQ;

    invoke-virtual {v0}, LX/9iQ;->getZoomableImageView()Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->c(F)V

    .line 1527640
    :cond_2
    iget-object v0, p0, LX/9iL;->a:Lcom/facebook/photos/photogallery/PhotoGallery;

    iget-object v1, p0, LX/9iL;->a:Lcom/facebook/photos/photogallery/PhotoGallery;

    invoke-virtual {v1}, Lcom/facebook/photos/photogallery/PhotoGallery;->getCurrentPhotoView()LX/9iQ;

    move-result-object v1

    .line 1527641
    iput-object v1, v0, Lcom/facebook/photos/photogallery/PhotoGallery;->g:LX/9iQ;

    .line 1527642
    goto :goto_0
.end method
