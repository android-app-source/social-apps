.class public final LX/8yi;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/8yi;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/8yg;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/8yj;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1426392
    const/4 v0, 0x0

    sput-object v0, LX/8yi;->a:LX/8yi;

    .line 1426393
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/8yi;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1426389
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1426390
    new-instance v0, LX/8yj;

    invoke-direct {v0}, LX/8yj;-><init>()V

    iput-object v0, p0, LX/8yi;->c:LX/8yj;

    .line 1426391
    return-void
.end method

.method public static c(LX/1De;)LX/8yg;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1426372
    new-instance v1, LX/8yh;

    invoke-direct {v1}, LX/8yh;-><init>()V

    .line 1426373
    sget-object v2, LX/8yi;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8yg;

    .line 1426374
    if-nez v2, :cond_0

    .line 1426375
    new-instance v2, LX/8yg;

    invoke-direct {v2}, LX/8yg;-><init>()V

    .line 1426376
    :cond_0
    invoke-static {v2, p0, v0, v0, v1}, LX/8yg;->a$redex0(LX/8yg;LX/1De;IILX/8yh;)V

    .line 1426377
    move-object v1, v2

    .line 1426378
    move-object v0, v1

    .line 1426379
    return-object v0
.end method

.method public static declared-synchronized q()LX/8yi;
    .locals 2

    .prologue
    .line 1426385
    const-class v1, LX/8yi;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/8yi;->a:LX/8yi;

    if-nez v0, :cond_0

    .line 1426386
    new-instance v0, LX/8yi;

    invoke-direct {v0}, LX/8yi;-><init>()V

    sput-object v0, LX/8yi;->a:LX/8yi;

    .line 1426387
    :cond_0
    sget-object v0, LX/8yi;->a:LX/8yi;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1426388
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 1426382
    check-cast p2, LX/8yh;

    .line 1426383
    iget v1, p2, LX/8yh;->a:F

    iget v2, p2, LX/8yh;->b:I

    iget-object v3, p2, LX/8yh;->c:LX/1dc;

    iget-object v4, p2, LX/8yh;->d:LX/1dc;

    iget-object v5, p2, LX/8yh;->e:LX/1dc;

    move-object v0, p1

    invoke-static/range {v0 .. v5}, LX/8yj;->a(LX/1De;FILX/1dc;LX/1dc;LX/1dc;)LX/1Dg;

    move-result-object v0

    .line 1426384
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1426380
    invoke-static {}, LX/1dS;->b()V

    .line 1426381
    const/4 v0, 0x0

    return-object v0
.end method
