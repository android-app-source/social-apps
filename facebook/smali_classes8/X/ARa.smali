.class public final LX/ARa;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:LX/Hqx;

.field public final synthetic b:LX/ARb;


# direct methods
.method public constructor <init>(LX/ARb;LX/Hqx;)V
    .locals 0

    .prologue
    .line 1673076
    iput-object p1, p0, LX/ARa;->b:LX/ARb;

    iput-object p2, p0, LX/ARa;->a:LX/Hqx;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 1673077
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v0

    .line 1673078
    sget-object v1, LX/1nY;->API_ERROR:LX/1nY;

    if-ne v0, v1, :cond_0

    .line 1673079
    iget-object v0, p0, LX/ARa;->b:LX/ARb;

    iget-object v0, v0, LX/ARb;->c:LX/03V;

    const-string v1, "composer_target_and_privacy_page_fetch_error"

    const-string v2, "Page info fetch failure"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1673080
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1673081
    iget-object v0, p0, LX/ARa;->a:LX/Hqx;

    invoke-virtual {v0}, LX/Hqx;->a()V

    .line 1673082
    return-void
.end method
