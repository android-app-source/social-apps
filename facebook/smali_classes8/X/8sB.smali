.class public LX/8sB;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/3iM;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Uh;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Or;LX/3iM;LX/0Ot;LX/0Uh;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/3iM;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1409823
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1409824
    iput-object p1, p0, LX/8sB;->a:Landroid/content/Context;

    .line 1409825
    iput-object p2, p0, LX/8sB;->b:LX/0Or;

    .line 1409826
    iput-object p3, p0, LX/8sB;->c:LX/3iM;

    .line 1409827
    iput-object p4, p0, LX/8sB;->d:LX/0Ot;

    .line 1409828
    iput-object p5, p0, LX/8sB;->e:LX/0Uh;

    .line 1409829
    return-void
.end method

.method public static a(LX/0QB;)LX/8sB;
    .locals 1

    .prologue
    .line 1409822
    invoke-static {p0}, LX/8sB;->b(LX/0QB;)LX/8sB;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/8sB;
    .locals 6

    .prologue
    .line 1409820
    new-instance v0, LX/8sB;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    const/16 v2, 0x19e

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p0}, LX/3iM;->a(LX/0QB;)LX/3iM;

    move-result-object v3

    check-cast v3, LX/3iM;

    const/16 v4, 0x259

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-direct/range {v0 .. v5}, LX/8sB;-><init>(Landroid/content/Context;LX/0Or;LX/3iM;LX/0Ot;LX/0Uh;)V

    .line 1409821
    return-object v0
.end method

.method public static b(LX/8sB;Lcom/facebook/graphql/model/GraphQLComment;)Z
    .locals 2

    .prologue
    .line 1409830
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, LX/8sB;->c:LX/3iM;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->H()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3iM;->c(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->OFFLINE:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-ne v0, v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/graphql/model/GraphQLComment;)Z
    .locals 1

    .prologue
    .line 1409819
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/36l;->e(Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(LX/8sB;Lcom/facebook/graphql/model/GraphQLComment;)Z
    .locals 2

    .prologue
    .line 1409814
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8sB;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1409815
    iget-boolean v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v0, v1

    .line 1409816
    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, LX/8sB;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1409817
    iget-object p0, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, p0

    .line 1409818
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLComment;)Z
    .locals 2
    .param p1    # Lcom/facebook/graphql/model/GraphQLComment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 1409809
    if-nez p1, :cond_1

    .line 1409810
    :cond_0
    :goto_0
    return v0

    .line 1409811
    :cond_1
    invoke-static {p1}, LX/36l;->e(Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1409812
    invoke-static {p0, p1}, LX/8sB;->b(LX/8sB;Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result v0

    goto :goto_0

    .line 1409813
    :cond_2
    invoke-static {p1}, LX/8sB;->c(Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-static {p0, p1}, LX/8sB;->b(LX/8sB;Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-static {p1}, LX/36l;->b(Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLComment;Landroid/view/View;Landroid/content/Context;LX/9Hi;LX/2yQ;LX/2dD;)Z
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1409772
    if-nez p3, :cond_1

    .line 1409773
    :cond_0
    :goto_0
    return v5

    .line 1409774
    :cond_1
    invoke-virtual {p0, p1}, LX/8sB;->a(Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1409775
    new-instance v7, LX/6WS;

    iget-object v0, p0, LX/8sB;->a:Landroid/content/Context;

    invoke-direct {v7, v0, v6}, LX/6WS;-><init>(Landroid/content/Context;I)V

    .line 1409776
    invoke-virtual {v7}, LX/5OM;->c()LX/5OG;

    move-result-object v1

    move-object v0, p0

    move-object v2, p3

    move-object v3, p1

    move-object v4, p4

    .line 1409777
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLComment;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p0

    if-eqz p0, :cond_2

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLComment;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_2

    .line 1409778
    const p0, 0x7f080fcd

    invoke-virtual {v2, p0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-interface {v1, p0}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object p0

    new-instance p1, LX/8s4;

    invoke-direct {p1, v0, v4, v3}, LX/8s4;-><init>(LX/8sB;LX/9Hi;Lcom/facebook/graphql/model/GraphQLComment;)V

    invoke-interface {p0, p1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    move-result-object p0

    .line 1409779
    if-eqz v5, :cond_2

    .line 1409780
    const p1, 0x7f0208fd

    invoke-interface {p0, p1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 1409781
    :cond_2
    iget-object p0, v0, LX/8sB;->e:LX/0Uh;

    const/16 p1, 0x289

    const/4 p3, 0x0

    invoke-virtual {p0, p1, p3}, LX/0Uh;->a(IZ)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1409782
    invoke-static {v3}, LX/8sC;->a(Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result p0

    if-eqz p0, :cond_9

    const p0, 0x7f080fd0

    :goto_1
    invoke-virtual {v2, p0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-interface {v1, p0}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object p0

    new-instance p1, LX/8s5;

    invoke-direct {p1, v0, v4, v3}, LX/8s5;-><init>(LX/8sB;LX/9Hi;Lcom/facebook/graphql/model/GraphQLComment;)V

    invoke-interface {p0, p1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    move-result-object p0

    .line 1409783
    if-eqz v5, :cond_3

    .line 1409784
    const p1, 0x7f0208cf

    invoke-interface {p0, p1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 1409785
    :cond_3
    invoke-static {v0, v3}, LX/8sB;->b(LX/8sB;Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1409786
    const p0, 0x7f080fce

    invoke-virtual {v2, p0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-interface {v1, p0}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object p0

    new-instance p1, LX/8s6;

    invoke-direct {p1, v0, v3, v4}, LX/8s6;-><init>(LX/8sB;Lcom/facebook/graphql/model/GraphQLComment;LX/9Hi;)V

    invoke-interface {p0, p1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    move-result-object p0

    .line 1409787
    if-eqz v5, :cond_4

    .line 1409788
    const p1, 0x7f020a0b

    invoke-interface {p0, p1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 1409789
    :cond_4
    invoke-static {v0, v3}, LX/8sB;->e(LX/8sB;Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1409790
    const p0, 0x7f080fd7

    invoke-virtual {v2, p0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-interface {v1, p0}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object p0

    new-instance p1, LX/8s7;

    invoke-direct {p1, v0, v3, v4}, LX/8s7;-><init>(LX/8sB;Lcom/facebook/graphql/model/GraphQLComment;LX/9Hi;)V

    invoke-interface {p0, p1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    move-result-object p0

    .line 1409791
    if-eqz v5, :cond_5

    .line 1409792
    const p1, 0x7f0208cf

    invoke-interface {p0, p1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 1409793
    :cond_5
    invoke-static {v3}, LX/8sB;->c(Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 1409794
    const p0, 0x7f080fd1

    invoke-virtual {v2, p0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-interface {v1, p0}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object p0

    new-instance p1, LX/8s8;

    invoke-direct {p1, v0, v4, v3}, LX/8s8;-><init>(LX/8sB;LX/9Hi;Lcom/facebook/graphql/model/GraphQLComment;)V

    invoke-interface {p0, p1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    move-result-object p0

    .line 1409795
    if-eqz v5, :cond_6

    .line 1409796
    const p1, 0x7f020952

    invoke-interface {p0, p1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 1409797
    :cond_6
    invoke-static {v0, v3}, LX/8sB;->b(LX/8sB;Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result p0

    if-nez p0, :cond_a

    invoke-static {v3}, LX/36l;->e(Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result p0

    if-nez p0, :cond_a

    const/4 p0, 0x1

    :goto_2
    move p0, p0

    .line 1409798
    if-eqz p0, :cond_7

    .line 1409799
    const p0, 0x7f080fd2

    invoke-virtual {v2, p0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-interface {v1, p0}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object p0

    new-instance p1, LX/8s9;

    invoke-direct {p1, v0, v4, v3}, LX/8s9;-><init>(LX/8sB;LX/9Hi;Lcom/facebook/graphql/model/GraphQLComment;)V

    invoke-interface {p0, p1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    move-result-object p0

    .line 1409800
    if-eqz v5, :cond_7

    .line 1409801
    const p1, 0x7f0209ae

    invoke-interface {p0, p1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 1409802
    :cond_7
    if-eqz v6, :cond_8

    .line 1409803
    const p0, 0x7f080017

    invoke-virtual {v2, p0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-interface {v1, p0}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object p0

    new-instance p1, LX/8sA;

    invoke-direct {p1, v0}, LX/8sA;-><init>(LX/8sB;)V

    invoke-interface {p0, p1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1409804
    :cond_8
    iput-object p5, v7, LX/0ht;->I:LX/2yQ;

    .line 1409805
    iput-object p6, v7, LX/0ht;->H:LX/2dD;

    .line 1409806
    invoke-virtual {v7, p2}, LX/0ht;->f(Landroid/view/View;)V

    move v5, v6

    .line 1409807
    goto/16 :goto_0

    .line 1409808
    :cond_9
    const p0, 0x7f080fcf

    goto/16 :goto_1

    :cond_a
    const/4 p0, 0x0

    goto :goto_2
.end method
