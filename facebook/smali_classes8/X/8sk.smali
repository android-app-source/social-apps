.class public LX/8sk;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0SG;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/8sj;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/Runnable;

.field public d:J


# direct methods
.method private constructor <init>(LX/0SG;)V
    .locals 1

    .prologue
    .line 1411757
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1411758
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/8sk;->b:Ljava/util/List;

    .line 1411759
    iput-object p1, p0, LX/8sk;->a:LX/0SG;

    .line 1411760
    return-void
.end method

.method public static a(LX/0SG;)LX/8sk;
    .locals 1

    .prologue
    .line 1411761
    new-instance v0, LX/8sk;

    invoke-direct {v0, p0}, LX/8sk;-><init>(LX/0SG;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Runnable;)LX/8sk;
    .locals 0

    .prologue
    .line 1411762
    iput-object p1, p0, LX/8sk;->c:Ljava/lang/Runnable;

    .line 1411763
    return-object p0
.end method

.method public final a()LX/8sm;
    .locals 7

    .prologue
    .line 1411764
    new-instance v1, LX/8sm;

    iget-object v2, p0, LX/8sk;->a:LX/0SG;

    iget-object v3, p0, LX/8sk;->c:Ljava/lang/Runnable;

    iget-wide v4, p0, LX/8sk;->d:J

    iget-object v0, p0, LX/8sk;->b:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, LX/8sm;-><init>(LX/0SG;Ljava/lang/Runnable;JLX/0Px;)V

    return-object v1
.end method
