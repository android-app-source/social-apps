.class public abstract LX/98h;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "F:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public a:J

.field public b:J

.field public c:LX/0SI;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/auth/viewercontext/ViewerContext;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:LX/98g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/98g",
            "<TT;TF;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1446230
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a(Landroid/content/Context;Landroid/content/Intent;)LX/98g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/content/Intent;",
            ")",
            "LX/98g",
            "<TT;TF;>;"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TF;)V"
        }
    .end annotation
.end method

.method public final b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)TF;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1446231
    iget-object v0, p0, LX/98h;->e:LX/98g;

    .line 1446232
    if-nez v0, :cond_0

    .line 1446233
    :goto_0
    return-object v1

    .line 1446234
    :cond_0
    iget-object v2, v0, LX/98g;->a:Ljava/lang/Object;

    move-object v2, v2

    .line 1446235
    invoke-static {p1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, LX/98h;->c:LX/0SI;

    if-nez v3, :cond_2

    const/4 v3, 0x0

    :goto_1
    iget-object v4, p0, LX/98h;->d:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static {v3, v4}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x1

    :goto_2
    move v2, v3

    .line 1446236
    if-eqz v2, :cond_1

    .line 1446237
    iget-object v2, v0, LX/98g;->b:Ljava/lang/Object;

    move-object v0, v2

    .line 1446238
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, LX/98h;->b:J

    .line 1446239
    :goto_3
    iput-object v1, p0, LX/98h;->e:LX/98g;

    move-object v1, v0

    .line 1446240
    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 1446241
    goto :goto_3

    :cond_2
    iget-object v3, p0, LX/98h;->c:LX/0SI;

    invoke-interface {v3}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v3

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    goto :goto_2
.end method

.method public abstract b()Z
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1446229
    iget-object v0, p0, LX/98h;->e:LX/98g;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()J
    .locals 4

    .prologue
    .line 1446228
    iget-wide v0, p0, LX/98h;->b:J

    iget-wide v2, p0, LX/98h;->a:J

    sub-long/2addr v0, v2

    return-wide v0
.end method
