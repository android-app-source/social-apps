.class public final LX/8wE;
.super Landroid/view/ViewGroup$MarginLayoutParams;
.source ""


# static fields
.field private static final c:LX/8wD;

.field private static final d:I

.field private static final e:I

.field private static final f:I

.field private static final g:I

.field private static final h:I

.field private static final i:I

.field private static final j:I

.field private static final k:I

.field private static final l:I

.field private static final m:I

.field private static final n:I

.field private static final o:I

.field private static final p:I


# instance fields
.field public a:LX/8wH;

.field public b:LX/8wH;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1422110
    new-instance v0, LX/8wD;

    const/high16 v1, -0x80000000

    const v2, -0x7fffffff

    invoke-direct {v0, v1, v2}, LX/8wD;-><init>(II)V

    .line 1422111
    sput-object v0, LX/8wE;->c:LX/8wD;

    invoke-virtual {v0}, LX/8wD;->a()I

    move-result v0

    sput v0, LX/8wE;->d:I

    .line 1422112
    const/16 v0, 0x2

    sput v0, LX/8wE;->e:I

    .line 1422113
    const/16 v0, 0x3

    sput v0, LX/8wE;->f:I

    .line 1422114
    const/16 v0, 0x4

    sput v0, LX/8wE;->g:I

    .line 1422115
    const/16 v0, 0x5

    sput v0, LX/8wE;->h:I

    .line 1422116
    const/16 v0, 0x6

    sput v0, LX/8wE;->i:I

    .line 1422117
    const/16 v0, 0xa

    sput v0, LX/8wE;->j:I

    .line 1422118
    const/16 v0, 0xb

    sput v0, LX/8wE;->k:I

    .line 1422119
    const/16 v0, 0xc

    sput v0, LX/8wE;->l:I

    .line 1422120
    const/16 v0, 0x7

    sput v0, LX/8wE;->m:I

    .line 1422121
    const/16 v0, 0x8

    sput v0, LX/8wE;->n:I

    .line 1422122
    const/16 v0, 0x9

    sput v0, LX/8wE;->o:I

    .line 1422123
    const/16 v0, 0xd

    sput v0, LX/8wE;->p:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1422108
    sget-object v0, LX/8wH;->a:LX/8wH;

    sget-object v1, LX/8wH;->a:LX/8wH;

    invoke-direct {p0, v0, v1}, LX/8wE;-><init>(LX/8wH;LX/8wH;)V

    .line 1422109
    return-void
.end method

.method private constructor <init>(IIIIIILX/8wH;LX/8wH;)V
    .locals 1

    .prologue
    .line 1422101
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 1422102
    sget-object v0, LX/8wH;->a:LX/8wH;

    iput-object v0, p0, LX/8wE;->a:LX/8wH;

    .line 1422103
    sget-object v0, LX/8wH;->a:LX/8wH;

    iput-object v0, p0, LX/8wE;->b:LX/8wH;

    .line 1422104
    invoke-virtual {p0, p3, p4, p5, p6}, LX/8wE;->setMargins(IIII)V

    .line 1422105
    iput-object p7, p0, LX/8wE;->a:LX/8wH;

    .line 1422106
    iput-object p8, p0, LX/8wE;->b:LX/8wH;

    .line 1422107
    return-void
.end method

.method private constructor <init>(LX/8wH;LX/8wH;)V
    .locals 9

    .prologue
    const/4 v1, -0x2

    const/high16 v3, -0x80000000

    .line 1422099
    move-object v0, p0

    move v2, v1

    move v4, v3

    move v5, v3

    move v6, v3

    move-object v7, p1

    move-object v8, p2

    invoke-direct/range {v0 .. v8}, LX/8wE;-><init>(IIIIIILX/8wH;LX/8wH;)V

    .line 1422100
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1422093
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1422094
    sget-object v0, LX/8wH;->a:LX/8wH;

    iput-object v0, p0, LX/8wE;->a:LX/8wH;

    .line 1422095
    sget-object v0, LX/8wH;->a:LX/8wH;

    iput-object v0, p0, LX/8wE;->b:LX/8wH;

    .line 1422096
    invoke-direct {p0, p1, p2}, LX/8wE;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1422097
    invoke-direct {p0, p1, p2}, LX/8wE;->b(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1422098
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 1422089
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1422090
    sget-object v0, LX/8wH;->a:LX/8wH;

    iput-object v0, p0, LX/8wE;->a:LX/8wH;

    .line 1422091
    sget-object v0, LX/8wH;->a:LX/8wH;

    iput-object v0, p0, LX/8wE;->b:LX/8wH;

    .line 1422092
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 1422051
    sget-object v0, LX/03r;->GridLayout_Layout:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 1422052
    :try_start_0
    sget v0, LX/8wE;->e:I

    const/high16 v2, -0x80000000

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    .line 1422053
    sget v2, LX/8wE;->f:I

    invoke-virtual {v1, v2, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, LX/8wE;->leftMargin:I

    .line 1422054
    sget v2, LX/8wE;->g:I

    invoke-virtual {v1, v2, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, LX/8wE;->topMargin:I

    .line 1422055
    sget v2, LX/8wE;->h:I

    invoke-virtual {v1, v2, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, LX/8wE;->rightMargin:I

    .line 1422056
    sget v2, LX/8wE;->i:I

    invoke-virtual {v1, v2, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, LX/8wE;->bottomMargin:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1422057
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 1422058
    return-void

    .line 1422059
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method private b(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    .prologue
    .line 1422076
    sget-object v0, LX/03r;->GridLayout_Layout:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 1422077
    :try_start_0
    sget v0, LX/8wE;->p:I

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 1422078
    sget v2, LX/8wE;->j:I

    const/high16 v3, -0x80000000

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    .line 1422079
    sget v3, LX/8wE;->k:I

    sget v4, LX/8wE;->d:I

    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    .line 1422080
    sget v4, LX/8wE;->l:I

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v4

    .line 1422081
    const/4 v5, 0x1

    invoke-static {v0, v5}, Landroid/support/v7/widget/GridLayout;->a(IZ)LX/8vz;

    move-result-object v5

    invoke-static {v2, v3, v5, v4}, Landroid/support/v7/widget/GridLayout;->a(IILX/8vz;F)LX/8wH;

    move-result-object v2

    iput-object v2, p0, LX/8wE;->b:LX/8wH;

    .line 1422082
    sget v2, LX/8wE;->m:I

    const/high16 v3, -0x80000000

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    .line 1422083
    sget v3, LX/8wE;->n:I

    sget v4, LX/8wE;->d:I

    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    .line 1422084
    sget v4, LX/8wE;->o:I

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v4

    .line 1422085
    const/4 v5, 0x0

    invoke-static {v0, v5}, Landroid/support/v7/widget/GridLayout;->a(IZ)LX/8vz;

    move-result-object v0

    invoke-static {v2, v3, v0, v4}, Landroid/support/v7/widget/GridLayout;->a(IILX/8vz;F)LX/8wH;

    move-result-object v0

    iput-object v0, p0, LX/8wE;->a:LX/8wH;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1422086
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 1422087
    return-void

    .line 1422088
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method


# virtual methods
.method public final a(LX/8wD;)V
    .locals 1

    .prologue
    .line 1422074
    iget-object v0, p0, LX/8wE;->a:LX/8wH;

    invoke-virtual {v0, p1}, LX/8wH;->a(LX/8wD;)LX/8wH;

    move-result-object v0

    iput-object v0, p0, LX/8wE;->a:LX/8wH;

    .line 1422075
    return-void
.end method

.method public final b(LX/8wD;)V
    .locals 1

    .prologue
    .line 1422072
    iget-object v0, p0, LX/8wE;->b:LX/8wH;

    invoke-virtual {v0, p1}, LX/8wH;->a(LX/8wD;)LX/8wH;

    move-result-object v0

    iput-object v0, p0, LX/8wE;->b:LX/8wH;

    .line 1422073
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1422066
    if-ne p0, p1, :cond_1

    .line 1422067
    :cond_0
    :goto_0
    return v0

    .line 1422068
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 1422069
    :cond_3
    check-cast p1, LX/8wE;

    .line 1422070
    iget-object v2, p0, LX/8wE;->b:LX/8wH;

    iget-object v3, p1, LX/8wE;->b:LX/8wH;

    invoke-virtual {v2, v3}, LX/8wH;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    .line 1422071
    :cond_4
    iget-object v2, p0, LX/8wE;->a:LX/8wH;

    iget-object v3, p1, LX/8wE;->a:LX/8wH;

    invoke-virtual {v2, v3}, LX/8wH;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1422063
    iget-object v0, p0, LX/8wE;->a:LX/8wH;

    invoke-virtual {v0}, LX/8wH;->hashCode()I

    move-result v0

    .line 1422064
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, LX/8wE;->b:LX/8wH;

    invoke-virtual {v1}, LX/8wH;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1422065
    return v0
.end method

.method public final setBaseAttributes(Landroid/content/res/TypedArray;II)V
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 1422060
    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result v0

    iput v0, p0, LX/8wE;->width:I

    .line 1422061
    invoke-virtual {p1, p3, v1}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result v0

    iput v0, p0, LX/8wE;->height:I

    .line 1422062
    return-void
.end method
