.class public LX/8yS;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/8yR;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/8yS;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8yT;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1425551
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/8yS;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/8yT;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1425647
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1425648
    iput-object p1, p0, LX/8yS;->b:LX/0Ot;

    .line 1425649
    return-void
.end method

.method public static a(LX/0QB;)LX/8yS;
    .locals 4

    .prologue
    .line 1425634
    sget-object v0, LX/8yS;->c:LX/8yS;

    if-nez v0, :cond_1

    .line 1425635
    const-class v1, LX/8yS;

    monitor-enter v1

    .line 1425636
    :try_start_0
    sget-object v0, LX/8yS;->c:LX/8yS;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1425637
    if-eqz v2, :cond_0

    .line 1425638
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1425639
    new-instance v3, LX/8yS;

    const/16 p0, 0x1935

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/8yS;-><init>(LX/0Ot;)V

    .line 1425640
    move-object v0, v3

    .line 1425641
    sput-object v0, LX/8yS;->c:LX/8yS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1425642
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1425643
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1425644
    :cond_1
    sget-object v0, LX/8yS;->c:LX/8yS;

    return-object v0

    .line 1425645
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1425646
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 16

    .prologue
    .line 1425631
    check-cast p2, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;

    .line 1425632
    move-object/from16 v0, p0

    iget-object v1, v0, LX/8yS;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8yT;

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    move-object/from16 v0, p2

    iget v4, v0, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->b:I

    move-object/from16 v0, p2

    iget v5, v0, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->c:I

    move-object/from16 v0, p2

    iget v6, v0, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->d:I

    move-object/from16 v0, p2

    iget v7, v0, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->e:I

    move-object/from16 v0, p2

    iget v8, v0, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->f:I

    move-object/from16 v0, p2

    iget-boolean v9, v0, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->g:Z

    move-object/from16 v0, p2

    iget-object v10, v0, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->h:Ljava/util/List;

    move-object/from16 v0, p2

    iget v11, v0, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->i:I

    move-object/from16 v0, p2

    iget v12, v0, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->j:I

    move-object/from16 v0, p2

    iget v13, v0, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->k:I

    move-object/from16 v0, p2

    iget v14, v0, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->l:I

    move-object/from16 v0, p2

    iget v15, v0, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->m:I

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v15}, LX/8yT;->a(LX/1De;Lcom/facebook/common/callercontext/CallerContext;IIIIIZLjava/util/List;IIIII)LX/1Dg;

    move-result-object v1

    .line 1425633
    return-object v1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1425629
    invoke-static {}, LX/1dS;->b()V

    .line 1425630
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;LX/1X1;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1X1",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 1425552
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v1

    .line 1425553
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v2

    .line 1425554
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v3

    .line 1425555
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v4

    .line 1425556
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v5

    .line 1425557
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v6

    .line 1425558
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v7

    .line 1425559
    iget-object v0, p0, LX/8yS;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-object v0, p1

    const/4 v9, 0x0

    .line 1425560
    sget-object v8, LX/03r;->FbFacepileComponent:[I

    invoke-virtual {v0, v8, v9}, LX/1De;->a([II)Landroid/content/res/TypedArray;

    move-result-object v10

    .line 1425561
    invoke-virtual {v10}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v11

    move v8, v9

    :goto_0
    if-ge v8, v11, :cond_7

    .line 1425562
    invoke-virtual {v10, v8}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result p0

    .line 1425563
    const/16 p1, 0x0

    if-ne p0, p1, :cond_1

    .line 1425564
    invoke-virtual {v10, p0, v9}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    .line 1425565
    iput-object p0, v1, LX/1np;->a:Ljava/lang/Object;

    .line 1425566
    :cond_0
    :goto_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 1425567
    :cond_1
    const/16 p1, 0x1

    if-ne p0, p1, :cond_2

    .line 1425568
    invoke-virtual {v10, p0, v9}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    .line 1425569
    iput-object p0, v2, LX/1np;->a:Ljava/lang/Object;

    .line 1425570
    goto :goto_1

    .line 1425571
    :cond_2
    const/16 p1, 0x2

    if-ne p0, p1, :cond_3

    .line 1425572
    invoke-virtual {v10, p0, v9}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    .line 1425573
    iput-object p0, v3, LX/1np;->a:Ljava/lang/Object;

    .line 1425574
    goto :goto_1

    .line 1425575
    :cond_3
    const/16 p1, 0x3

    if-ne p0, p1, :cond_4

    .line 1425576
    invoke-virtual {v10, p0, v9}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    .line 1425577
    iput-object p0, v4, LX/1np;->a:Ljava/lang/Object;

    .line 1425578
    goto :goto_1

    .line 1425579
    :cond_4
    const/16 p1, 0x4

    if-ne p0, p1, :cond_5

    .line 1425580
    invoke-virtual {v10, p0, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p0

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    .line 1425581
    iput-object p0, v5, LX/1np;->a:Ljava/lang/Object;

    .line 1425582
    goto :goto_1

    .line 1425583
    :cond_5
    const/16 p1, 0x5

    if-ne p0, p1, :cond_6

    .line 1425584
    invoke-virtual {v10, p0, v9}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    .line 1425585
    iput-object p0, v6, LX/1np;->a:Ljava/lang/Object;

    .line 1425586
    goto :goto_1

    .line 1425587
    :cond_6
    const/16 p1, 0x6

    if-ne p0, p1, :cond_0

    .line 1425588
    invoke-virtual {v10, p0, v9}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    .line 1425589
    iput-object p0, v7, LX/1np;->a:Ljava/lang/Object;

    .line 1425590
    goto :goto_1

    .line 1425591
    :cond_7
    invoke-virtual {v10}, Landroid/content/res/TypedArray;->recycle()V

    .line 1425592
    check-cast p2, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;

    .line 1425593
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1425594
    if-eqz v0, :cond_8

    .line 1425595
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1425596
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p2, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->b:I

    .line 1425597
    :cond_8
    invoke-static {v1}, LX/1cy;->a(LX/1np;)V

    .line 1425598
    iget-object v0, v2, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1425599
    if-eqz v0, :cond_9

    .line 1425600
    iget-object v0, v2, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1425601
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p2, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->d:I

    .line 1425602
    :cond_9
    invoke-static {v2}, LX/1cy;->a(LX/1np;)V

    .line 1425603
    iget-object v0, v3, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1425604
    if-eqz v0, :cond_a

    .line 1425605
    iget-object v0, v3, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1425606
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p2, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->e:I

    .line 1425607
    :cond_a
    invoke-static {v3}, LX/1cy;->a(LX/1np;)V

    .line 1425608
    iget-object v0, v4, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1425609
    if-eqz v0, :cond_b

    .line 1425610
    iget-object v0, v4, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1425611
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p2, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->f:I

    .line 1425612
    :cond_b
    invoke-static {v4}, LX/1cy;->a(LX/1np;)V

    .line 1425613
    iget-object v0, v5, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1425614
    if-eqz v0, :cond_c

    .line 1425615
    iget-object v0, v5, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1425616
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p2, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->g:Z

    .line 1425617
    :cond_c
    invoke-static {v5}, LX/1cy;->a(LX/1np;)V

    .line 1425618
    iget-object v0, v6, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1425619
    if-eqz v0, :cond_d

    .line 1425620
    iget-object v0, v6, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1425621
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p2, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->i:I

    .line 1425622
    :cond_d
    invoke-static {v6}, LX/1cy;->a(LX/1np;)V

    .line 1425623
    iget-object v0, v7, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1425624
    if-eqz v0, :cond_e

    .line 1425625
    iget-object v0, v7, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1425626
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p2, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->j:I

    .line 1425627
    :cond_e
    invoke-static {v7}, LX/1cy;->a(LX/1np;)V

    .line 1425628
    return-void
.end method
