.class public final LX/A63;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:I

.field public c:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$SquareHeaderImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Z

.field public e:Z

.field public f:I

.field public g:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelSubtitleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelTitleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1623347
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v8, 0x0

    .line 1623348
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1623349
    iget-object v1, p0, LX/A63;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1623350
    iget-object v3, p0, LX/A63;->c:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$SquareHeaderImageModel;

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1623351
    iget-object v5, p0, LX/A63;->g:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelSubtitleModel;

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1623352
    iget-object v6, p0, LX/A63;->h:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelTitleModel;

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1623353
    const/16 v7, 0x8

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 1623354
    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 1623355
    iget v1, p0, LX/A63;->b:I

    invoke-virtual {v0, v4, v1, v8}, LX/186;->a(III)V

    .line 1623356
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1623357
    const/4 v1, 0x3

    iget-boolean v3, p0, LX/A63;->d:Z

    invoke-virtual {v0, v1, v3}, LX/186;->a(IZ)V

    .line 1623358
    const/4 v1, 0x4

    iget-boolean v3, p0, LX/A63;->e:Z

    invoke-virtual {v0, v1, v3}, LX/186;->a(IZ)V

    .line 1623359
    const/4 v1, 0x5

    iget v3, p0, LX/A63;->f:I

    invoke-virtual {v0, v1, v3, v8}, LX/186;->a(III)V

    .line 1623360
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1623361
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1623362
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1623363
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1623364
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1623365
    invoke-virtual {v1, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1623366
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1623367
    new-instance v1, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;

    invoke-direct {v1, v0}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;-><init>(LX/15i;)V

    .line 1623368
    return-object v1
.end method
