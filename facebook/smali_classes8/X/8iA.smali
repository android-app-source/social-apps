.class public LX/8iA;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:I


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .prologue
    .line 1391260
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1391261
    iput p1, p0, LX/8iA;->a:I

    .line 1391262
    iput p2, p0, LX/8iA;->b:I

    .line 1391263
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1391264
    instance-of v1, p1, LX/8iA;

    if-nez v1, :cond_1

    .line 1391265
    :cond_0
    :goto_0
    return v0

    .line 1391266
    :cond_1
    check-cast p1, LX/8iA;

    .line 1391267
    iget v1, p0, LX/8iA;->a:I

    iget v2, p1, LX/8iA;->a:I

    if-ne v1, v2, :cond_0

    iget v1, p0, LX/8iA;->b:I

    iget v2, p1, LX/8iA;->b:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1391268
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, LX/8iA;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, LX/8iA;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
