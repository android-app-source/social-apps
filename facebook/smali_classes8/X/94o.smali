.class public final LX/94o;
.super LX/4An;
.source ""


# instance fields
.field public final synthetic a:LX/94q;


# direct methods
.method public constructor <init>(LX/94q;)V
    .locals 0

    .prologue
    .line 1435648
    iput-object p1, p0, LX/94o;->a:LX/94q;

    invoke-direct {p0}, LX/4An;-><init>()V

    return-void
.end method


# virtual methods
.method public final onProgress(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 5

    .prologue
    .line 1435649
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/upload/ContactsUploadState;

    .line 1435650
    iget-object v1, p0, LX/94o;->a:LX/94q;

    iget-object v1, v1, LX/94q;->e:LX/0Zb;

    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "contacts_upload_running"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/94o;->a:LX/94q;

    invoke-static {v3}, LX/94q;->a(LX/94q;)Ljava/lang/String;

    move-result-object v3

    .line 1435651
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1435652
    move-object v2, v2

    .line 1435653
    const-string v3, "num_processed"

    .line 1435654
    iget v4, v0, Lcom/facebook/contacts/upload/ContactsUploadState;->b:I

    move v4, v4

    .line 1435655
    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "num_matched"

    .line 1435656
    iget v4, v0, Lcom/facebook/contacts/upload/ContactsUploadState;->c:I

    move v4, v4

    .line 1435657
    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "total"

    .line 1435658
    iget v4, v0, Lcom/facebook/contacts/upload/ContactsUploadState;->d:I

    move v4, v4

    .line 1435659
    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1435660
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Contacts upload state ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1435661
    iget-object v1, p0, LX/94o;->a:LX/94q;

    invoke-static {v1, v0}, LX/94q;->a$redex0(LX/94q;Lcom/facebook/contacts/upload/ContactsUploadState;)V

    .line 1435662
    return-void
.end method
