.class public final LX/8qx;
.super LX/8qw;
.source ""


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/8r1;


# direct methods
.method public constructor <init>(LX/8r1;Z)V
    .locals 0

    .prologue
    .line 1408058
    iput-object p1, p0, LX/8qx;->b:LX/8r1;

    iput-boolean p2, p0, LX/8qx;->a:Z

    invoke-direct {p0, p1}, LX/8qw;-><init>(LX/8r1;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1408059
    iget-object v0, p0, LX/8qx;->b:LX/8r1;

    iget-object v0, v0, LX/8r1;->g:LX/1zt;

    sget-object v1, LX/1zt;->c:LX/1zt;

    if-ne v0, v1, :cond_0

    sget-object v0, LX/1zt;->b:LX/1zt;

    .line 1408060
    :goto_0
    iget-object v1, p0, LX/8qx;->b:LX/8r1;

    iget-object v1, v1, LX/8r1;->f:LX/8qr;

    .line 1408061
    iget-object p0, v1, LX/8qr;->b:LX/8qq;

    move-object v1, p0

    .line 1408062
    invoke-interface {v1, p1, v0}, LX/8qq;->a(Landroid/view/View;LX/1zt;)V

    .line 1408063
    return-void

    .line 1408064
    :cond_0
    sget-object v0, LX/1zt;->a:LX/1zt;

    goto :goto_0
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .prologue
    .line 1408065
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 1408066
    iget-boolean v0, p0, LX/8qx;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8qx;->b:LX/8r1;

    iget-object v0, v0, LX/8r1;->g:LX/1zt;

    .line 1408067
    iget p0, v0, LX/1zt;->g:I

    move v0, p0

    .line 1408068
    :goto_0
    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 1408069
    return-void

    .line 1408070
    :cond_0
    iget-object v0, p0, LX/8qx;->b:LX/8r1;

    iget v0, v0, LX/8r1;->d:I

    goto :goto_0
.end method
