.class public final LX/932;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8ky;


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/composer/minutiae/stickerpicker/ComposerStickerPickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/minutiae/stickerpicker/ComposerStickerPickerFragment;)V
    .locals 0

    .prologue
    .line 1433195
    iput-object p1, p0, LX/932;->a:Lcom/facebook/composer/minutiae/stickerpicker/ComposerStickerPickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1433182
    return-void
.end method

.method public final a(Lcom/facebook/stickers/model/Sticker;)V
    .locals 5

    .prologue
    .line 1433183
    if-nez p1, :cond_0

    .line 1433184
    :goto_0
    return-void

    .line 1433185
    :cond_0
    invoke-static {p1}, LX/5Rb;->a(Lcom/facebook/stickers/model/Sticker;)Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v0

    .line 1433186
    iget-object v1, p0, LX/932;->a:Lcom/facebook/composer/minutiae/stickerpicker/ComposerStickerPickerFragment;

    iget-object v1, v1, Lcom/facebook/composer/minutiae/stickerpicker/ComposerStickerPickerFragment;->c:LX/91C;

    .line 1433187
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1433188
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getPackId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1433189
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v3, LX/91A;->STICKER_PICKER_SELECTED_STICKER:LX/91A;

    invoke-virtual {v3}, LX/91A;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "composer"

    .line 1433190
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1433191
    move-object v2, v2

    .line 1433192
    sget-object v3, LX/91B;->STICKER_ID:LX/91B;

    invoke-virtual {v3}, LX/91B;->getParamKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getStickerId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/91B;->PACK_ID:LX/91B;

    invoke-virtual {v3}, LX/91B;->getParamKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getPackId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 1433193
    invoke-static {v1, v2}, LX/91C;->a(LX/91C;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1433194
    iget-object v0, p0, LX/932;->a:Lcom/facebook/composer/minutiae/stickerpicker/ComposerStickerPickerFragment;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/stickerpicker/ComposerStickerPickerFragment;->d:LX/90U;

    invoke-interface {v0, p1}, LX/90U;->a(Lcom/facebook/stickers/model/Sticker;)V

    goto :goto_0
.end method
