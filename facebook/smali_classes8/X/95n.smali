.class public LX/95n;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2jy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TEdge:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/2jy",
        "<TTEdge;>;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/concurrent/atomic/AtomicInteger;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:J

.field private final e:LX/2js;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2js",
            "<TTEdge;>;"
        }
    .end annotation
.end field

.field private final f:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<TTEdge;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:I

.field public final h:LX/0Sh;

.field private final i:Ljava/util/concurrent/Executor;

.field private final j:Ljava/util/concurrent/Executor;

.field public final k:LX/2k1;

.field private final l:LX/2kT;

.field public final m:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public final n:LX/1BA;

.field public final o:LX/0SG;

.field public final p:LX/03V;

.field private final q:LX/15j;

.field public final r:LX/2kI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2kI",
            "<TTEdge;>;"
        }
    .end annotation
.end field

.field public final s:Ljava/util/concurrent/locks/ReentrantLock;

.field public final t:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field public u:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mDatabase"
    .end annotation
.end field

.field public final v:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public volatile w:Z

.field public volatile x:Z

.field public volatile y:LX/95j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/95j",
            "<TTEdge;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:LX/2kb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mStateChangeLock"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;JLX/2js;LX/0QK;ILX/0Sh;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;LX/2jz;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/1BA;LX/0SG;LX/03V;LX/0ox;)V
    .locals 6
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # J
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/2js;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/0QK;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .param p9    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J",
            "LX/2js",
            "<TTEdge;>;",
            "LX/0QK",
            "<TTEdge;",
            "Ljava/lang/String;",
            ">;I",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Ljava/util/concurrent/Executor;",
            "Ljava/util/concurrent/Executor;",
            "LX/2jz;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "Lcom/facebook/localstats/LocalStatsLogger;",
            "LX/0SG;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0ox;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1437683
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1437684
    new-instance v1, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v1, p0, LX/95n;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 1437685
    new-instance v1, LX/2kI;

    invoke-direct {v1}, LX/2kI;-><init>()V

    iput-object v1, p0, LX/95n;->r:LX/2kI;

    .line 1437686
    new-instance v1, LX/95m;

    invoke-direct {v1, p0}, LX/95m;-><init>(LX/95n;)V

    iput-object v1, p0, LX/95n;->s:Ljava/util/concurrent/locks/ReentrantLock;

    .line 1437687
    new-instance v1, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v1, p0, LX/95n;->t:Ljava/util/Queue;

    .line 1437688
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/95n;->u:Z

    .line 1437689
    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v1, p0, LX/95n;->v:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 1437690
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/95n;->x:Z

    .line 1437691
    iput-object p1, p0, LX/95n;->b:Ljava/lang/String;

    .line 1437692
    invoke-static {p1}, LX/2kN;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/95n;->c:Ljava/lang/String;

    .line 1437693
    iput-wide p2, p0, LX/95n;->d:J

    .line 1437694
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2js;

    iput-object v1, p0, LX/95n;->e:LX/2js;

    .line 1437695
    iput-object p5, p0, LX/95n;->f:LX/0QK;

    .line 1437696
    iput p6, p0, LX/95n;->g:I

    .line 1437697
    iput-object p7, p0, LX/95n;->h:LX/0Sh;

    .line 1437698
    iput-object p8, p0, LX/95n;->i:Ljava/util/concurrent/Executor;

    .line 1437699
    iput-object p9, p0, LX/95n;->j:Ljava/util/concurrent/Executor;

    .line 1437700
    invoke-virtual/range {p10 .. p10}, LX/2jz;->a()LX/2k1;

    move-result-object v1

    iput-object v1, p0, LX/95n;->k:LX/2k1;

    .line 1437701
    iget-object v1, p0, LX/95n;->k:LX/2k1;

    iget-object v2, p0, LX/95n;->c:Ljava/lang/String;

    new-instance v3, LX/95l;

    invoke-direct {v3, p0}, LX/95l;-><init>(LX/95n;)V

    invoke-interface {v1, v2, v3}, LX/2k1;->a(Ljava/lang/String;LX/2kR;)LX/2kT;

    move-result-object v1

    iput-object v1, p0, LX/95n;->l:LX/2kT;

    .line 1437702
    move-object/from16 v0, p11

    iput-object v0, p0, LX/95n;->m:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 1437703
    move-object/from16 v0, p12

    iput-object v0, p0, LX/95n;->n:LX/1BA;

    .line 1437704
    move-object/from16 v0, p13

    iput-object v0, p0, LX/95n;->o:LX/0SG;

    .line 1437705
    move-object/from16 v0, p14

    iput-object v0, p0, LX/95n;->p:LX/03V;

    .line 1437706
    sget-object v1, LX/95o;->a:LX/0Tn;

    iget-object v2, p0, LX/95n;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v1

    check-cast v1, LX/0Tn;

    move-object/from16 v0, p15

    invoke-virtual {v0, v1}, LX/0ox;->a(LX/0Tn;)LX/15j;

    move-result-object v1

    iput-object v1, p0, LX/95n;->q:LX/15j;

    .line 1437707
    return-void
.end method

.method private a(LX/5Mb;ZLjava/lang/String;Lcom/facebook/graphql/cursor/edgestore/PageInfo;J)LX/2nf;
    .locals 9
    .param p4    # Lcom/facebook/graphql/cursor/edgestore/PageInfo;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5Mb",
            "<TTEdge;>;Z",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/cursor/edgestore/PageInfo;",
            "J)",
            "LX/2nf;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mStateChangeLock"
    .end annotation

    .prologue
    .line 1437708
    iget-object v0, p1, LX/5Mb;->b:LX/0Px;

    move-object v0, v0

    .line 1437709
    invoke-static {v0}, LX/9JA;->b(LX/0Px;)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 1437710
    if-nez v2, :cond_0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1437711
    invoke-static {v0}, LX/9JA;->a(LX/0Px;)LX/0Px;

    move-result-object v0

    .line 1437712
    invoke-static {v0}, LX/9JA;->b(LX/0Px;)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 1437713
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1437714
    :cond_0
    new-instance v3, LX/95c;

    iget-object v0, p0, LX/95n;->e:LX/2js;

    iget-object v1, p0, LX/95n;->f:LX/0QK;

    invoke-direct {v3, p1, p3, v0, v1}, LX/95c;-><init>(LX/5Mb;Ljava/lang/String;LX/2js;LX/0QK;)V

    .line 1437715
    invoke-virtual {v3}, LX/95c;->b()V

    .line 1437716
    iget-object v8, p0, LX/95n;->k:LX/2k1;

    monitor-enter v8

    .line 1437717
    :try_start_0
    invoke-static {p0}, LX/95n;->k(LX/95n;)V

    .line 1437718
    iget-object v0, p0, LX/95n;->k:LX/2k1;

    iget-object v1, p0, LX/95n;->z:LX/2kb;

    move-wide v4, p5

    move-object v6, p4

    move v7, p2

    invoke-interface/range {v0 .. v7}, LX/2k1;->a(LX/2kb;Ljava/nio/ByteBuffer;LX/95b;JLcom/facebook/graphql/cursor/edgestore/PageInfo;Z)LX/2nf;

    move-result-object v0

    .line 1437719
    monitor-exit v8

    return-object v0

    .line 1437720
    :catchall_0
    move-exception v0

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(LX/2nf;ILX/2nj;LX/3DP;LX/3Cb;)V
    .locals 11
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mStateChangeLock"
    .end annotation

    .prologue
    .line 1437721
    invoke-static {p0, p1}, LX/95n;->a$redex0(LX/95n;LX/2nf;)LX/95j;

    move-result-object v0

    .line 1437722
    iget-object v1, p0, LX/95n;->y:LX/95j;

    .line 1437723
    invoke-interface {p1}, LX/2nf;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 1437724
    iget-object v3, p0, LX/95n;->p:LX/03V;

    iget-object v4, p0, LX/95n;->n:LX/1BA;

    invoke-static {v1, v0, v2, v3, v4}, LX/95n;->b(LX/95j;LX/95j;Landroid/os/Bundle;LX/03V;LX/1BA;)Z

    .line 1437725
    const-string v3, "DELETED_ROW_IDS"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v3

    const-string v4, "INSERTED_ROW_IDS"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v4

    const/4 v5, 0x0

    move-object v2, p3

    invoke-virtual/range {v0 .. v5}, LX/95j;->a(LX/95j;LX/2nj;[J[J[J)LX/0Px;

    move-result-object v4

    .line 1437726
    invoke-static {p0, v0}, LX/95n;->c(LX/95n;LX/95j;)LX/95j;

    .line 1437727
    iget-object v2, p0, LX/95n;->m:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x920001

    const/4 v5, 0x2

    invoke-interface {v2, v3, p2, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 1437728
    new-instance v2, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$4;

    move-object v3, p0

    move-object/from16 v5, p5

    move-object v6, v1

    move-object v7, v0

    move-object v8, p3

    move-object v9, p4

    move v10, p2

    invoke-direct/range {v2 .. v10}, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$4;-><init>(LX/95n;LX/0Px;LX/3Cb;LX/95j;LX/95j;LX/2nj;LX/3DP;I)V

    invoke-static {p0, v2}, LX/95n;->a$redex0(LX/95n;Ljava/lang/Runnable;)V

    .line 1437729
    return-void
.end method

.method public static a(LX/95n;Z)V
    .locals 0

    .prologue
    .line 1437730
    iput-boolean p1, p0, LX/95n;->w:Z

    .line 1437731
    return-void
.end method

.method public static a$redex0(LX/95n;LX/2nf;)LX/95j;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2nf;",
            ")",
            "LX/95j",
            "<TTEdge;>;"
        }
    .end annotation

    .prologue
    .line 1437732
    iget v0, p0, LX/95n;->g:I

    if-lez v0, :cond_0

    .line 1437733
    new-instance v0, LX/95k;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v0, p1, v1}, LX/95k;-><init>(LX/2nf;Ljava/util/ArrayList;)V

    .line 1437734
    :goto_0
    invoke-virtual {v0}, LX/95j;->f()V

    .line 1437735
    return-object v0

    .line 1437736
    :cond_0
    new-instance v0, LX/95j;

    invoke-direct {v0, p1}, LX/95j;-><init>(LX/2nf;)V

    goto :goto_0
.end method

.method public static a$redex0(LX/95n;I)V
    .locals 3
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mStateChangeLock"
    .end annotation

    .prologue
    .line 1437737
    iget-object v0, p0, LX/95n;->r:LX/2kI;

    iget-object v1, p0, LX/95n;->y:LX/95j;

    invoke-static {v1}, LX/95n;->b(LX/2kM;)LX/2kM;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2kI;->a(LX/2kM;)V

    .line 1437738
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/95n;->x:Z

    .line 1437739
    iget-object v0, p0, LX/95n;->m:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x920002

    const/16 v2, 0xbf

    invoke-interface {v0, v1, p1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 1437740
    return-void
.end method

.method public static a$redex0(LX/95n;Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 1437742
    iget-object v0, p0, LX/95n;->j:Ljava/util/concurrent/Executor;

    const v1, -0x323b1fbb

    invoke-static {v0, p1, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1437743
    return-void
.end method

.method public static b(LX/2kM;)LX/2kM;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TEdge:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2kM",
            "<TTEdge;>;)",
            "LX/2kM",
            "<TTEdge;>;"
        }
    .end annotation

    .prologue
    .line 1437741
    if-nez p0, :cond_0

    sget-object p0, LX/2kL;->a:LX/2kM;

    :cond_0
    return-object p0
.end method

.method public static b(LX/95n;LX/2nj;LX/3DP;I)V
    .locals 7
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    .line 1437756
    iget-object v0, p0, LX/95n;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 1437757
    :try_start_0
    iget-object v0, p0, LX/95n;->y:LX/95j;

    instance-of v0, v0, LX/95k;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 1437758
    iget-object v0, p0, LX/95n;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 1437759
    :goto_0
    return-void

    .line 1437760
    :cond_0
    :try_start_1
    iget-object v3, p0, LX/95n;->y:LX/95j;

    check-cast v3, LX/95k;

    .line 1437761
    invoke-virtual {v3, p3}, LX/95k;->b(I)LX/95k;

    move-result-object v4

    .line 1437762
    if-eqz v4, :cond_1

    .line 1437763
    invoke-static {p0, v4}, LX/95n;->c(LX/95n;LX/95j;)LX/95j;

    .line 1437764
    invoke-virtual {v3}, LX/95j;->c()I

    move-result v0

    invoke-virtual {v4}, LX/95j;->c()I

    move-result v1

    invoke-virtual {v3}, LX/95j;->c()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, LX/3CY;->a(II)LX/3CY;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 1437765
    new-instance v0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$5;

    move-object v1, p0

    move-object v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$5;-><init>(LX/95n;LX/0Px;LX/95k;LX/95k;LX/2nj;LX/3DP;)V

    invoke-static {p0, v0}, LX/95n;->a$redex0(LX/95n;Ljava/lang/Runnable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1437766
    :goto_1
    iget-object v0, p0, LX/95n;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    .line 1437767
    :cond_1
    :try_start_2
    new-instance v0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$6;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$6;-><init>(LX/95n;LX/2nj;LX/3DP;)V

    invoke-static {p0, v0}, LX/95n;->a$redex0(LX/95n;Ljava/lang/Runnable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 1437768
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/95n;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method private b(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 1437769
    iget-object v0, p0, LX/95n;->i:Ljava/util/concurrent/Executor;

    const v1, -0x7816f29

    invoke-static {v0, p1, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1437770
    return-void
.end method

.method public static b(LX/95j;LX/95j;Landroid/os/Bundle;LX/03V;LX/1BA;)Z
    .locals 7
    .param p0    # LX/95j;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TEdge:",
            "Ljava/lang/Object;",
            ">(",
            "LX/95j",
            "<TTEdge;>;",
            "LX/95j",
            "<TTEdge;>;",
            "Landroid/os/Bundle;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/localstats/LocalStatsLogger;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x2

    .line 1437747
    invoke-virtual {p1}, LX/95j;->g()I

    move-result v2

    .line 1437748
    if-eqz p0, :cond_1

    .line 1437749
    const-string v0, "SESSION_VERSION"

    const/4 v3, -0x1

    invoke-virtual {p2, v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 1437750
    invoke-virtual {p0}, LX/95j;->g()I

    move-result v4

    .line 1437751
    add-int/lit8 v0, v4, 0x1

    if-eq v0, v2, :cond_1

    .line 1437752
    const/16 v0, 0xaf

    .line 1437753
    const-string v5, "ConnectionController"

    const-string v6, "Session version error during update, new=%d, change=%d, old=%d"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v6, v2, v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v5, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1437754
    :goto_0
    const v2, 0x920009

    invoke-virtual {p4, v2, v0}, LX/1BA;->a(IS)V

    .line 1437755
    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public static c(LX/95n;LX/95j;)LX/95j;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/95j",
            "<TTEdge;>;)",
            "LX/95j",
            "<TTEdge;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1437671
    iget-object v0, p0, LX/95n;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 1437672
    :try_start_0
    invoke-virtual {p1}, LX/95j;->j()V

    .line 1437673
    iget-object v0, p0, LX/95n;->y:LX/95j;

    .line 1437674
    iput-object p1, p0, LX/95n;->y:LX/95j;

    .line 1437675
    if-eqz v0, :cond_0

    .line 1437676
    invoke-virtual {v0}, LX/95j;->j()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1437677
    :cond_0
    iget-object v1, p0, LX/95n;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-object v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/95n;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public static d(LX/95j;)V
    .locals 0
    .param p0    # LX/95j;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TEdge:",
            "Ljava/lang/Object;",
            ">(",
            "LX/95j",
            "<TTEdge;>;)V"
        }
    .end annotation

    .prologue
    .line 1437744
    if-eqz p0, :cond_0

    .line 1437745
    invoke-virtual {p0}, LX/95j;->close()V

    .line 1437746
    :cond_0
    return-void
.end method

.method public static f$redex0(LX/95n;)V
    .locals 1
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mStateChangeLock"
    .end annotation

    .prologue
    .line 1437678
    iget-object v0, p0, LX/95n;->z:LX/2kb;

    if-eqz v0, :cond_0

    .line 1437679
    iget-object v0, p0, LX/95n;->z:LX/2kb;

    invoke-interface {v0}, LX/2kb;->close()V

    .line 1437680
    const/4 v0, 0x0

    iput-object v0, p0, LX/95n;->z:LX/2kb;

    .line 1437681
    :cond_0
    return-void
.end method

.method public static g(LX/95n;)Z
    .locals 1

    .prologue
    .line 1437682
    iget-boolean v0, p0, LX/95n;->w:Z

    return v0
.end method

.method public static k(LX/95n;)V
    .locals 1
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mDatabase"
    .end annotation

    .prologue
    .line 1437629
    :goto_0
    iget-object v0, p0, LX/95n;->t:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1437630
    iget-object v0, p0, LX/95n;->t:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 1437631
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 1437632
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1437628
    iget-object v0, p0, LX/95n;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/0Rl;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rl",
            "<TTEdge;>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1437624
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1437625
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1437626
    new-instance v0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$2;-><init>(LX/95n;LX/0Rl;Ljava/lang/String;)V

    invoke-direct {p0, v0}, LX/95n;->b(Ljava/lang/Runnable;)V

    .line 1437627
    return-void
.end method

.method public final a(LX/2kJ;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2kJ",
            "<TTEdge;>;)V"
        }
    .end annotation

    .prologue
    .line 1437622
    iget-object v0, p0, LX/95n;->r:LX/2kI;

    invoke-virtual {v0, p1}, LX/2kI;->a(LX/2kJ;)V

    .line 1437623
    return-void
.end method

.method public final a(LX/2nj;LX/3DP;LX/5Mb;JLX/3Cb;)V
    .locals 10
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2nj;",
            "LX/3DP;",
            "LX/5Mb",
            "<TTEdge;>;J",
            "LX/3Cb;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const v2, 0x920001

    .line 1437566
    iget-object v0, p0, LX/95n;->h:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 1437567
    iget-boolean v0, p0, LX/95n;->x:Z

    if-nez v0, :cond_0

    .line 1437568
    :goto_0
    return-void

    .line 1437569
    :cond_0
    iget-object v0, p0, LX/95n;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v8

    .line 1437570
    iget-object v0, p0, LX/95n;->m:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v2, v8}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 1437571
    iget-object v0, p0, LX/95n;->m:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v1, "DatabaseConnectionStore"

    invoke-interface {v0, v2, v8, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 1437572
    iget-object v0, p0, LX/95n;->m:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v1, p0, LX/95n;->c:Ljava/lang/String;

    invoke-interface {v0, v2, v8, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 1437573
    invoke-virtual {p1}, LX/2nj;->b()LX/2nk;

    move-result-object v0

    .line 1437574
    sget-object v1, LX/2nk;->INITIAL:LX/2nk;

    if-eq v0, v1, :cond_1

    invoke-static {p0}, LX/95n;->g(LX/95n;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v3, 0x1

    .line 1437575
    :cond_2
    invoke-static {p1, p4, p5}, LX/95e;->a(LX/2nj;J)Ljava/lang/String;

    move-result-object v4

    .line 1437576
    invoke-static {p1, p2, p3, v4}, LX/95e;->a(LX/2nj;LX/3DP;LX/5Mb;Ljava/lang/String;)Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    move-result-object v5

    .line 1437577
    iget-object v0, p0, LX/95n;->m:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/16 v1, 0x17

    invoke-interface {v0, v2, v8, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 1437578
    iget-wide v0, p0, LX/95n;->d:J

    invoke-static {v0, v1}, LX/5Ma;->a(J)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-wide v0, p0, LX/95n;->d:J

    const-wide/16 v6, 0x3e8

    mul-long/2addr v6, v0

    .line 1437579
    :goto_1
    iget-object v0, p0, LX/95n;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    move-object v1, p0

    move-object v2, p3

    .line 1437580
    :try_start_0
    invoke-direct/range {v1 .. v7}, LX/95n;->a(LX/5Mb;ZLjava/lang/String;Lcom/facebook/graphql/cursor/edgestore/PageInfo;J)LX/2nf;

    move-result-object v1

    .line 1437581
    if-nez v1, :cond_4

    .line 1437582
    iget-object v0, p0, LX/95n;->m:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x920001

    const/4 v2, 0x3

    invoke-interface {v0, v1, v8, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 1437583
    new-instance v0, LX/5Md;

    invoke-direct {v0}, LX/5Md;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1437584
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/95n;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    .line 1437585
    :cond_3
    const-wide/32 v6, 0x19bfcc00

    goto :goto_1

    .line 1437586
    :cond_4
    :try_start_1
    iget-object v0, p0, LX/95n;->m:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x920001

    const/16 v3, 0x1e

    invoke-interface {v0, v2, v8, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 1437587
    invoke-static {p0}, LX/95n;->g(LX/95n;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1437588
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/95n;->a(LX/95n;Z)V

    .line 1437589
    :cond_5
    iget-object v0, p0, LX/95n;->m:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x920001

    const/16 v3, 0x1a

    invoke-interface {v0, v2, v8, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 1437590
    iget-object v0, p0, LX/95n;->m:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x920001

    const/16 v3, 0xbd

    invoke-interface {v0, v2, v8, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    move-object v0, p0

    move v2, v8

    move-object v3, p1

    move-object v4, p2

    move-object/from16 v5, p6

    .line 1437591
    invoke-direct/range {v0 .. v5}, LX/95n;->a(LX/2nf;ILX/2nj;LX/3DP;LX/3Cb;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1437592
    iget-object v0, p0, LX/95n;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto/16 :goto_0
.end method

.method public final a(LX/5Mb;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5Mb",
            "<TTEdge;>;)V"
        }
    .end annotation

    .prologue
    .line 1437565
    return-void
.end method

.method public final a(Ljava/lang/String;LX/3Cf;)V
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/3Cf",
            "<TTEdge;>;)V"
        }
    .end annotation

    .prologue
    .line 1437593
    iget-object v0, p0, LX/95n;->h:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 1437594
    const/4 v1, 0x0

    .line 1437595
    :try_start_0
    iget-object v0, p0, LX/95n;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 1437596
    :try_start_1
    iget-object v0, p0, LX/95n;->z:LX/2kb;

    if-nez v0, :cond_1

    .line 1437597
    iget-object v0, p0, LX/95n;->p:LX/03V;

    const-string v2, "ConnectionController"

    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "visitEdgesWithTagHint called on closed session"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1437598
    :try_start_2
    iget-object v0, p0, LX/95n;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1437599
    :cond_0
    :goto_0
    return-void

    .line 1437600
    :cond_1
    :try_start_3
    iget-object v2, p0, LX/95n;->k:LX/2k1;

    monitor-enter v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1437601
    :try_start_4
    invoke-static {p0}, LX/95n;->k(LX/95n;)V

    .line 1437602
    iget-object v0, p0, LX/95n;->k:LX/2k1;

    iget-object v3, p0, LX/95n;->z:LX/2kb;

    invoke-interface {v0, v3, p1}, LX/2k1;->a(LX/2kb;Ljava/lang/String;)LX/2nf;

    move-result-object v1

    .line 1437603
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1437604
    :try_start_5
    iget-object v0, p0, LX/95n;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 1437605
    invoke-interface {v1}, LX/2nf;->moveToFirst()Z
    :try_end_5
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    move-result v0

    if-eqz v0, :cond_4

    .line 1437606
    :cond_2
    :try_start_6
    invoke-interface {v1}, LX/2nf;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    .line 1437607
    if-eqz v0, :cond_3

    invoke-interface {p2, v0}, LX/3Cf;->a(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1437608
    invoke-interface {p2, v0}, LX/3Cf;->b(Ljava/lang/Object;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 1437609
    :cond_3
    :goto_1
    :try_start_7
    invoke-interface {v1}, LX/2nf;->moveToNext()Z
    :try_end_7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    move-result v0

    if-nez v0, :cond_2

    .line 1437610
    :cond_4
    if-eqz v1, :cond_0

    .line 1437611
    invoke-interface {v1}, LX/2nf;->close()V

    goto :goto_0

    .line 1437612
    :catchall_0
    move-exception v0

    :try_start_8
    monitor-exit v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :try_start_9
    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 1437613
    :catchall_1
    move-exception v0

    :try_start_a
    iget-object v2, p0, LX/95n;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
    :try_end_a
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 1437614
    :catch_0
    move-exception v0

    .line 1437615
    :try_start_b
    const-string v2, "ConnectionController"

    const-string v3, "Error in visitEdgesWithTagHint"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 1437616
    if-eqz v1, :cond_0

    .line 1437617
    invoke-interface {v1}, LX/2nf;->close()V

    goto :goto_0

    .line 1437618
    :catch_1
    move-exception v0

    .line 1437619
    :try_start_c
    const-string v2, "ConnectionController"

    const-string v3, "Error visiting edges with tag hint"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_c
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_c .. :try_end_c} :catch_0
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    goto :goto_1

    .line 1437620
    :catchall_2
    move-exception v0

    if-eqz v1, :cond_5

    .line 1437621
    invoke-interface {v1}, LX/2nf;->close()V

    :cond_5
    throw v0
.end method

.method public final a(LX/2nj;LX/3DP;I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1437633
    iget-object v2, p0, LX/95n;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 1437634
    :try_start_0
    iget-object v2, p0, LX/95n;->y:LX/95j;

    if-eqz v2, :cond_0

    .line 1437635
    iget-object v2, p1, LX/2nj;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1437636
    invoke-static {v2}, LX/3DQ;->a(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_2

    .line 1437637
    :cond_0
    iget-object v0, p0, LX/95n;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    move v0, v1

    .line 1437638
    :cond_1
    :goto_0
    return v0

    .line 1437639
    :cond_2
    iget-object v2, p0, LX/95n;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 1437640
    iget-object v2, p0, LX/95n;->v:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2, v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1437641
    new-instance v1, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$1;-><init>(LX/95n;LX/2nj;LX/3DP;I)V

    invoke-direct {p0, v1}, LX/95n;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 1437642
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/95n;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTEdge;TTEdge;)Z"
        }
    .end annotation

    .prologue
    .line 1437643
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "replaceEdge not implemented yet."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b()V
    .locals 8

    .prologue
    .line 1437644
    iget-object v0, p0, LX/95n;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 1437645
    :try_start_0
    iget-object v0, p0, LX/95n;->z:LX/2kb;

    if-nez v0, :cond_0

    .line 1437646
    iget-object v0, p0, LX/95n;->k:LX/2k1;

    iget-object v1, p0, LX/95n;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/2k1;->a(Ljava/lang/String;)LX/2kb;

    move-result-object v0

    iput-object v0, p0, LX/95n;->z:LX/2kb;

    .line 1437647
    :cond_0
    iget-boolean v0, p0, LX/95n;->x:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 1437648
    iget-object v0, p0, LX/95n;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 1437649
    :goto_0
    return-void

    .line 1437650
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/95n;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v7

    .line 1437651
    iget-object v0, p0, LX/95n;->m:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x920002

    invoke-interface {v0, v1, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 1437652
    iget-object v0, p0, LX/95n;->m:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x920002

    const-string v2, "DatabaseConnectionStore"

    invoke-interface {v0, v1, v7, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 1437653
    iget-object v0, p0, LX/95n;->y:LX/95j;

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1437654
    new-instance v0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$InitializeRunnable;

    iget-object v2, p0, LX/95n;->r:LX/2kI;

    iget-object v3, p0, LX/95n;->m:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v4, p0, LX/95n;->q:LX/15j;

    iget-object v5, p0, LX/95n;->p:LX/03V;

    iget-object v6, p0, LX/95n;->k:LX/2k1;

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$InitializeRunnable;-><init>(LX/95n;LX/2kI;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/15j;LX/03V;LX/2k1;I)V

    invoke-direct {p0, v0}, LX/95n;->b(Ljava/lang/Runnable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1437655
    iget-object v0, p0, LX/95n;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    .line 1437656
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 1437657
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/95n;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final b(LX/2kJ;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2kJ",
            "<TTEdge;>;)V"
        }
    .end annotation

    .prologue
    .line 1437658
    iget-object v0, p0, LX/95n;->r:LX/2kI;

    invoke-virtual {v0, p1}, LX/2kI;->b(LX/2kJ;)V

    .line 1437659
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1437660
    iget-object v0, p0, LX/95n;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 1437661
    :try_start_0
    iget-object v0, p0, LX/95n;->l:LX/2kT;

    invoke-interface {v0}, LX/2kT;->close()V

    .line 1437662
    iget-object v0, p0, LX/95n;->y:LX/95j;

    invoke-static {v0}, LX/95n;->d(LX/95j;)V

    .line 1437663
    const/4 v0, 0x0

    iput-object v0, p0, LX/95n;->y:LX/95j;

    .line 1437664
    invoke-static {p0}, LX/95n;->f$redex0(LX/95n;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1437665
    iget-object v0, p0, LX/95n;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 1437666
    return-void

    .line 1437667
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/95n;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1437668
    new-instance v0, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$3;

    invoke-direct {v0, p0}, Lcom/facebook/controller/connectioncontroller/store/databasestore/DatabaseConnectionStore$3;-><init>(LX/95n;)V

    invoke-direct {p0, v0}, LX/95n;->b(Ljava/lang/Runnable;)V

    .line 1437669
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1437670
    const/4 v0, 0x1

    return v0
.end method
