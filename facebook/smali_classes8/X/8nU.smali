.class public final LX/8nU;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8JX;

.field public final synthetic b:LX/8nW;


# direct methods
.method public constructor <init>(LX/8nW;LX/8JX;)V
    .locals 0

    .prologue
    .line 1401098
    iput-object p1, p0, LX/8nU;->b:LX/8nW;

    iput-object p2, p0, LX/8nU;->a:LX/8JX;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1401099
    const-string v0, "fetch_group_members_to_mentions"

    const-string v1, "Fail to load group members for suggestions"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1401100
    iget-object v0, p0, LX/8nU;->a:LX/8JX;

    const-string v1, ""

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0, v1, v2}, LX/8JX;->a(Ljava/lang/CharSequence;Ljava/util/List;)V

    .line 1401101
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 14

    .prologue
    .line 1401102
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v13, 0x0

    const/4 v9, 0x0

    .line 1401103
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 1401104
    if-eqz p1, :cond_0

    .line 1401105
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1401106
    if-nez v0, :cond_1

    .line 1401107
    :cond_0
    iget-object v0, p0, LX/8nU;->a:LX/8JX;

    const-string v1, ""

    invoke-interface {v0, v1, v10}, LX/8JX;->a(Ljava/lang/CharSequence;Ljava/util/List;)V

    .line 1401108
    :goto_0
    return-void

    .line 1401109
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1401110
    check-cast v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel;

    invoke-virtual {v0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel;->a()Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel;->a()LX/0Px;

    move-result-object v11

    .line 1401111
    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v12

    move v8, v9

    :goto_1
    if-ge v8, v12, :cond_2

    invoke-virtual {v11, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel$NodesModel;

    .line 1401112
    invoke-virtual {v2}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel$NodesModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v5, v0, LX/1vs;->b:I

    .line 1401113
    iget-object v0, p0, LX/8nU;->b:LX/8nW;

    iget-object v0, v0, LX/8nW;->b:LX/3iT;

    new-instance v1, Lcom/facebook/user/model/Name;

    invoke-virtual {v2}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v13, v13, v3}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToSuggestionsQueryModels$FetchGroupMembersToSuggestionsModel$GroupMembersModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v4, v5, v9}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/7Gr;->USER:LX/7Gr;

    const-string v6, "group_members"

    const-string v7, ""

    invoke-virtual/range {v0 .. v7}, LX/3iT;->a(Lcom/facebook/user/model/Name;JLjava/lang/String;LX/7Gr;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/tagging/model/TaggingProfile;

    move-result-object v0

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1401114
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_1

    .line 1401115
    :cond_2
    iget-object v0, p0, LX/8nU;->a:LX/8JX;

    const-string v1, ""

    invoke-interface {v0, v1, v10}, LX/8JX;->a(Ljava/lang/CharSequence;Ljava/util/List;)V

    goto :goto_0
.end method
