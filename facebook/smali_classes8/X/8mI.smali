.class public final LX/8mI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Mb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3Mb",
        "<",
        "LX/8jS;",
        "LX/8jT;",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/stickers/store/StickerStoreActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/store/StickerStoreActivity;Z)V
    .locals 0

    .prologue
    .line 1398608
    iput-object p1, p0, LX/8mI;->b:Lcom/facebook/stickers/store/StickerStoreActivity;

    iput-boolean p2, p0, LX/8mI;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(LX/8jT;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v9, 0x0

    .line 1398600
    iget-object v7, p1, LX/8jT;->a:Lcom/facebook/stickers/model/StickerPack;

    .line 1398601
    invoke-static {v7}, Lcom/facebook/stickers/store/StickerStoreActivity;->b(Lcom/facebook/stickers/model/StickerPack;)Ljava/lang/String;

    move-result-object v8

    .line 1398602
    iget-object v0, p0, LX/8mI;->b:Lcom/facebook/stickers/store/StickerStoreActivity;

    iget-object v0, v0, Lcom/facebook/stickers/store/StickerStoreActivity;->r:LX/11i;

    sget-object v1, LX/8lJ;->d:LX/8lI;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 1398603
    if-eqz v0, :cond_0

    .line 1398604
    const-string v1, "StickerCreateStickerStoreActivity"

    iget-object v3, p0, LX/8mI;->b:Lcom/facebook/stickers/store/StickerStoreActivity;

    iget-object v3, v3, Lcom/facebook/stickers/store/StickerStoreActivity;->q:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    const v6, 0x5b00ef09

    move-object v3, v2

    invoke-static/range {v0 .. v6}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    .line 1398605
    :cond_0
    iget-object v0, p0, LX/8mI;->b:Lcom/facebook/stickers/store/StickerStoreActivity;

    iget-boolean v3, p0, LX/8mI;->a:Z

    move-object v1, v7

    move v2, v9

    move-object v4, v8

    move v5, v9

    .line 1398606
    invoke-static/range {v0 .. v5}, Lcom/facebook/stickers/store/StickerStoreActivity;->a$redex0(Lcom/facebook/stickers/store/StickerStoreActivity;Lcom/facebook/stickers/model/StickerPack;ZZLjava/lang/String;Z)V

    .line 1398607
    return-void
.end method

.method private a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1398593
    iget-object v0, p0, LX/8mI;->b:Lcom/facebook/stickers/store/StickerStoreActivity;

    iget-object v0, v0, Lcom/facebook/stickers/store/StickerStoreActivity;->u:LX/03V;

    const-string v1, "StickerStoreActivity_StickerPackFromIdLoadFailed"

    const-string v2, "Failed to load sticker pack from sticker id"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1398594
    iget-object v0, p0, LX/8mI;->b:Lcom/facebook/stickers/store/StickerStoreActivity;

    invoke-static {v0}, Lcom/facebook/stickers/store/StickerStoreActivity;->m(Lcom/facebook/stickers/store/StickerStoreActivity;)V

    .line 1398595
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 0

    .prologue
    .line 1398599
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1398598
    return-void
.end method

.method public final synthetic b(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1398597
    check-cast p2, LX/8jT;

    invoke-direct {p0, p2}, LX/8mI;->a(LX/8jT;)V

    return-void
.end method

.method public final synthetic c(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1398596
    check-cast p2, Ljava/lang/Throwable;

    invoke-direct {p0, p2}, LX/8mI;->a(Ljava/lang/Throwable;)V

    return-void
.end method
