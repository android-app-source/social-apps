.class public abstract LX/9VI;
.super LX/9VF;
.source ""


# instance fields
.field private final d:F

.field private final e:F


# direct methods
.method public constructor <init>(JJIFF)V
    .locals 1

    .prologue
    .line 1499209
    invoke-direct/range {p0 .. p5}, LX/9VF;-><init>(JJI)V

    .line 1499210
    iput p6, p0, LX/9VI;->d:F

    .line 1499211
    iput p7, p0, LX/9VI;->e:F

    .line 1499212
    return-void
.end method


# virtual methods
.method public abstract a(LX/9VU;F)V
.end method

.method public final a(LX/9VU;J)V
    .locals 10

    .prologue
    .line 1499213
    iget v2, p0, LX/9VI;->d:F

    iget v3, p0, LX/9VI;->e:F

    iget-wide v4, p0, LX/9VF;->a:J

    iget-wide v6, p0, LX/9VF;->b:J

    move-wide v0, p2

    .line 1499214
    sub-long v8, v0, v4

    long-to-float v8, v8

    .line 1499215
    long-to-float v9, v6

    cmpl-float v9, v8, v9

    if-lez v9, :cond_0

    .line 1499216
    long-to-float v8, v6

    .line 1499217
    :cond_0
    long-to-float v9, v6

    div-float/2addr v8, v9

    .line 1499218
    sub-float v9, v3, v2

    .line 1499219
    mul-float/2addr v8, v9

    add-float/2addr v8, v2

    move v0, v8

    .line 1499220
    invoke-virtual {p0, p1, v0}, LX/9VI;->a(LX/9VU;F)V

    .line 1499221
    return-void
.end method
