.class public final LX/9lS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/rapidreporting/ui/DialogStateData;

.field public final synthetic b:Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;

.field public final synthetic c:LX/9lb;


# direct methods
.method public constructor <init>(LX/9lb;Lcom/facebook/rapidreporting/ui/DialogStateData;Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;)V
    .locals 0

    .prologue
    .line 1532878
    iput-object p1, p0, LX/9lS;->c:LX/9lb;

    iput-object p2, p0, LX/9lS;->a:Lcom/facebook/rapidreporting/ui/DialogStateData;

    iput-object p3, p0, LX/9lS;->b:Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1532879
    iget-object v0, p0, LX/9lS;->c:LX/9lb;

    iget-object v1, p0, LX/9lS;->b:Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;

    sget-object v2, LX/9lY;->NETWORK_ERROR:LX/9lY;

    invoke-static {v0, v1, v2}, LX/9lb;->a$redex0(LX/9lb;Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;LX/9lY;)V

    .line 1532880
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1532881
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1532882
    if-eqz p1, :cond_1

    .line 1532883
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1532884
    if-eqz v0, :cond_1

    .line 1532885
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1532886
    check-cast v0, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel;

    invoke-virtual {v0}, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 1532887
    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    .line 1532888
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1532889
    check-cast v0, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel;

    invoke-virtual {v0}, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1532890
    const/4 v4, 0x2

    invoke-virtual {v3, v0, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    :goto_1
    if-eqz v1, :cond_4

    .line 1532891
    iget-object v0, p0, LX/9lS;->a:Lcom/facebook/rapidreporting/ui/DialogStateData;

    .line 1532892
    iput-object p1, v0, Lcom/facebook/rapidreporting/ui/DialogStateData;->d:Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1532893
    iget-object v1, v0, Lcom/facebook/rapidreporting/ui/DialogStateData;->d:Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1532894
    iget-object v2, v1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v2

    .line 1532895
    check-cast v1, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel;

    invoke-virtual {v1}, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel;->a()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const/4 v3, 0x0

    const-class v4, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$ConfirmationPromptModel$FollowupActionsModel;

    invoke-virtual {v2, v1, v3, v4}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v1

    .line 1532896
    if-eqz v1, :cond_5

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 1532897
    :goto_2
    invoke-static {v1}, Lcom/facebook/rapidreporting/ui/GuidedActionItem;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, v0, Lcom/facebook/rapidreporting/ui/DialogStateData;->l:Ljava/util/List;

    .line 1532898
    iget-object v0, p0, LX/9lS;->c:LX/9lb;

    iget-object v1, p0, LX/9lS;->b:Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;

    .line 1532899
    iget-object v2, v1, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->q:Lcom/facebook/rapidreporting/ui/DialogStateData;

    move-object v2, v2

    .line 1532900
    iget-object v3, v2, Lcom/facebook/rapidreporting/ui/DialogStateData;->a:Lcom/facebook/rapidreporting/ui/DialogConfig;

    move-object v3, v3

    .line 1532901
    iget-object v4, v3, Lcom/facebook/rapidreporting/ui/DialogConfig;->d:LX/Js1;

    move-object v3, v4

    .line 1532902
    if-nez v3, :cond_6

    .line 1532903
    invoke-static {v0, v1}, LX/9lb;->e(LX/9lb;Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;)V

    .line 1532904
    :goto_3
    return-void

    :cond_0
    move v0, v2

    .line 1532905
    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_1

    .line 1532906
    :cond_4
    iget-object v0, p0, LX/9lS;->c:LX/9lb;

    iget-object v1, p0, LX/9lS;->b:Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;

    sget-object v2, LX/9lY;->SERVER_ERROR:LX/9lY;

    invoke-static {v0, v1, v2}, LX/9lb;->a$redex0(LX/9lb;Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;LX/9lY;)V

    goto :goto_3

    .line 1532907
    :cond_5
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 1532908
    goto :goto_2

    .line 1532909
    :cond_6
    iget-object v4, v0, LX/9lb;->c:LX/0TD;

    new-instance p0, LX/9lT;

    invoke-direct {p0, v0, v3, v2}, LX/9lT;-><init>(LX/9lb;LX/Js1;Lcom/facebook/rapidreporting/ui/DialogStateData;)V

    invoke-interface {v4, p0}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 1532910
    iget-object v3, v0, LX/9lb;->b:LX/1Ck;

    sget-object v4, LX/9la;->FETCH_ADDITIONAL_DATA:LX/9la;

    new-instance p0, LX/9lU;

    invoke-direct {p0, v0, v1}, LX/9lU;-><init>(LX/9lb;Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;)V

    invoke-static {p0}, LX/0Vd;->of(LX/0TF;)LX/0Vd;

    move-result-object p0

    invoke-virtual {v3, v4, v2, p0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_3
.end method
