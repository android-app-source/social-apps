.class public LX/AQY;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1671432
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1671433
    return-void
.end method

.method public static a(Landroid/content/Intent;)Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;
    .locals 5

    .prologue
    .line 1671434
    const/4 v1, 0x0

    .line 1671435
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, p0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 1671436
    const-string v0, "life_event_model"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    .line 1671437
    sget-object v3, LX/AQX;->a:[I

    .line 1671438
    iget-object v4, v0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->b:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    move-object v4, v4

    .line 1671439
    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 1671440
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1671441
    :pswitch_0
    new-instance v0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventMarriageInterstitialFragment;

    invoke-direct {v0}, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventMarriageInterstitialFragment;-><init>()V

    .line 1671442
    :goto_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1671443
    const-string v3, "intent"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1671444
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1671445
    return-object v0

    .line 1671446
    :pswitch_1
    new-instance v0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventEngagementInterstitialFragment;

    invoke-direct {v0}, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventEngagementInterstitialFragment;-><init>()V

    goto :goto_0

    .line 1671447
    :pswitch_2
    new-instance v0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventWorkInterstitialFragment;

    invoke-direct {v0}, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventWorkInterstitialFragment;-><init>()V

    goto :goto_0

    .line 1671448
    :pswitch_3
    iget-object v3, v0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->i:Lcom/facebook/graphql/enums/GraphQLLifeEventEducationExperienceType;

    move-object v3, v3

    .line 1671449
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLLifeEventEducationExperienceType;->COLLEGE:Lcom/facebook/graphql/enums/GraphQLLifeEventEducationExperienceType;

    invoke-virtual {v3, v4}, Lcom/facebook/graphql/enums/GraphQLLifeEventEducationExperienceType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1671450
    new-instance v0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventCollegeInterstitialFragment;

    invoke-direct {v0}, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventCollegeInterstitialFragment;-><init>()V

    goto :goto_0

    .line 1671451
    :cond_0
    iget-object v3, v0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->i:Lcom/facebook/graphql/enums/GraphQLLifeEventEducationExperienceType;

    move-object v0, v3

    .line 1671452
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLLifeEventEducationExperienceType;->HIGHSCHOOL:Lcom/facebook/graphql/enums/GraphQLLifeEventEducationExperienceType;

    invoke-virtual {v0, v3}, Lcom/facebook/graphql/enums/GraphQLLifeEventEducationExperienceType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1671453
    new-instance v0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventHighSchoolInterstitialFragment;

    invoke-direct {v0}, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventHighSchoolInterstitialFragment;-><init>()V

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
