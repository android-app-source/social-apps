.class public final LX/9n7;
.super LX/5pb;
.source ""

# interfaces
.implements LX/5pQ;
.implements LX/9mw;


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "RCTTiming"
.end annotation


# instance fields
.field private final a:LX/5qI;

.field public final b:Ljava/lang/Object;

.field public final c:Ljava/lang/Object;

.field public final d:Ljava/util/PriorityQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/PriorityQueue",
            "<",
            "LX/9n5;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/react/bridge/ExecutorToken;",
            "Landroid/util/SparseArray",
            "<",
            "LX/9n5;",
            ">;>;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public final g:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final h:LX/9n6;

.field private final i:LX/9n4;

.field public j:Lcom/facebook/react/modules/core/Timing$IdleCallbackRunnable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:LX/5r6;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Z

.field private m:Z

.field public final n:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/react/bridge/ExecutorToken;",
            ">;"
        }
    .end annotation
.end field

.field public final o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/react/bridge/ExecutorToken;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/5pY;LX/5qI;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1536776
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 1536777
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/9n7;->b:Ljava/lang/Object;

    .line 1536778
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/9n7;->c:Ljava/lang/Object;

    .line 1536779
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/9n7;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 1536780
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/9n7;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 1536781
    new-instance v0, LX/9n6;

    invoke-direct {v0, p0}, LX/9n6;-><init>(LX/9n7;)V

    iput-object v0, p0, LX/9n7;->h:LX/9n6;

    .line 1536782
    new-instance v0, LX/9n4;

    invoke-direct {v0, p0}, LX/9n4;-><init>(LX/9n7;)V

    iput-object v0, p0, LX/9n7;->i:LX/9n4;

    .line 1536783
    iput-boolean v2, p0, LX/9n7;->l:Z

    .line 1536784
    iput-boolean v2, p0, LX/9n7;->m:Z

    .line 1536785
    iput-object p2, p0, LX/9n7;->a:LX/5qI;

    .line 1536786
    new-instance v0, Ljava/util/PriorityQueue;

    const/16 v1, 0xb

    new-instance v2, LX/9n3;

    invoke-direct {v2, p0}, LX/9n3;-><init>(LX/9n7;)V

    invoke-direct {v0, v1, v2}, Ljava/util/PriorityQueue;-><init>(ILjava/util/Comparator;)V

    iput-object v0, p0, LX/9n7;->d:Ljava/util/PriorityQueue;

    .line 1536787
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/9n7;->e:Ljava/util/Map;

    .line 1536788
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/9n7;->n:Ljava/util/Set;

    .line 1536789
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/9n7;->o:Ljava/util/List;

    .line 1536790
    return-void
.end method

.method public static synthetic f(LX/9n7;)LX/5pY;
    .locals 1

    .prologue
    .line 1536791
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 1536792
    return-object v0
.end method

.method private h()V
    .locals 2

    .prologue
    .line 1536793
    iget-object v1, p0, LX/9n7;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 1536794
    :try_start_0
    iget-object v0, p0, LX/9n7;->n:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1536795
    invoke-static {p0}, LX/9n7;->w(LX/9n7;)V

    .line 1536796
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static synthetic i(LX/9n7;)LX/5pY;
    .locals 1

    .prologue
    .line 1536774
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 1536775
    return-object v0
.end method

.method private i()V
    .locals 1

    .prologue
    .line 1536801
    iget-object v0, p0, LX/9n7;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9n7;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1536802
    invoke-direct {p0}, LX/9n7;->v()V

    .line 1536803
    :cond_0
    return-void
.end method

.method public static synthetic m(LX/9n7;)LX/5pY;
    .locals 1

    .prologue
    .line 1536804
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 1536805
    return-object v0
.end method

.method private u()V
    .locals 3

    .prologue
    .line 1536806
    iget-boolean v0, p0, LX/9n7;->l:Z

    if-nez v0, :cond_0

    .line 1536807
    iget-object v0, p0, LX/9n7;->k:LX/5r6;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5r6;

    sget-object v1, LX/5r4;->TIMERS_EVENTS:LX/5r4;

    iget-object v2, p0, LX/9n7;->h:LX/9n6;

    invoke-virtual {v0, v1, v2}, LX/5r6;->a(LX/5r4;Landroid/view/Choreographer$FrameCallback;)V

    .line 1536808
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/9n7;->l:Z

    .line 1536809
    :cond_0
    return-void
.end method

.method private v()V
    .locals 3

    .prologue
    .line 1536810
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 1536811
    invoke-static {v0}, LX/9mv;->a(LX/5pX;)LX/9mv;

    move-result-object v0

    .line 1536812
    iget-boolean v1, p0, LX/9n7;->l:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/9n7;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, LX/9mv;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1536813
    iget-object v0, p0, LX/9n7;->k:LX/5r6;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5r6;

    sget-object v1, LX/5r4;->TIMERS_EVENTS:LX/5r4;

    iget-object v2, p0, LX/9n7;->h:LX/9n6;

    invoke-virtual {v0, v1, v2}, LX/5r6;->b(LX/5r4;Landroid/view/Choreographer$FrameCallback;)V

    .line 1536814
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/9n7;->l:Z

    .line 1536815
    :cond_0
    return-void
.end method

.method public static w(LX/9n7;)V
    .locals 3

    .prologue
    .line 1536816
    iget-boolean v0, p0, LX/9n7;->m:Z

    if-nez v0, :cond_0

    .line 1536817
    iget-object v0, p0, LX/9n7;->k:LX/5r6;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5r6;

    sget-object v1, LX/5r4;->IDLE_EVENT:LX/5r4;

    iget-object v2, p0, LX/9n7;->i:LX/9n4;

    invoke-virtual {v0, v1, v2}, LX/5r6;->a(LX/5r4;Landroid/view/Choreographer$FrameCallback;)V

    .line 1536818
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/9n7;->m:Z

    .line 1536819
    :cond_0
    return-void
.end method

.method public static x(LX/9n7;)V
    .locals 3

    .prologue
    .line 1536820
    iget-boolean v0, p0, LX/9n7;->m:Z

    if-eqz v0, :cond_0

    .line 1536821
    iget-object v0, p0, LX/9n7;->k:LX/5r6;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5r6;

    sget-object v1, LX/5r4;->IDLE_EVENT:LX/5r4;

    iget-object v2, p0, LX/9n7;->i:LX/9n4;

    invoke-virtual {v0, v1, v2}, LX/5r6;->b(LX/5r4;Landroid/view/Choreographer$FrameCallback;)V

    .line 1536822
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/9n7;->m:Z

    .line 1536823
    :cond_0
    return-void
.end method


# virtual methods
.method public final bM_()V
    .locals 2

    .prologue
    .line 1536797
    iget-object v0, p0, LX/9n7;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1536798
    invoke-direct {p0}, LX/9n7;->u()V

    .line 1536799
    invoke-direct {p0}, LX/9n7;->h()V

    .line 1536800
    return-void
.end method

.method public final bN_()V
    .locals 2

    .prologue
    .line 1536770
    iget-object v0, p0, LX/9n7;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1536771
    invoke-direct {p0}, LX/9n7;->v()V

    .line 1536772
    invoke-direct {p0}, LX/9n7;->i()V

    .line 1536773
    return-void
.end method

.method public final bO_()V
    .locals 0

    .prologue
    .line 1536767
    invoke-direct {p0}, LX/9n7;->v()V

    .line 1536768
    invoke-direct {p0}, LX/9n7;->i()V

    .line 1536769
    return-void
.end method

.method public final bP_()Z
    .locals 1

    .prologue
    .line 1536701
    const/4 v0, 0x1

    return v0
.end method

.method public final createTimer(Lcom/facebook/react/bridge/ExecutorToken;IIDZ)V
    .locals 10
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1536745
    invoke-static {}, LX/5pv;->a()J

    move-result-wide v2

    .line 1536746
    double-to-long v4, p4

    .line 1536747
    iget-object v0, p0, LX/9n7;->a:LX/5qI;

    invoke-interface {v0}, LX/5qI;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1536748
    sub-long v0, v4, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    .line 1536749
    const-wide/32 v6, 0xea60

    cmp-long v0, v0, v6

    if-lez v0, :cond_0

    .line 1536750
    invoke-virtual {p0}, LX/5pb;->s()LX/5pY;

    move-result-object v0

    const-class v1, Lcom/facebook/react/modules/core/JSTimersExecution;

    invoke-virtual {v0, p1, v1}, LX/5pX;->a(Lcom/facebook/react/bridge/ExecutorToken;Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/modules/core/JSTimersExecution;

    const-string v1, "Debugger and device times have drifted by more than 60s. Please correct this by running adb shell \"date `date +%m%d%H%M%Y.%S`\" on your debugger machine."

    invoke-interface {v0, v1}, Lcom/facebook/react/modules/core/JSTimersExecution;->emitTimeDriftWarning(Ljava/lang/String;)V

    .line 1536751
    :cond_0
    const-wide/16 v0, 0x0

    sub-long v2, v4, v2

    int-to-long v4, p3

    add-long/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 1536752
    if-nez p3, :cond_1

    if-nez p6, :cond_1

    .line 1536753
    invoke-static {}, LX/5op;->a()LX/5pD;

    move-result-object v1

    .line 1536754
    invoke-interface {v1, p2}, LX/5pD;->pushInt(I)V

    .line 1536755
    invoke-virtual {p0}, LX/5pb;->s()LX/5pY;

    move-result-object v0

    const-class v2, Lcom/facebook/react/modules/core/JSTimersExecution;

    invoke-virtual {v0, p1, v2}, LX/5pX;->a(Lcom/facebook/react/bridge/ExecutorToken;Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/modules/core/JSTimersExecution;

    invoke-interface {v0, v1}, Lcom/facebook/react/modules/core/JSTimersExecution;->callTimers(LX/5pD;)V

    .line 1536756
    :goto_0
    return-void

    .line 1536757
    :cond_1
    invoke-static {}, LX/5pv;->b()J

    move-result-wide v2

    const-wide/32 v4, 0xf4240

    div-long/2addr v2, v4

    add-long v4, v2, v0

    .line 1536758
    new-instance v1, LX/9n5;

    const/4 v8, 0x0

    move-object v2, p1

    move v3, p2

    move v6, p3

    move/from16 v7, p6

    invoke-direct/range {v1 .. v8}, LX/9n5;-><init>(Lcom/facebook/react/bridge/ExecutorToken;IJIZB)V

    .line 1536759
    iget-object v2, p0, LX/9n7;->b:Ljava/lang/Object;

    monitor-enter v2

    .line 1536760
    :try_start_0
    iget-object v0, p0, LX/9n7;->d:Ljava/util/PriorityQueue;

    invoke-virtual {v0, v1}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 1536761
    iget-object v0, p0, LX/9n7;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/SparseArray;

    .line 1536762
    if-nez v0, :cond_2

    .line 1536763
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 1536764
    iget-object v3, p0, LX/9n7;->e:Ljava/util/Map;

    invoke-interface {v3, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1536765
    :cond_2
    invoke-virtual {v0, p2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1536766
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1536738
    invoke-static {}, LX/5r6;->a()LX/5r6;

    move-result-object v0

    iput-object v0, p0, LX/9n7;->k:LX/5r6;

    .line 1536739
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 1536740
    invoke-virtual {v0, p0}, LX/5pX;->a(LX/5pQ;)V

    .line 1536741
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 1536742
    invoke-static {v0}, LX/9mv;->a(LX/5pX;)LX/9mv;

    move-result-object v0

    .line 1536743
    invoke-virtual {v0, p0}, LX/9mv;->a(LX/9mw;)V

    .line 1536744
    return-void
.end method

.method public final deleteTimer(Lcom/facebook/react/bridge/ExecutorToken;I)V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1536724
    iget-object v2, p0, LX/9n7;->b:Ljava/lang/Object;

    monitor-enter v2

    .line 1536725
    :try_start_0
    iget-object v0, p0, LX/9n7;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/SparseArray;

    .line 1536726
    if-nez v0, :cond_0

    .line 1536727
    monitor-exit v2

    .line 1536728
    :goto_0
    return-void

    .line 1536729
    :cond_0
    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/9n5;

    .line 1536730
    if-nez v1, :cond_1

    .line 1536731
    monitor-exit v2

    goto :goto_0

    .line 1536732
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1536733
    :cond_1
    :try_start_1
    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->remove(I)V

    .line 1536734
    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 1536735
    iget-object v0, p0, LX/9n7;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1536736
    :cond_2
    iget-object v0, p0, LX/9n7;->d:Ljava/util/PriorityQueue;

    invoke-virtual {v0, v1}, Ljava/util/PriorityQueue;->remove(Ljava/lang/Object;)Z

    .line 1536737
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 1536718
    invoke-direct {p0}, LX/9n7;->v()V

    .line 1536719
    invoke-static {p0}, LX/9n7;->x(LX/9n7;)V

    .line 1536720
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 1536721
    invoke-static {v0}, LX/9mv;->a(LX/5pX;)LX/9mv;

    move-result-object v0

    .line 1536722
    invoke-virtual {v0, p0}, LX/9mv;->b(LX/9mw;)V

    .line 1536723
    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1536717
    const-string v0, "RCTTiming"

    return-object v0
.end method

.method public final gg_()V
    .locals 2

    .prologue
    .line 1536710
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 1536711
    invoke-static {v0}, LX/9mv;->a(LX/5pX;)LX/9mv;

    move-result-object v0

    .line 1536712
    invoke-virtual {v0}, LX/9mv;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1536713
    iget-object v0, p0, LX/9n7;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1536714
    invoke-direct {p0}, LX/9n7;->v()V

    .line 1536715
    invoke-direct {p0}, LX/9n7;->i()V

    .line 1536716
    :cond_0
    return-void
.end method

.method public final setSendIdleEvents(Lcom/facebook/react/bridge/ExecutorToken;Z)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1536702
    iget-object v1, p0, LX/9n7;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 1536703
    if-eqz p2, :cond_0

    .line 1536704
    :try_start_0
    iget-object v0, p0, LX/9n7;->n:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1536705
    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1536706
    new-instance v0, Lcom/facebook/react/modules/core/Timing$2;

    invoke-direct {v0, p0}, Lcom/facebook/react/modules/core/Timing$2;-><init>(LX/9n7;)V

    invoke-static {v0}, LX/5pe;->a(Ljava/lang/Runnable;)V

    .line 1536707
    return-void

    .line 1536708
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/9n7;->n:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1536709
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
