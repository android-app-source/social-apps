.class public final enum LX/8ic;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8ic;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8ic;

.field public static final enum DEFAULT:LX/8ic;

.field public static final enum HTC_ONE:LX/8ic;

.field public static final enum MOTO_E:LX/8ic;

.field public static final enum NEXUS_5:LX/8ic;

.field public static final enum SAMSUNG_S3:LX/8ic;

.field public static final enum SAMSUNG_S4:LX/8ic;

.field public static final enum SAMSUNG_S5:LX/8ic;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1391838
    new-instance v0, LX/8ic;

    const-string v1, "SAMSUNG_S3"

    invoke-direct {v0, v1, v3}, LX/8ic;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8ic;->SAMSUNG_S3:LX/8ic;

    .line 1391839
    new-instance v0, LX/8ic;

    const-string v1, "SAMSUNG_S4"

    invoke-direct {v0, v1, v4}, LX/8ic;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8ic;->SAMSUNG_S4:LX/8ic;

    .line 1391840
    new-instance v0, LX/8ic;

    const-string v1, "SAMSUNG_S5"

    invoke-direct {v0, v1, v5}, LX/8ic;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8ic;->SAMSUNG_S5:LX/8ic;

    .line 1391841
    new-instance v0, LX/8ic;

    const-string v1, "HTC_ONE"

    invoke-direct {v0, v1, v6}, LX/8ic;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8ic;->HTC_ONE:LX/8ic;

    .line 1391842
    new-instance v0, LX/8ic;

    const-string v1, "NEXUS_5"

    invoke-direct {v0, v1, v7}, LX/8ic;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8ic;->NEXUS_5:LX/8ic;

    .line 1391843
    new-instance v0, LX/8ic;

    const-string v1, "MOTO_E"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/8ic;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8ic;->MOTO_E:LX/8ic;

    .line 1391844
    new-instance v0, LX/8ic;

    const-string v1, "DEFAULT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/8ic;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8ic;->DEFAULT:LX/8ic;

    .line 1391845
    const/4 v0, 0x7

    new-array v0, v0, [LX/8ic;

    sget-object v1, LX/8ic;->SAMSUNG_S3:LX/8ic;

    aput-object v1, v0, v3

    sget-object v1, LX/8ic;->SAMSUNG_S4:LX/8ic;

    aput-object v1, v0, v4

    sget-object v1, LX/8ic;->SAMSUNG_S5:LX/8ic;

    aput-object v1, v0, v5

    sget-object v1, LX/8ic;->HTC_ONE:LX/8ic;

    aput-object v1, v0, v6

    sget-object v1, LX/8ic;->NEXUS_5:LX/8ic;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/8ic;->MOTO_E:LX/8ic;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/8ic;->DEFAULT:LX/8ic;

    aput-object v2, v0, v1

    sput-object v0, LX/8ic;->$VALUES:[LX/8ic;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1391846
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8ic;
    .locals 1

    .prologue
    .line 1391847
    const-class v0, LX/8ic;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8ic;

    return-object v0
.end method

.method public static values()[LX/8ic;
    .locals 1

    .prologue
    .line 1391848
    sget-object v0, LX/8ic;->$VALUES:[LX/8ic;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8ic;

    return-object v0
.end method
