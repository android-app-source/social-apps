.class public LX/9ib;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/74q;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/facerec/manager/FaceBoxPrioritizer;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation
.end field

.field private final d:LX/7yZ;

.field public final e:LX/75S;

.field public final f:LX/75Q;

.field public final g:LX/75F;

.field public h:LX/9iZ;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Ot;LX/9iZ;Ljava/util/concurrent/ExecutorService;LX/75S;LX/75Q;LX/75F;)V
    .locals 1
    .param p4    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/facerec/manager/FaceBoxPrioritizer;",
            ">;",
            "LX/9iZ;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/75S;",
            "LX/75Q;",
            "LX/75F;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1528069
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1528070
    iput-object p1, p0, LX/9ib;->a:Landroid/content/Context;

    .line 1528071
    iput-object p2, p0, LX/9ib;->b:LX/0Ot;

    .line 1528072
    iput-object p4, p0, LX/9ib;->c:Ljava/util/concurrent/ExecutorService;

    .line 1528073
    iput-object p3, p0, LX/9ib;->h:LX/9iZ;

    .line 1528074
    new-instance v0, LX/9ia;

    invoke-direct {v0, p0}, LX/9ia;-><init>(LX/9ib;)V

    move-object v0, v0

    .line 1528075
    iput-object v0, p0, LX/9ib;->d:LX/7yZ;

    .line 1528076
    iput-object p5, p0, LX/9ib;->e:LX/75S;

    .line 1528077
    iput-object p6, p0, LX/9ib;->f:LX/75Q;

    .line 1528078
    iput-object p7, p0, LX/9ib;->g:LX/75F;

    .line 1528079
    return-void
.end method

.method public static a(LX/0QB;)LX/9ib;
    .locals 9

    .prologue
    .line 1528080
    new-instance v1, LX/9ib;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    const/16 v3, 0x1c2d

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {p0}, LX/9iZ;->b(LX/0QB;)LX/9iZ;

    move-result-object v4

    check-cast v4, LX/9iZ;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/75S;->a(LX/0QB;)LX/75S;

    move-result-object v6

    check-cast v6, LX/75S;

    invoke-static {p0}, LX/75Q;->a(LX/0QB;)LX/75Q;

    move-result-object v7

    check-cast v7, LX/75Q;

    invoke-static {p0}, LX/75F;->a(LX/0QB;)LX/75F;

    move-result-object v8

    check-cast v8, LX/75F;

    invoke-direct/range {v1 .. v8}, LX/9ib;-><init>(Landroid/content/Context;LX/0Ot;LX/9iZ;Ljava/util/concurrent/ExecutorService;LX/75S;LX/75Q;LX/75F;)V

    .line 1528081
    move-object v0, v1

    .line 1528082
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1528083
    iget-object v0, p0, LX/9ib;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    const/4 v1, 0x0

    .line 1528084
    iput-object v1, v0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->n:LX/7yZ;

    .line 1528085
    iget-object v0, p0, LX/9ib;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    invoke-virtual {v0}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->b()V

    .line 1528086
    return-void
.end method

.method public final a(Lcom/facebook/ipc/media/MediaItem;LX/74p;)V
    .locals 3

    .prologue
    .line 1528087
    instance-of v0, p1, Lcom/facebook/photos/base/media/PhotoItem;

    if-nez v0, :cond_1

    .line 1528088
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, p1

    .line 1528089
    check-cast v0, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1528090
    iget-object v1, v0, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    move-object v0, v1

    .line 1528091
    check-cast v0, Lcom/facebook/photos/base/photos/LocalPhoto;

    .line 1528092
    iget-object v1, p0, LX/9ib;->g:LX/75F;

    invoke-virtual {v1, v0}, LX/75F;->c(LX/74x;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1528093
    iget-object v0, p0, LX/9ib;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    iget-object v1, p0, LX/9ib;->d:LX/7yZ;

    .line 1528094
    iput-object v1, v0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->n:LX/7yZ;

    .line 1528095
    iget-object v0, p0, LX/9ib;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/photos/tagging/GridTagSuggestRunner$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/photos/tagging/GridTagSuggestRunner$1;-><init>(LX/9ib;Lcom/facebook/ipc/media/MediaItem;LX/74p;)V

    const v2, 0x6f7656cf

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1528096
    iget-object v0, p0, LX/9ib;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    .line 1528097
    const/4 p0, 0x0

    iput p0, v0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->r:I

    .line 1528098
    return-void
.end method
