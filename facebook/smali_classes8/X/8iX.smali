.class public final LX/8iX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/sounds/configurator/AudioConfigurator;

.field private b:Z


# direct methods
.method public constructor <init>(Lcom/facebook/sounds/configurator/AudioConfigurator;)V
    .locals 0

    .prologue
    .line 1391562
    iput-object p1, p0, LX/8iX;->a:Lcom/facebook/sounds/configurator/AudioConfigurator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/sounds/configurator/AudioConfigurator;B)V
    .locals 0

    .prologue
    .line 1391561
    invoke-direct {p0, p1}, LX/8iX;-><init>(Lcom/facebook/sounds/configurator/AudioConfigurator;)V

    return-void
.end method


# virtual methods
.method public final onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 10

    .prologue
    const-wide/16 v2, 0x0

    .line 1391543
    int-to-double v0, p2

    const-wide v4, 0x40c3880000000000L    # 10000.0

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    move-wide v6, v2

    invoke-static/range {v0 .. v9}, LX/8YN;->a(DDDDD)D

    move-result-wide v0

    .line 1391544
    sget-object v2, Lcom/facebook/sounds/configurator/AudioConfigurator;->b:Ljava/text/DecimalFormat;

    invoke-virtual {v2, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    .line 1391545
    iget-boolean v3, p0, LX/8iX;->b:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, LX/8iX;->a:Lcom/facebook/sounds/configurator/AudioConfigurator;

    iget-object v3, v3, Lcom/facebook/sounds/configurator/AudioConfigurator;->l:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1391546
    iget-object v3, p0, LX/8iX;->a:Lcom/facebook/sounds/configurator/AudioConfigurator;

    iget-object v3, v3, Lcom/facebook/sounds/configurator/AudioConfigurator;->l:Landroid/widget/EditText;

    invoke-virtual {v3, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1391547
    :cond_0
    iget-object v2, p0, LX/8iX;->a:Lcom/facebook/sounds/configurator/AudioConfigurator;

    iget-object v2, v2, Lcom/facebook/sounds/configurator/AudioConfigurator;->k:LX/8iO;

    double-to-float v0, v0

    .line 1391548
    iput v0, v2, LX/8iO;->c:F

    .line 1391549
    iget-object v0, p0, LX/8iX;->a:Lcom/facebook/sounds/configurator/AudioConfigurator;

    invoke-static {v0}, Lcom/facebook/sounds/configurator/AudioConfigurator;->d(Lcom/facebook/sounds/configurator/AudioConfigurator;)V

    .line 1391550
    iget-object v0, p0, LX/8iX;->a:Lcom/facebook/sounds/configurator/AudioConfigurator;

    iget-object v0, v0, Lcom/facebook/sounds/configurator/AudioConfigurator;->o:LX/8ih;

    if-eqz v0, :cond_1

    .line 1391551
    iget-object v0, p0, LX/8iX;->a:Lcom/facebook/sounds/configurator/AudioConfigurator;

    iget-object v0, v0, Lcom/facebook/sounds/configurator/AudioConfigurator;->o:LX/8ih;

    invoke-virtual {v0}, LX/8ih;->a()V

    .line 1391552
    iget-object v0, p0, LX/8iX;->a:Lcom/facebook/sounds/configurator/AudioConfigurator;

    const/4 v1, 0x0

    .line 1391553
    iput-object v1, v0, Lcom/facebook/sounds/configurator/AudioConfigurator;->o:LX/8ih;

    .line 1391554
    :cond_1
    return-void
.end method

.method public final onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1

    .prologue
    .line 1391559
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8iX;->b:Z

    .line 1391560
    return-void
.end method

.method public final onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2

    .prologue
    .line 1391555
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/8iX;->b:Z

    .line 1391556
    iget-object v0, p0, LX/8iX;->a:Lcom/facebook/sounds/configurator/AudioConfigurator;

    .line 1391557
    invoke-static {v0}, Lcom/facebook/sounds/configurator/AudioConfigurator;->c$redex0(Lcom/facebook/sounds/configurator/AudioConfigurator;)V

    .line 1391558
    return-void
.end method
