.class public final LX/8z8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/text/style/CharacterStyle;

.field public b:Landroid/text/style/CharacterStyle;

.field private final c:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 1426951
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1426952
    iput-object p1, p0, LX/8z8;->c:Landroid/content/res/Resources;

    .line 1426953
    return-void
.end method


# virtual methods
.method public final a()LX/8z7;
    .locals 5

    .prologue
    .line 1426954
    new-instance v2, LX/8z7;

    iget-object v0, p0, LX/8z8;->a:Landroid/text/style/CharacterStyle;

    if-nez v0, :cond_0

    new-instance v0, LX/8zE;

    iget-object v1, p0, LX/8z8;->c:Landroid/content/res/Resources;

    const v3, 0x7f0a0507

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, LX/8zE;-><init>(I)V

    :goto_0
    iget-object v1, p0, LX/8z8;->b:Landroid/text/style/CharacterStyle;

    if-nez v1, :cond_1

    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    iget-object v3, p0, LX/8z8;->c:Landroid/content/res/Resources;

    const v4, 0x7f0a00d1

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v1, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    :goto_1
    invoke-direct {v2, v0, v1}, LX/8z7;-><init>(Landroid/text/style/CharacterStyle;Landroid/text/style/CharacterStyle;)V

    return-object v2

    :cond_0
    iget-object v0, p0, LX/8z8;->a:Landroid/text/style/CharacterStyle;

    goto :goto_0

    :cond_1
    iget-object v1, p0, LX/8z8;->b:Landroid/text/style/CharacterStyle;

    goto :goto_1
.end method
