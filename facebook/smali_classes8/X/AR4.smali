.class public LX/AR4;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/AR3;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1672359
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1672360
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/AR3;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData:",
            "Ljava/lang/Object;",
            ":",
            "LX/0jI;",
            ":",
            "LX/0io;",
            ":",
            "LX/0iw;",
            ":",
            "LX/0ix;",
            ":",
            "LX/0iy;",
            ":",
            "LX/0iz;",
            ":",
            "LX/0j0;",
            ":",
            "LX/0j1;",
            ":",
            "LX/0j2;",
            ":",
            "LX/0j3;",
            ":",
            "LX/0j8;",
            ":",
            "LX/0j4;",
            ":",
            "LX/0iq;",
            ":",
            "LX/0ir;",
            ":",
            "LX/0jA;",
            ":",
            "LX/0jB;",
            ":",
            "LX/0jC;",
            ":",
            "LX/0jD;",
            ":",
            "LX/0j6;",
            ":",
            "LX/0is;",
            ":",
            "LX/0ip;",
            ":",
            "LX/0jG;",
            ":",
            "LX/0jE;",
            ":",
            "LX/0jH;",
            ":",
            "LX/0jF;",
            "DerivedData:",
            "Ljava/lang/Object;",
            ":",
            "LX/2rg;",
            ":",
            "LX/5RE;",
            "Services:",
            "Ljava/lang/Object;",
            ":",
            "LX/0il",
            "<TModelData;>;:",
            "LX/0ik",
            "<TDerivedData;>;>(TServices;)",
            "LX/AR3",
            "<TModelData;TDerivedData;TServices;>;"
        }
    .end annotation

    .prologue
    .line 1672361
    new-instance v0, LX/AR3;

    move-object v1, p1

    check-cast v1, LX/0il;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {p0}, LX/AR0;->b(LX/0QB;)LX/AR0;

    move-result-object v3

    check-cast v3, LX/AR0;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v5

    check-cast v5, LX/0kL;

    invoke-static {p0}, LX/8LV;->b(LX/0QB;)LX/8LV;

    move-result-object v6

    check-cast v6, LX/8LV;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {p0}, LX/75Q;->a(LX/0QB;)LX/75Q;

    move-result-object v8

    check-cast v8, LX/75Q;

    invoke-static {p0}, LX/75F;->a(LX/0QB;)LX/75F;

    move-result-object v9

    check-cast v9, LX/75F;

    invoke-static {p0}, LX/1EZ;->a(LX/0QB;)LX/1EZ;

    move-result-object v10

    check-cast v10, LX/1EZ;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v11

    check-cast v11, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/7Dh;->a(LX/0QB;)LX/7Dh;

    move-result-object v12

    check-cast v12, LX/7Dh;

    invoke-direct/range {v0 .. v12}, LX/AR3;-><init>(LX/0il;Landroid/content/Context;LX/AR0;LX/0SG;LX/0kL;LX/8LV;LX/03V;LX/75Q;LX/75F;LX/1EZ;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/7Dh;)V

    .line 1672362
    const/16 v1, 0x12cb

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    .line 1672363
    iput-object v1, v0, LX/AR3;->a:LX/0Or;

    .line 1672364
    return-object v0
.end method
