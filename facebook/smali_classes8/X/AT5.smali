.class public final LX/AT5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8JH;


# instance fields
.field public final synthetic a:LX/AT6;


# direct methods
.method public constructor <init>(LX/AT6;)V
    .locals 0

    .prologue
    .line 1675160
    iput-object p1, p0, LX/AT5;->a:LX/AT6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/RectF;)V
    .locals 2

    .prologue
    .line 1675153
    iget-object v0, p0, LX/AT5;->a:LX/AT6;

    iget-object v0, v0, LX/AT6;->g:LX/9ic;

    invoke-virtual {v0, p1}, LX/9ic;->a(Landroid/graphics/RectF;)Lcom/facebook/photos/base/tagging/FaceBox;

    move-result-object v0

    .line 1675154
    iget-object v1, p0, LX/AT5;->a:LX/AT6;

    iget-object v1, v1, LX/AT6;->i:LX/ATV;

    .line 1675155
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1675156
    iget-object p0, v1, LX/ATV;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object p0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ad:LX/ATL;

    iget-object p1, v1, LX/ATV;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object p1, p1, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1675157
    iget-object v1, p0, LX/ATL;->a:LX/ATO;

    iget-object v1, v1, LX/ATO;->G:LX/0wd;

    invoke-virtual {v1}, LX/0wd;->i()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1675158
    :goto_0
    return-void

    .line 1675159
    :cond_0
    iget-object v1, p0, LX/ATL;->a:LX/ATO;

    iget-object v1, v1, LX/ATO;->B:LX/ASe;

    invoke-interface {v1, p1, v0}, LX/ASe;->a(Lcom/facebook/composer/attachments/ComposerAttachment;Lcom/facebook/photos/base/tagging/FaceBox;)V

    goto :goto_0
.end method
