.class public final LX/A6i;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/transliteration/TransliterationFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/transliteration/TransliterationFragment;)V
    .locals 0

    .prologue
    .line 1624671
    iput-object p1, p0, LX/A6i;->a:Lcom/facebook/transliteration/TransliterationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 1624672
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1624673
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 9

    .prologue
    const/high16 v8, 0x1060000

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 1624674
    iget-object v0, p0, LX/A6i;->a:Lcom/facebook/transliteration/TransliterationFragment;

    invoke-static {v0}, Lcom/facebook/transliteration/TransliterationFragment;->c(Lcom/facebook/transliteration/TransliterationFragment;)I

    move-result v0

    .line 1624675
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1624676
    invoke-static {v4}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v2

    .line 1624677
    if-le v0, v2, :cond_0

    .line 1624678
    if-nez v2, :cond_2

    move v0, v3

    .line 1624679
    :cond_0
    :goto_0
    if-lez v0, :cond_3

    add-int/lit8 v2, v0, -0x1

    invoke-interface {p1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    const/16 v5, 0x20

    if-ne v2, v5, :cond_3

    .line 1624680
    iget-object v2, p0, LX/A6i;->a:Lcom/facebook/transliteration/TransliterationFragment;

    add-int/lit8 v0, v0, -0x1

    invoke-static {v4, v0}, LX/A6p;->a(Ljava/lang/String;I)LX/A6P;

    move-result-object v0

    .line 1624681
    iput-object v0, v2, Lcom/facebook/transliteration/TransliterationFragment;->j:LX/A6P;

    .line 1624682
    move v0, v1

    move v2, v1

    .line 1624683
    :goto_1
    iget-object v5, p0, LX/A6i;->a:Lcom/facebook/transliteration/TransliterationFragment;

    iget-object v5, v5, Lcom/facebook/transliteration/TransliterationFragment;->j:LX/A6P;

    if-nez v5, :cond_4

    .line 1624684
    iget-object v0, p0, LX/A6i;->a:Lcom/facebook/transliteration/TransliterationFragment;

    iget-object v0, v0, Lcom/facebook/transliteration/TransliterationFragment;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1624685
    iget-object v0, p0, LX/A6i;->a:Lcom/facebook/transliteration/TransliterationFragment;

    iget-object v0, v0, Lcom/facebook/transliteration/TransliterationFragment;->i:LX/A6V;

    invoke-virtual {v0}, LX/A6V;->a()V

    .line 1624686
    iget-object v0, p0, LX/A6i;->a:Lcom/facebook/transliteration/TransliterationFragment;

    iget-object v0, v0, Lcom/facebook/transliteration/TransliterationFragment;->c:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v1, p0, LX/A6i;->a:Lcom/facebook/transliteration/TransliterationFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1624687
    iget-object v0, p0, LX/A6i;->a:Lcom/facebook/transliteration/TransliterationFragment;

    iget-object v0, v0, Lcom/facebook/transliteration/TransliterationFragment;->c:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setEnabled(Z)V

    .line 1624688
    :cond_1
    :goto_2
    return-void

    .line 1624689
    :cond_2
    add-int/lit8 v0, v2, -0x1

    goto :goto_0

    .line 1624690
    :cond_3
    iget-object v2, p0, LX/A6i;->a:Lcom/facebook/transliteration/TransliterationFragment;

    invoke-static {v4, v0}, LX/A6p;->a(Ljava/lang/String;I)LX/A6P;

    move-result-object v0

    .line 1624691
    iput-object v0, v2, Lcom/facebook/transliteration/TransliterationFragment;->j:LX/A6P;

    .line 1624692
    if-ne p4, p3, :cond_9

    move v0, v3

    move v2, v1

    .line 1624693
    goto :goto_1

    .line 1624694
    :cond_4
    iget-object v5, p0, LX/A6i;->a:Lcom/facebook/transliteration/TransliterationFragment;

    iget-object v6, p0, LX/A6i;->a:Lcom/facebook/transliteration/TransliterationFragment;

    iget-object v6, v6, Lcom/facebook/transliteration/TransliterationFragment;->f:LX/A6e;

    iget-object v7, p0, LX/A6i;->a:Lcom/facebook/transliteration/TransliterationFragment;

    iget-object v7, v7, Lcom/facebook/transliteration/TransliterationFragment;->j:LX/A6P;

    iget-object v7, v7, LX/A6P;->a:Ljava/lang/String;

    invoke-virtual {v6, v7}, LX/A6e;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v6

    .line 1624695
    iput-object v6, v5, Lcom/facebook/transliteration/TransliterationFragment;->h:Ljava/util/List;

    .line 1624696
    if-eqz v2, :cond_7

    .line 1624697
    iget-object v1, p0, LX/A6i;->a:Lcom/facebook/transliteration/TransliterationFragment;

    iget-object v1, v1, Lcom/facebook/transliteration/TransliterationFragment;->i:LX/A6V;

    const/4 v5, 0x1

    .line 1624698
    iget-object v2, v1, LX/A6V;->f:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-le v2, v5, :cond_a

    .line 1624699
    iget-object v2, v1, LX/A6V;->f:Ljava/util/ArrayList;

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1624700
    :goto_3
    move-object v2, v2

    .line 1624701
    if-eqz v2, :cond_5

    .line 1624702
    if-eqz v0, :cond_6

    .line 1624703
    iget-object v0, p0, LX/A6i;->a:Lcom/facebook/transliteration/TransliterationFragment;

    iget-object v0, v0, Lcom/facebook/transliteration/TransliterationFragment;->j:LX/A6P;

    invoke-static {v4, v0, v2}, LX/A6p;->a(Ljava/lang/String;LX/A6P;Ljava/lang/String;)LX/A6Q;

    move-result-object v0

    .line 1624704
    iget v1, v0, LX/A6Q;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/A6Q;->b:I

    .line 1624705
    :goto_4
    iget-object v1, p0, LX/A6i;->a:Lcom/facebook/transliteration/TransliterationFragment;

    invoke-virtual {v1, v0}, Lcom/facebook/transliteration/TransliterationFragment;->a(LX/A6Q;)V

    .line 1624706
    iget-object v0, p0, LX/A6i;->a:Lcom/facebook/transliteration/TransliterationFragment;

    iget-object v0, v0, Lcom/facebook/transliteration/TransliterationFragment;->g:LX/A6X;

    iget-object v1, p0, LX/A6i;->a:Lcom/facebook/transliteration/TransliterationFragment;

    iget-object v1, v1, Lcom/facebook/transliteration/TransliterationFragment;->j:LX/A6P;

    iget-object v1, v1, LX/A6P;->a:Ljava/lang/String;

    iget-object v4, p0, LX/A6i;->a:Lcom/facebook/transliteration/TransliterationFragment;

    iget-object v4, v4, Lcom/facebook/transliteration/TransliterationFragment;->l:LX/A6N;

    iget-object v5, p0, LX/A6i;->a:Lcom/facebook/transliteration/TransliterationFragment;

    iget-object v5, v5, Lcom/facebook/transliteration/TransliterationFragment;->f:LX/A6e;

    .line 1624707
    iget v6, v5, LX/A6e;->h:I

    move v5, v6

    .line 1624708
    invoke-virtual/range {v0 .. v5}, LX/A6X;->a(Ljava/lang/String;Ljava/lang/String;ILX/A6N;I)V

    .line 1624709
    :cond_5
    iget-object v0, p0, LX/A6i;->a:Lcom/facebook/transliteration/TransliterationFragment;

    iget-object v0, v0, Lcom/facebook/transliteration/TransliterationFragment;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1624710
    iget-object v0, p0, LX/A6i;->a:Lcom/facebook/transliteration/TransliterationFragment;

    iget-object v0, v0, Lcom/facebook/transliteration/TransliterationFragment;->i:LX/A6V;

    invoke-virtual {v0}, LX/A6V;->a()V

    .line 1624711
    iget-object v0, p0, LX/A6i;->a:Lcom/facebook/transliteration/TransliterationFragment;

    iget-object v0, v0, Lcom/facebook/transliteration/TransliterationFragment;->c:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v1, p0, LX/A6i;->a:Lcom/facebook/transliteration/TransliterationFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1624712
    iget-object v0, p0, LX/A6i;->a:Lcom/facebook/transliteration/TransliterationFragment;

    iget-object v0, v0, Lcom/facebook/transliteration/TransliterationFragment;->c:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setEnabled(Z)V

    goto/16 :goto_2

    .line 1624713
    :cond_6
    iget-object v0, p0, LX/A6i;->a:Lcom/facebook/transliteration/TransliterationFragment;

    iget-object v0, v0, Lcom/facebook/transliteration/TransliterationFragment;->j:LX/A6P;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v0, v1}, LX/A6p;->a(Ljava/lang/String;LX/A6P;Ljava/lang/String;)LX/A6Q;

    move-result-object v0

    goto :goto_4

    .line 1624714
    :cond_7
    iget-object v0, p0, LX/A6i;->a:Lcom/facebook/transliteration/TransliterationFragment;

    iget-object v0, v0, Lcom/facebook/transliteration/TransliterationFragment;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1624715
    iget-object v0, p0, LX/A6i;->a:Lcom/facebook/transliteration/TransliterationFragment;

    iget-object v0, v0, Lcom/facebook/transliteration/TransliterationFragment;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x3

    if-le v0, v2, :cond_8

    .line 1624716
    iget-object v0, p0, LX/A6i;->a:Lcom/facebook/transliteration/TransliterationFragment;

    iget-object v0, v0, Lcom/facebook/transliteration/TransliterationFragment;->c:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v2, p0, LX/A6i;->a:Lcom/facebook/transliteration/TransliterationFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x1060012

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1624717
    iget-object v0, p0, LX/A6i;->a:Lcom/facebook/transliteration/TransliterationFragment;

    iget-object v0, v0, Lcom/facebook/transliteration/TransliterationFragment;->c:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setEnabled(Z)V

    .line 1624718
    :cond_8
    iget-object v0, p0, LX/A6i;->a:Lcom/facebook/transliteration/TransliterationFragment;

    iget-object v0, v0, Lcom/facebook/transliteration/TransliterationFragment;->i:LX/A6V;

    iget-object v1, p0, LX/A6i;->a:Lcom/facebook/transliteration/TransliterationFragment;

    iget-object v1, v1, Lcom/facebook/transliteration/TransliterationFragment;->h:Ljava/util/List;

    iget-object v2, p0, LX/A6i;->a:Lcom/facebook/transliteration/TransliterationFragment;

    iget-object v2, v2, Lcom/facebook/transliteration/TransliterationFragment;->j:LX/A6P;

    iget-object v2, v2, LX/A6P;->a:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 1624719
    iget-object v3, v0, LX/A6V;->f:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 1624720
    iget-object v3, v0, LX/A6V;->f:Ljava/util/ArrayList;

    invoke-virtual {v3, v4, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1624721
    iget-object v3, v0, LX/A6V;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1624722
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_b

    .line 1624723
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1624724
    iget-object v4, v0, LX/A6V;->f:Ljava/util/ArrayList;

    invoke-virtual {v4, v5, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1624725
    iget-object v4, v0, LX/A6V;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v4, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1624726
    :goto_5
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-le v3, v5, :cond_c

    .line 1624727
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1624728
    iget-object v4, v0, LX/A6V;->f:Ljava/util/ArrayList;

    invoke-virtual {v4, v6, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1624729
    iget-object v4, v0, LX/A6V;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v4, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1624730
    :goto_6
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-le v3, v6, :cond_d

    .line 1624731
    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1624732
    iget-object v4, v0, LX/A6V;->f:Ljava/util/ArrayList;

    const/4 v5, 0x3

    invoke-virtual {v4, v5, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1624733
    iget-object v4, v0, LX/A6V;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v4, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1624734
    :goto_7
    goto/16 :goto_2

    :cond_9
    move v0, v3

    move v2, v3

    goto/16 :goto_1

    :cond_a
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 1624735
    :cond_b
    iget-object v3, v0, LX/A6V;->c:Lcom/facebook/resources/ui/FbTextView;

    const-string v4, " "

    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5

    .line 1624736
    :cond_c
    iget-object v3, v0, LX/A6V;->d:Lcom/facebook/resources/ui/FbTextView;

    const-string v4, " "

    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_6

    .line 1624737
    :cond_d
    iget-object v3, v0, LX/A6V;->e:Lcom/facebook/resources/ui/FbTextView;

    const-string v4, " "

    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_7
.end method
