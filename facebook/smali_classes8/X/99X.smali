.class public final enum LX/99X;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/99X;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/99X;

.field public static final enum FIRST:LX/99X;

.field public static final enum INNER:LX/99X;

.field public static final enum LAST:LX/99X;

.field public static final enum UNKNOWN:LX/99X;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1447380
    new-instance v0, LX/99X;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, LX/99X;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/99X;->UNKNOWN:LX/99X;

    .line 1447381
    new-instance v0, LX/99X;

    const-string v1, "FIRST"

    invoke-direct {v0, v1, v3}, LX/99X;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/99X;->FIRST:LX/99X;

    .line 1447382
    new-instance v0, LX/99X;

    const-string v1, "INNER"

    invoke-direct {v0, v1, v4}, LX/99X;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/99X;->INNER:LX/99X;

    .line 1447383
    new-instance v0, LX/99X;

    const-string v1, "LAST"

    invoke-direct {v0, v1, v5}, LX/99X;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/99X;->LAST:LX/99X;

    .line 1447384
    const/4 v0, 0x4

    new-array v0, v0, [LX/99X;

    sget-object v1, LX/99X;->UNKNOWN:LX/99X;

    aput-object v1, v0, v2

    sget-object v1, LX/99X;->FIRST:LX/99X;

    aput-object v1, v0, v3

    sget-object v1, LX/99X;->INNER:LX/99X;

    aput-object v1, v0, v4

    sget-object v1, LX/99X;->LAST:LX/99X;

    aput-object v1, v0, v5

    sput-object v0, LX/99X;->$VALUES:[LX/99X;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1447379
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/99X;
    .locals 1

    .prologue
    .line 1447378
    const-class v0, LX/99X;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/99X;

    return-object v0
.end method

.method public static values()[LX/99X;
    .locals 1

    .prologue
    .line 1447377
    sget-object v0, LX/99X;->$VALUES:[LX/99X;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/99X;

    return-object v0
.end method
