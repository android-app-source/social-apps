.class public final LX/9es;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V
    .locals 0

    .prologue
    .line 1520652
    iput-object p1, p0, LX/9es;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x2

    const v1, 0x62bad275

    invoke-static {v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1520653
    iget-object v1, p0, LX/9es;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    .line 1520654
    iget-boolean p1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->p:Z

    move v1, p1

    .line 1520655
    if-eqz v1, :cond_1

    iget-object v1, p0, LX/9es;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-boolean v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->j:Z

    if-nez v1, :cond_0

    iget-object v1, p0, LX/9es;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->N:LX/9eb;

    invoke-interface {v1}, LX/9eb;->l()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1520656
    :cond_0
    iget-object v1, p0, LX/9es;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    invoke-static {v1}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->v$redex0(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V

    .line 1520657
    :goto_0
    const v1, 0x582f3acc

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1520658
    :cond_1
    iget-object v1, p0, LX/9es;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    .line 1520659
    iget-boolean p1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->p:Z

    move v1, p1

    .line 1520660
    if-nez v1, :cond_2

    .line 1520661
    iget-object v1, p0, LX/9es;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->b(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;Z)V

    goto :goto_0

    .line 1520662
    :cond_2
    iget-object v1, p0, LX/9es;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    invoke-static {v1, v2}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->b(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;Z)V

    goto :goto_0
.end method
