.class public final LX/AE7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public final synthetic b:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic c:LX/AEA;


# direct methods
.method public constructor <init>(LX/AEA;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 1646021
    iput-object p1, p0, LX/AE7;->c:LX/AEA;

    iput-object p2, p0, LX/AE7;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iput-object p3, p0, LX/AE7;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 1646022
    iget-object v0, p0, LX/AE7;->c:LX/AEA;

    iget-object v1, v0, LX/AEA;->a:LX/2dj;

    iget-object v0, p0, LX/AE7;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    sget-object v4, LX/2h8;->NEWSFEED:LX/2h8;

    move-object v6, v5

    invoke-virtual/range {v1 .. v6}, LX/2dj;->a(JLX/2h8;LX/2hC;LX/5P2;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 1646023
    iget-object v0, p0, LX/AE7;->c:LX/AEA;

    iget-object v0, v0, LX/AEA;->l:LX/1Sl;

    invoke-virtual {v0}, LX/1Sl;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1646024
    iget-object v0, p0, LX/AE7;->c:LX/AEA;

    iget-object v2, v0, LX/AEA;->c:LX/0bH;

    new-instance v3, LX/1Ne;

    iget-object v0, p0, LX/AE7;->c:LX/AEA;

    iget-object v4, v0, LX/AEA;->d:LX/189;

    iget-object v5, p0, LX/AE7;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v0, p0, LX/AE7;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/1VO;->w(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v4, v5, v0}, LX/189;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Z)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    invoke-direct {v3, v0}, LX/1Ne;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b7;)V

    .line 1646025
    :cond_0
    iget-object v0, p0, LX/AE7;->c:LX/AEA;

    iget-object v0, v0, LX/AEA;->e:LX/0Sh;

    new-instance v2, LX/AE6;

    invoke-direct {v2, p0}, LX/AE6;-><init>(LX/AE7;)V

    invoke-virtual {v0, v1, v2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1646026
    return-void

    .line 1646027
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
