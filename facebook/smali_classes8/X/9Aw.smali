.class public LX/9Aw;
.super Landroid/graphics/drawable/Drawable;
.source ""


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1zf;",
            ">;"
        }
    .end annotation
.end field

.field private final b:I

.field private final c:I

.field private d:I

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "LX/0Ot",
            "<",
            "LX/1zf;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1450927
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 1450928
    iput-object p2, p0, LX/9Aw;->a:LX/0Ot;

    .line 1450929
    const v0, 0x7f0b0fe5

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/9Aw;->b:I

    .line 1450930
    const v0, 0x7f0b0fe6

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/9Aw;->c:I

    .line 1450931
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/9Aw;->e:Ljava/util/List;

    .line 1450932
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 1450913
    iget-object v0, p0, LX/9Aw;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1450914
    iget-object v0, p0, LX/9Aw;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1zf;

    .line 1450915
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->M()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1zf;->a(Ljava/util/List;)LX/0Px;

    move-result-object v1

    .line 1450916
    if-eqz v1, :cond_0

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1450917
    :cond_0
    invoke-virtual {v0}, LX/1zf;->d()LX/0Px;

    move-result-object v0

    move-object v1, v0

    .line 1450918
    :cond_1
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v5

    move v2, v3

    move v4, v3

    :goto_0
    if-ge v2, v5, :cond_2

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1zt;

    .line 1450919
    invoke-virtual {v0}, LX/1zt;->e()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1450920
    iget v6, p0, LX/9Aw;->b:I

    iget v7, p0, LX/9Aw;->b:I

    invoke-virtual {v0, v3, v3, v6, v7}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1450921
    iget-object v6, p0, LX/9Aw;->e:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1450922
    add-int/lit8 v4, v4, 0x1

    .line 1450923
    const/16 v0, 0x8

    if-eq v4, v0, :cond_2

    .line 1450924
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1450925
    :cond_2
    iget-object v0, p0, LX/9Aw;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, LX/9Aw;->b:I

    iget v2, p0, LX/9Aw;->c:I

    add-int/2addr v1, v2

    mul-int/2addr v0, v1

    iput v0, p0, LX/9Aw;->d:I

    .line 1450926
    return-void
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 1450906
    iget-object v0, p0, LX/9Aw;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1450907
    :goto_0
    return-void

    .line 1450908
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1450909
    iget-object v0, p0, LX/9Aw;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 1450910
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1450911
    iget v0, p0, LX/9Aw;->b:I

    iget v2, p0, LX/9Aw;->c:I

    add-int/2addr v0, v2

    int-to-float v0, v0

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_1

    .line 1450912
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0
.end method

.method public final getIntrinsicHeight()I
    .locals 1

    .prologue
    .line 1450933
    iget v0, p0, LX/9Aw;->b:I

    return v0
.end method

.method public final getIntrinsicWidth()I
    .locals 1

    .prologue
    .line 1450905
    iget v0, p0, LX/9Aw;->d:I

    return v0
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 1450904
    const/4 v0, -0x2

    return v0
.end method

.method public final setAlpha(I)V
    .locals 0

    .prologue
    .line 1450903
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    .prologue
    .line 1450902
    return-void
.end method
