.class public final LX/ACt;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1643217
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1643218
    return-void
.end method

.method public static a(LX/ACs;)Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/ACs;",
            ")",
            "Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1643219
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1643220
    sget-object v0, LX/ACr;->a:[I

    invoke-virtual {p0}, LX/ACs;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1643221
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Got an unknown SelectorType: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/ACs;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 1643222
    :pswitch_0
    new-instance v0, Lcom/facebook/adinterfaces/ui/selector/LocationTargetingFragment;

    invoke-direct {v0}, Lcom/facebook/adinterfaces/ui/selector/LocationTargetingFragment;-><init>()V

    .line 1643223
    :goto_0
    return-object v0

    :pswitch_1
    new-instance v0, Lcom/facebook/adinterfaces/ui/selector/InterestTargetingSelectorFragment;

    invoke-direct {v0}, Lcom/facebook/adinterfaces/ui/selector/InterestTargetingSelectorFragment;-><init>()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
