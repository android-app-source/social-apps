.class public abstract LX/8uk;
.super Landroid/text/style/ReplacementSpan;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "LX/8QK;",
        ">",
        "Landroid/text/style/ReplacementSpan;"
    }
.end annotation


# static fields
.field public static final a:[I

.field public static final b:[I

.field public static final c:[I

.field public static final d:[I


# instance fields
.field public final e:Landroid/graphics/Rect;

.field public final f:LX/8QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public final g:Landroid/content/res/Resources;

.field public final h:I

.field public final i:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1415663
    new-array v0, v2, [I

    sput-object v0, LX/8uk;->a:[I

    .line 1415664
    new-array v0, v3, [I

    const v1, -0x101009e

    aput v1, v0, v2

    sput-object v0, LX/8uk;->b:[I

    .line 1415665
    new-array v0, v3, [I

    const v1, 0x10100a1

    aput v1, v0, v2

    sput-object v0, LX/8uk;->c:[I

    .line 1415666
    new-array v0, v3, [I

    const v1, 0x10100a7

    aput v1, v0, v2

    sput-object v0, LX/8uk;->d:[I

    .line 1415667
    return-void
.end method

.method public constructor <init>(LX/8QK;Landroid/content/res/Resources;IZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/content/res/Resources;",
            "IZ)V"
        }
    .end annotation

    .prologue
    .line 1415656
    invoke-direct {p0}, Landroid/text/style/ReplacementSpan;-><init>()V

    .line 1415657
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/8uk;->e:Landroid/graphics/Rect;

    .line 1415658
    iput-object p1, p0, LX/8uk;->f:LX/8QK;

    .line 1415659
    iput-object p2, p0, LX/8uk;->g:Landroid/content/res/Resources;

    .line 1415660
    iput p3, p0, LX/8uk;->h:I

    .line 1415661
    iput-boolean p4, p0, LX/8uk;->i:Z

    .line 1415662
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 1415668
    return-void
.end method

.method public a(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1415655
    return-void
.end method

.method public final a(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 1415653
    iget-object v0, p0, LX/8uk;->e:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 1415654
    return-void
.end method

.method public b(Landroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 1415651
    invoke-virtual {p1}, Landroid/graphics/Rect;->setEmpty()V

    .line 1415652
    return-void
.end method

.method public final c()[I
    .locals 2

    .prologue
    .line 1415639
    iget-object v0, p0, LX/8uk;->f:LX/8QK;

    invoke-virtual {v0}, LX/8QK;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1415640
    sget-object v0, LX/8uk;->b:[I

    .line 1415641
    :goto_0
    return-object v0

    .line 1415642
    :cond_0
    iget-object v0, p0, LX/8uk;->f:LX/8QK;

    .line 1415643
    iget-boolean v1, v0, LX/8QK;->c:Z

    move v0, v1

    .line 1415644
    if-eqz v0, :cond_1

    .line 1415645
    sget-object v0, LX/8uk;->c:[I

    goto :goto_0

    .line 1415646
    :cond_1
    iget-object v0, p0, LX/8uk;->f:LX/8QK;

    .line 1415647
    iget-boolean v1, v0, LX/8QK;->d:Z

    move v0, v1

    .line 1415648
    if-eqz v0, :cond_2

    .line 1415649
    sget-object v0, LX/8uk;->d:[I

    goto :goto_0

    .line 1415650
    :cond_2
    sget-object v0, LX/8uk;->a:[I

    goto :goto_0
.end method
