.class public final LX/8l3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/stickers/model/StickerPack;

.field public final synthetic b:Lcom/facebook/stickers/keyboard/StickerPackPageView;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/keyboard/StickerPackPageView;Lcom/facebook/stickers/model/StickerPack;)V
    .locals 0

    .prologue
    .line 1397312
    iput-object p1, p0, LX/8l3;->b:Lcom/facebook/stickers/keyboard/StickerPackPageView;

    iput-object p2, p0, LX/8l3;->a:Lcom/facebook/stickers/model/StickerPack;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x3d66e111

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1397313
    iget-object v1, p0, LX/8l3;->b:Lcom/facebook/stickers/keyboard/StickerPackPageView;

    iget-object v1, v1, Lcom/facebook/stickers/keyboard/StickerPackPageView;->b:Lcom/facebook/stickers/client/StickerDownloadManager;

    iget-object v2, p0, LX/8l3;->a:Lcom/facebook/stickers/model/StickerPack;

    .line 1397314
    invoke-virtual {v1, v2}, Lcom/facebook/stickers/client/StickerDownloadManager;->c(Lcom/facebook/stickers/model/StickerPack;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1397315
    sget-object v4, Lcom/facebook/stickers/client/StickerDownloadManager;->b:Ljava/lang/Class;

    const-string v5, "Download manager was not downloading this sticker pack."

    invoke-static {v4, v5}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1397316
    :goto_0
    const v1, -0x578b8d69

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1397317
    :cond_0
    iget-object v4, v1, Lcom/facebook/stickers/client/StickerDownloadManager;->g:Ljava/util/HashMap;

    .line 1397318
    iget-object v5, v2, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v5, v5

    .line 1397319
    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1Mv;

    .line 1397320
    if-eqz v4, :cond_1

    .line 1397321
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, LX/1Mv;->a(Z)V

    .line 1397322
    :cond_1
    new-instance v4, LX/3ea;

    sget-object v5, LX/3do;->DOWNLOADED_PACKS:LX/3do;

    sget-object v6, LX/0rS;->PREFER_CACHE_IF_UP_TO_DATE:LX/0rS;

    invoke-direct {v4, v5, v6}, LX/3ea;-><init>(LX/3do;LX/0rS;)V

    invoke-virtual {v4}, LX/3ea;->a()Lcom/facebook/stickers/service/FetchStickerPacksParams;

    move-result-object v4

    .line 1397323
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 1397324
    const-string v5, "fetchStickerPacksParams"

    invoke-virtual {v6, v5, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1397325
    iget-object v4, v1, Lcom/facebook/stickers/client/StickerDownloadManager;->c:LX/0aG;

    const-string v5, "fetch_sticker_packs"

    sget-object v7, LX/1ME;->BY_EXCEPTION:LX/1ME;

    sget-object v8, Lcom/facebook/stickers/client/StickerDownloadManager;->a:Lcom/facebook/common/callercontext/CallerContext;

    const v9, 0x1e7fe65f

    invoke-static/range {v4 .. v9}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v4

    invoke-interface {v4}, LX/1MF;->start()LX/1ML;

    move-result-object v4

    .line 1397326
    new-instance v5, LX/8jN;

    invoke-direct {v5, v1, v2}, LX/8jN;-><init>(Lcom/facebook/stickers/client/StickerDownloadManager;Lcom/facebook/stickers/model/StickerPack;)V

    iget-object v6, v1, Lcom/facebook/stickers/client/StickerDownloadManager;->d:Ljava/util/concurrent/ExecutorService;

    invoke-static {v4, v5, v6}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method
