.class public LX/938;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1433487
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Z)LX/937;
    .locals 2

    .prologue
    .line 1433488
    invoke-static {}, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->newBuilder()LX/937;

    move-result-object v0

    .line 1433489
    iput-object p0, v0, LX/937;->k:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1433490
    move-object v0, v0

    .line 1433491
    iput-object p1, v0, LX/937;->l:Ljava/lang/String;

    .line 1433492
    move-object v0, v0

    .line 1433493
    sget-object v1, LX/93E;->VERB_PICKER:LX/93E;

    .line 1433494
    iput-object v1, v0, LX/937;->o:LX/93E;

    .line 1433495
    move-object v0, v0

    .line 1433496
    move-object v0, v0

    .line 1433497
    iput-object p2, v0, LX/937;->e:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1433498
    move-object v0, v0

    .line 1433499
    invoke-static {p3}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v1

    .line 1433500
    iput-object v1, v0, LX/937;->d:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1433501
    move-object v0, v0

    .line 1433502
    iput-boolean p4, v0, LX/937;->h:Z

    .line 1433503
    move-object v0, v0

    .line 1433504
    return-object v0
.end method
