.class public LX/9jf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/9je;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0SG;


# direct methods
.method public constructor <init>(LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1530310
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1530311
    iput-object p1, p0, LX/9jf;->a:LX/0SG;

    .line 1530312
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 7

    .prologue
    .line 1530315
    check-cast p1, LX/9je;

    .line 1530316
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 1530317
    iget-object v0, p1, LX/9je;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 1530318
    new-instance v2, LX/10w;

    invoke-direct {v2, v0, v1}, LX/10w;-><init>(J)V

    .line 1530319
    new-instance v3, LX/162;

    new-instance v5, LX/0mC;

    const/4 v6, 0x0

    invoke-direct {v5, v6}, LX/0mC;-><init>(Z)V

    invoke-direct {v3, v5}, LX/162;-><init>(LX/0mC;)V

    .line 1530320
    invoke-virtual {v3, v2}, LX/162;->a(LX/0lF;)LX/162;

    .line 1530321
    move-object v0, v3

    .line 1530322
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "page_ids"

    invoke-virtual {v0}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1530323
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "flag"

    iget-object v2, p1, LX/9je;->b:LX/9jK;

    iget-object v2, v2, LX/9jK;->value:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1530324
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "value"

    const-string v2, "1"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1530325
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "call_id"

    iget-object v2, p0, LX/9jf;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1530326
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "JSON"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1530327
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "entry_point"

    iget-object v2, p1, LX/9je;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1530328
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "endpoint"

    iget-object v2, p1, LX/9je;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1530329
    new-instance v0, LX/14N;

    const-string v1, "FlagPlace"

    const-string v2, "GET"

    const-string v3, "method/places.setFlag"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1530313
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1530314
    const/4 v0, 0x0

    return-object v0
.end method
