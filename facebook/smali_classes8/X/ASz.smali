.class public final LX/ASz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3IA;
.implements LX/3IB;


# instance fields
.field public final synthetic a:LX/AT1;

.field private b:Z


# direct methods
.method public constructor <init>(LX/AT1;)V
    .locals 1

    .prologue
    .line 1674746
    iput-object p1, p0, LX/ASz;->a:LX/AT1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1674747
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/ASz;->b:Z

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1674748
    return-void
.end method

.method public final a(FF)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1674733
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget-boolean v0, p0, LX/ASz;->b:Z

    if-nez v0, :cond_0

    .line 1674734
    iget-object v0, p0, LX/ASz;->a:LX/AT1;

    invoke-virtual {v0}, LX/AT1;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 1674735
    iput-boolean v2, p0, LX/ASz;->b:Z

    .line 1674736
    :cond_0
    iget-object v0, p0, LX/ASz;->a:LX/AT1;

    iget-object v0, v0, LX/AT1;->f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v0, p1, p2}, LX/2qW;->c(FF)V

    .line 1674737
    iget-object v0, p0, LX/ASz;->a:LX/AT1;

    iget-boolean v0, v0, LX/AT1;->C:Z

    if-nez v0, :cond_1

    .line 1674738
    iget-object v0, p0, LX/ASz;->a:LX/AT1;

    .line 1674739
    iput-boolean v2, v0, LX/AT1;->C:Z

    .line 1674740
    iget-object v0, p0, LX/ASz;->a:LX/AT1;

    const/4 v2, 0x0

    .line 1674741
    iget-object v1, v0, LX/AT1;->j:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 1674742
    iget-object v1, v0, LX/AT1;->h:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setClickable(Z)V

    .line 1674743
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/AT1;->a(LX/AT1;FF)V

    .line 1674744
    iget-object v1, v0, LX/AT1;->w:LX/3IP;

    iget v2, v0, LX/AT1;->G:F

    const p0, 0x3f666666    # 0.9f

    div-float/2addr v2, p0

    const/16 p0, 0xc8

    invoke-virtual {v1, v2, p0, v0}, LX/3IP;->a(FILX/3II;)V

    .line 1674745
    :cond_1
    return-void
.end method

.method public final a(F)Z
    .locals 1

    .prologue
    .line 1674749
    iget-object v0, p0, LX/ASz;->a:LX/AT1;

    iget-object v0, v0, LX/AT1;->f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v0, p1}, LX/2qW;->a(F)V

    .line 1674750
    const/4 v0, 0x1

    return v0
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 1674718
    iget-object v0, p0, LX/ASz;->a:LX/AT1;

    iget-boolean v0, v0, LX/AT1;->B:Z

    if-nez v0, :cond_0

    .line 1674719
    iget-object v0, p0, LX/ASz;->a:LX/AT1;

    iget-object v0, v0, LX/AT1;->a:LX/1xG;

    iget-object v1, p0, LX/ASz;->a:LX/AT1;

    iget-object v1, v1, LX/AT1;->d:Ljava/lang/String;

    sget-object v2, LX/7Dj;->COMPOSER:LX/7Dj;

    .line 1674720
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "spherical_photo_initial_view_modified"

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1674721
    invoke-static {v0, v3, v1, v2}, LX/1xG;->b(LX/1xG;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/7Dj;)V

    .line 1674722
    :cond_0
    iget-object v0, p0, LX/ASz;->a:LX/AT1;

    const/4 v1, 0x1

    .line 1674723
    iput-boolean v1, v0, LX/AT1;->B:Z

    .line 1674724
    iget-object v0, p0, LX/ASz;->a:LX/AT1;

    iget-object v0, v0, LX/AT1;->a:LX/1xG;

    iget-object v1, p0, LX/ASz;->a:LX/AT1;

    iget-object v1, v1, LX/AT1;->d:Ljava/lang/String;

    sget-object v2, LX/7Dj;->COMPOSER:LX/7Dj;

    .line 1674725
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "spherical_photo_drag_start"

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1674726
    invoke-static {v0, v3, v1, v2}, LX/1xG;->b(LX/1xG;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/7Dj;)V

    .line 1674727
    iget-object v0, p0, LX/ASz;->a:LX/AT1;

    iget-object v0, v0, LX/AT1;->f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v0}, LX/2qW;->f()V

    .line 1674728
    iget-object v0, p0, LX/ASz;->a:LX/AT1;

    iget-object v0, v0, LX/AT1;->z:LX/ASy;

    invoke-virtual {v0}, LX/ASy;->cancel()V

    .line 1674729
    iget-object v0, p0, LX/ASz;->a:LX/AT1;

    iget-object v0, v0, LX/AT1;->v:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/ASz;->a:LX/AT1;

    iget-object v0, v0, LX/AT1;->v:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1674730
    iget-object v0, p0, LX/ASz;->a:LX/AT1;

    iget-object v0, v0, LX/AT1;->v:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 1674731
    iget-object v0, p0, LX/ASz;->a:LX/AT1;

    iget-object v0, v0, LX/AT1;->m:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1674732
    :cond_1
    return-void
.end method

.method public final b(FF)V
    .locals 2

    .prologue
    .line 1674714
    iget-object v0, p0, LX/ASz;->a:LX/AT1;

    iget-object v0, v0, LX/AT1;->f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LX/2qW;->d(FF)V

    .line 1674715
    iget-object v0, p0, LX/ASz;->a:LX/AT1;

    iget-object v0, v0, LX/AT1;->z:LX/ASy;

    invoke-virtual {v0}, LX/ASy;->start()Landroid/os/CountDownTimer;

    .line 1674716
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/ASz;->b:Z

    .line 1674717
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1674711
    const/4 v0, 0x1

    return v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1674712
    iget-object v0, p0, LX/ASz;->a:LX/AT1;

    iget-object v0, v0, LX/AT1;->f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v0}, LX/2qW;->g()V

    .line 1674713
    return-void
.end method
