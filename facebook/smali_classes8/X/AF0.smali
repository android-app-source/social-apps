.class public LX/AF0;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0fO;

.field public final b:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "LX/AFW;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "LX/7h0;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0fO;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1647192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1647193
    new-instance v0, LX/AEy;

    invoke-direct {v0, p0}, LX/AEy;-><init>(LX/AF0;)V

    iput-object v0, p0, LX/AF0;->b:Ljava/util/Comparator;

    .line 1647194
    new-instance v0, LX/AEz;

    invoke-direct {v0, p0}, LX/AEz;-><init>(LX/AF0;)V

    iput-object v0, p0, LX/AF0;->c:Ljava/util/Comparator;

    .line 1647195
    iput-object p1, p0, LX/AF0;->a:LX/0fO;

    .line 1647196
    return-void
.end method

.method public static a(LX/AF0;Ljava/util/List;)LX/AFW;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/7h0;",
            ">;)",
            "LX/AFW;"
        }
    .end annotation

    .prologue
    .line 1647197
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1647198
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1647199
    invoke-static {p0, p1, v0, v1}, LX/AF0;->a(LX/AF0;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 1647200
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1647201
    iget-object v3, p0, LX/AF0;->c:Ljava/util/Comparator;

    invoke-static {v2, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1647202
    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/7h0;

    move-object v2, v3

    .line 1647203
    new-instance v3, LX/7gx;

    invoke-direct {v3, v2}, LX/7gx;-><init>(LX/7h0;)V

    .line 1647204
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 1647205
    const/4 v9, 0x0

    .line 1647206
    invoke-interface {v1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/7h0;

    .line 1647207
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    .line 1647208
    iput v6, v3, LX/7gx;->j:I

    .line 1647209
    move-object v6, v3

    .line 1647210
    iput-boolean v9, v6, LX/7gx;->l:Z

    .line 1647211
    move-object v6, v6

    .line 1647212
    const-wide/16 v7, -0x1

    .line 1647213
    iput-wide v7, v6, LX/7gx;->m:J

    .line 1647214
    move-object v6, v6

    .line 1647215
    iget-object v7, v2, LX/7h0;->k:Ljava/lang/String;

    move-object v7, v7

    .line 1647216
    iput-object v7, v6, LX/7gx;->k:Ljava/lang/String;

    .line 1647217
    move-object v6, v6

    .line 1647218
    iget-object v7, v5, LX/7h0;->f:Landroid/net/Uri;

    move-object v7, v7

    .line 1647219
    iput-object v7, v6, LX/7gx;->f:Landroid/net/Uri;

    .line 1647220
    move-object v6, v6

    .line 1647221
    iget-object v7, v5, LX/7h0;->e:Landroid/net/Uri;

    move-object v7, v7

    .line 1647222
    iput-object v7, v6, LX/7gx;->e:Landroid/net/Uri;

    .line 1647223
    move-object v6, v6

    .line 1647224
    iget-object v7, v5, LX/7h0;->g:Landroid/net/Uri;

    move-object v7, v7

    .line 1647225
    iput-object v7, v6, LX/7gx;->g:Landroid/net/Uri;

    .line 1647226
    move-object v6, v6

    .line 1647227
    iget-object v7, v5, LX/7h0;->b:LX/7gy;

    move-object v5, v7

    .line 1647228
    iput-object v5, v6, LX/7gx;->n:LX/7gy;

    .line 1647229
    move-object v5, v6

    .line 1647230
    iput-boolean v9, v5, LX/7gx;->h:Z

    .line 1647231
    move-object v5, v5

    .line 1647232
    invoke-virtual {v5}, LX/7gx;->a()LX/7h0;

    move-result-object v5

    .line 1647233
    new-instance v6, LX/AFV;

    invoke-direct {v6}, LX/AFV;-><init>()V

    .line 1647234
    iput-object v5, v6, LX/AFV;->a:LX/7h0;

    .line 1647235
    move-object v6, v6

    .line 1647236
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/7h0;

    .line 1647237
    invoke-static {v5}, LX/AF0;->a(LX/7h0;)Lcom/facebook/audience/model/MinimalReplyThread;

    move-result-object v5

    invoke-virtual {v6, v5}, LX/AFV;->a(Lcom/facebook/audience/model/MinimalReplyThread;)LX/AFV;

    goto :goto_0

    .line 1647238
    :cond_0
    iget-object v5, p0, LX/AF0;->a:LX/0fO;

    invoke-virtual {v5}, LX/0fO;->r()Z

    move-result v5

    if-nez v5, :cond_1

    .line 1647239
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/7h0;

    .line 1647240
    invoke-static {v5}, LX/AF0;->a(LX/7h0;)Lcom/facebook/audience/model/MinimalReplyThread;

    move-result-object v5

    invoke-virtual {v6, v5}, LX/AFV;->a(Lcom/facebook/audience/model/MinimalReplyThread;)LX/AFV;

    goto :goto_1

    .line 1647241
    :cond_1
    invoke-virtual {v6}, LX/AFV;->a()LX/AFW;

    move-result-object v5

    move-object v0, v5

    .line 1647242
    :goto_2
    return-object v0

    .line 1647243
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    .line 1647244
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7h0;

    .line 1647245
    iget-boolean v4, v1, LX/7h0;->l:Z

    move v4, v4

    .line 1647246
    if-eqz v4, :cond_3

    .line 1647247
    :goto_3
    move-object v1, v1

    .line 1647248
    if-nez v1, :cond_4

    .line 1647249
    new-instance v0, LX/AFV;

    invoke-direct {v0}, LX/AFV;-><init>()V

    invoke-virtual {v3}, LX/7gx;->a()LX/7h0;

    move-result-object v1

    .line 1647250
    iput-object v1, v0, LX/AFV;->a:LX/7h0;

    .line 1647251
    move-object v0, v0

    .line 1647252
    invoke-virtual {v0}, LX/AFV;->a()LX/AFW;

    move-result-object v0

    goto :goto_2

    .line 1647253
    :cond_4
    const/4 v9, 0x1

    .line 1647254
    iput-boolean v9, v3, LX/7gx;->l:Z

    .line 1647255
    move-object v5, v3

    .line 1647256
    iget-wide v10, v1, LX/7h0;->m:J

    move-wide v7, v10

    .line 1647257
    iput-wide v7, v5, LX/7gx;->m:J

    .line 1647258
    move-object v5, v5

    .line 1647259
    iget-object v6, v1, LX/7h0;->k:Ljava/lang/String;

    move-object v6, v6

    .line 1647260
    iput-object v6, v5, LX/7gx;->k:Ljava/lang/String;

    .line 1647261
    move-object v5, v5

    .line 1647262
    iget-object v6, v1, LX/7h0;->b:LX/7gy;

    move-object v6, v6

    .line 1647263
    iput-object v6, v5, LX/7gx;->n:LX/7gy;

    .line 1647264
    move-object v5, v5

    .line 1647265
    iget-object v6, v1, LX/7h0;->f:Landroid/net/Uri;

    move-object v6, v6

    .line 1647266
    iput-object v6, v5, LX/7gx;->f:Landroid/net/Uri;

    .line 1647267
    move-object v5, v5

    .line 1647268
    iget-object v6, v1, LX/7h0;->e:Landroid/net/Uri;

    move-object v6, v6

    .line 1647269
    iput-object v6, v5, LX/7gx;->e:Landroid/net/Uri;

    .line 1647270
    move-object v5, v5

    .line 1647271
    iget-object v6, v1, LX/7h0;->g:Landroid/net/Uri;

    move-object v6, v6

    .line 1647272
    iput-object v6, v5, LX/7gx;->g:Landroid/net/Uri;

    .line 1647273
    move-object v5, v5

    .line 1647274
    iput-boolean v9, v5, LX/7gx;->h:Z

    .line 1647275
    move-object v5, v5

    .line 1647276
    const/4 v6, 0x0

    .line 1647277
    iput v6, v5, LX/7gx;->j:I

    .line 1647278
    move-object v5, v5

    .line 1647279
    invoke-virtual {v5}, LX/7gx;->a()LX/7h0;

    move-result-object v5

    .line 1647280
    new-instance v6, LX/AFV;

    invoke-direct {v6}, LX/AFV;-><init>()V

    .line 1647281
    iput-object v5, v6, LX/AFV;->a:LX/7h0;

    .line 1647282
    move-object v6, v6

    .line 1647283
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_5
    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/7h0;

    .line 1647284
    iget-boolean v8, v5, LX/7h0;->l:Z

    move v8, v8

    .line 1647285
    if-eqz v8, :cond_5

    .line 1647286
    invoke-static {v5}, LX/AF0;->a(LX/7h0;)Lcom/facebook/audience/model/MinimalReplyThread;

    move-result-object v5

    invoke-virtual {v6, v5}, LX/AFV;->a(Lcom/facebook/audience/model/MinimalReplyThread;)LX/AFV;

    goto :goto_4

    .line 1647287
    :cond_6
    invoke-virtual {v6}, LX/AFV;->a()LX/AFW;

    move-result-object v5

    move-object v0, v5

    .line 1647288
    goto/16 :goto_2

    .line 1647289
    :cond_7
    new-instance v0, LX/AFV;

    invoke-direct {v0}, LX/AFV;-><init>()V

    invoke-virtual {v3}, LX/7gx;->a()LX/7h0;

    move-result-object v1

    .line 1647290
    iput-object v1, v0, LX/AFV;->a:LX/7h0;

    .line 1647291
    move-object v0, v0

    .line 1647292
    invoke-virtual {v0}, LX/AFV;->a()LX/AFW;

    move-result-object v0

    goto/16 :goto_2

    :cond_8
    const/4 v1, 0x0

    goto/16 :goto_3
.end method

.method public static a(LX/7h0;)Lcom/facebook/audience/model/MinimalReplyThread;
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1647293
    invoke-static {}, Lcom/facebook/audience/model/MinimalReplyThread;->newBuilder()LX/7gm;

    move-result-object v0

    .line 1647294
    iget-object v2, p0, LX/7h0;->i:Ljava/lang/String;

    move-object v2, v2

    .line 1647295
    iput-object v2, v0, LX/7gm;->a:Ljava/lang/String;

    .line 1647296
    move-object v0, v0

    .line 1647297
    iget-object v2, p0, LX/7h0;->e:Landroid/net/Uri;

    move-object v2, v2

    .line 1647298
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1647299
    iput-object v2, v0, LX/7gm;->d:Ljava/lang/String;

    .line 1647300
    move-object v0, v0

    .line 1647301
    iget-object v2, p0, LX/7h0;->f:Landroid/net/Uri;

    move-object v2, v2

    .line 1647302
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1647303
    iput-object v2, v0, LX/7gm;->c:Ljava/lang/String;

    .line 1647304
    move-object v2, v0

    .line 1647305
    iget-object v0, p0, LX/7h0;->g:Landroid/net/Uri;

    move-object v0, v0

    .line 1647306
    if-eqz v0, :cond_1

    move v0, v1

    .line 1647307
    :goto_0
    if-eqz v0, :cond_0

    .line 1647308
    iput-boolean v1, v2, LX/7gm;->b:Z

    .line 1647309
    move-object v0, v2

    .line 1647310
    iget-object v1, p0, LX/7h0;->g:Landroid/net/Uri;

    move-object v1, v1

    .line 1647311
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1647312
    iput-object v1, v0, LX/7gm;->e:Ljava/lang/String;

    .line 1647313
    :cond_0
    invoke-virtual {v2}, LX/7gm;->a()Lcom/facebook/audience/model/MinimalReplyThread;

    move-result-object v0

    return-object v0

    .line 1647314
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(LX/AF0;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/7h0;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/7h0;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/7h0;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1647315
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7h0;

    .line 1647316
    iget-object v2, v0, LX/7h0;->a:LX/7gz;

    sget-object p1, LX/7gz;->SENDING:LX/7gz;

    if-eq v2, p1, :cond_1

    iget-object v2, v0, LX/7h0;->a:LX/7gz;

    sget-object p1, LX/7gz;->FAILED:LX/7gz;

    if-ne v2, p1, :cond_4

    :cond_1
    const/4 v2, 0x1

    :goto_1
    move v2, v2

    .line 1647317
    if-nez v2, :cond_0

    .line 1647318
    iget-boolean v2, v0, LX/7h0;->h:Z

    move v2, v2

    .line 1647319
    if-eqz v2, :cond_2

    .line 1647320
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1647321
    :cond_2
    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1647322
    :cond_3
    iget-object v0, p0, LX/AF0;->c:Ljava/util/Comparator;

    invoke-static {p2, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1647323
    iget-object v0, p0, LX/AF0;->c:Ljava/util/Comparator;

    invoke-static {p3, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1647324
    return-void

    :cond_4
    const/4 v2, 0x0

    goto :goto_1
.end method
