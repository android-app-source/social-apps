.class public final LX/9rL;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 1552748
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_c

    .line 1552749
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1552750
    :goto_0
    return v1

    .line 1552751
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1552752
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_b

    .line 1552753
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1552754
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1552755
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_1

    if-eqz v9, :cond_1

    .line 1552756
    const-string v10, "__type__"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_2

    const-string v10, "__typename"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 1552757
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v8

    goto :goto_1

    .line 1552758
    :cond_3
    const-string v10, "action"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 1552759
    invoke-static {p0, p1}, LX/9tj;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1552760
    :cond_4
    const-string v10, "header_style"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 1552761
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    goto :goto_1

    .line 1552762
    :cond_5
    const-string v10, "icon_image"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 1552763
    invoke-static {p0, p1}, LX/5tK;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1552764
    :cond_6
    const-string v10, "message"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 1552765
    invoke-static {p0, p1}, LX/5Q6;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1552766
    :cond_7
    const-string v10, "profiles"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 1552767
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1552768
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->START_ARRAY:LX/15z;

    if-ne v9, v10, :cond_8

    .line 1552769
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_ARRAY:LX/15z;

    if-eq v9, v10, :cond_8

    .line 1552770
    invoke-static {p0, p1}, LX/5tH;->a(LX/15w;LX/186;)I

    move-result v9

    .line 1552771
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1552772
    :cond_8
    invoke-static {v3, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v3

    move v3, v3

    .line 1552773
    goto/16 :goto_1

    .line 1552774
    :cond_9
    const-string v10, "secondary_actions"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 1552775
    invoke-static {p0, p1}, LX/9tj;->b(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 1552776
    :cond_a
    const-string v10, "sub_message"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1552777
    invoke-static {p0, p1}, LX/5Q6;->a(LX/15w;LX/186;)I

    move-result v0

    goto/16 :goto_1

    .line 1552778
    :cond_b
    const/16 v9, 0x8

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1552779
    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 1552780
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 1552781
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 1552782
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1552783
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1552784
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1552785
    const/4 v1, 0x6

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1552786
    const/4 v1, 0x7

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1552787
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_c
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x0

    .line 1552788
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1552789
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1552790
    if-eqz v0, :cond_0

    .line 1552791
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1552792
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1552793
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1552794
    if-eqz v0, :cond_1

    .line 1552795
    const-string v1, "action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1552796
    invoke-static {p0, v0, p2, p3}, LX/9tj;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1552797
    :cond_1
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1552798
    if-eqz v0, :cond_2

    .line 1552799
    const-string v0, "header_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1552800
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1552801
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1552802
    if-eqz v0, :cond_3

    .line 1552803
    const-string v1, "icon_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1552804
    invoke-static {p0, v0, p2}, LX/5tK;->a(LX/15i;ILX/0nX;)V

    .line 1552805
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1552806
    if-eqz v0, :cond_4

    .line 1552807
    const-string v1, "message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1552808
    invoke-static {p0, v0, p2, p3}, LX/5Q6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1552809
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1552810
    if-eqz v0, :cond_6

    .line 1552811
    const-string v1, "profiles"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1552812
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1552813
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_5

    .line 1552814
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/5tH;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1552815
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1552816
    :cond_5
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1552817
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1552818
    if-eqz v0, :cond_7

    .line 1552819
    const-string v1, "secondary_actions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1552820
    invoke-static {p0, v0, p2, p3}, LX/9tj;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1552821
    :cond_7
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1552822
    if-eqz v0, :cond_8

    .line 1552823
    const-string v1, "sub_message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1552824
    invoke-static {p0, v0, p2, p3}, LX/5Q6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1552825
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1552826
    return-void
.end method
