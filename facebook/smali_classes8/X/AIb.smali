.class public final LX/AIb;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 19

    .prologue
    .line 1659951
    const/4 v13, 0x0

    .line 1659952
    const/4 v12, 0x0

    .line 1659953
    const/4 v11, 0x0

    .line 1659954
    const/4 v10, 0x0

    .line 1659955
    const/4 v9, 0x0

    .line 1659956
    const/4 v8, 0x0

    .line 1659957
    const/4 v5, 0x0

    .line 1659958
    const-wide/16 v6, 0x0

    .line 1659959
    const/4 v4, 0x0

    .line 1659960
    const/4 v3, 0x0

    .line 1659961
    const/4 v2, 0x0

    .line 1659962
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->START_OBJECT:LX/15z;

    if-eq v14, v15, :cond_d

    .line 1659963
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1659964
    const/4 v2, 0x0

    .line 1659965
    :goto_0
    return v2

    .line 1659966
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v15, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v15, :cond_a

    .line 1659967
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1659968
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1659969
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v15

    sget-object v16, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_0

    if-eqz v6, :cond_0

    .line 1659970
    const-string v15, "all_direct_users"

    invoke-virtual {v6, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 1659971
    invoke-static/range {p0 .. p1}, LX/AIW;->a(LX/15w;LX/186;)I

    move-result v6

    move v14, v6

    goto :goto_1

    .line 1659972
    :cond_1
    const-string v15, "direct_reactions"

    invoke-virtual {v6, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 1659973
    invoke-static/range {p0 .. p1}, LX/AIY;->a(LX/15w;LX/186;)I

    move-result v6

    move v13, v6

    goto :goto_1

    .line 1659974
    :cond_2
    const-string v15, "id"

    invoke-virtual {v6, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 1659975
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    move v12, v6

    goto :goto_1

    .line 1659976
    :cond_3
    const-string v15, "is_seen_by_viewer"

    invoke-virtual {v6, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 1659977
    const/4 v3, 0x1

    .line 1659978
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v11, v6

    goto :goto_1

    .line 1659979
    :cond_4
    const-string v15, "latest_message"

    invoke-virtual {v6, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 1659980
    invoke-static/range {p0 .. p1}, LX/AIU;->a(LX/15w;LX/186;)I

    move-result v6

    move v10, v6

    goto :goto_1

    .line 1659981
    :cond_5
    const-string v15, "root_message"

    invoke-virtual {v6, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 1659982
    invoke-static/range {p0 .. p1}, LX/AIU;->a(LX/15w;LX/186;)I

    move-result v6

    move v9, v6

    goto :goto_1

    .line 1659983
    :cond_6
    const-string v15, "seen_direct_users"

    invoke-virtual {v6, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 1659984
    invoke-static/range {p0 .. p1}, LX/AIa;->a(LX/15w;LX/186;)I

    move-result v6

    move v7, v6

    goto/16 :goto_1

    .line 1659985
    :cond_7
    const-string v15, "time"

    invoke-virtual {v6, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 1659986
    const/4 v2, 0x1

    .line 1659987
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    goto/16 :goto_1

    .line 1659988
    :cond_8
    const-string v15, "url"

    invoke-virtual {v6, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 1659989
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    move v8, v6

    goto/16 :goto_1

    .line 1659990
    :cond_9
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1659991
    :cond_a
    const/16 v6, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 1659992
    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v14}, LX/186;->b(II)V

    .line 1659993
    const/4 v6, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v13}, LX/186;->b(II)V

    .line 1659994
    const/4 v6, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v12}, LX/186;->b(II)V

    .line 1659995
    if-eqz v3, :cond_b

    .line 1659996
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->a(IZ)V

    .line 1659997
    :cond_b
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 1659998
    const/4 v3, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 1659999
    const/4 v3, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 1660000
    if-eqz v2, :cond_c

    .line 1660001
    const/4 v3, 0x7

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1660002
    :cond_c
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1660003
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_d
    move v14, v13

    move v13, v12

    move v12, v11

    move v11, v10

    move v10, v9

    move v9, v8

    move v8, v4

    move-wide/from16 v17, v6

    move v7, v5

    move-wide/from16 v4, v17

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1660004
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1660005
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1660006
    if-eqz v0, :cond_0

    .line 1660007
    const-string v1, "all_direct_users"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1660008
    invoke-static {p0, v0, p2, p3}, LX/AIW;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1660009
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1660010
    if-eqz v0, :cond_1

    .line 1660011
    const-string v1, "direct_reactions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1660012
    invoke-static {p0, v0, p2, p3}, LX/AIY;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1660013
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1660014
    if-eqz v0, :cond_2

    .line 1660015
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1660016
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1660017
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1660018
    if-eqz v0, :cond_3

    .line 1660019
    const-string v1, "is_seen_by_viewer"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1660020
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1660021
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1660022
    if-eqz v0, :cond_4

    .line 1660023
    const-string v1, "latest_message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1660024
    invoke-static {p0, v0, p2, p3}, LX/AIU;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1660025
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1660026
    if-eqz v0, :cond_5

    .line 1660027
    const-string v1, "root_message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1660028
    invoke-static {p0, v0, p2, p3}, LX/AIU;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1660029
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1660030
    if-eqz v0, :cond_6

    .line 1660031
    const-string v1, "seen_direct_users"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1660032
    invoke-static {p0, v0, p2, p3}, LX/AIa;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1660033
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1660034
    cmp-long v2, v0, v2

    if-eqz v2, :cond_7

    .line 1660035
    const-string v2, "time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1660036
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1660037
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1660038
    if-eqz v0, :cond_8

    .line 1660039
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1660040
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1660041
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1660042
    return-void
.end method
