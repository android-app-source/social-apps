.class public final LX/9wp;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;)LX/15i;
    .locals 15

    .prologue
    .line 1588402
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1588403
    const/4 v11, 0x0

    .line 1588404
    const/4 v10, 0x0

    .line 1588405
    const/4 v9, 0x0

    .line 1588406
    const/4 v8, 0x0

    .line 1588407
    const/4 v7, 0x0

    .line 1588408
    const/4 v6, 0x0

    .line 1588409
    const/4 v3, 0x0

    .line 1588410
    const-wide/16 v4, 0x0

    .line 1588411
    const/4 v2, 0x0

    .line 1588412
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->START_OBJECT:LX/15z;

    if-eq v12, v13, :cond_1

    .line 1588413
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1588414
    const/4 v2, 0x0

    .line 1588415
    :goto_0
    move v1, v2

    .line 1588416
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1588417
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    return-object v0

    .line 1588418
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1588419
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->END_OBJECT:LX/15z;

    if-eq v12, v13, :cond_9

    .line 1588420
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v12

    .line 1588421
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1588422
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v13, v14, :cond_1

    if-eqz v12, :cond_1

    .line 1588423
    const-string v13, "action"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 1588424
    invoke-static {p0, v0}, LX/9tj;->a(LX/15w;LX/186;)I

    move-result v11

    goto :goto_1

    .line 1588425
    :cond_2
    const-string v13, "background_color"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 1588426
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 1588427
    :cond_3
    const-string v13, "customer_data"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 1588428
    invoke-static {p0, v0}, LX/9wo;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 1588429
    :cond_4
    const-string v13, "image_block_image"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 1588430
    invoke-static {p0, v0}, LX/5tK;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1588431
    :cond_5
    const-string v13, "message_snippet"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 1588432
    invoke-static {p0, v0}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1588433
    :cond_6
    const-string v13, "other_user_name"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_7

    .line 1588434
    invoke-static {p0, v0}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1588435
    :cond_7
    const-string v13, "tertiary_message"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_8

    .line 1588436
    invoke-static {p0, v0}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1588437
    :cond_8
    const-string v13, "timestamp"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 1588438
    const/4 v2, 0x1

    .line 1588439
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v4

    goto/16 :goto_1

    .line 1588440
    :cond_9
    const/16 v12, 0x8

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 1588441
    const/4 v12, 0x0

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 1588442
    const/4 v11, 0x1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 1588443
    const/4 v10, 0x2

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 1588444
    const/4 v9, 0x3

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 1588445
    const/4 v8, 0x4

    invoke-virtual {v0, v8, v7}, LX/186;->b(II)V

    .line 1588446
    const/4 v7, 0x5

    invoke-virtual {v0, v7, v6}, LX/186;->b(II)V

    .line 1588447
    const/4 v6, 0x6

    invoke-virtual {v0, v6, v3}, LX/186;->b(II)V

    .line 1588448
    if-eqz v2, :cond_a

    .line 1588449
    const/4 v3, 0x7

    const-wide/16 v6, 0x0

    move-object v2, v0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1588450
    :cond_a
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method
