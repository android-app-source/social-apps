.class public final LX/9fX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9fH;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/editgallery/TextEditController;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/editgallery/TextEditController;)V
    .locals 0

    .prologue
    .line 1522081
    iput-object p1, p0, LX/9fX;->a:Lcom/facebook/photos/editgallery/TextEditController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1522110
    iget-object v0, p0, LX/9fX;->a:Lcom/facebook/photos/editgallery/TextEditController;

    iget-object v1, p0, LX/9fX;->a:Lcom/facebook/photos/editgallery/TextEditController;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/TextEditController;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0813d7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/photos/editgallery/TextEditController;->a$redex0(Lcom/facebook/photos/editgallery/TextEditController;Ljava/lang/String;)V

    .line 1522111
    iget-object v0, p0, LX/9fX;->a:Lcom/facebook/photos/editgallery/TextEditController;

    invoke-static {v0}, Lcom/facebook/photos/editgallery/TextEditController;->r(Lcom/facebook/photos/editgallery/TextEditController;)V

    .line 1522112
    return-void
.end method

.method public final a(LX/362;)V
    .locals 2

    .prologue
    .line 1522106
    iget-object v0, p0, LX/9fX;->a:Lcom/facebook/photos/editgallery/TextEditController;

    const/4 v1, 0x1

    .line 1522107
    iput-boolean v1, v0, Lcom/facebook/photos/editgallery/TextEditController;->m:Z

    .line 1522108
    iget-object v0, p0, LX/9fX;->a:Lcom/facebook/photos/editgallery/TextEditController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/TextEditController;->w:LX/9f8;

    invoke-static {p1}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->c(LX/362;)LX/5i7;

    move-result-object v1

    invoke-interface {v0, v1}, LX/9f8;->a(LX/5i7;)V

    .line 1522109
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1522113
    iget-object v0, p0, LX/9fX;->a:Lcom/facebook/photos/editgallery/TextEditController;

    const/4 v1, 0x1

    .line 1522114
    iput-boolean v1, v0, Lcom/facebook/photos/editgallery/TextEditController;->m:Z

    .line 1522115
    iget-object v0, p0, LX/9fX;->a:Lcom/facebook/photos/editgallery/TextEditController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/TextEditController;->w:LX/9f8;

    iget-object v1, p0, LX/9fX;->a:Lcom/facebook/photos/editgallery/TextEditController;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/TextEditController;->o:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    invoke-virtual {v1}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->getSelectedItem()LX/362;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->c(LX/362;)LX/5i7;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/9f8;->a(Ljava/lang/String;LX/5i7;)V

    .line 1522116
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 1522090
    iget-object v0, p0, LX/9fX;->a:Lcom/facebook/photos/editgallery/TextEditController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/TextEditController;->o:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    invoke-virtual {v0}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->getSelectedItem()LX/362;

    move-result-object v0

    .line 1522091
    instance-of v1, v0, Lcom/facebook/photos/creativeediting/model/TextParams;

    if-nez v1, :cond_0

    .line 1522092
    :goto_0
    return-void

    .line 1522093
    :cond_0
    check-cast v0, Lcom/facebook/photos/creativeediting/model/TextParams;

    .line 1522094
    iget-object v1, p0, LX/9fX;->a:Lcom/facebook/photos/editgallery/TextEditController;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/TextEditController;->n:LX/9e1;

    invoke-virtual {v1, v0}, LX/9e1;->setTextParams(Lcom/facebook/photos/creativeediting/model/TextParams;)V

    .line 1522095
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/TextParams;->g()Ljava/lang/String;

    move-result-object v0

    .line 1522096
    if-eqz v0, :cond_1

    .line 1522097
    iget-object v1, p0, LX/9fX;->a:Lcom/facebook/photos/editgallery/TextEditController;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/TextEditController;->q:Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;

    .line 1522098
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1522099
    iget-object v2, v1, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->e:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1522100
    iget-object v2, v1, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->e:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1522101
    :cond_1
    iget-object v0, p0, LX/9fX;->a:Lcom/facebook/photos/editgallery/TextEditController;

    iget-object v1, p0, LX/9fX;->a:Lcom/facebook/photos/editgallery/TextEditController;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/TextEditController;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0813d7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/photos/editgallery/TextEditController;->a$redex0(Lcom/facebook/photos/editgallery/TextEditController;Ljava/lang/String;)V

    .line 1522102
    iget-object v0, p0, LX/9fX;->a:Lcom/facebook/photos/editgallery/TextEditController;

    invoke-static {v0}, Lcom/facebook/photos/editgallery/TextEditController;->r(Lcom/facebook/photos/editgallery/TextEditController;)V

    .line 1522103
    iget-object v0, p0, LX/9fX;->a:Lcom/facebook/photos/editgallery/TextEditController;

    const/4 v1, 0x1

    .line 1522104
    iput-boolean v1, v0, Lcom/facebook/photos/editgallery/TextEditController;->m:Z

    .line 1522105
    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1522086
    iget-object v0, p0, LX/9fX;->a:Lcom/facebook/photos/editgallery/TextEditController;

    const/4 v1, 0x1

    .line 1522087
    iput-boolean v1, v0, Lcom/facebook/photos/editgallery/TextEditController;->m:Z

    .line 1522088
    iget-object v0, p0, LX/9fX;->a:Lcom/facebook/photos/editgallery/TextEditController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/TextEditController;->w:LX/9f8;

    iget-object v1, p0, LX/9fX;->a:Lcom/facebook/photos/editgallery/TextEditController;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/TextEditController;->o:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    invoke-virtual {v1}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->getSelectedItem()LX/362;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->c(LX/362;)LX/5i7;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/9f8;->b(Ljava/lang/String;LX/5i7;)V

    .line 1522089
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1522082
    iget-object v0, p0, LX/9fX;->a:Lcom/facebook/photos/editgallery/TextEditController;

    const/4 v1, 0x1

    .line 1522083
    iput-boolean v1, v0, Lcom/facebook/photos/editgallery/TextEditController;->m:Z

    .line 1522084
    iget-object v0, p0, LX/9fX;->a:Lcom/facebook/photos/editgallery/TextEditController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/TextEditController;->w:LX/9f8;

    iget-object v1, p0, LX/9fX;->a:Lcom/facebook/photos/editgallery/TextEditController;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/TextEditController;->o:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    invoke-virtual {v1}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->getSelectedItem()LX/362;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->c(LX/362;)LX/5i7;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/9f8;->c(Ljava/lang/String;LX/5i7;)V

    .line 1522085
    return-void
.end method
