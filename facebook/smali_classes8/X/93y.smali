.class public LX/93y;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/93t;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1434353
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1434354
    return-void
.end method


# virtual methods
.method public final a(LX/93q;LX/93w;Lcom/facebook/graphql/model/GraphQLPrivacyOption;Ljava/lang/Object;)LX/93t;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData:",
            "Ljava/lang/Object;",
            ":",
            "LX/0it;",
            ":",
            "LX/0iq;",
            "DerivedData::",
            "LX/5Qu;",
            "Services:",
            "Ljava/lang/Object;",
            ":",
            "LX/0il",
            "<TModelData;>;:",
            "LX/0ik",
            "<TDerivedData;>;>(",
            "LX/93q;",
            "LX/93w;",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;",
            "TServices;)",
            "LX/93t",
            "<TModelData;TDerivedData;TServices;>;"
        }
    .end annotation

    .prologue
    .line 1434355
    new-instance v0, LX/93t;

    move-object v4, p4

    check-cast v4, LX/0il;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    invoke-static {p0}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(LX/0QB;)Lcom/facebook/privacy/PrivacyOperationsClient;

    move-result-object v7

    check-cast v7, Lcom/facebook/privacy/PrivacyOperationsClient;

    const/16 v1, 0xfc2

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v8}, LX/93t;-><init>(LX/93q;LX/93w;Lcom/facebook/graphql/model/GraphQLPrivacyOption;LX/0il;LX/03V;LX/1Ck;Lcom/facebook/privacy/PrivacyOperationsClient;LX/0Ot;)V

    .line 1434356
    return-object v0
.end method
