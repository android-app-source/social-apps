.class public final LX/8pm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel;",
        ">;",
        "LX/44w",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/graphql/model/GraphQLActor;",
        ">;",
        "Lcom/facebook/graphql/model/GraphQLPageInfo;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8pn;


# direct methods
.method public constructor <init>(LX/8pn;)V
    .locals 0

    .prologue
    .line 1406343
    iput-object p1, p0, LX/8pm;->a:LX/8pn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1406344
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1406345
    if-nez p1, :cond_0

    .line 1406346
    const/4 v0, 0x0

    .line 1406347
    :goto_0
    return-object v0

    .line 1406348
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1406349
    check-cast v0, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel;

    invoke-static {v0}, LX/6BQ;->a(Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 1406350
    iget-object v1, p0, LX/8pm;->a:LX/8pn;

    iget-object v1, v1, LX/8pn;->c:LX/1K9;

    new-instance v2, LX/82h;

    invoke-direct {v2, v0}, LX/82h;-><init>(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    invoke-virtual {v1, v2}, LX/1K9;->a(LX/1KJ;)V

    .line 1406351
    if-eqz v0, :cond_1

    invoke-static {v0}, LX/16z;->n(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {v0}, LX/16z;->n(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;->j()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1406352
    :cond_1
    const/4 v1, 0x0

    .line 1406353
    :goto_1
    move-object v0, v1

    .line 1406354
    goto :goto_0

    .line 1406355
    :cond_2
    invoke-static {v0}, LX/16z;->n(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v2

    .line 1406356
    new-instance v1, LX/44w;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;->j()LX/0Px;

    move-result-object p0

    invoke-static {v2}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    invoke-direct {v1, p0, v2}, LX/44w;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1
.end method
