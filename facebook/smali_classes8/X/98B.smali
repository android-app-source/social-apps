.class public final LX/98B;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 21

    .prologue
    .line 1445068
    const/16 v17, 0x0

    .line 1445069
    const/16 v16, 0x0

    .line 1445070
    const/4 v15, 0x0

    .line 1445071
    const/4 v14, 0x0

    .line 1445072
    const/4 v13, 0x0

    .line 1445073
    const/4 v12, 0x0

    .line 1445074
    const/4 v11, 0x0

    .line 1445075
    const/4 v10, 0x0

    .line 1445076
    const/4 v9, 0x0

    .line 1445077
    const/4 v8, 0x0

    .line 1445078
    const/4 v7, 0x0

    .line 1445079
    const/4 v6, 0x0

    .line 1445080
    const/4 v5, 0x0

    .line 1445081
    const/4 v4, 0x0

    .line 1445082
    const/4 v3, 0x0

    .line 1445083
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_1

    .line 1445084
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1445085
    const/4 v3, 0x0

    .line 1445086
    :goto_0
    return v3

    .line 1445087
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1445088
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_11

    .line 1445089
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v18

    .line 1445090
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1445091
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_1

    if-eqz v18, :cond_1

    .line 1445092
    const-string v19, "__type__"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_2

    const-string v19, "__typename"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_3

    .line 1445093
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v17

    goto :goto_1

    .line 1445094
    :cond_3
    const-string v19, "category"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_4

    .line 1445095
    invoke-static/range {p0 .. p1}, LX/98A;->a(LX/15w;LX/186;)I

    move-result v16

    goto :goto_1

    .line 1445096
    :cond_4
    const-string v19, "city"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_5

    .line 1445097
    invoke-static/range {p0 .. p1}, LX/989;->a(LX/15w;LX/186;)I

    move-result v15

    goto :goto_1

    .line 1445098
    :cond_5
    const-string v19, "fri"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_6

    .line 1445099
    invoke-static/range {p0 .. p1}, LX/98F;->a(LX/15w;LX/186;)I

    move-result v14

    goto :goto_1

    .line 1445100
    :cond_6
    const-string v19, "mon"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_7

    .line 1445101
    invoke-static/range {p0 .. p1}, LX/98G;->a(LX/15w;LX/186;)I

    move-result v13

    goto :goto_1

    .line 1445102
    :cond_7
    const-string v19, "parent_place"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_8

    .line 1445103
    invoke-static/range {p0 .. p1}, LX/98M;->a(LX/15w;LX/186;)I

    move-result v12

    goto :goto_1

    .line 1445104
    :cond_8
    const-string v19, "photo"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_9

    .line 1445105
    invoke-static/range {p0 .. p1}, LX/98N;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 1445106
    :cond_9
    const-string v19, "sat"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_a

    .line 1445107
    invoke-static/range {p0 .. p1}, LX/98H;->a(LX/15w;LX/186;)I

    move-result v10

    goto/16 :goto_1

    .line 1445108
    :cond_a
    const-string v19, "street"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_b

    .line 1445109
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto/16 :goto_1

    .line 1445110
    :cond_b
    const-string v19, "sun"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_c

    .line 1445111
    invoke-static/range {p0 .. p1}, LX/98I;->a(LX/15w;LX/186;)I

    move-result v8

    goto/16 :goto_1

    .line 1445112
    :cond_c
    const-string v19, "text"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_d

    .line 1445113
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto/16 :goto_1

    .line 1445114
    :cond_d
    const-string v19, "thu"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_e

    .line 1445115
    invoke-static/range {p0 .. p1}, LX/98J;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 1445116
    :cond_e
    const-string v19, "tue"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_f

    .line 1445117
    invoke-static/range {p0 .. p1}, LX/98K;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 1445118
    :cond_f
    const-string v19, "wed"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_10

    .line 1445119
    invoke-static/range {p0 .. p1}, LX/98L;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 1445120
    :cond_10
    const-string v19, "zip"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 1445121
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 1445122
    :cond_11
    const/16 v18, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1445123
    const/16 v18, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1445124
    const/16 v17, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1445125
    const/16 v16, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 1445126
    const/4 v15, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v14}, LX/186;->b(II)V

    .line 1445127
    const/4 v14, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 1445128
    const/4 v13, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 1445129
    const/4 v12, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 1445130
    const/4 v11, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 1445131
    const/16 v10, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 1445132
    const/16 v9, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 1445133
    const/16 v8, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v7}, LX/186;->b(II)V

    .line 1445134
    const/16 v7, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v6}, LX/186;->b(II)V

    .line 1445135
    const/16 v6, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v5}, LX/186;->b(II)V

    .line 1445136
    const/16 v5, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 1445137
    const/16 v4, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1445138
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1445139
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1445140
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1445141
    if-eqz v0, :cond_0

    .line 1445142
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1445143
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1445144
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1445145
    if-eqz v0, :cond_1

    .line 1445146
    const-string v1, "category"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1445147
    invoke-static {p0, v0, p2}, LX/98A;->a(LX/15i;ILX/0nX;)V

    .line 1445148
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1445149
    if-eqz v0, :cond_2

    .line 1445150
    const-string v1, "city"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1445151
    invoke-static {p0, v0, p2, p3}, LX/989;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1445152
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1445153
    if-eqz v0, :cond_3

    .line 1445154
    const-string v1, "fri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1445155
    invoke-static {p0, v0, p2, p3}, LX/98F;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1445156
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1445157
    if-eqz v0, :cond_4

    .line 1445158
    const-string v1, "mon"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1445159
    invoke-static {p0, v0, p2, p3}, LX/98G;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1445160
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1445161
    if-eqz v0, :cond_5

    .line 1445162
    const-string v1, "parent_place"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1445163
    invoke-static {p0, v0, p2}, LX/98M;->a(LX/15i;ILX/0nX;)V

    .line 1445164
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1445165
    if-eqz v0, :cond_6

    .line 1445166
    const-string v1, "photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1445167
    invoke-static {p0, v0, p2, p3}, LX/98N;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1445168
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1445169
    if-eqz v0, :cond_7

    .line 1445170
    const-string v1, "sat"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1445171
    invoke-static {p0, v0, p2, p3}, LX/98H;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1445172
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1445173
    if-eqz v0, :cond_8

    .line 1445174
    const-string v1, "street"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1445175
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1445176
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1445177
    if-eqz v0, :cond_9

    .line 1445178
    const-string v1, "sun"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1445179
    invoke-static {p0, v0, p2, p3}, LX/98I;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1445180
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1445181
    if-eqz v0, :cond_a

    .line 1445182
    const-string v1, "text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1445183
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1445184
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1445185
    if-eqz v0, :cond_b

    .line 1445186
    const-string v1, "thu"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1445187
    invoke-static {p0, v0, p2, p3}, LX/98J;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1445188
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1445189
    if-eqz v0, :cond_c

    .line 1445190
    const-string v1, "tue"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1445191
    invoke-static {p0, v0, p2, p3}, LX/98K;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1445192
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1445193
    if-eqz v0, :cond_d

    .line 1445194
    const-string v1, "wed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1445195
    invoke-static {p0, v0, p2, p3}, LX/98L;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1445196
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1445197
    if-eqz v0, :cond_e

    .line 1445198
    const-string v1, "zip"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1445199
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1445200
    :cond_e
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1445201
    return-void
.end method
