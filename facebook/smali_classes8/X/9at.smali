.class public LX/9at;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1514135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1514136
    iput-object p1, p0, LX/9at;->a:Landroid/content/Context;

    .line 1514137
    return-void
.end method

.method public static a(LX/0QB;)LX/9at;
    .locals 1

    .prologue
    .line 1514138
    invoke-static {p0}, LX/9at;->b(LX/0QB;)LX/9at;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/9at;
    .locals 2

    .prologue
    .line 1514139
    new-instance v1, LX/9at;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LX/9at;-><init>(Landroid/content/Context;)V

    .line 1514140
    return-object v1
.end method


# virtual methods
.method public final a(LX/9au;Lcom/facebook/auth/viewercontext/ViewerContext;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 1514141
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/9at;->a(LX/9au;Lcom/facebook/auth/viewercontext/ViewerContext;Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/9au;Lcom/facebook/auth/viewercontext/ViewerContext;Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Landroid/content/Intent;
    .locals 3
    .param p3    # Lcom/facebook/ipc/composer/intent/ComposerTargetData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1514142
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/9at;->a:Landroid/content/Context;

    const-class v2, Lcom/facebook/photos/albumcreator/AlbumCreatorActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1514143
    const-string v1, "source"

    invoke-virtual {p1}, LX/9au;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1514144
    if-eqz p2, :cond_0

    .line 1514145
    const-string v1, "com.facebook.orca.auth.OVERRIDDEN_VIEWER_CONTEXT"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1514146
    :cond_0
    if-eqz p3, :cond_1

    .line 1514147
    const-string v1, "extra_composer_target_data"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1514148
    :cond_1
    return-object v0
.end method
