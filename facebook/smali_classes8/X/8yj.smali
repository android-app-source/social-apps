.class public LX/8yj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1426394
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/1De;FILX/1dc;LX/1dc;LX/1dc;)LX/1Dg;
    .locals 12
    .param p1    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/1dc;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DRAWABLE:LX/32B;
        .end annotation
    .end param
    .param p4    # LX/1dc;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DRAWABLE:LX/32B;
        .end annotation
    .end param
    .param p5    # LX/1dc;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DRAWABLE:LX/32B;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "FI",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 1426395
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_1

    const/high16 v0, 0x40a00000    # 5.0f

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1426396
    if-nez p3, :cond_7

    .line 1426397
    invoke-static {p0}, LX/1ni;->a(LX/1De;)LX/1nm;

    move-result-object v0

    const v1, 0x7f021898

    invoke-virtual {v0, v1}, LX/1nm;->h(I)LX/1nm;

    move-result-object v0

    invoke-virtual {v0}, LX/1n6;->b()LX/1dc;

    move-result-object v2

    .line 1426398
    :goto_1
    if-nez p4, :cond_6

    .line 1426399
    invoke-static {p0}, LX/1ni;->a(LX/1De;)LX/1nm;

    move-result-object v0

    const v1, 0x7f021895

    invoke-virtual {v0, v1}, LX/1nm;->h(I)LX/1nm;

    move-result-object v0

    invoke-virtual {v0}, LX/1n6;->b()LX/1dc;

    move-result-object v1

    .line 1426400
    :goto_2
    if-nez p5, :cond_0

    .line 1426401
    invoke-static {p0}, LX/1ni;->a(LX/1De;)LX/1nm;

    move-result-object v0

    const v3, 0x7f021897

    invoke-virtual {v0, v3}, LX/1nm;->h(I)LX/1nm;

    move-result-object v0

    invoke-virtual {v0}, LX/1n6;->b()LX/1dc;

    move-result-object p5

    .line 1426402
    :cond_0
    invoke-static {p0}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const/4 v3, 0x2

    invoke-interface {v0, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    .line 1426403
    float-to-double v6, p1

    const-wide/high16 v8, 0x3fd0000000000000L    # 0.25

    add-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->floor(D)D

    move-result-wide v6

    .line 1426404
    float-to-double v8, p1

    const-wide/high16 v10, 0x3fd0000000000000L    # 0.25

    sub-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    .line 1426405
    const/4 v0, 0x0

    move v3, v0

    :goto_3
    const/4 v0, 0x5

    if-ge v3, v0, :cond_5

    .line 1426406
    int-to-double v10, v3

    cmpg-double v0, v10, v6

    if-gez v0, :cond_2

    move-object v0, v1

    .line 1426407
    :goto_4
    invoke-static {p0}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v5

    invoke-virtual {v5, v0}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const/4 v10, 0x0

    if-lez v3, :cond_4

    move v0, p2

    :goto_5
    invoke-interface {v5, v10, v0}, LX/1Di;->h(II)LX/1Di;

    move-result-object v0

    invoke-interface {v4, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 1426408
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    .line 1426409
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1426410
    :cond_2
    int-to-double v10, v3

    cmpl-double v0, v10, v8

    if-ltz v0, :cond_3

    move-object v0, v2

    .line 1426411
    goto :goto_4

    :cond_3
    move-object/from16 v0, p5

    .line 1426412
    goto :goto_4

    .line 1426413
    :cond_4
    const/4 v0, 0x0

    goto :goto_5

    .line 1426414
    :cond_5
    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0

    :cond_6
    move-object/from16 v1, p4

    goto :goto_2

    :cond_7
    move-object v2, p3

    goto :goto_1
.end method
