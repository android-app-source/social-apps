.class public LX/9Iy;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/9Ix;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/lang/CharSequence;

.field private final c:I


# direct methods
.method public constructor <init>(Ljava/util/ArrayList;Ljava/lang/CharSequence;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "LX/9Ix;",
            ">;",
            "Ljava/lang/CharSequence;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 1463877
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1463878
    iput-object p1, p0, LX/9Iy;->a:Ljava/util/ArrayList;

    .line 1463879
    iput-object p2, p0, LX/9Iy;->b:Ljava/lang/CharSequence;

    .line 1463880
    iput p3, p0, LX/9Iy;->c:I

    .line 1463881
    return-void
.end method

.method public static newBuilder()LX/9Iw;
    .locals 2

    .prologue
    .line 1463874
    new-instance v0, LX/9Iw;

    invoke-direct {v0}, LX/9Iw;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1463876
    iget-object v0, p0, LX/9Iy;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final a(I)LX/9Ix;
    .locals 1

    .prologue
    .line 1463882
    iget-object v0, p0, LX/9Iy;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Ix;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1463875
    iget-object v0, p0, LX/9Iy;->b:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9Iy;->b:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 1463873
    iget v0, p0, LX/9Iy;->c:I

    return v0
.end method
