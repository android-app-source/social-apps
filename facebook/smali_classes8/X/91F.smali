.class public final enum LX/91F;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/91F;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/91F;

.field public static final enum CONTEXTUAL_SUGGESTION_ACTION:LX/91F;


# instance fields
.field public final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1430264
    new-instance v0, LX/91F;

    const-string v1, "CONTEXTUAL_SUGGESTION_ACTION"

    const-string v2, "minutiae_suggestion_action"

    invoke-direct {v0, v1, v3, v2}, LX/91F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/91F;->CONTEXTUAL_SUGGESTION_ACTION:LX/91F;

    .line 1430265
    const/4 v0, 0x1

    new-array v0, v0, [LX/91F;

    sget-object v1, LX/91F;->CONTEXTUAL_SUGGESTION_ACTION:LX/91F;

    aput-object v1, v0, v3

    sput-object v0, LX/91F;->$VALUES:[LX/91F;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1430259
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1430260
    iput-object p3, p0, LX/91F;->name:Ljava/lang/String;

    .line 1430261
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/91F;
    .locals 1

    .prologue
    .line 1430263
    const-class v0, LX/91F;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/91F;

    return-object v0
.end method

.method public static values()[LX/91F;
    .locals 1

    .prologue
    .line 1430262
    sget-object v0, LX/91F;->$VALUES:[LX/91F;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/91F;

    return-object v0
.end method
