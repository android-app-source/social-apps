.class public final LX/9dR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:F

.field public final synthetic b:LX/9dK;


# direct methods
.method public constructor <init>(LX/9dK;F)V
    .locals 0

    .prologue
    .line 1518129
    iput-object p1, p0, LX/9dR;->b:LX/9dK;

    iput p2, p0, LX/9dR;->a:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 6

    .prologue
    const v3, 0x3f4ccccd    # 0.8f

    .line 1518130
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    .line 1518131
    const/4 v1, 0x0

    .line 1518132
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v2

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    .line 1518133
    iget v1, p0, LX/9dR;->a:F

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v2

    sub-float/2addr v2, v3

    mul-float/2addr v1, v2

    const v2, 0x40a00001    # 5.0000005f

    mul-float/2addr v1, v2

    .line 1518134
    iget-object v2, p0, LX/9dR;->b:LX/9dK;

    iget-object v2, v2, LX/9dK;->g:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setRotation(F)V

    .line 1518135
    iget-object v2, p0, LX/9dR;->b:LX/9dK;

    iget-object v2, v2, LX/9dK;->g:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, -0x40800000    # -1.0f

    mul-float/2addr v1, v3

    float-to-double v4, v1

    invoke-static {v4, v5}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v1, v4

    mul-float/2addr v1, v2

    .line 1518136
    :cond_0
    iget-object v2, p0, LX/9dR;->b:LX/9dK;

    iget-object v2, v2, LX/9dK;->g:Landroid/widget/ImageView;

    iget-object v3, p0, LX/9dR;->b:LX/9dK;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-static {v3, v4}, LX/9dK;->b(LX/9dK;F)F

    move-result v3

    add-float/2addr v1, v3

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setTranslationX(F)V

    .line 1518137
    iget-object v1, p0, LX/9dR;->b:LX/9dK;

    iget-object v1, v1, LX/9dK;->i:LX/9cz;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 1518138
    iget-object v2, v1, LX/9cz;->a:LX/9d5;

    iget-object v2, v2, LX/9d5;->L:LX/9dI;

    const/4 v1, 0x0

    .line 1518139
    const/4 v3, 0x0

    invoke-static {v2, v0, v3, v1, v1}, LX/9dI;->a$redex0(LX/9dI;FFZZ)V

    .line 1518140
    iget-object v0, p0, LX/9dR;->b:LX/9dK;

    iget-object v0, v0, LX/9dK;->h:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 1518141
    iget-object v0, p0, LX/9dR;->b:LX/9dK;

    iget-object v0, v0, LX/9dK;->h:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 1518142
    :cond_1
    return-void
.end method
