.class public final LX/AEu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AEs;


# instance fields
.field public final synthetic a:Lcom/facebook/audience/model/ReplyThread;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/AFL;

.field public final synthetic d:LX/AEv;


# direct methods
.method public constructor <init>(LX/AEv;Lcom/facebook/audience/model/ReplyThread;Ljava/lang/String;LX/AFL;)V
    .locals 0

    .prologue
    .line 1647127
    iput-object p1, p0, LX/AEu;->d:LX/AEv;

    iput-object p2, p0, LX/AEu;->a:Lcom/facebook/audience/model/ReplyThread;

    iput-object p3, p0, LX/AEu;->b:Ljava/lang/String;

    iput-object p4, p0, LX/AEu;->c:LX/AFL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Map;)V
    .locals 8
    .param p1    # Ljava/util/Map;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/os/Parcel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1647128
    iget-object v1, p0, LX/AEu;->a:Lcom/facebook/audience/model/ReplyThread;

    .line 1647129
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/AEu;->b:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1647130
    iget-object v0, p0, LX/AEu;->b:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcel;

    invoke-static {v0}, LX/AEv;->b(Landroid/os/Parcel;)Lcom/facebook/audience/direct/model/CachedSeenModel;

    move-result-object v0

    .line 1647131
    iget-object v2, p0, LX/AEu;->a:Lcom/facebook/audience/model/ReplyThread;

    invoke-static {v2}, LX/7h2;->a(Lcom/facebook/audience/model/ReplyThread;)Lcom/facebook/audience/model/Reply;

    move-result-object v2

    .line 1647132
    iget-wide v6, v2, Lcom/facebook/audience/model/Reply;->i:J

    move-wide v2, v6

    .line 1647133
    iget-wide v6, v0, Lcom/facebook/audience/direct/model/CachedSeenModel;->a:J

    move-wide v4, v6

    .line 1647134
    cmp-long v0, v2, v4

    if-gtz v0, :cond_0

    .line 1647135
    iget-object v0, p0, LX/AEu;->a:Lcom/facebook/audience/model/ReplyThread;

    invoke-static {v0}, Lcom/facebook/audience/model/ReplyThread;->a(Lcom/facebook/audience/model/ReplyThread;)LX/7gs;

    move-result-object v0

    const/4 v1, 0x1

    .line 1647136
    iput-boolean v1, v0, LX/7gs;->c:Z

    .line 1647137
    move-object v0, v0

    .line 1647138
    invoke-virtual {v0}, LX/7gs;->a()Lcom/facebook/audience/model/ReplyThread;

    move-result-object v0

    .line 1647139
    :goto_0
    iget-object v1, p0, LX/AEu;->c:LX/AFL;

    invoke-virtual {v1, v0}, LX/AFL;->a(Lcom/facebook/audience/model/ReplyThread;)V

    .line 1647140
    return-void

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method
