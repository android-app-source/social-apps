.class public LX/ARx;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/2rw;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1673525
    sget-object v0, LX/2rw;->UNDIRECTED:LX/2rw;

    sget-object v1, LX/2rw;->USER:LX/2rw;

    sget-object v2, LX/2rw;->GROUP:LX/2rw;

    sget-object v3, LX/2rw;->PAGE:LX/2rw;

    invoke-static {v0, v1, v2, v3}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/ARx;->a:LX/0Rf;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1673519
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1673520
    return-void
.end method

.method public static a(LX/0QB;)LX/ARx;
    .locals 1

    .prologue
    .line 1673522
    new-instance v0, LX/ARx;

    invoke-direct {v0}, LX/ARx;-><init>()V

    .line 1673523
    move-object v0, v0

    .line 1673524
    return-object v0
.end method


# virtual methods
.method public final a()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/2rw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1673521
    sget-object v0, LX/ARx;->a:LX/0Rf;

    const-class v1, LX/2rw;

    invoke-static {v0, v1}, LX/0RA;->a(Ljava/lang/Iterable;Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    return-object v0
.end method
