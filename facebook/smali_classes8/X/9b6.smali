.class public final LX/9b6;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/9b7;


# direct methods
.method public constructor <init>(LX/9b7;)V
    .locals 0

    .prologue
    .line 1514289
    iput-object p1, p0, LX/9b6;->a:LX/9b7;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1514290
    iget-object v0, p0, LX/9b6;->a:LX/9b7;

    iget-object v0, v0, LX/9b7;->c:LX/9b8;

    iget-object v0, v0, LX/9b8;->d:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 1514291
    iget-object v0, p0, LX/9b6;->a:LX/9b7;

    iget-object v0, v0, LX/9b7;->c:LX/9b8;

    const/4 v1, 0x0

    iput-object v1, v0, LX/9b8;->d:LX/4BY;

    .line 1514292
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1514293
    iget-object v0, p0, LX/9b6;->a:LX/9b7;

    iget-object v0, v0, LX/9b7;->c:LX/9b8;

    iget-object v0, v0, LX/9b8;->d:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 1514294
    iget-object v0, p0, LX/9b6;->a:LX/9b7;

    iget-object v0, v0, LX/9b7;->c:LX/9b8;

    const/4 v1, 0x0

    iput-object v1, v0, LX/9b8;->d:LX/4BY;

    .line 1514295
    iget-object v0, p0, LX/9b6;->a:LX/9b7;

    iget-object v0, v0, LX/9b7;->a:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 1514296
    iget-object v0, p0, LX/9b6;->a:LX/9b7;

    iget-object v0, v0, LX/9b7;->c:LX/9b8;

    invoke-virtual {v0}, LX/9b8;->b()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0811fc

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1514297
    iget-object v0, p0, LX/9b6;->a:LX/9b7;

    iget-object v0, v0, LX/9b7;->c:LX/9b8;

    iget-object v0, v0, LX/9b8;->e:LX/9bY;

    new-instance v1, LX/9bc;

    iget-object v2, p0, LX/9b6;->a:LX/9b7;

    iget-object v2, v2, LX/9b7;->c:LX/9b8;

    iget-object v2, v2, LX/9b8;->b:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/9bc;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1514298
    iget-object v0, p0, LX/9b6;->a:LX/9b7;

    iget-boolean v0, v0, LX/9b7;->b:Z

    if-eqz v0, :cond_1

    .line 1514299
    iget-object v0, p0, LX/9b6;->a:LX/9b7;

    iget-object v0, v0, LX/9b7;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->onBackPressed()V

    .line 1514300
    :cond_0
    :goto_0
    return-void

    .line 1514301
    :cond_1
    iget-object v0, p0, LX/9b6;->a:LX/9b7;

    iget-object v0, v0, LX/9b7;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method
