.class public LX/9JT;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I
    .annotation build Lcom/facebook/graphql/cursor/simple/SessionUpdate$UpdateType;
    .end annotation
.end field

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:[B

.field public final f:I

.field public final g:[B

.field public final h:I

.field private final i:J

.field private final j:J

.field public final k:[I

.field public final l:Ljava/lang/String;

.field public final m:Lcom/facebook/graphql/cursor/edgestore/PageInfo;

.field public final n:I


# direct methods
.method private constructor <init>(IIII[BI[BIJJ[ILjava/lang/String;Lcom/facebook/graphql/cursor/edgestore/PageInfo;I)V
    .locals 1
    .param p1    # I
        .annotation build Lcom/facebook/graphql/cursor/simple/SessionUpdate$UpdateType;
        .end annotation
    .end param

    .prologue
    .line 1465323
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1465324
    iput p1, p0, LX/9JT;->a:I

    .line 1465325
    iput p2, p0, LX/9JT;->b:I

    .line 1465326
    iput p3, p0, LX/9JT;->c:I

    .line 1465327
    iput p4, p0, LX/9JT;->d:I

    .line 1465328
    iput-object p5, p0, LX/9JT;->e:[B

    .line 1465329
    iput p6, p0, LX/9JT;->f:I

    .line 1465330
    iput-object p7, p0, LX/9JT;->g:[B

    .line 1465331
    iput p8, p0, LX/9JT;->h:I

    .line 1465332
    iput-wide p9, p0, LX/9JT;->i:J

    .line 1465333
    iput-wide p11, p0, LX/9JT;->j:J

    .line 1465334
    iput-object p13, p0, LX/9JT;->k:[I

    .line 1465335
    iput-object p14, p0, LX/9JT;->l:Ljava/lang/String;

    .line 1465336
    move-object/from16 v0, p15

    iput-object v0, p0, LX/9JT;->m:Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    .line 1465337
    move/from16 v0, p16

    iput v0, p0, LX/9JT;->n:I

    .line 1465338
    return-void
.end method

.method public static a(I)LX/9JT;
    .locals 18

    .prologue
    .line 1465259
    new-instance v1, LX/9JT;

    const/4 v2, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    const-wide/16 v12, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v3, p0

    invoke-direct/range {v1 .. v17}, LX/9JT;-><init>(IIII[BI[BIJJ[ILjava/lang/String;Lcom/facebook/graphql/cursor/edgestore/PageInfo;I)V

    return-object v1
.end method

.method public static a(III)LX/9JT;
    .locals 18

    .prologue
    .line 1465322
    new-instance v1, LX/9JT;

    const/4 v2, 0x3

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    const-wide/16 v12, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v3, p0

    move/from16 v4, p1

    move/from16 v5, p2

    invoke-direct/range {v1 .. v17}, LX/9JT;-><init>(IIII[BI[BIJJ[ILjava/lang/String;Lcom/facebook/graphql/cursor/edgestore/PageInfo;I)V

    return-object v1
.end method

.method public static a(IIIIIJJ[ILjava/lang/String;)LX/9JT;
    .locals 19

    .prologue
    .line 1465321
    new-instance v1, LX/9JT;

    const/4 v2, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v3, p0

    move/from16 v4, p1

    move/from16 v5, p2

    move/from16 v7, p3

    move/from16 v9, p4

    move-wide/from16 v10, p5

    move-wide/from16 v12, p7

    move-object/from16 v14, p9

    move-object/from16 v15, p10

    invoke-direct/range {v1 .. v17}, LX/9JT;-><init>(IIII[BI[BIJJ[ILjava/lang/String;Lcom/facebook/graphql/cursor/edgestore/PageInfo;I)V

    return-object v1
.end method

.method public static a(I[B)LX/9JT;
    .locals 18

    .prologue
    .line 1465320
    new-instance v1, LX/9JT;

    const/4 v2, 0x5

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    const-wide/16 v12, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v3, p0

    move-object/from16 v6, p1

    invoke-direct/range {v1 .. v17}, LX/9JT;-><init>(IIII[BI[BIJJ[ILjava/lang/String;Lcom/facebook/graphql/cursor/edgestore/PageInfo;I)V

    return-object v1
.end method

.method public static a(Lcom/facebook/graphql/cursor/edgestore/PageInfo;)LX/9JT;
    .locals 18

    .prologue
    .line 1465319
    new-instance v1, LX/9JT;

    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    const-wide/16 v12, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v17, 0x0

    move-object/from16 v16, p0

    invoke-direct/range {v1 .. v17}, LX/9JT;-><init>(IIII[BI[BIJJ[ILjava/lang/String;Lcom/facebook/graphql/cursor/edgestore/PageInfo;I)V

    return-object v1
.end method

.method public static a(Ljava/nio/ByteBuffer;)LX/9JT;
    .locals 13

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1465267
    new-instance v12, LX/9JJ;

    invoke-direct {v12, p0}, LX/9JJ;-><init>(Ljava/nio/ByteBuffer;)V

    .line 1465268
    invoke-virtual {v12}, LX/9JJ;->a()B

    move-result v0

    .line 1465269
    packed-switch v0, :pswitch_data_0

    .line 1465270
    invoke-static {v6}, LX/0PB;->checkState(Z)V

    .line 1465271
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1465272
    :pswitch_0
    invoke-virtual {v12}, LX/9JJ;->b()I

    move-result v1

    .line 1465273
    invoke-virtual {v12}, LX/9JJ;->b()I

    move-result v2

    .line 1465274
    invoke-virtual {v12}, LX/9JJ;->b()I

    move-result v3

    .line 1465275
    invoke-virtual {v12}, LX/9JJ;->c()S

    move-result v4

    .line 1465276
    invoke-virtual {v12}, LX/9JJ;->b()I

    move-result v5

    .line 1465277
    invoke-virtual {v12}, LX/9JJ;->d()J

    move-result-wide v6

    .line 1465278
    invoke-virtual {v12}, LX/9JJ;->d()J

    move-result-wide v8

    .line 1465279
    iget-object v0, v12, LX/9JJ;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v11

    .line 1465280
    if-nez v11, :cond_6

    .line 1465281
    const/4 v0, 0x0

    .line 1465282
    :cond_0
    move-object v10, v0

    .line 1465283
    invoke-virtual {v12}, LX/9JJ;->e()Ljava/lang/String;

    move-result-object v11

    .line 1465284
    invoke-virtual {v12}, LX/9JJ;->h()V

    .line 1465285
    invoke-static/range {v1 .. v11}, LX/9JT;->a(IIIIIJJ[ILjava/lang/String;)LX/9JT;

    move-result-object v0

    goto :goto_0

    .line 1465286
    :pswitch_1
    invoke-virtual {v12}, LX/9JJ;->b()I

    move-result v1

    .line 1465287
    invoke-virtual {v12}, LX/9JJ;->h()V

    .line 1465288
    if-ne v0, v5, :cond_1

    invoke-static {v1}, LX/9JT;->a(I)LX/9JT;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-static {v1}, LX/9JT;->b(I)LX/9JT;

    move-result-object v0

    goto :goto_0

    .line 1465289
    :pswitch_2
    invoke-virtual {v12}, LX/9JJ;->e()Ljava/lang/String;

    move-result-object v0

    .line 1465290
    invoke-virtual {v12}, LX/9JJ;->e()Ljava/lang/String;

    move-result-object v1

    .line 1465291
    invoke-virtual {v12}, LX/9JJ;->e()Ljava/lang/String;

    move-result-object v2

    .line 1465292
    invoke-virtual {v12}, LX/9JJ;->e()Ljava/lang/String;

    move-result-object v3

    .line 1465293
    invoke-virtual {v12}, LX/9JJ;->a()B

    move-result v4

    if-ne v4, v5, :cond_2

    move v4, v5

    .line 1465294
    :goto_1
    invoke-virtual {v12}, LX/9JJ;->a()B

    move-result v7

    if-ne v7, v5, :cond_3

    .line 1465295
    :goto_2
    invoke-virtual {v12}, LX/9JJ;->d()J

    move-result-wide v6

    .line 1465296
    invoke-virtual {v12}, LX/9JJ;->h()V

    .line 1465297
    invoke-static/range {v0 .. v7}, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZJ)Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    move-result-object v0

    invoke-static {v0}, LX/9JT;->a(Lcom/facebook/graphql/cursor/edgestore/PageInfo;)LX/9JT;

    move-result-object v0

    goto :goto_0

    :cond_2
    move v4, v6

    .line 1465298
    goto :goto_1

    :cond_3
    move v5, v6

    .line 1465299
    goto :goto_2

    .line 1465300
    :pswitch_3
    invoke-virtual {v12}, LX/9JJ;->b()I

    move-result v1

    .line 1465301
    invoke-virtual {v12}, LX/9JJ;->b()I

    move-result v2

    .line 1465302
    invoke-virtual {v12}, LX/9JJ;->b()I

    move-result v3

    .line 1465303
    invoke-virtual {v12}, LX/9JJ;->h()V

    .line 1465304
    const/4 v4, 0x3

    if-ne v0, v4, :cond_4

    invoke-static {v1, v2, v3}, LX/9JT;->a(III)LX/9JT;

    move-result-object v0

    goto/16 :goto_0

    :cond_4
    invoke-static {v1, v2, v3}, LX/9JT;->b(III)LX/9JT;

    move-result-object v0

    goto/16 :goto_0

    .line 1465305
    :pswitch_4
    invoke-virtual {v12}, LX/9JJ;->b()I

    move-result v1

    .line 1465306
    invoke-virtual {v12}, LX/9JJ;->g()[B

    move-result-object v2

    .line 1465307
    invoke-virtual {v12}, LX/9JJ;->h()V

    .line 1465308
    const/4 v3, 0x5

    if-ne v0, v3, :cond_5

    invoke-static {v1, v2}, LX/9JT;->a(I[B)LX/9JT;

    move-result-object v0

    goto/16 :goto_0

    :cond_5
    invoke-static {v1, v2}, LX/9JT;->b(I[B)LX/9JT;

    move-result-object v0

    goto/16 :goto_0

    .line 1465309
    :pswitch_5
    invoke-virtual {v12}, LX/9JJ;->b()I

    move-result v0

    .line 1465310
    invoke-virtual {v12}, LX/9JJ;->h()V

    .line 1465311
    invoke-static {v0}, LX/9JT;->c(I)LX/9JT;

    move-result-object v0

    goto/16 :goto_0

    .line 1465312
    :pswitch_6
    invoke-virtual {v12}, LX/9JJ;->c()S

    move-result v0

    .line 1465313
    invoke-virtual {v12}, LX/9JJ;->g()[B

    move-result-object v1

    .line 1465314
    invoke-static {v0, v1}, LX/9JT;->c(I[B)LX/9JT;

    move-result-object v0

    goto/16 :goto_0

    .line 1465315
    :cond_6
    new-array v0, v11, [I

    .line 1465316
    const/4 v10, 0x0

    :goto_3
    if-ge v10, v11, :cond_0

    .line 1465317
    iget-object p0, v12, LX/9JJ;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result p0

    aput p0, v0, v10

    .line 1465318
    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_1
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static b(I)LX/9JT;
    .locals 18

    .prologue
    .line 1465266
    new-instance v1, LX/9JT;

    const/4 v2, 0x7

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    const-wide/16 v12, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v3, p0

    invoke-direct/range {v1 .. v17}, LX/9JT;-><init>(IIII[BI[BIJJ[ILjava/lang/String;Lcom/facebook/graphql/cursor/edgestore/PageInfo;I)V

    return-object v1
.end method

.method public static b(III)LX/9JT;
    .locals 18

    .prologue
    .line 1465265
    new-instance v1, LX/9JT;

    const/4 v2, 0x4

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    const-wide/16 v12, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v3, p0

    move/from16 v4, p1

    move/from16 v5, p2

    invoke-direct/range {v1 .. v17}, LX/9JT;-><init>(IIII[BI[BIJJ[ILjava/lang/String;Lcom/facebook/graphql/cursor/edgestore/PageInfo;I)V

    return-object v1
.end method

.method public static b(I[B)LX/9JT;
    .locals 18

    .prologue
    .line 1465264
    new-instance v1, LX/9JT;

    const/4 v2, 0x6

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    const-wide/16 v12, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v3, p0

    move-object/from16 v6, p1

    invoke-direct/range {v1 .. v17}, LX/9JT;-><init>(IIII[BI[BIJJ[ILjava/lang/String;Lcom/facebook/graphql/cursor/edgestore/PageInfo;I)V

    return-object v1
.end method

.method public static c(I)LX/9JT;
    .locals 18

    .prologue
    .line 1465263
    new-instance v1, LX/9JT;

    const/16 v2, 0x8

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    const-wide/16 v12, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v17, p0

    invoke-direct/range {v1 .. v17}, LX/9JT;-><init>(IIII[BI[BIJJ[ILjava/lang/String;Lcom/facebook/graphql/cursor/edgestore/PageInfo;I)V

    return-object v1
.end method

.method public static c(I[B)LX/9JT;
    .locals 18

    .prologue
    .line 1465262
    new-instance v1, LX/9JT;

    const/16 v2, 0x9

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    const-wide/16 v12, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v7, p0

    move-object/from16 v8, p1

    invoke-direct/range {v1 .. v17}, LX/9JT;-><init>(IIII[BI[BIJJ[ILjava/lang/String;Lcom/facebook/graphql/cursor/edgestore/PageInfo;I)V

    return-object v1
.end method


# virtual methods
.method public final i()J
    .locals 2

    .prologue
    .line 1465261
    iget-wide v0, p0, LX/9JT;->i:J

    return-wide v0
.end method

.method public final j()J
    .locals 2

    .prologue
    .line 1465260
    iget-wide v0, p0, LX/9JT;->j:J

    return-wide v0
.end method

.method public final o()Ljava/nio/ByteBuffer;
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1465152
    new-instance v4, LX/9JL;

    invoke-direct {v4}, LX/9JL;-><init>()V

    .line 1465153
    const/4 v0, 0x0

    .line 1465154
    iget v3, p0, LX/9JT;->a:I

    packed-switch v3, :pswitch_data_0

    .line 1465155
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 1465156
    :goto_0
    iget-object v1, v0, LX/9JK;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    if-nez v1, :cond_4

    const/4 v1, 0x1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1465157
    iget-object v1, v0, LX/9JK;->a:Ljava/nio/ByteBuffer;

    .line 1465158
    const/4 v2, 0x0

    iput-object v2, v0, LX/9JK;->a:Ljava/nio/ByteBuffer;

    .line 1465159
    move-object v0, v1

    .line 1465160
    return-object v0

    .line 1465161
    :pswitch_0
    invoke-virtual {v4}, LX/9JL;->a()V

    .line 1465162
    invoke-virtual {v4}, LX/9JL;->b()V

    .line 1465163
    invoke-virtual {v4}, LX/9JL;->b()V

    .line 1465164
    invoke-virtual {v4}, LX/9JL;->b()V

    .line 1465165
    invoke-virtual {v4}, LX/9JL;->c()V

    .line 1465166
    invoke-virtual {v4}, LX/9JL;->b()V

    .line 1465167
    invoke-virtual {v4}, LX/9JL;->d()V

    .line 1465168
    invoke-virtual {v4}, LX/9JL;->d()V

    .line 1465169
    iget-object v0, p0, LX/9JT;->k:[I

    .line 1465170
    iget v2, v4, LX/9JL;->a:I

    if-nez v0, :cond_5

    const/4 v1, 0x0

    :goto_2
    add-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v2

    iput v1, v4, LX/9JL;->a:I

    .line 1465171
    iget-object v0, p0, LX/9JT;->l:Ljava/lang/String;

    invoke-virtual {v4, v0}, LX/9JL;->a(Ljava/lang/String;)V

    .line 1465172
    new-instance v0, LX/9JK;

    .line 1465173
    iget v1, v4, LX/9JL;->a:I

    move v1, v1

    .line 1465174
    invoke-direct {v0, v1}, LX/9JK;-><init>(I)V

    .line 1465175
    iget v1, p0, LX/9JT;->a:I

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, LX/9JK;->a(B)V

    .line 1465176
    iget v1, p0, LX/9JT;->b:I

    invoke-virtual {v0, v1}, LX/9JK;->a(I)V

    .line 1465177
    iget v1, p0, LX/9JT;->c:I

    invoke-virtual {v0, v1}, LX/9JK;->a(I)V

    .line 1465178
    iget v1, p0, LX/9JT;->d:I

    invoke-virtual {v0, v1}, LX/9JK;->a(I)V

    .line 1465179
    iget v1, p0, LX/9JT;->f:I

    int-to-short v1, v1

    invoke-virtual {v0, v1}, LX/9JK;->a(S)V

    .line 1465180
    iget v1, p0, LX/9JT;->h:I

    invoke-virtual {v0, v1}, LX/9JK;->a(I)V

    .line 1465181
    iget-wide v2, p0, LX/9JT;->i:J

    invoke-virtual {v0, v2, v3}, LX/9JK;->a(J)V

    .line 1465182
    iget-wide v2, p0, LX/9JT;->j:J

    invoke-virtual {v0, v2, v3}, LX/9JK;->a(J)V

    .line 1465183
    iget-object v1, p0, LX/9JT;->k:[I

    const/4 v3, 0x0

    .line 1465184
    if-nez v1, :cond_0

    move v2, v3

    .line 1465185
    :goto_3
    iget-object v4, v0, LX/9JK;->a:Ljava/nio/ByteBuffer;

    int-to-short v2, v2

    invoke-virtual {v4, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1465186
    if-eqz v1, :cond_1

    .line 1465187
    array-length v2, v1

    :goto_4
    if-ge v3, v2, :cond_1

    aget v4, v1, v3

    .line 1465188
    iget-object v5, v0, LX/9JK;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1465189
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 1465190
    :cond_0
    array-length v2, v1

    goto :goto_3

    .line 1465191
    :cond_1
    iget-object v1, p0, LX/9JT;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/9JK;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1465192
    :pswitch_1
    invoke-virtual {v4}, LX/9JL;->a()V

    .line 1465193
    invoke-virtual {v4}, LX/9JL;->b()V

    .line 1465194
    new-instance v0, LX/9JK;

    .line 1465195
    iget v1, v4, LX/9JL;->a:I

    move v1, v1

    .line 1465196
    invoke-direct {v0, v1}, LX/9JK;-><init>(I)V

    .line 1465197
    iget v1, p0, LX/9JT;->a:I

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, LX/9JK;->a(B)V

    .line 1465198
    iget v1, p0, LX/9JT;->b:I

    invoke-virtual {v0, v1}, LX/9JK;->a(I)V

    goto/16 :goto_0

    .line 1465199
    :pswitch_2
    invoke-virtual {v4}, LX/9JL;->a()V

    .line 1465200
    iget-object v0, p0, LX/9JT;->m:Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    iget-object v0, v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->a:Ljava/lang/String;

    invoke-virtual {v4, v0}, LX/9JL;->a(Ljava/lang/String;)V

    .line 1465201
    iget-object v0, p0, LX/9JT;->m:Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    iget-object v0, v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->b:Ljava/lang/String;

    invoke-virtual {v4, v0}, LX/9JL;->a(Ljava/lang/String;)V

    .line 1465202
    iget-object v0, p0, LX/9JT;->m:Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    iget-object v0, v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->c:Ljava/lang/String;

    invoke-virtual {v4, v0}, LX/9JL;->a(Ljava/lang/String;)V

    .line 1465203
    iget-object v0, p0, LX/9JT;->m:Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    iget-object v0, v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->d:Ljava/lang/String;

    invoke-virtual {v4, v0}, LX/9JL;->a(Ljava/lang/String;)V

    .line 1465204
    invoke-virtual {v4}, LX/9JL;->a()V

    .line 1465205
    invoke-virtual {v4}, LX/9JL;->a()V

    .line 1465206
    invoke-virtual {v4}, LX/9JL;->d()V

    .line 1465207
    new-instance v3, LX/9JK;

    .line 1465208
    iget v0, v4, LX/9JL;->a:I

    move v0, v0

    .line 1465209
    invoke-direct {v3, v0}, LX/9JK;-><init>(I)V

    .line 1465210
    iget v0, p0, LX/9JT;->a:I

    int-to-byte v0, v0

    invoke-virtual {v3, v0}, LX/9JK;->a(B)V

    .line 1465211
    iget-object v0, p0, LX/9JT;->m:Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    iget-object v0, v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->a:Ljava/lang/String;

    invoke-virtual {v3, v0}, LX/9JK;->a(Ljava/lang/String;)V

    .line 1465212
    iget-object v0, p0, LX/9JT;->m:Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    iget-object v0, v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->b:Ljava/lang/String;

    invoke-virtual {v3, v0}, LX/9JK;->a(Ljava/lang/String;)V

    .line 1465213
    iget-object v0, p0, LX/9JT;->m:Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    iget-object v0, v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->c:Ljava/lang/String;

    invoke-virtual {v3, v0}, LX/9JK;->a(Ljava/lang/String;)V

    .line 1465214
    iget-object v0, p0, LX/9JT;->m:Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    iget-object v0, v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->d:Ljava/lang/String;

    invoke-virtual {v3, v0}, LX/9JK;->a(Ljava/lang/String;)V

    .line 1465215
    iget-object v0, p0, LX/9JT;->m:Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    iget-boolean v0, v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->e:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_5
    invoke-virtual {v3, v0}, LX/9JK;->a(B)V

    .line 1465216
    iget-object v0, p0, LX/9JT;->m:Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    iget-boolean v0, v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->f:Z

    if-eqz v0, :cond_3

    :goto_6
    invoke-virtual {v3, v1}, LX/9JK;->a(B)V

    .line 1465217
    iget-object v0, p0, LX/9JT;->m:Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    iget-wide v0, v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->h:J

    invoke-virtual {v3, v0, v1}, LX/9JK;->a(J)V

    move-object v0, v3

    .line 1465218
    goto/16 :goto_0

    :cond_2
    move v0, v2

    .line 1465219
    goto :goto_5

    :cond_3
    move v1, v2

    .line 1465220
    goto :goto_6

    .line 1465221
    :pswitch_3
    invoke-virtual {v4}, LX/9JL;->a()V

    .line 1465222
    invoke-virtual {v4}, LX/9JL;->b()V

    .line 1465223
    invoke-virtual {v4}, LX/9JL;->b()V

    .line 1465224
    invoke-virtual {v4}, LX/9JL;->b()V

    .line 1465225
    new-instance v0, LX/9JK;

    .line 1465226
    iget v1, v4, LX/9JL;->a:I

    move v1, v1

    .line 1465227
    invoke-direct {v0, v1}, LX/9JK;-><init>(I)V

    .line 1465228
    iget v1, p0, LX/9JT;->a:I

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, LX/9JK;->a(B)V

    .line 1465229
    iget v1, p0, LX/9JT;->b:I

    invoke-virtual {v0, v1}, LX/9JK;->a(I)V

    .line 1465230
    iget v1, p0, LX/9JT;->c:I

    invoke-virtual {v0, v1}, LX/9JK;->a(I)V

    .line 1465231
    iget v1, p0, LX/9JT;->d:I

    invoke-virtual {v0, v1}, LX/9JK;->a(I)V

    goto/16 :goto_0

    .line 1465232
    :pswitch_4
    invoke-virtual {v4}, LX/9JL;->a()V

    .line 1465233
    invoke-virtual {v4}, LX/9JL;->b()V

    .line 1465234
    iget-object v0, p0, LX/9JT;->e:[B

    invoke-virtual {v4, v0}, LX/9JL;->a([B)V

    .line 1465235
    new-instance v0, LX/9JK;

    .line 1465236
    iget v1, v4, LX/9JL;->a:I

    move v1, v1

    .line 1465237
    invoke-direct {v0, v1}, LX/9JK;-><init>(I)V

    .line 1465238
    iget v1, p0, LX/9JT;->a:I

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, LX/9JK;->a(B)V

    .line 1465239
    iget v1, p0, LX/9JT;->b:I

    invoke-virtual {v0, v1}, LX/9JK;->a(I)V

    .line 1465240
    iget-object v1, p0, LX/9JT;->e:[B

    invoke-virtual {v0, v1}, LX/9JK;->a([B)V

    goto/16 :goto_0

    .line 1465241
    :pswitch_5
    invoke-virtual {v4}, LX/9JL;->a()V

    .line 1465242
    invoke-virtual {v4}, LX/9JL;->b()V

    .line 1465243
    new-instance v0, LX/9JK;

    .line 1465244
    iget v1, v4, LX/9JL;->a:I

    move v1, v1

    .line 1465245
    invoke-direct {v0, v1}, LX/9JK;-><init>(I)V

    .line 1465246
    iget v1, p0, LX/9JT;->a:I

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, LX/9JK;->a(B)V

    .line 1465247
    iget v1, p0, LX/9JT;->n:I

    invoke-virtual {v0, v1}, LX/9JK;->a(I)V

    goto/16 :goto_0

    .line 1465248
    :pswitch_6
    invoke-virtual {v4}, LX/9JL;->a()V

    .line 1465249
    invoke-virtual {v4}, LX/9JL;->c()V

    .line 1465250
    iget-object v0, p0, LX/9JT;->g:[B

    invoke-virtual {v4, v0}, LX/9JL;->a([B)V

    .line 1465251
    new-instance v0, LX/9JK;

    .line 1465252
    iget v1, v4, LX/9JL;->a:I

    move v1, v1

    .line 1465253
    invoke-direct {v0, v1}, LX/9JK;-><init>(I)V

    .line 1465254
    iget v1, p0, LX/9JT;->a:I

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, LX/9JK;->a(B)V

    .line 1465255
    iget v1, p0, LX/9JT;->f:I

    int-to-short v1, v1

    invoke-virtual {v0, v1}, LX/9JK;->a(S)V

    .line 1465256
    iget-object v1, p0, LX/9JT;->g:[B

    invoke-virtual {v0, v1}, LX/9JK;->a([B)V

    goto/16 :goto_0

    .line 1465257
    :cond_4
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 1465258
    :cond_5
    array-length v1, v0

    mul-int/lit8 v1, v1, 0x4

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_1
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
