.class public abstract LX/94V;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/0h5;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public a:Landroid/view/LayoutInflater;

.field public b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

.field private final c:Landroid/view/View$OnClickListener;

.field public d:LX/63W;

.field public e:LX/03V;

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1435341
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1435342
    new-instance v0, LX/94S;

    invoke-direct {v0, p0}, LX/94S;-><init>(LX/94V;)V

    iput-object v0, p0, LX/94V;->c:Landroid/view/View$OnClickListener;

    .line 1435343
    invoke-direct {p0}, LX/94V;->a()V

    .line 1435344
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1435345
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1435346
    new-instance v0, LX/94S;

    invoke-direct {v0, p0}, LX/94S;-><init>(LX/94V;)V

    iput-object v0, p0, LX/94V;->c:Landroid/view/View$OnClickListener;

    .line 1435347
    invoke-direct {p0}, LX/94V;->a()V

    .line 1435348
    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    .line 1435349
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/94V;->setOrientation(I)V

    .line 1435350
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v4

    check-cast p0, LX/94V;

    invoke-static {v4}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    invoke-static {v4}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    const/16 v0, 0x97

    invoke-static {v4, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    iput-object v2, p0, LX/94V;->a:Landroid/view/LayoutInflater;

    iput-object v3, p0, LX/94V;->e:LX/03V;

    iput-object v4, p0, LX/94V;->f:LX/0Ot;

    .line 1435351
    return-void
.end method


# virtual methods
.method public a(LX/94c;)V
    .locals 0

    .prologue
    .line 1435352
    return-void
.end method

.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 1435353
    return-void
.end method

.method public final d_(I)Landroid/view/View;
    .locals 3

    .prologue
    .line 1435354
    iget-object v0, p0, LX/94V;->e:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "#setCustomTitleView"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "method not supported"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1435355
    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract getPrimaryButtonDivider()Landroid/view/View;
.end method

.method public abstract getPrimaryTextButton()Lcom/facebook/widget/text/SimpleVariableTextLayoutView;
.end method

.method public abstract getSecondaryButton()Landroid/view/View;
.end method

.method public abstract getTitleTextView()Lcom/facebook/widget/text/SimpleVariableTextLayoutView;
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x1341b9e4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1435339
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/widget/CustomLinearLayout;->onSizeChanged(IIII)V

    .line 1435340
    const/16 v1, 0x2d

    const v2, -0x4ed6b98b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setButtonSpecs(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/titlebar/TitleBarButtonSpec;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1435295
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 1435296
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1435297
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    iput-object v0, p0, LX/94V;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 1435298
    :goto_0
    invoke-virtual {p0}, LX/94V;->getPrimaryTextButton()Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;->setVisibility(I)V

    .line 1435299
    invoke-virtual {p0}, LX/94V;->getPrimaryButtonDivider()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1435300
    iget-object v0, p0, LX/94V;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/94V;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    sget-object v1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    if-ne v0, v1, :cond_2

    .line 1435301
    :cond_0
    :goto_1
    return-void

    .line 1435302
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, LX/94V;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    goto :goto_0

    .line 1435303
    :cond_2
    iget-object v0, p0, LX/94V;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 1435304
    iget-object v1, v0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->i:Ljava/lang/String;

    move-object v0, v1

    .line 1435305
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1435306
    invoke-virtual {p0}, LX/94V;->getPrimaryTextButton()Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    move-result-object v0

    iget-object v1, p0, LX/94V;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 1435307
    iget-object p1, v1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->i:Ljava/lang/String;

    move-object v1, p1

    .line 1435308
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;->setText(Ljava/lang/CharSequence;)V

    .line 1435309
    invoke-virtual {p0}, LX/94V;->getPrimaryTextButton()Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;->setVisibility(I)V

    .line 1435310
    invoke-virtual {p0}, LX/94V;->getPrimaryButtonDivider()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1435311
    :cond_3
    iget-object v0, p0, LX/94V;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 1435312
    iget-object v1, v0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->k:Ljava/lang/String;

    move-object v0, v1

    .line 1435313
    if-eqz v0, :cond_4

    .line 1435314
    invoke-virtual {p0}, LX/94V;->getPrimaryTextButton()Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    move-result-object v0

    iget-object v1, p0, LX/94V;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 1435315
    iget-object v2, v1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->k:Ljava/lang/String;

    move-object v1, v2

    .line 1435316
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1435317
    :cond_4
    invoke-virtual {p0}, LX/94V;->getPrimaryTextButton()Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    move-result-object v0

    iget-object v1, p0, LX/94V;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 1435318
    iget-boolean v2, v1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->u:Z

    move v1, v2

    .line 1435319
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;->setEnabled(Z)V

    goto :goto_1
.end method

.method public setCustomTitleView(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1435322
    iget-object v0, p0, LX/94V;->e:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "#setCustomTitleView"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "method not supported"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1435323
    return-void
.end method

.method public setHasBackButton(Z)V
    .locals 3

    .prologue
    .line 1435324
    if-nez p1, :cond_0

    .line 1435325
    iget-object v0, p0, LX/94V;->e:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "#setHasBackButton"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "All composer title has back button."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1435326
    :cond_0
    return-void
.end method

.method public setHasFbLogo(Z)V
    .locals 0

    .prologue
    .line 1435327
    return-void
.end method

.method public setOnBackPressedListener(LX/63J;)V
    .locals 2

    .prologue
    .line 1435320
    invoke-virtual {p0}, LX/94V;->getSecondaryButton()Landroid/view/View;

    move-result-object v0

    new-instance v1, LX/94U;

    invoke-direct {v1, p0, p1}, LX/94U;-><init>(LX/94V;LX/63J;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1435321
    return-void
.end method

.method public setOnToolbarButtonListener(LX/63W;)V
    .locals 2

    .prologue
    .line 1435328
    iput-object p1, p0, LX/94V;->d:LX/63W;

    .line 1435329
    invoke-virtual {p0}, LX/94V;->getPrimaryTextButton()Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    move-result-object v0

    iget-object v1, p0, LX/94V;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1435330
    return-void
.end method

.method public setShowDividers(Z)V
    .locals 0

    .prologue
    .line 1435331
    return-void
.end method

.method public setTitle(I)V
    .locals 1

    .prologue
    .line 1435332
    invoke-virtual {p0}, LX/94V;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, LX/94V;->setTitle(Ljava/lang/String;)V

    .line 1435333
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1435334
    invoke-virtual {p0}, LX/94V;->getTitleTextView()Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    move-result-object v0

    .line 1435335
    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;->setText(Ljava/lang/CharSequence;)V

    .line 1435336
    new-instance v1, LX/94T;

    invoke-direct {v1, p0, v0}, LX/94T;-><init>(LX/94V;Lcom/facebook/widget/text/SimpleVariableTextLayoutView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1435337
    return-void
.end method

.method public setTitlebarAsModal(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 1435338
    return-void
.end method
