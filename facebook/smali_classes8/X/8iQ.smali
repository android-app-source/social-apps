.class public LX/8iQ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/8iQ;


# instance fields
.field public a:F

.field public b:LX/8iN;

.field public c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LX/8iO;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/8iN;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1391433
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1391434
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, LX/8iQ;->a:F

    .line 1391435
    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8iN;

    iput-object v0, p0, LX/8iQ;->b:LX/8iN;

    .line 1391436
    iget-object v0, p0, LX/8iQ;->b:LX/8iN;

    invoke-static {p0, v0}, LX/8iQ;->a(LX/8iQ;LX/8iN;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/8iQ;->c:Ljava/util/Map;

    .line 1391437
    return-void
.end method

.method public static a(LX/0QB;)LX/8iQ;
    .locals 4

    .prologue
    .line 1391438
    sget-object v0, LX/8iQ;->d:LX/8iQ;

    if-nez v0, :cond_1

    .line 1391439
    const-class v1, LX/8iQ;

    monitor-enter v1

    .line 1391440
    :try_start_0
    sget-object v0, LX/8iQ;->d:LX/8iQ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1391441
    if-eqz v2, :cond_0

    .line 1391442
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1391443
    new-instance v3, LX/8iQ;

    const/16 p0, 0x3565

    invoke-static {v0, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, p0}, LX/8iQ;-><init>(LX/0Or;)V

    .line 1391444
    move-object v0, v3

    .line 1391445
    sput-object v0, LX/8iQ;->d:LX/8iQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1391446
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1391447
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1391448
    :cond_1
    sget-object v0, LX/8iQ;->d:LX/8iQ;

    return-object v0

    .line 1391449
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1391450
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/8iQ;LX/8iN;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8iN;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LX/8iO;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1391451
    const-string v0, "AudioConfigRegistry.loadAudioConfig"

    const v1, -0x77104381

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1391452
    :try_start_0
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 1391453
    if-nez v0, :cond_0

    .line 1391454
    const-string v0, ""

    .line 1391455
    :cond_0
    const-string v1, ".*GT-I9305.*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ".*GT-I9300T.*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ".*SGH-T999.*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ".*SGH-I747.*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ".*SCH-R530.*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ".*SCH-I535.*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ".*SCH-R530U.*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ".*SCH-R530M.*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ".*SPH-L710.*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ".*SHV-E210S.*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ".*SHV-E210K.*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1391456
    :cond_1
    sget-object v0, LX/8ic;->SAMSUNG_S3:LX/8ic;

    .line 1391457
    :goto_0
    move-object v0, v0

    .line 1391458
    invoke-virtual {p1, v0}, LX/8iN;->b(LX/8ic;)F

    move-result v1

    .line 1391459
    iput v1, p0, LX/8iQ;->a:F

    .line 1391460
    iget-object v1, p1, LX/8iN;->a:LX/0P1;

    invoke-virtual {v1, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_a

    .line 1391461
    iget-object v1, p1, LX/8iN;->a:LX/0P1;

    invoke-virtual {v1, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8id;

    .line 1391462
    iget-object p1, v1, LX/8id;->b:Ljava/util/Map;

    move-object v1, p1

    .line 1391463
    :goto_1
    move-object v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1391464
    const v1, 0x68b8b55

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, 0x7d190414

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 1391465
    :cond_2
    :try_start_1
    const-string v1, ".*SCH-I545.*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ".*SGH-I337.*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ".*SGH-M919.*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ".*SPH-L720.*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ".*GT-I9505.*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ".*SGH-I537.*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ".*GT-I9505G.*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ".*GT-I9500.*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ".*SGH-I337M.*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1391466
    :cond_3
    sget-object v0, LX/8ic;->SAMSUNG_S4:LX/8ic;

    goto :goto_0

    .line 1391467
    :cond_4
    const-string v1, ".*SM-G900.*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1391468
    sget-object v0, LX/8ic;->SAMSUNG_S5:LX/8ic;

    goto :goto_0

    .line 1391469
    :cond_5
    const-string v1, ".*HTC One.*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, ".*HTCONE.*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, ".*HTC6500LVW.*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, ".*HTC_PN071.*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1391470
    :cond_6
    sget-object v0, LX/8ic;->HTC_ONE:LX/8ic;

    goto/16 :goto_0

    .line 1391471
    :cond_7
    const-string v1, ".*XT102.*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1391472
    sget-object v0, LX/8ic;->MOTO_E:LX/8ic;

    goto/16 :goto_0

    .line 1391473
    :cond_8
    const-string v1, ".*LG-D82.*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1391474
    sget-object v0, LX/8ic;->NEXUS_5:LX/8ic;

    goto/16 :goto_0

    .line 1391475
    :cond_9
    sget-object v0, LX/8ic;->DEFAULT:LX/8ic;

    goto/16 :goto_0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_a
    const/4 v1, 0x0

    goto/16 :goto_1
.end method


# virtual methods
.method public final a(I)F
    .locals 2

    .prologue
    .line 1391476
    iget-object v0, p0, LX/8iQ;->c:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8iO;

    move-object v0, v0

    .line 1391477
    if-eqz v0, :cond_0

    .line 1391478
    iget v1, v0, LX/8iO;->c:F

    move v0, v1

    .line 1391479
    :goto_0
    iget v1, p0, LX/8iQ;->a:F

    mul-float/2addr v0, v1

    return v0

    .line 1391480
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public final b()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/8iO;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1391481
    iget-object v0, p0, LX/8iQ;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1391482
    new-instance v1, LX/8iP;

    invoke-direct {v1}, LX/8iP;-><init>()V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1391483
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 5

    .prologue
    .line 1391484
    invoke-virtual {p0}, LX/8iQ;->b()Ljava/util/List;

    move-result-object v0

    .line 1391485
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1391486
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "master_volume : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1391487
    iget v3, p0, LX/8iQ;->a:F

    move v3, v3

    .line 1391488
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1391489
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8iO;

    .line 1391490
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v0, LX/8iO;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1391491
    iget v4, v0, LX/8iO;->c:F

    move v0, v4

    .line 1391492
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1391493
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
