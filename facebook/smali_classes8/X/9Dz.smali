.class public LX/9Dz;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/animation/ValueAnimator;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Landroid/animation/AnimatorListenerAdapter;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1456336
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1456337
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/9Dz;->a:Ljava/util/Map;

    .line 1456338
    new-instance v0, LX/9Dy;

    invoke-direct {v0, p0}, LX/9Dy;-><init>(LX/9Dz;)V

    iput-object v0, p0, LX/9Dz;->b:Landroid/animation/AnimatorListenerAdapter;

    .line 1456339
    return-void
.end method

.method public static a(LX/0QB;)LX/9Dz;
    .locals 3

    .prologue
    .line 1456340
    const-class v1, LX/9Dz;

    monitor-enter v1

    .line 1456341
    :try_start_0
    sget-object v0, LX/9Dz;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1456342
    sput-object v2, LX/9Dz;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1456343
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1456344
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1456345
    new-instance v0, LX/9Dz;

    invoke-direct {v0}, LX/9Dz;-><init>()V

    .line 1456346
    move-object v0, v0

    .line 1456347
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1456348
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/9Dz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1456349
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1456350
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Landroid/animation/ValueAnimator;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1456351
    iget-object v0, p0, LX/9Dz;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    return-object v0
.end method
