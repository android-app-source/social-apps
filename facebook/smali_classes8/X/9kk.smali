.class public LX/9kk;
.super LX/9kh;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/9kk;


# instance fields
.field public a:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;",
            ">;>;"
        }
    .end annotation
.end field

.field public final c:LX/0tX;

.field public final d:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1532149
    invoke-direct {p0}, LX/9kh;-><init>()V

    .line 1532150
    iput-object p1, p0, LX/9kk;->c:LX/0tX;

    .line 1532151
    iput-object p2, p0, LX/9kk;->d:LX/1Ck;

    .line 1532152
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/9kk;->a:LX/0am;

    .line 1532153
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/9kk;->b:LX/0am;

    .line 1532154
    return-void
.end method

.method public static a(LX/0QB;)LX/9kk;
    .locals 5

    .prologue
    .line 1532136
    sget-object v0, LX/9kk;->e:LX/9kk;

    if-nez v0, :cond_1

    .line 1532137
    const-class v1, LX/9kk;

    monitor-enter v1

    .line 1532138
    :try_start_0
    sget-object v0, LX/9kk;->e:LX/9kk;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1532139
    if-eqz v2, :cond_0

    .line 1532140
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1532141
    new-instance p0, LX/9kk;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-direct {p0, v3, v4}, LX/9kk;-><init>(LX/0tX;LX/1Ck;)V

    .line 1532142
    move-object v0, p0

    .line 1532143
    sput-object v0, LX/9kk;->e:LX/9kk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1532144
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1532145
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1532146
    :cond_1
    sget-object v0, LX/9kk;->e:LX/9kk;

    return-object v0

    .line 1532147
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1532148
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static c(LX/9kk;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1532135
    iget-object v0, p0, LX/9kk;->a:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9kk;->a:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1532119
    invoke-static {p0, p1}, LX/9kk;->c(LX/9kk;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1532120
    iget-object v0, p0, LX/9kk;->b:LX/0am;

    .line 1532121
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 1532122
    invoke-virtual {v0, v1}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 1532123
    :goto_0
    return-object v0

    .line 1532124
    :cond_0
    const/4 v2, 0x1

    .line 1532125
    invoke-static {p0, p1}, LX/9kk;->c(LX/9kk;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1532126
    iget-object v0, p0, LX/9kk;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/9kk;->d:LX/1Ck;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ck;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1532127
    :cond_1
    :goto_1
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1532128
    goto :goto_0

    .line 1532129
    :cond_2
    invoke-static {p1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/9kk;->a:LX/0am;

    .line 1532130
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/9kk;->b:LX/0am;

    .line 1532131
    new-instance v0, LX/9kS;

    invoke-direct {v0}, LX/9kS;-><init>()V

    move-object v0, v0

    .line 1532132
    const-string v1, "query"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/9kS;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 1532133
    iget-object v1, p0, LX/9kk;->d:LX/1Ck;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, p0, LX/9kk;->c:LX/0tX;

    invoke-virtual {v3, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    invoke-static {v0}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v3, LX/9kj;

    invoke-direct {v3, p0}, LX/9kj;-><init>(LX/9kk;)V

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1532134
    invoke-virtual {p0}, LX/9kh;->b()V

    goto :goto_1
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 1532118
    iget-object v0, p0, LX/9kk;->d:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->b()LX/1M1;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1M1;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
