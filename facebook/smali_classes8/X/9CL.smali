.class public final LX/9CL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4nF;


# instance fields
.field public final synthetic a:LX/9CP;


# direct methods
.method public constructor <init>(LX/9CP;)V
    .locals 0

    .prologue
    .line 1453777
    iput-object p1, p0, LX/9CL;->a:LX/9CP;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 1453778
    iget-object v0, p0, LX/9CL;->a:LX/9CP;

    iget-object v0, v0, LX/9CP;->g:LX/9FG;

    iget-object v1, p0, LX/9CL;->a:LX/9CP;

    invoke-static {v1}, LX/9CP;->T(LX/9CP;)LX/9FF;

    move-result-object v1

    .line 1453779
    iget-object v3, v0, LX/9FG;->a:LX/0if;

    sget-object v4, LX/0ig;->j:LX/0ih;

    iget-wide v5, v0, LX/9FG;->b:J

    const-string v7, "keyboard_down"

    invoke-virtual {v1}, LX/9FF;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {v3 .. v8}, LX/0if;->a(LX/0ih;JLjava/lang/String;Ljava/lang/String;)V

    .line 1453780
    iget-object v0, p0, LX/9CL;->a:LX/9CP;

    .line 1453781
    iput-boolean v2, v0, LX/9CP;->I:Z

    .line 1453782
    iget-object v0, p0, LX/9CL;->a:LX/9CP;

    iget-boolean v0, v0, LX/9CP;->F:Z

    if-eqz v0, :cond_1

    .line 1453783
    iget-object v0, p0, LX/9CL;->a:LX/9CP;

    .line 1453784
    iput-boolean v2, v0, LX/9CP;->F:Z

    .line 1453785
    :cond_0
    :goto_0
    return-void

    .line 1453786
    :cond_1
    iget-object v0, p0, LX/9CL;->a:LX/9CP;

    iget-boolean v0, v0, LX/9CP;->G:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/9CL;->a:LX/9CP;

    invoke-static {v0}, LX/9CP;->O(LX/9CP;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1453787
    iget-object v0, p0, LX/9CL;->a:LX/9CP;

    invoke-static {v0}, LX/9CP;->w(LX/9CP;)V

    goto :goto_0
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 1453788
    return-void
.end method

.method public final e_(I)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 1453789
    iget-object v0, p0, LX/9CL;->a:LX/9CP;

    iget-object v0, v0, LX/9CP;->g:LX/9FG;

    iget-object v1, p0, LX/9CL;->a:LX/9CP;

    invoke-static {v1}, LX/9CP;->T(LX/9CP;)LX/9FF;

    move-result-object v1

    .line 1453790
    iget-object v3, v0, LX/9FG;->a:LX/0if;

    sget-object v4, LX/0ig;->j:LX/0ih;

    iget-wide v5, v0, LX/9FG;->b:J

    const-string v7, "keyboard_up"

    invoke-virtual {v1}, LX/9FF;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {v3 .. v8}, LX/0if;->a(LX/0ih;JLjava/lang/String;Ljava/lang/String;)V

    .line 1453791
    iget-object v0, p0, LX/9CL;->a:LX/9CP;

    .line 1453792
    iput-boolean v2, v0, LX/9CP;->F:Z

    .line 1453793
    iget-object v0, p0, LX/9CL;->a:LX/9CP;

    iget-boolean v0, v0, LX/9CP;->I:Z

    if-eqz v0, :cond_0

    .line 1453794
    iget-object v0, p0, LX/9CL;->a:LX/9CP;

    .line 1453795
    iput-boolean v2, v0, LX/9CP;->I:Z

    .line 1453796
    iget-object v0, p0, LX/9CL;->a:LX/9CP;

    invoke-virtual {v0}, LX/9CP;->i()V

    .line 1453797
    :cond_0
    iget-object v0, p0, LX/9CL;->a:LX/9CP;

    invoke-static {v0}, LX/9CP;->x(LX/9CP;)V

    .line 1453798
    return-void
.end method
