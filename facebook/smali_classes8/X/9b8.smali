.class public LX/9b8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/facebook/graphql/model/GraphQLAlbum;

.field public c:LX/4BY;

.field public d:LX/4BY;

.field public final e:LX/9bY;

.field public final f:LX/9fy;

.field public final g:LX/1Ck;


# direct methods
.method public constructor <init>(LX/0Or;Lcom/facebook/graphql/model/GraphQLAlbum;LX/9bY;LX/9fy;LX/1Ck;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Landroid/content/Context;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLAlbum;",
            "LX/9bY;",
            "LX/9fy;",
            "LX/1Ck;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1514335
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1514336
    iput-object p1, p0, LX/9b8;->a:LX/0Or;

    .line 1514337
    iput-object p2, p0, LX/9b8;->b:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 1514338
    iput-object p3, p0, LX/9b8;->e:LX/9bY;

    .line 1514339
    iput-object p4, p0, LX/9b8;->f:LX/9fy;

    .line 1514340
    iput-object p5, p0, LX/9b8;->g:LX/1Ck;

    .line 1514341
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1514323
    invoke-virtual {p0}, LX/9b8;->b()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0300cf

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1514324
    const v0, 0x7f0d051a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    .line 1514325
    iget-object v2, p0, LX/9b8;->b:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1514326
    iget-object v2, p0, LX/9b8;->b:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbEditText;->setSelection(I)V

    .line 1514327
    invoke-virtual {p0}, LX/9b8;->b()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0038

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbEditText;->setTextColor(I)V

    .line 1514328
    new-instance v2, LX/0ju;

    invoke-virtual {p0}, LX/9b8;->b()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0811df

    invoke-virtual {v2, v3}, LX/0ju;->a(I)LX/0ju;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v1

    const v2, 0x7f0811de

    invoke-virtual {v1, v2, v4}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const v2, 0x7f0811e1

    invoke-virtual {v1, v2, v4}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1}, LX/0ju;->a()LX/2EJ;

    move-result-object v1

    .line 1514329
    new-instance v2, LX/9b3;

    invoke-direct {v2, p0, v0}, LX/9b3;-><init>(LX/9b8;Lcom/facebook/resources/ui/FbEditText;)V

    invoke-virtual {v1, v2}, LX/2EJ;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 1514330
    invoke-virtual {v1}, LX/2EJ;->show()V

    .line 1514331
    const/4 v2, -0x1

    invoke-virtual {v1, v2}, LX/2EJ;->a(I)Landroid/widget/Button;

    move-result-object v2

    .line 1514332
    new-instance v3, LX/9b5;

    invoke-direct {v3, p0, v0, v1}, LX/9b5;-><init>(LX/9b8;Lcom/facebook/resources/ui/FbEditText;LX/2EJ;)V

    move-object v0, v3

    .line 1514333
    invoke-virtual {v2, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1514334
    return-void
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 1514342
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/9b8;->a(Landroid/app/Activity;Z)V

    .line 1514343
    return-void
.end method

.method public final a(Landroid/app/Activity;Z)V
    .locals 6

    .prologue
    .line 1514318
    new-instance v1, LX/0ju;

    iget-object v0, p0, LX/9b8;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, LX/9b8;->b()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0811f8

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    invoke-virtual {p0}, LX/9b8;->b()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0811f9

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, LX/9b8;->b:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    const v1, 0x7f0811f7

    .line 1514319
    new-instance v2, LX/9b7;

    invoke-direct {v2, p0, p1, p2}, LX/9b7;-><init>(LX/9b8;Landroid/app/Activity;Z)V

    move-object v2, v2

    .line 1514320
    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f0811fa

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 1514321
    return-void
.end method

.method public final b()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1514322
    iget-object v0, p0, LX/9b8;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    return-object v0
.end method
