.class public final LX/9Fd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/9FA;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLComment;

.field public final synthetic c:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public final synthetic d:Lcom/facebook/feedback/ui/rows/CommentFailedStatusPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/rows/CommentFailedStatusPartDefinition;LX/9FA;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 0

    .prologue
    .line 1458780
    iput-object p1, p0, LX/9Fd;->d:Lcom/facebook/feedback/ui/rows/CommentFailedStatusPartDefinition;

    iput-object p2, p0, LX/9Fd;->a:LX/9FA;

    iput-object p3, p0, LX/9Fd;->b:Lcom/facebook/graphql/model/GraphQLComment;

    iput-object p4, p0, LX/9Fd;->c:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, -0x1af48b1d

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1458781
    iget-object v1, p0, LX/9Fd;->d:Lcom/facebook/feedback/ui/rows/CommentFailedStatusPartDefinition;

    iget-object v2, p0, LX/9Fd;->a:LX/9FA;

    iget-object v3, p0, LX/9Fd;->b:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v4, p0, LX/9Fd;->c:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1458782
    new-instance v6, LX/6WS;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-direct {v6, p0}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 1458783
    const p0, 0x7f110035

    invoke-virtual {v6, p0}, LX/5OM;->b(I)V

    .line 1458784
    new-instance p0, LX/9Fe;

    invoke-direct {p0, v1, v2, v3, v4}, LX/9Fe;-><init>(Lcom/facebook/feedback/ui/rows/CommentFailedStatusPartDefinition;LX/9FA;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1458785
    iput-object p0, v6, LX/5OM;->p:LX/5OO;

    .line 1458786
    invoke-virtual {v6, p1}, LX/0ht;->f(Landroid/view/View;)V

    .line 1458787
    const v1, -0x4995cc86

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
