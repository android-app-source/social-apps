.class public final LX/8oj;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1404684
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1404685
    return-void
.end method

.method public static a(Ljava/lang/CharSequence;LX/8oc;)I
    .locals 6

    .prologue
    .line 1404678
    const-string v0, "@\\[(\\d+):(\\d+:)?((\\w|\\s|[-\'])+)\\]"

    const/16 v1, 0x40

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    move-object v1, v0

    .line 1404679
    const/4 v0, 0x0

    .line 1404680
    :goto_0
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1404681
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->start()I

    move-result v2

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->end()I

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const/4 v4, 0x3

    invoke-virtual {v1, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1, v0, v2, v3, v4}, LX/8oc;->a(IILjava/lang/Long;Ljava/lang/String;)V

    .line 1404682
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->end()I

    move-result v0

    goto :goto_0

    .line 1404683
    :cond_0
    return v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;LX/0Rf;)LX/0Px;
    .locals 8
    .param p1    # LX/0Rf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLTextWithEntities;",
            "LX/0Rf",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1404669
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1404670
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1404671
    :goto_0
    return-object v0

    .line 1404672
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1404673
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_2

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    .line 1404674
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v5

    invoke-static {v5, p1}, LX/8oj;->a(LX/171;LX/0Rf;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1404675
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntity;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1404676
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1404677
    :cond_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/175;)Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 1404648
    if-eqz p0, :cond_0

    invoke-interface {p0}, LX/175;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 1404649
    :goto_0
    return-object v0

    .line 1404650
    :cond_1
    invoke-interface {p0}, LX/175;->b()LX/0Px;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1404651
    invoke-interface {p0}, LX/175;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1404652
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-interface {p0}, LX/175;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1404653
    invoke-interface {p0}, LX/175;->b()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1404654
    new-instance v3, LX/8oh;

    invoke-direct {v3}, LX/8oh;-><init>()V

    invoke-static {v0, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1404655
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1W5;

    .line 1404656
    invoke-interface {v0}, LX/1W5;->a()LX/171;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, LX/1W5;->a()LX/171;

    move-result-object v4

    invoke-static {v4, v1}, LX/8oj;->a(LX/171;LX/0Rf;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1404657
    :try_start_0
    invoke-interface {p0}, LX/175;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0}, LX/1W5;->c()I

    move-result v5

    invoke-interface {v0}, LX/1W5;->b()I

    move-result v6

    invoke-static {v4, v5, v6}, LX/1yM;->a(Ljava/lang/String;II)LX/1yN;

    move-result-object v4

    .line 1404658
    iget v5, v4, LX/1yN;->a:I

    move v5, v5

    .line 1404659
    iget v6, v4, LX/1yN;->a:I

    move v6, v6

    .line 1404660
    iget v7, v4, LX/1yN;->b:I

    move v7, v7

    .line 1404661
    add-int/2addr v6, v7

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "@["

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, LX/1W5;->a()LX/171;

    move-result-object v0

    invoke-interface {v0}, LX/171;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, ":"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p0}, LX/175;->a()Ljava/lang/String;

    move-result-object v7

    .line 1404662
    iget v8, v4, LX/1yN;->a:I

    move v8, v8

    .line 1404663
    iget v9, v4, LX/1yN;->a:I

    move v9, v9

    .line 1404664
    iget v10, v4, LX/1yN;->b:I

    move v4, v10

    .line 1404665
    add-int/2addr v4, v9

    invoke-virtual {v7, v8, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "]"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v5, v6, v0}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch LX/47A; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1404666
    :catch_0
    move-exception v0

    .line 1404667
    const-string v4, "MentionsUtils"

    invoke-virtual {v0}, LX/47A;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1404668
    :cond_4
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public static a(Landroid/text/Editable;Z)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 1404590
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2, p0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1404591
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    const-class v3, LX/7Gl;

    invoke-virtual {v2, v1, v0, v3}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7Gl;

    .line 1404592
    if-eqz v0, :cond_0

    array-length v3, v0

    if-nez v3, :cond_2

    .line 1404593
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 1404594
    :goto_0
    return-object v0

    .line 1404595
    :cond_1
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1404596
    :cond_2
    array-length v3, v0

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v0, v1

    .line 1404597
    const-string v5, "@[%d:%s]"

    invoke-virtual {v4}, LX/7Gl;->b()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    .line 1404598
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 1404599
    iget-object v7, v4, LX/7Gl;->b:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result p0

    const/4 v7, 0x0

    move v8, v7

    :goto_2
    if-ge v8, p0, :cond_3

    iget-object v7, v4, LX/7Gl;->b:Ljava/util/ArrayList;

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/7Gk;

    .line 1404600
    iget-object p1, v7, LX/7Gk;->b:Ljava/lang/String;

    move-object v7, p1

    .line 1404601
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1404602
    const/16 v7, 0x20

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1404603
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto :goto_2

    .line 1404604
    :cond_3
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    move-object v7, v7

    .line 1404605
    invoke-static {v5, v6, v7}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 1404606
    invoke-virtual {v2, v4}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    move-result v6

    invoke-virtual {v2, v4}, Landroid/text/SpannableStringBuilder;->getSpanEnd(Ljava/lang/Object;)I

    move-result v4

    invoke-virtual {v2, v6, v4, v5}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1404607
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1404608
    :cond_4
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1404643
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1404644
    new-instance v1, LX/8og;

    invoke-direct {v1, v0, p0}, LX/8og;-><init>(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    .line 1404645
    invoke-static {p0, v1}, LX/8oj;->a(Ljava/lang/CharSequence;LX/8oc;)I

    move-result v1

    .line 1404646
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    invoke-interface {p0, v1, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 1404647
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/text/Editable;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/text/Editable;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLEntityAtRange;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1404616
    invoke-interface {p0}, Landroid/text/Editable;->length()I

    move-result v0

    const-class v1, LX/7Gl;

    invoke-interface {p0, v2, v0, v1}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7Gl;

    .line 1404617
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 1404618
    if-eqz v0, :cond_0

    .line 1404619
    array-length v5, v0

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_0

    aget-object v1, v0, v3

    .line 1404620
    invoke-virtual {v1, p0}, LX/7Gl;->a(Landroid/text/Editable;)I

    move-result v6

    .line 1404621
    :try_start_0
    invoke-virtual {v1}, LX/7Gl;->b()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1}, LX/7Gl;->c()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    invoke-static {v7, v8}, LX/16z;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLObjectType;)Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v7

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v9, LX/1yN;

    invoke-virtual {v1, p0}, LX/7Gl;->b(Landroid/text/Editable;)I

    move-result v1

    sub-int/2addr v1, v6

    invoke-direct {v9, v6, v1}, LX/1yN;-><init>(II)V

    invoke-static {v8, v9}, LX/1yM;->a(Ljava/lang/String;LX/1yN;)LX/1yL;

    move-result-object v1

    invoke-static {v7, v1}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLEntity;LX/1yL;)Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    move-result-object v1

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch LX/47A; {:try_start_0 .. :try_end_0} :catch_0

    .line 1404622
    :goto_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    .line 1404623
    :catch_0
    move-exception v1

    .line 1404624
    const-string v6, "MentionsUtils"

    invoke-virtual {v1}, LX/47A;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v1}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1404625
    :cond_0
    invoke-interface {p0}, Landroid/text/Editable;->length()I

    move-result v0

    const-class v1, LX/7Gi;

    invoke-interface {p0, v2, v0, v1}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7Gi;

    .line 1404626
    if-eqz v0, :cond_2

    .line 1404627
    array-length v3, v0

    :goto_2
    if-ge v2, v3, :cond_2

    aget-object v1, v0, v2

    .line 1404628
    invoke-virtual {v1, p0}, LX/7Gi;->a(Landroid/text/Editable;)I

    move-result v5

    .line 1404629
    invoke-virtual {v1, p0}, LX/7Gi;->b(Landroid/text/Editable;)I

    move-result v1

    .line 1404630
    sub-int v6, v1, v5

    const/4 v7, 0x1

    if-le v6, v7, :cond_1

    .line 1404631
    add-int/lit8 v6, v5, 0x1

    invoke-interface {p0, v6, v1}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1404632
    :try_start_1
    new-instance v7, LX/170;

    invoke-direct {v7}, LX/170;-><init>()V

    .line 1404633
    iput-object v6, v7, LX/170;->X:Ljava/lang/String;

    .line 1404634
    move-object v6, v7

    .line 1404635
    new-instance v7, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v8, -0x7333ac54

    invoke-direct {v7, v8}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 1404636
    iput-object v7, v6, LX/170;->ab:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1404637
    move-object v6, v6

    .line 1404638
    invoke-virtual {v6}, LX/170;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v6

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    new-instance v8, LX/1yN;

    sub-int/2addr v1, v5

    invoke-direct {v8, v5, v1}, LX/1yN;-><init>(II)V

    invoke-static {v7, v8}, LX/1yM;->a(Ljava/lang/String;LX/1yN;)LX/1yL;

    move-result-object v1

    invoke-static {v6, v1}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLEntity;LX/1yL;)Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    move-result-object v1

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch LX/47A; {:try_start_1 .. :try_end_1} :catch_1

    .line 1404639
    :cond_1
    :goto_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    .line 1404640
    :catch_1
    move-exception v1

    .line 1404641
    const-string v5, "MentionsUtils"

    invoke-virtual {v1}, LX/47A;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v1}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 1404642
    :cond_2
    return-object v4
.end method

.method private static a(LX/171;LX/0Rf;)Z
    .locals 2
    .param p1    # LX/0Rf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/171;",
            "LX/0Rf",
            "<",
            "Ljava/lang/Integer;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1404609
    invoke-interface {p0}, LX/171;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1404610
    :cond_0
    :goto_0
    return v0

    .line 1404611
    :cond_1
    if-eqz p1, :cond_2

    invoke-interface {p0}, LX/171;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1404612
    :cond_2
    invoke-interface {p0}, LX/171;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    .line 1404613
    :sswitch_0
    :try_start_0
    invoke-interface {p0}, LX/171;->e()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1404614
    const/4 v0, 0x1

    goto :goto_0

    .line 1404615
    :catch_0
    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x25d6af -> :sswitch_0
        0x285feb -> :sswitch_0
        0x41e065f -> :sswitch_0
    .end sparse-switch
.end method
