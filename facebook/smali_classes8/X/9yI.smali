.class public LX/9yI;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/9lb;

.field public final c:LX/1Ck;

.field private final d:LX/0SI;

.field private final e:LX/0tX;

.field public final f:LX/03V;

.field public g:Lcom/facebook/reportingcoordinator/ReportingCoordinatorDialogFragment;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1596069
    const-class v0, LX/9yI;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/9yI;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/9lb;LX/1Ck;LX/0SI;LX/0tX;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1596061
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1596062
    iput-object p1, p0, LX/9yI;->b:LX/9lb;

    .line 1596063
    iput-object p2, p0, LX/9yI;->c:LX/1Ck;

    .line 1596064
    iput-object p3, p0, LX/9yI;->d:LX/0SI;

    .line 1596065
    iput-object p4, p0, LX/9yI;->e:LX/0tX;

    .line 1596066
    iput-object p5, p0, LX/9yI;->f:LX/03V;

    .line 1596067
    return-void
.end method

.method public static a(LX/0QB;)LX/9yI;
    .locals 1

    .prologue
    .line 1596068
    invoke-static {p0}, LX/9yI;->b(LX/0QB;)LX/9yI;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0gc;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1596044
    if-nez p1, :cond_0

    .line 1596045
    new-instance v1, Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;

    invoke-direct {v1}, Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;-><init>()V

    .line 1596046
    const-wide/16 v3, -0x1

    invoke-static {p2, p3, v3, v4}, Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;->a(Ljava/lang/String;Ljava/lang/String;J)Landroid/os/Bundle;

    move-result-object v2

    .line 1596047
    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1596048
    move-object v0, v1

    .line 1596049
    :goto_0
    invoke-virtual {v0, p0, p4}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1596050
    return-void

    .line 1596051
    :cond_0
    new-instance v0, LX/9Vd;

    invoke-direct {v0}, LX/9Vd;-><init>()V

    .line 1596052
    iget-object v1, v0, LX/9Vd;->a:Landroid/os/Bundle;

    const-string v2, "tracking_data"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1596053
    move-object v0, v0

    .line 1596054
    iget-object v1, v0, LX/9Vd;->b:Landroid/os/Bundle;

    move-object v0, v1

    .line 1596055
    new-instance v1, Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;

    invoke-direct {v1}, Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;-><init>()V

    .line 1596056
    const-wide/16 v3, -0x1

    invoke-static {p2, p3, v3, v4}, Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;->a(Ljava/lang/String;Ljava/lang/String;J)Landroid/os/Bundle;

    move-result-object v2

    .line 1596057
    const-string v3, "extras"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1596058
    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1596059
    move-object v0, v1

    .line 1596060
    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/9yI;
    .locals 6

    .prologue
    .line 1596042
    new-instance v0, LX/9yI;

    invoke-static {p0}, LX/9lb;->b(LX/0QB;)LX/9lb;

    move-result-object v1

    check-cast v1, LX/9lb;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v2

    check-cast v2, LX/1Ck;

    invoke-static {p0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v3

    check-cast v3, LX/0SI;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-direct/range {v0 .. v5}, LX/9yI;-><init>(LX/9lb;LX/1Ck;LX/0SI;LX/0tX;LX/03V;)V

    .line 1596043
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;LX/9lZ;)V
    .locals 4
    .param p4    # LX/9lZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;",
            ">;",
            "Ljava/lang/String;",
            "LX/9lZ;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1596024
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1596025
    check-cast v0, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    .line 1596026
    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_0

    move-object v1, v0

    .line 1596027
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1596028
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->bh()Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->bh()Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1596029
    invoke-interface {v0}, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;->o()Ljava/lang/String;

    move-result-object v0

    .line 1596030
    new-instance v1, LX/9mA;

    invoke-direct {v1}, LX/9mA;-><init>()V

    .line 1596031
    iput-object v0, v1, LX/9mA;->a:Ljava/lang/String;

    .line 1596032
    move-object v1, v1

    .line 1596033
    iput-object p3, v1, LX/9mA;->b:Ljava/lang/String;

    .line 1596034
    move-object v1, v1

    .line 1596035
    iput-object p4, v1, LX/9mA;->c:LX/9lZ;

    .line 1596036
    move-object v1, v1

    .line 1596037
    invoke-virtual {v1}, LX/9mA;->a()Lcom/facebook/rapidreporting/ui/DialogConfig;

    move-result-object v1

    .line 1596038
    invoke-static {p1}, LX/4tq;->a(Landroid/content/Context;)LX/0ew;

    move-result-object v2

    invoke-interface {v2}, LX/0ew;->iC_()LX/0gc;

    move-result-object v2

    .line 1596039
    iget-object v3, p0, LX/9yI;->b:LX/9lb;

    invoke-virtual {v3, v2, v1}, LX/9lb;->a(LX/0gc;Lcom/facebook/rapidreporting/ui/DialogConfig;)V

    .line 1596040
    :goto_0
    return-void

    .line 1596041
    :cond_0
    invoke-static {p1}, LX/4tq;->a(Landroid/content/Context;)LX/0ew;

    move-result-object v1

    invoke-interface {v1}, LX/0ew;->iC_()LX/0gc;

    move-result-object v1

    invoke-static {p2}, LX/181;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v2

    invoke-virtual {v2}, LX/162;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0}, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;->o()Ljava/lang/String;

    move-result-object v0

    sget-object v3, LX/9yI;->a:Ljava/lang/String;

    invoke-static {v1, v2, v0, p3, v3}, LX/9yI;->a(LX/0gc;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/9lZ;)V
    .locals 13
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1596010
    invoke-static {p1}, LX/4tq;->a(Landroid/content/Context;)LX/0ew;

    move-result-object v2

    invoke-interface {v2}, LX/0ew;->iC_()LX/0gc;

    move-result-object v5

    .line 1596011
    invoke-static {}, Lcom/facebook/reportingcoordinator/ReportingCoordinatorDialogFragment;->a()Lcom/facebook/reportingcoordinator/ReportingCoordinatorDialogFragment;

    move-result-object v2

    iput-object v2, p0, LX/9yI;->g:Lcom/facebook/reportingcoordinator/ReportingCoordinatorDialogFragment;

    .line 1596012
    iget-object v2, p0, LX/9yI;->g:Lcom/facebook/reportingcoordinator/ReportingCoordinatorDialogFragment;

    invoke-virtual {v2, p0}, Lcom/facebook/reportingcoordinator/ReportingCoordinatorDialogFragment;->a(LX/9yI;)V

    .line 1596013
    iget-object v2, p0, LX/9yI;->g:Lcom/facebook/reportingcoordinator/ReportingCoordinatorDialogFragment;

    sget-object v3, LX/9yI;->a:Ljava/lang/String;

    invoke-virtual {v2, v5, v3}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1596014
    new-instance v2, LX/9mA;

    invoke-direct {v2}, LX/9mA;-><init>()V

    invoke-virtual {v2, p2}, LX/9mA;->a(Ljava/lang/String;)LX/9mA;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, LX/9mA;->b(Ljava/lang/String;)LX/9mA;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, LX/9mA;->a(LX/9lZ;)LX/9mA;

    move-result-object v2

    invoke-virtual {v2}, LX/9mA;->a()Lcom/facebook/rapidreporting/ui/DialogConfig;

    move-result-object v2

    .line 1596015
    new-instance v4, Lcom/facebook/rapidreporting/ui/DialogStateData;

    invoke-direct {v4, v2}, Lcom/facebook/rapidreporting/ui/DialogStateData;-><init>(Lcom/facebook/rapidreporting/ui/DialogConfig;)V

    .line 1596016
    invoke-static {}, LX/9lx;->a()LX/9lw;

    move-result-object v2

    .line 1596017
    const-string v3, "object_id"

    invoke-virtual {v2, v3, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1596018
    if-eqz p4, :cond_0

    .line 1596019
    const-string v3, "location"

    move-object/from16 v0, p4

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1596020
    :cond_0
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    sget-object v3, LX/0zS;->a:LX/0zS;

    invoke-virtual {v2, v3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v2

    const-wide/16 v6, 0x78

    invoke-virtual {v2, v6, v7}, LX/0zO;->a(J)LX/0zO;

    move-result-object v2

    .line 1596021
    iget-object v3, p0, LX/9yI;->d:LX/0SI;

    invoke-interface {v3}, LX/0SI;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0zO;->a(Lcom/facebook/auth/viewercontext/ViewerContext;)LX/0zO;

    .line 1596022
    iget-object v10, p0, LX/9yI;->c:LX/1Ck;

    sget-object v11, LX/9yH;->FETCH_METADATA:LX/9yH;

    iget-object v3, p0, LX/9yI;->e:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v12

    new-instance v2, LX/9yG;

    move-object v3, p0

    move-object/from16 v6, p3

    move-object v7, p2

    move-object/from16 v8, p4

    move-object v9, p1

    invoke-direct/range {v2 .. v9}, LX/9yG;-><init>(LX/9yI;Lcom/facebook/rapidreporting/ui/DialogStateData;LX/0gc;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    invoke-static {v2}, LX/0Vd;->of(LX/0TF;)LX/0Vd;

    move-result-object v2

    invoke-virtual {v10, v11, v12, v2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1596023
    return-void
.end method
