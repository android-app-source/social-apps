.class public final LX/8if;
.super Landroid/os/Handler;
.source ""


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Z

.field public final synthetic c:Landroid/content/Context;

.field public final synthetic d:LX/8iQ;

.field private e:Landroid/media/MediaPlayer;


# direct methods
.method public constructor <init>(Landroid/os/Looper;IZLandroid/content/Context;LX/8iQ;)V
    .locals 0

    .prologue
    .line 1391854
    iput p2, p0, LX/8if;->a:I

    iput-boolean p3, p0, LX/8if;->b:Z

    iput-object p4, p0, LX/8if;->c:Landroid/content/Context;

    iput-object p5, p0, LX/8if;->d:LX/8iQ;

    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 6

    .prologue
    .line 1391855
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 1391856
    :cond_0
    :goto_0
    return-void

    .line 1391857
    :pswitch_0
    iget-object v0, p0, LX/8if;->e:Landroid/media/MediaPlayer;

    if-nez v0, :cond_1

    .line 1391858
    iget v0, p0, LX/8if;->a:I

    iget-boolean v1, p0, LX/8if;->b:Z

    iget-object v2, p0, LX/8if;->c:Landroid/content/Context;

    iget-object v3, p0, LX/8if;->d:LX/8iQ;

    .line 1391859
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    if-ne v4, v5, :cond_5

    const/4 v4, 0x1

    :goto_1
    move v4, v4

    .line 1391860
    if-nez v4, :cond_4

    const/4 v4, 0x1

    :goto_2
    const-string v5, "Trying to play a sound on the UI Thread"

    invoke-static {v4, v5}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1391861
    invoke-static {v2, v0}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v4

    .line 1391862
    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 1391863
    invoke-virtual {v4, v1}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 1391864
    invoke-virtual {v3, v0}, LX/8iQ;->a(I)F

    move-result v5

    .line 1391865
    invoke-virtual {v4, v5, v5}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 1391866
    move-object v0, v4

    .line 1391867
    iput-object v0, p0, LX/8if;->e:Landroid/media/MediaPlayer;

    .line 1391868
    :cond_1
    iget-object v0, p0, LX/8if;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1391869
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 1391870
    iget-object v0, p0, LX/8if;->e:Landroid/media/MediaPlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 1391871
    :cond_2
    iget-object v0, p0, LX/8if;->d:LX/8iQ;

    iget v1, p0, LX/8if;->a:I

    invoke-virtual {v0, v1}, LX/8iQ;->a(I)F

    move-result v0

    .line 1391872
    iget-object v1, p0, LX/8if;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v1, v0, v0}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 1391873
    iget-object v0, p0, LX/8if;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    goto :goto_0

    .line 1391874
    :pswitch_1
    iget-object v0, p0, LX/8if;->e:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 1391875
    iget-object v0, p0, LX/8if;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1391876
    iget-object v0, p0, LX/8if;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    goto :goto_0

    .line 1391877
    :pswitch_2
    iget-object v0, p0, LX/8if;->e:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 1391878
    iget-object v0, p0, LX/8if;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1391879
    iget-object v0, p0, LX/8if;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 1391880
    :cond_3
    iget-object v0, p0, LX/8if;->e:Landroid/media/MediaPlayer;

    .line 1391881
    if-nez v0, :cond_6

    .line 1391882
    :goto_3
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    goto/16 :goto_0

    .line 1391883
    :cond_4
    const/4 v4, 0x0

    goto :goto_2

    :cond_5
    const/4 v4, 0x0

    goto :goto_1

    .line 1391884
    :cond_6
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1391885
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 1391886
    :cond_7
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 1391887
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
