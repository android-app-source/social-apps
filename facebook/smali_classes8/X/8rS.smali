.class public final LX/8rS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/ufiservices/ui/CommentEditView;


# direct methods
.method public constructor <init>(Lcom/facebook/ufiservices/ui/CommentEditView;)V
    .locals 0

    .prologue
    .line 1408870
    iput-object p1, p0, LX/8rS;->a:Lcom/facebook/ufiservices/ui/CommentEditView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 1408867
    iget-object v0, p0, LX/8rS;->a:Lcom/facebook/ufiservices/ui/CommentEditView;

    iget-object v1, v0, Lcom/facebook/ufiservices/ui/CommentEditView;->h:Landroid/widget/Button;

    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8rS;->a:Lcom/facebook/ufiservices/ui/CommentEditView;

    invoke-static {v0}, Lcom/facebook/ufiservices/ui/CommentEditView;->c(Lcom/facebook/ufiservices/ui/CommentEditView;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1408868
    return-void

    .line 1408869
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1408865
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1408866
    return-void
.end method
