.class public final LX/ARF;
.super LX/79T;
.source ""


# instance fields
.field public final synthetic a:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic b:Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0

    .prologue
    .line 1672856
    iput-object p1, p0, LX/ARF;->b:Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;

    iput-object p2, p0, LX/ARF;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, LX/79T;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1672854
    iget-object v0, p0, LX/ARF;->a:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v1, 0x0

    const v2, 0x81c3827

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 1672855
    return-void
.end method

.method public final a([Ljava/lang/String;[Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1672851
    iget-object v0, p0, LX/ARF;->b:Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;

    iget-object v0, v0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->q:LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, LX/ARF;->b:Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;

    invoke-virtual {v2}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0814a5    # 1.808822E38f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1672852
    iget-object v0, p0, LX/ARF;->a:Lcom/google/common/util/concurrent/SettableFuture;

    new-instance v1, Ljava/lang/SecurityException;

    const-string v2, "Permission denied"

    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 1672853
    return-void
.end method
