.class public LX/9fL;
.super LX/1a1;
.source ""


# instance fields
.field public final synthetic l:Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;

.field public m:Lcom/facebook/resources/ui/FbTextView;

.field private n:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1521752
    iput-object p1, p0, LX/9fL;->l:Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;

    .line 1521753
    invoke-direct {p0, p2}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 1521754
    iput-object p2, p0, LX/9fL;->n:Landroid/view/View;

    .line 1521755
    const v0, 0x7f0d0d14

    invoke-static {p2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/9fL;->m:Lcom/facebook/resources/ui/FbTextView;

    .line 1521756
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;LX/33B;)LX/1aZ;
    .locals 4
    .param p2    # LX/33B;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1521757
    invoke-static {p1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    .line 1521758
    iput-object p2, v0, LX/1bX;->j:LX/33B;

    .line 1521759
    move-object v0, v0

    .line 1521760
    new-instance v1, LX/1o9;

    iget-object v2, p0, LX/9fL;->l:Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;

    iget v2, v2, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;->k:I

    iget-object v3, p0, LX/9fL;->l:Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;

    iget v3, v3, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;->k:I

    invoke-direct {v1, v2, v3}, LX/1o9;-><init>(II)V

    .line 1521761
    iput-object v1, v0, LX/1bX;->c:LX/1o9;

    .line 1521762
    move-object v0, v0

    .line 1521763
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/1bX;->a(Z)LX/1bX;

    move-result-object v0

    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 1521764
    iget-object v1, p0, LX/9fL;->l:Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;->f:LX/1Ad;

    invoke-virtual {v1}, LX/1Ad;->o()LX/1Ad;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    sget-object v1, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 1521765
    return-object v0
.end method

.method public a(Lcom/facebook/photos/creativeediting/model/SwipeableParams;)V
    .locals 2

    .prologue
    .line 1521766
    iget-object v0, p1, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1521767
    iget-object v1, p0, LX/9fL;->m:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1521768
    iget-object v0, p0, LX/9fL;->n:Landroid/view/View;

    .line 1521769
    iget-object v1, p1, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1521770
    new-instance p1, LX/9fK;

    invoke-direct {p1, p0, v1}, LX/9fK;-><init>(LX/9fL;Ljava/lang/String;)V

    move-object v1, p1

    .line 1521771
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1521772
    invoke-virtual {p0}, LX/9fL;->x()V

    .line 1521773
    return-void
.end method

.method public final b(Z)LX/1af;
    .locals 3

    .prologue
    .line 1521774
    if-eqz p1, :cond_0

    sget-object v0, LX/1Up;->g:LX/1Up;

    .line 1521775
    :goto_0
    new-instance v1, LX/1Uo;

    iget-object v2, p0, LX/9fL;->l:Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;

    iget-object v2, v2, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v1, v0}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v0

    const/4 v1, 0x0

    .line 1521776
    iput v1, v0, LX/1Uo;->d:I

    .line 1521777
    move-object v0, v0

    .line 1521778
    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 1521779
    return-object v0

    .line 1521780
    :cond_0
    sget-object v0, LX/1Up;->c:LX/1Up;

    goto :goto_0
.end method

.method public final x()V
    .locals 3

    .prologue
    .line 1521781
    iget-object v0, p0, LX/9fL;->m:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v0, :cond_0

    .line 1521782
    iget-object v0, p0, LX/9fL;->m:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/9fL;->l:Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;->c:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1521783
    iget-object v0, p0, LX/9fL;->m:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/9fL;->l:Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00fc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1521784
    :cond_0
    return-void
.end method
