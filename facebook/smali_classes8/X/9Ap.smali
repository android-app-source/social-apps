.class public LX/9Ap;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1450476
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/8s1;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 1450477
    const-string v0, "story_feedback_flyout"

    invoke-interface {p1, p0, v0, p2}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;LX/8s1;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/feedback/reactions/ui/ReactorsListFragment;
    .locals 2

    .prologue
    .line 1450478
    new-instance v0, LX/8qQ;

    invoke-direct {v0}, LX/8qQ;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v1

    .line 1450479
    iput-object v1, v0, LX/8qQ;->a:Ljava/lang/String;

    .line 1450480
    move-object v0, v0

    .line 1450481
    const-string v1, "story_feedback_flyout"

    .line 1450482
    iput-object v1, v0, LX/8qQ;->l:Ljava/lang/String;

    .line 1450483
    move-object v0, v0

    .line 1450484
    const/4 v1, 0x1

    .line 1450485
    iput-boolean v1, v0, LX/8qQ;->i:Z

    .line 1450486
    move-object v0, v0

    .line 1450487
    invoke-virtual {v0}, LX/8qQ;->a()Lcom/facebook/ufiservices/flyout/ProfileListParams;

    move-result-object v0

    .line 1450488
    new-instance v1, Lcom/facebook/feedback/reactions/ui/ReactorsListFragment;

    invoke-direct {v1}, Lcom/facebook/feedback/reactions/ui/ReactorsListFragment;-><init>()V

    .line 1450489
    invoke-virtual {v0}, Lcom/facebook/ufiservices/flyout/ProfileListParams;->m()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1450490
    return-object v1
.end method
