.class public LX/A6X;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/A6X;


# instance fields
.field private final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1624367
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1624368
    iput-object p1, p0, LX/A6X;->a:LX/0Zb;

    .line 1624369
    return-void
.end method

.method public static a(LX/0QB;)LX/A6X;
    .locals 4

    .prologue
    .line 1624370
    sget-object v0, LX/A6X;->b:LX/A6X;

    if-nez v0, :cond_1

    .line 1624371
    const-class v1, LX/A6X;

    monitor-enter v1

    .line 1624372
    :try_start_0
    sget-object v0, LX/A6X;->b:LX/A6X;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1624373
    if-eqz v2, :cond_0

    .line 1624374
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1624375
    new-instance p0, LX/A6X;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/A6X;-><init>(LX/0Zb;)V

    .line 1624376
    move-object v0, p0

    .line 1624377
    sput-object v0, LX/A6X;->b:LX/A6X;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1624378
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1624379
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1624380
    :cond_1
    sget-object v0, LX/A6X;->b:LX/A6X;

    return-object v0

    .line 1624381
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1624382
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/A6X;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1624383
    iget-object v0, p0, LX/A6X;->a:LX/0Zb;

    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "transliteration_composer"

    .line 1624384
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1624385
    move-object v1, v1

    .line 1624386
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1624387
    return-void
.end method

.method public static a(LX/A6X;Ljava/lang/String;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1624388
    iget-object v0, p0, LX/A6X;->a:LX/0Zb;

    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "transliteration_composer"

    .line 1624389
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1624390
    move-object v1, v1

    .line 1624391
    invoke-virtual {v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1624392
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1624393
    sget-object v0, LX/A6W;->HELP_OPENED:LX/A6W;

    iget-object v0, v0, LX/A6W;->eventName:Ljava/lang/String;

    invoke-static {p0, v0}, LX/A6X;->a(LX/A6X;Ljava/lang/String;)V

    .line 1624394
    return-void
.end method

.method public final a(Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    .line 1624395
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1624396
    const-string v1, "old"

    invoke-virtual {p1}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1624397
    const-string v1, "new"

    invoke-virtual {p2}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1624398
    sget-object v1, LX/A6W;->PREFERENCE_CHANGED:LX/A6W;

    iget-object v1, v1, LX/A6W;->eventName:Ljava/lang/String;

    invoke-static {p0, v1, v0}, LX/A6X;->a(LX/A6X;Ljava/lang/String;Ljava/util/Map;)V

    .line 1624399
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;ILX/A6N;I)V
    .locals 3

    .prologue
    .line 1624400
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1624401
    const-string v1, "source"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1624402
    const-string v1, "target"

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1624403
    const-string v1, "algorithm"

    invoke-virtual {p4}, LX/A6N;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1624404
    const-string v1, "version"

    invoke-static {p5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1624405
    const-string v1, "index"

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1624406
    sget-object v1, LX/A6W;->WORD_TRANSLITERATED:LX/A6W;

    iget-object v1, v1, LX/A6W;->eventName:Ljava/lang/String;

    invoke-static {p0, v1, v0}, LX/A6X;->a(LX/A6X;Ljava/lang/String;Ljava/util/Map;)V

    .line 1624407
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1624408
    sget-object v0, LX/A6W;->HELP_CANCELLED:LX/A6W;

    iget-object v0, v0, LX/A6W;->eventName:Ljava/lang/String;

    invoke-static {p0, v0}, LX/A6X;->a(LX/A6X;Ljava/lang/String;)V

    .line 1624409
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 1624410
    sget-object v0, LX/A6W;->DOWNLOAD_FAILED:LX/A6W;

    iget-object v0, v0, LX/A6W;->eventName:Ljava/lang/String;

    invoke-static {p0, v0}, LX/A6X;->a(LX/A6X;Ljava/lang/String;)V

    .line 1624411
    return-void
.end method
