.class public LX/APz;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Ljava/lang/Boolean;)V
    .locals 0
    .param p2    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1670634
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1670635
    iput-object p1, p0, LX/APz;->a:Landroid/content/res/Resources;

    .line 1670636
    iput-object p2, p0, LX/APz;->b:Ljava/lang/Boolean;

    .line 1670637
    return-void
.end method

.method public static a(LX/0QB;)LX/APz;
    .locals 1

    .prologue
    .line 1670638
    invoke-static {p0}, LX/APz;->b(LX/0QB;)LX/APz;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/APz;
    .locals 3

    .prologue
    .line 1670639
    new-instance v2, LX/APz;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-static {p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-direct {v2, v0, v1}, LX/APz;-><init>(Landroid/content/res/Resources;Ljava/lang/Boolean;)V

    .line 1670640
    return-object v2
.end method


# virtual methods
.method public final a(LX/AQ4;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Lcom/facebook/ipc/composer/intent/ComposerTargetData;Lcom/facebook/ipc/composer/intent/ComposerPageData;)Ljava/lang/String;
    .locals 6
    .param p4    # Lcom/facebook/ipc/composer/intent/ComposerPageData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/AQ4",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/ipc/composer/intent/ComposerConfiguration;",
            "Lcom/facebook/ipc/composer/intent/ComposerTargetData;",
            "Lcom/facebook/ipc/composer/intent/ComposerPageData;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1670641
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, LX/APz;->a(LX/AQ4;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Lcom/facebook/ipc/composer/intent/ComposerTargetData;Lcom/facebook/ipc/composer/intent/ComposerPageData;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/AQ4;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Lcom/facebook/ipc/composer/intent/ComposerTargetData;Lcom/facebook/ipc/composer/intent/ComposerPageData;Z)Ljava/lang/String;
    .locals 5
    .param p4    # Lcom/facebook/ipc/composer/intent/ComposerPageData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/AQ4",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/ipc/composer/intent/ComposerConfiguration;",
            "Lcom/facebook/ipc/composer/intent/ComposerTargetData;",
            "Lcom/facebook/ipc/composer/intent/ComposerPageData;",
            "Z)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 1670642
    if-eqz p1, :cond_0

    invoke-interface {p1}, LX/AQ4;->a()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1670643
    invoke-interface {p1}, LX/AQ4;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1670644
    :goto_0
    return-object v0

    .line 1670645
    :cond_0
    invoke-virtual {p2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isGroupMemberBioPost()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1670646
    iget-object v0, p0, LX/APz;->a:Landroid/content/res/Resources;

    const v1, 0x7f081404

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1670647
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1670648
    iget-object v0, p0, LX/APz;->a:Landroid/content/res/Resources;

    const v1, 0x7f081403

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1670649
    :cond_2
    sget-object v0, LX/APy;->a:[I

    iget-object v1, p3, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    invoke-virtual {v1}, LX/2rw;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1670650
    iget-object v0, p3, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    goto :goto_0

    .line 1670651
    :pswitch_0
    invoke-virtual {p2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getComposerType()LX/2rt;

    move-result-object v0

    sget-object v1, LX/2rt;->SHARE:LX/2rt;

    if-ne v0, v1, :cond_4

    .line 1670652
    iget-object v1, p0, LX/APz;->a:Landroid/content/res/Resources;

    iget-object v0, p0, LX/APz;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f08140b

    :goto_1
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    const v0, 0x7f08140a

    goto :goto_1

    .line 1670653
    :cond_4
    if-eqz p5, :cond_5

    iget-object v1, p0, LX/APz;->a:Landroid/content/res/Resources;

    const v0, 0x7f081491

    :goto_2
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_5
    iget-object v1, p0, LX/APz;->a:Landroid/content/res/Resources;

    iget-object v0, p0, LX/APz;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6

    const v0, 0x7f0813ff

    goto :goto_2

    :cond_6
    const v0, 0x7f0813fe

    goto :goto_2

    .line 1670654
    :pswitch_1
    iget-object v0, p0, LX/APz;->a:Landroid/content/res/Resources;

    const v1, 0x7f081448

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p3, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1670655
    :pswitch_2
    if-eqz p4, :cond_7

    invoke-virtual {p4}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getPostAsPageViewerContext()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, LX/APz;->a:Landroid/content/res/Resources;

    const v1, 0x7f081446

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p3, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, LX/APz;->a:Landroid/content/res/Resources;

    const v1, 0x7f08144c

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p3, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1670656
    :pswitch_3
    iget-object v0, p0, LX/APz;->a:Landroid/content/res/Resources;

    const v1, 0x7f08144b

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p3, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
