.class public LX/ASB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0io;",
        ":",
        "Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData$ProvidesAudienceEducatorData;",
        ":",
        "LX/0j0;",
        ":",
        "LX/0j1;",
        ":",
        "LX/0j3;",
        ":",
        "LX/0j4;",
        ":",
        "LX/0j6;",
        ":",
        "LX/0ip;",
        ":",
        "LX/0jF;",
        "DerivedData::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesIsMinutiaeSupported;",
        ":",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesIsPrivacyPillSupported;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;>",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/0iA;

.field private final c:LX/AS2;

.field private final d:LX/ASP;

.field private final e:LX/ASH;

.field private final f:LX/ASV;

.field private final g:LX/AS8;

.field public final h:Landroid/view/ViewGroup;

.field private final i:Landroid/view/ViewGroup;

.field public final j:LX/0il;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TServices;"
        }
    .end annotation
.end field

.field private final k:LX/HqU;

.field private final l:LX/Hrb;

.field public final m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/3l4;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0iA;LX/AS2;LX/ASP;LX/ASH;LX/ASV;LX/AS8;Landroid/view/ViewGroup;Landroid/view/ViewGroup;LX/0il;LX/HqU;LX/Hrb;LX/0Px;)V
    .locals 0
    .param p8    # Landroid/view/ViewGroup;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # Landroid/view/ViewGroup;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p11    # LX/HqU;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p12    # LX/Hrb;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p13    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0iA;",
            "LX/AS2;",
            "LX/ASP;",
            "LX/ASH;",
            "LX/ASV;",
            "LX/AS8;",
            "Landroid/view/ViewGroup;",
            "Landroid/view/ViewGroup;",
            "TServices;",
            "Lcom/facebook/composer/tip/StickyGuardrailInterstitialController$DataProvider;",
            "Lcom/facebook/composer/tip/StickyGuardrailInterstitialController$StickyGuardrailCallback;",
            "LX/0Px",
            "<",
            "LX/3l4;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1673721
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1673722
    iput-object p1, p0, LX/ASB;->a:Landroid/content/Context;

    .line 1673723
    iput-object p2, p0, LX/ASB;->b:LX/0iA;

    .line 1673724
    iput-object p3, p0, LX/ASB;->c:LX/AS2;

    .line 1673725
    iput-object p4, p0, LX/ASB;->d:LX/ASP;

    .line 1673726
    iput-object p5, p0, LX/ASB;->e:LX/ASH;

    .line 1673727
    iput-object p6, p0, LX/ASB;->f:LX/ASV;

    .line 1673728
    iput-object p7, p0, LX/ASB;->g:LX/AS8;

    .line 1673729
    iput-object p8, p0, LX/ASB;->h:Landroid/view/ViewGroup;

    .line 1673730
    iput-object p9, p0, LX/ASB;->i:Landroid/view/ViewGroup;

    .line 1673731
    iput-object p10, p0, LX/ASB;->j:LX/0il;

    .line 1673732
    iput-object p11, p0, LX/ASB;->k:LX/HqU;

    .line 1673733
    iput-object p12, p0, LX/ASB;->l:LX/Hrb;

    .line 1673734
    iput-object p13, p0, LX/ASB;->m:LX/0Px;

    .line 1673735
    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/3l6;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1673736
    iget-object v0, p0, LX/ASB;->g:LX/AS8;

    iget-object v1, p0, LX/ASB;->h:Landroid/view/ViewGroup;

    .line 1673737
    new-instance v4, LX/AS7;

    invoke-static {v0}, LX/3kp;->b(LX/0QB;)LX/3kp;

    move-result-object v2

    check-cast v2, LX/3kp;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {v4, v1, v2, v3}, LX/AS7;-><init>(Landroid/view/ViewGroup;LX/3kp;Landroid/content/Context;)V

    .line 1673738
    move-object v0, v4

    .line 1673739
    iget-object v1, p0, LX/ASB;->h:Landroid/view/ViewGroup;

    iget-object v2, p0, LX/ASB;->i:Landroid/view/ViewGroup;

    iget-object v3, p0, LX/ASB;->j:LX/0il;

    .line 1673740
    new-instance v4, LX/AS1;

    check-cast v3, LX/0il;

    invoke-direct {v4, v1, v2, v3}, LX/AS1;-><init>(Landroid/view/ViewGroup;Landroid/view/ViewGroup;LX/0il;)V

    .line 1673741
    move-object v1, v4

    .line 1673742
    iget-object v2, p0, LX/ASB;->h:Landroid/view/ViewGroup;

    iget-object v3, p0, LX/ASB;->i:Landroid/view/ViewGroup;

    iget-object v4, p0, LX/ASB;->j:LX/0il;

    .line 1673743
    new-instance v5, LX/ASO;

    check-cast v4, LX/0il;

    invoke-direct {v5, v2, v3, v4}, LX/ASO;-><init>(Landroid/view/ViewGroup;Landroid/view/ViewGroup;LX/0il;)V

    .line 1673744
    move-object v2, v5

    .line 1673745
    iget-object v3, p0, LX/ASB;->e:LX/ASH;

    iget-object v4, p0, LX/ASB;->h:Landroid/view/ViewGroup;

    iget-object v5, p0, LX/ASB;->i:Landroid/view/ViewGroup;

    iget-object v6, p0, LX/ASB;->j:LX/0il;

    .line 1673746
    new-instance v7, LX/ASG;

    const/16 v8, 0x1b

    invoke-static {v3, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x34c

    invoke-static {v3, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const/16 v10, 0x259

    invoke-static {v3, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    move-object v13, v6

    check-cast v13, LX/0il;

    move-object v11, v4

    move-object v12, v5

    invoke-direct/range {v7 .. v13}, LX/ASG;-><init>(LX/0Ot;LX/0Or;LX/0Ot;Landroid/view/ViewGroup;Landroid/view/ViewGroup;LX/0il;)V

    .line 1673747
    move-object v3, v7

    .line 1673748
    iget-object v4, p0, LX/ASB;->f:LX/ASV;

    iget-object v5, p0, LX/ASB;->k:LX/HqU;

    iget-object v6, p0, LX/ASB;->l:LX/Hrb;

    invoke-virtual {v4, v5, v6}, LX/ASV;->a(LX/HqU;LX/Hrb;)LX/ASU;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
