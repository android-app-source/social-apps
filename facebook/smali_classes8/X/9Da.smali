.class public LX/9Da;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/21l;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/21l",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0gX;

.field public final b:LX/1K9;

.field public final c:Ljava/util/concurrent/Executor;

.field public final d:LX/3iR;

.field public final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/0gM;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/0gM;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/0gM;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$FriendsTypingSubscriptionModel;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/0ad;

.field public final m:LX/0tE;

.field private final n:LX/0tF;

.field public final o:LX/0tG;

.field public final p:LX/0ti;

.field public final q:LX/3PF;

.field public final r:LX/8yF;

.field public final s:LX/3PH;

.field private final t:LX/3PG;

.field public u:LX/9DA;

.field public v:Lcom/facebook/graphql/model/GraphQLStory;


# direct methods
.method public constructor <init>(LX/0gX;LX/1K9;Ljava/util/concurrent/Executor;LX/3iR;LX/0ad;LX/0tE;LX/0tF;LX/0tG;LX/0ti;LX/3PF;LX/8yF;LX/3PH;LX/3PG;)V
    .locals 1
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1455761
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1455762
    iput-object p1, p0, LX/9Da;->a:LX/0gX;

    .line 1455763
    iput-object p2, p0, LX/9Da;->b:LX/1K9;

    .line 1455764
    iput-object p3, p0, LX/9Da;->c:Ljava/util/concurrent/Executor;

    .line 1455765
    iput-object p4, p0, LX/9Da;->d:LX/3iR;

    .line 1455766
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/9Da;->e:Ljava/util/Map;

    .line 1455767
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/9Da;->g:Ljava/util/Map;

    .line 1455768
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/9Da;->f:Ljava/util/Map;

    .line 1455769
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/9Da;->h:Ljava/util/Map;

    .line 1455770
    iput-object p5, p0, LX/9Da;->l:LX/0ad;

    .line 1455771
    iput-object p6, p0, LX/9Da;->m:LX/0tE;

    .line 1455772
    iput-object p7, p0, LX/9Da;->n:LX/0tF;

    .line 1455773
    new-instance v0, LX/9DX;

    invoke-direct {v0, p0}, LX/9DX;-><init>(LX/9Da;)V

    move-object v0, v0

    .line 1455774
    iput-object v0, p0, LX/9Da;->i:LX/0TF;

    .line 1455775
    new-instance v0, LX/9DY;

    invoke-direct {v0, p0}, LX/9DY;-><init>(LX/9Da;)V

    move-object v0, v0

    .line 1455776
    iput-object v0, p0, LX/9Da;->j:LX/0TF;

    .line 1455777
    new-instance v0, LX/9DZ;

    invoke-direct {v0, p0}, LX/9DZ;-><init>(LX/9Da;)V

    move-object v0, v0

    .line 1455778
    iput-object v0, p0, LX/9Da;->k:LX/0TF;

    .line 1455779
    iput-object p8, p0, LX/9Da;->o:LX/0tG;

    .line 1455780
    iput-object p9, p0, LX/9Da;->p:LX/0ti;

    .line 1455781
    iput-object p10, p0, LX/9Da;->q:LX/3PF;

    .line 1455782
    iput-object p11, p0, LX/9Da;->r:LX/8yF;

    .line 1455783
    iput-object p12, p0, LX/9Da;->s:LX/3PH;

    .line 1455784
    iput-object p13, p0, LX/9Da;->t:LX/3PG;

    .line 1455785
    return-void
.end method

.method public static a(LX/0QB;)LX/9Da;
    .locals 1

    .prologue
    .line 1455866
    invoke-static {p0}, LX/9Da;->b(LX/0QB;)LX/9Da;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/Set;LX/0QK;LX/0TF;Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0QK",
            "<",
            "Ljava/lang/String;",
            "LX/0gV",
            "<TT;>;>;",
            "LX/0TF",
            "<TT;>;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/0gM;",
            ">;",
            "Ljava/util/Map",
            "<",
            "LX/0gV",
            "<",
            "LX/0jT;",
            ">;",
            "LX/0TF",
            "<",
            "LX/0jT;",
            ">;>;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/0gV;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1455859
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 1455860
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1455861
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {p3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1455862
    invoke-interface {p1, v0}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0gV;

    .line 1455863
    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1455864
    invoke-interface {p4, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1455865
    :cond_1
    return-object v2
.end method

.method private a(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/0gM;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1455854
    new-instance v0, Ljava/util/HashSet;

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    .line 1455855
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 1455856
    invoke-interface {p1}, Ljava/util/Map;->clear()V

    .line 1455857
    iget-object v1, p0, LX/9Da;->a:LX/0gX;

    invoke-virtual {v1, v0}, LX/0gX;->a(Ljava/util/Set;)V

    .line 1455858
    return-void
.end method

.method private static a(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "LX/0gV;",
            "LX/2o1;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/0gV;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/0gM;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1455846
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1455847
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2o1;

    .line 1455848
    iget-object v3, v1, LX/2o1;->a:LX/0gM;

    move-object v3, v3

    .line 1455849
    if-eqz v3, :cond_0

    .line 1455850
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    .line 1455851
    iget-object v3, v1, LX/2o1;->a:LX/0gM;

    move-object v1, v3

    .line 1455852
    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1455853
    :cond_1
    return-void
.end method

.method private a(Ljava/util/Map;Ljava/util/Set;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/0gM;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1455840
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 1455841
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, p2}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 1455842
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 1455843
    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 1455844
    invoke-direct {p0, v0}, LX/9Da;->a(Ljava/util/Map;)V

    .line 1455845
    :cond_0
    return-void
.end method

.method public static b(LX/0QB;)LX/9Da;
    .locals 15

    .prologue
    .line 1455832
    new-instance v0, LX/9Da;

    invoke-static {p0}, LX/0gX;->a(LX/0QB;)LX/0gX;

    move-result-object v1

    check-cast v1, LX/0gX;

    invoke-static {p0}, LX/1K9;->a(LX/0QB;)LX/1K9;

    move-result-object v2

    check-cast v2, LX/1K9;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/3iR;->a(LX/0QB;)LX/3iR;

    move-result-object v4

    check-cast v4, LX/3iR;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static {p0}, LX/0tE;->b(LX/0QB;)LX/0tE;

    move-result-object v6

    check-cast v6, LX/0tE;

    invoke-static {p0}, LX/0tF;->a(LX/0QB;)LX/0tF;

    move-result-object v7

    check-cast v7, LX/0tF;

    invoke-static {p0}, LX/0tG;->a(LX/0QB;)LX/0tG;

    move-result-object v8

    check-cast v8, LX/0tG;

    invoke-static {p0}, LX/0ti;->b(LX/0QB;)LX/0ti;

    move-result-object v9

    check-cast v9, LX/0ti;

    const-class v10, LX/3PF;

    invoke-interface {p0, v10}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/3PF;

    .line 1455833
    new-instance v12, LX/8yF;

    .line 1455834
    new-instance v14, LX/8yE;

    invoke-static {p0}, LX/0ti;->b(LX/0QB;)LX/0ti;

    move-result-object v11

    check-cast v11, LX/0ti;

    const-class v13, LX/3Ki;

    invoke-interface {p0, v13}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/3Ki;

    invoke-direct {v14, v11, v13}, LX/8yE;-><init>(LX/0ti;LX/3Ki;)V

    .line 1455835
    move-object v11, v14

    .line 1455836
    check-cast v11, LX/8yE;

    invoke-direct {v12, v11}, LX/8yF;-><init>(LX/8yE;)V

    .line 1455837
    move-object v11, v12

    .line 1455838
    check-cast v11, LX/8yF;

    invoke-static {p0}, LX/3PH;->b(LX/0QB;)LX/3PH;

    move-result-object v12

    check-cast v12, LX/3PH;

    const-class v13, LX/3PG;

    invoke-interface {p0, v13}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/3PG;

    invoke-direct/range {v0 .. v13}, LX/9Da;-><init>(LX/0gX;LX/1K9;Ljava/util/concurrent/Executor;LX/3iR;LX/0ad;LX/0tE;LX/0tF;LX/0tG;LX/0ti;LX/3PF;LX/8yF;LX/3PH;LX/3PG;)V

    .line 1455839
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1455828
    iget-object v0, p0, LX/9Da;->e:Ljava/util/Map;

    invoke-direct {p0, v0}, LX/9Da;->a(Ljava/util/Map;)V

    .line 1455829
    iget-object v0, p0, LX/9Da;->g:Ljava/util/Map;

    invoke-direct {p0, v0}, LX/9Da;->a(Ljava/util/Map;)V

    .line 1455830
    iget-object v0, p0, LX/9Da;->f:Ljava/util/Map;

    invoke-direct {p0, v0}, LX/9Da;->a(Ljava/util/Map;)V

    .line 1455831
    return-void
.end method

.method public final a(LX/9DA;)V
    .locals 0

    .prologue
    .line 1455826
    iput-object p1, p0, LX/9Da;->u:LX/9DA;

    .line 1455827
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1455787
    if-nez p1, :cond_1

    .line 1455788
    :cond_0
    :goto_0
    return-void

    .line 1455789
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    :goto_1
    iput-object v0, p0, LX/9Da;->v:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1455790
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1455791
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1455792
    if-eqz v0, :cond_0

    .line 1455793
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    .line 1455794
    :goto_2
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 1455795
    const/4 v5, 0x0

    .line 1455796
    iget-object v2, p0, LX/9Da;->l:LX/0ad;

    sget-short v4, LX/0wn;->aI:S

    invoke-interface {v2, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, LX/9Da;->l:LX/0ad;

    sget-short v4, LX/0wn;->aH:S

    invoke-interface {v2, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1455797
    :cond_2
    new-instance v2, LX/9DV;

    invoke-direct {v2, p0}, LX/9DV;-><init>(LX/9Da;)V

    iget-object v4, p0, LX/9Da;->k:LX/0TF;

    iget-object v5, p0, LX/9Da;->f:Ljava/util/Map;

    invoke-static {v1, v2, v4, v5, v3}, LX/9Da;->a(Ljava/util/Set;LX/0QK;LX/0TF;Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v2

    .line 1455798
    :goto_3
    move-object v4, v2

    .line 1455799
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 1455800
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v5, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1455801
    iget-object v2, p0, LX/9Da;->h:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v2, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1455802
    iget-object v2, p0, LX/9Da;->n:LX/0tF;

    const/4 v6, 0x0

    .line 1455803
    invoke-virtual {v2}, LX/0tF;->a()Z

    move-result v7

    if-eqz v7, :cond_3

    iget-object v7, v2, LX/0tF;->a:LX/0ad;

    sget-short v8, LX/0wn;->ai:S

    invoke-interface {v7, v8, v6}, LX/0ad;->a(SZ)Z

    move-result v7

    if-eqz v7, :cond_3

    const/4 v6, 0x1

    :cond_3
    move v2, v6

    .line 1455804
    if-eqz v2, :cond_7

    .line 1455805
    invoke-static {v0}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    const/4 v0, 0x0

    move v2, v0

    :goto_4
    if-ge v2, v7, :cond_7

    invoke-virtual {v6, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1455806
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v8

    if-eqz v8, :cond_4

    .line 1455807
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v5, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1455808
    iget-object v8, p0, LX/9Da;->h:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-interface {v8, v9, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1455809
    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 1455810
    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 1455811
    :cond_6
    sget-object v1, LX/0Re;->a:LX/0Re;

    move-object v1, v1

    .line 1455812
    goto/16 :goto_2

    .line 1455813
    :cond_7
    iget-object v0, p0, LX/9Da;->l:LX/0ad;

    sget-short v2, LX/0wn;->al:S

    const/4 v6, 0x0

    invoke-interface {v0, v2, v6}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1455814
    new-instance v0, LX/9DT;

    invoke-direct {v0, p0}, LX/9DT;-><init>(LX/9Da;)V

    iget-object v2, p0, LX/9Da;->i:LX/0TF;

    iget-object v6, p0, LX/9Da;->e:Ljava/util/Map;

    invoke-static {v5, v0, v2, v6, v3}, LX/9Da;->a(Ljava/util/Set;LX/0QK;LX/0TF;Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    .line 1455815
    :goto_5
    move-object v0, v0

    .line 1455816
    iget-object v2, p0, LX/9Da;->l:LX/0ad;

    sget-short v6, LX/0wn;->aD:S

    const/4 v7, 0x0

    invoke-interface {v2, v6, v7}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 1455817
    new-instance v2, LX/9DU;

    invoke-direct {v2, p0}, LX/9DU;-><init>(LX/9Da;)V

    iget-object v6, p0, LX/9Da;->j:LX/0TF;

    iget-object v7, p0, LX/9Da;->g:Ljava/util/Map;

    invoke-static {v5, v2, v6, v7, v3}, LX/9Da;->a(Ljava/util/Set;LX/0QK;LX/0TF;Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v2

    .line 1455818
    :goto_6
    move-object v2, v2

    .line 1455819
    iget-object v6, p0, LX/9Da;->a:LX/0gX;

    invoke-virtual {v6, v3}, LX/0gX;->a(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v3

    .line 1455820
    iget-object v6, p0, LX/9Da;->f:Ljava/util/Map;

    invoke-static {v3, v4, v6}, LX/9Da;->a(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V

    .line 1455821
    iget-object v4, p0, LX/9Da;->e:Ljava/util/Map;

    invoke-static {v3, v0, v4}, LX/9Da;->a(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V

    .line 1455822
    iget-object v0, p0, LX/9Da;->g:Ljava/util/Map;

    invoke-static {v3, v2, v0}, LX/9Da;->a(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V

    .line 1455823
    iget-object v0, p0, LX/9Da;->f:Ljava/util/Map;

    invoke-direct {p0, v0, v1}, LX/9Da;->a(Ljava/util/Map;Ljava/util/Set;)V

    .line 1455824
    iget-object v0, p0, LX/9Da;->e:Ljava/util/Map;

    invoke-direct {p0, v0, v5}, LX/9Da;->a(Ljava/util/Map;Ljava/util/Set;)V

    .line 1455825
    iget-object v0, p0, LX/9Da;->g:Ljava/util/Map;

    invoke-direct {p0, v0, v5}, LX/9Da;->a(Ljava/util/Map;Ljava/util/Set;)V

    goto/16 :goto_0

    :cond_8
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    goto/16 :goto_3

    :cond_9
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    goto :goto_5

    :cond_a
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    goto :goto_6
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1455786
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {p0, p1}, LX/9Da;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    return-void
.end method
