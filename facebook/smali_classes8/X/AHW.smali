.class public final LX/AHW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$DirectMessageThreadSubscriptionModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/AFM;

.field public final synthetic c:LX/AHZ;


# direct methods
.method public constructor <init>(LX/AHZ;Ljava/lang/String;LX/AFM;)V
    .locals 0

    .prologue
    .line 1655692
    iput-object p1, p0, LX/AHW;->c:LX/AHZ;

    iput-object p2, p0, LX/AHW;->a:Ljava/lang/String;

    iput-object p3, p0, LX/AHW;->b:LX/AFM;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1655693
    sget-object v0, LX/AHZ;->a:Ljava/lang/String;

    const-string v1, "Reply result not available"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1655694
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1655695
    check-cast p1, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$DirectMessageThreadSubscriptionModel;

    .line 1655696
    if-nez p1, :cond_0

    .line 1655697
    sget-object v0, LX/AHZ;->a:Ljava/lang/String;

    const-string v1, "null result."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1655698
    :goto_0
    return-void

    .line 1655699
    :cond_0
    new-instance v0, LX/AHd;

    sget-object v1, LX/AHc;->SUBSCRIPTION:LX/AHc;

    iget-object v2, p0, LX/AHW;->c:LX/AHZ;

    iget-object v2, v2, LX/AHZ;->d:Ljava/lang/String;

    iget-object v3, p0, LX/AHW;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$DirectMessageThreadSubscriptionModel;->a()Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, LX/AHd;-><init>(LX/AHc;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;)V

    .line 1655700
    iget-object v1, p0, LX/AHW;->b:LX/AFM;

    invoke-virtual {v0}, LX/AHa;->a()Lcom/facebook/audience/model/ReplyThread;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/AFM;->a(Lcom/facebook/audience/model/ReplyThread;)V

    goto :goto_0
.end method
