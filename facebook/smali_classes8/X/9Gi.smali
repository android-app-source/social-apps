.class public LX/9Gi;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1460123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1460124
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1460125
    iput-object v0, p0, LX/9Gi;->a:LX/0Ot;

    .line 1460126
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/drawee/fbpipeline/FbDraweeView;Landroid/net/Uri;)V
    .locals 4

    .prologue
    .line 1460127
    invoke-static {p3}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    new-instance v1, LX/1o9;

    invoke-virtual {p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iget v3, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-direct {v1, v2, v3}, LX/1o9;-><init>(II)V

    .line 1460128
    iput-object v1, v0, LX/1bX;->c:LX/1o9;

    .line 1460129
    move-object v0, v0

    .line 1460130
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v1

    .line 1460131
    iget-object v0, p0, LX/9Gi;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {p2}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, v1}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, p1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 1460132
    invoke-virtual {p2, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1460133
    return-void
.end method
