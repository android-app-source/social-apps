.class public final LX/9Ey;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Z

.field public final synthetic b:LX/9F0;


# direct methods
.method public constructor <init>(LX/9F0;)V
    .locals 0

    .prologue
    .line 1457824
    iput-object p1, p0, LX/9Ey;->b:LX/9F0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onError(I)V
    .locals 4

    .prologue
    .line 1457790
    iget-object v0, p0, LX/9Ey;->b:LX/9F0;

    iget-object v0, v0, LX/9F0;->e:Lcom/facebook/feedback/ui/SpeechCommentDialog;

    if-nez v0, :cond_0

    .line 1457791
    :goto_0
    return-void

    .line 1457792
    :cond_0
    iget-object v0, p0, LX/9Ey;->b:LX/9F0;

    .line 1457793
    packed-switch p1, :pswitch_data_0

    .line 1457794
    iget-object v1, v0, LX/9F0;->l:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/20q;

    const/4 v2, 0x0

    .line 1457795
    invoke-virtual {v1}, LX/20q;->a()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1457796
    :goto_1
    move-object v1, v2

    .line 1457797
    invoke-static {v0, v1}, LX/9F0;->b(LX/9F0;Ljava/lang/String;)V

    .line 1457798
    :goto_2
    iget-object v2, v0, LX/9F0;->f:Landroid/support/v4/app/FragmentActivity;

    const/4 v3, 0x0

    invoke-static {v2, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1457799
    iget-object v0, p0, LX/9Ey;->b:LX/9F0;

    iget-object v0, v0, LX/9F0;->e:Lcom/facebook/feedback/ui/SpeechCommentDialog;

    invoke-virtual {v0}, Lcom/facebook/feedback/ui/SpeechCommentDialog;->b()V

    .line 1457800
    iget-object v0, p0, LX/9Ey;->b:LX/9F0;

    const/4 v1, 0x0

    .line 1457801
    iput-boolean v1, v0, LX/9F0;->g:Z

    .line 1457802
    goto :goto_0

    .line 1457803
    :pswitch_0
    iget-object v1, v0, LX/9F0;->l:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/20q;

    const/4 v2, 0x0

    .line 1457804
    invoke-virtual {v1}, LX/20q;->a()Z

    move-result v3

    if-nez v3, :cond_3

    .line 1457805
    :goto_3
    move-object v1, v2

    .line 1457806
    invoke-static {v0, v1}, LX/9F0;->b(LX/9F0;Ljava/lang/String;)V

    goto :goto_2

    .line 1457807
    :pswitch_1
    iget-object v1, v0, LX/9F0;->f:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08003a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1457808
    invoke-static {v0, v1}, LX/9F0;->b(LX/9F0;Ljava/lang/String;)V

    goto :goto_2

    .line 1457809
    :pswitch_2
    iget-object v1, v0, LX/9F0;->f:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08126a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1457810
    invoke-static {v0, v1}, LX/9F0;->b(LX/9F0;Ljava/lang/String;)V

    goto :goto_2

    .line 1457811
    :pswitch_3
    iget-object v1, v0, LX/9F0;->l:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/20q;

    const/4 v2, 0x0

    .line 1457812
    invoke-virtual {v1}, LX/20q;->a()Z

    move-result v3

    if-nez v3, :cond_5

    .line 1457813
    :goto_4
    move-object v1, v2

    .line 1457814
    invoke-static {v0, v1}, LX/9F0;->b(LX/9F0;Ljava/lang/String;)V

    goto :goto_2

    .line 1457815
    :cond_1
    iget-object v3, v1, LX/20q;->l:Ljava/lang/String;

    if-nez v3, :cond_2

    .line 1457816
    iget-object v3, v1, LX/20q;->a:LX/0ad;

    sget-char p1, LX/0wn;->E:C

    invoke-interface {v3, p1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/20q;->l:Ljava/lang/String;

    .line 1457817
    :cond_2
    iget-object v2, v1, LX/20q;->l:Ljava/lang/String;

    goto :goto_1

    .line 1457818
    :cond_3
    iget-object v3, v1, LX/20q;->k:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 1457819
    iget-object v3, v1, LX/20q;->a:LX/0ad;

    sget-char p1, LX/0wn;->D:C

    invoke-interface {v3, p1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/20q;->k:Ljava/lang/String;

    .line 1457820
    :cond_4
    iget-object v2, v1, LX/20q;->k:Ljava/lang/String;

    goto :goto_3

    .line 1457821
    :cond_5
    iget-object v3, v1, LX/20q;->j:Ljava/lang/String;

    if-nez v3, :cond_6

    .line 1457822
    iget-object v3, v1, LX/20q;->a:LX/0ad;

    sget-char p1, LX/0wn;->C:C

    invoke-interface {v3, p1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/20q;->j:Ljava/lang/String;

    .line 1457823
    :cond_6
    iget-object v2, v1, LX/20q;->j:Ljava/lang/String;

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
