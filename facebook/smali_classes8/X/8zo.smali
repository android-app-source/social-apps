.class public LX/8zo;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/91M;


# direct methods
.method public constructor <init>(LX/91M;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1428175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1428176
    iput-object p1, p0, LX/8zo;->a:LX/91M;

    .line 1428177
    return-void
.end method

.method public static a(LX/0QB;)LX/8zo;
    .locals 4

    .prologue
    .line 1428178
    const-class v1, LX/8zo;

    monitor-enter v1

    .line 1428179
    :try_start_0
    sget-object v0, LX/8zo;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1428180
    sput-object v2, LX/8zo;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1428181
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1428182
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1428183
    new-instance p0, LX/8zo;

    invoke-static {v0}, LX/91M;->a(LX/0QB;)LX/91M;

    move-result-object v3

    check-cast v3, LX/91M;

    invoke-direct {p0, v3}, LX/8zo;-><init>(LX/91M;)V

    .line 1428184
    move-object v0, p0

    .line 1428185
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1428186
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8zo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1428187
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1428188
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
