.class public LX/9XE;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/9XE;


# instance fields
.field public final a:LX/0Zb;

.field private final b:LX/17V;


# direct methods
.method public constructor <init>(LX/0Zb;LX/17V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1502557
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1502558
    iput-object p1, p0, LX/9XE;->a:LX/0Zb;

    .line 1502559
    iput-object p2, p0, LX/9XE;->b:LX/17V;

    .line 1502560
    return-void
.end method

.method public static a(LX/0QB;)LX/9XE;
    .locals 5

    .prologue
    .line 1502561
    sget-object v0, LX/9XE;->c:LX/9XE;

    if-nez v0, :cond_1

    .line 1502562
    const-class v1, LX/9XE;

    monitor-enter v1

    .line 1502563
    :try_start_0
    sget-object v0, LX/9XE;->c:LX/9XE;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1502564
    if-eqz v2, :cond_0

    .line 1502565
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1502566
    new-instance p0, LX/9XE;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/17V;->b(LX/0QB;)LX/17V;

    move-result-object v4

    check-cast v4, LX/17V;

    invoke-direct {p0, v3, v4}, LX/9XE;-><init>(LX/0Zb;LX/17V;)V

    .line 1502567
    move-object v0, p0

    .line 1502568
    sput-object v0, LX/9XE;->c:LX/9XE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1502569
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1502570
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1502571
    :cond_1
    sget-object v0, LX/9XE;->c:LX/9XE;

    return-object v0

    .line 1502572
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1502573
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/9X2;Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 4

    .prologue
    .line 1502574
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-interface {p0}, LX/9X2;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1502575
    iput-object p1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1502576
    move-object v0, v0

    .line 1502577
    const-string v1, "page_id"

    invoke-virtual {v0, v1, p2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "event_type"

    invoke-interface {p0}, LX/9X2;->getType()LX/9XC;

    move-result-object v2

    invoke-virtual {v2}, LX/9XC;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    return-object v0
.end method

.method public static c(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 1502578
    const-string v0, "pages_public_view"

    invoke-static {p0, v0, p1, p2}, LX/9XE;->a(LX/9X2;Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    return-object v0
.end method

.method public static c(LX/9XE;JLcom/facebook/graphql/enums/GraphQLPageActionType;)V
    .locals 5

    .prologue
    .line 1502580
    iget-object v0, p0, LX/9XE;->a:LX/0Zb;

    sget-object v1, LX/9XB;->EVENT_PAGE_DETAILS_LOADED:LX/9XB;

    invoke-static {v1, p1, p2}, LX/9XE;->c(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "ref"

    sget-object v3, LX/89z;->PAGE_INTERNAL:LX/89z;

    iget-object v3, v3, LX/89z;->loggingName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "insight_profile_tab"

    invoke-virtual {v1, v2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1502581
    return-void
.end method

.method public static d(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 1502579
    const-string v0, "pages_admin_panel"

    invoke-static {p0, v0, p1, p2}, LX/9XE;->a(LX/9X2;Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(JLX/89y;LX/89z;)V
    .locals 7
    .param p3    # LX/89y;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/89z;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1502592
    const/4 v6, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v1 .. v6}, LX/9XE;->a(JLX/89y;LX/89z;Ljava/lang/String;)V

    .line 1502593
    return-void
.end method

.method public final a(JLX/89y;LX/89z;Ljava/lang/String;)V
    .locals 3
    .param p3    # LX/89y;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/89z;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1502582
    sget-object v0, LX/9XB;->EVENT_PAGE_DETAILS_LOADED:LX/9XB;

    invoke-static {v0, p1, p2}, LX/9XE;->c(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "insight_profile_tab"

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;->HOME:Lcom/facebook/graphql/enums/GraphQLPagesInsightsPageProfileTab;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1502583
    if-eqz p3, :cond_0

    .line 1502584
    const-string v1, "page_profile_type"

    invoke-virtual {p3}, LX/89y;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1502585
    :cond_0
    if-eqz p4, :cond_2

    .line 1502586
    const-string v1, "ref"

    iget-object v2, p4, LX/89z;->loggingName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1502587
    :goto_0
    if-eqz p5, :cond_1

    .line 1502588
    const-string v1, "location"

    invoke-virtual {v0, v1, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1502589
    :cond_1
    iget-object v1, p0, LX/9XE;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1502590
    return-void

    .line 1502591
    :cond_2
    const-string v1, "ref"

    sget-object v2, LX/89z;->UNKNOWN:LX/89z;

    iget-object v2, v2, LX/89z;->loggingName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0
.end method

.method public final a(JLjava/lang/String;Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;)V
    .locals 3
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1502553
    iget-object v0, p0, LX/9XE;->a:LX/0Zb;

    sget-object v1, LX/9XI;->EVENT_TAPPED_CALL_TO_ACTION:LX/9XI;

    invoke-static {v1, p1, p2}, LX/9XE;->c(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "ccta_id"

    invoke-virtual {v1, v2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "ccta_type"

    invoke-virtual {v1, v2, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "ccta_ref"

    invoke-virtual {v1, v2, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1502554
    return-void
.end method

.method public final a(JLjava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1502555
    iget-object v0, p0, LX/9XE;->a:LX/0Zb;

    sget-object v1, LX/9X8;->EVENT_ADMIN_CLICK_PROMOTE:LX/9X8;

    invoke-static {v1, p1, p2}, LX/9XE;->d(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "label"

    invoke-virtual {v1, v2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "ref"

    invoke-virtual {v1, v2, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1502556
    return-void
.end method

.method public final a(JLjava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 3

    .prologue
    .line 1502551
    iget-object v0, p0, LX/9XE;->a:LX/0Zb;

    sget-object v1, LX/9XI;->EVENT_TAPPED_ALBUM:LX/9XI;

    invoke-static {v1, p1, p2}, LX/9XE;->c(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "location"

    invoke-virtual {v1, v2, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "album_id"

    invoke-virtual {v1, v2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "album_name"

    invoke-virtual {v1, v2, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "album_position"

    invoke-virtual {v1, v2, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "is_video_album"

    invoke-virtual {v1, v2, p7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1502552
    return-void
.end method

.method public final a(JLjava/lang/String;Z)V
    .locals 3

    .prologue
    .line 1502549
    iget-object v0, p0, LX/9XE;->a:LX/0Zb;

    sget-object v1, LX/9XI;->EVENT_TAPPED_PHOTO:LX/9XI;

    invoke-static {v1, p1, p2}, LX/9XE;->c(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "location"

    invoke-virtual {v1, v2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "is_video"

    invoke-virtual {v1, v2, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1502550
    return-void
.end method

.method public final a(LX/9X2;J)V
    .locals 2

    .prologue
    .line 1502547
    const-string v0, "pages_public_view"

    invoke-virtual {p0, v0, p1, p2, p3}, LX/9XE;->a(Ljava/lang/String;LX/9X2;J)V

    .line 1502548
    return-void
.end method

.method public final a(LX/9X7;JLcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;Lcom/facebook/graphql/enums/GraphQLPageActionType;)V
    .locals 4
    .param p5    # Lcom/facebook/graphql/enums/GraphQLPageActionType;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1502542
    invoke-static {p1, p2, p3}, LX/9XE;->c(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "template_id"

    invoke-virtual {p4}, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1502543
    if-eqz p5, :cond_0

    .line 1502544
    const-string v1, "action"

    invoke-virtual {v0, v1, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1502545
    :cond_0
    iget-object v1, p0, LX/9XE;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1502546
    return-void
.end method

.method public final a(LX/9X7;JLcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1502540
    iget-object v0, p0, LX/9XE;->a:LX/0Zb;

    invoke-static {p1, p2, p3}, LX/9XE;->c(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "template_id"

    invoke-virtual {p4}, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "location"

    invoke-virtual {v1, v2, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1502541
    return-void
.end method

.method public final a(Ljava/lang/String;LX/9X2;J)V
    .locals 3

    .prologue
    .line 1502538
    iget-object v0, p0, LX/9XE;->a:LX/0Zb;

    invoke-static {p2, p1, p3, p4}, LX/9XE;->a(LX/9X2;Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1502539
    return-void
.end method

.method public final a(Ljava/lang/String;LX/9X2;JLjava/lang/String;)V
    .locals 3

    .prologue
    .line 1502536
    iget-object v0, p0, LX/9XE;->a:LX/0Zb;

    invoke-static {p2, p1, p3, p4}, LX/9XE;->a(LX/9X2;Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "story_id"

    invoke-virtual {v1, v2, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1502537
    return-void
.end method

.method public final a(ZJZ)V
    .locals 4

    .prologue
    .line 1502532
    if-eqz p1, :cond_0

    sget-object v0, LX/9XB;->EVENT_PAGE_RECOMMENDATION_SUCCESS:LX/9XB;

    :goto_0
    check-cast v0, LX/9X2;

    .line 1502533
    iget-object v1, p0, LX/9XE;->a:LX/0Zb;

    invoke-static {v0, p2, p3}, LX/9XE;->c(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v2, "has_photo"

    invoke-virtual {v0, v2, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1502534
    return-void

    .line 1502535
    :cond_0
    sget-object v0, LX/9XA;->EVENT_PAGE_RECOMMENDATION_ERROR:LX/9XA;

    goto :goto_0
.end method

.method public final b(LX/9X2;J)V
    .locals 2

    .prologue
    .line 1502530
    const-string v0, "pages_admin_panel"

    invoke-virtual {p0, v0, p1, p2, p3}, LX/9XE;->a(Ljava/lang/String;LX/9X2;J)V

    .line 1502531
    return-void
.end method

.method public final f(JLjava/lang/String;)V
    .locals 3

    .prologue
    .line 1502528
    iget-object v0, p0, LX/9XE;->a:LX/0Zb;

    sget-object v1, LX/9XI;->EVENT_TAPPED_CREATE_ALBUM:LX/9XI;

    invoke-static {v1, p1, p2}, LX/9XE;->c(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "location"

    invoke-virtual {v1, v2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1502529
    return-void
.end method
