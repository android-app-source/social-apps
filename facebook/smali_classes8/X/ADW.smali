.class public LX/ADW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/ADW;


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public final b:LX/0Zb;

.field private final c:LX/ADT;

.field public d:J


# direct methods
.method public constructor <init>(LX/0Zb;LX/ADT;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1644802
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1644803
    iput-object p1, p0, LX/ADW;->b:LX/0Zb;

    .line 1644804
    iput-object p2, p0, LX/ADW;->c:LX/ADT;

    .line 1644805
    return-void
.end method

.method public static a(LX/0QB;)LX/ADW;
    .locals 5

    .prologue
    .line 1644806
    sget-object v0, LX/ADW;->e:LX/ADW;

    if-nez v0, :cond_1

    .line 1644807
    const-class v1, LX/ADW;

    monitor-enter v1

    .line 1644808
    :try_start_0
    sget-object v0, LX/ADW;->e:LX/ADW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1644809
    if-eqz v2, :cond_0

    .line 1644810
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1644811
    new-instance p0, LX/ADW;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/ADT;->b(LX/0QB;)LX/ADT;

    move-result-object v4

    check-cast v4, LX/ADT;

    invoke-direct {p0, v3, v4}, LX/ADW;-><init>(LX/0Zb;LX/ADT;)V

    .line 1644812
    move-object v0, p0

    .line 1644813
    sput-object v0, LX/ADW;->e:LX/ADW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1644814
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1644815
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1644816
    :cond_1
    sget-object v0, LX/ADW;->e:LX/ADW;

    return-object v0

    .line 1644817
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1644818
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/ADW;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)V
    .locals 4

    .prologue
    .line 1644819
    sget-object v0, LX/ADV;->START_STATE:LX/ADV;

    invoke-virtual {v0}, LX/ADV;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/ADW;->a:Ljava/lang/String;

    .line 1644820
    iget-wide v2, p1, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->c:J

    move-wide v0, v2

    .line 1644821
    iput-wide v0, p0, LX/ADW;->d:J

    .line 1644822
    return-void
.end method

.method public static a(LX/ADW;Lcom/facebook/adspayments/analytics/PaymentsReliabilityLogEvent;)V
    .locals 3

    .prologue
    .line 1644823
    instance-of v0, p1, Lcom/facebook/adspayments/analytics/PaymentsReliabilityTransitionLogEvent;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 1644824
    check-cast v0, Lcom/facebook/adspayments/analytics/PaymentsReliabilityTransitionLogEvent;

    .line 1644825
    iget-object v1, v0, Lcom/facebook/adspayments/analytics/PaymentsReliabilityTransitionLogEvent;->c:Ljava/lang/String;

    move-object v1, v1

    .line 1644826
    iget-object v2, p0, LX/ADW;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1644827
    :goto_0
    return-void

    .line 1644828
    :cond_0
    iget-object v1, v0, Lcom/facebook/adspayments/analytics/PaymentsReliabilityTransitionLogEvent;->c:Ljava/lang/String;

    move-object v0, v1

    .line 1644829
    iput-object v0, p0, LX/ADW;->a:Ljava/lang/String;

    .line 1644830
    :cond_1
    iget-object v0, p0, LX/ADW;->b:LX/0Zb;

    invoke-interface {v0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1644831
    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)Lcom/facebook/adspayments/analytics/PaymentsReliabilityErrorLogEvent;
    .locals 6

    .prologue
    .line 1644832
    iget-wide v0, p0, LX/ADW;->d:J

    .line 1644833
    iget-wide v4, p2, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->c:J

    move-wide v2, v4

    .line 1644834
    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 1644835
    invoke-static {p0, p2}, LX/ADW;->a(LX/ADW;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)V

    .line 1644836
    :cond_0
    new-instance v0, Lcom/facebook/adspayments/analytics/PaymentsReliabilityErrorLogEvent;

    invoke-direct {v0, p1, p2}, Lcom/facebook/adspayments/analytics/PaymentsReliabilityErrorLogEvent;-><init>(Ljava/lang/Throwable;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)V

    .line 1644837
    iget-object v1, p0, LX/ADW;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/adspayments/analytics/PaymentsReliabilityLogEvent;->n(Ljava/lang/String;)Lcom/facebook/adspayments/analytics/PaymentsReliabilityLogEvent;

    .line 1644838
    invoke-static {p0, v0}, LX/ADW;->a(LX/ADW;Lcom/facebook/adspayments/analytics/PaymentsReliabilityLogEvent;)V

    .line 1644839
    return-object v0
.end method

.method public final a(LX/ADV;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)Lcom/facebook/adspayments/analytics/PaymentsReliabilityTransitionLogEvent;
    .locals 1

    .prologue
    .line 1644840
    invoke-virtual {p1}, LX/ADV;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, LX/ADW;->b(Ljava/lang/String;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)Lcom/facebook/adspayments/analytics/PaymentsReliabilityTransitionLogEvent;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/adspayments/analytics/PaymentsLogEvent;)V
    .locals 3

    .prologue
    .line 1644841
    const-string v0, "mobile_ads_payments_holdout_2016_h2"

    iget-object v1, p0, LX/ADW;->c:LX/ADT;

    .line 1644842
    const/16 v2, 0x56f

    invoke-virtual {v1, v2}, LX/ADT;->a(I)Z

    move-result v2

    move v1, v2

    .line 1644843
    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1644844
    iget-object v0, p0, LX/ADW;->b:LX/0Zb;

    invoke-interface {v0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1644845
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)V
    .locals 1

    .prologue
    .line 1644846
    new-instance v0, Lcom/facebook/adspayments/analytics/PaymentsLogEvent;

    invoke-direct {v0, p1, p2}, Lcom/facebook/adspayments/analytics/PaymentsLogEvent;-><init>(Ljava/lang/String;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)V

    invoke-virtual {p0, v0}, LX/ADW;->a(Lcom/facebook/adspayments/analytics/PaymentsLogEvent;)V

    .line 1644847
    return-void
.end method

.method public final b(Ljava/lang/String;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)Lcom/facebook/adspayments/analytics/PaymentsReliabilityTransitionLogEvent;
    .locals 6

    .prologue
    .line 1644848
    iget-wide v0, p0, LX/ADW;->d:J

    .line 1644849
    iget-wide v4, p2, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->c:J

    move-wide v2, v4

    .line 1644850
    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 1644851
    invoke-static {p0, p2}, LX/ADW;->a(LX/ADW;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)V

    .line 1644852
    :cond_0
    new-instance v0, Lcom/facebook/adspayments/analytics/PaymentsReliabilityTransitionLogEvent;

    invoke-direct {v0, p1, p2}, Lcom/facebook/adspayments/analytics/PaymentsReliabilityTransitionLogEvent;-><init>(Ljava/lang/String;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)V

    .line 1644853
    iget-object v1, p0, LX/ADW;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/adspayments/analytics/PaymentsReliabilityLogEvent;->n(Ljava/lang/String;)Lcom/facebook/adspayments/analytics/PaymentsReliabilityLogEvent;

    .line 1644854
    invoke-static {p0, v0}, LX/ADW;->a(LX/ADW;Lcom/facebook/adspayments/analytics/PaymentsReliabilityLogEvent;)V

    .line 1644855
    return-object v0
.end method
