.class public final enum LX/9lY;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9lY;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9lY;

.field public static final enum NETWORK_ERROR:LX/9lY;

.field public static final enum SERVER_ERROR:LX/9lY;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1532960
    new-instance v0, LX/9lY;

    const-string v1, "NETWORK_ERROR"

    invoke-direct {v0, v1, v2}, LX/9lY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9lY;->NETWORK_ERROR:LX/9lY;

    .line 1532961
    new-instance v0, LX/9lY;

    const-string v1, "SERVER_ERROR"

    invoke-direct {v0, v1, v3}, LX/9lY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9lY;->SERVER_ERROR:LX/9lY;

    .line 1532962
    const/4 v0, 0x2

    new-array v0, v0, [LX/9lY;

    sget-object v1, LX/9lY;->NETWORK_ERROR:LX/9lY;

    aput-object v1, v0, v2

    sget-object v1, LX/9lY;->SERVER_ERROR:LX/9lY;

    aput-object v1, v0, v3

    sput-object v0, LX/9lY;->$VALUES:[LX/9lY;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1532959
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9lY;
    .locals 1

    .prologue
    .line 1532957
    const-class v0, LX/9lY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9lY;

    return-object v0
.end method

.method public static values()[LX/9lY;
    .locals 1

    .prologue
    .line 1532958
    sget-object v0, LX/9lY;->$VALUES:[LX/9lY;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9lY;

    return-object v0
.end method
