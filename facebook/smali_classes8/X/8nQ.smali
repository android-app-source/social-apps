.class public final LX/8nQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$TagSearchModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Z

.field public final synthetic c:Z

.field public final synthetic d:LX/8JX;

.field public final synthetic e:Ljava/lang/CharSequence;

.field public final synthetic f:LX/8nR;


# direct methods
.method public constructor <init>(LX/8nR;ZZZLX/8JX;Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 1400976
    iput-object p1, p0, LX/8nQ;->f:LX/8nR;

    iput-boolean p2, p0, LX/8nQ;->a:Z

    iput-boolean p3, p0, LX/8nQ;->b:Z

    iput-boolean p4, p0, LX/8nQ;->c:Z

    iput-object p5, p0, LX/8nQ;->d:LX/8JX;

    iput-object p6, p0, LX/8nQ;->e:Ljava/lang/CharSequence;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1400977
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 1400978
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1400979
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 1400980
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1400981
    check-cast v0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$TagSearchModel;

    invoke-virtual {v0}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$TagSearchModel;->a()Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$TagSearchModel$ResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$TagSearchModel$ResultsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_5

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;

    .line 1400982
    if-nez v0, :cond_1

    .line 1400983
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1400984
    :cond_1
    :try_start_0
    iget-object v5, p0, LX/8nQ;->f:LX/8nR;

    invoke-static {v5, v0}, LX/8nR;->a$redex0(LX/8nR;Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;)LX/7Gr;

    move-result-object v5

    .line 1400985
    iget-boolean v6, p0, LX/8nQ;->a:Z

    if-nez v6, :cond_2

    sget-object v6, LX/7Gr;->SELF:LX/7Gr;

    if-eq v5, v6, :cond_0

    :cond_2
    iget-boolean v6, p0, LX/8nQ;->b:Z

    if-nez v6, :cond_3

    sget-object v6, LX/7Gr;->USER:LX/7Gr;

    if-eq v5, v6, :cond_0

    :cond_3
    iget-boolean v6, p0, LX/8nQ;->c:Z

    if-nez v6, :cond_4

    sget-object v6, LX/7Gr;->PAGE:LX/7Gr;

    if-eq v5, v6, :cond_0

    .line 1400986
    :cond_4
    iget-object v5, p0, LX/8nQ;->f:LX/8nR;

    const-string v6, "graphql_search"

    sget-object v7, LX/8nE;->OTHERS:LX/8nE;

    invoke-virtual {v7}, LX/8nE;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v0, v6, v7}, LX/8nR;->a$redex0(LX/8nR;Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/tagging/model/TaggingProfile;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1400987
    :catch_0
    move-exception v0

    .line 1400988
    sget-object v5, LX/8nR;->a:Ljava/lang/Class;

    const-string v6, "Error handling tag search result"

    invoke-static {v5, v6, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1400989
    :cond_5
    iget-object v0, p0, LX/8nQ;->d:LX/8JX;

    iget-object v1, p0, LX/8nQ;->e:Ljava/lang/CharSequence;

    invoke-interface {v0, v1, v2}, LX/8JX;->a(Ljava/lang/CharSequence;Ljava/util/List;)V

    .line 1400990
    return-void
.end method
