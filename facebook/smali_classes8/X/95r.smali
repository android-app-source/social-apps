.class public final LX/95r;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8Nj;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLComment;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLComment;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/1L9;

.field public final synthetic e:LX/3iQ;


# direct methods
.method public constructor <init>(LX/3iQ;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;LX/1L9;)V
    .locals 0

    .prologue
    .line 1438002
    iput-object p1, p0, LX/95r;->e:LX/3iQ;

    iput-object p2, p0, LX/95r;->a:Lcom/facebook/graphql/model/GraphQLComment;

    iput-object p3, p0, LX/95r;->b:Lcom/facebook/graphql/model/GraphQLComment;

    iput-object p4, p0, LX/95r;->c:Ljava/lang/String;

    iput-object p5, p0, LX/95r;->d:LX/1L9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 13

    .prologue
    .line 1438003
    iget-object v0, p0, LX/95r;->e:LX/3iQ;

    iget-object v1, p0, LX/95r;->a:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v2, p0, LX/95r;->b:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/95r;->c:Ljava/lang/String;

    iget-object v4, p0, LX/95r;->d:LX/1L9;

    const/4 v9, 0x0

    .line 1438004
    iget-object v11, v0, LX/3iQ;->d:LX/1Ck;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "fetch_video_comment_"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    iget-object v5, v0, LX/3iQ;->k:LX/3H7;

    const/4 v8, 0x0

    move-object v6, v3

    move-object v7, v2

    move v10, v9

    invoke-virtual/range {v5 .. v10}, LX/3H7;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    new-instance v6, LX/95s;

    invoke-direct {v6, v0, v4, v1}, LX/95s;-><init>(LX/3iQ;LX/1L9;Lcom/facebook/graphql/model/GraphQLComment;)V

    invoke-virtual {v11, v12, v5, v6}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1438005
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1438006
    iget-object v0, p0, LX/95r;->d:LX/1L9;

    if-eqz v0, :cond_0

    .line 1438007
    iget-object v0, p0, LX/95r;->d:LX/1L9;

    iget-object v1, p0, LX/95r;->a:Lcom/facebook/graphql/model/GraphQLComment;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/1L9;->a(Ljava/lang/Object;Lcom/facebook/fbservice/service/ServiceException;)V

    .line 1438008
    :cond_0
    return-void
.end method
