.class public final LX/9dF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public a:I

.field public final synthetic b:LX/9dI;


# direct methods
.method public constructor <init>(LX/9dI;)V
    .locals 1

    .prologue
    .line 1517823
    iput-object p1, p0, LX/9dF;->b:LX/9dI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1517824
    const/4 v0, -0x1

    iput v0, p0, LX/9dF;->a:I

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1517825
    iget-object v0, p0, LX/9dF;->b:LX/9dI;

    iget-object v0, v0, LX/9dI;->h:Landroid/view/View$OnTouchListener;

    if-eqz v0, :cond_0

    .line 1517826
    iget-object v0, p0, LX/9dF;->b:LX/9dI;

    iget-object v0, v0, LX/9dI;->h:Landroid/view/View$OnTouchListener;

    invoke-interface {v0, p1, p2}, Landroid/view/View$OnTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 1517827
    :cond_0
    iget-object v0, p0, LX/9dF;->b:LX/9dI;

    iget-object v0, v0, LX/9dI;->g:LX/Jvf;

    if-eqz v0, :cond_1

    .line 1517828
    iget-object v0, p0, LX/9dF;->b:LX/9dI;

    iget-object v0, v0, LX/9dI;->g:LX/Jvf;

    .line 1517829
    iget-object v1, v0, LX/Jvf;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v1, v1, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->w:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    .line 1517830
    iget-boolean v0, v1, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->s:Z

    if-eqz v0, :cond_1

    .line 1517831
    iget-object v0, v1, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->e:Lcom/facebook/optic/CameraPreviewView;

    invoke-virtual {v0, p2}, Lcom/facebook/optic/CameraPreviewView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1517832
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_3

    iget v0, p0, LX/9dF;->a:I

    if-nez v0, :cond_3

    .line 1517833
    :cond_2
    :goto_0
    return v2

    .line 1517834
    :cond_3
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    iput v0, p0, LX/9dF;->a:I

    .line 1517835
    iget-object v0, p0, LX/9dF;->b:LX/9dI;

    iget-object v0, v0, LX/9dI;->s:Landroid/view/GestureDetector;

    invoke-virtual {v0, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/9dF;->b:LX/9dI;

    iget-object v0, v0, LX/9dI;->q:LX/5jK;

    invoke-virtual {v0}, LX/5jK;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_4
    iget-object v0, p0, LX/9dF;->b:LX/9dI;

    iget-object v0, v0, LX/9dI;->r:LX/9d1;

    invoke-interface {v0}, LX/9d1;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1517836
    sget-object v0, LX/9dI;->a:Ljava/lang/String;

    const-string v1, "There\'s mixed touch events being thrown."

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1517837
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-eq v0, v2, :cond_5

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    iget-object v0, p0, LX/9dF;->b:LX/9dI;

    iget-boolean v0, v0, LX/9dI;->k:Z

    if-nez v0, :cond_2

    .line 1517838
    :cond_5
    iget-object v0, p0, LX/9dF;->b:LX/9dI;

    iget-object v0, v0, LX/9dI;->x:LX/Ar0;

    if-eqz v0, :cond_6

    .line 1517839
    iget-object v0, p0, LX/9dF;->b:LX/9dI;

    iget-object v0, v0, LX/9dI;->x:LX/Ar0;

    invoke-virtual {v0}, LX/Ar0;->a()V

    .line 1517840
    :cond_6
    iget-object v0, p0, LX/9dF;->b:LX/9dI;

    invoke-static {v0, v3}, LX/9dI;->c(LX/9dI;Z)V

    .line 1517841
    iget-object v0, p0, LX/9dF;->b:LX/9dI;

    invoke-virtual {v0, v3}, LX/9dI;->a(Z)V

    goto :goto_0
.end method
