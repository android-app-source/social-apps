.class public final LX/A8Q;
.super LX/A8O;
.source ""


# instance fields
.field public final synthetic a:LX/A8S;

.field private final b:LX/90h;


# direct methods
.method public constructor <init>(LX/A8S;LX/90h;)V
    .locals 0

    .prologue
    .line 1627656
    iput-object p1, p0, LX/A8Q;->a:LX/A8S;

    invoke-direct {p0}, LX/A8O;-><init>()V

    .line 1627657
    iput-object p2, p0, LX/A8Q;->b:LX/90h;

    .line 1627658
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1627633
    iget-object v0, p0, LX/A8Q;->a:LX/A8S;

    invoke-static {v0}, LX/A8S;->c(LX/A8S;)V

    .line 1627634
    iget-object v0, p0, LX/A8Q;->a:LX/A8S;

    const/4 v1, 0x0

    .line 1627635
    iput-boolean v1, v0, LX/A8S;->k:Z

    .line 1627636
    iget-object v0, p0, LX/A8Q;->a:LX/A8S;

    iget-object v0, v0, LX/A8S;->c:LX/A8O;

    if-eqz v0, :cond_0

    .line 1627637
    iget-object v0, p0, LX/A8Q;->a:LX/A8S;

    iget-object v0, v0, LX/A8S;->c:LX/A8O;

    invoke-virtual {v0}, LX/A8O;->a()V

    .line 1627638
    :cond_0
    return-void
.end method

.method public final a(II)V
    .locals 3

    .prologue
    .line 1627651
    iget-object v0, p0, LX/A8Q;->a:LX/A8S;

    const/4 v1, 0x1

    .line 1627652
    iput-boolean v1, v0, LX/A8S;->k:Z

    .line 1627653
    iget-object v0, p0, LX/A8Q;->a:LX/A8S;

    iget-object v0, v0, LX/A8S;->c:LX/A8O;

    if-eqz v0, :cond_0

    .line 1627654
    iget-object v0, p0, LX/A8Q;->a:LX/A8S;

    iget-object v1, v0, LX/A8S;->c:LX/A8O;

    iget-object v0, p0, LX/A8Q;->a:LX/A8S;

    iget-object v0, v0, LX/A8S;->j:Ljava/util/Map;

    iget-object v2, p0, LX/A8Q;->b:LX/90h;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/2addr v0, p1

    invoke-virtual {v1, v0, p2}, LX/A8O;->a(II)V

    .line 1627655
    :cond_0
    return-void
.end method

.method public final a(III)V
    .locals 3

    .prologue
    .line 1627659
    iget-object v0, p0, LX/A8Q;->a:LX/A8S;

    const/4 v1, 0x1

    .line 1627660
    iput-boolean v1, v0, LX/A8S;->k:Z

    .line 1627661
    iget-object v0, p0, LX/A8Q;->a:LX/A8S;

    iget-object v0, v0, LX/A8S;->c:LX/A8O;

    if-eqz v0, :cond_0

    .line 1627662
    iget-object v0, p0, LX/A8Q;->a:LX/A8S;

    iget-object v0, v0, LX/A8S;->j:Ljava/util/Map;

    iget-object v1, p0, LX/A8Q;->b:LX/90h;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1627663
    iget-object v1, p0, LX/A8Q;->a:LX/A8S;

    iget-object v1, v1, LX/A8S;->c:LX/A8O;

    add-int v2, v0, p1

    add-int/2addr v0, p2

    invoke-virtual {v1, v2, v0, p3}, LX/A8O;->a(III)V

    .line 1627664
    :cond_0
    return-void
.end method

.method public final b(II)V
    .locals 3

    .prologue
    .line 1627645
    iget-object v0, p0, LX/A8Q;->a:LX/A8S;

    invoke-static {v0}, LX/A8S;->c(LX/A8S;)V

    .line 1627646
    iget-object v0, p0, LX/A8Q;->a:LX/A8S;

    const/4 v1, 0x0

    .line 1627647
    iput-boolean v1, v0, LX/A8S;->k:Z

    .line 1627648
    iget-object v0, p0, LX/A8Q;->a:LX/A8S;

    iget-object v0, v0, LX/A8S;->c:LX/A8O;

    if-eqz v0, :cond_0

    .line 1627649
    iget-object v0, p0, LX/A8Q;->a:LX/A8S;

    iget-object v1, v0, LX/A8S;->c:LX/A8O;

    iget-object v0, p0, LX/A8Q;->a:LX/A8S;

    iget-object v0, v0, LX/A8S;->j:Ljava/util/Map;

    iget-object v2, p0, LX/A8Q;->b:LX/90h;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/2addr v0, p1

    invoke-virtual {v1, v0, p2}, LX/A8O;->b(II)V

    .line 1627650
    :cond_0
    return-void
.end method

.method public final c(II)V
    .locals 3

    .prologue
    .line 1627639
    iget-object v0, p0, LX/A8Q;->a:LX/A8S;

    invoke-static {v0}, LX/A8S;->c(LX/A8S;)V

    .line 1627640
    iget-object v0, p0, LX/A8Q;->a:LX/A8S;

    const/4 v1, 0x0

    .line 1627641
    iput-boolean v1, v0, LX/A8S;->k:Z

    .line 1627642
    iget-object v0, p0, LX/A8Q;->a:LX/A8S;

    iget-object v0, v0, LX/A8S;->c:LX/A8O;

    if-eqz v0, :cond_0

    .line 1627643
    iget-object v0, p0, LX/A8Q;->a:LX/A8S;

    iget-object v1, v0, LX/A8S;->c:LX/A8O;

    iget-object v0, p0, LX/A8Q;->a:LX/A8S;

    iget-object v0, v0, LX/A8S;->j:Ljava/util/Map;

    iget-object v2, p0, LX/A8Q;->b:LX/90h;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/2addr v0, p1

    invoke-virtual {v1, v0, p2}, LX/A8O;->c(II)V

    .line 1627644
    :cond_0
    return-void
.end method
