.class public LX/A5p;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8RK;


# instance fields
.field public a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/3fr;

.field public final c:LX/0Sh;

.field public final d:LX/2RQ;

.field private final e:LX/3Oq;


# direct methods
.method public constructor <init>(LX/3fr;LX/0Sh;LX/3Oq;LX/2RQ;)V
    .locals 1
    .param p3    # LX/3Oq;
        .annotation runtime Lcom/facebook/contacts/module/ContactLinkQueryType;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1622358
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1622359
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/A5p;->a:Ljava/util/Set;

    .line 1622360
    iput-object p1, p0, LX/A5p;->b:LX/3fr;

    .line 1622361
    iput-object p2, p0, LX/A5p;->c:LX/0Sh;

    .line 1622362
    iput-object p3, p0, LX/A5p;->e:LX/3Oq;

    .line 1622363
    iput-object p4, p0, LX/A5p;->d:LX/2RQ;

    .line 1622364
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1622365
    iget-object v0, p0, LX/A5p;->c:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 1622366
    iget-object v0, p0, LX/A5p;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1622367
    iget-object v0, p0, LX/A5p;->b:LX/3fr;

    iget-object v1, p0, LX/A5p;->d:LX/2RQ;

    invoke-virtual {v1}, LX/2RQ;->a()LX/2RR;

    move-result-object v1

    .line 1622368
    iput-object p1, v1, LX/2RR;->e:Ljava/lang/String;

    .line 1622369
    move-object v1, v1

    .line 1622370
    sget-object v2, LX/3Oq;->ME:LX/3Oq;

    iget-object v3, p0, LX/A5p;->e:LX/3Oq;

    invoke-static {v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 1622371
    iput-object v2, v1, LX/2RR;->c:Ljava/util/Collection;

    .line 1622372
    move-object v1, v1

    .line 1622373
    sget-object v2, LX/2RS;->NAME:LX/2RS;

    .line 1622374
    iput-object v2, v1, LX/2RR;->n:LX/2RS;

    .line 1622375
    move-object v1, v1

    .line 1622376
    invoke-virtual {v0, v1}, LX/3fr;->a(LX/2RR;)LX/6N1;

    move-result-object v1

    .line 1622377
    :goto_0
    :try_start_0
    invoke-interface {v1}, LX/6N1;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1622378
    iget-object v2, p0, LX/A5p;->a:Ljava/util/Set;

    invoke-interface {v1}, LX/6N1;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/Contact;

    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/Contact;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1622379
    :catchall_0
    move-exception v0

    invoke-interface {v1}, LX/6N1;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, LX/6N1;->close()V

    .line 1622380
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1622381
    invoke-direct {p0, p1}, LX/A5p;->b(Ljava/lang/String;)V

    .line 1622382
    return-void
.end method

.method public final a(LX/8QL;)Z
    .locals 2

    .prologue
    .line 1622383
    instance-of v0, p1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    if-eqz v0, :cond_0

    .line 1622384
    iget-object v0, p0, LX/A5p;->a:Ljava/util/Set;

    check-cast p1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 1622385
    iget-object v1, p1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->g:Lcom/facebook/user/model/UserKey;

    move-object v1, v1

    .line 1622386
    invoke-virtual {v1}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 1622387
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
