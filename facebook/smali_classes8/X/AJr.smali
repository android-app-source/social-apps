.class public final LX/AJr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AJq;


# instance fields
.field public final synthetic a:Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;


# direct methods
.method public constructor <init>(Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;)V
    .locals 0

    .prologue
    .line 1662170
    iput-object p1, p0, LX/AJr;->a:Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1662171
    sget v0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->g:I

    if-gtz v0, :cond_0

    .line 1662172
    iget-object v0, p0, LX/AJr;->a:Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;

    iget-object v0, v0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->i:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v0

    .line 1662173
    sput v0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->g:I

    .line 1662174
    :cond_0
    sget v0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->e:I

    mul-int/2addr v0, p1

    sget v1, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->f:I

    add-int/lit8 v2, p1, -0x1

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    sget v1, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->g:I

    if-lt v0, v1, :cond_1

    .line 1662175
    iget-object v0, p0, LX/AJr;->a:Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;

    iget-object v0, v0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->k:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1662176
    iget-object v0, p0, LX/AJr;->a:Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;

    iget-object v0, v0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->i:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->g_(I)V

    .line 1662177
    :goto_0
    return-void

    .line 1662178
    :cond_1
    iget-object v0, p0, LX/AJr;->a:Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;

    iget-object v0, v0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->k:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
