.class public LX/94Z;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/94V;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/94X;


# direct methods
.method public constructor <init>(LX/94V;LX/94X;)V
    .locals 2

    .prologue
    .line 1435453
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1435454
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1435455
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/94Z;->a:Ljava/lang/ref/WeakReference;

    .line 1435456
    invoke-virtual {p0, p2}, LX/94Z;->a(LX/94X;)V

    .line 1435457
    return-void
.end method


# virtual methods
.method public final a(LX/94X;)V
    .locals 1

    .prologue
    .line 1435441
    iput-object p1, p0, LX/94Z;->b:LX/94X;

    .line 1435442
    iget-object v0, p0, LX/94Z;->b:LX/94X;

    const-string p1, "No state!"

    invoke-static {v0, p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1435443
    iget-object v0, p0, LX/94Z;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/94V;

    .line 1435444
    iget-object p1, p0, LX/94Z;->b:LX/94X;

    iget-object p1, p1, LX/94X;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, LX/94V;->setTitle(Ljava/lang/String;)V

    .line 1435445
    const/4 p1, 0x1

    invoke-virtual {v0, p1}, LX/94V;->setShowDividers(Z)V

    .line 1435446
    iget-object p1, p0, LX/94Z;->b:LX/94X;

    iget-object p1, p1, LX/94X;->d:LX/94c;

    invoke-virtual {v0, p1}, LX/94V;->a(LX/94c;)V

    .line 1435447
    iget-object p1, p0, LX/94Z;->b:LX/94X;

    iget-object p1, p1, LX/94X;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 1435448
    if-eqz p1, :cond_0

    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object p1

    :goto_0
    invoke-virtual {v0, p1}, LX/94V;->setButtonSpecs(Ljava/util/List;)V

    .line 1435449
    iget-object p1, p0, LX/94Z;->b:LX/94X;

    iget-object p1, p1, LX/94X;->c:LX/63W;

    invoke-virtual {v0, p1}, LX/94V;->setOnToolbarButtonListener(LX/63W;)V

    .line 1435450
    return-void

    .line 1435451
    :cond_0
    sget-object p1, LX/0Q7;->a:LX/0Px;

    move-object p1, p1

    .line 1435452
    goto :goto_0
.end method
