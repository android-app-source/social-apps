.class public LX/AHd;
.super LX/AHa;
.source ""


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field public final d:Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;

.field private final e:LX/AHc;

.field private final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1655765
    const-class v0, LX/AHd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AHd;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/AHc;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;)V
    .locals 1

    .prologue
    .line 1655766
    invoke-direct {p0, p2, p3}, LX/AHa;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1655767
    iput-object p4, p0, LX/AHd;->d:Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;

    .line 1655768
    iput-object p1, p0, LX/AHd;->e:LX/AHc;

    .line 1655769
    new-instance p2, Ljava/util/HashSet;

    invoke-direct {p2}, Ljava/util/HashSet;-><init>()V

    .line 1655770
    iget-object v0, p0, LX/AHd;->d:Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->n()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$SeenDirectUsersModel;

    move-result-object v0

    .line 1655771
    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$SeenDirectUsersModel;->a()LX/0Px;

    move-result-object p3

    invoke-virtual {p3}, LX/0Px;->size()I

    move-result p4

    const/4 v0, 0x0

    move p1, v0

    :goto_0
    if-ge p1, p4, :cond_0

    invoke-virtual {p3, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$SeenDirectUsersModel$EdgesModel;

    .line 1655772
    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$SeenDirectUsersModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1655773
    add-int/lit8 v0, p1, 0x1

    move p1, v0

    goto :goto_0

    .line 1655774
    :cond_0
    move-object v0, p2

    .line 1655775
    iput-object v0, p0, LX/AHd;->f:Ljava/util/Set;

    .line 1655776
    return-void
.end method

.method public static h(LX/AHd;)LX/0Px;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/model/Reply;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1655777
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, LX/AHd;->d:Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->k()Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel$DirectMessagesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel$DirectMessagesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1655778
    iget-object v0, p0, LX/AHd;->d:Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->k()Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel$DirectMessagesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel$DirectMessagesModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel$DirectMessagesModel$EdgesModel;

    .line 1655779
    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel$DirectMessagesModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;->j()Lcom/facebook/graphql/enums/GraphQLBackstagePostTypeEnum;

    move-result-object v5

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLBackstagePostTypeEnum;->TEXT:Lcom/facebook/graphql/enums/GraphQLBackstagePostTypeEnum;

    if-ne v5, v6, :cond_0

    .line 1655780
    new-instance v5, LX/AHS;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel$DirectMessagesModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    move-result-object v0

    iget-object v6, p0, LX/AHa;->b:Ljava/lang/String;

    invoke-virtual {p0}, LX/AHd;->b()Lcom/facebook/audience/model/AudienceControlData;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, LX/AHd;->f:Ljava/util/Set;

    invoke-direct {v5, v0, v6, v7, v8}, LX/AHS;-><init>(Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1655781
    invoke-virtual {v5}, LX/AHR;->a()Lcom/facebook/audience/model/Reply;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1655782
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1655783
    :cond_1
    sget-object v0, LX/7h2;->a:Ljava/util/Comparator;

    invoke-static {v2, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1655784
    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 1655785
    return-object v0
.end method


# virtual methods
.method public final b()Lcom/facebook/audience/model/AudienceControlData;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1655786
    iget-object v1, p0, LX/AHd;->d:Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;

    invoke-virtual {v1}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->j()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$AllDirectUsersModel;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/AHd;->d:Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;

    invoke-virtual {v1}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->j()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$AllDirectUsersModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$AllDirectUsersModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/AHd;->d:Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;

    invoke-virtual {v1}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->j()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$AllDirectUsersModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$AllDirectUsersModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1655787
    :cond_0
    const/4 v0, 0x0

    .line 1655788
    :goto_0
    return-object v0

    .line 1655789
    :cond_1
    iget-object v1, p0, LX/AHd;->d:Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;

    invoke-virtual {v1}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->j()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$AllDirectUsersModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$AllDirectUsersModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 1655790
    iget-object v1, p0, LX/AHd;->d:Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;

    invoke-virtual {v1}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->j()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$AllDirectUsersModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$AllDirectUsersModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$AllDirectUsersModel$EdgesModel;

    .line 1655791
    invoke-static {}, Lcom/facebook/audience/model/AudienceControlData;->newBuilder()Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$AllDirectUsersModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setId(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$AllDirectUsersModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setName(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$AllDirectUsersModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setProfileUri(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$AllDirectUsersModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->k()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setLowResProfileUri(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/model/AudienceControlData$Builder;->a()Lcom/facebook/audience/model/AudienceControlData;

    move-result-object v0

    goto :goto_0

    .line 1655792
    :cond_2
    iget-object v1, p0, LX/AHd;->d:Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;

    invoke-virtual {v1}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->j()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$AllDirectUsersModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$AllDirectUsersModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    move v1, v0

    .line 1655793
    :goto_1
    if-ge v1, v3, :cond_4

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$AllDirectUsersModel$EdgesModel;

    .line 1655794
    iget-object v4, p0, LX/AHa;->b:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$AllDirectUsersModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->j()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 1655795
    invoke-static {}, Lcom/facebook/audience/model/AudienceControlData;->newBuilder()Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$AllDirectUsersModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setId(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$AllDirectUsersModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setName(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$AllDirectUsersModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setProfileUri(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$AllDirectUsersModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->k()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setLowResProfileUri(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/model/AudienceControlData$Builder;->a()Lcom/facebook/audience/model/AudienceControlData;

    move-result-object v0

    goto/16 :goto_0

    .line 1655796
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1655797
    :cond_4
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unable to retrieve thread owner - user is the only participant."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final c()Lcom/facebook/audience/model/Reply;
    .locals 5

    .prologue
    .line 1655798
    new-instance v0, LX/AHS;

    iget-object v1, p0, LX/AHd;->d:Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;

    invoke-virtual {v1}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    move-result-object v1

    iget-object v2, p0, LX/AHa;->b:Ljava/lang/String;

    invoke-virtual {p0}, LX/AHd;->b()Lcom/facebook/audience/model/AudienceControlData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/AHd;->f:Ljava/util/Set;

    invoke-direct {v0, v1, v2, v3, v4}, LX/AHS;-><init>(Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1655799
    invoke-virtual {v0}, LX/AHR;->a()Lcom/facebook/audience/model/Reply;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/model/Reply;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1655800
    sget-object v0, LX/AHb;->a:[I

    iget-object v1, p0, LX/AHd;->e:LX/AHc;

    invoke-virtual {v1}, LX/AHc;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1655801
    invoke-static {p0}, LX/AHd;->h(LX/AHd;)LX/0Px;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1655802
    :pswitch_0
    invoke-static {p0}, LX/AHd;->h(LX/AHd;)LX/0Px;

    move-result-object v2

    .line 1655803
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 1655804
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/model/Reply;

    .line 1655805
    iget-boolean v5, v0, Lcom/facebook/audience/model/Reply;->c:Z

    move v5, v5

    .line 1655806
    if-nez v5, :cond_0

    .line 1655807
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1655808
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1655809
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 1655810
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 1655811
    iget-object v0, p0, LX/AHd;->f:Ljava/util/Set;

    iget-object v1, p0, LX/AHa;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1655812
    invoke-super {p0}, LX/AHa;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/AHd;->d:Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;

    if-eqz v0, :cond_1

    move v0, v1

    .line 1655813
    :goto_0
    if-nez v0, :cond_0

    .line 1655814
    sget-object v3, LX/AHd;->c:Ljava/lang/String;

    const-string v4, "Could not parse the thread participants for thread %s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v5, p0, LX/AHd;->d:Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;

    invoke-virtual {v5}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->l()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v2

    invoke-static {v3, v4, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1655815
    :cond_0
    return v0

    :cond_1
    move v0, v2

    .line 1655816
    goto :goto_0
.end method
