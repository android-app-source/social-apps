.class public final enum LX/AP5;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/AP5;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/AP5;

.field public static final enum FRIENDS_OF_TAGGEES:LX/AP5;

.field public static final enum NONE:LX/AP5;

.field public static final enum TAGGEES:LX/AP5;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1669953
    new-instance v0, LX/AP5;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LX/AP5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AP5;->NONE:LX/AP5;

    .line 1669954
    new-instance v0, LX/AP5;

    const-string v1, "TAGGEES"

    invoke-direct {v0, v1, v3}, LX/AP5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AP5;->TAGGEES:LX/AP5;

    .line 1669955
    new-instance v0, LX/AP5;

    const-string v1, "FRIENDS_OF_TAGGEES"

    invoke-direct {v0, v1, v4}, LX/AP5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AP5;->FRIENDS_OF_TAGGEES:LX/AP5;

    .line 1669956
    const/4 v0, 0x3

    new-array v0, v0, [LX/AP5;

    sget-object v1, LX/AP5;->NONE:LX/AP5;

    aput-object v1, v0, v2

    sget-object v1, LX/AP5;->TAGGEES:LX/AP5;

    aput-object v1, v0, v3

    sget-object v1, LX/AP5;->FRIENDS_OF_TAGGEES:LX/AP5;

    aput-object v1, v0, v4

    sput-object v0, LX/AP5;->$VALUES:[LX/AP5;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1669957
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/AP5;
    .locals 1

    .prologue
    .line 1669952
    const-class v0, LX/AP5;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/AP5;

    return-object v0
.end method

.method public static values()[LX/AP5;
    .locals 1

    .prologue
    .line 1669951
    sget-object v0, LX/AP5;->$VALUES:[LX/AP5;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/AP5;

    return-object v0
.end method
