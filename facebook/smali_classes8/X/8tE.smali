.class public LX/8tE;
.super LX/8tD;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public final b:Lcom/facebook/userfilter/UserSearchService;

.field private final c:Ljava/lang/Boolean;

.field public final d:Ljava/lang/String;

.field public final e:LX/8vE;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1412646
    const-class v0, LX/8tE;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/8tE;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Zr;Landroid/content/Context;LX/8RL;Lcom/facebook/userfilter/UserSearchService;Ljava/lang/Boolean;LX/8vE;)V
    .locals 1
    .param p5    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .param p6    # LX/8vE;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1412647
    const/4 v0, 0x1

    invoke-direct {p0, p1, p3, p6, v0}, LX/8tD;-><init>(LX/0Zr;LX/8RK;LX/8vE;Z)V

    .line 1412648
    iput-object p4, p0, LX/8tE;->b:Lcom/facebook/userfilter/UserSearchService;

    .line 1412649
    iput-object p5, p0, LX/8tE;->c:Ljava/lang/Boolean;

    .line 1412650
    iput-object p6, p0, LX/8tE;->e:LX/8vE;

    .line 1412651
    const v0, 0x7f0823cc

    invoke-virtual {p2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/8tE;->d:Ljava/lang/String;

    .line 1412652
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1412653
    iget-object v0, p0, LX/8tE;->b:Lcom/facebook/userfilter/UserSearchService;

    .line 1412654
    iget-object p0, v0, Lcom/facebook/userfilter/UserSearchService;->e:Ljava/util/Set;

    invoke-interface {p0}, Ljava/util/Set;->clear()V

    .line 1412655
    iget-object p0, v0, Lcom/facebook/userfilter/UserSearchService;->e:Ljava/util/Set;

    invoke-interface {p0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 1412656
    return-void
.end method

.method public final b(Ljava/lang/CharSequence;)LX/39y;
    .locals 5

    .prologue
    .line 1412657
    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_4

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1412658
    if-nez v0, :cond_1

    iget-object v0, p0, LX/8tE;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1412659
    :cond_1
    invoke-super {p0, p1}, LX/8tD;->b(Ljava/lang/CharSequence;)LX/39y;

    move-result-object v0

    .line 1412660
    :goto_1
    return-object v0

    .line 1412661
    :cond_2
    new-instance v0, LX/39y;

    invoke-direct {v0}, LX/39y;-><init>()V

    .line 1412662
    :try_start_0
    iget-object v1, p0, LX/8tE;->b:Lcom/facebook/userfilter/UserSearchService;

    invoke-virtual {v1, p1}, Lcom/facebook/userfilter/UserSearchService;->a(Ljava/lang/CharSequence;)LX/0Px;

    move-result-object v1

    .line 1412663
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1412664
    new-instance v3, LX/623;

    iget-object v4, p0, LX/8tE;->d:Ljava/lang/String;

    invoke-direct {v3, v4, v1}, LX/623;-><init>(Ljava/lang/String;LX/0Px;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1412665
    iget-object v1, p0, LX/8tE;->e:LX/8vE;

    invoke-virtual {v1}, LX/8vE;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v3, v1, -0x1

    .line 1412666
    const/4 v1, 0x0

    :goto_2
    if-ge v1, v3, :cond_3

    .line 1412667
    new-instance v4, LX/623;

    invoke-direct {v4}, LX/623;-><init>()V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1412668
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1412669
    :cond_3
    iput-object v2, v0, LX/39y;->a:Ljava/lang/Object;

    .line 1412670
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    iput v1, v0, LX/39y;->b:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1412671
    goto :goto_1

    .line 1412672
    :catch_0
    invoke-super {p0, p1}, LX/8tD;->b(Ljava/lang/CharSequence;)LX/39y;

    move-result-object v0

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1412673
    iget-object v0, p0, LX/8tE;->b:Lcom/facebook/userfilter/UserSearchService;

    .line 1412674
    iget-object p0, v0, Lcom/facebook/userfilter/UserSearchService;->f:Ljava/util/Set;

    invoke-interface {p0}, Ljava/util/Set;->clear()V

    .line 1412675
    iget-object p0, v0, Lcom/facebook/userfilter/UserSearchService;->f:Ljava/util/Set;

    invoke-interface {p0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 1412676
    return-void
.end method
