.class public LX/8nK;
.super LX/8nB;
.source ""


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/3iT;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/3iT;)V
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/3iT;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1400769
    invoke-direct {p0}, LX/8nB;-><init>()V

    .line 1400770
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/8nK;->c:Ljava/util/List;

    .line 1400771
    iput-object p1, p0, LX/8nK;->a:LX/0Or;

    .line 1400772
    iput-object p2, p0, LX/8nK;->b:LX/3iT;

    .line 1400773
    return-void
.end method

.method public static b(LX/0QB;)LX/8nK;
    .locals 3

    .prologue
    .line 1400810
    new-instance v1, LX/8nK;

    const/16 v0, 0x15e8

    invoke-static {p0, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p0}, LX/3iT;->b(LX/0QB;)LX/3iT;

    move-result-object v0

    check-cast v0, LX/3iT;

    invoke-direct {v1, v2, v0}, LX/8nK;-><init>(LX/0Or;LX/3iT;)V

    .line 1400811
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;ZZZZ)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "ZZZZ)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1400792
    iget-object v0, p0, LX/8nK;->c:Ljava/util/List;

    .line 1400793
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 1400794
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/tagging/model/TaggingProfile;

    .line 1400795
    if-nez p2, :cond_1

    .line 1400796
    iget-object p0, v1, Lcom/facebook/tagging/model/TaggingProfile;->e:LX/7Gr;

    move-object p0, p0

    .line 1400797
    sget-object p1, LX/7Gr;->SELF:LX/7Gr;

    if-eq p0, p1, :cond_0

    .line 1400798
    :cond_1
    if-nez p3, :cond_2

    .line 1400799
    iget-object p0, v1, Lcom/facebook/tagging/model/TaggingProfile;->e:LX/7Gr;

    move-object p0, p0

    .line 1400800
    sget-object p1, LX/7Gr;->USER:LX/7Gr;

    if-eq p0, p1, :cond_0

    .line 1400801
    :cond_2
    if-nez p4, :cond_3

    .line 1400802
    iget-object p0, v1, Lcom/facebook/tagging/model/TaggingProfile;->e:LX/7Gr;

    move-object p0, p0

    .line 1400803
    sget-object p1, LX/7Gr;->PAGE:LX/7Gr;

    if-eq p0, p1, :cond_0

    .line 1400804
    :cond_3
    if-nez p5, :cond_4

    .line 1400805
    iget-object p0, v1, Lcom/facebook/tagging/model/TaggingProfile;->e:LX/7Gr;

    move-object p0, p0

    .line 1400806
    sget-object p1, LX/7Gr;->TEXT:LX/7Gr;

    if-eq p0, p1, :cond_0

    .line 1400807
    :cond_4
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1400808
    :cond_5
    move-object v0, v2

    .line 1400809
    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 12

    .prologue
    .line 1400776
    if-nez p1, :cond_0

    .line 1400777
    :goto_0
    return-void

    .line 1400778
    :cond_0
    iget-object v0, p0, LX/8nK;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1400779
    const/4 v0, 0x0

    :goto_1
    invoke-static {p1}, LX/16z;->f(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 1400780
    invoke-static {p1, v0}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLFeedback;I)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v1

    const/4 v8, 0x0

    .line 1400781
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v7

    .line 1400782
    if-eqz v7, :cond_1

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1400783
    iget-object v11, p0, LX/8nK;->c:Ljava/util/List;

    iget-object v2, p0, LX/8nK;->b:LX/3iT;

    new-instance v3, Lcom/facebook/user/model/Name;

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v8, v8, v4}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    if-nez v6, :cond_3

    move-object v6, v8

    .line 1400784
    :goto_2
    iget-object v9, p0, LX/8nK;->a:LX/0Or;

    invoke-interface {v9}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 1400785
    if-eqz v9, :cond_4

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 1400786
    sget-object v9, LX/7Gr;->SELF:LX/7Gr;

    .line 1400787
    :goto_3
    move-object v7, v9

    .line 1400788
    const-string v9, "comments"

    sget-object v10, LX/8nE;->COMMENT_AUTHORS:LX/8nE;

    invoke-virtual {v10}, LX/8nE;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {v2 .. v10}, LX/3iT;->a(Lcom/facebook/user/model/Name;JLjava/lang/String;LX/7Gr;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/tagging/model/TaggingProfile;

    move-result-object v2

    invoke-interface {v11, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1400789
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1400790
    :cond_2
    iget-object v0, p0, LX/8nK;->c:Ljava/util/List;

    invoke-static {v0}, Lcom/facebook/tagging/model/TaggingProfile;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/8nK;->c:Ljava/util/List;

    goto :goto_0

    .line 1400791
    :cond_3
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v6

    goto :goto_2

    :cond_4
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    invoke-static {v9}, Lcom/facebook/tagging/model/TaggingProfile;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;)LX/7Gr;

    move-result-object v9

    goto :goto_3
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1400775
    const-string v0, "comments"

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1400774
    sget-object v0, LX/8nE;->COMMENT_AUTHORS:LX/8nE;

    invoke-virtual {v0}, LX/8nE;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
