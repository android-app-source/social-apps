.class public final enum LX/9lB;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9lB;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9lB;

.field public static final enum ACTIVE:LX/9lB;

.field public static final enum INACTIVE:LX/9lB;


# instance fields
.field public final value:I


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1532500
    new-instance v0, LX/9lB;

    const-string v1, "INACTIVE"

    invoke-direct {v0, v1, v2, v2}, LX/9lB;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/9lB;->INACTIVE:LX/9lB;

    .line 1532501
    new-instance v0, LX/9lB;

    const-string v1, "ACTIVE"

    invoke-direct {v0, v1, v3, v3}, LX/9lB;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/9lB;->ACTIVE:LX/9lB;

    .line 1532502
    const/4 v0, 0x2

    new-array v0, v0, [LX/9lB;

    sget-object v1, LX/9lB;->INACTIVE:LX/9lB;

    aput-object v1, v0, v2

    sget-object v1, LX/9lB;->ACTIVE:LX/9lB;

    aput-object v1, v0, v3

    sput-object v0, LX/9lB;->$VALUES:[LX/9lB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 1532495
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1532496
    iput p3, p0, LX/9lB;->value:I

    .line 1532497
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9lB;
    .locals 1

    .prologue
    .line 1532498
    const-class v0, LX/9lB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9lB;

    return-object v0
.end method

.method public static values()[LX/9lB;
    .locals 1

    .prologue
    .line 1532499
    sget-object v0, LX/9lB;->$VALUES:[LX/9lB;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9lB;

    return-object v0
.end method
