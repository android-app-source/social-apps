.class public LX/8pb;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# static fields
.field private static final c:Ljava/lang/Object;


# instance fields
.field private final a:I

.field public final b:LX/0aq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aq",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1406114
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/8pb;->c:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/16 v1, 0x14

    .line 1406115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1406116
    iput v1, p0, LX/8pb;->a:I

    .line 1406117
    new-instance v0, LX/0aq;

    invoke-direct {v0, v1}, LX/0aq;-><init>(I)V

    iput-object v0, p0, LX/8pb;->b:LX/0aq;

    .line 1406118
    return-void
.end method

.method public static a(LX/0QB;)LX/8pb;
    .locals 7

    .prologue
    .line 1406119
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 1406120
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 1406121
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 1406122
    if-nez v1, :cond_0

    .line 1406123
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1406124
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 1406125
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 1406126
    sget-object v1, LX/8pb;->c:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1406127
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 1406128
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1406129
    :cond_1
    if-nez v1, :cond_4

    .line 1406130
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 1406131
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 1406132
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    .line 1406133
    new-instance v0, LX/8pb;

    invoke-direct {v0}, LX/8pb;-><init>()V

    .line 1406134
    move-object v1, v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1406135
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 1406136
    if-nez v1, :cond_2

    .line 1406137
    sget-object v0, LX/8pb;->c:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8pb;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1406138
    :goto_1
    if-eqz v0, :cond_3

    .line 1406139
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1406140
    :goto_3
    check-cast v0, LX/8pb;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1406141
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 1406142
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1406143
    :catchall_1
    move-exception v0

    .line 1406144
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1406145
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1406146
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 1406147
    :cond_2
    :try_start_8
    sget-object v0, LX/8pb;->c:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8pb;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;
    .locals 1

    .prologue
    .line 1406148
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1406149
    const/4 v0, 0x0

    .line 1406150
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/8pb;->b:LX/0aq;

    invoke-virtual {v0, p1}, LX/0aq;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1406151
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1406152
    :cond_0
    :goto_0
    return v0

    .line 1406153
    :cond_1
    iget-object v1, p0, LX/8pb;->b:LX/0aq;

    invoke-virtual {v1}, LX/0aq;->b()I

    move-result v1

    .line 1406154
    iget-object v2, p0, LX/8pb;->b:LX/0aq;

    invoke-virtual {v2, p1, p2}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1406155
    iget-object v2, p0, LX/8pb;->b:LX/0aq;

    invoke-virtual {v2}, LX/0aq;->b()I

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
