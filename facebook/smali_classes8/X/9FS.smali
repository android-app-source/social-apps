.class public final LX/9FS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/9FT;

.field public final synthetic b:Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;LX/9FT;)V
    .locals 0

    .prologue
    .line 1458599
    iput-object p1, p0, LX/9FS;->b:Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;

    iput-object p2, p0, LX/9FS;->a:LX/9FT;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x7bd6fdd2

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1458590
    iget-object v0, p0, LX/9FS;->b:Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;

    iget-object v2, v0, Lcom/facebook/feedback/ui/rows/CommentAddPlaceInfoAttachmentPartDefinition;->d:LX/9jH;

    iget-object v0, p0, LX/9FS;->a:LX/9FT;

    iget-object v3, v0, LX/9FT;->b:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v4, Landroid/app/Activity;

    invoke-static {v0, v4}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iget-object v4, p0, LX/9FS;->a:LX/9FT;

    iget-object v4, v4, LX/9FT;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v4}, LX/1WF;->h(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    .line 1458591
    sget-object v6, LX/9jG;->SOCIAL_SEARCH_COMMENT:LX/9jG;

    const-string p0, "add_location_comment_place_info"

    invoke-static {v2, v6, p0}, LX/9jH;->a(LX/9jH;LX/9jG;Ljava/lang/String;)LX/9jF;

    move-result-object v6

    .line 1458592
    iput-object v3, v6, LX/9jF;->d:Ljava/lang/String;

    .line 1458593
    move-object v6, v6

    .line 1458594
    iput-object v4, v6, LX/9jF;->v:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1458595
    move-object v6, v6

    .line 1458596
    invoke-virtual {v6}, LX/9jF;->a()Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    move-result-object p0

    .line 1458597
    iget-object v6, v2, LX/9jH;->a:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0, p0}, LX/9jD;->a(Landroid/content/Context;Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;)Landroid/content/Intent;

    move-result-object p0

    const/16 p1, 0x138a

    invoke-interface {v6, p0, p1, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1458598
    const v0, -0xaf73288

    invoke-static {v5, v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
