.class public LX/95j;
.super LX/2ni;
.source ""

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/2ni",
        "<TT;>;",
        "Ljava/io/Closeable;"
    }
.end annotation


# instance fields
.field private final a:LX/2nf;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mCursor"
    .end annotation
.end field

.field private final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/2nm;",
            ">;"
        }
    .end annotation
.end field

.field private c:J

.field private d:Ljava/lang/Throwable;


# direct methods
.method public constructor <init>(LX/2nf;)V
    .locals 2

    .prologue
    .line 1437251
    invoke-direct {p0}, LX/2ni;-><init>()V

    .line 1437252
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/95j;->c:J

    .line 1437253
    const/4 v0, 0x0

    iput-object v0, p0, LX/95j;->d:Ljava/lang/Throwable;

    .line 1437254
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nf;

    iput-object v0, p0, LX/95j;->a:LX/2nf;

    .line 1437255
    invoke-static {p1}, LX/95j;->a(LX/2nf;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/95j;->b:LX/0Px;

    .line 1437256
    invoke-virtual {p0}, LX/95j;->j()V

    .line 1437257
    return-void
.end method

.method public constructor <init>(LX/95j;)V
    .locals 2

    .prologue
    .line 1437258
    invoke-direct {p0}, LX/2ni;-><init>()V

    .line 1437259
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/95j;->c:J

    .line 1437260
    const/4 v0, 0x0

    iput-object v0, p0, LX/95j;->d:Ljava/lang/Throwable;

    .line 1437261
    iget-object v1, p1, LX/95j;->a:LX/2nf;

    monitor-enter v1

    .line 1437262
    :try_start_0
    iget-object v0, p1, LX/95j;->a:LX/2nf;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nf;

    iput-object v0, p0, LX/95j;->a:LX/2nf;

    .line 1437263
    iget-object v0, p0, LX/95j;->a:LX/2nf;

    invoke-static {v0}, LX/95j;->a(LX/2nf;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/95j;->b:LX/0Px;

    .line 1437264
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1437265
    invoke-virtual {p0}, LX/95j;->j()V

    .line 1437266
    return-void

    .line 1437267
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static a(LX/2nf;)LX/0Px;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2nf;",
            ")",
            "LX/0Px",
            "<",
            "LX/2nm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1437268
    invoke-interface {p0}, LX/2nf;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1437269
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1437270
    :goto_0
    return-object v0

    .line 1437271
    :cond_0
    invoke-interface {p0}, LX/2nf;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "CHUNKS"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 1437272
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1437273
    :cond_1
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1437274
    goto :goto_0

    .line 1437275
    :cond_2
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 1437276
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_3

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    .line 1437277
    new-instance v5, LX/2nl;

    iget-object v6, v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->a:Ljava/lang/String;

    iget-object v7, v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->c:Ljava/lang/String;

    sget-object v8, LX/2nk;->BEFORE:LX/2nk;

    iget-boolean v9, v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->e:Z

    invoke-direct {v5, v6, v7, v8, v9}, LX/2nl;-><init>(Ljava/lang/String;Ljava/lang/String;LX/2nk;Z)V

    .line 1437278
    new-instance v6, LX/2nl;

    iget-object v7, v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->b:Ljava/lang/String;

    iget-object v8, v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->d:Ljava/lang/String;

    sget-object v9, LX/2nk;->AFTER:LX/2nk;

    iget-boolean v0, v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->f:Z

    invoke-direct {v6, v7, v8, v9, v0}, LX/2nl;-><init>(Ljava/lang/String;Ljava/lang/String;LX/2nk;Z)V

    .line 1437279
    new-instance v0, LX/2nm;

    invoke-direct {v0, v5, v6}, LX/2nm;-><init>(LX/2nj;LX/2nj;)V

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1437280
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1437281
    :cond_3
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method private a([JZ)Landroid/util/SparseArray;
    .locals 8
    .param p1    # [J
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([JZ)",
            "Landroid/util/SparseArray",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 1437282
    if-eqz p1, :cond_5

    array-length v0, p1

    if-lez v0, :cond_5

    .line 1437283
    invoke-static {p1}, Ljava/util/Arrays;->sort([J)V

    .line 1437284
    iget-object v5, p0, LX/95j;->a:LX/2nf;

    monitor-enter v5

    .line 1437285
    :try_start_0
    iget-object v0, p0, LX/95j;->a:LX/2nf;

    invoke-interface {v0}, LX/2nf;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1437286
    new-instance v0, Landroid/util/SparseArray;

    array-length v2, p1

    invoke-direct {v0, v2}, Landroid/util/SparseArray;-><init>(I)V

    move v2, v4

    .line 1437287
    :cond_0
    iget-object v3, p0, LX/95j;->a:LX/2nf;

    invoke-interface {v3}, LX/2nf;->a()J

    move-result-wide v6

    invoke-static {p1, v6, v7}, Ljava/util/Arrays;->binarySearch([JJ)I

    move-result v3

    .line 1437288
    if-ltz v3, :cond_3

    array-length v6, p1

    if-ge v3, v6, :cond_3

    const/4 v3, 0x1

    .line 1437289
    :goto_0
    if-eqz v3, :cond_1

    .line 1437290
    add-int/lit8 v2, v2, 0x1

    .line 1437291
    iget-object v3, p0, LX/95j;->a:LX/2nf;

    invoke-interface {v3}, LX/2nf;->getPosition()I

    move-result v6

    .line 1437292
    if-eqz p2, :cond_6

    .line 1437293
    iget-object v3, p0, LX/95j;->a:LX/2nf;

    invoke-interface {v3}, LX/2nf;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v3

    .line 1437294
    :goto_1
    invoke-virtual {v0, v6, v3}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 1437295
    :cond_1
    array-length v3, p1

    if-ge v2, v3, :cond_2

    iget-object v3, p0, LX/95j;->a:LX/2nf;

    invoke-interface {v3}, LX/2nf;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1437296
    :cond_2
    monitor-exit v5

    .line 1437297
    :goto_2
    return-object v0

    :cond_3
    move v3, v4

    .line 1437298
    goto :goto_0

    .line 1437299
    :cond_4
    monitor-exit v5

    :cond_5
    move-object v0, v1

    .line 1437300
    goto :goto_2

    .line 1437301
    :catchall_0
    move-exception v0

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_6
    move-object v3, v1

    goto :goto_1
.end method

.method private b([JZ)LX/0tf;
    .locals 10
    .param p1    # [J
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([JZ)",
            "LX/0tf",
            "<",
            "LX/3rL",
            "<",
            "Ljava/lang/Integer;",
            "TT;>;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 1437303
    if-eqz p1, :cond_5

    array-length v0, p1

    if-lez v0, :cond_5

    .line 1437304
    invoke-static {p1}, Ljava/util/Arrays;->sort([J)V

    .line 1437305
    iget-object v5, p0, LX/95j;->a:LX/2nf;

    monitor-enter v5

    .line 1437306
    :try_start_0
    iget-object v0, p0, LX/95j;->a:LX/2nf;

    invoke-interface {v0}, LX/2nf;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1437307
    new-instance v0, LX/0tf;

    array-length v2, p1

    invoke-direct {v0, v2}, LX/0tf;-><init>(I)V

    move v2, v4

    .line 1437308
    :cond_0
    iget-object v3, p0, LX/95j;->a:LX/2nf;

    invoke-interface {v3}, LX/2nf;->a()J

    move-result-wide v6

    .line 1437309
    invoke-static {p1, v6, v7}, Ljava/util/Arrays;->binarySearch([JJ)I

    move-result v3

    .line 1437310
    if-ltz v3, :cond_3

    array-length v8, p1

    if-ge v3, v8, :cond_3

    const/4 v3, 0x1

    .line 1437311
    :goto_0
    if-eqz v3, :cond_1

    .line 1437312
    add-int/lit8 v2, v2, 0x1

    .line 1437313
    iget-object v3, p0, LX/95j;->a:LX/2nf;

    invoke-interface {v3}, LX/2nf;->getPosition()I

    move-result v8

    .line 1437314
    if-eqz p2, :cond_6

    .line 1437315
    iget-object v3, p0, LX/95j;->a:LX/2nf;

    invoke-interface {v3}, LX/2nf;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v3

    .line 1437316
    :goto_1
    new-instance v9, LX/3rL;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-direct {v9, v8, v3}, LX/3rL;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v6, v7, v9}, LX/0tf;->c(JLjava/lang/Object;)V

    .line 1437317
    :cond_1
    array-length v3, p1

    if-ge v2, v3, :cond_2

    iget-object v3, p0, LX/95j;->a:LX/2nf;

    invoke-interface {v3}, LX/2nf;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1437318
    :cond_2
    monitor-exit v5

    .line 1437319
    :goto_2
    return-object v0

    :cond_3
    move v3, v4

    .line 1437320
    goto :goto_0

    .line 1437321
    :cond_4
    monitor-exit v5

    :cond_5
    move-object v0, v1

    .line 1437322
    goto :goto_2

    .line 1437323
    :catchall_0
    move-exception v0

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_6
    move-object v3, v1

    goto :goto_1
.end method


# virtual methods
.method public a(LX/95j;LX/2nj;[J[J[J)LX/0Px;
    .locals 8
    .param p1    # LX/95j;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/2nj;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # [J
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # [J
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # [J
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/95j",
            "<TT;>;",
            "LX/2nj;",
            "[J[J[J)",
            "LX/0Px",
            "<",
            "LX/3CY;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 1437302
    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, LX/95j;->a(LX/95j;[J[J[JLX/95f;LX/95f;LX/95f;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/95j;[J[J[JLX/95f;LX/95f;LX/95f;)LX/0Px;
    .locals 11
    .param p1    # LX/95j;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # [J
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # [J
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # [J
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/95f;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/95f;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/95j",
            "<TT;>;[J[J[J",
            "LX/95f",
            "<TT;>;",
            "LX/95f",
            "<TT;>;",
            "LX/95f",
            "<TT;>;)",
            "LX/0Px",
            "<",
            "LX/3CY;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1437324
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1437325
    if-eqz p1, :cond_2

    invoke-virtual {p1}, LX/95j;->c()I

    move-result v2

    if-lez v2, :cond_2

    .line 1437326
    const/4 v2, 0x0

    invoke-direct {p1, p2, v2}, LX/95j;->a([JZ)Landroid/util/SparseArray;

    move-result-object v3

    .line 1437327
    if-eqz v3, :cond_2

    .line 1437328
    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_0
    if-ltz v2, :cond_2

    .line 1437329
    if-eqz p5, :cond_0

    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v5

    const/4 v6, 0x0

    move-object/from16 v0, p5

    invoke-interface {v0, v5, v6}, LX/95f;->a(ILjava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1437330
    :cond_0
    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v5

    const/4 v6, 0x1

    invoke-static {v5, v6}, LX/3CY;->b(II)LX/3CY;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1437331
    :cond_1
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 1437332
    :cond_2
    if-eqz p6, :cond_5

    const/4 v2, 0x1

    :goto_1
    invoke-direct {p0, p3, v2}, LX/95j;->a([JZ)Landroid/util/SparseArray;

    move-result-object v5

    .line 1437333
    if-eqz v5, :cond_3

    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-lez v2, :cond_3

    .line 1437334
    const/4 v2, 0x0

    invoke-virtual {v5, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v6

    .line 1437335
    if-nez p6, :cond_6

    .line 1437336
    array-length v2, p3

    invoke-static {v6, v2}, LX/3CY;->a(II)LX/3CY;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1437337
    :cond_3
    :goto_2
    if-eqz p4, :cond_b

    if-eqz p1, :cond_b

    invoke-virtual {p1}, LX/95j;->c()I

    move-result v2

    if-lez v2, :cond_b

    .line 1437338
    if-eqz p7, :cond_8

    const/4 v2, 0x1

    :goto_3
    invoke-direct {p0, p4, v2}, LX/95j;->b([JZ)LX/0tf;

    move-result-object v5

    .line 1437339
    const/4 v2, 0x0

    invoke-direct {p1, p4, v2}, LX/95j;->b([JZ)LX/0tf;

    move-result-object v6

    .line 1437340
    if-eqz v5, :cond_b

    if-eqz v6, :cond_b

    .line 1437341
    invoke-virtual {v5}, LX/0tf;->a()I

    move-result v2

    invoke-virtual {v6}, LX/0tf;->a()I

    move-result v3

    if-ne v2, v3, :cond_9

    const/4 v2, 0x1

    :goto_4
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 1437342
    const/4 v2, 0x0

    move v3, v2

    :goto_5
    invoke-virtual {v5}, LX/0tf;->a()I

    move-result v2

    if-ge v3, v2, :cond_b

    .line 1437343
    invoke-virtual {v5, v3}, LX/0tf;->b(I)J

    move-result-wide v8

    .line 1437344
    invoke-virtual {v5, v8, v9}, LX/0tf;->a(J)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3rL;

    iget-object v2, v2, LX/3rL;->a:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 1437345
    invoke-virtual {v6, v8, v9}, LX/0tf;->a(J)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3rL;

    iget-object v2, v2, LX/3rL;->a:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 1437346
    const/4 v10, 0x1

    invoke-static {v2, v7, v10}, LX/3CY;->a(III)LX/3CY;

    move-result-object v10

    .line 1437347
    if-eqz p7, :cond_a

    .line 1437348
    invoke-virtual {v5, v8, v9}, LX/0tf;->a(J)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3rL;

    iget-object v2, v2, LX/3rL;->b:Ljava/lang/Object;

    move-object/from16 v0, p7

    invoke-interface {v0, v7, v2}, LX/95f;->a(ILjava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1437349
    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1437350
    :cond_4
    :goto_6
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_5

    .line 1437351
    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 1437352
    :cond_6
    const/4 v3, 0x1

    .line 1437353
    const/4 v2, 0x0

    :goto_7
    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    move-result v7

    if-ge v2, v7, :cond_7

    .line 1437354
    invoke-virtual {v5, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v7

    .line 1437355
    add-int v8, v6, v2

    move-object/from16 v0, p6

    invoke-interface {v0, v8, v7}, LX/95f;->a(ILjava/lang/Object;)Z

    move-result v7

    and-int/2addr v3, v7

    .line 1437356
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 1437357
    :cond_7
    if-eqz v3, :cond_3

    .line 1437358
    array-length v2, p3

    invoke-static {v6, v2}, LX/3CY;->a(II)LX/3CY;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 1437359
    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 1437360
    :cond_9
    const/4 v2, 0x0

    goto :goto_4

    .line 1437361
    :cond_a
    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 1437362
    :cond_b
    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    return-object v2
.end method

.method public a(I)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 1437236
    iget-object v1, p0, LX/95j;->a:LX/2nf;

    monitor-enter v1

    .line 1437237
    :try_start_0
    invoke-virtual {p0}, LX/95j;->j()V

    .line 1437238
    iget-object v0, p0, LX/95j;->a:LX/2nf;

    invoke-interface {v0, p1}, LX/2nf;->moveToPosition(I)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1437239
    iget-object v0, p0, LX/95j;->a:LX/2nf;

    invoke-interface {v0}, LX/2nf;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1437240
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(II)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1437241
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p2}, Ljava/util/ArrayList;-><init>(I)V

    .line 1437242
    iget-object v2, p0, LX/95j;->a:LX/2nf;

    monitor-enter v2

    .line 1437243
    :try_start_0
    iget-object v0, p0, LX/95j;->a:LX/2nf;

    invoke-interface {v0, p1}, LX/2nf;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1437244
    const/4 v0, 0x0

    .line 1437245
    :cond_0
    iget-object v3, p0, LX/95j;->a:LX/2nf;

    invoke-interface {v3}, LX/2nf;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1437246
    add-int/lit8 v0, v0, 0x1

    .line 1437247
    iget-object v3, p0, LX/95j;->a:LX/2nf;

    invoke-interface {v3}, LX/2nf;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    if-lt v0, p2, :cond_0

    .line 1437248
    :cond_1
    monitor-exit v2

    .line 1437249
    return-object v1

    .line 1437250
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public c()I
    .locals 2

    .prologue
    .line 1437192
    iget-object v1, p0, LX/95j;->a:LX/2nf;

    monitor-enter v1

    .line 1437193
    :try_start_0
    invoke-virtual {p0}, LX/95j;->j()V

    .line 1437194
    iget-object v0, p0, LX/95j;->a:LX/2nf;

    invoke-interface {v0}, LX/2nf;->getCount()I

    move-result v0

    monitor-exit v1

    return v0

    .line 1437195
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public close()V
    .locals 3

    .prologue
    .line 1437196
    iget-object v1, p0, LX/95j;->a:LX/2nf;

    monitor-enter v1

    .line 1437197
    :try_start_0
    invoke-virtual {p0}, LX/95j;->j()V

    .line 1437198
    iget-object v0, p0, LX/95j;->a:LX/2nf;

    invoke-interface {v0}, LX/2nf;->close()V

    .line 1437199
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 1437200
    if-eqz v0, :cond_0

    .line 1437201
    new-instance v0, Ljava/lang/Exception;

    const-string v2, "Cursor was closed here"

    invoke-direct {v0, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/95j;->d:Ljava/lang/Throwable;

    .line 1437202
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2nm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1437203
    iget-object v0, p0, LX/95j;->b:LX/0Px;

    return-object v0
.end method

.method public f()V
    .locals 8

    .prologue
    .line 1437204
    iget-object v2, p0, LX/95j;->a:LX/2nf;

    monitor-enter v2

    .line 1437205
    :try_start_0
    iget-object v0, p0, LX/95j;->a:LX/2nf;

    instance-of v0, v0, LX/2nd;

    if-nez v0, :cond_0

    .line 1437206
    monitor-exit v2

    .line 1437207
    :goto_0
    return-void

    .line 1437208
    :cond_0
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    mul-double/2addr v0, v4

    double-to-int v0, v0

    .line 1437209
    iget-object v1, p0, LX/95j;->a:LX/2nf;

    invoke-interface {v1, v0}, LX/2nf;->moveToPosition(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    move v1, v0

    .line 1437210
    :try_start_1
    iget-object v0, p0, LX/95j;->a:LX/2nf;

    check-cast v0, LX/2nd;

    invoke-virtual {v0}, LX/2nd;->e()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1437211
    :goto_1
    :try_start_2
    iget-object v3, p0, LX/95j;->a:LX/2nf;

    add-int/lit8 v0, v1, 0xa

    invoke-interface {v3, v0}, LX/2nf;->moveToPosition(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1437212
    :cond_2
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1437213
    :catch_0
    move-exception v0

    .line 1437214
    :try_start_3
    const-class v3, LX/95j;

    const-string v4, "Model file at row %d is not valid"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v0, v4, v5}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public g()I
    .locals 4

    .prologue
    .line 1437215
    iget-object v1, p0, LX/95j;->a:LX/2nf;

    monitor-enter v1

    .line 1437216
    :try_start_0
    iget-object v0, p0, LX/95j;->a:LX/2nf;

    invoke-interface {v0}, LX/2nf;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "SESSION_VERSION"

    const/4 v3, -0x1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    monitor-exit v1

    return v0

    .line 1437217
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public h()Z
    .locals 2

    .prologue
    .line 1437218
    iget-object v1, p0, LX/95j;->a:LX/2nf;

    monitor-enter v1

    .line 1437219
    :try_start_0
    iget-object v0, p0, LX/95j;->a:LX/2nf;

    invoke-interface {v0}, LX/2nf;->isClosed()Z

    move-result v0

    monitor-exit v1

    return v0

    .line 1437220
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public declared-synchronized i()J
    .locals 5

    .prologue
    const-wide/16 v0, 0x0

    .line 1437221
    monitor-enter p0

    :try_start_0
    iget-wide v2, p0, LX/95j;->c:J

    cmp-long v2, v2, v0

    if-lez v2, :cond_1

    .line 1437222
    iget-wide v0, p0, LX/95j;->c:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1437223
    :cond_0
    :goto_0
    monitor-exit p0

    return-wide v0

    .line 1437224
    :cond_1
    :try_start_1
    iget-object v2, p0, LX/95j;->a:LX/2nf;

    monitor-enter v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1437225
    :try_start_2
    iget-object v3, p0, LX/95j;->a:LX/2nf;

    invoke-interface {v3}, LX/2nf;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "CHUNKS"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 1437226
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1437227
    if-eqz v3, :cond_0

    :try_start_3
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1437228
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    iget-wide v0, v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->h:J

    iput-wide v0, p0, LX/95j;->c:J

    .line 1437229
    iget-wide v0, p0, LX/95j;->c:J
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    .line 1437230
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1437231
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final j()V
    .locals 4

    .prologue
    .line 1437232
    iget-object v1, p0, LX/95j;->a:LX/2nf;

    monitor-enter v1

    .line 1437233
    :try_start_0
    iget-object v0, p0, LX/95j;->a:LX/2nf;

    invoke-interface {v0}, LX/2nf;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1437234
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Attempting to access closed connection state"

    iget-object v3, p0, LX/95j;->d:Ljava/lang/Throwable;

    invoke-direct {v0, v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 1437235
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method
