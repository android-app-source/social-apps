.class public LX/9hX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4VT;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/4VT",
        "<",
        "Lcom/facebook/graphql/model/GraphQLMedia;",
        "LX/4XC;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/facebook/graphql/model/GraphQLPlace;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLPlace;)V
    .locals 1
    .param p2    # Lcom/facebook/graphql/model/GraphQLPlace;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1526722
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1526723
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/9hX;->a:Ljava/lang/String;

    .line 1526724
    iput-object p2, p0, LX/9hX;->b:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 1526725
    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1526715
    iget-object v0, p0, LX/9hX;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/16f;LX/40U;)V
    .locals 2

    .prologue
    .line 1526718
    check-cast p1, Lcom/facebook/graphql/model/GraphQLMedia;

    check-cast p2, LX/4XC;

    .line 1526719
    iget-object v0, p0, LX/9hX;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1526720
    :goto_0
    return-void

    .line 1526721
    :cond_0
    iget-object v0, p0, LX/9hX;->b:Lcom/facebook/graphql/model/GraphQLPlace;

    invoke-virtual {p2, v0}, LX/4XC;->a(Lcom/facebook/graphql/model/GraphQLPlace;)V

    goto :goto_0
.end method

.method public final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMedia;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1526717
    const-class v0, Lcom/facebook/graphql/model/GraphQLMedia;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1526716
    const-string v0, "EditLocationMutatingVisitor"

    return-object v0
.end method
