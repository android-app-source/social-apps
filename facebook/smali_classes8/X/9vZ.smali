.class public final LX/9vZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:I

.field public d:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel$FirstTeamObjectModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:I

.field public g:Z

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel$MatchPageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:J

.field public k:I

.field public l:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel$SecondTeamObjectModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:I

.field public o:Z

.field public p:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel$MatchModel$WinningTeamModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1573129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel;
    .locals 14

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x0

    const/4 v11, 0x0

    .line 1573130
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1573131
    iget-object v1, p0, LX/9vZ;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1573132
    iget-object v2, p0, LX/9vZ;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1573133
    iget-object v3, p0, LX/9vZ;->d:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel$FirstTeamObjectModel;

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1573134
    iget-object v4, p0, LX/9vZ;->e:Ljava/lang/String;

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1573135
    iget-object v5, p0, LX/9vZ;->h:Ljava/lang/String;

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1573136
    iget-object v6, p0, LX/9vZ;->i:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel$MatchPageModel;

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1573137
    iget-object v7, p0, LX/9vZ;->l:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel$SecondTeamObjectModel;

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1573138
    iget-object v8, p0, LX/9vZ;->m:Ljava/lang/String;

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1573139
    iget-object v9, p0, LX/9vZ;->p:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeMatchComponentFragmentModel$MatchModel$WinningTeamModel;

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1573140
    const/16 v10, 0x10

    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 1573141
    invoke-virtual {v0, v11, v1}, LX/186;->b(II)V

    .line 1573142
    invoke-virtual {v0, v13, v2}, LX/186;->b(II)V

    .line 1573143
    const/4 v1, 0x2

    iget v2, p0, LX/9vZ;->c:I

    invoke-virtual {v0, v1, v2, v11}, LX/186;->a(III)V

    .line 1573144
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1573145
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1573146
    const/4 v1, 0x5

    iget v2, p0, LX/9vZ;->f:I

    invoke-virtual {v0, v1, v2, v11}, LX/186;->a(III)V

    .line 1573147
    const/4 v1, 0x6

    iget-boolean v2, p0, LX/9vZ;->g:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 1573148
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1573149
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1573150
    const/16 v1, 0x9

    iget-wide v2, p0, LX/9vZ;->j:J

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1573151
    const/16 v1, 0xa

    iget v2, p0, LX/9vZ;->k:I

    invoke-virtual {v0, v1, v2, v11}, LX/186;->a(III)V

    .line 1573152
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1573153
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1573154
    const/16 v1, 0xd

    iget v2, p0, LX/9vZ;->n:I

    invoke-virtual {v0, v1, v2, v11}, LX/186;->a(III)V

    .line 1573155
    const/16 v1, 0xe

    iget-boolean v2, p0, LX/9vZ;->o:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 1573156
    const/16 v1, 0xf

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 1573157
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1573158
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1573159
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1573160
    invoke-virtual {v1, v11}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1573161
    new-instance v0, LX/15i;

    move-object v2, v12

    move-object v3, v12

    move v4, v13

    move-object v5, v12

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1573162
    new-instance v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel;

    invoke-direct {v1, v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel;-><init>(LX/15i;)V

    .line 1573163
    return-object v1
.end method
