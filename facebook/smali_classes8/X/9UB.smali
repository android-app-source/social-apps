.class public abstract LX/9UB;
.super LX/9UA;
.source ""


# instance fields
.field public final f:Landroid/content/Context;

.field public final g:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 1
    .param p2    # Landroid/database/Cursor;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1497679
    invoke-direct {p0}, LX/9UA;-><init>()V

    .line 1497680
    iput-object p1, p0, LX/9UB;->f:Landroid/content/Context;

    .line 1497681
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, LX/9UB;->g:Landroid/view/LayoutInflater;

    .line 1497682
    invoke-virtual {p0, p2}, LX/9UB;->a(Landroid/database/Cursor;)V

    .line 1497683
    return-void
.end method


# virtual methods
.method public a(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1497669
    invoke-virtual {p0, p1, p2}, LX/3Tf;->a(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookProfile;

    .line 1497670
    if-nez p4, :cond_0

    .line 1497671
    invoke-virtual {p0, v0}, LX/9UB;->a(Lcom/facebook/ipc/model/FacebookProfile;)Landroid/view/View;

    move-result-object p4

    .line 1497672
    :cond_0
    const v1, 0x7f0d057e

    invoke-virtual {p4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 1497673
    iget-object v2, v0, Lcom/facebook/ipc/model/FacebookProfile;->mImageUrl:Ljava/lang/String;

    .line 1497674
    if-nez v2, :cond_1

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 1497675
    iget-object v2, v0, Lcom/facebook/ipc/model/FacebookProfile;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 1497676
    iget-object v0, v0, Lcom/facebook/ipc/model/FacebookProfile;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1497677
    return-object p4

    .line 1497678
    :cond_1
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    goto :goto_0
.end method

.method public a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1497660
    if-nez p2, :cond_1

    .line 1497661
    if-nez p1, :cond_0

    .line 1497662
    iget-object v0, p0, LX/9UB;->g:Landroid/view/LayoutInflater;

    const v1, 0x7f031065

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    :goto_0
    move-object v0, v1

    .line 1497663
    check-cast v0, Landroid/widget/TextView;

    .line 1497664
    iget-object v2, p0, LX/9UA;->e:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/9US;

    .line 1497665
    iget-object p0, v2, LX/9US;->a:Ljava/lang/String;

    move-object v2, p0

    .line 1497666
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1497667
    return-object v1

    .line 1497668
    :cond_0
    iget-object v0, p0, LX/9UB;->g:Landroid/view/LayoutInflater;

    const v1, 0x7f031067

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    :cond_1
    move-object v1, p2

    goto :goto_0
.end method

.method public a(Lcom/facebook/ipc/model/FacebookProfile;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1497641
    iget-object v0, p0, LX/9UB;->g:Landroid/view/LayoutInflater;

    const v1, 0x7f031071

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1497642
    return-object v0
.end method

.method public abstract a(Ljava/lang/Object;)Ljava/lang/String;
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 9
    .param p1    # Landroid/database/Cursor;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 1497643
    iput-object p1, p0, LX/9UB;->d:Landroid/database/Cursor;

    .line 1497644
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/9UB;->e:Ljava/util/List;

    .line 1497645
    if-nez p1, :cond_0

    .line 1497646
    :goto_0
    return-void

    .line 1497647
    :cond_0
    const/4 v5, 0x0

    .line 1497648
    const/4 v2, -0x1

    .line 1497649
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v6

    .line 1497650
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move v3, v1

    move v0, v1

    .line 1497651
    :goto_1
    if-ge v3, v6, :cond_2

    .line 1497652
    invoke-virtual {p0, p1}, LX/9UB;->b(Landroid/database/Cursor;)Ljava/lang/Object;

    move-result-object v4

    .line 1497653
    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 1497654
    if-lez v0, :cond_1

    .line 1497655
    iget-object v7, p0, LX/9UA;->e:Ljava/util/List;

    new-instance v8, LX/9US;

    invoke-virtual {p0, v5}, LX/9UB;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v8, v5, v2, v0}, LX/9US;-><init>(Ljava/lang/String;II)V

    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    move v0, v1

    move v2, v3

    .line 1497656
    :goto_2
    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v0, v0, 0x1

    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-object v5, v4

    goto :goto_1

    .line 1497657
    :cond_2
    if-lez v0, :cond_3

    .line 1497658
    iget-object v1, p0, LX/9UA;->e:Ljava/util/List;

    new-instance v3, LX/9US;

    invoke-virtual {p0, v5}, LX/9UB;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v2, v0}, LX/9US;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1497659
    :cond_3
    const v0, -0x3e71cb9c

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0

    :cond_4
    move-object v4, v5

    goto :goto_2
.end method

.method public abstract b(Landroid/database/Cursor;)Ljava/lang/Object;
.end method
