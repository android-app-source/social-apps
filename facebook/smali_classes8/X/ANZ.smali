.class public final enum LX/ANZ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ANZ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ANZ;

.field public static final enum IMAGE:LX/ANZ;

.field public static final enum NONE:LX/ANZ;

.field public static final enum PHOTO:LX/ANZ;

.field public static final enum VIDEO:LX/ANZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1667751
    new-instance v0, LX/ANZ;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LX/ANZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ANZ;->NONE:LX/ANZ;

    .line 1667752
    new-instance v0, LX/ANZ;

    const-string v1, "IMAGE"

    invoke-direct {v0, v1, v3}, LX/ANZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ANZ;->IMAGE:LX/ANZ;

    .line 1667753
    new-instance v0, LX/ANZ;

    const-string v1, "PHOTO"

    invoke-direct {v0, v1, v4}, LX/ANZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ANZ;->PHOTO:LX/ANZ;

    .line 1667754
    new-instance v0, LX/ANZ;

    const-string v1, "VIDEO"

    invoke-direct {v0, v1, v5}, LX/ANZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ANZ;->VIDEO:LX/ANZ;

    .line 1667755
    const/4 v0, 0x4

    new-array v0, v0, [LX/ANZ;

    sget-object v1, LX/ANZ;->NONE:LX/ANZ;

    aput-object v1, v0, v2

    sget-object v1, LX/ANZ;->IMAGE:LX/ANZ;

    aput-object v1, v0, v3

    sget-object v1, LX/ANZ;->PHOTO:LX/ANZ;

    aput-object v1, v0, v4

    sget-object v1, LX/ANZ;->VIDEO:LX/ANZ;

    aput-object v1, v0, v5

    sput-object v0, LX/ANZ;->$VALUES:[LX/ANZ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1667756
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ANZ;
    .locals 1

    .prologue
    .line 1667757
    const-class v0, LX/ANZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ANZ;

    return-object v0
.end method

.method public static values()[LX/ANZ;
    .locals 1

    .prologue
    .line 1667758
    sget-object v0, LX/ANZ;->$VALUES:[LX/ANZ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ANZ;

    return-object v0
.end method
