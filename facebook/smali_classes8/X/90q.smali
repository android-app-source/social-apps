.class public final LX/90q;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/92P;

.field public final synthetic b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;LX/92P;)V
    .locals 0

    .prologue
    .line 1429491
    iput-object p1, p0, LX/90q;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    iput-object p2, p0, LX/90q;->a:LX/92P;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1429505
    iget-object v0, p0, LX/90q;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->k:LX/92C;

    invoke-virtual {v0}, LX/92C;->g()V

    .line 1429506
    iget-object v0, p0, LX/90q;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    invoke-static {v0, p1}, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->a$redex0(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;Ljava/lang/Throwable;)V

    .line 1429507
    iget-object v0, p0, LX/90q;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    invoke-static {v0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->l(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;)V

    .line 1429508
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1429492
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1429493
    iget-object v0, p0, LX/90q;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->q:LX/4nw;

    invoke-virtual {v0}, LX/4nw;->c()V

    .line 1429494
    iget-object v0, p0, LX/90q;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->k:LX/92C;

    iget-object v1, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    invoke-virtual {v0, v1}, LX/92C;->a(LX/0ta;)V

    .line 1429495
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1429496
    check-cast v0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;

    .line 1429497
    iget-object v1, p0, LX/90q;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    .line 1429498
    iput-object v0, v1, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->v:LX/5LG;

    .line 1429499
    iget-object v1, p0, LX/90q;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->K()Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;

    move-result-object v0

    iget-object v2, p0, LX/90q;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    iget-object v2, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->x:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    iget-object v4, p0, LX/90q;->a:LX/92P;

    iget-object v4, v4, LX/92P;->e:Ljava/lang/String;

    invoke-static {v1, v0, v2, v3, v4}, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->a$redex0(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;Ljava/lang/String;LX/0ta;Ljava/lang/String;)V

    .line 1429500
    iget-object v0, p0, LX/90q;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->y:Ljava/lang/String;

    iget-object v1, p0, LX/90q;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    iget-object v1, v1, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->x:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0YN;->c(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1429501
    iget-object v0, p0, LX/90q;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    iget-object v1, p0, LX/90q;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    iget-object v1, v1, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->y:Ljava/lang/String;

    .line 1429502
    invoke-static {v0, v1}, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->c$redex0(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;Ljava/lang/String;)V

    .line 1429503
    :cond_0
    iget-object v0, p0, LX/90q;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    invoke-static {v0}, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->l(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;)V

    .line 1429504
    return-void
.end method
