.class public final LX/ALH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ALG;


# instance fields
.field public final synthetic a:Lcom/facebook/backstage/camera/CameraView;


# direct methods
.method public constructor <init>(Lcom/facebook/backstage/camera/CameraView;)V
    .locals 0

    .prologue
    .line 1664721
    iput-object p1, p0, LX/ALH;->a:Lcom/facebook/backstage/camera/CameraView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1664722
    invoke-static {}, Lcom/facebook/optic/CameraPreviewView;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1664723
    iget-object v0, p0, LX/ALH;->a:Lcom/facebook/backstage/camera/CameraView;

    invoke-virtual {v0}, Lcom/facebook/backstage/camera/CameraView;->c()V

    .line 1664724
    iget-object v0, p0, LX/ALH;->a:Lcom/facebook/backstage/camera/CameraView;

    iget-object v0, v0, Lcom/facebook/backstage/camera/CameraView;->j:Lcom/facebook/optic/CameraPreviewView;

    new-instance v1, LX/ALD;

    invoke-direct {v1, p0}, LX/ALD;-><init>(LX/ALH;)V

    invoke-virtual {v0, v1}, Lcom/facebook/optic/CameraPreviewView;->a(LX/5f5;)V

    .line 1664725
    :cond_0
    return-void
.end method

.method public final a(F)V
    .locals 5

    .prologue
    const/high16 v4, 0x43b40000    # 360.0f

    const/high16 v3, 0x43340000    # 180.0f

    const/high16 v2, 0x3f800000    # 1.0f

    .line 1664726
    rem-float v0, p1, v4

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 1664727
    cmpl-float v1, v0, v3

    if-lez v1, :cond_0

    .line 1664728
    sub-float v0, v4, v0

    .line 1664729
    :cond_0
    div-float/2addr v0, v3

    sub-float v0, v2, v0

    .line 1664730
    iget-object v1, p0, LX/ALH;->a:Lcom/facebook/backstage/camera/CameraView;

    iget-object v1, v1, Lcom/facebook/backstage/camera/CameraView;->o:Landroid/widget/ImageView;

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setRotation(F)V

    .line 1664731
    iget-object v1, p0, LX/ALH;->a:Lcom/facebook/backstage/camera/CameraView;

    iget-object v1, v1, Lcom/facebook/backstage/camera/CameraView;->o:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 1664732
    iget-object v1, p0, LX/ALH;->a:Lcom/facebook/backstage/camera/CameraView;

    iget-object v1, v1, Lcom/facebook/backstage/camera/CameraView;->p:Landroid/widget/ImageView;

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setRotation(F)V

    .line 1664733
    iget-object v1, p0, LX/ALH;->a:Lcom/facebook/backstage/camera/CameraView;

    iget-object v1, v1, Lcom/facebook/backstage/camera/CameraView;->p:Landroid/widget/ImageView;

    sub-float v0, v2, v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 1664734
    return-void
.end method
