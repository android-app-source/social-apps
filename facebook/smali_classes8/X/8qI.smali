.class public final LX/8qI;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLComment;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLComment;

.field public final synthetic c:Lcom/facebook/ufiservices/flyout/EditCommentFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/ufiservices/flyout/EditCommentFragment;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 0

    .prologue
    .line 1406649
    iput-object p1, p0, LX/8qI;->c:Lcom/facebook/ufiservices/flyout/EditCommentFragment;

    iput-object p2, p0, LX/8qI;->a:Lcom/facebook/graphql/model/GraphQLComment;

    iput-object p3, p0, LX/8qI;->b:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 4

    .prologue
    .line 1406650
    iget-object v0, p0, LX/8qI;->c:Lcom/facebook/ufiservices/flyout/EditCommentFragment;

    iget-object v0, v0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->g:LX/1K9;

    new-instance v1, LX/8q4;

    iget-object v2, p0, LX/8qI;->a:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v3, p0, LX/8qI;->c:Lcom/facebook/ufiservices/flyout/EditCommentFragment;

    iget-object v3, v3, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->u:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/8q4;-><init>(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/1K9;->a(LX/1KJ;)V

    .line 1406651
    iget-object v0, p0, LX/8qI;->c:Lcom/facebook/ufiservices/flyout/EditCommentFragment;

    invoke-static {v0, p1}, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->a$redex0(Lcom/facebook/ufiservices/flyout/EditCommentFragment;Lcom/facebook/fbservice/service/ServiceException;)V

    .line 1406652
    iget-object v0, p0, LX/8qI;->c:Lcom/facebook/ufiservices/flyout/EditCommentFragment;

    iget-object v0, v0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->a:LX/0Zb;

    .line 1406653
    const-string v1, "comment_edit_failed"

    iget-object v2, p0, LX/8qI;->a:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/8qI;->c:Lcom/facebook/ufiservices/flyout/EditCommentFragment;

    iget-object v3, v3, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->t:Ljava/lang/String;

    invoke-static {v1, v2, v3}, LX/17V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1406654
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 10

    .prologue
    .line 1406655
    iget-object v0, p0, LX/8qI;->c:Lcom/facebook/ufiservices/flyout/EditCommentFragment;

    iget-object v0, v0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->u:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    .line 1406656
    iget-object v0, p0, LX/8qI;->c:Lcom/facebook/ufiservices/flyout/EditCommentFragment;

    iget-object v1, p0, LX/8qI;->c:Lcom/facebook/ufiservices/flyout/EditCommentFragment;

    iget-object v1, v1, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->i:LX/20j;

    iget-object v2, p0, LX/8qI;->c:Lcom/facebook/ufiservices/flyout/EditCommentFragment;

    iget-object v2, v2, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->u:Lcom/facebook/graphql/model/GraphQLFeedback;

    iget-object v3, p0, LX/8qI;->b:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v1, v2, v3}, LX/20j;->b(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 1406657
    iput-object v1, v0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->u:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1406658
    iget-object v0, p0, LX/8qI;->b:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1406659
    iget-object v0, p0, LX/8qI;->c:Lcom/facebook/ufiservices/flyout/EditCommentFragment;

    iget-object v1, p0, LX/8qI;->b:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v1

    .line 1406660
    iget-object v4, v0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->h:LX/3H7;

    iget-object v5, v0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->u:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    const/4 v8, 0x0

    iget-object v6, v0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->u:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v6}, LX/16z;->b(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v9

    move-object v6, v1

    invoke-virtual/range {v4 .. v9}, LX/3H7;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 1406661
    new-instance v5, LX/8qJ;

    invoke-direct {v5, v0}, LX/8qJ;-><init>(Lcom/facebook/ufiservices/flyout/EditCommentFragment;)V

    .line 1406662
    iget-object v6, v0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->l:LX/1Ck;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "fetch_comment_"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4}, LX/52F;->a(Ljava/lang/Object;)Ljava/util/concurrent/Callable;

    move-result-object v4

    invoke-virtual {v6, v7, v4, v5}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1406663
    :cond_0
    return-void
.end method
