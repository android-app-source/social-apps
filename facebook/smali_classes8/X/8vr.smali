.class public final LX/8vr;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static p:F

.field private static final q:[F

.field private static final r:[F


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:F

.field public f:F

.field public g:J

.field public h:I

.field private i:I

.field private j:I

.field public k:Z

.field public l:I

.field public m:F

.field public n:I

.field private o:F


# direct methods
.method public static constructor <clinit>()V
    .locals 16

    .prologue
    const v13, 0x3e333333    # 0.175f

    const/4 v4, 0x0

    const-wide v14, 0x3ee4f8b588e368f1L    # 1.0E-5

    const/16 v12, 0x64

    const/high16 v1, 0x3f800000    # 1.0f

    .line 1421347
    const-wide v2, 0x3fe8f5c28f5c28f6L    # 0.78

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    const-wide v6, 0x3feccccccccccccdL    # 0.9

    invoke-static {v6, v7}, Ljava/lang/Math;->log(D)D

    move-result-wide v6

    div-double/2addr v2, v6

    double-to-float v0, v2

    sput v0, LX/8vr;->p:F

    .line 1421348
    const/16 v0, 0x65

    new-array v0, v0, [F

    sput-object v0, LX/8vr;->q:[F

    .line 1421349
    const/16 v0, 0x65

    new-array v0, v0, [F

    sput-object v0, LX/8vr;->r:[F

    .line 1421350
    const/4 v0, 0x0

    move v5, v0

    move v2, v4

    :goto_0
    if-ge v5, v12, :cond_4

    .line 1421351
    int-to-float v0, v5

    const/high16 v3, 0x42c80000    # 100.0f

    div-float v6, v0, v3

    move v0, v1

    move v3, v2

    .line 1421352
    :goto_1
    sub-float v2, v0, v3

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v2, v7

    add-float/2addr v2, v3

    .line 1421353
    const/high16 v7, 0x40400000    # 3.0f

    mul-float/2addr v7, v2

    sub-float v8, v1, v2

    mul-float/2addr v7, v8

    .line 1421354
    sub-float v8, v1, v2

    mul-float/2addr v8, v13

    const v9, 0x3eb33334    # 0.35000002f

    mul-float/2addr v9, v2

    add-float/2addr v8, v9

    mul-float/2addr v8, v7

    mul-float v9, v2, v2

    mul-float/2addr v9, v2

    add-float/2addr v8, v9

    .line 1421355
    sub-float v9, v8, v6

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    float-to-double v10, v9

    cmpg-double v9, v10, v14

    if-ltz v9, :cond_1

    .line 1421356
    cmpl-float v7, v8, v6

    if-lez v7, :cond_0

    move v0, v2

    goto :goto_1

    :cond_0
    move v3, v2

    .line 1421357
    goto :goto_1

    .line 1421358
    :cond_1
    sget-object v0, LX/8vr;->q:[F

    sub-float v8, v1, v2

    const/high16 v9, 0x3f000000    # 0.5f

    mul-float/2addr v8, v9

    add-float/2addr v8, v2

    mul-float/2addr v7, v8

    mul-float v8, v2, v2

    mul-float/2addr v2, v8

    add-float/2addr v2, v7

    aput v2, v0, v5

    move v0, v1

    .line 1421359
    :goto_2
    sub-float v2, v0, v4

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v2, v7

    add-float/2addr v2, v4

    .line 1421360
    const/high16 v7, 0x40400000    # 3.0f

    mul-float/2addr v7, v2

    sub-float v8, v1, v2

    mul-float/2addr v7, v8

    .line 1421361
    sub-float v8, v1, v2

    const/high16 v9, 0x3f000000    # 0.5f

    mul-float/2addr v8, v9

    add-float/2addr v8, v2

    mul-float/2addr v8, v7

    mul-float v9, v2, v2

    mul-float/2addr v9, v2

    add-float/2addr v8, v9

    .line 1421362
    sub-float v9, v8, v6

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    float-to-double v10, v9

    cmpg-double v9, v10, v14

    if-ltz v9, :cond_3

    .line 1421363
    cmpl-float v7, v8, v6

    if-lez v7, :cond_2

    move v0, v2

    goto :goto_2

    :cond_2
    move v4, v2

    .line 1421364
    goto :goto_2

    .line 1421365
    :cond_3
    sget-object v0, LX/8vr;->r:[F

    sub-float v6, v1, v2

    mul-float/2addr v6, v13

    const v8, 0x3eb33334    # 0.35000002f

    mul-float/2addr v8, v2

    add-float/2addr v6, v8

    mul-float/2addr v6, v7

    mul-float v7, v2, v2

    mul-float/2addr v2, v7

    add-float/2addr v2, v6

    aput v2, v0, v5

    .line 1421366
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    move v2, v3

    goto/16 :goto_0

    .line 1421367
    :cond_4
    sget-object v0, LX/8vr;->q:[F

    sget-object v2, LX/8vr;->r:[F

    aput v1, v2, v12

    aput v1, v0, v12

    .line 1421368
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1421340
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1421341
    invoke-static {}, Landroid/view/ViewConfiguration;->getScrollFriction()F

    move-result v0

    iput v0, p0, LX/8vr;->m:F

    .line 1421342
    const/4 v0, 0x0

    iput v0, p0, LX/8vr;->n:I

    .line 1421343
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8vr;->k:Z

    .line 1421344
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x43200000    # 160.0f

    mul-float/2addr v0, v1

    .line 1421345
    const v1, 0x43c10b3d

    mul-float/2addr v0, v1

    const v1, 0x3f570a3d    # 0.84f

    mul-float/2addr v0, v1

    iput v0, p0, LX/8vr;->o:F

    .line 1421346
    return-void
.end method

.method private static a(I)F
    .locals 1

    .prologue
    .line 1421339
    if-lez p0, :cond_0

    const/high16 v0, -0x3b060000    # -2000.0f

    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x44fa0000    # 2000.0f

    goto :goto_0
.end method

.method private a(II)V
    .locals 6

    .prologue
    .line 1421329
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/8vr;->k:Z

    .line 1421330
    const/4 v0, 0x1

    iput v0, p0, LX/8vr;->n:I

    .line 1421331
    iput p1, p0, LX/8vr;->a:I

    .line 1421332
    iput p2, p0, LX/8vr;->c:I

    .line 1421333
    sub-int v0, p1, p2

    .line 1421334
    invoke-static {v0}, LX/8vr;->a(I)F

    move-result v1

    iput v1, p0, LX/8vr;->f:F

    .line 1421335
    neg-int v1, v0

    iput v1, p0, LX/8vr;->d:I

    .line 1421336
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v1

    iput v1, p0, LX/8vr;->l:I

    .line 1421337
    const-wide v2, 0x408f400000000000L    # 1000.0

    const-wide/high16 v4, -0x4000000000000000L    # -2.0

    int-to-double v0, v0

    mul-double/2addr v0, v4

    iget v4, p0, LX/8vr;->f:F

    float-to-double v4, v4

    div-double/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    iput v0, p0, LX/8vr;->h:I

    .line 1421338
    return-void
.end method

.method public static a(LX/8vr;IIII)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1421312
    if-le p1, p2, :cond_0

    if-ge p1, p3, :cond_0

    .line 1421313
    const-string v1, "OverScroller"

    const-string v2, "startAfterEdge called from a valid position"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1421314
    iput-boolean v0, p0, LX/8vr;->k:Z

    .line 1421315
    :goto_0
    return-void

    .line 1421316
    :cond_0
    if-le p1, p3, :cond_1

    move v4, v0

    .line 1421317
    :goto_1
    if-eqz v4, :cond_2

    move v2, p3

    .line 1421318
    :goto_2
    sub-int v3, p1, v2

    .line 1421319
    mul-int v5, v3, p4

    if-ltz v5, :cond_3

    .line 1421320
    :goto_3
    if-eqz v0, :cond_4

    .line 1421321
    invoke-direct {p0, p1, v2, p4}, LX/8vr;->f(III)V

    goto :goto_0

    :cond_1
    move v4, v1

    .line 1421322
    goto :goto_1

    :cond_2
    move v2, p2

    .line 1421323
    goto :goto_2

    :cond_3
    move v0, v1

    .line 1421324
    goto :goto_3

    .line 1421325
    :cond_4
    invoke-direct {p0, p4}, LX/8vr;->c(I)D

    move-result-wide v0

    .line 1421326
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    int-to-double v6, v3

    cmpl-double v0, v0, v6

    if-lez v0, :cond_7

    .line 1421327
    if-eqz v4, :cond_5

    move v3, p2

    :goto_4
    if-eqz v4, :cond_6

    move v4, p1

    :goto_5
    iget v5, p0, LX/8vr;->l:I

    move-object v0, p0

    move v1, p1

    move v2, p4

    invoke-virtual/range {v0 .. v5}, LX/8vr;->a(IIIII)V

    goto :goto_0

    :cond_5
    move v3, p1

    goto :goto_4

    :cond_6
    move v4, p3

    goto :goto_5

    .line 1421328
    :cond_7
    invoke-direct {p0, p1, v2}, LX/8vr;->a(II)V

    goto :goto_0
.end method

.method public static b(LX/8vr;I)D
    .locals 3

    .prologue
    .line 1421311
    const v0, 0x3eb33333    # 0.35f

    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iget v1, p0, LX/8vr;->m:F

    iget v2, p0, LX/8vr;->o:F

    mul-float/2addr v1, v2

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    return-wide v0
.end method

.method private c(I)D
    .locals 8

    .prologue
    .line 1421308
    invoke-static {p0, p1}, LX/8vr;->b(LX/8vr;I)D

    move-result-wide v0

    .line 1421309
    sget v2, LX/8vr;->p:F

    float-to-double v2, v2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v2, v4

    .line 1421310
    iget v4, p0, LX/8vr;->m:F

    iget v5, p0, LX/8vr;->o:F

    mul-float/2addr v4, v5

    float-to-double v4, v4

    sget v6, LX/8vr;->p:F

    float-to-double v6, v6

    div-double v2, v6, v2

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    mul-double/2addr v0, v4

    return-wide v0
.end method

.method private d(III)V
    .locals 6

    .prologue
    const/high16 v4, 0x42c80000    # 100.0f

    .line 1421296
    sub-int v0, p2, p1

    .line 1421297
    sub-int v1, p3, p1

    .line 1421298
    int-to-float v1, v1

    int-to-float v0, v0

    div-float v0, v1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 1421299
    mul-float v1, v4, v0

    float-to-int v1, v1

    .line 1421300
    const/16 v2, 0x64

    if-ge v1, v2, :cond_0

    .line 1421301
    int-to-float v2, v1

    div-float/2addr v2, v4

    .line 1421302
    add-int/lit8 v3, v1, 0x1

    int-to-float v3, v3

    div-float/2addr v3, v4

    .line 1421303
    sget-object v4, LX/8vr;->r:[F

    aget v4, v4, v1

    .line 1421304
    sget-object v5, LX/8vr;->r:[F

    add-int/lit8 v1, v1, 0x1

    aget v1, v5, v1

    .line 1421305
    sub-float/2addr v0, v2

    sub-float v2, v3, v2

    div-float/2addr v0, v2

    sub-float/2addr v1, v4

    mul-float/2addr v0, v1

    add-float/2addr v0, v4

    .line 1421306
    iget v1, p0, LX/8vr;->h:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, LX/8vr;->h:I

    .line 1421307
    :cond_0
    return-void
.end method

.method private static d(LX/8vr;)V
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 1421369
    iget v0, p0, LX/8vr;->d:I

    iget v1, p0, LX/8vr;->d:I

    mul-int/2addr v0, v1

    int-to-float v0, v0

    iget v1, p0, LX/8vr;->f:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    mul-float/2addr v1, v3

    div-float/2addr v0, v1

    .line 1421370
    iget v1, p0, LX/8vr;->d:I

    int-to-float v1, v1

    invoke-static {v1}, Ljava/lang/Math;->signum(F)F

    move-result v1

    .line 1421371
    iget v2, p0, LX/8vr;->l:I

    int-to-float v2, v2

    cmpl-float v2, v0, v2

    if-lez v2, :cond_0

    .line 1421372
    neg-float v0, v1

    iget v1, p0, LX/8vr;->d:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iget v1, p0, LX/8vr;->d:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iget v1, p0, LX/8vr;->l:I

    int-to-float v1, v1

    mul-float/2addr v1, v3

    div-float/2addr v0, v1

    iput v0, p0, LX/8vr;->f:F

    .line 1421373
    iget v0, p0, LX/8vr;->l:I

    int-to-float v0, v0

    .line 1421374
    :cond_0
    float-to-int v1, v0

    iput v1, p0, LX/8vr;->l:I

    .line 1421375
    const/4 v1, 0x2

    iput v1, p0, LX/8vr;->n:I

    .line 1421376
    iget v1, p0, LX/8vr;->a:I

    iget v2, p0, LX/8vr;->d:I

    if-lez v2, :cond_1

    :goto_0
    float-to-int v0, v0

    add-int/2addr v0, v1

    iput v0, p0, LX/8vr;->c:I

    .line 1421377
    const/high16 v0, 0x447a0000    # 1000.0f

    iget v1, p0, LX/8vr;->d:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iget v1, p0, LX/8vr;->f:F

    div-float/2addr v0, v1

    float-to-int v0, v0

    neg-int v0, v0

    iput v0, p0, LX/8vr;->h:I

    .line 1421378
    return-void

    .line 1421379
    :cond_1
    neg-float v0, v0

    goto :goto_0
.end method

.method private f(III)V
    .locals 7

    .prologue
    .line 1421285
    if-nez p3, :cond_0

    sub-int v0, p1, p2

    :goto_0
    invoke-static {v0}, LX/8vr;->a(I)F

    move-result v0

    iput v0, p0, LX/8vr;->f:F

    .line 1421286
    neg-int v1, p3

    int-to-float v1, v1

    iget v2, p0, LX/8vr;->f:F

    div-float/2addr v1, v2

    .line 1421287
    mul-int v2, p3, p3

    int-to-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    iget v3, p0, LX/8vr;->f:F

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    div-float/2addr v2, v3

    .line 1421288
    sub-int v3, p2, p1

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    int-to-float v3, v3

    .line 1421289
    const-wide/high16 v5, 0x4000000000000000L    # 2.0

    add-float/2addr v2, v3

    float-to-double v3, v2

    mul-double/2addr v3, v5

    iget v2, p0, LX/8vr;->f:F

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    float-to-double v5, v2

    div-double/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v3

    double-to-float v2, v3

    .line 1421290
    iget-wide v3, p0, LX/8vr;->g:J

    const/high16 v5, 0x447a0000    # 1000.0f

    sub-float v1, v2, v1

    mul-float/2addr v1, v5

    float-to-int v1, v1

    int-to-long v5, v1

    sub-long/2addr v3, v5

    iput-wide v3, p0, LX/8vr;->g:J

    .line 1421291
    iput p2, p0, LX/8vr;->a:I

    .line 1421292
    iget v1, p0, LX/8vr;->f:F

    neg-float v1, v1

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, LX/8vr;->d:I

    .line 1421293
    invoke-static {p0}, LX/8vr;->d(LX/8vr;)V

    .line 1421294
    return-void

    :cond_0
    move v0, p3

    .line 1421295
    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1421282
    iget v0, p0, LX/8vr;->c:I

    iput v0, p0, LX/8vr;->b:I

    .line 1421283
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8vr;->k:Z

    .line 1421284
    return-void
.end method

.method public final a(III)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1421274
    iput-boolean v2, p0, LX/8vr;->k:Z

    .line 1421275
    iput p1, p0, LX/8vr;->a:I

    .line 1421276
    add-int v0, p1, p2

    iput v0, p0, LX/8vr;->c:I

    .line 1421277
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LX/8vr;->g:J

    .line 1421278
    iput p3, p0, LX/8vr;->h:I

    .line 1421279
    const/4 v0, 0x0

    iput v0, p0, LX/8vr;->f:F

    .line 1421280
    iput v2, p0, LX/8vr;->d:I

    .line 1421281
    return-void
.end method

.method public final a(IIIII)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 1421249
    iput p5, p0, LX/8vr;->l:I

    .line 1421250
    iput-boolean v2, p0, LX/8vr;->k:Z

    .line 1421251
    iput p2, p0, LX/8vr;->d:I

    int-to-float v0, p2

    iput v0, p0, LX/8vr;->e:F

    .line 1421252
    iput v2, p0, LX/8vr;->i:I

    iput v2, p0, LX/8vr;->h:I

    .line 1421253
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LX/8vr;->g:J

    .line 1421254
    iput p1, p0, LX/8vr;->a:I

    iput p1, p0, LX/8vr;->b:I

    .line 1421255
    if-gt p1, p4, :cond_0

    if-ge p1, p3, :cond_2

    .line 1421256
    :cond_0
    invoke-static {p0, p1, p3, p4, p2}, LX/8vr;->a(LX/8vr;IIII)V

    .line 1421257
    :cond_1
    :goto_0
    return-void

    .line 1421258
    :cond_2
    iput v2, p0, LX/8vr;->n:I

    .line 1421259
    const-wide/16 v0, 0x0

    .line 1421260
    if-eqz p2, :cond_3

    .line 1421261
    invoke-static {p0, p2}, LX/8vr;->b(LX/8vr;I)D

    move-result-wide v4

    .line 1421262
    sget v6, LX/8vr;->p:F

    float-to-double v6, v6

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v6, v8

    .line 1421263
    const-wide v8, 0x408f400000000000L    # 1000.0

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->exp(D)D

    move-result-wide v4

    mul-double/2addr v4, v8

    double-to-int v4, v4

    move v0, v4

    .line 1421264
    iput v0, p0, LX/8vr;->i:I

    iput v0, p0, LX/8vr;->h:I

    .line 1421265
    invoke-direct {p0, p2}, LX/8vr;->c(I)D

    move-result-wide v0

    .line 1421266
    :cond_3
    int-to-float v2, p2

    invoke-static {v2}, Ljava/lang/Math;->signum(F)F

    move-result v2

    float-to-double v2, v2

    mul-double/2addr v0, v2

    double-to-int v0, v0

    iput v0, p0, LX/8vr;->j:I

    .line 1421267
    iget v0, p0, LX/8vr;->j:I

    add-int/2addr v0, p1

    iput v0, p0, LX/8vr;->c:I

    .line 1421268
    iget v0, p0, LX/8vr;->c:I

    if-ge v0, p3, :cond_4

    .line 1421269
    iget v0, p0, LX/8vr;->a:I

    iget v1, p0, LX/8vr;->c:I

    invoke-direct {p0, v0, v1, p3}, LX/8vr;->d(III)V

    .line 1421270
    iput p3, p0, LX/8vr;->c:I

    .line 1421271
    :cond_4
    iget v0, p0, LX/8vr;->c:I

    if-le v0, p4, :cond_1

    .line 1421272
    iget v0, p0, LX/8vr;->a:I

    iget v1, p0, LX/8vr;->c:I

    invoke-direct {p0, v0, v1, p4}, LX/8vr;->d(III)V

    .line 1421273
    iput p4, p0, LX/8vr;->c:I

    goto :goto_0
.end method

.method public final b(F)V
    .locals 3

    .prologue
    .line 1421247
    iget v0, p0, LX/8vr;->a:I

    iget v1, p0, LX/8vr;->c:I

    iget v2, p0, LX/8vr;->a:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    mul-float/2addr v1, p1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, LX/8vr;->b:I

    .line 1421248
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1421236
    iget v1, p0, LX/8vr;->n:I

    packed-switch v1, :pswitch_data_0

    .line 1421237
    :goto_0
    invoke-virtual {p0}, LX/8vr;->c()Z

    .line 1421238
    const/4 v0, 0x1

    :cond_0
    :pswitch_0
    return v0

    .line 1421239
    :pswitch_1
    iget v1, p0, LX/8vr;->h:I

    iget v2, p0, LX/8vr;->i:I

    if-ge v1, v2, :cond_0

    .line 1421240
    iget v0, p0, LX/8vr;->c:I

    iput v0, p0, LX/8vr;->a:I

    .line 1421241
    iget v0, p0, LX/8vr;->e:F

    float-to-int v0, v0

    iput v0, p0, LX/8vr;->d:I

    .line 1421242
    iget v0, p0, LX/8vr;->d:I

    invoke-static {v0}, LX/8vr;->a(I)F

    move-result v0

    iput v0, p0, LX/8vr;->f:F

    .line 1421243
    iget-wide v0, p0, LX/8vr;->g:J

    iget v2, p0, LX/8vr;->h:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/8vr;->g:J

    .line 1421244
    invoke-static {p0}, LX/8vr;->d(LX/8vr;)V

    goto :goto_0

    .line 1421245
    :pswitch_2
    iget-wide v0, p0, LX/8vr;->g:J

    iget v2, p0, LX/8vr;->h:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/8vr;->g:J

    .line 1421246
    iget v0, p0, LX/8vr;->c:I

    iget v1, p0, LX/8vr;->a:I

    invoke-direct {p0, v0, v1}, LX/8vr;->a(II)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final b(III)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1421225
    iput-boolean v0, p0, LX/8vr;->k:Z

    .line 1421226
    iput p1, p0, LX/8vr;->c:I

    iput p1, p0, LX/8vr;->a:I

    .line 1421227
    iput v1, p0, LX/8vr;->d:I

    .line 1421228
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, LX/8vr;->g:J

    .line 1421229
    iput v1, p0, LX/8vr;->h:I

    .line 1421230
    if-ge p1, p2, :cond_1

    .line 1421231
    invoke-direct {p0, p1, p2}, LX/8vr;->a(II)V

    .line 1421232
    :cond_0
    :goto_0
    iget-boolean v2, p0, LX/8vr;->k:Z

    if-nez v2, :cond_2

    :goto_1
    return v0

    .line 1421233
    :cond_1
    if-le p1, p3, :cond_0

    .line 1421234
    invoke-direct {p0, p1, p3}, LX/8vr;->a(II)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 1421235
    goto :goto_1
.end method

.method public final c()Z
    .locals 8

    .prologue
    const/high16 v7, 0x447a0000    # 1000.0f

    const/high16 v6, 0x40000000    # 2.0f

    const/high16 v5, 0x42c80000    # 100.0f

    .line 1421193
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    .line 1421194
    iget-wide v2, p0, LX/8vr;->g:J

    sub-long v2, v0, v2

    .line 1421195
    iget v0, p0, LX/8vr;->h:I

    int-to-long v0, v0

    cmp-long v0, v2, v0

    if-lez v0, :cond_0

    .line 1421196
    const/4 v0, 0x0

    .line 1421197
    :goto_0
    return v0

    .line 1421198
    :cond_0
    const-wide/16 v0, 0x0

    .line 1421199
    iget v4, p0, LX/8vr;->n:I

    packed-switch v4, :pswitch_data_0

    .line 1421200
    :goto_1
    iget v2, p0, LX/8vr;->a:I

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    add-int/2addr v0, v2

    iput v0, p0, LX/8vr;->b:I

    .line 1421201
    const/4 v0, 0x1

    goto :goto_0

    .line 1421202
    :pswitch_0
    long-to-float v0, v2

    iget v1, p0, LX/8vr;->i:I

    int-to-float v1, v1

    div-float v2, v0, v1

    .line 1421203
    mul-float v0, v5, v2

    float-to-int v3, v0

    .line 1421204
    const/high16 v1, 0x3f800000    # 1.0f

    .line 1421205
    const/4 v0, 0x0

    .line 1421206
    const/16 v4, 0x64

    if-ge v3, v4, :cond_1

    .line 1421207
    int-to-float v0, v3

    div-float v1, v0, v5

    .line 1421208
    add-int/lit8 v0, v3, 0x1

    int-to-float v0, v0

    div-float/2addr v0, v5

    .line 1421209
    sget-object v4, LX/8vr;->q:[F

    aget v4, v4, v3

    .line 1421210
    sget-object v5, LX/8vr;->q:[F

    add-int/lit8 v3, v3, 0x1

    aget v3, v5, v3

    .line 1421211
    sub-float/2addr v3, v4

    sub-float/2addr v0, v1

    div-float v0, v3, v0

    .line 1421212
    sub-float v1, v2, v1

    mul-float/2addr v1, v0

    add-float/2addr v1, v4

    .line 1421213
    :cond_1
    iget v2, p0, LX/8vr;->j:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-double v2, v1

    .line 1421214
    iget v1, p0, LX/8vr;->j:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iget v1, p0, LX/8vr;->i:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    mul-float/2addr v0, v7

    iput v0, p0, LX/8vr;->e:F

    move-wide v0, v2

    .line 1421215
    goto :goto_1

    .line 1421216
    :pswitch_1
    long-to-float v0, v2

    div-float/2addr v0, v7

    .line 1421217
    iget v1, p0, LX/8vr;->d:I

    int-to-float v1, v1

    iget v2, p0, LX/8vr;->f:F

    mul-float/2addr v2, v0

    add-float/2addr v1, v2

    iput v1, p0, LX/8vr;->e:F

    .line 1421218
    iget v1, p0, LX/8vr;->d:I

    int-to-float v1, v1

    mul-float/2addr v1, v0

    iget v2, p0, LX/8vr;->f:F

    mul-float/2addr v2, v0

    mul-float/2addr v0, v2

    div-float/2addr v0, v6

    add-float/2addr v0, v1

    float-to-double v0, v0

    .line 1421219
    goto :goto_1

    .line 1421220
    :pswitch_2
    long-to-float v0, v2

    iget v1, p0, LX/8vr;->h:I

    int-to-float v1, v1

    div-float v2, v0, v1

    .line 1421221
    mul-float v3, v2, v2

    .line 1421222
    iget v0, p0, LX/8vr;->d:I

    int-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v4

    .line 1421223
    iget v0, p0, LX/8vr;->l:I

    int-to-float v0, v0

    mul-float/2addr v0, v4

    const/high16 v1, 0x40400000    # 3.0f

    mul-float/2addr v1, v3

    mul-float v5, v6, v2

    mul-float/2addr v5, v3

    sub-float/2addr v1, v5

    mul-float/2addr v0, v1

    float-to-double v0, v0

    .line 1421224
    iget v5, p0, LX/8vr;->l:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    const/high16 v5, 0x40c00000    # 6.0f

    mul-float/2addr v4, v5

    neg-float v2, v2

    add-float/2addr v2, v3

    mul-float/2addr v2, v4

    iput v2, p0, LX/8vr;->e:F

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
