.class public final LX/92V;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;",
        ">;",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/92H;

.field public final synthetic c:LX/92X;


# direct methods
.method public constructor <init>(LX/92X;ZLX/92H;)V
    .locals 0

    .prologue
    .line 1432330
    iput-object p1, p0, LX/92V;->c:LX/92X;

    iput-boolean p2, p0, LX/92V;->a:Z

    iput-object p3, p0, LX/92V;->b:LX/92H;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1432331
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1432332
    iget-boolean v0, p0, LX/92V;->a:Z

    if-nez v0, :cond_0

    .line 1432333
    iget-object v0, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v0, v0

    .line 1432334
    sget-object v1, LX/0ta;->FROM_SERVER:LX/0ta;

    if-eq v0, v1, :cond_1

    .line 1432335
    :cond_0
    iget-object v0, p0, LX/92V;->c:LX/92X;

    iget-object v0, v0, LX/92X;->a:LX/92Z;

    iget-object v1, p0, LX/92V;->b:LX/92H;

    .line 1432336
    iget-object v2, v0, LX/92Z;->c:LX/0tX;

    sget-object p0, LX/0zS;->d:LX/0zS;

    invoke-static {v0, v1, p0}, LX/92Z;->a(LX/92Z;LX/92H;LX/0zS;)LX/0zO;

    move-result-object p0

    invoke-virtual {v2, p0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    .line 1432337
    :cond_1
    return-object p1
.end method
