.class public LX/9H1;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/3Wr;
.implements LX/3Ws;


# instance fields
.field public a:LX/9HC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final b:Landroid/widget/TextView;

.field public final c:Landroid/widget/TextView;

.field private final d:LX/9HB;

.field private final e:LX/9HK;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1460719
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/9H1;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1460720
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1460721
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/9H1;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1460722
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1460723
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1460724
    const-class v0, LX/9H1;

    invoke-static {v0, p0}, LX/9H1;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1460725
    const v0, 0x7f0302c5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1460726
    const v0, 0x7f0d09bc

    invoke-virtual {p0, v0}, LX/9H1;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/9H1;->b:Landroid/widget/TextView;

    .line 1460727
    const v0, 0x7f0d09bd

    invoke-virtual {p0, v0}, LX/9H1;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/9H1;->c:Landroid/widget/TextView;

    .line 1460728
    iget-object v0, p0, LX/9H1;->a:LX/9HC;

    invoke-virtual {v0, p0}, LX/9HC;->a(Landroid/view/View;)LX/9HB;

    move-result-object v0

    iput-object v0, p0, LX/9H1;->d:LX/9HB;

    .line 1460729
    iget-object v0, p0, LX/9H1;->d:LX/9HB;

    invoke-virtual {v0}, LX/9HB;->a()V

    .line 1460730
    new-instance v0, LX/9HK;

    invoke-direct {v0, p1}, LX/9HK;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/9H1;->e:LX/9HK;

    .line 1460731
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/9H1;

    const-class p0, LX/9HC;

    invoke-interface {v1, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/9HC;

    iput-object v1, p1, LX/9H1;->a:LX/9HC;

    return-void
.end method


# virtual methods
.method public final a(LX/9CP;)V
    .locals 1

    .prologue
    .line 1460715
    iget-object v0, p0, LX/9H1;->e:LX/9HK;

    invoke-virtual {v0, p1}, LX/9HK;->a(LX/9CP;)V

    .line 1460716
    return-void
.end method

.method public final a(Landroid/animation/ValueAnimator;)V
    .locals 1

    .prologue
    .line 1460717
    iget-object v0, p0, LX/9H1;->d:LX/9HB;

    invoke-virtual {v0, p1}, LX/9HB;->a(Landroid/animation/ValueAnimator;)V

    .line 1460718
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1460713
    iget-object v0, p0, LX/9H1;->e:LX/9HK;

    invoke-virtual {v0}, LX/9HK;->a()V

    .line 1460714
    return-void
.end method

.method public final dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1460712
    iget-object v0, p0, LX/9H1;->e:LX/9HK;

    invoke-virtual {v0, p1}, LX/9HK;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1460709
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->draw(Landroid/graphics/Canvas;)V

    .line 1460710
    iget-object v0, p0, LX/9H1;->e:LX/9HK;

    invoke-virtual {v0, p1}, LX/9HK;->a(Landroid/graphics/Canvas;)V

    .line 1460711
    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1460708
    iget-object v0, p0, LX/9H1;->d:LX/9HB;

    invoke-virtual {v0, p1}, LX/9HB;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1460704
    iput v0, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1460705
    iput v0, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1460706
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1460707
    return-void
.end method

.method public setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 1

    .prologue
    .line 1460700
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1460701
    iget-object v0, p0, LX/9H1;->d:LX/9HB;

    .line 1460702
    iput-object p1, v0, LX/9HB;->c:Landroid/view/View$OnTouchListener;

    .line 1460703
    return-void
.end method
