.class public final enum LX/9X4;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/9X2;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9X4;",
        ">;",
        "LX/9X2;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9X4;

.field public static final enum EVENT_ADMIN_ADD_EVENT:LX/9X4;

.field public static final enum EVENT_ADMIN_ADD_TO_FAVORITES:LX/9X4;

.field public static final enum EVENT_ADMIN_EDIT_PAGE:LX/9X4;

.field public static final enum EVENT_ADMIN_PROMOTE:LX/9X4;

.field public static final enum EVENT_ADMIN_REMOVE_FROM_FAVORTIES:LX/9X4;

.field public static final enum EVENT_ADMIN_SETTINGS:LX/9X4;

.field public static final enum EVENT_ADMIN_SHARE_PHOTO:LX/9X4;

.field public static final enum EVENT_ADMIN_SHARE_VIDEO:LX/9X4;

.field public static final enum EVENT_ADMIN_WRITE_POST:LX/9X4;


# instance fields
.field private mEventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1502274
    new-instance v0, LX/9X4;

    const-string v1, "EVENT_ADMIN_WRITE_POST"

    const-string v2, "admin_tapped_post"

    invoke-direct {v0, v1, v4, v2}, LX/9X4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X4;->EVENT_ADMIN_WRITE_POST:LX/9X4;

    .line 1502275
    new-instance v0, LX/9X4;

    const-string v1, "EVENT_ADMIN_SHARE_PHOTO"

    const-string v2, "admin_tapped_photo"

    invoke-direct {v0, v1, v5, v2}, LX/9X4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X4;->EVENT_ADMIN_SHARE_PHOTO:LX/9X4;

    .line 1502276
    new-instance v0, LX/9X4;

    const-string v1, "EVENT_ADMIN_EDIT_PAGE"

    const-string v2, "admin_tapped_edit_page"

    invoke-direct {v0, v1, v6, v2}, LX/9X4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X4;->EVENT_ADMIN_EDIT_PAGE:LX/9X4;

    .line 1502277
    new-instance v0, LX/9X4;

    const-string v1, "EVENT_ADMIN_SETTINGS"

    const-string v2, "admin_tapped_settings"

    invoke-direct {v0, v1, v7, v2}, LX/9X4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X4;->EVENT_ADMIN_SETTINGS:LX/9X4;

    .line 1502278
    new-instance v0, LX/9X4;

    const-string v1, "EVENT_ADMIN_PROMOTE"

    const-string v2, "admin_tapped_promote"

    invoke-direct {v0, v1, v8, v2}, LX/9X4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X4;->EVENT_ADMIN_PROMOTE:LX/9X4;

    .line 1502279
    new-instance v0, LX/9X4;

    const-string v1, "EVENT_ADMIN_ADD_TO_FAVORITES"

    const/4 v2, 0x5

    const-string v3, "admin_tapped_add_to_favorites"

    invoke-direct {v0, v1, v2, v3}, LX/9X4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X4;->EVENT_ADMIN_ADD_TO_FAVORITES:LX/9X4;

    .line 1502280
    new-instance v0, LX/9X4;

    const-string v1, "EVENT_ADMIN_REMOVE_FROM_FAVORTIES"

    const/4 v2, 0x6

    const-string v3, "admin_tapped_remove_from_favorites"

    invoke-direct {v0, v1, v2, v3}, LX/9X4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X4;->EVENT_ADMIN_REMOVE_FROM_FAVORTIES:LX/9X4;

    .line 1502281
    new-instance v0, LX/9X4;

    const-string v1, "EVENT_ADMIN_ADD_EVENT"

    const/4 v2, 0x7

    const-string v3, "admin_tapped_event"

    invoke-direct {v0, v1, v2, v3}, LX/9X4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X4;->EVENT_ADMIN_ADD_EVENT:LX/9X4;

    .line 1502282
    new-instance v0, LX/9X4;

    const-string v1, "EVENT_ADMIN_SHARE_VIDEO"

    const/16 v2, 0x8

    const-string v3, "admin_tapped_video"

    invoke-direct {v0, v1, v2, v3}, LX/9X4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X4;->EVENT_ADMIN_SHARE_VIDEO:LX/9X4;

    .line 1502283
    const/16 v0, 0x9

    new-array v0, v0, [LX/9X4;

    sget-object v1, LX/9X4;->EVENT_ADMIN_WRITE_POST:LX/9X4;

    aput-object v1, v0, v4

    sget-object v1, LX/9X4;->EVENT_ADMIN_SHARE_PHOTO:LX/9X4;

    aput-object v1, v0, v5

    sget-object v1, LX/9X4;->EVENT_ADMIN_EDIT_PAGE:LX/9X4;

    aput-object v1, v0, v6

    sget-object v1, LX/9X4;->EVENT_ADMIN_SETTINGS:LX/9X4;

    aput-object v1, v0, v7

    sget-object v1, LX/9X4;->EVENT_ADMIN_PROMOTE:LX/9X4;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/9X4;->EVENT_ADMIN_ADD_TO_FAVORITES:LX/9X4;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/9X4;->EVENT_ADMIN_REMOVE_FROM_FAVORTIES:LX/9X4;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/9X4;->EVENT_ADMIN_ADD_EVENT:LX/9X4;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/9X4;->EVENT_ADMIN_SHARE_VIDEO:LX/9X4;

    aput-object v2, v0, v1

    sput-object v0, LX/9X4;->$VALUES:[LX/9X4;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1502267
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1502268
    iput-object p3, p0, LX/9X4;->mEventName:Ljava/lang/String;

    .line 1502269
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9X4;
    .locals 1

    .prologue
    .line 1502273
    const-class v0, LX/9X4;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9X4;

    return-object v0
.end method

.method public static values()[LX/9X4;
    .locals 1

    .prologue
    .line 1502272
    sget-object v0, LX/9X4;->$VALUES:[LX/9X4;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9X4;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1502271
    iget-object v0, p0, LX/9X4;->mEventName:Ljava/lang/String;

    return-object v0
.end method

.method public final getType()LX/9XC;
    .locals 1

    .prologue
    .line 1502270
    sget-object v0, LX/9XC;->ADMIN_ACTION_BAR:LX/9XC;

    return-object v0
.end method
