.class public final enum LX/9XJ;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/9X2;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9XJ;",
        ">;",
        "LX/9X2;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9XJ;

.field public static final enum EVENT_SHOWN_UNIT:LX/9XJ;

.field public static final enum EVENT_VIEWED_FB_EVENT:LX/9XJ;

.field public static final enum EVENT_VIEWED_PAGE_POST:LX/9XJ;

.field public static final enum EVENT_VIEWED_PHOTO:LX/9XJ;

.field public static final enum EVENT_VIEWED_SETTINGS:LX/9XJ;

.field public static final enum EVENT_VIEW_RECENT_POST:LX/9XJ;

.field public static final enum PAGE_EVENT_VIEW_CONSUME_ACTION:LX/9XJ;

.field public static final enum PAGE_EVENT_VIEW_DEEPLINK_ACTION:LX/9XJ;

.field public static final enum PAGE_EVENT_VIEW_GET_NOTIFICATION_ERROR:LX/9XJ;

.field public static final enum PAGE_EVENT_VIEW_SAVE_ACTION:LX/9XJ;

.field public static final enum PAGE_EVENT_VIEW_SERVICES_CARD:LX/9XJ;

.field public static final enum PAGE_EVENT_VIEW_SERVICES_CARD_NUX:LX/9XJ;


# instance fields
.field private mEventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1502732
    new-instance v0, LX/9XJ;

    const-string v1, "EVENT_VIEW_RECENT_POST"

    const-string v2, "view_recent_post"

    invoke-direct {v0, v1, v4, v2}, LX/9XJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XJ;->EVENT_VIEW_RECENT_POST:LX/9XJ;

    .line 1502733
    new-instance v0, LX/9XJ;

    const-string v1, "EVENT_VIEWED_PHOTO"

    const-string v2, "viewed_photo"

    invoke-direct {v0, v1, v5, v2}, LX/9XJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XJ;->EVENT_VIEWED_PHOTO:LX/9XJ;

    .line 1502734
    new-instance v0, LX/9XJ;

    const-string v1, "EVENT_VIEWED_PAGE_POST"

    const-string v2, "viewed_page_post"

    invoke-direct {v0, v1, v6, v2}, LX/9XJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XJ;->EVENT_VIEWED_PAGE_POST:LX/9XJ;

    .line 1502735
    new-instance v0, LX/9XJ;

    const-string v1, "EVENT_VIEWED_FB_EVENT"

    const-string v2, "viewed_event"

    invoke-direct {v0, v1, v7, v2}, LX/9XJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XJ;->EVENT_VIEWED_FB_EVENT:LX/9XJ;

    .line 1502736
    new-instance v0, LX/9XJ;

    const-string v1, "EVENT_VIEWED_SETTINGS"

    const-string v2, "view_settings"

    invoke-direct {v0, v1, v8, v2}, LX/9XJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XJ;->EVENT_VIEWED_SETTINGS:LX/9XJ;

    .line 1502737
    new-instance v0, LX/9XJ;

    const-string v1, "EVENT_SHOWN_UNIT"

    const/4 v2, 0x5

    const-string v3, "shown_unit"

    invoke-direct {v0, v1, v2, v3}, LX/9XJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XJ;->EVENT_SHOWN_UNIT:LX/9XJ;

    .line 1502738
    new-instance v0, LX/9XJ;

    const-string v1, "PAGE_EVENT_VIEW_SAVE_ACTION"

    const/4 v2, 0x6

    const-string v3, "view_save_action"

    invoke-direct {v0, v1, v2, v3}, LX/9XJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XJ;->PAGE_EVENT_VIEW_SAVE_ACTION:LX/9XJ;

    .line 1502739
    new-instance v0, LX/9XJ;

    const-string v1, "PAGE_EVENT_VIEW_CONSUME_ACTION"

    const/4 v2, 0x7

    const-string v3, "view_consume_action"

    invoke-direct {v0, v1, v2, v3}, LX/9XJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XJ;->PAGE_EVENT_VIEW_CONSUME_ACTION:LX/9XJ;

    .line 1502740
    new-instance v0, LX/9XJ;

    const-string v1, "PAGE_EVENT_VIEW_DEEPLINK_ACTION"

    const/16 v2, 0x8

    const-string v3, "view_deeplink_action"

    invoke-direct {v0, v1, v2, v3}, LX/9XJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XJ;->PAGE_EVENT_VIEW_DEEPLINK_ACTION:LX/9XJ;

    .line 1502741
    new-instance v0, LX/9XJ;

    const-string v1, "PAGE_EVENT_VIEW_SERVICES_CARD_NUX"

    const/16 v2, 0x9

    const-string v3, "page_service_card_impression"

    invoke-direct {v0, v1, v2, v3}, LX/9XJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XJ;->PAGE_EVENT_VIEW_SERVICES_CARD_NUX:LX/9XJ;

    .line 1502742
    new-instance v0, LX/9XJ;

    const-string v1, "PAGE_EVENT_VIEW_SERVICES_CARD"

    const/16 v2, 0xa

    const-string v3, "page_service_card_client_impression"

    invoke-direct {v0, v1, v2, v3}, LX/9XJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XJ;->PAGE_EVENT_VIEW_SERVICES_CARD:LX/9XJ;

    .line 1502743
    new-instance v0, LX/9XJ;

    const-string v1, "PAGE_EVENT_VIEW_GET_NOTIFICATION_ERROR"

    const/16 v2, 0xb

    const-string v3, "page_get_notification_error"

    invoke-direct {v0, v1, v2, v3}, LX/9XJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XJ;->PAGE_EVENT_VIEW_GET_NOTIFICATION_ERROR:LX/9XJ;

    .line 1502744
    const/16 v0, 0xc

    new-array v0, v0, [LX/9XJ;

    sget-object v1, LX/9XJ;->EVENT_VIEW_RECENT_POST:LX/9XJ;

    aput-object v1, v0, v4

    sget-object v1, LX/9XJ;->EVENT_VIEWED_PHOTO:LX/9XJ;

    aput-object v1, v0, v5

    sget-object v1, LX/9XJ;->EVENT_VIEWED_PAGE_POST:LX/9XJ;

    aput-object v1, v0, v6

    sget-object v1, LX/9XJ;->EVENT_VIEWED_FB_EVENT:LX/9XJ;

    aput-object v1, v0, v7

    sget-object v1, LX/9XJ;->EVENT_VIEWED_SETTINGS:LX/9XJ;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/9XJ;->EVENT_SHOWN_UNIT:LX/9XJ;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/9XJ;->PAGE_EVENT_VIEW_SAVE_ACTION:LX/9XJ;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/9XJ;->PAGE_EVENT_VIEW_CONSUME_ACTION:LX/9XJ;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/9XJ;->PAGE_EVENT_VIEW_DEEPLINK_ACTION:LX/9XJ;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/9XJ;->PAGE_EVENT_VIEW_SERVICES_CARD_NUX:LX/9XJ;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/9XJ;->PAGE_EVENT_VIEW_SERVICES_CARD:LX/9XJ;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/9XJ;->PAGE_EVENT_VIEW_GET_NOTIFICATION_ERROR:LX/9XJ;

    aput-object v2, v0, v1

    sput-object v0, LX/9XJ;->$VALUES:[LX/9XJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1502729
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1502730
    iput-object p3, p0, LX/9XJ;->mEventName:Ljava/lang/String;

    .line 1502731
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9XJ;
    .locals 1

    .prologue
    .line 1502745
    const-class v0, LX/9XJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9XJ;

    return-object v0
.end method

.method public static values()[LX/9XJ;
    .locals 1

    .prologue
    .line 1502728
    sget-object v0, LX/9XJ;->$VALUES:[LX/9XJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9XJ;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1502726
    iget-object v0, p0, LX/9XJ;->mEventName:Ljava/lang/String;

    return-object v0
.end method

.method public final getType()LX/9XC;
    .locals 1

    .prologue
    .line 1502727
    sget-object v0, LX/9XC;->VIEW:LX/9XC;

    return-object v0
.end method
