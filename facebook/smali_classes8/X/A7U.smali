.class public final enum LX/A7U;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/A7U;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/A7U;

.field public static final enum CLOSED:LX/A7U;

.field public static final enum OPENED:LX/A7U;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1626361
    new-instance v0, LX/A7U;

    const-string v1, "OPENED"

    invoke-direct {v0, v1, v2}, LX/A7U;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/A7U;->OPENED:LX/A7U;

    .line 1626362
    new-instance v0, LX/A7U;

    const-string v1, "CLOSED"

    invoke-direct {v0, v1, v3}, LX/A7U;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/A7U;->CLOSED:LX/A7U;

    .line 1626363
    const/4 v0, 0x2

    new-array v0, v0, [LX/A7U;

    sget-object v1, LX/A7U;->OPENED:LX/A7U;

    aput-object v1, v0, v2

    sget-object v1, LX/A7U;->CLOSED:LX/A7U;

    aput-object v1, v0, v3

    sput-object v0, LX/A7U;->$VALUES:[LX/A7U;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1626364
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/A7U;
    .locals 1

    .prologue
    .line 1626365
    const-class v0, LX/A7U;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/A7U;

    return-object v0
.end method

.method public static values()[LX/A7U;
    .locals 1

    .prologue
    .line 1626366
    sget-object v0, LX/A7U;->$VALUES:[LX/A7U;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/A7U;

    return-object v0
.end method
