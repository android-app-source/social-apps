.class public LX/8ma;
.super Landroid/widget/ArrayAdapter;
.source ""

# interfaces
.implements LX/61x;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/facebook/stickers/model/StickerPack;",
        ">;",
        "LX/61x;"
    }
.end annotation


# instance fields
.field public a:LX/8mP;

.field public b:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;"
        }
    .end annotation
.end field

.field public c:Z

.field public d:Z

.field private e:Landroid/content/Context;

.field private final f:LX/4m4;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/4m4;)V
    .locals 3

    .prologue
    .line 1399317
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 1399318
    invoke-virtual {p0}, LX/8ma;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f010526

    const v2, 0x7f0e055d

    invoke-static {v0, v1, v2}, LX/0WH;->a(Landroid/content/Context;II)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/8ma;->e:Landroid/content/Context;

    .line 1399319
    iput-object p2, p0, LX/8ma;->f:LX/4m4;

    .line 1399320
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;Ljava/util/LinkedHashMap;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1399321
    iput-object p2, p0, LX/8ma;->b:Ljava/util/LinkedHashMap;

    .line 1399322
    iput-boolean p3, p0, LX/8ma;->c:Z

    .line 1399323
    iput-boolean v0, p0, LX/8ma;->d:Z

    .line 1399324
    invoke-virtual {p0, v0}, LX/8ma;->setNotifyOnChange(Z)V

    .line 1399325
    invoke-virtual {p0}, LX/8ma;->clear()V

    .line 1399326
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/StickerPack;

    .line 1399327
    iget-boolean v2, p0, LX/8ma;->c:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, LX/8ma;->b:Ljava/util/LinkedHashMap;

    .line 1399328
    iget-object v3, v0, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1399329
    invoke-virtual {v2, v3}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1399330
    :cond_1
    invoke-virtual {p0, v0}, LX/8ma;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 1399331
    :cond_2
    const v0, 0x30ff5dde

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1399332
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1399333
    const/4 v0, 0x0

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 1399334
    invoke-virtual {p0}, LX/8ma;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 1399335
    instance-of v0, p2, Lcom/facebook/stickers/store/StickerStoreListItemView;

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Lcom/facebook/stickers/store/StickerStoreListItemView;

    .line 1399336
    iget-boolean v1, v0, Lcom/facebook/stickers/store/StickerStoreListItemView;->w:Z

    move v0, v1

    .line 1399337
    if-eqz v0, :cond_0

    .line 1399338
    check-cast p2, Lcom/facebook/stickers/store/StickerStoreListItemView;

    move-object v0, p2

    .line 1399339
    :goto_0
    invoke-virtual {p0, p1}, LX/8ma;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/stickers/model/StickerPack;

    .line 1399340
    iget-object v2, p0, LX/8ma;->b:Ljava/util/LinkedHashMap;

    .line 1399341
    iget-object v4, v1, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v4, v4

    .line 1399342
    invoke-virtual {v2, v4}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    .line 1399343
    iget-boolean v4, p0, LX/8ma;->c:Z

    iget-object v5, p0, LX/8ma;->f:LX/4m4;

    .line 1399344
    iget-object p1, v1, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object p1, p1

    .line 1399345
    iput-object p1, v0, Lcom/facebook/stickers/store/StickerStoreListItemView;->r:Ljava/lang/String;

    .line 1399346
    iput-object v1, v0, Lcom/facebook/stickers/store/StickerStoreListItemView;->s:Lcom/facebook/stickers/model/StickerPack;

    .line 1399347
    iput-boolean v2, v0, Lcom/facebook/stickers/store/StickerStoreListItemView;->t:Z

    .line 1399348
    iput-object v3, v0, Lcom/facebook/stickers/store/StickerStoreListItemView;->u:Ljava/lang/String;

    .line 1399349
    iput-boolean v4, v0, Lcom/facebook/stickers/store/StickerStoreListItemView;->v:Z

    .line 1399350
    const/4 p1, 0x1

    iput-boolean p1, v0, Lcom/facebook/stickers/store/StickerStoreListItemView;->w:Z

    .line 1399351
    invoke-static {v5}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object p1

    iput-object p1, v0, Lcom/facebook/stickers/store/StickerStoreListItemView;->x:LX/0am;

    .line 1399352
    invoke-virtual {v0}, Lcom/facebook/stickers/store/StickerStoreListItemView;->a()V

    .line 1399353
    new-instance v4, LX/8mY;

    invoke-direct {v4, p0, v1, v2, v3}, LX/8mY;-><init>(LX/8ma;Lcom/facebook/stickers/model/StickerPack;ZLjava/lang/String;)V

    .line 1399354
    iget-object v2, v0, Lcom/facebook/stickers/store/StickerStoreListItemView;->h:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1399355
    new-instance v2, LX/8mZ;

    invoke-direct {v2, p0, v1, v0}, LX/8mZ;-><init>(LX/8ma;Lcom/facebook/stickers/model/StickerPack;Lcom/facebook/stickers/store/StickerStoreListItemView;)V

    .line 1399356
    iget-object v1, v0, Lcom/facebook/stickers/store/StickerStoreListItemView;->g:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1399357
    return-object v0

    .line 1399358
    :cond_0
    new-instance v0, Lcom/facebook/stickers/store/StickerStoreListItemView;

    iget-object v1, p0, LX/8ma;->e:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/stickers/store/StickerStoreListItemView;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method
