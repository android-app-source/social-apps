.class public final LX/A1A;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 56

    .prologue
    .line 1609571
    const/16 v49, 0x0

    .line 1609572
    const/16 v48, 0x0

    .line 1609573
    const/16 v47, 0x0

    .line 1609574
    const/16 v46, 0x0

    .line 1609575
    const/16 v45, 0x0

    .line 1609576
    const/16 v44, 0x0

    .line 1609577
    const/16 v43, 0x0

    .line 1609578
    const/16 v42, 0x0

    .line 1609579
    const/16 v41, 0x0

    .line 1609580
    const/16 v40, 0x0

    .line 1609581
    const/16 v39, 0x0

    .line 1609582
    const/16 v38, 0x0

    .line 1609583
    const-wide/16 v36, 0x0

    .line 1609584
    const/16 v35, 0x0

    .line 1609585
    const/16 v34, 0x0

    .line 1609586
    const/16 v33, 0x0

    .line 1609587
    const/16 v32, 0x0

    .line 1609588
    const/16 v31, 0x0

    .line 1609589
    const/16 v30, 0x0

    .line 1609590
    const/16 v29, 0x0

    .line 1609591
    const/16 v28, 0x0

    .line 1609592
    const/16 v27, 0x0

    .line 1609593
    const/16 v26, 0x0

    .line 1609594
    const/16 v25, 0x0

    .line 1609595
    const/16 v24, 0x0

    .line 1609596
    const/16 v23, 0x0

    .line 1609597
    const/16 v22, 0x0

    .line 1609598
    const/16 v21, 0x0

    .line 1609599
    const/16 v20, 0x0

    .line 1609600
    const-wide/16 v18, 0x0

    .line 1609601
    const/16 v17, 0x0

    .line 1609602
    const/16 v16, 0x0

    .line 1609603
    const/4 v15, 0x0

    .line 1609604
    const/4 v14, 0x0

    .line 1609605
    const/4 v13, 0x0

    .line 1609606
    const/4 v12, 0x0

    .line 1609607
    const/4 v11, 0x0

    .line 1609608
    const/4 v10, 0x0

    .line 1609609
    const/4 v9, 0x0

    .line 1609610
    const/4 v8, 0x0

    .line 1609611
    const/4 v7, 0x0

    .line 1609612
    const/4 v6, 0x0

    .line 1609613
    const/4 v5, 0x0

    .line 1609614
    const/4 v4, 0x0

    .line 1609615
    const/4 v3, 0x0

    .line 1609616
    const/4 v2, 0x0

    .line 1609617
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v50

    sget-object v51, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v50

    move-object/from16 v1, v51

    if-eq v0, v1, :cond_31

    .line 1609618
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1609619
    const/4 v2, 0x0

    .line 1609620
    :goto_0
    return v2

    .line 1609621
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v51, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v51

    if-eq v2, v0, :cond_27

    .line 1609622
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1609623
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1609624
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v51

    sget-object v52, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    if-eq v0, v1, :cond_0

    if-eqz v2, :cond_0

    .line 1609625
    const-string v51, "__type__"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-nez v51, :cond_1

    const-string v51, "__typename"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_2

    .line 1609626
    :cond_1
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v2

    move/from16 v50, v2

    goto :goto_1

    .line 1609627
    :cond_2
    const-string v51, "accessibility_caption"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_3

    .line 1609628
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v49, v2

    goto :goto_1

    .line 1609629
    :cond_3
    const-string v51, "app_center_categories"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_4

    .line 1609630
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v48, v2

    goto :goto_1

    .line 1609631
    :cond_4
    const-string v51, "bio_text"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_5

    .line 1609632
    invoke-static/range {p0 .. p1}, LX/8hY;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v47, v2

    goto :goto_1

    .line 1609633
    :cond_5
    const-string v51, "can_viewer_follow"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_6

    .line 1609634
    const/4 v2, 0x1

    .line 1609635
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v13

    move/from16 v46, v13

    move v13, v2

    goto/16 :goto_1

    .line 1609636
    :cond_6
    const-string v51, "can_viewer_join"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_7

    .line 1609637
    const/4 v2, 0x1

    .line 1609638
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    move/from16 v45, v12

    move v12, v2

    goto/16 :goto_1

    .line 1609639
    :cond_7
    const-string v51, "can_viewer_like"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_8

    .line 1609640
    const/4 v2, 0x1

    .line 1609641
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move/from16 v44, v7

    move v7, v2

    goto/16 :goto_1

    .line 1609642
    :cond_8
    const-string v51, "category_names"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_9

    .line 1609643
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v43, v2

    goto/16 :goto_1

    .line 1609644
    :cond_9
    const-string v51, "community_category"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_a

    .line 1609645
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v42, v2

    goto/16 :goto_1

    .line 1609646
    :cond_a
    const-string v51, "connection_style"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_b

    .line 1609647
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v41, v2

    goto/16 :goto_1

    .line 1609648
    :cond_b
    const-string v51, "cover_photo"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_c

    .line 1609649
    invoke-static/range {p0 .. p1}, LX/8g0;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v40, v2

    goto/16 :goto_1

    .line 1609650
    :cond_c
    const-string v51, "does_viewer_like"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_d

    .line 1609651
    const/4 v2, 0x1

    .line 1609652
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move/from16 v39, v6

    move v6, v2

    goto/16 :goto_1

    .line 1609653
    :cond_d
    const-string v51, "end_timestamp"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_e

    .line 1609654
    const/4 v2, 0x1

    .line 1609655
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 1609656
    :cond_e
    const-string v51, "eventSocialContext"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_f

    .line 1609657
    invoke-static/range {p0 .. p1}, LX/8gc;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v38, v2

    goto/16 :goto_1

    .line 1609658
    :cond_f
    const-string v51, "event_place"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_10

    .line 1609659
    invoke-static/range {p0 .. p1}, LX/8gb;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v37, v2

    goto/16 :goto_1

    .line 1609660
    :cond_10
    const-string v51, "friendship_status"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_11

    .line 1609661
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v36, v2

    goto/16 :goto_1

    .line 1609662
    :cond_11
    const-string v51, "group_members"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_12

    .line 1609663
    invoke-static/range {p0 .. p1}, LX/8gp;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v35, v2

    goto/16 :goto_1

    .line 1609664
    :cond_12
    const-string v51, "id"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_13

    .line 1609665
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v34, v2

    goto/16 :goto_1

    .line 1609666
    :cond_13
    const-string v51, "image"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_14

    .line 1609667
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v33, v2

    goto/16 :goto_1

    .line 1609668
    :cond_14
    const-string v51, "imageHigh"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_15

    .line 1609669
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v32, v2

    goto/16 :goto_1

    .line 1609670
    :cond_15
    const-string v51, "is_all_day"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_16

    .line 1609671
    const/4 v2, 0x1

    .line 1609672
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    move/from16 v31, v11

    move v11, v2

    goto/16 :goto_1

    .line 1609673
    :cond_16
    const-string v51, "is_verified"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_17

    .line 1609674
    const/4 v2, 0x1

    .line 1609675
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    move/from16 v30, v10

    move v10, v2

    goto/16 :goto_1

    .line 1609676
    :cond_17
    const-string v51, "mutual_friends"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_18

    .line 1609677
    invoke-static/range {p0 .. p1}, LX/8hZ;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v29, v2

    goto/16 :goto_1

    .line 1609678
    :cond_18
    const-string v51, "name"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_19

    .line 1609679
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v28, v2

    goto/16 :goto_1

    .line 1609680
    :cond_19
    const-string v51, "page_call_to_action"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_1a

    .line 1609681
    invoke-static/range {p0 .. p1}, LX/8gz;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v27, v2

    goto/16 :goto_1

    .line 1609682
    :cond_1a
    const-string v51, "page_likers"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_1b

    .line 1609683
    invoke-static/range {p0 .. p1}, LX/8h5;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v26, v2

    goto/16 :goto_1

    .line 1609684
    :cond_1b
    const-string v51, "photo_owner"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_1c

    .line 1609685
    invoke-static/range {p0 .. p1}, LX/8hD;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v25, v2

    goto/16 :goto_1

    .line 1609686
    :cond_1c
    const-string v51, "profile_picture"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_1d

    .line 1609687
    invoke-static/range {p0 .. p1}, LX/3lU;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v24, v2

    goto/16 :goto_1

    .line 1609688
    :cond_1d
    const-string v51, "social_context"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_1e

    .line 1609689
    invoke-static/range {p0 .. p1}, LX/8gq;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v21, v2

    goto/16 :goto_1

    .line 1609690
    :cond_1e
    const-string v51, "start_timestamp"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_1f

    .line 1609691
    const/4 v2, 0x1

    .line 1609692
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v22

    move v9, v2

    goto/16 :goto_1

    .line 1609693
    :cond_1f
    const-string v51, "time_range_sentence"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_20

    .line 1609694
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 1609695
    :cond_20
    const-string v51, "verification_status"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_21

    .line 1609696
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 1609697
    :cond_21
    const-string v51, "video_channel_is_viewer_following"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_22

    .line 1609698
    const/4 v2, 0x1

    .line 1609699
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v8

    move/from16 v18, v8

    move v8, v2

    goto/16 :goto_1

    .line 1609700
    :cond_22
    const-string v51, "viewer_guest_status"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_23

    .line 1609701
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 1609702
    :cond_23
    const-string v51, "viewer_join_state"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_24

    .line 1609703
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v16, v2

    goto/16 :goto_1

    .line 1609704
    :cond_24
    const-string v51, "viewer_watch_status"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v51

    if-eqz v51, :cond_25

    .line 1609705
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 1609706
    :cond_25
    const-string v51, "visibility_sentence"

    move-object/from16 v0, v51

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_26

    .line 1609707
    invoke-static/range {p0 .. p1}, LX/8gr;->a(LX/15w;LX/186;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 1609708
    :cond_26
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1609709
    :cond_27
    const/16 v2, 0x25

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1609710
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v50

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1609711
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v49

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1609712
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v48

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1609713
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1609714
    if-eqz v13, :cond_28

    .line 1609715
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1609716
    :cond_28
    if-eqz v12, :cond_29

    .line 1609717
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1609718
    :cond_29
    if-eqz v7, :cond_2a

    .line 1609719
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1609720
    :cond_2a
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1609721
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1609722
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1609723
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1609724
    if-eqz v6, :cond_2b

    .line 1609725
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1609726
    :cond_2b
    if-eqz v3, :cond_2c

    .line 1609727
    const/16 v3, 0xc

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1609728
    :cond_2c
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1609729
    const/16 v2, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1609730
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1609731
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1609732
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1609733
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1609734
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1609735
    if-eqz v11, :cond_2d

    .line 1609736
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1609737
    :cond_2d
    if-eqz v10, :cond_2e

    .line 1609738
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1609739
    :cond_2e
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1609740
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1609741
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1609742
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1609743
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1609744
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1609745
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1609746
    if-eqz v9, :cond_2f

    .line 1609747
    const/16 v3, 0x1d

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v22

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1609748
    :cond_2f
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1609749
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1609750
    if-eqz v8, :cond_30

    .line 1609751
    const/16 v2, 0x20

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1609752
    :cond_30
    const/16 v2, 0x21

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1609753
    const/16 v2, 0x22

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1609754
    const/16 v2, 0x23

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1609755
    const/16 v2, 0x24

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1609756
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_31
    move/from16 v50, v49

    move/from16 v49, v48

    move/from16 v48, v47

    move/from16 v47, v46

    move/from16 v46, v45

    move/from16 v45, v44

    move/from16 v44, v43

    move/from16 v43, v42

    move/from16 v42, v41

    move/from16 v41, v40

    move/from16 v40, v39

    move/from16 v39, v38

    move/from16 v38, v35

    move/from16 v35, v32

    move/from16 v32, v29

    move/from16 v29, v26

    move/from16 v26, v23

    move/from16 v53, v20

    move/from16 v20, v17

    move/from16 v17, v14

    move v14, v11

    move v11, v5

    move/from16 v54, v24

    move/from16 v24, v21

    move/from16 v21, v53

    move/from16 v55, v25

    move/from16 v25, v22

    move-wide/from16 v22, v18

    move/from16 v19, v16

    move/from16 v18, v15

    move/from16 v16, v13

    move v15, v12

    move v13, v10

    move v12, v9

    move v10, v4

    move v9, v3

    move-wide/from16 v4, v36

    move v3, v6

    move/from16 v36, v33

    move v6, v7

    move/from16 v37, v34

    move/from16 v34, v31

    move v7, v8

    move/from16 v33, v30

    move/from16 v30, v27

    move/from16 v31, v28

    move v8, v2

    move/from16 v27, v54

    move/from16 v28, v55

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v3, 0x7

    const/4 v2, 0x2

    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 1609757
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1609758
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1609759
    if-eqz v0, :cond_0

    .line 1609760
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609761
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1609762
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1609763
    if-eqz v0, :cond_1

    .line 1609764
    const-string v1, "accessibility_caption"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609765
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1609766
    :cond_1
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1609767
    if-eqz v0, :cond_2

    .line 1609768
    const-string v0, "app_center_categories"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609769
    invoke-virtual {p0, p1, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1609770
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1609771
    if-eqz v0, :cond_3

    .line 1609772
    const-string v1, "bio_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609773
    invoke-static {p0, v0, p2}, LX/8hY;->a(LX/15i;ILX/0nX;)V

    .line 1609774
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1609775
    if-eqz v0, :cond_4

    .line 1609776
    const-string v1, "can_viewer_follow"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609777
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1609778
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1609779
    if-eqz v0, :cond_5

    .line 1609780
    const-string v1, "can_viewer_join"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609781
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1609782
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1609783
    if-eqz v0, :cond_6

    .line 1609784
    const-string v1, "can_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609785
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1609786
    :cond_6
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1609787
    if-eqz v0, :cond_7

    .line 1609788
    const-string v0, "category_names"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609789
    invoke-virtual {p0, p1, v3}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1609790
    :cond_7
    invoke-virtual {p0, p1, v6}, LX/15i;->g(II)I

    move-result v0

    .line 1609791
    if-eqz v0, :cond_8

    .line 1609792
    const-string v0, "community_category"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609793
    invoke-virtual {p0, p1, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1609794
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1609795
    if-eqz v0, :cond_9

    .line 1609796
    const-string v0, "connection_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609797
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1609798
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1609799
    if-eqz v0, :cond_a

    .line 1609800
    const-string v1, "cover_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609801
    invoke-static {p0, v0, p2, p3}, LX/8g0;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1609802
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1609803
    if-eqz v0, :cond_b

    .line 1609804
    const-string v1, "does_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609805
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1609806
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1609807
    cmp-long v2, v0, v4

    if-eqz v2, :cond_c

    .line 1609808
    const-string v2, "end_timestamp"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609809
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1609810
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1609811
    if-eqz v0, :cond_d

    .line 1609812
    const-string v1, "eventSocialContext"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609813
    invoke-static {p0, v0, p2}, LX/8gc;->a(LX/15i;ILX/0nX;)V

    .line 1609814
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1609815
    if-eqz v0, :cond_e

    .line 1609816
    const-string v1, "event_place"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609817
    invoke-static {p0, v0, p2, p3}, LX/8gb;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1609818
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1609819
    if-eqz v0, :cond_f

    .line 1609820
    const-string v0, "friendship_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609821
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1609822
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1609823
    if-eqz v0, :cond_10

    .line 1609824
    const-string v1, "group_members"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609825
    invoke-static {p0, v0, p2, p3}, LX/8gp;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1609826
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1609827
    if-eqz v0, :cond_11

    .line 1609828
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609829
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1609830
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1609831
    if-eqz v0, :cond_12

    .line 1609832
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609833
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1609834
    :cond_12
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1609835
    if-eqz v0, :cond_13

    .line 1609836
    const-string v1, "imageHigh"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609837
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1609838
    :cond_13
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1609839
    if-eqz v0, :cond_14

    .line 1609840
    const-string v1, "is_all_day"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609841
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1609842
    :cond_14
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1609843
    if-eqz v0, :cond_15

    .line 1609844
    const-string v1, "is_verified"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609845
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1609846
    :cond_15
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1609847
    if-eqz v0, :cond_16

    .line 1609848
    const-string v1, "mutual_friends"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609849
    invoke-static {p0, v0, p2}, LX/8hZ;->a(LX/15i;ILX/0nX;)V

    .line 1609850
    :cond_16
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1609851
    if-eqz v0, :cond_17

    .line 1609852
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609853
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1609854
    :cond_17
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1609855
    if-eqz v0, :cond_18

    .line 1609856
    const-string v1, "page_call_to_action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609857
    invoke-static {p0, v0, p2, p3}, LX/8gz;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1609858
    :cond_18
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1609859
    if-eqz v0, :cond_19

    .line 1609860
    const-string v1, "page_likers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609861
    invoke-static {p0, v0, p2}, LX/8h5;->a(LX/15i;ILX/0nX;)V

    .line 1609862
    :cond_19
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1609863
    if-eqz v0, :cond_1a

    .line 1609864
    const-string v1, "photo_owner"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609865
    invoke-static {p0, v0, p2}, LX/8hD;->a(LX/15i;ILX/0nX;)V

    .line 1609866
    :cond_1a
    const/16 v0, 0x1b

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1609867
    if-eqz v0, :cond_1b

    .line 1609868
    const-string v1, "profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609869
    invoke-static {p0, v0, p2}, LX/3lU;->a(LX/15i;ILX/0nX;)V

    .line 1609870
    :cond_1b
    const/16 v0, 0x1c

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1609871
    if-eqz v0, :cond_1c

    .line 1609872
    const-string v1, "social_context"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609873
    invoke-static {p0, v0, p2}, LX/8gq;->a(LX/15i;ILX/0nX;)V

    .line 1609874
    :cond_1c
    const/16 v0, 0x1d

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1609875
    cmp-long v2, v0, v4

    if-eqz v2, :cond_1d

    .line 1609876
    const-string v2, "start_timestamp"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609877
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1609878
    :cond_1d
    const/16 v0, 0x1e

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1609879
    if-eqz v0, :cond_1e

    .line 1609880
    const-string v1, "time_range_sentence"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609881
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1609882
    :cond_1e
    const/16 v0, 0x1f

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1609883
    if-eqz v0, :cond_1f

    .line 1609884
    const-string v0, "verification_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609885
    const/16 v0, 0x1f

    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1609886
    :cond_1f
    const/16 v0, 0x20

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1609887
    if-eqz v0, :cond_20

    .line 1609888
    const-string v1, "video_channel_is_viewer_following"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609889
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1609890
    :cond_20
    const/16 v0, 0x21

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1609891
    if-eqz v0, :cond_21

    .line 1609892
    const-string v0, "viewer_guest_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609893
    const/16 v0, 0x21

    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1609894
    :cond_21
    const/16 v0, 0x22

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1609895
    if-eqz v0, :cond_22

    .line 1609896
    const-string v0, "viewer_join_state"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609897
    const/16 v0, 0x22

    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1609898
    :cond_22
    const/16 v0, 0x23

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1609899
    if-eqz v0, :cond_23

    .line 1609900
    const-string v0, "viewer_watch_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609901
    const/16 v0, 0x23

    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1609902
    :cond_23
    const/16 v0, 0x24

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1609903
    if-eqz v0, :cond_24

    .line 1609904
    const-string v1, "visibility_sentence"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1609905
    invoke-static {p0, v0, p2}, LX/8gr;->a(LX/15i;ILX/0nX;)V

    .line 1609906
    :cond_24
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1609907
    return-void
.end method
