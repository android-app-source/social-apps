.class public LX/A7m;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1626731
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1626732
    iput-object p1, p0, LX/A7m;->a:Landroid/content/Context;

    .line 1626733
    return-void
.end method

.method public static a(Landroid/view/View;[F)Landroid/animation/ObjectAnimator;
    .locals 2

    .prologue
    .line 1626734
    new-instance v0, Landroid/animation/ObjectAnimator;

    invoke-direct {v0}, Landroid/animation/ObjectAnimator;-><init>()V

    .line 1626735
    invoke-virtual {v0, p0}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 1626736
    const-string v1, "translationY"

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setPropertyName(Ljava/lang/String;)V

    .line 1626737
    invoke-virtual {v0, p1}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    .line 1626738
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setRepeatCount(I)V

    .line 1626739
    return-object v0
.end method

.method public static a([FI)[F
    .locals 4

    .prologue
    .line 1626740
    array-length v0, p0

    new-array v1, v0, [F

    .line 1626741
    const/4 v0, 0x0

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    .line 1626742
    aget v2, p0, v0

    int-to-float v3, p1

    mul-float/2addr v2, v3

    aput v2, v1, v0

    .line 1626743
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1626744
    :cond_0
    return-object v1
.end method
