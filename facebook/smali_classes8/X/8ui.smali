.class public LX/8ui;
.super Landroid/graphics/drawable/Drawable;
.source ""

# interfaces
.implements Landroid/graphics/drawable/Drawable$Callback;
.implements LX/1en;


# instance fields
.field public final a:Landroid/graphics/Paint;

.field private final b:Landroid/graphics/Rect;

.field public final c:Landroid/graphics/RectF;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:I

.field public g:Z

.field public h:I

.field public i:F

.field public j:Z

.field public k:LX/8uh;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1415561
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 1415562
    sget-object v0, LX/8uh;->ONE_LETTER:LX/8uh;

    iput-object v0, p0, LX/8ui;->k:LX/8uh;

    .line 1415563
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/8ui;->a:Landroid/graphics/Paint;

    .line 1415564
    iget-object v0, p0, LX/8ui;->a:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFlags(I)V

    .line 1415565
    iget-object v0, p0, LX/8ui;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 1415566
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/8ui;->b:Landroid/graphics/Rect;

    .line 1415567
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/8ui;->c:Landroid/graphics/RectF;

    .line 1415568
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    .line 1415569
    invoke-direct {p0}, LX/8ui;-><init>()V

    .line 1415570
    sget-object v0, LX/03r;->UserInitialsDrawable:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1415571
    invoke-static {}, LX/8uh;->values()[LX/8uh;

    move-result-object v1

    const/16 v2, 0x2

    sget-object v3, LX/8uh;->ONE_LETTER:LX/8uh;

    invoke-virtual {v3}, LX/8uh;->ordinal()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, p0, LX/8ui;->k:LX/8uh;

    .line 1415572
    const/16 v1, 0x1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a003d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    .line 1415573
    const/16 v2, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0052

    invoke-static {v3, v4}, LX/0tP;->a(Landroid/content/res/Resources;I)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    int-to-float v2, v2

    .line 1415574
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1415575
    invoke-virtual {p0, v1}, LX/8ui;->b(I)V

    .line 1415576
    invoke-virtual {p0, v2}, LX/8ui;->a(F)V

    .line 1415577
    return-void
.end method

.method private static a(Ljava/lang/String;LX/8uh;)Ljava/lang/String;
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1415578
    invoke-static {p0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1415579
    :cond_0
    :goto_0
    return-object v0

    .line 1415580
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 1415581
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1415582
    sget-object v2, Landroid/util/Patterns;->PHONE:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1415583
    const/16 v0, 0x20

    invoke-static {v1, v0}, LX/0YN;->a(Ljava/lang/String;C)Ljava/util/List;

    move-result-object v1

    .line 1415584
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/8ui;->d(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->appendCodePoint(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1415585
    sget-object v0, LX/8uh;->TWO_LETTER:LX/8uh;

    if-ne p1, v0, :cond_2

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v3, 0x1

    if-le v0, v3, :cond_2

    .line 1415586
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/8ui;->d(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->appendCodePoint(I)Ljava/lang/StringBuilder;

    .line 1415587
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static c(LX/8ui;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1415588
    iget-object v0, p0, LX/8ui;->d:Ljava/lang/String;

    invoke-static {p1, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1415589
    iput-object p1, p0, LX/8ui;->d:Ljava/lang/String;

    .line 1415590
    invoke-virtual {p0}, LX/8ui;->invalidateSelf()V

    .line 1415591
    :cond_0
    return-void
.end method

.method private static d(Ljava/lang/String;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1415550
    move v0, v1

    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 1415551
    invoke-virtual {p0, v0}, Ljava/lang/String;->codePointAt(I)I

    move-result v2

    .line 1415552
    invoke-static {v2}, Ljava/lang/Character;->isLetterOrDigit(I)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v2

    .line 1415553
    :goto_1
    return v0

    .line 1415554
    :cond_0
    invoke-static {v2}, Ljava/lang/Character;->charCount(I)I

    move-result v2

    add-int/2addr v0, v2

    goto :goto_0

    .line 1415555
    :cond_1
    invoke-virtual {p0, v1}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1415592
    iput-object v1, p0, LX/8ui;->e:Ljava/lang/String;

    .line 1415593
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/8ui;->g:Z

    .line 1415594
    invoke-static {p0, v1}, LX/8ui;->c(LX/8ui;Ljava/lang/String;)V

    .line 1415595
    return-void
.end method

.method public final a(F)V
    .locals 1

    .prologue
    .line 1415596
    iget-object v0, p0, LX/8ui;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1415597
    return-void
.end method

.method public final a(Landroid/content/Context;I)V
    .locals 2
    .param p2    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param

    .prologue
    .line 1415598
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    .line 1415599
    iget-object v0, p0, LX/8ui;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, p2}, Landroid/content/Context;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1415600
    :goto_0
    return-void

    .line 1415601
    :cond_0
    iget-object v0, p0, LX/8ui;->a:Landroid/graphics/Paint;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0
.end method

.method public final a(Landroid/graphics/Typeface;)V
    .locals 1

    .prologue
    .line 1415602
    iget-object v0, p0, LX/8ui;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 1415603
    return-void
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1415556
    iget-object v0, p0, LX/8ui;->e:Ljava/lang/String;

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1415557
    iput-object p1, p0, LX/8ui;->e:Ljava/lang/String;

    .line 1415558
    iget-object v0, p0, LX/8ui;->k:LX/8uh;

    invoke-static {p1, v0}, LX/8ui;->a(Ljava/lang/String;LX/8uh;)Ljava/lang/String;

    move-result-object v0

    .line 1415559
    invoke-static {v0}, LX/1fg;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/8ui;->c(LX/8ui;Ljava/lang/String;)V

    .line 1415560
    :cond_0
    iget-object v0, p0, LX/8ui;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Landroid/graphics/drawable/Drawable;
    .locals 3

    .prologue
    .line 1415604
    new-instance v0, LX/8ui;

    invoke-direct {v0}, LX/8ui;-><init>()V

    .line 1415605
    iget-object v1, p0, LX/8ui;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1415606
    iput-object v1, v0, LX/8ui;->d:Ljava/lang/String;

    .line 1415607
    iget-object v1, p0, LX/8ui;->k:LX/8uh;

    move-object v1, v1

    .line 1415608
    iput-object v1, v0, LX/8ui;->k:LX/8uh;

    .line 1415609
    iget-object v1, p0, LX/8ui;->e:Ljava/lang/String;

    iput-object v1, v0, LX/8ui;->e:Ljava/lang/String;

    .line 1415610
    iget v1, p0, LX/8ui;->f:I

    iput v1, v0, LX/8ui;->f:I

    .line 1415611
    iget-boolean v1, p0, LX/8ui;->g:Z

    iput-boolean v1, v0, LX/8ui;->g:Z

    .line 1415612
    iget v1, p0, LX/8ui;->h:I

    iput v1, v0, LX/8ui;->h:I

    .line 1415613
    iget v1, p0, LX/8ui;->i:F

    iput v1, v0, LX/8ui;->i:F

    .line 1415614
    iget-object v1, v0, LX/8ui;->a:Landroid/graphics/Paint;

    iget-object v2, p0, LX/8ui;->a:Landroid/graphics/Paint;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->set(Landroid/graphics/Paint;)V

    .line 1415615
    iget-boolean v1, p0, LX/8ui;->j:Z

    iput-boolean v1, v0, LX/8ui;->j:Z

    .line 1415616
    return-object v0
.end method

.method public final b(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 1415514
    iget-object v0, p0, LX/8ui;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1415515
    return-void
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1415516
    iget-object v0, p0, LX/8ui;->e:Ljava/lang/String;

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final c(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 1415517
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8ui;->g:Z

    .line 1415518
    iput p1, p0, LX/8ui;->f:I

    .line 1415519
    return-void
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1415520
    iget-object v0, p0, LX/8ui;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 1415521
    :goto_0
    invoke-virtual {p0}, LX/8ui;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    .line 1415522
    iget-boolean v3, p0, LX/8ui;->g:Z

    if-nez v3, :cond_3

    .line 1415523
    :cond_0
    :goto_1
    if-eqz v0, :cond_1

    .line 1415524
    iget-object v0, p0, LX/8ui;->a:Landroid/graphics/Paint;

    iget-object v3, p0, LX/8ui;->d:Ljava/lang/String;

    iget-object v4, p0, LX/8ui;->d:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    iget-object v5, p0, LX/8ui;->b:Landroid/graphics/Rect;

    invoke-virtual {v0, v3, v1, v4, v5}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 1415525
    iget-object v0, p0, LX/8ui;->d:Ljava/lang/String;

    invoke-virtual {v2}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v1

    invoke-virtual {v2}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v2

    iget-object v3, p0, LX/8ui;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    add-float/2addr v2, v3

    iget-object v3, p0, LX/8ui;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1415526
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 1415527
    goto :goto_0

    .line 1415528
    :cond_3
    if-nez v0, :cond_4

    iget-boolean v3, p0, LX/8ui;->j:Z

    if-eqz v3, :cond_0

    .line 1415529
    :cond_4
    iget-object v3, p0, LX/8ui;->a:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->getColor()I

    move-result v3

    .line 1415530
    iget-object v4, p0, LX/8ui;->a:Landroid/graphics/Paint;

    iget v5, p0, LX/8ui;->f:I

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 1415531
    iget-object v4, p0, LX/8ui;->c:Landroid/graphics/RectF;

    invoke-virtual {v4, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 1415532
    iget-object v4, p0, LX/8ui;->c:Landroid/graphics/RectF;

    iget-object v5, p0, LX/8ui;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 1415533
    iget-object v4, p0, LX/8ui;->a:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1415534
    iget-object v4, p0, LX/8ui;->a:Landroid/graphics/Paint;

    iget v5, p0, LX/8ui;->i:F

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1415535
    iget-object v4, p0, LX/8ui;->a:Landroid/graphics/Paint;

    iget v5, p0, LX/8ui;->h:I

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 1415536
    iget-object v4, p0, LX/8ui;->c:Landroid/graphics/RectF;

    iget-object v5, p0, LX/8ui;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 1415537
    iget-object v4, p0, LX/8ui;->a:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1415538
    iget-object v4, p0, LX/8ui;->a:Landroid/graphics/Paint;

    invoke-virtual {v4, v3}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_1
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 1415539
    const/4 v0, -0x3

    return v0
.end method

.method public final invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 1415540
    invoke-virtual {p0}, LX/8ui;->invalidateSelf()V

    .line 1415541
    return-void
.end method

.method public final scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V
    .locals 1

    .prologue
    .line 1415542
    invoke-virtual {p0, p2, p3, p4}, LX/8ui;->scheduleSelf(Ljava/lang/Runnable;J)V

    .line 1415543
    return-void
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 1415544
    iget-object v0, p0, LX/8ui;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1415545
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 1415546
    iget-object v0, p0, LX/8ui;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 1415547
    return-void
.end method

.method public final unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 1415548
    invoke-virtual {p0, p2}, LX/8ui;->unscheduleSelf(Ljava/lang/Runnable;)V

    .line 1415549
    return-void
.end method
