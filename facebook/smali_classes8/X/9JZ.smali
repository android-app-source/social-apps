.class public final LX/9JZ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1465422
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLImage;)LX/1Fb;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1465423
    if-nez p0, :cond_1

    .line 1465424
    :cond_0
    :goto_0
    return-object v2

    .line 1465425
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1465426
    const/4 v1, 0x0

    .line 1465427
    if-nez p0, :cond_3

    .line 1465428
    :goto_1
    move v1, v1

    .line 1465429
    if-eqz v1, :cond_0

    .line 1465430
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1465431
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1465432
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1465433
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1465434
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 1465435
    const-string v1, "DefaultGraphQLConversionHelper.getDefaultImageFields"

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1465436
    :cond_2
    new-instance v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-direct {v2, v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;-><init>(LX/15i;)V

    goto :goto_0

    .line 1465437
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1465438
    const/4 v4, 0x3

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1465439
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v4

    invoke-virtual {v0, v1, v4, v1}, LX/186;->a(III)V

    .line 1465440
    const/4 v4, 0x1

    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1465441
    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v4

    invoke-virtual {v0, v3, v4, v1}, LX/186;->a(III)V

    .line 1465442
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1465443
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    goto :goto_1
.end method

.method public static a(LX/1VU;)Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 4

    .prologue
    .line 1465444
    if-nez p0, :cond_0

    .line 1465445
    const/4 v0, 0x0

    .line 1465446
    :goto_0
    return-object v0

    .line 1465447
    :cond_0
    new-instance v0, LX/3dM;

    invoke-direct {v0}, LX/3dM;-><init>()V

    .line 1465448
    invoke-interface {p0}, LX/1VU;->b()Z

    move-result v1

    .line 1465449
    iput-boolean v1, v0, LX/3dM;->e:Z

    .line 1465450
    invoke-interface {p0}, LX/1VU;->c()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/3dM;->c(Z)LX/3dM;

    .line 1465451
    invoke-interface {p0}, LX/1VU;->d()Z

    move-result v1

    .line 1465452
    iput-boolean v1, v0, LX/3dM;->g:Z

    .line 1465453
    invoke-interface {p0}, LX/1VU;->e()Z

    move-result v1

    .line 1465454
    iput-boolean v1, v0, LX/3dM;->i:Z

    .line 1465455
    invoke-interface {p0}, LX/1VU;->r_()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/3dM;->g(Z)LX/3dM;

    .line 1465456
    invoke-interface {p0}, LX/1VU;->s_()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/3dM;->j(Z)LX/3dM;

    .line 1465457
    invoke-interface {p0}, LX/1VU;->j()Ljava/lang/String;

    move-result-object v1

    .line 1465458
    iput-object v1, v0, LX/3dM;->y:Ljava/lang/String;

    .line 1465459
    invoke-interface {p0}, LX/1VU;->k()Ljava/lang/String;

    move-result-object v1

    .line 1465460
    iput-object v1, v0, LX/3dM;->D:Ljava/lang/String;

    .line 1465461
    invoke-interface {p0}, LX/1VU;->l()LX/17A;

    move-result-object v1

    .line 1465462
    if-nez v1, :cond_1

    .line 1465463
    const/4 v2, 0x0

    .line 1465464
    :goto_1
    move-object v1, v2

    .line 1465465
    invoke-virtual {v0, v1}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)LX/3dM;

    .line 1465466
    invoke-interface {p0}, LX/1VU;->m()LX/172;

    move-result-object v1

    .line 1465467
    if-nez v1, :cond_2

    .line 1465468
    const/4 v2, 0x0

    .line 1465469
    :goto_2
    move-object v1, v2

    .line 1465470
    invoke-virtual {v0, v1}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;)LX/3dM;

    .line 1465471
    invoke-virtual {v0}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    goto :goto_0

    .line 1465472
    :cond_1
    new-instance v2, LX/3dN;

    invoke-direct {v2}, LX/3dN;-><init>()V

    .line 1465473
    invoke-interface {v1}, LX/17A;->a()I

    move-result v3

    .line 1465474
    iput v3, v2, LX/3dN;->b:I

    .line 1465475
    invoke-virtual {v2}, LX/3dN;->a()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v2

    goto :goto_1

    .line 1465476
    :cond_2
    new-instance v2, LX/4ZH;

    invoke-direct {v2}, LX/4ZH;-><init>()V

    .line 1465477
    invoke-interface {v1}, LX/172;->a()I

    move-result v3

    .line 1465478
    iput v3, v2, LX/4ZH;->b:I

    .line 1465479
    invoke-interface {v1}, LX/172;->b()I

    move-result v3

    .line 1465480
    iput v3, v2, LX/4ZH;->e:I

    .line 1465481
    invoke-virtual {v2}, LX/4ZH;->a()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v2

    goto :goto_2
.end method

.method public static a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;
    .locals 2

    .prologue
    .line 1465482
    if-nez p0, :cond_0

    .line 1465483
    const/4 v0, 0x0

    .line 1465484
    :goto_0
    return-object v0

    .line 1465485
    :cond_0
    new-instance v0, LX/2dc;

    invoke-direct {v0}, LX/2dc;-><init>()V

    .line 1465486
    invoke-interface {p0}, LX/1Fb;->a()I

    move-result v1

    .line 1465487
    iput v1, v0, LX/2dc;->c:I

    .line 1465488
    invoke-interface {p0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    .line 1465489
    iput-object v1, v0, LX/2dc;->h:Ljava/lang/String;

    .line 1465490
    invoke-interface {p0}, LX/1Fb;->c()I

    move-result v1

    .line 1465491
    iput v1, v0, LX/2dc;->i:I

    .line 1465492
    invoke-virtual {v0}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0us;)Lcom/facebook/graphql/model/GraphQLPageInfo;
    .locals 2

    .prologue
    .line 1465493
    if-nez p0, :cond_0

    .line 1465494
    const/4 v0, 0x0

    .line 1465495
    :goto_0
    return-object v0

    .line 1465496
    :cond_0
    new-instance v0, LX/17L;

    invoke-direct {v0}, LX/17L;-><init>()V

    .line 1465497
    invoke-interface {p0}, LX/0us;->a()Ljava/lang/String;

    move-result-object v1

    .line 1465498
    iput-object v1, v0, LX/17L;->c:Ljava/lang/String;

    .line 1465499
    invoke-interface {p0}, LX/0us;->b()Z

    move-result v1

    .line 1465500
    iput-boolean v1, v0, LX/17L;->d:Z

    .line 1465501
    invoke-interface {p0}, LX/0us;->c()Z

    move-result v1

    .line 1465502
    iput-boolean v1, v0, LX/17L;->e:Z

    .line 1465503
    invoke-interface {p0}, LX/0us;->p_()Ljava/lang/String;

    move-result-object v1

    .line 1465504
    iput-object v1, v0, LX/17L;->f:Ljava/lang/String;

    .line 1465505
    invoke-virtual {v0}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/174;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 2

    .prologue
    .line 1465506
    if-nez p0, :cond_0

    .line 1465507
    const/4 v0, 0x0

    .line 1465508
    :goto_0
    return-object v0

    .line 1465509
    :cond_0
    new-instance v0, LX/173;

    invoke-direct {v0}, LX/173;-><init>()V

    .line 1465510
    invoke-interface {p0}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    .line 1465511
    iput-object v1, v0, LX/173;->f:Ljava/lang/String;

    .line 1465512
    invoke-virtual {v0}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/175;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 8

    .prologue
    .line 1465513
    if-nez p0, :cond_0

    .line 1465514
    const/4 v0, 0x0

    .line 1465515
    :goto_0
    return-object v0

    .line 1465516
    :cond_0
    new-instance v2, LX/173;

    invoke-direct {v2}, LX/173;-><init>()V

    .line 1465517
    invoke-interface {p0}, LX/175;->b()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1465518
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1465519
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-interface {p0}, LX/175;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1465520
    invoke-interface {p0}, LX/175;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1W5;

    .line 1465521
    if-nez v0, :cond_3

    .line 1465522
    const/4 v4, 0x0

    .line 1465523
    :goto_2
    move-object v0, v4

    .line 1465524
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1465525
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1465526
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1465527
    iput-object v0, v2, LX/173;->e:LX/0Px;

    .line 1465528
    :cond_2
    invoke-interface {p0}, LX/175;->a()Ljava/lang/String;

    move-result-object v0

    .line 1465529
    iput-object v0, v2, LX/173;->f:Ljava/lang/String;

    .line 1465530
    invoke-virtual {v2}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    goto :goto_0

    .line 1465531
    :cond_3
    new-instance v4, LX/4W6;

    invoke-direct {v4}, LX/4W6;-><init>()V

    .line 1465532
    invoke-interface {v0}, LX/1W5;->a()LX/171;

    move-result-object v5

    .line 1465533
    if-nez v5, :cond_4

    .line 1465534
    const/4 v6, 0x0

    .line 1465535
    :goto_3
    move-object v5, v6

    .line 1465536
    iput-object v5, v4, LX/4W6;->b:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 1465537
    invoke-interface {v0}, LX/1W5;->b()I

    move-result v5

    .line 1465538
    iput v5, v4, LX/4W6;->c:I

    .line 1465539
    invoke-interface {v0}, LX/1W5;->c()I

    move-result v5

    .line 1465540
    iput v5, v4, LX/4W6;->d:I

    .line 1465541
    invoke-virtual {v4}, LX/4W6;->a()Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    move-result-object v4

    goto :goto_2

    .line 1465542
    :cond_4
    new-instance v6, LX/170;

    invoke-direct {v6}, LX/170;-><init>()V

    .line 1465543
    invoke-interface {v5}, LX/171;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    .line 1465544
    iput-object v7, v6, LX/170;->ab:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1465545
    invoke-interface {v5}, LX/171;->c()LX/0Px;

    move-result-object v7

    .line 1465546
    iput-object v7, v6, LX/170;->b:LX/0Px;

    .line 1465547
    invoke-interface {v5}, LX/171;->d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    move-result-object v7

    .line 1465548
    iput-object v7, v6, LX/170;->l:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    .line 1465549
    invoke-interface {v5}, LX/171;->e()Ljava/lang/String;

    move-result-object v7

    .line 1465550
    iput-object v7, v6, LX/170;->o:Ljava/lang/String;

    .line 1465551
    invoke-interface {v5}, LX/171;->v_()Ljava/lang/String;

    move-result-object v7

    .line 1465552
    iput-object v7, v6, LX/170;->A:Ljava/lang/String;

    .line 1465553
    invoke-interface {v5}, LX/171;->w_()Ljava/lang/String;

    move-result-object v7

    .line 1465554
    iput-object v7, v6, LX/170;->X:Ljava/lang/String;

    .line 1465555
    invoke-interface {v5}, LX/171;->j()Ljava/lang/String;

    move-result-object v7

    .line 1465556
    iput-object v7, v6, LX/170;->Y:Ljava/lang/String;

    .line 1465557
    invoke-virtual {v6}, LX/170;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v6

    goto :goto_3
.end method
