.class public LX/94q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile n:LX/94q;


# instance fields
.field public final b:LX/0aG;

.field private final c:LX/0Xl;

.field public final d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final e:LX/0Zb;

.field public final f:LX/00H;

.field public final g:LX/9Tk;

.field public final h:LX/0SG;

.field private final i:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/contacts/upload/ContactsUploadStateListener;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/30I;

.field private k:Lcom/facebook/contacts/upload/ContactsUploadState;

.field private l:Lcom/facebook/contacts/upload/ContactsUploadVisibility;

.field public m:LX/1ML;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1435788
    const-class v0, LX/94q;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/94q;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0aG;LX/0Xl;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Zb;LX/00H;LX/9Tk;LX/0SG;Ljava/util/Set;LX/30I;)V
    .locals 1
    .param p2    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0aG;",
            "LX/0Xl;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Zb;",
            "LX/00H;",
            "LX/9Tk;",
            "LX/0SG;",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/contacts/upload/ContactsUploadStateListener;",
            ">;",
            "LX/30I;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1435775
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1435776
    invoke-static {}, Lcom/facebook/contacts/upload/ContactsUploadState;->e()Lcom/facebook/contacts/upload/ContactsUploadState;

    move-result-object v0

    iput-object v0, p0, LX/94q;->k:Lcom/facebook/contacts/upload/ContactsUploadState;

    .line 1435777
    sget-object v0, Lcom/facebook/contacts/upload/ContactsUploadVisibility;->HIDE:Lcom/facebook/contacts/upload/ContactsUploadVisibility;

    iput-object v0, p0, LX/94q;->l:Lcom/facebook/contacts/upload/ContactsUploadVisibility;

    .line 1435778
    iput-object p1, p0, LX/94q;->b:LX/0aG;

    .line 1435779
    iput-object p2, p0, LX/94q;->c:LX/0Xl;

    .line 1435780
    iput-object p3, p0, LX/94q;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1435781
    iput-object p4, p0, LX/94q;->e:LX/0Zb;

    .line 1435782
    iput-object p5, p0, LX/94q;->f:LX/00H;

    .line 1435783
    iput-object p6, p0, LX/94q;->g:LX/9Tk;

    .line 1435784
    iput-object p7, p0, LX/94q;->h:LX/0SG;

    .line 1435785
    iput-object p8, p0, LX/94q;->i:Ljava/util/Set;

    .line 1435786
    iput-object p9, p0, LX/94q;->j:LX/30I;

    .line 1435787
    return-void
.end method

.method public static a(LX/0QB;)LX/94q;
    .locals 14

    .prologue
    .line 1435760
    sget-object v0, LX/94q;->n:LX/94q;

    if-nez v0, :cond_1

    .line 1435761
    const-class v1, LX/94q;

    monitor-enter v1

    .line 1435762
    :try_start_0
    sget-object v0, LX/94q;->n:LX/94q;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1435763
    if-eqz v2, :cond_0

    .line 1435764
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1435765
    new-instance v3, LX/94q;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v4

    check-cast v4, LX/0aG;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v5

    check-cast v5, LX/0Xl;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v7

    check-cast v7, LX/0Zb;

    const-class v8, LX/00H;

    invoke-interface {v0, v8}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/00H;

    invoke-static {v0}, LX/9Tk;->a(LX/0QB;)LX/9Tk;

    move-result-object v9

    check-cast v9, LX/9Tk;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v10

    check-cast v10, LX/0SG;

    .line 1435766
    new-instance v11, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v12

    new-instance v13, LX/953;

    invoke-direct {v13, v0}, LX/953;-><init>(LX/0QB;)V

    invoke-direct {v11, v12, v13}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v11, v11

    .line 1435767
    invoke-static {v0}, LX/30I;->b(LX/0QB;)LX/30I;

    move-result-object v12

    check-cast v12, LX/30I;

    invoke-direct/range {v3 .. v12}, LX/94q;-><init>(LX/0aG;LX/0Xl;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Zb;LX/00H;LX/9Tk;LX/0SG;Ljava/util/Set;LX/30I;)V

    .line 1435768
    move-object v0, v3

    .line 1435769
    sput-object v0, LX/94q;->n:LX/94q;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1435770
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1435771
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1435772
    :cond_1
    sget-object v0, LX/94q;->n:LX/94q;

    return-object v0

    .line 1435773
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1435774
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static synthetic a(LX/94q;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1435758
    const-string v0, "contacts_upload"

    move-object v0, v0

    .line 1435759
    return-object v0
.end method

.method private a(Lcom/facebook/contacts/upload/ContactsUploadState;Lcom/facebook/contacts/upload/ContactsUploadVisibility;)V
    .locals 2

    .prologue
    .line 1435752
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1435753
    const-string v1, "com.facebook.orca.contacts.CONTACTS_UPLOAD_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1435754
    const-string v1, "state"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1435755
    const-string v1, "visibility"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1435756
    iget-object v1, p0, LX/94q;->c:LX/0Xl;

    invoke-interface {v1, v0}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 1435757
    return-void
.end method

.method public static a$redex0(LX/94q;Lcom/facebook/contacts/upload/ContactsUploadState;)V
    .locals 2

    .prologue
    .line 1435745
    invoke-direct {p0, p1}, LX/94q;->b(Lcom/facebook/contacts/upload/ContactsUploadState;)V

    .line 1435746
    invoke-direct {p0}, LX/94q;->f()Lcom/facebook/contacts/upload/ContactsUploadVisibility;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/94q;->a(Lcom/facebook/contacts/upload/ContactsUploadState;Lcom/facebook/contacts/upload/ContactsUploadVisibility;)V

    .line 1435747
    iget-object v0, p1, Lcom/facebook/contacts/upload/ContactsUploadState;->a:LX/94z;

    move-object v0, v0

    .line 1435748
    sget-object v1, LX/94z;->SUCCEEDED:LX/94z;

    if-ne v0, v1, :cond_0

    .line 1435749
    iget-object v0, p0, LX/94q;->i:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JuP;

    .line 1435750
    invoke-virtual {v0, p1}, LX/JuP;->a(Lcom/facebook/contacts/upload/ContactsUploadState;)V

    goto :goto_0

    .line 1435751
    :cond_0
    return-void
.end method

.method private b(Lcom/facebook/contacts/upload/ContactsUploadState;)V
    .locals 1

    .prologue
    .line 1435703
    monitor-enter p0

    .line 1435704
    :try_start_0
    iput-object p1, p0, LX/94q;->k:Lcom/facebook/contacts/upload/ContactsUploadState;

    .line 1435705
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private declared-synchronized f()Lcom/facebook/contacts/upload/ContactsUploadVisibility;
    .locals 1

    .prologue
    .line 1435744
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/94q;->l:Lcom/facebook/contacts/upload/ContactsUploadVisibility;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/facebook/contacts/upload/ContactsUploadVisibility;)LX/1ML;
    .locals 4

    .prologue
    .line 1435713
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1435714
    iget-object v0, p0, LX/94q;->m:LX/1ML;

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1435715
    if-eqz v0, :cond_1

    .line 1435716
    iget-object v0, p0, LX/94q;->l:Lcom/facebook/contacts/upload/ContactsUploadVisibility;

    sget-object v1, Lcom/facebook/contacts/upload/ContactsUploadVisibility;->HIDE:Lcom/facebook/contacts/upload/ContactsUploadVisibility;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/facebook/contacts/upload/ContactsUploadVisibility;->SHOW:Lcom/facebook/contacts/upload/ContactsUploadVisibility;

    if-ne p1, v0, :cond_0

    .line 1435717
    iput-object p1, p0, LX/94q;->l:Lcom/facebook/contacts/upload/ContactsUploadVisibility;

    .line 1435718
    invoke-virtual {p0}, LX/94q;->b()Lcom/facebook/contacts/upload/ContactsUploadState;

    move-result-object v0

    invoke-direct {p0}, LX/94q;->f()Lcom/facebook/contacts/upload/ContactsUploadVisibility;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/94q;->a(Lcom/facebook/contacts/upload/ContactsUploadState;Lcom/facebook/contacts/upload/ContactsUploadVisibility;)V

    .line 1435719
    :cond_0
    :goto_1
    iget-object v0, p0, LX/94q;->m:LX/1ML;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 1435720
    :cond_1
    :try_start_1
    iput-object p1, p0, LX/94q;->l:Lcom/facebook/contacts/upload/ContactsUploadVisibility;

    .line 1435721
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/facebook/contacts/upload/ContactsUploadState;->a(III)Lcom/facebook/contacts/upload/ContactsUploadState;

    move-result-object v0

    invoke-direct {p0, v0}, LX/94q;->b(Lcom/facebook/contacts/upload/ContactsUploadState;)V

    .line 1435722
    iget-object v0, p0, LX/94q;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/32V;->c:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1435723
    iget-object v0, p0, LX/94q;->j:LX/30I;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/30I;->a(Z)V

    .line 1435724
    iget-object v0, p0, LX/94q;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/32V;->d:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1435725
    iget-object v0, p0, LX/94q;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/32V;->h:LX/0Tn;

    iget-object v2, p0, LX/94q;->h:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1435726
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1435727
    const-string v0, "forceFullUploadAndTurnOffGlobalKillSwitch"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1435728
    iget-object v0, p0, LX/94q;->f:LX/00H;

    .line 1435729
    iget-object v2, v0, LX/00H;->j:LX/01T;

    move-object v0, v2

    .line 1435730
    sget-object v2, LX/01T;->FB4A:LX/01T;

    if-ne v0, v2, :cond_4

    const-string v0, "contacts_upload_friend_finder"

    .line 1435731
    :goto_2
    const-string v2, "contacts_upload_friend_finder"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1435732
    iget-object v2, p0, LX/94q;->g:LX/9Tk;

    .line 1435733
    sget-object v3, LX/9Tj;->TURN_ON_CONTINUOUS_SYNC:LX/9Tj;

    invoke-static {v2, v3}, LX/9Tk;->a(LX/9Tk;LX/9Tj;)V

    .line 1435734
    :cond_2
    iget-object v2, p0, LX/94q;->b:LX/0aG;

    const v3, 0x4297b9e7

    invoke-static {v2, v0, v1, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    new-instance v1, LX/94o;

    invoke-direct {v1, p0}, LX/94o;-><init>(LX/94q;)V

    invoke-interface {v0, v1}, LX/1MF;->setOnProgressListener(LX/4An;)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    iput-object v0, p0, LX/94q;->m:LX/1ML;

    .line 1435735
    iget-object v0, p0, LX/94q;->m:LX/1ML;

    new-instance v1, LX/94p;

    invoke-direct {v1, p0}, LX/94p;-><init>(LX/94q;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1435736
    iget-object v0, p0, LX/94q;->e:LX/0Zb;

    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "contacts_upload_started"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1435737
    const-string v2, "contacts_upload"

    move-object v2, v2

    .line 1435738
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1435739
    move-object v1, v1

    .line 1435740
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1435741
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/facebook/contacts/upload/ContactsUploadState;->a(III)Lcom/facebook/contacts/upload/ContactsUploadState;

    move-result-object v0

    invoke-static {p0, v0}, LX/94q;->a$redex0(LX/94q;Lcom/facebook/contacts/upload/ContactsUploadState;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 1435742
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    :try_start_2
    const/4 v0, 0x0

    goto/16 :goto_0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1435743
    :cond_4
    const-string v0, "contacts_upload_messaging"

    goto :goto_2
.end method

.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 1435709
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/facebook/contacts/upload/ContactsUploadVisibility;->HIDE:Lcom/facebook/contacts/upload/ContactsUploadVisibility;

    iput-object v0, p0, LX/94q;->l:Lcom/facebook/contacts/upload/ContactsUploadVisibility;

    .line 1435710
    invoke-static {}, Lcom/facebook/contacts/upload/ContactsUploadState;->e()Lcom/facebook/contacts/upload/ContactsUploadState;

    move-result-object v0

    invoke-static {p0, v0}, LX/94q;->a$redex0(LX/94q;Lcom/facebook/contacts/upload/ContactsUploadState;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1435711
    monitor-exit p0

    return-void

    .line 1435712
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Lcom/facebook/contacts/upload/ContactsUploadState;
    .locals 1

    .prologue
    .line 1435708
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/94q;->k:Lcom/facebook/contacts/upload/ContactsUploadState;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final clearUserData()V
    .locals 0

    .prologue
    .line 1435706
    invoke-virtual {p0}, LX/94q;->a()V

    .line 1435707
    return-void
.end method
