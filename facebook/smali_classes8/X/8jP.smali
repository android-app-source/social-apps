.class public final LX/8jP;
.super LX/4An;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/stickers/model/StickerPack;

.field public final synthetic b:Lcom/facebook/stickers/client/StickerDownloadManager;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/client/StickerDownloadManager;Lcom/facebook/stickers/model/StickerPack;)V
    .locals 0

    .prologue
    .line 1392394
    iput-object p1, p0, LX/8jP;->b:Lcom/facebook/stickers/client/StickerDownloadManager;

    iput-object p2, p0, LX/8jP;->a:Lcom/facebook/stickers/model/StickerPack;

    invoke-direct {p0}, LX/4An;-><init>()V

    return-void
.end method


# virtual methods
.method public final onProgress(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 4

    .prologue
    .line 1392395
    iget-object v0, p1, Lcom/facebook/fbservice/service/OperationResult;->resultDataString:Ljava/lang/String;

    move-object v0, v0

    .line 1392396
    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    .line 1392397
    iget-object v1, p0, LX/8jP;->b:Lcom/facebook/stickers/client/StickerDownloadManager;

    iget-object v1, v1, Lcom/facebook/stickers/client/StickerDownloadManager;->h:Ljava/util/HashMap;

    iget-object v2, p0, LX/8jP;->a:Lcom/facebook/stickers/model/StickerPack;

    .line 1392398
    iget-object v3, v2, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v2, v3

    .line 1392399
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1392400
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.facebook.orca.stickers.DOWNLOAD_PROGRESS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1392401
    const-string v2, "stickerPack"

    iget-object v3, p0, LX/8jP;->a:Lcom/facebook/stickers/model/StickerPack;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1392402
    const-string v2, "progress"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1392403
    iget-object v0, p0, LX/8jP;->b:Lcom/facebook/stickers/client/StickerDownloadManager;

    iget-object v0, v0, Lcom/facebook/stickers/client/StickerDownloadManager;->e:LX/0Xl;

    invoke-interface {v0, v1}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 1392404
    return-void
.end method
