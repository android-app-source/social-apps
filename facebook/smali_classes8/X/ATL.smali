.class public final LX/ATL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/ATO;


# direct methods
.method public constructor <init>(LX/ATO;)V
    .locals 0

    .prologue
    .line 1675502
    iput-object p1, p0, LX/ATL;->a:LX/ATO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/ATL;ILcom/facebook/composer/attachments/ComposerAttachment;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1675494
    iget-object v0, p0, LX/ATL;->a:LX/ATO;

    iget-object v0, v0, LX/ATO;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ASn;

    .line 1675495
    invoke-interface {v0, p2}, LX/ASn;->c(Lcom/facebook/composer/attachments/ComposerAttachment;)V

    .line 1675496
    iget-object v0, p0, LX/ATL;->a:LX/ATO;

    iget-object v0, v0, LX/ATO;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1675497
    iget-object v0, p0, LX/ATL;->a:LX/ATO;

    iget-object v0, v0, LX/ATO;->a:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1675498
    iget-object v0, p0, LX/ATL;->a:LX/ATO;

    iget-object v0, v0, LX/ATO;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ATE;

    .line 1675499
    iput-object p2, v0, LX/ATE;->e:Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1675500
    iget-object v0, p0, LX/ATL;->a:LX/ATO;

    iget-object v0, v0, LX/ATO;->B:LX/ASe;

    invoke-interface {v0, p1, p2, v1, v1}, LX/ASe;->a(ILcom/facebook/composer/attachments/ComposerAttachment;ZZ)V

    .line 1675501
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/composer/attachments/ComposerAttachment;Lcom/facebook/ipc/media/MediaItem;Lcom/facebook/photos/creativeediting/model/CreativeEditingData;Z)V
    .locals 4

    .prologue
    .line 1675479
    iget-object v0, p0, LX/ATL;->a:LX/ATO;

    iget-object v0, v0, LX/ATO;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 1675480
    const/4 v0, -0x1

    if-ne v1, v0, :cond_0

    .line 1675481
    :goto_0
    return-void

    .line 1675482
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    const/4 v2, 0x0

    .line 1675483
    iput-object v2, v0, Lcom/facebook/ipc/media/MediaItem;->f:Ljava/lang/String;

    .line 1675484
    invoke-static {p1}, LX/7kv;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)LX/7kv;

    move-result-object v0

    .line 1675485
    iput-object p3, v0, LX/7kv;->e:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1675486
    move-object v0, v0

    .line 1675487
    iget-object v2, p0, LX/ATL;->a:LX/ATO;

    iget-object v2, v2, LX/ATO;->o:LX/74n;

    invoke-virtual {v0, p2, v2}, LX/7kv;->a(Lcom/facebook/ipc/media/MediaItem;LX/74n;)LX/7kv;

    move-result-object v0

    invoke-virtual {v0}, LX/7kv;->a()Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v2

    .line 1675488
    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    invoke-static {v0, p2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p3}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    if-eqz p4, :cond_3

    :cond_1
    const/4 v0, 0x1

    .line 1675489
    :goto_1
    iget-object v3, p0, LX/ATL;->a:LX/ATO;

    iget-boolean v3, v3, LX/ATO;->S:Z

    if-nez v3, :cond_2

    .line 1675490
    iget-object v3, p0, LX/ATL;->a:LX/ATO;

    .line 1675491
    iput-boolean v0, v3, LX/ATO;->S:Z

    .line 1675492
    :cond_2
    iget-object v3, p0, LX/ATL;->a:LX/ATO;

    iget-object v3, v3, LX/ATO;->B:LX/ASe;

    invoke-interface {v3, v1, v2, v0, p4}, LX/ASe;->a(ILcom/facebook/composer/attachments/ComposerAttachment;ZZ)V

    goto :goto_0

    .line 1675493
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/composer/attachments/ComposerAttachment;Lcom/facebook/ipc/media/MediaItem;Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 1675464
    iget-object v0, p0, LX/ATL;->a:LX/ATO;

    iget-object v0, v0, LX/ATO;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 1675465
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1675466
    :goto_0
    return-void

    .line 1675467
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    .line 1675468
    iput-object v3, v1, Lcom/facebook/ipc/media/MediaItem;->f:Ljava/lang/String;

    .line 1675469
    invoke-static {p1}, LX/7kv;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)LX/7kv;

    move-result-object v1

    .line 1675470
    iput-object p3, v1, LX/7kv;->f:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 1675471
    move-object v1, v1

    .line 1675472
    iget-object v2, p0, LX/ATL;->a:LX/ATO;

    iget-object v2, v2, LX/ATO;->o:LX/74n;

    invoke-virtual {v1, p2, v2}, LX/7kv;->a(Lcom/facebook/ipc/media/MediaItem;LX/74n;)LX/7kv;

    move-result-object v1

    .line 1675473
    iput-object v3, v1, LX/7kv;->g:Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    .line 1675474
    move-object v1, v1

    .line 1675475
    iput-object p4, v1, LX/7kv;->i:Ljava/lang/String;

    .line 1675476
    move-object v1, v1

    .line 1675477
    invoke-virtual {v1}, LX/7kv;->a()Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v1

    .line 1675478
    iget-object v2, p0, LX/ATL;->a:LX/ATO;

    iget-object v2, v2, LX/ATO;->B:LX/ASe;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-interface {v2, v0, v1, v3, v4}, LX/ASe;->a(ILcom/facebook/composer/attachments/ComposerAttachment;ZZ)V

    goto :goto_0
.end method

.method public final b()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1675439
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1675440
    iget-object v0, p0, LX/ATL;->a:LX/ATO;

    iget-object v0, v0, LX/ATO;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1675441
    invoke-static {v0}, LX/7kq;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1675442
    iget-object v4, p0, LX/ATL;->a:LX/ATO;

    iget-object v4, v4, LX/ATO;->a:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 1675443
    const/4 v4, -0x1

    if-ne v0, v4, :cond_2

    .line 1675444
    :cond_1
    return-void

    .line 1675445
    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1675446
    :cond_3
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v2

    :goto_1
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 1675447
    iget-object v0, p0, LX/ATL;->a:LX/ATO;

    iget-object v0, v0, LX/ATO;->a:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-static {v0}, LX/7kv;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)LX/7kv;

    move-result-object v0

    const-string v6, "high"

    .line 1675448
    iput-object v6, v0, LX/7kv;->i:Ljava/lang/String;

    .line 1675449
    move-object v0, v0

    .line 1675450
    invoke-virtual {v0}, LX/7kv;->a()Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v0

    .line 1675451
    iget-object v6, p0, LX/ATL;->a:LX/ATO;

    iget-object v6, v6, LX/ATO;->B:LX/ASe;

    const/4 v7, 0x1

    invoke-interface {v6, v5, v0, v2, v7}, LX/ASe;->a(ILcom/facebook/composer/attachments/ComposerAttachment;ZZ)V

    .line 1675452
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method public final b(Lcom/facebook/composer/attachments/ComposerAttachment;)V
    .locals 2

    .prologue
    .line 1675461
    iget-object v0, p0, LX/ATL;->a:LX/ATO;

    iget-object v0, v0, LX/ATO;->G:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1675462
    :goto_0
    return-void

    .line 1675463
    :cond_0
    iget-object v0, p0, LX/ATL;->a:LX/ATO;

    iget-object v0, v0, LX/ATO;->B:LX/ASe;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, LX/ASe;->a(Lcom/facebook/composer/attachments/ComposerAttachment;Lcom/facebook/photos/base/tagging/FaceBox;)V

    goto :goto_0
.end method

.method public final c(Lcom/facebook/composer/attachments/ComposerAttachment;)V
    .locals 4

    .prologue
    .line 1675453
    iget-object v0, p0, LX/ATL;->a:LX/ATO;

    iget-object v0, v0, LX/ATO;->G:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1675454
    :goto_0
    return-void

    .line 1675455
    :cond_0
    iget-object v0, p0, LX/ATL;->a:LX/ATO;

    iget-object v1, p0, LX/ATL;->a:LX/ATO;

    iget-object v1, v1, LX/ATO;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 1675456
    iput v1, v0, LX/ATO;->I:I

    .line 1675457
    iget-object v0, p0, LX/ATL;->a:LX/ATO;

    iget-object v1, v0, LX/ATO;->G:LX/0wd;

    iget-object v0, p0, LX/ATL;->a:LX/ATO;

    iget v0, v0, LX/ATO;->I:I

    iget-object v2, p0, LX/ATL;->a:LX/ATO;

    iget-object v2, v2, LX/ATO;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-eq v0, v2, :cond_1

    iget-object v0, p0, LX/ATL;->a:LX/ATO;

    iget-object v0, v0, LX/ATO;->aa:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0jA;

    invoke-interface {v0}, LX/0jA;->getSlideshowData()Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    move-result-object v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 1675458
    :goto_1
    iput-boolean v0, v1, LX/0wd;->c:Z

    .line 1675459
    move-object v0, v1

    .line 1675460
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
