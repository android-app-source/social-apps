.class public LX/AR8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public volatile a:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/facebook/composer/system/model/ComposerModelImpl;

.field public final c:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

.field private final d:LX/8LV;

.field private final e:LX/1EZ;

.field public final f:Lcom/facebook/ipc/composer/model/ComposerDateInfo;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/system/model/ComposerModelImpl;Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;Lcom/facebook/ipc/composer/model/ComposerDateInfo;LX/8LV;LX/1EZ;)V
    .locals 0
    .param p1    # Lcom/facebook/composer/system/model/ComposerModelImpl;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/ipc/composer/model/ComposerDateInfo;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1672538
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1672539
    iput-object p1, p0, LX/AR8;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    .line 1672540
    iput-object p2, p0, LX/AR8;->c:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    .line 1672541
    iput-object p3, p0, LX/AR8;->f:Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    .line 1672542
    iput-object p4, p0, LX/AR8;->d:LX/8LV;

    .line 1672543
    iput-object p5, p0, LX/AR8;->e:LX/1EZ;

    .line 1672544
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;)Z
    .locals 13

    .prologue
    .line 1672545
    iget-object v0, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->composerSessionId:Ljava/lang/String;

    iget-object v1, p0, LX/AR8;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1672546
    iget-object v0, p0, LX/AR8;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->l(LX/0Px;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1672547
    const/4 v0, 0x0

    .line 1672548
    :goto_0
    return v0

    .line 1672549
    :cond_0
    iget-object v0, p0, LX/AR8;->d:LX/8LV;

    iget-object v1, p0, LX/AR8;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/7kq;->s(LX/0Px;)LX/0Px;

    move-result-object v1

    iget-object v2, p0, LX/AR8;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/7kq;->u(LX/0Px;)LX/0Px;

    move-result-object v2

    new-instance v3, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    iget-object v4, p0, LX/AR8;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v4}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, LX/AR8;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v4}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v5

    move-object v4, p1

    .line 1672550
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1672551
    new-instance v6, LX/8LQ;

    invoke-direct {v6}, LX/8LQ;-><init>()V

    .line 1672552
    iput-object v5, v6, LX/8LQ;->a:Ljava/lang/String;

    .line 1672553
    move-object v6, v6

    .line 1672554
    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v7

    .line 1672555
    iput-object v7, v6, LX/8LQ;->b:LX/0Px;

    .line 1672556
    move-object v6, v6

    .line 1672557
    iput-object v2, v6, LX/8LQ;->c:LX/0Px;

    .line 1672558
    move-object v6, v6

    .line 1672559
    const-string v7, "life_event"

    .line 1672560
    iput-object v7, v6, LX/8LQ;->i:Ljava/lang/String;

    .line 1672561
    move-object v6, v6

    .line 1672562
    iput-object v4, v6, LX/8LQ;->w:Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;

    .line 1672563
    move-object v6, v6

    .line 1672564
    iput-object v3, v6, LX/8LQ;->o:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 1672565
    move-object v6, v6

    .line 1672566
    sget-object v7, LX/8LR;->LIFE_EVENT:LX/8LR;

    .line 1672567
    iput-object v7, v6, LX/8LQ;->p:LX/8LR;

    .line 1672568
    move-object v6, v6

    .line 1672569
    sget-object v7, LX/8LS;->LIFE_EVENT:LX/8LS;

    .line 1672570
    iput-object v7, v6, LX/8LQ;->q:LX/8LS;

    .line 1672571
    move-object v6, v6

    .line 1672572
    iget-object v7, v0, LX/8LV;->b:LX/0SG;

    invoke-interface {v7}, LX/0SG;->a()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    div-long/2addr v8, v10

    .line 1672573
    iput-wide v8, v6, LX/8LQ;->x:J

    .line 1672574
    move-object v6, v6

    .line 1672575
    invoke-virtual {v6}, LX/8LQ;->a()Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v6

    move-object v0, v6

    .line 1672576
    iget-object v1, p0, LX/AR8;->e:LX/1EZ;

    invoke-virtual {v1, v0}, LX/1EZ;->a(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1672577
    const/4 v0, 0x1

    goto :goto_0
.end method
