.class public LX/8hr;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0y2;


# direct methods
.method public constructor <init>(LX/0y2;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1390807
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1390808
    iput-object p1, p0, LX/8hr;->a:LX/0y2;

    .line 1390809
    return-void
.end method

.method public static b(LX/0QB;)LX/8hr;
    .locals 2

    .prologue
    .line 1390810
    new-instance v1, LX/8hr;

    invoke-static {p0}, LX/0y2;->b(LX/0QB;)LX/0y2;

    move-result-object v0

    check-cast v0, LX/0y2;

    invoke-direct {v1, v0}, LX/8hr;-><init>(LX/0y2;)V

    .line 1390811
    return-object v1
.end method


# virtual methods
.method public final a()LX/4FR;
    .locals 5

    .prologue
    .line 1390795
    iget-object v0, p0, LX/8hr;->a:LX/0y2;

    invoke-virtual {v0}, LX/0y2;->a()Lcom/facebook/location/ImmutableLocation;

    move-result-object v0

    .line 1390796
    if-eqz v0, :cond_1

    .line 1390797
    new-instance v2, LX/4FR;

    invoke-direct {v2}, LX/4FR;-><init>()V

    .line 1390798
    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/4FR;->a(Ljava/lang/Double;)LX/4FR;

    .line 1390799
    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/4FR;->b(Ljava/lang/Double;)LX/4FR;

    .line 1390800
    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v1

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1390801
    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v1

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->doubleValue()D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/4FR;->c(Ljava/lang/Double;)LX/4FR;

    .line 1390802
    :cond_0
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/4FR;->a(Ljava/lang/Boolean;)LX/4FR;

    .line 1390803
    move-object v0, v2

    .line 1390804
    :goto_0
    return-object v0

    :cond_1
    const-wide/16 v3, 0x0

    .line 1390805
    new-instance v1, LX/4FR;

    invoke-direct {v1}, LX/4FR;-><init>()V

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4FR;->a(Ljava/lang/Double;)LX/4FR;

    move-result-object v1

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4FR;->b(Ljava/lang/Double;)LX/4FR;

    move-result-object v1

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4FR;->c(Ljava/lang/Double;)LX/4FR;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4FR;->a(Ljava/lang/Boolean;)LX/4FR;

    move-result-object v1

    move-object v0, v1

    .line 1390806
    goto :goto_0
.end method
