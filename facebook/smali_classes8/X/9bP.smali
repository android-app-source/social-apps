.class public final LX/9bP;
.super LX/79T;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLPhoto;

.field public final synthetic b:Landroid/support/v4/app/FragmentActivity;

.field public final synthetic c:J

.field public final synthetic d:Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;Lcom/facebook/graphql/model/GraphQLPhoto;Landroid/support/v4/app/FragmentActivity;J)V
    .locals 0

    .prologue
    .line 1514888
    iput-object p1, p0, LX/9bP;->d:Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    iput-object p2, p0, LX/9bP;->a:Lcom/facebook/graphql/model/GraphQLPhoto;

    iput-object p3, p0, LX/9bP;->b:Landroid/support/v4/app/FragmentActivity;

    iput-wide p4, p0, LX/9bP;->c:J

    invoke-direct {p0}, LX/79T;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 15

    .prologue
    .line 1514889
    iget-object v0, p0, LX/9bP;->d:Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    iget-object v1, p0, LX/9bP;->a:Lcom/facebook/graphql/model/GraphQLPhoto;

    iget-object v2, p0, LX/9bP;->b:Landroid/support/v4/app/FragmentActivity;

    iget-wide v4, p0, LX/9bP;->c:J

    const/4 v9, 0x1

    .line 1514890
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v6

    .line 1514891
    const v7, 0x7f0811da

    invoke-static {v7, v9, v9}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(IZZ)Landroid/support/v4/app/DialogFragment;

    move-result-object v11

    .line 1514892
    invoke-virtual {v6}, LX/0gc;->a()LX/0hH;

    move-result-object v7

    const-string v8, "DownloadingCoverPhotoDialog"

    invoke-virtual {v11, v7, v8, v9}, Landroid/support/v4/app/DialogFragment;->a(LX/0hH;Ljava/lang/String;Z)I

    .line 1514893
    invoke-virtual {v6}, LX/0gc;->b()Z

    .line 1514894
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPhoto;->K()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v1}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a(Lcom/facebook/graphql/model/GraphQLPhoto;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    invoke-static {v6}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v14

    move-object v7, v0

    move-object v10, v2

    move-wide v12, v4

    invoke-static/range {v7 .. v14}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a(Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;JLandroid/support/v4/app/FragmentActivity;Landroid/support/v4/app/DialogFragment;JLandroid/net/Uri;)V

    .line 1514895
    return-void
.end method
