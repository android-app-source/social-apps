.class public final LX/9FQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8qq;


# instance fields
.field public final synthetic a:LX/9FA;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLComment;

.field public final synthetic c:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public final synthetic d:Z

.field public final synthetic e:LX/9FG;

.field public final synthetic f:Lcom/facebook/graphql/model/GraphQLComment;

.field public final synthetic g:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public final synthetic h:Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;LX/9FA;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;ZLX/9FG;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 0

    .prologue
    .line 1458553
    iput-object p1, p0, LX/9FQ;->h:Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;

    iput-object p2, p0, LX/9FQ;->a:LX/9FA;

    iput-object p3, p0, LX/9FQ;->b:Lcom/facebook/graphql/model/GraphQLComment;

    iput-object p4, p0, LX/9FQ;->c:Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-boolean p5, p0, LX/9FQ;->d:Z

    iput-object p6, p0, LX/9FQ;->e:LX/9FG;

    iput-object p7, p0, LX/9FQ;->f:Lcom/facebook/graphql/model/GraphQLComment;

    iput-object p8, p0, LX/9FQ;->g:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1458554
    iget-object v0, p0, LX/9FQ;->a:LX/9FA;

    iget-object v1, p0, LX/9FQ;->b:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1458555
    iget-object p0, v0, LX/9FA;->b:LX/9Bi;

    if-eqz p0, :cond_0

    .line 1458556
    iget-object p0, v0, LX/9FA;->b:LX/9Bi;

    invoke-interface {p0, v1}, LX/9Bi;->c(Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 1458557
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1458558
    iget-object v0, p0, LX/9FQ;->a:LX/9FA;

    iget-object v1, p0, LX/9FQ;->b:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v0, v1}, LX/9FA;->b(Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 1458559
    return-void
.end method

.method public final a(Landroid/view/View;LX/1zt;)V
    .locals 6

    .prologue
    .line 1458560
    iget-object v0, p0, LX/9FQ;->a:LX/9FA;

    iget-object v1, p0, LX/9FQ;->b:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v2, p0, LX/9FQ;->c:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0, v1, v2, p2}, LX/9FA;->a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;LX/1zt;)V

    .line 1458561
    iget v0, p2, LX/1zt;->e:I

    move v0, v0

    .line 1458562
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1458563
    iget-object v0, p0, LX/9FQ;->h:Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3RX;

    const-string v1, "like_comment"

    invoke-virtual {v0, v1}, LX/3RX;->a(Ljava/lang/String;)V

    .line 1458564
    iget-object v0, p0, LX/9FQ;->h:Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;

    iget-object v1, p0, LX/9FQ;->c:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1458565
    iget-object v2, v0, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;->l:LX/20l;

    invoke-virtual {v2}, LX/20l;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1458566
    :cond_0
    :goto_0
    return-void

    .line 1458567
    :cond_1
    iget-object v2, v0, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;->m:LX/0iA;

    new-instance v3, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v4, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->COMMENT_UFI_LIKE_CLICKED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v3, v4}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    const-class v4, LX/9FL;

    invoke-virtual {v2, v3, v4}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v2

    check-cast v2, LX/9FL;

    .line 1458568
    if-eqz v2, :cond_0

    .line 1458569
    invoke-virtual {v2, p1, v1}, LX/9FL;->a(Landroid/view/View;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1458570
    iget-object v3, v0, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;->n:LX/20m;

    const-string v4, "feedback_comments"

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v5

    sget-object p0, LX/9BS;->COMMENT_FOOTER:LX/9BS;

    invoke-virtual {v2}, LX/9FL;->b()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v3, v4, v5, p0, p2}, LX/20m;->a(Ljava/lang/String;Ljava/lang/String;LX/9BS;Ljava/lang/String;)V

    .line 1458571
    iget-object v3, v0, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;->m:LX/0iA;

    invoke-virtual {v3}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v3

    invoke-virtual {v2}, LX/9FL;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1458572
    iget-object v0, p0, LX/9FQ;->a:LX/9FA;

    iget-object v1, p0, LX/9FQ;->b:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v0, v1, p2, p3}, LX/9FA;->a(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;Ljava/lang/String;)V

    .line 1458573
    return-void
.end method

.method public final b(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1458574
    iget-object v0, p0, LX/9FQ;->a:LX/9FA;

    iget-object v1, p0, LX/9FQ;->b:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v2, p0, LX/9FQ;->c:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0, v1, v2}, LX/9FA;->a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1458575
    return-void
.end method

.method public final c(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1458576
    iget-object v0, p0, LX/9FQ;->a:LX/9FA;

    iget-object v1, p0, LX/9FQ;->b:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v0, v1}, LX/9FA;->c(Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 1458577
    return-void
.end method

.method public final d(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 1458578
    iget-boolean v0, p0, LX/9FQ;->d:Z

    if-eqz v0, :cond_0

    .line 1458579
    iget-object v0, p0, LX/9FQ;->e:LX/9FG;

    invoke-virtual {v0}, LX/9FG;->h()V

    .line 1458580
    :cond_0
    iget-object v0, p0, LX/9FQ;->a:LX/9FA;

    iget-object v1, p0, LX/9FQ;->b:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v2, p0, LX/9FQ;->f:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v3, p0, LX/9FQ;->g:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0, v1, v2, v3}, LX/9FA;->a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1458581
    return-void
.end method

.method public final e(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1458582
    iget-object v0, p0, LX/9FQ;->a:LX/9FA;

    iget-object v1, p0, LX/9FQ;->b:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v0, v1}, LX/9FA;->a(Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 1458583
    return-void
.end method
