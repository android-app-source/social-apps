.class public LX/9UM;
.super LX/9UL;
.source ""

# interfaces
.implements Landroid/widget/Filterable;


# instance fields
.field public g:Ljava/lang/String;

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/ipc/model/FacebookProfile;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/9UK;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1497928
    invoke-direct {p0, p1}, LX/9UL;-><init>(Landroid/content/Context;)V

    .line 1497929
    new-instance v0, LX/9UK;

    invoke-direct {v0, p0}, LX/9UK;-><init>(LX/9UM;)V

    iput-object v0, p0, LX/9UM;->i:LX/9UK;

    .line 1497930
    return-void
.end method


# virtual methods
.method public final a(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 1497931
    invoke-virtual {p0, p1, p2}, LX/9UL;->a(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookProfile;

    .line 1497932
    if-nez p4, :cond_0

    .line 1497933
    iget-object v1, p0, LX/9UL;->d:Landroid/view/LayoutInflater;

    const v2, 0x7f031071

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p5, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p4

    .line 1497934
    :cond_0
    const v1, 0x7f0d057e

    invoke-virtual {p4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 1497935
    iget-object v2, v0, Lcom/facebook/ipc/model/FacebookProfile;->mImageUrl:Ljava/lang/String;

    .line 1497936
    if-nez v2, :cond_1

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 1497937
    iget-object v2, v0, Lcom/facebook/ipc/model/FacebookProfile;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 1497938
    const v2, 0x7f0e013f

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleTextAppearance(I)V

    .line 1497939
    iget-object v0, v0, Lcom/facebook/ipc/model/FacebookProfile;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1497940
    return-object p4

    .line 1497941
    :cond_1
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/ipc/model/FacebookProfile;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1497942
    iput-object p1, p0, LX/9UM;->h:Ljava/util/List;

    .line 1497943
    iget-object v0, p0, LX/9UM;->i:LX/9UK;

    iget-object v1, p0, LX/9UM;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/9UK;->filter(Ljava/lang/CharSequence;)V

    .line 1497944
    const v0, -0x6a291b1f

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1497945
    return-void
.end method

.method public final getFilter()Landroid/widget/Filter;
    .locals 1

    .prologue
    .line 1497946
    iget-object v0, p0, LX/9UM;->i:LX/9UK;

    return-object v0
.end method
