.class public LX/9I1;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/9Hz;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedback/ui/rows/views/specs/CommentPhotoAttachmentComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1462421
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/9I1;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedback/ui/rows/views/specs/CommentPhotoAttachmentComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1462384
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1462385
    iput-object p1, p0, LX/9I1;->b:LX/0Ot;

    .line 1462386
    return-void
.end method

.method public static a(LX/0QB;)LX/9I1;
    .locals 4

    .prologue
    .line 1462410
    const-class v1, LX/9I1;

    monitor-enter v1

    .line 1462411
    :try_start_0
    sget-object v0, LX/9I1;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1462412
    sput-object v2, LX/9I1;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1462413
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1462414
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1462415
    new-instance v3, LX/9I1;

    const/16 p0, 0x1de0

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/9I1;-><init>(LX/0Ot;)V

    .line 1462416
    move-object v0, v3

    .line 1462417
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1462418
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/9I1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1462419
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1462420
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1462422
    const v0, 0x7130770c

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 1462399
    check-cast p2, LX/9I0;

    .line 1462400
    iget-object v0, p0, LX/9I1;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedback/ui/rows/views/specs/CommentPhotoAttachmentComponentSpec;

    iget-object v1, p2, LX/9I0;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget-boolean v2, p2, LX/9I0;->b:Z

    const/16 p2, 0x18

    const/4 p0, 0x6

    const/4 v9, 0x4

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 1462401
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    .line 1462402
    iget-object v4, v0, Lcom/facebook/feedback/ui/rows/views/specs/CommentPhotoAttachmentComponentSpec;->d:LX/1ev;

    invoke-virtual {v4, v3}, LX/1ev;->b(Lcom/facebook/graphql/model/GraphQLMedia;)LX/1f6;

    move-result-object v3

    .line 1462403
    iget v4, v3, LX/1f6;->e:I

    move v4, v4

    .line 1462404
    iget v5, v3, LX/1f6;->f:I

    move v5, v5

    .line 1462405
    invoke-static {v4, v5}, LX/26J;->a(II)F

    move-result v4

    .line 1462406
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    iget-object v6, v0, Lcom/facebook/feedback/ui/rows/views/specs/CommentPhotoAttachmentComponentSpec;->c:LX/1nu;

    invoke-virtual {v6, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v6

    invoke-virtual {v3}, LX/1f6;->a()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v6, v3}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v3

    sget-object v6, Lcom/facebook/feedback/ui/rows/views/specs/CommentPhotoAttachmentComponentSpec;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v6}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/1nw;->c(F)LX/1nw;

    move-result-object v3

    const v4, 0x7f0a0046

    invoke-virtual {v3, v4}, LX/1nw;->h(I)LX/1nw;

    move-result-object v3

    const v4, 0x7f02162b

    invoke-virtual {v3, v4}, LX/1nw;->k(I)LX/1nw;

    move-result-object v3

    const v4, 0x7f021af6

    invoke-virtual {v3, v4}, LX/1nw;->i(I)LX/1nw;

    move-result-object v3

    const/16 v4, 0x3e8

    invoke-virtual {v3, v4}, LX/1nw;->j(I)LX/1nw;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {v3, p0, v9}, LX/1Di;->h(II)LX/1Di;

    move-result-object v3

    invoke-interface {v3, v7, v9}, LX/1Di;->h(II)LX/1Di;

    move-result-object v3

    const/4 v4, 0x3

    invoke-interface {v3, v4, p0}, LX/1Di;->h(II)LX/1Di;

    move-result-object v3

    const v4, 0x7f020aa7

    invoke-interface {v3, v4}, LX/1Di;->x(I)LX/1Di;

    move-result-object v3

    .line 1462407
    const v4, 0x7130770c

    const/4 v6, 0x0

    invoke-static {p1, v4, v6}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 1462408
    invoke-interface {v3, v4}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v3

    invoke-interface {v5, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    if-eqz v2, :cond_0

    const/4 v3, 0x0

    :goto_0
    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1462409
    return-object v0

    :cond_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v7}, LX/1Dh;->C(I)LX/1Dh;

    move-result-object v3

    const/16 v5, 0x8

    invoke-interface {v3, v5, v8}, LX/1Dh;->y(II)LX/1Dh;

    move-result-object v3

    const/4 v5, 0x2

    invoke-interface {v3, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v7}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v3

    const v5, 0x7f0e01b6

    invoke-static {p1, v8, v5}, LX/5Jt;->a(LX/1De;II)LX/5Jr;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    invoke-interface {v5, p2}, LX/1Di;->j(I)LX/1Di;

    move-result-object v5

    invoke-interface {v5, p2}, LX/1Di;->r(I)LX/1Di;

    move-result-object v5

    invoke-interface {v3, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1462387
    invoke-static {}, LX/1dS;->b()V

    .line 1462388
    iget v0, p1, LX/1dQ;->b:I

    .line 1462389
    packed-switch v0, :pswitch_data_0

    .line 1462390
    :goto_0
    return-object v2

    .line 1462391
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1462392
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1462393
    check-cast v1, LX/9I0;

    .line 1462394
    iget-object v3, p0, LX/9I1;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedback/ui/rows/views/specs/CommentPhotoAttachmentComponentSpec;

    iget-object p1, v1, LX/9I0;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget-boolean p2, v1, LX/9I0;->b:Z

    .line 1462395
    if-nez p2, :cond_0

    .line 1462396
    :goto_1
    goto :goto_0

    .line 1462397
    :cond_0
    iget-object p0, v3, Lcom/facebook/feedback/ui/rows/views/specs/CommentPhotoAttachmentComponentSpec;->a:LX/0Or;

    invoke-interface {p0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/26J;

    .line 1462398
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, v1, p1}, LX/26J;->b(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x7130770c
        :pswitch_0
    .end packed-switch
.end method
