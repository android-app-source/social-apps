.class public LX/9VW;
.super LX/0GT;
.source ""

# interfaces
.implements LX/0Ju;


# static fields
.field public static final b:[LX/9VF;


# instance fields
.field public final a:Ljava/lang/String;

.field private volatile c:J

.field private d:F

.field public e:LX/9VV;

.field private f:LX/9VB;

.field public g:LX/9VA;

.field private h:J

.field public i:LX/9VE;

.field public j:[LX/9VU;

.field public k:LX/0Kx;

.field public l:LX/9VH;

.field private m:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v6, 0x0

    const-wide/32 v4, 0xb71b0

    const-wide/32 v2, 0x3d090

    const/4 v8, 0x1

    .line 1499373
    const/4 v0, 0x4

    new-array v0, v0, [LX/9VF;

    new-instance v1, LX/9VS;

    invoke-direct/range {v1 .. v6}, LX/9VS;-><init>(JJI)V

    aput-object v1, v0, v6

    new-instance v1, LX/9VG;

    invoke-direct {v1, v2, v3, v8, v8}, LX/9VG;-><init>(JIZ)V

    aput-object v1, v0, v8

    const/4 v7, 0x2

    new-instance v1, LX/9VS;

    move v6, v8

    invoke-direct/range {v1 .. v6}, LX/9VS;-><init>(JJI)V

    aput-object v1, v0, v7

    const/4 v7, 0x3

    new-instance v1, LX/9VK;

    move v6, v8

    invoke-direct/range {v1 .. v6}, LX/9VK;-><init>(JJI)V

    aput-object v1, v0, v7

    sput-object v0, LX/9VW;->b:[LX/9VF;

    return-void
.end method

.method public constructor <init>(LX/0Kx;)V
    .locals 2

    .prologue
    .line 1499374
    invoke-direct {p0}, LX/0GT;-><init>()V

    .line 1499375
    const-class v0, LX/9VW;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/9VW;->a:Ljava/lang/String;

    .line 1499376
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/9VW;->c:J

    .line 1499377
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, LX/9VW;->d:F

    .line 1499378
    iput-object p1, p0, LX/9VW;->k:LX/0Kx;

    .line 1499379
    return-void
.end method

.method private m()V
    .locals 4

    .prologue
    .line 1499380
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/9VW;->m:Z

    .line 1499381
    iget-object v1, p0, LX/9VW;->j:[LX/9VU;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 1499382
    iget-object p0, v3, LX/9VU;->e:LX/0GT;

    check-cast p0, LX/9VL;

    invoke-interface {p0}, LX/9VL;->gf_()V

    .line 1499383
    iget-object p0, v3, LX/9VU;->f:LX/0GT;

    if-eqz p0, :cond_0

    .line 1499384
    iget-object p0, v3, LX/9VU;->f:LX/0GT;

    check-cast p0, LX/9VL;

    invoke-interface {p0}, LX/9VL;->gf_()V

    .line 1499385
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1499386
    :cond_1
    return-void
.end method

.method private n()V
    .locals 15

    .prologue
    const/4 v0, 0x0

    .line 1499387
    iget-object v1, p0, LX/9VW;->i:LX/9VE;

    invoke-virtual {v1}, LX/9VE;->c()V

    .line 1499388
    iget-object v1, p0, LX/9VW;->f:LX/9VB;

    if-nez v1, :cond_0

    iget v1, p0, LX/9VW;->d:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    .line 1499389
    new-instance v1, LX/9VB;

    iget-object v2, p0, LX/9VW;->g:LX/9VA;

    invoke-direct {v1, v2}, LX/9VB;-><init>(LX/9VA;)V

    iput-object v1, p0, LX/9VW;->f:LX/9VB;

    .line 1499390
    :cond_0
    const/16 v1, 0x4000

    invoke-static {v1}, Landroid/opengl/GLES20;->glClear(I)V

    .line 1499391
    iget-object v2, p0, LX/9VW;->j:[LX/9VU;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 1499392
    iget-object v5, v4, LX/9VU;->d:LX/9VD;

    .line 1499393
    iget-boolean v6, v5, LX/9VD;->e:Z

    move v5, v6

    .line 1499394
    if-eqz v5, :cond_1

    .line 1499395
    iget-object v4, v4, LX/9VU;->d:LX/9VD;

    .line 1499396
    iget-object v5, v4, LX/9VD;->c:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v5}, Landroid/graphics/SurfaceTexture;->updateTexImage()V

    .line 1499397
    iget-object v5, v4, LX/9VD;->c:Landroid/graphics/SurfaceTexture;

    iget-object v6, v4, LX/9VD;->f:[F

    invoke-virtual {v5, v6}, Landroid/graphics/SurfaceTexture;->getTransformMatrix([F)V

    .line 1499398
    const/4 v5, 0x0

    iput-boolean v5, v4, LX/9VD;->e:Z

    .line 1499399
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1499400
    :cond_2
    iget-object v1, p0, LX/9VW;->l:LX/9VH;

    iget-object v2, p0, LX/9VW;->j:[LX/9VU;

    iget-wide v4, p0, LX/9VW;->h:J

    invoke-virtual {v1, v2, v4, v5}, LX/9VH;->a([LX/9VU;J)V

    .line 1499401
    iget-object v7, p0, LX/9VW;->j:[LX/9VU;

    array-length v8, v7

    move v6, v0

    :goto_1
    if-ge v6, v8, :cond_3

    aget-object v5, v7, v6

    .line 1499402
    iget-object v0, p0, LX/9VW;->f:LX/9VB;

    iget-object v1, v5, LX/9VU;->d:LX/9VD;

    .line 1499403
    iget-object v2, v1, LX/9VD;->f:[F

    move-object v1, v2

    .line 1499404
    iget-object v2, v5, LX/9VU;->d:LX/9VD;

    .line 1499405
    iget v3, v2, LX/9VD;->d:I

    move v2, v3

    .line 1499406
    iget v3, v5, LX/9VU;->a:F

    iget v4, v5, LX/9VU;->b:F

    iget-boolean v5, v5, LX/9VU;->c:Z

    .line 1499407
    float-to-double v9, v4

    const-wide v11, 0x3f1a36e2eb1c432dL    # 1.0E-4

    cmpg-double v9, v9, v11

    if-gez v9, :cond_5

    .line 1499408
    :goto_2
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_1

    .line 1499409
    :cond_3
    iget-object v0, p0, LX/9VW;->i:LX/9VE;

    .line 1499410
    iget-object v1, v0, LX/9VC;->a:LX/9VA;

    .line 1499411
    iget-object v2, v1, LX/9VA;->f:Landroid/opengl/EGLDisplay;

    .line 1499412
    invoke-static {v0}, LX/9VE;->g(LX/9VE;)V

    .line 1499413
    iget-object v3, v0, LX/9VE;->c:Landroid/opengl/EGLSurface;

    move-object v3, v3

    .line 1499414
    invoke-static {v2, v3}, Landroid/opengl/EGL14;->eglSwapBuffers(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 1499415
    invoke-static {}, Landroid/opengl/EGL14;->eglGetError()I

    move-result v2

    .line 1499416
    :goto_3
    move v1, v2

    .line 1499417
    move v0, v1

    .line 1499418
    const/16 v1, 0x300e

    if-ne v0, v1, :cond_4

    .line 1499419
    iget-object v0, p0, LX/9VW;->a:Ljava/lang/String;

    const-string v1, "EGL_CONTEXT_LOST!"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1499420
    :cond_4
    return-void

    .line 1499421
    :cond_5
    const v9, 0x84c0

    invoke-static {v9}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 1499422
    const v9, 0x8d65

    invoke-static {v9, v2}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 1499423
    if-eqz v5, :cond_6

    const/4 v14, 0x1

    :goto_4
    move-object v9, v0

    move-object v10, v1

    move v11, v2

    move v12, v3

    move v13, v4

    invoke-static/range {v9 .. v14}, LX/9VB;->a(LX/9VB;[FIFFI)V

    goto :goto_2

    :cond_6
    const/4 v14, 0x0

    goto :goto_4

    :cond_7
    const/16 v2, 0x3000

    goto :goto_3
.end method


# virtual methods
.method public final X_()I
    .locals 1

    .prologue
    .line 1499424
    const/4 v0, 0x1

    return v0
.end method

.method public final a(I)LX/0L4;
    .locals 1

    .prologue
    .line 1499445
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(IIIF)V
    .locals 2

    .prologue
    .line 1499425
    int-to-float v0, p2

    int-to-float v1, p1

    div-float/2addr v0, v1

    iput v0, p0, LX/9VW;->d:F

    .line 1499426
    return-void
.end method

.method public final a(IJ)V
    .locals 0

    .prologue
    .line 1499427
    return-void
.end method

.method public final a(ILjava/lang/Object;)V
    .locals 9

    .prologue
    const-wide/32 v4, 0x7a120

    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 1499428
    packed-switch p1, :pswitch_data_0

    .line 1499429
    :cond_0
    :goto_0
    return-void

    .line 1499430
    :pswitch_0
    iget-boolean v0, p0, LX/9VW;->m:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, LX/9VW;->c:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1499431
    iget-wide v0, p0, LX/9VW;->h:J

    add-long/2addr v0, v4

    iput-wide v0, p0, LX/9VW;->c:J

    .line 1499432
    iget-object v0, p0, LX/9VW;->l:LX/9VH;

    const/4 v1, 0x2

    new-array v7, v1, [LX/9VF;

    new-instance v1, LX/9VT;

    iget-wide v2, p0, LX/9VW;->h:J

    invoke-direct/range {v1 .. v6}, LX/9VT;-><init>(JJI)V

    aput-object v1, v7, v6

    new-instance v1, LX/9VG;

    iget-wide v2, p0, LX/9VW;->h:J

    invoke-direct {v1, v2, v3, v6, v8}, LX/9VG;-><init>(JIZ)V

    aput-object v1, v7, v8

    .line 1499433
    iget-object v1, v0, LX/9VH;->c:Ljava/util/LinkedList;

    invoke-static {v7}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    .line 1499434
    goto :goto_0

    .line 1499435
    :pswitch_1
    check-cast p2, LX/9VV;

    iput-object p2, p0, LX/9VW;->e:LX/9VV;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(JJ)V
    .locals 5

    .prologue
    .line 1499436
    iput-wide p1, p0, LX/9VW;->h:J

    .line 1499437
    iget-boolean v0, p0, LX/9VW;->m:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, LX/9VW;->c:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, LX/9VW;->h:J

    iget-wide v2, p0, LX/9VW;->c:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 1499438
    invoke-direct {p0}, LX/9VW;->m()V

    .line 1499439
    :cond_0
    iget-boolean v0, p0, LX/9VW;->m:Z

    if-nez v0, :cond_1

    iget v0, p0, LX/9VW;->d:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 1499440
    :cond_1
    :goto_0
    return-void

    .line 1499441
    :cond_2
    :try_start_0
    invoke-direct {p0}, LX/9VW;->n()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1499442
    :catch_0
    move-exception v0

    .line 1499443
    iget-object v1, p0, LX/9VW;->a:Ljava/lang/String;

    const-string v2, "render"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1499444
    invoke-direct {p0}, LX/9VW;->m()V

    goto :goto_0
.end method

.method public final a(LX/0L3;)V
    .locals 0

    .prologue
    .line 1499371
    return-void
.end method

.method public final a(Landroid/media/MediaCodec$CryptoException;)V
    .locals 0

    .prologue
    .line 1499372
    return-void
.end method

.method public final a(Landroid/view/Surface;)V
    .locals 0

    .prologue
    .line 1499360
    return-void
.end method

.method public final a(Ljava/lang/String;JJ)V
    .locals 0

    .prologue
    .line 1499359
    return-void
.end method

.method public final a(J)Z
    .locals 5

    .prologue
    .line 1499330
    iget-object v0, p0, LX/9VW;->e:LX/9VV;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9VW;->j:[LX/9VU;

    if-nez v0, :cond_1

    .line 1499331
    :cond_0
    const/4 v0, 0x0

    .line 1499332
    :goto_0
    return v0

    .line 1499333
    :cond_1
    const/4 v4, 0x0

    .line 1499334
    iget-object v0, p0, LX/9VW;->e:LX/9VV;

    iget v1, v0, LX/9VV;->b:I

    .line 1499335
    iget-object v0, p0, LX/9VW;->e:LX/9VV;

    iget v2, v0, LX/9VV;->c:I

    .line 1499336
    iget-object v0, p0, LX/9VW;->e:LX/9VV;

    iget-object v0, v0, LX/9VV;->a:Landroid/view/SurfaceHolder;

    invoke-interface {v0}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    .line 1499337
    new-instance v3, LX/9VA;

    invoke-direct {v3}, LX/9VA;-><init>()V

    iput-object v3, p0, LX/9VW;->g:LX/9VA;

    .line 1499338
    invoke-static {v4, v4, v4, v4}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    .line 1499339
    const/4 v3, 0x0

    .line 1499340
    invoke-static {v3, v3, v1, v2}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 1499341
    iget-object v3, p0, LX/9VW;->g:LX/9VA;

    .line 1499342
    new-instance v4, LX/9VE;

    invoke-direct {v4, v3, v0}, LX/9VE;-><init>(LX/9VA;Landroid/view/Surface;)V

    .line 1499343
    iget-object p1, v3, LX/9VA;->h:Ljava/util/ArrayList;

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1499344
    move-object v0, v4

    .line 1499345
    iput-object v0, p0, LX/9VW;->i:LX/9VE;

    .line 1499346
    iget-object v0, p0, LX/9VW;->i:LX/9VE;

    invoke-virtual {v0}, LX/9VE;->c()V

    .line 1499347
    const/4 v0, 0x0

    :goto_1
    iget-object v3, p0, LX/9VW;->j:[LX/9VU;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 1499348
    iget-object v3, p0, LX/9VW;->j:[LX/9VU;

    aget-object v3, v3, v0

    iget-object v4, p0, LX/9VW;->g:LX/9VA;

    .line 1499349
    new-instance p1, LX/9VD;

    invoke-direct {p1, v4, v1, v2}, LX/9VD;-><init>(LX/9VA;II)V

    .line 1499350
    iget-object p2, v4, LX/9VA;->h:Ljava/util/ArrayList;

    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1499351
    move-object v4, p1

    .line 1499352
    iput-object v4, v3, LX/9VU;->d:LX/9VD;

    .line 1499353
    iget-object v3, p0, LX/9VW;->j:[LX/9VU;

    aget-object v3, v3, v0

    iget-object v3, v3, LX/9VU;->d:LX/9VD;

    .line 1499354
    iget-object v4, v3, LX/9VC;->b:Landroid/view/Surface;

    move-object v3, v4

    .line 1499355
    iget-object v4, p0, LX/9VW;->k:LX/0Kx;

    iget-object p1, p0, LX/9VW;->j:[LX/9VU;

    aget-object p1, p1, v0

    iget-object p1, p1, LX/9VU;->e:LX/0GT;

    const/4 p2, 0x1

    invoke-interface {v4, p1, p2, v3}, LX/0Kx;->a(LX/0GS;ILjava/lang/Object;)V

    .line 1499356
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1499357
    :cond_2
    new-instance v0, LX/9VH;

    sget-object v1, LX/9VW;->b:[LX/9VF;

    invoke-direct {v0, v1}, LX/9VH;-><init>([LX/9VF;)V

    iput-object v0, p0, LX/9VW;->l:LX/9VH;

    .line 1499358
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(J)V
    .locals 1

    .prologue
    .line 1499329
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1499328
    iget-boolean v0, p0, LX/9VW;->m:Z

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1499361
    const/4 v0, 0x1

    return v0
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 1499362
    return-void
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 1499363
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public final f()J
    .locals 2

    .prologue
    .line 1499364
    const-wide/16 v0, -0x3

    return-wide v0
.end method

.method public final h()V
    .locals 0

    .prologue
    .line 1499365
    return-void
.end method

.method public final q()V
    .locals 3

    .prologue
    .line 1499366
    :try_start_0
    iget-object v0, p0, LX/9VW;->g:LX/9VA;

    invoke-virtual {v0}, LX/9VA;->d()V

    .line 1499367
    const/4 v0, 0x0

    iput-object v0, p0, LX/9VW;->g:LX/9VA;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1499368
    :goto_0
    return-void

    .line 1499369
    :catch_0
    move-exception v0

    .line 1499370
    iget-object v1, p0, LX/9VW;->a:Ljava/lang/String;

    const-string v2, "shutGlContext"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
