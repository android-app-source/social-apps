.class public final LX/9XR;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1502846
    const-class v1, Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel;

    const v0, -0x3dc09d54

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FetchCTAAddableActionsQuery"

    const-string v6, "d7a6f9ebbcd25b0821ac5da9d7f7823d"

    const-string v7, "node"

    const-string v8, "10155261262166729"

    const-string v9, "10155263176686729"

    .line 1502847
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1502848
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1502849
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1502850
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1502851
    sparse-switch v0, :sswitch_data_0

    .line 1502852
    :goto_0
    return-object p1

    .line 1502853
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1502854
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1502855
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1502856
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x71e86c6d -> :sswitch_1
        -0x2fe52f35 -> :sswitch_0
        0x41ee846d -> :sswitch_3
        0x78326898 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1502857
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 1502858
    :goto_1
    return v0

    .line 1502859
    :pswitch_0
    const-string v2, "3"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 1502860
    :pswitch_1
    const-string v0, "undefined"

    invoke-static {p2, v0}, LX/0wE;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x33
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
