.class public LX/8sr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8sj;


# instance fields
.field private final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/8sj;",
            ">;"
        }
    .end annotation
.end field

.field private final b:J

.field private final c:Landroid/animation/Animator$AnimatorListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Px;JLandroid/animation/Animator$AnimatorListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/8sj;",
            ">;J",
            "Landroid/animation/Animator$AnimatorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1411856
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1411857
    iput-object p1, p0, LX/8sr;->a:LX/0Px;

    .line 1411858
    iput-wide p2, p0, LX/8sr;->b:J

    .line 1411859
    iput-object p4, p0, LX/8sr;->c:Landroid/animation/Animator$AnimatorListener;

    .line 1411860
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 1411861
    iget-wide v0, p0, LX/8sr;->b:J

    return-wide v0
.end method

.method public final a(F)Landroid/animation/Animator;
    .locals 6

    .prologue
    .line 1411862
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 1411863
    new-instance v3, Ljava/util/ArrayList;

    iget-object v0, p0, LX/8sr;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1411864
    iget-object v0, p0, LX/8sr;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    iget-object v0, p0, LX/8sr;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8sj;

    .line 1411865
    invoke-interface {v0, p1}, LX/8sj;->a(F)Landroid/animation/Animator;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1411866
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1411867
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr v0, p1

    iget-wide v4, p0, LX/8sr;->b:J

    long-to-float v1, v4

    mul-float/2addr v0, v1

    float-to-long v0, v0

    invoke-virtual {v2, v0, v1}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 1411868
    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 1411869
    iget-object v0, p0, LX/8sr;->c:Landroid/animation/Animator$AnimatorListener;

    if-eqz v0, :cond_1

    .line 1411870
    iget-object v0, p0, LX/8sr;->c:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1411871
    :cond_1
    return-object v2
.end method
