.class public final LX/8wy;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/8wz;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:LX/1dQ;

.field public final synthetic d:LX/8wz;


# direct methods
.method public constructor <init>(LX/8wz;)V
    .locals 1

    .prologue
    .line 1423310
    iput-object p1, p0, LX/8wy;->d:LX/8wz;

    .line 1423311
    move-object v0, p1

    .line 1423312
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1423313
    const/4 v0, 0x0

    iput v0, p0, LX/8wy;->b:I

    .line 1423314
    return-void
.end method

.method public synthetic constructor <init>(LX/8wz;B)V
    .locals 0

    .prologue
    .line 1423315
    invoke-direct {p0, p1}, LX/8wy;-><init>(LX/8wz;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1423316
    const-string v0, "PostPostBadgeComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1423317
    if-ne p0, p1, :cond_1

    .line 1423318
    :cond_0
    :goto_0
    return v0

    .line 1423319
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1423320
    goto :goto_0

    .line 1423321
    :cond_3
    check-cast p1, LX/8wy;

    .line 1423322
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1423323
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1423324
    if-eq v2, v3, :cond_0

    .line 1423325
    iget v2, p0, LX/8wy;->a:I

    iget v3, p1, LX/8wy;->a:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1423326
    goto :goto_0

    .line 1423327
    :cond_4
    iget v2, p0, LX/8wy;->b:I

    iget v3, p1, LX/8wy;->b:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1423328
    goto :goto_0
.end method
