.class public final LX/8lc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8lZ;


# instance fields
.field public final synthetic a:LX/8li;

.field public final synthetic b:LX/8ld;


# direct methods
.method public constructor <init>(LX/8ld;LX/8li;)V
    .locals 0

    .prologue
    .line 1397858
    iput-object p1, p0, LX/8lc;->b:LX/8ld;

    iput-object p2, p0, LX/8lc;->a:LX/8li;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/stickers/model/Sticker;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1397853
    sget-object v0, LX/8lR;->a:[I

    iget-object v1, p0, LX/8lc;->b:LX/8ld;

    iget-object v1, v1, LX/8ld;->b:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-object v1, v1, Lcom/facebook/stickers/search/StickerSearchContainer;->L:LX/8lg;

    invoke-virtual {v1}, LX/8lg;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1397854
    iget-object v0, p0, LX/8lc;->b:LX/8ld;

    iget-object v0, v0, LX/8ld;->b:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-object v0, v0, Lcom/facebook/stickers/search/StickerSearchContainer;->t:LX/8j9;

    iget-object v1, p0, LX/8lc;->a:LX/8li;

    iget-object v1, v1, LX/8li;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/8j9;->c(Ljava/lang/String;)V

    .line 1397855
    :goto_0
    return-void

    .line 1397856
    :pswitch_0
    iget-object v0, p0, LX/8lc;->b:LX/8ld;

    iget-object v0, v0, LX/8ld;->b:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-object v0, v0, Lcom/facebook/stickers/search/StickerSearchContainer;->F:Lcom/facebook/stickers/ui/StickerGridViewAdapter;

    invoke-virtual {v0, p1}, Lcom/facebook/stickers/ui/StickerGridViewAdapter;->a(LX/0Px;)V

    .line 1397857
    iget-object v0, p0, LX/8lc;->b:LX/8ld;

    iget-object v0, v0, LX/8ld;->b:Lcom/facebook/stickers/search/StickerSearchContainer;

    sget-object v1, LX/8lg;->SEARCH_FINISHED_WITH_RESULTS:LX/8lg;

    invoke-static {v0, v1}, Lcom/facebook/stickers/search/StickerSearchContainer;->setCurrentState(Lcom/facebook/stickers/search/StickerSearchContainer;LX/8lg;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
    .end packed-switch
.end method
