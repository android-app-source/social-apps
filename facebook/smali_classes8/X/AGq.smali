.class public LX/AGq;
.super LX/AGp;
.source ""


# instance fields
.field private final c:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel;


# direct methods
.method public constructor <init>(Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel;Lcom/facebook/audience/model/AudienceControlData;Lcom/facebook/audience/model/AudienceControlData;)V
    .locals 0

    .prologue
    .line 1653227
    invoke-direct {p0, p2, p3}, LX/AGp;-><init>(Lcom/facebook/audience/model/AudienceControlData;Lcom/facebook/audience/model/AudienceControlData;)V

    .line 1653228
    iput-object p1, p0, LX/AGq;->c:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel;

    .line 1653229
    return-void
.end method

.method private a(Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$LastPostsModel$LastPostsEdgesModel;)Z
    .locals 2

    .prologue
    .line 1653226
    invoke-virtual {p1}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$LastPostsModel$LastPostsEdgesModel;->a()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;->k()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->l()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel$OwnerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel$OwnerModel;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/AGp;->b:Lcom/facebook/audience/model/AudienceControlData;

    invoke-virtual {v1}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private m()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$LastPostsModel$LastPostsEdgesModel;
    .locals 2

    .prologue
    .line 1653225
    iget-object v0, p0, LX/AGq;->c:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel;->a()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel;->l()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$LastPostsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$LastPostsModel;->j()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$LastPostsModel$LastPostsEdgesModel;

    return-object v0
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1653224
    iget-object v0, p0, LX/AGq;->c:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel;->a()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel;->j()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$FirstPostModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$FirstPostModel;->a()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$FirstPostModel$FirstPostEdgesModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$FirstPostModel$FirstPostEdgesModel;->a()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/facebook/audience/model/AudienceControlData;
    .locals 1

    .prologue
    .line 1653223
    iget-object v0, p0, LX/AGp;->a:Lcom/facebook/audience/model/AudienceControlData;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1653222
    iget-object v0, p0, LX/AGq;->c:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel;->a()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel;->m()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()LX/7gz;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1653207
    invoke-direct {p0}, LX/AGq;->m()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$LastPostsModel$LastPostsEdgesModel;

    move-result-object v0

    .line 1653208
    invoke-direct {p0, v0}, LX/AGq;->a(Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$LastPostsModel$LastPostsEdgesModel;)Z

    move-result v1

    .line 1653209
    const/4 v3, 0x0

    .line 1653210
    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$LastPostsModel$LastPostsEdgesModel;->a()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;->l()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_3

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel;

    .line 1653211
    iget-object v7, p0, LX/AGp;->a:Lcom/facebook/audience/model/AudienceControlData;

    invoke-virtual {v7}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel;->a()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1653212
    const/4 v2, 0x1

    .line 1653213
    :goto_1
    move v0, v2

    .line 1653214
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 1653215
    sget-object v0, LX/7gz;->SEEN:LX/7gz;

    .line 1653216
    :goto_2
    return-object v0

    .line 1653217
    :cond_0
    if-eqz v1, :cond_1

    if-nez v0, :cond_1

    .line 1653218
    sget-object v0, LX/7gz;->SENT:LX/7gz;

    goto :goto_2

    .line 1653219
    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    .line 1653220
    :cond_2
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_0

    :cond_3
    move v2, v3

    .line 1653221
    goto :goto_1
.end method

.method public final f()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 1653187
    iget-object v0, p0, LX/AGq;->c:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel;->a()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel;->j()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$FirstPostModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$FirstPostModel;->a()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$FirstPostModel$FirstPostEdgesModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$FirstPostModel$FirstPostEdgesModel;->a()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;->k()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->m()Ljava/lang/String;

    move-result-object v0

    .line 1653188
    if-eqz v0, :cond_0

    .line 1653189
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1653190
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Landroid/net/Uri;
    .locals 3

    .prologue
    .line 1653206
    iget-object v0, p0, LX/AGq;->c:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel;->a()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel;->j()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$FirstPostModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$FirstPostModel;->a()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$FirstPostModel$FirstPostEdgesModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$FirstPostModel$FirstPostEdgesModel;->a()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;->k()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final h()Landroid/net/Uri;
    .locals 3

    .prologue
    .line 1653205
    iget-object v0, p0, LX/AGq;->c:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel;->a()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel;->j()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$FirstPostModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$FirstPostModel;->a()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$FirstPostModel$FirstPostEdgesModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$FirstPostModel$FirstPostEdgesModel;->a()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;->k()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->k()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final i()J
    .locals 4

    .prologue
    .line 1653204
    invoke-direct {p0}, LX/AGq;->m()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$LastPostsModel$LastPostsEdgesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$LastPostsModel$LastPostsEdgesModel;->a()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;->k()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public final j()LX/7gy;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1653193
    iget-object v1, p0, LX/AGq;->c:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel;

    invoke-virtual {v1}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel;->a()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel;->l()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$LastPostsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$LastPostsModel;->a()I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 1653194
    :goto_0
    invoke-direct {p0}, LX/AGq;->m()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$LastPostsModel$LastPostsEdgesModel;

    move-result-object v1

    invoke-direct {p0, v1}, LX/AGq;->a(Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$LastPostsModel$LastPostsEdgesModel;)Z

    move-result v1

    .line 1653195
    if-eqz v0, :cond_2

    .line 1653196
    if-eqz v1, :cond_1

    .line 1653197
    sget-object v0, LX/7gy;->OUTGOING_STORY:LX/7gy;

    .line 1653198
    :goto_1
    return-object v0

    .line 1653199
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1653200
    :cond_1
    sget-object v0, LX/7gy;->INCOMING_STORY:LX/7gy;

    goto :goto_1

    .line 1653201
    :cond_2
    if-eqz v1, :cond_3

    .line 1653202
    sget-object v0, LX/7gy;->OUTOING_REPLY:LX/7gy;

    goto :goto_1

    .line 1653203
    :cond_3
    sget-object v0, LX/7gy;->INCOMING_REPLY:LX/7gy;

    goto :goto_1
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1653192
    iget-object v0, p0, LX/AGq;->c:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel;->a()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 1653191
    invoke-super {p0}, LX/AGp;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AGq;->c:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel;->a()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel;->l()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$LastPostsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$LastPostsModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
