.class public LX/9Iv;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/images/CSFBFeedNetworkImageSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/9Iv",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/images/CSFBFeedNetworkImageSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1463842
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1463843
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/9Iv;->b:LX/0Zi;

    .line 1463844
    iput-object p1, p0, LX/9Iv;->a:LX/0Ot;

    .line 1463845
    return-void
.end method

.method public static a(LX/0QB;)LX/9Iv;
    .locals 4

    .prologue
    .line 1463821
    const-class v1, LX/9Iv;

    monitor-enter v1

    .line 1463822
    :try_start_0
    sget-object v0, LX/9Iv;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1463823
    sput-object v2, LX/9Iv;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1463824
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1463825
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1463826
    new-instance v3, LX/9Iv;

    const/16 p0, 0x1fed

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/9Iv;-><init>(LX/0Ot;)V

    .line 1463827
    move-object v0, v3

    .line 1463828
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1463829
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/9Iv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1463830
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1463831
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 1463834
    check-cast p2, LX/9Iu;

    .line 1463835
    iget-object v0, p0, LX/9Iv;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/images/CSFBFeedNetworkImageSpec;

    iget-object v1, p2, LX/9Iu;->a:Lcom/facebook/java2js/JSValue;

    .line 1463836
    const-string v2, "url"

    invoke-virtual {v1, v2}, Lcom/facebook/java2js/JSValue;->getStringProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1463837
    const-string v3, "focus"

    invoke-virtual {v1, v3}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v3

    .line 1463838
    new-instance v4, Landroid/graphics/PointF;

    const-string v5, "x"

    invoke-virtual {v3, v5}, Lcom/facebook/java2js/JSValue;->getNumberProperty(Ljava/lang/String;)D

    move-result-wide v6

    double-to-float v5, v6

    const-string v6, "y"

    invoke-virtual {v3, v6}, Lcom/facebook/java2js/JSValue;->getNumberProperty(Ljava/lang/String;)D

    move-result-wide v6

    double-to-float v3, v6

    invoke-direct {v4, v5, v3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1463839
    const-string v3, "aspectRatio"

    invoke-virtual {v1, v3}, Lcom/facebook/java2js/JSValue;->getNumberProperty(Ljava/lang/String;)D

    move-result-wide v6

    .line 1463840
    iget-object v3, v0, Lcom/facebook/feedplugins/images/CSFBFeedNetworkImageSpec;->b:LX/1nu;

    invoke-virtual {v3, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/1nw;->a(Landroid/graphics/PointF;)LX/1nw;

    move-result-object v3

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v2

    sget-object v3, Lcom/facebook/feedplugins/images/CSFBFeedNetworkImageSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v3

    const-string v2, "prefetchInfo"

    invoke-virtual {v1, v2}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v2

    const-class v4, LX/1f9;

    invoke-virtual {v2, v4}, Lcom/facebook/java2js/JSValue;->to(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1f9;

    invoke-virtual {v3, v2}, LX/1nw;->a(LX/1f9;)LX/1nw;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/1nw;->a(Z)LX/1nw;

    move-result-object v3

    invoke-static {v6, v7}, Ljava/lang/Double;->isNaN(D)Z

    move-result v2

    if-nez v2, :cond_0

    double-to-float v2, v6

    :goto_0
    invoke-virtual {v3, v2}, LX/1nw;->c(F)LX/1nw;

    move-result-object v3

    const-string v2, "toolbox"

    invoke-virtual {v1, v2}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/java2js/JSValue;->getJavaObject()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Pp;

    invoke-virtual {v3, v2}, LX/1nw;->a(LX/1Pp;)LX/1nw;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const v3, -0x777778

    invoke-interface {v2, v3}, LX/1Di;->y(I)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 1463841
    return-object v0

    :cond_0
    const/high16 v2, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1463832
    invoke-static {}, LX/1dS;->b()V

    .line 1463833
    const/4 v0, 0x0

    return-object v0
.end method
