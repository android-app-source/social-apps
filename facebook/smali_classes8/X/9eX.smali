.class public final LX/9eX;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/9ec;


# direct methods
.method public constructor <init>(LX/9ec;)V
    .locals 0

    .prologue
    .line 1519747
    iput-object p1, p0, LX/9eX;->a:LX/9ec;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/RectF;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1519748
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1519749
    if-nez p2, :cond_0

    iget-object v0, p0, LX/9eX;->a:LX/9ec;

    iget-boolean v0, v0, LX/9ec;->D:Z

    if-eqz v0, :cond_1

    .line 1519750
    :cond_0
    iget-object v0, p0, LX/9eX;->a:LX/9ec;

    iget-object v0, v0, LX/9ec;->z:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1519751
    if-nez p1, :cond_3

    .line 1519752
    :cond_1
    :goto_0
    iget-object v0, p0, LX/9eX;->a:LX/9ec;

    .line 1519753
    iput-object p1, v0, LX/9ec;->x:Landroid/graphics/RectF;

    .line 1519754
    if-eqz p2, :cond_2

    .line 1519755
    iget-object v0, p0, LX/9eX;->a:LX/9ec;

    .line 1519756
    iput-boolean v1, v0, LX/9ec;->G:Z

    .line 1519757
    iget-object v0, p0, LX/9eX;->a:LX/9ec;

    .line 1519758
    iput-boolean v1, v0, LX/9ec;->I:Z

    .line 1519759
    iget-object v0, p0, LX/9eX;->a:LX/9ec;

    const/4 v1, 0x0

    .line 1519760
    iput-boolean v1, v0, LX/9ec;->w:Z

    .line 1519761
    iget-object v0, p0, LX/9eX;->a:LX/9ec;

    iget-object v1, p0, LX/9eX;->a:LX/9ec;

    iget-object v1, v1, LX/9ec;->y:LX/9bx;

    .line 1519762
    iget-object v2, v1, LX/9bx;->E:Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;

    move-object v1, v2

    .line 1519763
    iput-object v1, v0, LX/9ec;->N:Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;

    .line 1519764
    :cond_2
    iget-object v0, p0, LX/9eX;->a:LX/9ec;

    invoke-static {v0}, LX/9ec;->s(LX/9ec;)V

    .line 1519765
    return-void

    .line 1519766
    :cond_3
    iget v2, p1, Landroid/graphics/RectF;->left:F

    invoke-virtual {v0}, Landroid/view/View;->getPaddingLeft()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iput v2, p1, Landroid/graphics/RectF;->left:F

    .line 1519767
    iget v2, p1, Landroid/graphics/RectF;->top:F

    invoke-virtual {v0}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iput v2, p1, Landroid/graphics/RectF;->top:F

    .line 1519768
    iget v2, p1, Landroid/graphics/RectF;->right:F

    invoke-virtual {v0}, Landroid/view/View;->getPaddingLeft()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iput v2, p1, Landroid/graphics/RectF;->right:F

    .line 1519769
    iget v2, p1, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v0}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iput v2, p1, Landroid/graphics/RectF;->bottom:F

    goto :goto_0
.end method
