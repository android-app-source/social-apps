.class public LX/9Di;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0g1;
.implements LX/21l;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0g1",
        "<",
        "LX/9Gk;",
        ">;",
        "LX/21l",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        ">;>;"
    }
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/9Gk;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1456056
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1456057
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/9Di;->a:Ljava/util/List;

    .line 1456058
    iput-object p1, p0, LX/9Di;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1456059
    return-void
.end method


# virtual methods
.method public final a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1456062
    iget-object v0, p0, LX/9Di;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Gk;

    return-object v0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v5, -0x1

    .line 1456063
    iget-object v0, p0, LX/9Di;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1456064
    iget-object v0, p0, LX/9Di;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v0, :cond_0

    .line 1456065
    iget-object v0, p0, LX/9Di;->a:Ljava/util/List;

    new-instance v1, LX/9Gp;

    iget-object v3, p0, LX/9Di;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {v1, v3}, LX/9Gp;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1456066
    :cond_0
    iput-object p1, p0, LX/9Di;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1456067
    if-nez p1, :cond_2

    .line 1456068
    :cond_1
    :goto_0
    return-void

    .line 1456069
    :cond_2
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1456070
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v0}, LX/21y;->getOrder(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/21y;

    move-result-object v7

    .line 1456071
    sget-object v0, LX/21y;->RANKED_ORDER:LX/21y;

    invoke-virtual {v0, v7}, LX/21y;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v3, LX/5Gs;->LOAD_BEFORE:LX/5Gs;

    .line 1456072
    :goto_1
    sget-object v0, LX/5Gs;->LOAD_BEFORE:LX/5Gs;

    if-ne v3, v0, :cond_6

    sget-object v0, LX/5Gs;->LOAD_AFTER:LX/5Gs;

    move-object v6, v0

    .line 1456073
    :goto_2
    new-instance v0, LX/9Gl;

    .line 1456074
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1456075
    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedback;

    sget-object v4, LX/9Fi;->TOP_LEVEL:LX/9Fi;

    invoke-direct/range {v0 .. v5}, LX/9Gl;-><init>(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;LX/5Gs;LX/9Fi;I)V

    .line 1456076
    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/LoadMoreCommentsComponentPartDefinition;->a(LX/9Gl;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1456077
    iget-object v1, p0, LX/9Di;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1456078
    :cond_3
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1456079
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v0}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v1

    .line 1456080
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1456081
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v0}, LX/21y;->isReverseOrder(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1456082
    invoke-virtual {v1}, LX/0Px;->reverse()LX/0Px;

    move-result-object v0

    move-object v1, v0

    .line 1456083
    :cond_4
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v4

    .line 1456084
    const/4 v0, 0x0

    move v3, v0

    :goto_3
    if-ge v3, v4, :cond_7

    .line 1456085
    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1456086
    iget-object v8, p0, LX/9Di;->a:Ljava/util/List;

    new-instance v9, LX/9Gv;

    invoke-virtual {p1, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-direct {v9, v0, v7}, LX/9Gv;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/21y;)V

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1456087
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    .line 1456088
    :cond_5
    sget-object v3, LX/5Gs;->LOAD_AFTER:LX/5Gs;

    goto :goto_1

    .line 1456089
    :cond_6
    sget-object v0, LX/5Gs;->LOAD_BEFORE:LX/5Gs;

    move-object v6, v0

    goto :goto_2

    .line 1456090
    :cond_7
    new-instance v0, LX/9Gl;

    .line 1456091
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1456092
    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedback;

    sget-object v4, LX/9Fi;->TOP_LEVEL:LX/9Fi;

    move-object v3, v6

    invoke-direct/range {v0 .. v5}, LX/9Gl;-><init>(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;LX/5Gs;LX/9Fi;I)V

    .line 1456093
    invoke-static {v0}, Lcom/facebook/feedback/ui/rows/LoadMoreCommentsComponentPartDefinition;->a(LX/9Gl;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1456094
    iget-object v1, p0, LX/9Di;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1456061
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {p0, p1}, LX/9Di;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    return-void
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1456060
    iget-object v0, p0, LX/9Di;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
