.class public final LX/9CS;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Landroid/app/ProgressDialog;

.field public final synthetic b:J

.field public final synthetic c:Landroid/content/Context;

.field public final synthetic d:Lcom/facebook/graphql/model/GraphQLComment;

.field public final synthetic e:LX/9Cd;


# direct methods
.method public constructor <init>(LX/9Cd;Landroid/app/ProgressDialog;JLandroid/content/Context;Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 1

    .prologue
    .line 1454115
    iput-object p1, p0, LX/9CS;->e:LX/9Cd;

    iput-object p2, p0, LX/9CS;->a:Landroid/app/ProgressDialog;

    iput-wide p3, p0, LX/9CS;->b:J

    iput-object p5, p0, LX/9CS;->c:Landroid/content/Context;

    iput-object p6, p0, LX/9CS;->d:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 6

    .prologue
    .line 1454126
    iget-object v0, p0, LX/9CS;->e:LX/9Cd;

    iget-object v0, v0, LX/9Cd;->d:LX/3iL;

    invoke-virtual {v0, p1}, LX/3iL;->a(Lcom/facebook/fbservice/service/ServiceException;)V

    .line 1454127
    iget-object v0, p0, LX/9CS;->a:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1454128
    iget-object v0, p0, LX/9CS;->e:LX/9Cd;

    iget-object v0, v0, LX/9Cd;->e:LX/0hx;

    iget-object v1, p0, LX/9CS;->e:LX/9Cd;

    iget-object v1, v1, LX/9Cd;->f:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, LX/9CS;->b:J

    sub-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, LX/0hx;->a(J)V

    .line 1454129
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1454116
    iget-object v0, p0, LX/9CS;->a:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1454117
    iget-object v0, p0, LX/9CS;->e:LX/9Cd;

    iget-object v0, v0, LX/9Cd;->e:LX/0hx;

    iget-object v1, p0, LX/9CS;->e:LX/9Cd;

    iget-object v1, v1, LX/9Cd;->f:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, LX/9CS;->b:J

    sub-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, LX/0hx;->a(J)V

    .line 1454118
    iget-object v0, p0, LX/9CS;->c:Landroid/content/Context;

    iget-object v1, p0, LX/9CS;->c:Landroid/content/Context;

    const v2, 0x7f080fd8

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, LX/9CS;->d:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kL;->a(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 1454119
    iget-object v0, p0, LX/9CS;->e:LX/9Cd;

    iget-object v0, v0, LX/9Cd;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1454120
    iget-boolean v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v0, v1

    .line 1454121
    if-eqz v0, :cond_0

    .line 1454122
    iget-object v0, p0, LX/9CS;->e:LX/9Cd;

    iget-object v0, v0, LX/9Cd;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Wc;

    iget-object v1, p0, LX/9CS;->e:LX/9Cd;

    iget-object v1, v1, LX/9Cd;->s:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1454123
    iget-object v2, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1454124
    sget-object v2, LX/9Wd;->COMMENTS:LX/9Wd;

    invoke-virtual {v0, v1, v2}, LX/9Wc;->a(Ljava/lang/String;LX/9Wd;)V

    .line 1454125
    :cond_0
    return-void
.end method
