.class public final LX/9rI;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 17

    .prologue
    .line 1552455
    const/4 v13, 0x0

    .line 1552456
    const/4 v12, 0x0

    .line 1552457
    const/4 v11, 0x0

    .line 1552458
    const/4 v10, 0x0

    .line 1552459
    const/4 v9, 0x0

    .line 1552460
    const/4 v8, 0x0

    .line 1552461
    const/4 v7, 0x0

    .line 1552462
    const/4 v6, 0x0

    .line 1552463
    const/4 v5, 0x0

    .line 1552464
    const/4 v4, 0x0

    .line 1552465
    const/4 v3, 0x0

    .line 1552466
    const/4 v2, 0x0

    .line 1552467
    const/4 v1, 0x0

    .line 1552468
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->START_OBJECT:LX/15z;

    if-eq v14, v15, :cond_1

    .line 1552469
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1552470
    const/4 v1, 0x0

    .line 1552471
    :goto_0
    return v1

    .line 1552472
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1552473
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->END_OBJECT:LX/15z;

    if-eq v14, v15, :cond_e

    .line 1552474
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v14

    .line 1552475
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1552476
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v15

    sget-object v16, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_1

    if-eqz v14, :cond_1

    .line 1552477
    const-string v15, "__type__"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_2

    const-string v15, "__typename"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 1552478
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v13

    goto :goto_1

    .line 1552479
    :cond_3
    const-string v15, "collapse_state"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 1552480
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v12

    goto :goto_1

    .line 1552481
    :cond_4
    const-string v15, "id"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 1552482
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto :goto_1

    .line 1552483
    :cond_5
    const-string v15, "impression_info"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 1552484
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 1552485
    :cond_6
    const-string v15, "page"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 1552486
    invoke-static/range {p0 .. p1}, LX/5tO;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 1552487
    :cond_7
    const-string v15, "reaction_attachments"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 1552488
    invoke-static/range {p0 .. p1}, LX/9rE;->a(LX/15w;LX/186;)I

    move-result v8

    goto/16 :goto_1

    .line 1552489
    :cond_8
    const-string v15, "reaction_unit_header"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_9

    .line 1552490
    invoke-static/range {p0 .. p1}, LX/9rL;->a(LX/15w;LX/186;)I

    move-result v7

    goto/16 :goto_1

    .line 1552491
    :cond_9
    const-string v15, "unit_score"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_a

    .line 1552492
    const/4 v1, 0x1

    .line 1552493
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    goto/16 :goto_1

    .line 1552494
    :cond_a
    const-string v15, "unit_style"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_b

    .line 1552495
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    goto/16 :goto_1

    .line 1552496
    :cond_b
    const-string v15, "unit_type_token"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_c

    .line 1552497
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_1

    .line 1552498
    :cond_c
    const-string v15, "welcome_note_message"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_d

    .line 1552499
    invoke-static/range {p0 .. p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 1552500
    :cond_d
    const-string v15, "welcome_note_photo"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 1552501
    invoke-static/range {p0 .. p1}, LX/5lp;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 1552502
    :cond_e
    const/16 v14, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->c(I)V

    .line 1552503
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 1552504
    const/4 v13, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 1552505
    const/4 v12, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 1552506
    const/4 v11, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 1552507
    const/4 v10, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 1552508
    const/4 v9, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 1552509
    const/4 v8, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v7}, LX/186;->b(II)V

    .line 1552510
    if-eqz v1, :cond_f

    .line 1552511
    const/4 v1, 0x7

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6, v7}, LX/186;->a(III)V

    .line 1552512
    :cond_f
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1552513
    const/16 v1, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1552514
    const/16 v1, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1552515
    const/16 v1, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1552516
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1552517
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1552518
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1552519
    if-eqz v0, :cond_0

    .line 1552520
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1552521
    invoke-static {p0, p1, v2, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1552522
    :cond_0
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1552523
    if-eqz v0, :cond_1

    .line 1552524
    const-string v0, "collapse_state"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1552525
    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1552526
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1552527
    if-eqz v0, :cond_2

    .line 1552528
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1552529
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1552530
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1552531
    if-eqz v0, :cond_3

    .line 1552532
    const-string v1, "impression_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1552533
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1552534
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1552535
    if-eqz v0, :cond_4

    .line 1552536
    const-string v1, "page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1552537
    invoke-static {p0, v0, p2, p3}, LX/5tO;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1552538
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1552539
    if-eqz v0, :cond_5

    .line 1552540
    const-string v1, "reaction_attachments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1552541
    invoke-static {p0, v0, p2, p3}, LX/9rE;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1552542
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1552543
    if-eqz v0, :cond_6

    .line 1552544
    const-string v1, "reaction_unit_header"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1552545
    invoke-static {p0, v0, p2, p3}, LX/9rL;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1552546
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1552547
    if-eqz v0, :cond_7

    .line 1552548
    const-string v1, "unit_score"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1552549
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1552550
    :cond_7
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1552551
    if-eqz v0, :cond_8

    .line 1552552
    const-string v0, "unit_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1552553
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1552554
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1552555
    if-eqz v0, :cond_9

    .line 1552556
    const-string v1, "unit_type_token"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1552557
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1552558
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1552559
    if-eqz v0, :cond_a

    .line 1552560
    const-string v1, "welcome_note_message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1552561
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 1552562
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1552563
    if-eqz v0, :cond_b

    .line 1552564
    const-string v1, "welcome_note_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1552565
    invoke-static {p0, v0, p2, p3}, LX/5lp;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1552566
    :cond_b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1552567
    return-void
.end method
