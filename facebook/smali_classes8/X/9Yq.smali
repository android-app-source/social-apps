.class public final LX/9Yq;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1506980
    const-class v1, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    const v0, 0x38b6a06

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FetchPageHeaderQuery"

    const-string v6, "bd305f3228d9b61642170c379bb7a212"

    const-string v7, "node"

    const-string v8, "10155261262226729"

    const-string v9, "10155263176701729"

    .line 1506981
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1506982
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1506983
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1506984
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1506985
    sparse-switch v0, :sswitch_data_0

    .line 1506986
    :goto_0
    return-object p1

    .line 1506987
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1506988
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1506989
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1506990
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1506991
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1506992
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 1506993
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 1506994
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 1506995
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 1506996
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 1506997
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 1506998
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 1506999
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 1507000
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 1507001
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 1507002
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    .line 1507003
    :sswitch_10
    const-string p1, "16"

    goto :goto_0

    .line 1507004
    :sswitch_11
    const-string p1, "17"

    goto :goto_0

    .line 1507005
    :sswitch_12
    const-string p1, "18"

    goto :goto_0

    .line 1507006
    :sswitch_13
    const-string p1, "19"

    goto :goto_0

    .line 1507007
    :sswitch_14
    const-string p1, "20"

    goto :goto_0

    .line 1507008
    :sswitch_15
    const-string p1, "21"

    goto :goto_0

    .line 1507009
    :sswitch_16
    const-string p1, "22"

    goto :goto_0

    .line 1507010
    :sswitch_17
    const-string p1, "23"

    goto :goto_0

    .line 1507011
    :sswitch_18
    const-string p1, "24"

    goto :goto_0

    .line 1507012
    :sswitch_19
    const-string p1, "25"

    goto :goto_0

    .line 1507013
    :sswitch_1a
    const-string p1, "26"

    goto :goto_0

    .line 1507014
    :sswitch_1b
    const-string p1, "27"

    goto :goto_0

    .line 1507015
    :sswitch_1c
    const-string p1, "28"

    goto :goto_0

    .line 1507016
    :sswitch_1d
    const-string p1, "29"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x73ba2555 -> :sswitch_11
        -0x71e86c6d -> :sswitch_1c
        -0x6a24640d -> :sswitch_15
        -0x680de62a -> :sswitch_9
        -0x65578195 -> :sswitch_2
        -0x6326fdb3 -> :sswitch_8
        -0x587e3090 -> :sswitch_5
        -0x50d75ef1 -> :sswitch_17
        -0x4651b691 -> :sswitch_d
        -0x4496acc9 -> :sswitch_a
        -0x41a91745 -> :sswitch_10
        -0x2fe52f35 -> :sswitch_f
        -0x2c456fff -> :sswitch_1a
        -0x1b87b280 -> :sswitch_7
        -0x17657c0a -> :sswitch_1b
        -0x12efdeb3 -> :sswitch_b
        -0x12da6d78 -> :sswitch_19
        -0xb9a91f6 -> :sswitch_4
        0x1312dd1 -> :sswitch_6
        0x683094a -> :sswitch_13
        0x86feefb -> :sswitch_18
        0xa1fa812 -> :sswitch_0
        0x214100e0 -> :sswitch_c
        0x22f8cce8 -> :sswitch_14
        0x291d8de0 -> :sswitch_12
        0x3052e0ff -> :sswitch_1
        0x5d1e9cc7 -> :sswitch_3
        0x73431f35 -> :sswitch_16
        0x73a026b5 -> :sswitch_e
        0x78326898 -> :sswitch_1d
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1507017
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 1507018
    :goto_1
    return v0

    .line 1507019
    :sswitch_0
    const-string v2, "24"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :sswitch_1
    const-string v2, "27"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "1"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_3
    const-string v2, "23"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    :sswitch_4
    const-string v2, "26"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x4

    goto :goto_0

    :sswitch_5
    const-string v2, "25"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x5

    goto :goto_0

    :sswitch_6
    const-string v2, "22"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x6

    goto :goto_0

    :sswitch_7
    const-string v2, "20"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x7

    goto :goto_0

    .line 1507020
    :pswitch_0
    const-string v0, "undefined"

    invoke-static {p2, v0}, LX/0wE;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 1507021
    :pswitch_1
    const-string v0, "undefined"

    invoke-static {p2, v0}, LX/0wE;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 1507022
    :pswitch_2
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1507023
    :pswitch_3
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1507024
    :pswitch_4
    const-string v0, "undefined"

    invoke-static {p2, v0}, LX/0wE;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 1507025
    :pswitch_5
    const-string v0, "undefined"

    invoke-static {p2, v0}, LX/0wE;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 1507026
    :pswitch_6
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1507027
    :pswitch_7
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x31 -> :sswitch_2
        0x63e -> :sswitch_7
        0x640 -> :sswitch_6
        0x641 -> :sswitch_3
        0x642 -> :sswitch_0
        0x643 -> :sswitch_5
        0x644 -> :sswitch_4
        0x645 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
