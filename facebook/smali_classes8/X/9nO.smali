.class public final LX/9nO;
.super LX/5pb;
.source ""

# interfaces
.implements LX/9mx;


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "AsyncSQLiteDBStorage"
.end annotation


# instance fields
.field public a:LX/9nP;

.field private b:Z


# direct methods
.method public constructor <init>(LX/5pY;)V
    .locals 1

    .prologue
    .line 1537225
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 1537226
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/9nO;->b:Z

    .line 1537227
    invoke-static {p1}, LX/9nP;->a(Landroid/content/Context;)LX/9nP;

    move-result-object v0

    iput-object v0, p0, LX/9nO;->a:LX/9nP;

    .line 1537228
    return-void
.end method

.method public static h(LX/9nO;)Z
    .locals 1

    .prologue
    .line 1537224
    iget-boolean v0, p0, LX/9nO;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/9nO;->a:LX/9nP;

    invoke-virtual {v0}, LX/9nP;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1537222
    iget-object v0, p0, LX/9nO;->a:LX/9nP;

    invoke-virtual {v0}, LX/9nP;->c()V

    .line 1537223
    return-void
.end method

.method public final clear(Lcom/facebook/react/bridge/Callback;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1537218
    new-instance v0, LX/9nM;

    .line 1537219
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 1537220
    invoke-direct {v0, p0, v1, p1}, LX/9nM;-><init>(LX/9nO;LX/5pX;Lcom/facebook/react/bridge/Callback;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, LX/9nM;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1537221
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1537215
    invoke-super {p0}, LX/5pb;->d()V

    .line 1537216
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/9nO;->b:Z

    .line 1537217
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 1537186
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/9nO;->b:Z

    .line 1537187
    return-void
.end method

.method public final getAllKeys(Lcom/facebook/react/bridge/Callback;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1537211
    new-instance v0, LX/9nN;

    .line 1537212
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 1537213
    invoke-direct {v0, p0, v1, p1}, LX/9nN;-><init>(LX/9nO;LX/5pX;Lcom/facebook/react/bridge/Callback;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, LX/9nN;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1537214
    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1537210
    const-string v0, "AsyncSQLiteDBStorage"

    return-object v0
.end method

.method public final multiGet(LX/5pC;Lcom/facebook/react/bridge/Callback;)V
    .locals 4
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1537204
    if-nez p1, :cond_0

    .line 1537205
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v3}, LX/9nH;->a(Ljava/lang/String;)LX/5pH;

    move-result-object v1

    aput-object v1, v0, v2

    const/4 v1, 0x1

    aput-object v3, v0, v1

    invoke-interface {p2, v0}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 1537206
    :goto_0
    return-void

    .line 1537207
    :cond_0
    new-instance v0, LX/9nI;

    .line 1537208
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 1537209
    invoke-direct {v0, p0, v1, p2, p1}, LX/9nI;-><init>(LX/9nO;LX/5pX;Lcom/facebook/react/bridge/Callback;LX/5pC;)V

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, LX/9nI;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public final multiMerge(LX/5pC;Lcom/facebook/react/bridge/Callback;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1537200
    new-instance v0, LX/9nL;

    .line 1537201
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 1537202
    invoke-direct {v0, p0, v1, p2, p1}, LX/9nL;-><init>(LX/9nO;LX/5pX;Lcom/facebook/react/bridge/Callback;LX/5pC;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, LX/9nL;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1537203
    return-void
.end method

.method public final multiRemove(LX/5pC;Lcom/facebook/react/bridge/Callback;)V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1537194
    invoke-interface {p1}, LX/5pC;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 1537195
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {v1}, LX/9nH;->a(Ljava/lang/String;)LX/5pH;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-interface {p2, v0}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 1537196
    :goto_0
    return-void

    .line 1537197
    :cond_0
    new-instance v0, LX/9nK;

    .line 1537198
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 1537199
    invoke-direct {v0, p0, v1, p2, p1}, LX/9nK;-><init>(LX/9nO;LX/5pX;Lcom/facebook/react/bridge/Callback;LX/5pC;)V

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, LX/9nK;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public final multiSet(LX/5pC;Lcom/facebook/react/bridge/Callback;)V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1537188
    invoke-interface {p1}, LX/5pC;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 1537189
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {v1}, LX/9nH;->a(Ljava/lang/String;)LX/5pH;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-interface {p2, v0}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 1537190
    :goto_0
    return-void

    .line 1537191
    :cond_0
    new-instance v0, LX/9nJ;

    .line 1537192
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 1537193
    invoke-direct {v0, p0, v1, p2, p1}, LX/9nJ;-><init>(LX/9nO;LX/5pX;Lcom/facebook/react/bridge/Callback;LX/5pC;)V

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, LX/9nJ;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
