.class public final LX/9GI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/9GJ;

.field public final synthetic b:LX/9FA;

.field public final synthetic c:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveLightweightRecSlotClickListenerPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveLightweightRecSlotClickListenerPartDefinition;LX/9GJ;LX/9FA;)V
    .locals 0

    .prologue
    .line 1459680
    iput-object p1, p0, LX/9GI;->c:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveLightweightRecSlotClickListenerPartDefinition;

    iput-object p2, p0, LX/9GI;->a:LX/9GJ;

    iput-object p3, p0, LX/9GI;->b:LX/9FA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v7, 0x2

    const/4 v0, 0x1

    const v1, -0x5af06de4

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1459681
    iget-object v1, p0, LX/9GI;->a:LX/9GJ;

    iget-object v1, v1, LX/9GJ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1}, LX/1WF;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v1

    .line 1459682
    iget-object v2, p0, LX/9GI;->c:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveLightweightRecSlotClickListenerPartDefinition;

    iget-object v3, p0, LX/9GI;->a:LX/9GJ;

    iget-object v3, v3, LX/9GJ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v3}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    iget-object v4, p0, LX/9GI;->a:LX/9GJ;

    iget-object v4, v4, LX/9GJ;->b:Ljava/lang/String;

    .line 1459683
    if-nez v3, :cond_3

    .line 1459684
    const/4 v5, 0x0

    .line 1459685
    :goto_0
    move-object v2, v5

    .line 1459686
    iget-object v3, p0, LX/9GI;->c:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveLightweightRecSlotClickListenerPartDefinition;

    iget-object v4, p0, LX/9GI;->a:LX/9GJ;

    iget-object v4, v4, LX/9GJ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v4}, LX/1WF;->h(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    iget-object v5, p0, LX/9GI;->a:LX/9GJ;

    iget-object v5, v5, LX/9GJ;->b:Ljava/lang/String;

    .line 1459687
    if-nez v4, :cond_4

    .line 1459688
    const/4 v6, 0x0

    .line 1459689
    :goto_1
    move-object v1, v6

    .line 1459690
    iget-object v3, p0, LX/9GI;->c:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveLightweightRecSlotClickListenerPartDefinition;

    iget-object v3, v3, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveLightweightRecSlotClickListenerPartDefinition;->b:LX/8y0;

    iget-object v4, p0, LX/9GI;->a:LX/9GJ;

    iget-object v4, v4, LX/9GJ;->b:Ljava/lang/String;

    iget-object v5, v1, LX/6PS;->b:Lcom/facebook/graphql/model/GraphQLComment;

    new-instance v6, LX/9GH;

    invoke-direct {v6, p0}, LX/9GH;-><init>(LX/9GI;)V

    .line 1459691
    new-instance v8, LX/4IJ;

    invoke-direct {v8}, LX/4IJ;-><init>()V

    .line 1459692
    const-string v9, "slot_id"

    invoke-virtual {v8, v9, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1459693
    move-object v8, v8

    .line 1459694
    invoke-static {}, LX/5HL;->c()LX/5HK;

    move-result-object v9

    .line 1459695
    const-string v10, "input"

    invoke-virtual {v9, v10, v8}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1459696
    invoke-static {v9}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v8

    invoke-static {v4, v5}, LX/8y0;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListRemoveLightweightRecMutationCallModel;

    move-result-object v9

    invoke-virtual {v8, v9}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v8

    .line 1459697
    iget-object v9, v3, LX/8y0;->b:LX/1Ck;

    const-string v10, "place_list_remove_lightweight_rec"

    iget-object p1, v3, LX/8y0;->a:LX/0tX;

    invoke-virtual {p1, v8}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    invoke-static {v6}, LX/0Vd;->of(LX/0TF;)LX/0Vd;

    move-result-object p1

    invoke-virtual {v9, v10, v8, p1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1459698
    iget-object v3, p0, LX/9GI;->b:LX/9FA;

    .line 1459699
    iget-boolean v4, v3, LX/9FA;->c:Z

    move v3, v4

    .line 1459700
    if-eqz v3, :cond_1

    .line 1459701
    if-nez v2, :cond_0

    .line 1459702
    const v1, -0x352e5aea    # -6869643.0f

    invoke-static {v7, v7, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1459703
    :goto_2
    return-void

    .line 1459704
    :cond_0
    invoke-static {v2}, LX/182;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    .line 1459705
    iget-object v2, p0, LX/9GI;->c:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveLightweightRecSlotClickListenerPartDefinition;

    iget-object v2, v2, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveLightweightRecSlotClickListenerPartDefinition;->e:LX/0bH;

    new-instance v3, LX/1Ne;

    invoke-direct {v3, v1}, LX/1Ne;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b7;)V

    .line 1459706
    :goto_3
    const v1, 0x6a0ff0df

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_2

    .line 1459707
    :cond_1
    if-nez v1, :cond_2

    .line 1459708
    const v1, 0x148f1f07

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_2

    .line 1459709
    :cond_2
    iget-object v2, p0, LX/9GI;->c:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveLightweightRecSlotClickListenerPartDefinition;

    iget-object v2, v2, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveLightweightRecSlotClickListenerPartDefinition;->f:LX/1K9;

    new-instance v3, LX/8q4;

    iget-object v4, v1, LX/6PS;->b:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v1, v1, LX/6PS;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v4, v1}, LX/8q4;-><init>(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/1K9;->a(LX/1KJ;)V

    goto :goto_3

    :cond_3
    iget-object v5, v2, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveLightweightRecSlotClickListenerPartDefinition;->d:LX/189;

    invoke-virtual {v5, v3, v4, v1}, LX/189;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v5

    goto/16 :goto_0

    :cond_4
    iget-object v6, v3, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveLightweightRecSlotClickListenerPartDefinition;->c:LX/20j;

    invoke-virtual {v6, v4, v1, v5}, LX/20j;->b(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;Ljava/lang/String;)LX/6PS;

    move-result-object v6

    goto/16 :goto_1
.end method
