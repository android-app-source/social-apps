.class public final enum LX/9X5;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/9X2;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9X5;",
        ">;",
        "LX/9X2;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9X5;

.field public static final enum EVENT_ADMIN_ACTIVITY_CLICK_ADMIN_FEED:LX/9X5;

.field public static final enum EVENT_ADMIN_ACTIVITY_CLICK_COMMUNICATIONS_HUB:LX/9X5;

.field public static final enum EVENT_ADMIN_ACTIVITY_CLICK_DRAFT_POST:LX/9X5;

.field public static final enum EVENT_ADMIN_ACTIVITY_CLICK_EDIT_UNI_STATUS:LX/9X5;

.field public static final enum EVENT_ADMIN_ACTIVITY_CLICK_FOLLOWERS:LX/9X5;

.field public static final enum EVENT_ADMIN_ACTIVITY_CLICK_INSIGHTS_WEEKLY_LIKE:LX/9X5;

.field public static final enum EVENT_ADMIN_ACTIVITY_CLICK_INSIGHTS_WEEKLY_POST_REACH:LX/9X5;

.field public static final enum EVENT_ADMIN_ACTIVITY_CLICK_MESSAGE:LX/9X5;

.field public static final enum EVENT_ADMIN_ACTIVITY_CLICK_NEW_LIKES:LX/9X5;

.field public static final enum EVENT_ADMIN_ACTIVITY_CLICK_NOTIF:LX/9X5;

.field public static final enum EVENT_ADMIN_ACTIVITY_CLICK_PAGES_FEED:LX/9X5;

.field public static final enum EVENT_ADMIN_ACTIVITY_CLICK_PAGE_COMMENTS:LX/9X5;

.field public static final enum EVENT_ADMIN_ACTIVITY_CLICK_PAGE_MESSAGES:LX/9X5;

.field public static final enum EVENT_ADMIN_ACTIVITY_CLICK_PAGE_TIPS:LX/9X5;

.field public static final enum EVENT_ADMIN_ACTIVITY_CLICK_RECENT_ACTIVITY:LX/9X5;

.field public static final enum EVENT_ADMIN_ACTIVITY_CLICK_RECENT_CHECK_INS:LX/9X5;

.field public static final enum EVENT_ADMIN_ACTIVITY_CLICK_RECENT_MENTIONS:LX/9X5;

.field public static final enum EVENT_ADMIN_ACTIVITY_CLICK_RECENT_REVIEWS:LX/9X5;

.field public static final enum EVENT_ADMIN_ACTIVITY_CLICK_RECENT_SHARES:LX/9X5;

.field public static final enum EVENT_ADMIN_ACTIVITY_CLICK_SCHEDULED_POST:LX/9X5;


# instance fields
.field private mEventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1502284
    new-instance v0, LX/9X5;

    const-string v1, "EVENT_ADMIN_ACTIVITY_CLICK_NOTIF"

    const-string v2, "admin_click_notif"

    invoke-direct {v0, v1, v4, v2}, LX/9X5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_NOTIF:LX/9X5;

    .line 1502285
    new-instance v0, LX/9X5;

    const-string v1, "EVENT_ADMIN_ACTIVITY_CLICK_NEW_LIKES"

    const-string v2, "admin_click_new_likes"

    invoke-direct {v0, v1, v5, v2}, LX/9X5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_NEW_LIKES:LX/9X5;

    .line 1502286
    new-instance v0, LX/9X5;

    const-string v1, "EVENT_ADMIN_ACTIVITY_CLICK_SCHEDULED_POST"

    const-string v2, "admin_click_scheduled_post"

    invoke-direct {v0, v1, v6, v2}, LX/9X5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_SCHEDULED_POST:LX/9X5;

    .line 1502287
    new-instance v0, LX/9X5;

    const-string v1, "EVENT_ADMIN_ACTIVITY_CLICK_MESSAGE"

    const-string v2, "admin_click_message"

    invoke-direct {v0, v1, v7, v2}, LX/9X5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_MESSAGE:LX/9X5;

    .line 1502288
    new-instance v0, LX/9X5;

    const-string v1, "EVENT_ADMIN_ACTIVITY_CLICK_EDIT_UNI_STATUS"

    const-string v2, "admin_click_edit_boosted_pagelike_status"

    invoke-direct {v0, v1, v8, v2}, LX/9X5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_EDIT_UNI_STATUS:LX/9X5;

    .line 1502289
    new-instance v0, LX/9X5;

    const-string v1, "EVENT_ADMIN_ACTIVITY_CLICK_INSIGHTS_WEEKLY_LIKE"

    const/4 v2, 0x5

    const-string v3, "admin_click_insights_graph_weekly_like"

    invoke-direct {v0, v1, v2, v3}, LX/9X5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_INSIGHTS_WEEKLY_LIKE:LX/9X5;

    .line 1502290
    new-instance v0, LX/9X5;

    const-string v1, "EVENT_ADMIN_ACTIVITY_CLICK_INSIGHTS_WEEKLY_POST_REACH"

    const/4 v2, 0x6

    const-string v3, "admin_click_insights_graph_weekly_post_reach"

    invoke-direct {v0, v1, v2, v3}, LX/9X5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_INSIGHTS_WEEKLY_POST_REACH:LX/9X5;

    .line 1502291
    new-instance v0, LX/9X5;

    const-string v1, "EVENT_ADMIN_ACTIVITY_CLICK_PAGES_FEED"

    const/4 v2, 0x7

    const-string v3, "admin_click_pages_feed"

    invoke-direct {v0, v1, v2, v3}, LX/9X5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_PAGES_FEED:LX/9X5;

    .line 1502292
    new-instance v0, LX/9X5;

    const-string v1, "EVENT_ADMIN_ACTIVITY_CLICK_ADMIN_FEED"

    const/16 v2, 0x8

    const-string v3, "admin_click_admin_feed"

    invoke-direct {v0, v1, v2, v3}, LX/9X5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_ADMIN_FEED:LX/9X5;

    .line 1502293
    new-instance v0, LX/9X5;

    const-string v1, "EVENT_ADMIN_ACTIVITY_CLICK_RECENT_ACTIVITY"

    const/16 v2, 0x9

    const-string v3, "admin_click_recent_activity"

    invoke-direct {v0, v1, v2, v3}, LX/9X5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_RECENT_ACTIVITY:LX/9X5;

    .line 1502294
    new-instance v0, LX/9X5;

    const-string v1, "EVENT_ADMIN_ACTIVITY_CLICK_RECENT_MENTIONS"

    const/16 v2, 0xa

    const-string v3, "admin_click_recent_mentions"

    invoke-direct {v0, v1, v2, v3}, LX/9X5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_RECENT_MENTIONS:LX/9X5;

    .line 1502295
    new-instance v0, LX/9X5;

    const-string v1, "EVENT_ADMIN_ACTIVITY_CLICK_RECENT_SHARES"

    const/16 v2, 0xb

    const-string v3, "admin_click_recent_shares"

    invoke-direct {v0, v1, v2, v3}, LX/9X5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_RECENT_SHARES:LX/9X5;

    .line 1502296
    new-instance v0, LX/9X5;

    const-string v1, "EVENT_ADMIN_ACTIVITY_CLICK_RECENT_REVIEWS"

    const/16 v2, 0xc

    const-string v3, "admin_click_recent_reviews"

    invoke-direct {v0, v1, v2, v3}, LX/9X5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_RECENT_REVIEWS:LX/9X5;

    .line 1502297
    new-instance v0, LX/9X5;

    const-string v1, "EVENT_ADMIN_ACTIVITY_CLICK_RECENT_CHECK_INS"

    const/16 v2, 0xd

    const-string v3, "admin_click_recent_check_ins"

    invoke-direct {v0, v1, v2, v3}, LX/9X5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_RECENT_CHECK_INS:LX/9X5;

    .line 1502298
    new-instance v0, LX/9X5;

    const-string v1, "EVENT_ADMIN_ACTIVITY_CLICK_DRAFT_POST"

    const/16 v2, 0xe

    const-string v3, "admin_click_draft_post"

    invoke-direct {v0, v1, v2, v3}, LX/9X5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_DRAFT_POST:LX/9X5;

    .line 1502299
    new-instance v0, LX/9X5;

    const-string v1, "EVENT_ADMIN_ACTIVITY_CLICK_FOLLOWERS"

    const/16 v2, 0xf

    const-string v3, "admin_activity_click_followers"

    invoke-direct {v0, v1, v2, v3}, LX/9X5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_FOLLOWERS:LX/9X5;

    .line 1502300
    new-instance v0, LX/9X5;

    const-string v1, "EVENT_ADMIN_ACTIVITY_CLICK_PAGE_TIPS"

    const/16 v2, 0x10

    const-string v3, "admin_click_page_tips"

    invoke-direct {v0, v1, v2, v3}, LX/9X5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_PAGE_TIPS:LX/9X5;

    .line 1502301
    new-instance v0, LX/9X5;

    const-string v1, "EVENT_ADMIN_ACTIVITY_CLICK_PAGE_COMMENTS"

    const/16 v2, 0x11

    const-string v3, "admin_click_page_comments"

    invoke-direct {v0, v1, v2, v3}, LX/9X5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_PAGE_COMMENTS:LX/9X5;

    .line 1502302
    new-instance v0, LX/9X5;

    const-string v1, "EVENT_ADMIN_ACTIVITY_CLICK_COMMUNICATIONS_HUB"

    const/16 v2, 0x12

    const-string v3, "admin_click_communications_hub"

    invoke-direct {v0, v1, v2, v3}, LX/9X5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_COMMUNICATIONS_HUB:LX/9X5;

    .line 1502303
    new-instance v0, LX/9X5;

    const-string v1, "EVENT_ADMIN_ACTIVITY_CLICK_PAGE_MESSAGES"

    const/16 v2, 0x13

    const-string v3, "admin_click_page_messages"

    invoke-direct {v0, v1, v2, v3}, LX/9X5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_PAGE_MESSAGES:LX/9X5;

    .line 1502304
    const/16 v0, 0x14

    new-array v0, v0, [LX/9X5;

    sget-object v1, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_NOTIF:LX/9X5;

    aput-object v1, v0, v4

    sget-object v1, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_NEW_LIKES:LX/9X5;

    aput-object v1, v0, v5

    sget-object v1, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_SCHEDULED_POST:LX/9X5;

    aput-object v1, v0, v6

    sget-object v1, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_MESSAGE:LX/9X5;

    aput-object v1, v0, v7

    sget-object v1, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_EDIT_UNI_STATUS:LX/9X5;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_INSIGHTS_WEEKLY_LIKE:LX/9X5;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_INSIGHTS_WEEKLY_POST_REACH:LX/9X5;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_PAGES_FEED:LX/9X5;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_ADMIN_FEED:LX/9X5;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_RECENT_ACTIVITY:LX/9X5;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_RECENT_MENTIONS:LX/9X5;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_RECENT_SHARES:LX/9X5;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_RECENT_REVIEWS:LX/9X5;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_RECENT_CHECK_INS:LX/9X5;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_DRAFT_POST:LX/9X5;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_FOLLOWERS:LX/9X5;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_PAGE_TIPS:LX/9X5;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_PAGE_COMMENTS:LX/9X5;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_COMMUNICATIONS_HUB:LX/9X5;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_PAGE_MESSAGES:LX/9X5;

    aput-object v2, v0, v1

    sput-object v0, LX/9X5;->$VALUES:[LX/9X5;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1502305
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1502306
    iput-object p3, p0, LX/9X5;->mEventName:Ljava/lang/String;

    .line 1502307
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9X5;
    .locals 1

    .prologue
    .line 1502308
    const-class v0, LX/9X5;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9X5;

    return-object v0
.end method

.method public static values()[LX/9X5;
    .locals 1

    .prologue
    .line 1502309
    sget-object v0, LX/9X5;->$VALUES:[LX/9X5;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9X5;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1502310
    iget-object v0, p0, LX/9X5;->mEventName:Ljava/lang/String;

    return-object v0
.end method

.method public final getType()LX/9XC;
    .locals 1

    .prologue
    .line 1502311
    sget-object v0, LX/9XC;->ADMIN_ACTIVITY_TAB:LX/9XC;

    return-object v0
.end method
