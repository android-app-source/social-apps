.class public LX/9ij;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/8Jj;


# instance fields
.field public a:LX/8Ji;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/ui/animations/ViewAnimatorFactory;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:F

.field private d:F

.field public e:Lcom/facebook/photos/base/tagging/Tag;

.field private f:Z

.field private g:Lcom/facebook/photos/tagging/shared/BubbleLayout;

.field public h:Landroid/widget/TextView;

.field public i:Landroid/widget/ImageView;

.field public j:LX/9ii;

.field public k:LX/8Hm;

.field public l:LX/8Hm;

.field public m:LX/8Hs;

.field public n:Z

.field private o:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private p:LX/8JG;

.field public q:I

.field public r:Z

.field private s:LX/8Jh;

.field public t:I

.field public u:I

.field public v:I

.field private w:LX/8Hs;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/photos/base/tagging/Tag;Z)V
    .locals 2

    .prologue
    .line 1528219
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1528220
    new-instance v0, LX/8Jh;

    invoke-direct {v0}, LX/8Jh;-><init>()V

    iput-object v0, p0, LX/9ij;->s:LX/8Jh;

    .line 1528221
    sget-object v0, LX/9ih;->FULL_NAME:LX/9ih;

    const/4 v1, 0x0

    invoke-direct {p0, p2, p3, v0, v1}, LX/9ij;->a(Lcom/facebook/photos/base/tagging/Tag;ZLX/9ih;Z)V

    .line 1528222
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/photos/base/tagging/Tag;ZLX/9ih;Z)V
    .locals 1

    .prologue
    .line 1528327
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1528328
    new-instance v0, LX/8Jh;

    invoke-direct {v0}, LX/8Jh;-><init>()V

    iput-object v0, p0, LX/9ij;->s:LX/8Jh;

    .line 1528329
    invoke-direct {p0, p2, p3, p4, p5}, LX/9ij;->a(Lcom/facebook/photos/base/tagging/Tag;ZLX/9ih;Z)V

    .line 1528330
    return-void
.end method

.method private static a(LX/8JG;)LX/364;
    .locals 2

    .prologue
    .line 1528316
    sget-object v0, LX/9ig;->a:[I

    invoke-virtual {p0}, LX/8JG;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1528317
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Not all directions implemented"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1528318
    :pswitch_0
    sget-object v0, LX/364;->BOTTOM:LX/364;

    .line 1528319
    :goto_0
    return-object v0

    .line 1528320
    :pswitch_1
    sget-object v0, LX/364;->TOP:LX/364;

    goto :goto_0

    .line 1528321
    :pswitch_2
    sget-object v0, LX/364;->RIGHT:LX/364;

    goto :goto_0

    .line 1528322
    :pswitch_3
    sget-object v0, LX/364;->LEFT:LX/364;

    goto :goto_0

    .line 1528323
    :pswitch_4
    sget-object v0, LX/364;->BOTTOMRIGHT:LX/364;

    goto :goto_0

    .line 1528324
    :pswitch_5
    sget-object v0, LX/364;->BOTTOMLEFT:LX/364;

    goto :goto_0

    .line 1528325
    :pswitch_6
    sget-object v0, LX/364;->TOPRIGHT:LX/364;

    goto :goto_0

    .line 1528326
    :pswitch_7
    sget-object v0, LX/364;->TOPLEFT:LX/364;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private static a(Lcom/facebook/photos/base/tagging/Tag;LX/9ih;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1528309
    sget-object v0, LX/9ih;->FIRST_NAME:LX/9ih;

    if-ne p1, v0, :cond_0

    .line 1528310
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/Tag;->b:Lcom/facebook/user/model/Name;

    move-object v0, v0

    .line 1528311
    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->a()Ljava/lang/String;

    move-result-object v0

    .line 1528312
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1528313
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/base/tagging/Tag;->b:Lcom/facebook/user/model/Name;

    move-object v0, v0

    .line 1528314
    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v0

    .line 1528315
    :cond_1
    return-object v0
.end method

.method private a(Lcom/facebook/photos/base/tagging/Tag;ZLX/9ih;Z)V
    .locals 7

    .prologue
    const/4 v4, 0x1

    .line 1528288
    const-class v0, LX/9ij;

    invoke-static {v0, p0}, LX/9ij;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1528289
    const v0, 0x7f030f42

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1528290
    invoke-virtual {p0}, LX/9ij;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b100c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, LX/9ij;->c:F

    .line 1528291
    invoke-virtual {p0}, LX/9ij;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1009

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, LX/9ij;->d:F

    .line 1528292
    iget v0, p0, LX/9ij;->c:F

    float-to-int v0, v0

    iget v1, p0, LX/9ij;->c:F

    iget v2, p0, LX/9ij;->d:F

    sub-float/2addr v1, v2

    float-to-int v1, v1

    iget v2, p0, LX/9ij;->c:F

    float-to-int v2, v2

    iget v3, p0, LX/9ij;->c:F

    float-to-int v3, v3

    invoke-virtual {p0, v0, v1, v2, v3}, LX/9ij;->setPadding(IIII)V

    .line 1528293
    iput-object p1, p0, LX/9ij;->e:Lcom/facebook/photos/base/tagging/Tag;

    .line 1528294
    iput-boolean p2, p0, LX/9ij;->f:Z

    .line 1528295
    sget-object v0, LX/8JG;->UP:LX/8JG;

    iput-object v0, p0, LX/9ij;->p:LX/8JG;

    .line 1528296
    const v0, 0x7f0d24f3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/tagging/shared/BubbleLayout;

    iput-object v0, p0, LX/9ij;->g:Lcom/facebook/photos/tagging/shared/BubbleLayout;

    .line 1528297
    const v0, 0x7f0d24f4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/9ij;->h:Landroid/widget/TextView;

    .line 1528298
    iget-object v0, p0, LX/9ij;->h:Landroid/widget/TextView;

    invoke-static {p1, p3}, LX/9ij;->a(Lcom/facebook/photos/base/tagging/Tag;LX/9ih;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1528299
    const v0, 0x7f0d24f5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/9ij;->i:Landroid/widget/ImageView;

    .line 1528300
    iget-object v0, p0, LX/9ij;->i:Landroid/widget/ImageView;

    invoke-virtual {p0}, LX/9ij;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0819af

    new-array v3, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1, p3}, LX/9ij;->a(Lcom/facebook/photos/base/tagging/Tag;LX/9ih;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1528301
    iput-boolean p4, p0, LX/9ij;->r:Z

    .line 1528302
    invoke-virtual {p0}, LX/9ij;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1014

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/9ij;->u:I

    .line 1528303
    invoke-virtual {p0}, LX/9ij;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1015

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/9ij;->t:I

    .line 1528304
    invoke-virtual {p0}, LX/9ij;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b100f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, LX/9ij;->v:I

    .line 1528305
    invoke-virtual {p0}, LX/9ij;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1528306
    iget-object v0, p0, LX/9ij;->i:Landroid/widget/ImageView;

    new-instance v1, LX/9id;

    invoke-direct {v1, p0}, LX/9id;-><init>(LX/9ij;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1528307
    new-instance v0, LX/8Hs;

    iget-object v1, p0, LX/9ij;->i:Landroid/widget/ImageView;

    const-wide/16 v2, 0x64

    iget-object v5, p0, LX/9ij;->b:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/4mV;

    invoke-direct/range {v0 .. v5}, LX/8Hs;-><init>(Landroid/view/View;JZLX/4mV;)V

    iput-object v0, p0, LX/9ij;->m:LX/8Hs;

    .line 1528308
    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, LX/9ij;

    new-instance v1, LX/8Ji;

    invoke-direct {v1}, LX/8Ji;-><init>()V

    move-object v1, v1

    move-object v1, v1

    check-cast v1, LX/8Ji;

    const/16 p0, 0x375a

    invoke-static {v2, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    iput-object v1, p1, LX/9ij;->a:LX/8Ji;

    iput-object v2, p1, LX/9ij;->b:LX/0Ot;

    return-void
.end method

.method private b(LX/8JG;LX/8Jh;)V
    .locals 3

    .prologue
    .line 1528277
    iget-object v0, p2, LX/8Jh;->b:Landroid/graphics/Rect;

    iget v1, p0, LX/9ij;->c:F

    neg-float v1, v1

    float-to-int v1, v1

    iget v2, p0, LX/9ij;->c:F

    neg-float v2, v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->inset(II)V

    .line 1528278
    iget v0, p0, LX/9ij;->d:F

    neg-float v0, v0

    float-to-int v0, v0

    iget-object v1, p2, LX/8Jh;->b:Landroid/graphics/Rect;

    .line 1528279
    sget-object v2, LX/8JG;->UP:LX/8JG;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/8JG;->UPLEFT:LX/8JG;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/8JG;->UPRIGHT:LX/8JG;

    if-ne p1, v2, :cond_1

    .line 1528280
    :cond_0
    iget v2, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v0

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 1528281
    :cond_1
    sget-object v2, LX/8JG;->DOWN:LX/8JG;

    if-eq p1, v2, :cond_2

    sget-object v2, LX/8JG;->DOWNLEFT:LX/8JG;

    if-eq p1, v2, :cond_2

    sget-object v2, LX/8JG;->DOWNRIGHT:LX/8JG;

    if-ne p1, v2, :cond_3

    .line 1528282
    :cond_2
    iget v2, v1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v2, v0

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 1528283
    :cond_3
    sget-object v2, LX/8JG;->LEFT:LX/8JG;

    if-eq p1, v2, :cond_4

    sget-object v2, LX/8JG;->UPLEFT:LX/8JG;

    if-eq p1, v2, :cond_4

    sget-object v2, LX/8JG;->DOWNLEFT:LX/8JG;

    if-ne p1, v2, :cond_5

    .line 1528284
    :cond_4
    iget v2, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v0

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 1528285
    :cond_5
    sget-object v2, LX/8JG;->RIGHT:LX/8JG;

    if-eq p1, v2, :cond_6

    sget-object v2, LX/8JG;->UPRIGHT:LX/8JG;

    if-eq p1, v2, :cond_6

    sget-object v2, LX/8JG;->DOWNRIGHT:LX/8JG;

    if-ne p1, v2, :cond_7

    .line 1528286
    :cond_6
    iget v2, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v2, v0

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 1528287
    :cond_7
    return-void
.end method

.method private getVisibilityAnimator()LX/8Hs;
    .locals 4

    .prologue
    .line 1528274
    iget-object v0, p0, LX/9ij;->w:LX/8Hs;

    if-nez v0, :cond_0

    .line 1528275
    new-instance v1, LX/8Hs;

    const-wide/16 v2, 0xc8

    iget-object v0, p0, LX/9ij;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4mV;

    invoke-direct {v1, p0, v2, v3, v0}, LX/8Hs;-><init>(Landroid/view/View;JLX/4mV;)V

    iput-object v1, p0, LX/9ij;->w:LX/8Hs;

    .line 1528276
    :cond_0
    iget-object v0, p0, LX/9ij;->w:LX/8Hs;

    return-object v0
.end method


# virtual methods
.method public final a(I)V
    .locals 4

    .prologue
    .line 1528271
    iget-object v0, p0, LX/9ij;->g:Lcom/facebook/photos/tagging/shared/BubbleLayout;

    invoke-virtual {v0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->getWidth()I

    move-result v0

    if-lez v0, :cond_0

    .line 1528272
    iget-object v0, p0, LX/9ij;->g:Lcom/facebook/photos/tagging/shared/BubbleLayout;

    const/high16 v1, 0x3f000000    # 0.5f

    int-to-float v2, p1

    iget-object v3, p0, LX/9ij;->g:Lcom/facebook/photos/tagging/shared/BubbleLayout;

    invoke-virtual {v3}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->setArrowPosition(F)V

    .line 1528273
    :cond_0
    return-void
.end method

.method public final a(LX/8JG;LX/8Jh;)V
    .locals 3

    .prologue
    .line 1528263
    iget-object v0, p0, LX/9ij;->g:Lcom/facebook/photos/tagging/shared/BubbleLayout;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->a(LX/8JG;LX/8Jh;)V

    .line 1528264
    iget-object v0, p2, LX/8Jh;->b:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->left:I

    iget v2, p0, LX/9ij;->v:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 1528265
    iget-object v0, p2, LX/8Jh;->b:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->right:I

    iget v2, p0, LX/9ij;->v:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 1528266
    iget-object v0, p0, LX/9ij;->e:Lcom/facebook/photos/base/tagging/Tag;

    invoke-virtual {v0}, Lcom/facebook/photos/base/tagging/Tag;->k()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/9ij;->n:Z

    if-nez v0, :cond_0

    .line 1528267
    iget-object v0, p2, LX/8Jh;->b:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->right:I

    iget v2, p0, LX/9ij;->u:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 1528268
    :cond_0
    iget-object v0, p2, LX/8Jh;->a:Landroid/graphics/Rect;

    iget-object v1, p2, LX/8Jh;->b:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 1528269
    invoke-direct {p0, p1, p2}, LX/9ij;->b(LX/8JG;LX/8Jh;)V

    .line 1528270
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1528254
    invoke-virtual {p0}, LX/9ij;->a()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1528255
    if-eqz p1, :cond_0

    .line 1528256
    iget-object v0, p0, LX/9ij;->k:LX/8Hm;

    invoke-virtual {p0, v0}, LX/9ij;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1528257
    :goto_0
    iput-boolean v2, p0, LX/9ij;->n:Z

    .line 1528258
    return-void

    .line 1528259
    :cond_0
    invoke-virtual {p0}, LX/9ij;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, LX/9ij;->q:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1528260
    iget-object v0, p0, LX/9ij;->m:LX/8Hs;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/8Hs;->a(Z)V

    .line 1528261
    iget-object v0, p0, LX/9ij;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1528262
    invoke-virtual {p0}, LX/9ij;->requestLayout()V

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1528218
    iget-boolean v0, p0, LX/9ij;->f:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/9ij;->e:Lcom/facebook/photos/base/tagging/Tag;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/9ij;->e:Lcom/facebook/photos/base/tagging/Tag;

    invoke-virtual {v0}, Lcom/facebook/photos/base/tagging/Tag;->k()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 1528248
    const/4 v0, 0x1

    .line 1528249
    if-eqz v0, :cond_0

    .line 1528250
    iget-object v1, p0, LX/9ij;->l:LX/8Hm;

    invoke-virtual {p0, v1}, LX/9ij;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1528251
    :goto_0
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/9ij;->n:Z

    .line 1528252
    return-void

    .line 1528253
    :cond_0
    iget-object v1, p0, LX/9ij;->i:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 1528245
    iget-object v0, p0, LX/9ij;->e:Lcom/facebook/photos/base/tagging/Tag;

    .line 1528246
    iget-boolean p0, v0, Lcom/facebook/photos/base/tagging/Tag;->e:Z

    move v0, p0

    .line 1528247
    return v0
.end method

.method public getArrowDirection()LX/8JG;
    .locals 1

    .prologue
    .line 1528244
    iget-object v0, p0, LX/9ij;->p:LX/8JG;

    return-object v0
.end method

.method public getArrowLength()I
    .locals 1

    .prologue
    .line 1528243
    iget v0, p0, LX/9ij;->d:F

    float-to-int v0, v0

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x1138e840

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1528239
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onAttachedToWindow()V

    .line 1528240
    invoke-virtual {p0}, LX/9ij;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1528241
    new-instance v1, Lcom/facebook/photos/tagging/ui/TagView$2;

    invoke-direct {v1, p0}, Lcom/facebook/photos/tagging/ui/TagView$2;-><init>(LX/9ij;)V

    invoke-static {p0, v1}, LX/8He;->b(Landroid/view/View;Ljava/lang/Runnable;)Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    move-result-object v1

    iput-object v1, p0, LX/9ij;->o:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 1528242
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x12db2331

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x4cba7cd9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1528234
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 1528235
    iget-object v1, p0, LX/9ij;->o:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    if-eqz v1, :cond_0

    .line 1528236
    iget-object v1, p0, LX/9ij;->o:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-static {p0, v1}, LX/8He;->b(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1528237
    const/4 v1, 0x0

    iput-object v1, p0, LX/9ij;->o:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 1528238
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x7854309f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setArrowDirection(LX/8JG;)V
    .locals 4

    .prologue
    .line 1528223
    iput-object p1, p0, LX/9ij;->p:LX/8JG;

    .line 1528224
    iget-object v0, p0, LX/9ij;->g:Lcom/facebook/photos/tagging/shared/BubbleLayout;

    iget-object v1, p0, LX/9ij;->p:LX/8JG;

    invoke-virtual {v0, v1}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->setArrowDirection(LX/8JG;)V

    .line 1528225
    iget-object v0, p0, LX/9ij;->e:Lcom/facebook/photos/base/tagging/Tag;

    invoke-static {p1}, LX/9ij;->a(LX/8JG;)LX/364;

    move-result-object v1

    .line 1528226
    iget-object v2, v0, Lcom/facebook/photos/base/tagging/Tag;->g:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1528227
    iget-object v2, v0, Lcom/facebook/photos/base/tagging/Tag;->a:Lcom/facebook/photos/base/tagging/TagTarget;

    invoke-interface {v2}, Lcom/facebook/photos/base/tagging/TagTarget;->e()Landroid/graphics/PointF;

    move-result-object v3

    iget-object v2, v0, Lcom/facebook/photos/base/tagging/Tag;->g:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    invoke-virtual {v3, v2}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    .line 1528228
    :cond_0
    iget-object v0, p0, LX/9ij;->s:LX/8Jh;

    .line 1528229
    iget-object v1, v0, LX/8Jh;->a:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->setEmpty()V

    .line 1528230
    iget-object v1, v0, LX/8Jh;->b:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->setEmpty()V

    .line 1528231
    iget-object v0, p0, LX/9ij;->s:LX/8Jh;

    invoke-direct {p0, p1, v0}, LX/9ij;->b(LX/8JG;LX/8Jh;)V

    .line 1528232
    iget-object v0, p0, LX/9ij;->s:LX/8Jh;

    invoke-virtual {v0}, LX/8Jh;->a()I

    move-result v0

    iget-object v1, p0, LX/9ij;->s:LX/8Jh;

    invoke-virtual {v1}, LX/8Jh;->b()I

    move-result v1

    iget-object v2, p0, LX/9ij;->s:LX/8Jh;

    invoke-virtual {v2}, LX/8Jh;->c()I

    move-result v2

    iget-object v3, p0, LX/9ij;->s:LX/8Jh;

    invoke-virtual {v3}, LX/8Jh;->d()I

    move-result v3

    invoke-virtual {p0, v0, v1, v2, v3}, LX/9ij;->setPadding(IIII)V

    .line 1528233
    return-void
.end method
