.class public final LX/9tJ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1561683
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 1561684
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1561685
    :goto_0
    return v1

    .line 1561686
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1561687
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_5

    .line 1561688
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1561689
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1561690
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 1561691
    const-string v6, "event_kind"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1561692
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    goto :goto_1

    .line 1561693
    :cond_2
    const-string v6, "event_viewer_capability"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1561694
    invoke-static {p0, p1}, LX/9tH;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1561695
    :cond_3
    const-string v6, "id"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1561696
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 1561697
    :cond_4
    const-string v6, "parent_group"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1561698
    invoke-static {p0, p1}, LX/9tI;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1561699
    :cond_5
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1561700
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1561701
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1561702
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1561703
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1561704
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1561705
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1561706
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1561707
    if-eqz v0, :cond_0

    .line 1561708
    const-string v0, "event_kind"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1561709
    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1561710
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1561711
    if-eqz v0, :cond_1

    .line 1561712
    const-string v1, "event_viewer_capability"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1561713
    invoke-static {p0, v0, p2}, LX/9tH;->a(LX/15i;ILX/0nX;)V

    .line 1561714
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1561715
    if-eqz v0, :cond_2

    .line 1561716
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1561717
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1561718
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1561719
    if-eqz v0, :cond_3

    .line 1561720
    const-string v1, "parent_group"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1561721
    invoke-static {p0, v0, p2}, LX/9tI;->a(LX/15i;ILX/0nX;)V

    .line 1561722
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1561723
    return-void
.end method
