.class public LX/9dD;
.super LX/4fO;
.source ""


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field private c:LX/1FJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FJ",
            "<",
            "LX/8Ht;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private d:Ljava/lang/String;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private e:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public f:LX/9dB;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:LX/1bh;

.field private h:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1517742
    const-class v0, LX/9dD;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/9dD;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1517743
    invoke-direct {p0}, LX/4fO;-><init>()V

    .line 1517744
    const/4 v0, 0x0

    iput-object v0, p0, LX/9dD;->c:LX/1FJ;

    .line 1517745
    sget-object v0, LX/5iL;->PassThrough:LX/5iL;

    invoke-virtual {v0}, LX/5iL;->name()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/9dD;->d:Ljava/lang/String;

    .line 1517746
    return-void
.end method


# virtual methods
.method public final a()LX/1bh;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1517747
    iget-boolean v0, p0, LX/9dD;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9dD;->g:LX/1bh;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/1FJ;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<",
            "LX/8Ht;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1517748
    monitor-enter p0

    .line 1517749
    :try_start_0
    iget-boolean v0, p0, LX/9dD;->e:Z

    if-eqz v0, :cond_0

    .line 1517750
    monitor-exit p0

    .line 1517751
    :goto_0
    return-void

    .line 1517752
    :cond_0
    invoke-static {p1}, LX/1FJ;->b(LX/1FJ;)LX/1FJ;

    move-result-object v0

    iput-object v0, p0, LX/9dD;->c:LX/1FJ;

    .line 1517753
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1517754
    invoke-virtual {p0}, LX/4fO;->c()V

    goto :goto_0

    .line 1517755
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 1517756
    monitor-enter p0

    .line 1517757
    :try_start_0
    iget-boolean v0, p0, LX/9dD;->e:Z

    if-eqz v0, :cond_2

    .line 1517758
    iget-object v0, p0, LX/9dD;->c:LX/1FJ;

    if-eqz v0, :cond_0

    .line 1517759
    iget-object v0, p0, LX/9dD;->c:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->close()V

    .line 1517760
    const/4 v0, 0x0

    iput-object v0, p0, LX/9dD;->c:LX/1FJ;

    .line 1517761
    :cond_0
    monitor-exit p0

    .line 1517762
    :cond_1
    :goto_0
    return-void

    .line 1517763
    :cond_2
    iget-object v0, p0, LX/9dD;->c:LX/1FJ;

    invoke-static {v0}, LX/1FJ;->b(LX/1FJ;)LX/1FJ;

    move-result-object v1

    .line 1517764
    iget-object v2, p0, LX/9dD;->d:Ljava/lang/String;

    .line 1517765
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1517766
    if-eqz v1, :cond_5

    invoke-virtual {v1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Ht;

    invoke-virtual {v0, p1, v2}, LX/8Ht;->a(Landroid/graphics/Bitmap;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/9dD;->f:LX/9dB;

    if-eqz v0, :cond_5

    .line 1517767
    iget-object v0, p0, LX/9dD;->f:LX/9dB;

    const/4 p1, 0x1

    .line 1517768
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1517769
    iget-object v2, v0, LX/9dB;->a:LX/9dC;

    iget-object v2, v2, LX/9dC;->c:LX/9dD;

    invoke-virtual {p0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1517770
    iget-object v2, v0, LX/9dB;->a:LX/9dC;

    .line 1517771
    iput-boolean p1, v2, LX/9dC;->f:Z

    .line 1517772
    :cond_3
    :goto_1
    invoke-virtual {v1}, LX/1FJ;->close()V

    .line 1517773
    :cond_4
    :goto_2
    if-eqz v1, :cond_1

    .line 1517774
    invoke-virtual {v1}, LX/1FJ;->close()V

    goto :goto_0

    .line 1517775
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1517776
    :cond_5
    iget-object v0, p0, LX/9dD;->f:LX/9dB;

    if-eqz v0, :cond_4

    .line 1517777
    iget-object v0, p0, LX/9dD;->f:LX/9dB;

    invoke-virtual {v0, p1}, LX/9dB;->a(Landroid/graphics/Bitmap;)V

    goto :goto_2

    .line 1517778
    :cond_6
    iget-object v2, v0, LX/9dB;->a:LX/9dC;

    iget-object v2, v2, LX/9dC;->d:LX/9dD;

    invoke-virtual {p0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1517779
    iget-object v2, v0, LX/9dB;->a:LX/9dC;

    .line 1517780
    iput-boolean p1, v2, LX/9dC;->g:Z

    .line 1517781
    goto :goto_1

    .line 1517782
    :cond_7
    iget-object v2, v0, LX/9dB;->a:LX/9dC;

    iget-object v2, v2, LX/9dC;->e:LX/9dD;

    invoke-virtual {p0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1517783
    iget-object v2, v0, LX/9dB;->a:LX/9dC;

    .line 1517784
    iput-boolean p1, v2, LX/9dC;->h:Z

    .line 1517785
    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1517786
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1517787
    monitor-enter p0

    .line 1517788
    :try_start_0
    iget-boolean v0, p0, LX/9dD;->e:Z

    if-eqz v0, :cond_0

    .line 1517789
    monitor-exit p0

    .line 1517790
    :goto_0
    return-void

    .line 1517791
    :cond_0
    iput-object p1, p0, LX/9dD;->d:Ljava/lang/String;

    .line 1517792
    new-instance v0, LX/1ed;

    invoke-direct {v0, p1}, LX/1ed;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/9dD;->g:LX/1bh;

    .line 1517793
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1517794
    invoke-virtual {p0}, LX/4fO;->c()V

    goto :goto_0

    .line 1517795
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1517796
    sget-object v0, LX/9dD;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1517797
    monitor-enter p0

    .line 1517798
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/9dD;->e:Z

    .line 1517799
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1517800
    invoke-virtual {p0}, LX/4fO;->c()V

    .line 1517801
    return-void

    .line 1517802
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final declared-synchronized e()V
    .locals 1

    .prologue
    .line 1517803
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LX/9dD;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1517804
    monitor-exit p0

    return-void

    .line 1517805
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
