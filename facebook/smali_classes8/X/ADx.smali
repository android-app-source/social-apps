.class public final LX/ADx;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:LX/2fp;

.field public final synthetic b:LX/1oP;

.field public final synthetic c:Lcom/facebook/story/UpdateTimelineAppCollectionParams;

.field public final synthetic d:LX/2fh;


# direct methods
.method public constructor <init>(LX/2fh;LX/2fp;LX/1oP;Lcom/facebook/story/UpdateTimelineAppCollectionParams;)V
    .locals 0

    .prologue
    .line 1645780
    iput-object p1, p0, LX/ADx;->d:LX/2fh;

    iput-object p2, p0, LX/ADx;->a:LX/2fp;

    iput-object p3, p0, LX/ADx;->b:LX/1oP;

    iput-object p4, p0, LX/ADx;->c:Lcom/facebook/story/UpdateTimelineAppCollectionParams;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 4

    .prologue
    .line 1645775
    iget-object v0, p0, LX/ADx;->a:LX/2fp;

    iget-object v1, p0, LX/ADx;->b:LX/1oP;

    iget-object v2, p0, LX/ADx;->c:Lcom/facebook/story/UpdateTimelineAppCollectionParams;

    .line 1645776
    iget-object v3, v2, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->c:LX/5vL;

    move-object v2, v3

    .line 1645777
    invoke-interface {v0, v1, v2}, LX/2fp;->a(LX/1oP;LX/5vL;)V

    .line 1645778
    iget-object v0, p0, LX/ADx;->d:LX/2fh;

    iget-object v0, v0, LX/2fh;->b:LX/03V;

    iget-object v1, p0, LX/ADx;->d:LX/2fh;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Failed to add item to Timeline Collection due to server exception %s."

    invoke-virtual {p1}, Lcom/facebook/fbservice/service/ServiceException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1645779
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1645773
    iget-object v0, p0, LX/ADx;->a:LX/2fp;

    invoke-interface {v0}, LX/2fp;->a()V

    .line 1645774
    return-void
.end method
