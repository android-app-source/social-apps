.class public final enum LX/8uh;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8uh;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8uh;

.field public static final enum ONE_LETTER:LX/8uh;

.field public static final enum TWO_LETTER:LX/8uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1415509
    new-instance v0, LX/8uh;

    const-string v1, "ONE_LETTER"

    invoke-direct {v0, v1, v2}, LX/8uh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8uh;->ONE_LETTER:LX/8uh;

    new-instance v0, LX/8uh;

    const-string v1, "TWO_LETTER"

    invoke-direct {v0, v1, v3}, LX/8uh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8uh;->TWO_LETTER:LX/8uh;

    .line 1415510
    const/4 v0, 0x2

    new-array v0, v0, [LX/8uh;

    sget-object v1, LX/8uh;->ONE_LETTER:LX/8uh;

    aput-object v1, v0, v2

    sget-object v1, LX/8uh;->TWO_LETTER:LX/8uh;

    aput-object v1, v0, v3

    sput-object v0, LX/8uh;->$VALUES:[LX/8uh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1415511
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8uh;
    .locals 1

    .prologue
    .line 1415512
    const-class v0, LX/8uh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8uh;

    return-object v0
.end method

.method public static values()[LX/8uh;
    .locals 1

    .prologue
    .line 1415513
    sget-object v0, LX/8uh;->$VALUES:[LX/8uh;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8uh;

    return-object v0
.end method
