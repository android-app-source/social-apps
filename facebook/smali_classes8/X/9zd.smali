.class public final LX/9zd;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1602128
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1602129
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1602130
    :goto_0
    return v1

    .line 1602131
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1602132
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1602133
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1602134
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1602135
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1602136
    const-string v4, "external_metadata"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1602137
    invoke-static {p0, p1}, LX/9zQ;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1602138
    :cond_2
    const-string v4, "native_metadata"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1602139
    invoke-static {p0, p1}, LX/9zQ;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1602140
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1602141
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1602142
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1602143
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1602144
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1602145
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1602146
    if-eqz v0, :cond_0

    .line 1602147
    const-string v1, "external_metadata"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1602148
    invoke-static {p0, v0, p2, p3}, LX/9zQ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1602149
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1602150
    if-eqz v0, :cond_1

    .line 1602151
    const-string v1, "native_metadata"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1602152
    invoke-static {p0, v0, p2, p3}, LX/9zQ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1602153
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1602154
    return-void
.end method
