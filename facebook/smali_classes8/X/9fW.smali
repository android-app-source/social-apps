.class public final LX/9fW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9de;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/editgallery/TextEditController;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/editgallery/TextEditController;)V
    .locals 0

    .prologue
    .line 1522080
    iput-object p1, p0, LX/9fW;->a:Lcom/facebook/photos/editgallery/TextEditController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1522068
    iget-object v0, p0, LX/9fW;->a:Lcom/facebook/photos/editgallery/TextEditController;

    iget-object v1, p0, LX/9fW;->a:Lcom/facebook/photos/editgallery/TextEditController;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/TextEditController;->n:LX/9e1;

    .line 1522069
    iget-object v2, v1, LX/9e1;->a:Lcom/facebook/photos/creativeediting/model/TextParams;

    move-object v1, v2

    .line 1522070
    invoke-static {v0, v1}, Lcom/facebook/photos/editgallery/TextEditController;->a$redex0(Lcom/facebook/photos/editgallery/TextEditController;Lcom/facebook/photos/creativeediting/model/TextParams;)V

    .line 1522071
    iget-object v0, p0, LX/9fW;->a:Lcom/facebook/photos/editgallery/TextEditController;

    iget-object v1, p0, LX/9fW;->a:Lcom/facebook/photos/editgallery/TextEditController;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/TextEditController;->l:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/photos/editgallery/TextEditController;->a$redex0(Lcom/facebook/photos/editgallery/TextEditController;Ljava/lang/String;)V

    .line 1522072
    iget-object v0, p0, LX/9fW;->a:Lcom/facebook/photos/editgallery/TextEditController;

    sget-object v1, LX/9fY;->TEXT_EDIT:LX/9fY;

    .line 1522073
    iput-object v1, v0, Lcom/facebook/photos/editgallery/TextEditController;->u:LX/9fY;

    .line 1522074
    iget-object v0, p0, LX/9fW;->a:Lcom/facebook/photos/editgallery/TextEditController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/TextEditController;->o:Lcom/facebook/photos/editgallery/EditableOverlayContainerView;

    invoke-virtual {v0}, Lcom/facebook/photos/editgallery/EditableOverlayContainerView;->getOverlayParamsForOriginalPhoto()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1522075
    iget-object v0, p0, LX/9fW;->a:Lcom/facebook/photos/editgallery/TextEditController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/TextEditController;->q:Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/facebook/photos/creativeediting/analytics/TextOnPhotosLoggingParams;->b:Z

    .line 1522076
    :cond_0
    iget-object v0, p0, LX/9fW;->a:Lcom/facebook/photos/editgallery/TextEditController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/TextEditController;->n:LX/9e1;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/9e1;->setTextParams(Lcom/facebook/photos/creativeediting/model/TextParams;)V

    .line 1522077
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 1522078
    iget-object v0, p0, LX/9fW;->a:Lcom/facebook/photos/editgallery/TextEditController;

    invoke-static {v0, p1}, Lcom/facebook/photos/editgallery/TextEditController;->a$redex0(Lcom/facebook/photos/editgallery/TextEditController;I)V

    .line 1522079
    return-void
.end method
