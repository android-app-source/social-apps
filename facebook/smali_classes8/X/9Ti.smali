.class public final enum LX/9Ti;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9Ti;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9Ti;

.field public static final enum FRIENDABLE_CONTACTS_API:LX/9Ti;

.field public static final enum FRIEND_FINDER_API:LX/9Ti;

.field public static final enum INVITABLE_CONTACTS_API:LX/9Ti;


# instance fields
.field private final mApiType:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1496634
    new-instance v0, LX/9Ti;

    const-string v1, "FRIEND_FINDER_API"

    const-string v2, "friend_finder_2.0"

    invoke-direct {v0, v1, v3, v2}, LX/9Ti;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Ti;->FRIEND_FINDER_API:LX/9Ti;

    .line 1496635
    new-instance v0, LX/9Ti;

    const-string v1, "FRIENDABLE_CONTACTS_API"

    const-string v2, "friendable_contacts"

    invoke-direct {v0, v1, v4, v2}, LX/9Ti;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Ti;->FRIENDABLE_CONTACTS_API:LX/9Ti;

    .line 1496636
    new-instance v0, LX/9Ti;

    const-string v1, "INVITABLE_CONTACTS_API"

    const-string v2, "invitable_contacts"

    invoke-direct {v0, v1, v5, v2}, LX/9Ti;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Ti;->INVITABLE_CONTACTS_API:LX/9Ti;

    .line 1496637
    const/4 v0, 0x3

    new-array v0, v0, [LX/9Ti;

    sget-object v1, LX/9Ti;->FRIEND_FINDER_API:LX/9Ti;

    aput-object v1, v0, v3

    sget-object v1, LX/9Ti;->FRIENDABLE_CONTACTS_API:LX/9Ti;

    aput-object v1, v0, v4

    sget-object v1, LX/9Ti;->INVITABLE_CONTACTS_API:LX/9Ti;

    aput-object v1, v0, v5

    sput-object v0, LX/9Ti;->$VALUES:[LX/9Ti;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1496641
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1496642
    iput-object p3, p0, LX/9Ti;->mApiType:Ljava/lang/String;

    .line 1496643
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9Ti;
    .locals 1

    .prologue
    .line 1496640
    const-class v0, LX/9Ti;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9Ti;

    return-object v0
.end method

.method public static values()[LX/9Ti;
    .locals 1

    .prologue
    .line 1496639
    sget-object v0, LX/9Ti;->$VALUES:[LX/9Ti;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9Ti;

    return-object v0
.end method


# virtual methods
.method public final getApiName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1496638
    iget-object v0, p0, LX/9Ti;->mApiType:Ljava/lang/String;

    return-object v0
.end method
