.class public final LX/ATJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7yZ;


# instance fields
.field public final synthetic a:LX/0il;

.field public final synthetic b:Ljava/util/List;

.field public final synthetic c:LX/ATO;


# direct methods
.method public constructor <init>(LX/ATO;LX/0il;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 1675413
    iput-object p1, p0, LX/ATJ;->c:LX/ATO;

    iput-object p2, p0, LX/ATJ;->a:LX/0il;

    iput-object p3, p0, LX/ATJ;->b:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/7ye;)V
    .locals 6

    .prologue
    .line 1675414
    iget-object v1, p0, LX/ATJ;->c:LX/ATO;

    iget-object v0, p0, LX/ATJ;->a:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->b(LX/0Px;)LX/0Px;

    move-result-object v0

    sget-object v2, LX/03R;->UNSET:LX/03R;

    invoke-virtual {v1, v0, v2}, LX/ATO;->a(LX/0Px;LX/03R;)V

    .line 1675415
    iget-object v0, p0, LX/ATJ;->c:LX/ATO;

    iget-object v1, p0, LX/ATJ;->b:Ljava/util/List;

    .line 1675416
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/ipc/media/MediaItem;

    .line 1675417
    instance-of v4, v2, Lcom/facebook/photos/base/media/PhotoItem;

    if-eqz v4, :cond_0

    .line 1675418
    check-cast v2, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1675419
    iget-object v4, v2, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    move-object v2, v4

    .line 1675420
    check-cast v2, Lcom/facebook/photos/base/photos/LocalPhoto;

    .line 1675421
    iget-object v4, v0, LX/ATO;->ai:LX/75F;

    invoke-virtual {v4, v2}, LX/75F;->c(LX/74x;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1675422
    const/4 v2, 0x0

    .line 1675423
    :goto_0
    move v0, v2

    .line 1675424
    if-eqz v0, :cond_1

    .line 1675425
    iget-object v0, p0, LX/ATJ;->c:LX/ATO;

    iget-object v0, v0, LX/ATO;->ak:LX/1EZ;

    iget-object v1, p0, LX/ATJ;->a:LX/0il;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/7kq;->e(LX/0Px;)LX/0Px;

    move-result-object v1

    iget-object v2, p0, LX/ATJ;->a:LX/0il;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0io;

    check-cast v2, LX/0j0;

    invoke-interface {v2}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/ATJ;->a:LX/0il;

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0io;

    check-cast v3, LX/0j4;

    invoke-interface {v3}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v3

    invoke-static {v3}, LX/2rm;->a(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, LX/ATJ;->a:LX/0il;

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0io;

    check-cast v3, LX/0j4;

    invoke-interface {v3}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v3

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/ipc/composer/intent/ComposerPageData;

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getPostAsPageViewerContext()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v3

    :goto_1
    iget-object v4, p0, LX/ATJ;->a:LX/0il;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0io;

    check-cast v4, LX/0j6;

    invoke-interface {v4}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v4

    iget-wide v4, v4, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-virtual/range {v0 .. v5}, LX/1EZ;->a(LX/0Px;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;J)V

    .line 1675426
    iget-object v0, p0, LX/ATJ;->c:LX/ATO;

    iget-object v0, v0, LX/ATO;->C:LX/7yZ;

    if-eqz v0, :cond_1

    .line 1675427
    iget-object v0, p0, LX/ATJ;->c:LX/ATO;

    iget-object v0, v0, LX/ATO;->C:LX/7yZ;

    invoke-interface {v0, p1}, LX/7yZ;->a(LX/7ye;)V

    .line 1675428
    :cond_1
    return-void

    .line 1675429
    :cond_2
    const/4 v3, 0x0

    goto :goto_1

    :cond_3
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public final b(LX/7ye;)V
    .locals 3

    .prologue
    .line 1675430
    iget-object v0, p0, LX/ATJ;->a:LX/0il;

    check-cast v0, LX/0ik;

    invoke-interface {v0}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5Qw;

    check-cast v0, LX/5Qz;

    invoke-interface {v0}, LX/5Qz;->C()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1675431
    iget-object v0, p1, LX/7ye;->d:Ljava/util/List;

    move-object v0, v0

    .line 1675432
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1675433
    :cond_0
    :goto_0
    return-void

    .line 1675434
    :cond_1
    iget-object v0, p0, LX/ATJ;->c:LX/ATO;

    iget-object v1, v0, LX/ATO;->al:LX/9iZ;

    iget-object v0, p0, LX/ATJ;->c:LX/ATO;

    iget-object v0, v0, LX/ATO;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v0, p0, LX/ATJ;->a:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->b(LX/0Px;)LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v2, p1, v0}, LX/9iZ;->a(Landroid/content/Context;LX/7ye;LX/0Px;)V

    .line 1675435
    iget-object v1, p0, LX/ATJ;->c:LX/ATO;

    iget-object v0, p0, LX/ATJ;->a:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->b(LX/0Px;)LX/0Px;

    move-result-object v0

    sget-object v2, LX/03R;->NO:LX/03R;

    invoke-virtual {v1, v0, v2}, LX/ATO;->a(LX/0Px;LX/03R;)V

    .line 1675436
    iget-object v0, p0, LX/ATJ;->c:LX/ATO;

    iget-object v0, v0, LX/ATO;->C:LX/7yZ;

    if-eqz v0, :cond_0

    .line 1675437
    iget-object v0, p0, LX/ATJ;->c:LX/ATO;

    iget-object v0, v0, LX/ATO;->C:LX/7yZ;

    invoke-interface {v0, p1}, LX/7yZ;->b(LX/7ye;)V

    goto :goto_0
.end method
