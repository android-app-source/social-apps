.class public LX/94x;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static G:LX/0Xm;

.field public static final a:Ljava/lang/String;

.field private static final b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/contacts/ContactSurface;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final A:LX/0Xl;

.field private final B:LX/03V;

.field private final C:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final D:LX/30I;

.field private final E:LX/2UP;

.field private F:Z

.field private final c:LX/2UR;

.field private final d:LX/95J;

.field public final e:LX/11H;

.field private final f:LX/6O0;

.field private final g:LX/6O2;

.field private final h:LX/6Nx;

.field public final i:LX/95N;

.field public final j:LX/2Iv;

.field public final k:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

.field private final l:LX/3fg;

.field public final m:LX/2UQ;

.field private final n:LX/959;

.field private final o:LX/958;

.field private final p:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final q:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final r:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final s:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final t:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final u:LX/3Lz;

.field private final v:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final w:LX/30b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/30b",
            "<",
            "Lcom/facebook/contacts/model/PhonebookContact;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final x:LX/30b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/30b",
            "<",
            "LX/95A;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final y:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final z:LX/0SG;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1436270
    const-class v0, LX/94x;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/94x;->a:Ljava/lang/String;

    .line 1436271
    sget-object v0, Lcom/facebook/contacts/ContactSurface;->GROWTH_CONTACT_IMPORTER:Lcom/facebook/contacts/ContactSurface;

    sget-object v1, Lcom/facebook/contacts/ContactSurface;->MESSENGER:Lcom/facebook/contacts/ContactSurface;

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/94x;->b:LX/0Rf;

    return-void
.end method

.method public constructor <init>(LX/2UR;LX/95J;LX/11H;LX/6O0;LX/6O2;LX/6Nx;LX/95N;LX/2Iv;Lcom/facebook/contacts/background/AddressBookPeriodicRunner;LX/3fg;LX/2UQ;LX/959;LX/958;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/3Lz;LX/0Or;LX/0SG;LX/0Xl;LX/03V;LX/0Or;LX/30I;LX/2UP;)V
    .locals 2
    .param p15    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p16    # LX/0Or;
        .annotation runtime Lcom/facebook/contacts/protocol/annotations/IsContactExtendedFieldsUploadEnabled;
        .end annotation
    .end param
    .param p17    # LX/0Or;
        .annotation runtime Lcom/facebook/contacts/upload/annotation/IsContactsUploadLimitEnabled;
        .end annotation
    .end param
    .param p18    # LX/0Or;
        .annotation runtime Lcom/facebook/contacts/protocol/annotations/IsMessengerEmailUploadEnabled;
        .end annotation
    .end param
    .param p20    # LX/0Or;
        .annotation runtime Lcom/facebook/contacts/upload/annotation/InContactsUploadDryRunMode;
        .end annotation
    .end param
    .param p22    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p24    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/contacts/abtest/ShowAllContactsInPeopleTab;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2UR;",
            "LX/95J;",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            "LX/6O0;",
            "LX/6O2;",
            "LX/6Nx;",
            "LX/95N;",
            "LX/2Iv;",
            "Lcom/facebook/contacts/background/AddressBookPeriodicRunner;",
            "LX/3fg;",
            "LX/2UQ;",
            "LX/959;",
            "LX/958;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/3Lz;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0SG;",
            "LX/0Xl;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/30I;",
            "LX/2UP;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1436238
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1436239
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/94x;->F:Z

    .line 1436240
    iput-object p1, p0, LX/94x;->c:LX/2UR;

    .line 1436241
    iput-object p2, p0, LX/94x;->d:LX/95J;

    .line 1436242
    iput-object p3, p0, LX/94x;->e:LX/11H;

    .line 1436243
    iput-object p4, p0, LX/94x;->f:LX/6O0;

    .line 1436244
    iput-object p5, p0, LX/94x;->g:LX/6O2;

    .line 1436245
    iput-object p6, p0, LX/94x;->h:LX/6Nx;

    .line 1436246
    iput-object p7, p0, LX/94x;->i:LX/95N;

    .line 1436247
    iput-object p8, p0, LX/94x;->j:LX/2Iv;

    .line 1436248
    iput-object p9, p0, LX/94x;->k:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    .line 1436249
    iput-object p10, p0, LX/94x;->l:LX/3fg;

    .line 1436250
    iput-object p11, p0, LX/94x;->m:LX/2UQ;

    .line 1436251
    move-object/from16 v0, p14

    iput-object v0, p0, LX/94x;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1436252
    iput-object p12, p0, LX/94x;->n:LX/959;

    .line 1436253
    iput-object p13, p0, LX/94x;->o:LX/958;

    .line 1436254
    move-object/from16 v0, p15

    iput-object v0, p0, LX/94x;->q:LX/0Or;

    .line 1436255
    move-object/from16 v0, p16

    iput-object v0, p0, LX/94x;->r:LX/0Or;

    .line 1436256
    move-object/from16 v0, p17

    iput-object v0, p0, LX/94x;->s:LX/0Or;

    .line 1436257
    move-object/from16 v0, p18

    iput-object v0, p0, LX/94x;->t:LX/0Or;

    .line 1436258
    move-object/from16 v0, p19

    iput-object v0, p0, LX/94x;->u:LX/3Lz;

    .line 1436259
    move-object/from16 v0, p20

    iput-object v0, p0, LX/94x;->v:LX/0Or;

    .line 1436260
    move-object/from16 v0, p21

    iput-object v0, p0, LX/94x;->z:LX/0SG;

    .line 1436261
    move-object/from16 v0, p22

    iput-object v0, p0, LX/94x;->A:LX/0Xl;

    .line 1436262
    move-object/from16 v0, p23

    iput-object v0, p0, LX/94x;->B:LX/03V;

    .line 1436263
    move-object/from16 v0, p24

    iput-object v0, p0, LX/94x;->C:LX/0Or;

    .line 1436264
    move-object/from16 v0, p25

    iput-object v0, p0, LX/94x;->D:LX/30I;

    .line 1436265
    move-object/from16 v0, p26

    iput-object v0, p0, LX/94x;->E:LX/2UP;

    .line 1436266
    new-instance v1, LX/94r;

    invoke-direct {v1, p0}, LX/94r;-><init>(LX/94x;)V

    iput-object v1, p0, LX/94x;->w:LX/30b;

    .line 1436267
    new-instance v1, LX/94s;

    invoke-direct {v1, p0}, LX/94s;-><init>(LX/94x;)V

    iput-object v1, p0, LX/94x;->x:LX/30b;

    .line 1436268
    new-instance v1, LX/94t;

    invoke-direct {v1, p0}, LX/94t;-><init>(LX/94x;)V

    iput-object v1, p0, LX/94x;->y:Ljava/util/Comparator;

    .line 1436269
    return-void
.end method

.method private static a(Lcom/facebook/contacts/server/UploadBulkContactsResult;)LX/0Px;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/contacts/server/UploadBulkContactsResult;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/server/UploadBulkContactChangeResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1436223
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1436224
    iget-object v0, p0, Lcom/facebook/contacts/server/UploadBulkContactsResult;->b:LX/0Px;

    move-object v3, v0

    .line 1436225
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/server/UploadBulkContactChangeResult;

    .line 1436226
    sget-object v5, LX/94u;->c:[I

    .line 1436227
    iget-object v6, v0, Lcom/facebook/contacts/server/UploadBulkContactChangeResult;->a:LX/6OQ;

    move-object v6, v6

    .line 1436228
    invoke-virtual {v6}, LX/6OQ;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 1436229
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1436230
    :pswitch_0
    iget-object v5, v0, Lcom/facebook/contacts/server/UploadBulkContactChangeResult;->e:LX/6OP;

    move-object v5, v5

    .line 1436231
    sget-object v6, LX/94u;->e:[I

    invoke-virtual {v5}, LX/6OP;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_1

    .line 1436232
    const/4 v6, 0x0

    :goto_2
    move v6, v6

    .line 1436233
    if-eqz v6, :cond_0

    .line 1436234
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1436235
    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Not including contact "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, ", confidence "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " too low."

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1436236
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    .line 1436237
    :pswitch_1
    const/4 v6, 0x1

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private a(Lcom/facebook/contacts/server/UploadBulkContactsResult;LX/94v;)LX/0Rf;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/contacts/server/UploadBulkContactsResult;",
            "LX/94v;",
            ")",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1436208
    invoke-static {p1}, LX/94x;->a(Lcom/facebook/contacts/server/UploadBulkContactsResult;)LX/0Px;

    move-result-object v4

    .line 1436209
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v5

    .line 1436210
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v6

    move v3, v1

    :goto_0
    if-ge v3, v6, :cond_1

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/server/UploadBulkContactChangeResult;

    .line 1436211
    const/4 v2, 0x0

    .line 1436212
    sget-object v7, LX/94u;->d:[I

    invoke-virtual {p2}, LX/94v;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    .line 1436213
    :goto_1
    if-eqz v2, :cond_0

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1436214
    invoke-virtual {v5, v2}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1436215
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 1436216
    :pswitch_0
    iget-object v2, v0, Lcom/facebook/contacts/server/UploadBulkContactChangeResult;->b:Ljava/lang/String;

    move-object v0, v2

    .line 1436217
    move-object v2, v0

    .line 1436218
    goto :goto_1

    .line 1436219
    :pswitch_1
    iget-object v2, v0, Lcom/facebook/contacts/server/UploadBulkContactChangeResult;->c:Ljava/lang/String;

    move-object v0, v2

    .line 1436220
    move-object v2, v0

    goto :goto_1

    :cond_0
    move v0, v1

    .line 1436221
    goto :goto_2

    .line 1436222
    :cond_1
    invoke-virtual {v5}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(LX/94x;LX/443;Lcom/facebook/contacts/ContactSurface;Z)LX/94l;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/443",
            "<",
            "Lcom/facebook/contacts/model/PhonebookContact;",
            "LX/95A;",
            ">;",
            "Lcom/facebook/contacts/ContactSurface;",
            "Z)",
            "LX/94l;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1436119
    iget-object v0, p0, LX/94x;->v:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1436120
    invoke-static {}, LX/94l;->newBuilder()LX/94m;

    move-result-object v0

    sget-object v1, LX/94k;->NONE:LX/94k;

    .line 1436121
    iput-object v1, v0, LX/94m;->a:LX/94k;

    .line 1436122
    move-object v0, v0

    .line 1436123
    iput-object v2, v0, LX/94m;->c:Lcom/facebook/contacts/model/PhonebookContact;

    .line 1436124
    move-object v0, v0

    .line 1436125
    iput-object v2, v0, LX/94m;->d:LX/95A;

    .line 1436126
    move-object v0, v0

    .line 1436127
    invoke-virtual {v0}, LX/94m;->a()LX/94l;

    move-result-object v0

    .line 1436128
    :goto_0
    return-object v0

    .line 1436129
    :cond_0
    if-eqz p3, :cond_1

    move-object v1, v2

    .line 1436130
    :goto_1
    iget-object v0, p1, LX/443;->b:Ljava/lang/Object;

    check-cast v0, LX/95A;

    .line 1436131
    if-nez v1, :cond_2

    .line 1436132
    invoke-static {}, LX/94l;->newBuilder()LX/94m;

    move-result-object v1

    sget-object v3, LX/94k;->DELETE:LX/94k;

    .line 1436133
    iput-object v3, v1, LX/94m;->a:LX/94k;

    .line 1436134
    move-object v1, v1

    .line 1436135
    iget-wide v4, v0, LX/95A;->a:J

    .line 1436136
    iput-wide v4, v1, LX/94m;->b:J

    .line 1436137
    move-object v0, v1

    .line 1436138
    iput-object v2, v0, LX/94m;->c:Lcom/facebook/contacts/model/PhonebookContact;

    .line 1436139
    move-object v0, v0

    .line 1436140
    iput-object v2, v0, LX/94m;->d:LX/95A;

    .line 1436141
    move-object v0, v0

    .line 1436142
    invoke-virtual {v0}, LX/94m;->a()LX/94l;

    move-result-object v0

    goto :goto_0

    .line 1436143
    :cond_1
    iget-object v0, p1, LX/443;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/contacts/model/PhonebookContact;

    move-object v1, v0

    goto :goto_1

    .line 1436144
    :cond_2
    if-nez v0, :cond_4

    .line 1436145
    invoke-static {p0, v1, p2}, LX/94x;->a(LX/94x;Lcom/facebook/contacts/model/PhonebookContact;Lcom/facebook/contacts/ContactSurface;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1436146
    invoke-static {}, LX/94l;->newBuilder()LX/94m;

    move-result-object v0

    sget-object v3, LX/94k;->NONE:LX/94k;

    .line 1436147
    iput-object v3, v0, LX/94m;->a:LX/94k;

    .line 1436148
    move-object v0, v0

    .line 1436149
    iget-object v3, v1, Lcom/facebook/contacts/model/PhonebookContact;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1436150
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 1436151
    iput-wide v4, v0, LX/94m;->b:J

    .line 1436152
    move-object v0, v0

    .line 1436153
    iput-object v1, v0, LX/94m;->c:Lcom/facebook/contacts/model/PhonebookContact;

    .line 1436154
    move-object v0, v0

    .line 1436155
    iput-object v2, v0, LX/94m;->d:LX/95A;

    .line 1436156
    move-object v0, v0

    .line 1436157
    invoke-virtual {v0}, LX/94m;->a()LX/94l;

    move-result-object v0

    goto :goto_0

    .line 1436158
    :cond_3
    invoke-static {}, LX/94l;->newBuilder()LX/94m;

    move-result-object v0

    sget-object v2, LX/94k;->ADD:LX/94k;

    .line 1436159
    iput-object v2, v0, LX/94m;->a:LX/94k;

    .line 1436160
    move-object v0, v0

    .line 1436161
    iget-object v2, v1, Lcom/facebook/contacts/model/PhonebookContact;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1436162
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1436163
    iput-wide v2, v0, LX/94m;->b:J

    .line 1436164
    move-object v0, v0

    .line 1436165
    iput-object v1, v0, LX/94m;->c:Lcom/facebook/contacts/model/PhonebookContact;

    .line 1436166
    move-object v0, v0

    .line 1436167
    invoke-static {p0, p2, v1}, LX/94x;->a(LX/94x;Lcom/facebook/contacts/ContactSurface;Lcom/facebook/contacts/model/PhonebookContact;)LX/95A;

    move-result-object v1

    .line 1436168
    iput-object v1, v0, LX/94m;->d:LX/95A;

    .line 1436169
    move-object v0, v0

    .line 1436170
    invoke-virtual {v0}, LX/94m;->a()LX/94l;

    move-result-object v0

    goto :goto_0

    .line 1436171
    :cond_4
    invoke-static {p0, p2, v1}, LX/94x;->b(LX/94x;Lcom/facebook/contacts/ContactSurface;Lcom/facebook/contacts/model/PhonebookContact;)Ljava/lang/String;

    move-result-object v3

    .line 1436172
    iget-object v4, v0, LX/95A;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1436173
    invoke-static {}, LX/94l;->newBuilder()LX/94m;

    move-result-object v2

    sget-object v3, LX/94k;->NONE:LX/94k;

    .line 1436174
    iput-object v3, v2, LX/94m;->a:LX/94k;

    .line 1436175
    move-object v2, v2

    .line 1436176
    iget-wide v4, v0, LX/95A;->a:J

    .line 1436177
    iput-wide v4, v2, LX/94m;->b:J

    .line 1436178
    move-object v2, v2

    .line 1436179
    iput-object v1, v2, LX/94m;->c:Lcom/facebook/contacts/model/PhonebookContact;

    .line 1436180
    move-object v1, v2

    .line 1436181
    iput-object v0, v1, LX/94m;->d:LX/95A;

    .line 1436182
    move-object v0, v1

    .line 1436183
    invoke-virtual {v0}, LX/94m;->a()LX/94l;

    move-result-object v0

    goto/16 :goto_0

    .line 1436184
    :cond_5
    invoke-static {p0, v1, p2}, LX/94x;->a(LX/94x;Lcom/facebook/contacts/model/PhonebookContact;Lcom/facebook/contacts/ContactSurface;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 1436185
    invoke-static {}, LX/94l;->newBuilder()LX/94m;

    move-result-object v1

    sget-object v3, LX/94k;->DELETE:LX/94k;

    .line 1436186
    iput-object v3, v1, LX/94m;->a:LX/94k;

    .line 1436187
    move-object v1, v1

    .line 1436188
    iget-wide v4, v0, LX/95A;->a:J

    .line 1436189
    iput-wide v4, v1, LX/94m;->b:J

    .line 1436190
    move-object v0, v1

    .line 1436191
    iput-object v2, v0, LX/94m;->c:Lcom/facebook/contacts/model/PhonebookContact;

    .line 1436192
    move-object v0, v0

    .line 1436193
    iput-object v2, v0, LX/94m;->d:LX/95A;

    .line 1436194
    move-object v0, v0

    .line 1436195
    invoke-virtual {v0}, LX/94m;->a()LX/94l;

    move-result-object v0

    goto/16 :goto_0

    .line 1436196
    :cond_6
    invoke-static {}, LX/94l;->newBuilder()LX/94m;

    move-result-object v2

    sget-object v3, LX/94k;->UPDATE:LX/94k;

    .line 1436197
    iput-object v3, v2, LX/94m;->a:LX/94k;

    .line 1436198
    move-object v2, v2

    .line 1436199
    iget-wide v4, v0, LX/95A;->a:J

    .line 1436200
    iput-wide v4, v2, LX/94m;->b:J

    .line 1436201
    move-object v0, v2

    .line 1436202
    iput-object v1, v0, LX/94m;->c:Lcom/facebook/contacts/model/PhonebookContact;

    .line 1436203
    move-object v0, v0

    .line 1436204
    invoke-static {p0, p2, v1}, LX/94x;->a(LX/94x;Lcom/facebook/contacts/ContactSurface;Lcom/facebook/contacts/model/PhonebookContact;)LX/95A;

    move-result-object v1

    .line 1436205
    iput-object v1, v0, LX/94m;->d:LX/95A;

    .line 1436206
    move-object v0, v0

    .line 1436207
    invoke-virtual {v0}, LX/94m;->a()LX/94l;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public static a(LX/0QB;)LX/94x;
    .locals 3

    .prologue
    .line 1436111
    const-class v1, LX/94x;

    monitor-enter v1

    .line 1436112
    :try_start_0
    sget-object v0, LX/94x;->G:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1436113
    sput-object v2, LX/94x;->G:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1436114
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1436115
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/94x;->b(LX/0QB;)LX/94x;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1436116
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/94x;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1436117
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1436118
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(LX/94x;Lcom/facebook/contacts/ContactSurface;Lcom/facebook/contacts/model/PhonebookContact;)LX/95A;
    .locals 4

    .prologue
    .line 1436108
    new-instance v0, LX/95A;

    .line 1436109
    iget-object v1, p2, Lcom/facebook/contacts/model/PhonebookContact;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1436110
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {p0, p1, p2}, LX/94x;->b(LX/94x;Lcom/facebook/contacts/ContactSurface;Lcom/facebook/contacts/model/PhonebookContact;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v3, v1}, LX/95A;-><init>(JLjava/lang/String;)V

    return-object v0
.end method

.method private a(Ljava/lang/String;LX/0P1;LX/0Px;Lcom/facebook/contacts/ContactSurface;)Lcom/facebook/contacts/server/UploadBulkFetchContactsResult;
    .locals 9
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/contacts/server/UploadBulkContactChange;",
            ">;",
            "LX/0Px",
            "<",
            "LX/95C;",
            ">;",
            "Lcom/facebook/contacts/ContactSurface;",
            ")",
            "Lcom/facebook/contacts/server/UploadBulkFetchContactsResult;"
        }
    .end annotation

    .prologue
    .line 1436070
    const-string v0, "uploadContactBatchForMessaging (%d changes)"

    invoke-virtual {p2}, LX/0P1;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x2edb691a

    invoke-static {v0, v1, v2}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 1436071
    iget-object v0, p0, LX/94x;->E:LX/2UP;

    sget-object v1, LX/95K;->BATCH_UPLOAD_START:LX/95K;

    invoke-virtual {v0, v1}, LX/2UP;->a(LX/95K;)V

    .line 1436072
    const/4 v2, 0x0

    .line 1436073
    const/4 v1, 0x0

    .line 1436074
    const/4 v0, 0x0

    move v3, v1

    move-object v4, v2

    move v2, v0

    .line 1436075
    :goto_0
    if-nez v3, :cond_2

    const/4 v0, 0x3

    if-ge v2, v0, :cond_2

    .line 1436076
    :try_start_0
    invoke-virtual {p2}, LX/0P1;->size()I

    .line 1436077
    new-instance v0, Lcom/facebook/contacts/server/UploadBulkContactsParams;

    invoke-virtual {p2}, LX/0P1;->values()LX/0Py;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, p1, v1, p4}, Lcom/facebook/contacts/server/UploadBulkContactsParams;-><init>(Ljava/lang/String;LX/0Px;Lcom/facebook/contacts/ContactSurface;)V

    .line 1436078
    iget-object v1, p0, LX/94x;->e:LX/11H;

    iget-object v5, p0, LX/94x;->f:LX/6O0;

    invoke-virtual {v1, v5, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/server/UploadBulkContactsResult;

    .line 1436079
    invoke-direct {p0, p3}, LX/94x;->a(LX/0Px;)V

    .line 1436080
    sget-object v1, LX/94v;->REMOTE_CONTACT_IDS:LX/94v;

    invoke-direct {p0, v0, v1}, LX/94x;->a(Lcom/facebook/contacts/server/UploadBulkContactsResult;LX/94v;)LX/0Rf;

    move-result-object v5

    .line 1436081
    const/4 v1, 0x0

    .line 1436082
    invoke-virtual {v5}, LX/0Rf;->size()I

    move-result v6

    if-lez v6, :cond_0

    .line 1436083
    new-instance v1, Lcom/facebook/contacts/server/FetchContactsParams;

    invoke-direct {v1, v5}, Lcom/facebook/contacts/server/FetchContactsParams;-><init>(LX/0Rf;)V

    .line 1436084
    iget-object v5, p0, LX/94x;->e:LX/11H;

    iget-object v6, p0, LX/94x;->h:LX/6Nx;

    invoke-virtual {v5, v6, v1}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/contacts/server/FetchContactsResult;

    .line 1436085
    iget-object v5, p0, LX/94x;->l:LX/3fg;

    .line 1436086
    iget-object v6, v1, Lcom/facebook/contacts/server/FetchContactsResult;->a:LX/0Px;

    move-object v6, v6

    .line 1436087
    invoke-direct {p0, v6}, LX/94x;->b(LX/0Px;)LX/0Py;

    move-result-object v6

    sget-object v7, LX/3gm;->INSERT:LX/3gm;

    .line 1436088
    iget-object v8, v1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v8, v8

    .line 1436089
    invoke-virtual {v5, v6, v7, v8}, LX/3fg;->a(LX/0Py;LX/3gm;LX/0ta;)V

    .line 1436090
    invoke-direct {p0, p2, v0}, LX/94x;->a(LX/0P1;Lcom/facebook/contacts/server/UploadBulkContactsResult;)V

    .line 1436091
    :cond_0
    new-instance v5, Lcom/facebook/contacts/server/UploadBulkFetchContactsResult;

    invoke-direct {v5, v0, v1}, Lcom/facebook/contacts/server/UploadBulkFetchContactsResult;-><init>(Lcom/facebook/contacts/server/UploadBulkContactsResult;Lcom/facebook/contacts/server/FetchContactsResult;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1436092
    const/4 v0, 0x1

    move v3, v0

    move-object v4, v5

    .line 1436093
    goto :goto_0

    .line 1436094
    :catch_0
    move-exception v0

    .line 1436095
    add-int/lit8 v1, v2, 0x1

    .line 1436096
    :try_start_1
    sget-object v2, LX/94x;->a:Ljava/lang/String;

    const-string v5, "Failed to upload contact batch, (try %s of %s ), error: %s "

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const/4 v8, 0x3

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    aput-object v0, v6, v7

    invoke-static {v2, v5, v6}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1436097
    iget-object v2, p0, LX/94x;->E:LX/2UP;

    sget-object v5, LX/95K;->BATCH_UPLOAD_ATTEMPT_ERROR:LX/95K;

    invoke-virtual {v2, v5}, LX/2UP;->a(LX/95K;)V

    .line 1436098
    const/4 v2, 0x3

    if-lt v1, v2, :cond_1

    .line 1436099
    sget-object v1, LX/94x;->a:Ljava/lang/String;

    const-string v2, "Giving up uploading contact batch."

    invoke-static {v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1436100
    iget-object v1, p0, LX/94x;->E:LX/2UP;

    sget-object v2, LX/95K;->BATCH_UPLOAD_FAIL:LX/95K;

    invoke-virtual {v1, v2}, LX/2UP;->a(LX/95K;)V

    .line 1436101
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1436102
    :catchall_0
    move-exception v0

    const v1, 0x6a710e2e

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_1
    move v2, v1

    .line 1436103
    goto/16 :goto_0

    .line 1436104
    :cond_2
    if-eqz v4, :cond_3

    const/4 v0, 0x1

    :goto_1
    :try_start_2
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1436105
    iget-object v0, p0, LX/94x;->E:LX/2UP;

    sget-object v1, LX/95K;->BATCH_UPLOAD_FINISH:LX/95K;

    invoke-virtual {v0, v1}, LX/2UP;->a(LX/95K;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1436106
    const v0, 0x3ebc84c7

    invoke-static {v0}, LX/02m;->a(I)V

    return-object v4

    .line 1436107
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(LX/1qK;LX/6Mz;LX/2Ta;LX/94w;ILjava/lang/String;Lcom/facebook/contacts/ContactSurface;I)Lcom/facebook/contacts/server/UploadContactsResult;
    .locals 26
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1qK;",
            "LX/6Mz;",
            "LX/2Ta",
            "<",
            "LX/95A;",
            ">;",
            "LX/94w;",
            "I",
            "Ljava/lang/String;",
            "Lcom/facebook/contacts/ContactSurface;",
            "I)",
            "Lcom/facebook/contacts/server/UploadContactsResult;"
        }
    .end annotation

    .prologue
    .line 1435812
    move-object/from16 v0, p0

    iget-object v4, v0, LX/94x;->E:LX/2UP;

    sget-object v5, LX/95K;->COMPUTE_DELTA_AND_UPLOAD:LX/95K;

    invoke-virtual {v4, v5}, LX/2UP;->a(LX/95K;)V

    .line 1435813
    invoke-static {}, LX/444;->newBuilder()LX/445;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, LX/94x;->w:LX/30b;

    invoke-virtual {v4, v5}, LX/445;->b(LX/30b;)LX/445;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, LX/94x;->x:LX/30b;

    invoke-virtual {v4, v5}, LX/445;->a(LX/30b;)LX/445;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, LX/94x;->y:Ljava/util/Comparator;

    invoke-virtual {v4, v5}, LX/445;->a(Ljava/util/Comparator;)LX/445;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, LX/445;->a(Ljava/util/Iterator;)LX/445;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, LX/445;->b(Ljava/util/Iterator;)LX/445;

    move-result-object v4

    invoke-virtual {v4}, LX/445;->a()LX/444;

    move-result-object v19

    .line 1435814
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v11

    .line 1435815
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 1435816
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v10

    .line 1435817
    new-instance v7, Ljava/util/HashSet;

    move/from16 v0, p5

    invoke-direct {v7, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 1435818
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 1435819
    invoke-virtual/range {p1 .. p1}, LX/1qK;->getCallback()LX/1qH;

    move-result-object v9

    .line 1435820
    const/4 v6, 0x0

    .line 1435821
    const/4 v5, 0x0

    .line 1435822
    const/4 v4, 0x0

    .line 1435823
    const/4 v14, 0x0

    .line 1435824
    const/4 v15, 0x0

    .line 1435825
    const/16 v16, 0x0

    move v13, v5

    move/from16 v17, v6

    move-object/from16 v18, v11

    move-object v11, v8

    move v5, v4

    move-object v8, v7

    move-object/from16 v7, p6

    .line 1435826
    :goto_0
    invoke-virtual/range {v19 .. v19}, LX/0Rr;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1435827
    invoke-virtual/range {v19 .. v19}, LX/0Rr;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/443;

    .line 1435828
    const/4 v12, 0x0

    .line 1435829
    iget-object v6, v4, LX/443;->a:Ljava/lang/Object;

    if-eqz v6, :cond_e

    sget-object v6, Lcom/facebook/contacts/ContactSurface;->GROWTH_CONTACT_IMPORTER:Lcom/facebook/contacts/ContactSurface;

    move-object/from16 v0, p7

    if-ne v0, v6, :cond_e

    .line 1435830
    add-int/lit8 v6, v5, 0x1

    move/from16 v0, p8

    if-le v6, v0, :cond_3

    move-object/from16 v0, p0

    iget-object v5, v0, LX/94x;->s:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/03R;

    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v5, v0}, LX/03R;->asBoolean(Z)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1435831
    iget-object v5, v4, LX/443;->b:Ljava/lang/Object;

    if-eqz v5, :cond_d

    .line 1435832
    const/4 v5, 0x1

    move v12, v6

    .line 1435833
    :goto_1
    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-static {v0, v4, v1, v5}, LX/94x;->a(LX/94x;LX/443;Lcom/facebook/contacts/ContactSurface;Z)LX/94l;

    move-result-object v5

    .line 1435834
    iget-object v6, v5, LX/94l;->a:LX/94k;

    sget-object v21, LX/94k;->ADD:LX/94k;

    move-object/from16 v0, v21

    if-eq v6, v0, :cond_0

    iget-object v6, v5, LX/94l;->a:LX/94k;

    sget-object v21, LX/94k;->UPDATE:LX/94k;

    move-object/from16 v0, v21

    if-ne v6, v0, :cond_1

    .line 1435835
    :cond_0
    iget-object v6, v5, LX/94l;->c:Lcom/facebook/contacts/model/PhonebookContact;

    .line 1435836
    iget-object v0, v6, Lcom/facebook/contacts/model/PhonebookContact;->n:LX/0Px;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, LX/0Px;->isEmpty()Z

    move-result v21

    if-eqz v21, :cond_1

    iget-object v0, v6, Lcom/facebook/contacts/model/PhonebookContact;->m:LX/0Px;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, LX/0Px;->isEmpty()Z

    move-result v21

    if-eqz v21, :cond_1

    .line 1435837
    move-object/from16 v0, p0

    iget-object v0, v0, LX/94x;->E:LX/2UP;

    move-object/from16 v21, v0

    iget-object v6, v6, Lcom/facebook/contacts/model/PhonebookContact;->a:Ljava/lang/String;

    iget-object v0, v5, LX/94l;->a:LX/94k;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, LX/94k;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v0, v6, v1}, LX/2UP;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1435838
    :cond_1
    iget-object v6, v5, LX/94l;->c:Lcom/facebook/contacts/model/PhonebookContact;

    if-eqz v6, :cond_2

    .line 1435839
    move-object/from16 v0, p4

    iget v6, v0, LX/94w;->a:I

    add-int/lit8 v6, v6, 0x1

    move-object/from16 v0, p4

    iput v6, v0, LX/94w;->a:I

    .line 1435840
    :cond_2
    iget-object v6, v5, LX/94l;->a:LX/94k;

    sget-object v21, LX/94k;->NONE:LX/94k;

    move-object/from16 v0, v21

    if-eq v6, v0, :cond_c

    .line 1435841
    iget-wide v0, v5, LX/94l;->b:J

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    .line 1435842
    invoke-interface {v8, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_4

    .line 1435843
    move-object/from16 v0, p0

    iget-object v4, v0, LX/94x;->B:LX/03V;

    const-string v5, "Duplicate contact ID returned by phonebook iterator"

    invoke-virtual {v4, v5, v6}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v12

    .line 1435844
    goto/16 :goto_0

    .line 1435845
    :cond_3
    iget-object v5, v4, LX/443;->a:Ljava/lang/Object;

    check-cast v5, Lcom/facebook/contacts/model/PhonebookContact;

    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-static {v0, v1, v5}, LX/94x;->b(LX/94x;Lcom/facebook/contacts/ContactSurface;Lcom/facebook/contacts/model/PhonebookContact;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v5, v12

    move v12, v6

    goto/16 :goto_1

    .line 1435846
    :cond_4
    sget-object v21, Lcom/facebook/contacts/ContactSurface;->GROWTH_CONTACT_IMPORTER:Lcom/facebook/contacts/ContactSurface;

    move-object/from16 v0, p7

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/facebook/contacts/ContactSurface;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_5

    .line 1435847
    sget-object v21, LX/94u;->a:[I

    iget-object v0, v5, LX/94l;->a:LX/94k;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, LX/94k;->buckContactChangeType:LX/6ON;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, LX/6ON;->ordinal()I

    move-result v22

    aget v21, v21, v22

    packed-switch v21, :pswitch_data_0

    .line 1435848
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Unsupported action type "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v5, LX/94l;->a:LX/94k;

    iget-object v5, v5, LX/94k;->buckContactChangeType:LX/6ON;

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1435849
    :pswitch_0
    new-instance v4, Lcom/facebook/contacts/server/UploadBulkContactChange;

    iget-object v0, v5, LX/94l;->c:Lcom/facebook/contacts/model/PhonebookContact;

    move-object/from16 v21, v0

    iget-object v0, v5, LX/94l;->a:LX/94k;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, LX/94k;->buckContactChangeType:LX/6ON;

    move-object/from16 v22, v0

    iget-object v0, v5, LX/94l;->c:Lcom/facebook/contacts/model/PhonebookContact;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    move-object/from16 v1, p7

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, LX/94x;->b(LX/94x;Lcom/facebook/contacts/ContactSurface;Lcom/facebook/contacts/model/PhonebookContact;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-direct {v4, v6, v0, v1, v2}, Lcom/facebook/contacts/server/UploadBulkContactChange;-><init>(Ljava/lang/String;Lcom/facebook/contacts/model/PhonebookContact;LX/6ON;Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v6, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1435850
    add-int/lit8 v14, v14, 0x1

    .line 1435851
    :goto_2
    new-instance v4, LX/95C;

    iget-object v6, v5, LX/94l;->a:LX/94k;

    iget-object v6, v6, LX/94k;->snapshotEntryChangeType:LX/95B;

    iget-wide v0, v5, LX/94l;->b:J

    move-wide/from16 v22, v0

    iget-object v5, v5, LX/94l;->d:LX/95A;

    move-wide/from16 v0, v22

    invoke-direct {v4, v6, v0, v1, v5}, LX/95C;-><init>(LX/95B;JLX/95A;)V

    invoke-virtual {v11, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1435852
    add-int/lit8 v13, v13, 0x1

    move/from16 v0, p5

    if-lt v13, v0, :cond_b

    .line 1435853
    invoke-virtual/range {v18 .. v18}, LX/0P2;->b()LX/0P1;

    move-result-object v5

    invoke-virtual {v11}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    move-object/from16 v4, p0

    move-object/from16 v8, p4

    move-object/from16 v11, p7

    invoke-direct/range {v4 .. v11}, LX/94x;->a(LX/0P1;LX/0Px;Ljava/lang/String;LX/94w;LX/1qH;Ljava/util/Map;Lcom/facebook/contacts/ContactSurface;)Ljava/lang/String;

    move-result-object p6

    .line 1435854
    add-int v5, v17, v13

    .line 1435855
    const/4 v4, 0x0

    .line 1435856
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v8

    .line 1435857
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 1435858
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    :goto_3
    move v13, v4

    move/from16 v17, v5

    move-object v11, v7

    move-object/from16 v18, v8

    move v5, v12

    move-object v8, v6

    move-object/from16 v7, p6

    .line 1435859
    goto/16 :goto_0

    .line 1435860
    :pswitch_1
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "D"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    new-instance v22, Lcom/facebook/contacts/server/UploadBulkContactChange;

    iget-object v0, v5, LX/94l;->c:Lcom/facebook/contacts/model/PhonebookContact;

    move-object/from16 v23, v0

    sget-object v24, LX/6ON;->DELETE:LX/6ON;

    iget-object v4, v4, LX/443;->b:Ljava/lang/Object;

    check-cast v4, LX/95A;

    iget-object v4, v4, LX/95A;->b:Ljava/lang/String;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-direct {v0, v6, v1, v2, v4}, Lcom/facebook/contacts/server/UploadBulkContactChange;-><init>(Ljava/lang/String;Lcom/facebook/contacts/model/PhonebookContact;LX/6ON;Ljava/lang/String;)V

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1435861
    new-instance v4, Lcom/facebook/contacts/server/UploadBulkContactChange;

    iget-object v0, v5, LX/94l;->c:Lcom/facebook/contacts/model/PhonebookContact;

    move-object/from16 v21, v0

    sget-object v22, LX/6ON;->ADD:LX/6ON;

    iget-object v0, v5, LX/94l;->c:Lcom/facebook/contacts/model/PhonebookContact;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    move-object/from16 v1, p7

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, LX/94x;->b(LX/94x;Lcom/facebook/contacts/ContactSurface;Lcom/facebook/contacts/model/PhonebookContact;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-direct {v4, v6, v0, v1, v2}, Lcom/facebook/contacts/server/UploadBulkContactChange;-><init>(Ljava/lang/String;Lcom/facebook/contacts/model/PhonebookContact;LX/6ON;Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v6, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1435862
    add-int/lit8 v15, v15, 0x1

    .line 1435863
    goto/16 :goto_2

    .line 1435864
    :pswitch_2
    new-instance v21, Lcom/facebook/contacts/server/UploadBulkContactChange;

    iget-object v0, v5, LX/94l;->c:Lcom/facebook/contacts/model/PhonebookContact;

    move-object/from16 v22, v0

    iget-object v0, v5, LX/94l;->a:LX/94k;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, LX/94k;->buckContactChangeType:LX/6ON;

    move-object/from16 v23, v0

    iget-object v4, v4, LX/443;->b:Ljava/lang/Object;

    check-cast v4, LX/95A;

    iget-object v4, v4, LX/95A;->b:Ljava/lang/String;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-direct {v0, v6, v1, v2, v4}, Lcom/facebook/contacts/server/UploadBulkContactChange;-><init>(Ljava/lang/String;Lcom/facebook/contacts/model/PhonebookContact;LX/6ON;Ljava/lang/String;)V

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v6, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1435865
    add-int/lit8 v16, v16, 0x1

    .line 1435866
    goto/16 :goto_2

    .line 1435867
    :cond_5
    sget-object v4, LX/94u;->a:[I

    iget-object v0, v5, LX/94l;->a:LX/94k;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, LX/94k;->buckContactChangeType:LX/6ON;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, LX/6ON;->ordinal()I

    move-result v21

    aget v4, v4, v21

    packed-switch v4, :pswitch_data_1

    .line 1435868
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Unsupported action type "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v5, LX/94l;->a:LX/94k;

    iget-object v5, v5, LX/94k;->buckContactChangeType:LX/6ON;

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1435869
    :pswitch_3
    add-int/lit8 v14, v14, 0x1

    .line 1435870
    :goto_4
    new-instance v4, Lcom/facebook/contacts/server/UploadBulkContactChange;

    iget-object v0, v5, LX/94l;->c:Lcom/facebook/contacts/model/PhonebookContact;

    move-object/from16 v21, v0

    iget-object v0, v5, LX/94l;->a:LX/94k;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, LX/94k;->buckContactChangeType:LX/6ON;

    move-object/from16 v22, v0

    iget-object v0, v5, LX/94l;->c:Lcom/facebook/contacts/model/PhonebookContact;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    move-object/from16 v1, p7

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, LX/94x;->b(LX/94x;Lcom/facebook/contacts/ContactSurface;Lcom/facebook/contacts/model/PhonebookContact;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-direct {v4, v6, v0, v1, v2}, Lcom/facebook/contacts/server/UploadBulkContactChange;-><init>(Ljava/lang/String;Lcom/facebook/contacts/model/PhonebookContact;LX/6ON;Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v6, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto/16 :goto_2

    .line 1435871
    :pswitch_4
    add-int/lit8 v15, v15, 0x1

    .line 1435872
    goto :goto_4

    .line 1435873
    :pswitch_5
    add-int/lit8 v16, v16, 0x1

    .line 1435874
    goto :goto_4

    .line 1435875
    :cond_6
    if-lez v13, :cond_7

    .line 1435876
    invoke-virtual/range {v18 .. v18}, LX/0P2;->b()LX/0P1;

    move-result-object v5

    invoke-virtual {v11}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    move-object/from16 v4, p0

    move-object/from16 v8, p4

    move-object/from16 v11, p7

    invoke-direct/range {v4 .. v11}, LX/94x;->a(LX/0P1;LX/0Px;Ljava/lang/String;LX/94w;LX/1qH;Ljava/util/Map;Lcom/facebook/contacts/ContactSurface;)Ljava/lang/String;

    move-result-object v7

    .line 1435877
    add-int v17, v17, v13

    .line 1435878
    :cond_7
    if-lez v17, :cond_8

    .line 1435879
    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.facebook.contacts.CONTACTS_UPLOAD_DONE"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1435880
    move-object/from16 v0, p0

    iget-object v5, v0, LX/94x;->A:LX/0Xl;

    invoke-interface {v5, v4}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 1435881
    :cond_8
    sget-object v4, Lcom/facebook/contacts/ContactSurface;->GROWTH_CONTACT_IMPORTER:Lcom/facebook/contacts/ContactSurface;

    move-object/from16 v0, p7

    if-ne v0, v4, :cond_9

    .line 1435882
    move-object/from16 v0, p0

    iget-object v4, v0, LX/94x;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v4

    sget-object v5, LX/32V;->k:LX/0Tn;

    invoke-static/range {v20 .. v20}, LX/958;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5, v6}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v4

    invoke-interface {v4}, LX/0hN;->commit()V

    .line 1435883
    :cond_9
    sget-object v4, Lcom/facebook/contacts/ContactSurface;->GROWTH_CONTACT_IMPORTER:Lcom/facebook/contacts/ContactSurface;

    move-object/from16 v0, p7

    invoke-virtual {v0, v4}, Lcom/facebook/contacts/ContactSurface;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    const-string v12, "fb4a"

    .line 1435884
    :goto_5
    add-int v4, v14, v16

    mul-int/lit8 v5, v15, 0x2

    add-int v17, v4, v5

    .line 1435885
    move-object/from16 v0, p0

    iget-object v11, v0, LX/94x;->E:LX/2UP;

    move-object/from16 v0, p4

    iget v13, v0, LX/94w;->a:I

    invoke-virtual/range {v11 .. v17}, LX/2UP;->a(Ljava/lang/String;IIIII)V

    .line 1435886
    new-instance v4, Lcom/facebook/contacts/server/UploadContactsResult;

    invoke-interface {v10}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-static {v5}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v5

    invoke-direct {v4, v7, v5}, Lcom/facebook/contacts/server/UploadContactsResult;-><init>(Ljava/lang/String;Ljava/util/List;)V

    return-object v4

    .line 1435887
    :cond_a
    const-string v12, "messenger"

    goto :goto_5

    :cond_b
    move v4, v13

    move/from16 v5, v17

    move-object v6, v8

    move-object/from16 p6, v7

    move-object/from16 v8, v18

    move-object v7, v11

    goto/16 :goto_3

    :cond_c
    move v5, v12

    goto/16 :goto_0

    :cond_d
    move v5, v6

    goto/16 :goto_0

    :cond_e
    move/from16 v25, v12

    move v12, v5

    move/from16 v5, v25

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private a(Ljava/lang/String;LX/0P1;LX/0Px;)Lcom/facebook/contacts/server/UploadFriendFinderContactsResult;
    .locals 10
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/contacts/server/UploadBulkContactChange;",
            ">;",
            "LX/0Px",
            "<",
            "LX/95C;",
            ">;)",
            "Lcom/facebook/contacts/server/UploadFriendFinderContactsResult;"
        }
    .end annotation

    .prologue
    .line 1436044
    const-string v0, "uploadContactBatchForFriendFinder (%d changes)"

    invoke-virtual {p2}, LX/0P1;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, -0x8e4b076

    invoke-static {v0, v1, v2}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 1436045
    iget-object v0, p0, LX/94x;->E:LX/2UP;

    sget-object v1, LX/95K;->BATCH_UPLOAD_START:LX/95K;

    invoke-virtual {v0, v1}, LX/2UP;->a(LX/95K;)V

    .line 1436046
    if-nez p1, :cond_0

    :try_start_0
    const-string p1, "(new import)"
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1436047
    :cond_0
    const/4 v2, 0x0

    .line 1436048
    const/4 v0, 0x0

    .line 1436049
    const/4 v1, 0x0

    move v3, v2

    move v2, v0

    .line 1436050
    :cond_1
    :goto_0
    if-nez v3, :cond_2

    const/4 v0, 0x3

    if-ge v2, v0, :cond_2

    .line 1436051
    :try_start_1
    invoke-virtual {p2}, LX/0P1;->size()I

    .line 1436052
    new-instance v0, Lcom/facebook/contacts/server/UploadFriendFinderContactsParams;

    sget-object v4, LX/6OZ;->CONTINUOUS_SYNC:LX/6OZ;

    invoke-virtual {p2}, LX/0P1;->values()LX/0Py;

    move-result-object v5

    invoke-static {v5}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v5

    iget-boolean v6, p0, LX/94x;->F:Z

    invoke-direct {v0, v4, p1, v5, v6}, Lcom/facebook/contacts/server/UploadFriendFinderContactsParams;-><init>(LX/6OZ;Ljava/lang/String;Ljava/util/List;Z)V

    .line 1436053
    iget-object v4, p0, LX/94x;->e:LX/11H;

    iget-object v5, p0, LX/94x;->g:LX/6O2;

    invoke-virtual {v4, v5, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/server/UploadFriendFinderContactsResult;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1436054
    :try_start_2
    invoke-direct {p0, p3}, LX/94x;->a(LX/0Px;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1436055
    const/4 v1, 0x1

    move v3, v1

    move-object v1, v0

    .line 1436056
    goto :goto_0

    .line 1436057
    :catch_0
    move-exception v0

    .line 1436058
    :goto_1
    add-int/lit8 v2, v2, 0x1

    .line 1436059
    :try_start_3
    sget-object v4, LX/94x;->a:Ljava/lang/String;

    const-string v5, "Failed to upload contact batch, (try %s of %s ), error: %s "

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const/4 v8, 0x3

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    aput-object v0, v6, v7

    invoke-static {v4, v5, v6}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1436060
    iget-object v4, p0, LX/94x;->E:LX/2UP;

    sget-object v5, LX/95K;->BATCH_UPLOAD_ATTEMPT_ERROR:LX/95K;

    invoke-virtual {v4, v5}, LX/2UP;->a(LX/95K;)V

    .line 1436061
    const/4 v4, 0x3

    if-lt v2, v4, :cond_1

    .line 1436062
    iget-object v1, p0, LX/94x;->E:LX/2UP;

    sget-object v2, LX/95K;->BATCH_UPLOAD_FAIL:LX/95K;

    invoke-virtual {v1, v2}, LX/2UP;->a(LX/95K;)V

    .line 1436063
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1436064
    :catchall_0
    move-exception v0

    const v1, -0x66269422

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 1436065
    :cond_2
    if-eqz v1, :cond_3

    const/4 v0, 0x1

    :goto_2
    :try_start_4
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1436066
    iget-object v0, p0, LX/94x;->E:LX/2UP;

    sget-object v2, LX/95K;->BATCH_UPLOAD_FINISH:LX/95K;

    invoke-virtual {v0, v2}, LX/2UP;->a(LX/95K;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1436067
    const v0, 0x6c9a0baf

    invoke-static {v0}, LX/02m;->a(I)V

    return-object v1

    .line 1436068
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 1436069
    :catch_1
    move-exception v1

    move-object v9, v1

    move-object v1, v0

    move-object v0, v9

    goto :goto_1
.end method

.method public static a(LX/94x;LX/1qK;Lcom/facebook/contacts/ContactSurface;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 10

    .prologue
    .line 1436008
    sget-object v0, LX/94x;->b:LX/0Rf;

    invoke-virtual {v0, p2}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1436009
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported ContactSurface: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forException(Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 1436010
    :goto_0
    return-object v0

    .line 1436011
    :cond_0
    iget-object v0, p0, LX/94x;->E:LX/2UP;

    sget-object v1, LX/95K;->OVERALL_UPLOAD_START:LX/95K;

    invoke-virtual {v0, v1}, LX/2UP;->a(LX/95K;)V

    .line 1436012
    iget-object v0, p0, LX/94x;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/32V;->o:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1436013
    sget-object v0, Lcom/facebook/contacts/ContactSurface;->GROWTH_CONTACT_IMPORTER:Lcom/facebook/contacts/ContactSurface;

    if-ne p2, v0, :cond_1

    iget-object v0, p0, LX/94x;->c:LX/2UR;

    invoke-virtual {v0}, LX/2UR;->b()LX/6Mz;

    move-result-object v2

    .line 1436014
    :goto_1
    if-nez v2, :cond_2

    .line 1436015
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Contacts content provider is unsupported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forException(Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 1436016
    :cond_1
    iget-object v0, p0, LX/94x;->c:LX/2UR;

    invoke-virtual {v0}, LX/2UR;->c()LX/6Mz;

    move-result-object v2

    goto :goto_1

    .line 1436017
    :cond_2
    iget-object v0, p0, LX/94x;->d:LX/95J;

    invoke-virtual {v0}, LX/95J;->a()LX/2Ta;

    move-result-object v3

    .line 1436018
    :try_start_0
    invoke-virtual {v2}, LX/6Mz;->c()I

    move-result v1

    .line 1436019
    iget-object v0, p1, LX/1qK;->mCallback:LX/1qH;

    move-object v0, v0

    .line 1436020
    if-eqz v0, :cond_3

    .line 1436021
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v4, v5, v1}, Lcom/facebook/contacts/upload/ContactsUploadState;->a(III)Lcom/facebook/contacts/upload/ContactsUploadState;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v4

    invoke-interface {v0, v4}, LX/1qH;->onOperationProgress(Lcom/facebook/fbservice/service/OperationResult;)V

    .line 1436022
    :cond_3
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1436023
    const-string v4, "contactsUploadPhonebookMaxLimit"

    const/16 v5, 0x2710

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v8

    .line 1436024
    sget-object v0, Lcom/facebook/contacts/ContactSurface;->GROWTH_CONTACT_IMPORTER:Lcom/facebook/contacts/ContactSurface;

    if-ne p2, v0, :cond_6

    if-le v1, v8, :cond_6

    iget-object v0, p0, LX/94x;->s:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v4, LX/94w;

    invoke-direct {v4, v8}, LX/94w;-><init>(I)V

    .line 1436025
    :goto_2
    iget-object v0, p0, LX/94x;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/32V;->e:LX/0Tn;

    const-string v5, "-1"

    invoke-interface {v0, v1, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 1436026
    const/4 v0, -0x1

    if-ne v5, v0, :cond_4

    .line 1436027
    const/16 v5, 0x64

    .line 1436028
    :cond_4
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1436029
    const-string v1, "forceFullUploadAndTurnOffGlobalKillSwitch"

    const/4 v7, 0x0

    invoke-virtual {v0, v1, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, LX/94x;->F:Z

    .line 1436030
    iget-boolean v0, p0, LX/94x;->F:Z

    if-eqz v0, :cond_5

    .line 1436031
    iget-object v0, p0, LX/94x;->m:LX/2UQ;

    invoke-virtual {v0}, LX/2UQ;->a()V

    .line 1436032
    iget-object v0, p0, LX/94x;->D:LX/30I;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/30I;->a(Z)V

    .line 1436033
    sget-object v0, Lcom/facebook/contacts/ContactSurface;->MESSENGER:Lcom/facebook/contacts/ContactSurface;

    if-ne p2, v0, :cond_5

    .line 1436034
    iget-object v0, p0, LX/94x;->e:LX/11H;

    iget-object v1, p0, LX/94x;->i:LX/95N;

    new-instance v7, LX/95M;

    const/4 v9, 0x1

    invoke-direct {v7, v9}, LX/95M;-><init>(Z)V

    invoke-virtual {v0, v1, v7}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    move-object v0, p0

    move-object v1, p1

    move-object v7, p2

    .line 1436035
    invoke-direct/range {v0 .. v8}, LX/94x;->a(LX/1qK;LX/6Mz;LX/2Ta;LX/94w;ILjava/lang/String;Lcom/facebook/contacts/ContactSurface;I)Lcom/facebook/contacts/server/UploadContactsResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1436036
    invoke-virtual {v2}, LX/6Mz;->d()V

    .line 1436037
    invoke-interface {v3}, LX/2Ta;->close()V

    .line 1436038
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Done uploading contacts (import ID "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1436039
    iget-object v1, p0, LX/94x;->E:LX/2UP;

    sget-object v2, LX/95K;->OVERALL_UPLOAD_FINISH:LX/95K;

    invoke-virtual {v1, v2}, LX/2UP;->a(LX/95K;)V

    .line 1436040
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_0

    .line 1436041
    :cond_6
    :try_start_1
    new-instance v4, LX/94w;

    invoke-direct {v4, v1}, LX/94w;-><init>(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 1436042
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/6Mz;->d()V

    .line 1436043
    invoke-interface {v3}, LX/2Ta;->close()V

    throw v0
.end method

.method private a(LX/0P1;LX/0Px;Ljava/lang/String;LX/94w;LX/1qH;Ljava/util/Map;Lcom/facebook/contacts/ContactSurface;)Ljava/lang/String;
    .locals 7
    .param p5    # LX/1qH;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/contacts/server/UploadBulkContactChange;",
            ">;",
            "LX/0Px",
            "<",
            "LX/95C;",
            ">;",
            "Ljava/lang/String;",
            "LX/94w;",
            "LX/1qH;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/contacts/graphql/Contact;",
            ">;",
            "Lcom/facebook/contacts/ContactSurface;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1435987
    sget-object v0, Lcom/facebook/contacts/ContactSurface;->GROWTH_CONTACT_IMPORTER:Lcom/facebook/contacts/ContactSurface;

    if-ne p7, v0, :cond_1

    .line 1435988
    invoke-direct {p0, p3, p1, p2}, LX/94x;->a(Ljava/lang/String;LX/0P1;LX/0Px;)Lcom/facebook/contacts/server/UploadFriendFinderContactsResult;

    move-result-object v0

    .line 1435989
    iget-object v1, v0, Lcom/facebook/contacts/server/UploadFriendFinderContactsResult;->a:Ljava/lang/String;

    move-object v0, v1

    .line 1435990
    :goto_0
    if-eqz p5, :cond_0

    .line 1435991
    iget v1, p4, LX/94w;->a:I

    invoke-interface {p6}, Ljava/util/Map;->size()I

    move-result v2

    iget v3, p4, LX/94w;->b:I

    invoke-static {v1, v2, v3}, Lcom/facebook/contacts/upload/ContactsUploadState;->a(III)Lcom/facebook/contacts/upload/ContactsUploadState;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    invoke-interface {p5, v1}, LX/1qH;->onOperationProgress(Lcom/facebook/fbservice/service/OperationResult;)V

    .line 1435992
    :cond_0
    if-eqz v0, :cond_5

    .line 1435993
    iget-object v1, p0, LX/94x;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/32V;->o:LX/0Tn;

    invoke-interface {v1, v2, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 1435994
    :goto_1
    return-object v0

    .line 1435995
    :cond_1
    invoke-direct {p0, p3, p1, p2, p7}, LX/94x;->a(Ljava/lang/String;LX/0P1;LX/0Px;Lcom/facebook/contacts/ContactSurface;)Lcom/facebook/contacts/server/UploadBulkFetchContactsResult;

    move-result-object v0

    .line 1435996
    iget-object v3, v0, Lcom/facebook/contacts/server/UploadBulkFetchContactsResult;->a:Lcom/facebook/contacts/server/UploadBulkContactsResult;

    .line 1435997
    iget-object v0, v0, Lcom/facebook/contacts/server/UploadBulkFetchContactsResult;->b:Lcom/facebook/contacts/server/FetchContactsResult;

    .line 1435998
    if-eqz v0, :cond_4

    .line 1435999
    iget-object v1, v0, Lcom/facebook/contacts/server/FetchContactsResult;->a:LX/0Px;

    move-object v4, v1

    .line 1436000
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    :goto_2
    if-ge v2, v5, :cond_4

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/Contact;

    .line 1436001
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/Contact;->x()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, LX/94x;->q:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/Contact;->c()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1436002
    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/Contact;->s()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, LX/94x;->C:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1436003
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/Contact;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p6, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1436004
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 1436005
    :cond_4
    iget-object v0, v3, Lcom/facebook/contacts/server/UploadBulkContactsResult;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1436006
    goto :goto_0

    .line 1436007
    :cond_5
    iget-object v1, p0, LX/94x;->B:LX/03V;

    sget-object v2, LX/94x;->a:Ljava/lang/String;

    const-string v3, "Null Import Id returned by server"

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private a(LX/0P1;Lcom/facebook/contacts/server/UploadBulkContactsResult;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/contacts/server/UploadBulkContactChange;",
            ">;",
            "Lcom/facebook/contacts/server/UploadBulkContactsResult;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1435966
    invoke-static {p2}, LX/94x;->a(Lcom/facebook/contacts/server/UploadBulkContactsResult;)LX/0Px;

    move-result-object v0

    const/4 v3, 0x0

    .line 1435967
    invoke-static {}, LX/18f;->c()LX/2Q8;

    move-result-object v5

    .line 1435968
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v6

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_2

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/contacts/server/UploadBulkContactChangeResult;

    .line 1435969
    iget-object v2, v1, Lcom/facebook/contacts/server/UploadBulkContactChangeResult;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1435970
    iget-object v7, v1, Lcom/facebook/contacts/server/UploadBulkContactChangeResult;->c:Ljava/lang/String;

    move-object v7, v7

    .line 1435971
    invoke-virtual {p1, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/contacts/server/UploadBulkContactChange;

    .line 1435972
    if-nez v2, :cond_1

    .line 1435973
    sget-object v2, LX/94x;->a:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Got change result that did not match a local contact ID, skipping: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1435974
    :cond_0
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_0

    .line 1435975
    :cond_1
    iget-object v1, v2, Lcom/facebook/contacts/server/UploadBulkContactChange;->c:Lcom/facebook/contacts/model/PhonebookContact;

    move-object v1, v1

    .line 1435976
    iget-object v8, v1, Lcom/facebook/contacts/model/PhonebookContact;->m:LX/0Px;

    .line 1435977
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v2, v3

    :goto_1
    if-ge v2, v9, :cond_0

    invoke-virtual {v8, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/contacts/model/PhonebookPhoneNumber;

    .line 1435978
    iget-object v1, v1, Lcom/facebook/contacts/model/PhonebookPhoneNumber;->a:Ljava/lang/String;

    .line 1435979
    :try_start_0
    iget-object v10, p0, LX/94x;->u:LX/3Lz;

    const/4 p2, 0x0

    invoke-virtual {v10, v1, p2}, LX/3Lz;->parse(Ljava/lang/String;Ljava/lang/String;)LX/4hT;

    move-result-object v10

    .line 1435980
    invoke-virtual {v5, v7, v10}, LX/2Q8;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/2Q8;
    :try_end_0
    .catch LX/4hE; {:try_start_0 .. :try_end_0} :catch_0

    .line 1435981
    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 1435982
    :catch_0
    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    aput-object v1, v10, v3

    goto :goto_2

    .line 1435983
    :cond_2
    invoke-virtual {v5}, LX/2Q8;->b()LX/18f;

    move-result-object v1

    move-object v0, v1

    .line 1435984
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Inserting phone indexes for contacts: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1435985
    iget-object v1, p0, LX/94x;->l:LX/3fg;

    invoke-virtual {v1, v0}, LX/3fg;->a(LX/18f;)V

    .line 1435986
    return-void
.end method

.method private a(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/95C;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1435964
    iget-object v0, p0, LX/94x;->m:LX/2UQ;

    invoke-virtual {v0, p1}, LX/2UQ;->a(Ljava/util/List;)V

    .line 1435965
    return-void
.end method

.method private static a(LX/94x;Lcom/facebook/contacts/model/PhonebookContact;Lcom/facebook/contacts/ContactSurface;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1435955
    sget-object v0, Lcom/facebook/contacts/ContactSurface;->GROWTH_CONTACT_IMPORTER:Lcom/facebook/contacts/ContactSurface;

    if-ne p2, v0, :cond_0

    move v0, v1

    .line 1435956
    :goto_0
    return v0

    .line 1435957
    :cond_0
    sget-object v0, Lcom/facebook/contacts/ContactSurface;->MESSENGER:Lcom/facebook/contacts/ContactSurface;

    if-eq p2, v0, :cond_1

    move v0, v2

    .line 1435958
    goto :goto_0

    .line 1435959
    :cond_1
    iget-object v0, p1, Lcom/facebook/contacts/model/PhonebookContact;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    .line 1435960
    goto :goto_0

    .line 1435961
    :cond_2
    iget-object v0, p1, Lcom/facebook/contacts/model/PhonebookContact;->m:LX/0Px;

    invoke-static {v0}, LX/94x;->a(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/94x;->t:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v2}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/facebook/contacts/model/PhonebookContact;->n:LX/0Px;

    invoke-static {v0}, LX/94x;->a(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    move v0, v2

    .line 1435962
    goto :goto_0

    :cond_4
    move v0, v1

    .line 1435963
    goto :goto_0
.end method

.method private static a(Ljava/util/List;)Z
    .locals 1

    .prologue
    .line 1435954
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(LX/0Px;)LX/0Py;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/graphql/Contact;",
            ">;)",
            "LX/0Py",
            "<",
            "Lcom/facebook/contacts/graphql/Contact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1435943
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1435944
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/Contact;

    .line 1435945
    new-instance v4, LX/3hB;

    invoke-direct {v4, v0}, LX/3hB;-><init>(Lcom/facebook/contacts/graphql/Contact;)V

    .line 1435946
    const/4 v5, 0x1

    .line 1435947
    iput-boolean v5, v4, LX/3hB;->z:Z

    .line 1435948
    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/Contact;->w()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-nez v0, :cond_0

    .line 1435949
    iget-object v0, p0, LX/94x;->z:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    .line 1435950
    iput-wide v6, v4, LX/3hB;->D:J

    .line 1435951
    :cond_0
    invoke-virtual {v4}, LX/3hB;->O()Lcom/facebook/contacts/graphql/Contact;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1435952
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1435953
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method private static b(LX/0QB;)LX/94x;
    .locals 29

    .prologue
    .line 1435941
    new-instance v2, LX/94x;

    invoke-static/range {p0 .. p0}, LX/2UR;->a(LX/0QB;)LX/2UR;

    move-result-object v3

    check-cast v3, LX/2UR;

    invoke-static/range {p0 .. p0}, LX/95J;->a(LX/0QB;)LX/95J;

    move-result-object v4

    check-cast v4, LX/95J;

    invoke-static/range {p0 .. p0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v5

    check-cast v5, LX/11H;

    invoke-static/range {p0 .. p0}, LX/6O0;->a(LX/0QB;)LX/6O0;

    move-result-object v6

    check-cast v6, LX/6O0;

    invoke-static/range {p0 .. p0}, LX/6O2;->a(LX/0QB;)LX/6O2;

    move-result-object v7

    check-cast v7, LX/6O2;

    invoke-static/range {p0 .. p0}, LX/6Nx;->a(LX/0QB;)LX/6Nx;

    move-result-object v8

    check-cast v8, LX/6Nx;

    invoke-static/range {p0 .. p0}, LX/95N;->a(LX/0QB;)LX/95N;

    move-result-object v9

    check-cast v9, LX/95N;

    invoke-static/range {p0 .. p0}, LX/2Iv;->a(LX/0QB;)LX/2Iv;

    move-result-object v10

    check-cast v10, LX/2Iv;

    invoke-static/range {p0 .. p0}, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->a(LX/0QB;)Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    move-result-object v11

    check-cast v11, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    invoke-static/range {p0 .. p0}, LX/3fg;->a(LX/0QB;)LX/3fg;

    move-result-object v12

    check-cast v12, LX/3fg;

    invoke-static/range {p0 .. p0}, LX/2UQ;->a(LX/0QB;)LX/2UQ;

    move-result-object v13

    check-cast v13, LX/2UQ;

    invoke-static/range {p0 .. p0}, LX/959;->a(LX/0QB;)LX/959;

    move-result-object v14

    check-cast v14, LX/959;

    invoke-static/range {p0 .. p0}, LX/958;->a(LX/0QB;)LX/958;

    move-result-object v15

    check-cast v15, LX/958;

    invoke-static/range {p0 .. p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v16

    check-cast v16, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v17, 0x15e7

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v17

    const/16 v18, 0x1479

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v18

    const/16 v19, 0x30e

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v19

    const/16 v20, 0x30a

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v20

    invoke-static/range {p0 .. p0}, LX/3Ly;->a(LX/0QB;)LX/3Lz;

    move-result-object v21

    check-cast v21, LX/3Lz;

    const/16 v22, 0x30b

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v22

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v23

    check-cast v23, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v24

    check-cast v24, LX/0Xl;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v25

    check-cast v25, LX/03V;

    const/16 v26, 0x150e

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v26

    invoke-static/range {p0 .. p0}, LX/30I;->a(LX/0QB;)LX/30I;

    move-result-object v27

    check-cast v27, LX/30I;

    invoke-static/range {p0 .. p0}, LX/2UP;->a(LX/0QB;)LX/2UP;

    move-result-object v28

    check-cast v28, LX/2UP;

    invoke-direct/range {v2 .. v28}, LX/94x;-><init>(LX/2UR;LX/95J;LX/11H;LX/6O0;LX/6O2;LX/6Nx;LX/95N;LX/2Iv;Lcom/facebook/contacts/background/AddressBookPeriodicRunner;LX/3fg;LX/2UQ;LX/959;LX/958;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/3Lz;LX/0Or;LX/0SG;LX/0Xl;LX/03V;LX/0Or;LX/30I;LX/2UP;)V

    .line 1435942
    return-object v2
.end method

.method private static b(LX/94x;Lcom/facebook/contacts/ContactSurface;Lcom/facebook/contacts/model/PhonebookContact;)Ljava/lang/String;
    .locals 13

    .prologue
    .line 1435908
    if-nez p2, :cond_0

    .line 1435909
    const-string v0, ""

    .line 1435910
    :goto_0
    return-object v0

    .line 1435911
    :cond_0
    sget-object v0, LX/94u;->b:[I

    invoke-virtual {p1}, Lcom/facebook/contacts/ContactSurface;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1435912
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported upload surface: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1435913
    :pswitch_0
    iget-object v0, p0, LX/94x;->r:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v5, 0x0

    .line 1435914
    if-eqz v0, :cond_4

    .line 1435915
    invoke-virtual {p2}, Lcom/facebook/contacts/model/PhonebookContact;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 1435916
    :goto_1
    move-object v0, v3

    .line 1435917
    goto :goto_0

    .line 1435918
    :pswitch_1
    iget-object v0, p0, LX/94x;->t:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v7, 0x0

    .line 1435919
    iget-object v3, p2, Lcom/facebook/contacts/model/PhonebookContact;->b:Ljava/lang/String;

    .line 1435920
    if-nez v3, :cond_1

    .line 1435921
    const/4 v3, 0x0

    invoke-static {v3}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v3

    int-to-long v3, v3

    .line 1435922
    :goto_2
    iget-object v8, p2, Lcom/facebook/contacts/model/PhonebookContact;->m:LX/0Px;

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move-wide v5, v3

    move v4, v7

    :goto_3
    if-ge v4, v9, :cond_2

    invoke-virtual {v8, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/contacts/model/PhonebookPhoneNumber;

    .line 1435923
    new-array v10, v12, [Ljava/lang/Object;

    iget-object v3, v3, Lcom/facebook/contacts/model/PhonebookPhoneNumber;->a:Ljava/lang/String;

    aput-object v3, v10, v7

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v10, v11

    invoke-static {v10}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v3

    int-to-long v5, v3

    .line 1435924
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_3

    .line 1435925
    :cond_1
    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v3, v4, v7

    iget-object v3, p2, Lcom/facebook/contacts/model/PhonebookContact;->c:Ljava/lang/String;

    aput-object v3, v4, v11

    iget-object v3, p2, Lcom/facebook/contacts/model/PhonebookContact;->d:Ljava/lang/String;

    aput-object v3, v4, v12

    invoke-static {v4}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v3

    int-to-long v3, v3

    goto :goto_2

    .line 1435926
    :cond_2
    if-eqz v0, :cond_3

    .line 1435927
    iget-object v8, p2, Lcom/facebook/contacts/model/PhonebookContact;->n:LX/0Px;

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v4, v7

    :goto_4
    if-ge v4, v9, :cond_3

    invoke-virtual {v8, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/contacts/model/PhonebookEmailAddress;

    .line 1435928
    new-array v10, v12, [Ljava/lang/Object;

    iget-object v3, v3, Lcom/facebook/contacts/model/PhonebookEmailAddress;->a:Ljava/lang/String;

    aput-object v3, v10, v7

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v10, v11

    invoke-static {v10}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v3

    int-to-long v5, v3

    .line 1435929
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_4

    :cond_3
    move-wide v3, v5

    .line 1435930
    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    move-object v0, v3

    .line 1435931
    goto/16 :goto_0

    .line 1435932
    :cond_4
    iget-object v3, p2, Lcom/facebook/contacts/model/PhonebookContact;->b:Ljava/lang/String;

    .line 1435933
    new-array v4, v11, [Ljava/lang/Object;

    aput-object v3, v4, v5

    invoke-static {v4}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v3

    int-to-long v7, v3

    .line 1435934
    iget-object v6, p2, Lcom/facebook/contacts/model/PhonebookContact;->n:LX/0Px;

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v9

    move v4, v5

    :goto_5
    if-ge v4, v9, :cond_5

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/contacts/model/PhonebookEmailAddress;

    .line 1435935
    new-array v10, v12, [Ljava/lang/Object;

    iget-object v3, v3, Lcom/facebook/contacts/model/PhonebookEmailAddress;->a:Ljava/lang/String;

    aput-object v3, v10, v5

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v10, v11

    invoke-static {v10}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v3

    int-to-long v7, v3

    .line 1435936
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_5

    .line 1435937
    :cond_5
    iget-object v6, p2, Lcom/facebook/contacts/model/PhonebookContact;->m:LX/0Px;

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v9

    move v4, v5

    :goto_6
    if-ge v4, v9, :cond_6

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/contacts/model/PhonebookPhoneNumber;

    .line 1435938
    new-array v10, v12, [Ljava/lang/Object;

    iget-object v3, v3, Lcom/facebook/contacts/model/PhonebookPhoneNumber;->a:Ljava/lang/String;

    aput-object v3, v10, v5

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v10, v11

    invoke-static {v10}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v3

    int-to-long v7, v3

    .line 1435939
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_6

    .line 1435940
    :cond_6
    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 1435888
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 1435889
    const-string v1, "contacts_upload_messaging"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1435890
    sget-object v0, Lcom/facebook/contacts/ContactSurface;->MESSENGER:Lcom/facebook/contacts/ContactSurface;

    invoke-static {p0, p1, v0}, LX/94x;->a(LX/94x;LX/1qK;Lcom/facebook/contacts/ContactSurface;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 1435891
    :goto_0
    return-object v0

    .line 1435892
    :cond_0
    const-string v1, "contacts_upload_friend_finder"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1435893
    sget-object v0, Lcom/facebook/contacts/ContactSurface;->GROWTH_CONTACT_IMPORTER:Lcom/facebook/contacts/ContactSurface;

    invoke-static {p0, p1, v0}, LX/94x;->a(LX/94x;LX/1qK;Lcom/facebook/contacts/ContactSurface;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 1435894
    goto :goto_0

    .line 1435895
    :cond_1
    const-string v1, "bulk_contacts_delete"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1435896
    const/4 v3, 0x0

    .line 1435897
    iget-object v0, p0, LX/94x;->e:LX/11H;

    iget-object v1, p0, LX/94x;->i:LX/95N;

    new-instance v2, LX/95M;

    invoke-direct {v2, v3}, LX/95M;-><init>(Z)V

    invoke-virtual {v0, v1, v2}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1435898
    iget-object v0, p0, LX/94x;->j:LX/2Iv;

    invoke-virtual {v0}, LX/0Tr;->f()V

    .line 1435899
    iget-object v0, p0, LX/94x;->k:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    invoke-virtual {v0}, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->clearUserData()V

    .line 1435900
    iget-object v0, p0, LX/94x;->m:LX/2UQ;

    invoke-virtual {v0}, LX/2UQ;->a()V

    .line 1435901
    iget-object v0, p0, LX/94x;->D:LX/30I;

    invoke-virtual {v0, v3}, LX/30I;->a(Z)V

    .line 1435902
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.facebook.contacts.CONTACT_BULK_DELETE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1435903
    iget-object v1, p0, LX/94x;->A:LX/0Xl;

    invoke-interface {v1, v0}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 1435904
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1435905
    move-object v0, v0

    .line 1435906
    goto :goto_0

    .line 1435907
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
