.class public final LX/8wC;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic v:Z


# instance fields
.field public final a:Z

.field public b:I

.field public c:LX/8wG;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/8wG",
            "<",
            "LX/8wH;",
            "LX/8w5;",
            ">;"
        }
    .end annotation
.end field

.field public d:Z

.field public e:LX/8wG;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/8wG",
            "<",
            "LX/8wD;",
            "LX/8wF;",
            ">;"
        }
    .end annotation
.end field

.field public f:Z

.field public g:LX/8wG;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/8wG",
            "<",
            "LX/8wD;",
            "LX/8wF;",
            ">;"
        }
    .end annotation
.end field

.field public h:Z

.field public i:[I

.field public j:Z

.field public k:[I

.field public l:Z

.field public m:[LX/8w9;

.field public n:Z

.field public o:[I

.field public p:Z

.field public q:Z

.field public r:Z

.field public s:[I

.field public t:[I

.field public u:Z

.field public final synthetic w:Landroid/support/v7/widget/GridLayout;

.field private x:I

.field private y:LX/8wF;

.field private z:LX/8wF;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1421877
    const-class v0, Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, LX/8wC;->v:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/support/v7/widget/GridLayout;Z)V
    .locals 2

    .prologue
    const/high16 v0, -0x80000000

    const/4 v1, 0x0

    .line 1421878
    iput-object p1, p0, LX/8wC;->w:Landroid/support/v7/widget/GridLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1421879
    iput v0, p0, LX/8wC;->b:I

    .line 1421880
    iput v0, p0, LX/8wC;->x:I

    .line 1421881
    iput-boolean v1, p0, LX/8wC;->d:Z

    .line 1421882
    iput-boolean v1, p0, LX/8wC;->f:Z

    .line 1421883
    iput-boolean v1, p0, LX/8wC;->h:Z

    .line 1421884
    iput-boolean v1, p0, LX/8wC;->j:Z

    .line 1421885
    iput-boolean v1, p0, LX/8wC;->l:Z

    .line 1421886
    iput-boolean v1, p0, LX/8wC;->n:Z

    .line 1421887
    iput-boolean v1, p0, LX/8wC;->p:Z

    .line 1421888
    iput-boolean v1, p0, LX/8wC;->r:Z

    .line 1421889
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8wC;->u:Z

    .line 1421890
    new-instance v0, LX/8wF;

    invoke-direct {v0, v1}, LX/8wF;-><init>(I)V

    iput-object v0, p0, LX/8wC;->y:LX/8wF;

    .line 1421891
    new-instance v0, LX/8wF;

    const v1, -0x186a0

    invoke-direct {v0, v1}, LX/8wF;-><init>(I)V

    iput-object v0, p0, LX/8wC;->z:LX/8wF;

    .line 1421892
    iput-boolean p2, p0, LX/8wC;->a:Z

    .line 1421893
    return-void
.end method

.method public synthetic constructor <init>(Landroid/support/v7/widget/GridLayout;ZB)V
    .locals 0

    .prologue
    .line 1421894
    invoke-direct {p0, p1, p2}, LX/8wC;-><init>(Landroid/support/v7/widget/GridLayout;Z)V

    return-void
.end method

.method private a(II)V
    .locals 2

    .prologue
    .line 1421895
    iget-object v0, p0, LX/8wC;->y:LX/8wF;

    iput p1, v0, LX/8wF;->a:I

    .line 1421896
    iget-object v0, p0, LX/8wC;->z:LX/8wF;

    neg-int v1, p2

    iput v1, v0, LX/8wF;->a:I

    .line 1421897
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/8wC;->p:Z

    .line 1421898
    return-void
.end method

.method private a(LX/8wG;Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8wG",
            "<",
            "LX/8wD;",
            "LX/8wF;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1421899
    iget-object v0, p1, LX/8wG;->c:[Ljava/lang/Object;

    check-cast v0, [LX/8wF;

    move v1, v2

    .line 1421900
    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_0

    .line 1421901
    aget-object v3, v0, v1

    invoke-virtual {v3}, LX/8wF;->a()V

    .line 1421902
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1421903
    :cond_0
    invoke-virtual {p0}, LX/8wC;->b()LX/8wG;

    move-result-object v0

    iget-object v0, v0, LX/8wG;->c:[Ljava/lang/Object;

    check-cast v0, [LX/8w5;

    .line 1421904
    :goto_1
    array-length v1, v0

    if-ge v2, v1, :cond_2

    .line 1421905
    aget-object v1, v0, v2

    invoke-virtual {v1, p2}, LX/8w5;->a(Z)I

    move-result v3

    .line 1421906
    invoke-virtual {p1, v2}, LX/8wG;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8wF;

    .line 1421907
    iget v4, v1, LX/8wF;->a:I

    if-eqz p2, :cond_1

    :goto_2
    invoke-static {v4, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    iput v3, v1, LX/8wF;->a:I

    .line 1421908
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1421909
    :cond_1
    neg-int v3, v3

    goto :goto_2

    .line 1421910
    :cond_2
    return-void
.end method

.method private a(Ljava/lang/String;[LX/8w9;[Z)V
    .locals 5

    .prologue
    .line 1421911
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1421912
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1421913
    const/4 v0, 0x0

    :goto_0
    array-length v3, p2

    if-ge v0, v3, :cond_2

    .line 1421914
    aget-object v3, p2, v0

    .line 1421915
    aget-boolean v4, p3, v0

    if-eqz v4, :cond_0

    .line 1421916
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1421917
    :cond_0
    iget-boolean v4, v3, LX/8w9;->c:Z

    if-nez v4, :cond_1

    .line 1421918
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1421919
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1421920
    :cond_2
    iget-object v0, p0, LX/8wC;->w:Landroid/support/v7/widget/GridLayout;

    iget-object v0, v0, Landroid/support/v7/widget/GridLayout;->j:Landroid/util/Printer;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " constraints: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0, v1}, LX/8wC;->b(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " are inconsistent; permanently removing: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0, v2}, LX/8wC;->b(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    .line 1421921
    return-void
.end method

.method private static a(Ljava/util/List;LX/8wD;LX/8wF;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/8w9;",
            ">;",
            "LX/8wD;",
            "LX/8wF;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1421922
    const/4 v0, 0x1

    invoke-static {p0, p1, p2, v0}, LX/8wC;->a(Ljava/util/List;LX/8wD;LX/8wF;Z)V

    .line 1421923
    return-void
.end method

.method private static a(Ljava/util/List;LX/8wD;LX/8wF;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/8w9;",
            ">;",
            "LX/8wD;",
            "LX/8wF;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 1421924
    invoke-virtual {p1}, LX/8wD;->a()I

    move-result v0

    if-nez v0, :cond_0

    .line 1421925
    :goto_0
    return-void

    .line 1421926
    :cond_0
    if-eqz p3, :cond_2

    .line 1421927
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8w9;

    .line 1421928
    iget-object v0, v0, LX/8w9;->a:LX/8wD;

    .line 1421929
    invoke-virtual {v0, p1}, LX/8wD;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    .line 1421930
    :cond_2
    new-instance v0, LX/8w9;

    invoke-direct {v0, p1, p2}, LX/8w9;-><init>(LX/8wD;LX/8wF;)V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private static a(Ljava/util/List;LX/8wG;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/8w9;",
            ">;",
            "LX/8wG",
            "<",
            "LX/8wD;",
            "LX/8wF;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1421931
    move v1, v2

    :goto_0
    iget-object v0, p1, LX/8wG;->b:[Ljava/lang/Object;

    check-cast v0, [LX/8wD;

    array-length v0, v0

    if-ge v1, v0, :cond_0

    .line 1421932
    iget-object v0, p1, LX/8wG;->b:[Ljava/lang/Object;

    check-cast v0, [LX/8wD;

    aget-object v3, v0, v1

    .line 1421933
    iget-object v0, p1, LX/8wG;->c:[Ljava/lang/Object;

    check-cast v0, [LX/8wF;

    aget-object v0, v0, v1

    invoke-static {p0, v3, v0, v2}, LX/8wC;->a(Ljava/util/List;LX/8wD;LX/8wF;Z)V

    .line 1421934
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1421935
    :cond_0
    return-void
.end method

.method private static a([I)V
    .locals 1

    .prologue
    .line 1421936
    const/4 v0, 0x0

    invoke-static {p0, v0}, Ljava/util/Arrays;->fill([II)V

    .line 1421937
    return-void
.end method

.method private a([LX/8w9;[I)V
    .locals 11

    .prologue
    const/4 v4, 0x0

    .line 1421938
    iget-boolean v0, p0, LX/8wC;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "horizontal"

    .line 1421939
    :goto_0
    invoke-virtual {p0}, LX/8wC;->a()I

    move-result v1

    add-int/lit8 v7, v1, 0x1

    .line 1421940
    const/4 v1, 0x0

    move v5, v4

    .line 1421941
    :goto_1
    array-length v2, p1

    if-ge v5, v2, :cond_2

    .line 1421942
    invoke-static {p2}, LX/8wC;->a([I)V

    move v6, v4

    .line 1421943
    :goto_2
    if-ge v6, v7, :cond_4

    .line 1421944
    array-length v8, p1

    move v2, v4

    move v3, v4

    :goto_3
    if-ge v2, v8, :cond_1

    .line 1421945
    aget-object v9, p1, v2

    invoke-static {p2, v9}, LX/8wC;->a([ILX/8w9;)Z

    move-result v9

    or-int/2addr v3, v9

    .line 1421946
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1421947
    :cond_0
    const-string v0, "vertical"

    goto :goto_0

    .line 1421948
    :cond_1
    if-nez v3, :cond_3

    .line 1421949
    if-eqz v1, :cond_2

    .line 1421950
    invoke-direct {p0, v0, p1, v1}, LX/8wC;->a(Ljava/lang/String;[LX/8w9;[Z)V

    .line 1421951
    :cond_2
    return-void

    .line 1421952
    :cond_3
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_2

    .line 1421953
    :cond_4
    array-length v2, p1

    new-array v2, v2, [Z

    move v6, v4

    .line 1421954
    :goto_4
    if-ge v6, v7, :cond_6

    .line 1421955
    array-length v8, p1

    move v3, v4

    :goto_5
    if-ge v3, v8, :cond_5

    .line 1421956
    aget-boolean v9, v2, v3

    aget-object v10, p1, v3

    invoke-static {p2, v10}, LX/8wC;->a([ILX/8w9;)Z

    move-result v10

    or-int/2addr v9, v10

    aput-boolean v9, v2, v3

    .line 1421957
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 1421958
    :cond_5
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    goto :goto_4

    .line 1421959
    :cond_6
    if-nez v5, :cond_7

    move-object v1, v2

    :cond_7
    move v3, v4

    .line 1421960
    :goto_6
    array-length v6, p1

    if-ge v3, v6, :cond_8

    .line 1421961
    aget-boolean v6, v2, v3

    if-eqz v6, :cond_9

    .line 1421962
    aget-object v6, p1, v3

    .line 1421963
    iget-object v8, v6, LX/8w9;->a:LX/8wD;

    iget v8, v8, LX/8wD;->a:I

    iget-object v9, v6, LX/8w9;->a:LX/8wD;

    iget v9, v9, LX/8wD;->b:I

    if-lt v8, v9, :cond_9

    .line 1421964
    iput-boolean v4, v6, LX/8w9;->c:Z

    .line 1421965
    :cond_8
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_1

    .line 1421966
    :cond_9
    add-int/lit8 v3, v3, 0x1

    goto :goto_6
.end method

.method private static a([ILX/8w9;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1422022
    iget-boolean v1, p1, LX/8w9;->c:Z

    if-nez v1, :cond_1

    .line 1422023
    :cond_0
    :goto_0
    return v0

    .line 1422024
    :cond_1
    iget-object v1, p1, LX/8w9;->a:LX/8wD;

    .line 1422025
    iget v2, v1, LX/8wD;->a:I

    .line 1422026
    iget v1, v1, LX/8wD;->b:I

    .line 1422027
    iget-object v3, p1, LX/8w9;->b:LX/8wF;

    iget v3, v3, LX/8wF;->a:I

    .line 1422028
    aget v2, p0, v2

    add-int/2addr v2, v3

    .line 1422029
    aget v3, p0, v1

    if-le v2, v3, :cond_0

    .line 1422030
    aput v2, p0, v1

    .line 1422031
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(Ljava/util/List;)[LX/8w9;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/8w9;",
            ">;)[",
            "LX/8w9;"
        }
    .end annotation

    .prologue
    .line 1421967
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [LX/8w9;

    invoke-interface {p1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8w9;

    invoke-direct {p0, v0}, LX/8wC;->b([LX/8w9;)[LX/8w9;

    move-result-object v0

    return-object v0
.end method

.method private b(II)I
    .locals 1

    .prologue
    .line 1421968
    invoke-direct {p0, p1, p2}, LX/8wC;->a(II)V

    .line 1421969
    invoke-virtual {p0}, LX/8wC;->e()[I

    move-result-object v0

    invoke-direct {p0, v0}, LX/8wC;->e([I)I

    move-result v0

    return v0
.end method

.method private b(Z)LX/8wG;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/8wG",
            "<",
            "LX/8wD;",
            "LX/8wF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1421970
    const-class v0, LX/8wD;

    const-class v1, LX/8wF;

    invoke-static {v0, v1}, LX/8wA;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/8wA;

    move-result-object v3

    .line 1421971
    invoke-virtual {p0}, LX/8wC;->b()LX/8wG;

    move-result-object v0

    iget-object v0, v0, LX/8wG;->b:[Ljava/lang/Object;

    check-cast v0, [LX/8wH;

    .line 1421972
    const/4 v1, 0x0

    array-length v4, v0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    .line 1421973
    if-eqz p1, :cond_0

    aget-object v1, v0, v2

    iget-object v1, v1, LX/8wH;->c:LX/8wD;

    .line 1421974
    :goto_1
    new-instance v5, LX/8wF;

    invoke-direct {v5}, LX/8wF;-><init>()V

    invoke-virtual {v3, v1, v5}, LX/8wA;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1421975
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1421976
    :cond_0
    aget-object v1, v0, v2

    iget-object v1, v1, LX/8wH;->c:LX/8wD;

    invoke-virtual {v1}, LX/8wD;->b()LX/8wD;

    move-result-object v1

    goto :goto_1

    .line 1421977
    :cond_1
    invoke-virtual {v3}, LX/8wA;->a()LX/8wG;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/util/List;)Ljava/lang/String;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/8w9;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1421978
    iget-boolean v0, p0, LX/8wC;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "x"

    move-object v1, v0

    .line 1421979
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1421980
    const/4 v0, 0x1

    .line 1421981
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v3, v2

    move v2, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8w9;

    .line 1421982
    if-eqz v2, :cond_1

    .line 1421983
    const/4 v2, 0x0

    .line 1421984
    :goto_2
    iget-object v5, v0, LX/8w9;->a:LX/8wD;

    iget v5, v5, LX/8wD;->a:I

    .line 1421985
    iget-object v6, v0, LX/8w9;->a:LX/8wD;

    iget v6, v6, LX/8wD;->b:I

    .line 1421986
    iget-object v0, v0, LX/8w9;->b:LX/8wF;

    iget v0, v0, LX/8wF;->a:I

    .line 1421987
    if-ge v5, v6, :cond_2

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "-"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ">="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1421988
    :cond_0
    const-string v0, "y"

    move-object v1, v0

    goto :goto_0

    .line 1421989
    :cond_1
    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    goto :goto_2

    .line 1421990
    :cond_2
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "-"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    neg-int v0, v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 1421991
    :cond_3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b([I)V
    .locals 1

    .prologue
    .line 1421992
    invoke-direct {p0}, LX/8wC;->p()[LX/8w9;

    move-result-object v0

    invoke-direct {p0, v0, p1}, LX/8wC;->a([LX/8w9;[I)V

    .line 1421993
    return-void
.end method

.method private b([LX/8w9;)[LX/8w9;
    .locals 1

    .prologue
    .line 1421994
    new-instance v0, LX/8wB;

    invoke-direct {v0, p0, p1}, LX/8wB;-><init>(LX/8wC;[LX/8w9;)V

    invoke-virtual {v0}, LX/8wB;->a()[LX/8w9;

    move-result-object v0

    return-object v0
.end method

.method private c(Z)V
    .locals 8

    .prologue
    .line 1421995
    if-eqz p1, :cond_1

    iget-object v0, p0, LX/8wC;->i:[I

    .line 1421996
    :goto_0
    const/4 v1, 0x0

    iget-object v2, p0, LX/8wC;->w:Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v2}, Landroid/support/v7/widget/GridLayout;->getChildCount()I

    move-result v3

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_4

    .line 1421997
    iget-object v1, p0, LX/8wC;->w:Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/GridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 1421998
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v5, 0x8

    if-eq v1, v5, :cond_0

    .line 1421999
    invoke-static {v4}, Landroid/support/v7/widget/GridLayout;->a(Landroid/view/View;)LX/8wE;

    move-result-object v1

    .line 1422000
    iget-boolean v5, p0, LX/8wC;->a:Z

    if-eqz v5, :cond_2

    iget-object v1, v1, LX/8wE;->b:LX/8wH;

    .line 1422001
    :goto_2
    iget-object v1, v1, LX/8wH;->c:LX/8wD;

    .line 1422002
    if-eqz p1, :cond_3

    iget v1, v1, LX/8wD;->a:I

    .line 1422003
    :goto_3
    aget v5, v0, v1

    iget-object v6, p0, LX/8wC;->w:Landroid/support/v7/widget/GridLayout;

    iget-boolean v7, p0, LX/8wC;->a:Z

    invoke-virtual {v6, v4, v7, p1}, Landroid/support/v7/widget/GridLayout;->a(Landroid/view/View;ZZ)I

    move-result v4

    invoke-static {v5, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    aput v4, v0, v1

    .line 1422004
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 1422005
    :cond_1
    iget-object v0, p0, LX/8wC;->k:[I

    goto :goto_0

    .line 1422006
    :cond_2
    iget-object v1, v1, LX/8wE;->a:LX/8wH;

    goto :goto_2

    .line 1422007
    :cond_3
    iget v1, v1, LX/8wD;->b:I

    goto :goto_3

    .line 1422008
    :cond_4
    return-void
.end method

.method private c([I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1422009
    invoke-direct {p0}, LX/8wC;->t()[I

    move-result-object v0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 1422010
    invoke-direct {p0, p1}, LX/8wC;->b([I)V

    .line 1422011
    invoke-direct {p0}, LX/8wC;->u()V

    .line 1422012
    iput-boolean v1, p0, LX/8wC;->n:Z

    .line 1422013
    iput-boolean v1, p0, LX/8wC;->f:Z

    .line 1422014
    iput-boolean v1, p0, LX/8wC;->h:Z

    .line 1422015
    iput-boolean v1, p0, LX/8wC;->d:Z

    .line 1422016
    invoke-direct {p0, p1}, LX/8wC;->b([I)V

    .line 1422017
    return-void
.end method

.method public static d(LX/8wC;I)V
    .locals 4

    .prologue
    .line 1422018
    invoke-direct {p0}, LX/8wC;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1422019
    invoke-direct {p0}, LX/8wC;->s()[I

    move-result-object v0

    iget-object v1, p0, LX/8wC;->w:Landroid/support/v7/widget/GridLayout;

    iget-object v2, p0, LX/8wC;->w:Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v2, p1}, Landroid/support/v7/widget/GridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    iget-boolean v3, p0, LX/8wC;->a:Z

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/widget/GridLayout;->a(Landroid/view/View;Z)I

    move-result v1

    aput v1, v0, p1

    .line 1422020
    :cond_0
    return-void
.end method

.method private d([I)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1421702
    invoke-direct {p0}, LX/8wC;->r()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1421703
    invoke-direct {p0, p1}, LX/8wC;->b([I)V

    .line 1421704
    :goto_0
    iget-boolean v1, p0, LX/8wC;->u:Z

    if-nez v1, :cond_1

    .line 1421705
    aget v1, p1, v0

    .line 1421706
    array-length v2, p1

    :goto_1
    if-ge v0, v2, :cond_1

    .line 1421707
    aget v3, p1, v0

    sub-int/2addr v3, v1

    aput v3, p1, v0

    .line 1421708
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1421709
    :cond_0
    invoke-direct {p0, p1}, LX/8wC;->c([I)V

    goto :goto_0

    .line 1421710
    :cond_1
    return-void
.end method

.method private e([I)I
    .locals 1

    .prologue
    .line 1422021
    invoke-virtual {p0}, LX/8wC;->a()I

    move-result v0

    aget v0, p1, v0

    return v0
.end method

.method private h()I
    .locals 6

    .prologue
    const/4 v3, -0x1

    .line 1421863
    const/4 v0, 0x0

    iget-object v1, p0, LX/8wC;->w:Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v1}, Landroid/support/v7/widget/GridLayout;->getChildCount()I

    move-result v4

    move v2, v0

    move v1, v3

    :goto_0
    if-ge v2, v4, :cond_1

    .line 1421864
    iget-object v0, p0, LX/8wC;->w:Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/GridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1421865
    invoke-static {v0}, Landroid/support/v7/widget/GridLayout;->a(Landroid/view/View;)LX/8wE;

    move-result-object v0

    .line 1421866
    iget-boolean v5, p0, LX/8wC;->a:Z

    if-eqz v5, :cond_0

    iget-object v0, v0, LX/8wE;->b:LX/8wH;

    .line 1421867
    :goto_1
    iget-object v0, v0, LX/8wH;->c:LX/8wD;

    .line 1421868
    iget v5, v0, LX/8wD;->a:I

    invoke-static {v1, v5}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1421869
    iget v5, v0, LX/8wD;->b:I

    invoke-static {v1, v5}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1421870
    invoke-virtual {v0}, LX/8wD;->a()I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1421871
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1421872
    :cond_0
    iget-object v0, v0, LX/8wE;->a:LX/8wH;

    goto :goto_1

    .line 1421873
    :cond_1
    if-ne v1, v3, :cond_2

    const/high16 v0, -0x80000000

    :goto_2
    return v0

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method private i()I
    .locals 2

    .prologue
    .line 1421874
    iget v0, p0, LX/8wC;->x:I

    const/high16 v1, -0x80000000

    if-ne v0, v1, :cond_0

    .line 1421875
    const/4 v0, 0x0

    invoke-direct {p0}, LX/8wC;->h()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LX/8wC;->x:I

    .line 1421876
    :cond_0
    iget v0, p0, LX/8wC;->x:I

    return v0
.end method

.method private j()LX/8wG;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/8wG",
            "<",
            "LX/8wH;",
            "LX/8w5;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1421672
    const-class v0, LX/8wH;

    const-class v1, LX/8w5;

    invoke-static {v0, v1}, LX/8wA;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/8wA;

    move-result-object v2

    .line 1421673
    const/4 v0, 0x0

    iget-object v1, p0, LX/8wC;->w:Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v1}, Landroid/support/v7/widget/GridLayout;->getChildCount()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 1421674
    iget-object v0, p0, LX/8wC;->w:Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/GridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1421675
    invoke-static {v0}, Landroid/support/v7/widget/GridLayout;->a(Landroid/view/View;)LX/8wE;

    move-result-object v0

    .line 1421676
    iget-boolean v4, p0, LX/8wC;->a:Z

    if-eqz v4, :cond_0

    iget-object v0, v0, LX/8wE;->b:LX/8wH;

    .line 1421677
    :goto_1
    iget-object v4, v0, LX/8wH;->d:LX/8vz;

    iget-boolean v5, p0, LX/8wC;->a:Z

    invoke-static {v4, v5}, Landroid/support/v7/widget/GridLayout;->a(LX/8vz;Z)LX/8vz;

    move-result-object v4

    invoke-virtual {v4}, LX/8vz;->a()LX/8w5;

    move-result-object v4

    .line 1421678
    invoke-virtual {v2, v0, v4}, LX/8wA;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1421679
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1421680
    :cond_0
    iget-object v0, v0, LX/8wE;->a:LX/8wH;

    goto :goto_1

    .line 1421681
    :cond_1
    invoke-virtual {v2}, LX/8wA;->a()LX/8wG;

    move-result-object v0

    return-object v0
.end method

.method private k()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1421682
    iget-object v0, p0, LX/8wC;->c:LX/8wG;

    iget-object v0, v0, LX/8wG;->c:[Ljava/lang/Object;

    check-cast v0, [LX/8w5;

    move v1, v2

    .line 1421683
    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_0

    .line 1421684
    aget-object v3, v0, v1

    invoke-virtual {v3}, LX/8w5;->a()V

    .line 1421685
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1421686
    :cond_0
    iget-object v0, p0, LX/8wC;->w:Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v0}, Landroid/support/v7/widget/GridLayout;->getChildCount()I

    move-result v7

    move v6, v2

    :goto_1
    if-ge v6, v7, :cond_3

    .line 1421687
    iget-object v0, p0, LX/8wC;->w:Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v0, v6}, Landroid/support/v7/widget/GridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1421688
    invoke-static {v2}, Landroid/support/v7/widget/GridLayout;->a(Landroid/view/View;)LX/8wE;

    move-result-object v0

    .line 1421689
    iget-boolean v1, p0, LX/8wC;->a:Z

    if-eqz v1, :cond_1

    iget-object v3, v0, LX/8wE;->b:LX/8wH;

    .line 1421690
    :goto_2
    iget v0, v3, LX/8wH;->e:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    iget-object v0, p0, LX/8wC;->w:Landroid/support/v7/widget/GridLayout;

    iget-boolean v1, p0, LX/8wC;->a:Z

    invoke-virtual {v0, v2, v1}, Landroid/support/v7/widget/GridLayout;->a(Landroid/view/View;Z)I

    move-result v5

    .line 1421691
    :goto_3
    iget-object v0, p0, LX/8wC;->c:LX/8wG;

    invoke-virtual {v0, v6}, LX/8wG;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8w5;

    iget-object v1, p0, LX/8wC;->w:Landroid/support/v7/widget/GridLayout;

    move-object v4, p0

    invoke-virtual/range {v0 .. v5}, LX/8w5;->a(Landroid/support/v7/widget/GridLayout;Landroid/view/View;LX/8wH;LX/8wC;I)V

    .line 1421692
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_1

    .line 1421693
    :cond_1
    iget-object v3, v0, LX/8wE;->a:LX/8wH;

    goto :goto_2

    .line 1421694
    :cond_2
    invoke-direct {p0}, LX/8wC;->s()[I

    move-result-object v0

    aget v0, v0, v6

    invoke-direct {p0}, LX/8wC;->t()[I

    move-result-object v1

    aget v1, v1, v6

    add-int v5, v0, v1

    goto :goto_3

    .line 1421695
    :cond_3
    return-void
.end method

.method private l()LX/8wG;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/8wG",
            "<",
            "LX/8wD;",
            "LX/8wF;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 1421696
    iget-object v0, p0, LX/8wC;->e:LX/8wG;

    if-nez v0, :cond_0

    .line 1421697
    invoke-direct {p0, v1}, LX/8wC;->b(Z)LX/8wG;

    move-result-object v0

    iput-object v0, p0, LX/8wC;->e:LX/8wG;

    .line 1421698
    :cond_0
    iget-boolean v0, p0, LX/8wC;->f:Z

    if-nez v0, :cond_1

    .line 1421699
    iget-object v0, p0, LX/8wC;->e:LX/8wG;

    invoke-direct {p0, v0, v1}, LX/8wC;->a(LX/8wG;Z)V

    .line 1421700
    iput-boolean v1, p0, LX/8wC;->f:Z

    .line 1421701
    :cond_1
    iget-object v0, p0, LX/8wC;->e:LX/8wG;

    return-object v0
.end method

.method private m()LX/8wG;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/8wG",
            "<",
            "LX/8wD;",
            "LX/8wF;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1421711
    iget-object v0, p0, LX/8wC;->g:LX/8wG;

    if-nez v0, :cond_0

    .line 1421712
    invoke-direct {p0, v1}, LX/8wC;->b(Z)LX/8wG;

    move-result-object v0

    iput-object v0, p0, LX/8wC;->g:LX/8wG;

    .line 1421713
    :cond_0
    iget-boolean v0, p0, LX/8wC;->h:Z

    if-nez v0, :cond_1

    .line 1421714
    iget-object v0, p0, LX/8wC;->g:LX/8wG;

    invoke-direct {p0, v0, v1}, LX/8wC;->a(LX/8wG;Z)V

    .line 1421715
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8wC;->h:Z

    .line 1421716
    :cond_1
    iget-object v0, p0, LX/8wC;->g:LX/8wG;

    return-object v0
.end method

.method private n()[LX/8w9;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1421717
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1421718
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1421719
    invoke-direct {p0}, LX/8wC;->l()LX/8wG;

    move-result-object v0

    invoke-static {v2, v0}, LX/8wC;->a(Ljava/util/List;LX/8wG;)V

    .line 1421720
    invoke-direct {p0}, LX/8wC;->m()LX/8wG;

    move-result-object v0

    invoke-static {v3, v0}, LX/8wC;->a(Ljava/util/List;LX/8wG;)V

    .line 1421721
    iget-boolean v0, p0, LX/8wC;->u:Z

    if-eqz v0, :cond_0

    move v0, v1

    .line 1421722
    :goto_0
    invoke-virtual {p0}, LX/8wC;->a()I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 1421723
    new-instance v4, LX/8wD;

    add-int/lit8 v5, v0, 0x1

    invoke-direct {v4, v0, v5}, LX/8wD;-><init>(II)V

    new-instance v5, LX/8wF;

    invoke-direct {v5, v1}, LX/8wF;-><init>(I)V

    invoke-static {v2, v4, v5}, LX/8wC;->a(Ljava/util/List;LX/8wD;LX/8wF;)V

    .line 1421724
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1421725
    :cond_0
    invoke-virtual {p0}, LX/8wC;->a()I

    move-result v0

    .line 1421726
    new-instance v4, LX/8wD;

    invoke-direct {v4, v1, v0}, LX/8wD;-><init>(II)V

    iget-object v5, p0, LX/8wC;->y:LX/8wF;

    invoke-static {v2, v4, v5, v1}, LX/8wC;->a(Ljava/util/List;LX/8wD;LX/8wF;Z)V

    .line 1421727
    new-instance v4, LX/8wD;

    invoke-direct {v4, v0, v1}, LX/8wD;-><init>(II)V

    iget-object v0, p0, LX/8wC;->z:LX/8wF;

    invoke-static {v3, v4, v0, v1}, LX/8wC;->a(Ljava/util/List;LX/8wD;LX/8wF;Z)V

    .line 1421728
    invoke-direct {p0, v2}, LX/8wC;->a(Ljava/util/List;)[LX/8w9;

    move-result-object v0

    .line 1421729
    invoke-direct {p0, v3}, LX/8wC;->a(Ljava/util/List;)[LX/8w9;

    move-result-object v1

    .line 1421730
    invoke-static {v0, v1}, Landroid/support/v7/widget/GridLayout;->a([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8w9;

    return-object v0
.end method

.method private o()V
    .locals 0

    .prologue
    .line 1421731
    invoke-direct {p0}, LX/8wC;->l()LX/8wG;

    .line 1421732
    invoke-direct {p0}, LX/8wC;->m()LX/8wG;

    .line 1421733
    return-void
.end method

.method private p()[LX/8w9;
    .locals 1

    .prologue
    .line 1421734
    iget-object v0, p0, LX/8wC;->m:[LX/8w9;

    if-nez v0, :cond_0

    .line 1421735
    invoke-direct {p0}, LX/8wC;->n()[LX/8w9;

    move-result-object v0

    iput-object v0, p0, LX/8wC;->m:[LX/8w9;

    .line 1421736
    :cond_0
    iget-boolean v0, p0, LX/8wC;->n:Z

    if-nez v0, :cond_1

    .line 1421737
    invoke-direct {p0}, LX/8wC;->o()V

    .line 1421738
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8wC;->n:Z

    .line 1421739
    :cond_1
    iget-object v0, p0, LX/8wC;->m:[LX/8w9;

    return-object v0
.end method

.method private q()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1421740
    iget-object v0, p0, LX/8wC;->w:Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v0}, Landroid/support/v7/widget/GridLayout;->getChildCount()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_2

    .line 1421741
    iget-object v0, p0, LX/8wC;->w:Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/GridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/GridLayout;->a(Landroid/view/View;)LX/8wE;

    move-result-object v0

    .line 1421742
    iget-boolean v4, p0, LX/8wC;->a:Z

    if-eqz v4, :cond_0

    iget-object v0, v0, LX/8wE;->b:LX/8wH;

    .line 1421743
    :goto_1
    iget v0, v0, LX/8wH;->e:F

    const/4 v4, 0x0

    cmpl-float v0, v0, v4

    if-eqz v0, :cond_1

    .line 1421744
    const/4 v0, 0x1

    .line 1421745
    :goto_2
    return v0

    .line 1421746
    :cond_0
    iget-object v0, v0, LX/8wE;->a:LX/8wH;

    goto :goto_1

    .line 1421747
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 1421748
    goto :goto_2
.end method

.method private r()Z
    .locals 1

    .prologue
    .line 1421749
    iget-boolean v0, p0, LX/8wC;->r:Z

    if-nez v0, :cond_0

    .line 1421750
    invoke-direct {p0}, LX/8wC;->q()Z

    move-result v0

    iput-boolean v0, p0, LX/8wC;->q:Z

    .line 1421751
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8wC;->r:Z

    .line 1421752
    :cond_0
    iget-boolean v0, p0, LX/8wC;->q:Z

    return v0
.end method

.method private s()[I
    .locals 1

    .prologue
    .line 1421753
    iget-object v0, p0, LX/8wC;->s:[I

    if-nez v0, :cond_0

    .line 1421754
    iget-object v0, p0, LX/8wC;->w:Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v0}, Landroid/support/v7/widget/GridLayout;->getChildCount()I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, LX/8wC;->s:[I

    .line 1421755
    :cond_0
    iget-object v0, p0, LX/8wC;->s:[I

    return-object v0
.end method

.method private t()[I
    .locals 1

    .prologue
    .line 1421756
    iget-object v0, p0, LX/8wC;->t:[I

    if-nez v0, :cond_0

    .line 1421757
    iget-object v0, p0, LX/8wC;->w:Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v0}, Landroid/support/v7/widget/GridLayout;->getChildCount()I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, LX/8wC;->t:[I

    .line 1421758
    :cond_0
    iget-object v0, p0, LX/8wC;->t:[I

    return-object v0
.end method

.method private u()V
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x0

    .line 1421759
    iget-object v0, p0, LX/8wC;->w:Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v0}, Landroid/support/v7/widget/GridLayout;->getChildCount()I

    move-result v6

    move v4, v3

    move v1, v5

    move v2, v3

    :goto_0
    if-ge v4, v6, :cond_1

    .line 1421760
    iget-object v0, p0, LX/8wC;->w:Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/GridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 1421761
    invoke-static {v7}, Landroid/support/v7/widget/GridLayout;->a(Landroid/view/View;)LX/8wE;

    move-result-object v0

    .line 1421762
    iget-boolean v8, p0, LX/8wC;->a:Z

    if-eqz v8, :cond_0

    iget-object v0, v0, LX/8wE;->b:LX/8wH;

    .line 1421763
    :goto_1
    iget v0, v0, LX/8wH;->e:F

    .line 1421764
    cmpl-float v8, v0, v5

    if-eqz v8, :cond_5

    .line 1421765
    iget-object v8, p0, LX/8wC;->w:Landroid/support/v7/widget/GridLayout;

    iget-boolean v9, p0, LX/8wC;->a:Z

    invoke-static {v8, v7, v9}, Landroid/support/v7/widget/GridLayout;->a(Landroid/support/v7/widget/GridLayout;Landroid/view/View;Z)I

    move-result v7

    invoke-direct {p0}, LX/8wC;->s()[I

    move-result-object v8

    aget v8, v8, v4

    sub-int/2addr v7, v8

    .line 1421766
    add-int/2addr v2, v7

    .line 1421767
    add-float/2addr v0, v1

    move v1, v2

    .line 1421768
    :goto_2
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 1421769
    :cond_0
    iget-object v0, v0, LX/8wE;->a:LX/8wH;

    goto :goto_1

    .line 1421770
    :cond_1
    iget-object v0, p0, LX/8wC;->w:Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v0}, Landroid/support/v7/widget/GridLayout;->getChildCount()I

    move-result v4

    :goto_3
    if-ge v3, v4, :cond_3

    .line 1421771
    iget-object v0, p0, LX/8wC;->w:Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/GridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/GridLayout;->a(Landroid/view/View;)LX/8wE;

    move-result-object v0

    .line 1421772
    iget-boolean v6, p0, LX/8wC;->a:Z

    if-eqz v6, :cond_2

    iget-object v0, v0, LX/8wE;->b:LX/8wH;

    .line 1421773
    :goto_4
    iget v0, v0, LX/8wH;->e:F

    .line 1421774
    cmpl-float v6, v0, v5

    if-eqz v6, :cond_4

    .line 1421775
    int-to-float v6, v2

    mul-float/2addr v6, v0

    div-float/2addr v6, v1

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    .line 1421776
    iget-object v7, p0, LX/8wC;->t:[I

    aput v6, v7, v3

    .line 1421777
    sub-int/2addr v2, v6

    .line 1421778
    sub-float v0, v1, v0

    move v1, v2

    .line 1421779
    :goto_5
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    move v1, v0

    goto :goto_3

    .line 1421780
    :cond_2
    iget-object v0, v0, LX/8wE;->a:LX/8wH;

    goto :goto_4

    .line 1421781
    :cond_3
    return-void

    :cond_4
    move v0, v1

    move v1, v2

    goto :goto_5

    :cond_5
    move v0, v1

    move v1, v2

    goto :goto_2
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 1421782
    iget v0, p0, LX/8wC;->b:I

    invoke-direct {p0}, LX/8wC;->i()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 1421783
    const/high16 v0, -0x80000000

    if-eq p1, v0, :cond_0

    invoke-direct {p0}, LX/8wC;->i()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 1421784
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-boolean v0, p0, LX/8wC;->a:Z

    if-eqz v0, :cond_1

    const-string v0, "column"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Count must be greater than or equal to the maximum of all grid indices (and spans) defined in the LayoutParams of each child"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/GridLayout;->b(Ljava/lang/String;)V

    .line 1421785
    :cond_0
    iput p1, p0, LX/8wC;->b:I

    .line 1421786
    return-void

    .line 1421787
    :cond_1
    const-string v0, "row"

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 1421788
    iput-boolean p1, p0, LX/8wC;->u:Z

    .line 1421789
    invoke-virtual {p0}, LX/8wC;->f()V

    .line 1421790
    return-void
.end method

.method public final a([LX/8w9;)[[LX/8w9;
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 1421791
    invoke-virtual {p0}, LX/8wC;->a()I

    move-result v1

    add-int/lit8 v2, v1, 0x1

    .line 1421792
    new-array v3, v2, [[LX/8w9;

    .line 1421793
    new-array v4, v2, [I

    .line 1421794
    array-length v5, p1

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_0

    aget-object v6, p1, v1

    .line 1421795
    iget-object v6, v6, LX/8w9;->a:LX/8wD;

    iget v6, v6, LX/8wD;->a:I

    aget v7, v4, v6

    add-int/lit8 v7, v7, 0x1

    aput v7, v4, v6

    .line 1421796
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    move v1, v0

    .line 1421797
    :goto_1
    if-ge v1, v2, :cond_1

    .line 1421798
    aget v5, v4, v1

    new-array v5, v5, [LX/8w9;

    aput-object v5, v3, v1

    .line 1421799
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1421800
    :cond_1
    invoke-static {v4, v0}, Ljava/util/Arrays;->fill([II)V

    .line 1421801
    array-length v1, p1

    :goto_2
    if-ge v0, v1, :cond_2

    aget-object v2, p1, v0

    .line 1421802
    iget-object v5, v2, LX/8w9;->a:LX/8wD;

    iget v5, v5, LX/8wD;->a:I

    .line 1421803
    aget-object v6, v3, v5

    aget v7, v4, v5

    add-int/lit8 v8, v7, 0x1

    aput v8, v4, v5

    aput-object v2, v6, v7

    .line 1421804
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1421805
    :cond_2
    return-object v3
.end method

.method public final b(I)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1421806
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 1421807
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 1421808
    sparse-switch v1, :sswitch_data_0

    .line 1421809
    sget-boolean v1, LX/8wC;->v:Z

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1421810
    :sswitch_0
    const v1, 0x186a0

    invoke-direct {p0, v0, v1}, LX/8wC;->b(II)I

    move-result v0

    .line 1421811
    :cond_0
    :goto_0
    return v0

    .line 1421812
    :sswitch_1
    invoke-direct {p0, v2, v2}, LX/8wC;->b(II)I

    move-result v0

    goto :goto_0

    .line 1421813
    :sswitch_2
    invoke-direct {p0, v0, v2}, LX/8wC;->b(II)I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_2
        0x0 -> :sswitch_0
        0x40000000 -> :sswitch_1
    .end sparse-switch
.end method

.method public final b()LX/8wG;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/8wG",
            "<",
            "LX/8wH;",
            "LX/8w5;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1421814
    iget-object v0, p0, LX/8wC;->c:LX/8wG;

    if-nez v0, :cond_0

    .line 1421815
    invoke-direct {p0}, LX/8wC;->j()LX/8wG;

    move-result-object v0

    iput-object v0, p0, LX/8wC;->c:LX/8wG;

    .line 1421816
    :cond_0
    iget-boolean v0, p0, LX/8wC;->d:Z

    if-nez v0, :cond_1

    .line 1421817
    invoke-direct {p0}, LX/8wC;->k()V

    .line 1421818
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8wC;->d:Z

    .line 1421819
    :cond_1
    iget-object v0, p0, LX/8wC;->c:LX/8wG;

    return-object v0
.end method

.method public final c(I)V
    .locals 0

    .prologue
    .line 1421820
    invoke-direct {p0, p1, p1}, LX/8wC;->a(II)V

    .line 1421821
    invoke-virtual {p0}, LX/8wC;->e()[I

    .line 1421822
    return-void
.end method

.method public final c()[I
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1421823
    iget-object v0, p0, LX/8wC;->i:[I

    if-nez v0, :cond_0

    .line 1421824
    invoke-virtual {p0}, LX/8wC;->a()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, LX/8wC;->i:[I

    .line 1421825
    :cond_0
    iget-boolean v0, p0, LX/8wC;->j:Z

    if-nez v0, :cond_1

    .line 1421826
    invoke-direct {p0, v1}, LX/8wC;->c(Z)V

    .line 1421827
    iput-boolean v1, p0, LX/8wC;->j:Z

    .line 1421828
    :cond_1
    iget-object v0, p0, LX/8wC;->i:[I

    return-object v0
.end method

.method public final d()[I
    .locals 1

    .prologue
    .line 1421829
    iget-object v0, p0, LX/8wC;->k:[I

    if-nez v0, :cond_0

    .line 1421830
    invoke-virtual {p0}, LX/8wC;->a()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, LX/8wC;->k:[I

    .line 1421831
    :cond_0
    iget-boolean v0, p0, LX/8wC;->l:Z

    if-nez v0, :cond_1

    .line 1421832
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/8wC;->c(Z)V

    .line 1421833
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8wC;->l:Z

    .line 1421834
    :cond_1
    iget-object v0, p0, LX/8wC;->k:[I

    return-object v0
.end method

.method public final e()[I
    .locals 1

    .prologue
    .line 1421835
    iget-object v0, p0, LX/8wC;->o:[I

    if-nez v0, :cond_0

    .line 1421836
    invoke-virtual {p0}, LX/8wC;->a()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 1421837
    new-array v0, v0, [I

    iput-object v0, p0, LX/8wC;->o:[I

    .line 1421838
    :cond_0
    iget-boolean v0, p0, LX/8wC;->p:Z

    if-nez v0, :cond_1

    .line 1421839
    iget-object v0, p0, LX/8wC;->o:[I

    invoke-direct {p0, v0}, LX/8wC;->d([I)V

    .line 1421840
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8wC;->p:Z

    .line 1421841
    :cond_1
    iget-object v0, p0, LX/8wC;->o:[I

    return-object v0
.end method

.method public final f()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1421842
    const/high16 v0, -0x80000000

    iput v0, p0, LX/8wC;->x:I

    .line 1421843
    iput-object v1, p0, LX/8wC;->c:LX/8wG;

    .line 1421844
    iput-object v1, p0, LX/8wC;->e:LX/8wG;

    .line 1421845
    iput-object v1, p0, LX/8wC;->g:LX/8wG;

    .line 1421846
    iput-object v1, p0, LX/8wC;->i:[I

    .line 1421847
    iput-object v1, p0, LX/8wC;->k:[I

    .line 1421848
    iput-object v1, p0, LX/8wC;->m:[LX/8w9;

    .line 1421849
    iput-object v1, p0, LX/8wC;->o:[I

    .line 1421850
    iput-object v1, p0, LX/8wC;->s:[I

    .line 1421851
    iput-object v1, p0, LX/8wC;->t:[I

    .line 1421852
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/8wC;->r:Z

    .line 1421853
    invoke-virtual {p0}, LX/8wC;->g()V

    .line 1421854
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1421855
    iput-boolean v0, p0, LX/8wC;->d:Z

    .line 1421856
    iput-boolean v0, p0, LX/8wC;->f:Z

    .line 1421857
    iput-boolean v0, p0, LX/8wC;->h:Z

    .line 1421858
    iput-boolean v0, p0, LX/8wC;->j:Z

    .line 1421859
    iput-boolean v0, p0, LX/8wC;->l:Z

    .line 1421860
    iput-boolean v0, p0, LX/8wC;->n:Z

    .line 1421861
    iput-boolean v0, p0, LX/8wC;->p:Z

    .line 1421862
    return-void
.end method
