.class public LX/9Hp;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/9Hn;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9Hq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1462176
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/9Hp;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/9Hq;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1462173
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1462174
    iput-object p1, p0, LX/9Hp;->b:LX/0Ot;

    .line 1462175
    return-void
.end method

.method public static a(LX/0QB;)LX/9Hp;
    .locals 4

    .prologue
    .line 1462162
    const-class v1, LX/9Hp;

    monitor-enter v1

    .line 1462163
    :try_start_0
    sget-object v0, LX/9Hp;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1462164
    sput-object v2, LX/9Hp;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1462165
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1462166
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1462167
    new-instance v3, LX/9Hp;

    const/16 p0, 0x1dda

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/9Hp;-><init>(LX/0Ot;)V

    .line 1462168
    move-object v0, v3

    .line 1462169
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1462170
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/9Hp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1462171
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1462172
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1462161
    const v0, 0x5b74fa0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 1

    .prologue
    .line 1462156
    iget-object v0, p0, LX/9Hp;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    const/16 p2, 0xff

    .line 1462157
    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v0

    const/16 p0, 0xc8

    invoke-static {p0, p2, p2, p2}, Landroid/graphics/Color;->argb(IIII)I

    move-result p0

    invoke-virtual {v0, p0}, LX/25Q;->h(I)LX/25Q;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    .line 1462158
    const p0, 0x5b74fa0

    const/4 p2, 0x0

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 1462159
    invoke-interface {v0, p0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v0

    const/4 p0, 0x1

    invoke-interface {v0, p0}, LX/1Di;->c(I)LX/1Di;

    move-result-object v0

    const/16 p0, 0x8

    const/4 p2, 0x0

    invoke-interface {v0, p0, p2}, LX/1Di;->m(II)LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    move-object v0, v0

    .line 1462160
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1462139
    invoke-static {}, LX/1dS;->b()V

    .line 1462140
    iget v0, p1, LX/1dQ;->b:I

    .line 1462141
    packed-switch v0, :pswitch_data_0

    .line 1462142
    :goto_0
    return-object v2

    .line 1462143
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1462144
    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1462145
    check-cast v1, LX/9Ho;

    .line 1462146
    iget-object p1, p0, LX/9Hp;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, LX/9Ho;->a:LX/9FA;

    .line 1462147
    iget-object p0, p1, LX/9FA;->f:LX/9Ce;

    move-object p0, p0

    .line 1462148
    if-eqz p0, :cond_0

    .line 1462149
    iget-object p0, p1, LX/9FA;->f:LX/9Ce;

    move-object p0, p0

    .line 1462150
    iget-object v0, p0, LX/9Ce;->b:LX/9CP;

    move-object p0, v0

    .line 1462151
    if-eqz p0, :cond_0

    .line 1462152
    iget-object p0, p1, LX/9FA;->f:LX/9Ce;

    move-object p0, p0

    .line 1462153
    iget-object v0, p0, LX/9Ce;->b:LX/9CP;

    move-object p0, v0

    .line 1462154
    invoke-virtual {p0}, LX/9CP;->b()V

    .line 1462155
    :cond_0
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x5b74fa0
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/9Hn;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1462131
    new-instance v1, LX/9Ho;

    invoke-direct {v1, p0}, LX/9Ho;-><init>(LX/9Hp;)V

    .line 1462132
    sget-object v2, LX/9Hp;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/9Hn;

    .line 1462133
    if-nez v2, :cond_0

    .line 1462134
    new-instance v2, LX/9Hn;

    invoke-direct {v2}, LX/9Hn;-><init>()V

    .line 1462135
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/9Hn;->a$redex0(LX/9Hn;LX/1De;IILX/9Ho;)V

    .line 1462136
    move-object v1, v2

    .line 1462137
    move-object v0, v1

    .line 1462138
    return-object v0
.end method
