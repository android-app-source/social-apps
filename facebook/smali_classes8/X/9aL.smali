.class public LX/9aL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/9aS;

.field public b:F

.field public c:F

.field public d:F

.field public e:F

.field public f:F

.field public g:F

.field private h:F

.field private i:F

.field private j:F

.field public k:F

.field public l:F

.field public m:J

.field public n:J


# direct methods
.method public constructor <init>(LX/9aS;)V
    .locals 0

    .prologue
    .line 1513569
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1513570
    invoke-virtual {p0, p1}, LX/9aL;->a(LX/9aS;)V

    .line 1513571
    return-void
.end method


# virtual methods
.method public final a(LX/9aS;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/high16 v1, 0x7f800000    # Float.POSITIVE_INFINITY

    const/high16 v0, -0x800000    # Float.NEGATIVE_INFINITY

    const/4 v3, 0x0

    .line 1513572
    iput-object p1, p0, LX/9aL;->a:LX/9aS;

    .line 1513573
    iput-wide v4, p0, LX/9aL;->m:J

    .line 1513574
    iput-wide v4, p0, LX/9aL;->n:J

    .line 1513575
    iput v3, p0, LX/9aL;->c:F

    .line 1513576
    iput v3, p0, LX/9aL;->d:F

    .line 1513577
    iget-object v2, p1, LX/9aS;->d:LX/9aT;

    invoke-interface {v2}, LX/9aT;->a()F

    move-result v2

    move v2, v2

    .line 1513578
    iput v2, p0, LX/9aL;->f:F

    .line 1513579
    iget-object v2, p1, LX/9aS;->e:LX/9aT;

    invoke-interface {v2}, LX/9aT;->a()F

    move-result v2

    move v2, v2

    .line 1513580
    iput v2, p0, LX/9aL;->g:F

    .line 1513581
    iget-object v2, p1, LX/9aS;->g:LX/9aT;

    invoke-interface {v2}, LX/9aT;->a()F

    move-result v2

    move v2, v2

    .line 1513582
    iput v2, p0, LX/9aL;->e:F

    .line 1513583
    iget-boolean v2, p1, LX/9aS;->f:Z

    move v2, v2

    .line 1513584
    if-eqz v2, :cond_2

    .line 1513585
    iget v2, p0, LX/9aL;->g:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    iget v0, p0, LX/9aL;->g:F

    :cond_0
    iput v0, p0, LX/9aL;->h:F

    .line 1513586
    iget v0, p0, LX/9aL;->g:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_1

    iget v0, p0, LX/9aL;->g:F

    :goto_0
    iput v0, p0, LX/9aL;->i:F

    .line 1513587
    :goto_1
    iget-object v0, p1, LX/9aS;->h:LX/9aT;

    invoke-interface {v0}, LX/9aT;->a()F

    move-result v0

    move v0, v0

    .line 1513588
    iput v0, p0, LX/9aL;->b:F

    .line 1513589
    iget-object v0, p1, LX/9aS;->i:LX/9aT;

    invoke-interface {v0}, LX/9aT;->a()F

    move-result v0

    move v0, v0

    .line 1513590
    iput v0, p0, LX/9aL;->j:F

    .line 1513591
    iget-object v0, p1, LX/9aS;->j:LX/9aT;

    invoke-interface {v0}, LX/9aT;->a()F

    move-result v0

    move v0, v0

    .line 1513592
    iput v0, p0, LX/9aL;->k:F

    .line 1513593
    iget-object v0, p1, LX/9aS;->l:LX/9aT;

    invoke-interface {v0}, LX/9aT;->a()F

    move-result v0

    move v0, v0

    .line 1513594
    iput v0, p0, LX/9aL;->l:F

    .line 1513595
    return-void

    :cond_1
    move v0, v1

    .line 1513596
    goto :goto_0

    .line 1513597
    :cond_2
    iput v0, p0, LX/9aL;->h:F

    .line 1513598
    iput v1, p0, LX/9aL;->i:F

    goto :goto_1
.end method

.method public final a(Landroid/graphics/Canvas;Landroid/graphics/Paint;J)V
    .locals 10

    .prologue
    .line 1513554
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1513555
    iget v0, p0, LX/9aL;->c:F

    iget v1, p0, LX/9aL;->d:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1513556
    iget v0, p0, LX/9aL;->b:F

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->rotate(F)V

    .line 1513557
    iget v0, p0, LX/9aL;->k:F

    iget v1, p0, LX/9aL;->k:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->scale(FF)V

    .line 1513558
    iget-object v0, p0, LX/9aL;->a:LX/9aS;

    .line 1513559
    iget-object v3, p0, LX/9aL;->a:LX/9aS;

    .line 1513560
    iget-wide v8, v3, LX/9aS;->k:J

    move-wide v3, v8

    .line 1513561
    const-wide/16 v5, 0x0

    cmp-long v5, v3, v5

    if-lez v5, :cond_0

    iget-wide v5, p0, LX/9aL;->m:J

    sub-long v5, p3, v5

    long-to-float v5, v5

    long-to-float v3, v3

    div-float v3, v5, v3

    .line 1513562
    :goto_0
    iget v4, p0, LX/9aL;->l:F

    add-float/2addr v3, v4

    const/high16 v4, 0x3f800000    # 1.0f

    rem-float/2addr v3, v4

    move v1, v3

    .line 1513563
    iget-object v2, v0, LX/9aS;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    int-to-float v2, v2

    const/4 v3, 0x0

    const v4, 0x3f7d70a4    # 0.99f

    invoke-static {v1, v3, v4}, LX/0yq;->b(FFF)F

    move-result v3

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 1513564
    iget-object v3, v0, LX/9aS;->b:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    .line 1513565
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    neg-int v3, v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    neg-int v4, v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    invoke-virtual {p1, v2, v3, v4, p2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1513566
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1513567
    return-void

    .line 1513568
    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public final b(J)V
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 1513536
    iget-wide v0, p0, LX/9aL;->n:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 1513537
    iput-wide p1, p0, LX/9aL;->n:J

    .line 1513538
    :cond_0
    iget-wide v0, p0, LX/9aL;->m:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    .line 1513539
    iput-wide p1, p0, LX/9aL;->m:J

    .line 1513540
    :cond_1
    iget-wide v0, p0, LX/9aL;->n:J

    sub-long v0, p1, v0

    long-to-float v0, v0

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    .line 1513541
    iget v1, p0, LX/9aL;->e:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_2

    iget v1, p0, LX/9aL;->g:F

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_3

    :cond_2
    iget v1, p0, LX/9aL;->e:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_4

    iget v1, p0, LX/9aL;->g:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_4

    .line 1513542
    :cond_3
    const/high16 v1, 0x40a00000    # 5.0f

    const/high16 v2, 0x41700000    # 15.0f

    iget v3, p0, LX/9aL;->g:F

    neg-float v3, v3

    mul-float/2addr v2, v3

    iget v3, p0, LX/9aL;->e:F

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    .line 1513543
    iget v2, p0, LX/9aL;->g:F

    iget v3, p0, LX/9aL;->e:F

    mul-float/2addr v1, v3

    mul-float/2addr v1, v0

    add-float/2addr v1, v2

    iput v1, p0, LX/9aL;->g:F

    .line 1513544
    iget v1, p0, LX/9aL;->f:F

    const/high16 v2, 0x3f800000    # 1.0f

    const/high16 v3, 0x41200000    # 10.0f

    mul-float/2addr v3, v0

    sub-float/2addr v2, v3

    mul-float/2addr v1, v2

    iput v1, p0, LX/9aL;->f:F

    .line 1513545
    :goto_0
    iget v1, p0, LX/9aL;->g:F

    iget v2, p0, LX/9aL;->h:F

    iget v3, p0, LX/9aL;->i:F

    invoke-static {v1, v2, v3}, LX/0yq;->b(FFF)F

    move-result v1

    iput v1, p0, LX/9aL;->g:F

    .line 1513546
    iget v1, p0, LX/9aL;->c:F

    iget v2, p0, LX/9aL;->f:F

    mul-float/2addr v2, v0

    add-float/2addr v1, v2

    iput v1, p0, LX/9aL;->c:F

    .line 1513547
    iget v1, p0, LX/9aL;->d:F

    iget v2, p0, LX/9aL;->g:F

    mul-float/2addr v2, v0

    add-float/2addr v1, v2

    iput v1, p0, LX/9aL;->d:F

    .line 1513548
    iget v1, p0, LX/9aL;->b:F

    iget v2, p0, LX/9aL;->j:F

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    iput v0, p0, LX/9aL;->b:F

    .line 1513549
    iput-wide p1, p0, LX/9aL;->n:J

    .line 1513550
    return-void

    .line 1513551
    :cond_4
    iget v1, p0, LX/9aL;->g:F

    iget v2, p0, LX/9aL;->e:F

    mul-float/2addr v2, v0

    add-float/2addr v1, v2

    iput v1, p0, LX/9aL;->g:F

    goto :goto_0
.end method

.method public final f()F
    .locals 1

    .prologue
    .line 1513553
    iget-object v0, p0, LX/9aL;->a:LX/9aS;

    invoke-virtual {v0}, LX/9aS;->b()I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method public final g()F
    .locals 1

    .prologue
    .line 1513552
    iget-object v0, p0, LX/9aL;->a:LX/9aS;

    invoke-virtual {v0}, LX/9aS;->c()I

    move-result v0

    int-to-float v0, v0

    return v0
.end method
