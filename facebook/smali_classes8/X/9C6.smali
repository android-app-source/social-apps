.class public final LX/9C6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/Object;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:Landroid/os/Bundle;

.field public final synthetic c:Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1453050
    iput-object p1, p0, LX/9C6;->c:Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;

    iput-object p2, p0, LX/9C6;->a:Landroid/view/View;

    iput-object p3, p0, LX/9C6;->b:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1453051
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-nez v0, :cond_0

    .line 1453052
    iget-object v0, p0, LX/9C6;->c:Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;

    iget-object v0, v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->b:Ljava/lang/String;

    const-string v2, "background thread error"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1453053
    :cond_0
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1453054
    check-cast p1, Ljava/util/List;

    const/4 v2, 0x1

    .line 1453055
    iget-object v0, p0, LX/9C6;->c:Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;

    iget-boolean v0, v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->ac:Z

    if-eqz v0, :cond_0

    .line 1453056
    :goto_0
    return-void

    .line 1453057
    :cond_0
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewGroup;

    if-nez v0, :cond_2

    .line 1453058
    :cond_1
    iget-object v0, p0, LX/9C6;->c:Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;

    iget-object v0, v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->b:Ljava/lang/String;

    const-string v2, "bg inflated view not found where expected"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1453059
    :cond_2
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1453060
    iget-object v1, p0, LX/9C6;->c:Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;

    iget-object v1, v1, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->p:LX/1nK;

    invoke-virtual {v1}, LX/1nK;->l()V

    .line 1453061
    iget-object v1, p0, LX/9C6;->a:Landroid/view/View;

    check-cast v1, Landroid/view/ViewGroup;

    .line 1453062
    const/4 v4, 0x0

    .line 1453063
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    new-array p1, v3, [Landroid/view/View;

    move v3, v4

    .line 1453064
    :goto_1
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ge v3, v2, :cond_3

    .line 1453065
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    aput-object v2, p1, v3

    .line 1453066
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1453067
    :cond_3
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 1453068
    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 1453069
    array-length v3, p1

    :goto_2
    if-ge v4, v3, :cond_4

    aget-object v2, p1, v4

    .line 1453070
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1453071
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 1453072
    :cond_4
    iget-object v0, p0, LX/9C6;->c:Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;

    iget-object v1, p0, LX/9C6;->a:Landroid/view/View;

    iget-object v2, p0, LX/9C6;->b:Landroid/os/Bundle;

    invoke-static {v0, v1, v2}, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->a$redex0(Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;Landroid/view/View;Landroid/os/Bundle;)V

    .line 1453073
    iget-object v0, p0, LX/9C6;->c:Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;

    iget-object v0, v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->p:LX/1nK;

    invoke-virtual {v0}, LX/1nK;->m()V

    .line 1453074
    iget-object v0, p0, LX/9C6;->c:Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;

    iget-object v0, v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->p:LX/1nK;

    .line 1453075
    iget-object v1, v0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0x390020

    const-string v3, "NNF_FlyoutBgInflatableFeedbackTotalTime"

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 1453076
    goto :goto_0
.end method
