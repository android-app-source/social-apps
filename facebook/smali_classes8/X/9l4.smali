.class public LX/9l4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9l2;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2gU;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1MZ;",
            ">;"
        }
    .end annotation
.end field

.field public c:Landroid/net/ConnectivityManager;

.field public d:LX/1tP;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;Landroid/net/ConnectivityManager;LX/1tP;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2gU;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1MZ;",
            ">;",
            "Landroid/net/ConnectivityManager;",
            "LX/1tP;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1532418
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1532419
    iput-object p1, p0, LX/9l4;->a:LX/0Ot;

    .line 1532420
    iput-object p2, p0, LX/9l4;->b:LX/0Ot;

    .line 1532421
    iput-object p3, p0, LX/9l4;->c:Landroid/net/ConnectivityManager;

    .line 1532422
    iput-object p4, p0, LX/9l4;->d:LX/1tP;

    .line 1532423
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/9lB;)V
    .locals 8

    .prologue
    .line 1532403
    iget-object v0, p0, LX/9l4;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1MZ;

    invoke-virtual {v0}, LX/1MZ;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1532404
    sget-object v0, LX/9lB;->ACTIVE:LX/9lB;

    if-ne p2, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1532405
    iget-object v1, p0, LX/9l4;->d:LX/1tP;

    invoke-virtual {v1}, LX/1tP;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1532406
    iget-object v1, p0, LX/9l4;->c:Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 1532407
    invoke-static {v1}, LX/0HX;->a(Landroid/net/NetworkInfo;)LX/0HW;

    move-result-object v1

    sget-object v2, LX/0HW;->MOBILE_2G:LX/0HW;

    if-ne v1, v2, :cond_2

    .line 1532408
    :cond_0
    :goto_1
    return-void

    .line 1532409
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1532410
    :cond_2
    new-instance v1, LX/6mj;

    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v5, v3}, LX/6mj;-><init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;)V

    .line 1532411
    :try_start_0
    new-instance v2, LX/1so;

    new-instance v3, LX/1sp;

    invoke-direct {v3}, LX/1sp;-><init>()V

    invoke-direct {v2, v3}, LX/1so;-><init>(LX/1sq;)V

    .line 1532412
    invoke-virtual {v2, v1}, LX/1so;->a(LX/1u2;)[B

    move-result-object v1

    .line 1532413
    array-length v2, v1

    add-int/lit8 v2, v2, 0x1

    new-array v2, v2, [B

    .line 1532414
    const/4 v3, 0x0

    const/4 v4, 0x1

    array-length v5, v1

    invoke-static {v1, v3, v2, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1532415
    iget-object v1, p0, LX/9l4;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2gU;

    const-string v3, "/t_st"

    sget-object v4, LX/2I2;->FIRE_AND_FORGET:LX/2I2;

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v2, v4, v5}, LX/2gU;->a(Ljava/lang/String;[BLX/2I2;LX/76H;)I
    :try_end_0
    .catch LX/7H0; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1532416
    :catch_0
    move-exception v1

    .line 1532417
    new-array v2, v7, [Ljava/lang/Object;

    invoke-virtual {v1}, LX/7H0;->getMessage()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v1}, LX/7H0;->getMessage()Ljava/lang/String;

    move-result-object v1

    :goto_2
    aput-object v1, v2, v6

    goto :goto_1

    :cond_3
    const-string v1, "NULL"

    goto :goto_2
.end method
