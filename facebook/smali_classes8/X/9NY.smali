.class public final LX/9NY;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "itemPrice"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "itemPrice"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:LX/2uF;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "photos"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel$StoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1476101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1476102
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel;
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v9, 0x0

    const/4 v2, 0x0

    .line 1476103
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1476104
    iget-object v1, p0, LX/9NY;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1476105
    iget-object v3, p0, LX/9NY;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1476106
    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    iget-object v6, p0, LX/9NY;->c:LX/15i;

    iget v7, p0, LX/9NY;->d:I

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const v5, -0x69b2f13c

    invoke-static {v6, v7, v5}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;

    move-result-object v5

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1476107
    iget-object v6, p0, LX/9NY;->e:LX/2uF;

    invoke-static {v6, v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v6

    .line 1476108
    iget-object v7, p0, LX/9NY;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel$StoryModel;

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1476109
    const/4 v8, 0x5

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 1476110
    invoke-virtual {v0, v9, v1}, LX/186;->b(II)V

    .line 1476111
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1476112
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1476113
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1476114
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1476115
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1476116
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1476117
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1476118
    invoke-virtual {v1, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1476119
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1476120
    new-instance v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel;

    invoke-direct {v1, v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel;-><init>(LX/15i;)V

    .line 1476121
    return-object v1

    .line 1476122
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
