.class public final LX/9UV;
.super Landroid/widget/Filter;
.source ""


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:LX/9UC;


# direct methods
.method public constructor <init>(LX/9UC;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1498033
    iput-object p1, p0, LX/9UV;->b:LX/9UC;

    iput-object p2, p0, LX/9UV;->a:Landroid/content/Context;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    .line 1498034
    return-void
.end method


# virtual methods
.method public final performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 9

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x0

    .line 1498035
    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 1498036
    sget-object v0, LX/0P0;->i:Landroid/net/Uri;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1498037
    iget-object v0, p0, LX/9UV;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, LX/9UW;->a:[Ljava/lang/String;

    const-string v3, "display_name IS NOT NULL AND LENGTH(display_name) > 0"

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 1498038
    iget-object v0, p0, LX/9UV;->b:LX/9UC;

    iget-object v0, v0, LX/9UC;->k:LX/0ad;

    sget-short v1, LX/1EB;->L:S

    invoke-interface {v0, v1, v6}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1498039
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1498040
    iget-object v0, p0, LX/9UV;->b:LX/9UC;

    iget-object v0, v0, LX/9UC;->i:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v5

    move v3, v6

    :goto_0
    if-ge v3, v5, :cond_1

    iget-object v0, p0, LX/9UV;->b:LX/9UC;

    iget-object v0, v0, LX/9UC;->i:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 1498041
    invoke-virtual {v0}, LX/8QK;->b()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/1fg;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, LX/1fg;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1498042
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1498043
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    :goto_1
    move-object v1, v0

    move-object v0, v2

    .line 1498044
    :goto_2
    new-instance v3, Landroid/widget/Filter$FilterResults;

    invoke-direct {v3}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 1498045
    if-eqz v0, :cond_4

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v2

    :goto_3
    iput v2, v3, Landroid/widget/Filter$FilterResults;->count:I

    .line 1498046
    new-instance v2, LX/9UU;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    :cond_2
    invoke-direct {v2, p0, v0, v4}, LX/9UU;-><init>(LX/9UV;Landroid/database/Cursor;LX/0Px;)V

    iput-object v2, v3, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 1498047
    return-object v3

    .line 1498048
    :cond_3
    iget-object v0, p0, LX/9UV;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, LX/0P0;->g:Landroid/net/Uri;

    sget-object v2, LX/9UW;->a:[Ljava/lang/String;

    const-string v3, "display_name IS NOT NULL AND LENGTH(display_name) > 0"

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1498049
    iget-object v1, p0, LX/9UV;->b:LX/9UC;

    iget-object v1, v1, LX/9UC;->k:LX/0ad;

    sget-short v2, LX/1EB;->L:S

    invoke-interface {v1, v2, v6}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1498050
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1498051
    iget-object v2, p0, LX/9UV;->b:LX/9UC;

    iget-object v2, v2, LX/9UC;->i:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    goto :goto_2

    :cond_4
    move v2, v6

    .line 1498052
    goto :goto_3

    :cond_5
    move-object v1, v4

    goto :goto_2

    :cond_6
    move-object v0, v4

    goto :goto_1
.end method

.method public final publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 3

    .prologue
    .line 1498053
    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, LX/9UU;

    .line 1498054
    if-eqz v0, :cond_1

    iget-object v1, v0, LX/9UU;->a:Landroid/database/Cursor;

    if-eqz v1, :cond_1

    .line 1498055
    iget-object v1, v0, LX/9UU;->b:LX/0Px;

    if-eqz v1, :cond_0

    .line 1498056
    iget-object v1, p0, LX/9UV;->b:LX/9UC;

    iget-object v2, v0, LX/9UU;->b:LX/0Px;

    iput-object v2, v1, LX/9UC;->j:LX/0Px;

    .line 1498057
    :cond_0
    iget-object v1, p0, LX/9UV;->b:LX/9UC;

    iget-object v0, v0, LX/9UU;->a:Landroid/database/Cursor;

    invoke-virtual {v1, v0}, LX/9UB;->a(Landroid/database/Cursor;)V

    .line 1498058
    :cond_1
    iget-object v0, p0, LX/9UV;->b:LX/9UC;

    const v1, 0x1bdb0e86

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1498059
    return-void
.end method
