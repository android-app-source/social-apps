.class public LX/ASV;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/ASU;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1674020
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1674021
    return-void
.end method


# virtual methods
.method public final a(LX/HqU;LX/Hrb;)LX/ASU;
    .locals 12

    .prologue
    .line 1674022
    new-instance v0, LX/ASU;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    const/16 v2, 0xfb6

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {p0}, LX/33A;->a(LX/0QB;)LX/33A;

    move-result-object v3

    check-cast v3, LX/33A;

    invoke-static {p0}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(LX/0QB;)Lcom/facebook/privacy/PrivacyOperationsClient;

    move-result-object v4

    check-cast v4, Lcom/facebook/privacy/PrivacyOperationsClient;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    const-class v6, Landroid/content/Context;

    invoke-interface {p0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-static {p0}, LX/339;->a(LX/0QB;)LX/339;

    move-result-object v7

    check-cast v7, LX/339;

    const/16 v8, 0x34c

    invoke-static {p0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v9

    check-cast v9, LX/0Uh;

    move-object v10, p1

    move-object v11, p2

    invoke-direct/range {v0 .. v11}, LX/ASU;-><init>(Landroid/content/res/Resources;LX/0Ot;LX/33A;Lcom/facebook/privacy/PrivacyOperationsClient;LX/0SG;Landroid/content/Context;LX/339;LX/0Or;LX/0Uh;LX/HqU;LX/Hrb;)V

    .line 1674023
    return-object v0
.end method
