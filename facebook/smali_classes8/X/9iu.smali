.class public final LX/9iu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8JW;


# instance fields
.field public final synthetic a:LX/9iw;


# direct methods
.method public constructor <init>(LX/9iw;)V
    .locals 0

    .prologue
    .line 1528750
    iput-object p1, p0, LX/9iu;->a:LX/9iw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Z
    .locals 11

    .prologue
    const/4 v0, 0x0

    .line 1528751
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 1528752
    iget-object v1, p0, LX/9iu;->a:LX/9iw;

    iget-object v1, v1, LX/9iw;->f:LX/74x;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/9iu;->a:LX/9iw;

    iget-object v1, v1, LX/9iw;->n:LX/75Q;

    iget-object v4, p0, LX/9iu;->a:LX/9iw;

    iget-object v4, v4, LX/9iw;->f:LX/74x;

    .line 1528753
    invoke-virtual {v1, v4}, LX/75Q;->a(LX/74x;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/photos/base/tagging/Tag;

    .line 1528754
    invoke-virtual {v5}, Lcom/facebook/photos/base/tagging/Tag;->j()Z

    move-result v7

    if-nez v7, :cond_0

    .line 1528755
    iget-wide v9, v5, Lcom/facebook/photos/base/tagging/Tag;->c:J

    move-wide v7, v9

    .line 1528756
    cmp-long v5, v7, v2

    if-nez v5, :cond_0

    .line 1528757
    const/4 v5, 0x1

    .line 1528758
    :goto_0
    move v1, v5

    .line 1528759
    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    :goto_1
    return v0

    .line 1528760
    :catch_0
    goto :goto_1

    :cond_2
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 6

    .prologue
    .line 1528761
    iget-object v0, p0, LX/9iu;->a:LX/9iw;

    iget-object v0, v0, LX/9iw;->f:LX/74x;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/9iu;->a:LX/9iw;

    iget-object v0, v0, LX/9iw;->n:LX/75Q;

    iget-object v1, p0, LX/9iu;->a:LX/9iw;

    iget-object v1, v1, LX/9iw;->f:LX/74x;

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1528762
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    move v2, v3

    :goto_0
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 1528763
    invoke-virtual {v0, v1}, LX/75Q;->a(LX/74x;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/base/tagging/Tag;

    .line 1528764
    invoke-virtual {v2}, Lcom/facebook/photos/base/tagging/Tag;->j()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 1528765
    iget-object p0, v2, Lcom/facebook/photos/base/tagging/Tag;->b:Lcom/facebook/user/model/Name;

    move-object v2, p0

    .line 1528766
    invoke-virtual {v2}, Lcom/facebook/user/model/Name;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1528767
    :goto_1
    move v0, v3

    .line 1528768
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    :cond_2
    move v2, v4

    .line 1528769
    goto :goto_0

    :cond_3
    move v3, v4

    .line 1528770
    goto :goto_1
.end method
