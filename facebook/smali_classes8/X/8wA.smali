.class public final LX/8wA;
.super Ljava/util/ArrayList;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/ArrayList",
        "<",
        "Landroid/util/Pair",
        "<TK;TV;>;>;"
    }
.end annotation


# instance fields
.field private final keyType:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TK;>;"
        }
    .end annotation
.end field

.field private final valueType:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TV;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/lang/Class;Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TK;>;",
            "Ljava/lang/Class",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1421637
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 1421638
    iput-object p1, p0, LX/8wA;->keyType:Ljava/lang/Class;

    .line 1421639
    iput-object p2, p0, LX/8wA;->valueType:Ljava/lang/Class;

    .line 1421640
    return-void
.end method

.method public static a(Ljava/lang/Class;Ljava/lang/Class;)LX/8wA;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TK;>;",
            "Ljava/lang/Class",
            "<TV;>;)",
            "LX/8wA",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1421641
    new-instance v0, LX/8wA;

    invoke-direct {v0, p0, p1}, LX/8wA;-><init>(Ljava/lang/Class;Ljava/lang/Class;)V

    return-object v0
.end method


# virtual methods
.method public final a()LX/8wG;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/8wG",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1421642
    invoke-virtual {p0}, LX/8wA;->size()I

    move-result v5

    .line 1421643
    iget-object v0, p0, LX/8wA;->keyType:Ljava/lang/Class;

    invoke-static {v0, v5}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 1421644
    iget-object v1, p0, LX/8wA;->valueType:Ljava/lang/Class;

    invoke-static {v1, v5}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/Object;

    check-cast v1, [Ljava/lang/Object;

    move v3, v4

    .line 1421645
    :goto_0
    if-ge v3, v5, :cond_0

    .line 1421646
    invoke-virtual {p0, v3}, LX/8wA;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    iget-object v2, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    aput-object v2, v0, v3

    .line 1421647
    invoke-virtual {p0, v3}, LX/8wA;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    aput-object v2, v1, v3

    .line 1421648
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 1421649
    :cond_0
    new-instance v2, LX/8wG;

    invoke-direct {v2, v0, v1}, LX/8wG;-><init>([Ljava/lang/Object;[Ljava/lang/Object;)V

    return-object v2
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)V"
        }
    .end annotation

    .prologue
    .line 1421650
    invoke-static {p1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/8wA;->add(Ljava/lang/Object;)Z

    .line 1421651
    return-void
.end method
