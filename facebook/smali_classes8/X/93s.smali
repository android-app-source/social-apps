.class public abstract LX/93s;
.super LX/93Q;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0it;",
        ":",
        "LX/0iq;",
        "DerivedData::",
        "LX/5Qu;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;>",
        "LX/93Q;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/privacy/PrivacyOperationsClient;

.field public final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/93w;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2c9;",
            ">;"
        }
    .end annotation
.end field

.field public d:Z

.field public e:I

.field private f:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

.field private g:Z

.field public h:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/93q;LX/93w;Lcom/facebook/graphql/model/GraphQLPrivacyOption;LX/0il;LX/03V;LX/1Ck;Lcom/facebook/privacy/PrivacyOperationsClient;LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/93q;",
            "LX/93w;",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;",
            "TServices;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/privacy/PrivacyOperationsClient;",
            "LX/0Ot",
            "<",
            "LX/2c9;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1434283
    invoke-direct {p0, p1, p5, p6}, LX/93Q;-><init>(LX/93q;LX/03V;LX/1Ck;)V

    .line 1434284
    iput-boolean v0, p0, LX/93s;->d:Z

    .line 1434285
    iput v0, p0, LX/93s;->e:I

    .line 1434286
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/93s;->g:Z

    .line 1434287
    iput-object p7, p0, LX/93s;->a:Lcom/facebook/privacy/PrivacyOperationsClient;

    .line 1434288
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/93s;->b:Ljava/lang/ref/WeakReference;

    .line 1434289
    iput-object p8, p0, LX/93s;->c:LX/0Ot;

    .line 1434290
    iput-object p3, p0, LX/93s;->f:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1434291
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/93s;->h:Ljava/lang/ref/WeakReference;

    .line 1434292
    return-void
.end method

.method private a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;Z)V
    .locals 4

    .prologue
    .line 1434296
    iget-object v0, p0, LX/93Q;->c:LX/1Ck;

    move-object v0, v0

    .line 1434297
    const-string v1, "fetch_privacy_options"

    iget-object v2, p0, LX/93s;->a:Lcom/facebook/privacy/PrivacyOperationsClient;

    sget-object v3, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    invoke-virtual {v2, v3}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(LX/0rS;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, LX/93u;

    invoke-direct {v3, p0, p1, p2}, LX/93u;-><init>(LX/93s;Lcom/facebook/composer/privacy/model/ComposerPrivacyData;Z)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1434298
    return-void
.end method

.method public static b(LX/93s;Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V
    .locals 4

    .prologue
    .line 1434293
    iget-object v0, p0, LX/93Q;->c:LX/1Ck;

    move-object v0, v0

    .line 1434294
    const-string v1, "prefetch_privacy_options"

    iget-object v2, p0, LX/93s;->a:Lcom/facebook/privacy/PrivacyOperationsClient;

    sget-object v3, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    invoke-virtual {v2, v3}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(LX/0rS;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, LX/93v;

    invoke-direct {v3, p0, p1}, LX/93v;-><init>(LX/93s;Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1434295
    return-void
.end method

.method public static b(LX/93s;Lcom/facebook/composer/privacy/model/ComposerPrivacyData;Lcom/facebook/privacy/model/PrivacyOptionsResult;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1434264
    invoke-virtual {p0, p1, p2}, LX/93s;->a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;Lcom/facebook/privacy/model/PrivacyOptionsResult;)Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v0

    .line 1434265
    iget-object v1, p0, LX/93s;->f:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-eqz v1, :cond_0

    .line 1434266
    iget-object v1, p0, LX/93s;->f:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v0, v1}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(Lcom/facebook/privacy/model/SelectablePrivacyData;Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v0

    .line 1434267
    :cond_0
    new-instance v1, LX/7lP;

    invoke-direct {v1, p1}, LX/7lP;-><init>(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    .line 1434268
    iput-boolean v2, v1, LX/7lP;->b:Z

    .line 1434269
    move-object v1, v1

    .line 1434270
    iput-boolean v2, v1, LX/7lP;->c:Z

    .line 1434271
    move-object v1, v1

    .line 1434272
    invoke-virtual {v1, v0}, LX/7lP;->a(Lcom/facebook/privacy/model/SelectablePrivacyData;)LX/7lP;

    move-result-object v0

    invoke-virtual {v0}, LX/7lP;->a()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    .line 1434273
    invoke-virtual {p0, v0}, LX/93Q;->a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    .line 1434274
    return-void
.end method

.method private static g()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1434275
    new-instance v0, LX/7lP;

    invoke-direct {v0}, LX/7lP;-><init>()V

    .line 1434276
    iput-boolean v1, v0, LX/7lP;->a:Z

    .line 1434277
    move-object v0, v0

    .line 1434278
    iput-boolean v1, v0, LX/7lP;->b:Z

    .line 1434279
    move-object v0, v0

    .line 1434280
    iput-boolean v1, v0, LX/7lP;->c:Z

    .line 1434281
    move-object v0, v0

    .line 1434282
    new-instance v1, LX/8QV;

    invoke-direct {v1}, LX/8QV;-><init>()V

    invoke-virtual {v1}, LX/8QV;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/7lP;->a(Lcom/facebook/privacy/model/SelectablePrivacyData;)LX/7lP;

    move-result-object v0

    invoke-virtual {v0}, LX/7lP;->a()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public abstract a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;Lcom/facebook/privacy/model/PrivacyOptionsResult;)Lcom/facebook/privacy/model/SelectablePrivacyData;
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 1434257
    invoke-super {p0}, LX/93Q;->a()V

    .line 1434258
    invoke-static {}, LX/93s;->g()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v1

    .line 1434259
    iget-boolean v0, p0, LX/93s;->d:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/93s;->g:Z

    if-eqz v0, :cond_1

    .line 1434260
    :cond_0
    invoke-virtual {p0, v1}, LX/93Q;->a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    .line 1434261
    :cond_1
    iget-boolean v0, p0, LX/93s;->d:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v1, v0}, LX/93s;->a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;Z)V

    .line 1434262
    return-void

    .line 1434263
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)V
    .locals 1

    .prologue
    .line 1434253
    iget-object v0, p0, LX/93s;->f:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/93s;->g:Z

    .line 1434254
    iput-object p1, p0, LX/93s;->f:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1434255
    return-void

    .line 1434256
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1434252
    iget-object v0, p0, LX/93s;->f:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-nez v0, :cond_0

    const-string v0, "selectable"

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "selectable:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/93s;->f:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 1434250
    invoke-static {}, LX/93s;->g()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, LX/93s;->a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;Z)V

    .line 1434251
    return-void
.end method
