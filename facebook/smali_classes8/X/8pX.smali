.class public final LX/8pX;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/api/graphql/commenttranslation/FetchCommentTranslationGraphQLModels$TranslatedCommentBodyModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/3Wt;

.field public final synthetic b:Ljava/util/HashMap;

.field public final synthetic c:LX/8pY;


# direct methods
.method public constructor <init>(LX/8pY;LX/3Wt;Ljava/util/HashMap;)V
    .locals 0

    .prologue
    .line 1405985
    iput-object p1, p0, LX/8pX;->c:LX/8pY;

    iput-object p2, p0, LX/8pX;->a:LX/3Wt;

    iput-object p3, p0, LX/8pX;->b:Ljava/util/HashMap;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1405969
    iget-object v0, p0, LX/8pX;->c:LX/8pY;

    iget-object v1, p0, LX/8pX;->a:LX/3Wt;

    invoke-static {v0, p1, v1}, LX/8pY;->a$redex0(LX/8pY;Ljava/lang/Throwable;LX/3Wt;)V

    .line 1405970
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1405971
    check-cast p1, Ljava/util/List;

    .line 1405972
    invoke-static {p1}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1405973
    iget-object v0, p0, LX/8pX;->c:LX/8pY;

    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Null comment translation"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/8pX;->a:LX/3Wt;

    invoke-static {v0, v1, v2}, LX/8pY;->a$redex0(LX/8pY;Ljava/lang/Throwable;LX/3Wt;)V

    .line 1405974
    :goto_0
    return-void

    .line 1405975
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/commenttranslation/FetchCommentTranslationGraphQLModels$TranslatedCommentBodyModel;

    .line 1405976
    invoke-virtual {v0}, Lcom/facebook/api/graphql/commenttranslation/FetchCommentTranslationGraphQLModels$TranslatedCommentBodyModel;->j()Ljava/lang/String;

    move-result-object v2

    .line 1405977
    invoke-virtual {v0}, Lcom/facebook/api/graphql/commenttranslation/FetchCommentTranslationGraphQLModels$TranslatedCommentBodyModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    .line 1405978
    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    .line 1405979
    iget-object v3, p0, LX/8pX;->b:Ljava/util/HashMap;

    invoke-virtual {v3, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1405980
    iget-object v3, p0, LX/8pX;->c:LX/8pY;

    iget-object v3, v3, LX/8pY;->b:LX/8pW;

    .line 1405981
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 1405982
    :goto_2
    goto :goto_1

    .line 1405983
    :cond_2
    iget-object v0, p0, LX/8pX;->a:LX/3Wt;

    iget-object v1, p0, LX/8pX;->b:Ljava/util/HashMap;

    invoke-interface {v0, v1}, LX/3Wt;->a(Ljava/util/Map;)V

    goto :goto_0

    .line 1405984
    :cond_3
    iget-object p1, v3, LX/8pW;->b:LX/0aq;

    invoke-virtual {p1, v2, v0}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2
.end method
