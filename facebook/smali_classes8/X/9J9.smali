.class public LX/9J9;
.super Landroid/database/MatrixCursor;
.source ""

# interfaces
.implements LX/2nf;


# instance fields
.field public final a:I

.field public final b:I

.field public final c:LX/2nf;


# direct methods
.method public constructor <init>(LX/2nf;II)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 1464406
    new-array v1, v3, [Ljava/lang/String;

    const-string v2, "_id"

    aput-object v2, v1, v0

    invoke-direct {p0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 1464407
    iput p2, p0, LX/9J9;->a:I

    .line 1464408
    iput p3, p0, LX/9J9;->b:I

    .line 1464409
    iput-object p1, p0, LX/9J9;->c:LX/2nf;

    .line 1464410
    iget-object v1, p0, LX/9J9;->c:LX/2nf;

    invoke-interface {v1}, LX/2nf;->getCount()I

    move-result v1

    .line 1464411
    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x1

    .line 1464412
    iget p1, p0, LX/9J9;->b:I

    if-ne p1, v2, :cond_2

    .line 1464413
    :cond_0
    :goto_0
    move v1, v1

    .line 1464414
    add-int/lit8 v1, v1, 0x1

    .line 1464415
    new-array v2, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v3, v2, v0

    .line 1464416
    :goto_1
    if-ge v0, v1, :cond_1

    .line 1464417
    invoke-super {p0, v2}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 1464418
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1464419
    :cond_1
    return-void

    .line 1464420
    :cond_2
    iget p1, p0, LX/9J9;->a:I

    sub-int p1, v1, p1

    .line 1464421
    if-lez p1, :cond_0

    .line 1464422
    add-int/lit8 p2, p1, 0x1

    iget p3, p0, LX/9J9;->b:I

    rem-int/2addr p2, p3

    .line 1464423
    add-int/lit8 p1, p1, 0x1

    iget p3, p0, LX/9J9;->b:I

    div-int/2addr p1, p3

    .line 1464424
    iget p3, p0, LX/9J9;->a:I

    add-int/2addr p1, p3

    if-nez p2, :cond_3

    const/4 v2, 0x0

    :cond_3
    add-int/2addr v2, p1

    add-int/lit8 v1, v2, -0x1

    goto :goto_0
.end method

.method public static f(LX/9J9;)I
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1464400
    invoke-virtual {p0}, LX/9J9;->getPosition()I

    move-result v0

    .line 1464401
    iget v1, p0, LX/9J9;->b:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 1464402
    :cond_0
    :goto_0
    return v0

    .line 1464403
    :cond_1
    iget v1, p0, LX/9J9;->a:I

    if-lt v0, v1, :cond_0

    .line 1464404
    iget v1, p0, LX/9J9;->a:I

    sub-int/2addr v0, v1

    .line 1464405
    iget v1, p0, LX/9J9;->a:I

    iget v2, p0, LX/9J9;->b:I

    mul-int/2addr v0, v2

    add-int/2addr v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 1464387
    iget-object v0, p0, LX/9J9;->c:LX/2nf;

    invoke-static {p0}, LX/9J9;->f(LX/9J9;)I

    move-result v1

    invoke-interface {v0, v1}, LX/2nf;->moveToPosition(I)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1464388
    iget-object v0, p0, LX/9J9;->c:LX/2nf;

    invoke-interface {v0}, LX/2nf;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(LX/2kb;)V
    .locals 1

    .prologue
    .line 1464398
    iget-object v0, p0, LX/9J9;->c:LX/2nf;

    invoke-interface {v0, p1}, LX/2nf;->a(LX/2kb;)V

    .line 1464399
    return-void
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 1464396
    iget-object v0, p0, LX/9J9;->c:LX/2nf;

    invoke-static {p0}, LX/9J9;->f(LX/9J9;)I

    move-result v1

    invoke-interface {v0, v1}, LX/2nf;->moveToPosition(I)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1464397
    iget-object v0, p0, LX/9J9;->c:LX/2nf;

    invoke-interface {v0}, LX/2nf;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public final c()Lcom/facebook/flatbuffers/Flattenable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">()TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1464394
    iget-object v0, p0, LX/9J9;->c:LX/2nf;

    invoke-static {p0}, LX/9J9;->f(LX/9J9;)I

    move-result v1

    invoke-interface {v0, v1}, LX/2nf;->moveToPosition(I)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1464395
    iget-object v0, p0, LX/9J9;->c:LX/2nf;

    invoke-interface {v0}, LX/2nf;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    return-object v0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 1464391
    iget-object v0, p0, LX/9J9;->c:LX/2nf;

    invoke-interface {v0}, LX/2nf;->close()V

    .line 1464392
    invoke-super {p0}, Landroid/database/MatrixCursor;->close()V

    .line 1464393
    return-void
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 1464389
    iget-object v0, p0, LX/9J9;->c:LX/2nf;

    invoke-static {p0}, LX/9J9;->f(LX/9J9;)I

    move-result v1

    invoke-interface {v0, v1}, LX/2nf;->moveToPosition(I)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1464390
    iget-object v0, p0, LX/9J9;->c:LX/2nf;

    invoke-interface {v0}, LX/2nf;->d()I

    move-result v0

    return v0
.end method
