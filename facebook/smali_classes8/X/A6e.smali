.class public LX/A6e;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/A6N;

.field public b:Z

.field private c:Landroid/content/Context;

.field public d:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Z

.field public final f:I

.field public final g:LX/0TD;

.field public h:I

.field public i:LX/A6r;

.field public final j:LX/A6X;

.field public final k:LX/A77;

.field public l:LX/A6z;

.field public m:LX/A79;

.field public n:LX/A73;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;Landroid/content/Context;LX/A6X;LX/A77;LX/A79;LX/A6t;LX/0Ot;LX/0Ot;LX/A73;LX/A6N;Ljava/lang/Boolean;Ljava/lang/Integer;)V
    .locals 2
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .param p10    # LX/A6N;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p11    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p12    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "Landroid/content/Context;",
            "LX/A6X;",
            "LX/A77;",
            "LX/A79;",
            "LX/A6t;",
            "LX/0Ot",
            "<",
            "LX/A6y;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/A6z;",
            ">;",
            "LX/A73;",
            "LX/A6N;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1624577
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1624578
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/A6e;->b:Z

    .line 1624579
    iput-object p10, p0, LX/A6e;->a:LX/A6N;

    .line 1624580
    invoke-virtual {p11}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/A6e;->e:Z

    .line 1624581
    invoke-virtual {p12}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/A6e;->f:I

    .line 1624582
    iput-object p2, p0, LX/A6e;->c:Landroid/content/Context;

    .line 1624583
    invoke-static {p1}, LX/0TA;->a(Ljava/util/concurrent/ExecutorService;)LX/0TD;

    move-result-object v0

    iput-object v0, p0, LX/A6e;->g:LX/0TD;

    .line 1624584
    iput-object p3, p0, LX/A6e;->j:LX/A6X;

    .line 1624585
    iput-object p4, p0, LX/A6e;->k:LX/A77;

    .line 1624586
    iput-object p5, p0, LX/A6e;->m:LX/A79;

    .line 1624587
    iput-object p9, p0, LX/A6e;->n:LX/A73;

    .line 1624588
    iget-object v0, p0, LX/A6e;->a:LX/A6N;

    sget-object v1, LX/A6N;->UNIGRAM:LX/A6N;

    if-ne v0, v1, :cond_1

    .line 1624589
    invoke-interface {p8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/A6z;

    iput-object v0, p0, LX/A6e;->l:LX/A6z;

    .line 1624590
    invoke-interface {p7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/A6r;

    iput-object v0, p0, LX/A6e;->i:LX/A6r;

    .line 1624591
    :cond_0
    :goto_0
    return-void

    .line 1624592
    :cond_1
    iget-object v0, p0, LX/A6e;->a:LX/A6N;

    sget-object v1, LX/A6N;->BIGRAM:LX/A6N;

    if-ne v0, v1, :cond_0

    .line 1624593
    invoke-virtual {p12}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1624594
    new-instance p1, LX/A6s;

    invoke-static {p6}, LX/A78;->b(LX/0QB;)LX/A78;

    move-result-object v1

    check-cast v1, LX/A78;

    invoke-direct {p1, v1, v0}, LX/A6s;-><init>(LX/A78;I)V

    .line 1624595
    move-object v0, p1

    .line 1624596
    iput-object v0, p0, LX/A6e;->i:LX/A6r;

    .line 1624597
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/A6e;->b:Z

    goto :goto_0
.end method

.method public static a(LX/A6e;ILjava/lang/String;Ljava/lang/String;I)V
    .locals 7

    .prologue
    .line 1624598
    iget-object v6, p0, LX/A6e;->g:LX/0TD;

    new-instance v0, LX/A6a;

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, LX/A6a;-><init>(LX/A6e;ILjava/lang/String;Ljava/lang/String;I)V

    invoke-interface {v6, v0}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1624599
    new-instance v1, LX/A6b;

    invoke-direct {v1, p0, p2, p3}, LX/A6b;-><init>(LX/A6e;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, LX/A6e;->g:LX/0TD;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1624600
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1624601
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1624602
    iget-boolean v1, p0, LX/A6e;->b:Z

    if-eqz v1, :cond_0

    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1624603
    :cond_0
    :goto_0
    return-object v0

    .line 1624604
    :cond_1
    const/16 v1, 0x14

    .line 1624605
    new-instance v2, Ljava/util/LinkedHashSet;

    invoke-direct {v2}, Ljava/util/LinkedHashSet;-><init>()V

    .line 1624606
    iget-boolean v3, p0, LX/A6e;->e:Z

    if-eqz v3, :cond_2

    .line 1624607
    iget-object v3, p0, LX/A6e;->n:LX/A73;

    .line 1624608
    iget-object v4, v3, LX/A73;->b:LX/A72;

    sget-wide v6, LX/A73;->a:D

    .line 1624609
    const/4 v5, 0x0

    invoke-static {v4, p1, v6, v7, v5}, LX/A72;->a(LX/A72;Ljava/lang/String;DI)Ljava/util/List;

    move-result-object v5

    move-object v4, v5

    .line 1624610
    move-object v3, v4

    .line 1624611
    if-eqz v3, :cond_2

    .line 1624612
    invoke-virtual {v2, v3}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    .line 1624613
    :cond_2
    iget-object v3, p0, LX/A6e;->i:LX/A6r;

    invoke-interface {v3, p1, v1}, LX/A6r;->a(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v3

    .line 1624614
    if-eqz v0, :cond_3

    .line 1624615
    invoke-virtual {v2, v3}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    .line 1624616
    :cond_3
    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1624617
    goto :goto_0
.end method
