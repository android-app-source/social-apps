.class public final LX/963;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1L9;

.field public final synthetic b:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Lcom/facebook/graphql/model/GraphQLPage;

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:LX/967;


# direct methods
.method public constructor <init>(LX/967;LX/1L9;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLPage;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1438119
    iput-object p1, p0, LX/963;->f:LX/967;

    iput-object p2, p0, LX/963;->a:LX/1L9;

    iput-object p3, p0, LX/963;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p4, p0, LX/963;->c:Ljava/lang/String;

    iput-object p5, p0, LX/963;->d:Lcom/facebook/graphql/model/GraphQLPage;

    iput-object p6, p0, LX/963;->e:Ljava/lang/String;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1438120
    iget-object v0, p0, LX/963;->a:LX/1L9;

    if-eqz v0, :cond_0

    .line 1438121
    iget-object v0, p0, LX/963;->a:LX/1L9;

    iget-object v1, p0, LX/963;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1438122
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v2

    .line 1438123
    invoke-static {p1}, Lcom/facebook/fbservice/service/ServiceException;->forException(Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/ServiceException;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/1L9;->a(Ljava/lang/Object;Lcom/facebook/fbservice/service/ServiceException;)V

    .line 1438124
    :cond_0
    iget-object v0, p0, LX/963;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1438125
    iget-object v0, p0, LX/963;->f:LX/967;

    iget-object v0, v0, LX/967;->d:LX/0Zb;

    .line 1438126
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, LX/963;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_fail"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/963;->d:Lcom/facebook/graphql/model/GraphQLPage;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/963;->d:Lcom/facebook/graphql/model/GraphQLPage;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPage;->w()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/963;->e:Ljava/lang/String;

    invoke-static {v1, v2, v3, v4}, LX/17V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1438127
    :cond_1
    return-void
.end method

.method public final bridge synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1438128
    return-void
.end method
