.class public LX/92X;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/92Z;

.field public final b:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "LX/92W;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/92c;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field public f:I

.field public g:Landroid/location/Location;


# direct methods
.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;LX/92Z;LX/1Ck;LX/92c;)V
    .locals 1
    .param p1    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1432343
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1432344
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/92X;->f:I

    .line 1432345
    iput-object p2, p0, LX/92X;->d:Ljava/lang/String;

    .line 1432346
    iput-object p4, p0, LX/92X;->a:LX/92Z;

    .line 1432347
    iput-object p5, p0, LX/92X;->b:LX/1Ck;

    .line 1432348
    iput-object p6, p0, LX/92X;->c:LX/92c;

    .line 1432349
    iput-object p3, p0, LX/92X;->e:Ljava/lang/String;

    .line 1432350
    return-void
.end method

.method public static b(LX/92X;LX/5LG;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5LG;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 1432351
    iget v3, p0, LX/92X;->f:I

    iget-object v4, p0, LX/92X;->d:Ljava/lang/String;

    iget-object v6, p0, LX/92X;->e:Ljava/lang/String;

    iget-object v8, p0, LX/92X;->g:Landroid/location/Location;

    move-object v0, p1

    move-object v1, p2

    move-object v2, p4

    move-object v5, p3

    invoke-static/range {v0 .. v8}, LX/92I;->a(LX/5LG;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLandroid/location/Location;)LX/92H;

    move-result-object v1

    .line 1432352
    iget-object v0, p0, LX/92X;->c:LX/92c;

    .line 1432353
    iget-object v2, v0, LX/92c;->b:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->a()V

    .line 1432354
    iget-object v2, v0, LX/92c;->c:Ljava/util/List;

    new-instance v3, LX/92b;

    invoke-direct {v3, v0}, LX/92b;-><init>(LX/92c;)V

    invoke-static {v2, v3}, LX/0Ph;->a(Ljava/lang/Iterable;LX/0Rl;)Z

    .line 1432355
    iget-object v2, v0, LX/92c;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3rL;

    .line 1432356
    iget-object v3, v2, LX/3rL;->a:Ljava/lang/Object;

    check-cast v3, LX/92H;

    .line 1432357
    iget-object v5, v3, LX/92H;->c:Ljava/lang/String;

    iget-object v0, v1, LX/92H;->c:Ljava/lang/String;

    invoke-static {v5, v0}, LX/0YN;->c(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    iget-object v5, v3, LX/92H;->b:Ljava/lang/String;

    iget-object v0, v1, LX/92H;->b:Ljava/lang/String;

    invoke-static {v5, v0}, LX/0YN;->c(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    iget-object v5, v3, LX/92H;->a:Ljava/lang/String;

    iget-object v0, v1, LX/92H;->a:Ljava/lang/String;

    invoke-static {v5, v0}, LX/0YN;->c(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    iget-object v5, v3, LX/92H;->i:Ljava/lang/String;

    iget-object v0, v1, LX/92H;->i:Ljava/lang/String;

    if-ne v5, v0, :cond_4

    iget-boolean v5, v3, LX/92H;->e:Z

    iget-boolean v0, v1, LX/92H;->e:Z

    if-ne v5, v0, :cond_4

    iget-boolean v5, v3, LX/92H;->h:Z

    iget-boolean v0, v1, LX/92H;->h:Z

    if-ne v5, v0, :cond_4

    iget v5, v3, LX/92H;->d:I

    iget v0, v1, LX/92H;->d:I

    if-ne v5, v0, :cond_4

    iget-object v5, v3, LX/92H;->j:Ljava/lang/String;

    iget-object v0, v1, LX/92H;->j:Ljava/lang/String;

    invoke-static {v5, v0}, LX/0YN;->c(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    const/4 v5, 0x1

    :goto_0
    move v3, v5

    .line 1432358
    if-eqz v3, :cond_0

    .line 1432359
    iget-object v2, v2, LX/3rL;->b:Ljava/lang/Object;

    check-cast v2, Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1432360
    :goto_1
    move-object v0, v2

    .line 1432361
    if-eqz v0, :cond_1

    const/4 v7, 0x1

    .line 1432362
    :cond_1
    if-nez v0, :cond_2

    .line 1432363
    iget-object v0, p0, LX/92X;->a:LX/92Z;

    invoke-virtual {v0, v1}, LX/92Z;->a(LX/92H;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1432364
    :cond_2
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, LX/92V;

    invoke-direct {v2, p0, v7, v1}, LX/92V;-><init>(LX/92X;ZLX/92H;)V

    invoke-static {v0, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    :cond_4
    const/4 v5, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/5LG;Ljava/lang/String;Ljava/lang/String;LX/0Vd;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5LG;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Vd",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1432365
    iget-object v0, p0, LX/92X;->b:LX/1Ck;

    sget-object v1, LX/92W;->SEARCH:LX/92W;

    const/4 v2, 0x0

    invoke-static {p0, p1, p2, p3, v2}, LX/92X;->b(LX/92X;LX/5LG;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1432366
    return-void
.end method

.method public final a(LX/5LG;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Vd;)Z
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5LG;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Vd",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;",
            ">;>;)Z"
        }
    .end annotation

    .prologue
    .line 1432367
    iget-object v0, p0, LX/92X;->b:LX/1Ck;

    sget-object v1, LX/92W;->SEARCH:LX/92W;

    .line 1432368
    new-instance v3, LX/92U;

    move-object v4, p0

    move-object v5, p1

    move-object v6, p2

    move-object v7, p3

    move-object v8, p4

    invoke-direct/range {v3 .. v8}, LX/92U;-><init>(LX/92X;LX/5LG;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v3

    .line 1432369
    invoke-virtual {v0, v1, v2, p5}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    move-result v0

    return v0
.end method
