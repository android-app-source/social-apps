.class public final LX/9b7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Landroid/app/Activity;

.field public final synthetic b:Z

.field public final synthetic c:LX/9b8;


# direct methods
.method public constructor <init>(LX/9b8;Landroid/app/Activity;Z)V
    .locals 0

    .prologue
    .line 1514302
    iput-object p1, p0, LX/9b7;->c:LX/9b8;

    iput-object p2, p0, LX/9b7;->a:Landroid/app/Activity;

    iput-boolean p3, p0, LX/9b7;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 1514303
    iget-object v0, p0, LX/9b7;->c:LX/9b8;

    new-instance v1, LX/4BY;

    iget-object v2, p0, LX/9b7;->c:LX/9b8;

    invoke-virtual {v2}, LX/9b8;->b()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/4BY;-><init>(Landroid/content/Context;)V

    iput-object v1, v0, LX/9b8;->d:LX/4BY;

    .line 1514304
    iget-object v0, p0, LX/9b7;->c:LX/9b8;

    iget-object v0, v0, LX/9b8;->d:LX/4BY;

    .line 1514305
    iput v3, v0, LX/4BY;->d:I

    .line 1514306
    iget-object v0, p0, LX/9b7;->c:LX/9b8;

    iget-object v0, v0, LX/9b8;->d:LX/4BY;

    iget-object v1, p0, LX/9b7;->c:LX/9b8;

    invoke-virtual {v1}, LX/9b8;->b()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0811fb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2EJ;->a(Ljava/lang/CharSequence;)V

    .line 1514307
    iget-object v0, p0, LX/9b7;->c:LX/9b8;

    iget-object v0, v0, LX/9b8;->d:LX/4BY;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/4BY;->a(Z)V

    .line 1514308
    iget-object v0, p0, LX/9b7;->c:LX/9b8;

    iget-object v0, v0, LX/9b8;->d:LX/4BY;

    invoke-virtual {v0, v3}, LX/4BY;->setCancelable(Z)V

    .line 1514309
    iget-object v0, p0, LX/9b7;->c:LX/9b8;

    iget-object v0, v0, LX/9b8;->d:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->show()V

    .line 1514310
    new-instance v0, LX/9b6;

    invoke-direct {v0, p0}, LX/9b6;-><init>(LX/9b7;)V

    .line 1514311
    iget-object v1, p0, LX/9b7;->c:LX/9b8;

    iget-object v1, v1, LX/9b8;->g:LX/1Ck;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "tasks-deletePhotoAlbum:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/9b7;->c:LX/9b8;

    iget-object v3, v3, LX/9b8;->b:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/9b7;->c:LX/9b8;

    iget-object v3, v3, LX/9b8;->f:LX/9fy;

    iget-object v4, p0, LX/9b7;->c:LX/9b8;

    iget-object v4, v4, LX/9b8;->b:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v4

    .line 1514312
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 1514313
    new-instance p0, Lcom/facebook/photos/data/method/DeletePhotoAlbumParams;

    invoke-direct {p0, v4}, Lcom/facebook/photos/data/method/DeletePhotoAlbumParams;-><init>(Ljava/lang/String;)V

    .line 1514314
    const-string p1, "deletePhotoAlbumParams"

    invoke-virtual {v5, p1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1514315
    iget-object p0, v3, LX/9fy;->a:LX/0aG;

    const-string p1, "delete_photo_album"

    const p2, -0x28b68647

    invoke-static {p0, p1, v5, p2}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v5

    invoke-interface {v5}, LX/1MF;->start()LX/1ML;

    move-result-object v5

    move-object v3, v5

    .line 1514316
    invoke-virtual {v1, v2, v3, v0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1514317
    return-void
.end method
