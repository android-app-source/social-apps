.class public abstract LX/93Q;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/93q;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/03V;

.field public final c:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/93q;LX/03V;LX/1Ck;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/93q;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1433765
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1433766
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/93Q;->a:Ljava/lang/ref/WeakReference;

    .line 1433767
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    iput-object v0, p0, LX/93Q;->b:LX/03V;

    .line 1433768
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    iput-object v0, p0, LX/93Q;->c:LX/1Ck;

    .line 1433769
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 1433770
    return-void
.end method

.method public final a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V
    .locals 3

    .prologue
    .line 1433771
    iget-object v0, p0, LX/93Q;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/93q;

    .line 1433772
    if-nez v0, :cond_0

    .line 1433773
    iget-object v0, p0, LX/93Q;->b:LX/03V;

    const-string v1, "privacy_updated_handler collected"

    const-string v2, "The privacyUpdatedHandler is null"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1433774
    :goto_0
    return-void

    .line 1433775
    :cond_0
    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, LX/93q;->a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;Z)V

    goto :goto_0
.end method

.method public abstract b()Ljava/lang/String;
.end method

.method public e()V
    .locals 1

    .prologue
    .line 1433776
    iget-object v0, p0, LX/93Q;->c:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 1433777
    return-void
.end method
