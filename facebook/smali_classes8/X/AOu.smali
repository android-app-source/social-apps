.class public abstract LX/AOu;
.super LX/0gG;
.source ""


# instance fields
.field private final a:LX/0Sh;

.field private b:I

.field private c:I

.field public d:Ljava/lang/Object;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Sh;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1669712
    invoke-direct {p0}, LX/0gG;-><init>()V

    .line 1669713
    iput v0, p0, LX/AOu;->b:I

    .line 1669714
    iput v0, p0, LX/AOu;->c:I

    .line 1669715
    iput-object p1, p0, LX/AOu;->a:LX/0Sh;

    .line 1669716
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1669732
    iget-object v0, p0, LX/AOu;->a:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1669733
    iget v0, p0, LX/AOu;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/AOu;->b:I

    .line 1669734
    invoke-virtual {p0, p1, p2}, LX/AOu;->b(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    .line 1669735
    iget v1, p0, LX/AOu;->b:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, LX/AOu;->b:I

    .line 1669736
    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1669727
    iget-object v0, p0, LX/AOu;->a:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1669728
    iget v0, p0, LX/AOu;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/AOu;->c:I

    .line 1669729
    invoke-virtual {p0, p1, p2, p3}, LX/AOu;->c(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 1669730
    iget v0, p0, LX/AOu;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/AOu;->c:I

    .line 1669731
    return-void
.end method

.method public abstract b(Landroid/view/ViewGroup;I)Ljava/lang/Object;
.end method

.method public final b(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 1669724
    invoke-super {p0, p1, p2, p3}, LX/0gG;->b(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 1669725
    iput-object p3, p0, LX/AOu;->d:Ljava/lang/Object;

    .line 1669726
    return-void
.end method

.method public abstract c(Landroid/view/ViewGroup;ILjava/lang/Object;)V
.end method

.method public final kV_()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1669717
    iget-object v0, p0, LX/AOu;->a:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1669718
    iget v0, p0, LX/AOu;->b:I

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Changing data set while instantiating item!"

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1669719
    iget v0, p0, LX/AOu;->c:I

    if-nez v0, :cond_1

    :goto_1
    const-string v0, "Changing data set while destroying item!"

    invoke-static {v1, v0}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1669720
    invoke-super {p0}, LX/0gG;->kV_()V

    .line 1669721
    return-void

    :cond_0
    move v0, v2

    .line 1669722
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1669723
    goto :goto_1
.end method
