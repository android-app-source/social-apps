.class public LX/A71;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/transliteration/api/DownloadModelParams;",
        "Lcom/facebook/transliteration/api/TransliterationModelResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1625373
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1625374
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 1625344
    check-cast p1, Lcom/facebook/transliteration/api/DownloadModelParams;

    .line 1625345
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1625346
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "algorithm"

    .line 1625347
    iget v3, p1, Lcom/facebook/transliteration/api/DownloadModelParams;->a:I

    move v3, v3

    .line 1625348
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1625349
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "language"

    .line 1625350
    iget v3, p1, Lcom/facebook/transliteration/api/DownloadModelParams;->b:I

    move v3, v3

    .line 1625351
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1625352
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "version"

    .line 1625353
    iget v3, p1, Lcom/facebook/transliteration/api/DownloadModelParams;->d:I

    move v3, v3

    .line 1625354
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1625355
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "dictionary"

    .line 1625356
    iget v3, p1, Lcom/facebook/transliteration/api/DownloadModelParams;->c:I

    move v3, v3

    .line 1625357
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1625358
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "TransliterationGetModel"

    .line 1625359
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1625360
    move-object v1, v1

    .line 1625361
    const-string v2, "GET"

    .line 1625362
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1625363
    move-object v1, v1

    .line 1625364
    const-string v2, "transliteration_model"

    .line 1625365
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1625366
    move-object v1, v1

    .line 1625367
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1625368
    move-object v0, v1

    .line 1625369
    sget-object v1, LX/14S;->JSONPARSER:LX/14S;

    .line 1625370
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1625371
    move-object v0, v0

    .line 1625372
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1625337
    const/4 v1, 0x0

    .line 1625338
    iget v0, p2, LX/1pN;->b:I

    move v0, v0

    .line 1625339
    const/16 v2, 0xc8

    if-eq v0, v2, :cond_0

    move-object v0, v1

    .line 1625340
    :goto_0
    return-object v0

    .line 1625341
    :cond_0
    invoke-virtual {p2}, LX/1pN;->e()LX/15w;

    move-result-object v0

    .line 1625342
    :try_start_0
    const-class v2, Lcom/facebook/transliteration/api/TransliterationModelResponse;

    invoke-virtual {v0, v2}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/transliteration/api/TransliterationModelResponse;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1625343
    :catch_0
    move-object v0, v1

    goto :goto_0
.end method
