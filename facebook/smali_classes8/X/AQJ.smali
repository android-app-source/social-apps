.class public final LX/AQJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;)V
    .locals 0

    .prologue
    .line 1671214
    iput-object p1, p0, LX/AQJ;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x4688ebe3

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1671215
    iget-object v0, p0, LX/AQJ;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1671216
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 1671217
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventIconsFieldsModel;

    .line 1671218
    iget-object v3, p0, LX/AQJ;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;

    invoke-static {v3}, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;->k(Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;)Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->l()LX/7l3;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventIconsFieldsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v4

    .line 1671219
    iput-object v4, v3, LX/7l3;->c:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1671220
    move-object v3, v3

    .line 1671221
    invoke-virtual {v0}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventIconsFieldsModel;->k()Ljava/lang/String;

    move-result-object v0

    .line 1671222
    iput-object v0, v3, LX/7l3;->d:Ljava/lang/String;

    .line 1671223
    move-object v0, v3

    .line 1671224
    iget-object v3, p0, LX/AQJ;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;

    iget-object v3, v3, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;->e:Lcom/facebook/composer/lifeevent/view/ComposerLifeEventView;

    invoke-virtual {v3}, Lcom/facebook/fbui/widget/contentview/ContentView;->getTitleText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1671225
    iput-object v3, v0, LX/7l3;->a:Ljava/lang/String;

    .line 1671226
    move-object v0, v0

    .line 1671227
    invoke-virtual {v0}, LX/7l3;->a()Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    move-result-object v0

    .line 1671228
    const-string v3, "extra_composer_life_event_icon_model"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1671229
    iget-object v0, p0, LX/AQJ;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    const/4 v3, -0x1

    invoke-virtual {v0, v3, v2}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1671230
    iget-object v0, p0, LX/AQJ;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 1671231
    :cond_0
    const v0, -0x2411ca85

    invoke-static {v5, v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
