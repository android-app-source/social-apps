.class public final LX/A6H;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 16

    .prologue
    .line 1624080
    const/4 v12, 0x0

    .line 1624081
    const/4 v11, 0x0

    .line 1624082
    const/4 v10, 0x0

    .line 1624083
    const/4 v9, 0x0

    .line 1624084
    const/4 v8, 0x0

    .line 1624085
    const/4 v7, 0x0

    .line 1624086
    const/4 v6, 0x0

    .line 1624087
    const/4 v5, 0x0

    .line 1624088
    const/4 v4, 0x0

    .line 1624089
    const/4 v3, 0x0

    .line 1624090
    const/4 v2, 0x0

    .line 1624091
    const/4 v1, 0x0

    .line 1624092
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v13, v14, :cond_1

    .line 1624093
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1624094
    const/4 v1, 0x0

    .line 1624095
    :goto_0
    return v1

    .line 1624096
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1624097
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->END_OBJECT:LX/15z;

    if-eq v13, v14, :cond_9

    .line 1624098
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v13

    .line 1624099
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1624100
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v14, v15, :cond_1

    if-eqz v13, :cond_1

    .line 1624101
    const-string v14, "id"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 1624102
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto :goto_1

    .line 1624103
    :cond_2
    const-string v14, "live_video_count"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 1624104
    const/4 v4, 0x1

    .line 1624105
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v11

    goto :goto_1

    .line 1624106
    :cond_3
    const-string v14, "square_header_image"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 1624107
    invoke-static/range {p0 .. p1}, LX/A6E;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 1624108
    :cond_4
    const-string v14, "video_channel_can_viewer_pin"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 1624109
    const/4 v3, 0x1

    .line 1624110
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    goto :goto_1

    .line 1624111
    :cond_5
    const-string v14, "video_channel_is_viewer_pinned"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 1624112
    const/4 v2, 0x1

    .line 1624113
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v8

    goto :goto_1

    .line 1624114
    :cond_6
    const-string v14, "video_channel_max_new_count"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 1624115
    const/4 v1, 0x1

    .line 1624116
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v7

    goto :goto_1

    .line 1624117
    :cond_7
    const-string v14, "video_channel_subtitle"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 1624118
    invoke-static/range {p0 .. p1}, LX/A6F;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1624119
    :cond_8
    const-string v14, "video_channel_title"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 1624120
    invoke-static/range {p0 .. p1}, LX/A6G;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 1624121
    :cond_9
    const/16 v13, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 1624122
    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 1624123
    if-eqz v4, :cond_a

    .line 1624124
    const/4 v4, 0x1

    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11, v12}, LX/186;->a(III)V

    .line 1624125
    :cond_a
    const/4 v4, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->b(II)V

    .line 1624126
    if-eqz v3, :cond_b

    .line 1624127
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->a(IZ)V

    .line 1624128
    :cond_b
    if-eqz v2, :cond_c

    .line 1624129
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->a(IZ)V

    .line 1624130
    :cond_c
    if-eqz v1, :cond_d

    .line 1624131
    const/4 v1, 0x5

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v7, v2}, LX/186;->a(III)V

    .line 1624132
    :cond_d
    const/4 v1, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1624133
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1624134
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1624135
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1624136
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1624137
    if-eqz v0, :cond_0

    .line 1624138
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1624139
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1624140
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1624141
    if-eqz v0, :cond_1

    .line 1624142
    const-string v1, "live_video_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1624143
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1624144
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1624145
    if-eqz v0, :cond_2

    .line 1624146
    const-string v1, "square_header_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1624147
    invoke-static {p0, v0, p2}, LX/A6E;->a(LX/15i;ILX/0nX;)V

    .line 1624148
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1624149
    if-eqz v0, :cond_3

    .line 1624150
    const-string v1, "video_channel_can_viewer_pin"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1624151
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1624152
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1624153
    if-eqz v0, :cond_4

    .line 1624154
    const-string v1, "video_channel_is_viewer_pinned"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1624155
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1624156
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1624157
    if-eqz v0, :cond_5

    .line 1624158
    const-string v1, "video_channel_max_new_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1624159
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1624160
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1624161
    if-eqz v0, :cond_6

    .line 1624162
    const-string v1, "video_channel_subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1624163
    invoke-static {p0, v0, p2}, LX/A6F;->a(LX/15i;ILX/0nX;)V

    .line 1624164
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1624165
    if-eqz v0, :cond_7

    .line 1624166
    const-string v1, "video_channel_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1624167
    invoke-static {p0, v0, p2}, LX/A6G;->a(LX/15i;ILX/0nX;)V

    .line 1624168
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1624169
    return-void
.end method
