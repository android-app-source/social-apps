.class public LX/8n5;
.super Landroid/widget/Filter;
.source ""


# instance fields
.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/8nB;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Sh;

.field private final c:LX/8p4;

.field private d:Z

.field private e:Z

.field public f:Z

.field public g:Z

.field private h:Z

.field public i:Z

.field public j:Ljava/lang/CharSequence;

.field public k:Ljava/lang/String;

.field private l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/8nB;",
            ">;"
        }
    .end annotation
.end field

.field private m:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/tagging/data/UberbarDataSource;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/8n8;


# direct methods
.method public constructor <init>(LX/0Sh;LX/8p4;LX/0Or;LX/0Or;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/8p4;",
            "LX/0Or",
            "<",
            "LX/8nB;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/tagging/data/UberbarDataSource;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1400423
    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    .line 1400424
    iput-boolean v0, p0, LX/8n5;->e:Z

    .line 1400425
    iput-boolean v1, p0, LX/8n5;->f:Z

    .line 1400426
    iput-boolean v0, p0, LX/8n5;->g:Z

    .line 1400427
    iput-boolean v0, p0, LX/8n5;->h:Z

    .line 1400428
    iput-boolean v0, p0, LX/8n5;->i:Z

    .line 1400429
    iput-object p1, p0, LX/8n5;->b:LX/0Sh;

    .line 1400430
    iput-object p2, p0, LX/8n5;->c:LX/8p4;

    .line 1400431
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/8n5;->a:Ljava/util/ArrayList;

    .line 1400432
    iput-boolean v1, p0, LX/8n5;->d:Z

    .line 1400433
    iput-object p3, p0, LX/8n5;->l:LX/0Or;

    .line 1400434
    iput-object p4, p0, LX/8n5;->m:LX/0Or;

    .line 1400435
    return-void
.end method

.method private a(Ljava/lang/CharSequence;LX/8nB;)Ljava/util/List;
    .locals 6
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "LX/8nB;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1400436
    iget-boolean v2, p0, LX/8n5;->e:Z

    iget-boolean v3, p0, LX/8n5;->f:Z

    iget-boolean v4, p0, LX/8n5;->g:Z

    iget-boolean v5, p0, LX/8n5;->h:Z

    move-object v0, p2

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/8nB;->a(Ljava/lang/CharSequence;ZZZZ)Ljava/util/List;

    move-result-object v0

    .line 1400437
    invoke-static {p0, p1, v0}, LX/8n5;->b(LX/8n5;Ljava/lang/CharSequence;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/8n5;Ljava/lang/CharSequence;Ljava/util/List;)Ljava/util/List;
    .locals 5
    .param p0    # LX/8n5;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1400438
    const/4 v0, 0x0

    .line 1400439
    if-eqz p1, :cond_2

    .line 1400440
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 1400441
    :goto_0
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 1400442
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/model/TaggingProfile;

    .line 1400443
    iget-object v4, p0, LX/8n5;->c:LX/8p4;

    invoke-virtual {v4, v0, v1}, LX/8p4;->a(Lcom/facebook/tagging/model/TaggingProfile;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1400444
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1400445
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    const/16 v4, 0x3e8

    if-lt v0, v4, :cond_0

    .line 1400446
    :cond_1
    return-object v2

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/8nB;Z)V
    .locals 1

    .prologue
    .line 1400447
    iget-object v0, p0, LX/8n5;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1400448
    :goto_0
    return-void

    .line 1400449
    :cond_0
    iput-boolean p2, p0, LX/8n5;->d:Z

    .line 1400450
    iget-object v0, p0, LX/8n5;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/util/List;)V
    .locals 3
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1400451
    iget-object v0, p0, LX/8n5;->b:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 1400452
    new-instance v0, Landroid/widget/Filter$FilterResults;

    invoke-direct {v0}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 1400453
    iput-object p2, v0, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 1400454
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    iput v1, v0, Landroid/widget/Filter$FilterResults;->count:I

    .line 1400455
    iget-object v1, p0, LX/8n5;->b:LX/0Sh;

    new-instance v2, Lcom/facebook/tagging/adapter/filters/MentionsTagTypeaheadFilter$2;

    invoke-direct {v2, p0, p1, v0}, Lcom/facebook/tagging/adapter/filters/MentionsTagTypeaheadFilter$2;-><init>(LX/8n5;Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V

    invoke-virtual {v1, v2}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    .line 1400456
    return-void
.end method

.method public final performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 12
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 1400457
    iget-boolean v0, p0, LX/8n5;->d:Z

    if-eqz v0, :cond_0

    .line 1400458
    iget-object v0, p0, LX/8n5;->l:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8nB;

    invoke-virtual {p0, v0, v1}, LX/8n5;->a(LX/8nB;Z)V

    .line 1400459
    iget-object v0, p0, LX/8n5;->m:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8nB;

    invoke-virtual {p0, v0, v1}, LX/8n5;->a(LX/8nB;Z)V

    .line 1400460
    :cond_0
    iget-object v0, p0, LX/8n5;->b:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 1400461
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v10

    .line 1400462
    iget-object v0, p0, LX/8n5;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v11

    move v9, v1

    :goto_0
    if-ge v9, v11, :cond_2

    iget-object v0, p0, LX/8n5;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8nB;

    .line 1400463
    invoke-virtual {v0}, LX/8nB;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1400464
    iget-object v1, p0, LX/8n5;->k:Ljava/lang/String;

    move-object v2, v1

    .line 1400465
    iget-boolean v3, p0, LX/8n5;->e:Z

    iget-boolean v4, p0, LX/8n5;->f:Z

    iget-boolean v5, p0, LX/8n5;->g:Z

    iget-boolean v6, p0, LX/8n5;->h:Z

    iget-boolean v7, p0, LX/8n5;->i:Z

    new-instance v8, LX/8n4;

    invoke-direct {v8, p0}, LX/8n4;-><init>(LX/8n5;)V

    move-object v1, p1

    invoke-virtual/range {v0 .. v8}, LX/8nB;->a(Ljava/lang/CharSequence;Ljava/lang/String;ZZZZZLX/8JX;)V

    .line 1400466
    :goto_1
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_0

    .line 1400467
    :cond_1
    invoke-direct {p0, p1, v0}, LX/8n5;->a(Ljava/lang/CharSequence;LX/8nB;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v10, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    goto :goto_1

    .line 1400468
    :cond_2
    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LX/8n5;->a(Ljava/lang/CharSequence;Ljava/util/List;)V

    .line 1400469
    const/4 v0, 0x0

    return-object v0
.end method

.method public final publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 4
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/widget/Filter$FilterResults;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1400470
    iget-object v0, p0, LX/8n5;->b:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1400471
    if-eqz p2, :cond_0

    .line 1400472
    iget-object v0, p0, LX/8n5;->j:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8n5;->j:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1400473
    :cond_0
    :goto_0
    return-void

    .line 1400474
    :cond_1
    iget-object v0, p0, LX/8n5;->n:LX/8n8;

    if-eqz v0, :cond_0

    .line 1400475
    iget-object v1, p0, LX/8n5;->n:LX/8n8;

    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    const/4 p1, 0x0

    .line 1400476
    if-nez v0, :cond_2

    .line 1400477
    :goto_1
    goto :goto_0

    .line 1400478
    :cond_2
    iget-object v2, v1, LX/8n8;->d:Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;

    .line 1400479
    iput-boolean p1, v2, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->k:Z

    .line 1400480
    iget-boolean v2, v1, LX/8n8;->t:Z

    if-eqz v2, :cond_3

    .line 1400481
    iget-object v2, v1, LX/8n8;->d:Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;

    invoke-virtual {v2}, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->b()V

    .line 1400482
    :cond_3
    iget-boolean v2, v1, LX/8n8;->s:Z

    if-nez v2, :cond_4

    .line 1400483
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/tagging/model/TaggingProfile;

    .line 1400484
    iget-object p0, v1, LX/8n8;->d:Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;

    invoke-virtual {p0, v2}, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->a(Lcom/facebook/tagging/model/TaggingProfile;)Z

    goto :goto_2

    .line 1400485
    :cond_4
    invoke-static {v1, v0}, LX/8n8;->b(LX/8n8;Ljava/util/List;)LX/0Px;

    move-result-object v2

    .line 1400486
    iget-object v3, v1, LX/8n8;->u:LX/8nW;

    if-eqz v3, :cond_5

    invoke-static {v2}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 1400487
    :cond_5
    iget-object v3, v1, LX/8n8;->k:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1400488
    sget-object v3, LX/8nE;->COWORKERS:LX/8nE;

    invoke-static {v1, v2, v3}, LX/8n8;->a(LX/8n8;LX/0Px;LX/8nE;)V

    .line 1400489
    :cond_6
    iget-object v3, v1, LX/8n8;->v:LX/8nZ;

    if-eqz v3, :cond_7

    invoke-static {v2}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 1400490
    :cond_7
    iget-object v2, v1, LX/8n8;->d:Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;

    const/4 v3, 0x0

    .line 1400491
    iget-boolean p0, v2, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->l:Z

    if-nez p0, :cond_d

    .line 1400492
    :cond_8
    :goto_3
    move v2, v3

    .line 1400493
    if-eqz v2, :cond_9

    .line 1400494
    iget-object v2, v1, LX/8n8;->d:Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;

    new-instance v3, Lcom/facebook/tagging/model/TagExpansionInfoHeader;

    sget-object p0, LX/8nE;->OTHERS:LX/8nE;

    invoke-virtual {p0}, LX/8nE;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v3, p0}, Lcom/facebook/tagging/model/TagExpansionInfoHeader;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->a(Lcom/facebook/tagging/model/TaggingProfile;)Z

    .line 1400495
    :cond_9
    iget-object v3, v1, LX/8n8;->d:Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;

    iget-boolean v2, v1, LX/8n8;->s:Z

    if-nez v2, :cond_a

    iget-object v2, v1, LX/8n8;->m:Ljava/util/Comparator;

    :goto_4
    invoke-virtual {v3, v2}, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->a(Ljava/util/Comparator;)V

    .line 1400496
    iput-boolean p1, v1, LX/8n8;->t:Z

    .line 1400497
    iget-object v2, v1, LX/8n8;->d:Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;

    const v3, -0x77a0669a

    invoke-static {v2, v3}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_1

    .line 1400498
    :cond_a
    iget-object v2, v1, LX/8n8;->l:Ljava/util/Comparator;

    goto :goto_4

    .line 1400499
    :cond_b
    iget-object v3, v1, LX/8n8;->u:LX/8nW;

    invoke-virtual {v3}, LX/8nB;->d()LX/0Px;

    move-result-object p2

    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v0

    const/4 v3, 0x0

    move p0, v3

    :goto_5
    if-ge p0, v0, :cond_5

    invoke-virtual {p2, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1400500
    invoke-static {v3}, LX/8nE;->valueOf(Ljava/lang/String;)LX/8nE;

    move-result-object v3

    invoke-static {v1, v2, v3}, LX/8n8;->a(LX/8n8;LX/0Px;LX/8nE;)V

    .line 1400501
    add-int/lit8 v3, p0, 0x1

    move p0, v3

    goto :goto_5

    .line 1400502
    :cond_c
    iget-object v3, v1, LX/8n8;->v:LX/8nZ;

    invoke-virtual {v3}, LX/8nB;->d()LX/0Px;

    move-result-object p2

    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v0

    const/4 v3, 0x0

    move p0, v3

    :goto_6
    if-ge p0, v0, :cond_7

    invoke-virtual {p2, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1400503
    invoke-static {v3}, LX/8nE;->valueOf(Ljava/lang/String;)LX/8nE;

    move-result-object v3

    invoke-static {v1, v2, v3}, LX/8n8;->a(LX/8n8;LX/0Px;LX/8nE;)V

    .line 1400504
    add-int/lit8 v3, p0, 0x1

    move p0, v3

    goto :goto_6

    .line 1400505
    :cond_d
    invoke-virtual {v2}, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->getCount()I

    move-result p2

    .line 1400506
    if-eqz p2, :cond_8

    move p0, v3

    .line 1400507
    :goto_7
    if-ge p0, p2, :cond_e

    .line 1400508
    invoke-static {v2, p0}, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->d(Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;I)Lcom/facebook/tagging/model/TaggingProfile;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/tagging/model/TagExpansionInfoHeader;

    if-nez v0, :cond_8

    .line 1400509
    add-int/lit8 p0, p0, 0x1

    goto :goto_7

    .line 1400510
    :cond_e
    const/4 v3, 0x1

    goto :goto_3
.end method
