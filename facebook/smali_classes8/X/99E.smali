.class public final LX/99E;
.super Landroid/graphics/drawable/Drawable$ConstantState;
.source ""


# instance fields
.field public final a:Landroid/graphics/drawable/Drawable;

.field public final b:Z

.field public final c:I


# direct methods
.method public constructor <init>(LX/99E;)V
    .locals 3

    .prologue
    .line 1447182
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable$ConstantState;-><init>()V

    .line 1447183
    iget v0, p1, LX/99E;->c:I

    iput v0, p0, LX/99E;->c:I

    .line 1447184
    iget-boolean v0, p1, LX/99E;->b:Z

    iput-boolean v0, p0, LX/99E;->b:Z

    .line 1447185
    iget-object v0, p1, LX/99E;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/99E;->a:Landroid/graphics/drawable/Drawable;

    .line 1447186
    iget-object v0, p0, LX/99E;->a:Landroid/graphics/drawable/Drawable;

    iget v1, p0, LX/99E;->c:I

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 1447187
    return-void
.end method

.method public constructor <init>(Landroid/graphics/drawable/Drawable;ZI)V
    .locals 0

    .prologue
    .line 1447188
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable$ConstantState;-><init>()V

    .line 1447189
    iput-object p1, p0, LX/99E;->a:Landroid/graphics/drawable/Drawable;

    .line 1447190
    iput-boolean p2, p0, LX/99E;->b:Z

    .line 1447191
    iput p3, p0, LX/99E;->c:I

    .line 1447192
    return-void
.end method


# virtual methods
.method public final getChangingConfigurations()I
    .locals 1

    .prologue
    .line 1447193
    const/4 v0, 0x0

    return v0
.end method

.method public final newDrawable()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 1447194
    new-instance v0, LX/99F;

    invoke-direct {v0, p0}, LX/99F;-><init>(LX/99E;)V

    return-object v0
.end method
