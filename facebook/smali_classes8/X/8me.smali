.class public final LX/8me;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:LX/8mf;

.field private b:J

.field private c:Landroid/widget/AdapterView$OnItemClickListener;


# direct methods
.method public constructor <init>(LX/8mf;Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 0

    .prologue
    .line 1399694
    iput-object p1, p0, LX/8me;->a:LX/8mf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1399695
    iput-object p2, p0, LX/8me;->c:Landroid/widget/AdapterView$OnItemClickListener;

    .line 1399696
    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1399697
    iget-object v0, p0, LX/8me;->a:LX/8mf;

    iget-object v0, v0, LX/8mf;->a:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    .line 1399698
    iget-wide v2, p0, LX/8me;->b:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x258

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    iget-object v2, p0, LX/8me;->c:Landroid/widget/AdapterView$OnItemClickListener;

    if-eqz v2, :cond_0

    .line 1399699
    iput-wide v0, p0, LX/8me;->b:J

    .line 1399700
    iget-object v0, p0, LX/8me;->c:Landroid/widget/AdapterView$OnItemClickListener;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemClickListener;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 1399701
    :cond_0
    return-void
.end method
