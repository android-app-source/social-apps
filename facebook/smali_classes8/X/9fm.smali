.class public final LX/9fm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

.field public final synthetic b:Landroid/net/Uri;

.field public final synthetic c:Landroid/graphics/RectF;

.field public final synthetic d:LX/434;

.field public final synthetic e:LX/9fn;


# direct methods
.method public constructor <init>(LX/9fn;Lcom/facebook/photos/creativeediting/model/CreativeEditingData;Landroid/net/Uri;Landroid/graphics/RectF;LX/434;)V
    .locals 0

    .prologue
    .line 1522646
    iput-object p1, p0, LX/9fm;->e:LX/9fn;

    iput-object p2, p0, LX/9fm;->a:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    iput-object p3, p0, LX/9fm;->b:Landroid/net/Uri;

    iput-object p4, p0, LX/9fm;->c:Landroid/graphics/RectF;

    iput-object p5, p0, LX/9fm;->d:LX/434;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1522647
    iget-object v0, p0, LX/9fm;->e:LX/9fn;

    iget-object v0, v0, LX/9fn;->b:LX/0TD;

    new-instance v1, Lcom/facebook/photos/editgallery/utils/CropImageUtils$2$1;

    invoke-direct {v1, p0}, Lcom/facebook/photos/editgallery/utils/CropImageUtils$2$1;-><init>(LX/9fm;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
