.class public LX/9dV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9dM;


# instance fields
.field private final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/SwipeableParams;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/3kp;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9dU;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Px;LX/0Ot;LX/3kp;)V
    .locals 2
    .param p1    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/SwipeableParams;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/9dU;",
            ">;",
            "LX/3kp;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 1518209
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1518210
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1518211
    iput-object p1, p0, LX/9dV;->a:LX/0Px;

    .line 1518212
    iput-object p2, p0, LX/9dV;->c:LX/0Ot;

    .line 1518213
    iput-object p3, p0, LX/9dV;->b:LX/3kp;

    .line 1518214
    iget-object v0, p0, LX/9dV;->b:LX/3kp;

    sget-object v1, LX/0dp;->m:LX/0Tn;

    invoke-virtual {v0, v1}, LX/3kp;->a(LX/0Tn;)V

    .line 1518215
    return-void

    .line 1518216
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 1518197
    iget-object v1, p0, LX/9dV;->b:LX/3kp;

    invoke-virtual {v1}, LX/3kp;->c()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1518198
    :cond_0
    :goto_0
    return v0

    .line 1518199
    :cond_1
    iget-object v1, p0, LX/9dV;->a:LX/0Px;

    sget-object v2, LX/5jI;->FILTER:LX/5jI;

    const/4 v4, 0x0

    .line 1518200
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v6

    move v5, v4

    :goto_1
    if-ge v5, v6, :cond_3

    invoke-virtual {v1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1518201
    iget-object p0, v3, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->c:LX/5jI;

    move-object v3, p0

    .line 1518202
    if-ne v3, v2, :cond_2

    .line 1518203
    const/4 v3, 0x1

    .line 1518204
    :goto_2
    move v1, v3

    .line 1518205
    if-eqz v1, :cond_0

    .line 1518206
    const/4 v0, 0x1

    goto :goto_0

    .line 1518207
    :cond_2
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_1

    :cond_3
    move v3, v4

    .line 1518208
    goto :goto_2
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1518217
    iget-object v0, p0, LX/9dV;->b:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->a()V

    .line 1518218
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1518196
    const/4 v0, 0x0

    return v0
.end method

.method public final d()LX/9dK;
    .locals 1

    .prologue
    .line 1518195
    iget-object v0, p0, LX/9dV;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9dK;

    return-object v0
.end method
