.class public LX/A8e;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/0Zj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zj",
            "<",
            "LX/1Jw;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/0Zj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zj",
            "<",
            "LX/1Jx;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0x20

    .line 1627920
    new-instance v0, LX/0Zj;

    invoke-direct {v0, v1}, LX/0Zj;-><init>(I)V

    sput-object v0, LX/A8e;->a:LX/0Zj;

    .line 1627921
    new-instance v0, LX/0Zj;

    invoke-direct {v0, v1}, LX/0Zj;-><init>(I)V

    sput-object v0, LX/A8e;->b:LX/0Zj;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1627946
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(II)LX/1Jw;
    .locals 1

    .prologue
    .line 1627936
    sget-object v0, LX/A8e;->a:LX/0Zj;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Jw;

    .line 1627937
    if-nez v0, :cond_0

    .line 1627938
    new-instance v0, LX/1Jw;

    invoke-direct {v0}, LX/1Jw;-><init>()V

    .line 1627939
    :cond_0
    invoke-virtual {v0, p0, p1}, LX/1Jw;->a(II)V

    .line 1627940
    return-object v0
.end method

.method public static a(LX/1Jw;LX/1Jw;)LX/1Jx;
    .locals 2
    .param p0    # LX/1Jw;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # LX/1Jw;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1627941
    sget-object v0, LX/A8e;->b:LX/0Zj;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Jx;

    .line 1627942
    if-nez v0, :cond_0

    .line 1627943
    new-instance v0, LX/1Jx;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LX/1Jx;-><init>(Z)V

    .line 1627944
    :cond_0
    invoke-virtual {v0, p0, p1}, LX/1Jx;->b(LX/1Jw;LX/1Jw;)V

    .line 1627945
    return-object v0
.end method

.method public static a(LX/1Jw;)V
    .locals 1

    .prologue
    .line 1627931
    iget-boolean v0, p0, LX/1Jw;->f:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1627932
    if-nez v0, :cond_0

    .line 1627933
    :goto_1
    return-void

    .line 1627934
    :cond_0
    invoke-virtual {p0}, LX/1Jw;->a()V

    .line 1627935
    sget-object v0, LX/A8e;->a:LX/0Zj;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/1Jx;)V
    .locals 1

    .prologue
    .line 1627923
    iget-boolean v0, p0, LX/1Jx;->a:Z

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1627924
    if-nez v0, :cond_0

    .line 1627925
    :goto_1
    return-void

    .line 1627926
    :cond_0
    iget-object v0, p0, LX/1Jx;->b:LX/1Jw;

    if-eqz v0, :cond_1

    .line 1627927
    iget-object v0, p0, LX/1Jx;->b:LX/1Jw;

    invoke-virtual {v0}, LX/1Jw;->a()V

    .line 1627928
    :cond_1
    iget-object v0, p0, LX/1Jx;->c:LX/1Jw;

    if-eqz v0, :cond_2

    .line 1627929
    iget-object v0, p0, LX/1Jx;->c:LX/1Jw;

    invoke-virtual {v0}, LX/1Jw;->a()V

    .line 1627930
    :cond_2
    sget-object v0, LX/A8e;->b:LX/0Zj;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/1Jw;)LX/1Jx;
    .locals 1
    .param p0    # LX/1Jw;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1627922
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/A8e;->a(LX/1Jw;LX/1Jw;)LX/1Jx;

    move-result-object v0

    return-object v0
.end method
