.class public final LX/9k1;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/places/pagetopics/FetchPageTopicsResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/places/common/SelectPageTopicActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/places/common/SelectPageTopicActivity;)V
    .locals 0

    .prologue
    .line 1530656
    iput-object p1, p0, LX/9k1;->a:Lcom/facebook/places/common/SelectPageTopicActivity;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/places/pagetopics/FetchPageTopicsResult;)V
    .locals 2

    .prologue
    .line 1530657
    iget-object v0, p0, LX/9k1;->a:Lcom/facebook/places/common/SelectPageTopicActivity;

    iget-object v0, v0, Lcom/facebook/places/common/SelectPageTopicActivity;->p:LX/9k4;

    invoke-virtual {p1}, Lcom/facebook/places/pagetopics/FetchPageTopicsResult;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9k4;->a(Ljava/util/List;)V

    .line 1530658
    iget-object v0, p0, LX/9k1;->a:Lcom/facebook/places/common/SelectPageTopicActivity;

    iget-object v0, v0, Lcom/facebook/places/common/SelectPageTopicActivity;->p:LX/9k4;

    const v1, 0x20e65ccf

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1530659
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1530660
    iget-object v0, p0, LX/9k1;->a:Lcom/facebook/places/common/SelectPageTopicActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/places/common/SelectPageTopicActivity;->b(Z)V

    .line 1530661
    iget-object v0, p0, LX/9k1;->a:Lcom/facebook/places/common/SelectPageTopicActivity;

    const v1, 0x7f0d0d65

    invoke-static {v0, v1}, Lcom/facebook/places/common/SelectPageTopicActivity;->a(Lcom/facebook/places/common/SelectPageTopicActivity;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1530662
    const v1, 0x7f080040

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1530663
    iget-object v0, p0, LX/9k1;->a:Lcom/facebook/places/common/SelectPageTopicActivity;

    const v1, 0x1020004

    invoke-static {v0, v1}, Lcom/facebook/places/common/SelectPageTopicActivity;->b(Lcom/facebook/places/common/SelectPageTopicActivity;I)Landroid/view/View;

    move-result-object v0

    .line 1530664
    new-instance v1, LX/9k0;

    invoke-direct {v1, p0, v0}, LX/9k0;-><init>(LX/9k1;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1530665
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1530666
    check-cast p1, Lcom/facebook/places/pagetopics/FetchPageTopicsResult;

    invoke-direct {p0, p1}, LX/9k1;->a(Lcom/facebook/places/pagetopics/FetchPageTopicsResult;)V

    return-void
.end method
