.class public LX/9nY;
.super Landroid/graphics/drawable/Drawable;
.source ""


# instance fields
.field private a:LX/5rI;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private b:LX/5rI;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private c:LX/5rI;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:LX/9nX;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Landroid/graphics/PathEffect;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Landroid/graphics/Path;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Landroid/graphics/Path;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Landroid/graphics/Path;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Landroid/graphics/RectF;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Landroid/graphics/RectF;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Z

.field private l:F

.field private final m:Landroid/graphics/Paint;

.field public n:I

.field private o:I

.field private p:[F
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1537489
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 1537490
    iput-boolean v2, p0, LX/9nY;->k:Z

    .line 1537491
    const/high16 v0, 0x7fc00000    # NaNf

    iput v0, p0, LX/9nY;->l:F

    .line 1537492
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/9nY;->m:Landroid/graphics/Paint;

    .line 1537493
    iput v2, p0, LX/9nY;->n:I

    .line 1537494
    const/16 v0, 0xff

    iput v0, p0, LX/9nY;->o:I

    return-void
.end method

.method private static a(FF)I
    .locals 3

    .prologue
    .line 1537495
    const v0, 0xffffff

    float-to-int v1, p1

    and-int/2addr v0, v1

    .line 1537496
    const/high16 v1, -0x1000000

    float-to-int v2, p0

    shl-int/lit8 v2, v2, 0x18

    and-int/2addr v1, v2

    .line 1537497
    or-int/2addr v0, v1

    return v0
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 1537498
    invoke-direct {p0}, LX/9nY;->b()V

    .line 1537499
    iget v0, p0, LX/9nY;->n:I

    iget v1, p0, LX/9nY;->o:I

    invoke-static {v0, v1}, LX/9nT;->a(II)I

    move-result v0

    .line 1537500
    ushr-int/lit8 v1, v0, 0x18

    if-eqz v1, :cond_0

    .line 1537501
    iget-object v1, p0, LX/9nY;->m:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 1537502
    iget-object v0, p0, LX/9nY;->m:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1537503
    iget-object v0, p0, LX/9nY;->f:Landroid/graphics/Path;

    iget-object v1, p0, LX/9nY;->m:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1537504
    :cond_0
    invoke-direct {p0}, LX/9nY;->d()F

    move-result v0

    .line 1537505
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-lez v1, :cond_1

    .line 1537506
    invoke-direct {p0}, LX/9nY;->e()I

    move-result v1

    .line 1537507
    iget-object v2, p0, LX/9nY;->m:Landroid/graphics/Paint;

    iget v3, p0, LX/9nY;->o:I

    invoke-static {v1, v3}, LX/9nT;->a(II)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1537508
    iget-object v1, p0, LX/9nY;->m:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1537509
    iget-object v1, p0, LX/9nY;->m:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1537510
    iget-object v0, p0, LX/9nY;->f:Landroid/graphics/Path;

    iget-object v1, p0, LX/9nY;->m:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1537511
    :cond_1
    return-void
.end method

.method private b(I)I
    .locals 1

    .prologue
    .line 1537512
    iget-object v0, p0, LX/9nY;->a:LX/5rI;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9nY;->a:LX/5rI;

    invoke-virtual {v0, p1}, LX/5rI;->a(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()V
    .locals 13

    .prologue
    const/4 v1, 0x0

    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 1537456
    iget-boolean v0, p0, LX/9nY;->k:Z

    if-nez v0, :cond_0

    .line 1537457
    :goto_0
    return-void

    .line 1537458
    :cond_0
    iput-boolean v9, p0, LX/9nY;->k:Z

    .line 1537459
    iget-object v0, p0, LX/9nY;->f:Landroid/graphics/Path;

    if-nez v0, :cond_1

    .line 1537460
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, LX/9nY;->f:Landroid/graphics/Path;

    .line 1537461
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/9nY;->i:Landroid/graphics/RectF;

    .line 1537462
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, LX/9nY;->g:Landroid/graphics/Path;

    .line 1537463
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/9nY;->j:Landroid/graphics/RectF;

    .line 1537464
    :cond_1
    iget-object v0, p0, LX/9nY;->f:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 1537465
    iget-object v0, p0, LX/9nY;->g:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 1537466
    iget-object v0, p0, LX/9nY;->i:Landroid/graphics/RectF;

    invoke-virtual {p0}, LX/9nY;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 1537467
    iget-object v0, p0, LX/9nY;->j:Landroid/graphics/RectF;

    invoke-virtual {p0}, LX/9nY;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 1537468
    invoke-direct {p0}, LX/9nY;->d()F

    move-result v0

    .line 1537469
    cmpl-float v2, v0, v1

    if-lez v2, :cond_2

    .line 1537470
    iget-object v2, p0, LX/9nY;->i:Landroid/graphics/RectF;

    const/high16 v3, 0x3f000000    # 0.5f

    mul-float/2addr v3, v0

    const/high16 v4, 0x3f000000    # 0.5f

    mul-float/2addr v0, v4

    invoke-virtual {v2, v3, v0}, Landroid/graphics/RectF;->inset(FF)V

    .line 1537471
    :cond_2
    iget v0, p0, LX/9nY;->l:F

    invoke-static {v0}, LX/1mo;->a(F)Z

    move-result v0

    if-nez v0, :cond_5

    iget v0, p0, LX/9nY;->l:F

    .line 1537472
    :goto_1
    iget-object v2, p0, LX/9nY;->p:[F

    if-eqz v2, :cond_6

    iget-object v2, p0, LX/9nY;->p:[F

    aget v2, v2, v9

    invoke-static {v2}, LX/1mo;->a(F)Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v2, p0, LX/9nY;->p:[F

    aget v2, v2, v9

    .line 1537473
    :goto_2
    iget-object v3, p0, LX/9nY;->p:[F

    if-eqz v3, :cond_7

    iget-object v3, p0, LX/9nY;->p:[F

    aget v3, v3, v10

    invoke-static {v3}, LX/1mo;->a(F)Z

    move-result v3

    if-nez v3, :cond_7

    iget-object v3, p0, LX/9nY;->p:[F

    aget v3, v3, v10

    .line 1537474
    :goto_3
    iget-object v4, p0, LX/9nY;->p:[F

    if-eqz v4, :cond_8

    iget-object v4, p0, LX/9nY;->p:[F

    aget v4, v4, v11

    invoke-static {v4}, LX/1mo;->a(F)Z

    move-result v4

    if-nez v4, :cond_8

    iget-object v4, p0, LX/9nY;->p:[F

    aget v4, v4, v11

    .line 1537475
    :goto_4
    iget-object v5, p0, LX/9nY;->p:[F

    if-eqz v5, :cond_3

    iget-object v5, p0, LX/9nY;->p:[F

    aget v5, v5, v12

    invoke-static {v5}, LX/1mo;->a(F)Z

    move-result v5

    if-nez v5, :cond_3

    iget-object v0, p0, LX/9nY;->p:[F

    aget v0, v0, v12

    .line 1537476
    :cond_3
    iget-object v5, p0, LX/9nY;->f:Landroid/graphics/Path;

    iget-object v6, p0, LX/9nY;->i:Landroid/graphics/RectF;

    const/16 v7, 0x8

    new-array v7, v7, [F

    aput v2, v7, v9

    aput v2, v7, v10

    aput v3, v7, v11

    aput v3, v7, v12

    const/4 v8, 0x4

    aput v4, v7, v8

    const/4 v8, 0x5

    aput v4, v7, v8

    const/4 v8, 0x6

    aput v0, v7, v8

    const/4 v8, 0x7

    aput v0, v7, v8

    sget-object v8, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v5, v6, v7, v8}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;[FLandroid/graphics/Path$Direction;)V

    .line 1537477
    iget-object v5, p0, LX/9nY;->a:LX/5rI;

    if-eqz v5, :cond_4

    .line 1537478
    iget-object v1, p0, LX/9nY;->a:LX/5rI;

    const/16 v5, 0x8

    invoke-virtual {v1, v5}, LX/5rI;->a(I)F

    move-result v1

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v1, v5

    .line 1537479
    :cond_4
    iget-object v5, p0, LX/9nY;->g:Landroid/graphics/Path;

    iget-object v6, p0, LX/9nY;->j:Landroid/graphics/RectF;

    const/16 v7, 0x8

    new-array v7, v7, [F

    add-float v8, v2, v1

    aput v8, v7, v9

    add-float/2addr v2, v1

    aput v2, v7, v10

    add-float v2, v3, v1

    aput v2, v7, v11

    add-float v2, v3, v1

    aput v2, v7, v12

    const/4 v2, 0x4

    add-float v3, v4, v1

    aput v3, v7, v2

    const/4 v2, 0x5

    add-float v3, v4, v1

    aput v3, v7, v2

    const/4 v2, 0x6

    add-float v3, v0, v1

    aput v3, v7, v2

    const/4 v2, 0x7

    add-float/2addr v0, v1

    aput v0, v7, v2

    sget-object v0, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v5, v6, v7, v0}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;[FLandroid/graphics/Path$Direction;)V

    goto/16 :goto_0

    :cond_5
    move v0, v1

    .line 1537480
    goto/16 :goto_1

    :cond_6
    move v2, v0

    .line 1537481
    goto/16 :goto_2

    :cond_7
    move v3, v0

    .line 1537482
    goto/16 :goto_3

    :cond_8
    move v4, v0

    .line 1537483
    goto :goto_4
.end method

.method private b(IF)V
    .locals 2

    .prologue
    .line 1537513
    iget-object v0, p0, LX/9nY;->b:LX/5rI;

    if-nez v0, :cond_0

    .line 1537514
    new-instance v0, LX/5rI;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LX/5rI;-><init>(F)V

    iput-object v0, p0, LX/9nY;->b:LX/5rI;

    .line 1537515
    :cond_0
    iget-object v0, p0, LX/9nY;->b:LX/5rI;

    invoke-virtual {v0, p1}, LX/5rI;->b(I)F

    move-result v0

    invoke-static {v0, p2}, LX/5qm;->a(FF)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1537516
    iget-object v0, p0, LX/9nY;->b:LX/5rI;

    invoke-virtual {v0, p1, p2}, LX/5rI;->a(IF)Z

    .line 1537517
    invoke-virtual {p0}, LX/9nY;->invalidateSelf()V

    .line 1537518
    :cond_1
    return-void
.end method

.method private b(Landroid/graphics/Canvas;)V
    .locals 14

    .prologue
    .line 1537524
    iget v0, p0, LX/9nY;->n:I

    iget v1, p0, LX/9nY;->o:I

    invoke-static {v0, v1}, LX/9nT;->a(II)I

    move-result v0

    .line 1537525
    ushr-int/lit8 v1, v0, 0x18

    if-eqz v1, :cond_0

    .line 1537526
    iget-object v1, p0, LX/9nY;->m:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 1537527
    iget-object v0, p0, LX/9nY;->m:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1537528
    invoke-virtual {p0}, LX/9nY;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    iget-object v1, p0, LX/9nY;->m:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1537529
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/9nY;->b(I)I

    move-result v0

    if-gtz v0, :cond_1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, LX/9nY;->b(I)I

    move-result v0

    if-gtz v0, :cond_1

    const/4 v0, 0x2

    invoke-direct {p0, v0}, LX/9nY;->b(I)I

    move-result v0

    if-gtz v0, :cond_1

    const/4 v0, 0x3

    invoke-direct {p0, v0}, LX/9nY;->b(I)I

    move-result v0

    if-lez v0, :cond_7

    .line 1537530
    :cond_1
    invoke-virtual {p0}, LX/9nY;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 1537531
    const/4 v1, 0x0

    invoke-direct {p0, v1}, LX/9nY;->b(I)I

    move-result v1

    .line 1537532
    const/4 v2, 0x1

    invoke-direct {p0, v2}, LX/9nY;->b(I)I

    move-result v2

    .line 1537533
    const/4 v3, 0x2

    invoke-direct {p0, v3}, LX/9nY;->b(I)I

    move-result v3

    .line 1537534
    const/4 v4, 0x3

    invoke-direct {p0, v4}, LX/9nY;->b(I)I

    move-result v4

    .line 1537535
    const/4 v5, 0x0

    invoke-direct {p0, v5}, LX/9nY;->c(I)I

    move-result v5

    .line 1537536
    const/4 v6, 0x1

    invoke-direct {p0, v6}, LX/9nY;->c(I)I

    move-result v6

    .line 1537537
    const/4 v7, 0x2

    invoke-direct {p0, v7}, LX/9nY;->c(I)I

    move-result v7

    .line 1537538
    const/4 v8, 0x3

    invoke-direct {p0, v8}, LX/9nY;->c(I)I

    move-result v8

    .line 1537539
    iget v9, v0, Landroid/graphics/Rect;->top:I

    .line 1537540
    iget v10, v0, Landroid/graphics/Rect;->left:I

    .line 1537541
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v11

    .line 1537542
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    .line 1537543
    iget-object v12, p0, LX/9nY;->m:Landroid/graphics/Paint;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1537544
    iget-object v12, p0, LX/9nY;->h:Landroid/graphics/Path;

    if-nez v12, :cond_2

    .line 1537545
    new-instance v12, Landroid/graphics/Path;

    invoke-direct {v12}, Landroid/graphics/Path;-><init>()V

    iput-object v12, p0, LX/9nY;->h:Landroid/graphics/Path;

    .line 1537546
    :cond_2
    if-lez v1, :cond_3

    if-eqz v5, :cond_3

    .line 1537547
    iget-object v12, p0, LX/9nY;->m:Landroid/graphics/Paint;

    invoke-virtual {v12, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 1537548
    iget-object v5, p0, LX/9nY;->h:Landroid/graphics/Path;

    invoke-virtual {v5}, Landroid/graphics/Path;->reset()V

    .line 1537549
    iget-object v5, p0, LX/9nY;->h:Landroid/graphics/Path;

    int-to-float v12, v10

    int-to-float v13, v9

    invoke-virtual {v5, v12, v13}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1537550
    iget-object v5, p0, LX/9nY;->h:Landroid/graphics/Path;

    add-int v12, v10, v1

    int-to-float v12, v12

    add-int v13, v9, v2

    int-to-float v13, v13

    invoke-virtual {v5, v12, v13}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1537551
    iget-object v5, p0, LX/9nY;->h:Landroid/graphics/Path;

    add-int v12, v10, v1

    int-to-float v12, v12

    add-int v13, v9, v0

    sub-int/2addr v13, v4

    int-to-float v13, v13

    invoke-virtual {v5, v12, v13}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1537552
    iget-object v5, p0, LX/9nY;->h:Landroid/graphics/Path;

    int-to-float v12, v10

    add-int v13, v9, v0

    int-to-float v13, v13

    invoke-virtual {v5, v12, v13}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1537553
    iget-object v5, p0, LX/9nY;->h:Landroid/graphics/Path;

    int-to-float v12, v10

    int-to-float v13, v9

    invoke-virtual {v5, v12, v13}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1537554
    iget-object v5, p0, LX/9nY;->h:Landroid/graphics/Path;

    iget-object v12, p0, LX/9nY;->m:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v12}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1537555
    :cond_3
    if-lez v2, :cond_4

    if-eqz v6, :cond_4

    .line 1537556
    iget-object v5, p0, LX/9nY;->m:Landroid/graphics/Paint;

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 1537557
    iget-object v5, p0, LX/9nY;->h:Landroid/graphics/Path;

    invoke-virtual {v5}, Landroid/graphics/Path;->reset()V

    .line 1537558
    iget-object v5, p0, LX/9nY;->h:Landroid/graphics/Path;

    int-to-float v6, v10

    int-to-float v12, v9

    invoke-virtual {v5, v6, v12}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1537559
    iget-object v5, p0, LX/9nY;->h:Landroid/graphics/Path;

    add-int v6, v10, v1

    int-to-float v6, v6

    add-int v12, v9, v2

    int-to-float v12, v12

    invoke-virtual {v5, v6, v12}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1537560
    iget-object v5, p0, LX/9nY;->h:Landroid/graphics/Path;

    add-int v6, v10, v11

    sub-int/2addr v6, v3

    int-to-float v6, v6

    add-int v12, v9, v2

    int-to-float v12, v12

    invoke-virtual {v5, v6, v12}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1537561
    iget-object v5, p0, LX/9nY;->h:Landroid/graphics/Path;

    add-int v6, v10, v11

    int-to-float v6, v6

    int-to-float v12, v9

    invoke-virtual {v5, v6, v12}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1537562
    iget-object v5, p0, LX/9nY;->h:Landroid/graphics/Path;

    int-to-float v6, v10

    int-to-float v12, v9

    invoke-virtual {v5, v6, v12}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1537563
    iget-object v5, p0, LX/9nY;->h:Landroid/graphics/Path;

    iget-object v6, p0, LX/9nY;->m:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v6}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1537564
    :cond_4
    if-lez v3, :cond_5

    if-eqz v7, :cond_5

    .line 1537565
    iget-object v5, p0, LX/9nY;->m:Landroid/graphics/Paint;

    invoke-virtual {v5, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 1537566
    iget-object v5, p0, LX/9nY;->h:Landroid/graphics/Path;

    invoke-virtual {v5}, Landroid/graphics/Path;->reset()V

    .line 1537567
    iget-object v5, p0, LX/9nY;->h:Landroid/graphics/Path;

    add-int v6, v10, v11

    int-to-float v6, v6

    int-to-float v7, v9

    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1537568
    iget-object v5, p0, LX/9nY;->h:Landroid/graphics/Path;

    add-int v6, v10, v11

    int-to-float v6, v6

    add-int v7, v9, v0

    int-to-float v7, v7

    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1537569
    iget-object v5, p0, LX/9nY;->h:Landroid/graphics/Path;

    add-int v6, v10, v11

    sub-int/2addr v6, v3

    int-to-float v6, v6

    add-int v7, v9, v0

    sub-int/2addr v7, v4

    int-to-float v7, v7

    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1537570
    iget-object v5, p0, LX/9nY;->h:Landroid/graphics/Path;

    add-int v6, v10, v11

    sub-int/2addr v6, v3

    int-to-float v6, v6

    add-int/2addr v2, v9

    int-to-float v2, v2

    invoke-virtual {v5, v6, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1537571
    iget-object v2, p0, LX/9nY;->h:Landroid/graphics/Path;

    add-int v5, v10, v11

    int-to-float v5, v5

    int-to-float v6, v9

    invoke-virtual {v2, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1537572
    iget-object v2, p0, LX/9nY;->h:Landroid/graphics/Path;

    iget-object v5, p0, LX/9nY;->m:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v5}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1537573
    :cond_5
    if-lez v4, :cond_6

    if-eqz v8, :cond_6

    .line 1537574
    iget-object v2, p0, LX/9nY;->m:Landroid/graphics/Paint;

    invoke-virtual {v2, v8}, Landroid/graphics/Paint;->setColor(I)V

    .line 1537575
    iget-object v2, p0, LX/9nY;->h:Landroid/graphics/Path;

    invoke-virtual {v2}, Landroid/graphics/Path;->reset()V

    .line 1537576
    iget-object v2, p0, LX/9nY;->h:Landroid/graphics/Path;

    int-to-float v5, v10

    add-int v6, v9, v0

    int-to-float v6, v6

    invoke-virtual {v2, v5, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1537577
    iget-object v2, p0, LX/9nY;->h:Landroid/graphics/Path;

    add-int v5, v10, v11

    int-to-float v5, v5

    add-int v6, v9, v0

    int-to-float v6, v6

    invoke-virtual {v2, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1537578
    iget-object v2, p0, LX/9nY;->h:Landroid/graphics/Path;

    add-int v5, v10, v11

    sub-int v3, v5, v3

    int-to-float v3, v3

    add-int v5, v9, v0

    sub-int/2addr v5, v4

    int-to-float v5, v5

    invoke-virtual {v2, v3, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1537579
    iget-object v2, p0, LX/9nY;->h:Landroid/graphics/Path;

    add-int/2addr v1, v10

    int-to-float v1, v1

    add-int v3, v9, v0

    sub-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {v2, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1537580
    iget-object v1, p0, LX/9nY;->h:Landroid/graphics/Path;

    int-to-float v2, v10

    add-int/2addr v0, v9

    int-to-float v0, v0

    invoke-virtual {v1, v2, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1537581
    iget-object v0, p0, LX/9nY;->h:Landroid/graphics/Path;

    iget-object v1, p0, LX/9nY;->m:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1537582
    :cond_6
    iget-object v0, p0, LX/9nY;->m:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1537583
    :cond_7
    return-void
.end method

.method private c(I)I
    .locals 2

    .prologue
    .line 1537519
    iget-object v0, p0, LX/9nY;->b:LX/5rI;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9nY;->b:LX/5rI;

    invoke-virtual {v0, p1}, LX/5rI;->a(I)F

    move-result v0

    .line 1537520
    :goto_0
    iget-object v1, p0, LX/9nY;->c:LX/5rI;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/9nY;->c:LX/5rI;

    invoke-virtual {v1, p1}, LX/5rI;->a(I)F

    move-result v1

    .line 1537521
    :goto_1
    invoke-static {v1, v0}, LX/9nY;->a(FF)I

    move-result v0

    return v0

    .line 1537522
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1537523
    :cond_1
    const/high16 v1, 0x437f0000    # 255.0f

    goto :goto_1
.end method

.method private c()V
    .locals 2

    .prologue
    .line 1537584
    iget-object v0, p0, LX/9nY;->d:LX/9nX;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9nY;->d:LX/9nX;

    invoke-direct {p0}, LX/9nY;->d()F

    move-result v1

    invoke-virtual {v0, v1}, LX/9nX;->getPathEffect(F)Landroid/graphics/PathEffect;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/9nY;->e:Landroid/graphics/PathEffect;

    .line 1537585
    iget-object v0, p0, LX/9nY;->m:Landroid/graphics/Paint;

    iget-object v1, p0, LX/9nY;->e:Landroid/graphics/PathEffect;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 1537586
    return-void

    .line 1537587
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(IF)V
    .locals 2

    .prologue
    .line 1537588
    iget-object v0, p0, LX/9nY;->c:LX/5rI;

    if-nez v0, :cond_0

    .line 1537589
    new-instance v0, LX/5rI;

    const/high16 v1, 0x437f0000    # 255.0f

    invoke-direct {v0, v1}, LX/5rI;-><init>(F)V

    iput-object v0, p0, LX/9nY;->c:LX/5rI;

    .line 1537590
    :cond_0
    iget-object v0, p0, LX/9nY;->c:LX/5rI;

    invoke-virtual {v0, p1}, LX/5rI;->b(I)F

    move-result v0

    invoke-static {v0, p2}, LX/5qm;->a(FF)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1537591
    iget-object v0, p0, LX/9nY;->c:LX/5rI;

    invoke-virtual {v0, p1, p2}, LX/5rI;->a(IF)Z

    .line 1537592
    invoke-virtual {p0}, LX/9nY;->invalidateSelf()V

    .line 1537593
    :cond_1
    return-void
.end method

.method private d()F
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 1537594
    iget-object v0, p0, LX/9nY;->a:LX/5rI;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9nY;->a:LX/5rI;

    invoke-virtual {v0, v1}, LX/5rI;->b(I)F

    move-result v0

    invoke-static {v0}, LX/1mo;->a(F)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/9nY;->a:LX/5rI;

    invoke-virtual {v0, v1}, LX/5rI;->b(I)F

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()I
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 1537484
    iget-object v0, p0, LX/9nY;->b:LX/5rI;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9nY;->b:LX/5rI;

    invoke-virtual {v0, v2}, LX/5rI;->b(I)F

    move-result v0

    invoke-static {v0}, LX/1mo;->a(F)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/9nY;->b:LX/5rI;

    invoke-virtual {v0, v2}, LX/5rI;->b(I)F

    move-result v0

    .line 1537485
    :goto_0
    iget-object v1, p0, LX/9nY;->c:LX/5rI;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/9nY;->c:LX/5rI;

    invoke-virtual {v1, v2}, LX/5rI;->b(I)F

    move-result v1

    invoke-static {v1}, LX/1mo;->a(F)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, LX/9nY;->c:LX/5rI;

    invoke-virtual {v1, v2}, LX/5rI;->b(I)F

    move-result v1

    .line 1537486
    :goto_1
    invoke-static {v1, v0}, LX/9nY;->a(FF)I

    move-result v0

    return v0

    .line 1537487
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1537488
    :cond_1
    const/high16 v1, 0x437f0000    # 255.0f

    goto :goto_1
.end method


# virtual methods
.method public final a(F)V
    .locals 1

    .prologue
    .line 1537595
    iget v0, p0, LX/9nY;->l:F

    invoke-static {v0, p1}, LX/5qm;->a(FF)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1537596
    iput p1, p0, LX/9nY;->l:F

    .line 1537597
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/9nY;->k:Z

    .line 1537598
    invoke-virtual {p0}, LX/9nY;->invalidateSelf()V

    .line 1537599
    :cond_0
    return-void
.end method

.method public final a(FI)V
    .locals 2

    .prologue
    .line 1537403
    iget-object v0, p0, LX/9nY;->p:[F

    if-nez v0, :cond_0

    .line 1537404
    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, LX/9nY;->p:[F

    .line 1537405
    iget-object v0, p0, LX/9nY;->p:[F

    const/high16 v1, 0x7fc00000    # NaNf

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    .line 1537406
    :cond_0
    iget-object v0, p0, LX/9nY;->p:[F

    aget v0, v0, p2

    invoke-static {v0, p1}, LX/5qm;->a(FF)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1537407
    iget-object v0, p0, LX/9nY;->p:[F

    aput p1, v0, p2

    .line 1537408
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/9nY;->k:Z

    .line 1537409
    invoke-virtual {p0}, LX/9nY;->invalidateSelf()V

    .line 1537410
    :cond_1
    return-void
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 1537411
    iput p1, p0, LX/9nY;->n:I

    .line 1537412
    invoke-virtual {p0}, LX/9nY;->invalidateSelf()V

    .line 1537413
    return-void
.end method

.method public final a(IF)V
    .locals 1

    .prologue
    .line 1537414
    iget-object v0, p0, LX/9nY;->a:LX/5rI;

    if-nez v0, :cond_0

    .line 1537415
    new-instance v0, LX/5rI;

    invoke-direct {v0}, LX/5rI;-><init>()V

    iput-object v0, p0, LX/9nY;->a:LX/5rI;

    .line 1537416
    :cond_0
    iget-object v0, p0, LX/9nY;->a:LX/5rI;

    invoke-virtual {v0, p1}, LX/5rI;->b(I)F

    move-result v0

    invoke-static {v0, p2}, LX/5qm;->a(FF)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1537417
    iget-object v0, p0, LX/9nY;->a:LX/5rI;

    invoke-virtual {v0, p1, p2}, LX/5rI;->a(IF)Z

    .line 1537418
    const/16 v0, 0x8

    if-ne p1, v0, :cond_1

    .line 1537419
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/9nY;->k:Z

    .line 1537420
    :cond_1
    invoke-virtual {p0}, LX/9nY;->invalidateSelf()V

    .line 1537421
    :cond_2
    return-void
.end method

.method public final a(IFF)V
    .locals 0

    .prologue
    .line 1537422
    invoke-direct {p0, p1, p2}, LX/9nY;->b(IF)V

    .line 1537423
    invoke-direct {p0, p1, p3}, LX/9nY;->c(IF)V

    .line 1537424
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1537425
    if-nez p1, :cond_1

    const/4 v0, 0x0

    .line 1537426
    :goto_0
    iget-object v1, p0, LX/9nY;->d:LX/9nX;

    if-eq v1, v0, :cond_0

    .line 1537427
    iput-object v0, p0, LX/9nY;->d:LX/9nX;

    .line 1537428
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/9nY;->k:Z

    .line 1537429
    invoke-virtual {p0}, LX/9nY;->invalidateSelf()V

    .line 1537430
    :cond_0
    return-void

    .line 1537431
    :cond_1
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/9nX;->valueOf(Ljava/lang/String;)LX/9nX;

    move-result-object v0

    goto :goto_0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 1537432
    invoke-direct {p0}, LX/9nY;->c()V

    .line 1537433
    iget-object v0, p0, LX/9nY;->p:[F

    if-nez v0, :cond_0

    iget v0, p0, LX/9nY;->l:F

    invoke-static {v0}, LX/1mo;->a(F)Z

    move-result v0

    if-nez v0, :cond_2

    iget v0, p0, LX/9nY;->l:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 1537434
    :goto_0
    iget-object v1, p0, LX/9nY;->d:LX/9nX;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/9nY;->d:LX/9nX;

    sget-object v2, LX/9nX;->SOLID:LX/9nX;

    if-ne v1, v2, :cond_3

    :cond_1
    if-nez v0, :cond_3

    .line 1537435
    invoke-direct {p0, p1}, LX/9nY;->b(Landroid/graphics/Canvas;)V

    .line 1537436
    :goto_1
    return-void

    .line 1537437
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 1537438
    :cond_3
    invoke-direct {p0, p1}, LX/9nY;->a(Landroid/graphics/Canvas;)V

    goto :goto_1
.end method

.method public final getAlpha()I
    .locals 1

    .prologue
    .line 1537439
    iget v0, p0, LX/9nY;->o:I

    return v0
.end method

.method public final getOpacity()I
    .locals 2

    .prologue
    .line 1537440
    iget v0, p0, LX/9nY;->n:I

    iget v1, p0, LX/9nY;->o:I

    invoke-static {v0, v1}, LX/9nT;->a(II)I

    move-result v0

    invoke-static {v0}, LX/9nT;->a(I)I

    move-result v0

    return v0
.end method

.method public final getOutline(Landroid/graphics/Outline;)V
    .locals 2

    .prologue
    .line 1537441
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_0

    .line 1537442
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->getOutline(Landroid/graphics/Outline;)V

    .line 1537443
    :goto_0
    return-void

    .line 1537444
    :cond_0
    iget v0, p0, LX/9nY;->l:F

    invoke-static {v0}, LX/1mo;->a(F)Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, LX/9nY;->l:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_2

    :cond_1
    iget-object v0, p0, LX/9nY;->p:[F

    if-eqz v0, :cond_3

    .line 1537445
    :cond_2
    invoke-direct {p0}, LX/9nY;->b()V

    .line 1537446
    iget-object v0, p0, LX/9nY;->g:Landroid/graphics/Path;

    invoke-virtual {p1, v0}, Landroid/graphics/Outline;->setConvexPath(Landroid/graphics/Path;)V

    goto :goto_0

    .line 1537447
    :cond_3
    invoke-virtual {p0}, LX/9nY;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/Outline;->setRect(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public final onBoundsChange(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 1537448
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->onBoundsChange(Landroid/graphics/Rect;)V

    .line 1537449
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/9nY;->k:Z

    .line 1537450
    return-void
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 1537451
    iget v0, p0, LX/9nY;->o:I

    if-eq p1, v0, :cond_0

    .line 1537452
    iput p1, p0, LX/9nY;->o:I

    .line 1537453
    invoke-virtual {p0}, LX/9nY;->invalidateSelf()V

    .line 1537454
    :cond_0
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    .prologue
    .line 1537455
    return-void
.end method
