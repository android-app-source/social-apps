.class public final enum LX/ALZ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ALZ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ALZ;

.field public static final enum PHOTO:LX/ALZ;

.field public static final enum VIDEO:LX/ALZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1665019
    new-instance v0, LX/ALZ;

    const-string v1, "PHOTO"

    invoke-direct {v0, v1, v2}, LX/ALZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ALZ;->PHOTO:LX/ALZ;

    .line 1665020
    new-instance v0, LX/ALZ;

    const-string v1, "VIDEO"

    invoke-direct {v0, v1, v3}, LX/ALZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ALZ;->VIDEO:LX/ALZ;

    .line 1665021
    const/4 v0, 0x2

    new-array v0, v0, [LX/ALZ;

    sget-object v1, LX/ALZ;->PHOTO:LX/ALZ;

    aput-object v1, v0, v2

    sget-object v1, LX/ALZ;->VIDEO:LX/ALZ;

    aput-object v1, v0, v3

    sput-object v0, LX/ALZ;->$VALUES:[LX/ALZ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1665022
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ALZ;
    .locals 1

    .prologue
    .line 1665023
    const-class v0, LX/ALZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ALZ;

    return-object v0
.end method

.method public static values()[LX/ALZ;
    .locals 1

    .prologue
    .line 1665024
    sget-object v0, LX/ALZ;->$VALUES:[LX/ALZ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ALZ;

    return-object v0
.end method
