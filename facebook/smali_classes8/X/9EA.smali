.class public final LX/9EA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/model/GraphQLComment;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/9EB;


# direct methods
.method public constructor <init>(LX/9EB;)V
    .locals 0

    .prologue
    .line 1456551
    iput-object p1, p0, LX/9EA;->a:LX/9EB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1456552
    check-cast p1, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1456553
    iget-object v0, p0, LX/9EA;->a:LX/9EB;

    iget-object v0, v0, LX/9EB;->i:LX/20j;

    iget-object v1, p0, LX/9EA;->a:LX/9EB;

    iget-object v1, v1, LX/9EB;->j:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0, v1, p1}, LX/20j;->c(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 1456554
    iget-object v1, p0, LX/9EA;->a:LX/9EB;

    iget-object v1, v1, LX/9EB;->c:LX/0QK;

    invoke-interface {v1, v0}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1456555
    const/4 v0, 0x0

    return-object v0
.end method
