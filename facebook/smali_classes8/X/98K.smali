.class public final LX/98K;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 1445485
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1445486
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1445487
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1445488
    invoke-static {p0, p1}, LX/98K;->b(LX/15w;LX/186;)I

    move-result v1

    .line 1445489
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1445490
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1445491
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1445492
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1445493
    cmp-long v2, v0, v4

    if-eqz v2, :cond_0

    .line 1445494
    const-string v2, "end"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1445495
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1445496
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1445497
    cmp-long v2, v0, v4

    if-eqz v2, :cond_1

    .line 1445498
    const-string v2, "start"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1445499
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1445500
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1445501
    return-void
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1445502
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1445503
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1445504
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2}, LX/98K;->a(LX/15i;ILX/0nX;)V

    .line 1445505
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1445506
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1445507
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v7, 0x1

    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 1445508
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 1445509
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1445510
    :goto_0
    return v1

    .line 1445511
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_3

    .line 1445512
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1445513
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1445514
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_0

    if-eqz v10, :cond_0

    .line 1445515
    const-string v11, "end"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 1445516
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v0, v7

    goto :goto_1

    .line 1445517
    :cond_1
    const-string v11, "start"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 1445518
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v8

    move v6, v7

    goto :goto_1

    .line 1445519
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1445520
    :cond_3
    const/4 v10, 0x2

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1445521
    if-eqz v0, :cond_4

    move-object v0, p1

    .line 1445522
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1445523
    :cond_4
    if-eqz v6, :cond_5

    move-object v0, p1

    move v1, v7

    move-wide v2, v8

    .line 1445524
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1445525
    :cond_5
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v6, v1

    move v0, v1

    move-wide v8, v4

    move-wide v2, v4

    goto :goto_1
.end method
