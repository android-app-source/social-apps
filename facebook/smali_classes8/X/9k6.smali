.class public LX/9k6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/9k6;


# instance fields
.field private a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1530825
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1530826
    iput-object p1, p0, LX/9k6;->a:Landroid/content/Context;

    .line 1530827
    return-void
.end method

.method public static a(LX/0QB;)LX/9k6;
    .locals 4

    .prologue
    .line 1530828
    sget-object v0, LX/9k6;->b:LX/9k6;

    if-nez v0, :cond_1

    .line 1530829
    const-class v1, LX/9k6;

    monitor-enter v1

    .line 1530830
    :try_start_0
    sget-object v0, LX/9k6;->b:LX/9k6;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1530831
    if-eqz v2, :cond_0

    .line 1530832
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1530833
    new-instance p0, LX/9k6;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, LX/9k6;-><init>(Landroid/content/Context;)V

    .line 1530834
    move-object v0, p0

    .line 1530835
    sput-object v0, LX/9k6;->b:LX/9k6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1530836
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1530837
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1530838
    :cond_1
    sget-object v0, LX/9k6;->b:LX/9k6;

    return-object v0

    .line 1530839
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1530840
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final clearUserData()V
    .locals 2

    .prologue
    .line 1530841
    iget-object v0, p0, LX/9k6;->a:Landroid/content/Context;

    const-string v1, "places.db"

    invoke-virtual {v0, v1}, Landroid/content/Context;->deleteDatabase(Ljava/lang/String;)Z

    .line 1530842
    return-void
.end method
