.class public final enum LX/9XI;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/9X2;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9XI;",
        ">;",
        "LX/9X2;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9XI;

.field public static final enum EVENT_LIKE_RECENT_POST:LX/9XI;

.field public static final enum EVENT_LIKE_STORY:LX/9XI;

.field public static final enum EVENT_PLACE_DELETE_INLINE_RATING_TAPPED:LX/9XI;

.field public static final enum EVENT_PLACE_REPORT_CLAIMED_CLOSED:LX/9XI;

.field public static final enum EVENT_PLACE_REPORT_CLAIMED_OPEN:LX/9XI;

.field public static final enum EVENT_REVIEW_NEEDY_PLACE_CARD_TAPPED:LX/9XI;

.field public static final enum EVENT_TAPPED_ABOUT_BLURB:LX/9XI;

.field public static final enum EVENT_TAPPED_ADD_PHOTOS:LX/9XI;

.field public static final enum EVENT_TAPPED_ADD_VIDEO:LX/9XI;

.field public static final enum EVENT_TAPPED_ALBUM:LX/9XI;

.field public static final enum EVENT_TAPPED_ALL_NEARBY_LOCATIONS_LIST:LX/9XI;

.field public static final enum EVENT_TAPPED_ALL_NEARBY_LOCATIONS_MAP:LX/9XI;

.field public static final enum EVENT_TAPPED_ALL_PHOTOS_AT_PAGE:LX/9XI;

.field public static final enum EVENT_TAPPED_CALL:LX/9XI;

.field public static final enum EVENT_TAPPED_CALL_TO_ACTION:LX/9XI;

.field public static final enum EVENT_TAPPED_CHECKIN:LX/9XI;

.field public static final enum EVENT_TAPPED_CHILD_LOCATION:LX/9XI;

.field public static final enum EVENT_TAPPED_CITY_HUB_PYML_MODULE_CTA:LX/9XI;

.field public static final enum EVENT_TAPPED_CITY_HUB_PYML_MODULE_LIKE:LX/9XI;

.field public static final enum EVENT_TAPPED_CITY_HUB_PYML_MODULE_PAGE:LX/9XI;

.field public static final enum EVENT_TAPPED_CITY_HUB_SOCIAL_MODULE_CTA:LX/9XI;

.field public static final enum EVENT_TAPPED_CITY_HUB_SOCIAL_MODULE_FACEPILE:LX/9XI;

.field public static final enum EVENT_TAPPED_CITY_HUB_SOCIAL_MODULE_LIKE:LX/9XI;

.field public static final enum EVENT_TAPPED_CITY_HUB_SOCIAL_MODULE_MESSAGE_FRIEND:LX/9XI;

.field public static final enum EVENT_TAPPED_CITY_HUB_SOCIAL_MODULE_PAGE:LX/9XI;

.field public static final enum EVENT_TAPPED_COPY_LINK:LX/9XI;

.field public static final enum EVENT_TAPPED_COVER_PHOTO:LX/9XI;

.field public static final enum EVENT_TAPPED_CREATE_ALBUM:LX/9XI;

.field public static final enum EVENT_TAPPED_CREATE_PAGE:LX/9XI;

.field public static final enum EVENT_TAPPED_CREATE_SHORTCUT:LX/9XI;

.field public static final enum EVENT_TAPPED_FB_EVENT:LX/9XI;

.field public static final enum EVENT_TAPPED_FB_EVENT_GOING:LX/9XI;

.field public static final enum EVENT_TAPPED_FB_EVENT_NOT_GOING:LX/9XI;

.field public static final enum EVENT_TAPPED_FOLLOW:LX/9XI;

.field public static final enum EVENT_TAPPED_FOLLOW_SWITCHER:LX/9XI;

.field public static final enum EVENT_TAPPED_FOLLOW_SWITCHER_REGULAR:LX/9XI;

.field public static final enum EVENT_TAPPED_FOLLOW_SWITCHER_SEE_FIRST:LX/9XI;

.field public static final enum EVENT_TAPPED_FOLLOW_SWITCHER_UNFOLLOW:LX/9XI;

.field public static final enum EVENT_TAPPED_FRIENDS_HERE_NOW:LX/9XI;

.field public static final enum EVENT_TAPPED_FRIENDS_LIKERS_VISITORS:LX/9XI;

.field public static final enum EVENT_TAPPED_FRIEND_TIMELINE:LX/9XI;

.field public static final enum EVENT_TAPPED_GET_DIRECTION:LX/9XI;

.field public static final enum EVENT_TAPPED_GET_NOTIFICATION:LX/9XI;

.field public static final enum EVENT_TAPPED_GOTO_SAVE_COLLECTION:LX/9XI;

.field public static final enum EVENT_TAPPED_IMPRESSUM:LX/9XI;

.field public static final enum EVENT_TAPPED_INVITE_FRIENDS:LX/9XI;

.field public static final enum EVENT_TAPPED_INVITE_FRIEND_FROM_SOCIAL_CONTEXT_UNIT:LX/9XI;

.field public static final enum EVENT_TAPPED_INVITE_MORE_FRIENDS:LX/9XI;

.field public static final enum EVENT_TAPPED_LIKE:LX/9XI;

.field public static final enum EVENT_TAPPED_MANAGE_ADS:LX/9XI;

.field public static final enum EVENT_TAPPED_MESSAGE:LX/9XI;

.field public static final enum EVENT_TAPPED_MORE_ACTION_BAR:LX/9XI;

.field public static final enum EVENT_TAPPED_PAGE_PHOTOS:LX/9XI;

.field public static final enum EVENT_TAPPED_PHOTO:LX/9XI;

.field public static final enum EVENT_TAPPED_PHOTOS_AT_PAGE:LX/9XI;

.field public static final enum EVENT_TAPPED_PHOTOS_BY_PAGE:LX/9XI;

.field public static final enum EVENT_TAPPED_PHOTOS_OF_PAGE:LX/9XI;

.field public static final enum EVENT_TAPPED_PLACES_MAP:LX/9XI;

.field public static final enum EVENT_TAPPED_PLACE_CLAIM:LX/9XI;

.field public static final enum EVENT_TAPPED_POSTS_BY_OTHERS:LX/9XI;

.field public static final enum EVENT_TAPPED_PROFILE_PHOTO:LX/9XI;

.field public static final enum EVENT_TAPPED_PUBLIC_PHOTOS:LX/9XI;

.field public static final enum EVENT_TAPPED_REPORT:LX/9XI;

.field public static final enum EVENT_TAPPED_REPORT_PROBLEM:LX/9XI;

.field public static final enum EVENT_TAPPED_REQUEST_RIDE:LX/9XI;

.field public static final enum EVENT_TAPPED_REVIEWS_CONTEXT_ITEM:LX/9XI;

.field public static final enum EVENT_TAPPED_REVIEW_IN_ACTION_BAR:LX/9XI;

.field public static final enum EVENT_TAPPED_SAVE_PLACE:LX/9XI;

.field public static final enum EVENT_TAPPED_SCROLL_BACK_TO_TOP:LX/9XI;

.field public static final enum EVENT_TAPPED_SEE_FULL_MENU:LX/9XI;

.field public static final enum EVENT_TAPPED_SEE_MORE_INFORMATION:LX/9XI;

.field public static final enum EVENT_TAPPED_SERVICES_CARD_ITEM:LX/9XI;

.field public static final enum EVENT_TAPPED_SERVICES_CARD_SEE_ALL:LX/9XI;

.field public static final enum EVENT_TAPPED_SHARE_PAGE:LX/9XI;

.field public static final enum EVENT_TAPPED_SHARE_PAGE_AS_POST:LX/9XI;

.field public static final enum EVENT_TAPPED_SUGGEST_EDIT:LX/9XI;

.field public static final enum EVENT_TAPPED_SUGGEST_PHOTO:LX/9XI;

.field public static final enum EVENT_TAPPED_TIMELINE_FOOTER_CREATE_PAGE:LX/9XI;

.field public static final enum EVENT_TAPPED_UNFOLLOW:LX/9XI;

.field public static final enum EVENT_TAPPED_UNLIKE:LX/9XI;

.field public static final enum EVENT_TAPPED_UNSAVE_PLACE:LX/9XI;

.field public static final enum EVENT_TAPPED_VIDEO_HUB_ALL_VIDEOS:LX/9XI;

.field public static final enum EVENT_TAPPED_VIDEO_HUB_CARD_HEADER:LX/9XI;

.field public static final enum EVENT_TAPPED_VIDEO_HUB_PLAYLIST_HEADER:LX/9XI;

.field public static final enum EVENT_TAPPED_VIEW_TIMELINE:LX/9XI;

.field public static final enum EVENT_TAPPED_WIKIPEDIA_PAGE:LX/9XI;

.field public static final enum PAGE_EVENT_TAPPED_CONSUME_ACTION:LX/9XI;

.field public static final enum PAGE_EVENT_TAPPED_DEEPLINK_ACTION:LX/9XI;

.field public static final enum PAGE_EVENT_TAPPED_SAVE_MEDIA_PAGE:LX/9XI;

.field public static final enum PAGE_EVENT_TAPPED_UNSAVE_MEDIA_PAGE:LX/9XI;


# instance fields
.field private mEventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1502628
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_LIKE"

    const-string v2, "tapped_like_page"

    invoke-direct {v0, v1, v4, v2}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_LIKE:LX/9XI;

    .line 1502629
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_UNLIKE"

    const-string v2, "tapped_unlike_page"

    invoke-direct {v0, v1, v5, v2}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_UNLIKE:LX/9XI;

    .line 1502630
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_MORE_ACTION_BAR"

    const-string v2, "tapped_more_button"

    invoke-direct {v0, v1, v6, v2}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_MORE_ACTION_BAR:LX/9XI;

    .line 1502631
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_CALL"

    const-string v2, "tapped_call_button"

    invoke-direct {v0, v1, v7, v2}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_CALL:LX/9XI;

    .line 1502632
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_MESSAGE"

    const-string v2, "tapped_message_button"

    invoke-direct {v0, v1, v8, v2}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_MESSAGE:LX/9XI;

    .line 1502633
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_CHECKIN"

    const/4 v2, 0x5

    const-string v3, "tapped_check_in"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_CHECKIN:LX/9XI;

    .line 1502634
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_SHARE_PAGE"

    const/4 v2, 0x6

    const-string v3, "tapped_share_page"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_SHARE_PAGE:LX/9XI;

    .line 1502635
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_SHARE_PAGE_AS_POST"

    const/4 v2, 0x7

    const-string v3, "tapped_share_page_as_post"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_SHARE_PAGE_AS_POST:LX/9XI;

    .line 1502636
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_REPORT_PROBLEM"

    const/16 v2, 0x8

    const-string v3, "tapped_report_problem"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_REPORT_PROBLEM:LX/9XI;

    .line 1502637
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_SAVE_PLACE"

    const/16 v2, 0x9

    const-string v3, "tapped_save_place"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_SAVE_PLACE:LX/9XI;

    .line 1502638
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_UNSAVE_PLACE"

    const/16 v2, 0xa

    const-string v3, "tapped_unsave_place"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_UNSAVE_PLACE:LX/9XI;

    .line 1502639
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_GOTO_SAVE_COLLECTION"

    const/16 v2, 0xb

    const-string v3, "tapped_goto_save_collection"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_GOTO_SAVE_COLLECTION:LX/9XI;

    .line 1502640
    new-instance v0, LX/9XI;

    const-string v1, "PAGE_EVENT_TAPPED_SAVE_MEDIA_PAGE"

    const/16 v2, 0xc

    const-string v3, "tapped_save_media_page"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->PAGE_EVENT_TAPPED_SAVE_MEDIA_PAGE:LX/9XI;

    .line 1502641
    new-instance v0, LX/9XI;

    const-string v1, "PAGE_EVENT_TAPPED_UNSAVE_MEDIA_PAGE"

    const/16 v2, 0xd

    const-string v3, "tapped_unsave_media_page"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->PAGE_EVENT_TAPPED_UNSAVE_MEDIA_PAGE:LX/9XI;

    .line 1502642
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_COPY_LINK"

    const/16 v2, 0xe

    const-string v3, "tapped_copy_link"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_COPY_LINK:LX/9XI;

    .line 1502643
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_CREATE_SHORTCUT"

    const/16 v2, 0xf

    const-string v3, "tapped_create_shortcut"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_CREATE_SHORTCUT:LX/9XI;

    .line 1502644
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_MANAGE_ADS"

    const/16 v2, 0x10

    const-string v3, "tapped_manage_ads"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_MANAGE_ADS:LX/9XI;

    .line 1502645
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_FOLLOW"

    const/16 v2, 0x11

    const-string v3, "tapped_follow_button"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_FOLLOW:LX/9XI;

    .line 1502646
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_UNFOLLOW"

    const/16 v2, 0x12

    const-string v3, "tapped_unfollow_button"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_UNFOLLOW:LX/9XI;

    .line 1502647
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_FOLLOW_SWITCHER"

    const/16 v2, 0x13

    const-string v3, "tapped_follow_switcher_button"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_FOLLOW_SWITCHER:LX/9XI;

    .line 1502648
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_FOLLOW_SWITCHER_UNFOLLOW"

    const/16 v2, 0x14

    const-string v3, "tapped_follow_switcher_unfollow"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_FOLLOW_SWITCHER_UNFOLLOW:LX/9XI;

    .line 1502649
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_FOLLOW_SWITCHER_REGULAR"

    const/16 v2, 0x15

    const-string v3, "tapped_follow_switcher_regular"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_FOLLOW_SWITCHER_REGULAR:LX/9XI;

    .line 1502650
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_FOLLOW_SWITCHER_SEE_FIRST"

    const/16 v2, 0x16

    const-string v3, "tapped_follow_switcher_see_first"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_FOLLOW_SWITCHER_SEE_FIRST:LX/9XI;

    .line 1502651
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_SUGGEST_EDIT"

    const/16 v2, 0x17

    const-string v3, "tapped_suggest_edit"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_SUGGEST_EDIT:LX/9XI;

    .line 1502652
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_REPORT"

    const/16 v2, 0x18

    const-string v3, "tapped_report_button"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_REPORT:LX/9XI;

    .line 1502653
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_CREATE_PAGE"

    const/16 v2, 0x19

    const-string v3, "tapped_create_page_button"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_CREATE_PAGE:LX/9XI;

    .line 1502654
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_GET_NOTIFICATION"

    const/16 v2, 0x1a

    const-string v3, "tapped_notification_setting"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_GET_NOTIFICATION:LX/9XI;

    .line 1502655
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_PLACE_CLAIM"

    const/16 v2, 0x1b

    const-string v3, "tapped_place_claim"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_PLACE_CLAIM:LX/9XI;

    .line 1502656
    new-instance v0, LX/9XI;

    const-string v1, "PAGE_EVENT_TAPPED_CONSUME_ACTION"

    const/16 v2, 0x1c

    const-string v3, "tapped_consume_action"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->PAGE_EVENT_TAPPED_CONSUME_ACTION:LX/9XI;

    .line 1502657
    new-instance v0, LX/9XI;

    const-string v1, "PAGE_EVENT_TAPPED_DEEPLINK_ACTION"

    const/16 v2, 0x1d

    const-string v3, "tapped_deeplink_action"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->PAGE_EVENT_TAPPED_DEEPLINK_ACTION:LX/9XI;

    .line 1502658
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_GET_DIRECTION"

    const/16 v2, 0x1e

    const-string v3, "tapped_directions_button"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_GET_DIRECTION:LX/9XI;

    .line 1502659
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_REQUEST_RIDE"

    const/16 v2, 0x1f

    const-string v3, "tapped_request_ride_button"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_REQUEST_RIDE:LX/9XI;

    .line 1502660
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_CHILD_LOCATION"

    const/16 v2, 0x20

    const-string v3, "tapped_nearby_location"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_CHILD_LOCATION:LX/9XI;

    .line 1502661
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_ALL_NEARBY_LOCATIONS_LIST"

    const/16 v2, 0x21

    const-string v3, "tapped_all_nearby_locations_list"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_ALL_NEARBY_LOCATIONS_LIST:LX/9XI;

    .line 1502662
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_ALL_NEARBY_LOCATIONS_MAP"

    const/16 v2, 0x22

    const-string v3, "tapped_all_nearby_locations_map"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_ALL_NEARBY_LOCATIONS_MAP:LX/9XI;

    .line 1502663
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_PLACES_MAP"

    const/16 v2, 0x23

    const-string v3, "tapped_places_map"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_PLACES_MAP:LX/9XI;

    .line 1502664
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_SEE_FULL_MENU"

    const/16 v2, 0x24

    const-string v3, "tapped_see_full_menu"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_SEE_FULL_MENU:LX/9XI;

    .line 1502665
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_IMPRESSUM"

    const/16 v2, 0x25

    const-string v3, "tapped_impressum"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_IMPRESSUM:LX/9XI;

    .line 1502666
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_PLACE_REPORT_CLAIMED_CLOSED"

    const/16 v2, 0x26

    const-string v3, "place_report_claimed_closed"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_PLACE_REPORT_CLAIMED_CLOSED:LX/9XI;

    .line 1502667
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_PLACE_REPORT_CLAIMED_OPEN"

    const/16 v2, 0x27

    const-string v3, "place_report_claimed_open"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_PLACE_REPORT_CLAIMED_OPEN:LX/9XI;

    .line 1502668
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_REVIEW_NEEDY_PLACE_CARD_TAPPED"

    const/16 v2, 0x28

    const-string v3, "review_needy_place_card_tapped"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_REVIEW_NEEDY_PLACE_CARD_TAPPED:LX/9XI;

    .line 1502669
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_REVIEWS_CONTEXT_ITEM"

    const/16 v2, 0x29

    const-string v3, "tapped_reviews_context_item"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_REVIEWS_CONTEXT_ITEM:LX/9XI;

    .line 1502670
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_REVIEW_IN_ACTION_BAR"

    const/16 v2, 0x2a

    const-string v3, "tapped_review_in_action_bar"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_REVIEW_IN_ACTION_BAR:LX/9XI;

    .line 1502671
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_PHOTOS_BY_PAGE"

    const/16 v2, 0x2b

    const-string v3, "tapped_photos_by_page"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_PHOTOS_BY_PAGE:LX/9XI;

    .line 1502672
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_PHOTOS_OF_PAGE"

    const/16 v2, 0x2c

    const-string v3, "tapped_photos_at_page"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_PHOTOS_OF_PAGE:LX/9XI;

    .line 1502673
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_PHOTOS_AT_PAGE"

    const/16 v2, 0x2d

    const-string v3, "tapped_photos_of_page"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_PHOTOS_AT_PAGE:LX/9XI;

    .line 1502674
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_ALL_PHOTOS_AT_PAGE"

    const/16 v2, 0x2e

    const-string v3, "tapped_all_photos"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_ALL_PHOTOS_AT_PAGE:LX/9XI;

    .line 1502675
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_PAGE_PHOTOS"

    const/16 v2, 0x2f

    const-string v3, "tapped_page_photos"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_PAGE_PHOTOS:LX/9XI;

    .line 1502676
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_PUBLIC_PHOTOS"

    const/16 v2, 0x30

    const-string v3, "tapped_public_photos"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_PUBLIC_PHOTOS:LX/9XI;

    .line 1502677
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_CREATE_ALBUM"

    const/16 v2, 0x31

    const-string v3, "tapped_create_album"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_CREATE_ALBUM:LX/9XI;

    .line 1502678
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_ALBUM"

    const/16 v2, 0x32

    const-string v3, "tapped_album"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_ALBUM:LX/9XI;

    .line 1502679
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_ADD_PHOTOS"

    const/16 v2, 0x33

    const-string v3, "tapped_add_photos"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_ADD_PHOTOS:LX/9XI;

    .line 1502680
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_PHOTO"

    const/16 v2, 0x34

    const-string v3, "tapped_photo"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_PHOTO:LX/9XI;

    .line 1502681
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_FB_EVENT"

    const/16 v2, 0x35

    const-string v3, "tapped_event"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_FB_EVENT:LX/9XI;

    .line 1502682
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_FB_EVENT_GOING"

    const/16 v2, 0x36

    const-string v3, "tapped_event_going"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_FB_EVENT_GOING:LX/9XI;

    .line 1502683
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_FB_EVENT_NOT_GOING"

    const/16 v2, 0x37

    const-string v3, "tapped_event_not_going"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_FB_EVENT_NOT_GOING:LX/9XI;

    .line 1502684
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_INVITE_FRIENDS"

    const/16 v2, 0x38

    const-string v3, "tapped_invite_friends"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_INVITE_FRIENDS:LX/9XI;

    .line 1502685
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_INVITE_MORE_FRIENDS"

    const/16 v2, 0x39

    const-string v3, "tapped_invite_more_friends"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_INVITE_MORE_FRIENDS:LX/9XI;

    .line 1502686
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_INVITE_FRIEND_FROM_SOCIAL_CONTEXT_UNIT"

    const/16 v2, 0x3a

    const-string v3, "tapped_invite_friend_from_social_context_unit"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_INVITE_FRIEND_FROM_SOCIAL_CONTEXT_UNIT:LX/9XI;

    .line 1502687
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_FRIENDS_HERE_NOW"

    const/16 v2, 0x3b

    const-string v3, "tapped_friends_here_now"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_FRIENDS_HERE_NOW:LX/9XI;

    .line 1502688
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_FRIENDS_LIKERS_VISITORS"

    const/16 v2, 0x3c

    const-string v3, "tapped_friend_likers_or_visitors"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_FRIENDS_LIKERS_VISITORS:LX/9XI;

    .line 1502689
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_VIDEO_HUB_CARD_HEADER"

    const/16 v2, 0x3d

    const-string v3, "tapped_videohub_card_header"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_VIDEO_HUB_CARD_HEADER:LX/9XI;

    .line 1502690
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_VIDEO_HUB_PLAYLIST_HEADER"

    const/16 v2, 0x3e

    const-string v3, "tapped_videohub_playlist_header"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_VIDEO_HUB_PLAYLIST_HEADER:LX/9XI;

    .line 1502691
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_VIDEO_HUB_ALL_VIDEOS"

    const/16 v2, 0x3f

    const-string v3, "tapped_videohub_all_videos"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_VIDEO_HUB_ALL_VIDEOS:LX/9XI;

    .line 1502692
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_ADD_VIDEO"

    const/16 v2, 0x40

    const-string v3, "tapped_add_video"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_ADD_VIDEO:LX/9XI;

    .line 1502693
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_SERVICES_CARD_SEE_ALL"

    const/16 v2, 0x41

    const-string v3, "page_service_card_see_all"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_SERVICES_CARD_SEE_ALL:LX/9XI;

    .line 1502694
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_SERVICES_CARD_ITEM"

    const/16 v2, 0x42

    const-string v3, "page_service_card_item"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_SERVICES_CARD_ITEM:LX/9XI;

    .line 1502695
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_CITY_HUB_SOCIAL_MODULE_CTA"

    const/16 v2, 0x43

    const-string v3, "city_hub_social_module_tapped_cta"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_CITY_HUB_SOCIAL_MODULE_CTA:LX/9XI;

    .line 1502696
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_CITY_HUB_SOCIAL_MODULE_FACEPILE"

    const/16 v2, 0x44

    const-string v3, "city_hub_social_module_tapped_facepile"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_CITY_HUB_SOCIAL_MODULE_FACEPILE:LX/9XI;

    .line 1502697
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_CITY_HUB_SOCIAL_MODULE_LIKE"

    const/16 v2, 0x45

    const-string v3, "city_hub_social_module_tapped_like"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_CITY_HUB_SOCIAL_MODULE_LIKE:LX/9XI;

    .line 1502698
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_CITY_HUB_SOCIAL_MODULE_MESSAGE_FRIEND"

    const/16 v2, 0x46

    const-string v3, "city_hub_social_module_tapped_message_friend"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_CITY_HUB_SOCIAL_MODULE_MESSAGE_FRIEND:LX/9XI;

    .line 1502699
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_CITY_HUB_SOCIAL_MODULE_PAGE"

    const/16 v2, 0x47

    const-string v3, "city_hub_social_module_tapped_page"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_CITY_HUB_SOCIAL_MODULE_PAGE:LX/9XI;

    .line 1502700
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_CITY_HUB_PYML_MODULE_CTA"

    const/16 v2, 0x48

    const-string v3, "city_hub_pyml_module_tapped_cta"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_CITY_HUB_PYML_MODULE_CTA:LX/9XI;

    .line 1502701
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_CITY_HUB_PYML_MODULE_LIKE"

    const/16 v2, 0x49

    const-string v3, "city_hub_pyml_module_tapped_like"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_CITY_HUB_PYML_MODULE_LIKE:LX/9XI;

    .line 1502702
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_CITY_HUB_PYML_MODULE_PAGE"

    const/16 v2, 0x4a

    const-string v3, "city_hub_pyml_module_tapped_page"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_CITY_HUB_PYML_MODULE_PAGE:LX/9XI;

    .line 1502703
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_ABOUT_BLURB"

    const/16 v2, 0x4b

    const-string v3, "tapped_about_info_blurb"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_ABOUT_BLURB:LX/9XI;

    .line 1502704
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_COVER_PHOTO"

    const/16 v2, 0x4c

    const-string v3, "tapped_page_cover_photo"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_COVER_PHOTO:LX/9XI;

    .line 1502705
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_SUGGEST_PHOTO"

    const/16 v2, 0x4d

    const-string v3, "tapped_suggest_photo"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_SUGGEST_PHOTO:LX/9XI;

    .line 1502706
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_PROFILE_PHOTO"

    const/16 v2, 0x4e

    const-string v3, "tapped_page_profile_picture"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_PROFILE_PHOTO:LX/9XI;

    .line 1502707
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_CALL_TO_ACTION"

    const/16 v2, 0x4f

    const-string v3, "tapped_call_to_action"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_CALL_TO_ACTION:LX/9XI;

    .line 1502708
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_VIEW_TIMELINE"

    const/16 v2, 0x50

    const-string v3, "tapped_view_timeline_button"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_VIEW_TIMELINE:LX/9XI;

    .line 1502709
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_SEE_MORE_INFORMATION"

    const/16 v2, 0x51

    const-string v3, "tapped_page_about"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_SEE_MORE_INFORMATION:LX/9XI;

    .line 1502710
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_FRIEND_TIMELINE"

    const/16 v2, 0x52

    const-string v3, "tapped_friend_timeline"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_FRIEND_TIMELINE:LX/9XI;

    .line 1502711
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_WIKIPEDIA_PAGE"

    const/16 v2, 0x53

    const-string v3, "tapped_page_wikipedia_page"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_WIKIPEDIA_PAGE:LX/9XI;

    .line 1502712
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_POSTS_BY_OTHERS"

    const/16 v2, 0x54

    const-string v3, "tapped_posts_by_others"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_POSTS_BY_OTHERS:LX/9XI;

    .line 1502713
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_PLACE_DELETE_INLINE_RATING_TAPPED"

    const/16 v2, 0x55

    const-string v3, "place_delete_inline_rating_tapped"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_PLACE_DELETE_INLINE_RATING_TAPPED:LX/9XI;

    .line 1502714
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_LIKE_RECENT_POST"

    const/16 v2, 0x56

    const-string v3, "like_recent_post"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_LIKE_RECENT_POST:LX/9XI;

    .line 1502715
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_LIKE_STORY"

    const/16 v2, 0x57

    const-string v3, "like_story"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_LIKE_STORY:LX/9XI;

    .line 1502716
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_SCROLL_BACK_TO_TOP"

    const/16 v2, 0x58

    const-string v3, "page_event_scroll_back_to_top_tapped"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_SCROLL_BACK_TO_TOP:LX/9XI;

    .line 1502717
    new-instance v0, LX/9XI;

    const-string v1, "EVENT_TAPPED_TIMELINE_FOOTER_CREATE_PAGE"

    const/16 v2, 0x59

    const-string v3, "tapped_timeline_footer_create_page"

    invoke-direct {v0, v1, v2, v3}, LX/9XI;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XI;->EVENT_TAPPED_TIMELINE_FOOTER_CREATE_PAGE:LX/9XI;

    .line 1502718
    const/16 v0, 0x5a

    new-array v0, v0, [LX/9XI;

    sget-object v1, LX/9XI;->EVENT_TAPPED_LIKE:LX/9XI;

    aput-object v1, v0, v4

    sget-object v1, LX/9XI;->EVENT_TAPPED_UNLIKE:LX/9XI;

    aput-object v1, v0, v5

    sget-object v1, LX/9XI;->EVENT_TAPPED_MORE_ACTION_BAR:LX/9XI;

    aput-object v1, v0, v6

    sget-object v1, LX/9XI;->EVENT_TAPPED_CALL:LX/9XI;

    aput-object v1, v0, v7

    sget-object v1, LX/9XI;->EVENT_TAPPED_MESSAGE:LX/9XI;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/9XI;->EVENT_TAPPED_CHECKIN:LX/9XI;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/9XI;->EVENT_TAPPED_SHARE_PAGE:LX/9XI;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/9XI;->EVENT_TAPPED_SHARE_PAGE_AS_POST:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/9XI;->EVENT_TAPPED_REPORT_PROBLEM:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/9XI;->EVENT_TAPPED_SAVE_PLACE:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/9XI;->EVENT_TAPPED_UNSAVE_PLACE:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/9XI;->EVENT_TAPPED_GOTO_SAVE_COLLECTION:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/9XI;->PAGE_EVENT_TAPPED_SAVE_MEDIA_PAGE:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/9XI;->PAGE_EVENT_TAPPED_UNSAVE_MEDIA_PAGE:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/9XI;->EVENT_TAPPED_COPY_LINK:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/9XI;->EVENT_TAPPED_CREATE_SHORTCUT:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/9XI;->EVENT_TAPPED_MANAGE_ADS:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/9XI;->EVENT_TAPPED_FOLLOW:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/9XI;->EVENT_TAPPED_UNFOLLOW:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/9XI;->EVENT_TAPPED_FOLLOW_SWITCHER:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/9XI;->EVENT_TAPPED_FOLLOW_SWITCHER_UNFOLLOW:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/9XI;->EVENT_TAPPED_FOLLOW_SWITCHER_REGULAR:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/9XI;->EVENT_TAPPED_FOLLOW_SWITCHER_SEE_FIRST:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/9XI;->EVENT_TAPPED_SUGGEST_EDIT:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/9XI;->EVENT_TAPPED_REPORT:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/9XI;->EVENT_TAPPED_CREATE_PAGE:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/9XI;->EVENT_TAPPED_GET_NOTIFICATION:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/9XI;->EVENT_TAPPED_PLACE_CLAIM:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/9XI;->PAGE_EVENT_TAPPED_CONSUME_ACTION:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/9XI;->PAGE_EVENT_TAPPED_DEEPLINK_ACTION:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, LX/9XI;->EVENT_TAPPED_GET_DIRECTION:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, LX/9XI;->EVENT_TAPPED_REQUEST_RIDE:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, LX/9XI;->EVENT_TAPPED_CHILD_LOCATION:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, LX/9XI;->EVENT_TAPPED_ALL_NEARBY_LOCATIONS_LIST:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, LX/9XI;->EVENT_TAPPED_ALL_NEARBY_LOCATIONS_MAP:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, LX/9XI;->EVENT_TAPPED_PLACES_MAP:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, LX/9XI;->EVENT_TAPPED_SEE_FULL_MENU:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, LX/9XI;->EVENT_TAPPED_IMPRESSUM:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, LX/9XI;->EVENT_PLACE_REPORT_CLAIMED_CLOSED:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, LX/9XI;->EVENT_PLACE_REPORT_CLAIMED_OPEN:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, LX/9XI;->EVENT_REVIEW_NEEDY_PLACE_CARD_TAPPED:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, LX/9XI;->EVENT_TAPPED_REVIEWS_CONTEXT_ITEM:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, LX/9XI;->EVENT_TAPPED_REVIEW_IN_ACTION_BAR:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, LX/9XI;->EVENT_TAPPED_PHOTOS_BY_PAGE:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, LX/9XI;->EVENT_TAPPED_PHOTOS_OF_PAGE:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, LX/9XI;->EVENT_TAPPED_PHOTOS_AT_PAGE:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, LX/9XI;->EVENT_TAPPED_ALL_PHOTOS_AT_PAGE:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, LX/9XI;->EVENT_TAPPED_PAGE_PHOTOS:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, LX/9XI;->EVENT_TAPPED_PUBLIC_PHOTOS:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, LX/9XI;->EVENT_TAPPED_CREATE_ALBUM:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, LX/9XI;->EVENT_TAPPED_ALBUM:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, LX/9XI;->EVENT_TAPPED_ADD_PHOTOS:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, LX/9XI;->EVENT_TAPPED_PHOTO:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, LX/9XI;->EVENT_TAPPED_FB_EVENT:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, LX/9XI;->EVENT_TAPPED_FB_EVENT_GOING:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, LX/9XI;->EVENT_TAPPED_FB_EVENT_NOT_GOING:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, LX/9XI;->EVENT_TAPPED_INVITE_FRIENDS:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, LX/9XI;->EVENT_TAPPED_INVITE_MORE_FRIENDS:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, LX/9XI;->EVENT_TAPPED_INVITE_FRIEND_FROM_SOCIAL_CONTEXT_UNIT:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, LX/9XI;->EVENT_TAPPED_FRIENDS_HERE_NOW:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, LX/9XI;->EVENT_TAPPED_FRIENDS_LIKERS_VISITORS:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, LX/9XI;->EVENT_TAPPED_VIDEO_HUB_CARD_HEADER:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, LX/9XI;->EVENT_TAPPED_VIDEO_HUB_PLAYLIST_HEADER:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, LX/9XI;->EVENT_TAPPED_VIDEO_HUB_ALL_VIDEOS:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, LX/9XI;->EVENT_TAPPED_ADD_VIDEO:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, LX/9XI;->EVENT_TAPPED_SERVICES_CARD_SEE_ALL:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, LX/9XI;->EVENT_TAPPED_SERVICES_CARD_ITEM:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, LX/9XI;->EVENT_TAPPED_CITY_HUB_SOCIAL_MODULE_CTA:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, LX/9XI;->EVENT_TAPPED_CITY_HUB_SOCIAL_MODULE_FACEPILE:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, LX/9XI;->EVENT_TAPPED_CITY_HUB_SOCIAL_MODULE_LIKE:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, LX/9XI;->EVENT_TAPPED_CITY_HUB_SOCIAL_MODULE_MESSAGE_FRIEND:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, LX/9XI;->EVENT_TAPPED_CITY_HUB_SOCIAL_MODULE_PAGE:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, LX/9XI;->EVENT_TAPPED_CITY_HUB_PYML_MODULE_CTA:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, LX/9XI;->EVENT_TAPPED_CITY_HUB_PYML_MODULE_LIKE:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, LX/9XI;->EVENT_TAPPED_CITY_HUB_PYML_MODULE_PAGE:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    sget-object v2, LX/9XI;->EVENT_TAPPED_ABOUT_BLURB:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    sget-object v2, LX/9XI;->EVENT_TAPPED_COVER_PHOTO:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    sget-object v2, LX/9XI;->EVENT_TAPPED_SUGGEST_PHOTO:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    sget-object v2, LX/9XI;->EVENT_TAPPED_PROFILE_PHOTO:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    sget-object v2, LX/9XI;->EVENT_TAPPED_CALL_TO_ACTION:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x50

    sget-object v2, LX/9XI;->EVENT_TAPPED_VIEW_TIMELINE:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x51

    sget-object v2, LX/9XI;->EVENT_TAPPED_SEE_MORE_INFORMATION:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x52

    sget-object v2, LX/9XI;->EVENT_TAPPED_FRIEND_TIMELINE:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x53

    sget-object v2, LX/9XI;->EVENT_TAPPED_WIKIPEDIA_PAGE:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x54

    sget-object v2, LX/9XI;->EVENT_TAPPED_POSTS_BY_OTHERS:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x55

    sget-object v2, LX/9XI;->EVENT_PLACE_DELETE_INLINE_RATING_TAPPED:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x56

    sget-object v2, LX/9XI;->EVENT_LIKE_RECENT_POST:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x57

    sget-object v2, LX/9XI;->EVENT_LIKE_STORY:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x58

    sget-object v2, LX/9XI;->EVENT_TAPPED_SCROLL_BACK_TO_TOP:LX/9XI;

    aput-object v2, v0, v1

    const/16 v1, 0x59

    sget-object v2, LX/9XI;->EVENT_TAPPED_TIMELINE_FOOTER_CREATE_PAGE:LX/9XI;

    aput-object v2, v0, v1

    sput-object v0, LX/9XI;->$VALUES:[LX/9XI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1502719
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1502720
    iput-object p3, p0, LX/9XI;->mEventName:Ljava/lang/String;

    .line 1502721
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9XI;
    .locals 1

    .prologue
    .line 1502722
    const-class v0, LX/9XI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9XI;

    return-object v0
.end method

.method public static values()[LX/9XI;
    .locals 1

    .prologue
    .line 1502723
    sget-object v0, LX/9XI;->$VALUES:[LX/9XI;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9XI;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1502724
    iget-object v0, p0, LX/9XI;->mEventName:Ljava/lang/String;

    return-object v0
.end method

.method public final getType()LX/9XC;
    .locals 1

    .prologue
    .line 1502725
    sget-object v0, LX/9XC;->TAP:LX/9XC;

    return-object v0
.end method
