.class public LX/9hK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Pb;
.implements LX/1Pc;
.implements LX/1Pd;
.implements LX/1Pn;
.implements LX/1Po;
.implements LX/1Pp;
.implements LX/1Pq;
.implements LX/1Pk;
.implements LX/1Pr;
.implements LX/1Pt;
.implements LX/1Pu;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/photos/mediagallery/environment/MediaGalleryEnvironment;"
    }
.end annotation


# instance fields
.field private final a:LX/1Pb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Pb;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .prologue
    .line 1526453
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1526454
    iput-object p1, p0, LX/9hK;->a:LX/1Pb;

    .line 1526455
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;LX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;LX/5P5;)LX/5Oh;
    .locals 6
    .param p5    # LX/5P5;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1526452
    iget-object v0, p0, LX/9hK;->a:LX/1Pb;

    check-cast v0, LX/1Pc;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, LX/1Pc;->a(Ljava/lang/String;Ljava/lang/String;LX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;LX/5P5;)LX/5Oh;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1KL;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1KL",
            "<TK;TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 1526451
    iget-object v0, p0, LX/9hK;->a:LX/1Pb;

    check-cast v0, LX/1Pr;

    invoke-interface {v0, p1}, LX/1Pr;->a(LX/1KL;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1KL;LX/0jW;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1KL",
            "<TK;TT;>;",
            "LX/0jW;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 1526450
    iget-object v0, p0, LX/9hK;->a:LX/1Pb;

    check-cast v0, LX/1Pr;

    invoke-interface {v0, p1, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1Rb;)V
    .locals 1

    .prologue
    .line 1526448
    iget-object v0, p0, LX/9hK;->a:LX/1Pb;

    check-cast v0, LX/1Pu;

    invoke-interface {v0, p1}, LX/1Pu;->a(LX/1Rb;)V

    .line 1526449
    return-void
.end method

.method public final a(LX/1aZ;Ljava/lang/String;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 1526446
    iget-object v0, p0, LX/9hK;->a:LX/1Pb;

    check-cast v0, LX/1Pp;

    invoke-interface {v0, p1, p2, p3, p4}, LX/1Pp;->a(LX/1aZ;Ljava/lang/String;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1526447
    return-void
.end method

.method public final a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 1526444
    iget-object v0, p0, LX/9hK;->a:LX/1Pb;

    check-cast v0, LX/1Pt;

    invoke-interface {v0, p1, p2}, LX/1Pt;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1526445
    return-void
.end method

.method public final a(LX/1f9;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 1526442
    iget-object v0, p0, LX/9hK;->a:LX/1Pb;

    check-cast v0, LX/1Pt;

    invoke-interface {v0, p1, p2, p3}, LX/1Pt;->a(LX/1f9;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1526443
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1526440
    iget-object v0, p0, LX/9hK;->a:LX/1Pb;

    check-cast v0, LX/1Pd;

    invoke-interface {v0, p1, p2, p3, p4}, LX/1Pd;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1526441
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1526438
    iget-object v0, p0, LX/9hK;->a:LX/1Pb;

    check-cast v0, LX/1Pp;

    invoke-interface {v0, p1}, LX/1Pp;->a(Ljava/lang/String;)V

    .line 1526439
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1526436
    iget-object v0, p0, LX/9hK;->a:LX/1Pb;

    invoke-interface {v0, p1, p2}, LX/1Pb;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1526437
    return-void
.end method

.method public final varargs a([Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 1

    .prologue
    .line 1526434
    iget-object v0, p0, LX/9hK;->a:LX/1Pb;

    check-cast v0, LX/1Pq;

    invoke-interface {v0, p1}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1526435
    return-void
.end method

.method public final varargs a([Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1526456
    iget-object v0, p0, LX/9hK;->a:LX/1Pb;

    check-cast v0, LX/1Pq;

    invoke-interface {v0, p1}, LX/1Pq;->a([Ljava/lang/Object;)V

    .line 1526457
    return-void
.end method

.method public final a(LX/1KL;Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1KL",
            "<TK;TT;>;TT;)Z"
        }
    .end annotation

    .prologue
    .line 1526418
    iget-object v0, p0, LX/9hK;->a:LX/1Pb;

    check-cast v0, LX/1Pr;

    invoke-interface {v0, p1, p2}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1526420
    iget-object v0, p0, LX/9hK;->a:LX/1Pb;

    check-cast v0, LX/1Pr;

    invoke-interface {v0, p1}, LX/1Pr;->b(Ljava/lang/String;)V

    .line 1526421
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1526422
    iget-object v0, p0, LX/9hK;->a:LX/1Pb;

    invoke-interface {v0, p1, p2}, LX/1Pb;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1526423
    return-void
.end method

.method public final c()LX/1PT;
    .locals 1

    .prologue
    .line 1526424
    iget-object v0, p0, LX/9hK;->a:LX/1Pb;

    check-cast v0, LX/1Po;

    invoke-interface {v0}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/1SX;
    .locals 1

    .prologue
    .line 1526419
    iget-object v0, p0, LX/9hK;->a:LX/1Pb;

    check-cast v0, LX/1Pk;

    invoke-interface {v0}, LX/1Pk;->e()LX/1SX;

    move-result-object v0

    return-object v0
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1526425
    iget-object v0, p0, LX/9hK;->a:LX/1Pb;

    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public final iN_()V
    .locals 1

    .prologue
    .line 1526426
    iget-object v0, p0, LX/9hK;->a:LX/1Pb;

    check-cast v0, LX/1Pq;

    invoke-interface {v0}, LX/1Pq;->iN_()V

    .line 1526427
    return-void
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 1526428
    const/4 v0, 0x0

    return v0
.end method

.method public final m()LX/1f9;
    .locals 1

    .prologue
    .line 1526429
    iget-object v0, p0, LX/9hK;->a:LX/1Pb;

    check-cast v0, LX/1Pt;

    invoke-interface {v0}, LX/1Pt;->m()LX/1f9;

    move-result-object v0

    return-object v0
.end method

.method public final o()LX/1Rb;
    .locals 1

    .prologue
    .line 1526430
    iget-object v0, p0, LX/9hK;->a:LX/1Pb;

    check-cast v0, LX/1Pu;

    invoke-interface {v0}, LX/1Pu;->o()LX/1Rb;

    move-result-object v0

    return-object v0
.end method

.method public final p()V
    .locals 1

    .prologue
    .line 1526431
    iget-object v0, p0, LX/9hK;->a:LX/1Pb;

    check-cast v0, LX/1Pu;

    invoke-interface {v0}, LX/1Pu;->p()V

    .line 1526432
    return-void
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 1526433
    iget-object v0, p0, LX/9hK;->a:LX/1Pb;

    check-cast v0, LX/1Pu;

    invoke-interface {v0}, LX/1Pu;->q()Z

    move-result v0

    return v0
.end method
