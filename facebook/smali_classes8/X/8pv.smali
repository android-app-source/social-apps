.class public LX/8pv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8pe;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final a:LX/0Sh;

.field private final b:Ljava/util/concurrent/ExecutorService;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1My;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0rq;

.field private final f:LX/0sa;

.field private g:I


# direct methods
.method public constructor <init>(LX/0Sh;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/0Ot;LX/0rq;LX/0sa;)V
    .locals 1
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1My;",
            ">;",
            "LX/0rq;",
            "LX/0sa;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1406593
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1406594
    const/4 v0, 0x0

    iput v0, p0, LX/8pv;->g:I

    .line 1406595
    iput-object p1, p0, LX/8pv;->a:LX/0Sh;

    .line 1406596
    iput-object p2, p0, LX/8pv;->b:Ljava/util/concurrent/ExecutorService;

    .line 1406597
    iput-object p3, p0, LX/8pv;->c:LX/0Ot;

    .line 1406598
    iput-object p4, p0, LX/8pv;->d:LX/0Ot;

    .line 1406599
    iput-object p5, p0, LX/8pv;->e:LX/0rq;

    .line 1406600
    iput-object p6, p0, LX/8pv;->f:LX/0sa;

    .line 1406601
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)LX/0zO;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "LX/0zO",
            "<",
            "Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1406560
    new-instance v0, LX/6At;

    invoke-direct {v0}, LX/6At;-><init>()V

    move-object v0, v0

    .line 1406561
    const-string v1, "profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "profile_pic_media_type"

    iget-object v2, p0, LX/8pv;->e:LX/0rq;

    invoke-virtual {v2}, LX/0rq;->b()LX/0wF;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v0

    const-string v1, "feedback_id"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "max_seen_by"

    const-string v2, "25"

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "after_seen_by"

    invoke-virtual {v0, v1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/6At;

    .line 1406562
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/8pv;
    .locals 10

    .prologue
    .line 1406582
    const-class v1, LX/8pv;

    monitor-enter v1

    .line 1406583
    :try_start_0
    sget-object v0, LX/8pv;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1406584
    sput-object v2, LX/8pv;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1406585
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1406586
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1406587
    new-instance v3, LX/8pv;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    const/16 v6, 0xafd

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xafc

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v8

    check-cast v8, LX/0rq;

    invoke-static {v0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v9

    check-cast v9, LX/0sa;

    invoke-direct/range {v3 .. v9}, LX/8pv;-><init>(LX/0Sh;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/0Ot;LX/0rq;LX/0sa;)V

    .line 1406588
    move-object v0, v3

    .line 1406589
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1406590
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8pv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1406591
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1406592
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;",
            ">;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/44w",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLPageInfo;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1406581
    new-instance v0, LX/8pu;

    invoke-direct {v0, p0}, LX/8pu;-><init>(LX/8pv;)V

    iget-object v1, p0, LX/8pv;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {p1, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()LX/89l;
    .locals 1

    .prologue
    .line 1406602
    sget-object v0, LX/89l;->SEEN_BY_FOR_FEEDBACK_ID:LX/89l;

    return-object v0
.end method

.method public final a(Lcom/facebook/ufiservices/flyout/ProfileListParams;LX/0Vd;LX/0TF;ZLjava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ufiservices/flyout/ProfileListParams;",
            "LX/0Vd",
            "<",
            "LX/44w",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLPageInfo;",
            ">;>;",
            "LX/0TF",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;>;Z",
            "Ljava/lang/String;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1406571
    iget-object v0, p1, Lcom/facebook/ufiservices/flyout/ProfileListParams;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1406572
    invoke-direct {p0, v0, p5}, LX/8pv;->a(Ljava/lang/String;Ljava/lang/String;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->d:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const/4 v1, 0x1

    .line 1406573
    iput-boolean v1, v0, LX/0zO;->p:Z

    .line 1406574
    move-object v1, v0

    .line 1406575
    iget-object v0, p0, LX/8pv;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1My;

    new-instance v2, LX/8pt;

    invoke-direct {v2, p0, p3}, LX/8pt;-><init>(LX/8pv;LX/0TF;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1406576
    iget-object v4, v1, LX/0zO;->m:LX/0gW;

    move-object v4, v4

    .line 1406577
    iget-object v5, v4, LX/0gW;->f:Ljava/lang/String;

    move-object v4, v5

    .line 1406578
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, LX/8pv;->g:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, LX/8pv;->g:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/1My;->a(LX/0zO;LX/0TF;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1406579
    iget-object v1, p0, LX/8pv;->a:LX/0Sh;

    invoke-direct {p0, v0}, LX/8pv;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-virtual {v1, v0, p2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1406580
    return-void
.end method

.method public final a(Lcom/facebook/ufiservices/flyout/ProfileListParams;LX/0Vd;ZLjava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ufiservices/flyout/ProfileListParams;",
            "LX/0Vd",
            "<",
            "LX/44w",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLPageInfo;",
            ">;>;Z",
            "Ljava/lang/String;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1406566
    iget-object v0, p1, Lcom/facebook/ufiservices/flyout/ProfileListParams;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1406567
    invoke-direct {p0, v0, p4}, LX/8pv;->a(Ljava/lang/String;Ljava/lang/String;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    .line 1406568
    iget-object v0, p0, LX/8pv;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 1406569
    iget-object v1, p0, LX/8pv;->a:LX/0Sh;

    invoke-direct {p0, v0}, LX/8pv;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-virtual {v1, v0, p2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1406570
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1406565
    const/4 v0, 0x1

    return v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1406563
    iget-object v0, p0, LX/8pv;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1My;

    invoke-virtual {v0}, LX/1My;->a()V

    .line 1406564
    return-void
.end method
