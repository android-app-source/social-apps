.class public final LX/92x;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field public static final c:LX/0U1;

.field public static final d:LX/0U1;

.field public static final e:LX/0U1;

.field public static final f:LX/0U1;

.field public static final g:LX/0U1;

.field public static final h:LX/0U1;

.field public static final i:LX/0U1;

.field public static final j:LX/0U1;

.field public static final k:LX/0U1;

.field public static final l:LX/0U1;

.field public static final m:LX/0U1;

.field public static final n:LX/0U1;

.field public static final o:LX/0U1;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1433122
    new-instance v0, LX/0U1;

    const-string v1, "id"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/92x;->a:LX/0U1;

    .line 1433123
    new-instance v0, LX/0U1;

    const-string v1, "legacy_api_id"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/92x;->b:LX/0U1;

    .line 1433124
    new-instance v0, LX/0U1;

    const-string v1, "present_participle"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/92x;->c:LX/0U1;

    .line 1433125
    new-instance v0, LX/0U1;

    const-string v1, "prompt"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/92x;->d:LX/0U1;

    .line 1433126
    new-instance v0, LX/0U1;

    const-string v1, "is_linking_verb"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/92x;->e:LX/0U1;

    .line 1433127
    new-instance v0, LX/0U1;

    const-string v1, "supports_audio_suggestion"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/92x;->f:LX/0U1;

    .line 1433128
    new-instance v0, LX/0U1;

    const-string v1, "supports_free_form"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/92x;->g:LX/0U1;

    .line 1433129
    new-instance v0, LX/0U1;

    const-string v1, "supports_offline_posting"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/92x;->h:LX/0U1;

    .line 1433130
    new-instance v0, LX/0U1;

    const-string v1, "glyph_uri"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/92x;->i:LX/0U1;

    .line 1433131
    new-instance v0, LX/0U1;

    const-string v1, "icon_image_large"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/92x;->j:LX/0U1;

    .line 1433132
    new-instance v0, LX/0U1;

    const-string v1, "icon_count"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/92x;->k:LX/0U1;

    .line 1433133
    new-instance v0, LX/0U1;

    const-string v1, "position"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/92x;->l:LX/0U1;

    .line 1433134
    new-instance v0, LX/0U1;

    const-string v1, "cache_key"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/92x;->m:LX/0U1;

    .line 1433135
    new-instance v0, LX/0U1;

    const-string v1, "user_id"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/92x;->n:LX/0U1;

    .line 1433136
    new-instance v0, LX/0U1;

    const-string v1, "cache_time"

    const-string v2, "LONG"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/92x;->o:LX/0U1;

    return-void
.end method
