.class public final LX/960;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1L9;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/961;


# direct methods
.method public constructor <init>(LX/961;LX/1L9;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1438080
    iput-object p1, p0, LX/960;->c:LX/961;

    iput-object p2, p0, LX/960;->a:LX/1L9;

    iput-object p3, p0, LX/960;->b:Ljava/lang/String;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1438074
    iget-object v0, p0, LX/960;->a:LX/1L9;

    if-eqz v0, :cond_0

    .line 1438075
    iget-object v0, p0, LX/960;->a:LX/1L9;

    iget-object v1, p0, LX/960;->b:Ljava/lang/String;

    invoke-static {p1}, Lcom/facebook/fbservice/service/ServiceException;->forException(Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/ServiceException;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/1L9;->a(Ljava/lang/Object;Lcom/facebook/fbservice/service/ServiceException;)V

    .line 1438076
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1438077
    iget-object v0, p0, LX/960;->a:LX/1L9;

    if-eqz v0, :cond_0

    .line 1438078
    iget-object v0, p0, LX/960;->a:LX/1L9;

    iget-object v1, p0, LX/960;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/1L9;->b(Ljava/lang/Object;)V

    .line 1438079
    :cond_0
    return-void
.end method
