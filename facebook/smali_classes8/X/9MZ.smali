.class public LX/9MZ;
.super LX/0rn;
.source ""


# instance fields
.field private final d:LX/0w9;

.field private final e:LX/0sa;

.field private final f:LX/0tG;

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0wp;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0tI;

.field private final i:LX/A5q;

.field private final j:LX/0sU;

.field private final k:LX/0sX;

.field public final l:LX/0ad;

.field private final m:LX/1BY;

.field private final n:LX/0tQ;

.field private final o:Z


# direct methods
.method public constructor <init>(LX/0w9;LX/0tG;LX/0Or;LX/0tI;LX/A5q;LX/0sO;LX/03V;LX/0Yl;LX/0SG;LX/0So;LX/0sa;LX/0ad;LX/0sU;LX/0sX;LX/0sZ;LX/1BY;LX/0tQ;Ljava/lang/Boolean;)V
    .locals 9
    .param p18    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0w9;",
            "LX/0tG;",
            "LX/0Or",
            "<",
            "LX/0wp;",
            ">;",
            "LX/0tI;",
            "LX/A5q;",
            "LX/0sO;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Yl;",
            "LX/0SG;",
            "LX/0So;",
            "LX/0sa;",
            "LX/0ad;",
            "LX/0sU;",
            "LX/0sX;",
            "LX/0sZ;",
            "LX/1BY;",
            "LX/0tQ;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1474487
    move-object v1, p0

    move-object v2, p6

    move-object/from16 v3, p7

    move-object/from16 v4, p8

    move-object/from16 v5, p9

    move-object/from16 v6, p10

    move-object/from16 v7, p12

    move-object/from16 v8, p15

    invoke-direct/range {v1 .. v8}, LX/0rn;-><init>(LX/0sO;LX/03V;LX/0Yl;LX/0SG;LX/0So;LX/0ad;LX/0sZ;)V

    .line 1474488
    iput-object p1, p0, LX/9MZ;->d:LX/0w9;

    .line 1474489
    iput-object p2, p0, LX/9MZ;->f:LX/0tG;

    .line 1474490
    iput-object p3, p0, LX/9MZ;->g:LX/0Or;

    .line 1474491
    iput-object p4, p0, LX/9MZ;->h:LX/0tI;

    .line 1474492
    iput-object p5, p0, LX/9MZ;->i:LX/A5q;

    .line 1474493
    move-object/from16 v0, p11

    iput-object v0, p0, LX/9MZ;->e:LX/0sa;

    .line 1474494
    move-object/from16 v0, p13

    iput-object v0, p0, LX/9MZ;->j:LX/0sU;

    .line 1474495
    move-object/from16 v0, p14

    iput-object v0, p0, LX/9MZ;->k:LX/0sX;

    .line 1474496
    move-object/from16 v0, p12

    iput-object v0, p0, LX/9MZ;->l:LX/0ad;

    .line 1474497
    move-object/from16 v0, p16

    iput-object v0, p0, LX/9MZ;->m:LX/1BY;

    .line 1474498
    move-object/from16 v0, p17

    iput-object v0, p0, LX/9MZ;->n:LX/0tQ;

    .line 1474499
    invoke-virtual/range {p18 .. p18}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, LX/9MZ;->o:Z

    .line 1474500
    return-void
.end method

.method public static b(LX/0QB;)LX/9MZ;
    .locals 20

    .prologue
    .line 1474462
    new-instance v1, LX/9MZ;

    invoke-static/range {p0 .. p0}, LX/0w9;->a(LX/0QB;)LX/0w9;

    move-result-object v2

    check-cast v2, LX/0w9;

    invoke-static/range {p0 .. p0}, LX/0tG;->a(LX/0QB;)LX/0tG;

    move-result-object v3

    check-cast v3, LX/0tG;

    const/16 v4, 0x12e4

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static/range {p0 .. p0}, LX/0tI;->a(LX/0QB;)LX/0tI;

    move-result-object v5

    check-cast v5, LX/0tI;

    invoke-static/range {p0 .. p0}, LX/A5q;->a(LX/0QB;)LX/A5q;

    move-result-object v6

    check-cast v6, LX/A5q;

    invoke-static/range {p0 .. p0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v7

    check-cast v7, LX/0sO;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-static/range {p0 .. p0}, LX/0Yl;->a(LX/0QB;)LX/0Yl;

    move-result-object v9

    check-cast v9, LX/0Yl;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v10

    check-cast v10, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v11

    check-cast v11, LX/0So;

    invoke-static/range {p0 .. p0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v12

    check-cast v12, LX/0sa;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v13

    check-cast v13, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/0sU;->a(LX/0QB;)LX/0sU;

    move-result-object v14

    check-cast v14, LX/0sU;

    invoke-static/range {p0 .. p0}, LX/0sX;->a(LX/0QB;)LX/0sX;

    move-result-object v15

    check-cast v15, LX/0sX;

    invoke-static/range {p0 .. p0}, LX/0sZ;->a(LX/0QB;)LX/0sZ;

    move-result-object v16

    check-cast v16, LX/0sZ;

    invoke-static/range {p0 .. p0}, LX/1BY;->a(LX/0QB;)LX/1BY;

    move-result-object v17

    check-cast v17, LX/1BY;

    invoke-static/range {p0 .. p0}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v18

    check-cast v18, LX/0tQ;

    invoke-static/range {p0 .. p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v19

    check-cast v19, Ljava/lang/Boolean;

    invoke-direct/range {v1 .. v19}, LX/9MZ;-><init>(LX/0w9;LX/0tG;LX/0Or;LX/0tI;LX/A5q;LX/0sO;LX/03V;LX/0Yl;LX/0SG;LX/0So;LX/0sa;LX/0ad;LX/0sU;LX/0sX;LX/0sZ;LX/1BY;LX/0tQ;Ljava/lang/Boolean;)V

    .line 1474463
    return-object v1
.end method

.method private static g(Lcom/facebook/api/feed/FetchFeedParams;)Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;
    .locals 1

    .prologue
    .line 1474484
    iget-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v0, v0

    .line 1474485
    iget-object p0, v0, Lcom/facebook/api/feedtype/FeedType;->e:Ljava/lang/Object;

    move-object v0, p0

    .line 1474486
    check-cast v0, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/api/feed/FetchFeedParams;LX/15w;)Lcom/facebook/graphql/model/GraphQLFeedHomeStories;
    .locals 3

    .prologue
    .line 1474470
    invoke-super {p0, p1, p2}, LX/0rn;->a(Lcom/facebook/api/feed/FetchFeedParams;LX/15w;)Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-result-object v0

    .line 1474471
    new-instance v1, LX/0uq;

    invoke-direct {v1}, LX/0uq;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k()LX/0Px;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/0rn;->a(LX/0Px;)LX/0Px;

    move-result-object v2

    .line 1474472
    iput-object v2, v1, LX/0uq;->d:LX/0Px;

    .line 1474473
    move-object v1, v1

    .line 1474474
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->n()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    .line 1474475
    iput-object v2, v1, LX/0uq;->g:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 1474476
    move-object v1, v1

    .line 1474477
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->j()Ljava/lang/String;

    move-result-object v2

    .line 1474478
    iput-object v2, v1, LX/0uq;->c:Ljava/lang/String;

    .line 1474479
    move-object v1, v1

    .line 1474480
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->a()I

    move-result v0

    .line 1474481
    iput v0, v1, LX/0uq;->b:I

    .line 1474482
    move-object v0, v1

    .line 1474483
    invoke-virtual {v0}, LX/0uq;->a()Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/api/feed/FetchFeedParams;Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/graphql/model/GraphQLFeedHomeStories;
    .locals 1

    .prologue
    .line 1474467
    invoke-super {p0, p1, p2}, LX/0rn;->a(Lcom/facebook/api/feed/FetchFeedParams;Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-result-object v0

    .line 1474468
    invoke-virtual {p0, v0}, LX/0rn;->a(Lcom/facebook/graphql/model/GraphQLFeedHomeStories;)V

    .line 1474469
    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1474464
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    .line 1474465
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    invoke-interface {v1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0

    .line 1474466
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 1474501
    check-cast p1, Lcom/facebook/api/feed/FetchFeedParams;

    .line 1474502
    invoke-static {p1}, LX/9MZ;->g(Lcom/facebook/api/feed/FetchFeedParams;)Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;

    move-result-object v0

    .line 1474503
    iget p0, v0, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->j:I

    move v0, p0

    .line 1474504
    return v0
.end method

.method public final b(Lcom/facebook/api/feed/FetchFeedParams;)V
    .locals 4

    .prologue
    .line 1474317
    invoke-super {p0, p1}, LX/0rn;->b(Lcom/facebook/api/feed/FetchFeedParams;)V

    .line 1474318
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->i:LX/0Px;

    move-object v0, v0

    .line 1474319
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1474320
    iget-object v1, p0, LX/9MZ;->m:LX/1BY;

    .line 1474321
    if-eqz v0, :cond_0

    .line 1474322
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result p0

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, p0, :cond_0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1474323
    iget-object p1, v1, LX/1BY;->a:LX/0re;

    invoke-virtual {p1, v2}, LX/0re;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1474324
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 1474325
    :cond_0
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1474326
    const-string v0, "fetch_group_feed"

    return-object v0
.end method

.method public final c(Lcom/facebook/api/feed/FetchFeedParams;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1474327
    const-string v0, "GroupsFeedNetworkTime"

    return-object v0
.end method

.method public final synthetic c(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1474328
    check-cast p1, Lcom/facebook/api/feed/FetchFeedParams;

    invoke-virtual {p0, p1}, LX/0rn;->b(Lcom/facebook/api/feed/FetchFeedParams;)V

    return-void
.end method

.method public final d(Lcom/facebook/api/feed/FetchFeedParams;)I
    .locals 1

    .prologue
    .line 1474329
    const v0, 0xa0082

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 6

    .prologue
    .line 1474330
    check-cast p1, Lcom/facebook/api/feed/FetchFeedParams;

    .line 1474331
    invoke-static {p1}, LX/9MZ;->g(Lcom/facebook/api/feed/FetchFeedParams;)Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;

    move-result-object v0

    const/4 v3, 0x1

    .line 1474332
    sget-object v1, LX/B1R;->a:[I

    iget-object v2, v0, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->b:LX/B1T;

    invoke-virtual {v2}, LX/B1T;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1474333
    new-instance v1, LX/9MW;

    invoke-direct {v1}, LX/9MW;-><init>()V

    move-object v1, v1

    .line 1474334
    iput-boolean v3, v1, LX/0gW;->l:Z

    .line 1474335
    move-object v1, v1

    .line 1474336
    :goto_0
    move-object v0, v1

    .line 1474337
    invoke-static {v0}, LX/0w9;->a(LX/0gW;)LX/0gW;

    .line 1474338
    iget-object v1, p0, LX/9MZ;->d:LX/0w9;

    invoke-virtual {v1, v0}, LX/0w9;->b(LX/0gW;)LX/0gW;

    .line 1474339
    iget-object v1, p0, LX/9MZ;->d:LX/0w9;

    invoke-virtual {v1, v0}, LX/0w9;->c(LX/0gW;)LX/0gW;

    .line 1474340
    invoke-static {v0}, LX/0w9;->d(LX/0gW;)LX/0gW;

    .line 1474341
    iget-object v1, p0, LX/9MZ;->f:LX/0tG;

    invoke-virtual {v1, v0}, LX/0tG;->a(LX/0gW;)V

    .line 1474342
    iget-object v1, p0, LX/9MZ;->h:LX/0tI;

    invoke-virtual {v1, v0}, LX/0tI;->a(LX/0gW;)V

    .line 1474343
    const-string v1, "action_location"

    sget-object v2, LX/0wD;->GROUP:LX/0wD;

    invoke-virtual {v2}, LX/0wD;->stringValueOf()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "gysc_member_profile_size"

    invoke-static {}, LX/0sa;->q()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "gysj_facepile_count_param"

    const-string v3, "4"

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "gysj_facepile_size_param"

    invoke-static {}, LX/0sa;->p()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "gysj_cover_photo_width_param"

    invoke-static {}, LX/0sa;->p()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    mul-int/lit8 v3, v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "gpymi_size_param"

    iget-object v3, p0, LX/9MZ;->e:LX/0sa;

    invoke-virtual {v3}, LX/0sa;->h()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "automatic_photo_captioning_enabled"

    iget-object v3, p0, LX/9MZ;->k:LX/0sX;

    invoke-virtual {v3}, LX/0sX;->a()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "enable_download"

    iget-object v3, p0, LX/9MZ;->n:LX/0tQ;

    invoke-virtual {v3}, LX/0tQ;->c()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "seen_by_count"

    const/4 v3, 0x4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "show_profiles_for_seen"

    const/4 v3, 0x0

    .line 1474344
    iget-object v4, p0, LX/9MZ;->l:LX/0ad;

    sget-short v5, LX/9Lj;->d:S

    invoke-interface {v4, v5, v3}, LX/0ad;->a(SZ)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, LX/9MZ;->l:LX/0ad;

    sget-short v5, LX/88j;->a:S

    invoke-interface {v4, v5, v3}, LX/0ad;->a(SZ)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    const/4 v3, 0x1

    :cond_1
    move v3, v3

    .line 1474345
    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1474346
    if-eqz p1, :cond_8

    .line 1474347
    const-string v1, "group_id"

    invoke-static {p1}, LX/9MZ;->g(Lcom/facebook/api/feed/FetchFeedParams;)Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;

    move-result-object v2

    .line 1474348
    iget-object v3, v2, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->a:Ljava/lang/String;

    move-object v2, v3

    .line 1474349
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "first"

    .line 1474350
    iget v3, p1, Lcom/facebook/api/feed/FetchFeedParams;->c:I

    move v3, v3

    .line 1474351
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1474352
    sget-object v1, Lcom/facebook/api/feedtype/FeedType$Name;->q:Lcom/facebook/api/feedtype/FeedType$Name;

    .line 1474353
    iget-object v2, p1, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v2, v2

    .line 1474354
    iget-object v3, v2, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v2, v3

    .line 1474355
    invoke-virtual {v1, v2}, Lcom/facebook/api/feedtype/FeedType$Name;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1474356
    const-string v1, "feed_story_render_location"

    const-string v2, "group_bio"

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1474357
    :cond_2
    invoke-static {p1}, LX/9MZ;->g(Lcom/facebook/api/feed/FetchFeedParams;)Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->g()LX/0Px;

    move-result-object v1

    .line 1474358
    if-eqz v1, :cond_3

    .line 1474359
    const-string v2, "story_ids"

    invoke-virtual {v0, v2, v1}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 1474360
    :cond_3
    invoke-static {p1}, LX/9MZ;->g(Lcom/facebook/api/feed/FetchFeedParams;)Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;

    move-result-object v1

    .line 1474361
    iget-object v2, v1, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->e:Ljava/util/List;

    if-eqz v2, :cond_e

    .line 1474362
    iget-object v2, v1, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->e:Ljava/util/List;

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    .line 1474363
    :goto_1
    move-object v1, v2

    .line 1474364
    if-eqz v1, :cond_4

    .line 1474365
    const-string v2, "comment_ids"

    invoke-virtual {v0, v2, v1}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 1474366
    :cond_4
    invoke-static {p1}, LX/9MZ;->g(Lcom/facebook/api/feed/FetchFeedParams;)Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;

    move-result-object v1

    .line 1474367
    iget-object v2, v1, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->f:Ljava/lang/String;

    move-object v1, v2

    .line 1474368
    if-eqz v1, :cond_5

    .line 1474369
    const-string v2, "topic_id"

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1474370
    :cond_5
    invoke-static {p1}, LX/9MZ;->g(Lcom/facebook/api/feed/FetchFeedParams;)Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;

    move-result-object v1

    .line 1474371
    iget-object v2, v1, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->g:Ljava/lang/String;

    move-object v1, v2

    .line 1474372
    if-eqz v1, :cond_6

    .line 1474373
    const-string v2, "member_id"

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1474374
    :cond_6
    invoke-static {p1}, LX/9MZ;->g(Lcom/facebook/api/feed/FetchFeedParams;)Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;

    move-result-object v1

    .line 1474375
    iget-object v2, v1, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->h:Ljava/lang/String;

    move-object v1, v2

    .line 1474376
    if-eqz v1, :cond_7

    .line 1474377
    const-string v2, "unit_id"

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1474378
    :cond_7
    invoke-static {p1}, LX/9MZ;->g(Lcom/facebook/api/feed/FetchFeedParams;)Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;

    move-result-object v1

    .line 1474379
    iget-object v2, v1, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->i:Ljava/lang/String;

    move-object v1, v2

    .line 1474380
    if-eqz v1, :cond_8

    .line 1474381
    const-string v2, "story_id"

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1474382
    :cond_8
    if-eqz p1, :cond_d

    .line 1474383
    iget-object v1, p1, Lcom/facebook/api/feed/FetchFeedParams;->f:LX/0gf;

    move-object v1, v1

    .line 1474384
    sget-object v2, LX/0gf;->INITIALIZATION:LX/0gf;

    if-ne v1, v2, :cond_d

    invoke-static {p1}, LX/9MZ;->g(Lcom/facebook/api/feed/FetchFeedParams;)Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->g()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_d

    .line 1474385
    iget-object v1, p1, Lcom/facebook/api/feed/FetchFeedParams;->e:Ljava/lang/String;

    move-object v1, v1

    .line 1474386
    if-eqz v1, :cond_9

    .line 1474387
    const-string v1, "after"

    .line 1474388
    iget-object v2, p1, Lcom/facebook/api/feed/FetchFeedParams;->e:Ljava/lang/String;

    move-object v2, v2

    .line 1474389
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1474390
    :cond_9
    :goto_2
    iget-object v1, p0, LX/9MZ;->g:LX/0Or;

    if-eqz v1, :cond_a

    iget-object v1, p0, LX/9MZ;->g:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    .line 1474391
    const/4 v1, 0x1

    move v1, v1

    .line 1474392
    if-eqz v1, :cond_a

    .line 1474393
    const-string v1, "scrubbing"

    const-string v2, "MPEG_DASH"

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1474394
    :cond_a
    iget-object v1, p1, Lcom/facebook/api/feed/FetchFeedParams;->i:LX/0Px;

    move-object v1, v1

    .line 1474395
    if-eqz v1, :cond_b

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_b

    .line 1474396
    const-string v2, "recent_vpvs"

    invoke-virtual {v0, v2, v1}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 1474397
    :cond_b
    iget-boolean v1, p0, LX/9MZ;->o:Z

    if-eqz v1, :cond_c

    .line 1474398
    const-string v1, "is_work_build"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 1474399
    :cond_c
    iget-object v1, p0, LX/9MZ;->j:LX/0sU;

    const-string v2, "GROUP"

    invoke-virtual {v1, v0, v2}, LX/0sU;->a(LX/0gW;Ljava/lang/String;)V

    .line 1474400
    return-object v0

    .line 1474401
    :cond_d
    const-string v1, "before"

    const-string v2, "after"

    invoke-static {v0, p1, v1, v2}, LX/0w9;->a(LX/0gW;Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    goto :goto_2

    .line 1474402
    :pswitch_0
    new-instance v1, LX/9RH;

    invoke-direct {v1}, LX/9RH;-><init>()V

    move-object v1, v1

    .line 1474403
    iput-boolean v3, v1, LX/0gW;->l:Z

    .line 1474404
    move-object v1, v1

    .line 1474405
    goto/16 :goto_0

    .line 1474406
    :pswitch_1
    new-instance v1, LX/9RF;

    invoke-direct {v1}, LX/9RF;-><init>()V

    move-object v1, v1

    .line 1474407
    iput-boolean v3, v1, LX/0gW;->l:Z

    .line 1474408
    move-object v1, v1

    .line 1474409
    goto/16 :goto_0

    .line 1474410
    :pswitch_2
    new-instance v1, LX/9RJ;

    invoke-direct {v1}, LX/9RJ;-><init>()V

    move-object v1, v1

    .line 1474411
    iput-boolean v3, v1, LX/0gW;->l:Z

    .line 1474412
    move-object v1, v1

    .line 1474413
    goto/16 :goto_0

    .line 1474414
    :pswitch_3
    new-instance v1, LX/9RB;

    invoke-direct {v1}, LX/9RB;-><init>()V

    move-object v1, v1

    .line 1474415
    iput-boolean v3, v1, LX/0gW;->l:Z

    .line 1474416
    move-object v1, v1

    .line 1474417
    goto/16 :goto_0

    .line 1474418
    :pswitch_4
    new-instance v1, LX/9Ls;

    invoke-direct {v1}, LX/9Ls;-><init>()V

    move-object v1, v1

    .line 1474419
    iput-boolean v3, v1, LX/0gW;->l:Z

    .line 1474420
    move-object v1, v1

    .line 1474421
    goto/16 :goto_0

    .line 1474422
    :pswitch_5
    new-instance v1, LX/9RD;

    invoke-direct {v1}, LX/9RD;-><init>()V

    move-object v1, v1

    .line 1474423
    iput-boolean v3, v1, LX/0gW;->l:Z

    .line 1474424
    move-object v1, v1

    .line 1474425
    goto/16 :goto_0

    .line 1474426
    :pswitch_6
    new-instance v1, LX/9RC;

    invoke-direct {v1}, LX/9RC;-><init>()V

    move-object v1, v1

    .line 1474427
    iput-boolean v3, v1, LX/0gW;->l:Z

    .line 1474428
    move-object v1, v1

    .line 1474429
    goto/16 :goto_0

    .line 1474430
    :pswitch_7
    new-instance v1, LX/9MX;

    invoke-direct {v1}, LX/9MX;-><init>()V

    move-object v1, v1

    .line 1474431
    iput-boolean v3, v1, LX/0gW;->l:Z

    .line 1474432
    move-object v1, v1

    .line 1474433
    goto/16 :goto_0

    .line 1474434
    :pswitch_8
    new-instance v1, LX/9M4;

    invoke-direct {v1}, LX/9M4;-><init>()V

    move-object v1, v1

    .line 1474435
    iput-boolean v3, v1, LX/0gW;->l:Z

    .line 1474436
    move-object v1, v1

    .line 1474437
    goto/16 :goto_0

    .line 1474438
    :pswitch_9
    new-instance v1, LX/9T5;

    invoke-direct {v1}, LX/9T5;-><init>()V

    move-object v1, v1

    .line 1474439
    iput-boolean v3, v1, LX/0gW;->l:Z

    .line 1474440
    move-object v1, v1

    .line 1474441
    goto/16 :goto_0

    .line 1474442
    :pswitch_a
    new-instance v1, LX/9MU;

    invoke-direct {v1}, LX/9MU;-><init>()V

    move-object v1, v1

    .line 1474443
    iput-boolean v3, v1, LX/0gW;->l:Z

    .line 1474444
    move-object v1, v1

    .line 1474445
    goto/16 :goto_0

    .line 1474446
    :pswitch_b
    new-instance v1, LX/9R9;

    invoke-direct {v1}, LX/9R9;-><init>()V

    move-object v1, v1

    .line 1474447
    iput-boolean v3, v1, LX/0gW;->l:Z

    .line 1474448
    move-object v1, v1

    .line 1474449
    goto/16 :goto_0

    .line 1474450
    :pswitch_c
    new-instance v1, LX/9Qu;

    invoke-direct {v1}, LX/9Qu;-><init>()V

    move-object v1, v1

    .line 1474451
    iput-boolean v3, v1, LX/0gW;->l:Z

    .line 1474452
    move-object v1, v1

    .line 1474453
    goto/16 :goto_0

    .line 1474454
    :pswitch_d
    new-instance v1, LX/9RL;

    invoke-direct {v1}, LX/9RL;-><init>()V

    move-object v1, v1

    .line 1474455
    iput-boolean v3, v1, LX/0gW;->l:Z

    .line 1474456
    move-object v1, v1

    .line 1474457
    goto/16 :goto_0

    .line 1474458
    :pswitch_e
    new-instance v1, LX/9Lu;

    invoke-direct {v1}, LX/9Lu;-><init>()V

    move-object v1, v1

    .line 1474459
    iput-boolean v3, v1, LX/0gW;->l:Z

    .line 1474460
    move-object v1, v1

    .line 1474461
    goto/16 :goto_0

    :cond_e
    const/4 v2, 0x0

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method
