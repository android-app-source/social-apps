.class public LX/8hv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/0y2;

.field private final b:LX/6aG;

.field public final c:LX/0wM;


# direct methods
.method public constructor <init>(LX/0y2;LX/6aG;LX/0wM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1390992
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1390993
    iput-object p1, p0, LX/8hv;->a:LX/0y2;

    .line 1390994
    iput-object p2, p0, LX/8hv;->b:LX/6aG;

    .line 1390995
    iput-object p3, p0, LX/8hv;->c:LX/0wM;

    .line 1390996
    return-void
.end method

.method public static a(LX/0QB;)LX/8hv;
    .locals 6

    .prologue
    .line 1390997
    const-class v1, LX/8hv;

    monitor-enter v1

    .line 1390998
    :try_start_0
    sget-object v0, LX/8hv;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1390999
    sput-object v2, LX/8hv;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1391000
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1391001
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1391002
    new-instance p0, LX/8hv;

    invoke-static {v0}, LX/0y2;->b(LX/0QB;)LX/0y2;

    move-result-object v3

    check-cast v3, LX/0y2;

    invoke-static {v0}, LX/6aG;->b(LX/0QB;)LX/6aG;

    move-result-object v4

    check-cast v4, LX/6aG;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v5

    check-cast v5, LX/0wM;

    invoke-direct {p0, v3, v4, v5}, LX/8hv;-><init>(LX/0y2;LX/6aG;LX/0wM;)V

    .line 1391003
    move-object v0, p0

    .line 1391004
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1391005
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8hv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1391006
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1391007
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
    .locals 2

    .prologue
    .line 1391008
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1391009
    :goto_0
    return-object p0

    .line 1391010
    :cond_0
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_1

    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v0

    const/16 v1, 0xa

    if-eq v0, v1, :cond_1

    .line 1391011
    const-string v0, " \u2022 "

    invoke-virtual {p0, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1391012
    :cond_1
    invoke-virtual {p0, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_0
.end method

.method public static a(LX/8hv;Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLRating;)Ljava/lang/CharSequence;
    .locals 12
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1391013
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/16 v11, 0x21

    const/4 v10, 0x0

    .line 1391014
    if-nez p2, :cond_0

    .line 1391015
    :goto_0
    move-object v0, v1

    .line 1391016
    return-object v0

    .line 1391017
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1391018
    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    const v4, 0x7f0a008a

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v3, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 1391019
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLRating;->k()D

    move-result-wide v5

    .line 1391020
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLRating;->a()I

    move-result v4

    .line 1391021
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 1391022
    if-gtz v4, :cond_1

    const-wide/16 v7, 0x0

    cmpl-double v7, v5, v7

    if-lez v7, :cond_4

    .line 1391023
    :cond_1
    iget-object v7, p0, LX/8hv;->c:LX/0wM;

    const v8, 0x7f0209e3

    const v9, -0xbd984e

    invoke-virtual {v7, v8, v9}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 1391024
    if-eqz v7, :cond_2

    .line 1391025
    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v8

    invoke-virtual {v7, v10, v10, v1, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1391026
    new-instance v1, Landroid/text/style/ImageSpan;

    const/4 v8, 0x1

    invoke-direct {v1, v7, v8}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 1391027
    :cond_2
    const-string v7, "%.1f"

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-static {v7, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1391028
    if-eqz v1, :cond_3

    .line 1391029
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    .line 1391030
    const-string v6, "*"

    invoke-virtual {v2, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1391031
    add-int/lit8 v6, v5, 0x1

    invoke-virtual {v2, v1, v5, v6, v11}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1391032
    :cond_3
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 1391033
    invoke-virtual {v2, v3, v10, v1, v11}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1391034
    if-eqz v0, :cond_4

    .line 1391035
    const-string v1, " ("

    invoke-virtual {v2, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/text/NumberFormat;->getNumberInstance()Ljava/text/NumberFormat;

    move-result-object v3

    int-to-long v5, v4

    invoke-virtual {v3, v5, v6}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    const-string v3, ")"

    invoke-virtual {v1, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_4
    move-object v1, v2

    .line 1391036
    goto/16 :goto_0
.end method

.method public static a(Landroid/content/res/Resources;Lcom/facebook/graphql/model/GraphQLTextWithEntities;Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;)Ljava/lang/CharSequence;
    .locals 5

    .prologue
    .line 1391037
    :try_start_0
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 1391038
    if-eqz p1, :cond_1

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    if-eq p2, v1, :cond_1

    .line 1391039
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/8hv;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1391040
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    .line 1391041
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    sub-int v3, v2, v1

    .line 1391042
    if-eqz p2, :cond_1

    .line 1391043
    const/4 v1, 0x0

    .line 1391044
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;->SHOW_AVAILABLE:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    if-ne p2, v4, :cond_2

    .line 1391045
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    const v4, 0x7f0a00d4

    invoke-virtual {p0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v1, v4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 1391046
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 1391047
    const/16 v4, 0x21

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1391048
    :cond_1
    :goto_1
    return-object v0

    .line 1391049
    :cond_2
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;->SHOW_UNDETERMINED:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    if-ne p2, v4, :cond_3

    .line 1391050
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    const v4, 0x7f0a014a

    invoke-virtual {p0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v1, v4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1391051
    :catch_0
    const-string v0, ""

    goto :goto_1

    .line 1391052
    :cond_3
    :try_start_1
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;->SHOW_UNAVAILABLE:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    if-ne p2, v4, :cond_0

    .line 1391053
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    const v4, 0x7f0a00d3

    invoke-virtual {p0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v1, v4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLNode;)Ljava/lang/CharSequence;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 1391054
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->x()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v7

    .line 1391055
    if-eqz v7, :cond_0

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLStreetAddress;->l()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1391056
    :cond_0
    :goto_0
    return-object v6

    .line 1391057
    :cond_1
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 1391058
    iget-object v1, p0, LX/8hv;->a:LX/0y2;

    invoke-virtual {v1}, LX/0y2;->a()Lcom/facebook/location/ImmutableLocation;

    move-result-object v2

    .line 1391059
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->fj()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v1

    .line 1391060
    if-eqz v2, :cond_2

    if-eqz v1, :cond_2

    .line 1391061
    new-instance v3, Landroid/location/Location;

    const-string v4, "places_search"

    invoke-direct {v3, v4}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 1391062
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLLocation;->a()D

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Landroid/location/Location;->setLatitude(D)V

    .line 1391063
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLLocation;->b()D

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Landroid/location/Location;->setLongitude(D)V

    .line 1391064
    iget-object v1, p0, LX/8hv;->b:LX/6aG;

    invoke-static {v3}, Lcom/facebook/location/ImmutableLocation;->a(Landroid/location/Location;)Lcom/facebook/location/ImmutableLocation;

    move-result-object v3

    const-wide v4, 0x40f3a53340000000L    # 80467.203125

    invoke-virtual/range {v1 .. v6}, LX/6aG;->a(Lcom/facebook/location/ImmutableLocation;Lcom/facebook/location/ImmutableLocation;DLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1391065
    invoke-static {v0, v1}, LX/8hv;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1391066
    :cond_2
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLStreetAddress;->l()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/8hv;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-object v6, v0

    .line 1391067
    goto :goto_0
.end method
