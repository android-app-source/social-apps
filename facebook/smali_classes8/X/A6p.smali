.class public LX/A6p;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1624954
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;I)LX/A6P;
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 1624955
    invoke-static {p0}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v4

    .line 1624956
    if-nez v4, :cond_1

    .line 1624957
    :cond_0
    :goto_0
    return-object v0

    .line 1624958
    :cond_1
    if-le p1, v4, :cond_2

    .line 1624959
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 1624960
    :cond_2
    const-string v2, "((?<=[^a-zA-Z])|(?=[^a-zA-Z]))"

    invoke-virtual {p0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 1624961
    array-length v6, v5

    .line 1624962
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v6}, Ljava/util/ArrayList;-><init>(I)V

    move v2, v1

    move v3, v1

    .line 1624963
    :goto_1
    if-ge v2, v6, :cond_5

    .line 1624964
    aget-object v8, v5, v2

    .line 1624965
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    .line 1624966
    add-int/lit8 v1, v1, -0x1

    add-int v9, v3, v1

    .line 1624967
    new-instance v1, LX/A6P;

    invoke-direct {v1, v8, v3, v9}, LX/A6P;-><init>(Ljava/lang/String;II)V

    .line 1624968
    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1624969
    if-lt p1, v3, :cond_4

    if-gt p1, v9, :cond_4

    .line 1624970
    const-string v3, "[a-zA-Z]+"

    invoke-virtual {v8, v3}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    move-object v0, v1

    .line 1624971
    goto :goto_0

    .line 1624972
    :cond_3
    if-lez v2, :cond_0

    add-int/lit8 v1, v2, -0x1

    aget-object v1, v5, v1

    const-string v2, "[a-zA-Z]+"

    invoke-virtual {v1, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1624973
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/A6P;

    goto :goto_0

    .line 1624974
    :cond_4
    add-int/lit8 v3, v9, 0x1

    .line 1624975
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 1624976
    :cond_5
    if-ne p1, v4, :cond_6

    .line 1624977
    add-int/lit8 v1, v6, -0x1

    aget-object v1, v5, v1

    .line 1624978
    const-string v2, "[a-zA-Z]+"

    invoke-virtual {v1, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1624979
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/A6P;

    goto :goto_0

    .line 1624980
    :cond_6
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public static a(Ljava/lang/String;LX/A6P;Ljava/lang/String;)LX/A6Q;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1624981
    invoke-static {p0}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v2

    .line 1624982
    if-lez v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1624983
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1624984
    iget v0, p1, LX/A6P;->b:I

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1624985
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1624986
    invoke-static {v0}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v1

    .line 1624987
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p1, LX/A6P;->c:I

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {p0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1624988
    new-instance v2, LX/A6Q;

    invoke-direct {v2, v0, v1}, LX/A6Q;-><init>(Ljava/lang/String;I)V

    return-object v2

    :cond_0
    move v0, v1

    .line 1624989
    goto :goto_0
.end method
