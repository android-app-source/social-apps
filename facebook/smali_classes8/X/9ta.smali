.class public final LX/9ta;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 1562244
    const/4 v9, 0x0

    .line 1562245
    const/4 v8, 0x0

    .line 1562246
    const/4 v7, 0x0

    .line 1562247
    const/4 v6, 0x0

    .line 1562248
    const/4 v5, 0x0

    .line 1562249
    const/4 v4, 0x0

    .line 1562250
    const/4 v3, 0x0

    .line 1562251
    const/4 v2, 0x0

    .line 1562252
    const/4 v1, 0x0

    .line 1562253
    const/4 v0, 0x0

    .line 1562254
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v10, v11, :cond_1

    .line 1562255
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1562256
    const/4 v0, 0x0

    .line 1562257
    :goto_0
    return v0

    .line 1562258
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1562259
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_b

    .line 1562260
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1562261
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1562262
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 1562263
    const-string v11, "__type__"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_2

    const-string v11, "__typename"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1562264
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v9

    goto :goto_1

    .line 1562265
    :cond_3
    const-string v11, "action"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 1562266
    invoke-static {p0, p1}, LX/9tZ;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1562267
    :cond_4
    const-string v11, "auto_execute_action_delay"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 1562268
    const/4 v0, 0x1

    .line 1562269
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v7

    goto :goto_1

    .line 1562270
    :cond_5
    const-string v11, "component_logical_path"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 1562271
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 1562272
    :cond_6
    const-string v11, "component_style"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 1562273
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    goto :goto_1

    .line 1562274
    :cond_7
    const-string v11, "component_tracking_data"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 1562275
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_1

    .line 1562276
    :cond_8
    const-string v11, "icon_image"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 1562277
    invoke-static {p0, p1}, LX/5tK;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 1562278
    :cond_9
    const-string v11, "message"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    .line 1562279
    invoke-static {p0, p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 1562280
    :cond_a
    const-string v11, "sub_message"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 1562281
    invoke-static {p0, p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v1

    goto/16 :goto_1

    .line 1562282
    :cond_b
    const/16 v10, 0x9

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1562283
    const/4 v10, 0x0

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 1562284
    const/4 v9, 0x1

    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 1562285
    if-eqz v0, :cond_c

    .line 1562286
    const/4 v0, 0x2

    const/4 v8, 0x0

    invoke-virtual {p1, v0, v7, v8}, LX/186;->a(III)V

    .line 1562287
    :cond_c
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1562288
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1562289
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1562290
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1562291
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1562292
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1562293
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 1562294
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1562295
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1562296
    if-eqz v0, :cond_0

    .line 1562297
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1562298
    invoke-static {p0, p1, v2, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1562299
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1562300
    if-eqz v0, :cond_1

    .line 1562301
    const-string v1, "action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1562302
    invoke-static {p0, v0, p2}, LX/9tZ;->a(LX/15i;ILX/0nX;)V

    .line 1562303
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1562304
    if-eqz v0, :cond_2

    .line 1562305
    const-string v1, "auto_execute_action_delay"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1562306
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1562307
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1562308
    if-eqz v0, :cond_3

    .line 1562309
    const-string v1, "component_logical_path"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1562310
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1562311
    :cond_3
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1562312
    if-eqz v0, :cond_4

    .line 1562313
    const-string v0, "component_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1562314
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1562315
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1562316
    if-eqz v0, :cond_5

    .line 1562317
    const-string v1, "component_tracking_data"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1562318
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1562319
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1562320
    if-eqz v0, :cond_6

    .line 1562321
    const-string v1, "icon_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1562322
    invoke-static {p0, v0, p2}, LX/5tK;->a(LX/15i;ILX/0nX;)V

    .line 1562323
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1562324
    if-eqz v0, :cond_7

    .line 1562325
    const-string v1, "message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1562326
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 1562327
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1562328
    if-eqz v0, :cond_8

    .line 1562329
    const-string v1, "sub_message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1562330
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 1562331
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1562332
    return-void
.end method
