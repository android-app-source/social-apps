.class public LX/AJD;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1660881
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1660882
    return-void
.end method

.method public static a(Landroid/os/Bundle;Landroid/os/Bundle;)Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;
    .locals 14
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1660883
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1660884
    invoke-static {}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->newBuilder()Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;

    move-result-object v2

    .line 1660885
    const-string v1, "extra_selected_audience"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/model/SharesheetSelectedAudience;

    .line 1660886
    if-nez v1, :cond_1

    .line 1660887
    :goto_0
    const-string v1, "extra_is_video"

    const/4 v3, 0x0

    invoke-virtual {p0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v2, v1}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;->setIsVideo(Z)Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;

    move-result-object v1

    const-string v2, "extra_inspiration_group_session_id"

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;->setInspirationGroupSessionId(Ljava/lang/String;)Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;

    move-result-object v1

    const-string v2, "extra_prompt_id"

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;->setPromptId(Ljava/lang/String;)Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;

    move-result-object v1

    const-string v2, "extra_media_content_id"

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;->setMediaContentId(Ljava/lang/String;)Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;

    move-result-object v1

    move-object v0, v1

    .line 1660888
    if-eqz p1, :cond_0

    .line 1660889
    const-string v1, "selected_audience"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 1660890
    const-string v2, "newsfeed_selected"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;->setIsNewsFeedSelected(Z)Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;

    move-result-object v2

    const-string v3, "my_day_selected"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;->setIsMyDaySelected(Z)Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;

    move-result-object v2

    if-eqz v1, :cond_5

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    :goto_1
    invoke-virtual {v2, v1}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;->setSelectedAudience(LX/0Px;)Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;

    move-result-object v1

    const-string v2, "search_query"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;->setSearchQuery(Ljava/lang/String;)Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;

    move-result-object v2

    const-string v1, "selectable_privacy_data"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-virtual {v2, v1}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;->setSelectablePrivacyData(Lcom/facebook/privacy/model/SelectablePrivacyData;)Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;

    move-result-object v1

    const-string v2, "session_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;->setSessionId(Ljava/lang/String;)Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;

    .line 1660891
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;->a()Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;

    move-result-object v0

    return-object v0

    .line 1660892
    :cond_1
    iget-object v4, v1, Lcom/facebook/audience/model/SharesheetSelectedAudience;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-object v4, v4

    .line 1660893
    invoke-virtual {v2, v4}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;->setSelectablePrivacyData(Lcom/facebook/privacy/model/SelectablePrivacyData;)Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;

    move-result-object v5

    .line 1660894
    iget-object v4, v1, Lcom/facebook/audience/model/SharesheetSelectedAudience;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-object v4, v4

    .line 1660895
    if-eqz v4, :cond_2

    const/4 v4, 0x1

    :goto_2
    invoke-virtual {v5, v4}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;->setIsNewsFeedSelected(Z)Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;

    move-result-object v4

    .line 1660896
    iget-boolean v5, v1, Lcom/facebook/audience/model/SharesheetSelectedAudience;->c:Z

    move v5, v5

    .line 1660897
    invoke-virtual {v4, v5}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;->setIsMyDaySelected(Z)Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;

    move-result-object v4

    .line 1660898
    iget-object v5, v1, Lcom/facebook/audience/model/SharesheetSelectedAudience;->a:LX/0Px;

    move-object v5, v5

    .line 1660899
    if-nez v5, :cond_3

    .line 1660900
    sget-object v6, LX/0Q7;->a:LX/0Px;

    move-object v6, v6

    .line 1660901
    :goto_3
    move-object v5, v6

    .line 1660902
    invoke-virtual {v4, v5}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;->setSelectedAudience(LX/0Px;)Lcom/facebook/audience/sharesheet/data/SharesheetParseResult$Builder;

    goto/16 :goto_0

    :cond_2
    const/4 v4, 0x0

    goto :goto_2

    .line 1660903
    :cond_3
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v10

    .line 1660904
    const/4 v7, 0x1

    .line 1660905
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v11

    const/4 v6, 0x0

    move v8, v7

    move v7, v6

    :goto_4
    if-ge v7, v11, :cond_4

    invoke-virtual {v5, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    .line 1660906
    invoke-static {}, Lcom/facebook/audience/model/AudienceControlData;->newBuilder()Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v9

    invoke-virtual {v6}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->a()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v12}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setId(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v9

    invoke-virtual {v6}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->b()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v12}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setName(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v9

    invoke-virtual {v6}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v9, v6}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setProfileUri(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v6

    add-int/lit8 v9, v8, 0x1

    invoke-virtual {v6, v8}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setRanking(I)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/audience/model/AudienceControlData$Builder;->a()Lcom/facebook/audience/model/AudienceControlData;

    move-result-object v6

    invoke-virtual {v10, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1660907
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    move v8, v9

    goto :goto_4

    .line 1660908
    :cond_4
    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    goto :goto_3

    .line 1660909
    :cond_5
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 1660910
    goto/16 :goto_1
.end method
