.class public final LX/8tX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView$SavedState;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1413304
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/os/Parcel;)Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView$SavedState;
    .locals 2

    .prologue
    .line 1413303
    new-instance v0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView$SavedState;

    invoke-direct {v0, p0}, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView$SavedState;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1413300
    invoke-static {p1}, LX/8tX;->a(Landroid/os/Parcel;)Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView$SavedState;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1413301
    new-array v0, p1, [Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView$SavedState;

    move-object v0, v0

    .line 1413302
    return-object v0
.end method
