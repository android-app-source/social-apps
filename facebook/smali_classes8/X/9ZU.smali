.class public final LX/9ZU;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1510308
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1510309
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1510310
    :goto_0
    return v1

    .line 1510311
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1510312
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1510313
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1510314
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1510315
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1510316
    const-string v4, "focus"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1510317
    invoke-static {p0, p1}, LX/9ZR;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1510318
    :cond_2
    const-string v4, "photo"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1510319
    invoke-static {p0, p1}, LX/9ZT;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1510320
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1510321
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1510322
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1510323
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 8

    .prologue
    .line 1510324
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1510325
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1510326
    if-eqz v0, :cond_2

    .line 1510327
    const-string v1, "focus"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1510328
    const-wide/16 v6, 0x0

    .line 1510329
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1510330
    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1510331
    cmpl-double v4, v2, v6

    if-eqz v4, :cond_0

    .line 1510332
    const-string v4, "x"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1510333
    invoke-virtual {p2, v2, v3}, LX/0nX;->a(D)V

    .line 1510334
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p0, v0, v2, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1510335
    cmpl-double v4, v2, v6

    if-eqz v4, :cond_1

    .line 1510336
    const-string v4, "y"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1510337
    invoke-virtual {p2, v2, v3}, LX/0nX;->a(D)V

    .line 1510338
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1510339
    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1510340
    if-eqz v0, :cond_3

    .line 1510341
    const-string v1, "photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1510342
    invoke-static {p0, v0, p2, p3}, LX/9ZT;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1510343
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1510344
    return-void
.end method
