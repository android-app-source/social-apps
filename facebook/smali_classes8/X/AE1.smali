.class public final LX/AE1;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/2yS;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/CharSequence;

.field public b:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public c:I

.field public d:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public e:I

.field public f:Z

.field public g:Ljava/lang/CharSequence;

.field public h:I

.field public i:I

.field public j:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public k:Landroid/view/View$OnClickListener;

.field public final synthetic l:LX/2yS;


# direct methods
.method public constructor <init>(LX/2yS;)V
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    .line 1645922
    iput-object p1, p0, LX/AE1;->l:LX/2yS;

    .line 1645923
    move-object v0, p1

    .line 1645924
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1645925
    iput v1, p0, LX/AE1;->c:I

    .line 1645926
    sget v0, LX/AE3;->a:I

    iput v0, p0, LX/AE1;->e:I

    .line 1645927
    iput v1, p0, LX/AE1;->h:I

    .line 1645928
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1645885
    const-string v0, "ActionButtonComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1645886
    if-ne p0, p1, :cond_1

    .line 1645887
    :cond_0
    :goto_0
    return v0

    .line 1645888
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1645889
    goto :goto_0

    .line 1645890
    :cond_3
    check-cast p1, LX/AE1;

    .line 1645891
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1645892
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1645893
    if-eq v2, v3, :cond_0

    .line 1645894
    iget-object v2, p0, LX/AE1;->a:Ljava/lang/CharSequence;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/AE1;->a:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/AE1;->a:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1645895
    goto :goto_0

    .line 1645896
    :cond_5
    iget-object v2, p1, LX/AE1;->a:Ljava/lang/CharSequence;

    if-nez v2, :cond_4

    .line 1645897
    :cond_6
    iget-object v2, p0, LX/AE1;->b:LX/1dc;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/AE1;->b:LX/1dc;

    iget-object v3, p1, LX/AE1;->b:LX/1dc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1645898
    goto :goto_0

    .line 1645899
    :cond_8
    iget-object v2, p1, LX/AE1;->b:LX/1dc;

    if-nez v2, :cond_7

    .line 1645900
    :cond_9
    iget v2, p0, LX/AE1;->c:I

    iget v3, p1, LX/AE1;->c:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 1645901
    goto :goto_0

    .line 1645902
    :cond_a
    iget-object v2, p0, LX/AE1;->d:LX/1dc;

    if-eqz v2, :cond_c

    iget-object v2, p0, LX/AE1;->d:LX/1dc;

    iget-object v3, p1, LX/AE1;->d:LX/1dc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    .line 1645903
    goto :goto_0

    .line 1645904
    :cond_c
    iget-object v2, p1, LX/AE1;->d:LX/1dc;

    if-nez v2, :cond_b

    .line 1645905
    :cond_d
    iget v2, p0, LX/AE1;->e:I

    iget v3, p1, LX/AE1;->e:I

    if-eq v2, v3, :cond_e

    move v0, v1

    .line 1645906
    goto :goto_0

    .line 1645907
    :cond_e
    iget-boolean v2, p0, LX/AE1;->f:Z

    iget-boolean v3, p1, LX/AE1;->f:Z

    if-eq v2, v3, :cond_f

    move v0, v1

    .line 1645908
    goto :goto_0

    .line 1645909
    :cond_f
    iget-object v2, p0, LX/AE1;->g:Ljava/lang/CharSequence;

    if-eqz v2, :cond_11

    iget-object v2, p0, LX/AE1;->g:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/AE1;->g:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    :cond_10
    move v0, v1

    .line 1645910
    goto :goto_0

    .line 1645911
    :cond_11
    iget-object v2, p1, LX/AE1;->g:Ljava/lang/CharSequence;

    if-nez v2, :cond_10

    .line 1645912
    :cond_12
    iget v2, p0, LX/AE1;->h:I

    iget v3, p1, LX/AE1;->h:I

    if-eq v2, v3, :cond_13

    move v0, v1

    .line 1645913
    goto/16 :goto_0

    .line 1645914
    :cond_13
    iget v2, p0, LX/AE1;->i:I

    iget v3, p1, LX/AE1;->i:I

    if-eq v2, v3, :cond_14

    move v0, v1

    .line 1645915
    goto/16 :goto_0

    .line 1645916
    :cond_14
    iget-object v2, p0, LX/AE1;->j:Landroid/util/SparseArray;

    if-eqz v2, :cond_16

    iget-object v2, p0, LX/AE1;->j:Landroid/util/SparseArray;

    iget-object v3, p1, LX/AE1;->j:Landroid/util/SparseArray;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_17

    :cond_15
    move v0, v1

    .line 1645917
    goto/16 :goto_0

    .line 1645918
    :cond_16
    iget-object v2, p1, LX/AE1;->j:Landroid/util/SparseArray;

    if-nez v2, :cond_15

    .line 1645919
    :cond_17
    iget-object v2, p0, LX/AE1;->k:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_18

    iget-object v2, p0, LX/AE1;->k:Landroid/view/View$OnClickListener;

    iget-object v3, p1, LX/AE1;->k:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1645920
    goto/16 :goto_0

    .line 1645921
    :cond_18
    iget-object v2, p1, LX/AE1;->k:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
