.class public final LX/AKz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/audience/ui/SnacksReplyFooterBar;


# direct methods
.method public constructor <init>(Lcom/facebook/audience/ui/SnacksReplyFooterBar;)V
    .locals 0

    .prologue
    .line 1664497
    iput-object p1, p0, LX/AKz;->a:Lcom/facebook/audience/ui/SnacksReplyFooterBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 1664496
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1664495
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 1664492
    iget-object v0, p0, LX/AKz;->a:Lcom/facebook/audience/ui/SnacksReplyFooterBar;

    iget-object v1, v0, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setEnabled(Z)V

    .line 1664493
    return-void

    .line 1664494
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
