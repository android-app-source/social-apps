.class public LX/9VY;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1499449
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 1499450
    return-void
.end method

.method public static a(Lcom/facebook/user/model/User;Ljava/lang/Boolean;)LX/03R;
    .locals 1
    .param p0    # Lcom/facebook/user/model/User;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/messaging/annotations/IsAnalyticsEnabledGk;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/annotations/IsAnalyticsEnabled;
    .end annotation

    .prologue
    .line 1499451
    if-nez p0, :cond_0

    .line 1499452
    sget-object v0, LX/03R;->UNSET:LX/03R;

    .line 1499453
    :goto_0
    return-object v0

    .line 1499454
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1499455
    sget-object v0, LX/03R;->YES:LX/03R;

    goto :goto_0

    .line 1499456
    :cond_1
    sget-object v0, LX/03R;->NO:LX/03R;

    goto :goto_0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 1499457
    return-void
.end method
