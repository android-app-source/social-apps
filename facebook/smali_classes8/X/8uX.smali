.class public LX/8uX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/8uX;


# instance fields
.field public a:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:Z

.field public final c:LX/8uW;


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1414990
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1414991
    new-instance v0, LX/8uW;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, LX/8uW;-><init>(I)V

    iput-object v0, p0, LX/8uX;->c:LX/8uW;

    .line 1414992
    return-void
.end method

.method public static a(LX/0QB;)LX/8uX;
    .locals 4

    .prologue
    .line 1414975
    sget-object v0, LX/8uX;->d:LX/8uX;

    if-nez v0, :cond_1

    .line 1414976
    const-class v1, LX/8uX;

    monitor-enter v1

    .line 1414977
    :try_start_0
    sget-object v0, LX/8uX;->d:LX/8uX;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1414978
    if-eqz v2, :cond_0

    .line 1414979
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1414980
    new-instance p0, LX/8uX;

    invoke-direct {p0}, LX/8uX;-><init>()V

    .line 1414981
    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v3

    check-cast v3, LX/0Xl;

    .line 1414982
    iput-object v3, p0, LX/8uX;->a:LX/0Xl;

    .line 1414983
    move-object v0, p0

    .line 1414984
    sput-object v0, LX/8uX;->d:LX/8uX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1414985
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1414986
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1414987
    :cond_1
    sget-object v0, LX/8uX;->d:LX/8uX;

    return-object v0

    .line 1414988
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1414989
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
