.class public LX/98j;
.super LX/5p5;
.source ""

# interfaces
.implements LX/0o1;


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "RKExceptionsManager"
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/98j;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/33u;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/03V;

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0o1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/03V;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/33u;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1446304
    invoke-direct {p0}, LX/5p5;-><init>()V

    .line 1446305
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/98j;->c:Ljava/util/Set;

    .line 1446306
    iput-object p1, p0, LX/98j;->a:LX/0Ot;

    .line 1446307
    iput-object p2, p0, LX/98j;->b:LX/03V;

    .line 1446308
    return-void
.end method

.method public static a(LX/0QB;)LX/98j;
    .locals 3

    .prologue
    .line 1446294
    sget-object v0, LX/98j;->d:LX/98j;

    if-nez v0, :cond_1

    .line 1446295
    const-class v1, LX/98j;

    monitor-enter v1

    .line 1446296
    :try_start_0
    sget-object v0, LX/98j;->d:LX/98j;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1446297
    if-eqz v2, :cond_0

    .line 1446298
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/98j;->b(LX/0QB;)LX/98j;

    move-result-object v0

    sput-object v0, LX/98j;->d:LX/98j;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1446299
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1446300
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1446301
    :cond_1
    sget-object v0, LX/98j;->d:LX/98j;

    return-object v0

    .line 1446302
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1446303
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Ljava/lang/String;LX/5pC;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1446285
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ", stack:\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1446286
    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1}, LX/5pC;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 1446287
    invoke-interface {p1, v0}, LX/5pC;->a(I)LX/5pG;

    move-result-object v2

    .line 1446288
    const-string v3, "methodName"

    invoke-interface {v2, v3}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "@"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "lineNumber"

    invoke-interface {v2, v4}, LX/5pG;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1446289
    const-string v3, "column"

    invoke-interface {v2, v3}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "column"

    invoke-interface {v2, v3}, LX/5pG;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "column"

    invoke-interface {v2, v3}, LX/5pG;->getType(Ljava/lang/String;)Lcom/facebook/react/bridge/ReadableType;

    move-result-object v3

    sget-object v4, Lcom/facebook/react/bridge/ReadableType;->Number:Lcom/facebook/react/bridge/ReadableType;

    if-ne v3, v4, :cond_0

    .line 1446290
    const-string v3, ":"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "column"

    invoke-interface {v2, v4}, LX/5pG;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1446291
    :cond_0
    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1446292
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1446293
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(LX/0QB;)LX/98j;
    .locals 3

    .prologue
    .line 1446283
    new-instance v1, LX/98j;

    const/16 v0, 0x53c

    invoke-static {p0, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-direct {v1, v2, v0}, LX/98j;-><init>(LX/0Ot;LX/03V;)V

    .line 1446284
    return-object v1
.end method

.method private h()LX/5qI;
    .locals 1

    .prologue
    .line 1446282
    iget-object v0, p0, LX/98j;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/33u;

    invoke-virtual {v0}, LX/33u;->c()LX/33y;

    move-result-object v0

    invoke-virtual {v0}, LX/33y;->a()LX/5qI;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/0o1;)V
    .locals 1

    .prologue
    .line 1446248
    invoke-static {}, LX/5pe;->b()V

    .line 1446249
    iget-object v0, p0, LX/98j;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1446250
    return-void
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 3

    .prologue
    .line 1446266
    invoke-direct {p0}, LX/98j;->h()LX/5qI;

    move-result-object v0

    .line 1446267
    invoke-interface {v0}, LX/5qI;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1446268
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    .line 1446269
    :goto_0
    return-void

    .line 1446270
    :cond_0
    iget-object v1, p0, LX/98j;->c:Ljava/util/Set;

    monitor-enter v1

    .line 1446271
    :try_start_0
    iget-object v0, p0, LX/98j;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1446272
    instance-of v0, p1, Ljava/lang/RuntimeException;

    if-eqz v0, :cond_1

    .line 1446273
    check-cast p1, Ljava/lang/RuntimeException;

    throw p1

    .line 1446274
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1446275
    :cond_1
    :try_start_1
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 1446276
    :cond_2
    iget-object v0, p0, LX/98j;->b:LX/03V;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1446277
    iget-object v0, p0, LX/98j;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/33u;

    .line 1446278
    invoke-virtual {v0}, LX/33u;->b()V

    .line 1446279
    new-instance v0, Ljava/util/HashSet;

    iget-object v2, p0, LX/98j;->c:Ljava/util/Set;

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 1446280
    new-instance v2, Lcom/facebook/fbreact/exceptionmanager/FbReactExceptionManager$1;

    invoke-direct {v2, p0, v0, p1}, Lcom/facebook/fbreact/exceptionmanager/FbReactExceptionManager$1;-><init>(LX/98j;Ljava/util/Set;Ljava/lang/Exception;)V

    invoke-static {v2}, LX/5pe;->a(Ljava/lang/Runnable;)V

    .line 1446281
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final b(LX/0o1;)V
    .locals 1

    .prologue
    .line 1446263
    invoke-static {}, LX/5pe;->b()V

    .line 1446264
    iget-object v0, p0, LX/98j;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1446265
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1446262
    const/4 v0, 0x1

    return v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1446261
    const-string v0, "RKExceptionsManager"

    return-object v0
.end method

.method public reportFatalException(Ljava/lang/String;LX/5pC;I)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1446257
    invoke-direct {p0}, LX/98j;->h()LX/5qI;

    move-result-object v0

    .line 1446258
    invoke-interface {v0}, LX/5qI;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1446259
    new-instance v0, LX/5pp;

    invoke-static {p1, p2}, LX/98j;->a(Ljava/lang/String;LX/5pC;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pp;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1446260
    :cond_0
    return-void
.end method

.method public reportSoftException(Ljava/lang/String;LX/5pC;I)V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1446253
    invoke-direct {p0}, LX/98j;->h()LX/5qI;

    move-result-object v0

    .line 1446254
    invoke-interface {v0}, LX/5qI;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1446255
    iget-object v0, p0, LX/98j;->b:LX/03V;

    new-instance v1, LX/98k;

    invoke-static {p1, p2}, LX/98j;->a(Ljava/lang/String;LX/5pC;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/98k;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1446256
    :cond_0
    return-void
.end method

.method public updateExceptionMessage(Ljava/lang/String;LX/5pC;I)V
    .locals 0
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1446251
    invoke-direct {p0}, LX/98j;->h()LX/5qI;

    .line 1446252
    return-void
.end method
