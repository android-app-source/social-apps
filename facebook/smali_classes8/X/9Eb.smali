.class public LX/9Eb;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/9Ea;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1457224
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1457225
    return-void
.end method


# virtual methods
.method public final a(LX/0QK;LX/9CC;LX/9D1;LX/9Do;LX/9Du;LX/9EL;)LX/9Ea;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            "Ljava/lang/Void;",
            ">;",
            "LX/9CC;",
            "LX/9D1;",
            "LX/9Do;",
            "LX/9Du;",
            "LX/9EL;",
            ")",
            "LX/9Ea;"
        }
    .end annotation

    .prologue
    .line 1457226
    new-instance v1, LX/9Ea;

    invoke-static/range {p0 .. p0}, LX/1K9;->a(LX/0QB;)LX/1K9;

    move-result-object v8

    check-cast v8, LX/1K9;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-static/range {p0 .. p0}, LX/20j;->a(LX/0QB;)LX/20j;

    move-result-object v10

    check-cast v10, LX/20j;

    invoke-static/range {p0 .. p0}, LX/1nK;->a(LX/0QB;)LX/1nK;

    move-result-object v11

    check-cast v11, LX/1nK;

    invoke-static/range {p0 .. p0}, LX/3iO;->a(LX/0QB;)LX/3iO;

    move-result-object v12

    check-cast v12, LX/3iO;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v13

    check-cast v13, LX/0ad;

    const/16 v2, 0x3567

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    const-class v2, LX/9F8;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v15

    check-cast v15, LX/9F8;

    const-class v2, LX/9EM;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v16

    check-cast v16, LX/9EM;

    invoke-static/range {p0 .. p0}, LX/1Ar;->a(LX/0QB;)LX/1Ar;

    move-result-object v17

    check-cast v17, LX/1Ar;

    invoke-static/range {p0 .. p0}, Lcom/facebook/delights/floating/DelightsFireworks;->a(LX/0QB;)Lcom/facebook/delights/floating/DelightsFireworks;

    move-result-object v18

    check-cast v18, Lcom/facebook/delights/floating/DelightsFireworks;

    invoke-static/range {p0 .. p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v19

    check-cast v19, LX/0Zb;

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    invoke-direct/range {v1 .. v19}, LX/9Ea;-><init>(LX/0QK;LX/9CC;LX/9D1;LX/9Do;LX/9Du;LX/9EL;LX/1K9;LX/03V;LX/20j;LX/1nK;LX/3iO;LX/0ad;LX/0Ot;LX/9F8;LX/9EM;LX/1Ar;Lcom/facebook/delights/floating/DelightsFireworks;LX/0Zb;)V

    .line 1457227
    return-object v1
.end method
