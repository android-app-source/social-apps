.class public abstract LX/9g2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Q:",
        "LX/9g8;",
        "E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "LX/9g5",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/Throwable;

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation
.end field

.field public e:LX/9g6;

.field private final f:LX/0Sh;

.field private final g:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1522951
    const-class v0, LX/9g2;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/9g2;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/9g8;LX/0Sh;LX/03V;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TQ;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1522941
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1522942
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, LX/9g2;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 1522943
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1522944
    iput-object v0, p0, LX/9g2;->d:LX/0Px;

    .line 1522945
    sget-object v0, LX/9g6;->DONE:LX/9g6;

    iput-object v0, p0, LX/9g2;->e:LX/9g6;

    .line 1522946
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1522947
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    iput-object v0, p0, LX/9g2;->f:LX/0Sh;

    .line 1522948
    iput-object p3, p0, LX/9g2;->g:LX/03V;

    .line 1522949
    new-instance v0, Ljava/lang/Throwable;

    invoke-direct {v0}, Ljava/lang/Throwable;-><init>()V

    iput-object v0, p0, LX/9g2;->c:Ljava/lang/Throwable;

    .line 1522950
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 1522939
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LX/9g2;->a(ILX/0am;)V

    .line 1522940
    return-void
.end method

.method public abstract a(ILX/0am;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public final a(LX/0Px;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 1522952
    iget-object v0, p0, LX/9g2;->f:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1522953
    iget-object v0, p0, LX/9g2;->e:LX/9g6;

    sget-object v1, LX/9g6;->CLOSED:LX/9g6;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Calling method of closed() fetcher"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1522954
    iput-object p1, p0, LX/9g2;->d:LX/0Px;

    .line 1522955
    iget-object v0, p0, LX/9g2;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9g5;

    .line 1522956
    :try_start_0
    invoke-interface {v0, p1}, LX/9g5;->a(LX/0Px;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1522957
    :catch_0
    move-exception v0

    .line 1522958
    iget-object v2, p0, LX/9g2;->g:LX/03V;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, LX/9g2;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "::notifyDataChanged"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Clients must not throw exceptions in listener callbacks"

    invoke-virtual {v2, v3, v4, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1522959
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1522960
    :cond_1
    return-void
.end method

.method public final a(LX/9g5;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/9g5",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 1522935
    iget-object v0, p0, LX/9g2;->e:LX/9g6;

    sget-object v1, LX/9g6;->CLOSED:LX/9g6;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Calling method of closed() fetcher"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1522936
    iget-object v0, p0, LX/9g2;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 1522937
    return-void

    .line 1522938
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/9g6;)V
    .locals 5

    .prologue
    .line 1522926
    iget-object v0, p0, LX/9g2;->f:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1522927
    iget-object v0, p0, LX/9g2;->e:LX/9g6;

    sget-object v1, LX/9g6;->CLOSED:LX/9g6;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Calling method of closed() fetcher"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1522928
    iput-object p1, p0, LX/9g2;->e:LX/9g6;

    .line 1522929
    iget-object v0, p0, LX/9g2;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9g5;

    .line 1522930
    :try_start_0
    invoke-interface {v0, p1}, LX/9g5;->a(LX/9g6;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1522931
    :catch_0
    move-exception v0

    .line 1522932
    iget-object v2, p0, LX/9g2;->g:LX/03V;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, LX/9g2;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "::notifyStatusChanged"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Clients must not throw exceptions in listener callbacks"

    invoke-virtual {v2, v3, v4, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1522933
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1522934
    :cond_1
    return-void
.end method

.method public final b(LX/9g5;)V
    .locals 1

    .prologue
    .line 1522924
    iget-object v0, p0, LX/9g2;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 1522925
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 1522920
    iget-object v0, p0, LX/9g2;->f:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1522921
    sget-object v0, LX/9g6;->CLOSED:LX/9g6;

    invoke-virtual {p0, v0}, LX/9g2;->a(LX/9g6;)V

    .line 1522922
    iget-object v0, p0, LX/9g2;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 1522923
    return-void
.end method

.method public abstract d()Z
.end method

.method public final finalize()V
    .locals 4

    .prologue
    .line 1522916
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 1522917
    iget-object v0, p0, LX/9g2;->e:LX/9g6;

    sget-object v1, LX/9g6;->CLOSED:LX/9g6;

    if-eq v0, v1, :cond_0

    .line 1522918
    iget-object v0, p0, LX/9g2;->g:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/9g2;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " finalized in wrong state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/9g2;->e:LX/9g6;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Client must always call close() when it\'s done with MediaFetcher"

    iget-object v3, p0, LX/9g2;->c:Ljava/lang/Throwable;

    invoke-virtual {v0, v1, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1522919
    :cond_0
    return-void
.end method
