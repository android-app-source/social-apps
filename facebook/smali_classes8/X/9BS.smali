.class public final enum LX/9BS;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9BS;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9BS;

.field public static final enum BLINGBAR:LX/9BS;

.field public static final enum COMMENT_FOOTER:LX/9BS;

.field public static final enum FOOTER:LX/9BS;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1451701
    new-instance v0, LX/9BS;

    const-string v1, "BLINGBAR"

    invoke-direct {v0, v1, v2}, LX/9BS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9BS;->BLINGBAR:LX/9BS;

    .line 1451702
    new-instance v0, LX/9BS;

    const-string v1, "COMMENT_FOOTER"

    invoke-direct {v0, v1, v3}, LX/9BS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9BS;->COMMENT_FOOTER:LX/9BS;

    .line 1451703
    new-instance v0, LX/9BS;

    const-string v1, "FOOTER"

    invoke-direct {v0, v1, v4}, LX/9BS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9BS;->FOOTER:LX/9BS;

    .line 1451704
    const/4 v0, 0x3

    new-array v0, v0, [LX/9BS;

    sget-object v1, LX/9BS;->BLINGBAR:LX/9BS;

    aput-object v1, v0, v2

    sget-object v1, LX/9BS;->COMMENT_FOOTER:LX/9BS;

    aput-object v1, v0, v3

    sget-object v1, LX/9BS;->FOOTER:LX/9BS;

    aput-object v1, v0, v4

    sput-object v0, LX/9BS;->$VALUES:[LX/9BS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1451700
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9BS;
    .locals 1

    .prologue
    .line 1451698
    const-class v0, LX/9BS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9BS;

    return-object v0
.end method

.method public static values()[LX/9BS;
    .locals 1

    .prologue
    .line 1451699
    sget-object v0, LX/9BS;->$VALUES:[LX/9BS;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9BS;

    return-object v0
.end method
