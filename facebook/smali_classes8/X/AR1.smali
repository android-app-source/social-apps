.class public LX/AR1;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0io;",
        ":",
        "LX/0iu;",
        ":",
        "LX/0j0;",
        ":",
        "LX/0j1;",
        ":",
        "LX/0j2;",
        ":",
        "LX/0j3;",
        ":",
        "LX/0j8;",
        ":",
        "LX/0j4;",
        ":",
        "LX/0iq;",
        ":",
        "LX/0j9;",
        ":",
        "LX/0j5;",
        ":",
        "LX/0jB;",
        ":",
        "LX/0jD;",
        ":",
        "LX/0j6;",
        ":",
        "LX/0ip;",
        ":",
        "LX/0jE;",
        ":",
        "LX/0jH;",
        ":",
        "LX/0jF;",
        "DerivedData::",
        "LX/2rg;",
        ":",
        "LX/5RE;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;>",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/ARN;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final d:LX/9A4;

.field private final e:LX/7l0;

.field public final f:LX/0ad;


# direct methods
.method public constructor <init>(LX/0il;LX/ARN;LX/0Or;LX/9A4;LX/7l0;LX/0ad;)V
    .locals 1
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/ARN;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "LX/ARN;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/9A4;",
            "LX/7l0;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1672217
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1672218
    iput-object p3, p0, LX/AR1;->b:LX/0Or;

    .line 1672219
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/AR1;->a:Ljava/lang/ref/WeakReference;

    .line 1672220
    iput-object p2, p0, LX/AR1;->c:LX/ARN;

    .line 1672221
    iput-object p4, p0, LX/AR1;->d:LX/9A4;

    .line 1672222
    iput-object p5, p0, LX/AR1;->e:LX/7l0;

    .line 1672223
    iput-object p6, p0, LX/AR1;->f:LX/0ad;

    .line 1672224
    return-void
.end method

.method private d()Z
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1672212
    iget-object v0, p0, LX/AR1;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1672213
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->m(LX/0Px;)Z

    move-result v0

    .line 1672214
    if-eqz v0, :cond_0

    invoke-direct {p0}, LX/AR1;->e()Z

    move-result v0

    :goto_0
    return v0

    .line 1672215
    :cond_0
    iget-object v0, p0, LX/AR1;->f:LX/0ad;

    sget-short v1, LX/1EB;->al:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 1672216
    goto :goto_0
.end method

.method private e()Z
    .locals 3

    .prologue
    .line 1672120
    iget-object v0, p0, LX/AR1;->f:LX/0ad;

    sget-short v1, LX/1EB;->ak:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1672198
    iget-object v0, p0, LX/AR1;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1672199
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0iu;

    invoke-interface {v1}, LX/0iu;->isBackoutDraft()Z

    move-result v1

    if-eqz v1, :cond_0

    move v0, v2

    .line 1672200
    :goto_0
    return v0

    .line 1672201
    :cond_0
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j3;

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->shouldUseOptimisticPosting()Z

    move-result v1

    if-nez v1, :cond_1

    move v0, v2

    .line 1672202
    goto :goto_0

    .line 1672203
    :cond_1
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0jF;

    invoke-interface {v1}, LX/0jF;->getPublishMode()LX/5Rn;

    move-result-object v1

    sget-object v3, LX/5Rn;->NORMAL:LX/5Rn;

    if-eq v1, v3, :cond_2

    move v0, v2

    .line 1672204
    goto :goto_0

    .line 1672205
    :cond_2
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/7kq;->m(LX/0Px;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-direct {p0}, LX/AR1;->e()Z

    move-result v1

    if-nez v1, :cond_3

    move v0, v2

    .line 1672206
    goto :goto_0

    .line 1672207
    :cond_3
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0ip;

    invoke-interface {v1}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0ip;

    invoke-interface {v0}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->f()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v2

    .line 1672208
    goto :goto_0

    .line 1672209
    :cond_4
    iget-object v0, p0, LX/AR1;->c:LX/ARN;

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/AR1;->c:LX/ARN;

    invoke-interface {v0}, LX/ARN;->a()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v2

    .line 1672210
    goto :goto_0

    .line 1672211
    :cond_5
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()LX/9A3;
    .locals 11

    .prologue
    const/4 v4, 0x0

    .line 1672121
    iget-object v0, p0, LX/AR1;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1672122
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j4;

    invoke-interface {v1}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v1

    invoke-static {v1}, LX/2rm;->a(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1672123
    new-instance v2, LX/0XI;

    invoke-direct {v2}, LX/0XI;-><init>()V

    sget-object v3, LX/0XG;->FACEBOOK_OBJECT:LX/0XG;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j6;

    invoke-interface {v1}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    iget-wide v6, v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    move-result-object v2

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j4;

    invoke-interface {v1}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getPageName()Ljava/lang/String;

    move-result-object v1

    .line 1672124
    iput-object v1, v2, LX/0XI;->h:Ljava/lang/String;

    .line 1672125
    move-object v2, v2

    .line 1672126
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j4;

    invoke-interface {v1}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getPageProfilePicUrl()Ljava/lang/String;

    move-result-object v1

    .line 1672127
    iput-object v1, v2, LX/0XI;->n:Ljava/lang/String;

    .line 1672128
    move-object v1, v2

    .line 1672129
    invoke-virtual {v1}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v1

    move-object v2, v1

    :goto_0
    move-object v1, v0

    .line 1672130
    check-cast v1, LX/0ik;

    invoke-interface {v1}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2rg;

    invoke-interface {v1}, LX/2rg;->n()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1672131
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j8;

    invoke-interface {v1}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->f()Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    move-result-object v1

    move-object v3, v1

    .line 1672132
    :goto_1
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0jD;

    invoke-interface {v1}, LX/0jD;->getTaggedUsers()LX/0Px;

    move-result-object v5

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v6

    iget-object v7, p0, LX/AR1;->e:LX/7l0;

    iget-object v1, p0, LX/AR1;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    .line 1672133
    iget-object v8, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, v8

    .line 1672134
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v5, v6, v7, v8, v9}, LX/7ky;->a(LX/0Px;LX/0Px;LX/7l0;J)LX/0Px;

    move-result-object v5

    .line 1672135
    iget-object v6, p0, LX/AR1;->d:LX/9A4;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j2;

    invoke-interface {v1}, LX/0j2;->getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-virtual {v6, v7, v5, v1}, LX/9A4;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;LX/0Px;LX/0Px;)LX/9A3;

    move-result-object v5

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j3;

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getAttachedStory()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 1672136
    iput-object v1, v5, LX/9A3;->n:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1672137
    move-object v5, v5

    .line 1672138
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j0;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v1

    .line 1672139
    iput-object v1, v5, LX/9A3;->j:Ljava/lang/String;

    .line 1672140
    move-object v5, v5

    .line 1672141
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j8;

    invoke-interface {v1}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v1

    .line 1672142
    iput-object v1, v5, LX/9A3;->l:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1672143
    move-object v5, v5

    .line 1672144
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0ip;

    invoke-interface {v1}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v1

    .line 1672145
    iput-object v1, v5, LX/9A3;->q:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1672146
    move-object v5, v5

    .line 1672147
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0jE;

    invoke-interface {v1}, LX/0jE;->getProductItemAttachment()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v1

    .line 1672148
    iput-object v1, v5, LX/9A3;->o:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    .line 1672149
    move-object v5, v5

    .line 1672150
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j1;

    invoke-interface {v1}, LX/0j1;->getTargetAlbum()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v1

    .line 1672151
    iput-object v1, v5, LX/9A3;->k:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 1672152
    iget-object v6, v5, LX/9A3;->h:LX/9A6;

    .line 1672153
    iput-object v1, v6, LX/9A6;->l:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 1672154
    move-object v5, v5

    .line 1672155
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j6;

    invoke-interface {v1}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    iget-wide v6, v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    .line 1672156
    iput-wide v6, v5, LX/9A3;->p:J

    .line 1672157
    move-object v1, v5

    .line 1672158
    iput-object v3, v1, LX/9A3;->m:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    .line 1672159
    move-object v3, v1

    .line 1672160
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0iq;

    invoke-interface {v1}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->f:Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;

    .line 1672161
    iput-object v1, v3, LX/9A3;->r:Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;

    .line 1672162
    move-object v3, v3

    .line 1672163
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j6;

    invoke-interface {v1}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    .line 1672164
    iput-object v1, v3, LX/9A3;->s:Ljava/lang/String;

    .line 1672165
    move-object v3, v3

    .line 1672166
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j6;

    invoke-interface {v1}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetProfilePicUrl:Ljava/lang/String;

    .line 1672167
    iput-object v1, v3, LX/9A3;->t:Ljava/lang/String;

    .line 1672168
    move-object v1, v3

    .line 1672169
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1672170
    iput-object v2, v1, LX/9A3;->u:Lcom/facebook/user/model/User;

    .line 1672171
    move-object v1, v1

    .line 1672172
    iget-object v2, p0, LX/AR1;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0il;

    .line 1672173
    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0io;

    check-cast v3, LX/0jB;

    invoke-interface {v3}, LX/0jB;->getReferencedStickerData()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v3

    if-nez v3, :cond_4

    .line 1672174
    const/4 v2, 0x0

    .line 1672175
    :goto_2
    move-object v2, v2

    .line 1672176
    iput-object v2, v1, LX/9A3;->x:Ljava/lang/String;

    .line 1672177
    move-object v2, v1

    .line 1672178
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j5;

    invoke-interface {v1}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v1

    .line 1672179
    iput-object v1, v2, LX/9A3;->A:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    .line 1672180
    move-object v1, v2

    .line 1672181
    invoke-direct {p0}, LX/AR1;->d()Z

    move-result v2

    .line 1672182
    iget-object v3, v1, LX/9A3;->h:LX/9A6;

    .line 1672183
    iput-boolean v2, v3, LX/9A6;->j:Z

    .line 1672184
    move-object v2, v1

    .line 1672185
    move-object v1, v0

    check-cast v1, LX/0ik;

    invoke-interface {v1}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2rg;

    check-cast v1, LX/5RE;

    invoke-interface {v1}, LX/5RE;->I()LX/5RF;

    move-result-object v1

    sget-object v3, LX/5RF;->SLIDESHOW:LX/5RF;

    if-ne v1, v3, :cond_2

    const/4 v1, 0x1

    .line 1672186
    :goto_3
    iget-object v3, v2, LX/9A3;->h:LX/9A6;

    .line 1672187
    iput-boolean v1, v3, LX/9A6;->i:Z

    .line 1672188
    move-object v2, v2

    .line 1672189
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0jH;

    invoke-interface {v1}, LX/0jH;->getPromptAnalytics()Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-result-object v1

    .line 1672190
    iput-object v1, v2, LX/9A3;->C:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 1672191
    move-object v2, v2

    .line 1672192
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j9;

    invoke-interface {v1}, LX/0j9;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v1

    instance-of v1, v1, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0j9;

    invoke-interface {v0}, LX/0j9;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v4

    .line 1672193
    :cond_0
    iput-object v4, v2, LX/9A3;->B:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    .line 1672194
    move-object v0, v2

    .line 1672195
    return-object v0

    .line 1672196
    :cond_1
    iget-object v1, p0, LX/AR1;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    move-object v2, v1

    goto/16 :goto_0

    .line 1672197
    :cond_2
    const/4 v1, 0x0

    goto :goto_3

    :cond_3
    move-object v3, v4

    goto/16 :goto_1

    :cond_4
    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0io;

    check-cast v3, LX/0jB;

    invoke-interface {v3}, LX/0jB;->getReferencedStickerData()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getStaticDiskUri()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0io;

    check-cast v2, LX/0jB;

    invoke-interface {v2}, LX/0jB;->getReferencedStickerData()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getStaticDiskUri()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    :cond_5
    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0io;

    check-cast v2, LX/0jB;

    invoke-interface {v2}, LX/0jB;->getReferencedStickerData()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getStaticWebUri()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2
.end method
