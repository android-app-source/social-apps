.class public LX/9nT;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1537338
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(I)I
    .locals 2

    .prologue
    .line 1537347
    ushr-int/lit8 v0, p0, 0x18

    .line 1537348
    const/16 v1, 0xff

    if-ne v0, v1, :cond_0

    .line 1537349
    const/4 v0, -0x1

    .line 1537350
    :goto_0
    return v0

    .line 1537351
    :cond_0
    if-nez v0, :cond_1

    .line 1537352
    const/4 v0, -0x2

    goto :goto_0

    .line 1537353
    :cond_1
    const/4 v0, -0x3

    goto :goto_0
.end method

.method public static a(II)I
    .locals 3

    .prologue
    const v2, 0xffffff

    .line 1537339
    const/16 v0, 0xff

    if-ne p1, v0, :cond_0

    .line 1537340
    :goto_0
    return p0

    .line 1537341
    :cond_0
    if-nez p1, :cond_1

    .line 1537342
    and-int/2addr p0, v2

    goto :goto_0

    .line 1537343
    :cond_1
    shr-int/lit8 v0, p1, 0x7

    add-int/2addr v0, p1

    .line 1537344
    ushr-int/lit8 v1, p0, 0x18

    .line 1537345
    mul-int/2addr v0, v1

    shr-int/lit8 v0, v0, 0x8

    .line 1537346
    shl-int/lit8 v0, v0, 0x18

    and-int v1, p0, v2

    or-int p0, v0, v1

    goto :goto_0
.end method
