.class public LX/9BO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/9BP;",
            "LX/0Or",
            "<+",
            "LX/9BM;",
            ">;>;"
        }
    .end annotation
.end field

.field private b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/9BP;",
            "LX/9BM;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0Or;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/9BQ;",
            ">;",
            "LX/0Or",
            "<",
            "LX/9BN;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1451680
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1451681
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    .line 1451682
    sget-object v1, LX/9BP;->THROB:LX/9BP;

    invoke-virtual {v0, v1, p1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1451683
    sget-object v1, LX/9BP;->BLINK:LX/9BP;

    invoke-virtual {v0, v1, p2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1451684
    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/9BO;->a:LX/0P1;

    .line 1451685
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/9BO;->b:Ljava/util/Map;

    .line 1451686
    return-void
.end method
