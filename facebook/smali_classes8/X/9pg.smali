.class public final LX/9pg;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 16

    .prologue
    .line 1545130
    const/4 v12, 0x0

    .line 1545131
    const/4 v11, 0x0

    .line 1545132
    const/4 v10, 0x0

    .line 1545133
    const/4 v9, 0x0

    .line 1545134
    const/4 v8, 0x0

    .line 1545135
    const/4 v7, 0x0

    .line 1545136
    const/4 v6, 0x0

    .line 1545137
    const/4 v5, 0x0

    .line 1545138
    const/4 v4, 0x0

    .line 1545139
    const/4 v3, 0x0

    .line 1545140
    const/4 v2, 0x0

    .line 1545141
    const/4 v1, 0x0

    .line 1545142
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v13, v14, :cond_1

    .line 1545143
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1545144
    const/4 v1, 0x0

    .line 1545145
    :goto_0
    return v1

    .line 1545146
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1545147
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->END_OBJECT:LX/15z;

    if-eq v13, v14, :cond_a

    .line 1545148
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v13

    .line 1545149
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1545150
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v14, v15, :cond_1

    if-eqz v13, :cond_1

    .line 1545151
    const-string v14, "first_team_fan_count"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 1545152
    const/4 v3, 0x1

    .line 1545153
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v12

    goto :goto_1

    .line 1545154
    :cond_2
    const-string v14, "first_team_object"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 1545155
    invoke-static/range {p0 .. p1}, LX/9pb;->a(LX/15w;LX/186;)I

    move-result v11

    goto :goto_1

    .line 1545156
    :cond_3
    const-string v14, "first_team_primary_color"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 1545157
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 1545158
    :cond_4
    const-string v14, "id"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 1545159
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 1545160
    :cond_5
    const-string v14, "match_page"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 1545161
    invoke-static/range {p0 .. p1}, LX/9pc;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1545162
    :cond_6
    const-string v14, "second_team_fan_count"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 1545163
    const/4 v2, 0x1

    .line 1545164
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v7

    goto :goto_1

    .line 1545165
    :cond_7
    const-string v14, "second_team_object"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 1545166
    invoke-static/range {p0 .. p1}, LX/9pf;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 1545167
    :cond_8
    const-string v14, "second_team_primary_color"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 1545168
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto/16 :goto_1

    .line 1545169
    :cond_9
    const-string v14, "viewer_can_vote_fan_favorite"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 1545170
    const/4 v1, 0x1

    .line 1545171
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v4

    goto/16 :goto_1

    .line 1545172
    :cond_a
    const/16 v13, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 1545173
    if-eqz v3, :cond_b

    .line 1545174
    const/4 v3, 0x0

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12, v13}, LX/186;->a(III)V

    .line 1545175
    :cond_b
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 1545176
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 1545177
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 1545178
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 1545179
    if-eqz v2, :cond_c

    .line 1545180
    const/4 v2, 0x5

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7, v3}, LX/186;->a(III)V

    .line 1545181
    :cond_c
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1545182
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 1545183
    if-eqz v1, :cond_d

    .line 1545184
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v4}, LX/186;->a(IZ)V

    .line 1545185
    :cond_d
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1545186
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1545187
    invoke-virtual {p0, p1, v2, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1545188
    if-eqz v0, :cond_0

    .line 1545189
    const-string v1, "first_team_fan_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1545190
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1545191
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1545192
    if-eqz v0, :cond_1

    .line 1545193
    const-string v1, "first_team_object"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1545194
    invoke-static {p0, v0, p2, p3}, LX/9pb;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1545195
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1545196
    if-eqz v0, :cond_2

    .line 1545197
    const-string v1, "first_team_primary_color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1545198
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1545199
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1545200
    if-eqz v0, :cond_3

    .line 1545201
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1545202
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1545203
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1545204
    if-eqz v0, :cond_4

    .line 1545205
    const-string v1, "match_page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1545206
    invoke-static {p0, v0, p2}, LX/9pc;->a(LX/15i;ILX/0nX;)V

    .line 1545207
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1545208
    if-eqz v0, :cond_5

    .line 1545209
    const-string v1, "second_team_fan_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1545210
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1545211
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1545212
    if-eqz v0, :cond_6

    .line 1545213
    const-string v1, "second_team_object"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1545214
    invoke-static {p0, v0, p2, p3}, LX/9pf;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1545215
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1545216
    if-eqz v0, :cond_7

    .line 1545217
    const-string v1, "second_team_primary_color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1545218
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1545219
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1545220
    if-eqz v0, :cond_8

    .line 1545221
    const-string v1, "viewer_can_vote_fan_favorite"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1545222
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1545223
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1545224
    return-void
.end method
