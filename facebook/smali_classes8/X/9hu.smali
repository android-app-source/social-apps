.class public LX/9hu;
.super LX/9hk;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/facebook/tagging/model/TaggingProfile;Landroid/graphics/PointF;)V
    .locals 10

    .prologue
    .line 1527075
    iget-wide v8, p2, Lcom/facebook/tagging/model/TaggingProfile;->b:J

    move-wide v0, v8

    .line 1527076
    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    .line 1527077
    iget-object v0, p2, Lcom/facebook/tagging/model/TaggingProfile;->a:Lcom/facebook/user/model/Name;

    move-object v0, v0

    .line 1527078
    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v3

    iget v0, p3, Landroid/graphics/PointF;->x:F

    float-to-double v4, v0

    iget v0, p3, Landroid/graphics/PointF;->y:F

    float-to-double v6, v0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, LX/9hu;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;DD)V

    .line 1527079
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;DD)V
    .locals 8

    .prologue
    .line 1527080
    new-instance v0, LX/9ht;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    move-wide v6, p6

    invoke-direct/range {v0 .. v7}, LX/9ht;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;DD)V

    invoke-direct {p0, p1, v0}, LX/9hk;-><init>(Ljava/lang/String;LX/9hl;)V

    .line 1527081
    return-void
.end method
