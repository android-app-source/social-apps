.class public LX/9iY;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;

.field private static final b:[Ljava/lang/String;


# instance fields
.field private final c:LX/74n;

.field public final d:Landroid/content/Context;

.field public final e:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation
.end field

.field private final f:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation
.end field

.field public g:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1527958
    const-class v0, LX/9iY;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/9iY;->a:Ljava/lang/String;

    .line 1527959
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v2, v0, v1

    sput-object v0, LX/9iY;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/74n;Landroid/content/Context;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;LX/0Ot;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p4    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/74n;",
            "Landroid/content/Context;",
            "Ljava/util/concurrent/ExecutorService;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1527960
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1527961
    iput-object p1, p0, LX/9iY;->c:LX/74n;

    .line 1527962
    iput-object p2, p0, LX/9iY;->d:Landroid/content/Context;

    .line 1527963
    iput-object p3, p0, LX/9iY;->e:Ljava/util/concurrent/ExecutorService;

    .line 1527964
    iput-object p4, p0, LX/9iY;->f:Ljava/util/concurrent/ExecutorService;

    .line 1527965
    iput-object p5, p0, LX/9iY;->h:LX/0Ot;

    .line 1527966
    return-void
.end method

.method public static a(I)LX/9iX;
    .locals 2

    .prologue
    .line 1527967
    packed-switch p0, :pswitch_data_0

    .line 1527968
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "unknown request code"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1527969
    :pswitch_0
    sget-object v0, LX/9iX;->VIDEO:LX/9iX;

    .line 1527970
    :goto_0
    return-object v0

    :pswitch_1
    sget-object v0, LX/9iX;->IMAGE:LX/9iX;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7d2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static a$redex0(LX/9iY;Landroid/net/Uri;LX/9Tq;)V
    .locals 3

    .prologue
    .line 1527971
    iget-object v0, p0, LX/9iY;->c:LX/74n;

    sget-object v1, LX/74j;->SINGLE_SHOT_CAMERA:LX/74j;

    invoke-virtual {v0, p1, v1}, LX/74n;->a(Landroid/net/Uri;LX/74j;)Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    .line 1527972
    if-nez v0, :cond_0

    .line 1527973
    :goto_0
    return-void

    .line 1527974
    :cond_0
    iget-object v1, p0, LX/9iY;->f:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/photos/simplecamera/SimpleCamera$2;

    invoke-direct {v2, p0, p2, v0}, Lcom/facebook/photos/simplecamera/SimpleCamera$2;-><init>(LX/9iY;LX/9Tq;Lcom/facebook/ipc/media/MediaItem;)V

    const v0, 0x39fac775

    invoke-static {v1, v2, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/9iY;
    .locals 6

    .prologue
    .line 1527975
    new-instance v0, LX/9iY;

    invoke-static {p0}, LX/74n;->b(LX/0QB;)LX/74n;

    move-result-object v1

    check-cast v1, LX/74n;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    const/16 v5, 0x259

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/9iY;-><init>(LX/74n;Landroid/content/Context;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;LX/0Ot;)V

    .line 1527976
    return-object v0
.end method

.method public static c(LX/9iY;)Ljava/io/File;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v6, 0x0

    .line 1527977
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0

    const-string v0, "_data"

    aput-object v0, v2, v3

    .line 1527978
    :try_start_0
    iget-object v0, p0, LX/9iY;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "_id DESC LIMIT 1"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1527979
    if-eqz v1, :cond_0

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-gtz v0, :cond_3

    .line 1527980
    :cond_0
    if-eqz v1, :cond_1

    .line 1527981
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    move-object v0, v6

    .line 1527982
    :cond_2
    :goto_0
    return-object v0

    .line 1527983
    :cond_3
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1527984
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1527985
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1527986
    if-eqz v1, :cond_2

    .line 1527987
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1527988
    :cond_4
    if-eqz v1, :cond_5

    .line 1527989
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_5
    :goto_1
    move-object v0, v6

    .line 1527990
    goto :goto_0

    .line 1527991
    :catch_0
    move-object v0, v6

    :goto_2
    if-eqz v0, :cond_5

    .line 1527992
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 1527993
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_3
    if-eqz v1, :cond_6

    .line 1527994
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0

    .line 1527995
    :catchall_1
    move-exception v0

    goto :goto_3

    :catch_1
    move-object v0, v1

    goto :goto_2
.end method


# virtual methods
.method public final a(LX/9iX;Landroid/content/Intent;LX/9Tq;)V
    .locals 3
    .param p2    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1527996
    iget-object v0, p0, LX/9iY;->e:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/photos/simplecamera/SimpleCamera$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/facebook/photos/simplecamera/SimpleCamera$1;-><init>(LX/9iY;LX/9iX;Landroid/content/Intent;LX/9Tq;)V

    const v2, 0x24ac350f

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1527997
    return-void
.end method

.method public final b(LX/9iX;)Landroid/content/Intent;
    .locals 7

    .prologue
    .line 1527998
    sget-object v0, LX/9iW;->a:[I

    invoke-virtual {p1}, LX/9iX;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1527999
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid camera type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1528000
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1528001
    new-instance v3, Ljava/io/File;

    sget-object v4, Landroid/os/Environment;->DIRECTORY_PICTURES:Ljava/lang/String;

    invoke-static {v4}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    const-string v5, "Facebook"

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1528002
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1528003
    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1528004
    iget-object v3, p0, LX/9iY;->h:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/03V;

    sget-object v4, LX/9iY;->a:Ljava/lang/String;

    const-string v5, "Can not create directory to store new photos"

    invoke-virtual {v3, v4, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1528005
    const/4 v3, 0x0

    .line 1528006
    :goto_0
    move-object v1, v3

    .line 1528007
    iput-object v1, p0, LX/9iY;->g:Landroid/net/Uri;

    .line 1528008
    const-string v1, "output"

    iget-object v2, p0, LX/9iY;->g:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1528009
    :goto_1
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1528010
    return-object v0

    .line 1528011
    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.VIDEO_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1528012
    const/4 v1, 0x0

    iput-object v1, p0, LX/9iY;->g:Landroid/net/Uri;

    goto :goto_1

    .line 1528013
    :cond_0
    const-string v4, "FB_IMG_%d.jpg"

    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1528014
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v5}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
