.class public final enum LX/9HA;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9HA;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9HA;

.field public static final enum NO_OFFSET:LX/9HA;

.field public static final enum PROFILE_PICTURE_OFFSET:LX/9HA;

.field public static final enum THREADED_PROFILE_PICTURE_OFFSET:LX/9HA;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1460920
    new-instance v0, LX/9HA;

    const-string v1, "NO_OFFSET"

    invoke-direct {v0, v1, v2}, LX/9HA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9HA;->NO_OFFSET:LX/9HA;

    .line 1460921
    new-instance v0, LX/9HA;

    const-string v1, "PROFILE_PICTURE_OFFSET"

    invoke-direct {v0, v1, v3}, LX/9HA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9HA;->PROFILE_PICTURE_OFFSET:LX/9HA;

    .line 1460922
    new-instance v0, LX/9HA;

    const-string v1, "THREADED_PROFILE_PICTURE_OFFSET"

    invoke-direct {v0, v1, v4}, LX/9HA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9HA;->THREADED_PROFILE_PICTURE_OFFSET:LX/9HA;

    .line 1460923
    const/4 v0, 0x3

    new-array v0, v0, [LX/9HA;

    sget-object v1, LX/9HA;->NO_OFFSET:LX/9HA;

    aput-object v1, v0, v2

    sget-object v1, LX/9HA;->PROFILE_PICTURE_OFFSET:LX/9HA;

    aput-object v1, v0, v3

    sget-object v1, LX/9HA;->THREADED_PROFILE_PICTURE_OFFSET:LX/9HA;

    aput-object v1, v0, v4

    sput-object v0, LX/9HA;->$VALUES:[LX/9HA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1460917
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9HA;
    .locals 1

    .prologue
    .line 1460919
    const-class v0, LX/9HA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9HA;

    return-object v0
.end method

.method public static values()[LX/9HA;
    .locals 1

    .prologue
    .line 1460918
    sget-object v0, LX/9HA;->$VALUES:[LX/9HA;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9HA;

    return-object v0
.end method
