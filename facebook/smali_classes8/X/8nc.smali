.class public LX/8nc;
.super LX/8nB;
.source ""


# instance fields
.field private final a:Ljava/lang/Long;

.field private final b:LX/0tX;

.field private final c:Ljava/util/concurrent/ExecutorService;

.field private final d:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Ljava/lang/Long;LX/0tX;Ljava/util/concurrent/ExecutorService;Landroid/content/res/Resources;)V
    .locals 0
    .param p1    # Ljava/lang/Long;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1401243
    invoke-direct {p0}, LX/8nB;-><init>()V

    .line 1401244
    iput-object p1, p0, LX/8nc;->a:Ljava/lang/Long;

    .line 1401245
    iput-object p2, p0, LX/8nc;->b:LX/0tX;

    .line 1401246
    iput-object p3, p0, LX/8nc;->c:Ljava/util/concurrent/ExecutorService;

    .line 1401247
    iput-object p4, p0, LX/8nc;->d:Landroid/content/res/Resources;

    .line 1401248
    return-void
.end method


# virtual methods
.method public final a(LX/8JX;)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 1401249
    const-string v1, ""

    const-string v2, ""

    const/4 v7, 0x1

    move-object v0, p0

    move v4, v3

    move v5, v3

    move v6, v3

    move-object v8, p1

    invoke-virtual/range {v0 .. v8}, LX/8nB;->a(Ljava/lang/CharSequence;Ljava/lang/String;ZZZZZLX/8JX;)V

    .line 1401250
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/lang/String;ZZZZZLX/8JX;)V
    .locals 4

    .prologue
    .line 1401251
    new-instance v0, LX/8oA;

    invoke-direct {v0}, LX/8oA;-><init>()V

    move-object v0, v0

    .line 1401252
    const-string v1, "page_id"

    iget-object v2, p0, LX/8nc;->a:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "query"

    invoke-virtual {v1, v2, p1}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    move-result-object v1

    const-string v2, "first"

    const/16 v3, 0xa

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1401253
    iget-object v1, p0, LX/8nc;->b:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    new-instance v1, LX/8na;

    invoke-direct {v1, p0, p8, p1}, LX/8na;-><init>(LX/8nc;LX/8JX;Ljava/lang/CharSequence;)V

    iget-object v2, p0, LX/8nc;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1401254
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1401255
    const/4 v0, 0x1

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1401256
    const-string v0, "product_data_source"

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1401257
    sget-object v0, LX/8nE;->PRODUCTS:LX/8nE;

    invoke-virtual {v0}, LX/8nE;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1401258
    iget-object v0, p0, LX/8nc;->a:Ljava/lang/Long;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1401259
    iget-object v0, p0, LX/8nc;->d:Landroid/content/res/Resources;

    const v1, 0x7f0813a1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
