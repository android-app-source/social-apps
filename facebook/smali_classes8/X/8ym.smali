.class public final LX/8ym;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/8ym;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/8yk;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/8yn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1426470
    const/4 v0, 0x0

    sput-object v0, LX/8ym;->a:LX/8ym;

    .line 1426471
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/8ym;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1426472
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1426473
    new-instance v0, LX/8yn;

    invoke-direct {v0}, LX/8yn;-><init>()V

    iput-object v0, p0, LX/8ym;->c:LX/8yn;

    .line 1426474
    return-void
.end method

.method public static c(LX/1De;)LX/8yk;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1426475
    new-instance v1, LX/8yl;

    invoke-direct {v1}, LX/8yl;-><init>()V

    .line 1426476
    sget-object v2, LX/8ym;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8yk;

    .line 1426477
    if-nez v2, :cond_0

    .line 1426478
    new-instance v2, LX/8yk;

    invoke-direct {v2}, LX/8yk;-><init>()V

    .line 1426479
    :cond_0
    invoke-static {v2, p0, v0, v0, v1}, LX/8yk;->a$redex0(LX/8yk;LX/1De;IILX/8yl;)V

    .line 1426480
    move-object v1, v2

    .line 1426481
    move-object v0, v1

    .line 1426482
    return-object v0
.end method

.method public static declared-synchronized q()LX/8ym;
    .locals 2

    .prologue
    .line 1426483
    const-class v1, LX/8ym;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/8ym;->a:LX/8ym;

    if-nez v0, :cond_0

    .line 1426484
    new-instance v0, LX/8ym;

    invoke-direct {v0}, LX/8ym;-><init>()V

    sput-object v0, LX/8ym;->a:LX/8ym;

    .line 1426485
    :cond_0
    sget-object v0, LX/8ym;->a:LX/8ym;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1426486
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 1426487
    check-cast p2, LX/8yl;

    .line 1426488
    iget-object v1, p2, LX/8yl;->a:Ljava/lang/CharSequence;

    iget v2, p2, LX/8yl;->b:I

    iget-object v3, p2, LX/8yl;->c:LX/1dc;

    iget-object v4, p2, LX/8yl;->d:LX/1dc;

    iget v5, p2, LX/8yl;->e:I

    move-object v0, p1

    const/4 p2, 0x2

    const/4 p0, 0x1

    const/4 p1, 0x0

    .line 1426489
    invoke-static {v0}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    .line 1426490
    invoke-interface {v6, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, p0}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, p2}, LX/1Dh;->S(I)LX/1Dh;

    .line 1426491
    if-eqz v4, :cond_0

    .line 1426492
    invoke-static {v0}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v7

    invoke-virtual {v7, v4}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    invoke-interface {v7, p0}, LX/1Di;->c(I)LX/1Di;

    move-result-object v7

    const/16 p0, 0x8

    invoke-interface {v7, p0, p1}, LX/1Di;->k(II)LX/1Di;

    move-result-object v7

    invoke-interface {v6, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 1426493
    :cond_0
    if-eqz v3, :cond_1

    .line 1426494
    invoke-static {v0}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v7

    invoke-virtual {v7, v3}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    const/4 p0, 0x6

    invoke-interface {v7, p0, v2}, LX/1Di;->e(II)LX/1Di;

    move-result-object v7

    invoke-interface {v6, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 1426495
    :cond_1
    invoke-static {v0, p1, v5}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v7

    invoke-virtual {v7, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v7

    invoke-interface {v6, v7}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    .line 1426496
    invoke-interface {v6}, LX/1Di;->k()LX/1Dg;

    move-result-object v6

    move-object v0, v6

    .line 1426497
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1426498
    invoke-static {}, LX/1dS;->b()V

    .line 1426499
    const/4 v0, 0x0

    return-object v0
.end method
