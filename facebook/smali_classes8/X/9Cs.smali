.class public final enum LX/9Cs;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9Cs;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9Cs;

.field public static final enum DELETE:LX/9Cs;

.field public static final enum UPDATE:LX/9Cs;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1454447
    new-instance v0, LX/9Cs;

    const-string v1, "UPDATE"

    invoke-direct {v0, v1, v2}, LX/9Cs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9Cs;->UPDATE:LX/9Cs;

    .line 1454448
    new-instance v0, LX/9Cs;

    const-string v1, "DELETE"

    invoke-direct {v0, v1, v3}, LX/9Cs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9Cs;->DELETE:LX/9Cs;

    .line 1454449
    const/4 v0, 0x2

    new-array v0, v0, [LX/9Cs;

    sget-object v1, LX/9Cs;->UPDATE:LX/9Cs;

    aput-object v1, v0, v2

    sget-object v1, LX/9Cs;->DELETE:LX/9Cs;

    aput-object v1, v0, v3

    sput-object v0, LX/9Cs;->$VALUES:[LX/9Cs;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1454450
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9Cs;
    .locals 1

    .prologue
    .line 1454451
    const-class v0, LX/9Cs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9Cs;

    return-object v0
.end method

.method public static values()[LX/9Cs;
    .locals 1

    .prologue
    .line 1454452
    sget-object v0, LX/9Cs;->$VALUES:[LX/9Cs;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9Cs;

    return-object v0
.end method
