.class public final LX/9GX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;)V
    .locals 0

    .prologue
    .line 1460012
    iput-object p1, p0, LX/9GX;->a:Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x128b81bd

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1460013
    check-cast p1, LX/9HJ;

    .line 1460014
    iget-object v1, p1, Lcom/facebook/attachments/ui/AttachmentViewVideo;->d:Lcom/facebook/video/player/RichVideoPlayer;

    move-object v1, v1

    .line 1460015
    if-nez v1, :cond_0

    .line 1460016
    const v1, 0x73d613e7

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1460017
    :goto_0
    return-void

    .line 1460018
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1460019
    sget-object v2, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 1460020
    :goto_1
    const v1, -0x2075c37

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0

    .line 1460021
    :cond_1
    sget-object v2, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    goto :goto_1
.end method
