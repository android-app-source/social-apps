.class public LX/93D;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1433531
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Intent;)Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;
    .locals 2

    .prologue
    .line 1433522
    const-string v0, "minutiae_configuration"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1433523
    const-string v0, "minutiae_configuration"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1433524
    iget-object v1, v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v1, v1

    .line 1433525
    if-nez v1, :cond_0

    .line 1433526
    invoke-static {v0}, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->a(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)LX/937;

    move-result-object v0

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1433527
    iput-object v1, v0, LX/937;->l:Ljava/lang/String;

    .line 1433528
    move-object v0, v0

    .line 1433529
    invoke-virtual {v0}, LX/937;->a()Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    move-result-object v0

    .line 1433530
    :cond_0
    return-object v0
.end method

.method public static a(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)Z
    .locals 1

    .prologue
    .line 1433514
    iget-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v0, v0

    .line 1433515
    if-eqz v0, :cond_0

    .line 1433516
    iget-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v0, v0

    .line 1433517
    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->q()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;
    .locals 1

    .prologue
    .line 1433518
    iget-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v0, v0

    .line 1433519
    if-eqz v0, :cond_0

    .line 1433520
    iget-object v0, p0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v0, v0

    .line 1433521
    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->q()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
