.class public final LX/97j;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Z

.field public b:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1442138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;)LX/97j;
    .locals 2

    .prologue
    .line 1442139
    new-instance v0, LX/97j;

    invoke-direct {v0}, LX/97j;-><init>()V

    .line 1442140
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;->a()Z

    move-result v1

    iput-boolean v1, v0, LX/97j;->a:Z

    .line 1442141
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;->j()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;

    move-result-object v1

    iput-object v1, v0, LX/97j;->b:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;

    .line 1442142
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 1442143
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1442144
    iget-object v1, p0, LX/97j;->b:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1442145
    const/4 v3, 0x2

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1442146
    iget-boolean v3, p0, LX/97j;->a:Z

    invoke-virtual {v0, v5, v3}, LX/186;->a(IZ)V

    .line 1442147
    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1442148
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1442149
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1442150
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1442151
    invoke-virtual {v1, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1442152
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1442153
    new-instance v1, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    invoke-direct {v1, v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;-><init>(LX/15i;)V

    .line 1442154
    return-object v1
.end method
