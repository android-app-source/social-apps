.class public LX/8nM;
.super LX/8nB;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final g:Ljava/lang/Object;


# instance fields
.field private final a:LX/3fr;

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/3Oq;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/3iT;

.field private final d:LX/8JL;

.field private e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/2RQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1400898
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/8nM;->g:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/3fr;LX/3iT;LX/8JL;LX/0Ot;LX/3Oq;LX/2RQ;)V
    .locals 3
    .param p5    # LX/3Oq;
        .annotation runtime Lcom/facebook/contacts/module/ContactLinkQueryType;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3fr;",
            "LX/3iT;",
            "LX/8JL;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/3Oq;",
            "LX/2RQ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1400928
    invoke-direct {p0}, LX/8nB;-><init>()V

    .line 1400929
    iput-object p1, p0, LX/8nM;->a:LX/3fr;

    .line 1400930
    iput-object p2, p0, LX/8nM;->c:LX/3iT;

    .line 1400931
    iput-object p3, p0, LX/8nM;->d:LX/8JL;

    .line 1400932
    iput-object p4, p0, LX/8nM;->e:LX/0Ot;

    .line 1400933
    iput-object p6, p0, LX/8nM;->f:LX/2RQ;

    .line 1400934
    sget-object v0, LX/3Oq;->PAGE:LX/3Oq;

    sget-object v1, LX/3Oq;->ME:LX/3Oq;

    sget-object v2, LX/3Oq;->UNMATCHED:LX/3Oq;

    invoke-static {p5, v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/8nM;->b:LX/0Px;

    .line 1400935
    return-void
.end method

.method public static a(LX/0QB;)LX/8nM;
    .locals 14

    .prologue
    .line 1400899
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 1400900
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 1400901
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 1400902
    if-nez v1, :cond_0

    .line 1400903
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1400904
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 1400905
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 1400906
    sget-object v1, LX/8nM;->g:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1400907
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 1400908
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1400909
    :cond_1
    if-nez v1, :cond_4

    .line 1400910
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 1400911
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 1400912
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 1400913
    new-instance v7, LX/8nM;

    invoke-static {v0}, LX/3fr;->a(LX/0QB;)LX/3fr;

    move-result-object v8

    check-cast v8, LX/3fr;

    invoke-static {v0}, LX/3iT;->b(LX/0QB;)LX/3iT;

    move-result-object v9

    check-cast v9, LX/3iT;

    invoke-static {v0}, LX/8JL;->a(LX/0QB;)LX/8JL;

    move-result-object v10

    check-cast v10, LX/8JL;

    const/16 v11, 0x259

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static {v0}, LX/6NS;->b(LX/0QB;)LX/3Oq;

    move-result-object v12

    check-cast v12, LX/3Oq;

    invoke-static {v0}, LX/2RQ;->b(LX/0QB;)LX/2RQ;

    move-result-object v13

    check-cast v13, LX/2RQ;

    invoke-direct/range {v7 .. v13}, LX/8nM;-><init>(LX/3fr;LX/3iT;LX/8JL;LX/0Ot;LX/3Oq;LX/2RQ;)V

    .line 1400914
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1400915
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 1400916
    if-nez v1, :cond_2

    .line 1400917
    sget-object v0, LX/8nM;->g:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8nM;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1400918
    :goto_1
    if-eqz v0, :cond_3

    .line 1400919
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1400920
    :goto_3
    check-cast v0, LX/8nM;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1400921
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 1400922
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1400923
    :catchall_1
    move-exception v0

    .line 1400924
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1400925
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1400926
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 1400927
    :cond_2
    :try_start_8
    sget-object v0, LX/8nM;->g:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8nM;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private a(Ljava/lang/CharSequence;)Lcom/facebook/tagging/model/TaggingProfile;
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 1400885
    iget-object v0, p0, LX/8nM;->c:LX/3iT;

    new-instance v1, Lcom/facebook/user/model/Name;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v4, v4, v2}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-wide/16 v2, -0x1

    sget-object v5, LX/7Gr;->TEXT:LX/7Gr;

    const-string v6, ""

    sget-object v7, LX/8nE;->TEXT:LX/8nE;

    invoke-virtual {v7}, LX/8nE;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v0 .. v7}, LX/3iT;->a(Lcom/facebook/user/model/Name;JLjava/lang/String;LX/7Gr;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/tagging/model/TaggingProfile;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/6N1;Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6N1;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1400886
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 1400887
    if-nez p1, :cond_0

    move-object v0, v8

    .line 1400888
    :goto_0
    return-object v0

    .line 1400889
    :cond_0
    :goto_1
    invoke-interface {p1}, LX/6N1;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1400890
    invoke-interface {p1}, LX/6N1;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/facebook/contacts/graphql/Contact;

    .line 1400891
    iget-object v0, p0, LX/8nM;->c:LX/3iT;

    invoke-virtual {v5}, Lcom/facebook/contacts/graphql/Contact;->e()Lcom/facebook/user/model/Name;

    move-result-object v1

    invoke-virtual {v5}, Lcom/facebook/contacts/graphql/Contact;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v5}, Lcom/facebook/contacts/graphql/Contact;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5}, Lcom/facebook/contacts/graphql/Contact;->A()LX/2RU;

    move-result-object v5

    .line 1400892
    sget-object v6, LX/8nL;->a:[I

    invoke-virtual {v5}, LX/2RU;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 1400893
    sget-object v6, LX/7Gr;->UNKNOWN:LX/7Gr;

    :goto_2
    move-object v5, v6

    .line 1400894
    move-object v6, p2

    move-object v7, p3

    invoke-virtual/range {v0 .. v7}, LX/3iT;->a(Lcom/facebook/user/model/Name;JLjava/lang/String;LX/7Gr;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/tagging/model/TaggingProfile;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    move-object v0, v8

    .line 1400895
    goto :goto_0

    .line 1400896
    :pswitch_0
    sget-object v6, LX/7Gr;->USER:LX/7Gr;

    goto :goto_2

    .line 1400897
    :pswitch_1
    sget-object v6, LX/7Gr;->PAGE:LX/7Gr;

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Ljava/util/List;Ljava/lang/CharSequence;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;",
            "Ljava/lang/CharSequence;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1400875
    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    .line 1400876
    iget-object v0, p0, LX/8nM;->d:LX/8JL;

    invoke-virtual {v0}, LX/8JL;->a()Ljava/util/List;

    move-result-object v3

    .line 1400877
    if-eqz v3, :cond_0

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1400878
    :cond_0
    return-void

    .line 1400879
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1400880
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/model/TaggingProfile;

    .line 1400881
    iget-object p0, v0, Lcom/facebook/tagging/model/TaggingProfile;->f:Ljava/lang/String;

    move-object v0, p0

    .line 1400882
    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1400883
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1400884
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private b(Ljava/lang/CharSequence;ILjava/lang/String;Z)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "I",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1400845
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    .line 1400846
    const/4 v1, 0x0

    .line 1400847
    :try_start_0
    iget-object v0, p0, LX/8nM;->a:LX/3fr;

    const/4 v4, 0x1

    .line 1400848
    iget-object v2, p0, LX/8nM;->f:LX/2RQ;

    invoke-virtual {v2}, LX/2RQ;->a()LX/2RR;

    move-result-object v2

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1400849
    iput-object v3, v2, LX/2RR;->e:Ljava/lang/String;

    .line 1400850
    move-object v2, v2

    .line 1400851
    iget-object v3, p0, LX/8nM;->b:LX/0Px;

    .line 1400852
    iput-object v3, v2, LX/2RR;->c:Ljava/util/Collection;

    .line 1400853
    move-object v2, v2

    .line 1400854
    sget-object v3, LX/2RS;->NAME:LX/2RS;

    .line 1400855
    iput-object v3, v2, LX/2RR;->n:LX/2RS;

    .line 1400856
    move-object v2, v2

    .line 1400857
    iput p2, v2, LX/2RR;->p:I

    .line 1400858
    move-object v2, v2

    .line 1400859
    const-string v3, "communication_rank"

    invoke-virtual {p3, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1400860
    if-eqz p4, :cond_3

    .line 1400861
    iput-boolean v4, v2, LX/2RR;->k:Z

    .line 1400862
    :cond_0
    :goto_0
    move-object v2, v2

    .line 1400863
    invoke-virtual {v0, v2}, LX/3fr;->a(LX/2RR;)LX/6N1;

    move-result-object v1

    .line 1400864
    const-string v0, "contacts_db"

    sget-object v2, LX/8nE;->FRIENDS:LX/8nE;

    invoke-virtual {v2}, LX/8nE;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, LX/8nM;->a(LX/6N1;Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1400865
    if-eqz v1, :cond_1

    .line 1400866
    invoke-interface {v1}, LX/6N1;->close()V

    .line 1400867
    :cond_1
    return-object v0

    .line 1400868
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    .line 1400869
    invoke-interface {v1}, LX/6N1;->close()V

    :cond_2
    throw v0

    .line 1400870
    :cond_3
    sget-object v3, LX/2RS;->COMMUNICATION_RANK:LX/2RS;

    .line 1400871
    iput-object v3, v2, LX/2RR;->n:LX/2RS;

    .line 1400872
    move-object v3, v2

    .line 1400873
    iput-boolean v4, v3, LX/2RR;->o:Z

    .line 1400874
    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;ZZZZ)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "ZZZZ)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v3, 0x3e8

    .line 1400818
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 1400819
    if-nez p1, :cond_0

    .line 1400820
    :goto_0
    return-object v0

    .line 1400821
    :cond_0
    const-string v0, ""

    .line 1400822
    const/4 v1, 0x0

    invoke-direct {p0, p1, v3, v0, v1}, LX/8nM;->b(Ljava/lang/CharSequence;ILjava/lang/String;Z)Ljava/util/List;

    move-result-object v1

    .line 1400823
    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v2, v3, :cond_1

    .line 1400824
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    rsub-int v2, v2, 0x3e8

    const/4 v3, 0x1

    invoke-direct {p0, p1, v2, v0, v3}, LX/8nM;->b(Ljava/lang/CharSequence;ILjava/lang/String;Z)Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1400825
    :cond_1
    invoke-direct {p0, v1, p1}, LX/8nM;->a(Ljava/util/List;Ljava/lang/CharSequence;)V

    .line 1400826
    if-eqz p5, :cond_2

    .line 1400827
    invoke-direct {p0, p1}, LX/8nM;->a(Ljava/lang/CharSequence;)Lcom/facebook/tagging/model/TaggingProfile;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1400828
    :cond_2
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 1400829
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/model/TaggingProfile;

    .line 1400830
    if-nez p2, :cond_4

    .line 1400831
    iget-object p0, v0, Lcom/facebook/tagging/model/TaggingProfile;->e:LX/7Gr;

    move-object p0, p0

    .line 1400832
    sget-object p1, LX/7Gr;->SELF:LX/7Gr;

    if-eq p0, p1, :cond_3

    .line 1400833
    :cond_4
    if-nez p3, :cond_5

    .line 1400834
    iget-object p0, v0, Lcom/facebook/tagging/model/TaggingProfile;->e:LX/7Gr;

    move-object p0, p0

    .line 1400835
    sget-object p1, LX/7Gr;->USER:LX/7Gr;

    if-eq p0, p1, :cond_3

    .line 1400836
    :cond_5
    if-nez p4, :cond_6

    .line 1400837
    iget-object p0, v0, Lcom/facebook/tagging/model/TaggingProfile;->e:LX/7Gr;

    move-object p0, p0

    .line 1400838
    sget-object p1, LX/7Gr;->PAGE:LX/7Gr;

    if-eq p0, p1, :cond_3

    .line 1400839
    :cond_6
    if-nez p5, :cond_7

    .line 1400840
    iget-object p0, v0, Lcom/facebook/tagging/model/TaggingProfile;->e:LX/7Gr;

    move-object p0, p0

    .line 1400841
    sget-object p1, LX/7Gr;->TEXT:LX/7Gr;

    if-eq p0, p1, :cond_3

    .line 1400842
    :cond_7
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1400843
    :cond_8
    move-object v0, v2

    .line 1400844
    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1400817
    const-string v0, "contacts_db"

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1400813
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1400814
    sget-object v1, LX/8nE;->FRIENDS:LX/8nE;

    invoke-virtual {v1}, LX/8nE;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1400815
    sget-object v1, LX/8nE;->TEXT:LX/8nE;

    invoke-virtual {v1}, LX/8nE;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1400816
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method
