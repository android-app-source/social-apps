.class public LX/9DN;
.super LX/9Bj;
.source ""


# instance fields
.field private a:LX/0hG;

.field private b:Lcom/facebook/content/SecureContextHelper;

.field private c:LX/17Y;

.field private d:Lcom/facebook/ufiservices/flyout/FeedbackParams;


# direct methods
.method public constructor <init>(LX/0hG;Lcom/facebook/ufiservices/flyout/FeedbackParams;Lcom/facebook/content/SecureContextHelper;LX/17Y;LX/1nL;)V
    .locals 1
    .param p1    # LX/0hG;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/ufiservices/flyout/FeedbackParams;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1455586
    invoke-virtual {p2}, Lcom/facebook/ufiservices/flyout/FeedbackParams;->h()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1, p5}, LX/9Bj;-><init>(Ljava/lang/String;LX/0hG;LX/1nL;)V

    .line 1455587
    iput-object p1, p0, LX/9DN;->a:LX/0hG;

    .line 1455588
    iput-object p3, p0, LX/9DN;->b:Lcom/facebook/content/SecureContextHelper;

    .line 1455589
    iput-object p4, p0, LX/9DN;->c:LX/17Y;

    .line 1455590
    iput-object p2, p0, LX/9DN;->d:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1455591
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;ZLcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V
    .locals 5
    .param p2    # Lcom/facebook/graphql/model/GraphQLComment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1455560
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 1455561
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Cannot show replies for reply without an ID"

    invoke-static {v0, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1455562
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v0

    .line 1455563
    :goto_1
    new-instance v2, Lcom/facebook/feedback/ui/ThreadedCommentsFeedbackFragment;

    invoke-direct {v2}, Lcom/facebook/feedback/ui/ThreadedCommentsFeedbackFragment;-><init>()V

    .line 1455564
    new-instance v3, LX/8qL;

    invoke-direct {v3}, LX/8qL;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    .line 1455565
    iput-object v4, v3, LX/8qL;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1455566
    move-object v3, v3

    .line 1455567
    iput-object v0, v3, LX/8qL;->m:Ljava/lang/String;

    .line 1455568
    move-object v0, v3

    .line 1455569
    invoke-static {v1}, LX/16z;->b(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v1

    .line 1455570
    iput-boolean v1, v0, LX/8qL;->p:Z

    .line 1455571
    move-object v0, v0

    .line 1455572
    iput-object p5, v0, LX/8qL;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1455573
    move-object v0, v0

    .line 1455574
    iput-object p3, v0, LX/8qL;->f:Ljava/lang/String;

    .line 1455575
    move-object v0, v0

    .line 1455576
    iput-boolean p4, v0, LX/8qL;->i:Z

    .line 1455577
    move-object v0, v0

    .line 1455578
    iget-object v1, p0, LX/9DN;->d:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1455579
    iget-object v3, v1, Lcom/facebook/ufiservices/flyout/FeedbackParams;->k:Ljava/lang/Long;

    move-object v1, v3

    .line 1455580
    invoke-virtual {v0, v1}, LX/8qL;->a(Ljava/lang/Long;)LX/8qL;

    move-result-object v0

    invoke-virtual {v0}, LX/8qL;->a()Lcom/facebook/ufiservices/flyout/FeedbackParams;

    move-result-object v0

    .line 1455581
    invoke-virtual {v0}, Lcom/facebook/ufiservices/flyout/FeedbackParams;->v()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1455582
    iget-object v0, p0, LX/9DN;->a:LX/0hG;

    invoke-interface {v0, v2}, LX/0hG;->a(LX/8qC;)V

    .line 1455583
    return-void

    .line 1455584
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1455585
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public a(Lcom/facebook/graphql/model/GraphQLComment;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1455556
    sget-object v0, LX/0ax;->bn:Ljava/lang/String;

    invoke-static {v0, p3, p4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1455557
    iget-object v1, p0, LX/9DN;->c:LX/17Y;

    invoke-interface {v1, p2, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1455558
    iget-object v1, p0, LX/9DN;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1455559
    return-void
.end method

.method public a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V
    .locals 6

    .prologue
    .line 1455554
    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/9DN;->a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;ZLcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V

    .line 1455555
    return-void
.end method

.method public a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V
    .locals 6

    .prologue
    .line 1455552
    const/4 v2, 0x0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LX/9DN;->a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;ZLcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V

    .line 1455553
    return-void
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 2

    .prologue
    .line 1455537
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 1455538
    new-instance v1, LX/8qQ;

    invoke-direct {v1}, LX/8qQ;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v0

    .line 1455539
    iput-object v0, v1, LX/8qQ;->a:Ljava/lang/String;

    .line 1455540
    move-object v0, v1

    .line 1455541
    sget-object v1, LX/89l;->LIKERS_FOR_FEEDBACK_ID:LX/89l;

    .line 1455542
    iput-object v1, v0, LX/8qQ;->d:LX/89l;

    .line 1455543
    move-object v0, v0

    .line 1455544
    invoke-virtual {v0}, LX/8qQ;->a()Lcom/facebook/ufiservices/flyout/ProfileListParams;

    move-result-object v0

    .line 1455545
    new-instance v1, Lcom/facebook/ufiservices/flyout/UFIProfileListFragment;

    invoke-direct {v1}, Lcom/facebook/ufiservices/flyout/UFIProfileListFragment;-><init>()V

    .line 1455546
    invoke-virtual {v0}, Lcom/facebook/ufiservices/flyout/ProfileListParams;->m()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1455547
    iget-object v0, p0, LX/9DN;->a:LX/0hG;

    invoke-interface {v0, v1}, LX/0hG;->a(LX/8qC;)V

    .line 1455548
    return-void
.end method

.method public final c(Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 2

    .prologue
    .line 1455549
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-static {v0}, LX/9Ap;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/feedback/reactions/ui/ReactorsListFragment;

    move-result-object v0

    .line 1455550
    iget-object v1, p0, LX/9DN;->a:LX/0hG;

    invoke-interface {v1, v0}, LX/0hG;->a(LX/8qC;)V

    .line 1455551
    return-void
.end method
