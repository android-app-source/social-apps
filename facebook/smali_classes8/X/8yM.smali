.class public LX/8yM;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0tX;

.field private final b:LX/1Ck;


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1425267
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1425268
    iput-object p1, p0, LX/8yM;->a:LX/0tX;

    .line 1425269
    iput-object p2, p0, LX/8yM;->b:LX/1Ck;

    .line 1425270
    return-void
.end method

.method public static b(LX/0QB;)LX/8yM;
    .locals 3

    .prologue
    .line 1425265
    new-instance v2, LX/8yM;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v1

    check-cast v1, LX/1Ck;

    invoke-direct {v2, v0, v1}, LX/8yM;-><init>(LX/0tX;LX/1Ck;)V

    .line 1425266
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;LX/0TF;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLWithinPageEditPlaceListMutationModel;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1425256
    new-instance v0, LX/4KT;

    invoke-direct {v0}, LX/4KT;-><init>()V

    .line 1425257
    const-string v1, "placelist_id"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1425258
    move-object v0, v0

    .line 1425259
    const-string v1, "within_page_id"

    invoke-virtual {v0, v1, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1425260
    move-object v0, v0

    .line 1425261
    new-instance v1, LX/5Hf;

    invoke-direct {v1}, LX/5Hf;-><init>()V

    move-object v1, v1

    .line 1425262
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1425263
    iget-object v0, p0, LX/8yM;->b:LX/1Ck;

    const-string v2, "within_page_edit"

    iget-object v3, p0, LX/8yM;->a:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v3, LX/8yL;

    invoke-direct {v3, p0, p3}, LX/8yL;-><init>(LX/8yM;LX/0TF;)V

    invoke-virtual {v0, v2, v1, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1425264
    return-void
.end method
