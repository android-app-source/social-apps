.class public LX/8yT;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile d:LX/8yT;


# instance fields
.field private final b:LX/8yZ;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1425650
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, LX/8yT;->a:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(LX/8yZ;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8yZ;",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1425651
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1425652
    iput-object p1, p0, LX/8yT;->b:LX/8yZ;

    .line 1425653
    iput-object p2, p0, LX/8yT;->c:LX/0Or;

    .line 1425654
    return-void
.end method

.method private a(Lcom/facebook/common/callercontext/CallerContext;LX/1De;LX/4Ab;IILandroid/net/Uri;)LX/1Di;
    .locals 3

    .prologue
    .line 1425655
    invoke-static {p2}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v0

    invoke-static {p2}, LX/1nf;->a(LX/1De;)LX/1nh;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/1nh;->h(I)LX/1nh;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1up;->a(LX/1n6;)LX/1up;

    move-result-object v1

    iget-object v0, p0, LX/8yT;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, p6}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/1up;->a(LX/4Ab;)LX/1up;

    move-result-object v0

    invoke-static {p2}, LX/1nf;->a(LX/1De;)LX/1nh;

    move-result-object v1

    invoke-virtual {v1, p4}, LX/1nh;->h(I)LX/1nh;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1up;->b(LX/1n6;)LX/1up;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    invoke-interface {v0, p5}, LX/1Di;->o(I)LX/1Di;

    move-result-object v0

    invoke-interface {v0, p5}, LX/1Di;->g(I)LX/1Di;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/8yT;
    .locals 5

    .prologue
    .line 1425656
    sget-object v0, LX/8yT;->d:LX/8yT;

    if-nez v0, :cond_1

    .line 1425657
    const-class v1, LX/8yT;

    monitor-enter v1

    .line 1425658
    :try_start_0
    sget-object v0, LX/8yT;->d:LX/8yT;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1425659
    if-eqz v2, :cond_0

    .line 1425660
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1425661
    new-instance v4, LX/8yT;

    invoke-static {v0}, LX/8yZ;->a(LX/0QB;)LX/8yZ;

    move-result-object v3

    check-cast v3, LX/8yZ;

    const/16 p0, 0x509

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/8yT;-><init>(LX/8yZ;LX/0Or;)V

    .line 1425662
    move-object v0, v4

    .line 1425663
    sput-object v0, LX/8yT;->d:LX/8yT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1425664
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1425665
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1425666
    :cond_1
    sget-object v0, LX/8yT;->d:LX/8yT;

    return-object v0

    .line 1425667
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1425668
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/common/callercontext/CallerContext;IIIIIZLjava/util/List;IIIII)LX/1Dg;
    .locals 17
    .param p2    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_SIZE:LX/32B;
        .end annotation
    .end param
    .param p4    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_SIZE:LX/32B;
        .end annotation
    .end param
    .param p5    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p6    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_SIZE:LX/32B;
        .end annotation
    .end param
    .param p7    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_OFFSET:LX/32B;
        .end annotation
    .end param
    .param p8    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->BOOL:LX/32B;
        .end annotation
    .end param
    .param p9    # Ljava/util/List;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p10    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->INT:LX/32B;
        .end annotation
    .end param
    .param p11    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->INT:LX/32B;
        .end annotation
    .end param
    .param p12    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_SIZE:LX/32B;
        .end annotation
    .end param
    .param p13    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p14    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_SIZE:LX/32B;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "IIIIIZ",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;IIIII)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 1425669
    invoke-virtual/range {p1 .. p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1425670
    const/high16 v3, -0x80000000

    move/from16 v0, p3

    if-ne v0, v3, :cond_0

    .line 1425671
    const v3, 0x7f0b152f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p3

    .line 1425672
    :cond_0
    const/high16 v3, -0x80000000

    move/from16 v0, p5

    if-ne v0, v3, :cond_1

    .line 1425673
    const v3, 0x7f0a06c0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result p5

    .line 1425674
    :cond_1
    const/high16 v3, -0x80000000

    move/from16 v0, p6

    if-ne v0, v3, :cond_2

    .line 1425675
    const v3, 0x7f0b1531

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p6

    .line 1425676
    :cond_2
    const/high16 v3, -0x80000000

    move/from16 v0, p7

    if-ne v0, v3, :cond_3

    .line 1425677
    const v3, 0x7f0b1530

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result p7

    .line 1425678
    :cond_3
    const/high16 v3, -0x80000000

    move/from16 v0, p10

    if-ne v0, v3, :cond_4

    .line 1425679
    const v3, 0x7f0c0050

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p10

    .line 1425680
    :cond_4
    const/high16 v3, -0x80000000

    move/from16 v0, p11

    if-ne v0, v3, :cond_5

    .line 1425681
    const v3, 0x7f0c0050

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p11

    .line 1425682
    :cond_5
    if-gez p10, :cond_6

    .line 1425683
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "faceCount cannot be < 0"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1425684
    :cond_6
    const/high16 v3, -0x80000000

    move/from16 v0, p12

    if-ne v0, v3, :cond_7

    .line 1425685
    const v3, 0x7f0b1532

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p12

    .line 1425686
    :cond_7
    const/high16 v2, -0x80000000

    move/from16 v0, p13

    if-ne v0, v2, :cond_15

    .line 1425687
    const/4 v6, 0x0

    .line 1425688
    :goto_0
    if-eqz p8, :cond_a

    move/from16 v0, p10

    move/from16 v1, p11

    if-le v0, v1, :cond_a

    const/4 v2, 0x1

    move v9, v2

    .line 1425689
    :goto_1
    invoke-interface/range {p9 .. p9}, Ljava/util/List;->size()I

    move-result v2

    move/from16 v0, p11

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 1425690
    if-eqz v9, :cond_b

    const/4 v2, 0x1

    :goto_2
    sub-int v11, v3, v2

    .line 1425691
    const/4 v2, 0x0

    .line 1425692
    if-eqz v9, :cond_14

    .line 1425693
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8yT;->b:LX/8yZ;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, LX/8yZ;->c(LX/1De;)LX/8yX;

    move-result-object v2

    sub-int v4, p10, v11

    invoke-virtual {v2, v4}, LX/8yX;->h(I)LX/8yX;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    move/from16 v0, p3

    invoke-interface {v2, v0}, LX/1Di;->o(I)LX/1Di;

    move-result-object v2

    move/from16 v0, p3

    invoke-interface {v2, v0}, LX/1Di;->g(I)LX/1Di;

    move-result-object v2

    .line 1425694
    if-eqz p7, :cond_8

    .line 1425695
    const/4 v4, 0x4

    move/from16 v0, p7

    invoke-interface {v2, v4, v0}, LX/1Di;->a(II)LX/1Di;

    :cond_8
    move-object v12, v2

    .line 1425696
    :goto_3
    const/high16 v2, -0x80000000

    move/from16 v0, p14

    if-ne v0, v2, :cond_c

    .line 1425697
    invoke-static {}, LX/4Ab;->e()LX/4Ab;

    move-result-object v5

    .line 1425698
    :goto_4
    move/from16 v0, p6

    int-to-float v2, v0

    move/from16 v0, p5

    invoke-virtual {v5, v0, v2}, LX/4Ab;->a(IF)LX/4Ab;

    move-result-object v2

    move/from16 v0, p12

    int-to-float v4, v0

    invoke-virtual {v2, v4}, LX/4Ab;->d(F)LX/4Ab;

    .line 1425699
    invoke-static/range {p1 .. p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/4 v4, 0x2

    invoke-interface {v2, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v13

    .line 1425700
    rem-int/lit8 v2, v3, 0x2

    if-eqz v2, :cond_11

    .line 1425701
    div-int/lit8 v14, v3, 0x2

    .line 1425702
    const/4 v2, 0x0

    move v10, v2

    :goto_5
    if-ge v10, v14, :cond_d

    .line 1425703
    sub-int v2, v14, v10

    mul-int v15, v2, p4

    .line 1425704
    sub-int v7, p3, v15

    move-object/from16 v0, p9

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/net/Uri;

    move-object/from16 v2, p0

    move-object/from16 v3, p2

    move-object/from16 v4, p1

    invoke-direct/range {v2 .. v8}, LX/8yT;->a(Lcom/facebook/common/callercontext/CallerContext;LX/1De;LX/4Ab;IILandroid/net/Uri;)LX/1Di;

    move-result-object v2

    .line 1425705
    const/4 v3, 0x1

    div-int/lit8 v4, v15, 0x2

    invoke-interface {v2, v3, v4}, LX/1Di;->a(II)LX/1Di;

    .line 1425706
    if-lez v10, :cond_9

    if-eqz p7, :cond_9

    .line 1425707
    const/4 v3, 0x4

    move/from16 v0, p7

    invoke-interface {v2, v3, v0}, LX/1Di;->a(II)LX/1Di;

    .line 1425708
    :cond_9
    invoke-interface {v13, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 1425709
    add-int/lit8 v2, v10, 0x1

    move v10, v2

    goto :goto_5

    .line 1425710
    :cond_a
    const/4 v2, 0x0

    move v9, v2

    goto/16 :goto_1

    .line 1425711
    :cond_b
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 1425712
    :cond_c
    move/from16 v0, p14

    int-to-float v2, v0

    invoke-static {v2}, LX/4Ab;->b(F)LX/4Ab;

    move-result-object v5

    goto :goto_4

    .line 1425713
    :cond_d
    invoke-static/range {p1 .. p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x3

    invoke-interface {v2, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v15

    .line 1425714
    add-int/lit8 v11, v11, -0x1

    move v10, v11

    .line 1425715
    :goto_6
    add-int/lit8 v2, v14, -0x1

    if-le v10, v2, :cond_f

    .line 1425716
    if-eqz v9, :cond_e

    if-ne v10, v11, :cond_e

    .line 1425717
    sub-int v2, v10, v14

    add-int/lit8 v2, v2, 0x1

    mul-int v2, v2, p4

    .line 1425718
    sub-int v3, p3, v2

    .line 1425719
    invoke-interface {v12, v3}, LX/1Di;->o(I)LX/1Di;

    .line 1425720
    invoke-interface {v12, v3}, LX/1Di;->g(I)LX/1Di;

    .line 1425721
    const/4 v3, 0x1

    div-int/lit8 v2, v2, 0x2

    invoke-interface {v12, v3, v2}, LX/1Di;->a(II)LX/1Di;

    .line 1425722
    invoke-interface {v15, v12}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 1425723
    :cond_e
    sub-int v2, v10, v14

    mul-int v16, v2, p4

    .line 1425724
    sub-int v7, p3, v16

    move-object/from16 v0, p9

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/net/Uri;

    move-object/from16 v2, p0

    move-object/from16 v3, p2

    move-object/from16 v4, p1

    invoke-direct/range {v2 .. v8}, LX/8yT;->a(Lcom/facebook/common/callercontext/CallerContext;LX/1De;LX/4Ab;IILandroid/net/Uri;)LX/1Di;

    move-result-object v2

    .line 1425725
    const/4 v3, 0x1

    div-int/lit8 v4, v16, 0x2

    invoke-interface {v2, v3, v4}, LX/1Di;->a(II)LX/1Di;

    .line 1425726
    const/4 v3, 0x4

    move/from16 v0, p7

    invoke-interface {v2, v3, v0}, LX/1Di;->a(II)LX/1Di;

    .line 1425727
    invoke-interface {v15, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 1425728
    add-int/lit8 v2, v10, -0x1

    move v10, v2

    goto :goto_6

    .line 1425729
    :cond_f
    invoke-interface {v13, v15}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 1425730
    :cond_10
    :goto_7
    invoke-interface {v13}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    return-object v2

    .line 1425731
    :cond_11
    const/4 v2, 0x0

    move v10, v2

    :goto_8
    if-ge v10, v11, :cond_13

    .line 1425732
    move-object/from16 v0, p9

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/net/Uri;

    move-object/from16 v2, p0

    move-object/from16 v3, p2

    move-object/from16 v4, p1

    move/from16 v7, p3

    invoke-direct/range {v2 .. v8}, LX/8yT;->a(Lcom/facebook/common/callercontext/CallerContext;LX/1De;LX/4Ab;IILandroid/net/Uri;)LX/1Di;

    move-result-object v2

    .line 1425733
    if-lez v10, :cond_12

    if-eqz p7, :cond_12

    .line 1425734
    const/4 v3, 0x4

    move/from16 v0, p7

    invoke-interface {v2, v3, v0}, LX/1Di;->a(II)LX/1Di;

    .line 1425735
    :cond_12
    invoke-interface {v13, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 1425736
    add-int/lit8 v2, v10, 0x1

    move v10, v2

    goto :goto_8

    .line 1425737
    :cond_13
    if-eqz v9, :cond_10

    .line 1425738
    invoke-interface {v13, v12}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    goto :goto_7

    :cond_14
    move-object v12, v2

    goto/16 :goto_3

    :cond_15
    move/from16 v6, p13

    goto/16 :goto_0
.end method
