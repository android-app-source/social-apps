.class public final LX/AIo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;


# direct methods
.method public constructor <init>(Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;)V
    .locals 0

    .prologue
    .line 1660353
    iput-object p1, p0, LX/AIo;->a:Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x2

    const v2, 0x7fad0757

    invoke-static {v1, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1660354
    iget-object v2, p0, LX/AIo;->a:Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;

    iget-object v2, v2, Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;->u:Lcom/facebook/resources/ui/FbCheckBox;

    iget-object v3, p0, LX/AIo;->a:Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;

    iget-object v3, v3, Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;->u:Lcom/facebook/resources/ui/FbCheckBox;

    invoke-virtual {v3}, Lcom/facebook/resources/ui/FbCheckBox;->isChecked()Z

    move-result v3

    if-nez v3, :cond_0

    :goto_0
    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbCheckBox;->setChecked(Z)V

    .line 1660355
    iget-object v0, p0, LX/AIo;->a:Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;

    iget-object v2, p0, LX/AIo;->a:Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;

    iget-object v2, v2, Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;->u:Lcom/facebook/resources/ui/FbCheckBox;

    invoke-virtual {v2}, Lcom/facebook/resources/ui/FbCheckBox;->isChecked()Z

    move-result v2

    invoke-static {v0, v2}, Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;->b(Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;Z)V

    .line 1660356
    iget-object v0, p0, LX/AIo;->a:Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;

    iget-object v0, v0, Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;->m:LX/AJ8;

    iget-object v2, p0, LX/AIo;->a:Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;

    iget-object v2, v2, Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;->n:Lcom/facebook/audience/model/AudienceControlData;

    iget-object v3, p0, LX/AIo;->a:Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;

    iget-object v3, v3, Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;->u:Lcom/facebook/resources/ui/FbCheckBox;

    invoke-virtual {v3}, Lcom/facebook/resources/ui/FbCheckBox;->isChecked()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, LX/AJ8;->a(Lcom/facebook/audience/model/AudienceControlData;Z)V

    .line 1660357
    const v0, 0x7ebb85e0

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 1660358
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
