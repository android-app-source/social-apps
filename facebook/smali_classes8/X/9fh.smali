.class public LX/9fh;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/graphics/PointF;


# direct methods
.method private constructor <init>(Landroid/graphics/PointF;)V
    .locals 0

    .prologue
    .line 1522547
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1522548
    iput-object p1, p0, LX/9fh;->a:Landroid/graphics/PointF;

    .line 1522549
    return-void
.end method

.method public static a(Landroid/view/View;)LX/9fh;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1522550
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1522551
    const/4 v0, 0x2

    new-array v0, v0, [I

    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v1

    aput v1, v0, v3

    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v1

    aput v1, v0, v4

    .line 1522552
    invoke-virtual {p0, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 1522553
    new-instance v1, LX/9fh;

    new-instance v2, Landroid/graphics/PointF;

    aget v3, v0, v3

    int-to-float v3, v3

    aget v0, v0, v4

    int-to-float v0, v0

    invoke-direct {v2, v3, v0}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-direct {v1, v2}, LX/9fh;-><init>(Landroid/graphics/PointF;)V

    return-object v1
.end method

.method public static a(Landroid/view/View;Landroid/graphics/RectF;)LX/9fh;
    .locals 5

    .prologue
    .line 1522554
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1522555
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1522556
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 1522557
    invoke-virtual {p0, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 1522558
    new-instance v1, LX/9fh;

    new-instance v2, Landroid/graphics/PointF;

    const/4 v3, 0x0

    aget v3, v0, v3

    int-to-float v3, v3

    iget v4, p1, Landroid/graphics/RectF;->left:F

    add-float/2addr v3, v4

    const/4 v4, 0x1

    aget v0, v0, v4

    int-to-float v0, v0

    iget v4, p1, Landroid/graphics/RectF;->top:F

    add-float/2addr v0, v4

    invoke-direct {v2, v3, v0}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-direct {v1, v2}, LX/9fh;-><init>(Landroid/graphics/PointF;)V

    return-object v1
.end method


# virtual methods
.method public final a()F
    .locals 1

    .prologue
    .line 1522559
    iget-object v0, p0, LX/9fh;->a:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    return v0
.end method
