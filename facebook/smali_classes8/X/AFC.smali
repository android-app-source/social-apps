.class public final LX/AFC;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Ljava/util/Map;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/util/concurrent/CountDownLatch;

.field public final synthetic d:LX/AFD;


# direct methods
.method public constructor <init>(LX/AFD;Ljava/util/Map;Ljava/lang/String;Ljava/util/concurrent/CountDownLatch;)V
    .locals 0

    .prologue
    .line 1647358
    iput-object p1, p0, LX/AFC;->d:LX/AFD;

    iput-object p2, p0, LX/AFC;->a:Ljava/util/Map;

    iput-object p3, p0, LX/AFC;->b:Ljava/lang/String;

    iput-object p4, p0, LX/AFC;->c:Ljava/util/concurrent/CountDownLatch;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Parcel;)V
    .locals 2
    .param p1    # Landroid/os/Parcel;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1647359
    if-eqz p1, :cond_0

    .line 1647360
    iget-object v0, p0, LX/AFC;->a:Ljava/util/Map;

    iget-object v1, p0, LX/AFC;->b:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1647361
    :cond_0
    iget-object v0, p0, LX/AFC;->c:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 1647362
    return-void
.end method
