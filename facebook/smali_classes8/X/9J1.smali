.class public final enum LX/9J1;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9J1;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9J1;

.field public static final enum AWESOMIZER_CLOSE:LX/9J1;

.field public static final enum CAMERA_CAPTURE_BUTTON_TAPPED:LX/9J1;

.field public static final enum CAMERA_EXITED:LX/9J1;

.field public static final enum COMPOSER_TAB_TAPPED:LX/9J1;

.field public static final enum ENTRY:LX/9J1;

.field public static final enum EXIT:LX/9J1;

.field public static final enum NUX_SHOWN:LX/9J1;

.field public static final enum PICKER_CLOSE:LX/9J1;

.field public static final enum PICKER_DESELECT:LX/9J1;

.field public static final enum PICKER_OPEN:LX/9J1;

.field public static final enum PICKER_SEARCH:LX/9J1;

.field public static final enum PICKER_SELECT:LX/9J1;

.field public static final enum PLACEHOLDER_CTA:LX/9J1;

.field public static final enum PLACEHOLDER_SHOWN:LX/9J1;

.field public static final enum PUBLISH_FROM_CAMERA_TAPPED:LX/9J1;


# instance fields
.field private mModule:Ljava/lang/String;

.field private mName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1464143
    new-instance v0, LX/9J1;

    const-string v1, "ENTRY"

    const-string v2, "goodfriends_enter"

    const-string v3, "good_friends_feed"

    invoke-direct {v0, v1, v5, v2, v3}, LX/9J1;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/9J1;->ENTRY:LX/9J1;

    .line 1464144
    new-instance v0, LX/9J1;

    const-string v1, "EXIT"

    const-string v2, "goodfriends_exit"

    const-string v3, "good_friends_feed"

    invoke-direct {v0, v1, v6, v2, v3}, LX/9J1;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/9J1;->EXIT:LX/9J1;

    .line 1464145
    new-instance v0, LX/9J1;

    const-string v1, "PICKER_OPEN"

    const-string v2, "goodfriends_picker_open"

    const-string v3, "good_friends_feed"

    invoke-direct {v0, v1, v7, v2, v3}, LX/9J1;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/9J1;->PICKER_OPEN:LX/9J1;

    .line 1464146
    new-instance v0, LX/9J1;

    const-string v1, "PICKER_CLOSE"

    const-string v2, "goodfrineds_picker_close"

    const-string v3, "good_friends_feed"

    invoke-direct {v0, v1, v8, v2, v3}, LX/9J1;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/9J1;->PICKER_CLOSE:LX/9J1;

    .line 1464147
    new-instance v0, LX/9J1;

    const-string v1, "PICKER_SELECT"

    const-string v2, "goodfriends_picker_select"

    const-string v3, "good_friends_feed"

    invoke-direct {v0, v1, v9, v2, v3}, LX/9J1;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/9J1;->PICKER_SELECT:LX/9J1;

    .line 1464148
    new-instance v0, LX/9J1;

    const-string v1, "PICKER_DESELECT"

    const/4 v2, 0x5

    const-string v3, "goodfriends_picker_deselect"

    const-string v4, "good_friends_feed"

    invoke-direct {v0, v1, v2, v3, v4}, LX/9J1;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/9J1;->PICKER_DESELECT:LX/9J1;

    .line 1464149
    new-instance v0, LX/9J1;

    const-string v1, "PICKER_SEARCH"

    const/4 v2, 0x6

    const-string v3, "goodfriends_picker_search"

    const-string v4, "good_friends_feed"

    invoke-direct {v0, v1, v2, v3, v4}, LX/9J1;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/9J1;->PICKER_SEARCH:LX/9J1;

    .line 1464150
    new-instance v0, LX/9J1;

    const-string v1, "AWESOMIZER_CLOSE"

    const/4 v2, 0x7

    const-string v3, "feed_awesomizer_card_close"

    const-string v4, "good_friends_feed"

    invoke-direct {v0, v1, v2, v3, v4}, LX/9J1;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/9J1;->AWESOMIZER_CLOSE:LX/9J1;

    .line 1464151
    new-instance v0, LX/9J1;

    const-string v1, "NUX_SHOWN"

    const/16 v2, 0x8

    const-string v3, "goodfriends_nux"

    const-string v4, "good_friends_feed"

    invoke-direct {v0, v1, v2, v3, v4}, LX/9J1;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/9J1;->NUX_SHOWN:LX/9J1;

    .line 1464152
    new-instance v0, LX/9J1;

    const-string v1, "PLACEHOLDER_SHOWN"

    const/16 v2, 0x9

    const-string v3, "goodfriends_placeholder_shown"

    const-string v4, "good_friends_feed"

    invoke-direct {v0, v1, v2, v3, v4}, LX/9J1;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/9J1;->PLACEHOLDER_SHOWN:LX/9J1;

    .line 1464153
    new-instance v0, LX/9J1;

    const-string v1, "PLACEHOLDER_CTA"

    const/16 v2, 0xa

    const-string v3, "goodfriends_placeholder_cta"

    const-string v4, "good_friends_feed"

    invoke-direct {v0, v1, v2, v3, v4}, LX/9J1;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/9J1;->PLACEHOLDER_CTA:LX/9J1;

    .line 1464154
    new-instance v0, LX/9J1;

    const-string v1, "COMPOSER_TAB_TAPPED"

    const/16 v2, 0xb

    const-string v3, "goodfriends_tapped_composer_tab"

    const-string v4, "good_friends_feed"

    invoke-direct {v0, v1, v2, v3, v4}, LX/9J1;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/9J1;->COMPOSER_TAB_TAPPED:LX/9J1;

    .line 1464155
    new-instance v0, LX/9J1;

    const-string v1, "CAMERA_CAPTURE_BUTTON_TAPPED"

    const/16 v2, 0xc

    const-string v3, "goodfriends_tapped_capture_button"

    const-string v4, "good_friends_feed"

    invoke-direct {v0, v1, v2, v3, v4}, LX/9J1;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/9J1;->CAMERA_CAPTURE_BUTTON_TAPPED:LX/9J1;

    .line 1464156
    new-instance v0, LX/9J1;

    const-string v1, "PUBLISH_FROM_CAMERA_TAPPED"

    const/16 v2, 0xd

    const-string v3, "goodfriends_tapped_publish_from_camera"

    const-string v4, "good_friends_feed"

    invoke-direct {v0, v1, v2, v3, v4}, LX/9J1;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/9J1;->PUBLISH_FROM_CAMERA_TAPPED:LX/9J1;

    .line 1464157
    new-instance v0, LX/9J1;

    const-string v1, "CAMERA_EXITED"

    const/16 v2, 0xe

    const-string v3, "goodfriends_exit_camera"

    const-string v4, "good_friends_feed"

    invoke-direct {v0, v1, v2, v3, v4}, LX/9J1;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/9J1;->CAMERA_EXITED:LX/9J1;

    .line 1464158
    const/16 v0, 0xf

    new-array v0, v0, [LX/9J1;

    sget-object v1, LX/9J1;->ENTRY:LX/9J1;

    aput-object v1, v0, v5

    sget-object v1, LX/9J1;->EXIT:LX/9J1;

    aput-object v1, v0, v6

    sget-object v1, LX/9J1;->PICKER_OPEN:LX/9J1;

    aput-object v1, v0, v7

    sget-object v1, LX/9J1;->PICKER_CLOSE:LX/9J1;

    aput-object v1, v0, v8

    sget-object v1, LX/9J1;->PICKER_SELECT:LX/9J1;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, LX/9J1;->PICKER_DESELECT:LX/9J1;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/9J1;->PICKER_SEARCH:LX/9J1;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/9J1;->AWESOMIZER_CLOSE:LX/9J1;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/9J1;->NUX_SHOWN:LX/9J1;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/9J1;->PLACEHOLDER_SHOWN:LX/9J1;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/9J1;->PLACEHOLDER_CTA:LX/9J1;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/9J1;->COMPOSER_TAB_TAPPED:LX/9J1;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/9J1;->CAMERA_CAPTURE_BUTTON_TAPPED:LX/9J1;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/9J1;->PUBLISH_FROM_CAMERA_TAPPED:LX/9J1;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/9J1;->CAMERA_EXITED:LX/9J1;

    aput-object v2, v0, v1

    sput-object v0, LX/9J1;->$VALUES:[LX/9J1;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1464159
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1464160
    iput-object p3, p0, LX/9J1;->mName:Ljava/lang/String;

    .line 1464161
    iput-object p4, p0, LX/9J1;->mModule:Ljava/lang/String;

    .line 1464162
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9J1;
    .locals 1

    .prologue
    .line 1464163
    const-class v0, LX/9J1;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9J1;

    return-object v0
.end method

.method public static values()[LX/9J1;
    .locals 1

    .prologue
    .line 1464164
    sget-object v0, LX/9J1;->$VALUES:[LX/9J1;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9J1;

    return-object v0
.end method


# virtual methods
.method public final getModule()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1464165
    iget-object v0, p0, LX/9J1;->mModule:Ljava/lang/String;

    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1464166
    iget-object v0, p0, LX/9J1;->mName:Ljava/lang/String;

    return-object v0
.end method
