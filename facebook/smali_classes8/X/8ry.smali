.class public LX/8ry;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8rg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/8rg",
        "<",
        "Lcom/facebook/friends/ui/SmartButtonLite;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1409646
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1409647
    return-void
.end method

.method public static a(Lcom/facebook/friends/ui/SmartButtonLite;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1409648
    invoke-virtual {p0}, Lcom/facebook/friends/ui/SmartButtonLite;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1409649
    sget-object v1, LX/8rx;->a:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1409650
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/friends/ui/SmartButtonLite;->setVisibility(I)V

    .line 1409651
    :goto_0
    return-void

    .line 1409652
    :pswitch_0
    const v1, 0x7f0101ef

    invoke-virtual {p0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setStyle(I)V

    .line 1409653
    const v1, 0x7f080f7b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f080f7c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/facebook/friends/ui/SmartButtonLite;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 1409654
    const v1, 0x7f080f9c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/friends/ui/SmartButtonLite;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1409655
    :goto_1
    invoke-virtual {p0, v3}, Lcom/facebook/friends/ui/SmartButtonLite;->setVisibility(I)V

    .line 1409656
    invoke-virtual {p0, v3}, Lcom/facebook/friends/ui/SmartButtonLite;->setFocusable(Z)V

    goto :goto_0

    .line 1409657
    :pswitch_1
    const v1, 0x7f0101ef

    invoke-virtual {p0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setStyle(I)V

    .line 1409658
    const v1, 0x7f080f7b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f080f7c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/facebook/friends/ui/SmartButtonLite;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 1409659
    const v1, 0x7f080f9f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/friends/ui/SmartButtonLite;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1409660
    :pswitch_2
    const v1, 0x7f0101e3

    invoke-virtual {p0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setStyle(I)V

    .line 1409661
    const v1, 0x7f080017

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v4}, Lcom/facebook/friends/ui/SmartButtonLite;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 1409662
    const v1, 0x7f080f9e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/friends/ui/SmartButtonLite;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1409663
    :pswitch_3
    const v1, 0x7f0101e3

    invoke-virtual {p0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setStyle(I)V

    .line 1409664
    const v1, 0x7f080f76

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v4}, Lcom/facebook/friends/ui/SmartButtonLite;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 1409665
    const v1, 0x7f080f9d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/friends/ui/SmartButtonLite;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final bridge synthetic a(Landroid/view/View;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;LX/8s1;)V
    .locals 0

    .prologue
    .line 1409666
    check-cast p1, Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-static {p1, p2}, LX/8ry;->a(Lcom/facebook/friends/ui/SmartButtonLite;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    return-void
.end method
