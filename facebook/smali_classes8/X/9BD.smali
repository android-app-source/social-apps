.class public final LX/9BD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Uh;


# instance fields
.field public final synthetic a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;)V
    .locals 0

    .prologue
    .line 1451404
    iput-object p1, p0, LX/9BD;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(II)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1451405
    iget-object v0, p0, LX/9BD;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v0, v0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->d:LX/82m;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9BD;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v0, v0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->s:LX/9BI;

    if-nez v0, :cond_1

    .line 1451406
    :cond_0
    :goto_0
    return v3

    .line 1451407
    :cond_1
    iget-object v0, p0, LX/9BD;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    const/4 v1, 0x1

    .line 1451408
    iput-boolean v1, v0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->C:Z

    .line 1451409
    iget-object v0, p0, LX/9BD;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v0, v0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->s:LX/9BI;

    invoke-static {v0, p1}, LX/9BI;->a$redex0(LX/9BI;I)LX/1zt;

    move-result-object v0

    .line 1451410
    iget-object v1, p0, LX/9BD;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v1, v1, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->d:LX/82m;

    sget-object v2, LX/82l;->TAP:LX/82l;

    invoke-virtual {v1, v2, v0}, LX/82m;->a(LX/82l;LX/1zt;)V

    .line 1451411
    if-ne p1, p2, :cond_0

    .line 1451412
    iget-object v1, p0, LX/9BD;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    const/16 p1, 0x14

    .line 1451413
    iget-object v2, v1, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->q:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2, v0}, Landroid/support/v4/view/ViewPager;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v2

    .line 1451414
    if-eqz v2, :cond_3

    .line 1451415
    const p0, 0x7f0d124b

    invoke-virtual {v2, p0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/listview/BetterListView;

    .line 1451416
    invoke-virtual {v2}, Lcom/facebook/widget/listview/BetterListView;->getFirstVisiblePosition()I

    move-result p0

    if-le p0, p1, :cond_2

    .line 1451417
    invoke-virtual {v2, p1}, Lcom/facebook/widget/listview/BetterListView;->setSelection(I)V

    .line 1451418
    :cond_2
    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Lcom/facebook/widget/listview/BetterListView;->smoothScrollToPosition(I)V

    .line 1451419
    :cond_3
    goto :goto_0
.end method
