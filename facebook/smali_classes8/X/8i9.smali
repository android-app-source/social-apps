.class public LX/8i9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/7CU;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7CV;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/8i8;

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/8i4;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/8i3;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/8i5;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/7CU;LX/0Ot;LX/8i8;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7CU;",
            "LX/0Ot",
            "<",
            "LX/7CV;",
            ">;",
            "LX/8i8;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1391236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1391237
    iput-object p1, p0, LX/8i9;->a:LX/7CU;

    .line 1391238
    iput-object p2, p0, LX/8i9;->b:LX/0Ot;

    .line 1391239
    iput-object p3, p0, LX/8i9;->c:LX/8i8;

    .line 1391240
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/8i9;->d:Ljava/util/Map;

    .line 1391241
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/8i9;->e:Ljava/util/Map;

    .line 1391242
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/8i9;->f:Ljava/util/Map;

    .line 1391243
    return-void
.end method

.method public static b(LX/0QB;)LX/8i9;
    .locals 4

    .prologue
    .line 1391244
    new-instance v2, LX/8i9;

    invoke-static {p0}, LX/7CU;->b(LX/0QB;)LX/7CU;

    move-result-object v0

    check-cast v0, LX/7CU;

    const/16 v1, 0x3519

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    .line 1391245
    new-instance v1, LX/8i8;

    invoke-direct {v1}, LX/8i8;-><init>()V

    .line 1391246
    move-object v1, v1

    .line 1391247
    move-object v1, v1

    .line 1391248
    check-cast v1, LX/8i8;

    invoke-direct {v2, v0, v3, v1}, LX/8i9;-><init>(LX/7CU;LX/0Ot;LX/8i8;)V

    .line 1391249
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/8i4;
    .locals 3

    .prologue
    .line 1391250
    iget-object v0, p0, LX/8i9;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1391251
    iget-object v0, p0, LX/8i9;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8i4;

    .line 1391252
    :goto_0
    return-object v0

    .line 1391253
    :cond_0
    new-instance v0, LX/8i4;

    iget-object v1, p0, LX/8i9;->a:LX/7CU;

    iget-object v2, p0, LX/8i9;->c:LX/8i8;

    invoke-direct {v0, v1, v2, p1}, LX/8i4;-><init>(LX/7CU;LX/8i8;Ljava/lang/String;)V

    .line 1391254
    iget-object v1, p0, LX/8i9;->d:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)LX/8i3;
    .locals 3

    .prologue
    .line 1391255
    iget-object v0, p0, LX/8i9;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1391256
    iget-object v0, p0, LX/8i9;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8i3;

    .line 1391257
    :goto_0
    return-object v0

    .line 1391258
    :cond_0
    new-instance v0, LX/8i3;

    iget-object v1, p0, LX/8i9;->a:LX/7CU;

    iget-object v2, p0, LX/8i9;->c:LX/8i8;

    invoke-direct {v0, v1, v2, p1}, LX/8i3;-><init>(LX/7CU;LX/8i8;Ljava/lang/String;)V

    .line 1391259
    iget-object v1, p0, LX/8i9;->e:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
