.class public LX/9Ly;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/B1W;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/B1W",
        "<",
        "Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$FetchCrossGroupForSalePostsIdsModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/api/feedtype/FeedType;

.field private final b:LX/0pn;


# direct methods
.method public constructor <init>(Lcom/facebook/api/feedtype/FeedType;LX/0pn;)V
    .locals 0
    .param p1    # Lcom/facebook/api/feedtype/FeedType;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1472332
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1472333
    iput-object p1, p0, LX/9Ly;->a:Lcom/facebook/api/feedtype/FeedType;

    .line 1472334
    iput-object p2, p0, LX/9Ly;->b:LX/0pn;

    .line 1472335
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/0Px;
    .locals 5

    .prologue
    .line 1472323
    check-cast p1, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$FetchCrossGroupForSalePostsIdsModel;

    .line 1472324
    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$FetchCrossGroupForSalePostsIdsModel;->a()Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$FetchCrossGroupForSalePostsIdsModel$MarketplaceCrossGroupStoriesModel;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1472325
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1472326
    :goto_0
    return-object v0

    .line 1472327
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1472328
    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$FetchCrossGroupForSalePostsIdsModel;->a()Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$FetchCrossGroupForSalePostsIdsModel$MarketplaceCrossGroupStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$FetchCrossGroupForSalePostsIdsModel$MarketplaceCrossGroupStoriesModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$FetchCrossGroupForSalePostsIdsModel$MarketplaceCrossGroupStoriesModel$NodesModel;

    .line 1472329
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$FetchCrossGroupForSalePostsIdsModel$MarketplaceCrossGroupStoriesModel$NodesModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1472330
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1472331
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()LX/0zO;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0zO",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$FetchCrossGroupForSalePostsIdsModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1472320
    new-instance v0, LX/9T4;

    invoke-direct {v0}, LX/9T4;-><init>()V

    move-object v0, v0

    .line 1472321
    const-string v1, "stories_to_fetch"

    iget-object v2, p0, LX/9Ly;->b:LX/0pn;

    invoke-virtual {v2}, LX/0pn;->d()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1472322
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    return-object v0
.end method

.method public final b()LX/0TF;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchMarketplaceCrossGroupStoriesGraphQLModels$FetchCrossGroupForSalePostsIdsModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1472319
    new-instance v0, LX/B1V;

    iget-object v1, p0, LX/9Ly;->a:Lcom/facebook/api/feedtype/FeedType;

    iget-object v2, p0, LX/9Ly;->b:LX/0pn;

    invoke-direct {v0, p0, v1, v2}, LX/B1V;-><init>(LX/B1W;Lcom/facebook/api/feedtype/FeedType;LX/0pn;)V

    return-object v0
.end method
