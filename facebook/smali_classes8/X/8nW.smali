.class public LX/8nW;
.super LX/8nB;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field private final a:Ljava/lang/Long;

.field public final b:LX/3iT;

.field private final c:Landroid/content/res/Resources;

.field private final d:LX/0tX;

.field private final e:Ljava/util/concurrent/ExecutorService;

.field private final f:Ljava/util/concurrent/ScheduledExecutorService;

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/8nM;

.field public final i:LX/0So;

.field public j:J

.field private k:Ljava/util/concurrent/ScheduledFuture;

.field public final l:LX/8nS;

.field private final m:Z


# direct methods
.method public constructor <init>(Ljava/lang/Long;LX/8nM;LX/3iT;Landroid/content/res/Resources;LX/0tX;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ScheduledExecutorService;LX/8nS;LX/0So;Ljava/lang/Boolean;)V
    .locals 1
    .param p1    # Ljava/lang/Long;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .param p7    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .param p10    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1401117
    invoke-direct {p0}, LX/8nB;-><init>()V

    .line 1401118
    iput-object p1, p0, LX/8nW;->a:Ljava/lang/Long;

    .line 1401119
    iput-object p3, p0, LX/8nW;->b:LX/3iT;

    .line 1401120
    iput-object p2, p0, LX/8nW;->h:LX/8nM;

    .line 1401121
    iput-object p4, p0, LX/8nW;->c:Landroid/content/res/Resources;

    .line 1401122
    iput-object p5, p0, LX/8nW;->d:LX/0tX;

    .line 1401123
    iput-object p6, p0, LX/8nW;->e:Ljava/util/concurrent/ExecutorService;

    .line 1401124
    iput-object p7, p0, LX/8nW;->f:Ljava/util/concurrent/ScheduledExecutorService;

    .line 1401125
    iput-object p8, p0, LX/8nW;->l:LX/8nS;

    .line 1401126
    invoke-virtual {p10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/8nW;->m:Z

    .line 1401127
    iput-object p9, p0, LX/8nW;->i:LX/0So;

    .line 1401128
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/8nW;->g:Ljava/util/List;

    .line 1401129
    return-void
.end method

.method public static b(LX/8nW;Ljava/lang/CharSequence;Ljava/lang/String;ZZZZZLX/8JX;)V
    .locals 11

    .prologue
    .line 1401147
    invoke-static {}, LX/8nf;->a()LX/8ne;

    move-result-object v0

    .line 1401148
    const-string v1, "target_id"

    iget-object v2, p0, LX/8nW;->a:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "query_string"

    invoke-virtual {v1, v2, p1}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    move-result-object v1

    const-string v2, "size"

    iget-object v3, p0, LX/8nW;->c:Landroid/content/res/Resources;

    const v4, 0x7f0b0bdb

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "entity_type"

    const-string v3, "user"

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "first"

    const/4 v3, 0x5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "is_work_build"

    iget-boolean v3, p0, LX/8nW;->m:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 1401149
    iget-object v1, p0, LX/8nW;->d:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v2, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v10

    .line 1401150
    new-instance v0, LX/8nT;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    move-object/from16 v4, p8

    move v5, p3

    move v6, p4

    move/from16 v7, p5

    move/from16 v8, p6

    move/from16 v9, p7

    invoke-direct/range {v0 .. v9}, LX/8nT;-><init>(LX/8nW;Ljava/lang/String;Ljava/lang/CharSequence;LX/8JX;ZZZZZ)V

    iget-object v1, p0, LX/8nW;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {v10, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1401151
    return-void
.end method


# virtual methods
.method public final a(LX/8JX;)V
    .locals 5

    .prologue
    .line 1401142
    new-instance v0, LX/8no;

    invoke-direct {v0}, LX/8no;-><init>()V

    move-object v0, v0

    .line 1401143
    const-string v1, "target_id"

    iget-object v2, p0, LX/8nW;->a:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "size"

    iget-object v3, p0, LX/8nW;->c:Landroid/content/res/Resources;

    const v4, 0x7f0b0bdb

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "first"

    const/4 v3, 0x5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1401144
    iget-object v1, p0, LX/8nW;->d:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v2, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 1401145
    new-instance v1, LX/8nU;

    invoke-direct {v1, p0, p1}, LX/8nU;-><init>(LX/8nW;LX/8JX;)V

    iget-object v2, p0, LX/8nW;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1401146
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/lang/String;ZZZZZLX/8JX;)V
    .locals 11

    .prologue
    .line 1401136
    if-nez p1, :cond_0

    .line 1401137
    :goto_0
    return-void

    .line 1401138
    :cond_0
    iget-object v0, p0, LX/8nW;->i:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iget-wide v2, p0, LX/8nW;->j:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0xc8

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    iget-object v0, p0, LX/8nW;->k:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_1

    .line 1401139
    iget-object v0, p0, LX/8nW;->k:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 1401140
    :cond_1
    iget-object v0, p0, LX/8nW;->i:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/8nW;->j:J

    .line 1401141
    iget-object v10, p0, LX/8nW;->f:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v0, Lcom/facebook/tagging/graphql/data/GroupMembersTaggingTypeaheadDataSource$1;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/facebook/tagging/graphql/data/GroupMembersTaggingTypeaheadDataSource$1;-><init>(LX/8nW;Ljava/lang/CharSequence;Ljava/lang/String;ZZZZZLX/8JX;)V

    const-wide/16 v2, 0xc8

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v10, v0, v2, v3, v1}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/8nW;->k:Ljava/util/concurrent/ScheduledFuture;

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1401135
    const/4 v0, 0x1

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1401134
    const-string v0, "group_members"

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1401131
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1401132
    iget-object v1, p0, LX/8nW;->g:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1401133
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1401130
    iget-object v0, p0, LX/8nW;->a:Ljava/lang/Long;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
