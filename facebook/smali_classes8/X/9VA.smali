.class public LX/9VA;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private b:[I

.field public c:[I

.field public d:[I

.field public e:Landroid/opengl/EGLConfig;

.field public f:Landroid/opengl/EGLDisplay;

.field public g:Landroid/opengl/EGLContext;

.field public h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/9VC;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1498999
    const-class v0, LX/9VA;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/9VA;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1499000
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1499001
    const/16 v0, 0xf

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, LX/9VA;->b:[I

    .line 1499002
    new-array v0, v1, [I

    const/16 v3, 0x3038

    aput v3, v0, v2

    iput-object v0, p0, LX/9VA;->c:[I

    .line 1499003
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, LX/9VA;->d:[I

    .line 1499004
    sget-object v0, Landroid/opengl/EGL14;->EGL_NO_DISPLAY:Landroid/opengl/EGLDisplay;

    iput-object v0, p0, LX/9VA;->f:Landroid/opengl/EGLDisplay;

    .line 1499005
    sget-object v0, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    iput-object v0, p0, LX/9VA;->g:Landroid/opengl/EGLContext;

    .line 1499006
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/9VA;->h:Ljava/util/ArrayList;

    .line 1499007
    invoke-static {v2}, Landroid/opengl/EGL14;->eglGetDisplay(I)Landroid/opengl/EGLDisplay;

    move-result-object v0

    iput-object v0, p0, LX/9VA;->f:Landroid/opengl/EGLDisplay;

    .line 1499008
    iget-object v0, p0, LX/9VA;->f:Landroid/opengl/EGLDisplay;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9VA;->f:Landroid/opengl/EGLDisplay;

    sget-object v3, Landroid/opengl/EGL14;->EGL_NO_DISPLAY:Landroid/opengl/EGLDisplay;

    if-eq v0, v3, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Failed to get EGL Display"

    invoke-static {v0, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1499009
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 1499010
    iget-object v3, p0, LX/9VA;->f:Landroid/opengl/EGLDisplay;

    invoke-static {v3, v0, v2, v0, v1}, Landroid/opengl/EGL14;->eglInitialize(Landroid/opengl/EGLDisplay;[II[II)Z

    .line 1499011
    const-string v0, "eglInitialize"

    invoke-static {v0}, LX/9VA;->b(Ljava/lang/String;)V

    .line 1499012
    iget-object v0, p0, LX/9VA;->b:[I

    const/4 v9, 0x1

    const/4 v6, 0x0

    .line 1499013
    new-array v7, v9, [Landroid/opengl/EGLConfig;

    .line 1499014
    new-array v10, v9, [I

    aput v6, v10, v6

    .line 1499015
    iget-object v4, p0, LX/9VA;->f:Landroid/opengl/EGLDisplay;

    move-object v5, v0

    move v8, v6

    move v11, v6

    invoke-static/range {v4 .. v11}, Landroid/opengl/EGL14;->eglChooseConfig(Landroid/opengl/EGLDisplay;[II[Landroid/opengl/EGLConfig;II[II)Z

    move-result v4

    .line 1499016
    const-string v5, "eglChooseConfig"

    invoke-static {v5}, LX/9VA;->b(Ljava/lang/String;)V

    .line 1499017
    if-eqz v4, :cond_1

    aget v4, v10, v6

    if-lez v4, :cond_1

    :goto_1
    const-string v4, "Failed to find matching EGL Config"

    invoke-static {v9, v4}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1499018
    aget-object v4, v7, v6

    move-object v0, v4

    .line 1499019
    iput-object v0, p0, LX/9VA;->e:Landroid/opengl/EGLConfig;

    .line 1499020
    iget-object v0, p0, LX/9VA;->e:Landroid/opengl/EGLConfig;

    .line 1499021
    iget-object v1, p0, LX/9VA;->f:Landroid/opengl/EGLDisplay;

    sget-object v2, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    iget-object v3, p0, LX/9VA;->d:[I

    const/4 v4, 0x0

    invoke-static {v1, v0, v2, v3, v4}, Landroid/opengl/EGL14;->eglCreateContext(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLConfig;Landroid/opengl/EGLContext;[II)Landroid/opengl/EGLContext;

    move-result-object v1

    .line 1499022
    const-string v2, "eglCreateContext"

    invoke-static {v2}, LX/9VA;->b(Ljava/lang/String;)V

    .line 1499023
    move-object v0, v1

    .line 1499024
    iput-object v0, p0, LX/9VA;->g:Landroid/opengl/EGLContext;

    .line 1499025
    return-void

    :cond_0
    move v0, v2

    .line 1499026
    goto :goto_0

    :cond_1
    move v9, v6

    .line 1499027
    goto :goto_1

    :array_0
    .array-data 4
        0x3040
        0x4
        0x3024
        0x8
        0x3023
        0x8
        0x3022
        0x8
        0x3021
        0x8
        0x3025
        0x0
        0x3026
        0x0
        0x3038
    .end array-data

    .line 1499028
    :array_1
    .array-data 4
        0x3098
        0x2
        0x3038
    .end array-data
.end method

.method public static a(ILjava/lang/String;)I
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 1499029
    invoke-static {p0}, Landroid/opengl/GLES20;->glCreateShader(I)I

    move-result v1

    .line 1499030
    const-string v2, "glCreateShader"

    invoke-static {v2}, LX/9VA;->a(Ljava/lang/String;)V

    .line 1499031
    invoke-static {v1, p1}, Landroid/opengl/GLES20;->glShaderSource(ILjava/lang/String;)V

    .line 1499032
    const-string v2, "glShaderSource"

    invoke-static {v2}, LX/9VA;->a(Ljava/lang/String;)V

    .line 1499033
    invoke-static {v1}, Landroid/opengl/GLES20;->glCompileShader(I)V

    .line 1499034
    new-array v2, v6, [I

    .line 1499035
    const v3, 0x8b81

    invoke-static {v1, v3, v2, v0}, Landroid/opengl/GLES20;->glGetShaderiv(II[II)V

    .line 1499036
    aget v2, v2, v0

    if-nez v2, :cond_0

    .line 1499037
    sget-object v2, LX/9VA;->a:Ljava/lang/String;

    const-string v3, "glCompileShader failed: [%s]\n%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1}, Landroid/opengl/GLES20;->glGetShaderInfoLog(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    aput-object p1, v4, v6

    invoke-static {v2, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1499038
    invoke-static {v1}, Landroid/opengl/GLES20;->glDeleteShader(I)V

    .line 1499039
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1499040
    invoke-static {}, Landroid/opengl/GLES20;->glGetError()I

    move-result v0

    .line 1499041
    if-eqz v0, :cond_0

    .line 1499042
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": glError 0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Landroid/opengl/GLU;->gluErrorString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1499043
    sget-object v1, LX/9VA;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1499044
    :cond_0
    return-void
.end method

.method public static b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1499045
    invoke-static {}, Landroid/opengl/EGL14;->eglGetError()I

    move-result v0

    .line 1499046
    const/16 v1, 0x3000

    if-eq v0, v1, :cond_0

    .line 1499047
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": eglError 0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1499048
    sget-object v1, LX/9VA;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1499049
    :cond_0
    return-void
.end method


# virtual methods
.method public final d()V
    .locals 4

    .prologue
    .line 1499050
    iget-object v0, p0, LX/9VA;->f:Landroid/opengl/EGLDisplay;

    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    sget-object v2, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    sget-object v3, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/EGL14;->eglMakeCurrent(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;)Z

    .line 1499051
    iget-object v0, p0, LX/9VA;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/9VA;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9VC;

    .line 1499052
    invoke-virtual {v0}, LX/9VC;->b()V

    .line 1499053
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1499054
    :cond_0
    iget-object v0, p0, LX/9VA;->g:Landroid/opengl/EGLContext;

    if-eqz v0, :cond_1

    .line 1499055
    iget-object v0, p0, LX/9VA;->f:Landroid/opengl/EGLDisplay;

    iget-object v1, p0, LX/9VA;->g:Landroid/opengl/EGLContext;

    invoke-static {v0, v1}, Landroid/opengl/EGL14;->eglDestroyContext(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLContext;)Z

    .line 1499056
    :cond_1
    iget-object v0, p0, LX/9VA;->f:Landroid/opengl/EGLDisplay;

    invoke-static {v0}, Landroid/opengl/EGL14;->eglTerminate(Landroid/opengl/EGLDisplay;)Z

    .line 1499057
    return-void
.end method
