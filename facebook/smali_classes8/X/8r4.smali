.class public LX/8r4;
.super LX/8r1;
.source ""


# instance fields
.field public n:Z


# direct methods
.method public constructor <init>(LX/8qr;LX/3iM;LX/0Or;ZZZZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8qr;",
            "LX/3iM;",
            "LX/0Or",
            "<",
            "LX/11S;",
            ">;ZZZZZ)V"
        }
    .end annotation

    .prologue
    .line 1408396
    invoke-direct/range {p0 .. p8}, LX/8r1;-><init>(LX/8qr;LX/3iM;LX/0Or;ZZZZZ)V

    .line 1408397
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/text/Spannable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1408398
    invoke-super {p0, p1, p2}, LX/8r1;->a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/8r4;->m:Ljava/util/List;

    .line 1408399
    iget-object v0, p0, LX/8r1;->m:Ljava/util/List;

    .line 1408400
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1408401
    invoke-static {p1}, LX/16z;->b(Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p1}, LX/16z;->d(Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    move-object v1, v3

    .line 1408402
    :goto_0
    move-object v1, v1

    .line 1408403
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1408404
    iget-object v0, p0, LX/8r1;->m:Ljava/util/List;

    return-object v0

    .line 1408405
    :cond_1
    invoke-static {p1}, LX/16z;->d(Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    move-result-object v2

    .line 1408406
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v1

    .line 1408407
    :goto_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->G()Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;

    move-result-object v4

    if-eqz v4, :cond_9

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->G()Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;->j()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    if-eqz v4, :cond_9

    .line 1408408
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->G()Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;->j()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v4

    .line 1408409
    :goto_2
    move-object v4, v4

    .line 1408410
    if-eqz v4, :cond_2

    if-nez v1, :cond_4

    :cond_2
    move-object v1, v3

    .line 1408411
    goto :goto_0

    .line 1408412
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .line 1408413
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->G()Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;

    move-result-object v5

    if-eqz v5, :cond_a

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->G()Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;->a()Z

    move-result v5

    if-eqz v5, :cond_a

    const/4 v5, 0x1

    :goto_3
    move v5, v5

    .line 1408414
    if-eqz v5, :cond_7

    .line 1408415
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;->REPLYABLE:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    if-ne v2, v5, :cond_5

    .line 1408416
    const v2, 0x7f080feb

    .line 1408417
    :goto_4
    invoke-virtual {p0}, LX/8r1;->a()Landroid/text/Spannable;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1408418
    iget-object v5, p0, LX/8r1;->f:LX/8qr;

    .line 1408419
    iget-object p2, v5, LX/8qr;->a:Landroid/content/Context;

    move-object v5, p2

    .line 1408420
    invoke-virtual {v5, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1408421
    new-instance v5, Landroid/text/SpannableString;

    invoke-virtual {p0, v2}, LX/8r1;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1408422
    new-instance v2, LX/8r3;

    invoke-direct {v2, p0, v1, v4}, LX/8r3;-><init>(LX/8r4;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-interface {v5}, Landroid/text/Spannable;->length()I

    move-result v4

    const/16 p2, 0x21

    invoke-interface {v5, v2, v1, v4, p2}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 1408423
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v1, v3

    .line 1408424
    goto/16 :goto_0

    .line 1408425
    :cond_5
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;->ALREADY_REPLIED:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    if-ne v2, v5, :cond_6

    .line 1408426
    const v2, 0x7f080fec

    goto :goto_4

    :cond_6
    move-object v1, v3

    .line 1408427
    goto/16 :goto_0

    .line 1408428
    :cond_7
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;->ALREADY_REPLIED:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    if-ne v2, v5, :cond_8

    .line 1408429
    const v2, 0x7f080fed

    .line 1408430
    const/4 v5, 0x1

    iput-boolean v5, p0, LX/8r4;->n:Z

    goto :goto_4

    :cond_8
    move-object v1, v3

    .line 1408431
    goto/16 :goto_0

    :cond_9
    const/4 v4, 0x0

    goto :goto_2

    :cond_a
    const/4 v5, 0x0

    goto :goto_3
.end method
