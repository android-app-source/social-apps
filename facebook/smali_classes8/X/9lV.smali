.class public final LX/9lV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;

.field public final synthetic b:LX/9lb;


# direct methods
.method public constructor <init>(LX/9lb;Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;)V
    .locals 0

    .prologue
    .line 1532943
    iput-object p1, p0, LX/9lV;->b:LX/9lb;

    iput-object p2, p0, LX/9lV;->a:Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1532944
    iget-object v0, p0, LX/9lV;->b:LX/9lb;

    iget-object v1, p0, LX/9lV;->a:Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;

    sget-object v2, LX/9lY;->NETWORK_ERROR:LX/9lY;

    invoke-static {v0, v1, v2}, LX/9lb;->a$redex0(LX/9lb;Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;LX/9lY;)V

    .line 1532945
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1532946
    iget-object v0, p0, LX/9lV;->b:LX/9lb;

    iget-object v1, p0, LX/9lV;->a:Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;

    invoke-static {v0, v1}, LX/9lb;->e(LX/9lb;Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;)V

    .line 1532947
    return-void
.end method
