.class public final enum LX/8t8;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8t8;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8t8;

.field public static final enum ADDRESS_BOOK_CONTACT:LX/8t8;

.field public static final enum PIC_SQUARE:LX/8t8;

.field public static final enum USER_KEY:LX/8t8;

.field public static final enum USER_KEY_WITH_FALLBACK_PIC_SQUARE:LX/8t8;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1412382
    new-instance v0, LX/8t8;

    const-string v1, "PIC_SQUARE"

    invoke-direct {v0, v1, v2}, LX/8t8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8t8;->PIC_SQUARE:LX/8t8;

    .line 1412383
    new-instance v0, LX/8t8;

    const-string v1, "USER_KEY"

    invoke-direct {v0, v1, v3}, LX/8t8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8t8;->USER_KEY:LX/8t8;

    .line 1412384
    new-instance v0, LX/8t8;

    const-string v1, "USER_KEY_WITH_FALLBACK_PIC_SQUARE"

    invoke-direct {v0, v1, v4}, LX/8t8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8t8;->USER_KEY_WITH_FALLBACK_PIC_SQUARE:LX/8t8;

    .line 1412385
    new-instance v0, LX/8t8;

    const-string v1, "ADDRESS_BOOK_CONTACT"

    invoke-direct {v0, v1, v5}, LX/8t8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8t8;->ADDRESS_BOOK_CONTACT:LX/8t8;

    .line 1412386
    const/4 v0, 0x4

    new-array v0, v0, [LX/8t8;

    sget-object v1, LX/8t8;->PIC_SQUARE:LX/8t8;

    aput-object v1, v0, v2

    sget-object v1, LX/8t8;->USER_KEY:LX/8t8;

    aput-object v1, v0, v3

    sget-object v1, LX/8t8;->USER_KEY_WITH_FALLBACK_PIC_SQUARE:LX/8t8;

    aput-object v1, v0, v4

    sget-object v1, LX/8t8;->ADDRESS_BOOK_CONTACT:LX/8t8;

    aput-object v1, v0, v5

    sput-object v0, LX/8t8;->$VALUES:[LX/8t8;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1412379
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8t8;
    .locals 1

    .prologue
    .line 1412381
    const-class v0, LX/8t8;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8t8;

    return-object v0
.end method

.method public static values()[LX/8t8;
    .locals 1

    .prologue
    .line 1412380
    sget-object v0, LX/8t8;->$VALUES:[LX/8t8;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8t8;

    return-object v0
.end method
