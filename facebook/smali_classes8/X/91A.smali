.class public final enum LX/91A;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/91A;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/91A;

.field public static final enum STICKER_PICKER_OPENED:LX/91A;

.field public static final enum STICKER_PICKER_SELECTED_STICKER:LX/91A;

.field public static final enum STICKER_PICKER_TAPPED_BACK_BUTTON:LX/91A;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1430198
    new-instance v0, LX/91A;

    const-string v1, "STICKER_PICKER_OPENED"

    const-string v2, "sticker_picker_opened"

    invoke-direct {v0, v1, v3, v2}, LX/91A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/91A;->STICKER_PICKER_OPENED:LX/91A;

    .line 1430199
    new-instance v0, LX/91A;

    const-string v1, "STICKER_PICKER_SELECTED_STICKER"

    const-string v2, "sticker_picker_selected_sticker"

    invoke-direct {v0, v1, v4, v2}, LX/91A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/91A;->STICKER_PICKER_SELECTED_STICKER:LX/91A;

    .line 1430200
    new-instance v0, LX/91A;

    const-string v1, "STICKER_PICKER_TAPPED_BACK_BUTTON"

    const-string v2, "sticker_picker_tapped_back_button"

    invoke-direct {v0, v1, v5, v2}, LX/91A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/91A;->STICKER_PICKER_TAPPED_BACK_BUTTON:LX/91A;

    .line 1430201
    const/4 v0, 0x3

    new-array v0, v0, [LX/91A;

    sget-object v1, LX/91A;->STICKER_PICKER_OPENED:LX/91A;

    aput-object v1, v0, v3

    sget-object v1, LX/91A;->STICKER_PICKER_SELECTED_STICKER:LX/91A;

    aput-object v1, v0, v4

    sget-object v1, LX/91A;->STICKER_PICKER_TAPPED_BACK_BUTTON:LX/91A;

    aput-object v1, v0, v5

    sput-object v0, LX/91A;->$VALUES:[LX/91A;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1430202
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1430203
    iput-object p3, p0, LX/91A;->name:Ljava/lang/String;

    .line 1430204
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/91A;
    .locals 1

    .prologue
    .line 1430205
    const-class v0, LX/91A;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/91A;

    return-object v0
.end method

.method public static values()[LX/91A;
    .locals 1

    .prologue
    .line 1430206
    sget-object v0, LX/91A;->$VALUES:[LX/91A;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/91A;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1430207
    iget-object v0, p0, LX/91A;->name:Ljava/lang/String;

    return-object v0
.end method
