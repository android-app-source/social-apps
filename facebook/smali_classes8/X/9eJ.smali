.class public final LX/9eJ;
.super LX/9eF;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)V
    .locals 0

    .prologue
    .line 1519451
    iput-object p1, p0, LX/9eJ;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    invoke-direct {p0}, LX/9eF;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1519452
    iget-object v0, p0, LX/9eJ;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v0, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    sget-object v2, LX/9eK;->ANIMATE_IN:LX/9eK;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1519453
    iget-object v0, p0, LX/9eJ;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v0, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->E:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 1519454
    iget-object v0, p0, LX/9eJ;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    invoke-static {v0}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->u(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)V

    .line 1519455
    return-void

    :cond_0
    move v0, v1

    .line 1519456
    goto :goto_0
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .locals 1

    .prologue
    .line 1519457
    iget-object v0, p0, LX/9eJ;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v0, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->t:LX/23g;

    invoke-virtual {v0}, LX/23g;->c()V

    .line 1519458
    return-void
.end method

.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 4

    .prologue
    .line 1519459
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v0

    const v1, 0x3cf5c28f    # 0.03f

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    .line 1519460
    iget-object v0, p0, LX/9eJ;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v0, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->K:Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;

    iget-object v1, p0, LX/9eJ;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v1, v1, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->v:LX/1L1;

    const/4 v2, 0x1

    iget-object v3, p0, LX/9eJ;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    invoke-static {v3}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->x(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->a(LX/1L1;ZI)V

    .line 1519461
    :cond_0
    return-void
.end method
