.class public LX/8ts;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/4Ba;
.implements LX/39D;


# instance fields
.field public a:LX/1wz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:F

.field private c:LX/31M;

.field public d:LX/8qU;

.field public e:Landroid/view/ViewGroup;

.field public f:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Landroid/view/ViewGroup;",
            ">;"
        }
    .end annotation
.end field

.field public g:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 1414035
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1414036
    const-class v0, LX/8ts;

    invoke-static {v0, p0}, LX/8ts;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1414037
    invoke-virtual {p0, p2}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1414038
    const v0, 0x7f0d0807

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/8ts;->e:Landroid/view/ViewGroup;

    .line 1414039
    const v0, 0x7f0d1605

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->d(I)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/8ts;->f:LX/0am;

    .line 1414040
    const v0, 0x7f0d0d5c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/8ts;->g:Landroid/view/ViewGroup;

    .line 1414041
    iget-object v0, p0, LX/8ts;->a:LX/1wz;

    .line 1414042
    iput-object p0, v0, LX/1wz;->q:LX/4Ba;

    .line 1414043
    iget-object v0, p0, LX/8ts;->a:LX/1wz;

    .line 1414044
    iput-object p0, v0, LX/1wz;->r:LX/39D;

    .line 1414045
    iget-object v0, p0, LX/8ts;->a:LX/1wz;

    sget-object v1, LX/31M;->UP:LX/31M;

    invoke-virtual {v1}, LX/31M;->flag()I

    move-result v1

    sget-object p1, LX/31M;->DOWN:LX/31M;

    invoke-virtual {p1}, LX/31M;->flag()I

    move-result p1

    or-int/2addr v1, p1

    .line 1414046
    iput v1, v0, LX/1wz;->p:I

    .line 1414047
    return-void
.end method

.method public static a(LX/8ts;LX/31M;)V
    .locals 2

    .prologue
    .line 1414030
    invoke-virtual {p0}, LX/8ts;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v0, LX/31M;->DOWN:LX/31M;

    if-ne v0, p1, :cond_0

    const v0, 0x7f0400df

    :goto_0
    invoke-static {v1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1414031
    new-instance v1, LX/8tq;

    invoke-direct {v1, p0}, LX/8tq;-><init>(LX/8ts;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1414032
    iget-object v1, p0, LX/8ts;->g:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1414033
    return-void

    .line 1414034
    :cond_0
    const v0, 0x7f0400e7

    goto :goto_0
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/8ts;

    invoke-static {p0}, LX/1wx;->b(LX/0QB;)LX/1wz;

    move-result-object p0

    check-cast p0, LX/1wz;

    iput-object p0, p1, LX/8ts;->a:LX/1wz;

    return-void
.end method

.method private b(I)V
    .locals 9

    .prologue
    .line 1414021
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/16 v1, 0x320

    if-ge v0, v1, :cond_0

    iget-object v0, p0, LX/8ts;->g:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v0

    int-to-double v0, v0

    const-wide/high16 v2, 0x3fd0000000000000L    # 0.25

    mul-double/2addr v0, v2

    double-to-int v0, v0

    iget v1, p0, LX/8ts;->b:F

    iget-object v2, p0, LX/8ts;->c:LX/31M;

    invoke-static {v0, v1, v2}, LX/8ti;->a(IFLX/31M;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1414022
    :cond_0
    iget-object v0, p0, LX/8ts;->c:LX/31M;

    invoke-static {p0, v0}, LX/8ts;->a(LX/8ts;LX/31M;)V

    .line 1414023
    :goto_0
    return-void

    .line 1414024
    :cond_1
    invoke-static {p0}, LX/8ts;->getViewToDrag(LX/8ts;)Landroid/view/View;

    move-result-object v4

    const-string v5, "translationY"

    const/4 v6, 0x2

    new-array v6, v6, [F

    const/4 v7, 0x0

    iget v8, p0, LX/8ts;->b:F

    aput v8, v6, v7

    const/4 v7, 0x1

    const/4 v8, 0x0

    aput v8, v6, v7

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 1414025
    const-wide/16 v6, 0x12c

    invoke-virtual {v4, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1414026
    new-instance v5, LX/8tr;

    invoke-direct {v5, p0}, LX/8tr;-><init>(LX/8ts;)V

    invoke-virtual {v4, v5}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1414027
    new-instance v5, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v4, v5}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1414028
    invoke-virtual {v4}, Landroid/animation/ObjectAnimator;->start()V

    .line 1414029
    goto :goto_0
.end method

.method public static getViewToDrag(LX/8ts;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1414018
    iget v0, p0, LX/8ts;->b:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 1414019
    iget-object v0, p0, LX/8ts;->g:Landroid/view/ViewGroup;

    .line 1414020
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/8ts;->e:Landroid/view/ViewGroup;

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1414016
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/8ts;->b(I)V

    .line 1414017
    return-void
.end method

.method public final a(LX/31M;I)V
    .locals 0

    .prologue
    .line 1414014
    invoke-direct {p0, p2}, LX/8ts;->b(I)V

    .line 1414015
    return-void
.end method

.method public final a(FF)Z
    .locals 1

    .prologue
    .line 1414048
    const/4 v0, 0x1

    return v0
.end method

.method public final a(FFLX/31M;)Z
    .locals 1

    .prologue
    .line 1414012
    iput-object p3, p0, LX/8ts;->c:LX/31M;

    .line 1414013
    iget-object v0, p0, LX/8ts;->e:Landroid/view/ViewGroup;

    invoke-static {v0, p1, p2}, LX/8ti;->a(Landroid/view/View;FF)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/8ts;->d:LX/8qU;

    invoke-virtual {v0, p1, p2, p3}, LX/8qU;->a(FFLX/31M;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1414011
    return-void
.end method

.method public final b(FFLX/31M;)V
    .locals 4

    .prologue
    .line 1414006
    iget v0, p0, LX/8ts;->b:F

    add-float/2addr v0, p2

    iput v0, p0, LX/8ts;->b:F

    .line 1414007
    invoke-static {p0}, LX/8ts;->getViewToDrag(LX/8ts;)Landroid/view/View;

    move-result-object v0

    iget v1, p0, LX/8ts;->b:F

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 1414008
    invoke-virtual {p0}, LX/8ts;->getHeight()I

    move-result v0

    int-to-double v0, v0

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v0, v2

    double-to-int v0, v0

    iget v1, p0, LX/8ts;->b:F

    invoke-static {v0, v1, p3}, LX/8ti;->a(IFLX/31M;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1414009
    invoke-static {p0, p3}, LX/8ts;->a(LX/8ts;LX/31M;)V

    .line 1414010
    :cond_0
    return-void
.end method

.method public final b(FF)Z
    .locals 1

    .prologue
    .line 1414005
    const/4 v0, 0x0

    return v0
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1414004
    iget-object v0, p0, LX/8ts;->a:LX/1wz;

    invoke-virtual {v0, p1}, LX/1wz;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x4abad113    # 6121609.5f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1414003
    iget-object v1, p0, LX/8ts;->a:LX/1wz;

    invoke-virtual {v1, p1}, LX/1wz;->b(Landroid/view/MotionEvent;)Z

    move-result v1

    const v2, -0x614337cf

    invoke-static {v3, v3, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v1
.end method
