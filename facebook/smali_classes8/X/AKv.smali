.class public final LX/AKv;
.super Landroid/os/Handler;
.source ""


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/audience/ui/ReplyThreadStoryView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/audience/ui/ReplyThreadStoryView;)V
    .locals 1

    .prologue
    .line 1664387
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 1664388
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/AKv;->a:Ljava/lang/ref/WeakReference;

    .line 1664389
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 1664390
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 1664391
    const-string v1, "current_position"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 1664392
    iget-object v0, p0, LX/AKv;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/ui/ReplyThreadStoryView;

    .line 1664393
    if-eqz v0, :cond_0

    .line 1664394
    iget-object v0, v0, Lcom/facebook/audience/ui/ReplyThreadStoryView;->j:LX/AKw;

    .line 1664395
    if-eqz v0, :cond_0

    .line 1664396
    invoke-interface {v0, v1}, LX/AKw;->a(I)V

    .line 1664397
    :cond_0
    return-void
.end method
