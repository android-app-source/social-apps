.class public LX/8ug;
.super Landroid/graphics/drawable/Drawable;
.source ""

# interfaces
.implements Landroid/graphics/drawable/Drawable$Callback;


# instance fields
.field public final a:Landroid/content/res/Resources;

.field private final b:[I

.field private final c:[Landroid/graphics/drawable/Drawable;

.field private d:LX/8ue;

.field public e:I

.field public f:Landroid/graphics/ColorFilter;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/16 v1, 0xf

    const/4 v4, 0x0

    .line 1415489
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 1415490
    new-array v0, v1, [I

    iput-object v0, p0, LX/8ug;->b:[I

    .line 1415491
    new-array v0, v1, [Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, LX/8ug;->c:[Landroid/graphics/drawable/Drawable;

    .line 1415492
    const/16 v0, 0xff

    iput v0, p0, LX/8ug;->e:I

    .line 1415493
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, LX/8ug;->a:Landroid/content/res/Resources;

    .line 1415494
    sget-object v0, LX/03r;->BadgedView:[I

    invoke-virtual {p1, p2, v0, p3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1415495
    iget-object v1, p0, LX/8ug;->b:[I

    const/16 v2, 0x0

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    aput v2, v1, v4

    .line 1415496
    iget-object v1, p0, LX/8ug;->b:[I

    const/4 v2, 0x1

    const/16 v3, 0x1

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    aput v3, v1, v2

    .line 1415497
    iget-object v1, p0, LX/8ug;->b:[I

    const/4 v2, 0x2

    const/16 v3, 0x3

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    aput v3, v1, v2

    .line 1415498
    iget-object v1, p0, LX/8ug;->b:[I

    const/4 v2, 0x3

    const/16 v3, 0x5

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    aput v3, v1, v2

    .line 1415499
    iget-object v1, p0, LX/8ug;->b:[I

    const/4 v2, 0x4

    const/16 v3, 0x2

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    aput v3, v1, v2

    .line 1415500
    iget-object v1, p0, LX/8ug;->b:[I

    const/4 v2, 0x5

    const/16 v3, 0x6

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    aput v3, v1, v2

    .line 1415501
    iget-object v1, p0, LX/8ug;->b:[I

    const/4 v2, 0x6

    const/16 v3, 0x7

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    aput v3, v1, v2

    .line 1415502
    iget-object v1, p0, LX/8ug;->b:[I

    const/4 v2, 0x7

    const/16 v3, 0x8

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    aput v3, v1, v2

    .line 1415503
    iget-object v1, p0, LX/8ug;->b:[I

    const/16 v2, 0x8

    const/16 v3, 0x9

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    aput v3, v1, v2

    .line 1415504
    iget-object v1, p0, LX/8ug;->b:[I

    const/16 v2, 0x9

    const/16 v3, 0xa

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    aput v3, v1, v2

    .line 1415505
    iget-object v1, p0, LX/8ug;->b:[I

    const/16 v2, 0xa

    const/16 v3, 0xb

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    aput v3, v1, v2

    .line 1415506
    iget-object v1, p0, LX/8ug;->b:[I

    const/16 v2, 0xe

    const/16 v3, 0xc

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    aput v3, v1, v2

    .line 1415507
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1415508
    return-void
.end method

.method public static a(LX/8ug;Landroid/graphics/drawable/Drawable;)V
    .locals 4
    .param p0    # LX/8ug;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1415483
    if-nez p1, :cond_0

    .line 1415484
    :goto_0
    return-void

    .line 1415485
    :cond_0
    invoke-virtual {p0}, LX/8ug;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 1415486
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    .line 1415487
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    .line 1415488
    iget v3, v0, Landroid/graphics/Rect;->right:I

    sub-int v1, v3, v1

    iget v3, v0, Landroid/graphics/Rect;->bottom:I

    sub-int v2, v3, v2

    iget v3, v0, Landroid/graphics/Rect;->right:I

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p1, v1, v2, v3, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto :goto_0
.end method

.method private b(LX/8ue;)Landroid/graphics/drawable/Drawable;
    .locals 4
    .param p1    # LX/8ue;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1415448
    sget-boolean v1, LX/007;->j:Z

    move v1, v1

    .line 1415449
    if-eqz v1, :cond_1

    .line 1415450
    :cond_0
    :goto_0
    return-object v0

    .line 1415451
    :cond_1
    const/4 v1, -0x1

    .line 1415452
    if-nez p1, :cond_2

    .line 1415453
    :goto_1
    move v1, v1

    .line 1415454
    if-ltz v1, :cond_0

    .line 1415455
    iget-object v0, p0, LX/8ug;->c:[Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, LX/8ug;->b:[I

    aget v2, v2, v1

    .line 1415456
    if-lez v2, :cond_3

    .line 1415457
    iget-object v3, p0, LX/8ug;->a:Landroid/content/res/Resources;

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 1415458
    if-eqz v3, :cond_3

    .line 1415459
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 1415460
    iget p1, p0, LX/8ug;->e:I

    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1415461
    iget-object p1, p0, LX/8ug;->f:Landroid/graphics/ColorFilter;

    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 1415462
    invoke-static {p0, v3}, LX/8ug;->a(LX/8ug;Landroid/graphics/drawable/Drawable;)V

    .line 1415463
    :goto_2
    move-object v2, v3

    .line 1415464
    aput-object v2, v0, v1

    .line 1415465
    iget-object v0, p0, LX/8ug;->c:[Landroid/graphics/drawable/Drawable;

    aget-object v0, v0, v1

    goto :goto_0

    .line 1415466
    :cond_2
    sget-object v2, LX/8uf;->a:[I

    invoke-virtual {p1}, LX/8ue;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_1

    .line 1415467
    :pswitch_0
    const/4 v1, 0x0

    goto :goto_1

    .line 1415468
    :pswitch_1
    const/4 v1, 0x1

    goto :goto_1

    .line 1415469
    :pswitch_2
    const/4 v1, 0x2

    goto :goto_1

    .line 1415470
    :pswitch_3
    const/4 v1, 0x3

    goto :goto_1

    .line 1415471
    :pswitch_4
    const/4 v1, 0x4

    goto :goto_1

    .line 1415472
    :pswitch_5
    const/4 v1, 0x5

    goto :goto_1

    .line 1415473
    :pswitch_6
    const/4 v1, 0x6

    goto :goto_1

    .line 1415474
    :pswitch_7
    const/4 v1, 0x7

    goto :goto_1

    .line 1415475
    :pswitch_8
    const/16 v1, 0x8

    goto :goto_1

    .line 1415476
    :pswitch_9
    const/16 v1, 0x9

    goto :goto_1

    .line 1415477
    :pswitch_a
    const/16 v1, 0xa

    goto :goto_1

    .line 1415478
    :pswitch_b
    const/16 v1, 0xe

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/8ue;)V
    .locals 1

    .prologue
    .line 1415444
    iget-object v0, p0, LX/8ug;->d:LX/8ue;

    if-eq p1, v0, :cond_0

    .line 1415445
    iput-object p1, p0, LX/8ug;->d:LX/8ue;

    .line 1415446
    invoke-virtual {p0}, LX/8ug;->invalidateSelf()V

    .line 1415447
    :cond_0
    return-void
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1415440
    iget-object v0, p0, LX/8ug;->d:LX/8ue;

    invoke-direct {p0, v0}, LX/8ug;->b(LX/8ue;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1415441
    if-eqz v0, :cond_0

    .line 1415442
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1415443
    :cond_0
    return-void
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 1415479
    iget-object v0, p0, LX/8ug;->d:LX/8ue;

    invoke-direct {p0, v0}, LX/8ug;->b(LX/8ue;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1415480
    if-eqz v0, :cond_0

    .line 1415481
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    move-result v0

    .line 1415482
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 1415437
    iget-object v0, p0, LX/8ug;->d:LX/8ue;

    invoke-direct {p0, v0}, LX/8ug;->b(LX/8ue;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 1415438
    invoke-virtual {p0}, LX/8ug;->invalidateSelf()V

    .line 1415439
    :cond_0
    return-void
.end method

.method public final scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V
    .locals 1

    .prologue
    .line 1415434
    iget-object v0, p0, LX/8ug;->d:LX/8ue;

    invoke-direct {p0, v0}, LX/8ug;->b(LX/8ue;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 1415435
    invoke-virtual {p0, p2, p3, p4}, LX/8ug;->scheduleSelf(Ljava/lang/Runnable;J)V

    .line 1415436
    :cond_0
    return-void
.end method

.method public final setAlpha(I)V
    .locals 3

    .prologue
    .line 1415427
    iput p1, p0, LX/8ug;->e:I

    .line 1415428
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/8ug;->c:[Landroid/graphics/drawable/Drawable;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 1415429
    iget-object v1, p0, LX/8ug;->c:[Landroid/graphics/drawable/Drawable;

    aget-object v1, v1, v0

    .line 1415430
    if-eqz v1, :cond_0

    .line 1415431
    iget v2, p0, LX/8ug;->e:I

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1415432
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1415433
    :cond_1
    return-void
.end method

.method public final setBounds(IIII)V
    .locals 2

    .prologue
    .line 1415422
    invoke-super {p0, p1, p2, p3, p4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1415423
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/8ug;->c:[Landroid/graphics/drawable/Drawable;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 1415424
    iget-object v1, p0, LX/8ug;->c:[Landroid/graphics/drawable/Drawable;

    aget-object v1, v1, v0

    invoke-static {p0, v1}, LX/8ug;->a(LX/8ug;Landroid/graphics/drawable/Drawable;)V

    .line 1415425
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1415426
    :cond_0
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 3

    .prologue
    .line 1415412
    iput-object p1, p0, LX/8ug;->f:Landroid/graphics/ColorFilter;

    .line 1415413
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/8ug;->c:[Landroid/graphics/drawable/Drawable;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 1415414
    iget-object v1, p0, LX/8ug;->c:[Landroid/graphics/drawable/Drawable;

    aget-object v1, v1, v0

    .line 1415415
    if-eqz v1, :cond_0

    .line 1415416
    iget-object v2, p0, LX/8ug;->f:Landroid/graphics/ColorFilter;

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 1415417
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1415418
    :cond_1
    return-void
.end method

.method public final unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 1415419
    iget-object v0, p0, LX/8ug;->d:LX/8ue;

    invoke-direct {p0, v0}, LX/8ug;->b(LX/8ue;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 1415420
    invoke-virtual {p0, p2}, LX/8ug;->unscheduleSelf(Ljava/lang/Runnable;)V

    .line 1415421
    :cond_0
    return-void
.end method
