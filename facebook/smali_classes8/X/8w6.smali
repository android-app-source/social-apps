.class public final LX/8w6;
.super LX/8w5;
.source ""


# instance fields
.field public final synthetic a:LX/8w7;

.field private e:I


# direct methods
.method public constructor <init>(LX/8w7;)V
    .locals 1

    .prologue
    .line 1421617
    iput-object p1, p0, LX/8w6;->a:LX/8w7;

    invoke-direct {p0}, LX/8w5;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/widget/GridLayout;Landroid/view/View;LX/8vz;IZ)I
    .locals 2

    .prologue
    .line 1421618
    const/4 v0, 0x0

    invoke-super/range {p0 .. p5}, LX/8w5;->a(Landroid/support/v7/widget/GridLayout;Landroid/view/View;LX/8vz;IZ)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public final a(Z)I
    .locals 2

    .prologue
    .line 1421616
    invoke-super {p0, p1}, LX/8w5;->a(Z)I

    move-result v0

    iget v1, p0, LX/8w6;->e:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1421613
    invoke-super {p0}, LX/8w5;->a()V

    .line 1421614
    const/high16 v0, -0x80000000

    iput v0, p0, LX/8w6;->e:I

    .line 1421615
    return-void
.end method

.method public final a(II)V
    .locals 2

    .prologue
    .line 1421610
    invoke-super {p0, p1, p2}, LX/8w5;->a(II)V

    .line 1421611
    iget v0, p0, LX/8w6;->e:I

    add-int v1, p1, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LX/8w6;->e:I

    .line 1421612
    return-void
.end method
