.class public LX/947;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private a:Landroid/text/TextPaint;

.field private b:Landroid/content/res/Resources;

.field private c:Landroid/graphics/drawable/Drawable;

.field private d:LX/8Rp;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/8Rp;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1434596
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1434597
    new-instance v0, Landroid/text/TextPaint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v0, p0, LX/947;->a:Landroid/text/TextPaint;

    .line 1434598
    const v0, 0x7f02197d

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/947;->c:Landroid/graphics/drawable/Drawable;

    .line 1434599
    iput-object p1, p0, LX/947;->b:Landroid/content/res/Resources;

    .line 1434600
    iput-object p2, p0, LX/947;->d:LX/8Rp;

    .line 1434601
    return-void
.end method

.method public static a(LX/0QB;)LX/947;
    .locals 5

    .prologue
    .line 1434602
    const-class v1, LX/947;

    monitor-enter v1

    .line 1434603
    :try_start_0
    sget-object v0, LX/947;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1434604
    sput-object v2, LX/947;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1434605
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1434606
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1434607
    new-instance p0, LX/947;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/8Rp;->b(LX/0QB;)LX/8Rp;

    move-result-object v4

    check-cast v4, LX/8Rp;

    invoke-direct {p0, v3, v4}, LX/947;-><init>(Landroid/content/res/Resources;LX/8Rp;)V

    .line 1434608
    move-object v0, p0

    .line 1434609
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1434610
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/947;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1434611
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1434612
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;LX/0Px;FIZZ)Landroid/text/SpannableStringBuilder;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "LX/8QL;",
            ">;FIZZ)",
            "Landroid/text/SpannableStringBuilder;"
        }
    .end annotation

    .prologue
    .line 1434613
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 1434614
    iget-object v0, p0, LX/947;->b:Landroid/content/res/Resources;

    const v1, 0x7f0b0b86

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iget-object v1, p0, LX/947;->b:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    .line 1434615
    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    sub-int v0, p4, v0

    .line 1434616
    if-lez v0, :cond_0

    if-nez p2, :cond_1

    :cond_0
    move-object v0, v3

    .line 1434617
    :goto_0
    return-object v0

    .line 1434618
    :cond_1
    iget-object v1, p0, LX/947;->a:Landroid/text/TextPaint;

    invoke-virtual {v1, p3}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 1434619
    if-eqz p6, :cond_2

    .line 1434620
    iget-object v1, p0, LX/947;->a:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    const/4 v4, 0x1

    invoke-static {v2, v4}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 1434621
    :cond_2
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_9

    invoke-virtual {p2, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8QL;

    .line 1434622
    iget-object v5, v1, LX/8QL;->a:LX/8vA;

    sget-object v6, LX/8vA;->TAG_EXPANSION:LX/8vA;

    if-ne v5, v6, :cond_8

    .line 1434623
    :goto_2
    move-object v1, v1

    .line 1434624
    if-eqz v1, :cond_7

    .line 1434625
    iget-object v2, p0, LX/947;->b:Landroid/content/res/Resources;

    invoke-virtual {v1}, LX/8QL;->d()I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    sub-int/2addr v0, v1

    move v1, v0

    .line 1434626
    :goto_3
    invoke-virtual {p2}, LX/0Px;->listIterator()LX/0Rb;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1434627
    invoke-interface {v4}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QL;

    .line 1434628
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, LX/8QK;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1434629
    const/4 v2, 0x0

    .line 1434630
    if-eqz p5, :cond_3

    .line 1434631
    iget-object v2, p0, LX/947;->b:Landroid/content/res/Resources;

    const v6, 0x7f0a010e

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 1434632
    iget-object v6, p0, LX/947;->a:Landroid/text/TextPaint;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/text/TextPaint;->setColor(I)V

    .line 1434633
    :goto_5
    new-instance v6, LX/8uj;

    invoke-direct {v6}, LX/8uj;-><init>()V

    .line 1434634
    iput-object v0, v6, LX/8uj;->a:LX/8QL;

    .line 1434635
    move-object v0, v6

    .line 1434636
    invoke-virtual {v0, v1}, LX/8uj;->a(I)LX/8uj;

    move-result-object v0

    iget-object v6, p0, LX/947;->a:Landroid/text/TextPaint;

    .line 1434637
    iput-object v6, v0, LX/8uj;->b:Landroid/text/TextPaint;

    .line 1434638
    move-object v0, v0

    .line 1434639
    iget-object v6, p0, LX/947;->b:Landroid/content/res/Resources;

    .line 1434640
    iput-object v6, v0, LX/8uj;->c:Landroid/content/res/Resources;

    .line 1434641
    move-object v0, v0

    .line 1434642
    iget-object v6, p0, LX/947;->c:Landroid/graphics/drawable/Drawable;

    .line 1434643
    iput-object v6, v0, LX/8uj;->e:Landroid/graphics/drawable/Drawable;

    .line 1434644
    move-object v0, v0

    .line 1434645
    iput-object v2, v0, LX/8uj;->h:Ljava/lang/Integer;

    .line 1434646
    move-object v0, v0

    .line 1434647
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/8uj;->b(I)LX/8uj;

    move-result-object v0

    invoke-interface {v4}, Ljava/util/ListIterator;->hasNext()Z

    move-result v2

    invoke-virtual {v0, v2}, LX/8uj;->a(Z)LX/8uj;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/8uj;->a(Landroid/content/Context;)LX/8ul;

    move-result-object v0

    .line 1434648
    invoke-virtual {v3, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1434649
    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    sub-int/2addr v2, v5

    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    const/16 v6, 0x21

    invoke-virtual {v3, v0, v2, v5, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_4

    .line 1434650
    :cond_3
    invoke-virtual {v0}, LX/8QK;->a()Z

    move-result v6

    if-nez v6, :cond_4

    .line 1434651
    iget-object v6, p0, LX/947;->a:Landroid/text/TextPaint;

    iget-object v7, p0, LX/947;->b:Landroid/content/res/Resources;

    const v8, 0x7f0a00e7

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/text/TextPaint;->setColor(I)V

    goto :goto_5

    .line 1434652
    :cond_4
    iget-boolean v6, v0, LX/8QK;->c:Z

    move v6, v6

    .line 1434653
    if-eqz v6, :cond_5

    .line 1434654
    iget-object v6, p0, LX/947;->a:Landroid/text/TextPaint;

    iget-object v7, p0, LX/947;->b:Landroid/content/res/Resources;

    const v8, 0x7f0a0048

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/text/TextPaint;->setColor(I)V

    goto :goto_5

    .line 1434655
    :cond_5
    iget-object v2, p0, LX/947;->b:Landroid/content/res/Resources;

    const v6, 0x7f0a00d2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 1434656
    iget-object v6, p0, LX/947;->a:Landroid/text/TextPaint;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/text/TextPaint;->setColor(I)V

    goto/16 :goto_5

    :cond_6
    move-object v0, v3

    .line 1434657
    goto/16 :goto_0

    :cond_7
    move v1, v0

    goto/16 :goto_3

    .line 1434658
    :cond_8
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto/16 :goto_1

    .line 1434659
    :cond_9
    const/4 v1, 0x0

    goto/16 :goto_2
.end method
