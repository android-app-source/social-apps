.class public LX/8lL;
.super LX/4nn;
.source ""


# instance fields
.field public final a:LX/4nn;

.field public final b:LX/0wd;

.field public c:LX/0w3;

.field public d:LX/0wW;

.field public e:Ljava/util/concurrent/ExecutorService;

.field public f:F

.field public g:Z

.field public h:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 1397716
    invoke-direct {p0, p1}, LX/4nn;-><init>(Landroid/content/Context;)V

    .line 1397717
    const-class v0, LX/8lL;

    invoke-static {v0, p0}, LX/8lL;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1397718
    new-instance v0, LX/4nn;

    invoke-virtual {p0}, LX/8lL;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/4nn;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/8lL;->a:LX/4nn;

    .line 1397719
    iget-object v0, p0, LX/8lL;->d:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    new-instance v1, LX/0wT;

    const-wide v2, 0x408a900000000000L    # 850.0

    const-wide/high16 v4, 0x4049000000000000L    # 50.0

    invoke-direct {v1, v2, v3, v4, v5}, LX/0wT;-><init>(DD)V

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    const/4 v1, 0x1

    .line 1397720
    iput-boolean v1, v0, LX/0wd;->c:Z

    .line 1397721
    move-object v0, v0

    .line 1397722
    iput-object v0, p0, LX/8lL;->b:LX/0wd;

    .line 1397723
    return-void
.end method

.method public static a(LX/4nn;LX/4nn;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 1397713
    :goto_0
    invoke-virtual {p0}, LX/4nn;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 1397714
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/4nn;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p1, v0, v1}, LX/4nn;->a(Landroid/view/View;Landroid/widget/FrameLayout$LayoutParams;)V

    goto :goto_0

    .line 1397715
    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/8lL;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0w3;->a(LX/0QB;)LX/0w3;

    move-result-object v2

    check-cast v2, LX/0w3;

    invoke-static {p0}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object p0

    check-cast p0, LX/0wW;

    iput-object v1, p1, LX/8lL;->e:Ljava/util/concurrent/ExecutorService;

    iput-object v2, p1, LX/8lL;->c:LX/0w3;

    iput-object p0, p1, LX/8lL;->d:LX/0wW;

    return-void
.end method

.method public static f(LX/8lL;)V
    .locals 2

    .prologue
    .line 1397707
    iget-object v0, p0, LX/8lL;->a:LX/4nn;

    invoke-virtual {v0}, LX/4nn;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 1397708
    :goto_0
    return-void

    .line 1397709
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/8lL;->g:Z

    .line 1397710
    iget-object v0, p0, LX/8lL;->a:LX/4nn;

    invoke-static {v0, p0}, LX/8lL;->a(LX/4nn;LX/4nn;)V

    .line 1397711
    invoke-static {p0}, LX/8lL;->getFullscreenHostView(LX/8lL;)Landroid/view/ViewGroup;

    move-result-object v0

    iget-object v1, p0, LX/8lL;->a:LX/4nn;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1397712
    invoke-virtual {p0}, LX/8lL;->requestLayout()V

    goto :goto_0
.end method

.method public static getFullscreenHostView(LX/8lL;)Landroid/view/ViewGroup;
    .locals 2

    .prologue
    .line 1397706
    invoke-virtual {p0}, LX/8lL;->getRootView()Landroid/view/View;

    move-result-object v0

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method public static getInlineContainerPositionY(LX/8lL;)F
    .locals 5

    .prologue
    const/4 v1, 0x2

    const/4 v4, 0x1

    .line 1397681
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    .line 1397682
    new-array v1, v1, [I

    fill-array-data v1, :array_1

    .line 1397683
    invoke-static {p0}, LX/8lL;->getFullscreenHostView(LX/8lL;)Landroid/view/ViewGroup;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result v2

    .line 1397684
    invoke-virtual {p0, v0}, LX/8lL;->getLocationOnScreen([I)V

    .line 1397685
    invoke-static {p0}, LX/8lL;->getFullscreenHostView(LX/8lL;)Landroid/view/ViewGroup;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->getLocationOnScreen([I)V

    .line 1397686
    aget v0, v0, v4

    aget v1, v1, v4

    sub-int/2addr v0, v1

    sub-int/2addr v0, v2

    int-to-float v0, v0

    return v0

    .line 1397687
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 1397688
    :array_1
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public static setPopOutLayoutExpansion(LX/8lL;F)V
    .locals 2

    .prologue
    .line 1397702
    iget-object v0, p0, LX/8lL;->a:LX/4nn;

    if-nez v0, :cond_0

    .line 1397703
    :goto_0
    return-void

    .line 1397704
    :cond_0
    iget v0, p0, LX/8lL;->f:F

    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, LX/0yq;->a(FFF)F

    move-result v0

    .line 1397705
    iget-object v1, p0, LX/8lL;->a:LX/4nn;

    invoke-virtual {v1, v0}, LX/4nn;->setTranslationY(F)V

    goto :goto_0
.end method


# virtual methods
.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0xc59d3e6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1397700
    iget-object v1, p0, LX/8lL;->b:LX/0wd;

    new-instance v2, LX/8lK;

    invoke-direct {v2, p0}, LX/8lK;-><init>(LX/8lL;)V

    invoke-virtual {v1, v2}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 1397701
    const/16 v1, 0x2d

    const v2, 0x757c4fb4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x43a48c56

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1397696
    invoke-static {p0}, LX/8lL;->f(LX/8lL;)V

    .line 1397697
    iget-object v1, p0, LX/8lL;->b:LX/0wd;

    invoke-virtual {v1}, LX/0wd;->k()LX/0wd;

    .line 1397698
    invoke-super {p0}, LX/4nn;->onDetachedFromWindow()V

    .line 1397699
    const/16 v1, 0x2d

    const v2, -0x3e395766

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onMeasure(II)V
    .locals 4

    .prologue
    .line 1397689
    iget-object v0, p0, LX/8lL;->c:LX/0w3;

    .line 1397690
    iget-boolean v1, v0, LX/0w3;->f:Z

    move v0, v1

    .line 1397691
    if-eqz v0, :cond_1

    invoke-virtual {p0}, LX/8lL;->findFocus()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/8lL;->a:LX/4nn;

    invoke-virtual {v0}, LX/4nn;->findFocus()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1397692
    :cond_0
    iget-object v0, p0, LX/8lL;->e:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/stickers/search/ExpandableFrameLayout$1;

    invoke-direct {v1, p0}, Lcom/facebook/stickers/search/ExpandableFrameLayout$1;-><init>(LX/8lL;)V

    const v2, -0x228990cb

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1397693
    :goto_0
    invoke-super {p0, p1, p2}, LX/4nn;->onMeasure(II)V

    .line 1397694
    return-void

    .line 1397695
    :cond_1
    iget-object v0, p0, LX/8lL;->b:LX/0wd;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    goto :goto_0
.end method
