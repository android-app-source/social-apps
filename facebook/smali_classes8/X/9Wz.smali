.class public LX/9Wz;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/2Sc;


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;LX/2Sc;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1502170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1502171
    iput-object p1, p0, LX/9Wz;->a:LX/0tX;

    .line 1502172
    iput-object p2, p0, LX/9Wz;->b:LX/1Ck;

    .line 1502173
    iput-object p3, p0, LX/9Wz;->c:LX/2Sc;

    .line 1502174
    return-void
.end method

.method public static a(LX/15i;I)Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;
    .locals 6
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "convert"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 1502175
    const v0, 0x3b86340b

    invoke-static {p0, p1, v0}, LX/3Sm;->a(LX/15i;II)LX/1vs;

    .line 1502176
    const-class v0, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel$SearchResultsModel$EdgesModel$NodeModel;

    invoke-virtual {p0, p1, v5, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1502177
    const-class v0, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel$SearchResultsModel$EdgesModel$NodeModel;

    invoke-virtual {p0, p1, v5, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel$SearchResultsModel$EdgesModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel$SearchResultsModel$EdgesModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1502178
    const-class v0, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel$SearchResultsModel$EdgesModel$NodeModel;

    invoke-virtual {p0, p1, v5, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel$SearchResultsModel$EdgesModel$NodeModel;

    .line 1502179
    invoke-virtual {v0}, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel$SearchResultsModel$EdgesModel$NodeModel;->c()Ljava/lang/String;

    move-result-object v2

    .line 1502180
    invoke-virtual {v0}, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel$SearchResultsModel$EdgesModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    .line 1502181
    invoke-virtual {v0}, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel$SearchResultsModel$EdgesModel$NodeModel;->e()Ljava/lang/String;

    move-result-object v4

    .line 1502182
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1502183
    new-instance v0, LX/7C4;

    sget-object v1, LX/3Ql;->BAD_SUGGESTION:LX/3Ql;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Missing id for entity of type "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7C4;-><init>(LX/3Ql;Ljava/lang/String;)V

    throw v0

    .line 1502184
    :cond_0
    const-class v1, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel$SearchResultsModel$EdgesModel$NodeModel;

    invoke-virtual {p0, p1, v5, v1}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel$SearchResultsModel$EdgesModel$NodeModel;

    invoke-virtual {v1}, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel$SearchResultsModel$EdgesModel$NodeModel;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1502185
    new-instance v0, LX/7C4;

    sget-object v1, LX/3Ql;->BAD_SUGGESTION:LX/3Ql;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Missing name for entity with type "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", id "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7C4;-><init>(LX/3Ql;Ljava/lang/String;)V

    throw v0

    .line 1502186
    :cond_1
    new-instance v1, LX/9X1;

    invoke-direct {v1}, LX/9X1;-><init>()V

    .line 1502187
    iput-object v2, v1, LX/9X1;->b:Ljava/lang/String;

    .line 1502188
    move-object v1, v1

    .line 1502189
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 1502190
    iput-object v2, v1, LX/9X1;->c:Ljava/lang/Integer;

    .line 1502191
    move-object v1, v1

    .line 1502192
    iput-object v4, v1, LX/9X1;->a:Ljava/lang/String;

    .line 1502193
    move-object v1, v1

    .line 1502194
    const/4 v2, 0x0

    invoke-virtual {p0, p1, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    .line 1502195
    iput-object v2, v1, LX/9X1;->f:Ljava/lang/String;

    .line 1502196
    move-object v1, v1

    .line 1502197
    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    .line 1502198
    iput-object v2, v1, LX/9X1;->e:Ljava/lang/String;

    .line 1502199
    move-object v1, v1

    .line 1502200
    invoke-virtual {v0}, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel$SearchResultsModel$EdgesModel$NodeModel;->d()Z

    move-result v2

    .line 1502201
    iput-boolean v2, v1, LX/9X1;->g:Z

    .line 1502202
    move-object v1, v1

    .line 1502203
    invoke-virtual {v0}, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel$SearchResultsModel$EdgesModel$NodeModel;->fR_()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1502204
    const-class v0, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel$SearchResultsModel$EdgesModel$NodeModel;

    invoke-virtual {p0, p1, v5, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel$SearchResultsModel$EdgesModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel$SearchResultsModel$EdgesModel$NodeModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1502205
    iput-object v0, v1, LX/9X1;->d:Landroid/net/Uri;

    .line 1502206
    :cond_2
    new-instance v0, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    invoke-direct {v0, v1}, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;-><init>(LX/9X1;)V

    move-object v0, v0

    .line 1502207
    return-object v0
.end method
