.class public final LX/9kv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:LX/9kw;


# direct methods
.method public constructor <init>(LX/9kw;)V
    .locals 0

    .prologue
    .line 1532262
    iput-object p1, p0, LX/9kv;->a:LX/9kw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 1532263
    iget-object v0, p0, LX/9kv;->a:LX/9kw;

    iget-object v1, v0, LX/9kw;->b:Landroid/widget/ImageButton;

    iget-object v0, p0, LX/9kv;->a:LX/9kw;

    iget-object v0, v0, LX/9kw;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x4

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1532264
    iget-object v0, p0, LX/9kv;->a:LX/9kw;

    iget-object v0, v0, LX/9kw;->i:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1532265
    iget-object v0, p0, LX/9kv;->a:LX/9kw;

    iget-object v0, v0, LX/9kw;->i:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/TextWatcher;

    invoke-interface {v0, p1}, Landroid/text/TextWatcher;->afterTextChanged(Landroid/text/Editable;)V

    .line 1532266
    :cond_0
    return-void

    .line 1532267
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 1532268
    iget-object v0, p0, LX/9kv;->a:LX/9kw;

    iget-object v0, v0, LX/9kw;->i:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1532269
    iget-object v0, p0, LX/9kv;->a:LX/9kw;

    iget-object v0, v0, LX/9kw;->i:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/TextWatcher;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/text/TextWatcher;->beforeTextChanged(Ljava/lang/CharSequence;III)V

    .line 1532270
    :cond_0
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 1532271
    iget-object v0, p0, LX/9kv;->a:LX/9kw;

    iget-object v0, v0, LX/9kw;->i:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1532272
    iget-object v0, p0, LX/9kv;->a:LX/9kw;

    iget-object v0, v0, LX/9kw;->i:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/TextWatcher;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/text/TextWatcher;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 1532273
    :cond_0
    return-void
.end method
