.class public LX/8uU;
.super Landroid/graphics/drawable/Drawable;
.source ""

# interfaces
.implements Landroid/graphics/drawable/Drawable$Callback;


# static fields
.field private static final a:I

.field private static final b:I


# instance fields
.field public c:LX/8uT;

.field private final d:Landroid/graphics/Paint;

.field private final e:Landroid/graphics/Paint;

.field public f:F

.field public g:F

.field private h:I

.field private i:Landroid/graphics/ColorFilter;

.field public j:I

.field public k:[Landroid/graphics/drawable/ShapeDrawable;

.field public l:I

.field public m:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v2, 0xff

    const/16 v1, 0xee

    .line 1414889
    const/16 v0, 0x7f

    invoke-static {v0, v2, v2, v2}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    sput v0, LX/8uU;->a:I

    .line 1414890
    invoke-static {v1, v1, v1}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    sput v0, LX/8uU;->b:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1414891
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 1414892
    sget-object v0, LX/8uT;->SQUARE:LX/8uT;

    iput-object v0, p0, LX/8uU;->c:LX/8uT;

    .line 1414893
    const/16 v0, 0xff

    iput v0, p0, LX/8uU;->h:I

    .line 1414894
    iput v4, p0, LX/8uU;->j:I

    .line 1414895
    iput v2, p0, LX/8uU;->l:I

    .line 1414896
    sget-object v0, LX/03r;->ThreadTileDrawable:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1414897
    const/16 v1, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    .line 1414898
    const/16 v2, 0x3

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    .line 1414899
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1414900
    cmpl-float v0, v2, v3

    if-lez v0, :cond_1

    .line 1414901
    iput v2, p0, LX/8uU;->f:F

    .line 1414902
    :goto_0
    if-eqz v1, :cond_0

    .line 1414903
    iget v0, p0, LX/8uU;->l:I

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1414904
    sget-object v0, LX/8uT;->CIRCLE:LX/8uT;

    iput-object v0, p0, LX/8uU;->c:LX/8uT;

    .line 1414905
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, LX/8uU;->f:F

    .line 1414906
    :cond_0
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/8uU;->d:Landroid/graphics/Paint;

    .line 1414907
    iget-object v0, p0, LX/8uU;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 1414908
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/8uU;->e:Landroid/graphics/Paint;

    .line 1414909
    iget-object v0, p0, LX/8uU;->e:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {p1, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1414910
    iget-object v0, p0, LX/8uU;->e:Landroid/graphics/Paint;

    sget v1, LX/8uU;->a:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1414911
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/graphics/drawable/ShapeDrawable;

    iput-object v0, p0, LX/8uU;->k:[Landroid/graphics/drawable/ShapeDrawable;

    .line 1414912
    return-void

    .line 1414913
    :cond_1
    const v0, 0x3f2aaaab

    iput v0, p0, LX/8uU;->f:F

    goto :goto_0

    .line 1414914
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(I[F)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1414915
    if-eqz p1, :cond_0

    if-ltz p1, :cond_2

    iget v0, p0, LX/8uU;->l:I

    if-ge p1, v0, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1414916
    if-eqz p2, :cond_1

    array-length v0, p2

    const/16 v3, 0x8

    if-ne v0, v3, :cond_3

    :cond_1
    move v0, v2

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1414917
    iget-object v0, p0, LX/8uU;->c:LX/8uT;

    sget-object v3, LX/8uT;->FILLED_ROUND_RECT:LX/8uT;

    if-ne v0, v3, :cond_4

    :goto_2
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 1414918
    iget-object v0, p0, LX/8uU;->k:[Landroid/graphics/drawable/ShapeDrawable;

    aget-object v1, v0, p1

    .line 1414919
    invoke-virtual {v1}, Landroid/graphics/drawable/ShapeDrawable;->getShape()Landroid/graphics/drawable/shapes/Shape;

    move-result-object v0

    check-cast v0, LX/8uM;

    .line 1414920
    invoke-virtual {v0, p2}, LX/8uM;->a([F)V

    .line 1414921
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/ShapeDrawable;->setShape(Landroid/graphics/drawable/shapes/Shape;)V

    .line 1414922
    return-void

    :cond_2
    move v0, v1

    .line 1414923
    goto :goto_0

    :cond_3
    move v0, v1

    .line 1414924
    goto :goto_1

    :cond_4
    move v2, v1

    .line 1414925
    goto :goto_2
.end method

.method public static a(LX/8uU;IFF)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1414956
    if-eqz p1, :cond_0

    if-ltz p1, :cond_2

    iget v0, p0, LX/8uU;->l:I

    if-ge p1, v0, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1414957
    iget-object v0, p0, LX/8uU;->c:LX/8uT;

    sget-object v3, LX/8uT;->CIRCLE:LX/8uT;

    if-ne v0, v3, :cond_3

    :goto_1
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 1414958
    iget-object v0, p0, LX/8uU;->k:[Landroid/graphics/drawable/ShapeDrawable;

    aget-object v1, v0, p1

    .line 1414959
    invoke-virtual {v1}, Landroid/graphics/drawable/ShapeDrawable;->getShape()Landroid/graphics/drawable/shapes/Shape;

    move-result-object v0

    check-cast v0, LX/8uR;

    .line 1414960
    invoke-virtual {v0, p2, p3}, LX/8uR;->a(FF)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1414961
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/ShapeDrawable;->setShape(Landroid/graphics/drawable/shapes/Shape;)V

    .line 1414962
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 1414963
    goto :goto_0

    :cond_3
    move v2, v1

    .line 1414964
    goto :goto_1
.end method

.method private static c(LX/8uU;)Landroid/graphics/drawable/ShapeDrawable;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1414926
    sget-object v0, LX/8uS;->a:[I

    iget-object v1, p0, LX/8uU;->c:LX/8uT;

    invoke-virtual {v1}, LX/8uT;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1414927
    new-instance v0, Landroid/graphics/drawable/shapes/RectShape;

    invoke-direct {v0}, Landroid/graphics/drawable/shapes/RectShape;-><init>()V

    .line 1414928
    :goto_0
    new-instance v1, Landroid/graphics/drawable/ShapeDrawable;

    invoke-direct {v1, v0}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 1414929
    invoke-virtual {v1, p0}, Landroid/graphics/drawable/ShapeDrawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1414930
    iget v0, p0, LX/8uU;->h:I

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/ShapeDrawable;->setAlpha(I)V

    .line 1414931
    iget-object v0, p0, LX/8uU;->i:Landroid/graphics/ColorFilter;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/ShapeDrawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 1414932
    invoke-virtual {p0}, LX/8uU;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/ShapeDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1414933
    invoke-virtual {v1}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    .line 1414934
    sget v2, LX/8uU;->b:I

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1414935
    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setDither(Z)V

    .line 1414936
    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 1414937
    return-object v1

    .line 1414938
    :pswitch_0
    new-instance v0, LX/8uR;

    const/high16 v1, 0x42b40000    # 90.0f

    const/high16 v2, 0x43b40000    # 360.0f

    invoke-direct {v0, v1, v2}, LX/8uR;-><init>(FF)V

    goto :goto_0

    .line 1414939
    :pswitch_1
    new-instance v0, LX/8uM;

    const/16 v1, 0x8

    new-array v1, v1, [F

    const/4 v2, 0x0

    iget v3, p0, LX/8uU;->g:F

    aput v3, v1, v2

    iget v2, p0, LX/8uU;->g:F

    aput v2, v1, v4

    const/4 v2, 0x2

    iget v3, p0, LX/8uU;->g:F

    aput v3, v1, v2

    const/4 v2, 0x3

    iget v3, p0, LX/8uU;->g:F

    aput v3, v1, v2

    const/4 v2, 0x4

    iget v3, p0, LX/8uU;->g:F

    aput v3, v1, v2

    const/4 v2, 0x5

    iget v3, p0, LX/8uU;->g:F

    aput v3, v1, v2

    const/4 v2, 0x6

    iget v3, p0, LX/8uU;->g:F

    aput v3, v1, v2

    const/4 v2, 0x7

    iget v3, p0, LX/8uU;->g:F

    aput v3, v1, v2

    invoke-direct {v0, v1, v5, v5}, LX/8uM;-><init>([FLandroid/graphics/RectF;[F)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static d(LX/8uU;)V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 1414825
    iget-object v0, p0, LX/8uU;->c:LX/8uT;

    sget-object v3, LX/8uT;->FILLED_ROUND_RECT:LX/8uT;

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1414826
    iget v0, p0, LX/8uU;->l:I

    packed-switch v0, :pswitch_data_0

    .line 1414827
    const/16 v0, 0x8

    new-array v0, v0, [F

    iget v3, p0, LX/8uU;->g:F

    aput v3, v0, v2

    iget v3, p0, LX/8uU;->g:F

    aput v3, v0, v1

    aput v5, v0, v6

    aput v5, v0, v7

    const/4 v3, 0x4

    aput v5, v0, v3

    const/4 v3, 0x5

    aput v5, v0, v3

    const/4 v3, 0x6

    iget v4, p0, LX/8uU;->g:F

    aput v4, v0, v3

    const/4 v3, 0x7

    iget v4, p0, LX/8uU;->g:F

    aput v4, v0, v3

    invoke-direct {p0, v2, v0}, LX/8uU;->a(I[F)V

    .line 1414828
    const/16 v0, 0x8

    new-array v0, v0, [F

    aput v5, v0, v2

    aput v5, v0, v1

    iget v3, p0, LX/8uU;->g:F

    aput v3, v0, v6

    iget v3, p0, LX/8uU;->g:F

    aput v3, v0, v7

    const/4 v3, 0x4

    aput v5, v0, v3

    const/4 v3, 0x5

    aput v5, v0, v3

    const/4 v3, 0x6

    aput v5, v0, v3

    const/4 v3, 0x7

    aput v5, v0, v3

    invoke-direct {p0, v1, v0}, LX/8uU;->a(I[F)V

    .line 1414829
    const/16 v0, 0x8

    new-array v0, v0, [F

    aput v5, v0, v2

    aput v5, v0, v1

    aput v5, v0, v6

    aput v5, v0, v7

    const/4 v1, 0x4

    iget v2, p0, LX/8uU;->g:F

    aput v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, LX/8uU;->g:F

    aput v2, v0, v1

    const/4 v1, 0x6

    aput v5, v0, v1

    const/4 v1, 0x7

    aput v5, v0, v1

    invoke-direct {p0, v6, v0}, LX/8uU;->a(I[F)V

    .line 1414830
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 1414831
    goto :goto_0

    .line 1414832
    :pswitch_0
    const/16 v0, 0x8

    new-array v0, v0, [F

    iget v3, p0, LX/8uU;->g:F

    aput v3, v0, v2

    iget v3, p0, LX/8uU;->g:F

    aput v3, v0, v1

    iget v1, p0, LX/8uU;->g:F

    aput v1, v0, v6

    iget v1, p0, LX/8uU;->g:F

    aput v1, v0, v7

    const/4 v1, 0x4

    iget v3, p0, LX/8uU;->g:F

    aput v3, v0, v1

    const/4 v1, 0x5

    iget v3, p0, LX/8uU;->g:F

    aput v3, v0, v1

    const/4 v1, 0x6

    iget v3, p0, LX/8uU;->g:F

    aput v3, v0, v1

    const/4 v1, 0x7

    iget v3, p0, LX/8uU;->g:F

    aput v3, v0, v1

    invoke-direct {p0, v2, v0}, LX/8uU;->a(I[F)V

    goto :goto_1

    .line 1414833
    :pswitch_1
    const/16 v0, 0x8

    new-array v0, v0, [F

    iget v3, p0, LX/8uU;->g:F

    aput v3, v0, v2

    iget v3, p0, LX/8uU;->g:F

    aput v3, v0, v1

    aput v5, v0, v6

    aput v5, v0, v7

    const/4 v3, 0x4

    aput v5, v0, v3

    const/4 v3, 0x5

    aput v5, v0, v3

    const/4 v3, 0x6

    iget v4, p0, LX/8uU;->g:F

    aput v4, v0, v3

    const/4 v3, 0x7

    iget v4, p0, LX/8uU;->g:F

    aput v4, v0, v3

    invoke-direct {p0, v2, v0}, LX/8uU;->a(I[F)V

    .line 1414834
    const/16 v0, 0x8

    new-array v0, v0, [F

    aput v5, v0, v2

    aput v5, v0, v1

    iget v2, p0, LX/8uU;->g:F

    aput v2, v0, v6

    iget v2, p0, LX/8uU;->g:F

    aput v2, v0, v7

    const/4 v2, 0x4

    iget v3, p0, LX/8uU;->g:F

    aput v3, v0, v2

    const/4 v2, 0x5

    iget v3, p0, LX/8uU;->g:F

    aput v3, v0, v2

    const/4 v2, 0x6

    aput v5, v0, v2

    const/4 v2, 0x7

    aput v5, v0, v2

    invoke-direct {p0, v1, v0}, LX/8uU;->a(I[F)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static f(LX/8uU;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1414940
    iget-object v0, p0, LX/8uU;->c:LX/8uT;

    sget-object v3, LX/8uT;->SQUARE:LX/8uT;

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1414941
    invoke-static {p0}, LX/8uU;->g(LX/8uU;)V

    .line 1414942
    invoke-virtual {p0}, LX/8uU;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 1414943
    iget v3, v0, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-float v4, v4

    iget v5, p0, LX/8uU;->f:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    float-to-int v3, v3

    .line 1414944
    iget v4, p0, LX/8uU;->l:I

    packed-switch v4, :pswitch_data_0

    .line 1414945
    iget-object v4, p0, LX/8uU;->k:[Landroid/graphics/drawable/ShapeDrawable;

    aget-object v2, v4, v2

    iget v4, v0, Landroid/graphics/Rect;->left:I

    iget v5, v0, Landroid/graphics/Rect;->top:I

    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v2, v4, v5, v3, v6}, Landroid/graphics/drawable/ShapeDrawable;->setBounds(IIII)V

    .line 1414946
    iget-object v2, p0, LX/8uU;->k:[Landroid/graphics/drawable/ShapeDrawable;

    aget-object v1, v2, v1

    iget v2, v0, Landroid/graphics/Rect;->top:I

    iget v4, v0, Landroid/graphics/Rect;->right:I

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerY()I

    move-result v5

    invoke-virtual {v1, v3, v2, v4, v5}, Landroid/graphics/drawable/ShapeDrawable;->setBounds(IIII)V

    .line 1414947
    iget-object v1, p0, LX/8uU;->k:[Landroid/graphics/drawable/ShapeDrawable;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    iget v4, v0, Landroid/graphics/Rect;->right:I

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, v3, v2, v4, v0}, Landroid/graphics/drawable/ShapeDrawable;->setBounds(IIII)V

    .line 1414948
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 1414949
    goto :goto_0

    .line 1414950
    :pswitch_0
    iget-object v1, p0, LX/8uU;->k:[Landroid/graphics/drawable/ShapeDrawable;

    aget-object v1, v1, v2

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/ShapeDrawable;->setBounds(Landroid/graphics/Rect;)V

    goto :goto_1

    .line 1414951
    :pswitch_1
    iget-object v4, p0, LX/8uU;->k:[Landroid/graphics/drawable/ShapeDrawable;

    aget-object v2, v4, v2

    iget v4, v0, Landroid/graphics/Rect;->left:I

    iget v5, v0, Landroid/graphics/Rect;->top:I

    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v2, v4, v5, v3, v6}, Landroid/graphics/drawable/ShapeDrawable;->setBounds(IIII)V

    .line 1414952
    iget-object v2, p0, LX/8uU;->k:[Landroid/graphics/drawable/ShapeDrawable;

    aget-object v1, v2, v1

    iget v2, v0, Landroid/graphics/Rect;->top:I

    iget v4, v0, Landroid/graphics/Rect;->right:I

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, v3, v2, v4, v0}, Landroid/graphics/drawable/ShapeDrawable;->setBounds(IIII)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static g(LX/8uU;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1414953
    iget-object v0, p0, LX/8uU;->k:[Landroid/graphics/drawable/ShapeDrawable;

    aget-object v0, v0, v2

    if-nez v0, :cond_0

    .line 1414954
    iget-object v0, p0, LX/8uU;->k:[Landroid/graphics/drawable/ShapeDrawable;

    invoke-static {p0}, LX/8uU;->c(LX/8uU;)Landroid/graphics/drawable/ShapeDrawable;

    move-result-object v1

    aput-object v1, v0, v2

    .line 1414955
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 1414844
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/8uU;->k:[Landroid/graphics/drawable/ShapeDrawable;

    array-length v0, v0

    if-gt p1, v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1414845
    iget v0, p0, LX/8uU;->l:I

    if-ne p1, v0, :cond_1

    .line 1414846
    :goto_1
    return-void

    .line 1414847
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1414848
    :cond_1
    invoke-static {p0}, LX/8uU;->g(LX/8uU;)V

    .line 1414849
    :goto_2
    iget-object v0, p0, LX/8uU;->k:[Landroid/graphics/drawable/ShapeDrawable;

    array-length v0, v0

    if-ge v1, v0, :cond_4

    .line 1414850
    if-ge v1, p1, :cond_3

    .line 1414851
    iget-object v0, p0, LX/8uU;->k:[Landroid/graphics/drawable/ShapeDrawable;

    aget-object v0, v0, v1

    if-nez v0, :cond_2

    .line 1414852
    iget-object v0, p0, LX/8uU;->k:[Landroid/graphics/drawable/ShapeDrawable;

    invoke-static {p0}, LX/8uU;->c(LX/8uU;)Landroid/graphics/drawable/ShapeDrawable;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1414853
    :cond_2
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1414854
    :cond_3
    iget-object v0, p0, LX/8uU;->k:[Landroid/graphics/drawable/ShapeDrawable;

    const/4 v2, 0x0

    aput-object v2, v0, v1

    goto :goto_3

    .line 1414855
    :cond_4
    iput p1, p0, LX/8uU;->l:I

    .line 1414856
    sget-object v0, LX/8uS;->a:[I

    iget-object v1, p0, LX/8uU;->c:LX/8uT;

    invoke-virtual {v1}, LX/8uT;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1414857
    invoke-static {p0}, LX/8uU;->f(LX/8uU;)V

    goto :goto_1

    .line 1414858
    :pswitch_0
    const/high16 p1, 0x43870000    # 270.0f

    const/4 v1, 0x1

    const/high16 v5, 0x43340000    # 180.0f

    const/4 v2, 0x0

    const/high16 v4, 0x42b40000    # 90.0f

    .line 1414859
    iget-object v0, p0, LX/8uU;->c:LX/8uT;

    sget-object v3, LX/8uT;->CIRCLE:LX/8uT;

    if-ne v0, v3, :cond_5

    move v0, v1

    :goto_4
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1414860
    iget v0, p0, LX/8uU;->l:I

    packed-switch v0, :pswitch_data_1

    .line 1414861
    invoke-static {p0, v2, v4, v5}, LX/8uU;->a(LX/8uU;IFF)V

    .line 1414862
    invoke-static {p0, v1, p1, v4}, LX/8uU;->a(LX/8uU;IFF)V

    .line 1414863
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-static {p0, v0, v1, v4}, LX/8uU;->a(LX/8uU;IFF)V

    .line 1414864
    :goto_5
    goto :goto_1

    .line 1414865
    :pswitch_1
    invoke-static {p0}, LX/8uU;->d(LX/8uU;)V

    goto :goto_1

    :cond_5
    move v0, v2

    .line 1414866
    goto :goto_4

    .line 1414867
    :pswitch_2
    const/high16 v0, 0x43b40000    # 360.0f

    invoke-static {p0, v2, v4, v0}, LX/8uU;->a(LX/8uU;IFF)V

    goto :goto_5

    .line 1414868
    :pswitch_3
    invoke-static {p0, v2, v4, v5}, LX/8uU;->a(LX/8uU;IFF)V

    .line 1414869
    invoke-static {p0, v1, p1, v5}, LX/8uU;->a(LX/8uU;IFF)V

    goto :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 9

    .prologue
    .line 1414870
    invoke-virtual {p0}, LX/8uU;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    .line 1414871
    invoke-static {p0}, LX/8uU;->g(LX/8uU;)V

    .line 1414872
    iget-boolean v0, p0, LX/8uU;->m:Z

    if-eqz v0, :cond_0

    .line 1414873
    invoke-virtual {v1}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v0

    invoke-virtual {v1}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    const/high16 v4, 0x40800000    # 4.0f

    sub-float/2addr v3, v4

    iget-object v4, p0, LX/8uU;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1414874
    :cond_0
    iget-object v2, p0, LX/8uU;->k:[Landroid/graphics/drawable/ShapeDrawable;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    .line 1414875
    if-eqz v4, :cond_1

    .line 1414876
    invoke-virtual {v4, p1}, Landroid/graphics/drawable/ShapeDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 1414877
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1414878
    :cond_2
    iget v0, p0, LX/8uU;->l:I

    const/4 v2, 0x1

    if-le v0, v2, :cond_3

    .line 1414879
    iget v0, v1, Landroid/graphics/Rect;->left:I

    .line 1414880
    iget v2, v1, Landroid/graphics/Rect;->top:I

    .line 1414881
    iget v3, v1, Landroid/graphics/Rect;->right:I

    .line 1414882
    iget v6, v1, Landroid/graphics/Rect;->bottom:I

    .line 1414883
    sub-int v1, v3, v0

    int-to-float v1, v1

    iget v3, p0, LX/8uU;->f:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    add-int v7, v0, v1

    .line 1414884
    sub-int v0, v6, v2

    div-int/lit8 v0, v0, 0x2

    add-int v8, v2, v0

    .line 1414885
    int-to-float v1, v7

    int-to-float v2, v2

    int-to-float v3, v7

    int-to-float v4, v6

    iget-object v5, p0, LX/8uU;->e:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1414886
    iget v0, p0, LX/8uU;->l:I

    const/4 v1, 0x2

    if-le v0, v1, :cond_3

    .line 1414887
    int-to-float v1, v7

    int-to-float v2, v8

    int-to-float v3, v6

    int-to-float v4, v8

    iget-object v5, p0, LX/8uU;->e:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1414888
    :cond_3
    return-void
.end method

.method public final getIntrinsicHeight()I
    .locals 1

    .prologue
    .line 1414843
    iget v0, p0, LX/8uU;->j:I

    return v0
.end method

.method public final getIntrinsicWidth()I
    .locals 1

    .prologue
    .line 1414842
    iget v0, p0, LX/8uU;->j:I

    return v0
.end method

.method public final getOpacity()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1414837
    iget-object v2, p0, LX/8uU;->k:[Landroid/graphics/drawable/ShapeDrawable;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 1414838
    if-eqz v4, :cond_0

    .line 1414839
    invoke-virtual {v4}, Landroid/graphics/drawable/ShapeDrawable;->getOpacity()I

    move-result v4

    invoke-static {v0, v4}, Landroid/graphics/drawable/Drawable;->resolveOpacity(II)I

    move-result v0

    .line 1414840
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1414841
    :cond_1
    return v0
.end method

.method public final invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 1414835
    invoke-virtual {p0}, LX/8uU;->invalidateSelf()V

    .line 1414836
    return-void
.end method

.method public final scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V
    .locals 1

    .prologue
    .line 1414823
    invoke-virtual {p0, p2, p3, p4}, LX/8uU;->scheduleSelf(Ljava/lang/Runnable;J)V

    .line 1414824
    return-void
.end method

.method public final setAlpha(I)V
    .locals 4

    .prologue
    .line 1414817
    iput p1, p0, LX/8uU;->h:I

    .line 1414818
    iget-object v1, p0, LX/8uU;->k:[Landroid/graphics/drawable/ShapeDrawable;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 1414819
    if-eqz v3, :cond_0

    .line 1414820
    invoke-virtual {v3, p1}, Landroid/graphics/drawable/ShapeDrawable;->setAlpha(I)V

    .line 1414821
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1414822
    :cond_1
    return-void
.end method

.method public final setBounds(IIII)V
    .locals 4

    .prologue
    .line 1414808
    invoke-super {p0, p1, p2, p3, p4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1414809
    sget-object v0, LX/8uS;->a:[I

    iget-object v1, p0, LX/8uU;->c:LX/8uT;

    invoke-virtual {v1}, LX/8uT;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1414810
    invoke-static {p0}, LX/8uU;->f(LX/8uU;)V

    .line 1414811
    :cond_0
    :goto_0
    return-void

    .line 1414812
    :pswitch_0
    iget-object v1, p0, LX/8uU;->k:[Landroid/graphics/drawable/ShapeDrawable;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 1414813
    if-eqz v3, :cond_1

    .line 1414814
    invoke-virtual {v3, p1, p2, p3, p4}, Landroid/graphics/drawable/ShapeDrawable;->setBounds(IIII)V

    .line 1414815
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1414816
    :pswitch_1
    invoke-static {p0}, LX/8uU;->d(LX/8uU;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 4

    .prologue
    .line 1414802
    iput-object p1, p0, LX/8uU;->i:Landroid/graphics/ColorFilter;

    .line 1414803
    iget-object v1, p0, LX/8uU;->k:[Landroid/graphics/drawable/ShapeDrawable;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 1414804
    if-eqz v3, :cond_0

    .line 1414805
    invoke-virtual {v3, p1}, Landroid/graphics/drawable/ShapeDrawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 1414806
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1414807
    :cond_1
    return-void
.end method

.method public final unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 1414800
    invoke-virtual {p0, p2}, LX/8uU;->unscheduleSelf(Ljava/lang/Runnable;)V

    .line 1414801
    return-void
.end method
