.class public LX/9bx;
.super Landroid/view/View;
.source ""


# instance fields
.field public A:F

.field public B:F

.field public C:F

.field public D:LX/9eX;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;

.field public F:Z

.field public G:LX/9bv;

.field public H:Z

.field public I:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

.field private final a:Landroid/graphics/Rect;

.field public b:LX/9bw;

.field public c:F

.field public d:F

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Z

.field public k:Z

.field public l:Z

.field public m:F

.field public n:F

.field public o:F

.field public p:F

.field public q:F

.field public r:Landroid/graphics/RectF;

.field public s:Landroid/graphics/RectF;

.field public t:Landroid/graphics/RectF;

.field public u:Landroid/graphics/Paint;

.field public v:Landroid/graphics/Paint;

.field public w:Landroid/graphics/Paint;

.field public x:Landroid/graphics/Paint;

.field public y:Landroid/graphics/Paint;

.field public z:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1515452
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1515453
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/9bx;->a:Landroid/graphics/Rect;

    .line 1515454
    iput-boolean v1, p0, LX/9bx;->e:Z

    .line 1515455
    iput-boolean v1, p0, LX/9bx;->f:Z

    .line 1515456
    iput-boolean v1, p0, LX/9bx;->g:Z

    .line 1515457
    iput-boolean v1, p0, LX/9bx;->h:Z

    .line 1515458
    iput-boolean v1, p0, LX/9bx;->i:Z

    .line 1515459
    iput-boolean v1, p0, LX/9bx;->j:Z

    .line 1515460
    iput-boolean v1, p0, LX/9bx;->k:Z

    .line 1515461
    iput-boolean v1, p0, LX/9bx;->l:Z

    .line 1515462
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/9bx;->r:Landroid/graphics/RectF;

    .line 1515463
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/9bx;->s:Landroid/graphics/RectF;

    .line 1515464
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    .line 1515465
    iput-boolean v1, p0, LX/9bx;->F:Z

    .line 1515466
    sget-object v0, LX/9bv;->FREE_FORM:LX/9bv;

    iput-object v0, p0, LX/9bx;->G:LX/9bv;

    .line 1515467
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/9bx;->H:Z

    .line 1515468
    const/4 p1, 0x1

    const v7, -0xff8501

    const v6, -0x5a000001

    const/high16 v5, 0x40e00000    # 7.0f

    .line 1515469
    invoke-virtual {p0}, LX/9bx;->a()V

    .line 1515470
    new-instance v0, LX/9bu;

    invoke-direct {v0, p0}, LX/9bu;-><init>(LX/9bx;)V

    move-object v0, v0

    .line 1515471
    invoke-virtual {p0, v0}, LX/9bx;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1515472
    invoke-virtual {p0}, LX/9bx;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 1515473
    const/high16 v1, 0x3f800000    # 1.0f

    mul-float/2addr v1, v0

    .line 1515474
    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v2, v0

    .line 1515475
    const/high16 v3, 0x3fc00000    # 1.5f

    mul-float/2addr v3, v0

    .line 1515476
    mul-float v4, v5, v0

    iput v4, p0, LX/9bx;->z:F

    .line 1515477
    const/high16 v4, 0x41f00000    # 30.0f

    mul-float/2addr v4, v0

    iput v4, p0, LX/9bx;->A:F

    .line 1515478
    iput v1, p0, LX/9bx;->B:F

    .line 1515479
    const/high16 v4, 0x42a00000    # 80.0f

    mul-float/2addr v4, v0

    iput v4, p0, LX/9bx;->C:F

    .line 1515480
    mul-float/2addr v0, v5

    iput v0, p0, LX/9bx;->q:F

    .line 1515481
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/9bx;->u:Landroid/graphics/Paint;

    .line 1515482
    iget-object v0, p0, LX/9bx;->u:Landroid/graphics/Paint;

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 1515483
    iget-object v0, p0, LX/9bx;->u:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1515484
    iget-object v0, p0, LX/9bx;->u:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1515485
    iget-object v0, p0, LX/9bx;->u:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1515486
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/9bx;->v:Landroid/graphics/Paint;

    .line 1515487
    iget-object v0, p0, LX/9bx;->v:Landroid/graphics/Paint;

    invoke-virtual {v0, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 1515488
    iget-object v0, p0, LX/9bx;->v:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1515489
    iget-object v0, p0, LX/9bx;->v:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1515490
    iget-object v0, p0, LX/9bx;->v:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1515491
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/9bx;->w:Landroid/graphics/Paint;

    .line 1515492
    iget-object v0, p0, LX/9bx;->w:Landroid/graphics/Paint;

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 1515493
    iget-object v0, p0, LX/9bx;->w:Landroid/graphics/Paint;

    const/16 v1, 0x96

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1515494
    iget-object v0, p0, LX/9bx;->w:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1515495
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/9bx;->x:Landroid/graphics/Paint;

    .line 1515496
    iget-object v0, p0, LX/9bx;->x:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1515497
    iget-object v0, p0, LX/9bx;->x:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1515498
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/9bx;->y:Landroid/graphics/Paint;

    .line 1515499
    iget-object v0, p0, LX/9bx;->y:Landroid/graphics/Paint;

    invoke-virtual {v0, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 1515500
    iget-object v0, p0, LX/9bx;->y:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1515501
    iget-object v0, p0, LX/9bx;->y:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1515502
    invoke-virtual {p0}, LX/9bx;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0813c1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/9bx;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1515503
    new-instance v0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;

    invoke-direct {v0}, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;-><init>()V

    iput-object v0, p0, LX/9bx;->E:Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;

    .line 1515504
    return-void
.end method

.method public static b$redex0(LX/9bx;FF)V
    .locals 14

    .prologue
    .line 1515505
    iget v0, p0, LX/9bx;->c:F

    sub-float v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v10

    .line 1515506
    iget v0, p0, LX/9bx;->d:F

    sub-float v0, p2, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v11

    .line 1515507
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget-object v1, p0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    div-float v12, v0, v1

    .line 1515508
    iget v0, p0, LX/9bx;->B:F

    cmpl-float v0, v10, v0

    if-gtz v0, :cond_0

    iget v0, p0, LX/9bx;->B:F

    cmpl-float v0, v11, v0

    if-lez v0, :cond_5

    .line 1515509
    :cond_0
    cmpl-float v0, v10, v11

    if-lez v0, :cond_6

    .line 1515510
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    .line 1515511
    :cond_1
    :goto_0
    return-void

    .line 1515512
    :cond_2
    iget v0, p0, LX/9bx;->c:F

    sub-float v0, p1, v0

    iget-object v1, p0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    div-float/2addr v0, v1

    .line 1515513
    iget v1, p0, LX/9bx;->c:F

    sub-float v1, p1, v1

    .line 1515514
    iget-object v2, p0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    mul-float/2addr v0, v2

    .line 1515515
    :goto_1
    iget v2, p0, LX/9bx;->c:F

    cmpg-float v2, p1, v2

    if-gez v2, :cond_7

    const/4 v2, 0x1

    .line 1515516
    :goto_2
    iget v3, p0, LX/9bx;->c:F

    cmpl-float v3, p1, v3

    if-lez v3, :cond_8

    const/4 v3, 0x1

    .line 1515517
    :goto_3
    iget v4, p0, LX/9bx;->d:F

    cmpg-float v4, p2, v4

    if-gez v4, :cond_9

    const/4 v4, 0x1

    .line 1515518
    :goto_4
    iget v5, p0, LX/9bx;->d:F

    cmpl-float v5, p2, v5

    if-lez v5, :cond_a

    const/4 v5, 0x1

    .line 1515519
    :goto_5
    iget-object v6, p0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v6

    sub-float/2addr v6, v1

    iget v7, p0, LX/9bx;->C:F

    cmpl-float v6, v6, v7

    if-lez v6, :cond_b

    const/4 v6, 0x1

    .line 1515520
    :goto_6
    iget-object v7, p0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/RectF;->width()F

    move-result v7

    add-float/2addr v7, v1

    iget v8, p0, LX/9bx;->C:F

    cmpl-float v7, v7, v8

    if-lez v7, :cond_c

    const/4 v7, 0x1

    .line 1515521
    :goto_7
    iget-object v8, p0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v8}, Landroid/graphics/RectF;->height()F

    move-result v8

    sub-float/2addr v8, v0

    iget v9, p0, LX/9bx;->C:F

    cmpl-float v8, v8, v9

    if-lez v8, :cond_d

    const/4 v8, 0x1

    .line 1515522
    :goto_8
    iget-object v9, p0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v9}, Landroid/graphics/RectF;->height()F

    move-result v9

    add-float/2addr v9, v0

    iget v13, p0, LX/9bx;->C:F

    cmpl-float v9, v9, v13

    if-lez v9, :cond_e

    const/4 v9, 0x1

    .line 1515523
    :goto_9
    iget-boolean v13, p0, LX/9bx;->i:Z

    if-eqz v13, :cond_11

    .line 1515524
    if-nez v2, :cond_3

    if-eqz v6, :cond_5

    :cond_3
    if-nez v4, :cond_4

    if-eqz v8, :cond_5

    .line 1515525
    :cond_4
    iget-object v2, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    add-float/2addr v2, v1

    iget-object v3, p0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_f

    .line 1515526
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v1, p0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iput v1, p0, LX/9bx;->o:F

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 1515527
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v1, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    iget-object v2, p0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    div-float/2addr v2, v12

    sub-float/2addr v1, v2

    iput v1, p0, LX/9bx;->n:F

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 1515528
    :cond_5
    :goto_a
    iget-object v0, p0, LX/9bx;->E:Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->f:Z

    .line 1515529
    invoke-virtual {p0}, LX/9bx;->invalidate()V

    goto/16 :goto_0

    .line 1515530
    :cond_6
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    .line 1515531
    iget v0, p0, LX/9bx;->d:F

    sub-float v0, p2, v0

    iget-object v1, p0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    div-float/2addr v0, v1

    .line 1515532
    iget-object v1, p0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    mul-float/2addr v1, v0

    .line 1515533
    iget v0, p0, LX/9bx;->d:F

    sub-float v0, p2, v0

    goto/16 :goto_1

    .line 1515534
    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 1515535
    :cond_8
    const/4 v3, 0x0

    goto/16 :goto_3

    .line 1515536
    :cond_9
    const/4 v4, 0x0

    goto/16 :goto_4

    .line 1515537
    :cond_a
    const/4 v5, 0x0

    goto/16 :goto_5

    .line 1515538
    :cond_b
    const/4 v6, 0x0

    goto/16 :goto_6

    .line 1515539
    :cond_c
    const/4 v7, 0x0

    goto/16 :goto_7

    .line 1515540
    :cond_d
    const/4 v8, 0x0

    goto/16 :goto_8

    .line 1515541
    :cond_e
    const/4 v9, 0x0

    goto :goto_9

    .line 1515542
    :cond_f
    iget-object v2, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    add-float/2addr v2, v0

    iget-object v3, p0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_10

    .line 1515543
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v1, p0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    iput v1, p0, LX/9bx;->n:F

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 1515544
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v1, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    iget-object v2, p0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    mul-float/2addr v2, v12

    sub-float/2addr v1, v2

    iput v1, p0, LX/9bx;->o:F

    iput v1, v0, Landroid/graphics/RectF;->left:F

    goto :goto_a

    .line 1515545
    :cond_10
    iget-object v2, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v3, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    add-float/2addr v1, v3

    iput v1, p0, LX/9bx;->o:F

    iput v1, v2, Landroid/graphics/RectF;->left:F

    .line 1515546
    iget-object v1, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v2, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    add-float/2addr v0, v2

    iput v0, p0, LX/9bx;->n:F

    iput v0, v1, Landroid/graphics/RectF;->top:F

    goto/16 :goto_a

    .line 1515547
    :cond_11
    iget-boolean v13, p0, LX/9bx;->j:Z

    if-eqz v13, :cond_1b

    .line 1515548
    cmpl-float v3, v10, v11

    if-lez v3, :cond_16

    .line 1515549
    if-nez v2, :cond_12

    if-eqz v6, :cond_5

    :cond_12
    if-nez v5, :cond_13

    if-eqz v9, :cond_5

    .line 1515550
    :cond_13
    iget-object v2, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    add-float/2addr v2, v1

    iget-object v3, p0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_14

    .line 1515551
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v1, p0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iput v1, p0, LX/9bx;->o:F

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 1515552
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v1, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    iget-object v2, p0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    div-float/2addr v2, v12

    add-float/2addr v1, v2

    iput v1, p0, LX/9bx;->n:F

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_a

    .line 1515553
    :cond_14
    iget-object v2, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v2, v0

    iget-object v3, p0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    cmpl-float v2, v2, v3

    if-lez v2, :cond_15

    .line 1515554
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v1, p0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    iput v1, p0, LX/9bx;->m:F

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 1515555
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v1, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    iget-object v2, p0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    mul-float/2addr v2, v12

    sub-float/2addr v1, v2

    iput v1, p0, LX/9bx;->o:F

    iput v1, v0, Landroid/graphics/RectF;->left:F

    goto/16 :goto_a

    .line 1515556
    :cond_15
    iget-object v2, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v3, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    add-float/2addr v1, v3

    iput v1, p0, LX/9bx;->o:F

    iput v1, v2, Landroid/graphics/RectF;->left:F

    .line 1515557
    iget-object v1, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v2, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    sub-float v0, v2, v0

    iput v0, p0, LX/9bx;->m:F

    iput v0, v1, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_a

    .line 1515558
    :cond_16
    if-nez v2, :cond_17

    if-eqz v7, :cond_5

    :cond_17
    if-nez v5, :cond_18

    if-eqz v8, :cond_5

    .line 1515559
    :cond_18
    iget-object v2, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v1

    iget-object v3, p0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_19

    .line 1515560
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v1, p0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iput v1, p0, LX/9bx;->o:F

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 1515561
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v1, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    iget-object v2, p0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    div-float/2addr v2, v12

    add-float/2addr v1, v2

    iput v1, p0, LX/9bx;->n:F

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_a

    .line 1515562
    :cond_19
    iget-object v2, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v2, v0

    iget-object v3, p0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    cmpl-float v2, v2, v3

    if-lez v2, :cond_1a

    .line 1515563
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v1, p0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    iput v1, p0, LX/9bx;->m:F

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 1515564
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v1, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    iget-object v2, p0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    mul-float/2addr v2, v12

    sub-float/2addr v1, v2

    iput v1, p0, LX/9bx;->o:F

    iput v1, v0, Landroid/graphics/RectF;->left:F

    goto/16 :goto_a

    .line 1515565
    :cond_1a
    iget-object v2, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v3, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    sub-float v1, v3, v1

    iput v1, p0, LX/9bx;->o:F

    iput v1, v2, Landroid/graphics/RectF;->left:F

    .line 1515566
    iget-object v1, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v2, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v0, v2

    iput v0, p0, LX/9bx;->m:F

    iput v0, v1, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_a

    .line 1515567
    :cond_1b
    iget-boolean v2, p0, LX/9bx;->k:Z

    if-eqz v2, :cond_25

    .line 1515568
    cmpl-float v2, v10, v11

    if-lez v2, :cond_20

    .line 1515569
    if-nez v3, :cond_1c

    if-eqz v6, :cond_5

    :cond_1c
    if-nez v4, :cond_1d

    if-eqz v9, :cond_5

    .line 1515570
    :cond_1d
    iget-object v2, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    add-float/2addr v2, v1

    iget-object v3, p0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    cmpl-float v2, v2, v3

    if-lez v2, :cond_1e

    .line 1515571
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v1, p0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    iput v1, p0, LX/9bx;->p:F

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 1515572
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v1, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    iget-object v2, p0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    div-float/2addr v2, v12

    sub-float/2addr v1, v2

    iput v1, p0, LX/9bx;->n:F

    iput v1, v0, Landroid/graphics/RectF;->top:F

    goto/16 :goto_a

    .line 1515573
    :cond_1e
    iget-object v2, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v0

    iget-object v3, p0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_1f

    .line 1515574
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v1, p0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    iput v1, p0, LX/9bx;->n:F

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 1515575
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v1, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    mul-float/2addr v2, v12

    add-float/2addr v1, v2

    iput v1, p0, LX/9bx;->o:F

    iput v1, v0, Landroid/graphics/RectF;->right:F

    goto/16 :goto_a

    .line 1515576
    :cond_1f
    iget-object v2, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v3, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    add-float/2addr v1, v3

    iput v1, p0, LX/9bx;->p:F

    iput v1, v2, Landroid/graphics/RectF;->right:F

    .line 1515577
    iget-object v1, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v2, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    sub-float v0, v2, v0

    iput v0, p0, LX/9bx;->n:F

    iput v0, v1, Landroid/graphics/RectF;->top:F

    goto/16 :goto_a

    .line 1515578
    :cond_20
    if-nez v3, :cond_21

    if-eqz v7, :cond_5

    :cond_21
    if-nez v4, :cond_22

    if-eqz v8, :cond_5

    .line 1515579
    :cond_22
    iget-object v2, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    sub-float/2addr v2, v1

    iget-object v3, p0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    cmpl-float v2, v2, v3

    if-lez v2, :cond_23

    .line 1515580
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v1, p0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    iput v1, p0, LX/9bx;->p:F

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 1515581
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v1, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    iget-object v2, p0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    div-float/2addr v2, v12

    sub-float/2addr v1, v2

    iput v1, p0, LX/9bx;->n:F

    iput v1, v0, Landroid/graphics/RectF;->top:F

    goto/16 :goto_a

    .line 1515582
    :cond_23
    iget-object v2, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    add-float/2addr v2, v0

    iget-object v3, p0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_24

    .line 1515583
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v1, p0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    iput v1, p0, LX/9bx;->n:F

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 1515584
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v1, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    mul-float/2addr v2, v12

    add-float/2addr v1, v2

    iput v1, p0, LX/9bx;->o:F

    iput v1, v0, Landroid/graphics/RectF;->right:F

    goto/16 :goto_a

    .line 1515585
    :cond_24
    iget-object v2, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v3, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    sub-float v1, v3, v1

    iput v1, p0, LX/9bx;->p:F

    iput v1, v2, Landroid/graphics/RectF;->right:F

    .line 1515586
    iget-object v1, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v2, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    add-float/2addr v0, v2

    iput v0, p0, LX/9bx;->n:F

    iput v0, v1, Landroid/graphics/RectF;->top:F

    goto/16 :goto_a

    .line 1515587
    :cond_25
    iget-boolean v2, p0, LX/9bx;->l:Z

    if-eqz v2, :cond_5

    .line 1515588
    if-nez v3, :cond_26

    if-eqz v6, :cond_5

    :cond_26
    if-nez v5, :cond_27

    if-eqz v8, :cond_5

    .line 1515589
    :cond_27
    iget-object v2, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    add-float/2addr v2, v1

    iget-object v3, p0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    cmpl-float v2, v2, v3

    if-lez v2, :cond_28

    .line 1515590
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v1, p0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    iput v1, p0, LX/9bx;->p:F

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 1515591
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v1, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    iget-object v2, p0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    div-float/2addr v2, v12

    add-float/2addr v1, v2

    iput v1, p0, LX/9bx;->n:F

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_a

    .line 1515592
    :cond_28
    iget-object v2, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v2, v0

    iget-object v3, p0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    cmpl-float v2, v2, v3

    if-lez v2, :cond_29

    .line 1515593
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v1, p0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    iput v1, p0, LX/9bx;->m:F

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 1515594
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v1, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    mul-float/2addr v2, v12

    add-float/2addr v1, v2

    iput v1, p0, LX/9bx;->o:F

    iput v1, v0, Landroid/graphics/RectF;->right:F

    goto/16 :goto_a

    .line 1515595
    :cond_29
    iget-object v2, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v3, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    add-float/2addr v1, v3

    iput v1, p0, LX/9bx;->p:F

    iput v1, v2, Landroid/graphics/RectF;->right:F

    .line 1515596
    iget-object v1, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v2, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v0, v2

    iput v0, p0, LX/9bx;->m:F

    iput v0, v1, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_a
.end method

.method public static setAdjustableOverlayItems(LX/9bx;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "LX/5i8;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1515597
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1515598
    :cond_0
    return-void

    .line 1515599
    :cond_1
    iget-object v0, p0, LX/9bx;->I:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->j()V

    .line 1515600
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5i8;

    .line 1515601
    iget-object v2, p0, LX/9bx;->I:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    invoke-virtual {v2, v0, p0}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->a(LX/5i8;Landroid/graphics/drawable/Drawable$Callback;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1515602
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, LX/9bx;->setVisibility(I)V

    .line 1515603
    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    const/high16 v2, 0x40400000    # 3.0f

    const/high16 v7, 0x40000000    # 2.0f

    .line 1515604
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 1515605
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1515606
    iget-object v0, p0, LX/9bx;->I:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9bx;->D:LX/9eX;

    if-eqz v0, :cond_0

    .line 1515607
    iget-object v0, p0, LX/9bx;->D:LX/9eX;

    .line 1515608
    iget-object v1, v0, LX/9eX;->a:LX/9ec;

    iget-object v1, v1, LX/9ec;->x:Landroid/graphics/RectF;

    if-eqz v1, :cond_2

    iget-object v1, v0, LX/9eX;->a:LX/9ec;

    iget-object v1, v1, LX/9ec;->x:Landroid/graphics/RectF;

    .line 1515609
    :goto_0
    if-nez v1, :cond_3

    .line 1515610
    const/4 v1, 0x0

    .line 1515611
    :goto_1
    move-object v0, v1

    .line 1515612
    if-eqz v0, :cond_0

    .line 1515613
    invoke-static {p0, v0}, LX/9bx;->setAdjustableOverlayItems(LX/9bx;Ljava/util/List;)V

    .line 1515614
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v1, p0, LX/9bx;->a:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    .line 1515615
    iget-object v0, p0, LX/9bx;->I:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    iget-object v1, p0, LX/9bx;->a:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    .line 1515616
    :cond_0
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    div-float/2addr v0, v2

    .line 1515617
    iget-object v1, p0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    div-float v6, v1, v2

    .line 1515618
    new-instance v1, Landroid/graphics/RectF;

    iget-object v2, p0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    iget-object v4, p0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    iget-object v5, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget-object v2, p0, LX/9bx;->w:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 1515619
    new-instance v1, Landroid/graphics/RectF;

    iget-object v2, p0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    iget-object v4, p0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    iget-object v5, p0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget-object v2, p0, LX/9bx;->w:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 1515620
    new-instance v1, Landroid/graphics/RectF;

    iget-object v2, p0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    iget-object v4, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    iget-object v5, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget-object v2, p0, LX/9bx;->w:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 1515621
    new-instance v1, Landroid/graphics/RectF;

    iget-object v2, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    iget-object v3, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    iget-object v4, p0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    iget-object v5, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget-object v2, p0, LX/9bx;->w:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 1515622
    iget-boolean v1, p0, LX/9bx;->F:Z

    if-eqz v1, :cond_1

    .line 1515623
    iget-object v1, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    add-float/2addr v1, v0

    iget-object v2, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    iget-object v3, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    mul-float/2addr v0, v7

    add-float/2addr v3, v0

    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v4, v0, Landroid/graphics/RectF;->bottom:F

    iget-object v5, p0, LX/9bx;->u:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1515624
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->top:F

    add-float v2, v0, v6

    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v3, v0, Landroid/graphics/RectF;->right:F

    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->top:F

    mul-float v4, v7, v6

    add-float/2addr v4, v0

    iget-object v5, p0, LX/9bx;->u:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1515625
    :cond_1
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v1, p0, LX/9bx;->v:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 1515626
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    iget-object v1, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    iget v2, p0, LX/9bx;->z:F

    iget-object v3, p0, LX/9bx;->x:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1515627
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    iget-object v1, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    iget v2, p0, LX/9bx;->z:F

    iget-object v3, p0, LX/9bx;->y:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1515628
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    iget-object v1, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    iget v2, p0, LX/9bx;->z:F

    iget-object v3, p0, LX/9bx;->x:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1515629
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    iget-object v1, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    iget v2, p0, LX/9bx;->z:F

    iget-object v3, p0, LX/9bx;->y:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1515630
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->right:F

    iget-object v1, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    iget v2, p0, LX/9bx;->z:F

    iget-object v3, p0, LX/9bx;->x:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1515631
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->right:F

    iget-object v1, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    iget v2, p0, LX/9bx;->z:F

    iget-object v3, p0, LX/9bx;->y:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1515632
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->right:F

    iget-object v1, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    iget v2, p0, LX/9bx;->z:F

    iget-object v3, p0, LX/9bx;->x:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1515633
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->right:F

    iget-object v1, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    iget v2, p0, LX/9bx;->z:F

    iget-object v3, p0, LX/9bx;->y:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1515634
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1515635
    return-void

    .line 1515636
    :cond_2
    iget-object v1, v0, LX/9eX;->a:LX/9ec;

    iget-object v1, v1, LX/9ec;->E:Landroid/graphics/RectF;

    goto/16 :goto_0

    .line 1515637
    :cond_3
    iget-object v3, v0, LX/9eX;->a:LX/9ec;

    iget-object v3, v3, LX/9ec;->s:Landroid/graphics/Rect;

    invoke-virtual {v1, v3}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    .line 1515638
    iget-object v1, v0, LX/9eX;->a:LX/9ec;

    iget-object v1, v1, LX/9ec;->u:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    .line 1515639
    iget-object v3, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->q:LX/0Px;

    move-object v1, v3

    .line 1515640
    iget-object v3, v0, LX/9eX;->a:LX/9ec;

    iget-object v3, v3, LX/9ec;->v:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFilterName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, LX/8GN;->b(LX/0Px;Ljava/lang/String;)LX/0Px;

    move-result-object v1

    goto/16 :goto_1
.end method

.method public setCropMode(LX/9bv;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/high16 v7, 0x40000000    # 2.0f

    const/4 v6, 0x0

    .line 1515641
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1515642
    iput-object p1, p0, LX/9bx;->G:LX/9bv;

    .line 1515643
    sget-object v0, LX/9bv;->FREE_FORM:LX/9bv;

    if-ne p1, v0, :cond_2

    .line 1515644
    new-instance v0, Landroid/graphics/RectF;

    iget-object v1, p0, LX/9bx;->r:Landroid/graphics/RectF;

    invoke-direct {v0, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    iput-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    .line 1515645
    const/4 v0, 0x0

    .line 1515646
    iput-boolean v0, p0, LX/9bx;->H:Z

    .line 1515647
    :goto_0
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    iput v0, p0, LX/9bx;->m:F

    .line 1515648
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->top:F

    iput v0, p0, LX/9bx;->n:F

    .line 1515649
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    iput v0, p0, LX/9bx;->o:F

    .line 1515650
    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->right:F

    iput v0, p0, LX/9bx;->p:F

    .line 1515651
    iget-object v0, p0, LX/9bx;->D:LX/9eX;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    cmpl-float v0, v0, v6

    if-nez v0, :cond_0

    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->top:F

    cmpl-float v0, v0, v6

    if-nez v0, :cond_0

    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->right:F

    cmpl-float v0, v0, v6

    if-nez v0, :cond_0

    iget-object v0, p0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    cmpl-float v0, v0, v6

    if-eqz v0, :cond_1

    .line 1515652
    :cond_0
    iget-object v0, p0, LX/9bx;->D:LX/9eX;

    new-instance v1, Landroid/graphics/RectF;

    iget-object v2, p0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-direct {v1, v2}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    invoke-virtual {v0, v1, v8}, LX/9eX;->a(Landroid/graphics/RectF;Z)V

    .line 1515653
    :cond_1
    invoke-virtual {p0}, LX/9bx;->invalidate()V

    .line 1515654
    return-void

    .line 1515655
    :cond_2
    iget-object v0, p0, LX/9bx;->r:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget-object v1, p0, LX/9bx;->r:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 1515656
    iget-object v1, p0, LX/9bx;->r:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    iget-object v2, p0, LX/9bx;->r:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_3

    .line 1515657
    new-instance v1, Landroid/graphics/RectF;

    iget-object v2, p0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    div-float v3, v0, v7

    add-float/2addr v2, v3

    iget-object v3, p0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    iget-object v4, p0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    div-float/2addr v0, v7

    sub-float v0, v4, v0

    iget-object v4, p0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v1, v2, v3, v0, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v1, p0, LX/9bx;->t:Landroid/graphics/RectF;

    .line 1515658
    :goto_1
    iput-boolean v8, p0, LX/9bx;->H:Z

    .line 1515659
    goto/16 :goto_0

    .line 1515660
    :cond_3
    new-instance v1, Landroid/graphics/RectF;

    iget-object v2, p0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    div-float v4, v0, v7

    add-float/2addr v3, v4

    iget-object v4, p0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    iget-object v5, p0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    div-float/2addr v0, v7

    sub-float v0, v5, v0

    invoke-direct {v1, v2, v3, v4, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v1, p0, LX/9bx;->t:Landroid/graphics/RectF;

    goto :goto_1
.end method

.method public setOnCropChangeListener(LX/9eX;)V
    .locals 1

    .prologue
    .line 1515661
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9eX;

    iput-object v0, p0, LX/9bx;->D:LX/9eX;

    .line 1515662
    return-void
.end method
