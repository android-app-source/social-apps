.class public LX/8n8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:LX/8ox;

.field public final c:LX/8p3;

.field public final d:Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;

.field public final e:LX/8ol;

.field public final f:LX/8nX;

.field public final g:LX/0ad;

.field public final h:I

.field public final i:I

.field public final j:LX/8n5;

.field public final k:Ljava/lang/Boolean;

.field public final l:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;"
        }
    .end annotation
.end field

.field public final m:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;"
        }
    .end annotation
.end field

.field public n:Z

.field public o:Ljava/lang/String;

.field public p:Lcom/facebook/tagging/model/TaggingProfile;

.field public q:Ljava/lang/Long;

.field public r:LX/7Gm;

.field public s:Z

.field public t:Z

.field public u:LX/8nW;

.field public final v:LX/8nZ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/8ox;LX/8p3;LX/8n3;LX/8ol;LX/01T;LX/8nX;LX/8n5;LX/0Ot;LX/0Uh;LX/0ad;Ljava/lang/Boolean;)V
    .locals 3
    .param p2    # LX/8ox;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p12    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/tagging/autocomplete/MentionsAutoCompleteBehavior$GetTextDelegate;",
            "LX/8p3;",
            "LX/8n3;",
            "LX/8ol;",
            "LX/01T;",
            "LX/8nX;",
            "LX/8n5;",
            "LX/0Ot",
            "<",
            "LX/8nZ;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0ad;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1400534
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1400535
    new-instance v0, LX/8n1;

    invoke-direct {v0}, LX/8n1;-><init>()V

    iput-object v0, p0, LX/8n8;->l:Ljava/util/Comparator;

    .line 1400536
    new-instance v0, LX/8n6;

    invoke-direct {v0, p0}, LX/8n6;-><init>(LX/8n8;)V

    iput-object v0, p0, LX/8n8;->m:Ljava/util/Comparator;

    .line 1400537
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8n8;->n:Z

    .line 1400538
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/8n8;->s:Z

    .line 1400539
    iput-object p1, p0, LX/8n8;->a:Landroid/content/Context;

    .line 1400540
    iput-object p2, p0, LX/8n8;->b:LX/8ox;

    .line 1400541
    iput-object p3, p0, LX/8n8;->c:LX/8p3;

    .line 1400542
    iput-object p7, p0, LX/8n8;->f:LX/8nX;

    .line 1400543
    iput-object p11, p0, LX/8n8;->g:LX/0ad;

    .line 1400544
    invoke-virtual {p4, p8}, LX/8n3;->a(LX/8n5;)Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;

    move-result-object v0

    iput-object v0, p0, LX/8n8;->d:Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;

    .line 1400545
    iput-object p5, p0, LX/8n8;->e:LX/8ol;

    .line 1400546
    iput-object p12, p0, LX/8n8;->k:Ljava/lang/Boolean;

    .line 1400547
    iput-object p8, p0, LX/8n8;->j:LX/8n5;

    .line 1400548
    iget-object v0, p0, LX/8n8;->j:LX/8n5;

    .line 1400549
    iput-object p0, v0, LX/8n5;->n:LX/8n8;

    .line 1400550
    iget-object v0, p0, LX/8n8;->d:Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;

    new-instance v1, LX/8n7;

    invoke-direct {v1, p0}, LX/8n7;-><init>(LX/8n8;)V

    .line 1400551
    iput-object v1, v0, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->j:LX/8my;

    .line 1400552
    sget-object v0, LX/01T;->PAA:LX/01T;

    if-ne p6, v0, :cond_0

    const/4 v0, 0x3

    :goto_0
    iput v0, p0, LX/8n8;->h:I

    .line 1400553
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v2, p0, LX/8n8;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 p4, 0x3

    .line 1400554
    sget-object p1, LX/1zb;->a:LX/1zb;

    move-object p1, p1

    .line 1400555
    new-array p2, p4, [Ljava/lang/Comparable;

    const/4 p3, 0x0

    aput-object v0, p2, p3

    const/4 p3, 0x1

    aput-object v1, p2, p3

    const/4 p3, 0x2

    aput-object v2, p2, p3

    invoke-static {p1, p4, p2}, LX/0dW;->a(Ljava/util/Comparator;I[Ljava/lang/Object;)LX/0dW;

    move-result-object p1

    move-object v0, p1

    .line 1400556
    invoke-virtual {v0}, LX/0dW;->first()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/8n8;->i:I

    .line 1400557
    const/16 v0, 0x3c8

    const/4 v1, 0x0

    invoke-virtual {p10, v0, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1400558
    invoke-interface {p9}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8nZ;

    iput-object v0, p0, LX/8n8;->v:LX/8nZ;

    .line 1400559
    iget-object v0, p0, LX/8n8;->d:Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;

    iget-object v1, p0, LX/8n8;->v:LX/8nZ;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->a(LX/8nB;Z)V

    .line 1400560
    :goto_1
    return-void

    .line 1400561
    :cond_0
    const/4 v0, 0x5

    goto :goto_0

    .line 1400562
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, LX/8n8;->v:LX/8nZ;

    goto :goto_1
.end method

.method public static a(LX/8n8;LX/0Px;LX/8nE;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;",
            "LX/8nE;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1400563
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 1400564
    :cond_0
    :goto_0
    return-void

    .line 1400565
    :cond_1
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    move v2, v0

    move v1, v0

    :goto_1
    if-ge v2, v3, :cond_2

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/model/TaggingProfile;

    .line 1400566
    invoke-virtual {p2}, LX/8nE;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1400567
    iget-object v5, v0, Lcom/facebook/tagging/model/TaggingProfile;->h:Ljava/lang/String;

    move-object v5, v5

    .line 1400568
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, LX/8n8;->d:Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;

    invoke-virtual {v4, v0}, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->a(Lcom/facebook/tagging/model/TaggingProfile;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1400569
    const/4 v0, 0x1

    .line 1400570
    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_1

    .line 1400571
    :cond_2
    if-eqz v1, :cond_0

    .line 1400572
    iget-object v0, p0, LX/8n8;->d:Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;

    new-instance v1, Lcom/facebook/tagging/model/TaggingProfileSectionHeader;

    iget-object v2, p0, LX/8n8;->a:Landroid/content/Context;

    invoke-virtual {p2}, LX/8nE;->getCustomizedNameResourceId()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p2}, LX/8nE;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/facebook/tagging/model/TaggingProfileSectionHeader;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->a(Lcom/facebook/tagging/model/TaggingProfile;)Z

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public static b(LX/8n8;Ljava/util/List;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1400573
    invoke-static {p1}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/8n8;->t:Z

    if-eqz v0, :cond_1

    .line 1400574
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1400575
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/8n8;->d:Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;

    .line 1400576
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1400577
    invoke-virtual {v2, p1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1400578
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v0}, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->getCount()I

    move-result p0

    if-ge v1, p0, :cond_3

    .line 1400579
    invoke-static {v0, v1}, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->c(Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;I)Z

    move-result p0

    if-nez p0, :cond_2

    .line 1400580
    invoke-static {v0, v1}, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->d(Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;I)Lcom/facebook/tagging/model/TaggingProfile;

    move-result-object p0

    invoke-virtual {v2, p0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1400581
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1400582
    :cond_3
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v0, v1

    .line 1400583
    goto :goto_0
.end method

.method public static g(LX/8n8;)V
    .locals 14

    .prologue
    .line 1400584
    iget-object v0, p0, LX/8n8;->e:LX/8ol;

    iget-object v1, p0, LX/8n8;->p:Lcom/facebook/tagging/model/TaggingProfile;

    iget-object v2, p0, LX/8n8;->d:Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;

    iget-object v3, p0, LX/8n8;->p:Lcom/facebook/tagging/model/TaggingProfile;

    .line 1400585
    iget-object v4, v2, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->f:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v4

    move v2, v4

    .line 1400586
    iget-object v3, p0, LX/8n8;->o:Ljava/lang/String;

    iget-object v4, p0, LX/8n8;->q:Ljava/lang/Long;

    iget-object v5, p0, LX/8n8;->r:LX/7Gm;

    .line 1400587
    iget-object v6, v0, LX/8ol;->a:LX/0Zb;

    const-string v7, "selection"

    invoke-static {v0, v7}, LX/8ol;->a(LX/8ol;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "selected_result_type"

    .line 1400588
    iget-object v9, v1, Lcom/facebook/tagging/model/TaggingProfile;->e:LX/7Gr;

    move-object v9, v9

    .line 1400589
    invoke-virtual {v7, v8, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "selected_result_id"

    .line 1400590
    iget-wide v12, v1, Lcom/facebook/tagging/model/TaggingProfile;->b:J

    move-wide v10, v12

    .line 1400591
    invoke-virtual {v7, v8, v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "selected_result_display_text"

    invoke-virtual {v1}, Lcom/facebook/tagging/model/TaggingProfile;->j()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "selected_result_data_source"

    .line 1400592
    iget-object v9, v1, Lcom/facebook/tagging/model/TaggingProfile;->i:Ljava/lang/String;

    move-object v9, v9

    .line 1400593
    invoke-virtual {v7, v8, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "selected_result_typeahead_type"

    .line 1400594
    iget-object v9, v1, Lcom/facebook/tagging/model/TaggingProfile;->h:Ljava/lang/String;

    move-object v9, v9

    .line 1400595
    invoke-virtual {v7, v8, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "selected_position"

    invoke-virtual {v7, v8, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "selected_input_query"

    invoke-virtual {v7, v8, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "group_id"

    invoke-virtual {v7, v8, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "surface"

    invoke-virtual {v7, v8, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    invoke-interface {v6, v7}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1400596
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 1

    .prologue
    .line 1400597
    iget-object v0, p0, LX/8n8;->d:Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;

    .line 1400598
    iget-object p0, v0, Lcom/facebook/tagging/adapter/MentionsTagTypeaheadAdapter;->d:LX/8n5;

    const/4 v0, 0x0

    .line 1400599
    iput-object v0, p0, LX/8n5;->j:Ljava/lang/CharSequence;

    .line 1400600
    iput-object v0, p0, LX/8n5;->k:Ljava/lang/String;

    .line 1400601
    return-void
.end method
