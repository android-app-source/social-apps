.class public final LX/9V7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Z

.field public final synthetic c:Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;


# direct methods
.method public constructor <init>(Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 1498948
    iput-object p1, p0, LX/9V7;->c:Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;

    iput-object p2, p0, LX/9V7;->a:Ljava/lang/String;

    iput-boolean p3, p0, LX/9V7;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5

    .prologue
    .line 1498949
    check-cast p1, Ljava/lang/String;

    .line 1498950
    if-eqz p1, :cond_0

    .line 1498951
    invoke-static {p1}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1498952
    :goto_0
    return-object v0

    .line 1498953
    :cond_0
    new-instance v1, LX/34X;

    iget-object v0, p0, LX/9V7;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    new-instance v3, LX/9V9;

    iget-object v0, p0, LX/9V7;->c:Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;

    iget-object v4, p0, LX/9V7;->a:Ljava/lang/String;

    invoke-direct {v3, v0, v4}, LX/9V9;-><init>(Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;Ljava/lang/String;)V

    sget-object v4, Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;->b:Lcom/facebook/common/callercontext/CallerContext;

    iget-boolean v0, p0, LX/9V7;->b:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    :goto_1
    invoke-direct {v1, v2, v3, v4, v0}, LX/34X;-><init>(Landroid/net/Uri;LX/1uy;Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/http/interfaces/RequestPriority;)V

    .line 1498954
    iget-object v0, p0, LX/9V7;->c:Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;

    iget-object v0, v0, Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9V5;

    invoke-virtual {v0, v1}, LX/3AP;->b(LX/34X;)LX/1j2;

    move-result-object v0

    .line 1498955
    iget-object v1, p0, LX/9V7;->c:Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;

    iget-object v1, v1, Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;->f:Ljava/util/Map;

    iget-object v2, p0, LX/9V7;->a:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1498956
    iget-object v1, v0, LX/1j2;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v0, v1

    .line 1498957
    goto :goto_0

    .line 1498958
    :cond_1
    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    goto :goto_1
.end method
