.class public final LX/9EV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Ve;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6Ve",
        "<",
        "LX/82g;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/9Ea;


# direct methods
.method public constructor <init>(LX/9Ea;)V
    .locals 0

    .prologue
    .line 1457065
    iput-object p1, p0, LX/9EV;->a:LX/9Ea;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1457066
    check-cast p1, LX/82g;

    .line 1457067
    iget-object v0, p0, LX/9EV;->a:LX/9Ea;

    iget-object v0, v0, LX/9Ea;->d:LX/9D1;

    if-eqz v0, :cond_0

    .line 1457068
    iget-object v0, p0, LX/9EV;->a:LX/9Ea;

    iget-object v0, v0, LX/9Ea;->d:LX/9D1;

    invoke-virtual {v0}, LX/9D1;->d()V

    .line 1457069
    :cond_0
    iget-object v1, p0, LX/9EV;->a:LX/9Ea;

    iget-object v1, v1, LX/9Ea;->u:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1457070
    iget-object v0, p1, LX/82f;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    move-object v2, v0

    .line 1457071
    iget-object v0, p1, LX/82g;->a:LX/5Gs;

    move-object v3, v0

    .line 1457072
    invoke-static {v1, v2, v3}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLFeedback;LX/5Gs;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 1457073
    iget-object v1, p0, LX/9EV;->a:LX/9Ea;

    iget-object v1, v1, LX/9Ea;->b:LX/0QK;

    invoke-interface {v1, v0}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1457074
    iget-object v0, p0, LX/9EV;->a:LX/9Ea;

    iget-object v0, v0, LX/9Ea;->j:LX/1nK;

    invoke-virtual {v0}, LX/1nK;->E()V

    .line 1457075
    iget-object v0, p0, LX/9EV;->a:LX/9Ea;

    iget-object v0, v0, LX/9Ea;->d:LX/9D1;

    if-eqz v0, :cond_1

    .line 1457076
    iget-object v0, p0, LX/9EV;->a:LX/9Ea;

    iget-object v0, v0, LX/9Ea;->d:LX/9D1;

    invoke-virtual {v0}, LX/9D1;->e()V

    .line 1457077
    :cond_1
    return-void
.end method
