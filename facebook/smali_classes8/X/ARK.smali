.class public LX/ARK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/74n;

.field public c:LX/8Vg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/74n;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1672877
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1672878
    iput-object p1, p0, LX/ARK;->a:Landroid/content/Context;

    .line 1672879
    iput-object p2, p0, LX/ARK;->b:LX/74n;

    .line 1672880
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/quicksilver/common/sharing/GameShareExtras;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1672881
    sget-object v0, LX/ARJ;->a:[I

    invoke-virtual {p1}, Lcom/facebook/quicksilver/common/sharing/GameShareExtras;->a()LX/8TI;

    move-result-object v1

    invoke-virtual {v1}, LX/8TI;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1672882
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1672883
    :pswitch_0
    sget-object v0, LX/21D;->INSTANT_GAME:LX/21D;

    const-string v1, "shareInstantGame"

    .line 1672884
    iget-object v2, p1, Lcom/facebook/quicksilver/common/sharing/GameShareExtras;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1672885
    const p0, -0x3ff252d0

    invoke-static {v2, p0}, LX/16z;->a(Ljava/lang/String;I)Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v2

    invoke-static {v2}, LX/89G;->a(Lcom/facebook/graphql/model/GraphQLEntity;)LX/89G;

    move-result-object v2

    invoke-virtual {v2}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    sget-object v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    move-object v0, v0

    .line 1672886
    goto :goto_0

    .line 1672887
    :pswitch_1
    check-cast p1, Lcom/facebook/quicksilver/common/sharing/GameScoreShareExtras;

    const/4 v0, 0x0

    .line 1672888
    iget-object v1, p1, Lcom/facebook/quicksilver/common/sharing/GameScoreShareExtras;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1672889
    iget-object v2, p0, LX/ARK;->c:LX/8Vg;

    iget-object v3, p0, LX/ARK;->a:Landroid/content/Context;

    invoke-virtual {v2, v3, v1}, LX/8Vg;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 1672890
    if-nez v2, :cond_1

    .line 1672891
    const/4 v2, 0x0

    .line 1672892
    :goto_1
    move-object v1, v2

    .line 1672893
    if-nez v1, :cond_0

    .line 1672894
    :goto_2
    move-object v0, v0

    .line 1672895
    goto :goto_0

    :cond_0
    sget-object v2, LX/21D;->INSTANT_GAME:LX/21D;

    const-string v3, "shareInstantGameScore"

    invoke-static {v2, v3}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialAttachments(LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    new-instance v2, Lcom/facebook/share/model/ComposerAppAttribution;

    .line 1672896
    iget-object v3, p1, Lcom/facebook/quicksilver/common/sharing/GameShareExtras;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1672897
    iget-object v4, p1, Lcom/facebook/quicksilver/common/sharing/GameShareExtras;->b:Ljava/lang/String;

    move-object v4, v4

    .line 1672898
    const-string v5, "null"

    invoke-direct {v2, v3, v4, v5, v0}, Lcom/facebook/share/model/ComposerAppAttribution;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialAppAttribution(Lcom/facebook/share/model/ComposerAppAttribution;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    sget-object v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    goto :goto_2

    .line 1672899
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "file://"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1672900
    iget-object v3, p0, LX/ARK;->b:LX/74n;

    sget-object v4, LX/74j;->DEFAULT:LX/74j;

    invoke-virtual {v3, v2, v4}, LX/74n;->a(Landroid/net/Uri;LX/74j;)Lcom/facebook/ipc/media/MediaItem;

    move-result-object v2

    .line 1672901
    invoke-static {v2}, LX/7kv;->a(Lcom/facebook/ipc/media/MediaItem;)LX/7kv;

    move-result-object v2

    invoke-virtual {v2}, LX/7kv;->a()Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v2

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
