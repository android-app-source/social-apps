.class public LX/9cL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/8GZ;

.field public final b:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

.field public c:Z

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/5i8;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/StickerParams;",
            ">;"
        }
    .end annotation
.end field

.field private f:Landroid/graphics/RectF;

.field private g:Landroid/view/View;

.field public h:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(LX/8GZ;LX/9cH;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1516480
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1516481
    iput-object p1, p0, LX/9cL;->a:LX/8GZ;

    .line 1516482
    iget-object v0, p0, LX/9cL;->h:Landroid/graphics/Rect;

    invoke-virtual {p2, v0}, LX/9cH;->a(Landroid/graphics/Rect;)Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v0

    iput-object v0, p0, LX/9cL;->b:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    .line 1516483
    return-void
.end method

.method public static a(LX/9cL;III)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1516484
    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    .line 1516485
    :cond_0
    :goto_0
    return-void

    .line 1516486
    :cond_1
    iget-object v0, p0, LX/9cL;->f:Landroid/graphics/RectF;

    if-eqz v0, :cond_2

    .line 1516487
    iget-object v0, p0, LX/9cL;->a:LX/8GZ;

    iget-object v2, p0, LX/9cL;->f:Landroid/graphics/RectF;

    invoke-virtual {v0, v2, p3}, LX/8GZ;->a(Landroid/graphics/RectF;I)V

    .line 1516488
    :cond_2
    iget-object v0, p0, LX/9cL;->d:LX/0Px;

    if-eqz v0, :cond_3

    .line 1516489
    iget-object v0, p0, LX/9cL;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_3

    iget-object v0, p0, LX/9cL;->d:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5i8;

    .line 1516490
    iget-object v4, p0, LX/9cL;->b:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    iget-object v5, p0, LX/9cL;->a:LX/8GZ;

    invoke-virtual {v5, v0}, LX/8GZ;->b(LX/362;)LX/362;

    move-result-object v0

    check-cast v0, LX/5i8;

    iget-object v5, p0, LX/9cL;->g:Landroid/view/View;

    invoke-virtual {v4, v0, v5}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->a(LX/5i8;Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1516491
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1516492
    :cond_3
    iget-object v0, p0, LX/9cL;->e:LX/0Px;

    if-eqz v0, :cond_4

    .line 1516493
    iget-object v0, p0, LX/9cL;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    :goto_2
    if-ge v1, v2, :cond_4

    iget-object v0, p0, LX/9cL;->e:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/StickerParams;

    .line 1516494
    iget-object v3, p0, LX/9cL;->b:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    iget-object v4, p0, LX/9cL;->g:Landroid/view/View;

    invoke-virtual {v3, v0, v4}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->a(LX/5i8;Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1516495
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1516496
    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/9cL;->c:Z

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1516497
    iget-object v0, p0, LX/9cL;->b:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->c()V

    .line 1516498
    return-void
.end method

.method public final a(LX/0Px;LX/0Px;IIILandroid/graphics/RectF;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/5i8;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/StickerParams;",
            ">;III",
            "Landroid/graphics/RectF;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1516499
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1516500
    :cond_1
    :goto_0
    return-void

    .line 1516501
    :cond_2
    iput-object p1, p0, LX/9cL;->d:LX/0Px;

    .line 1516502
    iput-object p2, p0, LX/9cL;->e:LX/0Px;

    .line 1516503
    iget-object v0, p0, LX/9cL;->b:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->j()V

    .line 1516504
    iput-object p6, p0, LX/9cL;->f:Landroid/graphics/RectF;

    .line 1516505
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v1, v1, p3, p4}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, LX/9cL;->h:Landroid/graphics/Rect;

    .line 1516506
    iput-object p7, p0, LX/9cL;->g:Landroid/view/View;

    .line 1516507
    iget-object v0, p0, LX/9cL;->b:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    iget-object v1, p0, LX/9cL;->h:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->c(Landroid/graphics/Rect;)V

    .line 1516508
    invoke-static {p0, p3, p4, p5}, LX/9cL;->a(LX/9cL;III)V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1516509
    iget-object v0, p0, LX/9cL;->b:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->d()V

    .line 1516510
    return-void
.end method
