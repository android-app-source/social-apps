.class public final LX/8r3;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/8r4;


# direct methods
.method public constructor <init>(LX/8r4;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1408391
    iput-object p1, p0, LX/8r3;->c:LX/8r4;

    iput-object p2, p0, LX/8r3;->a:Ljava/lang/String;

    iput-object p3, p0, LX/8r3;->b:Ljava/lang/String;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1408386
    iget-object v0, p0, LX/8r3;->c:LX/8r4;

    iget-boolean v0, v0, LX/8r4;->n:Z

    if-nez v0, :cond_0

    .line 1408387
    iget-object v0, p0, LX/8r3;->c:LX/8r4;

    iget-object v0, v0, LX/8r1;->f:LX/8qr;

    .line 1408388
    iget-object v1, v0, LX/8qr;->b:LX/8qq;

    move-object v0, v1

    .line 1408389
    iget-object v1, p0, LX/8r3;->a:Ljava/lang/String;

    iget-object v2, p0, LX/8r3;->b:Ljava/lang/String;

    invoke-interface {v0, p1, v1, v2}, LX/8qq;->a(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 1408390
    :cond_0
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .prologue
    .line 1408392
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 1408393
    iget-object v0, p0, LX/8r3;->c:LX/8r4;

    iget-boolean v0, v0, LX/8r4;->n:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8r3;->c:LX/8r4;

    iget v0, v0, LX/8r1;->c:I

    :goto_0
    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 1408394
    return-void

    .line 1408395
    :cond_0
    iget-object v0, p0, LX/8r3;->c:LX/8r4;

    iget v0, v0, LX/8r1;->d:I

    goto :goto_0
.end method
