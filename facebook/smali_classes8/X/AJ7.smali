.class public LX/AJ7;
.super LX/16T;
.source ""

# interfaces
.implements LX/16E;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1660630
    invoke-direct {p0}, LX/16T;-><init>()V

    .line 1660631
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 1660632
    const-wide/32 v0, 0x5265c00

    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 1

    .prologue
    .line 1660633
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    return-object v0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1660634
    check-cast p2, Landroid/view/View;

    .line 1660635
    new-instance v0, LX/0hs;

    const/4 v1, 0x2

    invoke-direct {v0, p1, v1}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 1660636
    const/4 v1, -0x1

    .line 1660637
    iput v1, v0, LX/0hs;->t:I

    .line 1660638
    const v1, 0x7f082887

    invoke-virtual {v0, v1}, LX/0hs;->b(I)V

    .line 1660639
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1660640
    const v2, 0x7f0b1b9a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    const v3, 0x7f0b1b9b

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v2, v4, v1, v4}, LX/0ht;->a(IIII)V

    .line 1660641
    invoke-virtual {v0, p2}, LX/0ht;->f(Landroid/view/View;)V

    .line 1660642
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1660643
    const-string v0, "4464"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1660644
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SHARESHEET_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
