.class public final LX/A0q;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 1607753
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 1607754
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1607755
    :goto_0
    return v1

    .line 1607756
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1607757
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_5

    .line 1607758
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1607759
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1607760
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 1607761
    const-string v5, "comments"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1607762
    invoke-static {p0, p1}, LX/2sx;->b(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1607763
    :cond_2
    const-string v5, "lineage_snippets"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1607764
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1607765
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_3

    .line 1607766
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_3

    .line 1607767
    const/4 v5, 0x0

    .line 1607768
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v6, :cond_b

    .line 1607769
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1607770
    :goto_3
    move v4, v5

    .line 1607771
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1607772
    :cond_3
    invoke-static {v2, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 1607773
    goto :goto_1

    .line 1607774
    :cond_4
    const-string v5, "match_words"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1607775
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1607776
    :cond_5
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1607777
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1607778
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1607779
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1607780
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    goto/16 :goto_1

    .line 1607781
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1607782
    :cond_8
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_a

    .line 1607783
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1607784
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1607785
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_8

    if-eqz v6, :cond_8

    .line 1607786
    const-string v7, "detail_sentences"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1607787
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1607788
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->START_ARRAY:LX/15z;

    if-ne v6, v7, :cond_9

    .line 1607789
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_ARRAY:LX/15z;

    if-eq v6, v7, :cond_9

    .line 1607790
    const/4 v7, 0x0

    .line 1607791
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v6, v8, :cond_f

    .line 1607792
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1607793
    :goto_6
    move v6, v7

    .line 1607794
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 1607795
    :cond_9
    invoke-static {v4, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v4

    move v4, v4

    .line 1607796
    goto :goto_4

    .line 1607797
    :cond_a
    const/4 v6, 0x1

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1607798
    invoke-virtual {p1, v5, v4}, LX/186;->b(II)V

    .line 1607799
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto/16 :goto_3

    :cond_b
    move v4, v5

    goto :goto_4

    .line 1607800
    :cond_c
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1607801
    :cond_d
    :goto_7
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_e

    .line 1607802
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1607803
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1607804
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_d

    if-eqz v8, :cond_d

    .line 1607805
    const-string v9, "text"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 1607806
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_7

    .line 1607807
    :cond_e
    const/4 v8, 0x1

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1607808
    invoke-virtual {p1, v7, v6}, LX/186;->b(II)V

    .line 1607809
    invoke-virtual {p1}, LX/186;->d()I

    move-result v7

    goto :goto_6

    :cond_f
    move v6, v7

    goto :goto_7
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    .line 1607810
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1607811
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1607812
    if-eqz v0, :cond_0

    .line 1607813
    const-string v1, "comments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1607814
    invoke-static {p0, v0, p2, p3}, LX/2sx;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1607815
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1607816
    if-eqz v0, :cond_2

    .line 1607817
    const-string v1, "lineage_snippets"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1607818
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1607819
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 1607820
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v3

    invoke-static {p0, v3, p2, p3}, LX/A0p;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1607821
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1607822
    :cond_1
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1607823
    :cond_2
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1607824
    if-eqz v0, :cond_3

    .line 1607825
    const-string v0, "match_words"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1607826
    invoke-virtual {p0, p1, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1607827
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1607828
    return-void
.end method
