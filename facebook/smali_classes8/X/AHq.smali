.class public final LX/AHq;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1656612
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 1656613
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1656614
    :goto_0
    return v1

    .line 1656615
    :cond_0
    const-string v8, "unseen"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1656616
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v3, v0

    move v0, v2

    .line 1656617
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_5

    .line 1656618
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1656619
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1656620
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 1656621
    const-string v8, "id"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1656622
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 1656623
    :cond_2
    const-string v8, "posts"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1656624
    invoke-static {p0, p1}, LX/AHm;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1656625
    :cond_3
    const-string v8, "thread_participants"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1656626
    invoke-static {p0, p1}, LX/AHp;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1656627
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1656628
    :cond_5
    const/4 v7, 0x4

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1656629
    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 1656630
    invoke-virtual {p1, v2, v5}, LX/186;->b(II)V

    .line 1656631
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1656632
    if-eqz v0, :cond_6

    .line 1656633
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->a(IZ)V

    .line 1656634
    :cond_6
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1656635
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1656636
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1656637
    if-eqz v0, :cond_0

    .line 1656638
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1656639
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1656640
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1656641
    if-eqz v0, :cond_1

    .line 1656642
    const-string v1, "posts"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1656643
    invoke-static {p0, v0, p2, p3}, LX/AHm;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1656644
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1656645
    if-eqz v0, :cond_2

    .line 1656646
    const-string v1, "thread_participants"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1656647
    invoke-static {p0, v0, p2, p3}, LX/AHp;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1656648
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1656649
    if-eqz v0, :cond_3

    .line 1656650
    const-string v1, "unseen"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1656651
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1656652
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1656653
    return-void
.end method
