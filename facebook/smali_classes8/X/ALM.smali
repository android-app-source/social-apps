.class public final LX/ALM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ALL;


# instance fields
.field public final synthetic a:Lcom/facebook/backstage/camera/CameraView;


# direct methods
.method public constructor <init>(Lcom/facebook/backstage/camera/CameraView;)V
    .locals 0

    .prologue
    .line 1664765
    iput-object p1, p0, LX/ALM;->a:Lcom/facebook/backstage/camera/CameraView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    const v3, 0x5e0001

    const/4 v4, 0x0

    .line 1664766
    iget-object v0, p0, LX/ALM;->a:Lcom/facebook/backstage/camera/CameraView;

    iget-object v0, v0, Lcom/facebook/backstage/camera/CameraView;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 1664767
    iget-object v0, p0, LX/ALM;->a:Lcom/facebook/backstage/camera/CameraView;

    iget-object v0, v0, Lcom/facebook/backstage/camera/CameraView;->j:Lcom/facebook/optic/CameraPreviewView;

    invoke-virtual {v0}, Lcom/facebook/optic/CameraPreviewView;->getPreviewFrame()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1664768
    iget-object v0, p0, LX/ALM;->a:Lcom/facebook/backstage/camera/CameraView;

    iget-object v0, v0, Lcom/facebook/backstage/camera/CameraView;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/16 v1, 0x3b

    invoke-interface {v0, v3, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    .line 1664769
    iget-object v0, p0, LX/ALM;->a:Lcom/facebook/backstage/camera/CameraView;

    iget-object v0, v0, Lcom/facebook/backstage/camera/CameraView;->x:LX/ALU;

    if-eqz v0, :cond_0

    .line 1664770
    new-instance v0, LX/7gj;

    sget-object v1, LX/7gi;->PHOTO:LX/7gi;

    const/4 v3, 0x0

    iget-object v5, p0, LX/ALM;->a:Lcom/facebook/backstage/camera/CameraView;

    iget-object v5, v5, Lcom/facebook/backstage/camera/CameraView;->w:LX/ALV;

    .line 1664771
    iget v6, v5, LX/ALV;->b:I

    move v5, v6

    .line 1664772
    iget-object v6, p0, LX/ALM;->a:Lcom/facebook/backstage/camera/CameraView;

    iget-object v6, v6, Lcom/facebook/backstage/camera/CameraView;->j:Lcom/facebook/optic/CameraPreviewView;

    invoke-virtual {v6}, Lcom/facebook/optic/CameraPreviewView;->getHeight()I

    move-result v6

    int-to-float v6, v6

    iget-object v7, p0, LX/ALM;->a:Lcom/facebook/backstage/camera/CameraView;

    iget-object v7, v7, Lcom/facebook/backstage/camera/CameraView;->j:Lcom/facebook/optic/CameraPreviewView;

    invoke-virtual {v7}, Lcom/facebook/optic/CameraPreviewView;->getWidth()I

    move-result v7

    int-to-float v7, v7

    div-float v7, v6, v7

    move v6, v4

    invoke-direct/range {v0 .. v7}, LX/7gj;-><init>(LX/7gi;Landroid/graphics/Bitmap;Ljava/lang/String;IIZF)V

    .line 1664773
    iget-object v1, p0, LX/ALM;->a:Lcom/facebook/backstage/camera/CameraView;

    iget-object v2, v1, Lcom/facebook/backstage/camera/CameraView;->e:LX/1Eo;

    iget-object v1, p0, LX/ALM;->a:Lcom/facebook/backstage/camera/CameraView;

    invoke-static {v1}, Lcom/facebook/backstage/camera/CameraView;->f(Lcom/facebook/backstage/camera/CameraView;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, LX/7gQ;->CAMERA_PHOTO_CAPTURE_FRONT:LX/7gQ;

    :goto_0
    invoke-virtual {v2, v1}, LX/1Eo;->a(LX/7gQ;)V

    .line 1664774
    iget-object v1, p0, LX/ALM;->a:Lcom/facebook/backstage/camera/CameraView;

    iget-object v1, v1, Lcom/facebook/backstage/camera/CameraView;->x:LX/ALU;

    invoke-interface {v1, v0}, LX/ALU;->a(LX/7gj;)V

    .line 1664775
    :cond_0
    return-void

    .line 1664776
    :cond_1
    sget-object v1, LX/7gQ;->CAMERA_PHOTO_CAPTURE:LX/7gQ;

    goto :goto_0
.end method

.method public final b()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1664777
    invoke-static {}, Lcom/facebook/optic/CameraPreviewView;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1664778
    iget-object v0, p0, LX/ALM;->a:Lcom/facebook/backstage/camera/CameraView;

    .line 1664779
    invoke-static {v0, v1, v1}, Lcom/facebook/backstage/camera/CameraView;->a$redex0(Lcom/facebook/backstage/camera/CameraView;ZZ)V

    .line 1664780
    iget-object v0, p0, LX/ALM;->a:Lcom/facebook/backstage/camera/CameraView;

    iget-object v1, v0, Lcom/facebook/backstage/camera/CameraView;->j:Lcom/facebook/optic/CameraPreviewView;

    new-instance v2, LX/ALI;

    invoke-direct {v2, p0}, LX/ALI;-><init>(LX/ALM;)V

    iget-object v0, p0, LX/ALM;->a:Lcom/facebook/backstage/camera/CameraView;

    iget-object v0, v0, Lcom/facebook/backstage/camera/CameraView;->b:LX/1Er;

    const-string v3, "BackstageTemp"

    const-string v4, ".mp4"

    sget-object v5, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    invoke-virtual {v0, v3, v4, v5}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-virtual {v1, v2, v0}, Lcom/facebook/optic/CameraPreviewView;->a(LX/5f5;Ljava/io/File;)V

    .line 1664781
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1664782
    invoke-static {}, Lcom/facebook/optic/CameraPreviewView;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1664783
    iget-object v0, p0, LX/ALM;->a:Lcom/facebook/backstage/camera/CameraView;

    iget-object v0, v0, Lcom/facebook/backstage/camera/CameraView;->u:Landroid/os/Handler;

    iget-object v1, p0, LX/ALM;->a:Lcom/facebook/backstage/camera/CameraView;

    iget-object v1, v1, Lcom/facebook/backstage/camera/CameraView;->v:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1664784
    iget-object v0, p0, LX/ALM;->a:Lcom/facebook/backstage/camera/CameraView;

    invoke-static {v0}, Lcom/facebook/backstage/camera/CameraView;->k(Lcom/facebook/backstage/camera/CameraView;)V

    .line 1664785
    iget-object v0, p0, LX/ALM;->a:Lcom/facebook/backstage/camera/CameraView;

    .line 1664786
    invoke-static {v0, v2, v2}, Lcom/facebook/backstage/camera/CameraView;->a$redex0(Lcom/facebook/backstage/camera/CameraView;ZZ)V

    .line 1664787
    iget-object v0, p0, LX/ALM;->a:Lcom/facebook/backstage/camera/CameraView;

    iget-object v0, v0, Lcom/facebook/backstage/camera/CameraView;->j:Lcom/facebook/optic/CameraPreviewView;

    new-instance v1, LX/ALJ;

    invoke-direct {v1, p0}, LX/ALJ;-><init>(LX/ALM;)V

    new-instance v2, LX/ALK;

    invoke-direct {v2, p0}, LX/ALK;-><init>(LX/ALM;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/optic/CameraPreviewView;->a(LX/5f5;LX/5f5;)V

    .line 1664788
    :cond_0
    return-void
.end method
