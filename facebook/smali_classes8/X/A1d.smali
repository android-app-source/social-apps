.class public final LX/A1d;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1611473
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1611474
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1611475
    :goto_0
    return v1

    .line 1611476
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1611477
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_3

    .line 1611478
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1611479
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1611480
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 1611481
    const-string v3, "edges"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1611482
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1611483
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->START_ARRAY:LX/15z;

    if-ne v2, v3, :cond_2

    .line 1611484
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_ARRAY:LX/15z;

    if-eq v2, v3, :cond_2

    .line 1611485
    const/4 v3, 0x0

    .line 1611486
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v4, :cond_8

    .line 1611487
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1611488
    :goto_3
    move v2, v3

    .line 1611489
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1611490
    :cond_2
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    move v0, v0

    .line 1611491
    goto :goto_1

    .line 1611492
    :cond_3
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1611493
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1611494
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    .line 1611495
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1611496
    :cond_6
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_7

    .line 1611497
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1611498
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1611499
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 1611500
    const-string v5, "node"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1611501
    invoke-static {p0, p1}, LX/A1f;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_4

    .line 1611502
    :cond_7
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1611503
    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 1611504
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_3

    :cond_8
    move v2, v3

    goto :goto_4
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1611505
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1611506
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1611507
    if-eqz v0, :cond_2

    .line 1611508
    const-string v1, "edges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1611509
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1611510
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 1611511
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    .line 1611512
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1611513
    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1611514
    if-eqz v3, :cond_0

    .line 1611515
    const-string p1, "node"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1611516
    invoke-static {p0, v3, p2}, LX/A1f;->a(LX/15i;ILX/0nX;)V

    .line 1611517
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1611518
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1611519
    :cond_1
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1611520
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1611521
    return-void
.end method
