.class public final LX/9M0;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNode;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    const/4 v3, 0x1

    .line 1472340
    const-class v1, Lcom/facebook/graphql/model/GraphQLNode;

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v5, "FetchBaseFeedbackForStories"

    const-string v6, "79b39eb6c5e4413b108f5adf115354a5"

    const-string v7, "nodes"

    const-string v8, "10155207367251729"

    const/4 v9, 0x0

    .line 1472341
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1472342
    move-object v0, p0

    move v4, v3

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1472343
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1472344
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1472345
    sparse-switch v0, :sswitch_data_0

    .line 1472346
    :goto_0
    return-object p1

    .line 1472347
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1472348
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1472349
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1472350
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1472351
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1472352
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x41a91745 -> :sswitch_2
        -0x3c54de38 -> :sswitch_1
        -0x3b85b241 -> :sswitch_5
        -0x30b65c8f -> :sswitch_0
        0x65f83c6e -> :sswitch_3
        0x7506f93c -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1472353
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 1472354
    :goto_1
    return v0

    .line 1472355
    :sswitch_0
    const-string v2, "0"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :sswitch_1
    const-string v2, "5"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    .line 1472356
    :pswitch_0
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1472357
    :pswitch_1
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x30 -> :sswitch_0
        0x35 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final l()Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;
    .locals 3

    .prologue
    .line 1472358
    new-instance v0, Lcom/facebook/groups/feed/protocol/FetchFeedbackForStories$FetchBaseFeedbackForStoriesString$1;

    sget-object v1, LX/16Z;->a:LX/16Z;

    const/16 v2, 0x6e

    invoke-direct {v0, p0, v1, v2}, Lcom/facebook/groups/feed/protocol/FetchFeedbackForStories$FetchBaseFeedbackForStoriesString$1;-><init>(LX/9M0;LX/16a;S)V

    return-object v0
.end method
