.class public LX/8t2;
.super Lcom/facebook/user/tiles/UserTileView;
.source ""


# instance fields
.field private c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1412115
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/8t2;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1412116
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1412113
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/8t2;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1412114
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 1412097
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/user/tiles/UserTileView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1412098
    invoke-virtual {p0}, LX/8t2;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b17e0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/8t2;->c:I

    .line 1412099
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileView;->a:Lcom/facebook/user/tiles/UserTileDrawableController;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/user/tiles/UserTileDrawableController;->a(Z)V

    .line 1412100
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V
    .locals 8

    .prologue
    .line 1412101
    iget-boolean v0, p0, Lcom/facebook/user/tiles/UserTileView;->b:Z

    if-eqz v0, :cond_0

    .line 1412102
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileView;->a:Lcom/facebook/user/tiles/UserTileDrawableController;

    invoke-virtual {p0}, LX/8t2;->getWidth()I

    move-result v1

    invoke-virtual {p0}, LX/8t2;->getHeight()I

    move-result v2

    invoke-virtual {p0}, LX/8t2;->getPaddingLeft()I

    move-result v3

    iget v4, p0, LX/8t2;->c:I

    add-int/2addr v3, v4

    invoke-virtual {p0}, LX/8t2;->getPaddingTop()I

    move-result v4

    iget v5, p0, LX/8t2;->c:I

    add-int/2addr v4, v5

    invoke-virtual {p0}, LX/8t2;->getPaddingRight()I

    move-result v5

    iget v6, p0, LX/8t2;->c:I

    add-int/2addr v5, v6

    invoke-virtual {p0}, LX/8t2;->getPaddingBottom()I

    move-result v6

    iget v7, p0, LX/8t2;->c:I

    add-int/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Lcom/facebook/user/tiles/UserTileDrawableController;->a(IIIIII)V

    .line 1412103
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/8t2;->b:Z

    .line 1412104
    :cond_0
    iget-object v0, p0, Lcom/facebook/user/tiles/UserTileView;->a:Lcom/facebook/user/tiles/UserTileDrawableController;

    .line 1412105
    iget-object v1, v0, Lcom/facebook/user/tiles/UserTileDrawableController;->n:Landroid/graphics/drawable/ShapeDrawable;

    move-object v0, v1

    .line 1412106
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 1412107
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    .line 1412108
    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1412109
    invoke-virtual {p0}, LX/8t2;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0700

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1412110
    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerY()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, LX/8t2;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {p1, v2, v0, v3, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1412111
    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1412112
    return-void
.end method
