.class public final LX/9BG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field public final synthetic a:LX/55h;

.field public final synthetic b:LX/1zt;

.field public final synthetic c:LX/9BI;


# direct methods
.method public constructor <init>(LX/9BI;LX/55h;LX/1zt;)V
    .locals 0

    .prologue
    .line 1451456
    iput-object p1, p0, LX/9BG;->c:LX/9BI;

    iput-object p2, p0, LX/9BG;->a:LX/55h;

    iput-object p3, p0, LX/9BG;->b:LX/1zt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 6

    .prologue
    .line 1451457
    if-nez p3, :cond_1

    .line 1451458
    :cond_0
    :goto_0
    return-void

    .line 1451459
    :cond_1
    add-int v0, p2, p3

    iget-object v1, p0, LX/9BG;->c:LX/9BI;

    iget-object v1, v1, LX/9BI;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget v1, v1, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->G:I

    add-int/2addr v0, v1

    if-lt v0, p4, :cond_0

    iget-object v0, p0, LX/9BG;->a:LX/55h;

    .line 1451460
    iget-boolean v1, v0, LX/55h;->g:Z

    move v0, v1

    .line 1451461
    if-eqz v0, :cond_0

    .line 1451462
    iget-object v0, p0, LX/9BG;->c:LX/9BI;

    iget-object v0, v0, LX/9BI;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v0, v0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->f:LX/9B3;

    iget-object v1, p0, LX/9BG;->b:LX/1zt;

    invoke-virtual {v0, v1}, LX/9B3;->d(LX/1zt;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1451463
    iget-object v0, p0, LX/9BG;->c:LX/9BI;

    iget-object v0, v0, LX/9BI;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v0, v0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->d:LX/82m;

    iget-object v1, p0, LX/9BG;->b:LX/1zt;

    .line 1451464
    iget-object v2, v0, LX/82m;->b:LX/0if;

    sget-object v3, LX/82m;->a:LX/0ih;

    const-string v4, "auto_load_more"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string p1, "reaction_"

    invoke-direct {v5, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1451465
    iget p1, v1, LX/1zt;->e:I

    move p1, p1

    .line 1451466
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 1451467
    :cond_2
    add-int v0, p2, p3

    if-lt v0, p4, :cond_3

    .line 1451468
    iget-object v0, p0, LX/9BG;->c:LX/9BI;

    iget-object v0, v0, LX/9BI;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v1, v0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->j:LX/9BT;

    iget-object v0, p0, LX/9BG;->c:LX/9BI;

    iget-object v0, v0, LX/9BI;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v0, v0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->y:Landroid/util/SparseArray;

    iget-object v2, p0, LX/9BG;->b:LX/1zt;

    .line 1451469
    iget v3, v2, LX/1zt;->e:I

    move v2, v3

    .line 1451470
    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const v3, 0x820009

    .line 1451471
    iget-object v2, v1, LX/9BT;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v2, v3, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->c(II)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1451472
    iget-object v2, v1, LX/9BT;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v2, v3, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->g(II)V

    .line 1451473
    :cond_3
    iget-object v0, p0, LX/9BG;->c:LX/9BI;

    iget-object v0, v0, LX/9BI;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v0, v0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->f:LX/9B3;

    iget-object v1, p0, LX/9BG;->b:LX/1zt;

    const/16 v2, 0x19

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, LX/9B3;->a(LX/1zt;IZ)V

    goto/16 :goto_0
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 1451474
    return-void
.end method
