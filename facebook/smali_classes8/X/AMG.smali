.class public LX/AMG;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/graphics/Bitmap;

.field private c:Lcom/facebook/caffe2/Caffe2$NativePeer;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1666011
    const-class v0, LX/AMG;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AMG;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>([B[B)V
    .locals 2

    .prologue
    .line 1666005
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1666006
    new-instance v0, LX/1tw;

    invoke-direct {v0}, LX/1tw;-><init>()V

    .line 1666007
    invoke-virtual {v0}, LX/1tw;->a()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, LX/1tw;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1666008
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unsupported CPU"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1666009
    :cond_0
    new-instance v0, Lcom/facebook/caffe2/Caffe2$NativePeer;

    invoke-direct {v0, p1, p2}, Lcom/facebook/caffe2/Caffe2$NativePeer;-><init>([B[B)V

    iput-object v0, p0, LX/AMG;->c:Lcom/facebook/caffe2/Caffe2$NativePeer;

    .line 1666010
    return-void
.end method


# virtual methods
.method public final a(II)LX/AMF;
    .locals 4

    .prologue
    .line 1666012
    iget-object v0, p0, LX/AMG;->c:Lcom/facebook/caffe2/Caffe2$NativePeer;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/caffe2/Caffe2$NativePeer;->runYuvImage(II)[I

    move-result-object v1

    .line 1666013
    array-length v0, v1

    if-nez v0, :cond_0

    .line 1666014
    const/4 v0, 0x0

    .line 1666015
    :goto_0
    return-object v0

    .line 1666016
    :cond_0
    array-length v0, v1

    const/4 v2, 0x4

    if-eq v0, v2, :cond_1

    .line 1666017
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Incorrect output dimensions"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1666018
    :cond_1
    new-instance v0, LX/AMF;

    const/4 v2, 0x2

    aget v2, v1, v2

    const/4 v3, 0x1

    aget v1, v1, v3

    iget-object v3, p0, LX/AMG;->c:Lcom/facebook/caffe2/Caffe2$NativePeer;

    invoke-virtual {v3}, Lcom/facebook/caffe2/Caffe2$NativePeer;->getOutputBytes()Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-direct {v0, v2, v1, v3}, LX/AMF;-><init>(IILjava/nio/ByteBuffer;)V

    goto :goto_0
.end method

.method public final a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 1665994
    iget-object v0, p0, LX/AMG;->c:Lcom/facebook/caffe2/Caffe2$NativePeer;

    invoke-virtual {v0, p1}, Lcom/facebook/caffe2/Caffe2$NativePeer;->runBitmap(Landroid/graphics/Bitmap;)[I

    move-result-object v0

    .line 1665995
    array-length v1, v0

    const/4 v2, 0x4

    if-eq v1, v2, :cond_0

    .line 1665996
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Incorrect output dimensions"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1665997
    :cond_0
    const/4 v1, 0x1

    aget v1, v0, v1

    .line 1665998
    const/4 v2, 0x2

    aget v0, v0, v2

    .line 1665999
    iget-object v2, p0, LX/AMG;->b:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_1

    iget-object v2, p0, LX/AMG;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-ne v2, v1, :cond_1

    iget-object v2, p0, LX/AMG;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    if-eq v2, v0, :cond_3

    .line 1666000
    :cond_1
    iget-object v2, p0, LX/AMG;->b:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_2

    .line 1666001
    iget-object v2, p0, LX/AMG;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 1666002
    :cond_2
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, LX/AMG;->b:Landroid/graphics/Bitmap;

    .line 1666003
    :cond_3
    iget-object v0, p0, LX/AMG;->c:Lcom/facebook/caffe2/Caffe2$NativePeer;

    iget-object v1, p0, LX/AMG;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/facebook/caffe2/Caffe2$NativePeer;->copyOutputBitmap(Landroid/graphics/Bitmap;)V

    .line 1666004
    iget-object v0, p0, LX/AMG;->b:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public final a(Ljava/nio/ByteBuffer;IILjava/nio/ByteBuffer;Ljava/nio/ByteBuffer;IIII)V
    .locals 10

    .prologue
    .line 1665991
    iget-object v0, p0, LX/AMG;->c:Lcom/facebook/caffe2/Caffe2$NativePeer;

    if-nez v0, :cond_0

    .line 1665992
    :goto_0
    return-void

    .line 1665993
    :cond_0
    iget-object v0, p0, LX/AMG;->c:Lcom/facebook/caffe2/Caffe2$NativePeer;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    invoke-virtual/range {v0 .. v9}, Lcom/facebook/caffe2/Caffe2$NativePeer;->putYuvImage(Ljava/nio/ByteBuffer;IILjava/nio/ByteBuffer;Ljava/nio/ByteBuffer;IIII)V

    goto :goto_0
.end method
