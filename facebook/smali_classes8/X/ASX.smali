.class public LX/ASX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0iK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0io;",
        ":",
        "Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData$ProvidesAudienceEducatorData;",
        ":",
        "LX/0j0;",
        ":",
        "LX/0j1;",
        ":",
        "LX/0j3;",
        ":",
        "LX/0j4;",
        ":",
        "LX/0j6;",
        ":",
        "LX/0ip;",
        ":",
        "LX/0jF;",
        "DerivedData::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesIsMinutiaeSupported;",
        ":",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesIsPrivacyPillSupported;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;>",
        "Ljava/lang/Object;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# static fields
.field public static final a:Lcom/facebook/interstitial/manager/InterstitialTrigger;


# instance fields
.field public final b:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "LX/ASW;",
            "Lcom/facebook/composer/tip/TipControllerInterface;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0iA;

.field private final d:LX/03V;

.field public final e:LX/ASB;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/ASB",
            "<TModelData;TDerivedData;TServices;>;"
        }
    .end annotation
.end field

.field public final f:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/3l6;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Landroid/view/ViewGroup;

.field private h:LX/ASW;

.field public i:Z

.field public j:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "LX/ASW;",
            ">;"
        }
    .end annotation
.end field

.field private k:LX/3l5;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1674149
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->COMPOSER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/ASX;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>(LX/0iA;LX/03V;LX/ASC;Landroid/view/ViewGroup;Landroid/view/ViewGroup;LX/0il;LX/HqU;LX/Hrb;LX/0Px;)V
    .locals 1
    .param p4    # Landroid/view/ViewGroup;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Landroid/view/ViewGroup;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/HqU;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/Hrb;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0iA;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/ASC;",
            "Landroid/view/ViewGroup;",
            "Landroid/view/ViewGroup;",
            "TServices;",
            "Lcom/facebook/composer/tip/StickyGuardrailInterstitialController$DataProvider;",
            "Lcom/facebook/composer/tip/StickyGuardrailInterstitialController$StickyGuardrailCallback;",
            "LX/0Px",
            "<",
            "LX/3l4;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1674138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1674139
    invoke-static {}, LX/0PM;->d()Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, LX/ASX;->b:Ljava/util/LinkedHashMap;

    .line 1674140
    const/4 v0, 0x0

    iput-object v0, p0, LX/ASX;->h:LX/ASW;

    .line 1674141
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/ASX;->i:Z

    .line 1674142
    const-class v0, LX/ASW;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, LX/ASX;->j:Ljava/util/EnumSet;

    .line 1674143
    iput-object p1, p0, LX/ASX;->c:LX/0iA;

    .line 1674144
    iput-object p2, p0, LX/ASX;->d:LX/03V;

    .line 1674145
    invoke-virtual/range {p3 .. p9}, LX/ASC;->a(Landroid/view/ViewGroup;Landroid/view/ViewGroup;Ljava/lang/Object;LX/HqU;LX/Hrb;LX/0Px;)LX/ASB;

    move-result-object v0

    iput-object v0, p0, LX/ASX;->e:LX/ASB;

    .line 1674146
    iget-object v0, p0, LX/ASX;->e:LX/ASB;

    invoke-virtual {v0}, LX/ASB;->a()LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/ASX;->f:LX/0Rf;

    .line 1674147
    iput-object p4, p0, LX/ASX;->g:Landroid/view/ViewGroup;

    .line 1674148
    return-void
.end method

.method private a(LX/3l6;)V
    .locals 4

    .prologue
    .line 1674134
    :try_start_0
    invoke-interface {p1}, LX/3l6;->e()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1674135
    :goto_0
    return-void

    .line 1674136
    :catch_0
    move-exception v0

    .line 1674137
    iget-object v1, p0, LX/ASX;->d:LX/03V;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#show failed with controller "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private a(LX/3l6;Z)V
    .locals 4

    .prologue
    .line 1674130
    :try_start_0
    invoke-interface {p1, p2}, LX/3l6;->a(Z)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1674131
    :goto_0
    return-void

    .line 1674132
    :catch_0
    move-exception v0

    .line 1674133
    iget-object v1, p0, LX/ASX;->d:LX/03V;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#hide failed with controller "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private b(Z)V
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1674123
    iget-object v0, p0, LX/ASX;->k:LX/3l5;

    if-eqz v0, :cond_1

    .line 1674124
    iget-object v0, p0, LX/ASX;->k:LX/3l5;

    invoke-direct {p0, v0, p1}, LX/ASX;->a(LX/3l6;Z)V

    .line 1674125
    if-eqz p1, :cond_0

    .line 1674126
    iget-object v0, p0, LX/ASX;->c:LX/0iA;

    invoke-virtual {v0}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v0

    iget-object v1, p0, LX/ASX;->k:LX/3l5;

    invoke-interface {v1}, LX/0i1;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 1674127
    :cond_0
    iput-object v2, p0, LX/ASX;->k:LX/3l5;

    .line 1674128
    iput-object v2, p0, LX/ASX;->h:LX/ASW;

    .line 1674129
    :cond_1
    return-void
.end method

.method private h()V
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1674103
    iget-object v0, p0, LX/ASX;->g:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/ASX;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/ASX;->i:Z

    if-eqz v0, :cond_1

    .line 1674104
    :cond_0
    :goto_0
    return-void

    .line 1674105
    :cond_1
    iget-object v0, p0, LX/ASX;->c:LX/0iA;

    sget-object v1, LX/ASX;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v2, LX/3l5;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/3l5;

    .line 1674106
    if-eqz v0, :cond_2

    .line 1674107
    iput-object v0, p0, LX/ASX;->k:LX/3l5;

    .line 1674108
    iget-object v0, p0, LX/ASX;->k:LX/3l5;

    invoke-direct {p0, v0}, LX/ASX;->a(LX/3l6;)V

    .line 1674109
    sget-object v0, LX/ASW;->INTERSTITIAL_NUX:LX/ASW;

    iput-object v0, p0, LX/ASX;->h:LX/ASW;

    goto :goto_0

    .line 1674110
    :cond_2
    iget-object v0, p0, LX/ASX;->j:Ljava/util/EnumSet;

    invoke-virtual {v0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ASW;

    .line 1674111
    iget-object v1, p0, LX/ASX;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Huu;

    .line 1674112
    if-eqz v1, :cond_3

    .line 1674113
    invoke-virtual {v1}, LX/Huu;->b()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1674114
    const/4 v2, 0x1

    iput-boolean v2, v1, LX/Huu;->c:Z

    .line 1674115
    iget-object v2, v1, LX/Huu;->a:LX/Huw;

    iget-object v2, v2, LX/Huw;->d:LX/0zw;

    invoke-virtual {v2}, LX/0zw;->a()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbTextView;

    new-instance v3, Lcom/facebook/composer/privacy/controller/TagExpansionPillViewController$2$1;

    invoke-direct {v3, v1}, Lcom/facebook/composer/privacy/controller/TagExpansionPillViewController$2$1;-><init>(LX/Huu;)V

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbTextView;->post(Ljava/lang/Runnable;)Z

    .line 1674116
    iput-object v0, p0, LX/ASX;->h:LX/ASW;

    .line 1674117
    iget-object v1, p0, LX/ASX;->j:Ljava/util/EnumSet;

    invoke-virtual {v1, v0}, Ljava/util/EnumSet;->remove(Ljava/lang/Object;)Z

    .line 1674118
    iget-object v0, p0, LX/ASX;->j:Ljava/util/EnumSet;

    invoke-virtual {v0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1674119
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1674120
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ASW;

    .line 1674121
    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, LX/ASX;->a(LX/ASW;Z)Z

    goto :goto_1

    .line 1674122
    :cond_4
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/5L2;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 1674093
    invoke-virtual {p0}, LX/ASX;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1674094
    :cond_0
    return-void

    .line 1674095
    :cond_1
    const/4 v0, 0x0

    .line 1674096
    iget-object v1, p0, LX/ASX;->f:LX/0Rf;

    invoke-virtual {v1}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :cond_2
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3l6;

    .line 1674097
    invoke-interface {v0, p1}, LX/3l6;->a(LX/5L2;)LX/ASZ;

    move-result-object v4

    .line 1674098
    sget-object v5, LX/ASZ;->SHOW:LX/ASZ;

    if-ne v4, v5, :cond_3

    if-nez v1, :cond_3

    iget-boolean v5, p0, LX/ASX;->i:Z

    if-nez v5, :cond_3

    .line 1674099
    invoke-direct {p0, v0}, LX/ASX;->a(LX/3l6;)V

    move v1, v2

    .line 1674100
    goto :goto_0

    .line 1674101
    :cond_3
    sget-object v5, LX/ASZ;->HIDE:LX/ASZ;

    if-ne v4, v5, :cond_2

    .line 1674102
    invoke-direct {p0, v0, v2}, LX/ASX;->a(LX/3l6;Z)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1674091
    invoke-direct {p0}, LX/ASX;->h()V

    .line 1674092
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 1674034
    iget-object v0, p0, LX/ASX;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ASW;

    .line 1674035
    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, LX/ASX;->a(LX/ASW;Z)Z

    goto :goto_0

    .line 1674036
    :cond_0
    iget-object v0, p0, LX/ASX;->f:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3l6;

    .line 1674037
    invoke-direct {p0, v0, p1}, LX/ASX;->a(LX/3l6;Z)V

    goto :goto_1

    .line 1674038
    :cond_1
    invoke-direct {p0, p1}, LX/ASX;->b(Z)V

    .line 1674039
    return-void
.end method

.method public final varargs a([LX/ASW;)V
    .locals 4

    .prologue
    .line 1674040
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p1, v0

    .line 1674041
    iget-object v3, p0, LX/ASX;->j:Ljava/util/EnumSet;

    invoke-virtual {v3, v2}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 1674042
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1674043
    :cond_0
    return-void
.end method

.method public final a(LX/ASW;Z)Z
    .locals 2

    .prologue
    .line 1674044
    iget-object v0, p0, LX/ASX;->h:LX/ASW;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ASX;->h:LX/ASW;

    if-eq v0, p1, :cond_1

    .line 1674045
    :cond_0
    const/4 v0, 0x0

    .line 1674046
    :goto_0
    return v0

    .line 1674047
    :cond_1
    iget-object v0, p0, LX/ASX;->h:LX/ASW;

    sget-object v1, LX/ASW;->INTERSTITIAL_NUX:LX/ASW;

    if-ne v0, v1, :cond_2

    .line 1674048
    invoke-direct {p0, p2}, LX/ASX;->b(Z)V

    .line 1674049
    const/4 v0, 0x1

    goto :goto_0

    .line 1674050
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, LX/ASX;->h:LX/ASW;

    .line 1674051
    iget-object v0, p0, LX/ASX;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Huu;

    .line 1674052
    iget-object v1, v0, LX/Huu;->b:LX/0hs;

    if-eqz v1, :cond_3

    .line 1674053
    const/4 v1, 0x0

    iput-object v1, v0, LX/Huu;->b:LX/0hs;

    .line 1674054
    const/4 v1, 0x1

    .line 1674055
    :goto_1
    move v0, v1

    .line 1674056
    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final c()V
    .locals 6

    .prologue
    .line 1674057
    iget-object v0, p0, LX/ASX;->e:LX/ASB;

    .line 1674058
    iget-object v1, v0, LX/ASB;->b:LX/0iA;

    sget-object v2, LX/AS4;->FEED_ONLY_POST_NUX:LX/AS4;

    iget-object v2, v2, LX/AS4;->interstitialId:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/0iA;->a(Ljava/lang/String;)LX/0i1;

    move-result-object v1

    check-cast v1, LX/ASE;

    .line 1674059
    iget-object v2, v0, LX/ASB;->a:Landroid/content/Context;

    iget-object v3, v0, LX/ASB;->h:Landroid/view/ViewGroup;

    .line 1674060
    iput-object v2, v1, LX/ASE;->b:Landroid/content/Context;

    .line 1674061
    iget-object v4, v1, LX/ASE;->a:LX/3kp;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-static {v5}, LX/AS4;->forControllerClass(Ljava/lang/Class;)LX/AS4;

    move-result-object v5

    iget-object v5, v5, LX/AS4;->prefKey:LX/0Tn;

    invoke-virtual {v4, v5}, LX/3kp;->a(LX/0Tn;)V

    .line 1674062
    const v4, 0x7f0d0a7a

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, v1, LX/ASE;->c:Landroid/view/View;

    .line 1674063
    iget-object v1, v0, LX/ASB;->b:LX/0iA;

    const-string v2, "4544"

    invoke-virtual {v1, v2}, LX/0iA;->a(Ljava/lang/String;)LX/0i1;

    move-result-object v1

    check-cast v1, LX/AS5;

    .line 1674064
    iget-object v2, v0, LX/ASB;->j:LX/0il;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0io;

    check-cast v2, LX/0j3;

    invoke-interface {v2}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->shouldShowPageVoiceSwitcher()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1674065
    iget-object v2, v0, LX/ASB;->h:Landroid/view/ViewGroup;

    .line 1674066
    iput-object v2, v1, LX/AS5;->b:Landroid/view/View;

    .line 1674067
    :cond_0
    iget-object v1, v0, LX/ASB;->m:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_1

    iget-object v1, v0, LX/ASB;->m:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3l4;

    .line 1674068
    iget-object v4, v0, LX/ASB;->h:Landroid/view/ViewGroup;

    invoke-interface {v1, v4}, LX/3l4;->a(Landroid/view/ViewGroup;)V

    .line 1674069
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1674070
    :cond_1
    invoke-direct {p0}, LX/ASX;->h()V

    .line 1674071
    return-void
.end method

.method public final d()Z
    .locals 9

    .prologue
    .line 1674072
    iget-object v0, p0, LX/ASX;->e:LX/ASB;

    iget-object v1, p0, LX/ASX;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    iget-object v2, p0, LX/ASX;->f:LX/0Rf;

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1674073
    invoke-static {}, LX/AS4;->values()[LX/AS4;

    move-result-object v7

    array-length v8, v7

    move v6, v5

    :goto_0
    if-ge v6, v8, :cond_1

    aget-object v3, v7, v6

    .line 1674074
    iget-object p0, v0, LX/ASB;->b:LX/0iA;

    iget-object v3, v3, LX/AS4;->interstitialId:Ljava/lang/String;

    invoke-virtual {p0, v3}, LX/0iA;->a(Ljava/lang/String;)LX/0i1;

    move-result-object v3

    check-cast v3, LX/3l5;

    .line 1674075
    invoke-interface {v3}, LX/3l6;->f()Z

    move-result v3

    if-eqz v3, :cond_0

    move v3, v4

    .line 1674076
    :goto_1
    move v0, v3

    .line 1674077
    return v0

    .line 1674078
    :cond_0
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    goto :goto_0

    .line 1674079
    :cond_1
    iget-object v3, v0, LX/ASB;->m:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v7

    move v6, v5

    :goto_2
    if-ge v6, v7, :cond_3

    iget-object v3, v0, LX/ASB;->m:LX/0Px;

    invoke-virtual {v3, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/3l4;

    .line 1674080
    invoke-interface {v3}, LX/3l6;->f()Z

    move-result v3

    if-eqz v3, :cond_2

    move v3, v4

    .line 1674081
    goto :goto_1

    .line 1674082
    :cond_2
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    goto :goto_2

    .line 1674083
    :cond_3
    invoke-virtual {v2}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/3l6;

    .line 1674084
    invoke-interface {v3}, LX/3l6;->f()Z

    move-result v3

    if-eqz v3, :cond_4

    move v3, v4

    .line 1674085
    goto :goto_1

    .line 1674086
    :cond_5
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_6
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Huu;

    .line 1674087
    iget-boolean v7, v3, LX/Huu;->c:Z

    move v3, v7

    .line 1674088
    if-eqz v3, :cond_6

    move v3, v4

    .line 1674089
    goto :goto_1

    :cond_7
    move v3, v5

    .line 1674090
    goto :goto_1
.end method
