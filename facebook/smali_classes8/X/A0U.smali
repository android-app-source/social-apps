.class public LX/A0U;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1605665
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1605666
    return-void
.end method

.method public static a(LX/0QB;)LX/A0U;
    .locals 1

    .prologue
    .line 1605667
    new-instance v0, LX/A0U;

    invoke-direct {v0}, LX/A0U;-><init>()V

    .line 1605668
    move-object v0, v0

    .line 1605669
    return-object v0
.end method


# virtual methods
.method public final a(LX/15i;I)LX/0Px;
    .locals 7
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getSnippets"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15i;",
            "I)",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1605670
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const/4 p0, 0x3

    const/4 v3, 0x0

    .line 1605671
    invoke-virtual {p1, p2, p0}, LX/15i;->g(II)I

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p1, p2, p0}, LX/15i;->g(II)I

    move-result v2

    invoke-virtual {p1, v2, v3}, LX/15i;->g(II)I

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    if-eqz v2, :cond_3

    .line 1605672
    invoke-virtual {p1, p2, p0}, LX/15i;->g(II)I

    move-result v2

    invoke-virtual {p1, v2, v3}, LX/15i;->g(II)I

    move-result v2

    .line 1605673
    invoke-virtual {p1, v2, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    :goto_1
    move-object v2, v2

    .line 1605674
    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 1605675
    const/4 v3, 0x1

    const p0, 0x6024f6f4

    const/4 v4, 0x0

    .line 1605676
    invoke-static {p1, p2, v4, p0}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-static {v2}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v2

    :goto_2
    if-eqz v2, :cond_7

    .line 1605677
    invoke-static {p1, p2, v4, p0}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v2

    .line 1605678
    if-eqz v2, :cond_5

    invoke-static {v2}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v2

    :goto_3
    invoke-virtual {v2}, LX/39O;->a()Z

    move-result v2

    if-nez v2, :cond_6

    move v2, v3

    :goto_4
    if-eqz v2, :cond_a

    .line 1605679
    invoke-static {p1, p2, v4, p0}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v2

    if-eqz v2, :cond_8

    invoke-static {v2}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v2

    :goto_5
    invoke-virtual {v2, v4}, LX/2uF;->a(I)LX/1vs;

    move-result-object v2

    iget-object v5, v2, LX/1vs;->a:LX/15i;

    iget v6, v2, LX/1vs;->b:I

    .line 1605680
    invoke-virtual {v5, v6, v4}, LX/15i;->g(II)I

    move-result v2

    if-eqz v2, :cond_9

    :goto_6
    if-eqz v3, :cond_c

    .line 1605681
    invoke-static {p1, p2, v4, p0}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v2

    if-eqz v2, :cond_b

    invoke-static {v2}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v2

    :goto_7
    invoke-virtual {v2, v4}, LX/2uF;->a(I)LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v5, v2, LX/1vs;->b:I

    invoke-virtual {v3, v5, v4}, LX/15i;->g(II)I

    move-result v2

    .line 1605682
    invoke-virtual {v3, v2, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    :goto_8
    move-object v2, v2

    .line 1605683
    if-nez v2, :cond_0

    .line 1605684
    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 1605685
    invoke-virtual {p1, p2, v4}, LX/15i;->g(II)I

    move-result v2

    if-eqz v2, :cond_e

    invoke-virtual {p1, p2, v4}, LX/15i;->g(II)I

    move-result v2

    invoke-virtual {p1, v2, v3}, LX/15i;->g(II)I

    move-result v2

    if-eqz v2, :cond_d

    const/4 v2, 0x1

    :goto_9
    if-eqz v2, :cond_f

    .line 1605686
    invoke-virtual {p1, p2, v4}, LX/15i;->g(II)I

    move-result v2

    invoke-virtual {p1, v2, v3}, LX/15i;->g(II)I

    move-result v2

    .line 1605687
    invoke-virtual {p1, v2, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    :goto_a
    move-object v2, v2

    .line 1605688
    :cond_0
    move-object v2, v2

    .line 1605689
    aput-object v2, v0, v1

    invoke-static {v0}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1605690
    invoke-static {}, LX/0Rj;->notNull()LX/0Rl;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Ph;->c(Ljava/lang/Iterable;LX/0Rl;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/lang/Iterable;)LX/0Px;

    move-result-object v0

    return-object v0

    :cond_1
    move v2, v3

    goto/16 :goto_0

    :cond_2
    move v2, v3

    goto/16 :goto_0

    :cond_3
    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_4
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v2

    goto/16 :goto_2

    :cond_5
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v2

    goto/16 :goto_3

    :cond_6
    move v2, v4

    goto/16 :goto_4

    :cond_7
    move v2, v4

    goto/16 :goto_4

    .line 1605691
    :cond_8
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v2

    goto/16 :goto_5

    :cond_9
    move v3, v4

    .line 1605692
    goto :goto_6

    :cond_a
    move v3, v4

    goto :goto_6

    .line 1605693
    :cond_b
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v2

    goto :goto_7

    .line 1605694
    :cond_c
    const/4 v2, 0x0

    goto :goto_8

    :cond_d
    move v2, v3

    goto :goto_9

    :cond_e
    move v2, v3

    goto :goto_9

    :cond_f
    const/4 v2, 0x0

    goto :goto_a
.end method
