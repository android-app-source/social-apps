.class public final LX/ASw;
.super LX/1ci;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1ci",
        "<",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AT1;


# direct methods
.method public constructor <init>(LX/AT1;)V
    .locals 0

    .prologue
    .line 1674687
    iput-object p1, p0, LX/ASw;->a:LX/AT1;

    invoke-direct {p0}, LX/1ci;-><init>()V

    return-void
.end method


# virtual methods
.method public final e(LX/1ca;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1674665
    invoke-interface {p1}, LX/1ca;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    .line 1674666
    if-eqz v0, :cond_0

    .line 1674667
    :try_start_0
    iget-object v1, p0, LX/ASw;->a:LX/AT1;

    iget-object v1, v1, LX/AT1;->q:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v3

    iget-object v1, p0, LX/ASw;->a:LX/AT1;

    invoke-virtual {v1}, LX/AT1;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    const/4 v7, 0x0

    .line 1674668
    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    move-object v6, v3

    move-object v8, v7

    move-object v9, v7

    move-object v10, v7

    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1674669
    if-nez v6, :cond_1

    .line 1674670
    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    .line 1674671
    :goto_0
    move-object v1, v5

    .line 1674672
    new-instance v3, Landroid/media/ExifInterface;

    invoke-direct {v3, v1}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 1674673
    const-string v1, "Orientation"

    const/4 v4, 0x0

    invoke-virtual {v3, v1, v4}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    move v2, v1

    .line 1674674
    :goto_1
    :try_start_1
    invoke-static {v0}, LX/1FJ;->a(LX/1FJ;)Z

    move-result v1

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1674675
    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ln;

    .line 1674676
    instance-of v1, v1, LX/1ll;

    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1674677
    iget-object v1, p0, LX/ASw;->a:LX/AT1;

    iget-object v1, v1, LX/AT1;->f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->a(LX/1FJ;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1674678
    invoke-virtual {v0}, LX/1FJ;->close()V

    .line 1674679
    :cond_0
    return-void

    .line 1674680
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, LX/1FJ;->close()V

    throw v1

    .line 1674681
    :catch_0
    goto :goto_1

    .line 1674682
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1674683
    const-string v5, "_data"

    invoke-interface {v6, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 1674684
    invoke-interface {v6, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1674685
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public final f(LX/1ca;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1674686
    return-void
.end method
