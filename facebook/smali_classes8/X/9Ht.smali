.class public LX/9Ht;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/9Hr;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9Hu;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1462259
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/9Ht;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/9Hu;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1462256
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1462257
    iput-object p1, p0, LX/9Ht;->b:LX/0Ot;

    .line 1462258
    return-void
.end method

.method public static a(LX/0QB;)LX/9Ht;
    .locals 4

    .prologue
    .line 1462260
    const-class v1, LX/9Ht;

    monitor-enter v1

    .line 1462261
    :try_start_0
    sget-object v0, LX/9Ht;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1462262
    sput-object v2, LX/9Ht;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1462263
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1462264
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1462265
    new-instance v3, LX/9Ht;

    const/16 p0, 0x1ddc

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/9Ht;-><init>(LX/0Ot;)V

    .line 1462266
    move-object v0, v3

    .line 1462267
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1462268
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/9Ht;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1462269
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1462270
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 1462247
    check-cast p2, LX/9Hs;

    .line 1462248
    iget-object v0, p0, LX/9Ht;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Hu;

    iget-object v1, p2, LX/9Hs;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const/4 p2, 0x0

    const/4 p0, 0x1

    .line 1462249
    invoke-static {v1}, LX/17R;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v2

    .line 1462250
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    iget-object v4, v0, LX/9Hu;->a:LX/9IQ;

    invoke-virtual {v4, p1}, LX/9IQ;->c(LX/1De;)LX/9IO;

    move-result-object v4

    invoke-virtual {v4, v1}, LX/9IO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/9IO;

    move-result-object v4

    invoke-virtual {v4, p0}, LX/9IO;->a(Z)LX/9IO;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    .line 1462251
    const v5, 0x568610d6

    const/4 v0, 0x0

    invoke-static {p1, v5, v0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v5, v5

    .line 1462252
    invoke-interface {v4, v5}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v3

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 1462253
    return-object v0

    :cond_0
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    const v4, 0x7f0a0162

    invoke-virtual {v2, v4}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    const v4, 0x7f0b004e

    invoke-virtual {v2, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, p0}, LX/1ne;->j(I)LX/1ne;

    move-result-object v2

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v4}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, p0}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    .line 1462254
    const v4, 0x56861912

    const/4 v5, 0x0

    invoke-static {p1, v4, v5}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 1462255
    invoke-interface {v2, v4}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v2

    const v4, 0x7f020b52

    invoke-interface {v2, v4}, LX/1Di;->x(I)LX/1Di;

    move-result-object v2

    const v4, 0x7f0b18ab

    invoke-interface {v2, p2, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const v4, 0x7f0b18ad

    invoke-interface {v2, p0, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const/4 v4, 0x2

    const v5, 0x7f0b18ac

    invoke-interface {v2, v4, v5}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const/4 v4, 0x3

    const v5, 0x7f0b18ae

    invoke-interface {v2, v4, v5}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1462227
    invoke-static {}, LX/1dS;->b()V

    .line 1462228
    iget v0, p1, LX/1dQ;->b:I

    .line 1462229
    sparse-switch v0, :sswitch_data_0

    .line 1462230
    :goto_0
    return-object v2

    .line 1462231
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 1462232
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1462233
    check-cast v1, LX/9Hs;

    .line 1462234
    iget-object p1, p0, LX/9Ht;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    .line 1462235
    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    .line 1462236
    if-nez v0, :cond_0

    .line 1462237
    :goto_1
    goto :goto_0

    .line 1462238
    :sswitch_1
    check-cast p2, LX/3Ae;

    .line 1462239
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1462240
    check-cast v1, LX/9Hs;

    .line 1462241
    iget-object v3, p0, LX/9Ht;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/9Hu;

    iget-object p1, v1, LX/9Hs;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1462242
    iget-object p2, v3, LX/9Hu;->c:LX/17W;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, p0, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 1462243
    goto :goto_0

    .line 1462244
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 1462245
    sget-object p1, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    goto :goto_1

    .line 1462246
    :cond_1
    sget-object p1, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x568610d6 -> :sswitch_0
        0x56861912 -> :sswitch_1
    .end sparse-switch
.end method
