.class public final LX/AAR;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1637700
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_6

    .line 1637701
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1637702
    :goto_0
    return v1

    .line 1637703
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_4

    .line 1637704
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1637705
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1637706
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v5, :cond_0

    .line 1637707
    const-string v6, "count"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1637708
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 1637709
    :cond_1
    const-string v6, "nodes"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1637710
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1637711
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, v6, :cond_2

    .line 1637712
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, v6, :cond_2

    .line 1637713
    const/4 v6, 0x0

    .line 1637714
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v7, :cond_b

    .line 1637715
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1637716
    :goto_3
    move v5, v6

    .line 1637717
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1637718
    :cond_2
    invoke-static {v3, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v3

    move v3, v3

    .line 1637719
    goto :goto_1

    .line 1637720
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1637721
    :cond_4
    const/4 v5, 0x2

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1637722
    if-eqz v0, :cond_5

    .line 1637723
    invoke-virtual {p1, v1, v4, v1}, LX/186;->a(III)V

    .line 1637724
    :cond_5
    invoke-virtual {p1, v2, v3}, LX/186;->b(II)V

    .line 1637725
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v3, v1

    move v4, v1

    goto :goto_1

    .line 1637726
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1637727
    :cond_8
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_a

    .line 1637728
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1637729
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1637730
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_8

    if-eqz v7, :cond_8

    .line 1637731
    const-string v8, "detailed_targeting_items"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 1637732
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1637733
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->START_ARRAY:LX/15z;

    if-ne v7, v8, :cond_9

    .line 1637734
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_ARRAY:LX/15z;

    if-eq v7, v8, :cond_9

    .line 1637735
    invoke-static {p0, p1}, LX/AA3;->b(LX/15w;LX/186;)I

    move-result v7

    .line 1637736
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 1637737
    :cond_9
    invoke-static {v5, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v5

    move v5, v5

    .line 1637738
    goto :goto_4

    .line 1637739
    :cond_a
    const/4 v7, 0x1

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1637740
    invoke-virtual {p1, v6, v5}, LX/186;->b(II)V

    .line 1637741
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto :goto_3

    :cond_b
    move v5, v6

    goto :goto_4
.end method
