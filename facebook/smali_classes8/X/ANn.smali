.class public final LX/ANn;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source ""


# instance fields
.field public final synthetic a:LX/ANp;


# direct methods
.method public constructor <init>(LX/ANp;)V
    .locals 0

    .prologue
    .line 1668377
    iput-object p1, p0, LX/ANn;->a:LX/ANp;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1668378
    const/4 v0, 0x1

    return v0
.end method

.method public final onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 2

    .prologue
    .line 1668379
    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 1668380
    const/4 v0, 0x0

    .line 1668381
    :goto_0
    return v0

    .line 1668382
    :cond_0
    const/4 v0, 0x0

    cmpl-float v0, p3, v0

    if-lez v0, :cond_1

    .line 1668383
    iget-object v0, p0, LX/ANn;->a:LX/ANp;

    iget-object v0, v0, LX/ANp;->g:LX/AOI;

    .line 1668384
    iget v1, v0, LX/AOH;->e:I

    if-gtz v1, :cond_2

    .line 1668385
    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, LX/AOH;->e(LX/AOH;I)V

    .line 1668386
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 1668387
    :cond_1
    iget-object v0, p0, LX/ANn;->a:LX/ANp;

    iget-object v0, v0, LX/ANp;->g:LX/AOI;

    .line 1668388
    iget v1, v0, LX/AOH;->e:I

    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result p0

    add-int/lit8 p0, p0, -0x1

    if-ne v1, p0, :cond_3

    .line 1668389
    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/AOH;->e(LX/AOH;I)V

    .line 1668390
    :goto_2
    goto :goto_1

    .line 1668391
    :cond_2
    iget v1, v0, LX/AOH;->e:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, LX/AOH;->e(LX/AOH;I)V

    goto :goto_1

    .line 1668392
    :cond_3
    iget v1, v0, LX/AOH;->e:I

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, LX/AOH;->e(LX/AOH;I)V

    goto :goto_2
.end method
