.class public LX/9HM;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedback/ui/rows/CommentPhotoAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedback/ui/rows/CommentShareAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedback/ui/rows/CommentStickerAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedback/ui/rows/CommentAnimatedImageShareAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0ad;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0ad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedback/ui/rows/CommentPhotoAttachmentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedback/ui/rows/CommentVideoAttachmentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedback/ui/rows/CommentShareAttachmentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedback/ui/rows/CommentStickerAttachmentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAttachmentSelectorPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedback/ui/rows/CommentAnimatedImageShareAttachmentPartDefinition;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1461078
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1461079
    iput-object p1, p0, LX/9HM;->a:LX/0Ot;

    .line 1461080
    iput-object p2, p0, LX/9HM;->b:LX/0Ot;

    .line 1461081
    iput-object p3, p0, LX/9HM;->c:LX/0Ot;

    .line 1461082
    iput-object p4, p0, LX/9HM;->d:LX/0Ot;

    .line 1461083
    iput-object p5, p0, LX/9HM;->e:LX/0Ot;

    .line 1461084
    iput-object p6, p0, LX/9HM;->f:LX/0Ot;

    .line 1461085
    iput-object p7, p0, LX/9HM;->g:LX/0ad;

    .line 1461086
    return-void
.end method
