.class public LX/AEA;
.super LX/2y5;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1PW;",
        ">",
        "LX/2y5",
        "<TE;>;"
    }
.end annotation


# static fields
.field private static n:LX/0Xm;


# instance fields
.field public final a:LX/2dj;

.field public final b:LX/2hZ;

.field public final c:LX/0bH;

.field public final d:LX/189;

.field public final e:LX/0Sh;

.field public final f:Landroid/content/res/ColorStateList;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:I

.field private final j:LX/1Nt;

.field private final k:LX/2yS;

.field public final l:LX/1Sl;

.field private final m:LX/2yT;


# direct methods
.method public constructor <init>(LX/2dj;LX/2hZ;LX/0bH;LX/189;LX/0Sh;Landroid/content/res/Resources;LX/2yS;LX/1Sl;LX/2yT;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1646106
    invoke-direct {p0}, LX/2y5;-><init>()V

    .line 1646107
    iput-object p1, p0, LX/AEA;->a:LX/2dj;

    .line 1646108
    iput-object p2, p0, LX/AEA;->b:LX/2hZ;

    .line 1646109
    iput-object p3, p0, LX/AEA;->c:LX/0bH;

    .line 1646110
    iput-object p4, p0, LX/AEA;->d:LX/189;

    .line 1646111
    iput-object p5, p0, LX/AEA;->e:LX/0Sh;

    .line 1646112
    iput-object p7, p0, LX/AEA;->k:LX/2yS;

    .line 1646113
    const v0, 0x7f0a09d0

    invoke-virtual {p6, v0}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, LX/AEA;->f:Landroid/content/res/ColorStateList;

    .line 1646114
    const v0, 0x7f08108c

    invoke-virtual {p6, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/AEA;->g:Ljava/lang/String;

    .line 1646115
    const v0, 0x7f08108d

    invoke-virtual {p6, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/AEA;->h:Ljava/lang/String;

    .line 1646116
    const v0, 0x7f0b00ef

    invoke-virtual {p6, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/AEA;->i:I

    .line 1646117
    new-instance v0, Lcom/facebook/attachments/angora/actionbutton/AddFriendActionButton$AddFriendActionButtonPartDefinition;

    invoke-direct {v0, p0}, Lcom/facebook/attachments/angora/actionbutton/AddFriendActionButton$AddFriendActionButtonPartDefinition;-><init>(LX/AEA;)V

    iput-object v0, p0, LX/AEA;->j:LX/1Nt;

    .line 1646118
    iput-object p8, p0, LX/AEA;->l:LX/1Sl;

    .line 1646119
    iput-object p9, p0, LX/AEA;->m:LX/2yT;

    .line 1646120
    return-void
.end method

.method public static a(LX/0QB;)LX/AEA;
    .locals 13

    .prologue
    .line 1646095
    const-class v1, LX/AEA;

    monitor-enter v1

    .line 1646096
    :try_start_0
    sget-object v0, LX/AEA;->n:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1646097
    sput-object v2, LX/AEA;->n:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1646098
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1646099
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1646100
    new-instance v3, LX/AEA;

    invoke-static {v0}, LX/2dj;->b(LX/0QB;)LX/2dj;

    move-result-object v4

    check-cast v4, LX/2dj;

    invoke-static {v0}, LX/2hZ;->b(LX/0QB;)LX/2hZ;

    move-result-object v5

    check-cast v5, LX/2hZ;

    invoke-static {v0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v6

    check-cast v6, LX/0bH;

    invoke-static {v0}, LX/189;->b(LX/0QB;)LX/189;

    move-result-object v7

    check-cast v7, LX/189;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v8

    check-cast v8, LX/0Sh;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v9

    check-cast v9, Landroid/content/res/Resources;

    invoke-static {v0}, LX/2yS;->a(LX/0QB;)LX/2yS;

    move-result-object v10

    check-cast v10, LX/2yS;

    invoke-static {v0}, LX/1Sl;->b(LX/0QB;)LX/1Sl;

    move-result-object v11

    check-cast v11, LX/1Sl;

    const-class v12, LX/2yT;

    invoke-interface {v0, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/2yT;

    invoke-direct/range {v3 .. v12}, LX/AEA;-><init>(LX/2dj;LX/2hZ;LX/0bH;LX/189;LX/0Sh;Landroid/content/res/Resources;LX/2yS;LX/1Sl;LX/2yT;)V

    .line 1646101
    move-object v0, v3

    .line 1646102
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1646103
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/AEA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1646104
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1646105
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/AEA;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1646063
    invoke-static {p1}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1646064
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1646065
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1646066
    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    .line 1646067
    :cond_0
    :goto_0
    return-void

    .line 1646068
    :cond_1
    const/4 v11, 0x0

    .line 1646069
    invoke-static {v0}, LX/1VO;->w(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v6

    .line 1646070
    if-eqz v6, :cond_4

    .line 1646071
    iget-object v6, p0, LX/AEA;->a:LX/2dj;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    sget-object v7, LX/2h9;->FEED:LX/2h9;

    invoke-virtual {v6, v8, v9, v7}, LX/2dj;->a(JLX/2h9;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    .line 1646072
    :goto_1
    move-object v2, v6

    .line 1646073
    iget-object v3, p0, LX/AEA;->l:LX/1Sl;

    invoke-virtual {v3}, LX/1Sl;->b()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1646074
    iget-object v3, p0, LX/AEA;->c:LX/0bH;

    new-instance v4, LX/1Ne;

    iget-object v5, p0, LX/AEA;->d:LX/189;

    invoke-static {v0}, LX/1VO;->w(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {v5, v1, v0}, LX/189;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Z)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    invoke-direct {v4, v0}, LX/1Ne;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v3, v4}, LX/0b4;->a(LX/0b7;)V

    .line 1646075
    :cond_2
    iget-object v0, p0, LX/AEA;->e:LX/0Sh;

    new-instance v3, LX/AE5;

    invoke-direct {v3, p0, p1, v1}, LX/AE5;-><init>(LX/AEA;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-virtual {v0, v2, v3}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_0

    .line 1646076
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 1646077
    :cond_4
    iget-object v7, p0, LX/AEA;->a:LX/2dj;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    sget-object v10, LX/2h8;->NEWSFEED:LX/2h8;

    move-object v12, v11

    invoke-virtual/range {v7 .. v12}, LX/2dj;->b(JLX/2h8;LX/2hC;LX/5P2;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    goto :goto_1
.end method


# virtual methods
.method public final a()LX/1Nt;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Landroid/view/View;",
            ":",
            "LX/35p;",
            ">()",
            "LX/1Nt",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;*TE;TV;>;"
        }
    .end annotation

    .prologue
    .line 1646094
    iget-object v0, p0, LX/AEA;->j:LX/1Nt;

    return-object v0
.end method

.method public final a(LX/1De;LX/1PW;Lcom/facebook/feed/rows/core/props/FeedProps;Z)LX/AE0;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "TE;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;Z)",
            "LX/AE0;"
        }
    .end annotation

    .prologue
    .line 1646078
    if-nez p3, :cond_0

    .line 1646079
    const/4 v0, 0x0

    .line 1646080
    :goto_0
    return-object v0

    .line 1646081
    :cond_0
    iget-object v0, p3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1646082
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1646083
    invoke-static {v0}, LX/1VO;->w(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v1

    .line 1646084
    iget-object v0, p0, LX/AEA;->m:LX/2yT;

    const/4 v2, 0x3

    iget-object v3, p0, LX/AEA;->k:LX/2yS;

    invoke-virtual {v0, p4, v2, v3}, LX/2yT;->a(ZILX/2yS;)LX/AE0;

    move-result-object v0

    const/4 v2, 0x1

    .line 1646085
    iput-boolean v2, v0, LX/AE0;->l:Z

    .line 1646086
    move-object v0, v0

    .line 1646087
    const v2, 0x7f0b00ef

    invoke-virtual {v0, v2}, LX/AE0;->e(I)LX/AE0;

    move-result-object v0

    const v2, 0x7f020a8d

    invoke-virtual {v0, v2}, LX/AE0;->b(I)LX/AE0;

    move-result-object v2

    if-eqz v1, :cond_1

    iget-object v0, p0, LX/AEA;->h:Ljava/lang/String;

    .line 1646088
    :goto_1
    iput-object v0, v2, LX/AE0;->g:Ljava/lang/CharSequence;

    .line 1646089
    move-object v2, v2

    .line 1646090
    if-eqz v1, :cond_2

    const v0, 0x7f0208a0

    :goto_2
    invoke-virtual {v2, v0}, LX/AE0;->a(I)LX/AE0;

    move-result-object v0

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a09d0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, LX/AE0;->d(I)LX/AE0;

    move-result-object v0

    new-instance v1, LX/AE4;

    invoke-direct {v1, p0, p3}, LX/AE4;-><init>(LX/AEA;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1646091
    iput-object v1, v0, LX/AE0;->o:Landroid/view/View$OnClickListener;

    .line 1646092
    move-object v0, v0

    .line 1646093
    goto :goto_0

    :cond_1
    iget-object v0, p0, LX/AEA;->g:Ljava/lang/String;

    goto :goto_1

    :cond_2
    const v0, 0x7f020886

    goto :goto_2
.end method
