.class public final LX/8s5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:LX/9Hi;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLComment;

.field public final synthetic c:LX/8sB;


# direct methods
.method public constructor <init>(LX/8sB;LX/9Hi;Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 0

    .prologue
    .line 1409739
    iput-object p1, p0, LX/8s5;->c:LX/8sB;

    iput-object p2, p0, LX/8s5;->a:LX/9Hi;

    iput-object p3, p0, LX/8s5;->b:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    .line 1409740
    iget-object v0, p0, LX/8s5;->a:LX/9Hi;

    iget-object v1, p0, LX/8s5;->b:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1409741
    iget-object v2, v0, LX/9Hi;->a:LX/9FA;

    iget-object v3, v0, LX/9Hi;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1409742
    iget-object v4, v2, LX/9FA;->e:LX/9Cd;

    iget-object p0, v2, LX/9FA;->a:Landroid/content/Context;

    .line 1409743
    invoke-static {v1}, LX/8sC;->a(Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 1409744
    iget-object p1, v4, LX/9Cd;->p:LX/3iQ;

    const v0, 0x7f0105b0

    new-instance v2, LX/9CY;

    invoke-direct {v2, v4, v3}, LX/9CY;-><init>(LX/9Cd;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    invoke-static {v4, v0, p0, v2}, LX/9Cd;->a(LX/9Cd;ILandroid/content/Context;LX/9CW;)LX/1L9;

    move-result-object v0

    .line 1409745
    iget-object v2, p1, LX/3iQ;->d:LX/1Ck;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v3, "unhide_comment_"

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v3, p1, LX/3iQ;->r:LX/3ic;

    invoke-virtual {v3, v1}, LX/3ic;->b(Lcom/facebook/graphql/model/GraphQLComment;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    invoke-static {p1, v1, v0}, LX/3iQ;->c(LX/3iQ;Lcom/facebook/graphql/model/GraphQLComment;LX/1L9;)LX/2h1;

    move-result-object p0

    invoke-virtual {v2, v4, v3, p0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1409746
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 1409747
    :cond_0
    iget-object p1, v4, LX/9Cd;->p:LX/3iQ;

    const v0, 0x7f0105af

    new-instance v2, LX/9CX;

    invoke-direct {v2, v4, v3}, LX/9CX;-><init>(LX/9Cd;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    invoke-static {v4, v0, p0, v2}, LX/9Cd;->a(LX/9Cd;ILandroid/content/Context;LX/9CW;)LX/1L9;

    move-result-object v0

    .line 1409748
    iget-object v2, p1, LX/3iQ;->d:LX/1Ck;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v3, "hide_comment_"

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v3, p1, LX/3iQ;->r:LX/3ic;

    invoke-virtual {v3, v1}, LX/3ic;->a(Lcom/facebook/graphql/model/GraphQLComment;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    invoke-static {p1, v1, v0}, LX/3iQ;->c(LX/3iQ;Lcom/facebook/graphql/model/GraphQLComment;LX/1L9;)LX/2h1;

    move-result-object p0

    invoke-virtual {v2, v4, v3, p0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1409749
    goto :goto_0
.end method
