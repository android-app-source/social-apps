.class public LX/9WQ;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/9Vy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/widget/CustomLinearLayout;",
        "LX/9Vy",
        "<",
        "Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryInterfaces$NegativeFeedbackPromptQueryFragment$Responses;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/resources/ui/FbEditText;

.field public b:Lcom/facebook/resources/ui/FbTextView;

.field public c:Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientAutoCompleteTextView;

.field public d:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1501168
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1501169
    invoke-static {p0}, LX/9W0;->a(Lcom/facebook/widget/CustomLinearLayout;)V

    .line 1501170
    const p1, 0x7f030bda

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1501171
    const p1, 0x7f0d1d83

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/resources/ui/FbEditText;

    iput-object p1, p0, LX/9WQ;->a:Lcom/facebook/resources/ui/FbEditText;

    .line 1501172
    const p1, 0x7f0d1d80

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, LX/9WQ;->d:Landroid/widget/LinearLayout;

    .line 1501173
    const p1, 0x7f0d1d81

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/resources/ui/FbTextView;

    iput-object p1, p0, LX/9WQ;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 1501174
    const p1, 0x7f0d1d82

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientAutoCompleteTextView;

    iput-object p1, p0, LX/9WQ;->c:Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientAutoCompleteTextView;

    .line 1501175
    return-void
.end method


# virtual methods
.method public getMessageBody()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1501176
    iget-object v0, p0, LX/9WQ;->a:Lcom/facebook/resources/ui/FbEditText;

    if-eqz v0, :cond_0

    .line 1501177
    iget-object v0, p0, LX/9WQ;->a:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1501178
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public setProgressBarVisibility(Z)V
    .locals 0

    .prologue
    .line 1501179
    return-void
.end method
