.class public final LX/95V;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2kJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/2kJ",
        "<TTEdge;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/95X;


# direct methods
.method public constructor <init>(LX/95X;)V
    .locals 0

    .prologue
    .line 1436914
    iput-object p1, p0, LX/95V;->a:LX/95X;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0Px;LX/3Cb;LX/2kM;LX/2kM;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/3CY;",
            ">;",
            "LX/3Cb;",
            "LX/2kM",
            "<TTEdge;>;",
            "LX/2kM",
            "<TTEdge;>;)V"
        }
    .end annotation

    .prologue
    .line 1436915
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v7

    const/4 v0, 0x0

    move v6, v0

    :goto_0
    if-ge v6, v7, :cond_3

    invoke-virtual {p1, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3CY;

    .line 1436916
    sget-object v1, LX/95U;->b:[I

    iget-object v2, v0, LX/3CY;->a:LX/3Ca;

    invoke-virtual {v2}, LX/3Ca;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1436917
    :goto_1
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 1436918
    :pswitch_0
    invoke-virtual {v0}, LX/3CY;->b()I

    move-result v1

    iget v0, v0, LX/3CY;->b:I

    .line 1436919
    const/4 v2, 0x0

    :goto_2
    if-ge v2, v0, :cond_0

    .line 1436920
    iget-object v3, p0, LX/95V;->a:LX/95X;

    add-int v4, v2, v1

    invoke-interface {p4, v4}, LX/2kM;->a(I)Ljava/lang/Object;

    move-result-object v4

    invoke-static {v3, v4}, LX/95X;->a$redex0(LX/95X;Ljava/lang/Object;)V

    .line 1436921
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1436922
    :cond_0
    goto :goto_1

    .line 1436923
    :pswitch_1
    invoke-virtual {v0}, LX/3CY;->a()I

    move-result v1

    iget v0, v0, LX/3CY;->b:I

    .line 1436924
    const/4 v2, 0x0

    :goto_3
    if-ge v2, v0, :cond_1

    .line 1436925
    iget-object v3, p0, LX/95V;->a:LX/95X;

    add-int v4, v2, v1

    invoke-interface {p3, v4}, LX/2kM;->a(I)Ljava/lang/Object;

    move-result-object v4

    invoke-static {v3, v4}, LX/95X;->b$redex0(LX/95X;Ljava/lang/Object;)V

    .line 1436926
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1436927
    :cond_1
    goto :goto_1

    .line 1436928
    :pswitch_2
    invoke-virtual {v0}, LX/3CY;->a()I

    move-result v1

    invoke-virtual {v0}, LX/3CY;->b()I

    move-result v2

    iget v3, v0, LX/3CY;->b:I

    move-object v0, p0

    move-object v4, p4

    move-object v5, p3

    .line 1436929
    const/4 v8, 0x0

    :goto_4
    if-ge v8, v3, :cond_2

    .line 1436930
    iget-object v9, v0, LX/95V;->a:LX/95X;

    add-int p2, v8, v1

    invoke-interface {v5, p2}, LX/2kM;->a(I)Ljava/lang/Object;

    move-result-object p2

    invoke-static {v9, p2}, LX/95X;->b$redex0(LX/95X;Ljava/lang/Object;)V

    .line 1436931
    iget-object v9, v0, LX/95V;->a:LX/95X;

    add-int p2, v8, v2

    invoke-interface {v4, p2}, LX/2kM;->a(I)Ljava/lang/Object;

    move-result-object p2

    invoke-static {v9, p2}, LX/95X;->a$redex0(LX/95X;Ljava/lang/Object;)V

    .line 1436932
    add-int/lit8 v8, v8, 0x1

    goto :goto_4

    .line 1436933
    :cond_2
    goto :goto_1

    .line 1436934
    :cond_3
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(LX/2kM;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2kM",
            "<TTEdge;>;)V"
        }
    .end annotation

    .prologue
    .line 1436935
    return-void
.end method

.method public final a(LX/2nj;LX/3DP;)V
    .locals 0

    .prologue
    .line 1436936
    return-void
.end method
