.class public LX/8zD;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final e:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Lcom/facebook/ipc/model/FacebookProfile;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/CharSequence;

.field public final b:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/model/FacebookProfile;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1426976
    new-instance v0, LX/8zB;

    invoke-direct {v0}, LX/8zB;-><init>()V

    sput-object v0, LX/8zD;->e:LX/0QK;

    return-void
.end method

.method public constructor <init>(LX/8zC;)V
    .locals 1

    .prologue
    .line 1426977
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1426978
    iget-object v0, p1, LX/8zC;->a:Ljava/lang/CharSequence;

    iput-object v0, p0, LX/8zD;->a:Ljava/lang/CharSequence;

    .line 1426979
    iget-object v0, p1, LX/8zC;->b:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    iput-object v0, p0, LX/8zD;->b:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1426980
    iget-object v0, p1, LX/8zC;->c:LX/0Px;

    iput-object v0, p0, LX/8zD;->c:LX/0Px;

    .line 1426981
    iget-object v0, p1, LX/8zC;->d:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    iput-object v0, p0, LX/8zD;->d:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1426982
    return-void
.end method


# virtual methods
.method public final a(LX/8zD;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1426983
    if-ne p0, p1, :cond_1

    .line 1426984
    :cond_0
    :goto_0
    return v0

    .line 1426985
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1426986
    goto :goto_0

    .line 1426987
    :cond_3
    iget-object v2, p0, LX/8zD;->a:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/8zD;->a:Ljava/lang/CharSequence;

    if-ne v2, v3, :cond_5

    .line 1426988
    const/4 v2, 0x1

    .line 1426989
    :goto_1
    move v2, v2

    .line 1426990
    if-eqz v2, :cond_4

    .line 1426991
    iget-object v2, p0, LX/8zD;->b:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    iget-object v3, p1, LX/8zD;->b:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    if-ne v2, v3, :cond_8

    .line 1426992
    const/4 v2, 0x1

    .line 1426993
    :goto_2
    move v2, v2

    .line 1426994
    if-eqz v2, :cond_4

    .line 1426995
    iget-object v2, p0, LX/8zD;->c:LX/0Px;

    iget-object v3, p1, LX/8zD;->c:LX/0Px;

    if-ne v2, v3, :cond_b

    .line 1426996
    const/4 v2, 0x1

    .line 1426997
    :goto_3
    move v2, v2

    .line 1426998
    if-eqz v2, :cond_4

    .line 1426999
    iget-object v2, p0, LX/8zD;->d:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    iget-object v3, p1, LX/8zD;->d:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    if-ne v2, v3, :cond_e

    .line 1427000
    const/4 v2, 0x1

    .line 1427001
    :goto_4
    move v2, v2

    .line 1427002
    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0

    .line 1427003
    :cond_5
    iget-object v2, p0, LX/8zD;->a:Ljava/lang/CharSequence;

    if-eqz v2, :cond_6

    iget-object v2, p1, LX/8zD;->a:Ljava/lang/CharSequence;

    if-nez v2, :cond_7

    .line 1427004
    :cond_6
    const/4 v2, 0x0

    goto :goto_1

    .line 1427005
    :cond_7
    iget-object v2, p0, LX/8zD;->a:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/8zD;->a:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    goto :goto_1

    .line 1427006
    :cond_8
    iget-object v2, p0, LX/8zD;->b:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    if-eqz v2, :cond_9

    iget-object v2, p1, LX/8zD;->b:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    if-nez v2, :cond_a

    .line 1427007
    :cond_9
    const/4 v2, 0x0

    goto :goto_2

    .line 1427008
    :cond_a
    iget-object v2, p0, LX/8zD;->b:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    iget-object v3, p1, LX/8zD;->b:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    invoke-virtual {v2, v3}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->b(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Z

    move-result v2

    goto :goto_2

    .line 1427009
    :cond_b
    iget-object v2, p0, LX/8zD;->c:LX/0Px;

    if-eqz v2, :cond_c

    iget-object v2, p1, LX/8zD;->c:LX/0Px;

    if-nez v2, :cond_d

    .line 1427010
    :cond_c
    const/4 v2, 0x0

    goto :goto_3

    .line 1427011
    :cond_d
    iget-object v2, p0, LX/8zD;->c:LX/0Px;

    sget-object v3, LX/8zD;->e:LX/0QK;

    invoke-static {v2, v3}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v2

    .line 1427012
    iget-object v3, p1, LX/8zD;->c:LX/0Px;

    sget-object v4, LX/8zD;->e:LX/0QK;

    invoke-static {v3, v4}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v3

    .line 1427013
    invoke-static {v2}, LX/0RA;->b(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v2

    .line 1427014
    invoke-static {v3}, LX/0RA;->b(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v3

    .line 1427015
    invoke-virtual {v2, v3}, Ljava/util/HashSet;->equals(Ljava/lang/Object;)Z

    move-result v2

    goto :goto_3

    .line 1427016
    :cond_e
    iget-object v2, p0, LX/8zD;->d:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    if-eqz v2, :cond_f

    iget-object v2, p1, LX/8zD;->d:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    if-nez v2, :cond_10

    .line 1427017
    :cond_f
    const/4 v2, 0x0

    goto :goto_4

    .line 1427018
    :cond_10
    iget-object v2, p0, LX/8zD;->d:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-virtual {v2}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, LX/8zD;->d:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-virtual {v3}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    goto :goto_4
.end method
