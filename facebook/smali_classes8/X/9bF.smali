.class public LX/9bF;
.super Landroid/widget/BaseAdapter;
.source ""


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLAlbum;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

.field private c:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

.field public d:Ljava/lang/String;

.field public e:Z

.field public f:Lcom/facebook/graphql/model/GraphQLAlbum;

.field public g:Z

.field public h:Z

.field public i:Ljava/lang/Integer;

.field public j:Z


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1514609
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 1514610
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/9bF;->a:Ljava/util/List;

    .line 1514611
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/9bF;->g:Z

    .line 1514612
    return-void
.end method

.method public static a(LX/0QB;)LX/9bF;
    .locals 1

    .prologue
    .line 1514552
    new-instance v0, LX/9bF;

    invoke-direct {v0}, LX/9bF;-><init>()V

    .line 1514553
    move-object v0, v0

    .line 1514554
    return-object v0
.end method

.method public static a(LX/9bF;)V
    .locals 2

    .prologue
    .line 1514598
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    .line 1514599
    iget-object v1, p0, LX/9bF;->b:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    if-eqz v1, :cond_0

    .line 1514600
    iget-object v1, p0, LX/9bF;->b:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1514601
    :cond_0
    iget-object v1, p0, LX/9bF;->a:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1514602
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1514603
    if-eqz v1, :cond_1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1514604
    new-instance v1, LX/4Vq;

    invoke-direct {v1}, LX/4Vq;-><init>()V

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1514605
    iput-object v0, v1, LX/4Vq;->b:LX/0Px;

    .line 1514606
    move-object v0, v1

    .line 1514607
    invoke-virtual {v0}, LX/4Vq;->a()Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    move-result-object v0

    iput-object v0, p0, LX/9bF;->c:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    .line 1514608
    :cond_1
    return-void
.end method


# virtual methods
.method public final b(Lcom/facebook/graphql/model/GraphQLAlbumsConnection;Z)V
    .locals 3

    .prologue
    .line 1514588
    iget-object v0, p0, LX/9bF;->a:Ljava/util/List;

    .line 1514589
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1514590
    if-eqz p1, :cond_0

    .line 1514591
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;->a()LX/0Px;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1514592
    :cond_0
    move-object v1, v1

    .line 1514593
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1514594
    invoke-static {p0}, LX/9bF;->a(LX/9bF;)V

    .line 1514595
    iput-boolean p2, p0, LX/9bF;->j:Z

    .line 1514596
    const v0, -0x620ce418

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1514597
    return-void
.end method

.method public final getCount()I
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1514613
    iget-object v0, p0, LX/9bF;->f:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-eqz v0, :cond_2

    move v0, v1

    .line 1514614
    :goto_0
    iget-boolean v3, p0, LX/9bF;->e:Z

    if-eqz v3, :cond_0

    .line 1514615
    add-int/lit8 v0, v0, 0x1

    .line 1514616
    :cond_0
    iget-object v3, p0, LX/9bF;->c:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    if-eqz v3, :cond_1

    iget-object v3, p0, LX/9bF;->c:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;->a()LX/0Px;

    move-result-object v3

    if-nez v3, :cond_3

    .line 1514617
    :cond_1
    :goto_1
    return v0

    :cond_2
    move v0, v2

    .line 1514618
    goto :goto_0

    .line 1514619
    :cond_3
    iget-object v3, p0, LX/9bF;->c:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    add-int/2addr v0, v3

    .line 1514620
    div-int/lit8 v3, v0, 0x2

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_4

    move v0, v2

    :goto_2
    add-int/2addr v0, v3

    iget-boolean v3, p0, LX/9bF;->j:Z

    if-eqz v3, :cond_5

    :goto_3
    add-int/2addr v0, v1

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2

    :cond_5
    move v1, v2

    goto :goto_3
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1514583
    const/4 v1, -0x2

    if-ne p1, v1, :cond_1

    .line 1514584
    :cond_0
    :goto_0
    return-object v0

    .line 1514585
    :cond_1
    const/4 v1, -0x1

    if-ne p1, v1, :cond_2

    .line 1514586
    iget-object v0, p0, LX/9bF;->f:Lcom/facebook/graphql/model/GraphQLAlbum;

    goto :goto_0

    .line 1514587
    :cond_2
    iget-object v1, p0, LX/9bF;->c:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/9bF;->c:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/9bF;->c:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-le v1, p1, :cond_0

    iget-object v0, p0, LX/9bF;->c:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbum;

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1514582
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    .line 1514555
    iget-boolean v0, p0, LX/9bF;->j:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, LX/9bF;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_1

    .line 1514556
    new-instance p2, Landroid/widget/ProgressBar;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p2, v0}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    .line 1514557
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {p2, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1514558
    :cond_0
    :goto_0
    return-object p2

    .line 1514559
    :cond_1
    sget-object v7, LX/9bI;->NONE:LX/9bI;

    .line 1514560
    const/4 v0, 0x0

    .line 1514561
    iget-boolean v1, p0, LX/9bF;->e:Z

    if-eqz v1, :cond_5

    iget-object v1, p0, LX/9bF;->f:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-eqz v1, :cond_5

    .line 1514562
    const/4 v0, 0x2

    .line 1514563
    :cond_2
    :goto_1
    if-nez p1, :cond_3

    .line 1514564
    packed-switch v0, :pswitch_data_0

    .line 1514565
    sget-object v7, LX/9bI;->NONE:LX/9bI;

    .line 1514566
    :cond_3
    :goto_2
    mul-int/lit8 v1, p1, 0x2

    sub-int/2addr v1, v0

    .line 1514567
    invoke-virtual {p0, v1}, LX/9bF;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 1514568
    mul-int/lit8 v2, p1, 0x2

    sub-int v0, v2, v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, LX/9bF;->getItem(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 1514569
    if-eqz p2, :cond_7

    instance-of v0, p2, Landroid/widget/ProgressBar;

    if-nez v0, :cond_7

    .line 1514570
    instance-of v0, p2, LX/9bJ;

    if-eqz v0, :cond_0

    .line 1514571
    check-cast p2, LX/9bJ;

    move-object v0, p2

    .line 1514572
    :goto_3
    iget-object v2, p0, LX/9bF;->i:Ljava/lang/Integer;

    if-eqz v2, :cond_4

    .line 1514573
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    iget-object v3, p0, LX/9bF;->i:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {v0, v2}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1514574
    :cond_4
    iget-object v2, p0, LX/9bF;->d:Ljava/lang/String;

    iget-boolean v3, p0, LX/9bF;->g:Z

    iget-boolean v6, p0, LX/9bF;->h:Z

    invoke-virtual/range {v0 .. v7}, LX/9bJ;->a(ILjava/lang/String;ZLcom/facebook/graphql/model/GraphQLAlbum;Lcom/facebook/graphql/model/GraphQLAlbum;ZLX/9bI;)V

    move-object p2, v0

    .line 1514575
    goto :goto_0

    .line 1514576
    :cond_5
    iget-boolean v1, p0, LX/9bF;->e:Z

    if-nez v1, :cond_6

    iget-object v1, p0, LX/9bF;->f:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-eqz v1, :cond_2

    .line 1514577
    :cond_6
    const/4 v0, 0x1

    goto :goto_1

    .line 1514578
    :pswitch_0
    iget-object v1, p0, LX/9bF;->f:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-eqz v1, :cond_3

    .line 1514579
    sget-object v7, LX/9bI;->LEFT:LX/9bI;

    goto :goto_2

    .line 1514580
    :pswitch_1
    sget-object v7, LX/9bI;->RIGHT:LX/9bI;

    goto :goto_2

    .line 1514581
    :cond_7
    new-instance v0, LX/9bJ;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, LX/9bJ;-><init>(Landroid/content/Context;)V

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
