.class public LX/AFD;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/facebook/compactdisk/StoreManagerFactory;

.field public final c:Lcom/facebook/compactdisk/DiskCache;

.field public final d:Ljava/util/concurrent/ExecutorService;

.field public final e:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1647387
    const-class v0, LX/AFD;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AFD;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/compactdisk/StoreManagerFactory;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;Ljava/lang/String;)V
    .locals 2
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1647388
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1647389
    iput-object p1, p0, LX/AFD;->b:Lcom/facebook/compactdisk/StoreManagerFactory;

    .line 1647390
    new-instance v0, Lcom/facebook/compactdisk/DiskCacheConfig;

    invoke-direct {v0}, Lcom/facebook/compactdisk/DiskCacheConfig;-><init>()V

    invoke-virtual {v0, p4}, Lcom/facebook/compactdisk/DiskCacheConfig;->name(Ljava/lang/String;)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/compactdisk/DiskCacheConfig;->sessionScoped(Z)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v0

    sget-object v1, Lcom/facebook/compactdisk/DiskArea;->CACHES:Lcom/facebook/compactdisk/DiskArea;

    invoke-virtual {v0, v1}, Lcom/facebook/compactdisk/DiskCacheConfig;->diskArea(Lcom/facebook/compactdisk/DiskArea;)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v0

    new-instance v1, Lcom/facebook/compactdisk/ManagedConfig;

    invoke-direct {v1}, Lcom/facebook/compactdisk/ManagedConfig;-><init>()V

    invoke-virtual {v0, v1}, Lcom/facebook/compactdisk/DiskCacheConfig;->subConfig(Lcom/facebook/compactdisk/SubConfig;)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v0

    .line 1647391
    iput-object p2, p0, LX/AFD;->d:Ljava/util/concurrent/ExecutorService;

    .line 1647392
    iput-object p3, p0, LX/AFD;->e:Ljava/util/concurrent/ExecutorService;

    .line 1647393
    iget-object v1, p0, LX/AFD;->b:Lcom/facebook/compactdisk/StoreManagerFactory;

    invoke-virtual {v1, v0}, Lcom/facebook/compactdisk/StoreManagerFactory;->a(Lcom/facebook/compactdisk/DiskCacheConfig;)Lcom/facebook/compactdisk/StoreManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/facebook/compactdisk/StoreManager;->a(Lcom/facebook/compactdisk/DiskCacheConfig;)Lcom/facebook/compactdisk/DiskCache;

    move-result-object v0

    iput-object v0, p0, LX/AFD;->c:Lcom/facebook/compactdisk/DiskCache;

    .line 1647394
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 1647395
    iget-object v0, p0, LX/AFD;->c:Lcom/facebook/compactdisk/DiskCache;

    .line 1647396
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 1647397
    const/4 p0, 0x0

    invoke-interface {p2, v1, p0}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1647398
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    move-result-object p0

    .line 1647399
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 1647400
    move-object v1, p0

    .line 1647401
    invoke-virtual {v0, p1, v1}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->store(Ljava/lang/String;[B)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1647402
    return-void
.end method

.method public final a(Ljava/util/List;LX/AEs;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/AEs;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1647403
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 1647404
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-static {v1}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    .line 1647405
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1647406
    new-instance v4, LX/AFC;

    invoke-direct {v4, p0, v1, v2, v0}, LX/AFC;-><init>(LX/AFD;Ljava/util/Map;Ljava/lang/String;Ljava/util/concurrent/CountDownLatch;)V

    .line 1647407
    iget-object v5, p0, LX/AFD;->c:Lcom/facebook/compactdisk/DiskCache;

    invoke-virtual {v5, v2}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->fetch(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    .line 1647408
    new-instance v6, Lcom/facebook/audience/direct/data/MultiKeyParcelDiskCache$3;

    invoke-direct {v6, p0, v5, v4, v2}, Lcom/facebook/audience/direct/data/MultiKeyParcelDiskCache$3;-><init>(LX/AFD;Lcom/google/common/util/concurrent/ListenableFuture;LX/AFC;Ljava/lang/String;)V

    iget-object p1, p0, LX/AFD;->d:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v5, v6, p1}, Lcom/google/common/util/concurrent/ListenableFuture;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 1647409
    goto :goto_0

    .line 1647410
    :cond_0
    iget-object v2, p0, LX/AFD;->e:Ljava/util/concurrent/ExecutorService;

    new-instance v3, Lcom/facebook/audience/direct/data/MultiKeyParcelDiskCache$2;

    invoke-direct {v3, p0, v0, p2, v1}, Lcom/facebook/audience/direct/data/MultiKeyParcelDiskCache$2;-><init>(LX/AFD;Ljava/util/concurrent/CountDownLatch;LX/AEs;Ljava/util/Map;)V

    const v4, -0x1c1e96f4

    invoke-static {v2, v3, v4}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1647411
    return-void
.end method
