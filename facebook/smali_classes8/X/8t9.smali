.class public final LX/8t9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/8t8;

.field public final c:Lcom/facebook/user/model/UserKey;

.field public final d:Lcom/facebook/user/model/PicSquare;

.field public final e:LX/8ue;

.field public final f:Ljava/lang/String;

.field public final g:Lcom/facebook/user/model/Name;

.field public final h:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1412504
    const-class v0, LX/8t9;

    sput-object v0, LX/8t9;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/8tA;)V
    .locals 1

    .prologue
    .line 1412387
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1412388
    iget-object v0, p1, LX/8tA;->a:LX/8t8;

    move-object v0, v0

    .line 1412389
    iput-object v0, p0, LX/8t9;->b:LX/8t8;

    .line 1412390
    iget-object v0, p1, LX/8tA;->b:Lcom/facebook/user/model/UserKey;

    move-object v0, v0

    .line 1412391
    iput-object v0, p0, LX/8t9;->c:Lcom/facebook/user/model/UserKey;

    .line 1412392
    iget-object v0, p1, LX/8tA;->c:Lcom/facebook/user/model/PicSquare;

    move-object v0, v0

    .line 1412393
    iput-object v0, p0, LX/8t9;->d:Lcom/facebook/user/model/PicSquare;

    .line 1412394
    iget-object v0, p1, LX/8tA;->d:LX/8ue;

    move-object v0, v0

    .line 1412395
    iput-object v0, p0, LX/8t9;->e:LX/8ue;

    .line 1412396
    iget-object v0, p1, LX/8tA;->e:Ljava/lang/String;

    move-object v0, v0

    .line 1412397
    iput-object v0, p0, LX/8t9;->f:Ljava/lang/String;

    .line 1412398
    iget-object v0, p1, LX/8tA;->f:Lcom/facebook/user/model/Name;

    move-object v0, v0

    .line 1412399
    iput-object v0, p0, LX/8t9;->g:Lcom/facebook/user/model/Name;

    .line 1412400
    iget v0, p1, LX/8tA;->g:I

    move v0, v0

    .line 1412401
    iput v0, p0, LX/8t9;->h:I

    .line 1412402
    return-void
.end method

.method public static a(Lcom/facebook/user/model/PicSquare;LX/8ue;)LX/8t9;
    .locals 2

    .prologue
    .line 1412496
    new-instance v0, LX/8tA;

    invoke-direct {v0}, LX/8tA;-><init>()V

    sget-object v1, LX/8t8;->PIC_SQUARE:LX/8t8;

    .line 1412497
    iput-object v1, v0, LX/8tA;->a:LX/8t8;

    .line 1412498
    move-object v0, v0

    .line 1412499
    iput-object p0, v0, LX/8tA;->c:Lcom/facebook/user/model/PicSquare;

    .line 1412500
    move-object v0, v0

    .line 1412501
    iput-object p1, v0, LX/8tA;->d:LX/8ue;

    .line 1412502
    move-object v0, v0

    .line 1412503
    invoke-virtual {v0}, LX/8tA;->h()LX/8t9;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/user/model/User;)LX/8t9;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1412487
    invoke-virtual {p0}, Lcom/facebook/user/model/User;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1412488
    iget-object v0, p0, Lcom/facebook/user/model/User;->ag:Lcom/facebook/user/model/User;

    move-object v0, v0

    .line 1412489
    if-eqz v0, :cond_0

    .line 1412490
    iget-object v0, p0, Lcom/facebook/user/model/User;->ag:Lcom/facebook/user/model/User;

    move-object v0, v0

    .line 1412491
    iget-object p0, v0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v0, p0

    .line 1412492
    invoke-static {v0, v1}, LX/8t9;->a(Lcom/facebook/user/model/UserKey;LX/8ue;)LX/8t9;

    move-result-object v0

    .line 1412493
    :goto_0
    return-object v0

    .line 1412494
    :cond_0
    invoke-static {p0}, LX/8t9;->b(Lcom/facebook/user/model/User;)LX/8t9;

    move-result-object v0

    goto :goto_0

    .line 1412495
    :cond_1
    invoke-static {p0, v1}, LX/8t9;->a(Lcom/facebook/user/model/User;LX/8ue;)LX/8t9;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/user/model/User;LX/8ue;)LX/8t9;
    .locals 1

    .prologue
    .line 1412481
    invoke-virtual {p0}, Lcom/facebook/user/model/User;->w()Lcom/facebook/user/model/PicSquare;

    move-result-object v0

    .line 1412482
    if-eqz v0, :cond_0

    .line 1412483
    invoke-static {v0, p1}, LX/8t9;->a(Lcom/facebook/user/model/PicSquare;LX/8ue;)LX/8t9;

    move-result-object v0

    .line 1412484
    :goto_0
    return-object v0

    .line 1412485
    :cond_0
    iget-object v0, p0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v0, v0

    .line 1412486
    invoke-static {v0, p1}, LX/8t9;->a(Lcom/facebook/user/model/UserKey;LX/8ue;)LX/8t9;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/user/model/UserKey;)LX/8t9;
    .locals 2

    .prologue
    .line 1412475
    new-instance v0, LX/8tA;

    invoke-direct {v0}, LX/8tA;-><init>()V

    sget-object v1, LX/8t8;->USER_KEY:LX/8t8;

    .line 1412476
    iput-object v1, v0, LX/8tA;->a:LX/8t8;

    .line 1412477
    move-object v0, v0

    .line 1412478
    iput-object p0, v0, LX/8tA;->b:Lcom/facebook/user/model/UserKey;

    .line 1412479
    move-object v0, v0

    .line 1412480
    invoke-virtual {v0}, LX/8tA;->h()LX/8t9;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/user/model/UserKey;LX/8ue;)LX/8t9;
    .locals 2

    .prologue
    .line 1412467
    new-instance v0, LX/8tA;

    invoke-direct {v0}, LX/8tA;-><init>()V

    sget-object v1, LX/8t8;->USER_KEY:LX/8t8;

    .line 1412468
    iput-object v1, v0, LX/8tA;->a:LX/8t8;

    .line 1412469
    move-object v0, v0

    .line 1412470
    iput-object p0, v0, LX/8tA;->b:Lcom/facebook/user/model/UserKey;

    .line 1412471
    move-object v0, v0

    .line 1412472
    iput-object p1, v0, LX/8tA;->d:LX/8ue;

    .line 1412473
    move-object v0, v0

    .line 1412474
    invoke-virtual {v0}, LX/8tA;->h()LX/8t9;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;Lcom/facebook/user/model/Name;LX/8ue;I)LX/8t9;
    .locals 2
    .param p3    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 1412455
    new-instance v0, LX/8tA;

    invoke-direct {v0}, LX/8tA;-><init>()V

    sget-object v1, LX/8t8;->ADDRESS_BOOK_CONTACT:LX/8t8;

    .line 1412456
    iput-object v1, v0, LX/8tA;->a:LX/8t8;

    .line 1412457
    move-object v0, v0

    .line 1412458
    iput-object p0, v0, LX/8tA;->e:Ljava/lang/String;

    .line 1412459
    move-object v0, v0

    .line 1412460
    iput-object p1, v0, LX/8tA;->f:Lcom/facebook/user/model/Name;

    .line 1412461
    move-object v0, v0

    .line 1412462
    iput-object p2, v0, LX/8tA;->d:LX/8ue;

    .line 1412463
    move-object v0, v0

    .line 1412464
    iput p3, v0, LX/8tA;->g:I

    .line 1412465
    move-object v0, v0

    .line 1412466
    invoke-virtual {v0}, LX/8tA;->h()LX/8t9;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/facebook/user/model/User;)LX/8t9;
    .locals 1

    .prologue
    .line 1412454
    sget-object v0, LX/8ue;->NONE:LX/8ue;

    invoke-static {p0, v0}, LX/8t9;->b(Lcom/facebook/user/model/User;LX/8ue;)LX/8t9;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/facebook/user/model/User;LX/8ue;)LX/8t9;
    .locals 3

    .prologue
    .line 1412451
    invoke-virtual {p0}, Lcom/facebook/user/model/User;->aw()Ljava/lang/String;

    move-result-object v0

    .line 1412452
    iget-object v1, p0, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    move-object v1, v1

    .line 1412453
    const/4 v2, 0x0

    invoke-static {v0, v1, p1, v2}, LX/8t9;->a(Ljava/lang/String;Lcom/facebook/user/model/Name;LX/8ue;I)LX/8t9;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1412418
    if-ne p0, p1, :cond_1

    .line 1412419
    :cond_0
    :goto_0
    return v0

    .line 1412420
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1412421
    goto :goto_0

    .line 1412422
    :cond_3
    check-cast p1, LX/8t9;

    .line 1412423
    iget-object v2, p0, LX/8t9;->b:LX/8t8;

    move-object v2, v2

    .line 1412424
    iget-object v3, p1, LX/8t9;->b:LX/8t8;

    move-object v3, v3

    .line 1412425
    invoke-virtual {v2, v3}, LX/8t8;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1412426
    goto :goto_0

    .line 1412427
    :cond_4
    iget-object v2, p0, LX/8t9;->d:Lcom/facebook/user/model/PicSquare;

    move-object v2, v2

    .line 1412428
    iget-object v3, p1, LX/8t9;->d:Lcom/facebook/user/model/PicSquare;

    move-object v3, v3

    .line 1412429
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 1412430
    goto :goto_0

    .line 1412431
    :cond_5
    iget-object v2, p0, LX/8t9;->c:Lcom/facebook/user/model/UserKey;

    move-object v2, v2

    .line 1412432
    iget-object v3, p1, LX/8t9;->c:Lcom/facebook/user/model/UserKey;

    move-object v3, v3

    .line 1412433
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1412434
    goto :goto_0

    .line 1412435
    :cond_6
    iget-object v2, p0, LX/8t9;->e:LX/8ue;

    move-object v2, v2

    .line 1412436
    iget-object v3, p1, LX/8t9;->e:LX/8ue;

    move-object v3, v3

    .line 1412437
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 1412438
    goto :goto_0

    .line 1412439
    :cond_7
    iget-object v2, p0, LX/8t9;->f:Ljava/lang/String;

    move-object v2, v2

    .line 1412440
    iget-object v3, p1, LX/8t9;->f:Ljava/lang/String;

    move-object v3, v3

    .line 1412441
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 1412442
    goto :goto_0

    .line 1412443
    :cond_8
    iget-object v2, p0, LX/8t9;->g:Lcom/facebook/user/model/Name;

    move-object v2, v2

    .line 1412444
    iget-object v3, p1, LX/8t9;->g:Lcom/facebook/user/model/Name;

    move-object v3, v3

    .line 1412445
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 1412446
    goto :goto_0

    .line 1412447
    :cond_9
    iget v2, p0, LX/8t9;->h:I

    move v2, v2

    .line 1412448
    iget v3, p1, LX/8t9;->h:I

    move v3, v3

    .line 1412449
    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1412450
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1412403
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 1412404
    iget-object v2, p0, LX/8t9;->b:LX/8t8;

    move-object v2, v2

    .line 1412405
    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 1412406
    iget-object v2, p0, LX/8t9;->d:Lcom/facebook/user/model/PicSquare;

    move-object v2, v2

    .line 1412407
    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 1412408
    iget-object v2, p0, LX/8t9;->c:Lcom/facebook/user/model/UserKey;

    move-object v2, v2

    .line 1412409
    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 1412410
    iget-object v2, p0, LX/8t9;->e:LX/8ue;

    move-object v2, v2

    .line 1412411
    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 1412412
    iget-object v2, p0, LX/8t9;->f:Ljava/lang/String;

    move-object v2, v2

    .line 1412413
    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 1412414
    iget-object v2, p0, LX/8t9;->g:Lcom/facebook/user/model/Name;

    move-object v2, v2

    .line 1412415
    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 1412416
    iget v2, p0, LX/8t9;->h:I

    move v2, v2

    .line 1412417
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
