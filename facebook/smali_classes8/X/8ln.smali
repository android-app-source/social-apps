.class public final LX/8ln;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        "LX/8lp;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8lo;

.field public final synthetic b:Lcom/facebook/stickers/search/TaggedStickersLoader;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/search/TaggedStickersLoader;LX/8lo;)V
    .locals 0

    .prologue
    .line 1398190
    iput-object p1, p0, LX/8ln;->b:Lcom/facebook/stickers/search/TaggedStickersLoader;

    iput-object p2, p0, LX/8ln;->a:LX/8lo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1398191
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 1398192
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/service/FetchTaggedStickersResult;

    .line 1398193
    iget-object v1, v0, Lcom/facebook/stickers/service/FetchTaggedStickersResult;->a:Ljava/util/Map;

    move-object v0, v1

    .line 1398194
    iget-object v1, p0, LX/8ln;->a:LX/8lo;

    iget-object v1, v1, LX/8lo;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 1398195
    new-instance v1, LX/8lp;

    invoke-direct {v1, v0}, LX/8lp;-><init>(LX/0Px;)V

    return-object v1
.end method
