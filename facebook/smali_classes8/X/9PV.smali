.class public final LX/9PV;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1485028
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 1485029
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1485030
    :goto_0
    return v1

    .line 1485031
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1485032
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_6

    .line 1485033
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1485034
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1485035
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 1485036
    const-string v6, "button_text"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1485037
    invoke-static {p0, p1}, LX/9PR;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1485038
    :cond_2
    const-string v6, "cover_image"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1485039
    invoke-static {p0, p1}, LX/9PS;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1485040
    :cond_3
    const-string v6, "messages"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1485041
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1485042
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, v6, :cond_4

    .line 1485043
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, v6, :cond_4

    .line 1485044
    invoke-static {p0, p1}, LX/9PT;->b(LX/15w;LX/186;)I

    move-result v5

    .line 1485045
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1485046
    :cond_4
    invoke-static {v2, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 1485047
    goto :goto_1

    .line 1485048
    :cond_5
    const-string v6, "title"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1485049
    invoke-static {p0, p1}, LX/9PU;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1485050
    :cond_6
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1485051
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1485052
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1485053
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1485054
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1485055
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_7
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1485056
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1485057
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1485058
    if-eqz v0, :cond_0

    .line 1485059
    const-string v1, "button_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1485060
    invoke-static {p0, v0, p2}, LX/9PR;->a(LX/15i;ILX/0nX;)V

    .line 1485061
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1485062
    if-eqz v0, :cond_1

    .line 1485063
    const-string v1, "cover_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1485064
    invoke-static {p0, v0, p2}, LX/9PS;->a(LX/15i;ILX/0nX;)V

    .line 1485065
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1485066
    if-eqz v0, :cond_3

    .line 1485067
    const-string v1, "messages"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1485068
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1485069
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 1485070
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2}, LX/9PT;->a(LX/15i;ILX/0nX;)V

    .line 1485071
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1485072
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1485073
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1485074
    if-eqz v0, :cond_4

    .line 1485075
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1485076
    invoke-static {p0, v0, p2}, LX/9PU;->a(LX/15i;ILX/0nX;)V

    .line 1485077
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1485078
    return-void
.end method
