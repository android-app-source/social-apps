.class public final LX/ATD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;
.implements LX/8oi;


# instance fields
.field public final synthetic a:LX/ATE;


# direct methods
.method public constructor <init>(LX/ATE;)V
    .locals 0

    .prologue
    .line 1675373
    iput-object p1, p0, LX/ATD;->a:LX/ATE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private b()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1675363
    iget-object v0, p0, LX/ATD;->a:LX/ATE;

    iget-object v0, v0, LX/ATE;->d:LX/ATL;

    iget-object v1, p0, LX/ATD;->a:LX/ATE;

    iget-object v1, v1, LX/ATE;->e:Lcom/facebook/composer/attachments/ComposerAttachment;

    iget-object v2, p0, LX/ATD;->a:LX/ATE;

    iget-object v2, v2, LX/ATE;->a:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-virtual {v2}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/ATD;->a:LX/ATE;

    iget-object v3, v3, LX/ATE;->a:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-virtual {v3}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-static {v3}, LX/8oj;->a(Landroid/text/Editable;)Ljava/util/List;

    move-result-object v3

    invoke-static {v2, v3, v4, v4}, LX/16z;->a(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    .line 1675364
    invoke-static {v1}, LX/7kv;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)LX/7kv;

    move-result-object v3

    .line 1675365
    iput-object v2, v3, LX/7kv;->d:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1675366
    move-object v3, v3

    .line 1675367
    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v4

    .line 1675368
    iput-object v4, v3, LX/7kv;->e:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1675369
    move-object v3, v3

    .line 1675370
    invoke-virtual {v3}, LX/7kv;->a()Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v3

    .line 1675371
    iget-object v4, v0, LX/ATL;->a:LX/ATO;

    iget-object v4, v4, LX/ATO;->a:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v4

    invoke-static {v0, v4, v3}, LX/ATL;->a(LX/ATL;ILcom/facebook/composer/attachments/ComposerAttachment;)V

    .line 1675372
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1675353
    invoke-direct {p0}, LX/ATD;->b()V

    .line 1675354
    return-void
.end method

.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1675357
    invoke-direct {p0}, LX/ATD;->b()V

    .line 1675358
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    const-class v2, Landroid/text/style/MetricAffectingSpan;

    invoke-interface {p1, v1, v0, v2}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/CharacterStyle;

    .line 1675359
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_0

    .line 1675360
    aget-object v2, v0, v1

    invoke-interface {p1, v2}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    .line 1675361
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1675362
    :cond_0
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1675356
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1675355
    return-void
.end method
