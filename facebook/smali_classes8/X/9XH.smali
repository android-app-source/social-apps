.class public final enum LX/9XH;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/9X2;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9XH;",
        ">;",
        "LX/9X2;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9XH;

.field public static final enum EVENT_FB4A_VISIT_ACTIVITY_TAB:LX/9XH;

.field public static final enum EVENT_FB4A_VISIT_INSIGHTS_TAB:LX/9XH;

.field public static final enum EVENT_FB4A_VISIT_PAGE_TAB:LX/9XH;

.field public static final enum EVENT_PMA_VISIT_INSIGHTS_TAB:LX/9XH;

.field public static final enum EVENT_PMA_VISIT_MESSAGE_TAB:LX/9XH;

.field public static final enum EVENT_PMA_VISIT_MORE_TAB:LX/9XH;

.field public static final enum EVENT_PMA_VISIT_NOTIFICATION_TAB:LX/9XH;

.field public static final enum EVENT_PMA_VISIT_PAGE_TAB:LX/9XH;

.field public static final enum EVENT_PRESENCE_TAB_SWITCH:LX/9XH;

.field public static final enum EVENT_VISIT_SETTINGS_TAB:LX/9XH;


# instance fields
.field private mEventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1502617
    new-instance v0, LX/9XH;

    const-string v1, "EVENT_FB4A_VISIT_PAGE_TAB"

    const-string v2, "visit_page_tab"

    invoke-direct {v0, v1, v4, v2}, LX/9XH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XH;->EVENT_FB4A_VISIT_PAGE_TAB:LX/9XH;

    .line 1502618
    new-instance v0, LX/9XH;

    const-string v1, "EVENT_FB4A_VISIT_ACTIVITY_TAB"

    const-string v2, "visit_activity_tab"

    invoke-direct {v0, v1, v5, v2}, LX/9XH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XH;->EVENT_FB4A_VISIT_ACTIVITY_TAB:LX/9XH;

    .line 1502619
    new-instance v0, LX/9XH;

    const-string v1, "EVENT_FB4A_VISIT_INSIGHTS_TAB"

    const-string v2, "visit_insights_tab"

    invoke-direct {v0, v1, v6, v2}, LX/9XH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XH;->EVENT_FB4A_VISIT_INSIGHTS_TAB:LX/9XH;

    .line 1502620
    new-instance v0, LX/9XH;

    const-string v1, "EVENT_PMA_VISIT_PAGE_TAB"

    const-string v2, "visit_page_tab_client"

    invoke-direct {v0, v1, v7, v2}, LX/9XH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XH;->EVENT_PMA_VISIT_PAGE_TAB:LX/9XH;

    .line 1502621
    new-instance v0, LX/9XH;

    const-string v1, "EVENT_PMA_VISIT_MESSAGE_TAB"

    const-string v2, "visit_message_tab_client"

    invoke-direct {v0, v1, v8, v2}, LX/9XH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XH;->EVENT_PMA_VISIT_MESSAGE_TAB:LX/9XH;

    .line 1502622
    new-instance v0, LX/9XH;

    const-string v1, "EVENT_PMA_VISIT_INSIGHTS_TAB"

    const/4 v2, 0x5

    const-string v3, "visit_insights_tab_client"

    invoke-direct {v0, v1, v2, v3}, LX/9XH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XH;->EVENT_PMA_VISIT_INSIGHTS_TAB:LX/9XH;

    .line 1502623
    new-instance v0, LX/9XH;

    const-string v1, "EVENT_PMA_VISIT_NOTIFICATION_TAB"

    const/4 v2, 0x6

    const-string v3, "visit_notification_tab_client"

    invoke-direct {v0, v1, v2, v3}, LX/9XH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XH;->EVENT_PMA_VISIT_NOTIFICATION_TAB:LX/9XH;

    .line 1502624
    new-instance v0, LX/9XH;

    const-string v1, "EVENT_PMA_VISIT_MORE_TAB"

    const/4 v2, 0x7

    const-string v3, "visit_more_tab_client"

    invoke-direct {v0, v1, v2, v3}, LX/9XH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XH;->EVENT_PMA_VISIT_MORE_TAB:LX/9XH;

    .line 1502625
    new-instance v0, LX/9XH;

    const-string v1, "EVENT_VISIT_SETTINGS_TAB"

    const/16 v2, 0x8

    const-string v3, "visit_settings_tab"

    invoke-direct {v0, v1, v2, v3}, LX/9XH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XH;->EVENT_VISIT_SETTINGS_TAB:LX/9XH;

    .line 1502626
    new-instance v0, LX/9XH;

    const-string v1, "EVENT_PRESENCE_TAB_SWITCH"

    const/16 v2, 0x9

    const-string v3, "page_presence_tab_switch"

    invoke-direct {v0, v1, v2, v3}, LX/9XH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XH;->EVENT_PRESENCE_TAB_SWITCH:LX/9XH;

    .line 1502627
    const/16 v0, 0xa

    new-array v0, v0, [LX/9XH;

    sget-object v1, LX/9XH;->EVENT_FB4A_VISIT_PAGE_TAB:LX/9XH;

    aput-object v1, v0, v4

    sget-object v1, LX/9XH;->EVENT_FB4A_VISIT_ACTIVITY_TAB:LX/9XH;

    aput-object v1, v0, v5

    sget-object v1, LX/9XH;->EVENT_FB4A_VISIT_INSIGHTS_TAB:LX/9XH;

    aput-object v1, v0, v6

    sget-object v1, LX/9XH;->EVENT_PMA_VISIT_PAGE_TAB:LX/9XH;

    aput-object v1, v0, v7

    sget-object v1, LX/9XH;->EVENT_PMA_VISIT_MESSAGE_TAB:LX/9XH;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/9XH;->EVENT_PMA_VISIT_INSIGHTS_TAB:LX/9XH;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/9XH;->EVENT_PMA_VISIT_NOTIFICATION_TAB:LX/9XH;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/9XH;->EVENT_PMA_VISIT_MORE_TAB:LX/9XH;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/9XH;->EVENT_VISIT_SETTINGS_TAB:LX/9XH;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/9XH;->EVENT_PRESENCE_TAB_SWITCH:LX/9XH;

    aput-object v2, v0, v1

    sput-object v0, LX/9XH;->$VALUES:[LX/9XH;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1502614
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1502615
    iput-object p3, p0, LX/9XH;->mEventName:Ljava/lang/String;

    .line 1502616
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9XH;
    .locals 1

    .prologue
    .line 1502613
    const-class v0, LX/9XH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9XH;

    return-object v0
.end method

.method public static values()[LX/9XH;
    .locals 1

    .prologue
    .line 1502610
    sget-object v0, LX/9XH;->$VALUES:[LX/9XH;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9XH;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1502612
    iget-object v0, p0, LX/9XH;->mEventName:Ljava/lang/String;

    return-object v0
.end method

.method public final getType()LX/9XC;
    .locals 1

    .prologue
    .line 1502611
    sget-object v0, LX/9XC;->TAB_SWITCH:LX/9XC;

    return-object v0
.end method
