.class public final LX/9KM;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1467822
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_3

    .line 1467823
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1467824
    :goto_0
    return v1

    .line 1467825
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1467826
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_2

    .line 1467827
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1467828
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1467829
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 1467830
    const-string v3, "synced_items"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1467831
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1467832
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v4, :cond_8

    .line 1467833
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1467834
    :goto_2
    move v0, v2

    .line 1467835
    goto :goto_1

    .line 1467836
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1467837
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1467838
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    .line 1467839
    :cond_4
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_6

    .line 1467840
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1467841
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1467842
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_4

    if-eqz v5, :cond_4

    .line 1467843
    const-string v6, "count"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1467844
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v4, v0

    move v0, v3

    goto :goto_3

    .line 1467845
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 1467846
    :cond_6
    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1467847
    if-eqz v0, :cond_7

    .line 1467848
    invoke-virtual {p1, v2, v4, v2}, LX/186;->a(III)V

    .line 1467849
    :cond_7
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_2

    :cond_8
    move v0, v2

    move v4, v2

    goto :goto_3
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1467850
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1467851
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1467852
    if-eqz v0, :cond_1

    .line 1467853
    const-string v1, "synced_items"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1467854
    const/4 v1, 0x0

    .line 1467855
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1467856
    invoke-virtual {p0, v0, v1, v1}, LX/15i;->a(III)I

    move-result v1

    .line 1467857
    if-eqz v1, :cond_0

    .line 1467858
    const-string p1, "count"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1467859
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 1467860
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1467861
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1467862
    return-void
.end method
