.class public final LX/9Cn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Ve;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6Ve",
        "<",
        "LX/8py;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/9Ct;


# direct methods
.method public constructor <init>(LX/9Ct;)V
    .locals 0

    .prologue
    .line 1454395
    iput-object p1, p0, LX/9Cn;->a:LX/9Ct;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1454396
    check-cast p1, LX/8py;

    .line 1454397
    iget-object v0, p0, LX/9Cn;->a:LX/9Ct;

    iget-object v0, v0, LX/9Ct;->h:LX/20j;

    iget-object v1, p0, LX/9Cn;->a:LX/9Ct;

    iget-object v1, v1, LX/9Ct;->m:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 1454398
    iget-object v2, p1, LX/8pw;->a:Lcom/facebook/graphql/model/GraphQLComment;

    move-object v2, v2

    .line 1454399
    invoke-virtual {v0, v1, v2}, LX/20j;->e(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 1454400
    iget-object v1, p0, LX/9Cn;->a:LX/9Ct;

    iget-object v1, v1, LX/9Ct;->m:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-static {v1}, LX/4Vu;->a(Lcom/facebook/graphql/model/GraphQLComment;)LX/4Vu;

    move-result-object v1

    .line 1454401
    iput-object v0, v1, LX/4Vu;->p:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1454402
    move-object v0, v1

    .line 1454403
    iget-object v1, p0, LX/9Cn;->a:LX/9Ct;

    iget-object v1, v1, LX/9Ct;->m:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->A()Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1454404
    iget-object v1, p0, LX/9Cn;->a:LX/9Ct;

    iget-object v2, p0, LX/9Cn;->a:LX/9Ct;

    iget-object v2, v2, LX/9Ct;->m:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLComment;->A()Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection;

    move-result-object v2

    .line 1454405
    iget-object v3, p1, LX/8pw;->a:Lcom/facebook/graphql/model/GraphQLComment;

    move-object v3, v3

    .line 1454406
    sget-object v4, LX/9Cs;->DELETE:LX/9Cs;

    invoke-static {v1, v2, v3, v4}, LX/9Ct;->a$redex0(LX/9Ct;Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection;Lcom/facebook/graphql/model/GraphQLComment;LX/9Cs;)Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection;

    move-result-object v1

    .line 1454407
    iput-object v1, v0, LX/4Vu;->r:Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection;

    .line 1454408
    :cond_0
    invoke-virtual {v0}, LX/4Vu;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    .line 1454409
    iget-object v1, p0, LX/9Cn;->a:LX/9Ct;

    iget-object v1, v1, LX/9Ct;->b:LX/0QK;

    invoke-interface {v1, v0}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1454410
    return-void
.end method
