.class public final LX/8pt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0TF;

.field public final synthetic b:LX/8pv;


# direct methods
.method public constructor <init>(LX/8pv;LX/0TF;)V
    .locals 0

    .prologue
    .line 1406529
    iput-object p1, p0, LX/8pt;->b:LX/8pv;

    iput-object p2, p0, LX/8pt;->a:LX/0TF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1406530
    iget-object v0, p0, LX/8pt;->a:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 1406531
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1406532
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1406533
    if-eqz p1, :cond_0

    .line 1406534
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1406535
    if-eqz v0, :cond_0

    .line 1406536
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1406537
    check-cast v0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;

    invoke-virtual {v0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->s()Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1406538
    :cond_0
    iget-object v0, p0, LX/8pt;->a:LX/0TF;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 1406539
    :goto_0
    return-void

    .line 1406540
    :cond_1
    iget-object v1, p0, LX/8pt;->a:LX/0TF;

    .line 1406541
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1406542
    check-cast v0, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;

    invoke-virtual {v0}, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;->s()Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;

    move-result-object v0

    invoke-static {v0}, LX/8pk;->a(Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$SeenByFeedbackFieldModel$SeenByModel;)Lcom/facebook/graphql/model/GraphQLSeenByConnection;

    move-result-object v0

    invoke-static {v0}, LX/16z;->b(Lcom/facebook/graphql/model/GraphQLSeenByConnection;)LX/0Px;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    goto :goto_0
.end method
