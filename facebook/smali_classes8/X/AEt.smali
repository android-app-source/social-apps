.class public final LX/AEt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AEs;


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:LX/AFA;

.field public final synthetic c:LX/AEv;


# direct methods
.method public constructor <init>(LX/AEv;LX/0Px;LX/AFA;)V
    .locals 0

    .prologue
    .line 1647096
    iput-object p1, p0, LX/AEt;->c:LX/AEv;

    iput-object p2, p0, LX/AEt;->a:LX/0Px;

    iput-object p3, p0, LX/AEt;->b:LX/AFA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Map;)V
    .locals 12
    .param p1    # Ljava/util/Map;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/os/Parcel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1647097
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 1647098
    iget-object v0, p0, LX/AEt;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_2

    iget-object v0, p0, LX/AEt;->a:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7h0;

    .line 1647099
    iget-object v1, v0, LX/7h0;->i:Ljava/lang/String;

    move-object v1, v1

    .line 1647100
    invoke-interface {p1, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1647101
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1647102
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1647103
    :cond_0
    iget-object v1, v0, LX/7h0;->i:Ljava/lang/String;

    move-object v1, v1

    .line 1647104
    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Parcel;

    invoke-static {v1}, LX/AEv;->b(Landroid/os/Parcel;)Lcom/facebook/audience/direct/model/CachedSeenModel;

    move-result-object v1

    .line 1647105
    iget-wide v10, v0, LX/7h0;->c:J

    move-wide v6, v10

    .line 1647106
    iget-wide v10, v1, Lcom/facebook/audience/direct/model/CachedSeenModel;->a:J

    move-wide v8, v10

    .line 1647107
    cmp-long v1, v6, v8

    if-lez v1, :cond_1

    .line 1647108
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1647109
    :cond_1
    new-instance v1, LX/7gx;

    invoke-direct {v1, v0}, LX/7gx;-><init>(LX/7h0;)V

    .line 1647110
    const/4 v0, 0x1

    .line 1647111
    iput-boolean v0, v1, LX/7gx;->h:Z

    .line 1647112
    invoke-virtual {v1}, LX/7gx;->a()LX/7h0;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1647113
    :cond_2
    iget-object v0, p0, LX/AEt;->b:LX/AFA;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1647114
    iget-object v2, v0, LX/AFA;->b:LX/0gI;

    iget-object v2, v2, LX/0gI;->j:LX/AFJ;

    new-instance v3, LX/AF9;

    invoke-direct {v3, v0}, LX/AF9;-><init>(LX/AFA;)V

    .line 1647115
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 1647116
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 1647117
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v8

    const/4 v4, 0x0

    move v5, v4

    :goto_2
    if-ge v5, v8, :cond_4

    invoke-virtual {v1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/7h0;

    .line 1647118
    iget-boolean v0, v4, LX/7h0;->h:Z

    move v0, v0

    .line 1647119
    if-eqz v0, :cond_3

    .line 1647120
    iget-object v0, v4, LX/7h0;->d:Lcom/facebook/audience/model/AudienceControlData;

    move-object v0, v0

    .line 1647121
    invoke-virtual {v0}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1647122
    iget-object v0, v4, LX/7h0;->i:Ljava/lang/String;

    move-object v4, v0

    .line 1647123
    invoke-interface {v7, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1647124
    :cond_3
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_2

    .line 1647125
    :cond_4
    iget-object v4, v2, LX/AFJ;->a:LX/AFD;

    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    invoke-virtual {v5, v6}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v5

    invoke-virtual {v5, v7}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v5

    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    new-instance v7, LX/AFH;

    invoke-direct {v7, v2, v6, v1, v3}, LX/AFH;-><init>(LX/AFJ;Ljava/util/Set;LX/0Px;LX/AF9;)V

    invoke-virtual {v4, v5, v7}, LX/AFD;->a(Ljava/util/List;LX/AEs;)V

    .line 1647126
    return-void
.end method
