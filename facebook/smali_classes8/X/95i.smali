.class public final LX/95i;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/95f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<EdgeType:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/95f",
        "<TEdgeType;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<TEdgeType;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<TEdgeType;>;)V"
        }
    .end annotation

    .prologue
    .line 1437185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1437186
    iput-object p1, p0, LX/95i;->a:Ljava/util/ArrayList;

    .line 1437187
    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/Object;)Z
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITEdgeType;)Z"
        }
    .end annotation

    .prologue
    .line 1437188
    iget-object v0, p0, LX/95i;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    if-eqz p2, :cond_0

    .line 1437189
    iget-object v0, p0, LX/95i;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, p2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1437190
    const/4 v0, 0x1

    .line 1437191
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
