.class public final LX/AIU;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    .line 1659728
    const/4 v8, 0x0

    .line 1659729
    const/4 v7, 0x0

    .line 1659730
    const/4 v6, 0x0

    .line 1659731
    const/4 v5, 0x0

    .line 1659732
    const/4 v4, 0x0

    .line 1659733
    const-wide/16 v2, 0x0

    .line 1659734
    const/4 v1, 0x0

    .line 1659735
    const/4 v0, 0x0

    .line 1659736
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v9, v10, :cond_a

    .line 1659737
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1659738
    const/4 v0, 0x0

    .line 1659739
    :goto_0
    return v0

    .line 1659740
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v10, :cond_8

    .line 1659741
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    .line 1659742
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1659743
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_0

    if-eqz v1, :cond_0

    .line 1659744
    const-string v10, "direct_message_type"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 1659745
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLBackstagePostTypeEnum;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBackstagePostTypeEnum;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    move v9, v1

    goto :goto_1

    .line 1659746
    :cond_1
    const-string v10, "id"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 1659747
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v8, v1

    goto :goto_1

    .line 1659748
    :cond_2
    const-string v10, "message_media"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 1659749
    invoke-static {p0, p1}, LX/AIT;->a(LX/15w;LX/186;)I

    move-result v1

    move v7, v1

    goto :goto_1

    .line 1659750
    :cond_3
    const-string v10, "message_owner"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 1659751
    invoke-static {p0, p1}, LX/AIe;->a(LX/15w;LX/186;)I

    move-result v1

    move v5, v1

    goto :goto_1

    .line 1659752
    :cond_4
    const-string v10, "message_text"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 1659753
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v4, v1

    goto :goto_1

    .line 1659754
    :cond_5
    const-string v10, "time"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 1659755
    const/4 v0, 0x1

    .line 1659756
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    goto :goto_1

    .line 1659757
    :cond_6
    const-string v10, "url"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1659758
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v6, v1

    goto/16 :goto_1

    .line 1659759
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1659760
    :cond_8
    const/4 v1, 0x7

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1659761
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 1659762
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 1659763
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 1659764
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1659765
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1659766
    if-eqz v0, :cond_9

    .line 1659767
    const/4 v1, 0x5

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1659768
    :cond_9
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1659769
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_a
    move v9, v8

    move v8, v7

    move v7, v6

    move v6, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 1659770
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1659771
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1659772
    if-eqz v0, :cond_0

    .line 1659773
    const-string v0, "direct_message_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1659774
    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1659775
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1659776
    if-eqz v0, :cond_1

    .line 1659777
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1659778
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1659779
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1659780
    if-eqz v0, :cond_2

    .line 1659781
    const-string v1, "message_media"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1659782
    invoke-static {p0, v0, p2, p3}, LX/AIT;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1659783
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1659784
    if-eqz v0, :cond_3

    .line 1659785
    const-string v1, "message_owner"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1659786
    invoke-static {p0, v0, p2, p3}, LX/AIe;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1659787
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1659788
    if-eqz v0, :cond_4

    .line 1659789
    const-string v1, "message_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1659790
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1659791
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1659792
    cmp-long v2, v0, v2

    if-eqz v2, :cond_5

    .line 1659793
    const-string v2, "time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1659794
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1659795
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1659796
    if-eqz v0, :cond_6

    .line 1659797
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1659798
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1659799
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1659800
    return-void
.end method
