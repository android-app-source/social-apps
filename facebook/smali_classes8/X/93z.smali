.class public final LX/93z;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/composer/privacy/common/graphql/FetchProfileDetailsGraphQLModels$ProfileDetailsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

.field public final synthetic b:LX/940;


# direct methods
.method public constructor <init>(LX/940;Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V
    .locals 0

    .prologue
    .line 1434357
    iput-object p1, p0, LX/93z;->b:LX/940;

    iput-object p2, p0, LX/93z;->a:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1434376
    new-instance v0, LX/7lP;

    iget-object v1, p0, LX/93z;->a:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    invoke-direct {v0, v1}, LX/7lP;-><init>(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    const/4 v1, 0x0

    .line 1434377
    iput-boolean v1, v0, LX/7lP;->b:Z

    .line 1434378
    move-object v0, v0

    .line 1434379
    invoke-virtual {v0}, LX/7lP;->a()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    .line 1434380
    iget-object v1, p0, LX/93z;->b:LX/940;

    invoke-virtual {v1, v0}, LX/93Q;->a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    .line 1434381
    instance-of v0, p1, LX/2Oo;

    if-eqz v0, :cond_0

    .line 1434382
    iget-object v0, p0, LX/93z;->b:LX/940;

    .line 1434383
    iget-object v1, v0, LX/93Q;->b:LX/03V;

    move-object v0, v1

    .line 1434384
    const-string v1, "composer_profile_details_fetch_error"

    const-string v2, "Failed to fetch profile details for composer"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1434385
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1434358
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1434359
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1434360
    check-cast v0, Lcom/facebook/composer/privacy/common/graphql/FetchProfileDetailsGraphQLModels$ProfileDetailsModel;

    .line 1434361
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/composer/privacy/common/graphql/FetchProfileDetailsGraphQLModels$ProfileDetailsModel;->a()LX/2rX;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/composer/privacy/common/graphql/FetchProfileDetailsGraphQLModels$ProfileDetailsModel;->a()LX/2rX;

    move-result-object v1

    invoke-interface {v1}, LX/2rX;->d()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/composer/privacy/common/graphql/FetchProfileDetailsGraphQLModels$ProfileDetailsModel;->a()LX/2rX;

    move-result-object v1

    invoke-interface {v1}, LX/2rX;->c()LX/1Fd;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1434362
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "privacy scope missing"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/93z;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    .line 1434363
    :goto_0
    return-void

    .line 1434364
    :cond_1
    new-instance v1, LX/7lN;

    iget-object v2, p0, LX/93z;->a:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-object v2, v2, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->a:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    invoke-direct {v1, v2}, LX/7lN;-><init>(Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;)V

    invoke-virtual {v0}, Lcom/facebook/composer/privacy/common/graphql/FetchProfileDetailsGraphQLModels$ProfileDetailsModel;->a()LX/2rX;

    move-result-object v2

    invoke-static {v2}, LX/940;->b(LX/2rX;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v2

    .line 1434365
    iput-object v2, v1, LX/7lN;->a:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    .line 1434366
    move-object v1, v1

    .line 1434367
    iget-object v2, p0, LX/93z;->b:LX/940;

    invoke-virtual {v0}, Lcom/facebook/composer/privacy/common/graphql/FetchProfileDetailsGraphQLModels$ProfileDetailsModel;->a()LX/2rX;

    move-result-object v0

    iget-object v3, p0, LX/93z;->b:LX/940;

    iget-object v3, v3, LX/940;->c:Ljava/lang/String;

    invoke-static {v2, v0, v3}, LX/940;->a$redex0(LX/940;LX/2rX;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1434368
    iput-object v0, v1, LX/7lN;->b:Ljava/lang/String;

    .line 1434369
    move-object v0, v1

    .line 1434370
    invoke-virtual {v0}, LX/7lN;->a()Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    move-result-object v0

    .line 1434371
    new-instance v1, LX/7lP;

    iget-object v2, p0, LX/93z;->a:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    invoke-direct {v1, v2}, LX/7lP;-><init>(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    const/4 v2, 0x0

    .line 1434372
    iput-boolean v2, v1, LX/7lP;->b:Z

    .line 1434373
    move-object v1, v1

    .line 1434374
    invoke-virtual {v1, v0}, LX/7lP;->a(Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;)LX/7lP;

    move-result-object v0

    invoke-virtual {v0}, LX/7lP;->a()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    .line 1434375
    iget-object v1, p0, LX/93z;->b:LX/940;

    invoke-virtual {v1, v0}, LX/93Q;->a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    goto :goto_0
.end method
