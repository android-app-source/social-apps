.class public final LX/8jj;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchOwnedStickerPacksQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1392695
    const-class v1, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchOwnedStickerPacksQueryModel;

    const v0, -0x6ec3211f

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FetchOwnedStickerPacksQuery"

    const-string v6, "c03b2bbc9c96db0415c2219b4eebbb94"

    const-string v7, "viewer"

    const-string v8, "10155093174976729"

    const-string v9, "10155259087891729"

    .line 1392696
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1392697
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1392698
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1392699
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1392700
    sparse-switch v0, :sswitch_data_0

    .line 1392701
    :goto_0
    return-object p1

    .line 1392702
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1392703
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1392704
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1392705
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1392706
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1392707
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x222e177d -> :sswitch_5
        0x58705dc -> :sswitch_0
        0x5ced2b0 -> :sswitch_1
        0x1df56d39 -> :sswitch_2
        0x73a026b5 -> :sswitch_3
        0x763c4507 -> :sswitch_4
    .end sparse-switch
.end method
