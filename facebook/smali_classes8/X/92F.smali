.class public LX/92F;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0dc;


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1432149
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "minutiae/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 1432150
    sput-object v0, LX/92F;->a:LX/0Tn;

    const-string v1, "minutiae_composer_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/92F;->b:LX/0Tn;

    .line 1432151
    sget-object v0, LX/92F;->a:LX/0Tn;

    const-string v1, "minutiae_reshare_composer_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/92F;->c:LX/0Tn;

    .line 1432152
    sget-object v0, LX/92F;->a:LX/0Tn;

    const-string v1, "ridge_full_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/92F;->d:LX/0Tn;

    .line 1432153
    sget-object v0, LX/92F;->a:LX/0Tn;

    const-string v1, "ridge_nux_dismiss"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/92F;->e:LX/0Tn;

    .line 1432154
    sget-object v0, LX/92F;->a:LX/0Tn;

    const-string v1, "last_minutiae_opened"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/92F;->f:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1432156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1432157
    return-void
.end method


# virtual methods
.method public final b()LX/0Rf;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1432155
    sget-object v0, LX/92F;->b:LX/0Tn;

    sget-object v1, LX/92F;->c:LX/0Tn;

    sget-object v2, LX/92F;->d:LX/0Tn;

    sget-object v3, LX/92F;->e:LX/0Tn;

    invoke-static {v0, v1, v2, v3}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
