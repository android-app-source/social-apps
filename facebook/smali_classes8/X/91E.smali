.class public final enum LX/91E;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/91E;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/91E;

.field public static final enum HIT_BACK:LX/91E;

.field public static final enum SEEN:LX/91E;

.field public static final enum SELECT:LX/91E;

.field public static final enum SKIP:LX/91E;


# instance fields
.field public final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1430249
    new-instance v0, LX/91E;

    const-string v1, "SEEN"

    const-string v2, "seen"

    invoke-direct {v0, v1, v3, v2}, LX/91E;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/91E;->SEEN:LX/91E;

    .line 1430250
    new-instance v0, LX/91E;

    const-string v1, "SELECT"

    const-string v2, "selected"

    invoke-direct {v0, v1, v4, v2}, LX/91E;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/91E;->SELECT:LX/91E;

    .line 1430251
    new-instance v0, LX/91E;

    const-string v1, "SKIP"

    const-string v2, "skipped"

    invoke-direct {v0, v1, v5, v2}, LX/91E;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/91E;->SKIP:LX/91E;

    .line 1430252
    new-instance v0, LX/91E;

    const-string v1, "HIT_BACK"

    const-string v2, "hit_back"

    invoke-direct {v0, v1, v6, v2}, LX/91E;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/91E;->HIT_BACK:LX/91E;

    .line 1430253
    const/4 v0, 0x4

    new-array v0, v0, [LX/91E;

    sget-object v1, LX/91E;->SEEN:LX/91E;

    aput-object v1, v0, v3

    sget-object v1, LX/91E;->SELECT:LX/91E;

    aput-object v1, v0, v4

    sget-object v1, LX/91E;->SKIP:LX/91E;

    aput-object v1, v0, v5

    sget-object v1, LX/91E;->HIT_BACK:LX/91E;

    aput-object v1, v0, v6

    sput-object v0, LX/91E;->$VALUES:[LX/91E;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1430254
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1430255
    iput-object p3, p0, LX/91E;->name:Ljava/lang/String;

    .line 1430256
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/91E;
    .locals 1

    .prologue
    .line 1430257
    const-class v0, LX/91E;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/91E;

    return-object v0
.end method

.method public static values()[LX/91E;
    .locals 1

    .prologue
    .line 1430258
    sget-object v0, LX/91E;->$VALUES:[LX/91E;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/91E;

    return-object v0
.end method
