.class public LX/8xd;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:LX/8xd;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# instance fields
.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field private final d:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LX/8xb;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field private final e:Ljava/lang/Object;

.field private final f:LX/8xc;

.field public g:Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1424525
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1424526
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/8xd;->b:Ljava/util/Set;

    .line 1424527
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/8xd;->c:Ljava/util/Map;

    .line 1424528
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/8xd;->d:Ljava/util/Queue;

    .line 1424529
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/8xd;->e:Ljava/lang/Object;

    .line 1424530
    new-instance v0, LX/8xc;

    invoke-direct {v0, p0}, LX/8xc;-><init>(LX/8xd;)V

    iput-object v0, p0, LX/8xd;->f:LX/8xc;

    .line 1424531
    return-void
.end method

.method public static declared-synchronized a()LX/8xd;
    .locals 2

    .prologue
    .line 1424521
    const-class v1, LX/8xd;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/8xd;->a:LX/8xd;

    if-nez v0, :cond_0

    .line 1424522
    new-instance v0, LX/8xd;

    invoke-direct {v0}, LX/8xd;-><init>()V

    sput-object v0, LX/8xd;->a:LX/8xd;

    .line 1424523
    :cond_0
    sget-object v0, LX/8xd;->a:LX/8xd;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1424524
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1424519
    iget-object v0, p0, LX/8xd;->g:Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;

    const-string v1, "onCompletedPersistedRelayQuery"

    invoke-interface {v0, v1, p1}, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;->emit(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1424520
    return-void
.end method

.method public static a$redex0(LX/8xd;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x2000

    .line 1424504
    const-string v0, "addSuccessfulResponse"

    invoke-static {v6, v7, v0}, LX/018;->a(JLjava/lang/String;)V

    .line 1424505
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1424506
    const-string v1, "successful_results"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "error_results"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "skipped_results"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_1

    .line 1424507
    :cond_0
    const-string v0, "Some results have errors or have been skipped"

    invoke-static {p0, p1, v0}, LX/8xd;->b$redex0(LX/8xd;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1424508
    :goto_0
    invoke-static {v6, v7}, LX/018;->a(J)V

    .line 1424509
    :goto_1
    return-void

    .line 1424510
    :cond_1
    :try_start_1
    const-string v1, "error"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1424511
    const-string v1, "error"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v0}, LX/8xd;->b$redex0(LX/8xd;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 1424512
    :catch_0
    move-exception v0

    .line 1424513
    :try_start_2
    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v0}, LX/8xd;->b$redex0(LX/8xd;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1424514
    invoke-static {v6, v7}, LX/018;->a(J)V

    goto :goto_1

    .line 1424515
    :cond_2
    :try_start_3
    iget-object v1, p0, LX/8xd;->e:Ljava/lang/Object;

    monitor-enter v1
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1424516
    :try_start_4
    iget-object v2, p0, LX/8xd;->d:Ljava/util/Queue;

    new-instance v3, LX/8xb;

    const/4 v4, 0x0

    invoke-direct {v3, p1, v0, v4}, LX/8xb;-><init>(Ljava/lang/String;Lorg/json/JSONObject;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 1424517
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v0
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1424518
    :catchall_1
    move-exception v0

    invoke-static {v6, v7}, LX/018;->a(J)V

    throw v0
.end method

.method public static b(LX/8xd;)V
    .locals 12

    .prologue
    const-wide/16 v8, 0x2000

    .line 1424454
    const-string v0, "processResults"

    invoke-static {v8, v9, v0}, LX/018;->a(JLjava/lang/String;)V

    .line 1424455
    iget-object v4, p0, LX/8xd;->e:Ljava/lang/Object;

    monitor-enter v4

    .line 1424456
    :try_start_0
    iget-object v0, p0, LX/8xd;->d:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 1424457
    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1424458
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8xb;

    .line 1424459
    iget-object v1, p0, LX/8xd;->b:Ljava/util/Set;

    .line 1424460
    iget-object v2, v0, LX/8xb;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1424461
    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1424462
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    .line 1424463
    iget-object v1, v0, LX/8xb;->a:Ljava/lang/String;

    move-object v6, v1

    .line 1424464
    iget-object v1, p0, LX/8xd;->c:Ljava/util/Map;

    invoke-interface {v1, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1424465
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Got response for entry that was not requested "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v6, v0}, LX/8xd;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1424466
    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1424467
    :cond_1
    :try_start_1
    iget-object v1, v0, LX/8xb;->c:Ljava/lang/String;

    move-object v1, v1

    .line 1424468
    if-eqz v1, :cond_2

    .line 1424469
    iget-object v1, v0, LX/8xb;->c:Ljava/lang/String;

    move-object v0, v1

    .line 1424470
    invoke-direct {p0, v6, v0}, LX/8xd;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1424471
    :cond_2
    :try_start_2
    iget-object v1, v0, LX/8xb;->b:Lorg/json/JSONObject;

    move-object v0, v1

    .line 1424472
    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    .line 1424473
    const-string v1, "successful_results"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1424474
    invoke-direct {p0, v6}, LX/8xd;->a(Ljava/lang/String;)V

    .line 1424475
    iget-object v0, p0, LX/8xd;->c:Ljava/util/Map;

    invoke-interface {v0, v6}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1424476
    iget-object v0, p0, LX/8xd;->b:Ljava/util/Set;

    invoke-interface {v0, v6}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1424477
    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1424478
    :goto_1
    return-void

    .line 1424479
    :cond_3
    :try_start_4
    invoke-virtual {v0}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v7

    .line 1424480
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1424481
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1424482
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1424483
    invoke-static {}, LX/5op;->a()LX/5pD;

    move-result-object v10

    .line 1424484
    invoke-interface {v10, v6}, LX/5pD;->pushString(Ljava/lang/String;)V

    .line 1424485
    invoke-interface {v10, v1}, LX/5pD;->pushString(Ljava/lang/String;)V

    .line 1424486
    invoke-interface {v10, v2}, LX/5pD;->pushString(Ljava/lang/String;)V

    .line 1424487
    iget-object v3, p0, LX/8xd;->g:Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;

    invoke-static {v3}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;

    const-string v11, "onNextPersistedRelayQuery"

    invoke-interface {v3, v11, v10}, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;->emit(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1424488
    iget-object v2, p0, LX/8xd;->c:Ljava/util/Map;

    invoke-interface {v2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    .line 1424489
    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 1424490
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Got response for query that was not requested "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v6, v1}, LX/8xd;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 1424491
    :catch_0
    move-exception v0

    .line 1424492
    :try_start_5
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v6, v0}, LX/8xd;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 1424493
    :cond_4
    :try_start_6
    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    .line 1424494
    if-nez v3, :cond_5

    .line 1424495
    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1424496
    :goto_3
    iget-object v1, p0, LX/8xd;->c:Ljava/util/Map;

    invoke-interface {v1, v6, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 1424497
    :cond_5
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 1424498
    :cond_6
    iget-object v0, p0, LX/8xd;->c:Ljava/util/Map;

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1424499
    invoke-direct {p0, v6}, LX/8xd;->a(Ljava/lang/String;)V

    .line 1424500
    iget-object v0, p0, LX/8xd;->c:Ljava/util/Map;

    invoke-interface {v0, v6}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1424501
    iget-object v0, p0, LX/8xd;->b:Ljava/util/Set;

    invoke-interface {v0, v6}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0

    .line 1424502
    :cond_7
    :try_start_7
    monitor-exit v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 1424503
    invoke-static {v8, v9}, LX/018;->a(J)V

    goto/16 :goto_1
.end method

.method public static b$redex0(LX/8xd;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1424451
    iget-object v1, p0, LX/8xd;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 1424452
    :try_start_0
    iget-object v0, p0, LX/8xd;->d:Ljava/util/Queue;

    new-instance v2, LX/8xb;

    const/4 v3, 0x0

    invoke-direct {v2, p1, v3, p2}, LX/8xb;-><init>(Ljava/lang/String;Lorg/json/JSONObject;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 1424453
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1424446
    iget-object v1, p0, LX/8xd;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 1424447
    :try_start_0
    invoke-static {p0, p1, p2}, LX/8xd;->d(LX/8xd;Ljava/lang/String;Ljava/lang/String;)V

    .line 1424448
    iget-object v0, p0, LX/8xd;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1424449
    iget-object v0, p0, LX/8xd;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1424450
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static d(LX/8xd;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1424440
    const-string v0, "React"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to prefetch "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", cause: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1424441
    invoke-static {}, LX/5op;->a()LX/5pD;

    move-result-object v1

    .line 1424442
    invoke-interface {v1, p1}, LX/5pD;->pushString(Ljava/lang/String;)V

    .line 1424443
    invoke-interface {v1, p2}, LX/5pD;->pushString(Ljava/lang/String;)V

    .line 1424444
    iget-object v0, p0, LX/8xd;->g:Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;

    const-string v2, "onErrorPersistedRelayQuery"

    invoke-interface {v0, v2, v1}, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;->emit(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1424445
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Lcom/facebook/fbreact/persistedqueries/Fb4aPersistedQueryRequestSender;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/facebook/catalyst/modules/persistedqueries/RelayPersistedQueriesPreloader$RelayPersistedQueryRequestSender;",
            "Z)V"
        }
    .end annotation

    .prologue
    const-wide/16 v2, 0x2000

    const/4 v0, 0x1

    .line 1424399
    const-string v1, "PersistedQueries.prefetch"

    invoke-static {v2, v3, v1}, LX/018;->a(JLjava/lang/String;)V

    .line 1424400
    const/4 v1, 0x0

    .line 1424401
    if-eqz p6, :cond_0

    .line 1424402
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 1424403
    const-string v2, "param"

    const-string v3, "after"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1424404
    const-string v2, "import"

    const-string v3, "boundaryCursor"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1424405
    const-string v2, "max_runs"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1424406
    :cond_0
    invoke-static {p1, p2, p3, p4, v1}, LX/8xZ;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Lorg/json/JSONObject;)Ljava/util/Map;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1424407
    if-nez v1, :cond_1

    .line 1424408
    const-string v0, "React"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Entry "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not found in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1424409
    :goto_0
    return-void

    .line 1424410
    :catch_0
    move-exception v0

    .line 1424411
    const-string v1, "React"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error getting prefetch queries for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1424412
    :cond_1
    iget-object v2, p0, LX/8xd;->e:Ljava/lang/Object;

    monitor-enter v2

    .line 1424413
    :try_start_1
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 1424414
    if-eqz p6, :cond_2

    :goto_1
    add-int/lit8 v4, v0, 0x1

    .line 1424415
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1424416
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v3, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1424417
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v5, p0, LX/8xd;->f:LX/8xc;

    .line 1424418
    iget-object v6, p5, Lcom/facebook/fbreact/persistedqueries/Fb4aPersistedQueryRequestSender;->d:LX/0TD;

    new-instance p1, LX/98y;

    invoke-direct {p1, p5, v0, p3}, LX/98y;-><init>(Lcom/facebook/fbreact/persistedqueries/Fb4aPersistedQueryRequestSender;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, p1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    .line 1424419
    new-instance p1, LX/98z;

    invoke-direct {p1, p5, v5, p3}, LX/98z;-><init>(Lcom/facebook/fbreact/persistedqueries/Fb4aPersistedQueryRequestSender;LX/8xc;Ljava/lang/String;)V

    invoke-static {v6, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1424420
    goto :goto_2

    .line 1424421
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1424422
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 1424423
    :cond_3
    :try_start_2
    iget-object v0, p0, LX/8xd;->c:Ljava/util/Map;

    invoke-interface {v0, p3, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1424424
    const-wide/16 v0, 0x2000

    invoke-static {v0, v1}, LX/018;->a(J)V

    .line 1424425
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;Ljava/lang/String;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x2000

    .line 1424426
    const-string v0, "PersistedQueries.getEntry"

    invoke-static {v2, v3, v0}, LX/018;->a(JLjava/lang/String;)V

    .line 1424427
    :try_start_0
    iput-object p1, p0, LX/8xd;->g:Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;

    .line 1424428
    iget-object v1, p0, LX/8xd;->e:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1424429
    :try_start_1
    iget-object v0, p0, LX/8xd;->c:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1424430
    const-string v0, "Entry was not prefetched"

    invoke-static {p0, p2, v0}, LX/8xd;->d(LX/8xd;Ljava/lang/String;Ljava/lang/String;)V

    .line 1424431
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1424432
    invoke-static {v2, v3}, LX/018;->a(J)V

    .line 1424433
    :goto_0
    return-void

    .line 1424434
    :cond_0
    :try_start_2
    iget-object v0, p0, LX/8xd;->b:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1424435
    invoke-static {p0}, LX/8xd;->b(LX/8xd;)V

    .line 1424436
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1424437
    invoke-static {v2, v3}, LX/018;->a(J)V

    goto :goto_0

    .line 1424438
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1424439
    :catchall_1
    move-exception v0

    invoke-static {v2, v3}, LX/018;->a(J)V

    throw v0
.end method
