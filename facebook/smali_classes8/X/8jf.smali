.class public final LX/8jf;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchAvailableTaggedStickersWithPreviewsQueryModel;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1392652
    const-class v1, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchAvailableTaggedStickersWithPreviewsQueryModel;

    const v0, 0x759ecf07

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x2

    const-string v5, "FetchAvailableTaggedStickersWithPreviewsQuery"

    const-string v6, "447ace8b10fa3e19e5833f9ab3b502f1"

    const-string v7, "nodes"

    const-string v8, "10155093174991729"

    const/4 v9, 0x0

    .line 1392653
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1392654
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1392655
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1392656
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1392657
    sparse-switch v0, :sswitch_data_0

    .line 1392658
    :goto_0
    return-object p1

    .line 1392659
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1392660
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1392661
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1392662
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1392663
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1392664
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x51392de8 -> :sswitch_3
        -0x46c3012f -> :sswitch_0
        0x1df56d39 -> :sswitch_1
        0x56cb4b91 -> :sswitch_5
        0x73a026b5 -> :sswitch_2
        0x763c4507 -> :sswitch_4
    .end sparse-switch
.end method

.method public final l()Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;
    .locals 2

    .prologue
    .line 1392665
    new-instance v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQL$FetchAvailableTaggedStickersWithPreviewsQueryString$1;

    const-class v1, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchAvailableTaggedStickersWithPreviewsQueryModel;

    invoke-direct {v0, p0, v1}, Lcom/facebook/stickers/graphql/FetchStickersGraphQL$FetchAvailableTaggedStickersWithPreviewsQueryString$1;-><init>(LX/8jf;Ljava/lang/Class;)V

    return-object v0
.end method
