.class public LX/APf;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0io;",
        "DerivedData::",
        "LX/5RE;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;>",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0il;)V
    .locals 2
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1670284
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1670285
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/APf;->a:Ljava/lang/ref/WeakReference;

    .line 1670286
    return-void
.end method


# virtual methods
.method public final a()LX/0P1;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1670287
    iget-object v0, p0, LX/APf;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    move-object v1, v0

    .line 1670288
    check-cast v1, LX/0ik;

    invoke-interface {v1}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/5RE;

    invoke-interface {v1}, LX/5RE;->I()LX/5RF;

    move-result-object v1

    .line 1670289
    sget-object v3, LX/5RF;->MULTIMEDIA:LX/5RF;

    if-eq v1, v3, :cond_1

    sget-object v3, LX/5RF;->MULTIPLE_PHOTOS:LX/5RF;

    if-eq v1, v3, :cond_1

    sget-object v3, LX/5RF;->SINGLE_PHOTO:LX/5RF;

    if-eq v1, v3, :cond_1

    sget-object v3, LX/5RF;->SINGLE_VIDEO:LX/5RF;

    if-eq v1, v3, :cond_1

    sget-object v3, LX/5RF;->MULTIPLE_VIDEOS:LX/5RF;

    if-eq v1, v3, :cond_1

    .line 1670290
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 1670291
    sget-object v2, LX/5RF;->STICKER:LX/5RF;

    if-ne v1, v2, :cond_0

    .line 1670292
    add-int/lit8 v0, v0, 0x1

    .line 1670293
    :cond_0
    invoke-virtual {v1}, LX/5RF;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v1, v0}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    .line 1670294
    :goto_0
    return-object v0

    .line 1670295
    :cond_1
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v5, :cond_3

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1670296
    invoke-static {v0}, LX/7kq;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1670297
    add-int/lit8 v0, v1, 0x1

    move v1, v2

    .line 1670298
    :goto_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    move v1, v0

    goto :goto_1

    .line 1670299
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v6, v1

    move v1, v0

    move v0, v6

    goto :goto_2

    .line 1670300
    :cond_3
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    .line 1670301
    if-lez v2, :cond_4

    .line 1670302
    const-string v3, "PHOTO"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1670303
    :cond_4
    if-lez v1, :cond_5

    .line 1670304
    const-string v2, "VIDEO"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1670305
    :cond_5
    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    goto :goto_0
.end method
