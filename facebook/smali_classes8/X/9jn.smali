.class public LX/9jn;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# static fields
.field private static final c:Ljava/lang/Object;


# instance fields
.field public final a:LX/0Sh;

.field public final b:LX/2SA;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2SA",
            "<",
            "LX/9jm;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1530410
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/9jn;->c:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Sh;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1530411
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1530412
    iput-object p1, p0, LX/9jn;->a:LX/0Sh;

    .line 1530413
    const/16 v0, 0x14

    invoke-static {v0}, LX/2SA;->a(I)LX/2SA;

    move-result-object v0

    iput-object v0, p0, LX/9jn;->b:LX/2SA;

    .line 1530414
    return-void
.end method

.method public static a(LX/0QB;)LX/9jn;
    .locals 7

    .prologue
    .line 1530415
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 1530416
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 1530417
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 1530418
    if-nez v1, :cond_0

    .line 1530419
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1530420
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 1530421
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 1530422
    sget-object v1, LX/9jn;->c:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1530423
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 1530424
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1530425
    :cond_1
    if-nez v1, :cond_4

    .line 1530426
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 1530427
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 1530428
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 1530429
    new-instance p0, LX/9jn;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v1

    check-cast v1, LX/0Sh;

    invoke-direct {p0, v1}, LX/9jn;-><init>(LX/0Sh;)V

    .line 1530430
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1530431
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 1530432
    if-nez v1, :cond_2

    .line 1530433
    sget-object v0, LX/9jn;->c:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9jn;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1530434
    :goto_1
    if-eqz v0, :cond_3

    .line 1530435
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1530436
    :goto_3
    check-cast v0, LX/9jn;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1530437
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 1530438
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1530439
    :catchall_1
    move-exception v0

    .line 1530440
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1530441
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1530442
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 1530443
    :cond_2
    :try_start_8
    sget-object v0, LX/9jn;->c:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9jn;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(LX/0TF;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TF",
            "<",
            "LX/9jN;",
            ">;)V"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1530444
    iget-object v0, p0, LX/9jn;->b:LX/2SA;

    invoke-virtual {v0}, LX/2SB;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9jm;

    .line 1530445
    if-eqz v0, :cond_0

    iget-object v1, v0, LX/9jm;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    if-nez v1, :cond_1

    .line 1530446
    :cond_0
    :goto_0
    return-void

    .line 1530447
    :cond_1
    iget-object v0, v0, LX/9jm;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v1

    invoke-static {v0, p1, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
