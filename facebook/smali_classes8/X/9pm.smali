.class public final LX/9pm;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1545397
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_5

    .line 1545398
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1545399
    :goto_0
    return v1

    .line 1545400
    :cond_0
    const-string v6, "is_truncated_at_beginning"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1545401
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v3, v0

    move v0, v2

    .line 1545402
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_3

    .line 1545403
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1545404
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1545405
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 1545406
    const-string v6, "formatted_message"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1545407
    invoke-static {p0, p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1545408
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1545409
    :cond_3
    const/4 v5, 0x2

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1545410
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1545411
    if-eqz v0, :cond_4

    .line 1545412
    invoke-virtual {p1, v2, v3}, LX/186;->a(IZ)V

    .line 1545413
    :cond_4
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1545414
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1545415
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1545416
    if-eqz v0, :cond_0

    .line 1545417
    const-string v1, "formatted_message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1545418
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 1545419
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1545420
    if-eqz v0, :cond_1

    .line 1545421
    const-string v1, "is_truncated_at_beginning"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1545422
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1545423
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1545424
    return-void
.end method
