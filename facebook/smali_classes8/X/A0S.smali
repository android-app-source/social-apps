.class public final enum LX/A0S;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/A0S;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/A0S;

.field public static final enum GRAPH_SEARCH:LX/A0S;

.field public static final enum SIMPLE_SEARCH:LX/A0S;

.field public static final enum VIDEO_SEARCH:LX/A0S;


# instance fields
.field private final mParamName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1605028
    new-instance v0, LX/A0S;

    const-string v1, "SIMPLE_SEARCH"

    const-string v2, "simple_search"

    invoke-direct {v0, v1, v3, v2}, LX/A0S;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/A0S;->SIMPLE_SEARCH:LX/A0S;

    .line 1605029
    new-instance v0, LX/A0S;

    const-string v1, "GRAPH_SEARCH"

    const-string v2, "graph_search"

    invoke-direct {v0, v1, v4, v2}, LX/A0S;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/A0S;->GRAPH_SEARCH:LX/A0S;

    .line 1605030
    new-instance v0, LX/A0S;

    const-string v1, "VIDEO_SEARCH"

    const-string v2, "video_search"

    invoke-direct {v0, v1, v5, v2}, LX/A0S;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/A0S;->VIDEO_SEARCH:LX/A0S;

    .line 1605031
    const/4 v0, 0x3

    new-array v0, v0, [LX/A0S;

    sget-object v1, LX/A0S;->SIMPLE_SEARCH:LX/A0S;

    aput-object v1, v0, v3

    sget-object v1, LX/A0S;->GRAPH_SEARCH:LX/A0S;

    aput-object v1, v0, v4

    sget-object v1, LX/A0S;->VIDEO_SEARCH:LX/A0S;

    aput-object v1, v0, v5

    sput-object v0, LX/A0S;->$VALUES:[LX/A0S;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1605022
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1605023
    iput-object p3, p0, LX/A0S;->mParamName:Ljava/lang/String;

    .line 1605024
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/A0S;
    .locals 1

    .prologue
    .line 1605025
    const-class v0, LX/A0S;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/A0S;

    return-object v0
.end method

.method public static values()[LX/A0S;
    .locals 1

    .prologue
    .line 1605026
    sget-object v0, LX/A0S;->$VALUES:[LX/A0S;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/A0S;

    return-object v0
.end method


# virtual methods
.method public final getParamName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1605027
    iget-object v0, p0, LX/A0S;->mParamName:Ljava/lang/String;

    return-object v0
.end method
