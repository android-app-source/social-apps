.class public final LX/9W5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:LX/9WC;


# direct methods
.method public constructor <init>(LX/9WC;)V
    .locals 0

    .prologue
    .line 1500592
    iput-object p1, p0, LX/9W5;->a:LX/9WC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1500563
    iget-object v0, p0, LX/9W5;->a:LX/9WC;

    const/4 v1, 0x0

    .line 1500564
    iput-boolean v1, v0, LX/9WC;->l:Z

    .line 1500565
    iget-object v0, p0, LX/9W5;->a:LX/9WC;

    check-cast p1, Landroid/widget/ListView;

    .line 1500566
    invoke-virtual {p1}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v1

    if-ge p3, v1, :cond_0

    .line 1500567
    invoke-static {v0}, LX/9WC;->h(LX/9WC;)V

    .line 1500568
    :goto_0
    return-void

    .line 1500569
    :cond_0
    invoke-virtual {p1, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/9WM;

    .line 1500570
    invoke-virtual {p1}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result p0

    sub-int p0, p3, p0

    .line 1500571
    sget-object p2, LX/9Tp;->e:LX/0P1;

    invoke-virtual {v1}, LX/9WM;->c()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object p4

    invoke-virtual {p2, p4}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    .line 1500572
    sget-object p2, LX/9W1;->a:[I

    .line 1500573
    iget-object p4, v1, LX/9WM;->c:LX/9To;

    move-object p4, p4

    .line 1500574
    invoke-virtual {p4}, LX/9To;->ordinal()I

    move-result p4

    aget p2, p2, p4

    packed-switch p2, :pswitch_data_0

    .line 1500575
    invoke-static {v0}, LX/9WC;->h(LX/9WC;)V

    .line 1500576
    :cond_1
    :goto_1
    invoke-virtual {p1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    check-cast v1, Landroid/widget/HeaderViewListAdapter;

    invoke-virtual {v1}, Landroid/widget/HeaderViewListAdapter;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    check-cast v1, Landroid/widget/BaseAdapter;

    .line 1500577
    const p0, 0x40e333e5    # 7.100085f

    invoke-static {v1, p0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0

    .line 1500578
    :pswitch_0
    invoke-static {v0}, LX/9WC;->h(LX/9WC;)V

    .line 1500579
    sget-object p0, LX/9To;->ASK_TO_CONFIRM:LX/9To;

    .line 1500580
    iput-object p0, v1, LX/9WM;->c:LX/9To;

    .line 1500581
    iget-object p0, v0, LX/9WC;->r:LX/9Ve;

    .line 1500582
    iget-object p2, v1, LX/9WM;->a:Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;

    move-object v1, p2

    .line 1500583
    invoke-virtual {v1}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->e()Ljava/lang/String;

    move-result-object v1

    .line 1500584
    new-instance p2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p4, "negativefeedback_require_confirmation"

    invoke-direct {p2, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1500585
    invoke-static {p0, p2, v1}, LX/9Ve;->a(LX/9Ve;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 1500586
    goto :goto_1

    .line 1500587
    :pswitch_1
    invoke-static {v0, v1, p0, p1}, LX/9WC;->a(LX/9WC;LX/9WM;ILandroid/widget/ListView;)V

    goto :goto_1

    .line 1500588
    :cond_2
    invoke-static {v0}, LX/9WC;->h(LX/9WC;)V

    .line 1500589
    iget-object p2, v1, LX/9WM;->c:LX/9To;

    move-object p2, p2

    .line 1500590
    sget-object p4, LX/9To;->COMPLETED:LX/9To;

    if-eq p2, p4, :cond_1

    .line 1500591
    invoke-static {v0, v1, p0, p1}, LX/9WC;->a(LX/9WC;LX/9WM;ILandroid/widget/ListView;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
