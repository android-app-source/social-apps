.class public final synthetic LX/8rH;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1408707
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->values()[Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/8rH;->a:[I

    :try_start_0
    sget-object v0, LX/8rH;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->HEADER_TWO:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_6

    :goto_0
    :try_start_1
    sget-object v0, LX/8rH;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->HEADER_ONE:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_5

    :goto_1
    :try_start_2
    sget-object v0, LX/8rH;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->ORDERED_LIST_ITEM:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_4

    :goto_2
    :try_start_3
    sget-object v0, LX/8rH;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->UNORDERED_LIST_ITEM:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :goto_3
    :try_start_4
    sget-object v0, LX/8rH;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->CODE:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_2

    :goto_4
    :try_start_5
    sget-object v0, LX/8rH;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->BLOCKQUOTE:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_1

    :goto_5
    :try_start_6
    sget-object v0, LX/8rH;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->UNSTYLED:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_0

    :goto_6
    return-void

    :catch_0
    goto :goto_6

    :catch_1
    goto :goto_5

    :catch_2
    goto :goto_4

    :catch_3
    goto :goto_3

    :catch_4
    goto :goto_2

    :catch_5
    goto :goto_1

    :catch_6
    goto :goto_0
.end method
