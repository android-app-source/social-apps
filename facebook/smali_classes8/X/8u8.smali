.class public abstract LX/8u8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/style/LeadingMarginSpan;


# instance fields
.field public a:F

.field public b:I

.field public c:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1414524
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-direct {p0, v0}, LX/8u8;-><init>(Landroid/util/DisplayMetrics;)V

    .line 1414525
    return-void
.end method

.method public constructor <init>(Landroid/util/DisplayMetrics;)V
    .locals 2

    .prologue
    .line 1414520
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1414521
    const/high16 v0, -0x1000000

    iput v0, p0, LX/8u8;->b:I

    .line 1414522
    const/4 v0, 0x2

    const/high16 v1, 0x41a00000    # 20.0f

    invoke-static {v0, v1, p1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    iput v0, p0, LX/8u8;->a:F

    .line 1414523
    return-void
.end method


# virtual methods
.method public a(I)LX/8u8;
    .locals 2

    .prologue
    .line 1414526
    iput p1, p0, LX/8u8;->b:I

    .line 1414527
    iget-object v0, p0, LX/8u8;->c:Landroid/graphics/Paint;

    if-eqz v0, :cond_0

    .line 1414528
    iget-object v0, p0, LX/8u8;->c:Landroid/graphics/Paint;

    iget v1, p0, LX/8u8;->b:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1414529
    :cond_0
    return-object p0
.end method

.method public final a(Landroid/graphics/Paint;)V
    .locals 2

    .prologue
    .line 1414515
    iget-object v0, p0, LX/8u8;->c:Landroid/graphics/Paint;

    if-nez v0, :cond_0

    .line 1414516
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, p1}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v0, p0, LX/8u8;->c:Landroid/graphics/Paint;

    .line 1414517
    iget-object v0, p0, LX/8u8;->c:Landroid/graphics/Paint;

    iget v1, p0, LX/8u8;->b:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1414518
    iget-object v0, p0, LX/8u8;->c:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1414519
    :cond_0
    return-void
.end method

.method public final getLeadingMargin(Z)I
    .locals 1

    .prologue
    .line 1414514
    iget v0, p0, LX/8u8;->a:F

    float-to-int v0, v0

    return v0
.end method
