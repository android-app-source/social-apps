.class public LX/ASY;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/ASX;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1674150
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1674151
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;Landroid/view/ViewGroup;Ljava/lang/Object;LX/HqU;LX/Hrb;LX/0Px;)LX/ASX;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData:",
            "Ljava/lang/Object;",
            ":",
            "LX/0io;",
            ":",
            "Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData$ProvidesAudienceEducatorData;",
            ":",
            "LX/0j0;",
            ":",
            "LX/0j1;",
            ":",
            "LX/0j3;",
            ":",
            "LX/0j4;",
            ":",
            "LX/0j6;",
            ":",
            "LX/0ip;",
            ":",
            "LX/0jF;",
            "DerivedData:",
            "Ljava/lang/Object;",
            ":",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesIsMinutiaeSupported;",
            ":",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesIsPrivacyPillSupported;",
            "Services:",
            "Ljava/lang/Object;",
            ":",
            "LX/0il",
            "<TModelData;>;:",
            "LX/0ik",
            "<TDerivedData;>;>(",
            "Landroid/view/ViewGroup;",
            "Landroid/view/ViewGroup;",
            "TServices;",
            "Lcom/facebook/composer/tip/StickyGuardrailInterstitialController$DataProvider;",
            "Lcom/facebook/composer/tip/StickyGuardrailInterstitialController$StickyGuardrailCallback;",
            "LX/0Px",
            "<",
            "LX/3l4;",
            ">;)",
            "LX/ASX",
            "<TModelData;TDerivedData;TServices;>;"
        }
    .end annotation

    .prologue
    .line 1674152
    new-instance v0, LX/ASX;

    invoke-static {p0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v1

    check-cast v1, LX/0iA;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    const-class v3, LX/ASC;

    invoke-interface {p0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/ASC;

    move-object v6, p3

    check-cast v6, LX/0il;

    move-object v4, p1

    move-object v5, p2

    move-object v7, p4

    move-object v8, p5

    move-object/from16 v9, p6

    invoke-direct/range {v0 .. v9}, LX/ASX;-><init>(LX/0iA;LX/03V;LX/ASC;Landroid/view/ViewGroup;Landroid/view/ViewGroup;LX/0il;LX/HqU;LX/Hrb;LX/0Px;)V

    .line 1674153
    return-object v0
.end method
