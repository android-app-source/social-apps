.class public final LX/91s;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/91v;


# direct methods
.method public constructor <init>(LX/91v;)V
    .locals 0

    .prologue
    .line 1431525
    iput-object p1, p0, LX/91s;->a:LX/91v;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1431526
    iget-object v0, p0, LX/91s;->a:LX/91v;

    const/4 v1, 0x0

    .line 1431527
    iput-boolean v1, v0, LX/91v;->o:Z

    .line 1431528
    new-instance v0, LX/91r;

    invoke-direct {v0, p0}, LX/91r;-><init>(LX/91s;)V

    .line 1431529
    invoke-static {p1}, LX/3d6;->forException(Ljava/lang/Throwable;)LX/1nY;

    move-result-object v1

    sget-object v2, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    if-ne v1, v2, :cond_0

    .line 1431530
    iget-object v1, p0, LX/91s;->a:LX/91v;

    iget-object v1, v1, LX/91v;->l:LX/909;

    invoke-interface {v1, v0}, LX/909;->a(LX/8zc;)V

    .line 1431531
    :goto_0
    iget-object v0, p0, LX/91s;->a:LX/91v;

    iget-object v1, v0, LX/91v;->f:LX/91D;

    iget-object v0, p0, LX/91s;->a:LX/91v;

    iget-object v0, v0, LX/91v;->d:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1431532
    iget-object v2, v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v2, v2

    .line 1431533
    iget-object v0, p0, LX/91s;->a:LX/91v;

    .line 1431534
    iget-object v3, v0, LX/91v;->g:LX/5LG;

    move-object v0, v3

    .line 1431535
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/91s;->a:LX/91v;

    .line 1431536
    iget-object v3, v0, LX/91v;->g:LX/5LG;

    move-object v0, v3

    .line 1431537
    invoke-interface {v0}, LX/5LG;->l()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v2, v0, p1}, LX/91D;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1431538
    return-void

    .line 1431539
    :cond_0
    iget-object v1, p0, LX/91s;->a:LX/91v;

    iget-object v1, v1, LX/91v;->l:LX/909;

    invoke-interface {v1, v0}, LX/909;->b(LX/8zc;)V

    goto :goto_0

    .line 1431540
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
