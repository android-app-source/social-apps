.class public final LX/ANJ;
.super LX/6Il;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/cameracore/ui/CameraCoreFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V
    .locals 0

    .prologue
    .line 1667594
    iput-object p1, p0, LX/ANJ;->a:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    invoke-direct {p0}, LX/6Il;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1667601
    iget-object v0, p0, LX/ANJ;->a:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    invoke-static {v0, p1}, Lcom/facebook/cameracore/ui/CameraCoreFragment;->a$redex0(Lcom/facebook/cameracore/ui/CameraCoreFragment;Ljava/lang/Throwable;)V

    .line 1667602
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1667595
    iget-object v0, p0, LX/ANJ;->a:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1667596
    iget-object v0, p0, LX/ANJ;->a:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    iget-object v0, v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->J:LX/03V;

    const-string v1, "cameracore_reload_camera"

    const-string v2, "Fragment is no longer added"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1667597
    :goto_0
    return-void

    .line 1667598
    :cond_0
    const/4 v0, 0x0

    .line 1667599
    sput-object v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->h:LX/6Ia;

    .line 1667600
    iget-object v0, p0, LX/ANJ;->a:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    invoke-static {v0}, Lcom/facebook/cameracore/ui/CameraCoreFragment;->e(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V

    goto :goto_0
.end method
