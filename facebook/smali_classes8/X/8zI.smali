.class public final synthetic LX/8zI;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1427038
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->values()[Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/8zI;->a:[I

    :try_start_0
    sget-object v0, LX/8zI;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->PEOPLE:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_0
    :try_start_1
    sget-object v0, LX/8zI;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->PERSON:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    :goto_1
    :try_start_2
    sget-object v0, LX/8zI;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->PLACE:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_2
    :try_start_3
    sget-object v0, LX/8zI;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->OBJECT:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_3
    :try_start_4
    sget-object v0, LX/8zI;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->ICON:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_4
    return-void

    :catch_0
    goto :goto_4

    :catch_1
    goto :goto_3

    :catch_2
    goto :goto_2

    :catch_3
    goto :goto_1

    :catch_4
    goto :goto_0
.end method
