.class public LX/ARC;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0jI;",
        ":",
        "LX/0iu;",
        ":",
        "LX/0iw;",
        ":",
        "LX/0ix;",
        ":",
        "LX/0iy;",
        ":",
        "LX/0iz;",
        ":",
        "LX/0j0;",
        ":",
        "LX/0j2;",
        ":",
        "LX/0j3;",
        ":",
        "LX/0j7;",
        ":",
        "LX/0j8;",
        ":",
        "LX/0j4;",
        ":",
        "LX/0iq;",
        ":",
        "LX/0j9;",
        ":",
        "LX/0ir;",
        ":",
        "LX/0j5;",
        ":",
        "LX/0jB;",
        ":",
        "LX/0jD;",
        ":",
        "LX/0j6;",
        ":",
        "LX/0ip;",
        ":",
        "LX/0jG;",
        ":",
        "LX/0jE;",
        ":",
        "LX/0jH;",
        ":",
        "LX/0jF;",
        "DerivedData::",
        "LX/5Qv;",
        ":",
        "LX/2rg;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;>",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public volatile a:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field private final c:LX/0oz;

.field private final d:LX/0SG;

.field public final e:LX/AR3;

.field public final f:LX/AR1;

.field public final g:LX/0ad;


# direct methods
.method public constructor <init>(LX/0il;LX/AR3;LX/AR1;LX/0oz;LX/0SG;LX/0ad;)V
    .locals 1
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/AR3;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/AR1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "LX/AR3;",
            "LX/AR1;",
            "LX/0oz;",
            "LX/0SG;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1672829
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1672830
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/ARC;->b:Ljava/lang/ref/WeakReference;

    .line 1672831
    iput-object p2, p0, LX/ARC;->e:LX/AR3;

    .line 1672832
    iput-object p3, p0, LX/ARC;->f:LX/AR1;

    .line 1672833
    iput-object p4, p0, LX/ARC;->c:LX/0oz;

    .line 1672834
    iput-object p5, p0, LX/ARC;->d:LX/0SG;

    .line 1672835
    iput-object p6, p0, LX/ARC;->g:LX/0ad;

    .line 1672836
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/composer/publish/common/PublishPostParams;Z)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 1672807
    iget-object v0, p0, LX/ARC;->f:LX/AR1;

    invoke-virtual {v0}, LX/AR1;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/ARC;->f:LX/AR1;

    invoke-virtual {v0}, LX/AR1;->b()LX/9A3;

    move-result-object v0

    invoke-virtual {v0}, LX/9A3;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    :goto_0
    const/4 v2, 0x1

    .line 1672808
    iget-object v1, p0, LX/ARC;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0il;

    .line 1672809
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 1672810
    if-eqz p2, :cond_3

    .line 1672811
    new-instance v1, LX/5M9;

    invoke-direct {v1, p1}, LX/5M9;-><init>(Lcom/facebook/composer/publish/common/PublishPostParams;)V

    .line 1672812
    iput-boolean v2, v1, LX/5M9;->r:Z

    .line 1672813
    move-object v1, v1

    .line 1672814
    invoke-virtual {v1}, LX/5M9;->a()Lcom/facebook/composer/publish/common/PublishPostParams;

    move-result-object p1

    .line 1672815
    const-string v1, "is_uploading_media"

    invoke-virtual {v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1672816
    :cond_0
    :goto_1
    const-string v1, "publishPostParams"

    invoke-virtual {v3, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1672817
    if-eqz v0, :cond_1

    .line 1672818
    const-string v1, "extra_optimistic_feed_story"

    invoke-static {v3, v1, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1672819
    :cond_1
    move-object v0, v3

    .line 1672820
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 1672821
    :cond_3
    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0jI;

    const/4 p0, 0x1

    .line 1672822
    move-object v4, v2

    check-cast v4, LX/0jF;

    invoke-interface {v4}, LX/0jF;->getPublishMode()LX/5Rn;

    move-result-object v4

    sget-object p2, LX/5Rn;->SCHEDULE_POST:LX/5Rn;

    if-ne v4, p2, :cond_4

    move v4, p0

    .line 1672823
    :goto_2
    move v2, v4

    .line 1672824
    if-eqz v2, :cond_0

    .line 1672825
    const-string v2, "extra_actor_viewer_context"

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0j4;

    invoke-interface {v1}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/ipc/composer/intent/ComposerPageData;

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getPostAsPageViewerContext()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    invoke-virtual {v3, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_1

    :cond_4
    move-object v4, v2

    .line 1672826
    check-cast v4, LX/0j4;

    invoke-interface {v4}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v4

    invoke-static {v4}, LX/2rm;->a(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Z

    move-result v4

    if-eqz v4, :cond_5

    check-cast v2, LX/0j6;

    invoke-interface {v2}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object p2, LX/2rw;->GROUP:LX/2rw;

    if-ne v4, p2, :cond_5

    move v4, p0

    .line 1672827
    goto :goto_2

    .line 1672828
    :cond_5
    const/4 v4, 0x0

    goto :goto_2
.end method

.method public final a()Lcom/facebook/composer/publish/common/PublishPostParams;
    .locals 15

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1672623
    iget-object v0, p0, LX/ARC;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1672624
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0j8;

    invoke-interface {v1}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v1

    if-nez v1, :cond_0

    move-object v1, v0

    check-cast v1, LX/0ik;

    invoke-interface {v1}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/5Qv;

    check-cast v1, LX/2rg;

    invoke-interface {v1}, LX/2rg;->n()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 1672625
    :cond_0
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0j8;

    invoke-interface {v1}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->j()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    move-object v3, v1

    .line 1672626
    :goto_0
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0jD;

    invoke-interface {v1}, LX/0jD;->getTaggedUsers()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/7ky;->a(LX/0Px;)LX/0Px;

    move-result-object v7

    .line 1672627
    iget-object v1, p0, LX/ARC;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    .line 1672628
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0jI;

    check-cast v2, LX/0j3;

    invoke-interface {v2}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v8

    .line 1672629
    new-instance v9, LX/5M9;

    invoke-direct {v9}, LX/5M9;-><init>()V

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0jI;

    check-cast v2, LX/0j0;

    invoke-interface {v2}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v2

    .line 1672630
    iput-object v2, v9, LX/5M9;->G:Ljava/lang/String;

    .line 1672631
    move-object v9, v9

    .line 1672632
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0jI;

    check-cast v2, LX/0j0;

    invoke-interface {v2}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v2

    .line 1672633
    iput-object v2, v9, LX/5M9;->s:Ljava/lang/String;

    .line 1672634
    move-object v2, v9

    .line 1672635
    iget-object v9, p0, LX/ARC;->d:LX/0SG;

    invoke-interface {v9}, LX/0SG;->a()J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    div-long/2addr v10, v12

    .line 1672636
    iput-wide v10, v2, LX/5M9;->u:J

    .line 1672637
    move-object v9, v2

    .line 1672638
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0jI;

    check-cast v2, LX/0j6;

    invoke-interface {v2}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v2

    iget-wide v10, v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    .line 1672639
    iput-wide v10, v9, LX/5M9;->b:J

    .line 1672640
    move-object v9, v9

    .line 1672641
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0jI;

    check-cast v2, LX/0j2;

    invoke-interface {v2}, LX/0j2;->getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {v2}, LX/8oj;->a(LX/175;)Ljava/lang/String;

    move-result-object v2

    .line 1672642
    iput-object v2, v9, LX/5M9;->c:Ljava/lang/String;

    .line 1672643
    move-object v2, v9

    .line 1672644
    iput-object v3, v2, LX/5M9;->d:Ljava/lang/String;

    .line 1672645
    move-object v2, v2

    .line 1672646
    invoke-virtual {v8}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getComposerType()LX/2rt;

    move-result-object v3

    .line 1672647
    iput-object v3, v2, LX/5M9;->q:LX/2rt;

    .line 1672648
    move-object v3, v2

    .line 1672649
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0jI;

    check-cast v2, LX/0j8;

    invoke-interface {v2}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v2

    if-eqz v2, :cond_7

    move v2, v5

    .line 1672650
    :goto_1
    iput-boolean v2, v3, LX/5M9;->t:Z

    .line 1672651
    move-object v2, v3

    .line 1672652
    invoke-virtual {v2, v7}, LX/5M9;->d(LX/0Px;)LX/5M9;

    move-result-object v2

    .line 1672653
    iget-object v3, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, v3

    .line 1672654
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    .line 1672655
    iput-wide v10, v2, LX/5M9;->k:J

    .line 1672656
    move-object v1, v2

    .line 1672657
    invoke-virtual {v8}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getNectarModule()Ljava/lang/String;

    move-result-object v2

    .line 1672658
    iput-object v2, v1, LX/5M9;->n:Ljava/lang/String;

    .line 1672659
    move-object v2, v1

    .line 1672660
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0ix;

    invoke-interface {v1}, LX/0ix;->isUserSelectedTags()Z

    move-result v1

    .line 1672661
    iput-boolean v1, v2, LX/5M9;->w:Z

    .line 1672662
    move-object v1, v2

    .line 1672663
    invoke-virtual {v8}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getPlatformConfiguration()Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->insightsPlatformRef:Ljava/lang/String;

    .line 1672664
    iput-object v2, v1, LX/5M9;->A:Ljava/lang/String;

    .line 1672665
    move-object v2, v1

    .line 1672666
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0j7;

    invoke-interface {v1}, LX/0j7;->getViewerCoordinates()Lcom/facebook/ipc/composer/model/ComposerLocation;

    move-result-object v1

    .line 1672667
    iput-object v1, v2, LX/5M9;->L:Lcom/facebook/ipc/composer/model/ComposerLocation;

    .line 1672668
    move-object v2, v2

    .line 1672669
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0jF;

    invoke-interface {v1}, LX/0jF;->getPublishMode()LX/5Rn;

    move-result-object v1

    .line 1672670
    iput-object v1, v2, LX/5M9;->p:LX/5Rn;

    .line 1672671
    move-object v2, v2

    .line 1672672
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0j8;

    invoke-interface {v1}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->c()Ljava/lang/String;

    move-result-object v1

    .line 1672673
    iput-object v1, v2, LX/5M9;->T:Ljava/lang/String;

    .line 1672674
    move-object v7, v2

    .line 1672675
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0iz;

    invoke-interface {v1}, LX/0iz;->getPublishScheduleTime()Ljava/lang/Long;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0iz;

    invoke-interface {v1}, LX/0iz;->getPublishScheduleTime()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    :goto_2
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v7, v1}, LX/5M9;->a(Ljava/lang/Long;)LX/5M9;

    move-result-object v2

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0jE;

    invoke-interface {v1}, LX/0jE;->getProductItemAttachment()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v1

    .line 1672676
    iput-object v1, v2, LX/5M9;->M:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    .line 1672677
    move-object v2, v2

    .line 1672678
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0iy;

    invoke-interface {v1}, LX/0iy;->getMarketplaceId()J

    move-result-wide v10

    .line 1672679
    iput-wide v10, v2, LX/5M9;->N:J

    .line 1672680
    move-object v1, v2

    .line 1672681
    invoke-virtual {v8}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isThrowbackPost()Z

    move-result v2

    .line 1672682
    iput-boolean v2, v1, LX/5M9;->P:Z

    .line 1672683
    move-object v2, v1

    .line 1672684
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0ip;

    invoke-interface {v1}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Lcom/facebook/ipc/composer/model/MinutiaeTag;

    move-result-object v1

    .line 1672685
    iput-object v1, v2, LX/5M9;->H:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    .line 1672686
    move-object v1, v2

    .line 1672687
    iget-object v2, p0, LX/ARC;->c:LX/0oz;

    invoke-virtual {v2}, LX/0oz;->c()LX/0p3;

    move-result-object v2

    invoke-virtual {v2}, LX/0p3;->name()Ljava/lang/String;

    move-result-object v2

    .line 1672688
    iput-object v2, v1, LX/5M9;->R:Ljava/lang/String;

    .line 1672689
    move-object v2, v1

    .line 1672690
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0j8;

    invoke-interface {v1}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->d()Z

    move-result v1

    .line 1672691
    iput-boolean v1, v2, LX/5M9;->S:Z

    .line 1672692
    move-object v2, v2

    .line 1672693
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0iq;

    invoke-interface {v1}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->a()Ljava/lang/String;

    move-result-object v1

    .line 1672694
    iput-object v1, v2, LX/5M9;->h:Ljava/lang/String;

    .line 1672695
    move-object v2, v2

    .line 1672696
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0ir;

    invoke-interface {v1}, LX/0ir;->getComposerSessionLoggingData()Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    move-result-object v1

    .line 1672697
    iput-object v1, v2, LX/5M9;->i:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    .line 1672698
    move-object v1, v2

    .line 1672699
    iput-boolean v5, v1, LX/5M9;->K:Z

    .line 1672700
    move-object v2, v1

    .line 1672701
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0j8;

    invoke-interface {v1}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->b()Z

    move-result v1

    .line 1672702
    iput-boolean v1, v2, LX/5M9;->v:Z

    .line 1672703
    move-object v1, v2

    .line 1672704
    invoke-virtual {v8}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getLaunchLoggingParams()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->getSourceSurface()LX/21D;

    move-result-object v2

    invoke-virtual {v2}, LX/21D;->getAnalyticsName()Ljava/lang/String;

    move-result-object v2

    .line 1672705
    iput-object v2, v1, LX/5M9;->U:Ljava/lang/String;

    .line 1672706
    move-object v1, v1

    .line 1672707
    iget-object v2, p0, LX/ARC;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0il;

    .line 1672708
    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0jI;

    check-cast v3, LX/0jB;

    invoke-interface {v3}, LX/0jB;->getReferencedStickerData()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v3

    if-nez v3, :cond_d

    .line 1672709
    const/4 v2, 0x0

    .line 1672710
    :goto_3
    move-object v2, v2

    .line 1672711
    iput-object v2, v1, LX/5M9;->J:Ljava/lang/String;

    .line 1672712
    move-object v2, v1

    .line 1672713
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0iu;

    invoke-interface {v1}, LX/0iu;->isBackoutDraft()Z

    move-result v1

    .line 1672714
    iput-boolean v1, v2, LX/5M9;->V:Z

    .line 1672715
    move-object v2, v2

    .line 1672716
    move-object v1, v0

    check-cast v1, LX/0ik;

    invoke-interface {v1}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/5Qv;

    invoke-interface {v1}, LX/5Qv;->i()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1672717
    iget-object v1, p0, LX/ARC;->g:LX/0ad;

    sget-short v3, LX/1aO;->ar:S

    const/4 v7, 0x0

    invoke-interface {v1, v3, v7}, LX/0ad;->a(SZ)Z

    move-result v1

    move v1, v1

    .line 1672718
    if-eqz v1, :cond_9

    move v1, v5

    .line 1672719
    :goto_4
    iput-boolean v1, v2, LX/5M9;->W:Z

    .line 1672720
    move-object v2, v2

    .line 1672721
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0j5;

    invoke-interface {v1}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v1

    if-eqz v1, :cond_a

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0j5;

    invoke-interface {v1}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->reshareContext:Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    if-eqz v1, :cond_a

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0j5;

    invoke-interface {v1}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->reshareContext:Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->shouldIncludeReshareContext()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1672722
    :goto_5
    iput-boolean v5, v2, LX/5M9;->Q:Z

    .line 1672723
    move-object v2, v2

    .line 1672724
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0iw;

    invoke-interface {v1}, LX/0iw;->isFeedOnlyPost()Z

    move-result v1

    .line 1672725
    iput-boolean v1, v2, LX/5M9;->ah:Z

    .line 1672726
    move-object v2, v2

    .line 1672727
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0jG;

    invoke-interface {v1}, LX/0jG;->r()Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    move-result-object v1

    if-eqz v1, :cond_b

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0jG;

    invoke-interface {v1}, LX/0jG;->r()Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    move-result-object v1

    .line 1672728
    iget-object v3, v1, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->a:Ljava/lang/String;

    move-object v1, v3

    .line 1672729
    :goto_6
    iput-object v1, v2, LX/5M9;->g:Ljava/lang/String;

    .line 1672730
    move-object v2, v2

    .line 1672731
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0j9;

    invoke-interface {v1}, LX/0j9;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0j9;

    invoke-interface {v1}, LX/0j9;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->a(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v4

    .line 1672732
    :cond_1
    iput-object v4, v2, LX/5M9;->aj:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    .line 1672733
    move-object v2, v2

    .line 1672734
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0jH;

    invoke-interface {v1}, LX/0jH;->getPromptAnalytics()Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-result-object v1

    .line 1672735
    iput-object v1, v2, LX/5M9;->ag:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 1672736
    move-object v2, v2

    .line 1672737
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0j3;

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isGroupMemberBioPost()Z

    move-result v1

    .line 1672738
    iput-boolean v1, v2, LX/5M9;->ak:Z

    .line 1672739
    move-object v1, v2

    .line 1672740
    invoke-virtual {v8}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isPlacelistPost()Z

    move-result v2

    .line 1672741
    iput-boolean v2, v1, LX/5M9;->ao:Z

    .line 1672742
    move-object v2, v1

    .line 1672743
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0j5;

    invoke-interface {v1}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v1

    .line 1672744
    if-eqz v1, :cond_4

    .line 1672745
    iget-boolean v3, v1, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->isMemeShare:Z

    .line 1672746
    iput-boolean v3, v2, LX/5M9;->ai:Z

    .line 1672747
    iget-object v3, v1, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    if-eqz v3, :cond_2

    .line 1672748
    iget-object v3, v1, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    .line 1672749
    iput-object v3, v2, LX/5M9;->j:Ljava/lang/String;

    .line 1672750
    move-object v3, v2

    .line 1672751
    invoke-virtual {v8}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getPlatformConfiguration()Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->nameForShareLink:Ljava/lang/String;

    .line 1672752
    iput-object v4, v3, LX/5M9;->B:Ljava/lang/String;

    .line 1672753
    move-object v3, v3

    .line 1672754
    invoke-virtual {v8}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getPlatformConfiguration()Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->captionForShareLink:Ljava/lang/String;

    .line 1672755
    iput-object v4, v3, LX/5M9;->C:Ljava/lang/String;

    .line 1672756
    move-object v3, v3

    .line 1672757
    invoke-virtual {v8}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getPlatformConfiguration()Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->descriptionForShareLink:Ljava/lang/String;

    .line 1672758
    iput-object v4, v3, LX/5M9;->D:Ljava/lang/String;

    .line 1672759
    move-object v3, v3

    .line 1672760
    invoke-virtual {v8}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getPlatformConfiguration()Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->pictureForShareLink:Ljava/lang/String;

    .line 1672761
    iput-object v4, v3, LX/5M9;->F:Ljava/lang/String;

    .line 1672762
    :cond_2
    iget-object v3, v1, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->quoteText:Ljava/lang/String;

    if-eqz v3, :cond_3

    .line 1672763
    iget-object v3, v1, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->quoteText:Ljava/lang/String;

    .line 1672764
    iput-object v3, v2, LX/5M9;->E:Ljava/lang/String;

    .line 1672765
    :cond_3
    iget-object v3, v1, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->shareable:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 1672766
    iput-object v3, v2, LX/5M9;->l:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 1672767
    move-object v3, v2

    .line 1672768
    iget-object v1, v1, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->shareTracking:Ljava/lang/String;

    .line 1672769
    iput-object v1, v3, LX/5M9;->m:Ljava/lang/String;

    .line 1672770
    :cond_4
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    check-cast v1, LX/0j3;

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialComposerCallToAction()Lcom/facebook/ipc/composer/model/ComposerCallToAction;

    move-result-object v1

    .line 1672771
    if-eqz v1, :cond_5

    .line 1672772
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->getLink()Ljava/lang/String;

    move-result-object v3

    .line 1672773
    iput-object v3, v2, LX/5M9;->j:Ljava/lang/String;

    .line 1672774
    move-object v3, v2

    .line 1672775
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->getTitle()Ljava/lang/String;

    move-result-object v4

    .line 1672776
    iput-object v4, v3, LX/5M9;->B:Ljava/lang/String;

    .line 1672777
    move-object v3, v3

    .line 1672778
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->getLink()Ljava/lang/String;

    move-result-object v4

    .line 1672779
    iput-object v4, v3, LX/5M9;->am:Ljava/lang/String;

    .line 1672780
    move-object v3, v3

    .line 1672781
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->getCallToActionType()Ljava/lang/String;

    move-result-object v4

    .line 1672782
    iput-object v4, v3, LX/5M9;->an:Ljava/lang/String;

    .line 1672783
    move-object v3, v3

    .line 1672784
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->getLinkImage()Ljava/lang/String;

    move-result-object v1

    .line 1672785
    iput-object v1, v3, LX/5M9;->F:Ljava/lang/String;

    .line 1672786
    :cond_5
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    invoke-interface {v1}, LX/0jI;->getAppAttribution()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 1672787
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    invoke-interface {v1}, LX/0jI;->getAppAttribution()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/share/model/ComposerAppAttribution;->b()Ljava/lang/String;

    move-result-object v1

    .line 1672788
    iput-object v1, v2, LX/5M9;->y:Ljava/lang/String;

    .line 1672789
    move-object v3, v2

    .line 1672790
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jI;

    invoke-interface {v1}, LX/0jI;->getAppAttribution()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/share/model/ComposerAppAttribution;->a()Ljava/lang/String;

    move-result-object v1

    .line 1672791
    iput-object v1, v3, LX/5M9;->x:Ljava/lang/String;

    .line 1672792
    move-object v1, v3

    .line 1672793
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jI;

    invoke-interface {v0}, LX/0jI;->getAppAttribution()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/share/model/ComposerAppAttribution;->c()Ljava/lang/String;

    move-result-object v0

    .line 1672794
    iput-object v0, v1, LX/5M9;->z:Ljava/lang/String;

    .line 1672795
    :cond_6
    invoke-virtual {v2}, LX/5M9;->a()Lcom/facebook/composer/publish/common/PublishPostParams;

    move-result-object v0

    return-object v0

    :cond_7
    move v2, v6

    .line 1672796
    goto/16 :goto_1

    :cond_8
    const-wide/16 v2, 0x0

    goto/16 :goto_2

    :cond_9
    move v1, v6

    goto/16 :goto_4

    :cond_a
    move v5, v6

    goto/16 :goto_5

    :cond_b
    move-object v1, v4

    goto/16 :goto_6

    :cond_c
    move-object v3, v4

    goto/16 :goto_0

    :cond_d
    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0jI;

    check-cast v2, LX/0jB;

    invoke-interface {v2}, LX/0jB;->getReferencedStickerData()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getStickerId()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3
.end method

.method public final b()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 1672797
    invoke-virtual {p0}, LX/ARC;->a()Lcom/facebook/composer/publish/common/PublishPostParams;

    move-result-object v0

    .line 1672798
    iget-object v1, p0, LX/ARC;->e:LX/AR3;

    invoke-virtual {v1}, LX/AR3;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1672799
    const/4 v1, 0x0

    .line 1672800
    :goto_0
    move v1, v1

    .line 1672801
    invoke-virtual {p0, v0, v1}, LX/ARC;->a(Lcom/facebook/composer/publish/common/PublishPostParams;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    .line 1672802
    :cond_0
    iget-object v1, p0, LX/ARC;->e:LX/AR3;

    .line 1672803
    invoke-virtual {v1}, LX/AR3;->c()Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/AR3;->a(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1672804
    iget-object v1, p0, LX/ARC;->f:LX/AR1;

    invoke-virtual {v1}, LX/AR1;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1672805
    iget-object v1, p0, LX/ARC;->e:LX/AR3;

    invoke-virtual {v1}, LX/AR3;->e()V

    .line 1672806
    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method
