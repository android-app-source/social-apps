.class public final LX/AOq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6ak;


# instance fields
.field public final synthetic a:Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;


# direct methods
.method public constructor <init>(Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;)V
    .locals 0

    .prologue
    .line 1669486
    iput-object p1, p0, LX/AOq;->a:Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/6ax;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1669487
    iget-object v0, p0, LX/AOq;->a:Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->l:LX/0Ri;

    invoke-interface {v0, p1}, LX/0Ri;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlaceListItem;

    .line 1669488
    iget-object v2, p0, LX/AOq;->a:Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;

    invoke-static {v2, v0}, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->d(Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;Lcom/facebook/graphql/model/GraphQLPlaceListItem;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1669489
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v2

    .line 1669490
    iget-object v3, p0, LX/AOq;->a:Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;

    iget-object v3, v3, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->a:LX/8yJ;

    iget-object v4, p0, LX/AOq;->a:Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;

    iget-object v4, v4, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->d:Ljava/lang/String;

    invoke-virtual {v3, v4, v2, v1}, LX/8yJ;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1669491
    iget-object v1, p0, LX/AOq;->a:Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;

    invoke-static {v1, v0}, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->e(Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;Lcom/facebook/graphql/model/GraphQLPlaceListItem;)V

    .line 1669492
    const/4 v0, 0x1

    .line 1669493
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method
