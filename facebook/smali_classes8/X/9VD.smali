.class public LX/9VD;
.super LX/9VC;
.source ""

# interfaces
.implements Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# instance fields
.field public final c:Landroid/graphics/SurfaceTexture;

.field public final d:I

.field public e:Z

.field public final f:[F


# direct methods
.method public constructor <init>(LX/9VA;II)V
    .locals 6

    .prologue
    .line 1499132
    invoke-direct {p0, p1}, LX/9VC;-><init>(LX/9VA;)V

    .line 1499133
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, LX/9VD;->f:[F

    .line 1499134
    const p1, 0x812f

    const/4 v5, 0x1

    const v4, 0x46180400    # 9729.0f

    const/4 v3, 0x0

    const v2, 0x8d65

    .line 1499135
    new-array v0, v5, [I

    const/4 v1, -0x1

    aput v1, v0, v3

    .line 1499136
    invoke-static {v5, v0, v3}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    .line 1499137
    const-string v1, "glGenTextures"

    invoke-static {v1}, LX/9VA;->a(Ljava/lang/String;)V

    .line 1499138
    aget v1, v0, v3

    invoke-static {v2, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 1499139
    const/16 v1, 0x2801

    invoke-static {v2, v1, v4}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    .line 1499140
    const/16 v1, 0x2800

    invoke-static {v2, v1, v4}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    .line 1499141
    const/16 v1, 0x2802

    invoke-static {v2, v1, p1}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 1499142
    const/16 v1, 0x2803

    invoke-static {v2, v1, p1}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 1499143
    aget v0, v0, v3

    move v0, v0

    .line 1499144
    iput v0, p0, LX/9VD;->d:I

    .line 1499145
    new-instance v0, Landroid/graphics/SurfaceTexture;

    iget v1, p0, LX/9VD;->d:I

    invoke-direct {v0, v1}, Landroid/graphics/SurfaceTexture;-><init>(I)V

    iput-object v0, p0, LX/9VD;->c:Landroid/graphics/SurfaceTexture;

    .line 1499146
    iget-object v0, p0, LX/9VD;->c:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0, p2, p3}, Landroid/graphics/SurfaceTexture;->setDefaultBufferSize(II)V

    .line 1499147
    iget-object v0, p0, LX/9VD;->c:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0, p0}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    .line 1499148
    new-instance v0, Landroid/view/Surface;

    iget-object v1, p0, LX/9VD;->c:Landroid/graphics/SurfaceTexture;

    invoke-direct {v0, v1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object v0, p0, LX/9VD;->b:Landroid/view/Surface;

    .line 1499149
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 3

    .prologue
    .line 1499150
    iget-object v0, p0, LX/9VC;->b:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 1499151
    iget-object v0, p0, LX/9VD;->c:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->release()V

    .line 1499152
    iget-object v0, p0, LX/9VC;->b:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 1499153
    iget v0, p0, LX/9VD;->d:I

    const/4 p0, 0x1

    const/4 v2, 0x0

    .line 1499154
    new-array v1, p0, [I

    aput v0, v1, v2

    invoke-static {p0, v1, v2}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    .line 1499155
    return-void
.end method

.method public final onFrameAvailable(Landroid/graphics/SurfaceTexture;)V
    .locals 1

    .prologue
    .line 1499156
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/9VD;->e:Z

    .line 1499157
    return-void
.end method
