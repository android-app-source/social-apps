.class public LX/9ed;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/9ec;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1520112
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1520113
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Lcom/facebook/photos/creativeediting/RotatingFrameLayout;Lcom/facebook/photos/editgallery/EditableOverlayContainerView;Lcom/facebook/drawee/fbpipeline/FbDraweeView;Landroid/view/View;LX/9f4;Ljava/lang/String;Lcom/facebook/photos/editgallery/EditGalleryFragmentController;LX/0am;Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;)LX/9ec;
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Lcom/facebook/photos/creativeediting/RotatingFrameLayout;",
            "Lcom/facebook/photos/editgallery/EditableOverlayContainerView;",
            "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
            "Landroid/view/View;",
            "Lcom/facebook/photos/editgallery/EditGalleryFragmentController$FileEditingListener;",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/editgallery/RotatingPhotoViewController;",
            "LX/0am",
            "<",
            "LX/9c7;",
            ">;",
            "Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;",
            ")",
            "LX/9ec;"
        }
    .end annotation

    .prologue
    .line 1520114
    new-instance v1, LX/9ec;

    invoke-static/range {p0 .. p0}, LX/0kL;->a(LX/0QB;)LX/0kL;

    move-result-object v12

    check-cast v12, LX/0kL;

    const-class v2, LX/9cH;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/9cH;

    const/16 v2, 0x2e40

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v14

    invoke-static/range {p0 .. p0}, LX/8GN;->a(LX/0QB;)LX/8GN;

    move-result-object v15

    check-cast v15, LX/8GN;

    invoke-static/range {p0 .. p0}, LX/9cK;->a(LX/0QB;)LX/9cK;

    move-result-object v16

    check-cast v16, LX/9cK;

    invoke-static/range {p0 .. p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v17

    check-cast v17, LX/0TD;

    invoke-static/range {p0 .. p0}, LX/8GT;->a(LX/0QB;)LX/8GT;

    move-result-object v18

    check-cast v18, LX/8GT;

    const/16 v2, 0x2e3d

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v19

    const/16 v2, 0x12b1

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v20

    invoke-static/range {p0 .. p0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v21

    check-cast v21, LX/1HI;

    const-class v2, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Landroid/content/Context;

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v1 .. v22}, LX/9ec;-><init>(Landroid/net/Uri;Lcom/facebook/photos/creativeediting/RotatingFrameLayout;Lcom/facebook/photos/editgallery/EditableOverlayContainerView;Lcom/facebook/drawee/fbpipeline/FbDraweeView;Landroid/view/View;LX/9f4;Ljava/lang/String;Lcom/facebook/photos/editgallery/EditGalleryFragmentController;LX/0am;Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;LX/0kL;LX/9cH;LX/0Or;LX/8GN;LX/9cK;LX/0TD;LX/8GT;LX/0Ot;LX/0Ot;LX/1HI;Landroid/content/Context;)V

    .line 1520115
    return-object v1
.end method
