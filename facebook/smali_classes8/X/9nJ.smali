.class public final LX/9nJ;
.super LX/5p8;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/5p8",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/react/bridge/Callback;

.field public final synthetic b:LX/5pC;

.field public final synthetic c:LX/9nO;


# direct methods
.method public constructor <init>(LX/9nO;LX/5pX;Lcom/facebook/react/bridge/Callback;LX/5pC;)V
    .locals 0

    .prologue
    .line 1537012
    iput-object p1, p0, LX/9nJ;->c:LX/9nO;

    iput-object p3, p0, LX/9nJ;->a:Lcom/facebook/react/bridge/Callback;

    iput-object p4, p0, LX/9nJ;->b:LX/5pC;

    invoke-direct {p0, p2}, LX/5p8;-><init>(LX/5pX;)V

    return-void
.end method

.method private varargs a()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1537013
    iget-object v0, p0, LX/9nJ;->c:LX/9nO;

    invoke-static {v0}, LX/9nO;->h(LX/9nO;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1537014
    iget-object v0, p0, LX/9nJ;->a:Lcom/facebook/react/bridge/Callback;

    new-array v1, v7, [Ljava/lang/Object;

    invoke-static {v2}, LX/9nH;->c(Ljava/lang/String;)LX/5pH;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 1537015
    :cond_0
    :goto_0
    return-void

    .line 1537016
    :cond_1
    const-string v0, "INSERT OR REPLACE INTO catalystLocalStorage VALUES (?, ?);"

    .line 1537017
    iget-object v1, p0, LX/9nJ;->c:LX/9nO;

    iget-object v1, v1, LX/9nO;->a:LX/9nP;

    invoke-virtual {v1}, LX/9nP;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v1

    .line 1537018
    :try_start_0
    iget-object v0, p0, LX/9nJ;->c:LX/9nO;

    iget-object v0, v0, LX/9nO;->a:LX/9nP;

    invoke-virtual {v0}, LX/9nP;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const v4, 0x6202bc22

    invoke-static {v0, v4}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    move v0, v3

    .line 1537019
    :goto_1
    iget-object v4, p0, LX/9nJ;->b:LX/5pC;

    invoke-interface {v4}, LX/5pC;->size()I

    move-result v4

    if-ge v0, v4, :cond_5

    .line 1537020
    iget-object v4, p0, LX/9nJ;->b:LX/5pC;

    invoke-interface {v4, v0}, LX/5pC;->b(I)LX/5pC;

    move-result-object v4

    invoke-interface {v4}, LX/5pC;->size()I

    move-result v4

    if-eq v4, v8, :cond_2

    .line 1537021
    const/4 v0, 0x0

    invoke-static {v0}, LX/9nH;->b(Ljava/lang/String;)LX/5pH;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1537022
    :try_start_1
    iget-object v0, p0, LX/9nJ;->c:LX/9nO;

    iget-object v0, v0, LX/9nO;->a:LX/9nP;

    invoke-virtual {v0}, LX/9nP;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const v3, 0x46067c15

    invoke-static {v0, v3}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1537023
    :catch_0
    move-exception v0

    .line 1537024
    const-string v3, "React"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1537025
    if-nez v1, :cond_0

    .line 1537026
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, LX/9nH;->a(Ljava/lang/String;Ljava/lang/String;)LX/5pH;

    goto :goto_0

    .line 1537027
    :cond_2
    :try_start_2
    iget-object v4, p0, LX/9nJ;->b:LX/5pC;

    invoke-interface {v4, v0}, LX/5pC;->b(I)LX/5pC;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, LX/5pC;->getString(I)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_3

    .line 1537028
    const/4 v0, 0x0

    invoke-static {v0}, LX/9nH;->a(Ljava/lang/String;)LX/5pH;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v1

    .line 1537029
    :try_start_3
    iget-object v0, p0, LX/9nJ;->c:LX/9nO;

    iget-object v0, v0, LX/9nO;->a:LX/9nP;

    invoke-virtual {v0}, LX/9nP;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const v3, 0xa4a321b

    invoke-static {v0, v3}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 1537030
    :catch_1
    move-exception v0

    .line 1537031
    const-string v3, "React"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1537032
    if-nez v1, :cond_0

    .line 1537033
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, LX/9nH;->a(Ljava/lang/String;Ljava/lang/String;)LX/5pH;

    goto/16 :goto_0

    .line 1537034
    :cond_3
    :try_start_4
    iget-object v4, p0, LX/9nJ;->b:LX/5pC;

    invoke-interface {v4, v0}, LX/5pC;->b(I)LX/5pC;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v4, v5}, LX/5pC;->getString(I)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_4

    .line 1537035
    const/4 v0, 0x0

    invoke-static {v0}, LX/9nH;->b(Ljava/lang/String;)LX/5pH;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v1

    .line 1537036
    :try_start_5
    iget-object v0, p0, LX/9nJ;->c:LX/9nO;

    iget-object v0, v0, LX/9nO;->a:LX/9nP;

    invoke-virtual {v0}, LX/9nP;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const v3, -0xa88466b

    invoke-static {v0, v3}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    goto/16 :goto_0

    .line 1537037
    :catch_2
    move-exception v0

    .line 1537038
    const-string v3, "React"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1537039
    if-nez v1, :cond_0

    .line 1537040
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, LX/9nH;->a(Ljava/lang/String;Ljava/lang/String;)LX/5pH;

    goto/16 :goto_0

    .line 1537041
    :cond_4
    :try_start_6
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 1537042
    const/4 v4, 0x1

    iget-object v5, p0, LX/9nJ;->b:LX/5pC;

    invoke-interface {v5, v0}, LX/5pC;->b(I)LX/5pC;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v5, v6}, LX/5pC;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 1537043
    const/4 v4, 0x2

    iget-object v5, p0, LX/9nJ;->b:LX/5pC;

    invoke-interface {v5, v0}, LX/5pC;->b(I)LX/5pC;

    move-result-object v5

    const/4 v6, 0x1

    invoke-interface {v5, v6}, LX/5pC;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 1537044
    const v4, -0x4f664741

    invoke-static {v4}, LX/03h;->a(I)V

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    const v4, -0x1ff181bf

    invoke-static {v4}, LX/03h;->a(I)V

    .line 1537045
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    .line 1537046
    :cond_5
    iget-object v0, p0, LX/9nJ;->c:LX/9nO;

    iget-object v0, v0, LX/9nO;->a:LX/9nP;

    invoke-virtual {v0}, LX/9nP;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 1537047
    :try_start_7
    iget-object v0, p0, LX/9nJ;->c:LX/9nO;

    iget-object v0, v0, LX/9nO;->a:LX/9nP;

    invoke-virtual {v0}, LX/9nP;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const v1, -0x510cde90

    invoke-static {v0, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3

    move-object v0, v2

    .line 1537048
    :cond_6
    :goto_2
    if-eqz v0, :cond_7

    .line 1537049
    iget-object v1, p0, LX/9nJ;->a:Lcom/facebook/react/bridge/Callback;

    new-array v2, v7, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-interface {v1, v2}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1537050
    :catch_3
    move-exception v0

    .line 1537051
    const-string v1, "React"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4, v0}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1537052
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, LX/9nH;->a(Ljava/lang/String;Ljava/lang/String;)LX/5pH;

    move-result-object v0

    goto :goto_2

    .line 1537053
    :catch_4
    move-exception v0

    .line 1537054
    :try_start_8
    const-string v1, "React"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4, v0}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1537055
    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/9nH;->a(Ljava/lang/String;Ljava/lang/String;)LX/5pH;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move-result-object v0

    .line 1537056
    :try_start_9
    iget-object v1, p0, LX/9nJ;->c:LX/9nO;

    iget-object v1, v1, LX/9nO;->a:LX/9nP;

    invoke-virtual {v1}, LX/9nP;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const v4, 0x508a53fd

    invoke-static {v1, v4}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_5

    goto :goto_2

    .line 1537057
    :catch_5
    move-exception v1

    .line 1537058
    const-string v4, "React"

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1537059
    if-nez v0, :cond_6

    .line 1537060
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, LX/9nH;->a(Ljava/lang/String;Ljava/lang/String;)LX/5pH;

    move-result-object v0

    goto :goto_2

    .line 1537061
    :catchall_0
    move-exception v0

    .line 1537062
    :try_start_a
    iget-object v1, p0, LX/9nJ;->c:LX/9nO;

    iget-object v1, v1, LX/9nO;->a:LX/9nP;

    invoke-virtual {v1}, LX/9nP;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const v3, -0x284c609

    invoke-static {v1, v3}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_6

    .line 1537063
    :goto_3
    throw v0

    .line 1537064
    :catch_6
    move-exception v1

    .line 1537065
    const-string v3, "React"

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1537066
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, LX/9nH;->a(Ljava/lang/String;Ljava/lang/String;)LX/5pH;

    goto :goto_3

    .line 1537067
    :cond_7
    iget-object v0, p0, LX/9nJ;->a:Lcom/facebook/react/bridge/Callback;

    new-array v1, v3, [Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public final bridge synthetic a([Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1537068
    invoke-direct {p0}, LX/9nJ;->a()V

    return-void
.end method
