.class public final LX/96Z;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 1439214
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_8

    .line 1439215
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1439216
    :goto_0
    return v1

    .line 1439217
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1439218
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_7

    .line 1439219
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1439220
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1439221
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_1

    if-eqz v6, :cond_1

    .line 1439222
    const-string v7, "crowdsourcing_place_question_value"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1439223
    invoke-static {p0, p1}, LX/96U;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1439224
    :cond_2
    const-string v7, "id"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1439225
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 1439226
    :cond_3
    const-string v7, "place_question_answers"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1439227
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1439228
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->START_ARRAY:LX/15z;

    if-ne v6, v7, :cond_4

    .line 1439229
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_ARRAY:LX/15z;

    if-eq v6, v7, :cond_4

    .line 1439230
    invoke-static {p0, p1}, LX/A82;->b(LX/15w;LX/186;)I

    move-result v6

    .line 1439231
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1439232
    :cond_4
    invoke-static {v3, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v3

    move v3, v3

    .line 1439233
    goto :goto_1

    .line 1439234
    :cond_5
    const-string v7, "place_question_subtext"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 1439235
    invoke-static {p0, p1}, LX/96X;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1439236
    :cond_6
    const-string v7, "place_question_text"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1439237
    invoke-static {p0, p1}, LX/96Y;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1439238
    :cond_7
    const/4 v6, 0x5

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1439239
    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1439240
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1439241
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1439242
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1439243
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1439244
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_8
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1439245
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1439246
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1439247
    if-eqz v0, :cond_0

    .line 1439248
    const-string v1, "crowdsourcing_place_question_value"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1439249
    invoke-static {p0, v0, p2}, LX/96U;->a(LX/15i;ILX/0nX;)V

    .line 1439250
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1439251
    if-eqz v0, :cond_1

    .line 1439252
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1439253
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1439254
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1439255
    if-eqz v0, :cond_3

    .line 1439256
    const-string v1, "place_question_answers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1439257
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1439258
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 1439259
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/96W;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1439260
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1439261
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1439262
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1439263
    if-eqz v0, :cond_4

    .line 1439264
    const-string v1, "place_question_subtext"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1439265
    invoke-static {p0, v0, p2}, LX/96X;->a(LX/15i;ILX/0nX;)V

    .line 1439266
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1439267
    if-eqz v0, :cond_5

    .line 1439268
    const-string v1, "place_question_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1439269
    invoke-static {p0, v0, p2}, LX/96Y;->a(LX/15i;ILX/0nX;)V

    .line 1439270
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1439271
    return-void
.end method
