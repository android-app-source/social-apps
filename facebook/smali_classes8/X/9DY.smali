.class public final LX/9DY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/9Da;


# direct methods
.method public constructor <init>(LX/9Da;)V
    .locals 0

    .prologue
    .line 1455737
    iput-object p1, p0, LX/9DY;->a:LX/9Da;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1455738
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1455739
    check-cast p1, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;

    .line 1455740
    iget-object v0, p0, LX/9DY;->a:LX/9Da;

    iget-object v0, v0, LX/9Da;->c:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$5$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/feedback/ui/FeedbackGraphQLSubscriber$5$1;-><init>(LX/9DY;Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;)V

    const v2, -0x3255d191

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1455741
    return-void
.end method
