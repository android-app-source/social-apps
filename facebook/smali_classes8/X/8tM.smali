.class public LX/8tM;
.super LX/25A;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/25A",
        "<",
        "LX/8tL;",
        ">;"
    }
.end annotation


# instance fields
.field private f:LX/2MV;

.field private g:Landroid/net/Uri;

.field private h:LX/8tL;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/2MV;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 1412958
    invoke-direct {p0, p1}, LX/25A;-><init>(Landroid/content/Context;)V

    .line 1412959
    iput-object p2, p0, LX/8tM;->f:LX/2MV;

    .line 1412960
    iput-object p3, p0, LX/8tM;->g:Landroid/net/Uri;

    .line 1412961
    return-void
.end method

.method private a(LX/8tL;)V
    .locals 0

    .prologue
    .line 1412954
    iput-object p1, p0, LX/8tM;->h:LX/8tL;

    .line 1412955
    invoke-super {p0, p1}, LX/25A;->b(Ljava/lang/Object;)V

    .line 1412956
    return-void
.end method


# virtual methods
.method public final synthetic b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1412957
    check-cast p1, LX/8tL;

    invoke-direct {p0, p1}, LX/8tM;->a(LX/8tL;)V

    return-void
.end method

.method public final d()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1412950
    :try_start_0
    new-instance v0, LX/8tL;

    iget-object v1, p0, LX/8tM;->f:LX/2MV;

    iget-object v2, p0, LX/8tM;->g:Landroid/net/Uri;

    invoke-interface {v1, v2}, LX/2MV;->a(Landroid/net/Uri;)LX/60x;

    move-result-object v1

    invoke-direct {v0, v1}, LX/8tL;-><init>(LX/60x;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1412951
    :goto_0
    return-object v0

    .line 1412952
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 1412953
    new-instance v0, LX/8tL;

    invoke-direct {v0, v1}, LX/8tL;-><init>(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 1412945
    iget-object v0, p0, LX/8tM;->h:LX/8tL;

    if-eqz v0, :cond_0

    .line 1412946
    iget-object v0, p0, LX/8tM;->h:LX/8tL;

    invoke-direct {p0, v0}, LX/8tM;->a(LX/8tL;)V

    .line 1412947
    :goto_0
    return-void

    .line 1412948
    :cond_0
    invoke-virtual {p0}, LX/0k9;->a()V

    .line 1412949
    goto :goto_0
.end method
