.class public LX/8wj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0aG;


# direct methods
.method public constructor <init>(LX/0aG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1422893
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1422894
    iput-object p1, p0, LX/8wj;->a:LX/0aG;

    .line 1422895
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1422896
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1422897
    new-instance v0, LX/8we;

    invoke-direct {v0}, LX/8we;-><init>()V

    .line 1422898
    iput-object p1, v0, LX/8we;->a:Ljava/lang/String;

    .line 1422899
    move-object v0, v0

    .line 1422900
    new-instance v1, Lcom/facebook/api/negative_feedback/NegativeFeedbackActionMethod$Params;

    invoke-direct {v1, v0}, Lcom/facebook/api/negative_feedback/NegativeFeedbackActionMethod$Params;-><init>(LX/8we;)V

    move-object v0, v1

    .line 1422901
    const-string v1, "negativeFeedbackActionParams"

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1422902
    iget-object v0, p0, LX/8wj;->a:LX/0aG;

    const-string v1, "negative_feedback_actions"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const/4 v4, 0x0

    const v5, 0x754b5453

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    return-object v0
.end method
