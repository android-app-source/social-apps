.class public final LX/AKX;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;)LX/15i;
    .locals 15

    .prologue
    .line 1663855
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1663856
    const/4 v9, 0x0

    .line 1663857
    const/4 v8, 0x0

    .line 1663858
    const/4 v5, 0x0

    .line 1663859
    const-wide/16 v6, 0x0

    .line 1663860
    const/4 v4, 0x0

    .line 1663861
    const/4 v3, 0x0

    .line 1663862
    const/4 v2, 0x0

    .line 1663863
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v10, v11, :cond_9

    .line 1663864
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1663865
    const/4 v2, 0x0

    .line 1663866
    :goto_0
    move v1, v2

    .line 1663867
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1663868
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    return-object v0

    .line 1663869
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v11, :cond_6

    .line 1663870
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1663871
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1663872
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_0

    if-eqz v6, :cond_0

    .line 1663873
    const-string v11, "id"

    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 1663874
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    move v10, v6

    goto :goto_1

    .line 1663875
    :cond_1
    const-string v11, "is_seen_by_viewer"

    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 1663876
    const/4 v3, 0x1

    .line 1663877
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v6

    move v9, v6

    goto :goto_1

    .line 1663878
    :cond_2
    const-string v11, "seen_direct_users"

    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1663879
    invoke-static {p0, v0}, LX/AKW;->a(LX/15w;LX/186;)I

    move-result v6

    move v7, v6

    goto :goto_1

    .line 1663880
    :cond_3
    const-string v11, "time"

    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 1663881
    const/4 v2, 0x1

    .line 1663882
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v4

    goto :goto_1

    .line 1663883
    :cond_4
    const-string v11, "url"

    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1663884
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    move v8, v6

    goto :goto_1

    .line 1663885
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1663886
    :cond_6
    const/4 v6, 0x5

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 1663887
    const/4 v6, 0x0

    invoke-virtual {v0, v6, v10}, LX/186;->b(II)V

    .line 1663888
    if-eqz v3, :cond_7

    .line 1663889
    const/4 v3, 0x1

    invoke-virtual {v0, v3, v9}, LX/186;->a(IZ)V

    .line 1663890
    :cond_7
    const/4 v3, 0x2

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 1663891
    if-eqz v2, :cond_8

    .line 1663892
    const/4 v3, 0x3

    const-wide/16 v6, 0x0

    move-object v2, v0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1663893
    :cond_8
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1663894
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_9
    move v10, v9

    move v9, v8

    move v8, v4

    move-wide v13, v6

    move v7, v5

    move-wide v4, v13

    goto/16 :goto_1
.end method
