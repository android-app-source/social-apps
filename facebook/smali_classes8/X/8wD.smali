.class public final LX/8wD;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:I


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .prologue
    .line 1422046
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1422047
    iput p1, p0, LX/8wD;->a:I

    .line 1422048
    iput p2, p0, LX/8wD;->b:I

    .line 1422049
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 1422045
    iget v0, p0, LX/8wD;->b:I

    iget v1, p0, LX/8wD;->a:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public final b()LX/8wD;
    .locals 3

    .prologue
    .line 1422050
    new-instance v0, LX/8wD;

    iget v1, p0, LX/8wD;->b:I

    iget v2, p0, LX/8wD;->a:I

    invoke-direct {v0, v1, v2}, LX/8wD;-><init>(II)V

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1422036
    if-ne p0, p1, :cond_1

    .line 1422037
    :cond_0
    :goto_0
    return v0

    .line 1422038
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1422039
    goto :goto_0

    .line 1422040
    :cond_3
    check-cast p1, LX/8wD;

    .line 1422041
    iget v2, p0, LX/8wD;->b:I

    iget v3, p1, LX/8wD;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1422042
    goto :goto_0

    .line 1422043
    :cond_4
    iget v2, p0, LX/8wD;->a:I

    iget v3, p1, LX/8wD;->a:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1422044
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1422033
    iget v0, p0, LX/8wD;->a:I

    .line 1422034
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LX/8wD;->b:I

    add-int/2addr v0, v1

    .line 1422035
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1422032
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, LX/8wD;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/8wD;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
