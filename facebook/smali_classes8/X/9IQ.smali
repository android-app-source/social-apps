.class public LX/9IQ;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/9IO;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedback/ui/rows/views/specs/CommentVideoComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1463021
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/9IQ;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedback/ui/rows/views/specs/CommentVideoComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1463035
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1463036
    iput-object p1, p0, LX/9IQ;->b:LX/0Ot;

    .line 1463037
    return-void
.end method

.method public static a(LX/0QB;)LX/9IQ;
    .locals 4

    .prologue
    .line 1463024
    const-class v1, LX/9IQ;

    monitor-enter v1

    .line 1463025
    :try_start_0
    sget-object v0, LX/9IQ;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1463026
    sput-object v2, LX/9IQ;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1463027
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1463028
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1463029
    new-instance v3, LX/9IQ;

    const/16 p0, 0x1dec

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/9IQ;-><init>(LX/0Ot;)V

    .line 1463030
    move-object v0, v3

    .line 1463031
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1463032
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/9IQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1463033
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1463034
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1463022
    invoke-static {}, LX/1dS;->b()V

    .line 1463023
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x321dbfaa

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1463015
    check-cast p6, LX/9IP;

    .line 1463016
    iget-object v1, p0, LX/9IQ;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v1, p6, LX/9IP;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1463017
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    .line 1463018
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result p0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v2

    invoke-static {p0, v2}, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->a(II)F

    move-result v2

    .line 1463019
    invoke-static {p3, p4, v2, p5}, LX/1oC;->a(IIFLX/1no;)V

    .line 1463020
    const/16 v1, 0x1f

    const v2, -0x577ec687

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1463009
    iget-object v0, p0, LX/9IQ;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 1463010
    new-instance v0, LX/7LN;

    invoke-direct {v0}, LX/7LN;-><init>()V

    sget-object v1, LX/04D;->COMMENT:LX/04D;

    .line 1463011
    iput-object v1, v0, LX/7LN;->a:LX/04D;

    .line 1463012
    move-object v0, v0

    .line 1463013
    const/4 v1, 0x3

    new-array v1, v1, [LX/2oy;

    const/4 v2, 0x0

    new-instance v3, Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-direct {v3, p1}, Lcom/facebook/video/player/plugins/VideoPlugin;-><init>(Landroid/content/Context;)V

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Lcom/facebook/video/player/plugins/CoverImagePlugin;

    sget-object p0, Lcom/facebook/feedback/ui/rows/views/specs/CommentVideoComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v3, p1, p0}, Lcom/facebook/video/player/plugins/CoverImagePlugin;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V

    aput-object v3, v1, v2

    const/4 v2, 0x2

    new-instance v3, LX/7N5;

    invoke-direct {v3, p1}, LX/7N5;-><init>(Landroid/content/Context;)V

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, LX/7LN;->a([LX/2oy;)LX/7LN;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/7LN;->a(Landroid/content/Context;)Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    move-object v0, v0

    .line 1463014
    return-object v0
.end method

.method public final b(LX/1De;LX/1X1;)V
    .locals 10

    .prologue
    .line 1463038
    check-cast p2, LX/9IP;

    .line 1463039
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v1

    .line 1463040
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v2

    .line 1463041
    iget-object v0, p0, LX/9IQ;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedback/ui/rows/views/specs/CommentVideoComponentSpec;

    iget-object v3, p2, LX/9IP;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget-boolean v4, p2, LX/9IP;->b:Z

    .line 1463042
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v6

    .line 1463043
    new-instance v5, LX/2pZ;

    invoke-direct {v5}, LX/2pZ;-><init>()V

    invoke-static {v6}, LX/9D4;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v7

    .line 1463044
    iput-object v7, v5, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1463045
    move-object v5, v5

    .line 1463046
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLMedia;->bB()I

    move-result v7

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLMedia;->S()I

    move-result v8

    invoke-static {v7, v8}, Lcom/facebook/feedback/ui/rows/views/CommentAnimatedImageShareView;->a(II)F

    move-result v7

    float-to-double v7, v7

    .line 1463047
    iput-wide v7, v5, LX/2pZ;->e:D

    .line 1463048
    move-object v5, v5

    .line 1463049
    const-string v7, "CoverImageParamsKey"

    invoke-static {v6}, LX/9D4;->b(Lcom/facebook/graphql/model/GraphQLMedia;)LX/1bf;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, LX/2pZ;->a(Ljava/lang/String;Ljava/lang/Object;)LX/2pZ;

    move-result-object v7

    const-string v8, "ShowGifPlayIconKey"

    invoke-static {v3}, LX/1VO;->k(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-static {v3}, LX/1VO;->l(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v5

    if-eqz v5, :cond_2

    :cond_0
    const/4 v5, 0x1

    :goto_0
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v7, v8, v5}, LX/2pZ;->a(Ljava/lang/String;Ljava/lang/Object;)LX/2pZ;

    move-result-object v5

    invoke-virtual {v5}, LX/2pZ;->b()LX/2pa;

    move-result-object v5

    .line 1463050
    iput-object v5, v1, LX/1np;->a:Ljava/lang/Object;

    .line 1463051
    if-eqz v4, :cond_1

    .line 1463052
    new-instance v5, LX/9IR;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v0, v6}, LX/9IR;-><init>(Lcom/facebook/feedback/ui/rows/views/specs/CommentVideoComponentSpec;Ljava/lang/String;)V

    .line 1463053
    iput-object v5, v2, LX/1np;->a:Ljava/lang/Object;

    .line 1463054
    :cond_1
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1463055
    check-cast v0, LX/2pa;

    iput-object v0, p2, LX/9IP;->c:LX/2pa;

    .line 1463056
    invoke-static {v1}, LX/1cy;->a(LX/1np;)V

    .line 1463057
    iget-object v0, v2, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1463058
    check-cast v0, LX/2oV;

    iput-object v0, p2, LX/9IP;->d:LX/2oV;

    .line 1463059
    invoke-static {v2}, LX/1cy;->a(LX/1np;)V

    .line 1463060
    return-void

    .line 1463061
    :cond_2
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public final c(LX/1De;)LX/9IO;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1463001
    new-instance v1, LX/9IP;

    invoke-direct {v1, p0}, LX/9IP;-><init>(LX/9IQ;)V

    .line 1463002
    sget-object v2, LX/9IQ;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/9IO;

    .line 1463003
    if-nez v2, :cond_0

    .line 1463004
    new-instance v2, LX/9IO;

    invoke-direct {v2}, LX/9IO;-><init>()V

    .line 1463005
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/9IO;->a$redex0(LX/9IO;LX/1De;IILX/9IP;)V

    .line 1463006
    move-object v1, v2

    .line 1463007
    move-object v0, v1

    .line 1463008
    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1463000
    const/4 v0, 0x1

    return v0
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 3

    .prologue
    .line 1462989
    check-cast p3, LX/9IP;

    .line 1462990
    iget-object v0, p0, LX/9IQ;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedback/ui/rows/views/specs/CommentVideoComponentSpec;

    check-cast p2, Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, p3, LX/9IP;->d:LX/2oV;

    iget-object v2, p3, LX/9IP;->c:LX/2pa;

    .line 1462991
    invoke-virtual {p2, v2}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 1462992
    if-eqz v1, :cond_0

    .line 1462993
    iget-object p0, v0, Lcom/facebook/feedback/ui/rows/views/specs/CommentVideoComponentSpec;->b:LX/1AV;

    invoke-virtual {p0, p2, v1}, LX/1AV;->a(Landroid/view/View;LX/2oV;)V

    .line 1462994
    :cond_0
    return-void
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 1462999
    sget-object v0, LX/1mv;->VIEW:LX/1mv;

    return-object v0
.end method

.method public final f(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 1462996
    iget-object v0, p0, LX/9IQ;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    check-cast p2, Lcom/facebook/video/player/RichVideoPlayer;

    .line 1462997
    invoke-virtual {p2}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    .line 1462998
    return-void
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 1462995
    const/16 v0, 0xf

    return v0
.end method
