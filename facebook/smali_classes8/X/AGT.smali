.class public final LX/AGT;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1652089
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 1652090
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1652091
    :goto_0
    return v1

    .line 1652092
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1652093
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 1652094
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1652095
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1652096
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 1652097
    const-string v5, "id"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1652098
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 1652099
    :cond_2
    const-string v5, "posts"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1652100
    invoke-static {p0, p1}, LX/AGP;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1652101
    :cond_3
    const-string v5, "thread_participants"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1652102
    invoke-static {p0, p1}, LX/AGS;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1652103
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1652104
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1652105
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1652106
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1652107
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1652108
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1652109
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1652110
    if-eqz v0, :cond_0

    .line 1652111
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1652112
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1652113
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1652114
    if-eqz v0, :cond_1

    .line 1652115
    const-string v1, "posts"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1652116
    invoke-static {p0, v0, p2, p3}, LX/AGP;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1652117
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1652118
    if-eqz v0, :cond_2

    .line 1652119
    const-string v1, "thread_participants"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1652120
    invoke-static {p0, v0, p2, p3}, LX/AGS;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1652121
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1652122
    return-void
.end method
