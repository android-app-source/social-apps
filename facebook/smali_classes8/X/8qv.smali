.class public final LX/8qv;
.super LX/4oc;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLComment;

.field public final synthetic b:LX/8r1;


# direct methods
.method public constructor <init>(LX/8r1;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 0

    .prologue
    .line 1408047
    iput-object p1, p0, LX/8qv;->b:LX/8r1;

    iput-object p3, p0, LX/8qv;->a:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-direct {p0, p2}, LX/4oc;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1408048
    iget-object v0, p0, LX/8qv;->b:LX/8r1;

    iget-object v0, v0, LX/8r1;->f:LX/8qr;

    .line 1408049
    iget-object p0, v0, LX/8qr;->b:LX/8qq;

    move-object v0, p0

    .line 1408050
    invoke-interface {v0, p1}, LX/8qq;->c(Landroid/view/View;)V

    .line 1408051
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .prologue
    .line 1408052
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 1408053
    iget-object v0, p0, LX/8qv;->b:LX/8r1;

    iget-boolean v0, v0, LX/8r1;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8qv;->a:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->s_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1408054
    iget-object v0, p0, LX/8qv;->b:LX/8r1;

    iget v0, v0, LX/8r1;->e:I

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 1408055
    :goto_0
    return-void

    .line 1408056
    :cond_0
    iget-object v0, p0, LX/8qv;->b:LX/8r1;

    iget v0, v0, LX/8r1;->d:I

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    goto :goto_0
.end method
