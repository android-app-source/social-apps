.class public LX/AP6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Or;
    .annotation runtime Lcom/facebook/groups/photos/gk/IsInGroupsPhotoAlbumsGk;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7mI;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7Dh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/groups/photos/gk/IsInGroupsPhotoAlbumsGk;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/7mI;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/7Dh;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1670016
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1670017
    iput-object p1, p0, LX/AP6;->a:LX/0Or;

    .line 1670018
    iput-object p2, p0, LX/AP6;->b:LX/0Ot;

    .line 1670019
    iput-object p3, p0, LX/AP6;->c:LX/0Ot;

    .line 1670020
    iput-object p4, p0, LX/AP6;->d:LX/0Ot;

    .line 1670021
    return-void
.end method

.method public static a(LX/0QB;)LX/AP6;
    .locals 1

    .prologue
    .line 1670015
    invoke-static {p0}, LX/AP6;->b(LX/0QB;)LX/AP6;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/AP6;LX/2rw;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1670014
    sget-object v0, LX/2rw;->UNDIRECTED:LX/2rw;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, LX/AP6;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v2, LX/1EB;->as:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/AP6;
    .locals 5

    .prologue
    .line 1669991
    new-instance v0, LX/AP6;

    const/16 v1, 0x31f

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v2, 0x2c12

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x1032

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x3572

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, LX/AP6;-><init>(LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 1669992
    return-object v0
.end method


# virtual methods
.method public final a(LX/ARN;LX/2rw;Ljava/lang/String;LX/5Rn;ZZLX/0Px;ZZ)Z
    .locals 4
    .param p1    # LX/ARN;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/ARN;",
            "LX/2rw;",
            "Ljava/lang/String;",
            "LX/5Rn;",
            "ZZ",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;ZZ)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1669993
    if-eqz p1, :cond_0

    invoke-interface {p1}, LX/ARN;->a()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 1669994
    :goto_0
    return v0

    .line 1669995
    :cond_0
    if-eqz p5, :cond_1

    move v0, v1

    .line 1669996
    goto :goto_0

    .line 1669997
    :cond_1
    invoke-static {p7}, LX/7kq;->j(LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p0, p2}, LX/AP6;->a(LX/AP6;LX/2rw;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    .line 1669998
    :goto_1
    if-nez v0, :cond_3

    move v0, v1

    .line 1669999
    goto :goto_0

    .line 1670000
    :cond_2
    invoke-static {p7}, LX/7kq;->p(LX/0Px;)Z

    move-result v0

    goto :goto_1

    .line 1670001
    :cond_3
    if-eqz p6, :cond_4

    move v0, v1

    .line 1670002
    goto :goto_0

    .line 1670003
    :cond_4
    if-eqz p8, :cond_5

    move v0, v1

    .line 1670004
    goto :goto_0

    .line 1670005
    :cond_5
    sget-object v0, LX/5Rn;->NORMAL:LX/5Rn;

    if-eq p4, v0, :cond_6

    move v0, v1

    .line 1670006
    goto :goto_0

    .line 1670007
    :cond_6
    sget-object v0, LX/2rw;->UNDIRECTED:LX/2rw;

    if-eq p2, v0, :cond_8

    sget-object v0, LX/2rw;->PAGE:LX/2rw;

    if-eq p2, v0, :cond_8

    sget-object v0, LX/2rw;->GROUP:LX/2rw;

    if-ne p2, v0, :cond_7

    iget-object v0, p0, LX/AP6;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v3, LX/03R;->YES:LX/03R;

    if-eq v0, v3, :cond_8

    :cond_7
    move v0, v1

    .line 1670008
    goto :goto_0

    .line 1670009
    :cond_8
    invoke-static {p7}, LX/7kq;->w(LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, LX/AP6;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Dh;

    invoke-virtual {v0}, LX/7Dh;->d()Z

    move-result v0

    if-nez v0, :cond_9

    move v0, v1

    .line 1670010
    goto :goto_0

    .line 1670011
    :cond_9
    sget-object v0, LX/2rw;->PAGE:LX/2rw;

    if-ne p2, v0, :cond_b

    if-eqz p9, :cond_a

    iget-object v0, p0, LX/AP6;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7mI;

    invoke-interface {v0, p3}, LX/7mI;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    :cond_a
    move v0, v1

    .line 1670012
    goto :goto_0

    :cond_b
    move v0, v2

    .line 1670013
    goto :goto_0
.end method
