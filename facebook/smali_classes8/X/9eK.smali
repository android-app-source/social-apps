.class public final enum LX/9eK;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9eK;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9eK;

.field public static final enum ANIMATE_IN:LX/9eK;

.field public static final enum ANIMATE_OUT:LX/9eK;

.field public static final enum ANIMATE_WAIT:LX/9eK;

.field public static final enum INIT:LX/9eK;

.field public static final enum NORMAL:LX/9eK;

.field public static final enum SWIPING_FRAME:LX/9eK;

.field public static final enum SWIPING_IMAGE:LX/9eK;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1519466
    new-instance v0, LX/9eK;

    const-string v1, "INIT"

    invoke-direct {v0, v1, v3}, LX/9eK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9eK;->INIT:LX/9eK;

    .line 1519467
    new-instance v0, LX/9eK;

    const-string v1, "ANIMATE_IN"

    invoke-direct {v0, v1, v4}, LX/9eK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9eK;->ANIMATE_IN:LX/9eK;

    .line 1519468
    new-instance v0, LX/9eK;

    const-string v1, "ANIMATE_WAIT"

    invoke-direct {v0, v1, v5}, LX/9eK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9eK;->ANIMATE_WAIT:LX/9eK;

    .line 1519469
    new-instance v0, LX/9eK;

    const-string v1, "SWIPING_IMAGE"

    invoke-direct {v0, v1, v6}, LX/9eK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9eK;->SWIPING_IMAGE:LX/9eK;

    .line 1519470
    new-instance v0, LX/9eK;

    const-string v1, "SWIPING_FRAME"

    invoke-direct {v0, v1, v7}, LX/9eK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9eK;->SWIPING_FRAME:LX/9eK;

    .line 1519471
    new-instance v0, LX/9eK;

    const-string v1, "ANIMATE_OUT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/9eK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9eK;->ANIMATE_OUT:LX/9eK;

    .line 1519472
    new-instance v0, LX/9eK;

    const-string v1, "NORMAL"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/9eK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9eK;->NORMAL:LX/9eK;

    .line 1519473
    const/4 v0, 0x7

    new-array v0, v0, [LX/9eK;

    sget-object v1, LX/9eK;->INIT:LX/9eK;

    aput-object v1, v0, v3

    sget-object v1, LX/9eK;->ANIMATE_IN:LX/9eK;

    aput-object v1, v0, v4

    sget-object v1, LX/9eK;->ANIMATE_WAIT:LX/9eK;

    aput-object v1, v0, v5

    sget-object v1, LX/9eK;->SWIPING_IMAGE:LX/9eK;

    aput-object v1, v0, v6

    sget-object v1, LX/9eK;->SWIPING_FRAME:LX/9eK;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/9eK;->ANIMATE_OUT:LX/9eK;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/9eK;->NORMAL:LX/9eK;

    aput-object v2, v0, v1

    sput-object v0, LX/9eK;->$VALUES:[LX/9eK;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1519462
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static isSwiping(LX/9eK;)Z
    .locals 1

    .prologue
    .line 1519465
    sget-object v0, LX/9eK;->SWIPING_IMAGE:LX/9eK;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/9eK;->SWIPING_FRAME:LX/9eK;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/9eK;
    .locals 1

    .prologue
    .line 1519464
    const-class v0, LX/9eK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9eK;

    return-object v0
.end method

.method public static values()[LX/9eK;
    .locals 1

    .prologue
    .line 1519463
    sget-object v0, LX/9eK;->$VALUES:[LX/9eK;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9eK;

    return-object v0
.end method
