.class public abstract LX/AOH;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "VH:",
        "LX/62U;",
        ">",
        "LX/1OM",
        "<TVH;>;"
    }
.end annotation


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:Landroid/view/View$OnClickListener;

.field private c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final d:LX/ANk;

.field public e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Px;ILX/ANk;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<TT;>;I",
            "LX/ANk;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 1668834
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1668835
    new-instance v0, LX/AOK;

    invoke-direct {v0, p0}, LX/AOK;-><init>(LX/AOH;)V

    iput-object v0, p0, LX/AOH;->b:Landroid/view/View$OnClickListener;

    .line 1668836
    iput v1, p0, LX/AOH;->e:I

    .line 1668837
    iput-object p1, p0, LX/AOH;->a:Landroid/content/Context;

    .line 1668838
    iput-object p2, p0, LX/AOH;->c:LX/0Px;

    .line 1668839
    iput-object p4, p0, LX/AOH;->d:LX/ANk;

    .line 1668840
    iput p3, p0, LX/AOH;->e:I

    .line 1668841
    iget v0, p0, LX/AOH;->e:I

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/AOH;->d:LX/ANk;

    if-eqz v0, :cond_0

    .line 1668842
    iget-object v0, p0, LX/AOH;->d:LX/ANk;

    iget v1, p0, LX/AOH;->e:I

    invoke-interface {v0, v1}, LX/ANk;->a(I)V

    .line 1668843
    :cond_0
    return-void
.end method

.method public static e(LX/AOH;I)V
    .locals 2

    .prologue
    .line 1668844
    iget v0, p0, LX/AOH;->e:I

    if-ne v0, p1, :cond_1

    .line 1668845
    :cond_0
    :goto_0
    return-void

    .line 1668846
    :cond_1
    iget v0, p0, LX/AOH;->e:I

    .line 1668847
    iput p1, p0, LX/AOH;->e:I

    .line 1668848
    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    .line 1668849
    invoke-virtual {p0, v0}, LX/1OM;->i_(I)V

    .line 1668850
    :cond_2
    iget v0, p0, LX/AOH;->e:I

    invoke-virtual {p0, v0}, LX/1OM;->i_(I)V

    .line 1668851
    iget-object v0, p0, LX/AOH;->d:LX/ANk;

    if-eqz v0, :cond_0

    .line 1668852
    iget-object v0, p0, LX/AOH;->d:LX/ANk;

    iget v1, p0, LX/AOH;->e:I

    invoke-interface {v0, v1}, LX/ANk;->a(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1668853
    iput-object p1, p0, LX/AOH;->c:LX/0Px;

    .line 1668854
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 1668855
    return-void
.end method

.method public final a(LX/1a1;I)V
    .locals 2

    .prologue
    .line 1668856
    check-cast p1, LX/62U;

    .line 1668857
    iget-object v0, p1, LX/62U;->l:Landroid/view/View;

    iget-object v1, p0, LX/AOH;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1668858
    iget-object v1, p1, LX/62U;->l:Landroid/view/View;

    iget v0, p0, LX/AOH;->e:I

    if-ne v0, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setSelected(Z)V

    .line 1668859
    iget-object v0, p1, LX/62U;->l:Landroid/view/View;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1668860
    iget-object v0, p0, LX/AOH;->c:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LX/AOH;->a(LX/62U;Ljava/lang/Object;)V

    .line 1668861
    return-void

    .line 1668862
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract a(LX/62U;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TVH;TT;)V"
        }
    .end annotation
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1668863
    iget-object v0, p0, LX/AOH;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
