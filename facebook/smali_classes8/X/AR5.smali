.class public LX/AR5;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1672365
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0io;LX/0SG;)Lcom/facebook/audience/model/UploadShot;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData::",
            "LX/0io;",
            ":",
            "LX/0iq;",
            ":",
            "LX/0is;",
            ">(TModelData;",
            "LX/0SG;",
            ")",
            "Lcom/facebook/audience/model/UploadShot;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1672366
    move-object v0, p0

    check-cast v0, LX/0iq;

    invoke-interface {v0}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    iget-object v3, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->g:LX/0Px;

    move-object v0, p0

    .line 1672367
    check-cast v0, LX/0iq;

    invoke-interface {v0}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    iget-boolean v0, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->h:Z

    .line 1672368
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    if-nez v0, :cond_0

    .line 1672369
    const/4 v0, 0x0

    .line 1672370
    :goto_0
    return-object v0

    .line 1672371
    :cond_0
    invoke-interface {p0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1672372
    invoke-interface {p0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->f(LX/0Px;)Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v4

    .line 1672373
    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1672374
    invoke-virtual {v4}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1672375
    invoke-virtual {v4}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getOriginalUri()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1672376
    invoke-virtual {v4}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1672377
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    move-object v0, p0

    .line 1672378
    check-cast v0, LX/0iq;

    invoke-interface {v0}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    iget-object v6, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->g:LX/0Px;

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v3, v2

    :goto_2
    if-ge v3, v7, :cond_2

    invoke-virtual {v6, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    .line 1672379
    invoke-static {}, Lcom/facebook/audience/model/AudienceControlData;->newBuilder()Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v8

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->a()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setId(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v8

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->b()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setName(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v8

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->c()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setProfileUri(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v8

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setLowResProfileUri(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/model/AudienceControlData$Builder;->a()Lcom/facebook/audience/model/AudienceControlData;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1672380
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_1
    move v0, v2

    .line 1672381
    goto :goto_1

    .line 1672382
    :cond_2
    invoke-static {}, Lcom/facebook/audience/model/UploadShot;->newBuilder()Lcom/facebook/audience/model/UploadShot$Builder;

    move-result-object v0

    invoke-virtual {v4}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getOriginalUri()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/audience/model/UploadShot$Builder;->setPath(Ljava/lang/String;)Lcom/facebook/audience/model/UploadShot$Builder;

    move-result-object v0

    invoke-virtual {v4}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/media/MediaItem;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/audience/model/UploadShot$Builder;->setMessage(Ljava/lang/String;)Lcom/facebook/audience/model/UploadShot$Builder;

    move-result-object v0

    invoke-virtual {v4}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/media/MediaItem;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/audience/model/UploadShot$Builder;->setCaption(Ljava/lang/String;)Lcom/facebook/audience/model/UploadShot$Builder;

    move-result-object v3

    invoke-virtual {v4}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 1672383
    iget-object v6, v0, Lcom/facebook/ipc/media/data/MediaData;->mType:LX/4gQ;

    move-object v0, v6

    .line 1672384
    sget-object v6, LX/4gQ;->Photo:LX/4gQ;

    if-ne v0, v6, :cond_3

    sget-object v0, LX/7gi;->PHOTO:LX/7gi;

    :goto_3
    invoke-virtual {v3, v0}, Lcom/facebook/audience/model/UploadShot$Builder;->setMediaType(LX/7gi;)Lcom/facebook/audience/model/UploadShot$Builder;

    move-result-object v0

    invoke-interface {p1}, LX/0SG;->a()J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Lcom/facebook/audience/model/UploadShot$Builder;->setCreatedAtTime(J)Lcom/facebook/audience/model/UploadShot$Builder;

    move-result-object v0

    sget-object v3, LX/7h9;->REGULAR:LX/7h9;

    invoke-virtual {v0, v3}, Lcom/facebook/audience/model/UploadShot$Builder;->setBackstagePostType(LX/7h9;)Lcom/facebook/audience/model/UploadShot$Builder;

    move-result-object v0

    invoke-virtual {v4}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/audience/model/UploadShot$Builder;->setCreativeEditingData(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Lcom/facebook/audience/model/UploadShot$Builder;

    move-result-object v0

    invoke-virtual {v4}, Lcom/facebook/composer/attachments/ComposerAttachment;->f()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/audience/model/UploadShot$Builder;->setVideoCreativeEditingData(Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;)Lcom/facebook/audience/model/UploadShot$Builder;

    move-result-object v3

    move-object v0, p0

    check-cast v0, LX/0is;

    invoke-static {v0}, LX/87S;->a(LX/0is;)LX/0Px;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/facebook/audience/model/UploadShot$Builder;->setInspirationPromptAnalytics(LX/0Px;)Lcom/facebook/audience/model/UploadShot$Builder;

    move-result-object v0

    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/audience/model/UploadShot$Builder;->setAudience(LX/0Px;)Lcom/facebook/audience/model/UploadShot$Builder;

    move-result-object v0

    check-cast p0, LX/0iq;

    invoke-interface {p0}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v3

    iget-boolean v3, v3, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->h:Z

    if-nez v3, :cond_4

    :goto_4
    invoke-virtual {v0, v1}, Lcom/facebook/audience/model/UploadShot$Builder;->setIsPrivate(Z)Lcom/facebook/audience/model/UploadShot$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/model/UploadShot$Builder;->a()Lcom/facebook/audience/model/UploadShot;

    move-result-object v0

    goto/16 :goto_0

    :cond_3
    sget-object v0, LX/7gi;->VIDEO:LX/7gi;

    goto :goto_3

    :cond_4
    move v1, v2

    goto :goto_4
.end method
