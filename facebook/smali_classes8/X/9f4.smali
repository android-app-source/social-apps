.class public final LX/9f4;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V
    .locals 0

    .prologue
    .line 1520778
    iput-object p1, p0, LX/9f4;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1520758
    iget-object v0, p0, LX/9f4;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-virtual {v0}, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->o()V

    .line 1520759
    iget-object v0, p0, LX/9f4;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    const/4 v1, 0x1

    .line 1520760
    iput-boolean v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->T:Z

    .line 1520761
    return-void
.end method

.method public final a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)V
    .locals 2

    .prologue
    .line 1520762
    iget-object v0, p0, LX/9f4;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    .line 1520763
    iput-object p1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1520764
    iget-object v0, p0, LX/9f4;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->l:Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-virtual {v0}, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->p()V

    .line 1520765
    iget-object v0, p0, LX/9f4;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    const/4 v1, 0x0

    .line 1520766
    iput-boolean v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->T:Z

    .line 1520767
    iget-object v0, p0, LX/9f4;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-boolean v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->U:Z

    if-nez v0, :cond_0

    .line 1520768
    iget-object v0, p0, LX/9f4;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    invoke-static {v0}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->m$redex0(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V

    .line 1520769
    iget-object v0, p0, LX/9f4;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    invoke-static {v0}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->p$redex0(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V

    .line 1520770
    iget-object v0, p0, LX/9f4;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->N:LX/9eb;

    iget-object v1, p0, LX/9f4;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    invoke-interface {v0, v1}, LX/9eb;->a(Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;)V

    .line 1520771
    :goto_0
    return-void

    .line 1520772
    :cond_0
    iget-object v0, p0, LX/9f4;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-boolean v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->j:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/9f4;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-static {v0}, LX/5iB;->e(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/9f4;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-static {v0}, LX/5iB;->b(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, LX/9f4;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-boolean v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->f:Z

    if-eqz v0, :cond_2

    .line 1520773
    iget-object v0, p0, LX/9f4;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    invoke-static {v0}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->s(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V

    goto :goto_0

    .line 1520774
    :cond_2
    iget-object v0, p0, LX/9f4;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    invoke-static {v0}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->z(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V

    .line 1520775
    iget-object v0, p0, LX/9f4;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->m:LX/9el;

    if-eqz v0, :cond_3

    .line 1520776
    iget-object v0, p0, LX/9f4;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->m:LX/9el;

    iget-object v1, p0, LX/9f4;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-interface {v0, v1}, LX/9el;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)V

    .line 1520777
    :cond_3
    iget-object v0, p0, LX/9f4;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->b(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;Z)V

    goto :goto_0
.end method
