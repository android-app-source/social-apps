.class public final LX/9HV;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/9HW;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1X1",
            "<*>;"
        }
    .end annotation
.end field

.field public b:LX/9FD;

.field public c:LX/9HA;

.field public final synthetic d:LX/9HW;


# direct methods
.method public constructor <init>(LX/9HW;)V
    .locals 1

    .prologue
    .line 1461361
    iput-object p1, p0, LX/9HV;->d:LX/9HW;

    .line 1461362
    move-object v0, p1

    .line 1461363
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1461364
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1461365
    const-string v0, "CommentBackgroundStylerComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1461366
    if-ne p0, p1, :cond_1

    .line 1461367
    :cond_0
    :goto_0
    return v0

    .line 1461368
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1461369
    goto :goto_0

    .line 1461370
    :cond_3
    check-cast p1, LX/9HV;

    .line 1461371
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1461372
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1461373
    if-eq v2, v3, :cond_0

    .line 1461374
    iget-object v2, p0, LX/9HV;->a:LX/1X1;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/9HV;->a:LX/1X1;

    iget-object v3, p1, LX/9HV;->a:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1461375
    goto :goto_0

    .line 1461376
    :cond_5
    iget-object v2, p1, LX/9HV;->a:LX/1X1;

    if-nez v2, :cond_4

    .line 1461377
    :cond_6
    iget-object v2, p0, LX/9HV;->b:LX/9FD;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/9HV;->b:LX/9FD;

    iget-object v3, p1, LX/9HV;->b:LX/9FD;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1461378
    goto :goto_0

    .line 1461379
    :cond_8
    iget-object v2, p1, LX/9HV;->b:LX/9FD;

    if-nez v2, :cond_7

    .line 1461380
    :cond_9
    iget-object v2, p0, LX/9HV;->c:LX/9HA;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/9HV;->c:LX/9HA;

    iget-object v3, p1, LX/9HV;->c:LX/9HA;

    invoke-virtual {v2, v3}, LX/9HA;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1461381
    goto :goto_0

    .line 1461382
    :cond_a
    iget-object v2, p1, LX/9HV;->c:LX/9HA;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 1461383
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/9HV;

    .line 1461384
    iget-object v1, v0, LX/9HV;->a:LX/1X1;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/9HV;->a:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v1

    :goto_0
    iput-object v1, v0, LX/9HV;->a:LX/1X1;

    .line 1461385
    return-object v0

    .line 1461386
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
