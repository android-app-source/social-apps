.class public LX/AOv;
.super LX/AOu;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPlaceListItem;",
            ">;"
        }
    .end annotation
.end field

.field public c:Z

.field private final d:LX/9Er;

.field public e:LX/AOi;


# direct methods
.method public constructor <init>(LX/0Sh;LX/9Er;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1669737
    invoke-direct {p0, p1}, LX/AOu;-><init>(LX/0Sh;)V

    .line 1669738
    iput-object p2, p0, LX/AOv;->d:LX/9Er;

    .line 1669739
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1669740
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1669741
    iget-object v0, p0, LX/AOv;->b:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/AOv;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public final b(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1669742
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1669743
    iget-object v0, p0, LX/AOv;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlaceListItem;

    .line 1669744
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    .line 1669745
    new-instance v3, LX/AOt;

    invoke-direct {v3, v1}, LX/AOt;-><init>(Landroid/content/Context;)V

    .line 1669746
    const-string v1, "full_map_mapview"

    .line 1669747
    iget-object v4, p0, LX/AOv;->d:LX/9Er;

    iget-object v5, p0, LX/AOv;->a:Ljava/lang/String;

    invoke-virtual {v4, v2, v5, v1}, LX/9Er;->a(Lcom/facebook/graphql/model/GraphQLPage;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/AOt;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1669748
    iget-boolean v1, p0, LX/AOv;->c:Z

    .line 1669749
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    .line 1669750
    if-nez v4, :cond_0

    .line 1669751
    :goto_0
    iget-object v0, p0, LX/AOv;->e:LX/AOi;

    .line 1669752
    iput-object v0, v3, LX/AOt;->h:LX/AOi;

    .line 1669753
    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1669754
    return-object v3

    .line 1669755
    :cond_0
    iget-object v2, v3, LX/AOt;->b:Lcom/facebook/widget/text/BetterTextView;

    iget-object v5, v3, LX/AOt;->a:LX/9Er;

    invoke-virtual {v5, v0}, LX/9Er;->a(Lcom/facebook/graphql/model/GraphQLPlaceListItem;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1669756
    iget-object v2, v3, LX/AOt;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1669757
    iget-object v2, v3, LX/AOt;->e:Lcom/facebook/widget/text/BetterTextView;

    iget-object v5, v3, LX/AOt;->a:LX/9Er;

    const/4 p2, 0x1

    invoke-virtual {v5, v4, p2}, LX/9Er;->a(Lcom/facebook/graphql/model/GraphQLPage;I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1669758
    iget-object v5, v3, LX/AOt;->f:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->j()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->j()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStreetAddress;->k()Ljava/lang/String;

    move-result-object v2

    :goto_1
    invoke-virtual {v5, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1669759
    iget-object v2, v3, LX/AOt;->c:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    invoke-static {v4}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 1669760
    if-eqz v1, :cond_2

    .line 1669761
    iget-object v2, v3, LX/AOt;->g:Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1669762
    iget-object v2, v3, LX/AOt;->g:Landroid/view/View;

    new-instance v4, LX/AOs;

    invoke-direct {v4, v3, v0}, LX/AOs;-><init>(LX/AOt;Lcom/facebook/graphql/model/GraphQLPlaceListItem;)V

    invoke-virtual {v2, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 1669763
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 1669764
    :cond_2
    iget-object v2, v3, LX/AOt;->g:Landroid/view/View;

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final c(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 1669765
    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1669766
    return-void
.end method
