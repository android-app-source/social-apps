.class public final LX/8vf;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/8vj;

.field public b:LX/8vg;

.field public c:I

.field public d:[Landroid/view/View;

.field public e:[Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public f:I

.field public g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0YU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0YU",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/8vj;)V
    .locals 1

    .prologue
    .line 1417431
    iput-object p1, p0, LX/8vf;->a:LX/8vj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1417432
    const/4 v0, 0x0

    new-array v0, v0, [Landroid/view/View;

    iput-object v0, p0, LX/8vf;->d:[Landroid/view/View;

    return-void
.end method

.method public static b(I)Z
    .locals 1

    .prologue
    .line 1417550
    if-ltz p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1417540
    if-gtz p1, :cond_0

    .line 1417541
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Can\'t have a viewTypeCount < 1"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1417542
    :cond_0
    new-array v2, p1, [Ljava/util/ArrayList;

    move v0, v1

    .line 1417543
    :goto_0
    if-ge v0, p1, :cond_1

    .line 1417544
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    aput-object v3, v2, v0

    .line 1417545
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1417546
    :cond_1
    iput p1, p0, LX/8vf;->f:I

    .line 1417547
    aget-object v0, v2, v1

    iput-object v0, p0, LX/8vf;->g:Ljava/util/ArrayList;

    .line 1417548
    iput-object v2, p0, LX/8vf;->e:[Ljava/util/ArrayList;

    .line 1417549
    return-void
.end method

.method public final a(II)V
    .locals 5

    .prologue
    .line 1417529
    iget-object v0, p0, LX/8vf;->d:[Landroid/view/View;

    array-length v0, v0

    if-ge v0, p1, :cond_0

    .line 1417530
    new-array v0, p1, [Landroid/view/View;

    iput-object v0, p0, LX/8vf;->d:[Landroid/view/View;

    .line 1417531
    :cond_0
    iput p2, p0, LX/8vf;->c:I

    .line 1417532
    iget-object v2, p0, LX/8vf;->d:[Landroid/view/View;

    .line 1417533
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, p1, :cond_2

    .line 1417534
    iget-object v0, p0, LX/8vf;->a:LX/8vj;

    invoke-virtual {v0, v1}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1417535
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/8vc;

    .line 1417536
    if-eqz v0, :cond_1

    iget v0, v0, LX/8vc;->a:I

    const/4 v4, -0x2

    if-eq v0, v4, :cond_1

    .line 1417537
    aput-object v3, v2, v1

    .line 1417538
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1417539
    :cond_2
    return-void
.end method

.method public final a(Landroid/view/View;I)V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 1417506
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/8vc;

    .line 1417507
    if-nez v0, :cond_1

    .line 1417508
    :cond_0
    :goto_0
    return-void

    .line 1417509
    :cond_1
    iput p2, v0, LX/8vc;->d:I

    .line 1417510
    iget v1, v0, LX/8vc;->a:I

    .line 1417511
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v0, v2, :cond_7

    invoke-virtual {p1}, Landroid/view/View;->hasTransientState()Z

    move-result v0

    .line 1417512
    :goto_1
    invoke-static {v1}, LX/8vf;->b(I)Z

    move-result v2

    if-eqz v2, :cond_2

    if-eqz v0, :cond_8

    .line 1417513
    :cond_2
    const/4 v2, -0x2

    if-ne v1, v2, :cond_3

    if-eqz v0, :cond_5

    .line 1417514
    :cond_3
    iget-object v1, p0, LX/8vf;->h:Ljava/util/ArrayList;

    if-nez v1, :cond_4

    .line 1417515
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, LX/8vf;->h:Ljava/util/ArrayList;

    .line 1417516
    :cond_4
    iget-object v1, p0, LX/8vf;->h:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1417517
    :cond_5
    if-eqz v0, :cond_0

    .line 1417518
    iget-object v0, p0, LX/8vf;->i:LX/0YU;

    if-nez v0, :cond_6

    .line 1417519
    new-instance v0, LX/0YU;

    invoke-direct {v0}, LX/0YU;-><init>()V

    iput-object v0, p0, LX/8vf;->i:LX/0YU;

    .line 1417520
    :cond_6
    invoke-virtual {p1}, Landroid/view/View;->onStartTemporaryDetach()V

    .line 1417521
    iget-object v0, p0, LX/8vf;->i:LX/0YU;

    invoke-virtual {v0, p2, p1}, LX/0YU;->a(ILjava/lang/Object;)V

    goto :goto_0

    .line 1417522
    :cond_7
    const/4 v0, 0x0

    goto :goto_1

    .line 1417523
    :cond_8
    invoke-virtual {p1}, Landroid/view/View;->onStartTemporaryDetach()V

    .line 1417524
    iget v0, p0, LX/8vf;->f:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_9

    .line 1417525
    iget-object v0, p0, LX/8vf;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1417526
    :goto_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 1417527
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    goto :goto_0

    .line 1417528
    :cond_9
    iget-object v0, p0, LX/8vf;->e:[Ljava/util/ArrayList;

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method public final b()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1417489
    iget v0, p0, LX/8vf;->f:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1417490
    iget-object v3, p0, LX/8vf;->g:Ljava/util/ArrayList;

    .line 1417491
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v2

    .line 1417492
    :goto_0
    if-ge v1, v4, :cond_2

    .line 1417493
    iget-object v5, p0, LX/8vf;->a:LX/8vj;

    add-int/lit8 v0, v4, -0x1

    sub-int/2addr v0, v1

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v5, v0, v2}, LX/8vj;->a(LX/8vj;Landroid/view/View;Z)V

    .line 1417494
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1417495
    :cond_0
    iget v4, p0, LX/8vf;->f:I

    move v3, v2

    .line 1417496
    :goto_1
    if-ge v3, v4, :cond_2

    .line 1417497
    iget-object v0, p0, LX/8vf;->e:[Ljava/util/ArrayList;

    aget-object v5, v0, v3

    .line 1417498
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v1, v2

    .line 1417499
    :goto_2
    if-ge v1, v6, :cond_1

    .line 1417500
    iget-object v7, p0, LX/8vf;->a:LX/8vj;

    add-int/lit8 v0, v6, -0x1

    sub-int/2addr v0, v1

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v7, v0, v2}, LX/8vj;->b(LX/8vj;Landroid/view/View;Z)V

    .line 1417501
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1417502
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 1417503
    :cond_2
    iget-object v0, p0, LX/8vf;->i:LX/0YU;

    if-eqz v0, :cond_3

    .line 1417504
    iget-object v0, p0, LX/8vf;->i:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->b()V

    .line 1417505
    :cond_3
    return-void
.end method

.method public final d()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1417482
    iget-object v0, p0, LX/8vf;->h:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 1417483
    :goto_0
    return-void

    .line 1417484
    :cond_0
    iget-object v0, p0, LX/8vf;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    .line 1417485
    :goto_1
    if-ge v1, v3, :cond_1

    .line 1417486
    iget-object v4, p0, LX/8vf;->a:LX/8vj;

    iget-object v0, p0, LX/8vf;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v4, v0, v2}, LX/8vj;->c(LX/8vj;Landroid/view/View;Z)V

    .line 1417487
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1417488
    :cond_1
    iget-object v0, p0, LX/8vf;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0
.end method

.method public final e()V
    .locals 11
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 1417433
    iget-object v6, p0, LX/8vf;->d:[Landroid/view/View;

    .line 1417434
    iget v1, p0, LX/8vf;->f:I

    if-le v1, v0, :cond_5

    move v1, v0

    .line 1417435
    :goto_0
    iget-object v4, p0, LX/8vf;->g:Ljava/util/ArrayList;

    .line 1417436
    array-length v0, v6

    .line 1417437
    add-int/lit8 v0, v0, -0x1

    move v5, v0

    :goto_1
    if-ltz v5, :cond_9

    .line 1417438
    aget-object v7, v6, v5

    .line 1417439
    if-eqz v7, :cond_4

    .line 1417440
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/8vc;

    .line 1417441
    iget v8, v0, LX/8vc;->a:I

    .line 1417442
    aput-object v10, v6, v5

    .line 1417443
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v9, 0x10

    if-lt v3, v9, :cond_6

    invoke-virtual {v7}, Landroid/view/View;->hasTransientState()Z

    move-result v3

    .line 1417444
    :goto_2
    invoke-static {v8}, LX/8vf;->b(I)Z

    move-result v9

    if-eqz v9, :cond_0

    if-eqz v3, :cond_7

    .line 1417445
    :cond_0
    const/4 v0, -0x2

    if-ne v8, v0, :cond_1

    if-eqz v3, :cond_2

    .line 1417446
    :cond_1
    iget-object v0, p0, LX/8vf;->a:LX/8vj;

    invoke-static {v0, v7, v2}, LX/8vj;->d(LX/8vj;Landroid/view/View;Z)V

    .line 1417447
    :cond_2
    if-eqz v3, :cond_4

    .line 1417448
    iget-object v0, p0, LX/8vf;->i:LX/0YU;

    if-nez v0, :cond_3

    .line 1417449
    new-instance v0, LX/0YU;

    invoke-direct {v0}, LX/0YU;-><init>()V

    iput-object v0, p0, LX/8vf;->i:LX/0YU;

    .line 1417450
    :cond_3
    iget-object v0, p0, LX/8vf;->i:LX/0YU;

    iget v3, p0, LX/8vf;->c:I

    add-int/2addr v3, v5

    invoke-virtual {v0, v3, v7}, LX/0YU;->a(ILjava/lang/Object;)V

    .line 1417451
    :cond_4
    :goto_3
    add-int/lit8 v0, v5, -0x1

    move v5, v0

    goto :goto_1

    :cond_5
    move v1, v2

    .line 1417452
    goto :goto_0

    :cond_6
    move v3, v2

    .line 1417453
    goto :goto_2

    .line 1417454
    :cond_7
    if-eqz v1, :cond_e

    .line 1417455
    iget-object v3, p0, LX/8vf;->e:[Ljava/util/ArrayList;

    aget-object v3, v3, v8

    .line 1417456
    :goto_4
    invoke-virtual {v7}, Landroid/view/View;->onStartTemporaryDetach()V

    .line 1417457
    iget v4, p0, LX/8vf;->c:I

    add-int/2addr v4, v5

    iput v4, v0, LX/8vc;->d:I

    .line 1417458
    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1417459
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xe

    if-lt v0, v4, :cond_8

    .line 1417460
    invoke-virtual {v7, v10}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    :cond_8
    move-object v4, v3

    goto :goto_3

    .line 1417461
    :cond_9
    const/4 v1, 0x0

    .line 1417462
    iget-object v0, p0, LX/8vf;->d:[Landroid/view/View;

    array-length v5, v0

    .line 1417463
    iget v6, p0, LX/8vf;->f:I

    .line 1417464
    iget-object v7, p0, LX/8vf;->e:[Ljava/util/ArrayList;

    move v4, v1

    .line 1417465
    :goto_5
    if-ge v4, v6, :cond_b

    .line 1417466
    aget-object v8, v7, v4

    .line 1417467
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 1417468
    sub-int v9, v0, v5

    .line 1417469
    add-int/lit8 v0, v0, -0x1

    move v2, v1

    .line 1417470
    :goto_6
    if-ge v2, v9, :cond_a

    .line 1417471
    iget-object v10, p0, LX/8vf;->a:LX/8vj;

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v10, v0, v1}, LX/8vj;->e(LX/8vj;Landroid/view/View;Z)V

    .line 1417472
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_6

    .line 1417473
    :cond_a
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_5

    .line 1417474
    :cond_b
    iget-object v0, p0, LX/8vf;->i:LX/0YU;

    if-eqz v0, :cond_d

    .line 1417475
    :goto_7
    iget-object v0, p0, LX/8vf;->i:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->a()I

    move-result v0

    if-ge v1, v0, :cond_d

    .line 1417476
    iget-object v0, p0, LX/8vf;->i:LX/0YU;

    invoke-virtual {v0, v1}, LX/0YU;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1417477
    invoke-virtual {v0}, Landroid/view/View;->hasTransientState()Z

    move-result v0

    if-nez v0, :cond_c

    .line 1417478
    iget-object v0, p0, LX/8vf;->i:LX/0YU;

    invoke-virtual {v0, v1}, LX/0YU;->d(I)V

    .line 1417479
    add-int/lit8 v1, v1, -0x1

    .line 1417480
    :cond_c
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    .line 1417481
    :cond_d
    return-void

    :cond_e
    move-object v3, v4

    goto :goto_4
.end method
