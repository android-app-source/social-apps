.class public final LX/ALy;
.super LX/7hR;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/backstage/ui/RotateButton;


# direct methods
.method public constructor <init>(Lcom/facebook/backstage/ui/RotateButton;)V
    .locals 0

    .prologue
    .line 1665657
    iput-object p1, p0, LX/ALy;->a:Lcom/facebook/backstage/ui/RotateButton;

    invoke-direct {p0}, LX/7hR;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/backstage/ui/RotateButton;B)V
    .locals 0

    .prologue
    .line 1665658
    invoke-direct {p0, p1}, LX/ALy;-><init>(Lcom/facebook/backstage/ui/RotateButton;)V

    return-void
.end method


# virtual methods
.method public final a(LX/8YK;)V
    .locals 4

    .prologue
    .line 1665659
    iget-object v0, p0, LX/ALy;->a:Lcom/facebook/backstage/ui/RotateButton;

    invoke-virtual {p1}, LX/8YK;->b()D

    move-result-wide v2

    double-to-float v1, v2

    invoke-virtual {v0, v1}, Lcom/facebook/backstage/ui/RotateButton;->setRotation(F)V

    .line 1665660
    iget-object v0, p0, LX/ALy;->a:Lcom/facebook/backstage/ui/RotateButton;

    iget-object v0, v0, Lcom/facebook/backstage/ui/RotateButton;->b:LX/ALG;

    if-eqz v0, :cond_0

    .line 1665661
    iget-object v0, p0, LX/ALy;->a:Lcom/facebook/backstage/ui/RotateButton;

    iget-object v0, v0, Lcom/facebook/backstage/ui/RotateButton;->b:LX/ALG;

    invoke-virtual {p1}, LX/8YK;->b()D

    move-result-wide v2

    double-to-float v1, v2

    invoke-interface {v0, v1}, LX/ALG;->a(F)V

    .line 1665662
    :cond_0
    return-void
.end method
