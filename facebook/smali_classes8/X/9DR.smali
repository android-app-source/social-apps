.class public final LX/9DR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1L9;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1L9",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Ve;

.field public final synthetic b:LX/3iK;


# direct methods
.method public constructor <init>(LX/3iK;LX/0Ve;)V
    .locals 0

    .prologue
    .line 1455665
    iput-object p1, p0, LX/9DR;->b:LX/3iK;

    iput-object p2, p0, LX/9DR;->a:LX/0Ve;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1455658
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1455659
    iget-object v0, p0, LX/9DR;->b:LX/3iK;

    iget-object v0, v0, LX/3iK;->a:LX/1K9;

    new-instance v1, LX/82h;

    invoke-direct {v1, p1}, LX/82h;-><init>(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    invoke-virtual {v0, v1}, LX/1K9;->a(LX/1KJ;)V

    .line 1455660
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1455661
    iget-object v0, p0, LX/9DR;->b:LX/3iK;

    iget-object v0, v0, LX/3iK;->b:LX/0bH;

    new-instance v1, LX/8q9;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->s_()Z

    move-result v3

    const/4 v4, 0x1

    invoke-direct {v1, v2, v3, v4, p1}, LX/8q9;-><init>(Ljava/lang/String;ZZLcom/facebook/graphql/model/GraphQLFeedback;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1455662
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->s_()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1455663
    iget-object v0, p0, LX/9DR;->b:LX/3iK;

    iget-object v0, v0, LX/3iK;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3RX;

    const-string v1, "like_main"

    invoke-virtual {v0, v1}, LX/3RX;->a(Ljava/lang/String;)V

    .line 1455664
    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/Object;Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 5

    .prologue
    .line 1455644
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1455645
    iget-object v0, p0, LX/9DR;->b:LX/3iK;

    iget-object v0, v0, LX/3iK;->a:LX/1K9;

    new-instance v1, LX/82h;

    invoke-direct {v1, p1}, LX/82h;-><init>(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    invoke-virtual {v0, v1}, LX/1K9;->a(LX/1KJ;)V

    .line 1455646
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1455647
    iget-object v0, p0, LX/9DR;->b:LX/3iK;

    iget-object v0, v0, LX/3iK;->b:LX/0bH;

    new-instance v1, LX/8q9;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->s_()Z

    move-result v3

    const/4 v4, 0x1

    invoke-direct {v1, v2, v3, v4, p1}, LX/8q9;-><init>(Ljava/lang/String;ZZLcom/facebook/graphql/model/GraphQLFeedback;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1455648
    :cond_0
    if-eqz p2, :cond_1

    .line 1455649
    iget-object v0, p0, LX/9DR;->b:LX/3iK;

    iget-object v0, v0, LX/3iK;->d:LX/3iL;

    invoke-virtual {v0, p2}, LX/3iL;->a(Lcom/facebook/fbservice/service/ServiceException;)V

    .line 1455650
    :cond_1
    iget-object v0, p0, LX/9DR;->a:LX/0Ve;

    if-eqz v0, :cond_2

    .line 1455651
    iget-object v0, p0, LX/9DR;->a:LX/0Ve;

    invoke-interface {v0, p2}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 1455652
    :cond_2
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1455654
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1455655
    iget-object v0, p0, LX/9DR;->a:LX/0Ve;

    if-eqz v0, :cond_0

    .line 1455656
    iget-object v0, p0, LX/9DR;->a:LX/0Ve;

    invoke-interface {v0, p1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 1455657
    :cond_0
    return-void
.end method

.method public final bridge synthetic c(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1455653
    return-void
.end method
