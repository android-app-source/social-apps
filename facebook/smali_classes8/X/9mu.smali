.class public LX/9mu;
.super LX/33y;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/react/ReactRootView;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/341;

.field public d:LX/9ms;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:LX/9mr;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final f:LX/346;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/348;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/5qI;

.field private final j:Z

.field private final k:LX/5pU;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public volatile l:LX/5pX;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final m:Landroid/content/Context;

.field private n:LX/98l;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Ljava/lang/String;

.field private p:Landroid/app/Activity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final q:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "LX/9mm;",
            ">;"
        }
    .end annotation
.end field

.field private volatile r:Z

.field private final s:LX/344;

.field public final t:LX/349;

.field private final u:LX/0o1;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final v:LX/342;

.field private final w:Z

.field private final x:Z

.field private final y:LX/5qP;

.field private final z:LX/98l;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1536333
    const-class v0, LX/9mu;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/9mu;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/app/Activity;LX/98l;LX/346;Ljava/lang/String;Ljava/util/List;ZLX/5pU;LX/341;LX/344;LX/0o1;LX/342;LX/345;ZZ)V
    .locals 3
    .param p2    # Landroid/app/Activity;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/98l;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/346;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # LX/5pU;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p13    # LX/345;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/app/Activity;",
            "LX/98l;",
            "LX/346;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "LX/348;",
            ">;Z",
            "LX/5pU;",
            "LX/341;",
            "LX/344;",
            "LX/0o1;",
            "LX/342;",
            "LX/345;",
            "ZZ)V"
        }
    .end annotation

    .prologue
    .line 1536334
    invoke-direct {p0}, LX/33y;-><init>()V

    .line 1536335
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, LX/9mu;->b:Ljava/util/List;

    .line 1536336
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-static {v1}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, LX/9mu;->q:Ljava/util/Collection;

    .line 1536337
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/9mu;->r:Z

    .line 1536338
    new-instance v1, LX/9mo;

    invoke-direct {v1, p0}, LX/9mo;-><init>(LX/9mu;)V

    iput-object v1, p0, LX/9mu;->y:LX/5qP;

    .line 1536339
    new-instance v1, LX/9mp;

    invoke-direct {v1, p0}, LX/9mp;-><init>(LX/9mu;)V

    iput-object v1, p0, LX/9mu;->z:LX/98l;

    .line 1536340
    invoke-static {p1}, LX/9mu;->a(Landroid/content/Context;)V

    .line 1536341
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Application;

    invoke-static {v1}, Lcom/facebook/react/common/ApplicationHolder;->a(Landroid/app/Application;)V

    .line 1536342
    invoke-static {p1}, LX/5ql;->a(Landroid/content/Context;)V

    .line 1536343
    iput-object p1, p0, LX/9mu;->m:Landroid/content/Context;

    .line 1536344
    iput-object p2, p0, LX/9mu;->p:Landroid/app/Activity;

    .line 1536345
    iput-object p3, p0, LX/9mu;->n:LX/98l;

    .line 1536346
    iput-object p4, p0, LX/9mu;->f:LX/346;

    .line 1536347
    iput-object p5, p0, LX/9mu;->g:Ljava/lang/String;

    .line 1536348
    iput-object p6, p0, LX/9mu;->h:Ljava/util/List;

    .line 1536349
    iput-boolean p7, p0, LX/9mu;->j:Z

    .line 1536350
    iget-object v1, p0, LX/9mu;->y:LX/5qP;

    iget-object v2, p0, LX/9mu;->g:Ljava/lang/String;

    move-object/from16 v0, p13

    invoke-static {p1, v1, v2, p7, v0}, LX/5qJ;->a(Landroid/content/Context;LX/5qP;Ljava/lang/String;ZLX/345;)LX/5qI;

    move-result-object v1

    iput-object v1, p0, LX/9mu;->i:LX/5qI;

    .line 1536351
    iput-object p8, p0, LX/9mu;->k:LX/5pU;

    .line 1536352
    iput-object p9, p0, LX/9mu;->c:LX/341;

    .line 1536353
    iput-object p10, p0, LX/9mu;->s:LX/344;

    .line 1536354
    new-instance v1, LX/349;

    invoke-direct {v1, p1}, LX/349;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, LX/9mu;->t:LX/349;

    .line 1536355
    iput-object p11, p0, LX/9mu;->u:LX/0o1;

    .line 1536356
    iput-object p12, p0, LX/9mu;->v:LX/342;

    .line 1536357
    move/from16 v0, p14

    iput-boolean v0, p0, LX/9mu;->w:Z

    .line 1536358
    move/from16 v0, p15

    iput-boolean v0, p0, LX/9mu;->x:Z

    .line 1536359
    return-void
.end method

.method public static synthetic a(LX/9mu;Lcom/facebook/react/cxxbridge/JavaScriptExecutor;LX/346;)LX/5pY;
    .locals 1

    .prologue
    .line 1536360
    invoke-direct {p0, p1, p2}, LX/9mu;->a(Lcom/facebook/react/cxxbridge/JavaScriptExecutor;LX/346;)LX/5pY;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/react/cxxbridge/JavaScriptExecutor;LX/346;)LX/5pY;
    .locals 10

    .prologue
    const-wide/16 v8, 0x2000

    .line 1536361
    const-string v0, "CREATE_REACT_CONTEXT_START"

    invoke-static {v0}, Lcom/facebook/react/bridge/ReactMarker;->logMarker(Ljava/lang/String;)V

    .line 1536362
    invoke-virtual {p2}, LX/346;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/9mu;->o:Ljava/lang/String;

    .line 1536363
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1536364
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 1536365
    new-instance v5, LX/5pK;

    invoke-direct {v5}, LX/5pK;-><init>()V

    .line 1536366
    new-instance v2, LX/5pY;

    iget-object v0, p0, LX/9mu;->m:Landroid/content/Context;

    invoke-direct {v2, v0}, LX/5pY;-><init>(Landroid/content/Context;)V

    .line 1536367
    iget-boolean v0, p0, LX/9mu;->j:Z

    if-eqz v0, :cond_0

    .line 1536368
    iget-object v0, p0, LX/9mu;->i:LX/5qI;

    .line 1536369
    iput-object v0, v2, LX/5pX;->i:LX/0o1;

    .line 1536370
    :cond_0
    const-string v0, "PROCESS_PACKAGES_START"

    invoke-static {v0}, Lcom/facebook/react/bridge/ReactMarker;->logMarker(Ljava/lang/String;)V

    .line 1536371
    const-string v0, "createAndProcessCoreModulesPackage"

    invoke-static {v8, v9, v0}, LX/018;->a(JLjava/lang/String;)V

    .line 1536372
    :try_start_0
    new-instance v1, Lcom/facebook/react/CoreModulesPackage;

    iget-object v0, p0, LX/9mu;->z:LX/98l;

    iget-object v6, p0, LX/9mu;->s:LX/344;

    invoke-direct {v1, p0, v0, v6}, Lcom/facebook/react/CoreModulesPackage;-><init>(LX/33y;LX/98l;LX/344;)V

    move-object v0, p0

    .line 1536373
    invoke-direct/range {v0 .. v5}, LX/9mu;->a(LX/348;LX/5pY;Ljava/util/List;Ljava/util/Map;LX/5pK;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1536374
    invoke-static {v8, v9}, LX/018;->a(J)V

    .line 1536375
    iget-object v0, p0, LX/9mu;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/348;

    .line 1536376
    const-string v0, "createAndProcessCustomReactPackage"

    invoke-static {v8, v9, v0}, LX/018;->a(JLjava/lang/String;)V

    move-object v0, p0

    .line 1536377
    :try_start_1
    invoke-direct/range {v0 .. v5}, LX/9mu;->a(LX/348;LX/5pY;Ljava/util/List;Ljava/util/Map;LX/5pK;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1536378
    invoke-static {v8, v9}, LX/018;->a(J)V

    goto :goto_0

    .line 1536379
    :catchall_0
    move-exception v0

    invoke-static {v8, v9}, LX/018;->a(J)V

    throw v0

    .line 1536380
    :catchall_1
    move-exception v0

    invoke-static {v8, v9}, LX/018;->a(J)V

    throw v0

    .line 1536381
    :cond_1
    const-string v0, "PROCESS_PACKAGES_END"

    invoke-static {v0}, Lcom/facebook/react/bridge/ReactMarker;->logMarker(Ljava/lang/String;)V

    .line 1536382
    const-string v0, "BUILD_NATIVE_MODULE_REGISTRY_START"

    invoke-static {v0}, Lcom/facebook/react/bridge/ReactMarker;->logMarker(Ljava/lang/String;)V

    .line 1536383
    const-string v0, "buildNativeModuleRegistry"

    invoke-static {v8, v9, v0}, LX/018;->a(JLjava/lang/String;)V

    .line 1536384
    :try_start_2
    new-instance v1, LX/5qE;

    invoke-direct {v1, v3, v4}, LX/5qE;-><init>(Ljava/util/List;Ljava/util/Map;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1536385
    invoke-static {v8, v9}, LX/018;->a(J)V

    .line 1536386
    const-string v0, "BUILD_NATIVE_MODULE_REGISTRY_END"

    invoke-static {v0}, Lcom/facebook/react/bridge/ReactMarker;->logMarker(Ljava/lang/String;)V

    .line 1536387
    iget-object v0, p0, LX/9mu;->u:LX/0o1;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/9mu;->u:LX/0o1;

    .line 1536388
    :goto_1
    new-instance v3, LX/5q1;

    invoke-direct {v3}, LX/5q1;-><init>()V

    invoke-static {}, LX/5pn;->c()LX/5pn;

    move-result-object v4

    .line 1536389
    iput-object v4, v3, LX/5q1;->a:LX/5pn;

    .line 1536390
    move-object v3, v3

    .line 1536391
    iput-object p1, v3, LX/5q1;->e:Lcom/facebook/react/cxxbridge/JavaScriptExecutor;

    .line 1536392
    move-object v3, v3

    .line 1536393
    iput-object v1, v3, LX/5q1;->c:LX/5qE;

    .line 1536394
    move-object v1, v3

    .line 1536395
    invoke-virtual {v5}, LX/5pK;->a()LX/5pM;

    move-result-object v3

    .line 1536396
    iput-object v3, v1, LX/5q1;->d:LX/5pM;

    .line 1536397
    move-object v1, v1

    .line 1536398
    iput-object p2, v1, LX/5q1;->b:LX/346;

    .line 1536399
    move-object v1, v1

    .line 1536400
    iput-object v0, v1, LX/5q1;->f:LX/0o1;

    .line 1536401
    move-object v0, v1

    .line 1536402
    const-string v1, "CREATE_CATALYST_INSTANCE_START"

    invoke-static {v1}, Lcom/facebook/react/bridge/ReactMarker;->logMarker(Ljava/lang/String;)V

    .line 1536403
    const-string v1, "createCatalystInstance"

    invoke-static {v8, v9, v1}, LX/018;->a(JLjava/lang/String;)V

    .line 1536404
    :try_start_3
    invoke-virtual {v0}, LX/5q1;->a()Lcom/facebook/react/cxxbridge/CatalystInstanceImpl;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    move-result-object v0

    .line 1536405
    invoke-static {v8, v9}, LX/018;->a(J)V

    .line 1536406
    const-string v1, "CREATE_CATALYST_INSTANCE_END"

    invoke-static {v1}, Lcom/facebook/react/bridge/ReactMarker;->logMarker(Ljava/lang/String;)V

    .line 1536407
    iget-object v1, p0, LX/9mu;->k:LX/5pU;

    if-eqz v1, :cond_2

    .line 1536408
    iget-object v1, p0, LX/9mu;->k:LX/5pU;

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/CatalystInstance;->a(LX/5pU;)V

    .line 1536409
    :cond_2
    invoke-virtual {v2, v0}, LX/5pX;->a(Lcom/facebook/react/bridge/CatalystInstance;)V

    .line 1536410
    invoke-interface {v0}, Lcom/facebook/react/bridge/CatalystInstance;->a()V

    .line 1536411
    return-object v2

    .line 1536412
    :catchall_2
    move-exception v0

    invoke-static {v8, v9}, LX/018;->a(J)V

    .line 1536413
    const-string v1, "BUILD_NATIVE_MODULE_REGISTRY_END"

    invoke-static {v1}, Lcom/facebook/react/bridge/ReactMarker;->logMarker(Ljava/lang/String;)V

    throw v0

    .line 1536414
    :cond_3
    iget-object v0, p0, LX/9mu;->i:LX/5qI;

    goto :goto_1

    .line 1536415
    :catchall_3
    move-exception v0

    invoke-static {v8, v9}, LX/018;->a(J)V

    .line 1536416
    const-string v1, "CREATE_CATALYST_INSTANCE_END"

    invoke-static {v1}, Lcom/facebook/react/bridge/ReactMarker;->logMarker(Ljava/lang/String;)V

    throw v0
.end method

.method private a(LX/348;LX/5pY;Ljava/util/List;Ljava/util/Map;LX/5pK;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/348;",
            "LX/5pY;",
            "Ljava/util/List",
            "<",
            "LX/5pS;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class;",
            "LX/5qQ;",
            ">;",
            "LX/5pK;",
            ")V"
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x2000

    .line 1536417
    const-string v0, "processPackage"

    invoke-static {v6, v7, v0}, LX/0BL;->a(JLjava/lang/String;)LX/0BN;

    move-result-object v0

    const-string v1, "className"

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0BN;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0BN;

    move-result-object v0

    invoke-virtual {v0}, LX/0BN;->a()V

    .line 1536418
    iget-boolean v0, p0, LX/9mu;->w:Z

    if-eqz v0, :cond_2

    instance-of v0, p1, LX/9mi;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 1536419
    check-cast v0, LX/9mi;

    .line 1536420
    invoke-virtual {v0}, LX/9mi;->b()LX/5qR;

    move-result-object v1

    .line 1536421
    invoke-interface {v1}, LX/5qR;->a()Ljava/util/Map;

    move-result-object v1

    .line 1536422
    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1536423
    invoke-interface {p4, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 1536424
    :cond_0
    invoke-virtual {v0, p2}, LX/9mi;->a(LX/5pY;)Ljava/util/List;

    move-result-object v0

    invoke-interface {p3, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1536425
    :cond_1
    invoke-interface {p1}, LX/348;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 1536426
    invoke-virtual {p5, v0}, LX/5pK;->a(Ljava/lang/Class;)LX/5pK;

    goto :goto_0

    .line 1536427
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is not a LazyReactPackage, falling back to old version."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1536428
    invoke-interface {p1, p2}, LX/348;->b(LX/5pY;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5p4;

    .line 1536429
    new-instance v2, LX/5pS;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    new-instance v4, LX/9mj;

    invoke-direct {v4, v0}, LX/9mj;-><init>(LX/5p4;)V

    invoke-direct {v2, v3, v4}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {p3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1536430
    :cond_3
    invoke-static {v6, v7}, LX/018;->a(J)V

    .line 1536431
    return-void
.end method

.method private static a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1536432
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/01L;->a(Landroid/content/Context;Z)V

    .line 1536433
    return-void
.end method

.method private static a(Lcom/facebook/react/ReactRootView;Lcom/facebook/react/bridge/CatalystInstance;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x2000

    .line 1536434
    const-string v0, "attachMeasuredRootViewToInstance"

    invoke-static {v8, v9, v0}, LX/018;->a(JLjava/lang/String;)V

    .line 1536435
    invoke-static {}, LX/5qG;->b()V

    .line 1536436
    invoke-virtual {p0}, Lcom/facebook/react/ReactRootView;->removeAllViews()V

    .line 1536437
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/facebook/react/ReactRootView;->setId(I)V

    .line 1536438
    const-class v0, LX/5rQ;

    invoke-interface {p1, v0}, Lcom/facebook/react/bridge/CatalystInstance;->b(Ljava/lang/Class;)LX/5p4;

    move-result-object v0

    check-cast v0, LX/5rQ;

    .line 1536439
    invoke-virtual {v0, p0}, LX/5rQ;->a(LX/5rH;)I

    move-result v0

    .line 1536440
    iput v0, p0, Lcom/facebook/react/ReactRootView;->e:I

    .line 1536441
    iget-object v1, p0, Lcom/facebook/react/ReactRootView;->c:Landroid/os/Bundle;

    move-object v1, v1

    .line 1536442
    invoke-static {v1}, LX/5py;->a(Landroid/os/Bundle;)Lcom/facebook/react/bridge/WritableNativeMap;

    move-result-object v1

    .line 1536443
    invoke-virtual {p0}, Lcom/facebook/react/ReactRootView;->getJSModuleName()Ljava/lang/String;

    move-result-object v2

    .line 1536444
    new-instance v3, Lcom/facebook/react/bridge/WritableNativeMap;

    invoke-direct {v3}, Lcom/facebook/react/bridge/WritableNativeMap;-><init>()V

    .line 1536445
    const-string v4, "rootTag"

    int-to-double v6, v0

    invoke-virtual {v3, v4, v6, v7}, Lcom/facebook/react/bridge/WritableNativeMap;->putDouble(Ljava/lang/String;D)V

    .line 1536446
    const-string v0, "initialProps"

    invoke-virtual {v3, v0, v1}, Lcom/facebook/react/bridge/WritableNativeMap;->a(Ljava/lang/String;LX/5pH;)V

    .line 1536447
    const-class v0, Lcom/facebook/react/uimanager/AppRegistry;

    invoke-interface {p1, v0}, Lcom/facebook/react/bridge/CatalystInstance;->a(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/uimanager/AppRegistry;

    invoke-interface {v0, v2, v3}, Lcom/facebook/react/uimanager/AppRegistry;->runApplication(Ljava/lang/String;LX/5pH;)V

    .line 1536448
    invoke-static {v8, v9}, LX/018;->a(J)V

    .line 1536449
    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 1536450
    iget-object v0, p0, LX/9mu;->l:LX/5pX;

    if-eqz v0, :cond_1

    .line 1536451
    if-nez p1, :cond_0

    iget-object v0, p0, LX/9mu;->c:LX/341;

    sget-object v1, LX/341;->BEFORE_RESUME:LX/341;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/9mu;->c:LX/341;

    sget-object v1, LX/341;->BEFORE_CREATE:LX/341;

    if-ne v0, v1, :cond_1

    .line 1536452
    :cond_0
    iget-object v0, p0, LX/9mu;->l:LX/5pX;

    iget-object v1, p0, LX/9mu;->p:Landroid/app/Activity;

    invoke-virtual {v0, v1}, LX/5pX;->a(Landroid/app/Activity;)V

    .line 1536453
    :cond_1
    sget-object v0, LX/341;->RESUMED:LX/341;

    iput-object v0, p0, LX/9mu;->c:LX/341;

    .line 1536454
    return-void
.end method

.method public static a$redex0(LX/9mu;LX/5pX;)V
    .locals 3

    .prologue
    .line 1536455
    invoke-static {}, LX/5qG;->b()V

    .line 1536456
    iget-object v0, p0, LX/9mu;->c:LX/341;

    sget-object v1, LX/341;->RESUMED:LX/341;

    if-ne v0, v1, :cond_0

    .line 1536457
    invoke-virtual {p1}, LX/5pX;->c()V

    .line 1536458
    :cond_0
    iget-object v0, p0, LX/9mu;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/ReactRootView;

    .line 1536459
    invoke-virtual {p1}, LX/5pX;->a()Lcom/facebook/react/bridge/CatalystInstance;

    move-result-object v2

    invoke-static {v0, v2}, LX/9mu;->b(Lcom/facebook/react/ReactRootView;Lcom/facebook/react/bridge/CatalystInstance;)V

    goto :goto_0

    .line 1536460
    :cond_1
    invoke-virtual {p1}, LX/5pX;->e()V

    .line 1536461
    iget-object v0, p0, LX/9mu;->t:LX/349;

    invoke-virtual {p1}, LX/5pX;->a()Lcom/facebook/react/bridge/CatalystInstance;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/349;->b(LX/33w;)V

    .line 1536462
    return-void
.end method

.method public static a$redex0(LX/9mu;LX/5q8;LX/346;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1536463
    invoke-static {}, LX/5qG;->b()V

    .line 1536464
    new-instance v0, LX/9ms;

    invoke-direct {v0, p0, p1, p2}, LX/9ms;-><init>(LX/9mu;LX/5q8;LX/346;)V

    .line 1536465
    iget-object v1, p0, LX/9mu;->e:LX/9mr;

    if-nez v1, :cond_0

    .line 1536466
    new-instance v1, LX/9mr;

    invoke-direct {v1, p0}, LX/9mr;-><init>(LX/9mu;)V

    iput-object v1, p0, LX/9mu;->e:LX/9mr;

    .line 1536467
    iget-object v1, p0, LX/9mu;->e:LX/9mr;

    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v3, 0x1

    new-array v3, v3, [LX/9ms;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, LX/9mr;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1536468
    :goto_0
    return-void

    .line 1536469
    :cond_0
    iput-object v0, p0, LX/9mu;->d:LX/9ms;

    goto :goto_0
.end method

.method public static b(LX/9mu;LX/5pY;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x2000

    const/4 v2, 0x0

    .line 1536470
    const-string v0, "SETUP_REACT_CONTEXT_START"

    invoke-static {v0}, Lcom/facebook/react/bridge/ReactMarker;->logMarker(Ljava/lang/String;)V

    .line 1536471
    const-string v0, "setupReactContext"

    invoke-static {v4, v5, v0}, LX/018;->a(JLjava/lang/String;)V

    .line 1536472
    invoke-static {}, LX/5qG;->b()V

    .line 1536473
    iget-object v0, p0, LX/9mu;->l:LX/5pX;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0nE;->a(Z)V

    .line 1536474
    invoke-static {p1}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5pX;

    iput-object v0, p0, LX/9mu;->l:LX/5pX;

    .line 1536475
    invoke-virtual {p1}, LX/5pX;->a()Lcom/facebook/react/bridge/CatalystInstance;

    move-result-object v0

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/bridge/CatalystInstance;

    .line 1536476
    invoke-interface {v0}, Lcom/facebook/react/bridge/CatalystInstance;->d()V

    .line 1536477
    iget-object v1, p0, LX/9mu;->t:LX/349;

    invoke-virtual {v1, v0}, LX/349;->a(LX/33w;)V

    .line 1536478
    invoke-direct {p0}, LX/9mu;->t()V

    .line 1536479
    iget-object v1, p0, LX/9mu;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/react/ReactRootView;

    .line 1536480
    invoke-static {v1, v0}, LX/9mu;->a(Lcom/facebook/react/ReactRootView;Lcom/facebook/react/bridge/CatalystInstance;)V

    goto :goto_1

    :cond_0
    move v0, v2

    .line 1536481
    goto :goto_0

    .line 1536482
    :cond_1
    iget-object v0, p0, LX/9mu;->q:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    new-array v0, v0, [LX/9mm;

    .line 1536483
    iget-object v1, p0, LX/9mu;->q:Ljava/util/Collection;

    invoke-interface {v1, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9mm;

    .line 1536484
    array-length v1, v0

    :goto_2
    if-ge v2, v1, :cond_2

    aget-object v3, v0, v2

    .line 1536485
    invoke-interface {v3, p1}, LX/9mm;->a(LX/5pX;)V

    .line 1536486
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1536487
    :cond_2
    invoke-static {v4, v5}, LX/018;->a(J)V

    .line 1536488
    const-string v0, "SETUP_REACT_CONTEXT_END"

    invoke-static {v0}, Lcom/facebook/react/bridge/ReactMarker;->logMarker(Ljava/lang/String;)V

    .line 1536489
    return-void
.end method

.method private static b(Lcom/facebook/react/ReactRootView;Lcom/facebook/react/bridge/CatalystInstance;)V
    .locals 2

    .prologue
    .line 1536490
    invoke-static {}, LX/5qG;->b()V

    .line 1536491
    const-class v0, Lcom/facebook/react/uimanager/AppRegistry;

    invoke-interface {p1, v0}, Lcom/facebook/react/bridge/CatalystInstance;->a(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/uimanager/AppRegistry;

    invoke-virtual {p0}, Lcom/facebook/react/ReactRootView;->getId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/facebook/react/uimanager/AppRegistry;->unmountApplicationComponentAtRootTag(I)V

    .line 1536492
    return-void
.end method

.method private n()V
    .locals 2

    .prologue
    .line 1536493
    invoke-static {}, LX/5qG;->b()V

    .line 1536494
    iget-boolean v0, p0, LX/9mu;->j:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/9mu;->g:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1536495
    iget-object v0, p0, LX/9mu;->i:LX/5qI;

    invoke-interface {v0}, LX/5qI;->b()LX/5qT;

    move-result-object v0

    .line 1536496
    iget-object v1, p0, LX/9mu;->i:LX/5qI;

    invoke-interface {v1}, LX/5qI;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, LX/5qT;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1536497
    invoke-direct {p0}, LX/9mu;->s()V

    .line 1536498
    :cond_0
    :goto_0
    return-void

    .line 1536499
    :cond_1
    iget-object v1, p0, LX/9mu;->f:LX/346;

    if-eqz v1, :cond_0

    .line 1536500
    new-instance v1, LX/9mq;

    invoke-direct {v1, p0, v0}, LX/9mq;-><init>(LX/9mu;LX/5qT;)V

    goto :goto_0

    .line 1536501
    :cond_2
    invoke-direct {p0}, LX/9mu;->o()V

    goto :goto_0
.end method

.method private o()V
    .locals 2

    .prologue
    .line 1536502
    new-instance v0, LX/5q9;

    iget-object v1, p0, LX/9mu;->v:LX/342;

    invoke-interface {v1}, LX/342;->a()Lcom/facebook/react/bridge/WritableNativeMap;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5q9;-><init>(Lcom/facebook/react/bridge/WritableNativeMap;)V

    iget-object v1, p0, LX/9mu;->f:LX/346;

    invoke-static {p0, v0, v1}, LX/9mu;->a$redex0(LX/9mu;LX/5q8;LX/346;)V

    .line 1536503
    return-void
.end method

.method public static p(LX/9mu;)V
    .locals 1

    .prologue
    .line 1536264
    invoke-static {}, LX/5qG;->b()V

    .line 1536265
    iget-object v0, p0, LX/9mu;->n:LX/98l;

    if-eqz v0, :cond_0

    .line 1536266
    iget-object v0, p0, LX/9mu;->n:LX/98l;

    invoke-interface {v0}, LX/98l;->a()V

    .line 1536267
    :cond_0
    return-void
.end method

.method private q()V
    .locals 2

    .prologue
    .line 1536504
    iget-object v0, p0, LX/9mu;->l:LX/5pX;

    if-eqz v0, :cond_0

    .line 1536505
    iget-object v0, p0, LX/9mu;->c:LX/341;

    sget-object v1, LX/341;->BEFORE_CREATE:LX/341;

    if-ne v0, v1, :cond_1

    .line 1536506
    iget-object v0, p0, LX/9mu;->l:LX/5pX;

    iget-object v1, p0, LX/9mu;->p:Landroid/app/Activity;

    invoke-virtual {v0, v1}, LX/5pX;->a(Landroid/app/Activity;)V

    .line 1536507
    iget-object v0, p0, LX/9mu;->l:LX/5pX;

    invoke-virtual {v0}, LX/5pX;->c()V

    .line 1536508
    :cond_0
    :goto_0
    sget-object v0, LX/341;->BEFORE_RESUME:LX/341;

    iput-object v0, p0, LX/9mu;->c:LX/341;

    .line 1536509
    return-void

    .line 1536510
    :cond_1
    iget-object v0, p0, LX/9mu;->c:LX/341;

    sget-object v1, LX/341;->RESUMED:LX/341;

    if-ne v0, v1, :cond_0

    .line 1536511
    iget-object v0, p0, LX/9mu;->l:LX/5pX;

    invoke-virtual {v0}, LX/5pX;->c()V

    goto :goto_0
.end method

.method private r()V
    .locals 2

    .prologue
    .line 1536512
    iget-object v0, p0, LX/9mu;->l:LX/5pX;

    if-eqz v0, :cond_1

    .line 1536513
    iget-object v0, p0, LX/9mu;->c:LX/341;

    sget-object v1, LX/341;->RESUMED:LX/341;

    if-ne v0, v1, :cond_0

    .line 1536514
    iget-object v0, p0, LX/9mu;->l:LX/5pX;

    invoke-virtual {v0}, LX/5pX;->c()V

    .line 1536515
    sget-object v0, LX/341;->BEFORE_RESUME:LX/341;

    iput-object v0, p0, LX/9mu;->c:LX/341;

    .line 1536516
    :cond_0
    iget-object v0, p0, LX/9mu;->c:LX/341;

    sget-object v1, LX/341;->BEFORE_RESUME:LX/341;

    if-ne v0, v1, :cond_1

    .line 1536517
    iget-object v0, p0, LX/9mu;->l:LX/5pX;

    invoke-virtual {v0}, LX/5pX;->d()V

    .line 1536518
    :cond_1
    sget-object v0, LX/341;->BEFORE_CREATE:LX/341;

    iput-object v0, p0, LX/9mu;->c:LX/341;

    .line 1536519
    return-void
.end method

.method private s()V
    .locals 3

    .prologue
    .line 1536520
    new-instance v0, LX/5q9;

    iget-object v1, p0, LX/9mu;->v:LX/342;

    invoke-interface {v1}, LX/342;->a()Lcom/facebook/react/bridge/WritableNativeMap;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5q9;-><init>(Lcom/facebook/react/bridge/WritableNativeMap;)V

    iget-object v1, p0, LX/9mu;->i:LX/5qI;

    invoke-interface {v1}, LX/5qI;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/9mu;->i:LX/5qI;

    invoke-interface {v2}, LX/5qI;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/346;->a(Ljava/lang/String;Ljava/lang/String;)LX/346;

    move-result-object v1

    invoke-static {p0, v0, v1}, LX/9mu;->a$redex0(LX/9mu;LX/5q8;LX/346;)V

    .line 1536521
    return-void
.end method

.method private t()V
    .locals 2

    .prologue
    .line 1536330
    iget-object v0, p0, LX/9mu;->c:LX/341;

    sget-object v1, LX/341;->RESUMED:LX/341;

    if-ne v0, v1, :cond_0

    .line 1536331
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LX/9mu;->a(Z)V

    .line 1536332
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()LX/5qI;
    .locals 1

    .prologue
    .line 1536522
    iget-object v0, p0, LX/9mu;->i:LX/5qI;

    return-object v0
.end method

.method public final a(LX/5pY;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5pY;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/react/uimanager/ViewManager;",
            ">;"
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x2000

    .line 1536255
    const-string v0, "CREATE_VIEW_MANAGERS_START"

    invoke-static {v0}, Lcom/facebook/react/bridge/ReactMarker;->logMarker(Ljava/lang/String;)V

    .line 1536256
    const-string v0, "createAllViewManagers"

    invoke-static {v4, v5, v0}, LX/018;->a(JLjava/lang/String;)V

    .line 1536257
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1536258
    iget-object v0, p0, LX/9mu;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/348;

    .line 1536259
    invoke-interface {v0, p1}, LX/348;->c(LX/5pY;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1536260
    :catchall_0
    move-exception v0

    invoke-static {v4, v5}, LX/018;->a(J)V

    .line 1536261
    const-string v1, "CREATE_VIEW_MANAGERS_END"

    invoke-static {v1}, Lcom/facebook/react/bridge/ReactMarker;->logMarker(Ljava/lang/String;)V

    throw v0

    .line 1536262
    :cond_0
    invoke-static {v4, v5}, LX/018;->a(J)V

    .line 1536263
    const-string v0, "CREATE_VIEW_MANAGERS_END"

    invoke-static {v0}, Lcom/facebook/react/bridge/ReactMarker;->logMarker(Ljava/lang/String;)V

    return-object v1
.end method

.method public final a(LX/9mm;)V
    .locals 1

    .prologue
    .line 1536268
    iget-object v0, p0, LX/9mu;->q:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 1536269
    return-void
.end method

.method public final a(Landroid/app/Activity;IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1536270
    iget-object v0, p0, LX/9mu;->l:LX/5pX;

    if-eqz v0, :cond_0

    .line 1536271
    iget-object v0, p0, LX/9mu;->l:LX/5pX;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/5pX;->a(Landroid/app/Activity;IILandroid/content/Intent;)V

    .line 1536272
    :cond_0
    return-void
.end method

.method public final a(Landroid/app/Activity;LX/98l;)V
    .locals 1

    .prologue
    .line 1536273
    invoke-static {}, LX/5qG;->b()V

    .line 1536274
    iput-object p2, p0, LX/9mu;->n:LX/98l;

    .line 1536275
    iput-object p1, p0, LX/9mu;->p:Landroid/app/Activity;

    .line 1536276
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/9mu;->a(Z)V

    .line 1536277
    return-void
.end method

.method public final a(Lcom/facebook/react/ReactRootView;)V
    .locals 1

    .prologue
    .line 1536278
    invoke-static {}, LX/5qG;->b()V

    .line 1536279
    iget-object v0, p0, LX/9mu;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1536280
    iget-object v0, p0, LX/9mu;->e:LX/9mr;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/9mu;->l:LX/5pX;

    if-eqz v0, :cond_0

    .line 1536281
    iget-object v0, p0, LX/9mu;->l:LX/5pX;

    invoke-virtual {v0}, LX/5pX;->a()Lcom/facebook/react/bridge/CatalystInstance;

    move-result-object v0

    invoke-static {p1, v0}, LX/9mu;->a(Lcom/facebook/react/ReactRootView;Lcom/facebook/react/bridge/CatalystInstance;)V

    .line 1536282
    :cond_0
    return-void
.end method

.method public final b()LX/349;
    .locals 1

    .prologue
    .line 1536283
    iget-object v0, p0, LX/9mu;->t:LX/349;

    return-object v0
.end method

.method public final b(LX/9mm;)V
    .locals 1

    .prologue
    .line 1536284
    iget-object v0, p0, LX/9mu;->q:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 1536285
    return-void
.end method

.method public final b(Lcom/facebook/react/ReactRootView;)V
    .locals 1

    .prologue
    .line 1536286
    invoke-static {}, LX/5qG;->b()V

    .line 1536287
    iget-object v0, p0, LX/9mu;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1536288
    iget-object v0, p0, LX/9mu;->l:LX/5pX;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9mu;->l:LX/5pX;

    invoke-virtual {v0}, LX/5pX;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1536289
    iget-object v0, p0, LX/9mu;->l:LX/5pX;

    invoke-virtual {v0}, LX/5pX;->a()Lcom/facebook/react/bridge/CatalystInstance;

    move-result-object v0

    invoke-static {p1, v0}, LX/9mu;->b(Lcom/facebook/react/ReactRootView;Lcom/facebook/react/bridge/CatalystInstance;)V

    .line 1536290
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1536291
    iget-boolean v0, p0, LX/9mu;->r:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v2, "createReactContextInBackground should only be called when creating the react application for the first time. When reloading JS, e.g. from a new file, explicitlyuse recreateReactContextInBackground"

    invoke-static {v0, v2}, LX/0nE;->a(ZLjava/lang/String;)V

    .line 1536292
    iput-boolean v1, p0, LX/9mu;->r:Z

    .line 1536293
    invoke-direct {p0}, LX/9mu;->n()V

    .line 1536294
    return-void

    .line 1536295
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1536296
    iget-boolean v0, p0, LX/9mu;->r:Z

    return v0
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 1536297
    invoke-static {}, LX/5qG;->b()V

    .line 1536298
    iget-object v0, p0, LX/9mu;->l:LX/5pX;

    .line 1536299
    iget-object v1, p0, LX/9mu;->l:LX/5pX;

    if-nez v1, :cond_0

    .line 1536300
    const-string v0, "React"

    const-string v1, "Instance detached from instance manager"

    invoke-static {v0, v1}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1536301
    invoke-static {p0}, LX/9mu;->p(LX/9mu;)V

    .line 1536302
    :goto_0
    return-void

    .line 1536303
    :cond_0
    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5pX;

    const-class v1, LX/9mz;

    invoke-virtual {v0, v1}, LX/5pX;->b(Ljava/lang/Class;)LX/5p4;

    move-result-object v0

    check-cast v0, LX/9mz;

    .line 1536304
    invoke-virtual {v0}, LX/9mz;->h()V

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 1536305
    invoke-static {}, LX/5qG;->b()V

    .line 1536306
    const/4 v0, 0x0

    iput-object v0, p0, LX/9mu;->n:LX/98l;

    .line 1536307
    invoke-direct {p0}, LX/9mu;->q()V

    .line 1536308
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 1536309
    invoke-static {}, LX/5qG;->b()V

    .line 1536310
    invoke-direct {p0}, LX/9mu;->r()V

    .line 1536311
    const/4 v0, 0x0

    iput-object v0, p0, LX/9mu;->p:Landroid/app/Activity;

    .line 1536312
    return-void
.end method

.method public final h()V
    .locals 0

    .prologue
    .line 1536313
    invoke-static {}, LX/5qG;->b()V

    .line 1536314
    return-void
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1536315
    iget-object v0, p0, LX/9mu;->o:Ljava/lang/String;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final j()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1536316
    invoke-static {}, LX/5qG;->b()V

    .line 1536317
    invoke-direct {p0}, LX/9mu;->r()V

    .line 1536318
    iget-object v0, p0, LX/9mu;->e:LX/9mr;

    if-eqz v0, :cond_0

    .line 1536319
    iget-object v0, p0, LX/9mu;->e:LX/9mr;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/9mr;->cancel(Z)Z

    .line 1536320
    :cond_0
    iget-object v0, p0, LX/9mu;->t:LX/349;

    iget-object v1, p0, LX/9mu;->m:Landroid/content/Context;

    invoke-virtual {v0, v1}, LX/349;->a(Landroid/content/Context;)V

    .line 1536321
    iget-object v0, p0, LX/9mu;->l:LX/5pX;

    if-eqz v0, :cond_1

    .line 1536322
    iget-object v0, p0, LX/9mu;->l:LX/5pX;

    invoke-virtual {v0}, LX/5pX;->e()V

    .line 1536323
    iput-object v2, p0, LX/9mu;->l:LX/5pX;

    .line 1536324
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/9mu;->r:Z

    .line 1536325
    :cond_1
    iput-object v2, p0, LX/9mu;->p:Landroid/app/Activity;

    .line 1536326
    invoke-static {}, LX/9nS;->a()LX/9nS;

    move-result-object v0

    invoke-virtual {v0}, LX/9nS;->b()V

    .line 1536327
    return-void
.end method

.method public final k()LX/5pX;
    .locals 1
    .annotation build Lcom/facebook/react/common/annotations/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1536328
    iget-object v0, p0, LX/9mu;->l:LX/5pX;

    return-object v0
.end method

.method public final l()LX/341;
    .locals 1

    .prologue
    .line 1536329
    iget-object v0, p0, LX/9mu;->c:LX/341;

    return-object v0
.end method
