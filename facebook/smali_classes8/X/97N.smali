.class public final LX/97N;
.super LX/0zP;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0zP",
        "<",
        "Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionMutationsModels$PlaceQuestionAnswerSubmitModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 12

    .prologue
    .line 1441525
    const-class v1, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionMutationsModels$PlaceQuestionAnswerSubmitModel;

    const v0, -0x734d2a90

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "PlaceQuestionAnswerSubmit"

    const-string v6, "62da53d7dbf8cb83c97653c85b0eef90"

    const-string v7, "place_question_submit_answer"

    const-string v8, "0"

    const-string v9, "10155215934116729"

    const/4 v10, 0x0

    .line 1441526
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v11, v0

    .line 1441527
    move-object v0, p0

    invoke-direct/range {v0 .. v11}, LX/0zP;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1441528
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1441519
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1441520
    sparse-switch v0, :sswitch_data_0

    .line 1441521
    :goto_0
    return-object p1

    .line 1441522
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1441523
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1441524
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x5fb57ca -> :sswitch_0
        0x294a4578 -> :sswitch_2
        0x3345d4af -> :sswitch_1
    .end sparse-switch
.end method
