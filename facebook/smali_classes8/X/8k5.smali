.class public final LX/8k5;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 1395452
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_3

    .line 1395453
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1395454
    :goto_0
    return v1

    .line 1395455
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1395456
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_2

    .line 1395457
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1395458
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1395459
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 1395460
    const-string v3, "owned_packs"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1395461
    const/4 v2, 0x0

    .line 1395462
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_8

    .line 1395463
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1395464
    :goto_2
    move v0, v2

    .line 1395465
    goto :goto_1

    .line 1395466
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1395467
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1395468
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    .line 1395469
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1395470
    :cond_5
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_7

    .line 1395471
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1395472
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1395473
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_5

    if-eqz v4, :cond_5

    .line 1395474
    const-string v5, "nodes"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1395475
    invoke-static {p0, p1}, LX/8kR;->b(LX/15w;LX/186;)I

    move-result v3

    goto :goto_3

    .line 1395476
    :cond_6
    const-string v5, "page_info"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1395477
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1395478
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v6, :cond_e

    .line 1395479
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1395480
    :goto_4
    move v0, v4

    .line 1395481
    goto :goto_3

    .line 1395482
    :cond_7
    const/4 v4, 0x2

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1395483
    invoke-virtual {p1, v2, v3}, LX/186;->b(II)V

    .line 1395484
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1395485
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_2

    :cond_8
    move v0, v2

    move v3, v2

    goto :goto_3

    .line 1395486
    :cond_9
    const-string v9, "has_next_page"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 1395487
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v6, v0

    move v0, v5

    .line 1395488
    :cond_a
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_c

    .line 1395489
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1395490
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1395491
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_a

    if-eqz v8, :cond_a

    .line 1395492
    const-string v9, "end_cursor"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 1395493
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_5

    .line 1395494
    :cond_b
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_5

    .line 1395495
    :cond_c
    const/4 v8, 0x2

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1395496
    invoke-virtual {p1, v4, v7}, LX/186;->b(II)V

    .line 1395497
    if-eqz v0, :cond_d

    .line 1395498
    invoke-virtual {p1, v5, v6}, LX/186;->a(IZ)V

    .line 1395499
    :cond_d
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto :goto_4

    :cond_e
    move v0, v4

    move v6, v4

    move v7, v4

    goto :goto_5
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1395500
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1395501
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1395502
    if-eqz v0, :cond_4

    .line 1395503
    const-string v1, "owned_packs"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1395504
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1395505
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1395506
    if-eqz v1, :cond_0

    .line 1395507
    const-string p1, "nodes"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1395508
    invoke-static {p0, v1, p2, p3}, LX/8kR;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1395509
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1395510
    if-eqz v1, :cond_3

    .line 1395511
    const-string p1, "page_info"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1395512
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1395513
    const/4 p1, 0x0

    invoke-virtual {p0, v1, p1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p1

    .line 1395514
    if-eqz p1, :cond_1

    .line 1395515
    const-string v0, "end_cursor"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1395516
    invoke-virtual {p2, p1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1395517
    :cond_1
    const/4 p1, 0x1

    invoke-virtual {p0, v1, p1}, LX/15i;->b(II)Z

    move-result p1

    .line 1395518
    if-eqz p1, :cond_2

    .line 1395519
    const-string v0, "has_next_page"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1395520
    invoke-virtual {p2, p1}, LX/0nX;->a(Z)V

    .line 1395521
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1395522
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1395523
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1395524
    return-void
.end method
