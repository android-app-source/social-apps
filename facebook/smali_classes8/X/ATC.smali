.class public LX/ATC;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1675331
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1675332
    return-void
.end method

.method public static a(FLandroid/widget/FrameLayout$LayoutParams;LX/ATB;)LX/ATB;
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 1675333
    iget v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v1, p1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v0, v1

    .line 1675334
    iget v1, p2, LX/ATB;->a:I

    invoke-static {v1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    sub-int v0, v1, v0

    .line 1675335
    int-to-float v1, v0

    div-float/2addr v1, p0

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 1675336
    new-instance v2, LX/ATB;

    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-direct {v2, v0, v1}, LX/ATB;-><init>(II)V

    return-object v2
.end method

.method public static a(Landroid/view/View;Landroid/view/View;Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v9, 0x5

    const/4 v1, 0x0

    const/4 v8, 0x1

    .line 1675337
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    if-nez v0, :cond_1

    .line 1675338
    :cond_0
    :goto_0
    return-void

    .line 1675339
    :cond_1
    const/4 v0, 0x2

    new-array v2, v0, [I

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    aput v0, v2, v1

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v0

    aput v0, v2, v8

    .line 1675340
    invoke-virtual {p1, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 1675341
    aget v0, v2, v8

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v0

    .line 1675342
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 1675343
    invoke-virtual {p1, v4}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1675344
    iget v0, v4, Landroid/graphics/Rect;->top:I

    if-lt v3, v0, :cond_0

    aget v0, v2, v8

    iget v5, v4, Landroid/graphics/Rect;->bottom:I

    if-gt v0, v5, :cond_0

    .line 1675345
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    move-result v5

    add-int/2addr v5, v0

    .line 1675346
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p2}, Landroid/view/View;->getPaddingBottom()I

    move-result v6

    add-int/2addr v0, v6

    invoke-virtual {p2}, Landroid/view/View;->getPaddingTop()I

    move-result v6

    add-int/2addr v0, v6

    mul-int/lit8 v0, v0, 0x2

    .line 1675347
    :goto_1
    iget v6, v4, Landroid/graphics/Rect;->top:I

    aget v7, v2, v8

    sub-int/2addr v6, v7

    if-le v6, v9, :cond_2

    iget v1, v4, Landroid/graphics/Rect;->top:I

    aget v2, v2, v8

    sub-int/2addr v1, v2

    .line 1675348
    :cond_2
    iget v2, v4, Landroid/graphics/Rect;->bottom:I

    sub-int v2, v3, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    if-le v2, v9, :cond_4

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v2

    if-le v2, v0, :cond_4

    .line 1675349
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v0

    add-int/2addr v0, v1

    sub-int/2addr v0, v5

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setY(F)V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 1675350
    goto :goto_1

    .line 1675351
    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    sub-int/2addr v0, v5

    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 1675352
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    sub-int/2addr v0, v5

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setY(F)V

    goto/16 :goto_0
.end method
