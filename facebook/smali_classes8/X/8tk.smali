.class public final LX/8tk;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:LX/8tn;


# direct methods
.method public constructor <init>(LX/8tn;)V
    .locals 0

    .prologue
    .line 1413769
    iput-object p1, p0, LX/8tk;->a:LX/8tn;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 8

    .prologue
    const-wide v6, 0x4066400000000000L    # 178.0

    .line 1413770
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v2

    .line 1413771
    iget-object v0, p0, LX/8tk;->a:LX/8tn;

    iget-object v0, v0, LX/8tn;->A:LX/31M;

    .line 1413772
    iget-object v1, p0, LX/8tk;->a:LX/8tn;

    iget-object v1, v1, LX/8tn;->u:LX/8tm;

    sget-object v4, LX/8tm;->NEEDS_REVEAL:LX/8tm;

    if-eq v1, v4, :cond_0

    iget-object v1, p0, LX/8tk;->a:LX/8tn;

    iget-object v1, v1, LX/8tn;->u:LX/8tm;

    sget-object v4, LX/8tm;->DISMISSING:LX/8tm;

    if-eq v1, v4, :cond_0

    iget-object v1, p0, LX/8tk;->a:LX/8tn;

    iget-object v1, v1, LX/8tn;->t:LX/31M;

    if-eqz v1, :cond_0

    .line 1413773
    iget-object v0, p0, LX/8tk;->a:LX/8tn;

    iget-object v0, v0, LX/8tn;->t:LX/31M;

    .line 1413774
    :cond_0
    iget-object v4, p0, LX/8tk;->a:LX/8tn;

    iget-object v4, v4, LX/8tn;->p:Landroid/view/ViewGroup;

    double-to-int v5, v2

    .line 1413775
    invoke-static {v4, v0, v5}, LX/8tn;->a(Landroid/view/View;LX/31M;I)V

    .line 1413776
    iget-object v0, p0, LX/8tk;->a:LX/8tn;

    iget-boolean v0, v0, LX/8tn;->x:Z

    if-eqz v0, :cond_1

    .line 1413777
    iget-object v0, p0, LX/8tk;->a:LX/8tn;

    iget-object v0, v0, LX/8tn;->A:LX/31M;

    invoke-virtual {v0}, LX/31M;->isYAxis()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/8tk;->a:LX/8tn;

    invoke-virtual {v0}, LX/8tn;->getHeight()I

    move-result v0

    .line 1413778
    :goto_0
    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    int-to-double v0, v0

    div-double v0, v2, v0

    mul-double/2addr v0, v6

    sub-double v0, v6, v0

    double-to-int v0, v0

    .line 1413779
    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1413780
    iget-object v1, p0, LX/8tk;->a:LX/8tn;

    iget-object v1, v1, LX/8tn;->l:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1413781
    :cond_1
    iget-object v0, p0, LX/8tk;->a:LX/8tn;

    iget-object v0, v0, LX/8tn;->k:LX/0xi;

    if-eqz v0, :cond_2

    .line 1413782
    iget-object v0, p0, LX/8tk;->a:LX/8tn;

    iget-object v0, v0, LX/8tn;->k:LX/0xi;

    invoke-interface {v0, p1}, LX/0xi;->a(LX/0wd;)V

    .line 1413783
    :cond_2
    return-void

    .line 1413784
    :cond_3
    iget-object v0, p0, LX/8tk;->a:LX/8tn;

    invoke-virtual {v0}, LX/8tn;->getWidth()I

    move-result v0

    goto :goto_0
.end method

.method public final b(LX/0wd;)V
    .locals 7

    .prologue
    .line 1413785
    iget-object v1, p0, LX/8tk;->a:LX/8tn;

    iget-object v1, v1, LX/8tn;->p:Landroid/view/ViewGroup;

    invoke-static {v1}, LX/0wc;->b(Landroid/view/View;)V

    .line 1413786
    sget-object v0, LX/8tj;->a:[I

    iget-object v1, p0, LX/8tk;->a:LX/8tn;

    iget-object v1, v1, LX/8tn;->u:LX/8tm;

    invoke-virtual {v1}, LX/8tm;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1413787
    :cond_0
    :goto_0
    iget-object v0, p0, LX/8tk;->a:LX/8tn;

    iget-object v0, v0, LX/8tn;->d:LX/1nJ;

    invoke-virtual {v0}, LX/1nJ;->c()V

    .line 1413788
    iget-object v0, p0, LX/8tk;->a:LX/8tn;

    sget-object v1, LX/8tm;->AT_REST:LX/8tm;

    .line 1413789
    iput-object v1, v0, LX/8tn;->u:LX/8tm;

    .line 1413790
    return-void

    .line 1413791
    :pswitch_0
    iget-object v0, p0, LX/8tk;->a:LX/8tn;

    iget-object v0, v0, LX/8tn;->j:LX/8qU;

    if-eqz v0, :cond_0

    .line 1413792
    iget-object v0, p0, LX/8tk;->a:LX/8tn;

    iget-object v0, v0, LX/8tn;->j:LX/8qU;

    invoke-virtual {v0}, LX/8qU;->d()V

    goto :goto_0

    .line 1413793
    :pswitch_1
    iget-object v0, p0, LX/8tk;->a:LX/8tn;

    iget-object v0, v0, LX/8tn;->j:LX/8qU;

    if-eqz v0, :cond_0

    .line 1413794
    iget-object v0, p0, LX/8tk;->a:LX/8tn;

    iget-object v0, v0, LX/8tn;->j:LX/8qU;

    invoke-virtual {v0}, LX/8qU;->b()V

    .line 1413795
    iget-object v0, p0, LX/8tk;->a:LX/8tn;

    const-wide v4, 0x3f50624dd2f1a9fcL    # 0.001

    .line 1413796
    iget-object v2, v0, LX/8tn;->f:LX/0wd;

    .line 1413797
    iput-wide v4, v2, LX/0wd;->l:D

    .line 1413798
    move-object v2, v2

    .line 1413799
    iput-wide v4, v2, LX/0wd;->k:D

    .line 1413800
    iget-object v2, v0, LX/8tn;->g:LX/0wd;

    .line 1413801
    iput-wide v4, v2, LX/0wd;->l:D

    .line 1413802
    move-object v2, v2

    .line 1413803
    iput-wide v4, v2, LX/0wd;->k:D

    .line 1413804
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
