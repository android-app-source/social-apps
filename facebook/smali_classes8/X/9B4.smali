.class public final LX/9B4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/friends/ui/SmartButtonLite;

.field public final synthetic d:Lcom/facebook/feedback/reactions/ui/ReactorsRowView;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/reactions/ui/ReactorsRowView;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/friends/ui/SmartButtonLite;)V
    .locals 0

    .prologue
    .line 1451168
    iput-object p1, p0, LX/9B4;->d:Lcom/facebook/feedback/reactions/ui/ReactorsRowView;

    iput-object p2, p0, LX/9B4;->a:Ljava/lang/String;

    iput-object p3, p0, LX/9B4;->b:Ljava/lang/String;

    iput-object p4, p0, LX/9B4;->c:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, -0x55ab1020

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1451169
    iget-object v1, p0, LX/9B4;->d:Lcom/facebook/feedback/reactions/ui/ReactorsRowView;

    iget-object v1, v1, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->m:LX/9XP;

    iget-object v2, p0, LX/9B4;->a:Ljava/lang/String;

    iget-object v3, p0, LX/9B4;->b:Ljava/lang/String;

    new-instance v4, Lcom/facebook/feedback/reactions/ui/ReactorsRowView$1$1;

    invoke-direct {v4, p0}, Lcom/facebook/feedback/reactions/ui/ReactorsRowView$1$1;-><init>(LX/9B4;)V

    .line 1451170
    new-instance v6, LX/9XK;

    invoke-direct {v6}, LX/9XK;-><init>()V

    move-object v6, v6

    .line 1451171
    new-instance p0, LX/4Gc;

    invoke-direct {p0}, LX/4Gc;-><init>()V

    .line 1451172
    const-string p1, "reactor_id"

    invoke-virtual {p0, p1, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1451173
    move-object p0, p0

    .line 1451174
    const-string p1, "feedback_id"

    invoke-virtual {p0, p1, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1451175
    move-object p0, p0

    .line 1451176
    iget-object p1, v6, LX/0gW;->h:Ljava/lang/String;

    move-object p1, p1

    .line 1451177
    const-string v2, "client_mutation_id"

    invoke-virtual {p0, v2, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1451178
    move-object p0, p0

    .line 1451179
    const-string p1, "input"

    invoke-virtual {v6, p1, p0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1451180
    invoke-static {v6}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v6

    .line 1451181
    iget-object p0, v1, LX/9XP;->b:LX/0Sh;

    iget-object p1, v1, LX/9XP;->a:LX/0tX;

    invoke-virtual {p1, v6}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    new-instance p1, LX/9XO;

    invoke-direct {p1, v1, v4}, LX/9XO;-><init>(LX/9XP;Ljava/lang/Runnable;)V

    invoke-virtual {p0, v6, p1}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1451182
    const v1, 0x7e52d3c4

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
