.class public final LX/AJE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:LX/AJH;


# direct methods
.method public constructor <init>(LX/AJH;)V
    .locals 0

    .prologue
    .line 1660911
    iput-object p1, p0, LX/AJE;->a:LX/AJH;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 1660912
    iget-object v0, p0, LX/AJE;->a:LX/AJH;

    iget-object v0, v0, LX/AJH;->e:LX/7gT;

    sget-object v1, LX/7gR;->TYPE_SEARCH_FRIENDS:LX/7gR;

    invoke-virtual {v0, v1}, LX/7gT;->a(LX/7gR;)V

    .line 1660913
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1660914
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 1660915
    iget-object v0, p0, LX/AJE;->a:LX/AJH;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 1660916
    iput-object v1, v0, LX/AJH;->i:Ljava/lang/String;

    .line 1660917
    iget-object v0, p0, LX/AJE;->a:LX/AJH;

    iget-object v0, v0, LX/AJH;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1660918
    iget-object v0, p0, LX/AJE;->a:LX/AJH;

    invoke-virtual {v0}, LX/AJH;->d()V

    .line 1660919
    iget-object v0, p0, LX/AJE;->a:LX/AJH;

    iget-object v0, v0, LX/AJH;->f:LX/AIu;

    .line 1660920
    iget-object v1, v0, LX/AIu;->b:LX/AIm;

    .line 1660921
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1660922
    iput-object v0, v1, LX/AIm;->d:LX/0Px;

    .line 1660923
    invoke-virtual {v1}, LX/1OM;->notifyDataSetChanged()V

    .line 1660924
    :goto_0
    return-void

    .line 1660925
    :cond_0
    iget-object v0, p0, LX/AJE;->a:LX/AJH;

    iget-object v1, p0, LX/AJE;->a:LX/AJH;

    iget-object v1, v1, LX/AJH;->i:Ljava/lang/String;

    .line 1660926
    iget-object p0, v0, LX/AJH;->d:LX/1Ck;

    const/4 p1, 0x0

    iget-object p2, v0, LX/AJH;->c:LX/BMr;

    invoke-static {v1}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object p3

    invoke-virtual {p2, p3}, LX/BMr;->a(LX/0am;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p2

    new-instance p3, LX/AJG;

    invoke-direct {p3, v0}, LX/AJG;-><init>(LX/AJH;)V

    invoke-virtual {p0, p1, p2, p3}, LX/1Ck;->b(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1660927
    goto :goto_0
.end method
