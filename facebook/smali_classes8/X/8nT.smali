.class public final LX/8nT;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/CharSequence;

.field public final synthetic c:LX/8JX;

.field public final synthetic d:Z

.field public final synthetic e:Z

.field public final synthetic f:Z

.field public final synthetic g:Z

.field public final synthetic h:Z

.field public final synthetic i:LX/8nW;


# direct methods
.method public constructor <init>(LX/8nW;Ljava/lang/String;Ljava/lang/CharSequence;LX/8JX;ZZZZZ)V
    .locals 0

    .prologue
    .line 1401097
    iput-object p1, p0, LX/8nT;->i:LX/8nW;

    iput-object p2, p0, LX/8nT;->a:Ljava/lang/String;

    iput-object p3, p0, LX/8nT;->b:Ljava/lang/CharSequence;

    iput-object p4, p0, LX/8nT;->c:LX/8JX;

    iput-boolean p5, p0, LX/8nT;->d:Z

    iput-boolean p6, p0, LX/8nT;->e:Z

    iput-boolean p7, p0, LX/8nT;->f:Z

    iput-boolean p8, p0, LX/8nT;->g:Z

    iput-boolean p9, p0, LX/8nT;->h:Z

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 14

    .prologue
    .line 1401090
    const-string v0, "fetch_group_members_to_mentions"

    const-string v1, "Fail to load group members for mentions "

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1401091
    iget-object v0, p0, LX/8nT;->i:LX/8nW;

    iget-object v0, v0, LX/8nW;->l:LX/8nS;

    iget-object v1, p0, LX/8nT;->a:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    iget-object v2, p0, LX/8nT;->i:LX/8nW;

    iget-object v2, v2, LX/8nW;->i:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    iget-object v4, p0, LX/8nT;->i:LX/8nW;

    iget-wide v4, v4, LX/8nW;->j:J

    sub-long/2addr v2, v4

    iget-object v4, p0, LX/8nT;->b:Ljava/lang/CharSequence;

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual/range {v0 .. v5}, LX/8nS;->a(ZJLjava/lang/String;Z)V

    .line 1401092
    iget-object v0, p0, LX/8nT;->i:LX/8nW;

    iget-object v1, p0, LX/8nT;->b:Ljava/lang/CharSequence;

    iget-boolean v2, p0, LX/8nT;->d:Z

    iget-boolean v3, p0, LX/8nT;->e:Z

    iget-boolean v4, p0, LX/8nT;->f:Z

    iget-boolean v5, p0, LX/8nT;->g:Z

    iget-object v7, p0, LX/8nT;->c:LX/8JX;

    .line 1401093
    iget-object v8, v0, LX/8nW;->h:LX/8nM;

    move-object v9, v1

    move v10, v2

    move v11, v3

    move v12, v4

    move v13, v5

    invoke-virtual/range {v8 .. v13}, LX/8nB;->a(Ljava/lang/CharSequence;ZZZZ)Ljava/util/List;

    move-result-object v8

    .line 1401094
    iget-object v9, v0, LX/8nW;->g:Ljava/util/List;

    iget-object v10, v0, LX/8nW;->h:LX/8nM;

    invoke-virtual {v10}, LX/8nB;->d()LX/0Px;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1401095
    invoke-interface {v7, v1, v8}, LX/8JX;->a(Ljava/lang/CharSequence;Ljava/util/List;)V

    .line 1401096
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 13

    .prologue
    .line 1401035
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1401036
    iget-object v0, p0, LX/8nT;->i:LX/8nW;

    iget-object v0, v0, LX/8nW;->l:LX/8nS;

    iget-object v1, p0, LX/8nT;->a:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    iget-object v2, p0, LX/8nT;->i:LX/8nW;

    iget-object v2, v2, LX/8nW;->i:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    iget-object v4, p0, LX/8nT;->i:LX/8nW;

    iget-wide v4, v4, LX/8nW;->j:J

    sub-long/2addr v2, v4

    iget-object v4, p0, LX/8nT;->b:Ljava/lang/CharSequence;

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, LX/8nS;->a(ZJLjava/lang/String;Z)V

    .line 1401037
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 1401038
    if-eqz p1, :cond_0

    .line 1401039
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1401040
    if-nez v0, :cond_1

    .line 1401041
    :cond_0
    iget-object v0, p0, LX/8nT;->c:LX/8JX;

    iget-object v1, p0, LX/8nT;->b:Ljava/lang/CharSequence;

    invoke-static {v10}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/8JX;->a(Ljava/lang/CharSequence;Ljava/util/List;)V

    .line 1401042
    :goto_0
    return-void

    .line 1401043
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1401044
    check-cast v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;

    invoke-virtual {v0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;->l()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v11

    .line 1401045
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1401046
    check-cast v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;

    invoke-virtual {v0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v0

    const/4 v2, 0x0

    const v3, 0x11b2952b

    invoke-static {v1, v0, v2, v3}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 1401047
    if-eqz v0, :cond_4

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    move-object v1, v0

    .line 1401048
    :goto_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1401049
    check-cast v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;

    .line 1401050
    invoke-virtual {v0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;->k()Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$ParentGroupModel;

    move-result-object v2

    if-eqz v2, :cond_b

    const/4 v2, 0x1

    .line 1401051
    :goto_2
    sget-object v3, LX/8nV;->a:[I

    invoke-virtual {v11}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 1401052
    sget-object v2, LX/8nE;->OTHERS:LX/8nE;

    :goto_3
    move-object v0, v2

    .line 1401053
    invoke-virtual {v0}, LX/8nE;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1401054
    invoke-interface {v1}, LX/3Sb;->b()LX/2sN;

    move-result-object v12

    :cond_2
    :goto_4
    invoke-interface {v12}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v12}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v2, v0, LX/1vs;->b:I

    .line 1401055
    if-eqz v2, :cond_2

    const/4 v0, 0x0

    const-class v3, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;

    invoke-virtual {v1, v2, v0, v3}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    const-class v3, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;

    invoke-virtual {v1, v2, v0, v3}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;->k()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1401056
    iget-object v0, p0, LX/8nT;->i:LX/8nW;

    iget-object v0, v0, LX/8nW;->g:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1401057
    iget-object v0, p0, LX/8nT;->i:LX/8nW;

    iget-object v0, v0, LX/8nW;->g:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1401058
    :cond_3
    const/4 v0, 0x0

    const-class v3, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;

    invoke-virtual {v1, v2, v0, v3}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;

    .line 1401059
    iget-object v0, p0, LX/8nT;->i:LX/8nW;

    iget-object v0, v0, LX/8nW;->b:LX/3iT;

    new-instance v1, Lcom/facebook/user/model/Name;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v9}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;->n()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v9}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/7Gr;->USER:LX/7Gr;

    const/4 v6, 0x0

    const-string v7, "group_members"

    invoke-virtual/range {v0 .. v8}, LX/3iT;->b(Lcom/facebook/user/model/Name;JLjava/lang/String;LX/7Gr;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/7Gq;

    move-result-object v1

    invoke-virtual {v9}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;->m()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {v9}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$MemberSectionModel$EdgesModel$NodeModel;->l()Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x1

    .line 1401060
    :goto_5
    iput-boolean v0, v1, LX/7Gq;->j:Z

    .line 1401061
    move-object v0, v1

    .line 1401062
    invoke-virtual {v0}, LX/7Gq;->l()Lcom/facebook/tagging/model/TaggingProfile;

    move-result-object v0

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 1401063
    :cond_4
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_1

    .line 1401064
    :cond_5
    const/4 v0, 0x0

    goto :goto_5

    .line 1401065
    :cond_6
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1401066
    check-cast v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;

    invoke-virtual {v0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_a

    .line 1401067
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1401068
    check-cast v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;

    invoke-virtual {v0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v0

    const/4 v2, 0x0

    const v3, -0x2f722704

    invoke-static {v1, v0, v2, v3}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 1401069
    if-eqz v0, :cond_9

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    .line 1401070
    :goto_6
    sget-object v1, LX/8nV;->a:[I

    invoke-virtual {v11}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    .line 1401071
    sget-object v1, LX/8nE;->OTHERS:LX/8nE;

    :goto_7
    move-object v1, v1

    .line 1401072
    invoke-virtual {v1}, LX/8nE;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1401073
    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v8

    :cond_7
    :goto_8
    invoke-interface {v8}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v8}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v2, v0, LX/1vs;->b:I

    .line 1401074
    if-eqz v2, :cond_7

    const/4 v0, 0x0

    const-class v3, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel;

    invoke-virtual {v1, v2, v0, v3}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    if-eqz v0, :cond_7

    const/4 v0, 0x0

    const-class v3, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel;

    invoke-virtual {v1, v2, v0, v3}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel;->k()Z

    move-result v0

    if-nez v0, :cond_7

    .line 1401075
    iget-object v0, p0, LX/8nT;->i:LX/8nW;

    iget-object v0, v0, LX/8nW;->g:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 1401076
    iget-object v0, p0, LX/8nT;->i:LX/8nW;

    iget-object v0, v0, LX/8nW;->g:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1401077
    :cond_8
    const/4 v0, 0x0

    const-class v3, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel;

    invoke-virtual {v1, v2, v0, v3}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel;

    .line 1401078
    iget-object v0, p0, LX/8nT;->i:LX/8nW;

    iget-object v0, v0, LX/8nW;->b:LX/3iT;

    new-instance v1, Lcom/facebook/user/model/Name;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v4}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel;->l()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v2, v3, v5}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v4}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel$GroupMentionsModel$NonMemberSectionModel$EdgesModel$NodeModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/7Gr;->USER:LX/7Gr;

    const-string v6, "non_group_members"

    invoke-virtual/range {v0 .. v7}, LX/3iT;->a(Lcom/facebook/user/model/Name;JLjava/lang/String;LX/7Gr;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/tagging/model/TaggingProfile;

    move-result-object v0

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 1401079
    :cond_9
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto/16 :goto_6

    .line 1401080
    :cond_a
    iget-object v0, p0, LX/8nT;->c:LX/8JX;

    iget-object v1, p0, LX/8nT;->b:Ljava/lang/CharSequence;

    invoke-interface {v0, v1, v10}, LX/8JX;->a(Ljava/lang/CharSequence;Ljava/util/List;)V

    goto/16 :goto_0

    .line 1401081
    :cond_b
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 1401082
    :pswitch_0
    if-eqz v2, :cond_c

    sget-object v2, LX/8nE;->OPEN_GROUP_MEMBERS:LX/8nE;

    goto/16 :goto_3

    :cond_c
    sget-object v2, LX/8nE;->PUBLIC_GROUP_MEMBERS:LX/8nE;

    goto/16 :goto_3

    .line 1401083
    :pswitch_1
    sget-object v2, LX/8nE;->CLOSE_GROUP_MEMBERS:LX/8nE;

    goto/16 :goto_3

    .line 1401084
    :pswitch_2
    invoke-virtual {v0}, Lcom/facebook/tagging/graphql/protocol/FetchGroupMembersToMentionQueryModels$FetchGroupMembersToMentionQueryModel;->j()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1401085
    sget-object v2, LX/8nE;->MULTI_COMPANY_GROUP_MEMBERS:LX/8nE;

    goto/16 :goto_3

    .line 1401086
    :cond_d
    sget-object v2, LX/8nE;->SECRET_GROUP_MEMBERS:LX/8nE;

    goto/16 :goto_3

    .line 1401087
    :pswitch_3
    sget-object v1, LX/8nE;->NOT_NOTIFIED_OTHERS:LX/8nE;

    goto/16 :goto_7

    .line 1401088
    :pswitch_4
    sget-object v1, LX/8nE;->NOT_NOTIFIED_AND_UNSEEN_OTHERS:LX/8nE;

    goto/16 :goto_7

    .line 1401089
    :pswitch_5
    sget-object v1, LX/8nE;->NOT_NOTIFIED_AND_UNSEEN_OTHERS:LX/8nE;

    goto/16 :goto_7

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
