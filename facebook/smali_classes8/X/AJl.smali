.class public abstract LX/AJl;
.super LX/1Of;
.source ""


# instance fields
.field public final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/1a1;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/1a1;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/AJk;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/1a1;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/AJk;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/1a1;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/1a1;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/1a1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1661966
    invoke-direct {p0}, LX/1Of;-><init>()V

    .line 1661967
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/AJl;->a:Ljava/util/ArrayList;

    .line 1661968
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/AJl;->b:Ljava/util/ArrayList;

    .line 1661969
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/AJl;->c:Ljava/util/ArrayList;

    .line 1661970
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/AJl;->d:Ljava/util/ArrayList;

    .line 1661971
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/AJl;->e:Ljava/util/ArrayList;

    .line 1661972
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/AJl;->f:Ljava/util/ArrayList;

    .line 1661973
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/AJl;->g:Ljava/util/ArrayList;

    .line 1661974
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/AJl;->h:Ljava/util/ArrayList;

    .line 1661975
    return-void
.end method

.method private static a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/1a1;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1662023
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    .line 1662024
    iget-object v0, v0, LX/1a1;->a:Landroid/view/View;

    invoke-static {v0}, LX/0vv;->v(Landroid/view/View;)LX/3sU;

    move-result-object v0

    invoke-virtual {v0}, LX/3sU;->a()V

    goto :goto_0

    .line 1662025
    :cond_0
    return-void
.end method

.method private h(LX/1a1;)V
    .locals 6

    .prologue
    .line 1662016
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    .line 1662017
    invoke-static {v0}, LX/0vv;->v(Landroid/view/View;)LX/3sU;

    move-result-object v0

    .line 1662018
    iget-object v1, p0, LX/AJl;->h:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1662019
    invoke-virtual {p0, v0}, LX/AJl;->b(LX/3sU;)V

    .line 1662020
    iget-wide v4, p0, LX/1Of;->d:J

    move-wide v2, v4

    .line 1662021
    invoke-virtual {v0, v2, v3}, LX/3sU;->a(J)LX/3sU;

    move-result-object v1

    new-instance v2, LX/AJh;

    invoke-direct {v2, p0, p1, v0}, LX/AJh;-><init>(LX/AJl;LX/1a1;LX/3sU;)V

    invoke-virtual {v1, v2}, LX/3sU;->a(LX/3oQ;)LX/3sU;

    move-result-object v0

    invoke-virtual {v0}, LX/3sU;->b()V

    .line 1662022
    return-void
.end method

.method private i(LX/1a1;)V
    .locals 1

    .prologue
    .line 1662013
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    invoke-static {v0}, LX/3os;->a(Landroid/view/View;)V

    .line 1662014
    invoke-virtual {p0, p1}, LX/1Of;->c(LX/1a1;)V

    .line 1662015
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 12

    .prologue
    const-wide/16 v4, 0x0

    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 1661976
    iget-object v1, p0, LX/AJl;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    move v1, v0

    .line 1661977
    :goto_0
    iget-object v3, p0, LX/AJl;->c:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    move v3, v0

    .line 1661978
    :goto_1
    iget-object v6, p0, LX/AJl;->b:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_3

    move v7, v0

    .line 1661979
    :goto_2
    if-nez v1, :cond_4

    if-nez v3, :cond_4

    if-nez v7, :cond_4

    .line 1661980
    :cond_0
    :goto_3
    return-void

    :cond_1
    move v1, v2

    .line 1661981
    goto :goto_0

    :cond_2
    move v3, v2

    .line 1661982
    goto :goto_1

    :cond_3
    move v7, v2

    .line 1661983
    goto :goto_2

    .line 1661984
    :cond_4
    iget-object v0, p0, LX/AJl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v8

    move v6, v2

    :goto_4
    if-ge v6, v8, :cond_5

    iget-object v0, p0, LX/AJl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    .line 1661985
    invoke-direct {p0, v0}, LX/AJl;->h(LX/1a1;)V

    .line 1661986
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_4

    .line 1661987
    :cond_5
    iget-object v0, p0, LX/AJl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1661988
    if-eqz v3, :cond_6

    .line 1661989
    iget-object v0, p0, LX/AJl;->e:Ljava/util/ArrayList;

    iget-object v6, p0, LX/AJl;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1661990
    iget-object v0, p0, LX/AJl;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1661991
    new-instance v6, Lcom/facebook/audience/sharesheet/ui/BaseItemAnimator$1;

    invoke-direct {v6, p0}, Lcom/facebook/audience/sharesheet/ui/BaseItemAnimator$1;-><init>(LX/AJl;)V

    .line 1661992
    if-eqz v1, :cond_8

    .line 1661993
    iget-object v0, p0, LX/AJl;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AJk;

    iget-object v0, v0, LX/AJk;->a:LX/1a1;

    iget-object v0, v0, LX/1a1;->a:Landroid/view/View;

    .line 1661994
    iget-wide v10, p0, LX/1Of;->d:J

    move-wide v8, v10

    .line 1661995
    invoke-static {v0, v6, v8, v9}, LX/0vv;->a(Landroid/view/View;Ljava/lang/Runnable;J)V

    .line 1661996
    :cond_6
    :goto_5
    if-eqz v7, :cond_0

    .line 1661997
    iget-object v0, p0, LX/AJl;->d:Ljava/util/ArrayList;

    iget-object v6, p0, LX/AJl;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1661998
    iget-object v0, p0, LX/AJl;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1661999
    new-instance v8, Lcom/facebook/audience/sharesheet/ui/BaseItemAnimator$2;

    invoke-direct {v8, p0}, Lcom/facebook/audience/sharesheet/ui/BaseItemAnimator$2;-><init>(LX/AJl;)V

    .line 1662000
    if-nez v1, :cond_7

    if-eqz v3, :cond_b

    .line 1662001
    :cond_7
    if-eqz v1, :cond_9

    .line 1662002
    iget-wide v10, p0, LX/1Of;->d:J

    move-wide v0, v10

    .line 1662003
    move-wide v6, v0

    .line 1662004
    :goto_6
    if-eqz v3, :cond_a

    .line 1662005
    iget-wide v10, p0, LX/1Of;->e:J

    move-wide v0, v10

    .line 1662006
    :goto_7
    add-long v4, v6, v0

    .line 1662007
    iget-object v0, p0, LX/AJl;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    iget-object v0, v0, LX/1a1;->a:Landroid/view/View;

    .line 1662008
    invoke-static {v0, v8, v4, v5}, LX/0vv;->a(Landroid/view/View;Ljava/lang/Runnable;J)V

    goto :goto_3

    .line 1662009
    :cond_8
    invoke-interface {v6}, Ljava/lang/Runnable;->run()V

    goto :goto_5

    :cond_9
    move-wide v6, v4

    .line 1662010
    goto :goto_6

    :cond_a
    move-wide v0, v4

    .line 1662011
    goto :goto_7

    .line 1662012
    :cond_b
    invoke-interface {v8}, Ljava/lang/Runnable;->run()V

    goto/16 :goto_3
.end method

.method public abstract a(LX/3sU;)V
.end method

.method public abstract a(LX/3sU;II)V
.end method

.method public abstract a(Landroid/view/View;)V
.end method

.method public abstract a(Landroid/view/View;II)V
.end method

.method public final a(LX/1a1;)Z
    .locals 1

    .prologue
    .line 1661962
    invoke-direct {p0, p1}, LX/AJl;->i(LX/1a1;)V

    .line 1661963
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {p0, v0}, LX/AJl;->b(Landroid/view/View;)V

    .line 1661964
    iget-object v0, p0, LX/AJl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1661965
    const/4 v0, 0x1

    return v0
.end method

.method public final a(LX/1a1;IIII)Z
    .locals 7

    .prologue
    .line 1661949
    invoke-direct {p0, p1}, LX/AJl;->i(LX/1a1;)V

    .line 1661950
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    .line 1661951
    int-to-float v1, p2

    iget-object v2, p1, LX/1a1;->a:Landroid/view/View;

    invoke-static {v2}, LX/0vv;->r(Landroid/view/View;)F

    move-result v2

    add-float/2addr v1, v2

    float-to-int v2, v1

    .line 1661952
    int-to-float v1, p3

    iget-object v3, p1, LX/1a1;->a:Landroid/view/View;

    invoke-static {v3}, LX/0vv;->s(Landroid/view/View;)F

    move-result v3

    add-float/2addr v1, v3

    float-to-int v3, v1

    .line 1661953
    sub-int v1, p4, v2

    .line 1661954
    sub-int v4, p5, v3

    .line 1661955
    if-nez v1, :cond_0

    if-nez v4, :cond_0

    .line 1661956
    invoke-virtual {p0, p1}, LX/1Of;->e(LX/1a1;)V

    .line 1661957
    const/4 v0, 0x0

    .line 1661958
    :goto_0
    return v0

    .line 1661959
    :cond_0
    invoke-virtual {p0, v0, v1, v4}, LX/AJl;->a(Landroid/view/View;II)V

    .line 1661960
    iget-object v6, p0, LX/AJl;->c:Ljava/util/ArrayList;

    new-instance v0, LX/AJk;

    move-object v1, p1

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, LX/AJk;-><init>(LX/1a1;IIII)V

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1661961
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(LX/1a1;LX/1a1;IIII)Z
    .locals 1

    .prologue
    .line 1662026
    const/4 v0, 0x1

    return v0
.end method

.method public final b(LX/1a1;IIII)V
    .locals 9

    .prologue
    .line 1661871
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    .line 1661872
    sub-int v3, p4, p2

    .line 1661873
    sub-int v4, p5, p3

    .line 1661874
    invoke-static {v0}, LX/0vv;->v(Landroid/view/View;)LX/3sU;

    move-result-object v5

    .line 1661875
    iget-object v0, p0, LX/AJl;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1661876
    invoke-virtual {p0, v5, v3, v4}, LX/AJl;->a(LX/3sU;II)V

    .line 1661877
    iget-wide v7, p0, LX/1Of;->e:J

    move-wide v0, v7

    .line 1661878
    invoke-virtual {v5, v0, v1}, LX/3sU;->a(J)LX/3sU;

    move-result-object v6

    new-instance v0, LX/AJj;

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, LX/AJj;-><init>(LX/AJl;LX/1a1;IILX/3sU;)V

    invoke-virtual {v6, v0}, LX/3sU;->a(LX/3oQ;)LX/3sU;

    move-result-object v0

    invoke-virtual {v0}, LX/3sU;->b()V

    .line 1661879
    return-void
.end method

.method public abstract b(LX/3sU;)V
.end method

.method public abstract b(Landroid/view/View;)V
.end method

.method public abstract b(Landroid/view/View;II)V
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1661880
    iget-object v0, p0, LX/AJl;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AJl;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AJl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AJl;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AJl;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AJl;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AJl;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AJl;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/1a1;)Z
    .locals 1

    .prologue
    .line 1661881
    invoke-direct {p0, p1}, LX/AJl;->i(LX/1a1;)V

    .line 1661882
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {p0, v0}, LX/AJl;->a(Landroid/view/View;)V

    .line 1661883
    iget-object v0, p0, LX/AJl;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1661884
    const/4 v0, 0x1

    return v0
.end method

.method public final c()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1661885
    iget-object v0, p0, LX/AJl;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    iget-object v0, p0, LX/AJl;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AJk;

    .line 1661886
    iget-object v4, v0, LX/AJk;->a:LX/1a1;

    iget-object v4, v4, LX/1a1;->a:Landroid/view/View;

    .line 1661887
    invoke-virtual {p0, v4}, LX/AJl;->e(Landroid/view/View;)V

    .line 1661888
    iget-object v4, v0, LX/AJk;->a:LX/1a1;

    invoke-virtual {p0, v4}, LX/1Of;->e(LX/1a1;)V

    .line 1661889
    iget-object v4, p0, LX/AJl;->c:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1661890
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1661891
    :cond_0
    iget-object v0, p0, LX/AJl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_1

    iget-object v0, p0, LX/AJl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    .line 1661892
    iget-object v4, v0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {p0, v4}, LX/AJl;->d(Landroid/view/View;)V

    .line 1661893
    invoke-virtual {p0, v0}, LX/1Of;->d(LX/1a1;)V

    .line 1661894
    iget-object v4, p0, LX/AJl;->a:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1661895
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1661896
    :cond_1
    iget-object v0, p0, LX/AJl;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    :goto_2
    if-ge v2, v3, :cond_2

    iget-object v0, p0, LX/AJl;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    .line 1661897
    iget-object v4, v0, LX/1a1;->a:Landroid/view/View;

    .line 1661898
    invoke-virtual {p0, v4}, LX/AJl;->c(Landroid/view/View;)V

    .line 1661899
    invoke-virtual {p0, v0}, LX/1Of;->f(LX/1a1;)V

    .line 1661900
    iget-object v4, p0, LX/AJl;->b:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1661901
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 1661902
    :cond_2
    invoke-virtual {p0}, LX/1Of;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1661903
    :goto_3
    return-void

    .line 1661904
    :cond_3
    iget-object v0, p0, LX/AJl;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    :goto_4
    if-ge v2, v3, :cond_4

    iget-object v0, p0, LX/AJl;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AJk;

    .line 1661905
    iget-object v4, v0, LX/AJk;->a:LX/1a1;

    .line 1661906
    iget-object v4, v4, LX/1a1;->a:Landroid/view/View;

    .line 1661907
    invoke-virtual {p0, v4}, LX/AJl;->e(Landroid/view/View;)V

    .line 1661908
    iget-object v4, v0, LX/AJk;->a:LX/1a1;

    invoke-virtual {p0, v4}, LX/1Of;->e(LX/1a1;)V

    .line 1661909
    iget-object v4, p0, LX/AJl;->e:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1661910
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 1661911
    :cond_4
    iget-object v0, p0, LX/AJl;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_5
    if-ge v1, v2, :cond_5

    iget-object v0, p0, LX/AJl;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    .line 1661912
    iget-object v3, v0, LX/1a1;->a:Landroid/view/View;

    .line 1661913
    invoke-virtual {p0, v3}, LX/AJl;->c(Landroid/view/View;)V

    .line 1661914
    invoke-virtual {p0, v0}, LX/1Of;->f(LX/1a1;)V

    .line 1661915
    iget-object v3, p0, LX/AJl;->d:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1661916
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 1661917
    :cond_5
    iget-object v0, p0, LX/AJl;->h:Ljava/util/ArrayList;

    invoke-static {v0}, LX/AJl;->a(Ljava/util/List;)V

    .line 1661918
    iget-object v0, p0, LX/AJl;->g:Ljava/util/ArrayList;

    invoke-static {v0}, LX/AJl;->a(Ljava/util/List;)V

    .line 1661919
    iget-object v0, p0, LX/AJl;->f:Ljava/util/ArrayList;

    invoke-static {v0}, LX/AJl;->a(Ljava/util/List;)V

    .line 1661920
    invoke-virtual {p0}, LX/1Of;->i()V

    goto :goto_3
.end method

.method public final c(LX/1a1;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1661921
    iget-object v3, p1, LX/1a1;->a:Landroid/view/View;

    .line 1661922
    invoke-static {v3}, LX/0vv;->v(Landroid/view/View;)LX/3sU;

    move-result-object v0

    invoke-virtual {v0}, LX/3sU;->a()V

    .line 1661923
    iget-object v0, p0, LX/AJl;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    iget-object v0, p0, LX/AJl;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AJk;

    .line 1661924
    iget-object v5, v0, LX/AJk;->a:LX/1a1;

    if-ne v5, p1, :cond_0

    .line 1661925
    invoke-virtual {p0, v3}, LX/AJl;->e(Landroid/view/View;)V

    .line 1661926
    invoke-virtual {p0, p1}, LX/1Of;->e(LX/1a1;)V

    .line 1661927
    iget-object v5, p0, LX/AJl;->c:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1661928
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1661929
    :cond_1
    iget-object v0, p0, LX/AJl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1661930
    invoke-virtual {p0, v3}, LX/AJl;->d(Landroid/view/View;)V

    .line 1661931
    invoke-virtual {p0, p1}, LX/1Of;->d(LX/1a1;)V

    .line 1661932
    :cond_2
    iget-object v0, p0, LX/AJl;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1661933
    invoke-virtual {p0, v3}, LX/AJl;->c(Landroid/view/View;)V

    .line 1661934
    invoke-virtual {p0, p1}, LX/1Of;->f(LX/1a1;)V

    .line 1661935
    :cond_3
    iget-object v0, p0, LX/AJl;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_1
    if-ge v1, v2, :cond_4

    iget-object v0, p0, LX/AJl;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AJk;

    .line 1661936
    iget-object v4, v0, LX/AJk;->a:LX/1a1;

    if-ne v4, p1, :cond_6

    .line 1661937
    invoke-virtual {p0, v3}, LX/AJl;->e(Landroid/view/View;)V

    .line 1661938
    invoke-virtual {p0, p1}, LX/1Of;->e(LX/1a1;)V

    .line 1661939
    iget-object v1, p0, LX/AJl;->e:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1661940
    :cond_4
    iget-object v0, p0, LX/AJl;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1661941
    invoke-virtual {p0, v3}, LX/AJl;->c(Landroid/view/View;)V

    .line 1661942
    invoke-virtual {p0, p1}, LX/1Of;->f(LX/1a1;)V

    .line 1661943
    :cond_5
    invoke-virtual {p0}, LX/AJl;->j()V

    .line 1661944
    return-void

    .line 1661945
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method public abstract c(Landroid/view/View;)V
.end method

.method public abstract d(Landroid/view/View;)V
.end method

.method public abstract e(Landroid/view/View;)V
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 1661946
    invoke-virtual {p0}, LX/1Of;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1661947
    invoke-virtual {p0}, LX/1Of;->i()V

    .line 1661948
    :cond_0
    return-void
.end method
