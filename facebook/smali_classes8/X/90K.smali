.class public final LX/90K;
.super LX/1OX;
.source ""


# instance fields
.field public final synthetic a:LX/1P0;

.field public final synthetic b:Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;LX/1P0;)V
    .locals 0

    .prologue
    .line 1428602
    iput-object p1, p0, LX/90K;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;

    iput-object p2, p0, LX/90K;->a:LX/1P0;

    invoke-direct {p0}, LX/1OX;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 4

    .prologue
    .line 1428603
    invoke-super {p0, p1, p2, p3}, LX/1OX;->a(Landroid/support/v7/widget/RecyclerView;II)V

    .line 1428604
    iget-object v0, p0, LX/90K;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->u:LX/8zZ;

    invoke-virtual {v0}, LX/5Jl;->a()I

    move-result v0

    .line 1428605
    iget-object v1, p0, LX/90K;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;

    iget-object v1, v1, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->r:LX/90G;

    .line 1428606
    iget v2, v1, LX/90G;->b:I

    move v1, v2

    .line 1428607
    if-eq v1, v0, :cond_1

    .line 1428608
    iget-object v1, p0, LX/90K;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;

    iget-object v1, v1, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->r:LX/90G;

    invoke-virtual {v1}, LX/90G;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1428609
    iget-object v1, p0, LX/90K;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;

    iget-object v1, v1, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->H:LX/918;

    iget-object v2, p0, LX/90K;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;

    iget-object v2, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->p:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1428610
    iget-object v3, v2, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v2, v3

    .line 1428611
    iget-object v3, p0, LX/90K;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;

    invoke-static {v3}, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->q(Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;)Ljava/lang/String;

    move-result-object v3

    .line 1428612
    const-string p1, "activities_selector_objects_first_scroll"

    invoke-static {p1, v2}, LX/918;->d(Ljava/lang/String;Ljava/lang/String;)LX/8yQ;

    move-result-object p1

    invoke-virtual {p1, v3}, LX/8yQ;->a(Ljava/lang/String;)LX/8yQ;

    move-result-object p1

    .line 1428613
    iget-object p2, p1, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object p1, p2

    .line 1428614
    iget-object p2, v1, LX/918;->a:LX/0Zb;

    invoke-interface {p2, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1428615
    :cond_0
    iget-object v1, p0, LX/90K;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;

    iget-object v1, v1, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->r:LX/90G;

    .line 1428616
    iput v0, v1, LX/90G;->b:I

    .line 1428617
    :cond_1
    iget-object v0, p0, LX/90K;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->v:LX/8zh;

    iget-object v1, p0, LX/90K;->a:LX/1P0;

    invoke-virtual {v1}, LX/1P1;->n()I

    move-result v1

    invoke-virtual {v0, v1}, LX/8zh;->a(I)V

    .line 1428618
    return-void
.end method
