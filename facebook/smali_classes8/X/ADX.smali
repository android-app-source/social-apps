.class public final enum LX/ADX;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/6LU;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ADX;",
        ">;",
        "LX/6LU",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ADX;

.field public static final enum NEW_USER:LX/ADX;

.field public static final enum POSTPAY:LX/ADX;

.field public static final enum PREPAY:LX/ADX;

.field public static final enum UNKNOWN:LX/ADX;


# instance fields
.field private final mValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1644890
    new-instance v0, LX/ADX;

    const-string v1, "UNKNOWN"

    const-string v2, "unknown"

    invoke-direct {v0, v1, v3, v2}, LX/ADX;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ADX;->UNKNOWN:LX/ADX;

    .line 1644891
    new-instance v0, LX/ADX;

    const-string v1, "PREPAY"

    const-string v2, "prepay"

    invoke-direct {v0, v1, v4, v2}, LX/ADX;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ADX;->PREPAY:LX/ADX;

    .line 1644892
    new-instance v0, LX/ADX;

    const-string v1, "POSTPAY"

    const-string v2, "postpay"

    invoke-direct {v0, v1, v5, v2}, LX/ADX;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ADX;->POSTPAY:LX/ADX;

    .line 1644893
    new-instance v0, LX/ADX;

    const-string v1, "NEW_USER"

    const-string v2, "new_user"

    invoke-direct {v0, v1, v6, v2}, LX/ADX;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ADX;->NEW_USER:LX/ADX;

    .line 1644894
    const/4 v0, 0x4

    new-array v0, v0, [LX/ADX;

    sget-object v1, LX/ADX;->UNKNOWN:LX/ADX;

    aput-object v1, v0, v3

    sget-object v1, LX/ADX;->PREPAY:LX/ADX;

    aput-object v1, v0, v4

    sget-object v1, LX/ADX;->POSTPAY:LX/ADX;

    aput-object v1, v0, v5

    sget-object v1, LX/ADX;->NEW_USER:LX/ADX;

    aput-object v1, v0, v6

    sput-object v0, LX/ADX;->$VALUES:[LX/ADX;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1644887
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1644888
    iput-object p3, p0, LX/ADX;->mValue:Ljava/lang/String;

    .line 1644889
    return-void
.end method

.method public static of(Ljava/lang/String;)LX/ADX;
    .locals 2

    .prologue
    .line 1644879
    invoke-static {}, LX/ADX;->values()[LX/ADX;

    move-result-object v0

    invoke-static {v0, p0}, LX/6LV;->a([LX/6LU;Ljava/lang/Object;)LX/6LU;

    move-result-object v0

    sget-object v1, LX/ADX;->UNKNOWN:LX/ADX;

    invoke-static {v0, v1}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ADX;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/ADX;
    .locals 1

    .prologue
    .line 1644886
    const-class v0, LX/ADX;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ADX;

    return-object v0
.end method

.method public static values()[LX/ADX;
    .locals 1

    .prologue
    .line 1644885
    sget-object v0, LX/ADX;->$VALUES:[LX/ADX;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ADX;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValue()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1644895
    invoke-virtual {p0}, LX/ADX;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1644884
    iget-object v0, p0, LX/ADX;->mValue:Ljava/lang/String;

    return-object v0
.end method

.method public final isLockedIntoPrepay()Z
    .locals 1

    .prologue
    .line 1644883
    sget-object v0, LX/ADX;->PREPAY:LX/ADX;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isNUX()Z
    .locals 1

    .prologue
    .line 1644882
    sget-object v0, LX/ADX;->NEW_USER:LX/ADX;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isPUX()Z
    .locals 1

    .prologue
    .line 1644881
    sget-object v0, LX/ADX;->NEW_USER:LX/ADX;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/ADX;->UNKNOWN:LX/ADX;

    if-eq p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1644880
    invoke-virtual {p0}, LX/ADX;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
