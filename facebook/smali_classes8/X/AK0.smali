.class public LX/AK0;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:J

.field private static volatile j:LX/AK0;


# instance fields
.field private final b:LX/AKg;

.field public final c:Ljava/util/concurrent/ExecutorService;

.field public final d:LX/7gh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7gh",
            "<",
            "Lcom/facebook/audience/snacks/data/SnacksDataProvider$SnacksDataProviderObserver;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/AK4;

.field public final f:LX/AKN;

.field public final g:LX/AJv;

.field private final h:LX/0SG;

.field public final i:LX/AKO;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1662454
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    sput-wide v0, LX/AK0;->a:J

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/AKg;LX/AK4;LX/AKN;LX/0SG;LX/AKO;)V
    .locals 2
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1662492
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1662493
    new-instance v0, LX/7gh;

    invoke-direct {v0}, LX/7gh;-><init>()V

    iput-object v0, p0, LX/AK0;->d:LX/7gh;

    .line 1662494
    new-instance v0, LX/AJv;

    invoke-direct {v0}, LX/AJv;-><init>()V

    iput-object v0, p0, LX/AK0;->g:LX/AJv;

    .line 1662495
    iput-object p1, p0, LX/AK0;->c:Ljava/util/concurrent/ExecutorService;

    .line 1662496
    iput-object p2, p0, LX/AK0;->b:LX/AKg;

    .line 1662497
    iput-object p3, p0, LX/AK0;->e:LX/AK4;

    .line 1662498
    iput-object p4, p0, LX/AK0;->f:LX/AKN;

    .line 1662499
    iput-object p5, p0, LX/AK0;->h:LX/0SG;

    .line 1662500
    iput-object p6, p0, LX/AK0;->i:LX/AKO;

    .line 1662501
    iget-object v0, p0, LX/AK0;->i:LX/AKO;

    new-instance v1, LX/AJw;

    invoke-direct {v1, p0}, LX/AJw;-><init>(LX/AK0;)V

    invoke-virtual {v0, v1}, LX/AKO;->a(LX/AJw;)V

    .line 1662502
    iget-object v0, p0, LX/AK0;->f:LX/AKN;

    new-instance v1, LX/AJx;

    invoke-direct {v1, p0}, LX/AJx;-><init>(LX/AK0;)V

    .line 1662503
    iput-object v1, v0, LX/AKN;->b:LX/AJx;

    .line 1662504
    return-void
.end method

.method public static a(LX/0QB;)LX/AK0;
    .locals 10

    .prologue
    .line 1662473
    sget-object v0, LX/AK0;->j:LX/AK0;

    if-nez v0, :cond_1

    .line 1662474
    const-class v1, LX/AK0;

    monitor-enter v1

    .line 1662475
    :try_start_0
    sget-object v0, LX/AK0;->j:LX/AK0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1662476
    if-eqz v2, :cond_0

    .line 1662477
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1662478
    new-instance v3, LX/AK0;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    .line 1662479
    new-instance v7, LX/AKg;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    invoke-direct {v7, v5, v6}, LX/AKg;-><init>(LX/0tX;Ljava/util/concurrent/ExecutorService;)V

    .line 1662480
    move-object v5, v7

    .line 1662481
    check-cast v5, LX/AKg;

    .line 1662482
    new-instance v6, LX/AK4;

    const/16 v7, 0x12cb

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-direct {v6, v7}, LX/AK4;-><init>(LX/0Or;)V

    .line 1662483
    move-object v6, v6

    .line 1662484
    check-cast v6, LX/AK4;

    invoke-static {v0}, LX/AKN;->a(LX/0QB;)LX/AKN;

    move-result-object v7

    check-cast v7, LX/AKN;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v8

    check-cast v8, LX/0SG;

    invoke-static {v0}, LX/AKO;->a(LX/0QB;)LX/AKO;

    move-result-object v9

    check-cast v9, LX/AKO;

    invoke-direct/range {v3 .. v9}, LX/AK0;-><init>(Ljava/util/concurrent/ExecutorService;LX/AKg;LX/AK4;LX/AKN;LX/0SG;LX/AKO;)V

    .line 1662485
    move-object v0, v3

    .line 1662486
    sput-object v0, LX/AK0;->j:LX/AK0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1662487
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1662488
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1662489
    :cond_1
    sget-object v0, LX/AK0;->j:LX/AK0;

    return-object v0

    .line 1662490
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1662491
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/AK0;LX/0Px;)LX/0Px;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/snacks/model/SnackThreadMetadata;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/snacks/model/SnackThreadMetadata;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1662465
    iget-object v0, p0, LX/AK0;->h:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    sget-wide v2, LX/AK0;->a:J

    sub-long v2, v0, v2

    .line 1662466
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 1662467
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_1

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;

    .line 1662468
    iget-wide v8, v0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->i:J

    move-wide v6, v8

    .line 1662469
    cmp-long v6, v6, v2

    if-lez v6, :cond_0

    .line 1662470
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1662471
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1662472
    :cond_1
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(ZLX/AJz;)V
    .locals 3

    .prologue
    .line 1662455
    iget-object v0, p0, LX/AK0;->b:LX/AKg;

    new-instance v1, LX/AJy;

    invoke-direct {v1, p0, p1, p2}, LX/AJy;-><init>(LX/AK0;ZLX/AJz;)V

    .line 1662456
    if-eqz p1, :cond_0

    sget-object v2, LX/0zS;->d:LX/0zS;

    .line 1662457
    :goto_0
    new-instance p0, LX/AKQ;

    invoke-direct {p0}, LX/AKQ;-><init>()V

    move-object p0, p0

    .line 1662458
    const-string p2, "3"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object p1

    invoke-virtual {p0, p2, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 1662459
    const-string p2, "2"

    const/16 p1, 0x32

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p0, p2, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1662460
    invoke-static {p0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object p0

    invoke-virtual {p0, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object p0

    .line 1662461
    iget-object p2, v0, LX/AKg;->b:LX/0tX;

    invoke-virtual {p2, p0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object p0

    move-object p0, p0

    .line 1662462
    new-instance p2, LX/AKf;

    invoke-direct {p2, v0, v1}, LX/AKf;-><init>(LX/AKg;LX/AJy;)V

    iget-object p1, v0, LX/AKg;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {p0, p2, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1662463
    return-void

    .line 1662464
    :cond_0
    sget-object v2, LX/0zS;->b:LX/0zS;

    goto :goto_0
.end method
