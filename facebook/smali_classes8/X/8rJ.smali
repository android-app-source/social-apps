.class public LX/8rJ;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field private final a:Lcom/facebook/content/SecureContextHelper;

.field private final b:LX/1nG;

.field private final c:LX/17Y;

.field private final d:Landroid/content/Context;

.field private final e:Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange;

.field private final f:Z


# direct methods
.method public constructor <init>(LX/17Y;Lcom/facebook/content/SecureContextHelper;LX/1nG;Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange;Z)V
    .locals 0
    .param p4    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1408715
    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    .line 1408716
    iput-object p1, p0, LX/8rJ;->c:LX/17Y;

    .line 1408717
    iput-object p2, p0, LX/8rJ;->a:Lcom/facebook/content/SecureContextHelper;

    .line 1408718
    iput-object p3, p0, LX/8rJ;->b:LX/1nG;

    .line 1408719
    iput-object p4, p0, LX/8rJ;->d:Landroid/content/Context;

    .line 1408720
    iput-object p5, p0, LX/8rJ;->e:Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange;

    .line 1408721
    iput-boolean p6, p0, LX/8rJ;->f:Z

    .line 1408722
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1408723
    iget-object v0, p0, LX/8rJ;->e:Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8rJ;->e:Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntity;->j()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1408724
    :cond_0
    :goto_0
    return-void

    .line 1408725
    :cond_1
    iget-object v0, p0, LX/8rJ;->b:LX/1nG;

    iget-object v1, p0, LX/8rJ;->e:Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComposedEntityAtRange;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v1

    invoke-static {v1}, LX/8rK;->a(Lcom/facebook/graphql/model/GraphQLEntity;)LX/1yA;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1nG;->a(LX/1yA;)Ljava/lang/String;

    move-result-object v0

    .line 1408726
    iget-object v1, p0, LX/8rJ;->c:LX/17Y;

    iget-object v2, p0, LX/8rJ;->d:Landroid/content/Context;

    invoke-interface {v1, v2, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1408727
    iget-boolean v1, p0, LX/8rJ;->f:Z

    if-nez v1, :cond_2

    .line 1408728
    iget-object v1, p0, LX/8rJ;->a:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/8rJ;->d:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0

    .line 1408729
    :cond_2
    iget-object v1, p0, LX/8rJ;->a:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/8rJ;->d:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 1408730
    invoke-super {p0, p1}, Landroid/text/style/ClickableSpan;->updateDrawState(Landroid/text/TextPaint;)V

    .line 1408731
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 1408732
    iget-object v1, p0, LX/8rJ;->d:Landroid/content/Context;

    iget-boolean v0, p0, LX/8rJ;->f:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0a0427

    :goto_0
    invoke-static {v1, v0}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 1408733
    return-void

    .line 1408734
    :cond_0
    const v0, 0x7f0a00aa

    goto :goto_0
.end method
