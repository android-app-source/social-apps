.class public final LX/9Eq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/9Er;

.field private final b:Lcom/facebook/graphql/model/GraphQLPage;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/9Er;Lcom/facebook/graphql/model/GraphQLPage;)V
    .locals 0

    .prologue
    .line 1457516
    iput-object p1, p0, LX/9Eq;->a:LX/9Er;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1457517
    iput-object p2, p0, LX/9Eq;->b:Lcom/facebook/graphql/model/GraphQLPage;

    .line 1457518
    return-void
.end method

.method public constructor <init>(LX/9Er;Lcom/facebook/graphql/model/GraphQLPage;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1457519
    iput-object p1, p0, LX/9Eq;->a:LX/9Er;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1457520
    iput-object p2, p0, LX/9Eq;->b:Lcom/facebook/graphql/model/GraphQLPage;

    .line 1457521
    iput-object p3, p0, LX/9Eq;->c:Ljava/lang/String;

    .line 1457522
    iput-object p4, p0, LX/9Eq;->d:Ljava/lang/String;

    .line 1457523
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x5ff3dd0d

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1457524
    iget-object v1, p0, LX/9Eq;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/9Eq;->d:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1457525
    iget-object v1, p0, LX/9Eq;->a:LX/9Er;

    iget-object v1, v1, LX/9Er;->d:LX/8yJ;

    iget-object v2, p0, LX/9Eq;->c:Ljava/lang/String;

    iget-object v3, p0, LX/9Eq;->b:Lcom/facebook/graphql/model/GraphQLPage;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/9Eq;->d:Ljava/lang/String;

    .line 1457526
    iget-object v6, v1, LX/8yJ;->b:LX/0Zb;

    const-string v7, "social_search_full_map_card_click"

    const/4 p1, 0x0

    invoke-interface {v6, v7, p1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v6

    .line 1457527
    invoke-virtual {v6}, LX/0oG;->a()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1457528
    const-string v7, "social_search"

    invoke-virtual {v6, v7}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 1457529
    iget-object v7, v1, LX/8yJ;->c:LX/0kv;

    iget-object p1, v1, LX/8yJ;->a:Landroid/content/Context;

    invoke-virtual {v7, p1}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    .line 1457530
    invoke-virtual {v6, v2}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    .line 1457531
    const-string v7, "story_graphql_id"

    invoke-virtual {v6, v7, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1457532
    const-string v7, "place_id"

    invoke-virtual {v6, v7, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1457533
    const-string v7, "entrypoint"

    invoke-virtual {v6, v7, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1457534
    invoke-virtual {v6}, LX/0oG;->d()V

    .line 1457535
    :cond_0
    sget-object v1, LX/0ax;->aE:Ljava/lang/String;

    iget-object v2, p0, LX/9Eq;->b:Lcom/facebook/graphql/model/GraphQLPage;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1457536
    iget-object v2, p0, LX/9Eq;->a:LX/9Er;

    iget-object v2, v2, LX/9Er;->b:LX/17W;

    iget-object v3, p0, LX/9Eq;->a:LX/9Er;

    iget-object v3, v3, LX/9Er;->a:Landroid/content/Context;

    invoke-virtual {v2, v3, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 1457537
    const v1, -0x38167bba

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
