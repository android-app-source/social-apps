.class public final LX/A9Q;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 16

    .prologue
    .line 1634377
    const/4 v12, 0x0

    .line 1634378
    const/4 v11, 0x0

    .line 1634379
    const/4 v10, 0x0

    .line 1634380
    const/4 v9, 0x0

    .line 1634381
    const/4 v8, 0x0

    .line 1634382
    const/4 v7, 0x0

    .line 1634383
    const/4 v6, 0x0

    .line 1634384
    const/4 v5, 0x0

    .line 1634385
    const/4 v4, 0x0

    .line 1634386
    const/4 v3, 0x0

    .line 1634387
    const/4 v2, 0x0

    .line 1634388
    const/4 v1, 0x0

    .line 1634389
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v13, v14, :cond_1

    .line 1634390
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1634391
    const/4 v1, 0x0

    .line 1634392
    :goto_0
    return v1

    .line 1634393
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1634394
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->END_OBJECT:LX/15z;

    if-eq v13, v14, :cond_b

    .line 1634395
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v13

    .line 1634396
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1634397
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v14, v15, :cond_1

    if-eqz v13, :cond_1

    .line 1634398
    const-string v14, "account_info"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 1634399
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto :goto_1

    .line 1634400
    :cond_2
    const-string v14, "ads_currency"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 1634401
    invoke-static/range {p0 .. p1}, LX/AA2;->a(LX/15w;LX/186;)I

    move-result v11

    goto :goto_1

    .line 1634402
    :cond_3
    const-string v14, "can_update_currency"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 1634403
    const/4 v2, 0x1

    .line 1634404
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    goto :goto_1

    .line 1634405
    :cond_4
    const-string v14, "id"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 1634406
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 1634407
    :cond_5
    const-string v14, "is_locked_into_checkout"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 1634408
    const/4 v1, 0x1

    .line 1634409
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v8

    goto :goto_1

    .line 1634410
    :cond_6
    const-string v14, "legacy_account_id"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 1634411
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 1634412
    :cond_7
    const-string v14, "name"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 1634413
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_1

    .line 1634414
    :cond_8
    const-string v14, "payment_info"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 1634415
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto/16 :goto_1

    .line 1634416
    :cond_9
    const-string v14, "spend_info"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_a

    .line 1634417
    invoke-static/range {p0 .. p1}, LX/A9O;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 1634418
    :cond_a
    const-string v14, "timezone_info"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 1634419
    invoke-static/range {p0 .. p1}, LX/A9P;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 1634420
    :cond_b
    const/16 v13, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 1634421
    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 1634422
    const/4 v12, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 1634423
    if-eqz v2, :cond_c

    .line 1634424
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->a(IZ)V

    .line 1634425
    :cond_c
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1634426
    if-eqz v1, :cond_d

    .line 1634427
    const/4 v1, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v8}, LX/186;->a(IZ)V

    .line 1634428
    :cond_d
    const/4 v1, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1634429
    const/4 v1, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1634430
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1634431
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1634432
    const/16 v1, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1634433
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1634434
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1634435
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1634436
    if-eqz v0, :cond_0

    .line 1634437
    const-string v1, "account_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634438
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1634439
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1634440
    if-eqz v0, :cond_1

    .line 1634441
    const-string v1, "ads_currency"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634442
    invoke-static {p0, v0, p2}, LX/AA2;->a(LX/15i;ILX/0nX;)V

    .line 1634443
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1634444
    if-eqz v0, :cond_2

    .line 1634445
    const-string v1, "can_update_currency"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634446
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1634447
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1634448
    if-eqz v0, :cond_3

    .line 1634449
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634450
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1634451
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1634452
    if-eqz v0, :cond_4

    .line 1634453
    const-string v1, "is_locked_into_checkout"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634454
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1634455
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1634456
    if-eqz v0, :cond_5

    .line 1634457
    const-string v1, "legacy_account_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634458
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1634459
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1634460
    if-eqz v0, :cond_6

    .line 1634461
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634462
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1634463
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1634464
    if-eqz v0, :cond_7

    .line 1634465
    const-string v1, "payment_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634466
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1634467
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1634468
    if-eqz v0, :cond_8

    .line 1634469
    const-string v1, "spend_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634470
    invoke-static {p0, v0, p2, p3}, LX/A9O;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1634471
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1634472
    if-eqz v0, :cond_9

    .line 1634473
    const-string v1, "timezone_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634474
    invoke-static {p0, v0, p2}, LX/BYT;->a(LX/15i;ILX/0nX;)V

    .line 1634475
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1634476
    return-void
.end method
