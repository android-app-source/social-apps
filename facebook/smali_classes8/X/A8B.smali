.class public final LX/A8B;
.super LX/5OU;
.source ""


# instance fields
.field public final synthetic h:Lcom/facebook/universalfeedback/ui/UniversalFeedbackPopoverViewFlipper;


# direct methods
.method public constructor <init>(Lcom/facebook/universalfeedback/ui/UniversalFeedbackPopoverViewFlipper;)V
    .locals 0

    .prologue
    .line 1627365
    iput-object p1, p0, LX/A8B;->h:Lcom/facebook/universalfeedback/ui/UniversalFeedbackPopoverViewFlipper;

    invoke-direct {p0, p1}, LX/5OU;-><init>(Lcom/facebook/fbui/popover/PopoverViewFlipper;)V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/universalfeedback/ui/UniversalFeedbackPopoverViewFlipper;B)V
    .locals 0

    .prologue
    .line 1627364
    invoke-direct {p0, p1}, LX/A8B;-><init>(Lcom/facebook/universalfeedback/ui/UniversalFeedbackPopoverViewFlipper;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1627354
    new-instance v0, LX/A84;

    iget-object v1, p0, LX/A8B;->h:Lcom/facebook/universalfeedback/ui/UniversalFeedbackPopoverViewFlipper;

    invoke-virtual {v1}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackPopoverViewFlipper;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/A84;-><init>(Landroid/content/Context;)V

    .line 1627355
    iget-object v1, p0, LX/5OU;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, LX/A84;->a(Landroid/view/View;)V

    .line 1627356
    iget-object v1, p0, LX/5OU;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, LX/A84;->a(Landroid/view/View;)V

    .line 1627357
    iget-object v0, p0, LX/A8B;->h:Lcom/facebook/universalfeedback/ui/UniversalFeedbackPopoverViewFlipper;

    invoke-virtual {v0}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackPopoverViewFlipper;->getPaddingLeft()I

    move-result v0

    iget-object v1, p0, LX/A8B;->h:Lcom/facebook/universalfeedback/ui/UniversalFeedbackPopoverViewFlipper;

    invoke-virtual {v1}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackPopoverViewFlipper;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    .line 1627358
    iget-object v1, p0, LX/5OU;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v1, v0

    iput v1, p0, LX/A8B;->e:I

    .line 1627359
    iget-object v1, p0, LX/5OU;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, LX/A8B;->f:I

    .line 1627360
    iget-object v0, p0, LX/A8B;->h:Lcom/facebook/universalfeedback/ui/UniversalFeedbackPopoverViewFlipper;

    invoke-virtual {v0}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackPopoverViewFlipper;->getPaddingTop()I

    move-result v0

    iget-object v1, p0, LX/A8B;->h:Lcom/facebook/universalfeedback/ui/UniversalFeedbackPopoverViewFlipper;

    invoke-virtual {v1}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackPopoverViewFlipper;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    .line 1627361
    iget-object v1, p0, LX/5OU;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v1, v0

    iput v1, p0, LX/A8B;->c:I

    .line 1627362
    iget-object v1, p0, LX/5OU;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, LX/A8B;->d:I

    .line 1627363
    return-void
.end method
