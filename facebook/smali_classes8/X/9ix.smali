.class public LX/9ix;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/9iw;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1528908
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1528909
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;LX/BIz;Lcom/facebook/photos/tagging/shared/TagTypeahead;IJLcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;)LX/9iw;
    .locals 19

    .prologue
    .line 1528910
    new-instance v3, LX/9iw;

    invoke-static/range {p0 .. p0}, LX/8nM;->a(LX/0QB;)LX/8nM;

    move-result-object v11

    check-cast v11, LX/8nB;

    const-class v2, LX/8nX;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/8nX;

    const-class v2, LX/8nd;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/8nd;

    invoke-static/range {p0 .. p0}, LX/K5H;->a(LX/0QB;)LX/K5H;

    move-result-object v14

    check-cast v14, LX/8nF;

    invoke-static/range {p0 .. p0}, LX/8Jc;->a(LX/0QB;)LX/8Jc;

    move-result-object v15

    check-cast v15, LX/8Jc;

    invoke-static/range {p0 .. p0}, LX/75Q;->a(LX/0QB;)LX/75Q;

    move-result-object v16

    check-cast v16, LX/75Q;

    invoke-static/range {p0 .. p0}, LX/75F;->a(LX/0QB;)LX/75F;

    move-result-object v17

    check-cast v17, LX/75F;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v18

    check-cast v18, LX/0ad;

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move/from16 v7, p4

    move-wide/from16 v8, p5

    move-object/from16 v10, p7

    invoke-direct/range {v3 .. v18}, LX/9iw;-><init>(Landroid/content/Context;LX/BIz;Lcom/facebook/photos/tagging/shared/TagTypeahead;IJLcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;LX/8nB;LX/8nX;LX/8nd;LX/8nF;LX/8Jc;LX/75Q;LX/75F;LX/0ad;)V

    .line 1528911
    return-object v3
.end method
