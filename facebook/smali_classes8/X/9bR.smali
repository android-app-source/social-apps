.class public final LX/9bR;
.super LX/1ci;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1ci",
        "<",
        "LX/1FJ",
        "<",
        "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/support/v4/app/DialogFragment;

.field public final synthetic b:Landroid/support/v4/app/FragmentActivity;

.field public final synthetic c:J

.field public final synthetic d:J

.field public final synthetic e:LX/1bf;

.field public final synthetic f:Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;Landroid/support/v4/app/DialogFragment;Landroid/support/v4/app/FragmentActivity;JJLX/1bf;)V
    .locals 0

    .prologue
    .line 1514930
    iput-object p1, p0, LX/9bR;->f:Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    iput-object p2, p0, LX/9bR;->a:Landroid/support/v4/app/DialogFragment;

    iput-object p3, p0, LX/9bR;->b:Landroid/support/v4/app/FragmentActivity;

    iput-wide p4, p0, LX/9bR;->c:J

    iput-wide p6, p0, LX/9bR;->d:J

    iput-object p8, p0, LX/9bR;->e:LX/1bf;

    invoke-direct {p0}, LX/1ci;-><init>()V

    return-void
.end method


# virtual methods
.method public final e(LX/1ca;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1514913
    invoke-interface {p1}, LX/1ca;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1514914
    :goto_0
    return-void

    .line 1514915
    :cond_0
    invoke-interface {p1}, LX/1ca;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    .line 1514916
    new-instance v2, LX/1lZ;

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1FK;

    invoke-direct {v2, v1}, LX/1lZ;-><init>(LX/1FK;)V

    .line 1514917
    invoke-static {v2}, LX/1la;->b(Ljava/io/InputStream;)LX/1lW;

    move-result-object v1

    .line 1514918
    sget-object v3, LX/1ld;->a:LX/1lW;

    if-ne v1, v3, :cond_1

    .line 1514919
    invoke-virtual {v2}, LX/1lZ;->reset()V

    .line 1514920
    new-instance v1, LX/9bQ;

    invoke-direct {v1, p0, v0}, LX/9bQ;-><init>(LX/9bR;LX/1FJ;)V

    .line 1514921
    iget-object v0, p0, LX/9bR;->f:Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    iget-object v0, v0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->h:Ljava/util/concurrent/ExecutorService;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/io/InputStream;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    invoke-virtual {v1, v0, v3}, LX/3nE;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 1514922
    :cond_1
    invoke-static {v2}, LX/1md;->a(Ljava/io/InputStream;)V

    .line 1514923
    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    .line 1514924
    iget-object v1, p0, LX/9bR;->f:Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    iget-wide v2, p0, LX/9bR;->c:J

    iget-object v4, p0, LX/9bR;->b:Landroid/support/v4/app/FragmentActivity;

    iget-object v5, p0, LX/9bR;->a:Landroid/support/v4/app/DialogFragment;

    iget-wide v6, p0, LX/9bR;->d:J

    iget-object v8, p0, LX/9bR;->e:LX/1bf;

    .line 1514925
    invoke-static/range {v1 .. v8}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a$redex0(Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;JLandroid/support/v4/app/FragmentActivity;Landroid/support/v4/app/DialogFragment;JLX/1bf;)V

    .line 1514926
    goto :goto_0
.end method

.method public final f(LX/1ca;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1514927
    iget-object v0, p0, LX/9bR;->b:Landroid/support/v4/app/FragmentActivity;

    .line 1514928
    const p0, 0x7f081200

    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p0

    invoke-virtual {p0}, Landroid/widget/Toast;->show()V

    .line 1514929
    return-void
.end method
