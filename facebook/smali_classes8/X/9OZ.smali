.class public final LX/9OZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Z

.field public C:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupRelatedInformationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGroupContentRestrictionReason;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Z

.field public I:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:Lcom/facebook/graphql/enums/GraphQLGroupPendingState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public M:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public N:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "visibilitySentence"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public O:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "visibilitySentence"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public P:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel$WorkCommunitiesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$ArchivedMessageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:J

.field public c:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "bookmarkImage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "bookmarkImage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/enums/GraphQLGroupCategory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CommunityLocationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "coverPhoto"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "coverPhoto"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:LX/2uF;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "emailDomains"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupMemberProfilesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "groupMembers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "groupMembers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:I

.field public r:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "groupPendingMembers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "groupPendingMembers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "groupPendingStories"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "groupPendingStories"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "groupReportedStories"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "groupReportedStories"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupTopicTagsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1481194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1481195
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;
    .locals 37

    .prologue
    .line 1481196
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1481197
    move-object/from16 v0, p0

    iget-object v3, v0, LX/9OZ;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$ArchivedMessageModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1481198
    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    move-object/from16 v0, p0

    iget-object v5, v0, LX/9OZ;->c:LX/15i;

    move-object/from16 v0, p0

    iget v6, v0, LX/9OZ;->d:I

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const v4, -0x2dcee0eb

    invoke-static {v5, v6, v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;

    move-result-object v4

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1481199
    move-object/from16 v0, p0

    iget-object v4, v0, LX/9OZ;->e:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-virtual {v2, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    .line 1481200
    move-object/from16 v0, p0

    iget-object v4, v0, LX/9OZ;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CommunityLocationModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1481201
    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    move-object/from16 v0, p0

    iget-object v5, v0, LX/9OZ;->g:LX/15i;

    move-object/from16 v0, p0

    iget v6, v0, LX/9OZ;->h:I

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const v4, -0x296bbbdf

    invoke-static {v5, v6, v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;

    move-result-object v4

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1481202
    move-object/from16 v0, p0

    iget-object v4, v0, LX/9OZ;->i:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 1481203
    move-object/from16 v0, p0

    iget-object v4, v0, LX/9OZ;->j:LX/2uF;

    invoke-static {v4, v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v13

    .line 1481204
    move-object/from16 v0, p0

    iget-object v4, v0, LX/9OZ;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 1481205
    move-object/from16 v0, p0

    iget-object v4, v0, LX/9OZ;->l:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 1481206
    move-object/from16 v0, p0

    iget-object v4, v0, LX/9OZ;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupEventsModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 1481207
    move-object/from16 v0, p0

    iget-object v4, v0, LX/9OZ;->n:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupMemberProfilesModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 1481208
    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_2
    move-object/from16 v0, p0

    iget-object v5, v0, LX/9OZ;->o:LX/15i;

    move-object/from16 v0, p0

    iget v6, v0, LX/9OZ;->p:I

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    const v4, -0x59f9285f

    invoke-static {v5, v6, v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;

    move-result-object v4

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 1481209
    move-object/from16 v0, p0

    iget-object v4, v0, LX/9OZ;->r:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 1481210
    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_3
    move-object/from16 v0, p0

    iget-object v5, v0, LX/9OZ;->s:LX/15i;

    move-object/from16 v0, p0

    iget v6, v0, LX/9OZ;->t:I

    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    const v4, -0x4020ab03

    invoke-static {v5, v6, v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;

    move-result-object v4

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 1481211
    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_4
    move-object/from16 v0, p0

    iget-object v5, v0, LX/9OZ;->u:LX/15i;

    move-object/from16 v0, p0

    iget v6, v0, LX/9OZ;->v:I

    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    const v4, -0x20692170

    invoke-static {v5, v6, v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;

    move-result-object v4

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 1481212
    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_5
    move-object/from16 v0, p0

    iget-object v5, v0, LX/9OZ;->w:LX/15i;

    move-object/from16 v0, p0

    iget v6, v0, LX/9OZ;->x:I

    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    const v4, -0x144dcd62

    invoke-static {v5, v6, v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;

    move-result-object v4

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 1481213
    move-object/from16 v0, p0

    iget-object v4, v0, LX/9OZ;->y:LX/0Px;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v23

    .line 1481214
    move-object/from16 v0, p0

    iget-object v4, v0, LX/9OZ;->A:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v24

    .line 1481215
    move-object/from16 v0, p0

    iget-object v4, v0, LX/9OZ;->C:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v25

    .line 1481216
    move-object/from16 v0, p0

    iget-object v4, v0, LX/9OZ;->D:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupRelatedInformationModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v26

    .line 1481217
    move-object/from16 v0, p0

    iget-object v4, v0, LX/9OZ;->E:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v27

    .line 1481218
    move-object/from16 v0, p0

    iget-object v4, v0, LX/9OZ;->F:LX/0Px;

    invoke-virtual {v2, v4}, LX/186;->c(Ljava/util/List;)I

    move-result v28

    .line 1481219
    move-object/from16 v0, p0

    iget-object v4, v0, LX/9OZ;->G:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v29

    .line 1481220
    move-object/from16 v0, p0

    iget-object v4, v0, LX/9OZ;->I:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v2, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v30

    .line 1481221
    move-object/from16 v0, p0

    iget-object v4, v0, LX/9OZ;->J:Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;

    invoke-virtual {v2, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v31

    .line 1481222
    move-object/from16 v0, p0

    iget-object v4, v0, LX/9OZ;->K:Lcom/facebook/graphql/enums/GraphQLGroupPendingState;

    invoke-virtual {v2, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v32

    .line 1481223
    move-object/from16 v0, p0

    iget-object v4, v0, LX/9OZ;->L:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    invoke-virtual {v2, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v33

    .line 1481224
    move-object/from16 v0, p0

    iget-object v4, v0, LX/9OZ;->M:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-virtual {v2, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v34

    .line 1481225
    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_6
    move-object/from16 v0, p0

    iget-object v5, v0, LX/9OZ;->N:LX/15i;

    move-object/from16 v0, p0

    iget v6, v0, LX/9OZ;->O:I

    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_6

    const v4, -0x73721085

    invoke-static {v5, v6, v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$DraculaImplementation;

    move-result-object v4

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v35

    .line 1481226
    move-object/from16 v0, p0

    iget-object v4, v0, LX/9OZ;->P:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$MultiCompanyGroupCompaniesModel$WorkCommunitiesModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v36

    .line 1481227
    const/16 v4, 0x23

    invoke-virtual {v2, v4}, LX/186;->c(I)V

    .line 1481228
    const/4 v4, 0x0

    invoke-virtual {v2, v4, v3}, LX/186;->b(II)V

    .line 1481229
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/9OZ;->b:J

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1481230
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 1481231
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v9}, LX/186;->b(II)V

    .line 1481232
    const/4 v3, 0x4

    invoke-virtual {v2, v3, v10}, LX/186;->b(II)V

    .line 1481233
    const/4 v3, 0x5

    invoke-virtual {v2, v3, v11}, LX/186;->b(II)V

    .line 1481234
    const/4 v3, 0x6

    invoke-virtual {v2, v3, v12}, LX/186;->b(II)V

    .line 1481235
    const/4 v3, 0x7

    invoke-virtual {v2, v3, v13}, LX/186;->b(II)V

    .line 1481236
    const/16 v3, 0x8

    invoke-virtual {v2, v3, v14}, LX/186;->b(II)V

    .line 1481237
    const/16 v3, 0x9

    invoke-virtual {v2, v3, v15}, LX/186;->b(II)V

    .line 1481238
    const/16 v3, 0xa

    move/from16 v0, v16

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1481239
    const/16 v3, 0xb

    move/from16 v0, v17

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1481240
    const/16 v3, 0xc

    move/from16 v0, v18

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1481241
    const/16 v3, 0xd

    move-object/from16 v0, p0

    iget v4, v0, LX/9OZ;->q:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 1481242
    const/16 v3, 0xe

    move/from16 v0, v19

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1481243
    const/16 v3, 0xf

    move/from16 v0, v20

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1481244
    const/16 v3, 0x10

    move/from16 v0, v21

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1481245
    const/16 v3, 0x11

    move/from16 v0, v22

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1481246
    const/16 v3, 0x12

    move/from16 v0, v23

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1481247
    const/16 v3, 0x13

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/9OZ;->z:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1481248
    const/16 v3, 0x14

    move/from16 v0, v24

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1481249
    const/16 v3, 0x15

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/9OZ;->B:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1481250
    const/16 v3, 0x16

    move/from16 v0, v25

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1481251
    const/16 v3, 0x17

    move/from16 v0, v26

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1481252
    const/16 v3, 0x18

    move/from16 v0, v27

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1481253
    const/16 v3, 0x19

    move/from16 v0, v28

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1481254
    const/16 v3, 0x1a

    move/from16 v0, v29

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1481255
    const/16 v3, 0x1b

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/9OZ;->H:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1481256
    const/16 v3, 0x1c

    move/from16 v0, v30

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1481257
    const/16 v3, 0x1d

    move/from16 v0, v31

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1481258
    const/16 v3, 0x1e

    move/from16 v0, v32

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1481259
    const/16 v3, 0x1f

    move/from16 v0, v33

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1481260
    const/16 v3, 0x20

    move/from16 v0, v34

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1481261
    const/16 v3, 0x21

    move/from16 v0, v35

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1481262
    const/16 v3, 0x22

    move/from16 v0, v36

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1481263
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1481264
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1481265
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1481266
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1481267
    new-instance v2, LX/15i;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1481268
    new-instance v3, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    invoke-direct {v3, v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;-><init>(LX/15i;)V

    .line 1481269
    return-object v3

    .line 1481270
    :catchall_0
    move-exception v2

    :try_start_7
    monitor-exit v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    throw v2

    .line 1481271
    :catchall_1
    move-exception v2

    :try_start_8
    monitor-exit v4
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    throw v2

    .line 1481272
    :catchall_2
    move-exception v2

    :try_start_9
    monitor-exit v4
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    throw v2

    .line 1481273
    :catchall_3
    move-exception v2

    :try_start_a
    monitor-exit v4
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    throw v2

    .line 1481274
    :catchall_4
    move-exception v2

    :try_start_b
    monitor-exit v4
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    throw v2

    .line 1481275
    :catchall_5
    move-exception v2

    :try_start_c
    monitor-exit v4
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_5

    throw v2

    .line 1481276
    :catchall_6
    move-exception v2

    :try_start_d
    monitor-exit v4
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_6

    throw v2
.end method
