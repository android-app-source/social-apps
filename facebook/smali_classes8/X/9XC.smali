.class public final enum LX/9XC;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9XC;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9XC;

.field public static final enum ACTION:LX/9XC;

.field public static final enum ADMIN:LX/9XC;

.field public static final enum ADMIN_ACTION_BAR:LX/9XC;

.field public static final enum ADMIN_ACTIVITY_TAB:LX/9XC;

.field public static final enum ADMIN_APPS_TAB:LX/9XC;

.field public static final enum ADMIN_CONTACT_INBOX:LX/9XC;

.field public static final enum ADMIN_EDIT_PAGE:LX/9XC;

.field public static final enum BOOKMARK:LX/9XC;

.field public static final enum NETWORK:LX/9XC;

.field public static final enum NETWORK_FAILURE:LX/9XC;

.field public static final enum NETWORK_SUCCESS:LX/9XC;

.field public static final enum OTHER:LX/9XC;

.field public static final enum REDIRECTION:LX/9XC;

.field public static final enum TAB_SWITCH:LX/9XC;

.field public static final enum TAP:LX/9XC;

.field public static final enum VIEW:LX/9XC;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1502463
    new-instance v0, LX/9XC;

    const-string v1, "TAP"

    invoke-direct {v0, v1, v3}, LX/9XC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9XC;->TAP:LX/9XC;

    .line 1502464
    new-instance v0, LX/9XC;

    const-string v1, "ACTION"

    invoke-direct {v0, v1, v4}, LX/9XC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9XC;->ACTION:LX/9XC;

    .line 1502465
    new-instance v0, LX/9XC;

    const-string v1, "NETWORK"

    invoke-direct {v0, v1, v5}, LX/9XC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9XC;->NETWORK:LX/9XC;

    .line 1502466
    new-instance v0, LX/9XC;

    const-string v1, "NETWORK_SUCCESS"

    invoke-direct {v0, v1, v6}, LX/9XC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9XC;->NETWORK_SUCCESS:LX/9XC;

    .line 1502467
    new-instance v0, LX/9XC;

    const-string v1, "NETWORK_FAILURE"

    invoke-direct {v0, v1, v7}, LX/9XC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9XC;->NETWORK_FAILURE:LX/9XC;

    .line 1502468
    new-instance v0, LX/9XC;

    const-string v1, "ADMIN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/9XC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9XC;->ADMIN:LX/9XC;

    .line 1502469
    new-instance v0, LX/9XC;

    const-string v1, "ADMIN_ACTIVITY_TAB"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/9XC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9XC;->ADMIN_ACTIVITY_TAB:LX/9XC;

    .line 1502470
    new-instance v0, LX/9XC;

    const-string v1, "ADMIN_EDIT_PAGE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/9XC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9XC;->ADMIN_EDIT_PAGE:LX/9XC;

    .line 1502471
    new-instance v0, LX/9XC;

    const-string v1, "ADMIN_ACTION_BAR"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/9XC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9XC;->ADMIN_ACTION_BAR:LX/9XC;

    .line 1502472
    new-instance v0, LX/9XC;

    const-string v1, "ADMIN_CONTACT_INBOX"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/9XC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9XC;->ADMIN_CONTACT_INBOX:LX/9XC;

    .line 1502473
    new-instance v0, LX/9XC;

    const-string v1, "VIEW"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/9XC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9XC;->VIEW:LX/9XC;

    .line 1502474
    new-instance v0, LX/9XC;

    const-string v1, "REDIRECTION"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/9XC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9XC;->REDIRECTION:LX/9XC;

    .line 1502475
    new-instance v0, LX/9XC;

    const-string v1, "TAB_SWITCH"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/9XC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9XC;->TAB_SWITCH:LX/9XC;

    .line 1502476
    new-instance v0, LX/9XC;

    const-string v1, "ADMIN_APPS_TAB"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/9XC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9XC;->ADMIN_APPS_TAB:LX/9XC;

    .line 1502477
    new-instance v0, LX/9XC;

    const-string v1, "BOOKMARK"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/9XC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9XC;->BOOKMARK:LX/9XC;

    .line 1502478
    new-instance v0, LX/9XC;

    const-string v1, "OTHER"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/9XC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9XC;->OTHER:LX/9XC;

    .line 1502479
    const/16 v0, 0x10

    new-array v0, v0, [LX/9XC;

    sget-object v1, LX/9XC;->TAP:LX/9XC;

    aput-object v1, v0, v3

    sget-object v1, LX/9XC;->ACTION:LX/9XC;

    aput-object v1, v0, v4

    sget-object v1, LX/9XC;->NETWORK:LX/9XC;

    aput-object v1, v0, v5

    sget-object v1, LX/9XC;->NETWORK_SUCCESS:LX/9XC;

    aput-object v1, v0, v6

    sget-object v1, LX/9XC;->NETWORK_FAILURE:LX/9XC;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/9XC;->ADMIN:LX/9XC;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/9XC;->ADMIN_ACTIVITY_TAB:LX/9XC;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/9XC;->ADMIN_EDIT_PAGE:LX/9XC;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/9XC;->ADMIN_ACTION_BAR:LX/9XC;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/9XC;->ADMIN_CONTACT_INBOX:LX/9XC;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/9XC;->VIEW:LX/9XC;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/9XC;->REDIRECTION:LX/9XC;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/9XC;->TAB_SWITCH:LX/9XC;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/9XC;->ADMIN_APPS_TAB:LX/9XC;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/9XC;->BOOKMARK:LX/9XC;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/9XC;->OTHER:LX/9XC;

    aput-object v2, v0, v1

    sput-object v0, LX/9XC;->$VALUES:[LX/9XC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1502480
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9XC;
    .locals 1

    .prologue
    .line 1502481
    const-class v0, LX/9XC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9XC;

    return-object v0
.end method

.method public static values()[LX/9XC;
    .locals 1

    .prologue
    .line 1502482
    sget-object v0, LX/9XC;->$VALUES:[LX/9XC;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9XC;

    return-object v0
.end method
