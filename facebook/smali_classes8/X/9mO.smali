.class public final LX/9mO;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:LX/9mP;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/9mP;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1535582
    iput-object p1, p0, LX/9mO;->a:LX/9mP;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    .line 1535583
    iput-object p2, p0, LX/9mO;->b:Ljava/lang/String;

    .line 1535584
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 1535585
    iget-object v0, p0, LX/9mO;->a:LX/9mP;

    iget-object v0, v0, LX/9mP;->e:Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;

    if-eqz v0, :cond_0

    .line 1535586
    iget-object v0, p0, LX/9mO;->a:LX/9mP;

    iget-object v0, v0, LX/9mP;->e:Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;

    iget-object v1, p0, LX/9mO;->b:Ljava/lang/String;

    .line 1535587
    iget-object v2, v0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->o:LX/9lc;

    iget-object v3, v0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->q:Lcom/facebook/rapidreporting/ui/DialogStateData;

    invoke-virtual {v3}, Lcom/facebook/rapidreporting/ui/DialogStateData;->b()Ljava/lang/String;

    move-result-object v3

    .line 1535588
    new-instance p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "fb4a_rapid_reporting_visit_help_center"

    invoke-direct {p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1535589
    const-string p1, "URL"

    invoke-virtual {p0, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1535590
    invoke-static {v2, p0, v3}, LX/9lc;->a(LX/9lc;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 1535591
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 1535592
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-static {}, LX/007;->p()Ljava/lang/String;

    move-result-object p0

    invoke-static {v3, p0}, LX/32v;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v3

    move v2, v3

    .line 1535593
    if-eqz v2, :cond_1

    .line 1535594
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 1535595
    sget-object v3, LX/0ax;->do:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {v3, p0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1535596
    iget-object v3, v0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->n:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-interface {v3, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1535597
    :cond_0
    :goto_0
    return-void

    .line 1535598
    :cond_1
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1535599
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1535600
    iget-object v3, v0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->n:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-interface {v3, v2, p0}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 1535601
    invoke-super {p0, p1}, Landroid/text/style/ClickableSpan;->updateDrawState(Landroid/text/TextPaint;)V

    .line 1535602
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 1535603
    iget-object v0, p0, LX/9mO;->a:LX/9mP;

    invoke-virtual {v0}, LX/9mP;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a008b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 1535604
    return-void
.end method
