.class public final LX/AQG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$CollegeEntitiesTypeAheadSuggestionsQueryModel;",
        ">;",
        "LX/0Px",
        "<",
        "Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$TypeAheadSuggestionFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventCollegeInterstitialFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventCollegeInterstitialFragment;)V
    .locals 0

    .prologue
    .line 1671063
    iput-object p1, p0, LX/AQG;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventCollegeInterstitialFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1671064
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1671065
    if-eqz p1, :cond_0

    .line 1671066
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1671067
    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    .line 1671068
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1671069
    :goto_1
    return-object v0

    .line 1671070
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1671071
    check-cast v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$CollegeEntitiesTypeAheadSuggestionsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$CollegeEntitiesTypeAheadSuggestionsQueryModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 1671072
    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    .line 1671073
    :cond_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1671074
    check-cast v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$CollegeEntitiesTypeAheadSuggestionsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$CollegeEntitiesTypeAheadSuggestionsQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v3, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$TypeAheadSuggestionFieldsModel;

    invoke-virtual {v1, v0, v2, v3}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_1

    .line 1671075
    :cond_4
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1671076
    goto :goto_1
.end method
