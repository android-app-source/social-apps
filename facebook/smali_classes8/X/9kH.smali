.class public LX/9kH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1530988
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1530989
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1530990
    const-string v0, "extra_logger_type"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/9kb;

    .line 1530991
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    new-instance v2, LX/9kG;

    invoke-direct {v2}, LX/9kG;-><init>()V

    if-eqz v0, :cond_0

    :goto_0
    const-string v3, "extra_logger_params"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    invoke-static {v1, v2, v4, v0, v3}, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->a(LX/0am;LX/9kF;ZLX/9kb;Landroid/os/Parcelable;)Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;

    move-result-object v0

    return-object v0

    :cond_0
    sget-object v0, LX/9kb;->NO_LOGGER:LX/9kb;

    goto :goto_0
.end method
