.class public final LX/ANs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ANk;


# instance fields
.field public final synthetic a:LX/ANt;


# direct methods
.method public constructor <init>(LX/ANt;)V
    .locals 0

    .prologue
    .line 1668417
    iput-object p1, p0, LX/ANs;->a:LX/ANt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1668418
    iget-object v0, p0, LX/ANs;->a:LX/ANt;

    iget-object v0, v0, LX/ANt;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->b(I)V

    .line 1668419
    iget-object v0, p0, LX/ANs;->a:LX/ANt;

    iget-object v0, v0, LX/ANt;->p:LX/ANo;

    if-eqz v0, :cond_0

    .line 1668420
    iget-object v0, p0, LX/ANs;->a:LX/ANt;

    iget-object v0, v0, LX/ANt;->p:LX/ANo;

    iget-object v1, p0, LX/ANs;->a:LX/ANt;

    iget-object v1, v1, LX/ANt;->h:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

    invoke-interface {v0, v1}, LX/ANo;->b(Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;)V

    .line 1668421
    iget-object v0, p0, LX/ANs;->a:LX/ANt;

    iget-object v0, v0, LX/ANt;->j:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 1668422
    :cond_0
    iget-object v0, p0, LX/ANs;->a:LX/ANt;

    iget-object v0, v0, LX/ANt;->r:Ljava/util/Map;

    iget-object v1, p0, LX/ANs;->a:LX/ANt;

    iget-object v1, v1, LX/ANt;->q:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ANo;

    .line 1668423
    iget-object v1, p0, LX/ANs;->a:LX/ANt;

    iget-object v1, v1, LX/ANt;->h:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

    invoke-interface {v0, v1}, LX/ANo;->a(Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;)V

    .line 1668424
    invoke-interface {v0}, LX/ANo;->a()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1668425
    iget-object v1, p0, LX/ANs;->a:LX/ANt;

    iget-object v1, v1, LX/ANt;->j:Landroid/view/ViewGroup;

    invoke-interface {v0}, LX/ANo;->a()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1668426
    iget-object v1, p0, LX/ANs;->a:LX/ANt;

    iget-object v1, v1, LX/ANt;->j:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1668427
    :goto_0
    iget-object v1, p0, LX/ANs;->a:LX/ANt;

    iget-object v1, v1, LX/ANt;->k:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1, v3}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 1668428
    iget-object v1, p0, LX/ANs;->a:LX/ANt;

    .line 1668429
    iput-object v0, v1, LX/ANt;->p:LX/ANo;

    .line 1668430
    return-void

    .line 1668431
    :cond_1
    iget-object v1, p0, LX/ANs;->a:LX/ANt;

    iget-object v1, v1, LX/ANt;->j:Landroid/view/ViewGroup;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0
.end method
