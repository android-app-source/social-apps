.class public LX/APN;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1670123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1670124
    return-void
.end method

.method public static c(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ZLX/ARN;)LX/03R;
    .locals 1
    .param p2    # LX/ARN;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1670125
    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getPlatformConfiguration()Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1670126
    sget-object v0, LX/03R;->NO:LX/03R;

    .line 1670127
    :goto_0
    return-object v0

    .line 1670128
    :cond_0
    if-eqz p1, :cond_1

    .line 1670129
    sget-object v0, LX/03R;->NO:LX/03R;

    goto :goto_0

    .line 1670130
    :cond_1
    if-eqz p2, :cond_2

    .line 1670131
    invoke-interface {p2}, LX/ARN;->a()Z

    move-result v0

    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    goto :goto_0

    .line 1670132
    :cond_2
    sget-object v0, LX/03R;->UNSET:LX/03R;

    goto :goto_0
.end method
