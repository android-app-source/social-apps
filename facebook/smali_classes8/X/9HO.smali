.class public LX/9HO;
.super Landroid/graphics/drawable/Drawable;
.source ""


# instance fields
.field private final a:Landroid/graphics/Paint;

.field private final b:Landroid/graphics/RectF;


# direct methods
.method public constructor <init>(FF)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1461207
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 1461208
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/9HO;->a:Landroid/graphics/Paint;

    .line 1461209
    iget-object v0, p0, LX/9HO;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1461210
    iget-object v0, p0, LX/9HO;->a:Landroid/graphics/Paint;

    const v1, -0x333334

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1461211
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v2, v2, p2, p1}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, LX/9HO;->b:Landroid/graphics/RectF;

    .line 1461212
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 1461213
    iget-object v0, p0, LX/9HO;->b:Landroid/graphics/RectF;

    iget-object v1, p0, LX/9HO;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 1461214
    return-void
.end method

.method public final getIntrinsicHeight()I
    .locals 1

    .prologue
    .line 1461215
    iget-object v0, p0, LX/9HO;->b:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public final getIntrinsicWidth()I
    .locals 1

    .prologue
    .line 1461216
    iget-object v0, p0, LX/9HO;->b:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 1461217
    const/4 v0, -0x3

    return v0
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 1461218
    iget-object v0, p0, LX/9HO;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1461219
    invoke-virtual {p0}, LX/9HO;->invalidateSelf()V

    .line 1461220
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 1461221
    iget-object v0, p0, LX/9HO;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 1461222
    invoke-virtual {p0}, LX/9HO;->invalidateSelf()V

    .line 1461223
    return-void
.end method
