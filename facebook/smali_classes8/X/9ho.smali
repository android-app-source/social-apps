.class public final LX/9ho;
.super LX/9hl;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/9hs;


# direct methods
.method public constructor <init>(LX/9hs;Ljava/lang/String;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1527011
    iput-object p1, p0, LX/9ho;->c:LX/9hs;

    iput-object p3, p0, LX/9ho;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    iput-object p4, p0, LX/9ho;->b:Ljava/lang/String;

    invoke-direct {p0, p2}, LX/9hl;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLMedia;
    .locals 3

    .prologue
    .line 1527012
    invoke-static {p1}, LX/4XB;->a(Lcom/facebook/graphql/model/GraphQLMedia;)LX/4XB;

    move-result-object v0

    iget-object v1, p0, LX/9ho;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    iget-object v2, p0, LX/9ho;->b:Ljava/lang/String;

    invoke-static {v1, v2}, LX/9hi;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v1

    .line 1527013
    iput-object v1, v0, LX/4XB;->E:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 1527014
    move-object v0, v0

    .line 1527015
    invoke-virtual {v0}, LX/4XB;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;)Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;
    .locals 3

    .prologue
    .line 1527016
    invoke-static {p1}, LX/5kN;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;)LX/5kN;

    move-result-object v0

    iget-object v1, p0, LX/9ho;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    iget-object v2, p0, LX/9ho;->b:Ljava/lang/String;

    invoke-static {v1, v2}, LX/9hi;->b(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Ljava/lang/String;)Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;

    move-result-object v1

    .line 1527017
    iput-object v1, v0, LX/5kN;->v:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;

    .line 1527018
    move-object v0, v0

    .line 1527019
    invoke-virtual {v0}, LX/5kN;->a()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;

    move-result-object v0

    return-object v0
.end method
