.class public abstract LX/8tJ;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""


# static fields
.field private static final d:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:Landroid/widget/VideoView;

.field public b:Landroid/widget/ImageView;

.field public c:LX/8tN;

.field private e:I

.field private f:I

.field public g:LX/8tI;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1412866
    const-class v0, LX/8tJ;

    sput-object v0, LX/8tJ;->d:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1412790
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 1412791
    iput v0, p0, LX/8tJ;->e:I

    .line 1412792
    iput v0, p0, LX/8tJ;->f:I

    .line 1412793
    sget-object v0, LX/8tI;->INIT:LX/8tI;

    iput-object v0, p0, LX/8tJ;->g:LX/8tI;

    .line 1412794
    invoke-direct {p0}, LX/8tJ;->f()V

    .line 1412795
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1412860
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1412861
    iput v0, p0, LX/8tJ;->e:I

    .line 1412862
    iput v0, p0, LX/8tJ;->f:I

    .line 1412863
    sget-object v0, LX/8tI;->INIT:LX/8tI;

    iput-object v0, p0, LX/8tJ;->g:LX/8tI;

    .line 1412864
    invoke-direct {p0}, LX/8tJ;->f()V

    .line 1412865
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1412854
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1412855
    iput v0, p0, LX/8tJ;->e:I

    .line 1412856
    iput v0, p0, LX/8tJ;->f:I

    .line 1412857
    sget-object v0, LX/8tI;->INIT:LX/8tI;

    iput-object v0, p0, LX/8tJ;->g:LX/8tI;

    .line 1412858
    invoke-direct {p0}, LX/8tJ;->f()V

    .line 1412859
    return-void
.end method

.method private b(LX/8tI;)V
    .locals 2

    .prologue
    .line 1412842
    iget-object v0, p0, LX/8tJ;->g:LX/8tI;

    if-ne p1, v0, :cond_1

    .line 1412843
    :cond_0
    :goto_0
    return-void

    .line 1412844
    :cond_1
    iget-object v0, p0, LX/8tJ;->g:LX/8tI;

    sget-object v1, LX/8tI;->PAUSED:LX/8tI;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, LX/8tJ;->g:LX/8tI;

    sget-object v1, LX/8tI;->PLAYING:LX/8tI;

    if-ne v0, v1, :cond_0

    .line 1412845
    :cond_2
    sget-object v0, LX/8tI;->PAUSED:LX/8tI;

    if-ne p1, v0, :cond_4

    .line 1412846
    iget-object v0, p0, LX/8tJ;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1412847
    iget-object v0, p0, LX/8tJ;->b:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1412848
    :cond_3
    iget-object v0, p0, LX/8tJ;->a:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->pause()V

    .line 1412849
    sget-object v0, LX/8tI;->PAUSED:LX/8tI;

    invoke-virtual {p0, v0}, LX/8tJ;->a(LX/8tI;)V

    goto :goto_0

    .line 1412850
    :cond_4
    sget-object v0, LX/8tI;->PLAYING:LX/8tI;

    if-ne p1, v0, :cond_0

    .line 1412851
    iget-object v0, p0, LX/8tJ;->b:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1412852
    iget-object v0, p0, LX/8tJ;->a:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->start()V

    .line 1412853
    sget-object v0, LX/8tI;->PLAYING:LX/8tI;

    invoke-virtual {p0, v0}, LX/8tJ;->a(LX/8tI;)V

    goto :goto_0
.end method

.method private f()V
    .locals 1

    .prologue
    .line 1412838
    const v0, 0x7f0315b4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1412839
    const v0, 0x7f0d30f0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/VideoView;

    iput-object v0, p0, LX/8tJ;->a:Landroid/widget/VideoView;

    .line 1412840
    const v0, 0x7f0d1333

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/8tJ;->b:Landroid/widget/ImageView;

    .line 1412841
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1412867
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    iget-object v0, p0, LX/8tJ;->g:LX/8tI;

    sget-object v1, LX/8tI;->PAUSED:LX/8tI;

    if-ne v0, v1, :cond_1

    .line 1412868
    iget-object v0, p0, LX/8tJ;->a:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->start()V

    .line 1412869
    iget-object v0, p0, LX/8tJ;->a:Landroid/widget/VideoView;

    invoke-virtual {v0, p1}, Landroid/widget/VideoView;->seekTo(I)V

    .line 1412870
    iget-object v0, p0, LX/8tJ;->a:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->pause()V

    .line 1412871
    iget-object v0, p0, LX/8tJ;->a:Landroid/widget/VideoView;

    invoke-static {v0, v2}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1412872
    :cond_0
    :goto_0
    return-void

    .line 1412873
    :cond_1
    iget-object v0, p0, LX/8tJ;->g:LX/8tI;

    sget-object v1, LX/8tI;->PAUSED:LX/8tI;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, LX/8tJ;->g:LX/8tI;

    sget-object v1, LX/8tI;->PLAYING:LX/8tI;

    if-ne v0, v1, :cond_0

    .line 1412874
    :cond_2
    iget-object v0, p0, LX/8tJ;->a:Landroid/widget/VideoView;

    invoke-virtual {v0, p1}, Landroid/widget/VideoView;->seekTo(I)V

    .line 1412875
    iget-object v0, p0, LX/8tJ;->a:Landroid/widget/VideoView;

    invoke-static {v0, v2}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public final a(II)V
    .locals 0

    .prologue
    .line 1412834
    iput p1, p0, LX/8tJ;->e:I

    .line 1412835
    iput p2, p0, LX/8tJ;->f:I

    .line 1412836
    invoke-virtual {p0}, LX/8tJ;->requestLayout()V

    .line 1412837
    return-void
.end method

.method public final a(LX/8tI;)V
    .locals 3

    .prologue
    .line 1412827
    iput-object p1, p0, LX/8tJ;->g:LX/8tI;

    .line 1412828
    iget-object v0, p0, LX/8tJ;->c:LX/8tN;

    if-eqz v0, :cond_0

    .line 1412829
    iget-object v0, p0, LX/8tJ;->c:LX/8tN;

    invoke-virtual {v0}, LX/8tN;->c()V

    .line 1412830
    :cond_0
    sget-object v0, LX/8tI;->ERROR:LX/8tI;

    if-ne p1, v0, :cond_1

    .line 1412831
    iget-object v0, p0, LX/8tJ;->a:Landroid/widget/VideoView;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const/high16 v2, -0x1000000

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {v0, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1412832
    :goto_0
    return-void

    .line 1412833
    :cond_1
    iget-object v0, p0, LX/8tJ;->a:Landroid/widget/VideoView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1412824
    iget-object v0, p0, LX/8tJ;->g:LX/8tI;

    sget-object v1, LX/8tI;->PAUSED:LX/8tI;

    if-eq v0, v1, :cond_0

    .line 1412825
    :goto_0
    return-void

    .line 1412826
    :cond_0
    sget-object v0, LX/8tI;->PLAYING:LX/8tI;

    invoke-direct {p0, v0}, LX/8tJ;->b(LX/8tI;)V

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1412821
    iget-object v0, p0, LX/8tJ;->g:LX/8tI;

    sget-object v1, LX/8tI;->PLAYING:LX/8tI;

    if-eq v0, v1, :cond_0

    .line 1412822
    :goto_0
    return-void

    .line 1412823
    :cond_0
    sget-object v0, LX/8tI;->PAUSED:LX/8tI;

    invoke-direct {p0, v0}, LX/8tJ;->b(LX/8tI;)V

    goto :goto_0
.end method

.method public getCurrentPosition()I
    .locals 1

    .prologue
    .line 1412820
    iget-object v0, p0, LX/8tJ;->a:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->getCurrentPosition()I

    move-result v0

    return v0
.end method

.method public final onMeasure(II)V
    .locals 8

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    const/4 v1, -0x1

    const/4 v3, 0x0

    .line 1412796
    iget v0, p0, LX/8tJ;->e:I

    if-eq v0, v1, :cond_0

    iget v0, p0, LX/8tJ;->f:I

    if-ne v0, v1, :cond_2

    .line 1412797
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomViewGroup;->onMeasure(II)V

    .line 1412798
    :cond_1
    return-void

    .line 1412799
    :cond_2
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 1412800
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 1412801
    invoke-virtual {p0}, LX/8tJ;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, LX/8tJ;->getPaddingRight()I

    move-result v4

    add-int/2addr v2, v4

    sub-int v2, v0, v2

    .line 1412802
    invoke-virtual {p0}, LX/8tJ;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, LX/8tJ;->getPaddingBottom()I

    move-result v4

    add-int/2addr v0, v4

    sub-int v0, v1, v0

    .line 1412803
    iget v1, p0, LX/8tJ;->e:I

    int-to-float v1, v1

    iget v4, p0, LX/8tJ;->f:I

    int-to-float v4, v4

    div-float v4, v1, v4

    .line 1412804
    iget v1, p0, LX/8tJ;->e:I

    mul-int/lit8 v1, v1, 0x2

    .line 1412805
    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 1412806
    int-to-float v1, v2

    div-float/2addr v1, v4

    float-to-int v1, v1

    .line 1412807
    if-le v1, v0, :cond_4

    .line 1412808
    int-to-float v1, v0

    mul-float/2addr v1, v4

    float-to-int v1, v1

    .line 1412809
    :goto_0
    invoke-static {v1, p1}, LX/8tJ;->resolveSize(II)I

    move-result v2

    .line 1412810
    invoke-static {v0, p2}, LX/8tJ;->resolveSize(II)I

    move-result v4

    .line 1412811
    invoke-virtual {p0, v2, v4}, LX/8tJ;->setMeasuredDimension(II)V

    .line 1412812
    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 1412813
    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 1412814
    invoke-virtual {p0}, LX/8tJ;->getChildCount()I

    move-result v7

    move v6, v3

    .line 1412815
    :goto_1
    if-ge v6, v7, :cond_1

    .line 1412816
    invoke-virtual {p0, v6}, LX/8tJ;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1412817
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v5, 0x8

    if-eq v0, v5, :cond_3

    move-object v0, p0

    move v5, v3

    .line 1412818
    invoke-virtual/range {v0 .. v5}, LX/8tJ;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 1412819
    :cond_3
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_1

    :cond_4
    move v0, v1

    move v1, v2

    goto :goto_0
.end method
