.class public LX/9eS;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "LX/9eR;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1519696
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1519697
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, LX/9eS;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 1519698
    return-void
.end method

.method public static a(LX/0QB;)LX/9eS;
    .locals 3

    .prologue
    .line 1519699
    const-class v1, LX/9eS;

    monitor-enter v1

    .line 1519700
    :try_start_0
    sget-object v0, LX/9eS;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1519701
    sput-object v2, LX/9eS;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1519702
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1519703
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1519704
    new-instance v0, LX/9eS;

    invoke-direct {v0}, LX/9eS;-><init>()V

    .line 1519705
    move-object v0, v0

    .line 1519706
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1519707
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/9eS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1519708
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1519709
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/9eR;)V
    .locals 1

    .prologue
    .line 1519694
    iget-object v0, p0, LX/9eS;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 1519695
    return-void
.end method

.method public final b(LX/9eR;)V
    .locals 1

    .prologue
    .line 1519692
    iget-object v0, p0, LX/9eS;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 1519693
    return-void
.end method
