.class public LX/9UL;
.super LX/9U9;
.source ""


# instance fields
.field public final c:Landroid/content/Context;

.field public final d:Landroid/view/LayoutInflater;

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/ipc/model/FacebookProfile;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1497910
    invoke-direct {p0}, LX/9U9;-><init>()V

    .line 1497911
    iput-object p1, p0, LX/9UL;->c:Landroid/content/Context;

    .line 1497912
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, LX/9UL;->d:Landroid/view/LayoutInflater;

    .line 1497913
    return-void
.end method


# virtual methods
.method public final a(I)I
    .locals 1

    .prologue
    .line 1497914
    const/4 v0, 0x0

    return v0
.end method

.method public a(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1497918
    invoke-virtual {p0, p1, p2}, LX/9UL;->a(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookProfile;

    .line 1497919
    if-nez p4, :cond_0

    .line 1497920
    iget-object v1, p0, LX/9UL;->d:Landroid/view/LayoutInflater;

    const v2, 0x7f031071

    const/4 p1, 0x0

    invoke-virtual {v1, v2, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1497921
    move-object p4, v1

    .line 1497922
    :cond_0
    const v1, 0x7f0d057e

    invoke-virtual {p4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 1497923
    iget-object v2, v0, Lcom/facebook/ipc/model/FacebookProfile;->mImageUrl:Ljava/lang/String;

    .line 1497924
    if-nez v2, :cond_1

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 1497925
    iget-object v0, v0, Lcom/facebook/ipc/model/FacebookProfile;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 1497926
    return-object p4

    .line 1497927
    :cond_1
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    goto :goto_0
.end method

.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1497915
    if-nez p2, :cond_0

    .line 1497916
    new-instance p2, Landroid/view/View;

    iget-object v0, p0, LX/9UL;->c:Landroid/content/Context;

    invoke-direct {p2, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1497917
    :cond_0
    return-object p2
.end method

.method public final a(II)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1497907
    const/4 v0, 0x0

    invoke-static {v0, p1}, LX/0sL;->a(II)V

    .line 1497908
    iget-object v0, p0, LX/9UL;->e:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1497909
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1497906
    iget-object v0, p0, LX/9UL;->e:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9UL;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(II)Z
    .locals 1

    .prologue
    .line 1497905
    const/4 v0, 0x1

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 1497904
    const/4 v0, 0x1

    return v0
.end method

.method public final c(I)I
    .locals 1

    .prologue
    .line 1497902
    const/4 v0, 0x0

    invoke-static {v0, p1}, LX/0sL;->a(II)V

    .line 1497903
    iget-object v0, p0, LX/9UL;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final c(II)I
    .locals 1

    .prologue
    .line 1497901
    const/4 v0, 0x1

    return v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 1497900
    const/4 v0, 0x2

    return v0
.end method
