.class public final LX/9Uw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Landroid/os/Message;


# direct methods
.method public constructor <init>(Landroid/os/Message;)V
    .locals 0

    .prologue
    .line 1498589
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1498590
    iput-object p1, p0, LX/9Uw;->a:Landroid/os/Message;

    .line 1498591
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1498592
    sget-object v0, Lcom/facebook/livephotos/LivePhotoView;->e:Ljava/lang/String;

    const-string v1, "Failed to prepare video"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1498593
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1498594
    check-cast p1, Ljava/lang/String;

    .line 1498595
    iget-object v0, p0, LX/9Uw;->a:Landroid/os/Message;

    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1498596
    iget-object v0, p0, LX/9Uw;->a:Landroid/os/Message;

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1498597
    return-void
.end method
