.class public final enum LX/9ih;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9ih;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9ih;

.field public static final enum FIRST_NAME:LX/9ih;

.field public static final enum FULL_NAME:LX/9ih;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1528212
    new-instance v0, LX/9ih;

    const-string v1, "FIRST_NAME"

    invoke-direct {v0, v1, v2}, LX/9ih;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9ih;->FIRST_NAME:LX/9ih;

    .line 1528213
    new-instance v0, LX/9ih;

    const-string v1, "FULL_NAME"

    invoke-direct {v0, v1, v3}, LX/9ih;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9ih;->FULL_NAME:LX/9ih;

    .line 1528214
    const/4 v0, 0x2

    new-array v0, v0, [LX/9ih;

    sget-object v1, LX/9ih;->FIRST_NAME:LX/9ih;

    aput-object v1, v0, v2

    sget-object v1, LX/9ih;->FULL_NAME:LX/9ih;

    aput-object v1, v0, v3

    sput-object v0, LX/9ih;->$VALUES:[LX/9ih;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1528215
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9ih;
    .locals 1

    .prologue
    .line 1528216
    const-class v0, LX/9ih;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9ih;

    return-object v0
.end method

.method public static values()[LX/9ih;
    .locals 1

    .prologue
    .line 1528217
    sget-object v0, LX/9ih;->$VALUES:[LX/9ih;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9ih;

    return-object v0
.end method
