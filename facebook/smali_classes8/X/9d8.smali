.class public final LX/9d8;
.super LX/1cC;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;)V
    .locals 0

    .prologue
    .line 1517531
    iput-object p1, p0, LX/9d8;->a:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    invoke-direct {p0}, LX/1cC;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 6
    .param p2    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1517532
    check-cast p2, LX/1ln;

    .line 1517533
    invoke-super {p0, p1, p2}, LX/1cC;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1517534
    iget-object v0, p0, LX/9d8;->a:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    iget-boolean v0, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->i:Z

    if-eqz v0, :cond_4

    .line 1517535
    iget-object v0, p0, LX/9d8;->a:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    iget-object v0, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->v:LX/1cC;

    if-ne p0, v0, :cond_2

    iget-object v0, p0, LX/9d8;->a:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    iget-object v0, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->p:LX/5iG;

    if-nez v0, :cond_2

    .line 1517536
    iget-object v0, p0, LX/9d8;->a:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    iget-object v1, p0, LX/9d8;->a:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    iget-object v2, p0, LX/9d8;->a:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    iget-object v2, v2, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->s:LX/1aX;

    iget-object v3, p0, LX/9d8;->a:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    iget-object v3, v3, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->y:LX/9cy;

    invoke-virtual {v3}, LX/9cy;->a()Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v3

    iget-object v4, p0, LX/9d8;->a:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    iget-object v4, v4, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->p:LX/5iG;

    invoke-static {v1, v2, v3, v4}, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->a$redex0(Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;LX/1aX;Lcom/facebook/photos/creativeediting/model/SwipeableParams;LX/5iG;)LX/5iG;

    move-result-object v1

    .line 1517537
    iput-object v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->p:LX/5iG;

    .line 1517538
    :cond_0
    :goto_0
    iget-object v0, p0, LX/9d8;->a:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    invoke-static {v0}, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->g(Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;)V

    .line 1517539
    :cond_1
    :goto_1
    return-void

    .line 1517540
    :cond_2
    iget-object v0, p0, LX/9d8;->a:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    iget-object v0, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->w:LX/1cC;

    if-ne p0, v0, :cond_3

    iget-object v0, p0, LX/9d8;->a:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    iget-object v0, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->q:LX/5iG;

    if-nez v0, :cond_3

    .line 1517541
    iget-object v0, p0, LX/9d8;->a:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    iget-object v1, p0, LX/9d8;->a:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    iget-object v2, p0, LX/9d8;->a:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    iget-object v2, v2, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->t:LX/1aX;

    iget-object v3, p0, LX/9d8;->a:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    iget-object v3, v3, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->y:LX/9cy;

    invoke-virtual {v3}, LX/9cy;->b()Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v3

    iget-object v4, p0, LX/9d8;->a:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    iget-object v4, v4, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->q:LX/5iG;

    invoke-static {v1, v2, v3, v4}, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->a$redex0(Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;LX/1aX;Lcom/facebook/photos/creativeediting/model/SwipeableParams;LX/5iG;)LX/5iG;

    move-result-object v1

    .line 1517542
    iput-object v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->q:LX/5iG;

    .line 1517543
    goto :goto_0

    .line 1517544
    :cond_3
    iget-object v0, p0, LX/9d8;->a:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    iget-object v0, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->x:LX/1cC;

    if-ne p0, v0, :cond_0

    iget-object v0, p0, LX/9d8;->a:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    iget-object v0, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->r:LX/5iG;

    if-nez v0, :cond_0

    .line 1517545
    iget-object v0, p0, LX/9d8;->a:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    iget-object v1, p0, LX/9d8;->a:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    iget-object v2, p0, LX/9d8;->a:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    iget-object v2, v2, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->u:LX/1aX;

    iget-object v3, p0, LX/9d8;->a:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    iget-object v3, v3, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->y:LX/9cy;

    invoke-virtual {v3}, LX/9cy;->c()Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v3

    iget-object v4, p0, LX/9d8;->a:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    iget-object v4, v4, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->r:LX/5iG;

    invoke-static {v1, v2, v3, v4}, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->a$redex0(Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;LX/1aX;Lcom/facebook/photos/creativeediting/model/SwipeableParams;LX/5iG;)LX/5iG;

    move-result-object v1

    .line 1517546
    iput-object v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->r:LX/5iG;

    .line 1517547
    goto :goto_0

    .line 1517548
    :cond_4
    iget-object v0, p0, LX/9d8;->a:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    iget-boolean v0, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->k:Z

    if-eqz v0, :cond_6

    .line 1517549
    iget-object v0, p0, LX/9d8;->a:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    iget-object v0, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9cr;

    .line 1517550
    invoke-interface {v0}, LX/9cr;->a()V

    goto :goto_2

    .line 1517551
    :cond_5
    iget-object v0, p0, LX/9d8;->a:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    const/4 v1, 0x0

    .line 1517552
    iput-boolean v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->k:Z

    .line 1517553
    goto :goto_1

    .line 1517554
    :cond_6
    iget-object v0, p0, LX/9d8;->a:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    const/4 v1, 0x1

    .line 1517555
    iput-boolean v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->i:Z

    .line 1517556
    iget-object v0, p0, LX/9d8;->a:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    iget-object v0, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9cr;

    .line 1517557
    iget-object v2, p0, LX/9d8;->a:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    iget-object v3, p0, LX/9d8;->a:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    iget-object v3, v3, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->t:LX/1aX;

    iget-object v4, p0, LX/9d8;->a:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    iget-object v4, v4, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->y:LX/9cy;

    invoke-virtual {v4}, LX/9cy;->b()Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v4

    iget-object v5, p0, LX/9d8;->a:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    iget-object v5, v5, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->q:LX/5iG;

    invoke-static {v2, v3, v4, v5}, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->a$redex0(Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;LX/1aX;Lcom/facebook/photos/creativeediting/model/SwipeableParams;LX/5iG;)LX/5iG;

    move-result-object v2

    invoke-interface {v0, v2}, LX/9cr;->a(LX/5iG;)V

    goto :goto_3
.end method

.method public final b(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1517558
    invoke-super {p0, p1, p2}, LX/1cC;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1517559
    return-void
.end method
