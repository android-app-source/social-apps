.class public final LX/AQR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$TypeAheadSuggestionFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/AQS;


# direct methods
.method public constructor <init>(LX/AQS;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1671389
    iput-object p1, p0, LX/AQR;->b:LX/AQS;

    iput-object p2, p0, LX/AQR;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1671390
    iget-object v0, p0, LX/AQR;->b:LX/AQS;

    iget-object v0, v0, LX/AQS;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;

    iget-object v0, v0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->k:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    if-eqz v0, :cond_0

    .line 1671391
    iget-object v0, p0, LX/AQR;->b:LX/AQS;

    iget-object v0, v0, LX/AQS;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;

    iget-object v0, v0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->k:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget-object v1, p0, LX/AQR;->b:LX/AQS;

    iget-object v1, v1, LX/AQS;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;

    const v2, 0x7f08003c

    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    .line 1671392
    :cond_0
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1671393
    check-cast p1, LX/0Px;

    .line 1671394
    iget-object v0, p0, LX/AQR;->b:LX/AQS;

    iget-object v0, v0, LX/AQS;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;

    iget-object v0, v0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->k:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    if-eqz v0, :cond_0

    .line 1671395
    iget-object v0, p0, LX/AQR;->b:LX/AQS;

    iget-object v0, v0, LX/AQS;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;

    iget-object v0, v0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->k:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 1671396
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1671397
    :cond_1
    iget-object v0, p0, LX/AQR;->b:LX/AQS;

    iget-object v0, v0, LX/AQS;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;

    iget-object v0, v0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->n:LX/AQb;

    iget-object v1, p0, LX/AQR;->a:Ljava/lang/String;

    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/AQb;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1671398
    :goto_0
    iget-object v0, p0, LX/AQR;->b:LX/AQS;

    iget-object v0, v0, LX/AQS;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;

    iget-object v0, v0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->m:LX/AQZ;

    iget-object v1, p0, LX/AQR;->b:LX/AQS;

    iget-object v1, v1, LX/AQS;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;

    iget-object v1, v1, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->n:LX/AQb;

    iget-object v2, p0, LX/AQR;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/AQb;->b(Ljava/lang/String;)LX/0Px;

    move-result-object v1

    .line 1671399
    iput-object v1, v0, LX/AQZ;->b:LX/0Px;

    .line 1671400
    iget-object v0, p0, LX/AQR;->b:LX/AQS;

    iget-object v0, v0, LX/AQS;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;

    iget-object v0, v0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->m:LX/AQZ;

    const v1, 0x565c1f75

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1671401
    return-void

    .line 1671402
    :cond_2
    iget-object v0, p0, LX/AQR;->b:LX/AQS;

    iget-object v0, v0, LX/AQS;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;

    iget-object v0, v0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->n:LX/AQb;

    iget-object v1, p0, LX/AQR;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, LX/AQb;->a(Ljava/lang/String;Ljava/util/List;)V

    goto :goto_0
.end method
