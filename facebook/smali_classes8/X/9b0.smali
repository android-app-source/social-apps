.class public final enum LX/9b0;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9b0;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9b0;

.field public static final enum DEFAULT:LX/9b0;

.field public static final enum ENABLE_OPTIONS:LX/9b0;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1514195
    new-instance v0, LX/9b0;

    const-string v1, "ENABLE_OPTIONS"

    invoke-direct {v0, v1, v2}, LX/9b0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9b0;->ENABLE_OPTIONS:LX/9b0;

    .line 1514196
    new-instance v0, LX/9b0;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v3}, LX/9b0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9b0;->DEFAULT:LX/9b0;

    .line 1514197
    const/4 v0, 0x2

    new-array v0, v0, [LX/9b0;

    sget-object v1, LX/9b0;->ENABLE_OPTIONS:LX/9b0;

    aput-object v1, v0, v2

    sget-object v1, LX/9b0;->DEFAULT:LX/9b0;

    aput-object v1, v0, v3

    sput-object v0, LX/9b0;->$VALUES:[LX/9b0;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1514192
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9b0;
    .locals 1

    .prologue
    .line 1514194
    const-class v0, LX/9b0;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9b0;

    return-object v0
.end method

.method public static values()[LX/9b0;
    .locals 1

    .prologue
    .line 1514193
    sget-object v0, LX/9b0;->$VALUES:[LX/9b0;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9b0;

    return-object v0
.end method
