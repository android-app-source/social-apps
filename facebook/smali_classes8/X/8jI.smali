.class public LX/8jI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile h:LX/8jI;


# instance fields
.field public final b:LX/0aG;

.field public final c:Ljava/util/concurrent/ScheduledExecutorService;

.field public final d:Ljava/lang/Object;

.field public final e:LX/0Xv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Xv",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Lcom/facebook/stickers/model/Sticker;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mFutureLock"
    .end annotation
.end field

.field public f:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mFutureLock"
    .end annotation
.end field

.field private final g:Ljava/lang/Runnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1392282
    const-class v0, LX/8jI;

    sput-object v0, LX/8jI;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0aG;Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 1
    .param p2    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1392283
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1392284
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/8jI;->d:Ljava/lang/Object;

    .line 1392285
    new-instance v0, Lcom/facebook/stickers/client/FetchStickerCoordinator$1;

    invoke-direct {v0, p0}, Lcom/facebook/stickers/client/FetchStickerCoordinator$1;-><init>(LX/8jI;)V

    iput-object v0, p0, LX/8jI;->g:Ljava/lang/Runnable;

    .line 1392286
    iput-object p1, p0, LX/8jI;->b:LX/0aG;

    .line 1392287
    iput-object p2, p0, LX/8jI;->c:Ljava/util/concurrent/ScheduledExecutorService;

    .line 1392288
    invoke-static {}, LX/0Xq;->t()LX/0Xq;

    move-result-object v0

    iput-object v0, p0, LX/8jI;->e:LX/0Xv;

    .line 1392289
    return-void
.end method

.method public static a(LX/0QB;)LX/8jI;
    .locals 5

    .prologue
    .line 1392290
    sget-object v0, LX/8jI;->h:LX/8jI;

    if-nez v0, :cond_1

    .line 1392291
    const-class v1, LX/8jI;

    monitor-enter v1

    .line 1392292
    :try_start_0
    sget-object v0, LX/8jI;->h:LX/8jI;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1392293
    if-eqz v2, :cond_0

    .line 1392294
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1392295
    new-instance p0, LX/8jI;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v3

    check-cast v3, LX/0aG;

    invoke-static {v0}, LX/0Xi;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-direct {p0, v3, v4}, LX/8jI;-><init>(LX/0aG;Ljava/util/concurrent/ScheduledExecutorService;)V

    .line 1392296
    move-object v0, p0

    .line 1392297
    sput-object v0, LX/8jI;->h:LX/8jI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1392298
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1392299
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1392300
    :cond_1
    sget-object v0, LX/8jI;->h:LX/8jI;

    return-object v0

    .line 1392301
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1392302
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Ljava/lang/String;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Lcom/facebook/stickers/model/Sticker;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1392303
    iget-object v1, p0, LX/8jI;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 1392304
    :try_start_0
    iget-object v0, p0, LX/8jI;->e:LX/0Xv;

    invoke-interface {v0, p1, p2}, LX/0Xu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1392305
    iget-boolean v0, p0, LX/8jI;->f:Z

    if-eqz v0, :cond_0

    .line 1392306
    monitor-exit v1

    .line 1392307
    :goto_0
    return-void

    .line 1392308
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8jI;->f:Z

    .line 1392309
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1392310
    iget-object v0, p0, LX/8jI;->c:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v1, p0, LX/8jI;->g:Ljava/lang/Runnable;

    const-wide/16 v2, 0x32

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    goto :goto_0

    .line 1392311
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/stickers/model/Sticker;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1392312
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    .line 1392313
    invoke-direct {p0, p1, v0}, LX/8jI;->a(Ljava/lang/String;Lcom/google/common/util/concurrent/SettableFuture;)V

    .line 1392314
    return-object v0
.end method
