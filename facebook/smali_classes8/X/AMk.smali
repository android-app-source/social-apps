.class public final LX/AMk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1uy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1uy",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;

.field public b:LX/AMT;

.field public c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/AML;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader$OnAssetDownloadProgressListener;",
            ">;"
        }
    .end annotation
.end field

.field public e:J


# direct methods
.method public constructor <init>(Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;LX/AMT;LX/AML;LX/AMK;)V
    .locals 1
    .param p4    # LX/AMK;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1666406
    iput-object p1, p0, LX/AMk;->a:Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1666407
    iput-object p2, p0, LX/AMk;->b:LX/AMT;

    .line 1666408
    new-instance v0, LX/0UE;

    invoke-direct {v0}, LX/0UE;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/AMk;->c:Ljava/util/Set;

    .line 1666409
    iget-object v0, p0, LX/AMk;->c:Ljava/util/Set;

    invoke-interface {v0, p3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1666410
    new-instance v0, LX/0UE;

    invoke-direct {v0}, LX/0UE;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/AMk;->d:Ljava/util/Set;

    .line 1666411
    if-eqz p4, :cond_0

    .line 1666412
    iget-object v0, p0, LX/AMk;->d:Ljava/util/Set;

    invoke-interface {v0, p4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1666413
    :cond_0
    return-void
.end method

.method public static a$redex0(LX/AMk;Ljava/io/File;)V
    .locals 4

    .prologue
    .line 1666414
    iget-object v0, p0, LX/AMk;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AML;

    .line 1666415
    iget-object v2, p0, LX/AMk;->a:Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;

    iget-object v2, v2, Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;->f:Landroid/os/Handler;

    new-instance v3, Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader$MsqrdAssetDownloadResultResponseHandler$3;

    invoke-direct {v3, p0, v0, p1}, Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader$MsqrdAssetDownloadResultResponseHandler$3;-><init>(LX/AMk;LX/AML;Ljava/io/File;)V

    const v0, 0x2391642d

    invoke-static {v2, v3, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0

    .line 1666416
    :cond_0
    return-void
.end method

.method public static a$redex0(LX/AMk;Ljava/lang/Throwable;)V
    .locals 12

    .prologue
    .line 1666417
    iget-object v0, p0, LX/AMk;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AMK;

    .line 1666418
    iget-object v2, p0, LX/AMk;->b:LX/AMT;

    iget-wide v4, p0, LX/AMk;->e:J

    .line 1666419
    iget-object v6, v0, LX/AMK;->a:Ljava/util/Map;

    .line 1666420
    iget-object v7, v2, LX/AMT;->d:Ljava/lang/String;

    move-object v7, v7

    .line 1666421
    invoke-interface {v6, v7}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    neg-long v6, v6

    .line 1666422
    iget-object v8, v0, LX/AMK;->b:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v8, v6, v7}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    move-result-wide v6

    long-to-double v6, v6

    .line 1666423
    iget-object v8, v0, LX/AMK;->c:Ljava/util/concurrent/atomic/AtomicLong;

    neg-long v10, v4

    invoke-virtual {v8, v10, v11}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    move-result-wide v8

    .line 1666424
    const-wide/16 v10, 0x0

    cmp-long v10, v8, v10

    if-eqz v10, :cond_0

    .line 1666425
    iget-object v10, v0, LX/AMK;->d:LX/AZo;

    long-to-double v8, v8

    div-double/2addr v6, v8

    invoke-virtual {v10, v6, v7}, LX/AZo;->a(D)V

    .line 1666426
    :cond_0
    goto :goto_0

    .line 1666427
    :cond_1
    iget-object v0, p0, LX/AMk;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AML;

    .line 1666428
    iget-object v2, p0, LX/AMk;->a:Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;

    iget-object v2, v2, Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;->f:Landroid/os/Handler;

    new-instance v3, Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader$MsqrdAssetDownloadResultResponseHandler$2;

    invoke-direct {v3, p0, v0, p1}, Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader$MsqrdAssetDownloadResultResponseHandler$2;-><init>(LX/AMk;LX/AML;Ljava/lang/Throwable;)V

    const v0, -0x71908788

    invoke-static {v2, v3, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_1

    .line 1666429
    :cond_2
    return-void
.end method


# virtual methods
.method public final a(Ljava/io/InputStream;JLX/2ur;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1666430
    iput-wide p2, p0, LX/AMk;->e:J

    .line 1666431
    iget-object v0, p0, LX/AMk;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AMK;

    .line 1666432
    iget-object v2, p0, LX/AMk;->b:LX/AMT;

    iget-wide v4, p0, LX/AMk;->e:J

    invoke-virtual {v0, v2, v4, v5}, LX/AMK;->a(LX/AMT;J)V

    goto :goto_0

    .line 1666433
    :cond_0
    iget-object v0, p0, LX/AMk;->a:Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;

    iget-object v0, v0, Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;->d:LX/AMf;

    iget-object v1, p0, LX/AMk;->b:LX/AMT;

    .line 1666434
    iget-object v2, v1, LX/AMT;->c:Ljava/lang/String;

    move-object v1, v2

    .line 1666435
    new-instance v2, LX/AMj;

    invoke-direct {v2, p0}, LX/AMj;-><init>(LX/AMk;)V

    .line 1666436
    :try_start_0
    iget-object v3, v0, LX/AMf;->a:Lcom/facebook/compactdisk/DiskCache;

    new-instance v4, LX/AMa;

    invoke-direct {v4, v0, p1, v2}, LX/AMa;-><init>(LX/AMf;Ljava/io/InputStream;LX/AMe;)V

    invoke-virtual {v3, v1, v4}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->storeToPath(Ljava/lang/String;Lcom/facebook/compactdisk/WriteCallback;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1666437
    iget-object v3, v0, LX/AMf;->a:Lcom/facebook/compactdisk/DiskCache;

    invoke-virtual {v3, v1}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->fetchPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1666438
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x0

    :goto_1
    invoke-interface {v2, v3}, LX/AMe;->a(Ljava/io/File;)V

    .line 1666439
    :goto_2
    const/4 v0, 0x0

    return-object v0

    .line 1666440
    :catch_0
    move-exception v3

    .line 1666441
    invoke-interface {v2, v3}, LX/AMe;->a(Ljava/lang/Throwable;)V

    goto :goto_2

    .line 1666442
    :cond_1
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method
