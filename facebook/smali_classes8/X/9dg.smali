.class public final LX/9dg;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source ""


# instance fields
.field public final synthetic a:LX/9dl;


# direct methods
.method public constructor <init>(LX/9dl;)V
    .locals 0

    .prologue
    .line 1518453
    iput-object p1, p0, LX/9dg;->a:LX/9dl;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1518434
    const/4 v2, 0x2

    new-array v2, v2, [I

    .line 1518435
    iget-object v3, p0, LX/9dg;->a:LX/9dl;

    invoke-static {v3, v2, p1}, LX/9dl;->a$redex0(LX/9dl;[ILandroid/view/MotionEvent;)V

    .line 1518436
    iget-object v3, p0, LX/9dg;->a:LX/9dl;

    aget v4, v2, v0

    aget v2, v2, v1

    invoke-static {v3, v4, v2, v0}, LX/9dl;->a$redex0(LX/9dl;IIZ)Z

    .line 1518437
    iget-object v2, p0, LX/9dg;->a:LX/9dl;

    invoke-virtual {v2}, LX/9dl;->getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v2

    .line 1518438
    iget-object v3, v2, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    move-object v2, v3

    .line 1518439
    if-nez v2, :cond_0

    .line 1518440
    :goto_0
    return v0

    .line 1518441
    :cond_0
    iget-object v0, p0, LX/9dg;->a:LX/9dl;

    invoke-virtual {v0}, LX/9dl;->getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1518442
    iget-object v2, v0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    invoke-interface {v2}, LX/5i8;->i()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1518443
    :goto_1
    move v0, v3

    .line 1518444
    if-eqz v0, :cond_1

    .line 1518445
    iget-object v0, p0, LX/9dg;->a:LX/9dl;

    invoke-virtual {v0}, LX/9dl;->invalidate()V

    :cond_1
    move v0, v1

    .line 1518446
    goto :goto_0

    .line 1518447
    :cond_2
    iget-object v2, v0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    invoke-interface {v2}, LX/5i8;->h()Z

    move-result v5

    .line 1518448
    iget-object v2, v0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b:Ljava/util/LinkedHashMap;

    iget-object v6, v0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    invoke-virtual {v2, v6}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1aX;

    .line 1518449
    iget-object v6, v0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b:Ljava/util/LinkedHashMap;

    iget-object p1, v0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    invoke-virtual {v6, p1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1518450
    iget-object v6, v0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    invoke-static {v6}, LX/5jA;->a(LX/362;)LX/5iE;

    move-result-object v6

    if-nez v5, :cond_3

    move v3, v4

    :cond_3
    invoke-interface {v6, v3}, LX/5iE;->a(Z)LX/5iE;

    move-result-object v3

    invoke-interface {v3}, LX/5iE;->b()LX/5i8;

    move-result-object v3

    iput-object v3, v0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    .line 1518451
    iget-object v3, v0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b:Ljava/util/LinkedHashMap;

    iget-object v5, v0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    invoke-virtual {v3, v5, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v3, v4

    .line 1518452
    goto :goto_1
.end method

.method public final onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1518392
    const/4 v0, 0x0

    return v0
.end method

.method public final onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1518400
    iget-object v2, p0, LX/9dg;->a:LX/9dl;

    iget-boolean v2, v2, LX/9dl;->h:Z

    if-nez v2, :cond_0

    .line 1518401
    const/4 v2, 0x2

    new-array v2, v2, [I

    .line 1518402
    iget-object v3, p0, LX/9dg;->a:LX/9dl;

    invoke-static {v3, v2, p1}, LX/9dl;->a$redex0(LX/9dl;[ILandroid/view/MotionEvent;)V

    .line 1518403
    iget-object v3, p0, LX/9dg;->a:LX/9dl;

    aget v4, v2, v0

    aget v2, v2, v1

    invoke-static {v3, v4, v2, v0}, LX/9dl;->a$redex0(LX/9dl;IIZ)Z

    .line 1518404
    :cond_0
    iget-object v2, p0, LX/9dg;->a:LX/9dl;

    invoke-virtual {v2}, LX/9dl;->getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v2

    .line 1518405
    iget-object v3, v2, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    move-object v2, v3

    .line 1518406
    if-nez v2, :cond_1

    .line 1518407
    :goto_0
    return v0

    .line 1518408
    :cond_1
    iget-object v0, p0, LX/9dg;->a:LX/9dl;

    .line 1518409
    iput-boolean v1, v0, LX/9dl;->h:Z

    .line 1518410
    iget-object v0, p0, LX/9dg;->a:LX/9dl;

    invoke-virtual {v0}, LX/9dl;->getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v0

    .line 1518411
    iget-object v2, v0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    move-object v0, v2

    .line 1518412
    iget-object v2, p0, LX/9dg;->a:LX/9dl;

    invoke-virtual {v2}, LX/9dl;->getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->d(LX/362;)I

    move-result v2

    int-to-float v2, v2

    .line 1518413
    iget-object v3, p0, LX/9dg;->a:LX/9dl;

    invoke-virtual {v3}, LX/9dl;->getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->e(LX/362;)I

    move-result v0

    int-to-float v0, v0

    .line 1518414
    iget-object v3, p0, LX/9dg;->a:LX/9dl;

    invoke-virtual {v3}, LX/9dl;->getWidth()I

    move-result v3

    int-to-float v3, v3

    .line 1518415
    iget-object v4, p0, LX/9dg;->a:LX/9dl;

    invoke-virtual {v4}, LX/9dl;->getHeight()I

    move-result v4

    int-to-float v4, v4

    .line 1518416
    sub-float/2addr v2, p3

    .line 1518417
    sub-float/2addr v0, p4

    .line 1518418
    neg-float v5, v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-static {v5, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 1518419
    neg-float v3, v4

    invoke-static {v0, v4}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 1518420
    iget-object v3, p0, LX/9dg;->a:LX/9dl;

    invoke-virtual {v3}, LX/9dl;->getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v3

    float-to-int v2, v2

    invoke-virtual {v3, v2}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->a(I)V

    .line 1518421
    iget-object v2, p0, LX/9dg;->a:LX/9dl;

    invoke-virtual {v2}, LX/9dl;->getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v2

    float-to-int v0, v0

    invoke-virtual {v2, v0}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b(I)V

    .line 1518422
    iget-object v0, p0, LX/9dg;->a:LX/9dl;

    invoke-virtual {v0}, LX/9dl;->getAnimationController()LX/9dr;

    move-result-object v0

    .line 1518423
    iget-boolean v2, v0, LX/9dr;->c:Z

    move v0, v2

    .line 1518424
    if-nez v0, :cond_2

    .line 1518425
    iget-object v0, p0, LX/9dg;->a:LX/9dl;

    invoke-virtual {v0}, LX/9dl;->getAnimationController()LX/9dr;

    move-result-object v0

    .line 1518426
    iget-object v6, v0, LX/9dr;->f:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    invoke-virtual {v6}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->a()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1518427
    :cond_2
    :goto_1
    iget-object v0, p0, LX/9dg;->a:LX/9dl;

    invoke-static {v0}, LX/9dl;->p(LX/9dl;)V

    .line 1518428
    iget-object v0, p0, LX/9dg;->a:LX/9dl;

    invoke-virtual {v0}, LX/9dl;->invalidate()V

    move v0, v1

    .line 1518429
    goto :goto_0

    .line 1518430
    :cond_3
    iget-object v6, v0, LX/9dr;->h:LX/0wd;

    sget-object v7, LX/9dr;->b:LX/0wT;

    invoke-virtual {v6, v7}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v6

    const/4 v7, 0x0

    .line 1518431
    iput-boolean v7, v6, LX/0wd;->c:Z

    .line 1518432
    move-object v6, v6

    .line 1518433
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v6, v8, v9}, LX/0wd;->b(D)LX/0wd;

    goto :goto_1
.end method

.method public final onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1518393
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 1518394
    iget-object v1, p0, LX/9dg;->a:LX/9dl;

    invoke-static {v1, v0, p1}, LX/9dl;->a$redex0(LX/9dl;[ILandroid/view/MotionEvent;)V

    .line 1518395
    iget-object v1, p0, LX/9dg;->a:LX/9dl;

    const/4 v2, 0x0

    aget v2, v0, v2

    aget v0, v0, v3

    invoke-static {v1, v2, v0, v3}, LX/9dl;->a$redex0(LX/9dl;IIZ)Z

    move-result v0

    .line 1518396
    if-eqz v0, :cond_0

    .line 1518397
    iget-object v1, p0, LX/9dg;->a:LX/9dl;

    invoke-virtual {v1}, LX/9dl;->invalidate()V

    .line 1518398
    :cond_0
    iget-object v1, p0, LX/9dg;->a:LX/9dl;

    invoke-virtual {v1, v0}, LX/9dl;->a(Z)V

    .line 1518399
    return v0
.end method
