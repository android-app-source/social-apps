.class public final LX/8zg;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/8zh;


# direct methods
.method public constructor <init>(LX/8zh;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1427834
    iput-object p1, p0, LX/8zg;->b:LX/8zh;

    iput-object p2, p0, LX/8zg;->a:Ljava/lang/String;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1427835
    new-instance v0, LX/8zf;

    invoke-direct {v0, p0}, LX/8zf;-><init>(LX/8zg;)V

    .line 1427836
    invoke-static {p1}, LX/3d6;->forException(Ljava/lang/Throwable;)LX/1nY;

    move-result-object v1

    sget-object v2, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    if-ne v1, v2, :cond_0

    .line 1427837
    iget-object v1, p0, LX/8zg;->b:LX/8zh;

    iget-object v1, v1, LX/8zh;->e:LX/909;

    invoke-interface {v1, v0}, LX/909;->a(LX/8zc;)V

    .line 1427838
    :goto_0
    iget-object v0, p0, LX/8zg;->b:LX/8zh;

    iget-object v0, v0, LX/8zh;->f:LX/918;

    iget-object v1, p0, LX/8zg;->b:LX/8zh;

    iget-object v1, v1, LX/8zh;->c:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1427839
    iget-object v2, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v1, v2

    .line 1427840
    const-string v2, "activities_selector_taggable_suggestions_load_failed"

    invoke-static {v2, v1}, LX/918;->d(Ljava/lang/String;Ljava/lang/String;)LX/8yQ;

    move-result-object v2

    .line 1427841
    iget-object v3, v2, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v2, v3

    .line 1427842
    const-string v3, "error"

    invoke-virtual {v2, v3, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1427843
    iget-object v3, v0, LX/918;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1427844
    iget-object v0, p0, LX/8zg;->b:LX/8zh;

    iget-object v0, v0, LX/8zh;->h:LX/92D;

    invoke-virtual {v0}, LX/92D;->f()V

    .line 1427845
    return-void

    .line 1427846
    :cond_0
    iget-object v1, p0, LX/8zg;->b:LX/8zh;

    iget-object v1, v1, LX/8zh;->e:LX/909;

    invoke-interface {v1, v0}, LX/909;->b(LX/8zc;)V

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 1427847
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1427848
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1427849
    check-cast v0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;

    .line 1427850
    iget-object v1, p0, LX/8zg;->b:LX/8zh;

    .line 1427851
    iput-object v0, v1, LX/8zh;->i:LX/5LG;

    .line 1427852
    iget-object v1, p0, LX/8zg;->b:LX/8zh;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->K()Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->K()Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v0

    iget-object v4, p0, LX/8zg;->a:Ljava/lang/String;

    .line 1427853
    const/4 v6, 0x0

    .line 1427854
    iput-object v0, v1, LX/8zh;->j:LX/0ut;

    .line 1427855
    iget-object v5, v1, LX/8zh;->a:LX/8zZ;

    iget-object v7, v1, LX/8zh;->i:LX/5LG;

    .line 1427856
    iput-object v7, v5, LX/8zZ;->f:LX/5LG;

    .line 1427857
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v8

    move v7, v6

    :goto_0
    if-ge v7, v8, :cond_4

    invoke-virtual {v2, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    .line 1427858
    invoke-virtual {v5}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->e()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v5}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->e()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->v_()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v5}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->e()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->v_()Ljava/lang/String;

    move-result-object v5

    iget-object v3, v1, LX/8zh;->l:Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1427859
    const/4 v5, 0x1

    .line 1427860
    :goto_1
    iget-object v7, v1, LX/8zh;->l:Ljava/lang/String;

    invoke-static {v7}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    if-nez v5, :cond_3

    iget-object v5, v1, LX/8zh;->i:LX/5LG;

    invoke-interface {v5}, LX/5LG;->q()Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, v1, LX/8zh;->i:LX/5LG;

    invoke-interface {v5}, LX/5LG;->z()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;

    move-result-object v5

    if-eqz v5, :cond_3

    iget-object v5, v1, LX/8zh;->i:LX/5LG;

    invoke-interface {v5}, LX/5LG;->A()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 1427861
    new-instance v5, LX/5LL;

    invoke-direct {v5}, LX/5LL;-><init>()V

    iget-object v7, v1, LX/8zh;->l:Ljava/lang/String;

    .line 1427862
    iput-object v7, v5, LX/5LL;->d:Ljava/lang/String;

    .line 1427863
    move-object v5, v5

    .line 1427864
    iput-boolean v6, v5, LX/5LL;->h:Z

    .line 1427865
    move-object v5, v5

    .line 1427866
    new-instance v7, LX/4aM;

    invoke-direct {v7}, LX/4aM;-><init>()V

    iget-object v8, v1, LX/8zh;->i:LX/5LG;

    invoke-interface {v8}, LX/5LG;->z()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;->a()Ljava/lang/String;

    move-result-object v8

    .line 1427867
    iput-object v8, v7, LX/4aM;->b:Ljava/lang/String;

    .line 1427868
    move-object v7, v7

    .line 1427869
    invoke-virtual {v7}, LX/4aM;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v7

    .line 1427870
    iput-object v7, v5, LX/5LL;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1427871
    move-object v5, v5

    .line 1427872
    new-instance v7, LX/5Lc;

    invoke-direct {v7}, LX/5Lc;-><init>()V

    iget-object v8, v1, LX/8zh;->l:Ljava/lang/String;

    .line 1427873
    iput-object v8, v7, LX/5Lc;->d:Ljava/lang/String;

    .line 1427874
    move-object v7, v7

    .line 1427875
    iput-boolean v6, v7, LX/5Lc;->c:Z

    .line 1427876
    move-object v7, v7

    .line 1427877
    new-instance v8, LX/4aM;

    invoke-direct {v8}, LX/4aM;-><init>()V

    iget-object v3, v1, LX/8zh;->i:LX/5LG;

    invoke-interface {v3}, LX/5LG;->A()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 1427878
    iput-object v3, v8, LX/4aM;->b:Ljava/lang/String;

    .line 1427879
    move-object v8, v8

    .line 1427880
    invoke-virtual {v8}, LX/4aM;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v8

    .line 1427881
    iput-object v8, v7, LX/5Lc;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1427882
    move-object v7, v7

    .line 1427883
    invoke-virtual {v7}, LX/5Lc;->a()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v7

    .line 1427884
    iput-object v7, v5, LX/5LL;->g:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    .line 1427885
    move-object v5, v5

    .line 1427886
    invoke-virtual {v5}, LX/5LL;->a()Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    move-result-object v5

    .line 1427887
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    invoke-virtual {v7, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v5

    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    .line 1427888
    iget-object v7, v1, LX/8zh;->a:LX/8zZ;

    .line 1427889
    iput-object v5, v7, LX/8zZ;->e:LX/0Px;

    .line 1427890
    iput v6, v7, LX/8zZ;->g:I

    .line 1427891
    invoke-virtual {v7}, LX/3mY;->bE_()V

    .line 1427892
    :goto_2
    iput-boolean v6, v1, LX/8zh;->r:Z

    .line 1427893
    iget-boolean v5, v1, LX/8zh;->q:Z

    if-eqz v5, :cond_0

    .line 1427894
    invoke-static {v1}, LX/8zh;->f(LX/8zh;)V

    .line 1427895
    :cond_0
    iput-object v4, v1, LX/8zh;->n:Ljava/lang/String;

    .line 1427896
    iget-object v0, p0, LX/8zg;->b:LX/8zh;

    iget-object v0, v0, LX/8zh;->l:Ljava/lang/String;

    iget-object v1, p0, LX/8zg;->b:LX/8zh;

    iget-object v1, v1, LX/8zh;->k:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0YN;->c(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1427897
    iget-object v0, p0, LX/8zg;->b:LX/8zh;

    iget-object v1, p0, LX/8zg;->b:LX/8zh;

    iget-object v1, v1, LX/8zh;->l:Ljava/lang/String;

    invoke-static {v0, v1}, LX/8zh;->c(LX/8zh;Ljava/lang/String;)V

    .line 1427898
    :cond_1
    iget-object v0, p0, LX/8zg;->b:LX/8zh;

    iget-object v0, v0, LX/8zh;->h:LX/92D;

    iget-object v1, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    invoke-virtual {v0, v1}, LX/92D;->a(LX/0ta;)V

    .line 1427899
    return-void

    .line 1427900
    :cond_2
    add-int/lit8 v5, v7, 0x1

    move v7, v5

    goto/16 :goto_0

    .line 1427901
    :cond_3
    iget-object v5, v1, LX/8zh;->a:LX/8zZ;

    .line 1427902
    iput-object v2, v5, LX/8zZ;->e:LX/0Px;

    .line 1427903
    const/4 v7, -0x1

    iput v7, v5, LX/8zZ;->g:I

    .line 1427904
    invoke-virtual {v5}, LX/3mY;->bE_()V

    .line 1427905
    goto :goto_2

    :cond_4
    move v5, v6

    goto/16 :goto_1
.end method
