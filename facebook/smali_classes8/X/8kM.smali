.class public final LX/8kM;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 1396049
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 1396050
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1396051
    :goto_0
    return v1

    .line 1396052
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1396053
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_6

    .line 1396054
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1396055
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1396056
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_1

    if-eqz v6, :cond_1

    .line 1396057
    const-string v7, "animated_image"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1396058
    invoke-static {p0, p1}, LX/8kN;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1396059
    :cond_2
    const-string v7, "id"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1396060
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 1396061
    :cond_3
    const-string v7, "pack"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1396062
    invoke-static {p0, p1}, LX/8kK;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1396063
    :cond_4
    const-string v7, "preview_image"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1396064
    invoke-static {p0, p1}, LX/8kN;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1396065
    :cond_5
    const-string v7, "thread_image"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1396066
    invoke-static {p0, p1}, LX/8kN;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1396067
    :cond_6
    const/4 v6, 0x5

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1396068
    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1396069
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1396070
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1396071
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1396072
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1396073
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1396074
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1396075
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1396076
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/8kM;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1396077
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1396078
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1396079
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 1396080
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1396081
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1396082
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1396083
    invoke-static {p0, p1}, LX/8kM;->a(LX/15w;LX/186;)I

    move-result v1

    .line 1396084
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1396085
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1396086
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1396087
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1396088
    if-eqz v0, :cond_0

    .line 1396089
    const-string v1, "animated_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1396090
    invoke-static {p0, v0, p2}, LX/8kN;->a(LX/15i;ILX/0nX;)V

    .line 1396091
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1396092
    if-eqz v0, :cond_1

    .line 1396093
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1396094
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1396095
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1396096
    if-eqz v0, :cond_2

    .line 1396097
    const-string v1, "pack"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1396098
    invoke-static {p0, v0, p2}, LX/8kK;->a(LX/15i;ILX/0nX;)V

    .line 1396099
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1396100
    if-eqz v0, :cond_3

    .line 1396101
    const-string v1, "preview_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1396102
    invoke-static {p0, v0, p2}, LX/8kN;->a(LX/15i;ILX/0nX;)V

    .line 1396103
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1396104
    if-eqz v0, :cond_4

    .line 1396105
    const-string v1, "thread_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1396106
    invoke-static {p0, v0, p2}, LX/8kN;->a(LX/15i;ILX/0nX;)V

    .line 1396107
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1396108
    return-void
.end method
