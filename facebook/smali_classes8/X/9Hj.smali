.class public LX/9Hj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Lcom/facebook/feedback/ui/environment/HasCommentActions;",
        ":",
        "LX/1Pn;",
        ":",
        "Lcom/facebook/feedback/ui/environment/HasLoggingParams;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field public a:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/1Uf;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0Uh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/8qs;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/9FH;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/3Cn;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/7mw;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private h:I


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1461820
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1461821
    const/4 v0, -0x1

    iput v0, p0, LX/9Hj;->h:I

    .line 1461822
    return-void
.end method

.method private a()I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1461793
    iget v0, p0, LX/9Hj;->h:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1461794
    iget-object v0, p0, LX/9Hj;->a:Landroid/content/Context;

    const v1, 0x7f0105a7

    const v2, 0x7f0e0601

    invoke-static {v0, v1, v2}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v0

    .line 1461795
    const/4 v1, 0x1

    new-array v1, v1, [I

    const v2, 0x1010098

    aput v2, v1, v3

    .line 1461796
    iget-object v2, p0, LX/9Hj;->a:Landroid/content/Context;

    invoke-virtual {v2, v0, v1}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1461797
    const v1, 0x7f0a0038

    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, LX/9Hj;->h:I

    .line 1461798
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1461799
    :cond_0
    iget v0, p0, LX/9Hj;->h:I

    return v0
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;LX/9FA;)LX/9Hi;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;",
            "Lcom/facebook/feedback/ui/environment/HasCommentActions;",
            ")",
            "Lcom/facebook/ufiservices/util/CommentMenuHelper$OnCommentClickListener;"
        }
    .end annotation

    .prologue
    .line 1461818
    invoke-static {p1}, LX/5Op;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 1461819
    new-instance v1, LX/9Hi;

    invoke-direct {v1, p2, v0, p0}, LX/9Hi;-><init>(LX/9FA;Lcom/facebook/graphql/model/GraphQLFeedback;Landroid/content/Context;)V

    return-object v1
.end method

.method public static a(LX/0QB;)LX/9Hj;
    .locals 13

    .prologue
    .line 1461800
    const-class v1, LX/9Hj;

    monitor-enter v1

    .line 1461801
    :try_start_0
    sget-object v0, LX/9Hj;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1461802
    sput-object v2, LX/9Hj;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1461803
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1461804
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1461805
    new-instance v3, LX/9Hj;

    invoke-direct {v3}, LX/9Hj;-><init>()V

    .line 1461806
    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v5

    check-cast v5, LX/1Uf;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, LX/0Uh;

    invoke-static {v0}, LX/8qs;->b(LX/0QB;)LX/8qs;

    move-result-object v7

    check-cast v7, LX/8qs;

    const-class v8, LX/9FH;

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/9FH;

    invoke-static {v0}, LX/3Cn;->b(LX/0QB;)LX/3Cn;

    move-result-object v9

    check-cast v9, LX/3Cn;

    .line 1461807
    new-instance p0, LX/7mw;

    invoke-direct {p0}, LX/7mw;-><init>()V

    .line 1461808
    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v10

    check-cast v10, LX/0Zb;

    invoke-static {v0}, Lcom/facebook/delights/floating/DelightsFireworks;->a(LX/0QB;)Lcom/facebook/delights/floating/DelightsFireworks;

    move-result-object v11

    check-cast v11, Lcom/facebook/delights/floating/DelightsFireworks;

    invoke-static {v0}, LX/1Ar;->b(LX/0QB;)LX/1Ar;

    move-result-object v12

    check-cast v12, LX/1Ar;

    .line 1461809
    iput-object v10, p0, LX/7mw;->a:LX/0Zb;

    iput-object v11, p0, LX/7mw;->b:Lcom/facebook/delights/floating/DelightsFireworks;

    iput-object v12, p0, LX/7mw;->c:LX/1Ar;

    .line 1461810
    move-object v10, p0

    .line 1461811
    check-cast v10, LX/7mw;

    .line 1461812
    iput-object v4, v3, LX/9Hj;->a:Landroid/content/Context;

    iput-object v5, v3, LX/9Hj;->b:LX/1Uf;

    iput-object v6, v3, LX/9Hj;->c:LX/0Uh;

    iput-object v7, v3, LX/9Hj;->d:LX/8qs;

    iput-object v8, v3, LX/9Hj;->e:LX/9FH;

    iput-object v9, v3, LX/9Hj;->f:LX/3Cn;

    iput-object v10, v3, LX/9Hj;->g:LX/7mw;

    .line 1461813
    move-object v0, v3

    .line 1461814
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1461815
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/9Hj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1461816
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1461817
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1461823
    const/4 v1, 0x0

    .line 1461824
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 1461825
    move-object v3, v0

    move-object v0, v1

    move-object v1, v3

    .line 1461826
    :goto_0
    if-eqz v1, :cond_1

    .line 1461827
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 1461828
    instance-of v2, v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v2, :cond_0

    .line 1461829
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1461830
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1461831
    :cond_0
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v2

    .line 1461832
    goto :goto_0

    .line 1461833
    :cond_1
    move-object v0, v0

    .line 1461834
    if-eqz v0, :cond_2

    invoke-static {v0}, LX/21y;->isRanked(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLComment;)Ljava/lang/CharSequence;
    .locals 5

    .prologue
    .line 1461753
    iget-object v0, p0, LX/9Hj;->b:LX/1Uf;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    invoke-static {v1}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLActor;)LX/1y5;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0}, LX/9Hj;->a()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, LX/1Uf;->a(LX/1y5;LX/0lF;IZ)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-static {v0}, LX/1Uf;->a(Landroid/text/Spannable;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)Ljava/lang/CharSequence;
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 1461754
    if-nez p2, :cond_0

    const/4 v3, 0x0

    .line 1461755
    :goto_0
    const v0, -0x36bd2417

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p0, LX/9Hj;->g:LX/7mw;

    invoke-static {v0, v1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v5

    .line 1461756
    iget-object v0, p0, LX/9Hj;->b:LX/1Uf;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-static {v1}, LX/1eD;->c(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/1eE;

    move-result-object v1

    invoke-direct {p0}, LX/9Hj;->a()I

    move-result v4

    invoke-virtual/range {v0 .. v5}, LX/1Uf;->a(LX/1eE;ZLX/0lF;ILX/0P1;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 1461757
    iget-object v1, p0, LX/9Hj;->f:LX/3Cn;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-virtual {v1, v3, v0}, LX/3Cn;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;Landroid/text/SpannableStringBuilder;)V

    .line 1461758
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1461759
    if-eqz v1, :cond_1

    iget-object v3, p0, LX/9Hj;->c:LX/0Uh;

    const/16 v4, 0xf6

    invoke-virtual {v3, v4, v6}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1461760
    :goto_1
    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/8u7;->a(Landroid/text/Spanned;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    :goto_2
    return-object v0

    .line 1461761
    :cond_0
    iget-object v0, p2, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->a:LX/162;

    move-object v3, v0

    .line 1461762
    goto :goto_0

    :cond_1
    move v2, v6

    .line 1461763
    goto :goto_1

    .line 1461764
    :cond_2
    invoke-static {v0}, LX/1Uf;->a(Landroid/text/Spannable;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_2
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/9FA;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;TE;)",
            "Ljava/util/List",
            "<",
            "Landroid/text/Spannable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1461765
    invoke-static {p1}, LX/5Op;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    .line 1461766
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v0

    .line 1461767
    check-cast v3, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1461768
    sget-object v0, LX/9Fi;->TOP_LEVEL:LX/9Fi;

    invoke-static {p1}, LX/9Fi;->getCommentLevel(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/9Fi;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9Fi;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v8, v4

    move-object v7, v3

    :goto_0
    move-object v0, p2

    .line 1461769
    check-cast v0, LX/9FA;

    .line 1461770
    iget-boolean v1, v0, LX/9FA;->d:Z

    move v0, v1

    .line 1461771
    if-nez v0, :cond_3

    const/4 v5, 0x1

    .line 1461772
    :goto_1
    if-eqz v5, :cond_4

    iget-object v1, p0, LX/9Hj;->e:LX/9FH;

    move-object v0, p2

    check-cast v0, LX/9FA;

    .line 1461773
    iget-object v2, v0, LX/9FA;->k:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v0, v2

    .line 1461774
    invoke-virtual {v0}, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->k()J

    move-result-wide v10

    invoke-virtual {v1, v10, v11}, LX/9FH;->a(J)LX/9FG;

    move-result-object v6

    .line 1461775
    :goto_2
    new-instance v0, LX/9Hh;

    move-object v1, p0

    move-object v2, p2

    invoke-direct/range {v0 .. v8}, LX/9Hh;-><init>(LX/9Hj;LX/9FA;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;ZLX/9FG;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1461776
    new-instance v1, LX/8qr;

    check-cast p2, LX/1Pn;

    invoke-interface {p2}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/8qr;-><init>(Landroid/content/Context;)V

    .line 1461777
    iput-object v0, v1, LX/8qr;->b:LX/8qq;

    .line 1461778
    iget-object v0, p0, LX/9Hj;->d:LX/8qs;

    invoke-virtual {v0, v1, v3, v7}, LX/8qs;->a(LX/8qr;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 1461779
    :cond_0
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 1461780
    move-object v1, v0

    .line 1461781
    :goto_3
    if-eqz v1, :cond_1

    .line 1461782
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1461783
    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLComment;

    if-nez v0, :cond_1

    .line 1461784
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 1461785
    move-object v1, v0

    goto :goto_3

    .line 1461786
    :cond_1
    if-nez v1, :cond_2

    move-object v8, v4

    move-object v7, v3

    .line 1461787
    goto :goto_0

    .line 1461788
    :cond_2
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1461789
    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1461790
    invoke-static {v1}, LX/5Op;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v8

    move-object v7, v0

    goto :goto_0

    .line 1461791
    :cond_3
    const/4 v5, 0x0

    goto :goto_1

    .line 1461792
    :cond_4
    const/4 v6, 0x0

    goto :goto_2
.end method
