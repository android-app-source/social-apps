.class public final LX/9tY;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 19

    .prologue
    .line 1562111
    const/4 v15, 0x0

    .line 1562112
    const/4 v14, 0x0

    .line 1562113
    const/4 v13, 0x0

    .line 1562114
    const/4 v12, 0x0

    .line 1562115
    const/4 v11, 0x0

    .line 1562116
    const/4 v10, 0x0

    .line 1562117
    const/4 v9, 0x0

    .line 1562118
    const/4 v8, 0x0

    .line 1562119
    const/4 v7, 0x0

    .line 1562120
    const/4 v6, 0x0

    .line 1562121
    const/4 v5, 0x0

    .line 1562122
    const/4 v4, 0x0

    .line 1562123
    const/4 v3, 0x0

    .line 1562124
    const/4 v2, 0x0

    .line 1562125
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_1

    .line 1562126
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1562127
    const/4 v2, 0x0

    .line 1562128
    :goto_0
    return v2

    .line 1562129
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1562130
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_c

    .line 1562131
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v16

    .line 1562132
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1562133
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v17

    sget-object v18, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_1

    if-eqz v16, :cond_1

    .line 1562134
    const-string v17, "__type__"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_2

    const-string v17, "__typename"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 1562135
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v15

    goto :goto_1

    .line 1562136
    :cond_3
    const-string v17, "id"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 1562137
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    goto :goto_1

    .line 1562138
    :cond_4
    const-string v17, "live_video_count"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 1562139
    const/4 v5, 0x1

    .line 1562140
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v13

    goto :goto_1

    .line 1562141
    :cond_5
    const-string v17, "square_header_image"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 1562142
    invoke-static/range {p0 .. p1}, LX/A6E;->a(LX/15w;LX/186;)I

    move-result v12

    goto :goto_1

    .line 1562143
    :cond_6
    const-string v17, "video_channel_can_viewer_pin"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 1562144
    const/4 v4, 0x1

    .line 1562145
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    goto :goto_1

    .line 1562146
    :cond_7
    const-string v17, "video_channel_curator"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 1562147
    invoke-static/range {p0 .. p1}, LX/7OV;->a(LX/15w;LX/186;)I

    move-result v10

    goto/16 :goto_1

    .line 1562148
    :cond_8
    const-string v17, "video_channel_is_viewer_pinned"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_9

    .line 1562149
    const/4 v3, 0x1

    .line 1562150
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    goto/16 :goto_1

    .line 1562151
    :cond_9
    const-string v17, "video_channel_max_new_count"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_a

    .line 1562152
    const/4 v2, 0x1

    .line 1562153
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v8

    goto/16 :goto_1

    .line 1562154
    :cond_a
    const-string v17, "video_channel_subtitle"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_b

    .line 1562155
    invoke-static/range {p0 .. p1}, LX/A6F;->a(LX/15w;LX/186;)I

    move-result v7

    goto/16 :goto_1

    .line 1562156
    :cond_b
    const-string v17, "video_channel_title"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_0

    .line 1562157
    invoke-static/range {p0 .. p1}, LX/A6G;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 1562158
    :cond_c
    const/16 v16, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1562159
    const/16 v16, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 1562160
    const/4 v15, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v14}, LX/186;->b(II)V

    .line 1562161
    if-eqz v5, :cond_d

    .line 1562162
    const/4 v5, 0x2

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v13, v14}, LX/186;->a(III)V

    .line 1562163
    :cond_d
    const/4 v5, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v12}, LX/186;->b(II)V

    .line 1562164
    if-eqz v4, :cond_e

    .line 1562165
    const/4 v4, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->a(IZ)V

    .line 1562166
    :cond_e
    const/4 v4, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->b(II)V

    .line 1562167
    if-eqz v3, :cond_f

    .line 1562168
    const/4 v3, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->a(IZ)V

    .line 1562169
    :cond_f
    if-eqz v2, :cond_10

    .line 1562170
    const/4 v2, 0x7

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8, v3}, LX/186;->a(III)V

    .line 1562171
    :cond_10
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1562172
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1562173
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1562174
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1562175
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1562176
    if-eqz v0, :cond_0

    .line 1562177
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1562178
    invoke-static {p0, p1, v2, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1562179
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1562180
    if-eqz v0, :cond_1

    .line 1562181
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1562182
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1562183
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1562184
    if-eqz v0, :cond_2

    .line 1562185
    const-string v1, "live_video_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1562186
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1562187
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1562188
    if-eqz v0, :cond_3

    .line 1562189
    const-string v1, "square_header_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1562190
    invoke-static {p0, v0, p2}, LX/A6E;->a(LX/15i;ILX/0nX;)V

    .line 1562191
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1562192
    if-eqz v0, :cond_4

    .line 1562193
    const-string v1, "video_channel_can_viewer_pin"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1562194
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1562195
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1562196
    if-eqz v0, :cond_5

    .line 1562197
    const-string v1, "video_channel_curator"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1562198
    invoke-static {p0, v0, p2, p3}, LX/7OV;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1562199
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1562200
    if-eqz v0, :cond_6

    .line 1562201
    const-string v1, "video_channel_is_viewer_pinned"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1562202
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1562203
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1562204
    if-eqz v0, :cond_7

    .line 1562205
    const-string v1, "video_channel_max_new_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1562206
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1562207
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1562208
    if-eqz v0, :cond_8

    .line 1562209
    const-string v1, "video_channel_subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1562210
    invoke-static {p0, v0, p2}, LX/A6F;->a(LX/15i;ILX/0nX;)V

    .line 1562211
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1562212
    if-eqz v0, :cond_9

    .line 1562213
    const-string v1, "video_channel_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1562214
    invoke-static {p0, v0, p2}, LX/A6G;->a(LX/15i;ILX/0nX;)V

    .line 1562215
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1562216
    return-void
.end method
