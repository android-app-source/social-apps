.class public LX/92T;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/03V;


# direct methods
.method public constructor <init>(LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1432303
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1432304
    iput-object p1, p0, LX/92T;->a:LX/03V;

    .line 1432305
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1432306
    iget-object v0, p0, LX/92T;->a:LX/03V;

    const-string v1, "MinutiaeGraphQLError"

    const-string v2, "Object model is invalid"

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    .line 1432307
    iput-boolean v3, v1, LX/0VK;->d:Z

    .line 1432308
    move-object v1, v1

    .line 1432309
    iput v3, v1, LX/0VK;->e:I

    .line 1432310
    move-object v1, v1

    .line 1432311
    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 1432312
    iput-object v2, v1, LX/0VK;->c:Ljava/lang/Throwable;

    .line 1432313
    move-object v1, v1

    .line 1432314
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 1432315
    return-void
.end method

.method public static b(LX/0QB;)LX/92T;
    .locals 2

    .prologue
    .line 1432316
    new-instance v1, LX/92T;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-direct {v1, v0}, LX/92T;-><init>(LX/03V;)V

    .line 1432317
    return-object v1
.end method


# virtual methods
.method public final a(LX/5LG;)Z
    .locals 3

    .prologue
    .line 1432318
    invoke-interface {p1}, LX/5LG;->A()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, LX/5LG;->A()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, LX/5LG;->n()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 1432319
    :goto_0
    if-nez v0, :cond_0

    .line 1432320
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Verb model has missing fields : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, LX/5LG;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, LX/92T;->a(Ljava/lang/String;)V

    .line 1432321
    :cond_0
    return v0

    .line 1432322
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;)Z
    .locals 3

    .prologue
    .line 1432323
    invoke-virtual {p1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->e()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->d()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->d()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->e()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 1432324
    :goto_0
    if-nez v0, :cond_0

    .line 1432325
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Object model has missing fields : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->e()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, LX/92T;->a(Ljava/lang/String;)V

    .line 1432326
    :cond_0
    return v0

    .line 1432327
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
