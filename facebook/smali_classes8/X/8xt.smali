.class public LX/8xt;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0tX;

.field private final b:LX/0sa;

.field public final c:LX/189;

.field public final d:LX/20j;

.field public final e:LX/0bH;

.field public final f:LX/1K9;

.field private final g:LX/1Ck;


# direct methods
.method public constructor <init>(LX/0tX;LX/0sa;LX/189;LX/20j;LX/0bH;LX/1K9;LX/1Ck;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1424821
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1424822
    iput-object p1, p0, LX/8xt;->a:LX/0tX;

    .line 1424823
    iput-object p2, p0, LX/8xt;->b:LX/0sa;

    .line 1424824
    iput-object p3, p0, LX/8xt;->c:LX/189;

    .line 1424825
    iput-object p4, p0, LX/8xt;->d:LX/20j;

    .line 1424826
    iput-object p5, p0, LX/8xt;->e:LX/0bH;

    .line 1424827
    iput-object p6, p0, LX/8xt;->f:LX/1K9;

    .line 1424828
    iput-object p7, p0, LX/8xt;->g:LX/1Ck;

    .line 1424829
    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLPage;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel;
    .locals 10
    .param p0    # Lcom/facebook/graphql/model/GraphQLPage;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Lcom/facebook/graphql/model/GraphQLComment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1424830
    new-instance v0, LX/5IN;

    invoke-direct {v0}, LX/5IN;-><init>()V

    .line 1424831
    if-eqz p0, :cond_0

    .line 1424832
    invoke-static {p0}, LX/5I9;->a(Lcom/facebook/graphql/model/GraphQLPage;)Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;)Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 1424833
    iput-object v1, v0, LX/5IN;->a:LX/0Px;

    .line 1424834
    :cond_0
    if-eqz p1, :cond_1

    .line 1424835
    invoke-static {p1}, LX/5I9;->a(Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;->a(Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;)Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;

    move-result-object v1

    .line 1424836
    iput-object v1, v0, LX/5IN;->b:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;

    .line 1424837
    :cond_1
    if-eqz p2, :cond_2

    .line 1424838
    invoke-static {p2}, LX/5I9;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel;->a(Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel;)Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel;

    move-result-object v1

    .line 1424839
    iput-object v1, v0, LX/5IN;->c:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel;

    .line 1424840
    :cond_2
    const/4 v6, 0x1

    const/4 v9, 0x0

    const/4 v4, 0x0

    .line 1424841
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1424842
    iget-object v3, v0, LX/5IN;->a:LX/0Px;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v3

    .line 1424843
    iget-object v5, v0, LX/5IN;->b:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;

    invoke-static {v2, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1424844
    iget-object v7, v0, LX/5IN;->c:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel;

    invoke-static {v2, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1424845
    const/4 v8, 0x3

    invoke-virtual {v2, v8}, LX/186;->c(I)V

    .line 1424846
    invoke-virtual {v2, v9, v3}, LX/186;->b(II)V

    .line 1424847
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1424848
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 1424849
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1424850
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1424851
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1424852
    invoke-virtual {v3, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1424853
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1424854
    new-instance v3, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel;

    invoke-direct {v3, v2}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel;-><init>(LX/15i;)V

    .line 1424855
    move-object v0, v3

    .line 1424856
    return-object v0
.end method

.method public static b(LX/0QB;)LX/8xt;
    .locals 8

    .prologue
    .line 1424857
    new-instance v0, LX/8xt;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {p0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v2

    check-cast v2, LX/0sa;

    invoke-static {p0}, LX/189;->b(LX/0QB;)LX/189;

    move-result-object v3

    check-cast v3, LX/189;

    invoke-static {p0}, LX/20j;->a(LX/0QB;)LX/20j;

    move-result-object v4

    check-cast v4, LX/20j;

    invoke-static {p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v5

    check-cast v5, LX/0bH;

    invoke-static {p0}, LX/1K9;->a(LX/0QB;)LX/1K9;

    move-result-object v6

    check-cast v6, LX/1K9;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v7

    check-cast v7, LX/1Ck;

    invoke-direct/range {v0 .. v7}, LX/8xt;-><init>(LX/0tX;LX/0sa;LX/189;LX/20j;LX/0bH;LX/1K9;LX/1Ck;)V

    .line 1424858
    return-object v0
.end method

.method public static b(Lcom/facebook/graphql/executor/GraphQLResult;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLPage;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/graphql/model/GraphQLPage;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1424859
    if-eqz p0, :cond_0

    .line 1424860
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1424861
    if-eqz v0, :cond_0

    .line 1424862
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1424863
    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel;->j()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1424864
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1424865
    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel;->j()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;->d()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1424866
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1424867
    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel;->j()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;->d()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move-object v0, v1

    .line 1424868
    :cond_1
    :goto_0
    return-object v0

    .line 1424869
    :cond_2
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1424870
    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel;->j()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;->d()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$AttachmentsModel;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$AttachmentsModel;->j()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;

    move-result-object v0

    .line 1424871
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->b()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_3
    move-object v0, v1

    .line 1424872
    goto :goto_0

    .line 1424873
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel$AttachmentsModel$TargetModel;->b()LX/0Px;

    move-result-object v3

    .line 1424874
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    :goto_1
    if-ge v2, v4, :cond_5

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;

    .line 1424875
    invoke-static {v0}, LX/5I9;->a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;)Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    .line 1424876
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 1424877
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_5
    move-object v0, v1

    .line 1424878
    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLPage;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLComment;LX/0TF;)V
    .locals 4
    .param p3    # Lcom/facebook/graphql/model/GraphQLPage;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/graphql/model/GraphQLComment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/0TF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/model/GraphQLPage;",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1424879
    new-instance v0, LX/4DL;

    invoke-direct {v0}, LX/4DL;-><init>()V

    .line 1424880
    const-string v1, "comment_id"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1424881
    move-object v0, v0

    .line 1424882
    invoke-static {p2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 1424883
    const-string v2, "place_ids"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1424884
    move-object v0, v0

    .line 1424885
    new-instance v1, LX/5II;

    invoke-direct {v1}, LX/5II;-><init>()V

    move-object v1, v1

    .line 1424886
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1424887
    const-string v0, "profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1424888
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    invoke-static {p3, p5, p4}, LX/8xt;->a(Lcom/facebook/graphql/model/GraphQLPage;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v0

    .line 1424889
    iget-object v1, p0, LX/8xt;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1424890
    if-eqz p6, :cond_0

    .line 1424891
    iget-object v1, p0, LX/8xt;->g:LX/1Ck;

    const-string v2, "add_place"

    invoke-static {p6}, LX/0Vd;->of(LX/0TF;)LX/0Vd;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1424892
    :cond_0
    return-void
.end method
