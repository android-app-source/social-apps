.class public final LX/9U4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;)V
    .locals 0

    .prologue
    .line 1497469
    iput-object p1, p0, LX/9U4;->a:Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 1497470
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 1497471
    const v0, 0x108005a

    .line 1497472
    :goto_0
    iget-object v1, p0, LX/9U4;->a:Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;

    iget-object v1, v1, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->t:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1497473
    return-void

    .line 1497474
    :cond_0
    const v0, 0x7f020d3d

    goto :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1497475
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 1497476
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 1497477
    iget-object v1, p0, LX/9U4;->a:Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;

    iput-object v0, v1, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->v:Ljava/lang/String;

    .line 1497478
    iget-object v0, p0, LX/9U4;->a:Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;

    iget-object v0, v0, Lcom/facebook/katana/activity/profilelist/ProfileListActivity;->w:LX/9U9;

    check-cast v0, LX/9UD;

    iget-object v0, v0, LX/9UC;->h:Landroid/widget/Filter;

    iget-object v1, p0, LX/9U4;->a:Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;

    iget-object v1, v1, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->v:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    .line 1497479
    iget-object v0, p0, LX/9U4;->a:Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;

    iget-object v0, v0, Lcom/facebook/katana/activity/profilelist/ProfileListActivity;->x:Lcom/facebook/widget/listview/BetterListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setFastScrollEnabled(Z)V

    .line 1497480
    return-void
.end method
