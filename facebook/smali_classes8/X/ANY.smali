.class public final LX/ANY;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/cameracore/ui/CameraCoreFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V
    .locals 0

    .prologue
    .line 1667750
    iput-object p1, p0, LX/ANY;->a:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1667741
    const/4 v0, 0x1

    return v0
.end method

.method public final onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1667749
    const/4 v0, 0x1

    return v0
.end method

.method public final onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1667742
    :try_start_0
    sget-object v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->h:LX/6Ia;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->h:LX/6Ia;

    invoke-interface {v1}, LX/6Ia;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->h:LX/6Ia;

    invoke-interface {v1}, LX/6Ia;->a()LX/6IP;

    move-result-object v1

    invoke-interface {v1}, LX/6IP;->j()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_1

    .line 1667743
    :cond_0
    :goto_0
    return v0

    .line 1667744
    :catch_0
    move-exception v1

    .line 1667745
    iget-object v2, p0, LX/ANY;->a:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    iget-object v2, v2, Lcom/facebook/cameracore/ui/CameraCoreFragment;->J:LX/03V;

    const-string v3, "cameracore_tap_to_focus"

    invoke-virtual {v2, v3, v1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1667746
    :cond_1
    iget-object v0, p0, LX/ANY;->a:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    iget-object v0, v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->R:Lcom/facebook/cameracore/ui/FocusView;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/cameracore/ui/FocusView;->a(II)V

    .line 1667747
    sget-object v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->h:LX/6Ia;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget-object v2, p0, LX/ANY;->a:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    iget-object v2, v2, Lcom/facebook/cameracore/ui/CameraCoreFragment;->f:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

    invoke-virtual {v2}, Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget-object v3, p0, LX/ANY;->a:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    iget-object v3, v3, Lcom/facebook/cameracore/ui/CameraCoreFragment;->f:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

    invoke-virtual {v3}, Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-interface {v0, v1, v2}, LX/6Ia;->a(FF)V

    .line 1667748
    const/4 v0, 0x1

    goto :goto_0
.end method
