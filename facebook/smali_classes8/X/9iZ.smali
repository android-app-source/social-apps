.class public LX/9iZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/75Q;

.field public final b:LX/75F;

.field public final c:LX/747;

.field private d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/facerec/manager/FaceBoxPrioritizer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/75Q;LX/75F;LX/747;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/facerec/manager/FaceBoxPrioritizer;",
            ">;",
            "LX/75Q;",
            "LX/75F;",
            "LX/747;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1528015
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1528016
    iput-object p1, p0, LX/9iZ;->d:LX/0Ot;

    .line 1528017
    iput-object p2, p0, LX/9iZ;->a:LX/75Q;

    .line 1528018
    iput-object p3, p0, LX/9iZ;->b:LX/75F;

    .line 1528019
    iput-object p4, p0, LX/9iZ;->c:LX/747;

    .line 1528020
    return-void
.end method

.method public static a(LX/0QB;)LX/9iZ;
    .locals 1

    .prologue
    .line 1528021
    invoke-static {p0}, LX/9iZ;->b(LX/0QB;)LX/9iZ;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/9iZ;
    .locals 5

    .prologue
    .line 1528022
    new-instance v3, LX/9iZ;

    const/16 v0, 0x1c2d

    invoke-static {p0, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {p0}, LX/75Q;->a(LX/0QB;)LX/75Q;

    move-result-object v0

    check-cast v0, LX/75Q;

    invoke-static {p0}, LX/75F;->a(LX/0QB;)LX/75F;

    move-result-object v1

    check-cast v1, LX/75F;

    invoke-static {p0}, LX/747;->b(LX/0QB;)LX/747;

    move-result-object v2

    check-cast v2, LX/747;

    invoke-direct {v3, v4, v0, v1, v2}, LX/9iZ;-><init>(LX/0Ot;LX/75Q;LX/75F;LX/747;)V

    .line 1528023
    return-object v3
.end method


# virtual methods
.method public final a(Landroid/content/Context;LX/7ye;LX/0Px;)V
    .locals 10
    .param p3    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/7ye;",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/base/media/PhotoItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1528024
    iget-object v0, p2, LX/7ye;->c:Lcom/facebook/photos/base/media/PhotoItem;

    move-object v3, v0

    .line 1528025
    if-nez v3, :cond_1

    .line 1528026
    :cond_0
    return-void

    .line 1528027
    :cond_1
    iget-object v0, p2, LX/7ye;->d:Ljava/util/List;

    move-object v0, v0

    .line 1528028
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/tagging/Tag;

    .line 1528029
    iget-object v2, p0, LX/9iZ;->a:LX/75Q;

    invoke-virtual {v2, v3, v0}, LX/75Q;->a(Lcom/facebook/photos/base/media/PhotoItem;Lcom/facebook/photos/base/tagging/Tag;)V

    goto :goto_0

    .line 1528030
    :cond_2
    if-eqz v3, :cond_3

    .line 1528031
    iget-object v0, v3, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    move-object v0, v0

    .line 1528032
    if-eqz v0, :cond_3

    .line 1528033
    iget-object v0, v3, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    move-object v0, v0

    .line 1528034
    instance-of v0, v0, LX/74x;

    if-eqz v0, :cond_3

    iget-object v1, p0, LX/9iZ;->b:LX/75F;

    .line 1528035
    iget-object v0, v3, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    move-object v0, v0

    .line 1528036
    check-cast v0, LX/74x;

    invoke-virtual {v1, v0}, LX/75F;->b(LX/74x;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1528037
    :cond_3
    :goto_1
    if-eqz p3, :cond_0

    .line 1528038
    invoke-virtual {p3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_2
    if-ge v2, v4, :cond_0

    invoke-virtual {p3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1528039
    invoke-virtual {v3}, Lcom/facebook/ipc/media/MediaItem;->c()J

    move-result-wide v6

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->c()J

    move-result-wide v8

    cmp-long v1, v6, v8

    if-nez v1, :cond_4

    if-eq v3, v0, :cond_4

    .line 1528040
    iget-object v1, v0, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    move-object v0, v1

    .line 1528041
    check-cast v0, LX/74x;

    .line 1528042
    iget-object v1, v3, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    move-object v1, v1

    .line 1528043
    check-cast v1, LX/74x;

    .line 1528044
    iget-object v5, p0, LX/9iZ;->a:LX/75Q;

    iget-object v6, p0, LX/9iZ;->a:LX/75Q;

    invoke-virtual {v6, v1}, LX/75Q;->a(LX/74x;)Ljava/util/List;

    move-result-object v6

    invoke-virtual {v5, v0, v6}, LX/75Q;->a(LX/74x;Ljava/util/List;)V

    .line 1528045
    iget-object v5, p0, LX/9iZ;->b:LX/75F;

    iget-object v6, p0, LX/9iZ;->b:LX/75F;

    invoke-virtual {v6, v1}, LX/75F;->a(LX/74x;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, LX/75F;->a(LX/74x;Ljava/util/List;)V

    .line 1528046
    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 1528047
    :cond_5
    iget-object v0, v3, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    move-object v0, v0

    .line 1528048
    check-cast v0, LX/74x;

    .line 1528049
    iget-object v1, p0, LX/9iZ;->b:LX/75F;

    invoke-virtual {v1, v0}, LX/75F;->a(LX/74x;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1528050
    iget-boolean v2, v0, Lcom/facebook/photos/base/tagging/FaceBox;->f:Z

    move v0, v2

    .line 1528051
    if-nez v0, :cond_6

    goto :goto_1

    .line 1528052
    :cond_7
    iget-object v0, p0, LX/9iZ;->a:LX/75Q;

    invoke-virtual {v0, p1, v3}, LX/75Q;->a(Landroid/content/Context;Lcom/facebook/photos/base/media/PhotoItem;)V

    goto :goto_1
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1528053
    iget-object v0, p0, LX/9iZ;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    invoke-virtual {v0}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->d()V

    .line 1528054
    return-void
.end method
