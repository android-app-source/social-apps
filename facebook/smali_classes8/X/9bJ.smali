.class public LX/9bJ;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private final A:Ljava/lang/StringBuilder;

.field private final B:Ljava/lang/StringBuilder;

.field private C:LX/9bI;

.field public a:D

.field public b:Lcom/facebook/resources/ui/FbTextView;

.field public c:Lcom/facebook/resources/ui/FbTextView;

.field public d:Lcom/facebook/graphql/model/GraphQLAlbum;

.field public e:Lcom/facebook/graphql/model/GraphQLAlbum;

.field public f:LX/9bY;

.field public g:Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;

.field public h:Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;

.field public i:Lcom/facebook/photos/albums/PandoraAlbumUploadButtonView;

.field public j:Ljava/lang/String;

.field private k:D

.field private l:D

.field private m:Lcom/facebook/photos/albums/AlbumsRowView;

.field private n:Lcom/facebook/resources/ui/FbTextView;

.field private o:Lcom/facebook/resources/ui/FbTextView;

.field private p:I

.field public q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9at;",
            ">;"
        }
    .end annotation
.end field

.field public s:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9XE;",
            ">;"
        }
    .end annotation
.end field

.field public t:Lcom/facebook/auth/viewercontext/ViewerContext;

.field public u:LX/0hL;

.field public v:LX/9bK;

.field private w:Z

.field public x:Ljava/lang/String;

.field public y:Z

.field private final z:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    .line 1514788
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1514789
    iput-wide v0, p0, LX/9bJ;->k:D

    .line 1514790
    iput-wide v0, p0, LX/9bJ;->l:D

    .line 1514791
    iput-wide v0, p0, LX/9bJ;->a:D

    .line 1514792
    const/4 v0, -0x1

    iput v0, p0, LX/9bJ;->p:I

    .line 1514793
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/9bJ;->y:Z

    .line 1514794
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/9bJ;->z:Landroid/graphics/Rect;

    .line 1514795
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, LX/9bJ;->A:Ljava/lang/StringBuilder;

    .line 1514796
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, LX/9bJ;->B:Ljava/lang/StringBuilder;

    .line 1514797
    invoke-direct {p0}, LX/9bJ;->f()V

    .line 1514798
    return-void
.end method

.method public static a(LX/9bJ;Lcom/facebook/graphql/model/GraphQLAlbum;Z)V
    .locals 9

    .prologue
    .line 1514778
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/9bJ;->f:LX/9bY;

    if-nez v0, :cond_1

    .line 1514779
    :cond_0
    :goto_0
    return-void

    .line 1514780
    :cond_1
    if-eqz p2, :cond_2

    .line 1514781
    new-instance v0, LX/9bm;

    invoke-direct {v0}, LX/9bm;-><init>()V

    .line 1514782
    iget-object v1, p0, LX/9bJ;->f:LX/9bY;

    invoke-virtual {v1, v0}, LX/0b4;->a(LX/0b7;)V

    .line 1514783
    :goto_1
    iget-boolean v0, p0, LX/9bJ;->y:Z

    if-eqz v0, :cond_0

    .line 1514784
    iget-object v0, p0, LX/9bJ;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/9XE;

    iget-object v0, p0, LX/9bJ;->x:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v5

    :goto_2
    iget-object v0, p0, LX/9bJ;->d:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-ne p1, v0, :cond_4

    iget v6, p0, LX/9bJ;->p:I

    :goto_3
    const-string v7, "pandora_albums_grid"

    move v8, p2

    invoke-virtual/range {v1 .. v8}, LX/9XE;->a(JLjava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V

    goto :goto_0

    .line 1514785
    :cond_2
    new-instance v0, LX/9be;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, v1}, LX/9be;-><init>(Lcom/facebook/graphql/model/GraphQLAlbum;Ljava/lang/String;)V

    .line 1514786
    iget-object v1, p0, LX/9bJ;->f:LX/9bY;

    invoke-virtual {v1, v0}, LX/0b4;->a(LX/0b7;)V

    goto :goto_1

    .line 1514787
    :cond_3
    const/4 v5, 0x0

    goto :goto_2

    :cond_4
    iget v0, p0, LX/9bJ;->p:I

    add-int/lit8 v6, v0, 0x1

    goto :goto_3
.end method

.method private f()V
    .locals 13

    .prologue
    const/4 v8, 0x0

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    .line 1514745
    const v0, 0x7f030ef3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1514746
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v12

    move-object v2, p0

    check-cast v2, LX/9bJ;

    invoke-static {v12}, LX/9bY;->a(LX/0QB;)LX/9bY;

    move-result-object v3

    check-cast v3, LX/9bY;

    const/16 v4, 0x455

    invoke-static {v12, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2dfd

    invoke-static {v12, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v9, 0x2b68

    invoke-static {v12, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {v12}, LX/0eQ;->b(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v10

    check-cast v10, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static {v12}, LX/0hL;->a(LX/0QB;)LX/0hL;

    move-result-object v11

    check-cast v11, LX/0hL;

    const-class v0, LX/9bL;

    invoke-interface {v12, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/9bL;

    iput-object v3, v2, LX/9bJ;->f:LX/9bY;

    iput-object v4, v2, LX/9bJ;->q:LX/0Ot;

    iput-object v5, v2, LX/9bJ;->r:LX/0Ot;

    iput-object v9, v2, LX/9bJ;->s:LX/0Ot;

    iput-object v10, v2, LX/9bJ;->t:Lcom/facebook/auth/viewercontext/ViewerContext;

    iget-object v0, v10, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v0

    iput-object v0, v2, LX/9bJ;->j:Ljava/lang/String;

    iput-object v11, v2, LX/9bJ;->u:LX/0hL;

    new-instance v0, LX/9bK;

    invoke-direct {v0, v2}, LX/9bK;-><init>(LX/9bJ;)V

    move-object v0, v0

    iput-object v0, v2, LX/9bJ;->v:LX/9bK;

    .line 1514747
    invoke-virtual {p0}, LX/9bJ;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-double v0, v0

    iput-wide v0, p0, LX/9bJ;->k:D

    .line 1514748
    invoke-virtual {p0}, LX/9bJ;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0ad5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-double v0, v0

    iput-wide v0, p0, LX/9bJ;->l:D

    .line 1514749
    iget-wide v0, p0, LX/9bJ;->k:D

    invoke-virtual {p0}, LX/9bJ;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0ada

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    mul-int/lit8 v2, v2, 0x3

    int-to-double v2, v2

    sub-double/2addr v0, v2

    div-double/2addr v0, v6

    iput-wide v0, p0, LX/9bJ;->a:D

    .line 1514750
    invoke-virtual {p0, p0}, LX/9bJ;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1514751
    const v0, 0x7f0d2471

    invoke-virtual {p0, v0}, LX/9bJ;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;

    iput-object v0, p0, LX/9bJ;->g:Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;

    .line 1514752
    const v0, 0x7f0d2472

    invoke-virtual {p0, v0}, LX/9bJ;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;

    iput-object v0, p0, LX/9bJ;->h:Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;

    .line 1514753
    const v0, 0x7f0d2473

    invoke-virtual {p0, v0}, LX/9bJ;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1514754
    invoke-virtual {p0}, LX/9bJ;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0ada

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget-wide v2, p0, LX/9bJ;->a:D

    double-to-int v2, v2

    invoke-virtual {p0}, LX/9bJ;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0add

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    add-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x0

    invoke-virtual {p0}, LX/9bJ;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0ade

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x0

    invoke-static {v0, v1, v2, v8, v3}, LX/0vv;->b(Landroid/view/View;IIII)V

    .line 1514755
    const v0, 0x7f0d2475

    invoke-virtual {p0, v0}, LX/9bJ;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/9bJ;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 1514756
    iget-object v0, p0, LX/9bJ;->c:Lcom/facebook/resources/ui/FbTextView;

    iget-wide v2, p0, LX/9bJ;->a:D

    iget-wide v4, p0, LX/9bJ;->l:D

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    double-to-int v1, v2

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setMaxWidth(I)V

    .line 1514757
    const v0, 0x7f0d2474

    invoke-virtual {p0, v0}, LX/9bJ;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/9bJ;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 1514758
    iget-object v0, p0, LX/9bJ;->b:Lcom/facebook/resources/ui/FbTextView;

    iget-wide v2, p0, LX/9bJ;->a:D

    iget-wide v4, p0, LX/9bJ;->l:D

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    double-to-int v1, v2

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setMaxWidth(I)V

    .line 1514759
    const v0, 0x7f0d2476

    invoke-virtual {p0, v0}, LX/9bJ;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1514760
    iget-wide v2, p0, LX/9bJ;->a:D

    double-to-int v1, v2

    invoke-virtual {p0}, LX/9bJ;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0ada

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    iget-wide v2, p0, LX/9bJ;->a:D

    double-to-int v2, v2

    invoke-virtual {p0}, LX/9bJ;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0add

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    add-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x0

    invoke-virtual {p0}, LX/9bJ;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0ade

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x0

    invoke-static {v0, v1, v2, v8, v3}, LX/0vv;->b(Landroid/view/View;IIII)V

    .line 1514761
    const v0, 0x7f0d2478

    invoke-virtual {p0, v0}, LX/9bJ;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/9bJ;->o:Lcom/facebook/resources/ui/FbTextView;

    .line 1514762
    iget-object v0, p0, LX/9bJ;->o:Lcom/facebook/resources/ui/FbTextView;

    iget-wide v2, p0, LX/9bJ;->a:D

    iget-wide v4, p0, LX/9bJ;->l:D

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    double-to-int v1, v2

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setMaxWidth(I)V

    .line 1514763
    const v0, 0x7f0d2477

    invoke-virtual {p0, v0}, LX/9bJ;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/9bJ;->n:Lcom/facebook/resources/ui/FbTextView;

    .line 1514764
    iget-object v0, p0, LX/9bJ;->n:Lcom/facebook/resources/ui/FbTextView;

    iget-wide v2, p0, LX/9bJ;->a:D

    iget-wide v4, p0, LX/9bJ;->l:D

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    double-to-int v1, v2

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setMaxWidth(I)V

    .line 1514765
    const v0, 0x7f0d2479

    invoke-virtual {p0, v0}, LX/9bJ;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/AlbumsRowView;

    iput-object v0, p0, LX/9bJ;->m:Lcom/facebook/photos/albums/AlbumsRowView;

    .line 1514766
    const v0, 0x7f0d2470

    invoke-virtual {p0, v0}, LX/9bJ;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/PandoraAlbumUploadButtonView;

    iput-object v0, p0, LX/9bJ;->i:Lcom/facebook/photos/albums/PandoraAlbumUploadButtonView;

    .line 1514767
    iget-object v0, p0, LX/9bJ;->i:Lcom/facebook/photos/albums/PandoraAlbumUploadButtonView;

    invoke-virtual {p0}, LX/9bJ;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0ada

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-static {v0, v1, v8, v8, v8}, LX/0vv;->b(Landroid/view/View;IIII)V

    .line 1514768
    iget-object v0, p0, LX/9bJ;->i:Lcom/facebook/photos/albums/PandoraAlbumUploadButtonView;

    new-instance v1, LX/9bG;

    invoke-direct {v1, p0}, LX/9bG;-><init>(LX/9bJ;)V

    invoke-virtual {v0, v1}, Lcom/facebook/photos/albums/PandoraAlbumUploadButtonView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1514769
    iget-object v0, p0, LX/9bJ;->m:Lcom/facebook/photos/albums/AlbumsRowView;

    invoke-virtual {v0}, Lcom/facebook/photos/albums/AlbumsRowView;->bringToFront()V

    .line 1514770
    iget-object v0, p0, LX/9bJ;->i:Lcom/facebook/photos/albums/PandoraAlbumUploadButtonView;

    invoke-virtual {v0}, Lcom/facebook/photos/albums/PandoraAlbumUploadButtonView;->bringToFront()V

    .line 1514771
    iget-object v0, p0, LX/9bJ;->g:Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;

    invoke-virtual {v0}, Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;->bringToFront()V

    .line 1514772
    iget-object v0, p0, LX/9bJ;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->bringToFront()V

    .line 1514773
    iget-object v0, p0, LX/9bJ;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->bringToFront()V

    .line 1514774
    iget-object v0, p0, LX/9bJ;->h:Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;

    invoke-virtual {v0}, Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;->bringToFront()V

    .line 1514775
    iget-object v0, p0, LX/9bJ;->n:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->bringToFront()V

    .line 1514776
    iget-object v0, p0, LX/9bJ;->o:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->bringToFront()V

    .line 1514777
    return-void
.end method


# virtual methods
.method public final a(LX/9bE;II)Landroid/graphics/Rect;
    .locals 4

    .prologue
    .line 1514639
    iget-wide v0, p0, LX/9bJ;->k:D

    double-to-int v0, v0

    div-int/lit8 v0, v0, 0x2

    .line 1514640
    sget-object v1, LX/9bH;->a:[I

    invoke-virtual {p1}, LX/9bE;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1514641
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1514642
    :pswitch_0
    iget-object v1, p0, LX/9bJ;->z:Landroid/graphics/Rect;

    add-int/2addr v0, p2

    invoke-virtual {p0}, LX/9bJ;->getHeight()I

    move-result v2

    add-int/2addr v2, p3

    invoke-virtual {v1, p2, p3, v0, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 1514643
    iget-object v0, p0, LX/9bJ;->z:Landroid/graphics/Rect;

    goto :goto_0

    .line 1514644
    :pswitch_1
    iget-object v1, p0, LX/9bJ;->z:Landroid/graphics/Rect;

    add-int/2addr v0, p2

    iget-wide v2, p0, LX/9bJ;->k:D

    double-to-int v2, v2

    add-int/2addr v2, p2

    invoke-virtual {p0}, LX/9bJ;->getHeight()I

    move-result v3

    add-int/2addr v3, p3

    invoke-virtual {v1, v0, p3, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1514645
    iget-object v0, p0, LX/9bJ;->z:Landroid/graphics/Rect;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(LX/9bE;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1514741
    sget-object v0, LX/9bH;->a:[I

    invoke-virtual {p1}, LX/9bE;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1514742
    const-string v0, ""

    :goto_0
    return-object v0

    .line 1514743
    :pswitch_0
    iget-object v0, p0, LX/9bJ;->A:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1514744
    :pswitch_1
    iget-object v0, p0, LX/9bJ;->B:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(ILjava/lang/String;ZLcom/facebook/graphql/model/GraphQLAlbum;Lcom/facebook/graphql/model/GraphQLAlbum;ZLX/9bI;)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 1514675
    iput p1, p0, LX/9bJ;->p:I

    .line 1514676
    iput-object p4, p0, LX/9bJ;->d:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 1514677
    iput-object p5, p0, LX/9bJ;->e:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 1514678
    iput-boolean p6, p0, LX/9bJ;->w:Z

    .line 1514679
    iput-object p2, p0, LX/9bJ;->x:Ljava/lang/String;

    .line 1514680
    iput-boolean p3, p0, LX/9bJ;->y:Z

    .line 1514681
    iput-object p7, p0, LX/9bJ;->C:LX/9bI;

    .line 1514682
    iget-object v0, p0, LX/9bJ;->m:Lcom/facebook/photos/albums/AlbumsRowView;

    invoke-virtual {v0}, Lcom/facebook/photos/albums/AlbumsRowView;->a()V

    .line 1514683
    iget-object v0, p0, LX/9bJ;->m:Lcom/facebook/photos/albums/AlbumsRowView;

    invoke-virtual {v0}, Lcom/facebook/photos/albums/AlbumsRowView;->invalidate()V

    .line 1514684
    iget-object v0, p0, LX/9bJ;->m:Lcom/facebook/photos/albums/AlbumsRowView;

    iget-boolean v3, p0, LX/9bJ;->w:Z

    invoke-virtual {p0}, LX/9bJ;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0ada

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {p0}, LX/9bJ;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0adb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    add-int/lit8 v5, v1, 0x0

    move-object v1, p4

    move-object v2, p5

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/photos/albums/AlbumsRowView;->a(Lcom/facebook/graphql/model/GraphQLAlbum;Lcom/facebook/graphql/model/GraphQLAlbum;ZII)V

    .line 1514685
    iget-object v0, p0, LX/9bJ;->A:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 1514686
    iget-object v0, p0, LX/9bJ;->B:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 1514687
    iget-object v0, p0, LX/9bJ;->C:LX/9bI;

    sget-object v1, LX/9bI;->LEFT:LX/9bI;

    if-eq v0, v1, :cond_2

    if-eqz p4, :cond_0

    invoke-virtual {p4}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p4}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1514688
    :cond_0
    iget-object v0, p0, LX/9bJ;->b:Lcom/facebook/resources/ui/FbTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1514689
    iget-object v0, p0, LX/9bJ;->c:Lcom/facebook/resources/ui/FbTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1514690
    iget-object v0, p0, LX/9bJ;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v7}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1514691
    iget-object v0, p0, LX/9bJ;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v7}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1514692
    iget-object v0, p0, LX/9bJ;->g:Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;

    invoke-virtual {v0, v7}, Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;->setVisibility(I)V

    .line 1514693
    iget-object v0, p0, LX/9bJ;->i:Lcom/facebook/photos/albums/PandoraAlbumUploadButtonView;

    invoke-virtual {v0, v6}, Lcom/facebook/photos/albums/PandoraAlbumUploadButtonView;->setVisibility(I)V

    .line 1514694
    iget-object v0, p0, LX/9bJ;->i:Lcom/facebook/photos/albums/PandoraAlbumUploadButtonView;

    invoke-virtual {v0}, Lcom/facebook/photos/albums/PandoraAlbumUploadButtonView;->bringToFront()V

    .line 1514695
    :goto_0
    iget-object v0, p0, LX/9bJ;->C:LX/9bI;

    sget-object v1, LX/9bI;->RIGHT:LX/9bI;

    if-eq v0, v1, :cond_5

    if-eqz p5, :cond_1

    invoke-virtual {p5}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p5}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_5

    .line 1514696
    :cond_1
    iget-object v0, p0, LX/9bJ;->n:Lcom/facebook/resources/ui/FbTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1514697
    iget-object v0, p0, LX/9bJ;->o:Lcom/facebook/resources/ui/FbTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1514698
    iget-object v0, p0, LX/9bJ;->n:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v7}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1514699
    iget-object v0, p0, LX/9bJ;->o:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v7}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1514700
    iget-object v0, p0, LX/9bJ;->h:Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;

    invoke-virtual {v0, v7}, Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;->setVisibility(I)V

    .line 1514701
    :goto_1
    iget-object v0, p0, LX/9bJ;->j:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 1514702
    iget-object v0, p0, LX/9bJ;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v7}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1514703
    iget-object v0, p0, LX/9bJ;->n:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v7}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1514704
    iget-object v0, p0, LX/9bJ;->c:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/9bJ;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1514705
    iget-object v0, p0, LX/9bJ;->o:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/9bJ;->n:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1514706
    iget-object v0, p0, LX/9bJ;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v6}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1514707
    iget-object v0, p0, LX/9bJ;->o:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v6}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1514708
    iget-object v0, p0, LX/9bJ;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, LX/9bJ;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a049c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1514709
    iget-object v0, p0, LX/9bJ;->o:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, LX/9bJ;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a049c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1514710
    :goto_2
    return-void

    .line 1514711
    :cond_2
    iget-object v0, p0, LX/9bJ;->C:LX/9bI;

    sget-object v1, LX/9bI;->LEFT:LX/9bI;

    if-ne v0, v1, :cond_4

    .line 1514712
    iget-object v0, p0, LX/9bJ;->i:Lcom/facebook/photos/albums/PandoraAlbumUploadButtonView;

    invoke-virtual {v0, v7}, Lcom/facebook/photos/albums/PandoraAlbumUploadButtonView;->setVisibility(I)V

    .line 1514713
    iget-object v0, p0, LX/9bJ;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v6}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1514714
    iget-object v0, p0, LX/9bJ;->g:Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;

    invoke-virtual {v0, v6}, Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;->setVisibility(I)V

    .line 1514715
    iget-object v0, p0, LX/9bJ;->b:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f0811d3

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1514716
    :cond_3
    iget-object v0, p0, LX/9bJ;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v7}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 1514717
    :cond_4
    iget-object v0, p0, LX/9bJ;->i:Lcom/facebook/photos/albums/PandoraAlbumUploadButtonView;

    invoke-virtual {v0, v7}, Lcom/facebook/photos/albums/PandoraAlbumUploadButtonView;->setVisibility(I)V

    .line 1514718
    iget-object v0, p0, LX/9bJ;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v6}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1514719
    iget-object v0, p0, LX/9bJ;->g:Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;

    invoke-virtual {v0, v6}, Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;->setVisibility(I)V

    .line 1514720
    iget-object v0, p0, LX/9bJ;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p4}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1514721
    iget-object v0, p0, LX/9bJ;->A:Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ". "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1514722
    invoke-virtual {p4}, Lcom/facebook/graphql/model/GraphQLAlbum;->v()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1514723
    iget-object v0, p0, LX/9bJ;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v6}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1514724
    iget-object v0, p0, LX/9bJ;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, LX/9bJ;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p4, v1}, LX/9bt;->a(Lcom/facebook/graphql/model/GraphQLAlbum;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 1514725
    :cond_5
    iget-object v0, p0, LX/9bJ;->C:LX/9bI;

    sget-object v1, LX/9bI;->RIGHT:LX/9bI;

    if-ne v0, v1, :cond_7

    .line 1514726
    iget-object v0, p0, LX/9bJ;->n:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v6}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1514727
    iget-object v0, p0, LX/9bJ;->h:Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;

    invoke-virtual {v0, v6}, Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;->setVisibility(I)V

    .line 1514728
    iget-object v0, p0, LX/9bJ;->n:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f0811d3

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1514729
    :cond_6
    iget-object v0, p0, LX/9bJ;->o:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v7}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 1514730
    :cond_7
    iget-object v0, p0, LX/9bJ;->n:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v6}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1514731
    iget-object v0, p0, LX/9bJ;->h:Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;

    invoke-virtual {v0, v6}, Lcom/facebook/photos/albums/PandoraAlbumLinesHeaderView;->setVisibility(I)V

    .line 1514732
    iget-object v0, p0, LX/9bJ;->n:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p5}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1514733
    iget-object v0, p0, LX/9bJ;->B:Ljava/lang/StringBuilder;

    invoke-virtual {p5}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ". "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1514734
    invoke-virtual {p5}, Lcom/facebook/graphql/model/GraphQLAlbum;->v()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 1514735
    iget-object v0, p0, LX/9bJ;->o:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v6}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1514736
    iget-object v0, p0, LX/9bJ;->o:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, LX/9bJ;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p5, v1}, LX/9bt;->a(Lcom/facebook/graphql/model/GraphQLAlbum;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1514737
    :cond_8
    iget-object v0, p0, LX/9bJ;->A:Ljava/lang/StringBuilder;

    iget-object v1, p0, LX/9bJ;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 1514738
    iget-object v0, p0, LX/9bJ;->B:Ljava/lang/StringBuilder;

    iget-object v1, p0, LX/9bJ;->o:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 1514739
    iget-object v0, p0, LX/9bJ;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, LX/9bJ;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a049b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1514740
    iget-object v0, p0, LX/9bJ;->o:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, LX/9bJ;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a049b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    goto/16 :goto_2
.end method

.method public final dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1514672
    iget-object v0, p0, LX/9bJ;->v:LX/9bK;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9bJ;->v:LX/9bK;

    invoke-virtual {v0, p1}, LX/556;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1514673
    const/4 v0, 0x1

    .line 1514674
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1514671
    iget-object v0, p0, LX/9bJ;->d:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/9bJ;->e:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;
    .locals 1

    .prologue
    .line 1514668
    iget-object v0, p0, LX/9bJ;->v:LX/9bK;

    .line 1514669
    iget-object p0, v0, LX/3t3;->b:Ljava/lang/Object;

    move-object v0, p0

    .line 1514670
    check-cast v0, Landroid/view/accessibility/AccessibilityNodeProvider;

    return-object v0
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8

    .prologue
    const/4 v3, 0x3

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1514647
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-eq v2, v1, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-eq v2, v3, :cond_0

    .line 1514648
    :goto_0
    return v0

    .line 1514649
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v3, :cond_1

    move v0, v1

    .line 1514650
    goto :goto_0

    .line 1514651
    :cond_1
    const/4 v2, 0x2

    new-array v2, v2, [I

    .line 1514652
    invoke-virtual {p0, v2}, LX/9bJ;->getLocationInWindow([I)V

    .line 1514653
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    aget v0, v2, v0

    int-to-float v0, v0

    sub-float v0, v3, v0

    float-to-int v0, v0

    .line 1514654
    iget-object v2, p0, LX/9bJ;->u:LX/0hL;

    invoke-virtual {v2}, LX/0hL;->a()Z

    move-result v2

    .line 1514655
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    if-ne v3, v1, :cond_4

    .line 1514656
    int-to-double v4, v0

    iget-wide v6, p0, LX/9bJ;->a:D

    cmpg-double v3, v4, v6

    if-gez v3, :cond_2

    if-eqz v2, :cond_3

    :cond_2
    int-to-double v4, v0

    iget-wide v6, p0, LX/9bJ;->a:D

    cmpl-double v0, v4, v6

    if-ltz v0, :cond_5

    if-eqz v2, :cond_5

    .line 1514657
    :cond_3
    iget-object v0, p0, LX/9bJ;->d:Lcom/facebook/graphql/model/GraphQLAlbum;

    iget-object v2, p0, LX/9bJ;->C:LX/9bI;

    .line 1514658
    sget-object v3, LX/9bI;->LEFT:LX/9bI;

    if-ne v2, v3, :cond_6

    const/4 v3, 0x1

    .line 1514659
    :goto_1
    invoke-static {p0, v0, v3}, LX/9bJ;->a(LX/9bJ;Lcom/facebook/graphql/model/GraphQLAlbum;Z)V

    .line 1514660
    :cond_4
    :goto_2
    move v0, v1

    .line 1514661
    goto :goto_0

    .line 1514662
    :cond_5
    iget-object v0, p0, LX/9bJ;->e:Lcom/facebook/graphql/model/GraphQLAlbum;

    iget-object v2, p0, LX/9bJ;->C:LX/9bI;

    .line 1514663
    sget-object v3, LX/9bI;->RIGHT:LX/9bI;

    if-ne v2, v3, :cond_7

    const/4 v3, 0x1

    .line 1514664
    :goto_3
    invoke-static {p0, v0, v3}, LX/9bJ;->a(LX/9bJ;Lcom/facebook/graphql/model/GraphQLAlbum;Z)V

    .line 1514665
    goto :goto_2

    .line 1514666
    :cond_6
    const/4 v3, 0x0

    goto :goto_1

    .line 1514667
    :cond_7
    const/4 v3, 0x0

    goto :goto_3
.end method

.method public final performAccessibilityAction(ILandroid/os/Bundle;)Z
    .locals 1

    .prologue
    .line 1514646
    invoke-virtual {p0}, LX/9bJ;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
