.class public final LX/9xy;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1593763
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1593764
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1593765
    :goto_0
    return v1

    .line 1593766
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1593767
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1593768
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1593769
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1593770
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1593771
    const-string v4, "icon_image"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1593772
    invoke-static {p0, p1}, LX/5tK;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1593773
    :cond_2
    const-string v4, "icon_sizing"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1593774
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    goto :goto_1

    .line 1593775
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1593776
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1593777
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1593778
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1593779
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1593780
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1593781
    if-eqz v0, :cond_0

    .line 1593782
    const-string v1, "icon_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1593783
    invoke-static {p0, v0, p2}, LX/5tK;->a(LX/15i;ILX/0nX;)V

    .line 1593784
    :cond_0
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1593785
    if-eqz v0, :cond_1

    .line 1593786
    const-string v0, "icon_sizing"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1593787
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1593788
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1593789
    return-void
.end method
