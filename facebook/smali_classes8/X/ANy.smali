.class public LX/ANy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ANi;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Landroid/net/Uri;

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1668593
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1668594
    const v0, 0x7f082769

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/ANy;->a:Ljava/lang/String;

    .line 1668595
    const v0, 0x7f020952

    invoke-static {p1, v0}, LX/AOb;->a(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, LX/ANy;->b:Landroid/net/Uri;

    .line 1668596
    iput-object p2, p0, LX/ANy;->c:LX/0Px;

    .line 1668597
    return-void
.end method


# virtual methods
.method public final a()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1668598
    iget-object v0, p0, LX/ANy;->b:Landroid/net/Uri;

    return-object v0
.end method
