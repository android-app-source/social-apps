.class public LX/A6w;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1625173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1625174
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/A6w;->a:Ljava/util/Map;

    .line 1625175
    iget-object v0, p0, LX/A6w;->a:Ljava/util/Map;

    const-string v1, "\u094d\u0905"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1625176
    iget-object v0, p0, LX/A6w;->a:Ljava/util/Map;

    const-string v1, "\u094d\u0906"

    const-string v2, "\u093e"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1625177
    iget-object v0, p0, LX/A6w;->a:Ljava/util/Map;

    const-string v1, "\u094d\u0907"

    const-string v2, "\u093f"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1625178
    iget-object v0, p0, LX/A6w;->a:Ljava/util/Map;

    const-string v1, "\u094d\u0908"

    const-string v2, "\u0940"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1625179
    iget-object v0, p0, LX/A6w;->a:Ljava/util/Map;

    const-string v1, "\u094d\u0909"

    const-string v2, "\u0941"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1625180
    iget-object v0, p0, LX/A6w;->a:Ljava/util/Map;

    const-string v1, "\u094d\u090a"

    const-string v2, "\u0942"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1625181
    iget-object v0, p0, LX/A6w;->a:Ljava/util/Map;

    const-string v1, "\u094d\u090f"

    const-string v2, "\u0947"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1625182
    iget-object v0, p0, LX/A6w;->a:Ljava/util/Map;

    const-string v1, "\u094d\u0910"

    const-string v2, "\u0948"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1625183
    iget-object v0, p0, LX/A6w;->a:Ljava/util/Map;

    const-string v1, "\u094d\u0913"

    const-string v2, "\u094b"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1625184
    iget-object v0, p0, LX/A6w;->a:Ljava/util/Map;

    const-string v1, "\u094d\u0914"

    const-string v2, "\u094c"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1625185
    iget-object v0, p0, LX/A6w;->a:Ljava/util/Map;

    const-string v1, "\u094d\u090b"

    const-string v2, "\u0943"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1625186
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1625164
    const-string v0, "\u094d"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1625165
    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 1625166
    :cond_0
    move-object v0, p1

    .line 1625167
    iget-object v1, p0, LX/A6w;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    .line 1625168
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1625169
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 1625170
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1625171
    :cond_1
    move-object v0, v0

    .line 1625172
    return-object v0
.end method
