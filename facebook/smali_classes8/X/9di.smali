.class public final LX/9di;
.super LX/9dh;
.source ""


# instance fields
.field public final synthetic a:LX/9dl;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public c:Z


# direct methods
.method public constructor <init>(LX/9dl;)V
    .locals 1

    .prologue
    .line 1518478
    iput-object p1, p0, LX/9di;->a:LX/9dl;

    invoke-direct {p0}, LX/9dh;-><init>()V

    .line 1518479
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/9di;->b:Ljava/util/List;

    .line 1518480
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/9di;->c:Z

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 1518481
    iget-object v0, p0, LX/9di;->a:LX/9dl;

    invoke-virtual {v0}, LX/9dl;->getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v0

    .line 1518482
    iget-object v1, v0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    move-object v0, v1

    .line 1518483
    if-nez v0, :cond_1

    .line 1518484
    :cond_0
    :goto_0
    return-void

    .line 1518485
    :cond_1
    iget-object v0, p0, LX/9di;->a:LX/9dl;

    invoke-virtual {v0}, LX/9dl;->getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v0

    .line 1518486
    iget-object v1, v0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    move-object v0, v1

    .line 1518487
    invoke-interface {v0}, LX/362;->c()F

    move-result v0

    .line 1518488
    const/high16 v9, 0x40a00000    # 5.0f

    const/4 v4, 0x0

    const/4 v6, 0x1

    .line 1518489
    iget-object v1, p0, LX/9di;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1518490
    iget-object v1, p0, LX/9di;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v6, :cond_5

    move v2, v4

    move v5, v4

    .line 1518491
    :goto_1
    iget-object v1, p0, LX/9di;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v2, v1, :cond_3

    .line 1518492
    iget-object v1, p0, LX/9di;->b:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v7

    iget-object v1, p0, LX/9di;->b:Ljava/util/List;

    add-int/lit8 v8, v2, 0x1

    invoke-interface {v1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    cmpl-float v1, v7, v1

    if-lez v1, :cond_7

    .line 1518493
    add-int/lit8 v5, v5, 0x1

    .line 1518494
    iget-object v1, p0, LX/9di;->b:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    cmpg-float v1, v1, v9

    if-ltz v1, :cond_2

    iget-object v1, p0, LX/9di;->b:Ljava/util/List;

    add-int/lit8 v7, v2, 0x1

    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    cmpg-float v1, v1, v9

    if-gez v1, :cond_7

    :cond_2
    move v4, v6

    .line 1518495
    :cond_3
    iget-object v1, p0, LX/9di;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    if-ge v5, v1, :cond_4

    if-ne v4, v6, :cond_5

    .line 1518496
    :cond_4
    iput-boolean v6, p0, LX/9di;->c:Z

    .line 1518497
    :cond_5
    iget-boolean v1, p0, LX/9di;->c:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 1518498
    sub-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x40a00000    # 5.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_6

    .line 1518499
    iget-object v0, p0, LX/9di;->a:LX/9dl;

    invoke-virtual {v0}, LX/9dl;->getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->a(F)V

    .line 1518500
    iget-object v0, p0, LX/9di;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1518501
    :cond_6
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/9di;->c:Z

    goto/16 :goto_0

    .line 1518502
    :cond_7
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto/16 :goto_1
.end method

.method public final a(LX/9e2;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1518467
    iget v1, p1, LX/9e2;->c:F

    move v1, v1

    .line 1518468
    float-to-int v1, v1

    .line 1518469
    iget v2, p1, LX/9e2;->d:F

    move v2, v2

    .line 1518470
    float-to-int v2, v2

    .line 1518471
    iget-object v3, p0, LX/9di;->a:LX/9dl;

    invoke-virtual {v3}, LX/9dl;->getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v3

    .line 1518472
    iget-object p1, v3, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    move-object v3, p1

    .line 1518473
    if-nez v3, :cond_0

    .line 1518474
    iget-object v3, p0, LX/9di;->a:LX/9dl;

    invoke-static {v3, v1, v2, v0}, LX/9dl;->a$redex0(LX/9dl;IIZ)Z

    .line 1518475
    :cond_0
    iget-object v1, p0, LX/9di;->a:LX/9dl;

    invoke-virtual {v1}, LX/9dl;->getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v1

    .line 1518476
    iget-object v2, v1, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    move-object v1, v2

    .line 1518477
    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public final b(LX/9e2;)Z
    .locals 3

    .prologue
    .line 1518458
    iget-object v0, p0, LX/9di;->a:LX/9dl;

    invoke-virtual {v0}, LX/9dl;->getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v0

    .line 1518459
    iget-object v1, v0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    move-object v0, v1

    .line 1518460
    if-nez v0, :cond_0

    .line 1518461
    const/4 v0, 0x0

    .line 1518462
    :goto_0
    return v0

    .line 1518463
    :cond_0
    invoke-virtual {p1}, LX/9e2;->c()F

    move-result v1

    .line 1518464
    iget-object v2, p0, LX/9di;->a:LX/9dl;

    invoke-virtual {v2}, LX/9dl;->getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v2

    invoke-interface {v0}, LX/362;->c()F

    move-result v0

    sub-float/2addr v0, v1

    const/high16 v1, 0x43b40000    # 360.0f

    rem-float/2addr v0, v1

    invoke-virtual {v2, v0}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->a(F)V

    .line 1518465
    iget-object v0, p0, LX/9di;->a:LX/9dl;

    invoke-virtual {v0}, LX/9dl;->invalidate()V

    .line 1518466
    const/4 v0, 0x1

    goto :goto_0
.end method
