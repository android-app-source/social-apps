.class public LX/9Ee;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0ad;

.field public b:LX/3oW;

.field public c:Landroid/view/View;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1457239
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1457240
    iput-object p1, p0, LX/9Ee;->a:LX/0ad;

    .line 1457241
    return-void
.end method

.method public static a(LX/0QB;)LX/9Ee;
    .locals 1

    .prologue
    .line 1457274
    invoke-static {p0}, LX/9Ee;->b(LX/0QB;)LX/9Ee;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/9Ee;
    .locals 2

    .prologue
    .line 1457272
    new-instance v1, LX/9Ee;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    invoke-direct {v1, v0}, LX/9Ee;-><init>(LX/0ad;)V

    .line 1457273
    return-object v1
.end method

.method private b()Z
    .locals 3

    .prologue
    .line 1457271
    iget-object v0, p0, LX/9Ee;->a:LX/0ad;

    sget-short v1, LX/0wn;->aP:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1457265
    invoke-direct {p0}, LX/9Ee;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1457266
    iget-object v0, p0, LX/9Ee;->b:LX/3oW;

    if-eqz v0, :cond_0

    .line 1457267
    iget-object v0, p0, LX/9Ee;->b:LX/3oW;

    invoke-virtual {v0}, LX/3oW;->c()V

    .line 1457268
    :cond_0
    :goto_0
    return-void

    .line 1457269
    :cond_1
    iget-object v0, p0, LX/9Ee;->c:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1457270
    iget-object v0, p0, LX/9Ee;->c:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(Landroid/view/View;LX/9Bo;)V
    .locals 7

    .prologue
    .line 1457242
    invoke-direct {p0}, LX/9Ee;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1457243
    const/4 v5, -0x1

    .line 1457244
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1457245
    const v1, 0x7f08125c

    iget-object v2, p0, LX/9Ee;->a:LX/0ad;

    sget v3, LX/0wn;->aN:I

    const/16 v4, 0x1388

    invoke-interface {v2, v3, v4}, LX/0ad;->a(II)I

    move-result v2

    invoke-static {p1, v1, v2}, LX/3oW;->a(Landroid/view/View;II)LX/3oW;

    move-result-object v1

    iput-object v1, p0, LX/9Ee;->b:LX/3oW;

    .line 1457246
    iget-object v1, p0, LX/9Ee;->b:LX/3oW;

    const v2, 0x7f080020

    new-instance v3, LX/9Ec;

    invoke-direct {v3, p0, p2}, LX/9Ec;-><init>(LX/9Ee;LX/9Bo;)V

    invoke-virtual {v1, v2, v3}, LX/3oW;->a(ILandroid/view/View$OnClickListener;)LX/3oW;

    .line 1457247
    iget-object v1, p0, LX/9Ee;->b:LX/3oW;

    .line 1457248
    iget-object v2, v1, LX/3oW;->d:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    move-object v1, v2

    .line 1457249
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    const v3, 0x7f0a00d2

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-direct {v2, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 1457250
    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1457251
    const v0, 0x7f0d0c51

    invoke-static {v1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1457252
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1457253
    const v0, 0x7f0d0b17

    invoke-static {v1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 1457254
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setAllCaps(Z)V

    .line 1457255
    iget-object v0, p0, LX/9Ee;->b:LX/3oW;

    invoke-virtual {v0, v5}, LX/3oW;->a(I)LX/3oW;

    .line 1457256
    iget-object v0, p0, LX/9Ee;->b:LX/3oW;

    invoke-virtual {v0}, LX/3oW;->b()V

    .line 1457257
    :goto_0
    return-void

    .line 1457258
    :cond_0
    const v1, 0x7f0d1192

    invoke-static {p1, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    .line 1457259
    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, LX/9Ee;->c:Landroid/view/View;

    .line 1457260
    iget-object v1, p0, LX/9Ee;->c:Landroid/view/View;

    const v2, 0x7f0d1194

    invoke-static {v1, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 1457261
    iget-object v1, p0, LX/9Ee;->c:Landroid/view/View;

    const v2, 0x7f0d1196

    invoke-static {v1, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v6

    .line 1457262
    iget-object v1, p0, LX/9Ee;->c:Landroid/view/View;

    const v2, 0x7f0d1195

    invoke-static {v1, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v5

    .line 1457263
    new-instance v1, LX/9Ed;

    move-object v2, p0

    move-object v3, p2

    invoke-direct/range {v1 .. v6}, LX/9Ed;-><init>(LX/9Ee;LX/9Bo;Landroid/widget/TextView;Landroid/view/View;Landroid/view/View;)V

    invoke-virtual {v6, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1457264
    goto :goto_0
.end method
