.class public final LX/A9O;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 1634267
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1634268
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1634269
    :goto_0
    return v1

    .line 1634270
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1634271
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1634272
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1634273
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1634274
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1634275
    const-string v4, "amount_spent"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1634276
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1634277
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v5, :cond_b

    .line 1634278
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1634279
    :goto_2
    move v2, v3

    .line 1634280
    goto :goto_1

    .line 1634281
    :cond_2
    const-string v4, "spend_limit"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1634282
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1634283
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v5, :cond_12

    .line 1634284
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1634285
    :goto_3
    move v0, v3

    .line 1634286
    goto :goto_1

    .line 1634287
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1634288
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1634289
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1634290
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 1634291
    :cond_5
    const-string v9, "offset"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 1634292
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v2

    move v6, v2

    move v2, v4

    .line 1634293
    :cond_6
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_9

    .line 1634294
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1634295
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1634296
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_6

    if-eqz v8, :cond_6

    .line 1634297
    const-string v9, "currency"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1634298
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_4

    .line 1634299
    :cond_7
    const-string v9, "offset_amount"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 1634300
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_4

    .line 1634301
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_4

    .line 1634302
    :cond_9
    const/4 v8, 0x3

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1634303
    invoke-virtual {p1, v3, v7}, LX/186;->b(II)V

    .line 1634304
    if-eqz v2, :cond_a

    .line 1634305
    invoke-virtual {p1, v4, v6, v3}, LX/186;->a(III)V

    .line 1634306
    :cond_a
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v5}, LX/186;->b(II)V

    .line 1634307
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_2

    :cond_b
    move v2, v3

    move v5, v3

    move v6, v3

    move v7, v3

    goto :goto_4

    .line 1634308
    :cond_c
    const-string v9, "offset"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_e

    .line 1634309
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v6, v0

    move v0, v4

    .line 1634310
    :cond_d
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_10

    .line 1634311
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1634312
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1634313
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_d

    if-eqz v8, :cond_d

    .line 1634314
    const-string v9, "currency"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_c

    .line 1634315
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_5

    .line 1634316
    :cond_e
    const-string v9, "offset_amount"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_f

    .line 1634317
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_5

    .line 1634318
    :cond_f
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_5

    .line 1634319
    :cond_10
    const/4 v8, 0x3

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1634320
    invoke-virtual {p1, v3, v7}, LX/186;->b(II)V

    .line 1634321
    if-eqz v0, :cond_11

    .line 1634322
    invoke-virtual {p1, v4, v6, v3}, LX/186;->a(III)V

    .line 1634323
    :cond_11
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1634324
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_3

    :cond_12
    move v0, v3

    move v5, v3

    move v6, v3

    move v7, v3

    goto :goto_5
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1634325
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1634326
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1634327
    if-eqz v0, :cond_3

    .line 1634328
    const-string v1, "amount_spent"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634329
    const/4 p3, 0x0

    .line 1634330
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1634331
    invoke-virtual {p0, v0, p3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1634332
    if-eqz v1, :cond_0

    .line 1634333
    const-string v2, "currency"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634334
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1634335
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1, p3}, LX/15i;->a(III)I

    move-result v1

    .line 1634336
    if-eqz v1, :cond_1

    .line 1634337
    const-string v2, "offset"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634338
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 1634339
    :cond_1
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1634340
    if-eqz v1, :cond_2

    .line 1634341
    const-string v2, "offset_amount"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634342
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1634343
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1634344
    :cond_3
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1634345
    if-eqz v0, :cond_7

    .line 1634346
    const-string v1, "spend_limit"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634347
    const/4 p1, 0x0

    .line 1634348
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1634349
    invoke-virtual {p0, v0, p1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1634350
    if-eqz v1, :cond_4

    .line 1634351
    const-string v2, "currency"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634352
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1634353
    :cond_4
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1, p1}, LX/15i;->a(III)I

    move-result v1

    .line 1634354
    if-eqz v1, :cond_5

    .line 1634355
    const-string v2, "offset"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634356
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 1634357
    :cond_5
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1634358
    if-eqz v1, :cond_6

    .line 1634359
    const-string v2, "offset_amount"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634360
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1634361
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1634362
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1634363
    return-void
.end method
