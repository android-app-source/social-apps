.class public LX/9e8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private final a:LX/9e6;

.field private final b:LX/9e6;

.field private c:LX/9e5;

.field private d:F

.field private e:F

.field private f:F


# direct methods
.method public constructor <init>(LX/9e5;LX/9e6;)V
    .locals 1
    .param p2    # LX/9e6;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1519272
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1519273
    new-instance v0, LX/9e7;

    invoke-direct {v0, p0}, LX/9e7;-><init>(LX/9e8;)V

    iput-object v0, p0, LX/9e8;->b:LX/9e6;

    .line 1519274
    iput-object p1, p0, LX/9e8;->c:LX/9e5;

    .line 1519275
    if-nez p2, :cond_0

    iget-object p2, p0, LX/9e8;->b:LX/9e6;

    :cond_0
    iput-object p2, p0, LX/9e8;->a:LX/9e6;

    .line 1519276
    return-void
.end method

.method private static a(FF)F
    .locals 2

    .prologue
    .line 1519277
    add-float v0, p0, p1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    return v0
.end method

.method private static a(FFFF)F
    .locals 6

    .prologue
    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    .line 1519278
    sub-float v0, p0, p2

    float-to-double v0, v0

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    sub-float v2, p1, p3

    float-to-double v2, v2

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method private a(IIILandroid/view/MotionEvent;)V
    .locals 4

    .prologue
    .line 1519279
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 1519280
    invoke-virtual {p4, p2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    iput v0, p0, LX/9e8;->d:F

    .line 1519281
    invoke-virtual {p4, p2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    iput v0, p0, LX/9e8;->e:F

    .line 1519282
    :cond_0
    :goto_0
    return-void

    .line 1519283
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 1519284
    invoke-virtual {p4, p2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    invoke-virtual {p4, p3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    invoke-static {v0, v1}, LX/9e8;->a(FF)F

    move-result v0

    iput v0, p0, LX/9e8;->d:F

    .line 1519285
    invoke-virtual {p4, p2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    invoke-virtual {p4, p3}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    invoke-static {v0, v1}, LX/9e8;->a(FF)F

    move-result v0

    iput v0, p0, LX/9e8;->e:F

    .line 1519286
    invoke-virtual {p4, p2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    invoke-virtual {p4, p2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    invoke-virtual {p4, p3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    invoke-virtual {p4, p3}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    invoke-static {v0, v1, v2, v3}, LX/9e8;->a(FFFF)F

    move-result v0

    iput v0, p0, LX/9e8;->f:F

    goto :goto_0
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1519287
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 1519288
    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    .line 1519289
    :goto_0
    :pswitch_0
    return v1

    .line 1519290
    :pswitch_1
    invoke-direct {p0, v1, v2, v2, p2}, LX/9e8;->a(IIILandroid/view/MotionEvent;)V

    .line 1519291
    iget-object v0, p0, LX/9e8;->a:LX/9e6;

    invoke-interface {v0}, LX/9e6;->a()V

    goto :goto_0

    .line 1519292
    :pswitch_2
    invoke-direct {p0, v3, v2, v1, p2}, LX/9e8;->a(IIILandroid/view/MotionEvent;)V

    .line 1519293
    iget-object v0, p0, LX/9e8;->a:LX/9e6;

    invoke-interface {v0}, LX/9e6;->a()V

    goto :goto_0

    .line 1519294
    :pswitch_3
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    .line 1519295
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v4

    if-ne v4, v3, :cond_1

    .line 1519296
    if-nez v0, :cond_0

    move v0, v1

    :goto_1
    invoke-direct {p0, v1, v0, v2, p2}, LX/9e8;->a(IIILandroid/view/MotionEvent;)V

    .line 1519297
    :goto_2
    iget-object v0, p0, LX/9e8;->a:LX/9e6;

    invoke-interface {v0}, LX/9e6;->a()V

    goto :goto_0

    :cond_0
    move v0, v2

    .line 1519298
    goto :goto_1

    .line 1519299
    :cond_1
    if-nez v0, :cond_2

    move v2, v1

    .line 1519300
    :cond_2
    if-eq v0, v1, :cond_3

    if-ne v2, v1, :cond_4

    :cond_3
    move v0, v3

    .line 1519301
    :goto_3
    invoke-direct {p0, v3, v2, v0, p2}, LX/9e8;->a(IIILandroid/view/MotionEvent;)V

    goto :goto_2

    :cond_4
    move v0, v1

    .line 1519302
    goto :goto_3

    .line 1519303
    :pswitch_4
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    if-ne v0, v1, :cond_6

    invoke-virtual {p2, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    .line 1519304
    :goto_4
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v3

    if-ne v3, v1, :cond_7

    invoke-virtual {p2, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    .line 1519305
    :goto_5
    iget-object v4, p0, LX/9e8;->c:LX/9e5;

    iget v5, p0, LX/9e8;->d:F

    sub-float v5, v0, v5

    iget v6, p0, LX/9e8;->e:F

    sub-float v6, v3, v6

    invoke-virtual {v4, v5, v6}, LX/9e5;->a(FF)V

    .line 1519306
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v4

    if-le v4, v1, :cond_5

    .line 1519307
    invoke-virtual {p2, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v4

    invoke-virtual {p2, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    invoke-virtual {p2, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v5

    invoke-virtual {p2, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v6

    invoke-static {v4, v2, v5, v6}, LX/9e8;->a(FFFF)F

    move-result v2

    .line 1519308
    iget-object v4, p0, LX/9e8;->c:LX/9e5;

    iget v5, p0, LX/9e8;->f:F

    div-float v5, v2, v5

    invoke-virtual {v4, v5, v0, v3}, LX/9e5;->a(FFF)V

    .line 1519309
    iput v2, p0, LX/9e8;->f:F

    .line 1519310
    :cond_5
    iput v0, p0, LX/9e8;->d:F

    .line 1519311
    iput v3, p0, LX/9e8;->e:F

    .line 1519312
    iget-object v0, p0, LX/9e8;->a:LX/9e6;

    invoke-interface {v0}, LX/9e6;->a()V

    goto/16 :goto_0

    .line 1519313
    :cond_6
    invoke-virtual {p2, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    invoke-virtual {p2, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    invoke-static {v0, v3}, LX/9e8;->a(FF)F

    move-result v0

    goto :goto_4

    .line 1519314
    :cond_7
    invoke-virtual {p2, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    invoke-virtual {p2, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    invoke-static {v3, v4}, LX/9e8;->a(FF)F

    move-result v3

    goto :goto_5

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
