.class public final enum LX/8nE;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8nE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8nE;

.field public static final enum CLOSE_GROUP_MEMBERS:LX/8nE;

.field public static final enum COMMENT_AUTHORS:LX/8nE;

.field public static final enum COWORKERS:LX/8nE;

.field public static final enum FRIENDS:LX/8nE;

.field public static final enum GOOD_FRIENDS:LX/8nE;

.field public static final enum GROUPS:LX/8nE;

.field public static final enum MULTI_COMPANY_GROUP_MEMBERS:LX/8nE;

.field public static final enum NOT_NOTIFIED_AND_UNSEEN_OTHERS:LX/8nE;

.field public static final enum NOT_NOTIFIED_OTHERS:LX/8nE;

.field public static final enum OPEN_GROUP_MEMBERS:LX/8nE;

.field public static final enum OTHERS:LX/8nE;

.field public static final enum PRODUCTS:LX/8nE;

.field public static final enum PUBLIC_GROUP_MEMBERS:LX/8nE;

.field public static final enum SECRET_GROUP_MEMBERS:LX/8nE;

.field public static final enum TEXT:LX/8nE;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1400674
    new-instance v0, LX/8nE;

    const-string v1, "COWORKERS"

    invoke-direct {v0, v1, v3}, LX/8nE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8nE;->COWORKERS:LX/8nE;

    .line 1400675
    new-instance v0, LX/8nE;

    const-string v1, "OPEN_GROUP_MEMBERS"

    invoke-direct {v0, v1, v4}, LX/8nE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8nE;->OPEN_GROUP_MEMBERS:LX/8nE;

    .line 1400676
    new-instance v0, LX/8nE;

    const-string v1, "PUBLIC_GROUP_MEMBERS"

    invoke-direct {v0, v1, v5}, LX/8nE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8nE;->PUBLIC_GROUP_MEMBERS:LX/8nE;

    .line 1400677
    new-instance v0, LX/8nE;

    const-string v1, "CLOSE_GROUP_MEMBERS"

    invoke-direct {v0, v1, v6}, LX/8nE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8nE;->CLOSE_GROUP_MEMBERS:LX/8nE;

    .line 1400678
    new-instance v0, LX/8nE;

    const-string v1, "SECRET_GROUP_MEMBERS"

    invoke-direct {v0, v1, v7}, LX/8nE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8nE;->SECRET_GROUP_MEMBERS:LX/8nE;

    .line 1400679
    new-instance v0, LX/8nE;

    const-string v1, "FRIENDS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/8nE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8nE;->FRIENDS:LX/8nE;

    .line 1400680
    new-instance v0, LX/8nE;

    const-string v1, "COMMENT_AUTHORS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/8nE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8nE;->COMMENT_AUTHORS:LX/8nE;

    .line 1400681
    new-instance v0, LX/8nE;

    const-string v1, "NOT_NOTIFIED_OTHERS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/8nE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8nE;->NOT_NOTIFIED_OTHERS:LX/8nE;

    .line 1400682
    new-instance v0, LX/8nE;

    const-string v1, "NOT_NOTIFIED_AND_UNSEEN_OTHERS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/8nE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8nE;->NOT_NOTIFIED_AND_UNSEEN_OTHERS:LX/8nE;

    .line 1400683
    new-instance v0, LX/8nE;

    const-string v1, "OTHERS"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/8nE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8nE;->OTHERS:LX/8nE;

    .line 1400684
    new-instance v0, LX/8nE;

    const-string v1, "GROUPS"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/8nE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8nE;->GROUPS:LX/8nE;

    .line 1400685
    new-instance v0, LX/8nE;

    const-string v1, "GOOD_FRIENDS"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/8nE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8nE;->GOOD_FRIENDS:LX/8nE;

    .line 1400686
    new-instance v0, LX/8nE;

    const-string v1, "TEXT"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/8nE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8nE;->TEXT:LX/8nE;

    .line 1400687
    new-instance v0, LX/8nE;

    const-string v1, "MULTI_COMPANY_GROUP_MEMBERS"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/8nE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8nE;->MULTI_COMPANY_GROUP_MEMBERS:LX/8nE;

    .line 1400688
    new-instance v0, LX/8nE;

    const-string v1, "PRODUCTS"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/8nE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8nE;->PRODUCTS:LX/8nE;

    .line 1400689
    const/16 v0, 0xf

    new-array v0, v0, [LX/8nE;

    sget-object v1, LX/8nE;->COWORKERS:LX/8nE;

    aput-object v1, v0, v3

    sget-object v1, LX/8nE;->OPEN_GROUP_MEMBERS:LX/8nE;

    aput-object v1, v0, v4

    sget-object v1, LX/8nE;->PUBLIC_GROUP_MEMBERS:LX/8nE;

    aput-object v1, v0, v5

    sget-object v1, LX/8nE;->CLOSE_GROUP_MEMBERS:LX/8nE;

    aput-object v1, v0, v6

    sget-object v1, LX/8nE;->SECRET_GROUP_MEMBERS:LX/8nE;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/8nE;->FRIENDS:LX/8nE;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/8nE;->COMMENT_AUTHORS:LX/8nE;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/8nE;->NOT_NOTIFIED_OTHERS:LX/8nE;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/8nE;->NOT_NOTIFIED_AND_UNSEEN_OTHERS:LX/8nE;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/8nE;->OTHERS:LX/8nE;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/8nE;->GROUPS:LX/8nE;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/8nE;->GOOD_FRIENDS:LX/8nE;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/8nE;->TEXT:LX/8nE;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/8nE;->MULTI_COMPANY_GROUP_MEMBERS:LX/8nE;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/8nE;->PRODUCTS:LX/8nE;

    aput-object v2, v0, v1

    sput-object v0, LX/8nE;->$VALUES:[LX/8nE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1400673
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8nE;
    .locals 1

    .prologue
    .line 1400653
    const-class v0, LX/8nE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8nE;

    return-object v0
.end method

.method public static values()[LX/8nE;
    .locals 1

    .prologue
    .line 1400672
    sget-object v0, LX/8nE;->$VALUES:[LX/8nE;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8nE;

    return-object v0
.end method


# virtual methods
.method public final getCustomizedNameResourceId()I
    .locals 2

    .prologue
    .line 1400657
    sget-object v0, LX/8nD;->a:[I

    invoke-virtual {p0}, LX/8nE;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1400658
    const v0, 0x7f0813a8

    :goto_0
    return v0

    .line 1400659
    :pswitch_0
    const v0, 0x7f0813a5

    goto :goto_0

    .line 1400660
    :pswitch_1
    const v0, 0x7f0813ae

    goto :goto_0

    .line 1400661
    :pswitch_2
    const v0, 0x7f0813ad

    goto :goto_0

    .line 1400662
    :pswitch_3
    const v0, 0x7f0813ab

    goto :goto_0

    .line 1400663
    :pswitch_4
    const v0, 0x7f0813aa

    goto :goto_0

    .line 1400664
    :pswitch_5
    const v0, 0x7f0813a6

    goto :goto_0

    .line 1400665
    :pswitch_6
    const v0, 0x7f0813a7

    goto :goto_0

    .line 1400666
    :pswitch_7
    const v0, 0x7f0813b0

    goto :goto_0

    .line 1400667
    :pswitch_8
    const v0, 0x7f0813af

    goto :goto_0

    .line 1400668
    :pswitch_9
    const v0, 0x7f0813a6

    goto :goto_0

    .line 1400669
    :pswitch_a
    const v0, 0x7f0813a9

    goto :goto_0

    .line 1400670
    :pswitch_b
    const v0, 0x7f0813b2

    goto :goto_0

    .line 1400671
    :pswitch_c
    const v0, 0x7f0813ac

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method public final isGroupTagTypeaheadDataType()Z
    .locals 2

    .prologue
    .line 1400654
    sget-object v0, LX/8nD;->a:[I

    invoke-virtual {p0}, LX/8nE;->ordinal()I

    move-result v1

    aget v0, v0, v1

    sparse-switch v0, :sswitch_data_0

    .line 1400655
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1400656
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x3 -> :sswitch_0
        0x4 -> :sswitch_0
        0x5 -> :sswitch_0
        0xd -> :sswitch_0
    .end sparse-switch
.end method
