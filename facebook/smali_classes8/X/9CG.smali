.class public LX/9CG;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/9CG;


# instance fields
.field private final a:LX/0wM;


# direct methods
.method public constructor <init>(LX/0wM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1453754
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1453755
    iput-object p1, p0, LX/9CG;->a:LX/0wM;

    .line 1453756
    return-void
.end method

.method public static a(LX/0QB;)LX/9CG;
    .locals 4

    .prologue
    .line 1453741
    sget-object v0, LX/9CG;->b:LX/9CG;

    if-nez v0, :cond_1

    .line 1453742
    const-class v1, LX/9CG;

    monitor-enter v1

    .line 1453743
    :try_start_0
    sget-object v0, LX/9CG;->b:LX/9CG;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1453744
    if-eqz v2, :cond_0

    .line 1453745
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1453746
    new-instance p0, LX/9CG;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v3

    check-cast v3, LX/0wM;

    invoke-direct {p0, v3}, LX/9CG;-><init>(LX/0wM;)V

    .line 1453747
    move-object v0, p0

    .line 1453748
    sput-object v0, LX/9CG;->b:LX/9CG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1453749
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1453750
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1453751
    :cond_1
    sget-object v0, LX/9CG;->b:LX/9CG;

    return-object v0

    .line 1453752
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1453753
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 1453726
    instance-of v0, p0, LX/9CF;

    if-nez v0, :cond_0

    .line 1453727
    :goto_0
    return-void

    .line 1453728
    :cond_0
    check-cast p0, LX/9CF;

    .line 1453729
    invoke-virtual {p0}, LX/9CF;->a()V

    goto :goto_0
.end method

.method public static a(Landroid/graphics/drawable/Drawable;Landroid/animation/ValueAnimator;)V
    .locals 1

    .prologue
    .line 1453735
    instance-of v0, p0, LX/9CF;

    if-nez v0, :cond_0

    .line 1453736
    :goto_0
    return-void

    .line 1453737
    :cond_0
    check-cast p0, LX/9CF;

    .line 1453738
    invoke-virtual {p0}, LX/9CF;->b()V

    .line 1453739
    iget-object v0, p0, LX/9CF;->c:Landroid/graphics/drawable/Drawable;

    invoke-static {p0, v0, p1}, LX/9CF;->a(LX/9CF;Landroid/graphics/drawable/Drawable;Landroid/animation/ValueAnimator;)V

    .line 1453740
    goto :goto_0
.end method

.method public static b(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 1453731
    instance-of v0, p0, LX/9CF;

    if-nez v0, :cond_0

    .line 1453732
    :goto_0
    return-void

    .line 1453733
    :cond_0
    check-cast p0, LX/9CF;

    .line 1453734
    invoke-virtual {p0}, LX/9CF;->b()V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 1453730
    new-instance v0, LX/9CF;

    iget-object v1, p0, LX/9CG;->a:LX/0wM;

    invoke-direct {v0, p1, v1}, LX/9CF;-><init>(Landroid/content/Context;LX/0wM;)V

    return-object v0
.end method
