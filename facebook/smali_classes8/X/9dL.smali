.class public LX/9dL;
.super LX/9dK;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/8Gc;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1518086
    invoke-direct {p0, p1, p2}, LX/9dK;-><init>(Landroid/content/Context;LX/8Gc;)V

    .line 1518087
    return-void
.end method


# virtual methods
.method public final a(F)F
    .locals 1

    .prologue
    .line 1518085
    const/4 v0, 0x0

    return v0
.end method

.method public final a()Landroid/widget/ImageView;
    .locals 2

    .prologue
    .line 1518082
    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, LX/9dK;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1518083
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1518084
    return-object v0
.end method

.method public final b()Landroid/widget/TextView;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1518073
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()F
    .locals 1

    .prologue
    .line 1518081
    const/4 v0, 0x0

    return v0
.end method

.method public final d()F
    .locals 1

    .prologue
    .line 1518080
    iget-object v0, p0, LX/9dK;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method public final e()LX/9dT;
    .locals 1

    .prologue
    .line 1518079
    sget-object v0, LX/9dT;->COUNTER_CLOCKWISE:LX/9dT;

    return-object v0
.end method

.method public final f()F
    .locals 1

    .prologue
    .line 1518078
    const/4 v0, 0x0

    return v0
.end method

.method public final g()F
    .locals 1

    .prologue
    .line 1518077
    const/4 v0, 0x0

    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 1518076
    const/4 v0, 0x1

    return v0
.end method

.method public final i()J
    .locals 2

    .prologue
    .line 1518075
    const-wide/16 v0, 0x1f4

    return-wide v0
.end method

.method public final j()J
    .locals 2

    .prologue
    .line 1518074
    const-wide/16 v0, 0xfa

    return-wide v0
.end method
