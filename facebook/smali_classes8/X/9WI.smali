.class public final enum LX/9WI;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9WI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9WI;

.field public static final enum ERROR:LX/9WI;

.field public static final enum FLOW_RESPONSES:LX/9WI;

.field public static final enum GUIDED_ACTIONS:LX/9WI;

.field public static final enum MESSAGE_COMPOSER:LX/9WI;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1501032
    new-instance v0, LX/9WI;

    const-string v1, "FLOW_RESPONSES"

    invoke-direct {v0, v1, v2}, LX/9WI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9WI;->FLOW_RESPONSES:LX/9WI;

    .line 1501033
    new-instance v0, LX/9WI;

    const-string v1, "GUIDED_ACTIONS"

    invoke-direct {v0, v1, v3}, LX/9WI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9WI;->GUIDED_ACTIONS:LX/9WI;

    .line 1501034
    new-instance v0, LX/9WI;

    const-string v1, "MESSAGE_COMPOSER"

    invoke-direct {v0, v1, v4}, LX/9WI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9WI;->MESSAGE_COMPOSER:LX/9WI;

    .line 1501035
    new-instance v0, LX/9WI;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v5}, LX/9WI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9WI;->ERROR:LX/9WI;

    .line 1501036
    const/4 v0, 0x4

    new-array v0, v0, [LX/9WI;

    sget-object v1, LX/9WI;->FLOW_RESPONSES:LX/9WI;

    aput-object v1, v0, v2

    sget-object v1, LX/9WI;->GUIDED_ACTIONS:LX/9WI;

    aput-object v1, v0, v3

    sget-object v1, LX/9WI;->MESSAGE_COMPOSER:LX/9WI;

    aput-object v1, v0, v4

    sget-object v1, LX/9WI;->ERROR:LX/9WI;

    aput-object v1, v0, v5

    sput-object v0, LX/9WI;->$VALUES:[LX/9WI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1501038
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9WI;
    .locals 1

    .prologue
    .line 1501039
    const-class v0, LX/9WI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9WI;

    return-object v0
.end method

.method public static values()[LX/9WI;
    .locals 1

    .prologue
    .line 1501037
    sget-object v0, LX/9WI;->$VALUES:[LX/9WI;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9WI;

    return-object v0
.end method
