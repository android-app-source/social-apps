.class public LX/APn;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/APm;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1670445
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1670446
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewStub;Ljava/lang/Object;LX/ARv;LX/Hqz;)LX/APm;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData:",
            "Ljava/lang/Object;",
            ":",
            "LX/0j6;",
            ":",
            "LX/0j3;",
            ":",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesIsPostCompositionOverlayShowing;",
            ":",
            "LX/0j4;",
            ":",
            "LX/0jF;",
            "DerivedData:",
            "Ljava/lang/Object;",
            ":",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesCanSubmit;",
            ":",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesIsPostCompositionViewSupported;",
            "PluginData:",
            "Ljava/lang/Object;",
            ":",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginGetters$ProvidesPluginPublishButtonTextGetter;",
            ":",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginGetters$ProvidesPluginTitleGetter;",
            "Services:",
            "Ljava/lang/Object;",
            ":",
            "LX/0il",
            "<TModelData;>;:",
            "LX/0ik",
            "<TDerivedData;>;:",
            "LX/0in",
            "<TPluginData;>;>(",
            "Landroid/view/ViewStub;",
            "TServices;",
            "Lcom/facebook/composer/controller/ComposerMenuCreator;",
            "Lcom/facebook/composer/controller/ComposerFb4aTitleBarController$Delegate;",
            ")",
            "LX/APm",
            "<TModelData;TDerivedData;TPluginData;TServices;>;"
        }
    .end annotation

    .prologue
    .line 1670447
    new-instance v0, LX/APm;

    move-object v2, p2

    check-cast v2, LX/0il;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static {p0}, LX/APz;->b(LX/0QB;)LX/APz;

    move-result-object v6

    check-cast v6, LX/APz;

    const-class v1, LX/APw;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/APw;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    move-object v1, p1

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v8}, LX/APm;-><init>(Landroid/view/ViewStub;LX/0il;LX/ARv;LX/Hqz;Landroid/content/Context;LX/APz;LX/APw;LX/0ad;)V

    .line 1670448
    return-object v0
.end method
