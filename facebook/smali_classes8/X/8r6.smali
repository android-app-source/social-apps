.class public final LX/8r6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        "Lcom/facebook/graphql/model/GraphQLComment;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/5Gr;

.field public final synthetic b:Z

.field public final synthetic c:LX/3iU;


# direct methods
.method public constructor <init>(LX/3iU;LX/5Gr;Z)V
    .locals 0

    .prologue
    .line 1408440
    iput-object p1, p0, LX/8r6;->c:LX/3iU;

    iput-object p2, p0, LX/8r6;->a:LX/5Gr;

    iput-boolean p3, p0, LX/8r6;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3

    .prologue
    .line 1408441
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 1408442
    iget-object v0, p1, Lcom/facebook/fbservice/service/OperationResult;->resultDataString:Ljava/lang/String;

    move-object v0, v0

    .line 1408443
    iget-object v1, p0, LX/8r6;->a:LX/5Gr;

    .line 1408444
    iput-object v0, v1, LX/5Gr;->g:Ljava/lang/String;

    .line 1408445
    iget-object v0, p0, LX/8r6;->c:LX/3iU;

    iget-object v0, v0, LX/3iU;->d:LX/3iV;

    iget-object v1, p0, LX/8r6;->a:LX/5Gr;

    invoke-virtual {v1}, LX/5Gr;->a()Lcom/facebook/api/ufiservices/common/AddCommentParams;

    move-result-object v1

    iget-boolean v2, p0, LX/8r6;->b:Z

    invoke-virtual {v0, v1, v2}, LX/3iV;->a(Lcom/facebook/api/ufiservices/common/AddCommentParams;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
