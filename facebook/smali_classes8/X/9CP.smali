.class public LX/9CP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/21l;
.implements LX/8ky;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/21l",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        ">;",
        "Lcom/facebook/feedback/ui/rows/views/DimmableView$Listener;",
        "LX/8ky;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public A:LX/9Ce;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:LX/9DF;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Lcom/facebook/ipc/media/MediaItem;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Lcom/facebook/ipc/media/MediaItem;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:LX/9CN;

.field public F:Z

.field public G:Z

.field public H:Z

.field public I:Z

.field public b:LX/3iG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8pb;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9Dp;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tF;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/9FG;

.field private final h:Z

.field public final i:Landroid/graphics/Rect;

.field public j:LX/0g8;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:LX/21o;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:LX/21p;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/stickers/keyboard/StickerKeyboardView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Landroid/view/View$OnFocusChangeListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:LX/4nG;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:LX/4nF;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:LX/9D1;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:LX/9FA;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1453960
    const-class v0, LX/9CP;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/9CP;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/9FA;LX/9D1;LX/9Ce;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;ZLX/9FH;)V
    .locals 4
    .param p1    # LX/9FA;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/9D1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/9Ce;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1453961
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1453962
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1453963
    iput-object v0, p0, LX/9CP;->c:LX/0Ot;

    .line 1453964
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1453965
    iput-object v0, p0, LX/9CP;->d:LX/0Ot;

    .line 1453966
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1453967
    iput-object v0, p0, LX/9CP;->e:LX/0Ot;

    .line 1453968
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1453969
    iput-object v0, p0, LX/9CP;->f:LX/0Ot;

    .line 1453970
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/9CP;->i:Landroid/graphics/Rect;

    .line 1453971
    sget-object v0, LX/9CN;->INACTIVE:LX/9CN;

    iput-object v0, p0, LX/9CP;->E:LX/9CN;

    .line 1453972
    iput-object p2, p0, LX/9CP;->y:LX/9D1;

    .line 1453973
    iput-object p1, p0, LX/9CP;->z:LX/9FA;

    .line 1453974
    iput-object p3, p0, LX/9CP;->A:LX/9Ce;

    .line 1453975
    iput-object p4, p0, LX/9CP;->m:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1453976
    iput-boolean p5, p0, LX/9CP;->h:Z

    .line 1453977
    iget-object v0, p0, LX/9CP;->m:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1453978
    iget-wide v2, v0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->j:J

    move-wide v0, v2

    .line 1453979
    invoke-virtual {p6, v0, v1}, LX/9FH;->a(J)LX/9FG;

    move-result-object v0

    iput-object v0, p0, LX/9CP;->g:LX/9FG;

    .line 1453980
    return-void
.end method

.method private A()V
    .locals 1

    .prologue
    .line 1453981
    iget-object v0, p0, LX/9CP;->n:LX/21o;

    if-eqz v0, :cond_0

    .line 1453982
    iget-object v0, p0, LX/9CP;->n:LX/21o;

    invoke-interface {v0}, LX/21o;->b()V

    .line 1453983
    :cond_0
    return-void
.end method

.method public static B(LX/9CP;)V
    .locals 2

    .prologue
    .line 1453949
    iget-object v0, p0, LX/9CP;->w:LX/4nG;

    if-eqz v0, :cond_0

    .line 1453950
    iget-object v0, p0, LX/9CP;->w:LX/4nG;

    iget-object v1, p0, LX/9CP;->x:LX/4nF;

    invoke-virtual {v0, v1}, LX/4nG;->b(LX/4nF;)V

    .line 1453951
    const/4 v0, 0x0

    iput-object v0, p0, LX/9CP;->w:LX/4nG;

    .line 1453952
    :cond_0
    return-void
.end method

.method public static C(LX/9CP;)V
    .locals 2

    .prologue
    .line 1453984
    iget-object v0, p0, LX/9CP;->n:LX/21o;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9CP;->C:Lcom/facebook/ipc/media/MediaItem;

    if-nez v0, :cond_1

    .line 1453985
    :cond_0
    :goto_0
    return-void

    .line 1453986
    :cond_1
    iget-object v0, p0, LX/9CP;->g:LX/9FG;

    invoke-static {p0}, LX/9CP;->T(LX/9CP;)LX/9FF;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9FG;->b(LX/9FF;)V

    .line 1453987
    iget-object v0, p0, LX/9CP;->n:LX/21o;

    iget-object v1, p0, LX/9CP;->C:Lcom/facebook/ipc/media/MediaItem;

    invoke-interface {v0, v1}, LX/21o;->setMediaItem(Lcom/facebook/ipc/media/MediaItem;)V

    .line 1453988
    const/4 v0, 0x0

    iput-object v0, p0, LX/9CP;->C:Lcom/facebook/ipc/media/MediaItem;

    goto :goto_0
.end method

.method public static D(LX/9CP;)V
    .locals 2

    .prologue
    .line 1453989
    iget-object v0, p0, LX/9CP;->D:Lcom/facebook/ipc/media/MediaItem;

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/9CP;->O(LX/9CP;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1453990
    :cond_0
    :goto_0
    return-void

    .line 1453991
    :cond_1
    iget-object v0, p0, LX/9CP;->g:LX/9FG;

    invoke-static {p0}, LX/9CP;->T(LX/9CP;)LX/9FF;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9FG;->b(LX/9FF;)V

    .line 1453992
    iget-object v0, p0, LX/9CP;->n:LX/21o;

    iget-object v1, p0, LX/9CP;->D:Lcom/facebook/ipc/media/MediaItem;

    invoke-interface {v0, v1}, LX/21o;->setMediaItem(Lcom/facebook/ipc/media/MediaItem;)V

    .line 1453993
    const/4 v0, 0x0

    iput-object v0, p0, LX/9CP;->D:Lcom/facebook/ipc/media/MediaItem;

    goto :goto_0
.end method

.method public static E(LX/9CP;)V
    .locals 1

    .prologue
    .line 1453994
    iget-object v0, p0, LX/9CP;->j:LX/0g8;

    if-nez v0, :cond_0

    .line 1453995
    :goto_0
    return-void

    .line 1453996
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/9CP;->j:LX/0g8;

    goto :goto_0
.end method

.method public static F(LX/9CP;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1453997
    iget-object v0, p0, LX/9CP;->n:LX/21o;

    if-nez v0, :cond_0

    .line 1453998
    :goto_0
    return-void

    .line 1453999
    :cond_0
    iget-object v0, p0, LX/9CP;->n:LX/21o;

    invoke-interface {v0, v1}, LX/21o;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1454000
    iget-object v0, p0, LX/9CP;->n:LX/21o;

    invoke-interface {v0, v1}, LX/21o;->setMediaPickerListener(LX/9DF;)V

    .line 1454001
    iput-object v1, p0, LX/9CP;->n:LX/21o;

    goto :goto_0
.end method

.method public static G(LX/9CP;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1454002
    iget-object v0, p0, LX/9CP;->t:Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;

    if-nez v0, :cond_0

    .line 1454003
    :goto_0
    return-void

    .line 1454004
    :cond_0
    iget-object v0, p0, LX/9CP;->t:Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;

    iget-object v1, p0, LX/9CP;->u:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    invoke-virtual {v0, v1}, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->removeView(Landroid/view/View;)V

    .line 1454005
    iget-object v0, p0, LX/9CP;->t:Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;

    .line 1454006
    iput-object v2, v0, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->h:LX/6Lk;

    .line 1454007
    iput-object v2, p0, LX/9CP;->t:Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;

    goto :goto_0
.end method

.method private H()Z
    .locals 1

    .prologue
    .line 1454008
    iget-object v0, p0, LX/9CP;->z:LX/9FA;

    if-nez v0, :cond_0

    .line 1454009
    const/4 v0, 0x0

    .line 1454010
    :goto_0
    return v0

    .line 1454011
    :cond_0
    iget-object v0, p0, LX/9CP;->z:LX/9FA;

    invoke-interface {v0}, LX/1Pq;->iN_()V

    .line 1454012
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static J(LX/9CP;)Z
    .locals 1

    .prologue
    .line 1454013
    iget-object v0, p0, LX/9CP;->n:LX/21o;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9CP;->n:LX/21o;

    invoke-interface {v0}, LX/21o;->getPendingComment()Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->a(Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static L(LX/9CP;)V
    .locals 7

    .prologue
    .line 1454057
    iget-object v0, p0, LX/9CP;->l:Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9CP;->n:LX/21o;

    if-eqz v0, :cond_0

    invoke-direct {p0}, LX/9CP;->N()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1454058
    :cond_0
    :goto_0
    return-void

    .line 1454059
    :cond_1
    iget-object v0, p0, LX/9CP;->l:Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;

    iget-object v0, v0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1454060
    iget-object v0, p0, LX/9CP;->g:LX/9FG;

    .line 1454061
    iget-object v2, v0, LX/9FG;->a:LX/0if;

    sget-object v3, LX/0ig;->j:LX/0ih;

    iget-wide v4, v0, LX/9FG;->b:J

    const-string v6, "draft_comment_text_restored"

    invoke-virtual {v2, v3, v4, v5, v6}, LX/0if;->a(LX/0ih;JLjava/lang/String;)V

    .line 1454062
    :cond_2
    iget-object v0, p0, LX/9CP;->l:Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;

    iget-object v0, v0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->f:Lcom/facebook/ipc/media/MediaItem;

    if-eqz v0, :cond_3

    .line 1454063
    iget-object v0, p0, LX/9CP;->g:LX/9FG;

    .line 1454064
    iget-object v2, v0, LX/9FG;->a:LX/0if;

    sget-object v3, LX/0ig;->j:LX/0ih;

    iget-wide v4, v0, LX/9FG;->b:J

    const-string v6, "draft_comment_photo_restored"

    invoke-virtual {v2, v3, v4, v5, v6}, LX/0if;->a(LX/0ih;JLjava/lang/String;)V

    .line 1454065
    :cond_3
    iget-object v0, p0, LX/9CP;->n:LX/21o;

    iget-object v1, p0, LX/9CP;->l:Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;

    invoke-interface {v0, v1}, LX/21o;->a(Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;)V

    .line 1454066
    const/4 v0, 0x0

    iput-object v0, p0, LX/9CP;->l:Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;

    goto :goto_0
.end method

.method private N()Z
    .locals 2

    .prologue
    .line 1454014
    iget-object v0, p0, LX/9CP;->E:LX/9CN;

    sget-object v1, LX/9CN;->TOP_LEVEL:LX/9CN;

    invoke-virtual {v0, v1}, LX/9CN;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static O(LX/9CP;)Z
    .locals 2

    .prologue
    .line 1454015
    iget-object v0, p0, LX/9CP;->E:LX/9CN;

    sget-object v1, LX/9CN;->REPLY_STICKY:LX/9CN;

    invoke-virtual {v0, v1}, LX/9CN;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static P(LX/9CP;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1454016
    sget-object v2, LX/9CM;->b:[I

    iget-object v3, p0, LX/9CP;->E:LX/9CN;

    invoke-virtual {v3}, LX/9CN;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1454017
    invoke-direct {p0}, LX/9CP;->S()V

    .line 1454018
    :goto_0
    return-void

    .line 1454019
    :pswitch_0
    iget-object v2, p0, LX/9CP;->n:LX/21o;

    if-eqz v2, :cond_0

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1454020
    invoke-direct {p0}, LX/9CP;->R()V

    goto :goto_0

    :cond_0
    move v0, v1

    .line 1454021
    goto :goto_1

    .line 1454022
    :pswitch_1
    iget-object v2, p0, LX/9CP;->n:LX/21o;

    if-eqz v2, :cond_1

    :goto_2
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1454023
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1454024
    iget-object v0, p0, LX/9CP;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_2

    move v0, v1

    :goto_3
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1454025
    iget-object v0, p0, LX/9CP;->q:Ljava/lang/String;

    if-eqz v0, :cond_3

    move v0, v1

    :goto_4
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1454026
    iget-object v0, p0, LX/9CP;->r:Ljava/lang/String;

    if-eqz v0, :cond_4

    move v0, v1

    :goto_5
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1454027
    iget-object v0, p0, LX/9CP;->p:Ljava/lang/String;

    if-eqz v0, :cond_5

    move v0, v1

    :goto_6
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1454028
    iget-object v0, p0, LX/9CP;->A:LX/9Ce;

    if-eqz v0, :cond_6

    :goto_7
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1454029
    goto :goto_0

    :cond_1
    move v0, v1

    .line 1454030
    goto :goto_2

    .line 1454031
    :pswitch_2
    invoke-direct {p0}, LX/9CP;->R()V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 1454032
    goto :goto_3

    :cond_3
    move v0, v2

    .line 1454033
    goto :goto_4

    :cond_4
    move v0, v2

    .line 1454034
    goto :goto_5

    :cond_5
    move v0, v2

    .line 1454035
    goto :goto_6

    :cond_6
    move v1, v2

    .line 1454036
    goto :goto_7

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private R()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1454037
    iget-object v0, p0, LX/9CP;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1454038
    iget-object v0, p0, LX/9CP;->q:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1454039
    iget-object v0, p0, LX/9CP;->r:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1454040
    iget-object v0, p0, LX/9CP;->p:Ljava/lang/String;

    if-nez v0, :cond_3

    :goto_3
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1454041
    return-void

    :cond_0
    move v0, v2

    .line 1454042
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1454043
    goto :goto_1

    :cond_2
    move v0, v2

    .line 1454044
    goto :goto_2

    :cond_3
    move v1, v2

    .line 1454045
    goto :goto_3
.end method

.method private S()V
    .locals 3

    .prologue
    .line 1454046
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized enum value"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/9CP;->E:LX/9CN;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static T(LX/9CP;)LX/9FF;
    .locals 1

    .prologue
    .line 1454047
    iget-boolean v0, p0, LX/9CP;->h:Z

    if-nez v0, :cond_0

    invoke-static {p0}, LX/9CP;->O(LX/9CP;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, LX/9FF;->REPLY:LX/9FF;

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, LX/9FF;->TOP_LEVEL:LX/9FF;

    goto :goto_0
.end method

.method public static a(Landroid/view/View;Z)LX/4nG;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1454049
    new-instance v0, LX/4nG;

    invoke-direct {v0, p0, p1}, LX/4nG;-><init>(Landroid/view/View;Z)V

    return-object v0
.end method

.method private b(LX/21p;)V
    .locals 1
    .param p1    # LX/21p;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1454050
    iget-object v0, p0, LX/9CP;->s:LX/21p;

    .line 1454051
    iput-object p1, p0, LX/9CP;->s:LX/21p;

    .line 1454052
    if-eqz v0, :cond_0

    .line 1454053
    invoke-interface {v0}, LX/21p;->e()V

    .line 1454054
    :cond_0
    iget-object v0, p0, LX/9CP;->s:LX/21p;

    if-eqz v0, :cond_1

    .line 1454055
    iget-object v0, p0, LX/9CP;->s:LX/21p;

    invoke-interface {v0}, LX/21p;->d()V

    .line 1454056
    :cond_1
    return-void
.end method

.method public static b(LX/9CP;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1453953
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    .line 1453954
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/9CP;->q:Ljava/lang/String;

    .line 1453955
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v1

    :cond_0
    iput-object v1, p0, LX/9CP;->r:Ljava/lang/String;

    .line 1453956
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/9CP;->p:Ljava/lang/String;

    .line 1453957
    invoke-virtual {p1, p2}, Lcom/facebook/graphql/model/GraphQLComment;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, LX/9CP;->H:Z

    .line 1453958
    return-void

    :cond_1
    move-object v0, v1

    .line 1453959
    goto :goto_0
.end method

.method public static d()V
    .locals 1

    .prologue
    .line 1454048
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public static e()V
    .locals 1

    .prologue
    .line 1453811
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public static q(LX/9CP;)V
    .locals 4

    .prologue
    .line 1453812
    invoke-static {p0}, LX/9CP;->O(LX/9CP;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1453813
    :goto_0
    return-void

    .line 1453814
    :cond_0
    iget-object v0, p0, LX/9CP;->n:LX/21o;

    iget-object v1, p0, LX/9CP;->q:Ljava/lang/String;

    iget-object v2, p0, LX/9CP;->r:Ljava/lang/String;

    iget-boolean v3, p0, LX/9CP;->H:Z

    invoke-interface {v0, v1, v2, v3}, LX/21o;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public static s(LX/9CP;)Z
    .locals 1

    .prologue
    .line 1453815
    iget-object v0, p0, LX/9CP;->u:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9CP;->u:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    invoke-virtual {v0}, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static t(LX/9CP;)V
    .locals 0

    .prologue
    .line 1453816
    invoke-direct {p0}, LX/9CP;->y()V

    .line 1453817
    invoke-static {p0}, LX/9CP;->u(LX/9CP;)Z

    .line 1453818
    return-void
.end method

.method public static u(LX/9CP;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 1453819
    invoke-static {p0}, LX/9CP;->O(LX/9CP;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1453820
    const/4 v0, 0x0

    .line 1453821
    :goto_0
    return v0

    .line 1453822
    :cond_0
    iget-object v1, p0, LX/9CP;->n:LX/21o;

    invoke-interface {v1}, LX/21o;->c()V

    .line 1453823
    iput-object v2, p0, LX/9CP;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1453824
    iput-object v2, p0, LX/9CP;->r:Ljava/lang/String;

    .line 1453825
    iput-object v2, p0, LX/9CP;->q:Ljava/lang/String;

    .line 1453826
    iput-object v2, p0, LX/9CP;->p:Ljava/lang/String;

    .line 1453827
    sget-object v1, LX/9CN;->TOP_LEVEL:LX/9CN;

    iput-object v1, p0, LX/9CP;->E:LX/9CN;

    .line 1453828
    iget-object v1, p0, LX/9CP;->n:LX/21o;

    invoke-interface {v1, v0}, LX/21o;->setIsVisible(Z)V

    .line 1453829
    invoke-static {p0}, LX/9CP;->P(LX/9CP;)V

    goto :goto_0
.end method

.method public static v(LX/9CP;)V
    .locals 4

    .prologue
    .line 1453830
    iget-object v0, p0, LX/9CP;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/9CP;->J(LX/9CP;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/9CP;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8pb;

    iget-object v1, p0, LX/9CP;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8pb;->a(Ljava/lang/String;)Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;

    move-result-object v0

    :goto_0
    move-object v0, v0

    .line 1453831
    iget-object v1, p0, LX/9CP;->n:LX/21o;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/21o;->setIsVisible(Z)V

    .line 1453832
    iget-object v1, p0, LX/9CP;->n:LX/21o;

    iget-object v2, p0, LX/9CP;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {p0}, LX/9CP;->s(LX/9CP;)Z

    move-result v3

    invoke-interface {v1, v2, v3}, LX/21o;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Z)V

    .line 1453833
    sget-object v1, LX/9CN;->REPLY_STICKY:LX/9CN;

    iput-object v1, p0, LX/9CP;->E:LX/9CN;

    .line 1453834
    invoke-static {p0}, LX/9CP;->P(LX/9CP;)V

    .line 1453835
    invoke-static {p0}, LX/9CP;->q(LX/9CP;)V

    .line 1453836
    iget-object v1, p0, LX/9CP;->n:LX/21o;

    invoke-interface {v1, v0}, LX/21o;->a(Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;)V

    .line 1453837
    invoke-static {p0}, LX/9CP;->D(LX/9CP;)V

    .line 1453838
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static w(LX/9CP;)V
    .locals 4

    .prologue
    .line 1453839
    iget-object v0, p0, LX/9CP;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tF;

    const/4 v1, 0x0

    .line 1453840
    invoke-virtual {v0}, LX/0tF;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, LX/0tF;->a:LX/0ad;

    sget-short v3, LX/0wn;->V:S

    invoke-interface {v2, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    move v0, v1

    .line 1453841
    if-eqz v0, :cond_1

    invoke-static {p0}, LX/9CP;->J(LX/9CP;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1453842
    :cond_1
    invoke-direct {p0}, LX/9CP;->y()V

    .line 1453843
    :goto_0
    return-void

    .line 1453844
    :cond_2
    invoke-static {p0}, LX/9CP;->t(LX/9CP;)V

    goto :goto_0
.end method

.method public static x(LX/9CP;)V
    .locals 5

    .prologue
    .line 1453845
    iget-object v0, p0, LX/9CP;->A:LX/9Ce;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9CP;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9CP;->A:LX/9Ce;

    iget-object v1, p0, LX/9CP;->p:Ljava/lang/String;

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1453846
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    move v2, v3

    :goto_0
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 1453847
    iget-object v2, v0, LX/9Ce;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1453848
    :goto_1
    move v0, v4

    .line 1453849
    if-eqz v0, :cond_0

    .line 1453850
    invoke-direct {p0}, LX/9CP;->H()Z

    .line 1453851
    :cond_0
    return-void

    :cond_1
    move v2, v4

    .line 1453852
    goto :goto_0

    .line 1453853
    :cond_2
    iput-object v1, v0, LX/9Ce;->a:Ljava/lang/String;

    move v4, v3

    .line 1453854
    goto :goto_1
.end method

.method private y()V
    .locals 3

    .prologue
    .line 1453855
    iget-object v0, p0, LX/9CP;->A:LX/9Ce;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9CP;->A:LX/9Ce;

    .line 1453856
    iget-object v1, v0, LX/9Ce;->a:Ljava/lang/String;

    .line 1453857
    const/4 v2, 0x0

    iput-object v2, v0, LX/9Ce;->a:Ljava/lang/String;

    .line 1453858
    move-object v0, v1

    .line 1453859
    if-eqz v0, :cond_0

    .line 1453860
    invoke-direct {p0}, LX/9CP;->H()Z

    .line 1453861
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1453862
    invoke-virtual {p0}, LX/9CP;->j()V

    .line 1453863
    return-void
.end method

.method public final a(Landroid/content/Context;Landroid/view/View;LX/21p;)V
    .locals 8

    .prologue
    .line 1453864
    iget-object v0, p0, LX/9CP;->t:Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 1453865
    :cond_0
    :goto_0
    return-void

    .line 1453866
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/9CP;->G:Z

    .line 1453867
    invoke-static {p1, p2}, LX/2Na;->a(Landroid/content/Context;Landroid/view/View;)V

    .line 1453868
    invoke-static {p0}, LX/9CP;->x(LX/9CP;)V

    .line 1453869
    iget-object v0, p0, LX/9CP;->u:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    if-nez v0, :cond_2

    .line 1453870
    new-instance v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    invoke-direct {v0, p1}, Lcom/facebook/stickers/keyboard/StickerKeyboardView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/9CP;->u:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    .line 1453871
    iget-object v0, p0, LX/9CP;->u:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    sget-object v1, LX/4m4;->COMMENTS:LX/4m4;

    invoke-virtual {v0, v1}, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->setInterface(LX/4m4;)V

    .line 1453872
    iget-object v0, p0, LX/9CP;->u:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    .line 1453873
    iput-object p0, v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->f:LX/8ky;

    .line 1453874
    iget-object v0, p0, LX/9CP;->t:Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;

    iget-object v1, p0, LX/9CP;->u:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    invoke-virtual {v0, v1}, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->addView(Landroid/view/View;)V

    .line 1453875
    iget-object v0, p0, LX/9CP;->t:Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;

    invoke-virtual {v0}, Lcom/facebook/common/ui/keyboard/CustomKeyboardLayout;->bringToFront()V

    .line 1453876
    :cond_2
    iget-object v0, p0, LX/9CP;->u:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    invoke-virtual {v0}, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->isShown()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1453877
    iget-object v0, p0, LX/9CP;->g:LX/9FG;

    invoke-static {p0}, LX/9CP;->T(LX/9CP;)LX/9FF;

    move-result-object v1

    .line 1453878
    iget-object v2, v0, LX/9FG;->a:LX/0if;

    sget-object v3, LX/0ig;->j:LX/0ih;

    iget-wide v4, v0, LX/9FG;->b:J

    const-string v6, "sticker_keyboard_shown"

    invoke-virtual {v1}, LX/9FF;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v2 .. v7}, LX/0if;->a(LX/0ih;JLjava/lang/String;Ljava/lang/String;)V

    .line 1453879
    :cond_3
    iget-object v0, p0, LX/9CP;->u:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->setVisibility(I)V

    .line 1453880
    iget-object v0, p0, LX/9CP;->u:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    invoke-virtual {v0}, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->requestFocus()Z

    .line 1453881
    invoke-direct {p0, p3}, LX/9CP;->b(LX/21p;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V
    .locals 6

    .prologue
    .line 1453882
    iget-object v0, p0, LX/9CP;->m:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    if-eqz v0, :cond_0

    .line 1453883
    iget-wide v4, p1, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->j:J

    move-wide v0, v4

    .line 1453884
    iget-object v2, p0, LX/9CP;->m:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1453885
    iget-wide v4, v2, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->j:J

    move-wide v2, v4

    .line 1453886
    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "new feedback logging params must have the same comments funnel logger instance id"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1453887
    iput-object p1, p0, LX/9CP;->m:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1453888
    return-void

    .line 1453889
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/stickers/model/Sticker;)V
    .locals 1

    .prologue
    .line 1453890
    iget-object v0, p0, LX/9CP;->s:LX/21p;

    if-nez v0, :cond_0

    .line 1453891
    :goto_0
    return-void

    .line 1453892
    :cond_0
    iget-object v0, p0, LX/9CP;->s:LX/21p;

    invoke-interface {v0, p1}, LX/21p;->a(Lcom/facebook/stickers/model/Sticker;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;)V
    .locals 8

    .prologue
    .line 1453893
    iget-object v0, p0, LX/9CP;->g:LX/9FG;

    invoke-static {p0}, LX/9CP;->T(LX/9CP;)LX/9FF;

    move-result-object v1

    .line 1453894
    iget-object v2, v0, LX/9FG;->a:LX/0if;

    sget-object v3, LX/0ig;->j:LX/0ih;

    iget-wide v4, v0, LX/9FG;->b:J

    invoke-static {p1}, LX/9FI;->a(Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1}, LX/9FF;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v2 .. v7}, LX/0if;->a(LX/0ih;JLjava/lang/String;Ljava/lang/String;)V

    .line 1453895
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/9CP;->F:Z

    .line 1453896
    iget-object v0, p0, LX/9CP;->n:LX/21o;

    if-eqz v0, :cond_0

    .line 1453897
    iget-object v0, p0, LX/9CP;->n:LX/21o;

    invoke-interface {v0}, LX/21o;->a()V

    .line 1453898
    :cond_0
    invoke-direct {p0}, LX/9CP;->A()V

    .line 1453899
    invoke-direct {p0}, LX/9CP;->N()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1453900
    iget-object v0, p0, LX/9CP;->b:LX/3iG;

    sget-object v1, Lcom/facebook/feedback/ui/FeedbackControllerParams;->a:Lcom/facebook/feedback/ui/FeedbackControllerParams;

    invoke-virtual {v0, v1}, LX/3iG;->a(Lcom/facebook/feedback/ui/FeedbackControllerParams;)LX/3iK;

    move-result-object v0

    iget-object v1, p0, LX/9CP;->k:Lcom/facebook/graphql/model/GraphQLFeedback;

    iget-object v2, p0, LX/9CP;->m:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-virtual {v0, p1, v1, v2}, LX/3iK;->a(Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V

    .line 1453901
    :cond_1
    :goto_0
    return-void

    .line 1453902
    :cond_2
    invoke-static {p0}, LX/9CP;->O(LX/9CP;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1453903
    iget-object v0, p0, LX/9CP;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1453904
    iget-object v1, p1, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->g:Lcom/facebook/ipc/media/StickerItem;

    if-nez v1, :cond_3

    .line 1453905
    invoke-static {p0}, LX/9CP;->t(LX/9CP;)V

    .line 1453906
    :cond_3
    iget-object v1, p0, LX/9CP;->b:LX/3iG;

    sget-object v2, Lcom/facebook/feedback/ui/FeedbackControllerParams;->a:Lcom/facebook/feedback/ui/FeedbackControllerParams;

    invoke-virtual {v1, v2}, LX/3iG;->a(Lcom/facebook/feedback/ui/FeedbackControllerParams;)LX/3iK;

    move-result-object v1

    iget-object v2, p0, LX/9CP;->m:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-virtual {v1, p1, v0, v2}, LX/3iK;->a(Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V

    .line 1453907
    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1453908
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1453909
    iput-object p1, p0, LX/9CP;->k:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1453910
    iget-object v0, p0, LX/9CP;->k:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-nez v0, :cond_1

    .line 1453911
    :cond_0
    :goto_0
    invoke-static {p0}, LX/9CP;->L(LX/9CP;)V

    .line 1453912
    return-void

    .line 1453913
    :cond_1
    iget-object v0, p0, LX/9CP;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8pb;

    iget-object p1, p0, LX/9CP;->k:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, LX/8pb;->a(Ljava/lang/String;)Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;

    move-result-object v0

    .line 1453914
    invoke-static {v0}, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->a(Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 1453915
    iput-object v0, p0, LX/9CP;->l:Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;

    goto :goto_0
.end method

.method public final a(LX/21p;)Z
    .locals 1

    .prologue
    .line 1453916
    iget-object v0, p0, LX/9CP;->s:LX/21p;

    if-ne v0, p1, :cond_0

    invoke-static {p0}, LX/9CP;->s(LX/9CP;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 6

    .prologue
    .line 1453917
    iget-object v0, p0, LX/9CP;->g:LX/9FG;

    .line 1453918
    iget-object v1, v0, LX/9FG;->a:LX/0if;

    sget-object v2, LX/0ig;->j:LX/0ih;

    iget-wide v3, v0, LX/9FG;->b:J

    const-string v5, "dimmed_view_clicked"

    invoke-virtual {v1, v2, v3, v4, v5}, LX/0if;->b(LX/0ih;JLjava/lang/String;)V

    .line 1453919
    invoke-static {p0}, LX/9CP;->w(LX/9CP;)V

    .line 1453920
    invoke-static {p0}, LX/9CP;->s(LX/9CP;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1453921
    invoke-virtual {p0}, LX/9CP;->k()V

    .line 1453922
    :goto_0
    return-void

    .line 1453923
    :cond_0
    invoke-direct {p0}, LX/9CP;->A()V

    goto :goto_0
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 1453924
    iget-object v0, p0, LX/9CP;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9CP;->y:LX/9D1;

    if-nez v0, :cond_1

    .line 1453925
    :cond_0
    :goto_0
    return-void

    .line 1453926
    :cond_1
    iget-object v0, p0, LX/9CP;->y:LX/9D1;

    iget-object v1, p0, LX/9CP;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/9D1;->a(Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public final j()V
    .locals 0

    .prologue
    .line 1453927
    invoke-virtual {p0}, LX/9CP;->k()V

    .line 1453928
    invoke-static {p0}, LX/9CP;->w(LX/9CP;)V

    .line 1453929
    return-void
.end method

.method public final k()V
    .locals 8

    .prologue
    .line 1453930
    iget-object v0, p0, LX/9CP;->u:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    if-nez v0, :cond_0

    .line 1453931
    :goto_0
    return-void

    .line 1453932
    :cond_0
    iget-object v0, p0, LX/9CP;->u:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    invoke-virtual {v0}, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1453933
    iget-object v0, p0, LX/9CP;->g:LX/9FG;

    invoke-static {p0}, LX/9CP;->T(LX/9CP;)LX/9FF;

    move-result-object v1

    .line 1453934
    iget-object v2, v0, LX/9FG;->a:LX/0if;

    sget-object v3, LX/0ig;->j:LX/0ih;

    iget-wide v4, v0, LX/9FG;->b:J

    const-string v6, "sticker_keyboard_hidden"

    invoke-virtual {v1}, LX/9FF;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v2 .. v7}, LX/0if;->a(LX/0ih;JLjava/lang/String;Ljava/lang/String;)V

    .line 1453935
    :cond_1
    iget-object v0, p0, LX/9CP;->u:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    .line 1453936
    iget-object v2, v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->k:Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;

    .line 1453937
    iget-object v3, v2, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->t:LX/8lL;

    if-eqz v3, :cond_2

    iget-object v3, v2, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->t:LX/8lL;

    .line 1453938
    iget-boolean v4, v3, LX/8lL;->g:Z

    move v3, v4

    .line 1453939
    if-eqz v3, :cond_2

    .line 1453940
    iget-object v3, v2, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->t:LX/8lL;

    .line 1453941
    invoke-virtual {v3}, LX/8lL;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "input_method"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/inputmethod/InputMethodManager;

    .line 1453942
    iget-object v5, v3, LX/8lL;->a:LX/4nn;

    invoke-virtual {v5}, LX/4nn;->getWindowToken()Landroid/os/IBinder;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1453943
    invoke-static {v3}, LX/8lL;->f(LX/8lL;)V

    .line 1453944
    iget-object v4, v3, LX/8lL;->b:LX/0wd;

    const-wide/16 v6, 0x0

    invoke-virtual {v4, v6, v7}, LX/0wd;->b(D)LX/0wd;

    .line 1453945
    iget-object v4, v3, LX/8lL;->b:LX/0wd;

    invoke-virtual {v4}, LX/0wd;->j()LX/0wd;

    .line 1453946
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/9CP;->G:Z

    .line 1453947
    iget-object v0, p0, LX/9CP;->u:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->setVisibility(I)V

    .line 1453948
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/9CP;->b(LX/21p;)V

    goto :goto_0
.end method
