.class public final enum LX/AMY;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/AMY;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/AMY;

.field public static final enum FETCH_DOWNLOADABLE_SHADER_FILTERS:LX/AMY;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1666310
    new-instance v0, LX/AMY;

    const-string v1, "FETCH_DOWNLOADABLE_SHADER_FILTERS"

    invoke-direct {v0, v1, v2}, LX/AMY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AMY;->FETCH_DOWNLOADABLE_SHADER_FILTERS:LX/AMY;

    .line 1666311
    const/4 v0, 0x1

    new-array v0, v0, [LX/AMY;

    sget-object v1, LX/AMY;->FETCH_DOWNLOADABLE_SHADER_FILTERS:LX/AMY;

    aput-object v1, v0, v2

    sput-object v0, LX/AMY;->$VALUES:[LX/AMY;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1666312
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/AMY;
    .locals 1

    .prologue
    .line 1666313
    const-class v0, LX/AMY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/AMY;

    return-object v0
.end method

.method public static values()[LX/AMY;
    .locals 1

    .prologue
    .line 1666314
    sget-object v0, LX/AMY;->$VALUES:[LX/AMY;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/AMY;

    return-object v0
.end method
