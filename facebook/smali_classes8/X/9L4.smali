.class public final LX/9L4;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Z

.field public i:Z

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1469572
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;
    .locals 14

    .prologue
    .line 1469573
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1469574
    iget-object v1, p0, LX/9L4;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1469575
    iget-object v2, p0, LX/9L4;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1469576
    iget-object v3, p0, LX/9L4;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1469577
    iget-object v4, p0, LX/9L4;->g:Ljava/lang/String;

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1469578
    iget-object v5, p0, LX/9L4;->j:Ljava/lang/String;

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1469579
    iget-object v6, p0, LX/9L4;->k:Ljava/lang/String;

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1469580
    iget-object v7, p0, LX/9L4;->l:Ljava/lang/String;

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1469581
    iget-object v8, p0, LX/9L4;->m:Ljava/lang/String;

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1469582
    iget-object v9, p0, LX/9L4;->n:Ljava/lang/String;

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1469583
    iget-object v10, p0, LX/9L4;->o:Ljava/lang/String;

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1469584
    iget-object v11, p0, LX/9L4;->p:Ljava/lang/String;

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 1469585
    iget-object v12, p0, LX/9L4;->q:Ljava/lang/String;

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 1469586
    const/16 v13, 0x11

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 1469587
    const/4 v13, 0x0

    invoke-virtual {v0, v13, v1}, LX/186;->b(II)V

    .line 1469588
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1469589
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1469590
    const/4 v1, 0x3

    iget-boolean v2, p0, LX/9L4;->d:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 1469591
    const/4 v1, 0x4

    iget-boolean v2, p0, LX/9L4;->e:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 1469592
    const/4 v1, 0x5

    iget-boolean v2, p0, LX/9L4;->f:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 1469593
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1469594
    const/4 v1, 0x7

    iget-boolean v2, p0, LX/9L4;->h:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 1469595
    const/16 v1, 0x8

    iget-boolean v2, p0, LX/9L4;->i:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 1469596
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1469597
    const/16 v1, 0xa

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1469598
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1469599
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1469600
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 1469601
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 1469602
    const/16 v1, 0xf

    invoke-virtual {v0, v1, v11}, LX/186;->b(II)V

    .line 1469603
    const/16 v1, 0x10

    invoke-virtual {v0, v1, v12}, LX/186;->b(II)V

    .line 1469604
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1469605
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1469606
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1469607
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1469608
    new-instance v0, LX/15i;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1469609
    new-instance v1, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;

    invoke-direct {v1, v0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;-><init>(LX/15i;)V

    .line 1469610
    return-object v1
.end method
