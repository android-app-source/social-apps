.class public final LX/99c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1R4;


# instance fields
.field public final synthetic a:LX/99d;


# direct methods
.method public constructor <init>(LX/99d;)V
    .locals 0

    .prologue
    .line 1447442
    iput-object p1, p0, LX/99c;->a:LX/99d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1447443
    iget-object v0, p0, LX/99c;->a:LX/99d;

    iget v0, v0, LX/99d;->d:I

    return v0
.end method

.method public final a(I)LX/1Rb;
    .locals 3

    .prologue
    .line 1447444
    iget-object v0, p0, LX/99c;->a:LX/99d;

    invoke-virtual {v0, p1}, LX/99d;->b(I)LX/1Qq;

    move-result-object v0

    .line 1447445
    iget-object v1, p0, LX/99c;->a:LX/99d;

    invoke-virtual {v1, v0}, LX/62C;->a(LX/1Cw;)I

    move-result v1

    sub-int v1, p1, v1

    .line 1447446
    invoke-interface {v0}, LX/1Qr;->e()LX/1R4;

    move-result-object v2

    if-nez v2, :cond_0

    .line 1447447
    const/4 v0, 0x0

    .line 1447448
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, LX/1Qr;->e()LX/1R4;

    move-result-object v0

    invoke-interface {v0, v1}, LX/1R4;->a(I)LX/1Rb;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1Rb;)Z
    .locals 2

    .prologue
    .line 1447449
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "getPositionForRowKey for MultiRowMultiAdapter is not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
