.class public LX/8rz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Bt;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/8rz;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1409693
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1409694
    return-void
.end method

.method public static a(LX/0QB;)LX/8rz;
    .locals 3

    .prologue
    .line 1409695
    sget-object v0, LX/8rz;->a:LX/8rz;

    if-nez v0, :cond_1

    .line 1409696
    const-class v1, LX/8rz;

    monitor-enter v1

    .line 1409697
    :try_start_0
    sget-object v0, LX/8rz;->a:LX/8rz;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1409698
    if-eqz v2, :cond_0

    .line 1409699
    :try_start_1
    new-instance v0, LX/8rz;

    invoke-direct {v0}, LX/8rz;-><init>()V

    .line 1409700
    move-object v0, v0

    .line 1409701
    sput-object v0, LX/8rz;->a:LX/8rz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1409702
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1409703
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1409704
    :cond_1
    sget-object v0, LX/8rz;->a:LX/8rz;

    return-object v0

    .line 1409705
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1409706
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/6Bu;)V
    .locals 4

    .prologue
    .line 1409707
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->STICKER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v0, v1, v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/6Bu;->a(Ljava/lang/Iterable;)LX/6Bu;

    .line 1409708
    return-void
.end method
