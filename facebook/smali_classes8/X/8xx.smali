.class public final LX/8xx;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsInterfaces$PlaceListLightweightCreateFields;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0TF;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:LX/8xy;


# direct methods
.method public constructor <init>(LX/8xy;LX/0TF;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1424937
    iput-object p1, p0, LX/8xx;->e:LX/8xy;

    iput-object p2, p0, LX/8xx;->a:LX/0TF;

    iput-object p3, p0, LX/8xx;->b:Ljava/lang/String;

    iput-object p4, p0, LX/8xx;->c:Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object p5, p0, LX/8xx;->d:Ljava/lang/String;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1424938
    iget-object v0, p0, LX/8xx;->a:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 1424939
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 13

    .prologue
    .line 1424940
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1424941
    iget-object v0, p0, LX/8xx;->a:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 1424942
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1424943
    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightCreateFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightCreateFieldsModel;->a()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserCreatedRecommendationDetailsModel;

    move-result-object v0

    invoke-static {v0}, LX/5I9;->a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserCreatedRecommendationDetailsModel;)Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;

    move-result-object v0

    .line 1424944
    iget-object v1, p0, LX/8xx;->e:LX/8xy;

    iget-object v2, p0, LX/8xx;->b:Ljava/lang/String;

    iget-object v3, p0, LX/8xx;->c:Lcom/facebook/graphql/model/GraphQLFeedback;

    iget-object v4, p0, LX/8xx;->d:Ljava/lang/String;

    .line 1424945
    if-nez v3, :cond_1

    .line 1424946
    const/4 v5, 0x0

    .line 1424947
    :goto_0
    move-object v0, v5

    .line 1424948
    if-eqz v0, :cond_0

    .line 1424949
    iget-object v1, p0, LX/8xx;->e:LX/8xy;

    iget-object v1, v1, LX/8xy;->d:LX/1K9;

    new-instance v2, LX/8q4;

    iget-object v3, v0, LX/6PS;->b:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v0, v0, LX/6PS;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, LX/8q4;-><init>(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/1K9;->a(LX/1KJ;)V

    .line 1424950
    :cond_0
    return-void

    :cond_1
    iget-object v5, v1, LX/8xy;->c:LX/20j;

    const/4 v7, 0x0

    .line 1424951
    invoke-static {v3}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v9

    .line 1424952
    invoke-virtual {v9}, LX/0Px;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_2

    move-object v6, v7

    .line 1424953
    :goto_1
    move-object v5, v6

    .line 1424954
    goto :goto_0

    .line 1424955
    :cond_2
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    const/4 v6, 0x0

    move v8, v6

    :goto_2
    if-ge v8, v10, :cond_5

    invoke-virtual {v9, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1424956
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1424957
    :goto_3
    if-nez v6, :cond_4

    move-object v6, v7

    .line 1424958
    goto :goto_1

    .line 1424959
    :cond_3
    add-int/lit8 v6, v8, 0x1

    move v8, v6

    goto :goto_2

    .line 1424960
    :cond_4
    invoke-static {v2, v6, v0}, LX/20j;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v7

    .line 1424961
    invoke-static {v9, v6, v7}, LX/20j;->a(LX/0Px;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;)LX/0Px;

    move-result-object v6

    .line 1424962
    invoke-static {v3}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/3dM;

    move-result-object v8

    iget-object v9, v5, LX/20j;->b:LX/0SG;

    invoke-interface {v9}, LX/0SG;->a()J

    move-result-wide v10

    .line 1424963
    iput-wide v10, v8, LX/3dM;->v:J

    .line 1424964
    move-object v8, v8

    .line 1424965
    invoke-static {v3}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v9

    invoke-static {v3}, LX/16z;->g(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v10

    invoke-static {v8, v6, v9, v10}, LX/20j;->a(LX/3dM;Ljava/util/List;ILcom/facebook/graphql/model/GraphQLPageInfo;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v8

    .line 1424966
    new-instance v6, LX/6PS;

    invoke-direct {v6, v8, v7}, LX/6PS;-><init>(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;)V

    goto :goto_1

    :cond_5
    move-object v6, v7

    goto :goto_3
.end method
