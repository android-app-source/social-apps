.class public LX/9LP;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/9LP;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0tX;

.field public final c:LX/3iV;

.field public final d:LX/0jU;

.field public final e:LX/0bH;

.field public final f:LX/189;

.field private final g:LX/1Ck;

.field public final h:LX/1Sl;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1470812
    const-class v0, LX/9LP;

    sput-object v0, LX/9LP;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0tX;LX/3iV;LX/0jU;LX/0bH;LX/189;LX/1Ck;LX/1Sl;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1470803
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1470804
    iput-object p1, p0, LX/9LP;->b:LX/0tX;

    .line 1470805
    iput-object p2, p0, LX/9LP;->c:LX/3iV;

    .line 1470806
    iput-object p3, p0, LX/9LP;->d:LX/0jU;

    .line 1470807
    iput-object p4, p0, LX/9LP;->e:LX/0bH;

    .line 1470808
    iput-object p5, p0, LX/9LP;->f:LX/189;

    .line 1470809
    iput-object p6, p0, LX/9LP;->g:LX/1Ck;

    .line 1470810
    iput-object p7, p0, LX/9LP;->h:LX/1Sl;

    .line 1470811
    return-void
.end method

.method public static a(LX/6Vy;)Ljava/lang/String;
    .locals 3
    .annotation build Lcom/facebook/graphql/calls/GroupSellSurface;
    .end annotation

    .prologue
    .line 1470734
    sget-object v0, LX/9LO;->a:[I

    invoke-virtual {p0}, LX/6Vy;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1470735
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized surface: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1470736
    :pswitch_0
    const-string v0, "YOUR_POSTS"

    .line 1470737
    :goto_0
    return-object v0

    .line 1470738
    :pswitch_1
    const-string v0, "GROUP_POST_CHEVRON"

    goto :goto_0

    .line 1470739
    :pswitch_2
    const-string v0, "FEED_POST_CHEVRON"

    goto :goto_0

    .line 1470740
    :pswitch_3
    const-string v0, "DELETE_INTERCEPT"

    goto :goto_0

    .line 1470741
    :pswitch_4
    const-string v0, "FEED_STORY"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 1

    .prologue
    .line 1470802
    invoke-static {p0}, LX/2vB;->d(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/9LP;
    .locals 8

    .prologue
    .line 1470800
    new-instance v0, LX/9LP;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {p0}, LX/3iV;->a(LX/0QB;)LX/3iV;

    move-result-object v2

    check-cast v2, LX/3iV;

    invoke-static {p0}, LX/0jU;->a(LX/0QB;)LX/0jU;

    move-result-object v3

    check-cast v3, LX/0jU;

    invoke-static {p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v4

    check-cast v4, LX/0bH;

    invoke-static {p0}, LX/189;->b(LX/0QB;)LX/189;

    move-result-object v5

    check-cast v5, LX/189;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    invoke-static {p0}, LX/1Sl;->b(LX/0QB;)LX/1Sl;

    move-result-object v7

    check-cast v7, LX/1Sl;

    invoke-direct/range {v0 .. v7}, LX/9LP;-><init>(LX/0tX;LX/3iV;LX/0jU;LX/0bH;LX/189;LX/1Ck;LX/1Sl;)V

    .line 1470801
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;LX/6Vy;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "LX/6Vy;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1470758
    invoke-static {p1}, LX/9LP;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    .line 1470759
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    .line 1470760
    if-eqz v0, :cond_2

    const-string v2, "IN_STOCK"

    .line 1470761
    :goto_0
    new-instance v3, LX/4IZ;

    invoke-direct {v3}, LX/4IZ;-><init>()V

    .line 1470762
    const-string v4, "story_id"

    invoke-virtual {v3, v4, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1470763
    move-object v3, v3

    .line 1470764
    const-string v4, "product_availability"

    invoke-virtual {v3, v4, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1470765
    move-object v2, v3

    .line 1470766
    invoke-static {p2}, LX/9LP;->a(LX/6Vy;)Ljava/lang/String;

    move-result-object v3

    .line 1470767
    const-string v4, "surface"

    invoke-virtual {v2, v4, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1470768
    move-object v2, v2

    .line 1470769
    new-instance v3, LX/9KR;

    invoke-direct {v3}, LX/9KR;-><init>()V

    move-object v3, v3

    .line 1470770
    const-string v4, "input"

    invoke-virtual {v3, v4, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1470771
    invoke-static {v3}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v2

    move-object v1, v2

    .line 1470772
    iget-object v2, p0, LX/9LP;->h:LX/1Sl;

    invoke-virtual {v2}, LX/1Sl;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1470773
    invoke-static {p1}, LX/2vB;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    .line 1470774
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1470775
    new-instance v3, LX/9KU;

    invoke-direct {v3}, LX/9KU;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v2

    .line 1470776
    iput-object v2, v3, LX/9KU;->b:Ljava/lang/String;

    .line 1470777
    move-object v2, v3

    .line 1470778
    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 1470779
    :goto_1
    iput-boolean v0, v2, LX/9KU;->c:Z

    .line 1470780
    move-object v0, v2

    .line 1470781
    const/4 v8, 0x1

    const/4 v10, 0x0

    const/4 v6, 0x0

    .line 1470782
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 1470783
    iget-object v5, v0, LX/9KU;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v4, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1470784
    iget-object v7, v0, LX/9KU;->b:Ljava/lang/String;

    invoke-virtual {v4, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1470785
    const/4 v9, 0x3

    invoke-virtual {v4, v9}, LX/186;->c(I)V

    .line 1470786
    invoke-virtual {v4, v10, v5}, LX/186;->b(II)V

    .line 1470787
    invoke-virtual {v4, v8, v7}, LX/186;->b(II)V

    .line 1470788
    const/4 v5, 0x2

    iget-boolean v7, v0, LX/9KU;->c:Z

    invoke-virtual {v4, v5, v7}, LX/186;->a(IZ)V

    .line 1470789
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 1470790
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 1470791
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 1470792
    invoke-virtual {v5, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1470793
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1470794
    new-instance v5, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$StoryModel$AttachmentsModel$TargetModel;

    invoke-direct {v5, v4}, Lcom/facebook/groupcommerce/protocol/GroupCommerceProductItemMutationsModels$ProductItemChangeAvailabilityCoreMutationFieldsModel$StoryModel$AttachmentsModel$TargetModel;-><init>(LX/15i;)V

    .line 1470795
    move-object v0, v5

    .line 1470796
    invoke-virtual {v1, v0}, LX/399;->a(LX/0jT;)LX/399;

    .line 1470797
    :cond_0
    iget-object v0, p0, LX/9LP;->b:LX/0tX;

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 1470798
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1470799
    :cond_2
    const-string v2, "OUT_OF_STOCK"

    goto/16 :goto_0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/6Vy;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/6Vy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1470742
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1470743
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1470744
    invoke-static {v0}, LX/9LP;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    .line 1470745
    iget-object v2, p0, LX/9LP;->h:LX/1Sl;

    invoke-virtual {v2}, LX/1Sl;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1470746
    iget-object v3, p0, LX/9LP;->f:LX/189;

    .line 1470747
    iget-object v2, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 1470748
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3, v2, v1}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLStory;Z)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    .line 1470749
    if-nez v3, :cond_1

    .line 1470750
    :cond_0
    :goto_0
    invoke-virtual {p0, v0, p2}, LX/9LP;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/6Vy;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 1470751
    iget-object v3, p0, LX/9LP;->g:LX/1Ck;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "toggle_availability"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, LX/9LN;

    invoke-direct {v5, p0, v0, v1}, LX/9LN;-><init>(LX/9LP;Lcom/facebook/graphql/model/GraphQLStory;Z)V

    invoke-virtual {v3, v4, v2, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1470752
    return-void

    .line 1470753
    :cond_1
    invoke-virtual {p1, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->b(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 1470754
    invoke-static {v2}, LX/182;->h(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStorySet;

    move-result-object v2

    .line 1470755
    if-eqz v2, :cond_2

    :goto_1
    check-cast v2, Lcom/facebook/graphql/model/FeedUnit;

    .line 1470756
    iget-object v3, p0, LX/9LP;->e:LX/0bH;

    new-instance v4, LX/1Ne;

    invoke-direct {v4, v2}, LX/1Ne;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v3, v4}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0

    :cond_2
    move-object v2, v3

    .line 1470757
    goto :goto_1
.end method
