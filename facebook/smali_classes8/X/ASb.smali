.class public LX/ASb;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/75F;

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/75F;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1674229
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1674230
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/ASb;->b:Ljava/util/List;

    .line 1674231
    iput-object p1, p0, LX/ASb;->a:LX/75F;

    .line 1674232
    return-void
.end method

.method public static a(LX/0QB;)LX/ASb;
    .locals 4

    .prologue
    .line 1674233
    const-class v1, LX/ASb;

    monitor-enter v1

    .line 1674234
    :try_start_0
    sget-object v0, LX/ASb;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1674235
    sput-object v2, LX/ASb;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1674236
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1674237
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1674238
    new-instance p0, LX/ASb;

    invoke-static {v0}, LX/75F;->a(LX/0QB;)LX/75F;

    move-result-object v3

    check-cast v3, LX/75F;

    invoke-direct {p0, v3}, LX/ASb;-><init>(LX/75F;)V

    .line 1674239
    move-object v0, p0

    .line 1674240
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1674241
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/ASb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1674242
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1674243
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/0So;Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;)Z
    .locals 4

    .prologue
    .line 1674244
    invoke-virtual {p1}, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;->shouldForceStopAutoTagging()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p0}, LX/0So;->now()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;->getPostDelayStartTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0xbb8

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/ipc/media/MediaItem;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ipc/media/MediaItem;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/base/tagging/FaceBox;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1674245
    instance-of v1, p1, Lcom/facebook/photos/base/media/PhotoItem;

    if-nez v1, :cond_1

    .line 1674246
    :cond_0
    :goto_0
    return-object v0

    .line 1674247
    :cond_1
    check-cast p1, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1674248
    iget-object v1, p1, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    move-object v1, v1

    .line 1674249
    instance-of v1, v1, LX/74x;

    if-eqz v1, :cond_0

    .line 1674250
    iget-object v0, p1, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    move-object v0, v0

    .line 1674251
    check-cast v0, LX/74x;

    .line 1674252
    iget-object v1, p0, LX/ASb;->a:LX/75F;

    invoke-virtual {v1, v0}, LX/75F;->a(LX/74x;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method
