.class public final LX/ASu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/AT1;


# direct methods
.method public constructor <init>(LX/AT1;)V
    .locals 0

    .prologue
    .line 1674641
    iput-object p1, p0, LX/ASu;->a:LX/AT1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x79b0192c

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1674642
    iget-object v1, p0, LX/ASu;->a:LX/AT1;

    iget-object v1, v1, LX/AT1;->z:LX/ASy;

    invoke-virtual {v1}, LX/ASy;->cancel()V

    .line 1674643
    iget-object v1, p0, LX/ASu;->a:LX/AT1;

    .line 1674644
    iget-boolean v3, v1, LX/AT1;->A:Z

    if-nez v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    invoke-static {v1, v3}, LX/AT1;->setSphericalUploadState(LX/AT1;Z)V

    .line 1674645
    iget-boolean v3, v1, LX/AT1;->A:Z

    if-eqz v3, :cond_1

    .line 1674646
    iget-object v3, v1, LX/AT1;->a:LX/1xG;

    iget-object v4, v1, LX/AT1;->d:Ljava/lang/String;

    sget-object p0, LX/7Dj;->COMPOSER:LX/7Dj;

    .line 1674647
    new-instance p1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "spherical_photo_toggle_on"

    invoke-direct {p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1674648
    invoke-static {v3, p1, v4, p0}, LX/1xG;->b(LX/1xG;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/7Dj;)V

    .line 1674649
    :goto_1
    const v1, 0x6888a777

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1674650
    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    .line 1674651
    :cond_1
    iget-object v3, v1, LX/AT1;->a:LX/1xG;

    iget-object v4, v1, LX/AT1;->d:Ljava/lang/String;

    sget-object p0, LX/7Dj;->COMPOSER:LX/7Dj;

    .line 1674652
    new-instance p1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "spherical_photo_toggle_off"

    invoke-direct {p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1674653
    invoke-static {v3, p1, v4, p0}, LX/1xG;->b(LX/1xG;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/7Dj;)V

    .line 1674654
    goto :goto_1
.end method
