.class public final LX/AQf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;)V
    .locals 0

    .prologue
    .line 1671566
    iput-object p1, p0, LX/AQf;->a:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v0, 0x2

    const v1, 0x2c7e0ceb

    invoke-static {v0, v8, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1671567
    const v0, 0x7f0d00d0

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    .line 1671568
    instance-of v2, v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;

    if-eqz v2, :cond_1

    .line 1671569
    check-cast v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;

    .line 1671570
    new-instance v2, LX/7l3;

    invoke-direct {v2, v0}, LX/7l3;-><init>(Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;)V

    invoke-virtual {v2}, LX/7l3;->a()Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    move-result-object v2

    .line 1671571
    iget-object v3, p0, LX/AQf;->a:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;

    .line 1671572
    sget-object v4, LX/21D;->TIMELINE:LX/21D;

    const-string v5, "lifeEventAfterClickSuggestion"

    invoke-static {v4, v5}, LX/1nC;->b(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v4

    new-instance v5, LX/5RH;

    invoke-direct {v5}, LX/5RH;-><init>()V

    invoke-virtual {v5}, LX/5RH;->a()Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialDateInfo(Lcom/facebook/ipc/composer/model/ComposerDateInfo;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v4

    new-instance v5, LX/89K;

    invoke-direct {v5}, LX/89K;-><init>()V

    invoke-static {}, LX/BQ4;->c()LX/BQ4;

    move-result-object v5

    invoke-static {v5}, LX/89K;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v4

    iget-object v5, p0, LX/AQf;->a:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;

    iget-object v5, v5, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->s:LX/0ad;

    sget-short v6, LX/1EB;->an:S

    invoke-interface {v5, v6, v7}, LX/0ad;->a(SZ)Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setUsePublishExperiment(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v4

    .line 1671573
    invoke-virtual {v0}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;->m()Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    move-result-object v5

    .line 1671574
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->ENGAGED:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    if-eq v5, v6, :cond_0

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->MARRIED:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    if-eq v5, v6, :cond_0

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->STARTED_JOB:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    if-eq v5, v6, :cond_0

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->GRADUATED:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    if-ne v5, v6, :cond_4

    .line 1671575
    :cond_0
    const/4 v5, 0x1

    .line 1671576
    :goto_0
    move v0, v5

    .line 1671577
    if-eqz v0, :cond_2

    .line 1671578
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {v3}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-direct {v0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 1671579
    new-instance v5, Landroid/content/ComponentName;

    const-class v6, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialActivity;

    invoke-direct {v5, v3, v6}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v5}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1671580
    const-string v5, "life_event_model"

    invoke-virtual {v0, v5, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1671581
    const-string v2, "extra_composer_configuration"

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v4

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1671582
    const/high16 v2, 0x2000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1671583
    iget-object v2, p0, LX/AQf;->a:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;

    iget-object v2, v2, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->p:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v2, v0, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1671584
    invoke-virtual {v3}, Landroid/app/Activity;->finish()V

    .line 1671585
    :cond_1
    :goto_1
    const v0, -0x1f2950e

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 1671586
    :cond_2
    const v0, 0x7f0d00cf

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1671587
    invoke-static {v3}, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventIconsActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 1671588
    const-string v4, "extra_composer_life_event_model"

    invoke-virtual {v0, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1671589
    const-string v2, "extra_composer_life_event_custom"

    invoke-virtual {v0, v2, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1671590
    iget-object v2, p0, LX/AQf;->a:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;

    iget-object v2, v2, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->p:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v2, v0, v7, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_1

    .line 1671591
    :cond_3
    iget-object v0, p0, LX/AQf;->a:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;

    iget-object v0, v0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeActivity;->q:LX/1Kf;

    const/4 v5, 0x0

    invoke-virtual {v4, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a(Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    const/16 v4, 0x6dc

    invoke-interface {v0, v5, v2, v4, v3}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    goto :goto_1

    :cond_4
    const/4 v5, 0x0

    goto :goto_0
.end method
