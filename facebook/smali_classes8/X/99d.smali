.class public LX/99d;
.super LX/62C;
.source ""

# interfaces
.implements LX/1Qr;


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1Qq;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/1R4;

.field public c:I

.field public d:I


# direct methods
.method public varargs constructor <init>(Z[LX/1Qq;)V
    .locals 1

    .prologue
    .line 1447503
    invoke-direct {p0, p1, p2}, LX/62C;-><init>(Z[LX/1Cw;)V

    .line 1447504
    new-instance v0, LX/99c;

    invoke-direct {v0, p0}, LX/99c;-><init>(LX/99d;)V

    move-object v0, v0

    .line 1447505
    iput-object v0, p0, LX/99d;->b:LX/1R4;

    .line 1447506
    invoke-static {p2}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/99d;->a:Ljava/util/List;

    .line 1447507
    return-void
.end method

.method private h(I)LX/1Qq;
    .locals 4

    .prologue
    .line 1447496
    iget-object v0, p0, LX/99d;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, p1

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Qq;

    .line 1447497
    invoke-interface {v0}, LX/1Qr;->d()I

    move-result v3

    .line 1447498
    if-ge v1, v3, :cond_0

    .line 1447499
    return-object v0

    .line 1447500
    :cond_0
    sub-int v0, v1, v3

    move v1, v0

    .line 1447501
    goto :goto_0

    .line 1447502
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FeedEdgeIndex should be less than total feed edge count, feedEdgeIndex = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ,feedEdgeCount = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, LX/99d;->d()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(LX/1Qq;)I
    .locals 3

    .prologue
    .line 1447489
    const/4 v0, 0x0

    .line 1447490
    iget-object v1, p0, LX/99d;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Qq;

    .line 1447491
    if-ne p1, v0, :cond_0

    .line 1447492
    return v1

    .line 1447493
    :cond_0
    invoke-interface {v0}, LX/1Qr;->d()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 1447494
    goto :goto_0

    .line 1447495
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Given MultiRowAdapter isn\'t part of list adapter."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final synthetic a(I)LX/1Cw;
    .locals 1

    .prologue
    .line 1447488
    invoke-virtual {p0, p1}, LX/99d;->b(I)LX/1Qq;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/5Mj;)V
    .locals 2

    .prologue
    .line 1447485
    iget-object v0, p0, LX/99d;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Qq;

    .line 1447486
    invoke-interface {v0, p1}, LX/0fn;->a(LX/5Mj;)V

    goto :goto_0

    .line 1447487
    :cond_0
    return-void
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 1447450
    iget-object v0, p0, LX/99d;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Qq;

    .line 1447451
    invoke-interface {v0, p1}, LX/1Qr;->a(Landroid/content/res/Configuration;)V

    goto :goto_0

    .line 1447452
    :cond_0
    return-void
.end method

.method public final b(I)LX/1Qq;
    .locals 1

    .prologue
    .line 1447484
    invoke-super {p0, p1}, LX/62C;->a(I)LX/1Cw;

    move-result-object v0

    check-cast v0, LX/1Qq;

    return-object v0
.end method

.method public final c(I)I
    .locals 2

    .prologue
    .line 1447481
    invoke-virtual {p0, p1}, LX/99d;->b(I)LX/1Qq;

    move-result-object v0

    .line 1447482
    invoke-virtual {p0, v0}, LX/62C;->a(LX/1Cw;)I

    move-result v1

    sub-int v1, p1, v1

    .line 1447483
    invoke-interface {v0, v1}, LX/1Qr;->c(I)I

    move-result v0

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 1447480
    iget v0, p0, LX/99d;->c:I

    return v0
.end method

.method public final e()LX/1R4;
    .locals 1

    .prologue
    .line 1447479
    iget-object v0, p0, LX/99d;->b:LX/1R4;

    return-object v0
.end method

.method public final g(I)I
    .locals 2

    .prologue
    .line 1447476
    invoke-virtual {p0, p1}, LX/99d;->b(I)LX/1Qq;

    move-result-object v0

    .line 1447477
    invoke-virtual {p0, v0}, LX/62C;->a(LX/1Cw;)I

    move-result v1

    sub-int v1, p1, v1

    .line 1447478
    invoke-interface {v0, v1}, LX/1Qr;->g(I)I

    move-result v0

    return v0
.end method

.method public final h_(I)I
    .locals 3

    .prologue
    .line 1447473
    invoke-virtual {p0, p1}, LX/99d;->b(I)LX/1Qq;

    move-result-object v0

    .line 1447474
    invoke-virtual {p0, v0}, LX/62C;->a(LX/1Cw;)I

    move-result v1

    sub-int v1, p1, v1

    .line 1447475
    invoke-virtual {p0, v0}, LX/99d;->a(LX/1Qq;)I

    move-result v2

    invoke-interface {v0, v1}, LX/1Qr;->h_(I)I

    move-result v0

    add-int/2addr v0, v2

    return v0
.end method

.method public final m_(I)I
    .locals 3

    .prologue
    .line 1447470
    invoke-direct {p0, p1}, LX/99d;->h(I)LX/1Qq;

    move-result-object v0

    .line 1447471
    invoke-virtual {p0, v0}, LX/99d;->a(LX/1Qq;)I

    move-result v1

    sub-int v1, p1, v1

    .line 1447472
    invoke-virtual {p0, v0}, LX/62C;->a(LX/1Cw;)I

    move-result v2

    invoke-interface {v0, v1}, LX/1Qr;->m_(I)I

    move-result v0

    add-int/2addr v0, v2

    return v0
.end method

.method public final n_(I)I
    .locals 3

    .prologue
    .line 1447467
    invoke-direct {p0, p1}, LX/99d;->h(I)LX/1Qq;

    move-result-object v0

    .line 1447468
    invoke-virtual {p0, v0}, LX/99d;->a(LX/1Qq;)I

    move-result v1

    sub-int v1, p1, v1

    .line 1447469
    invoke-virtual {p0, v0}, LX/62C;->a(LX/1Cw;)I

    move-result v2

    invoke-interface {v0, v1}, LX/1Qr;->n_(I)I

    move-result v0

    add-int/2addr v0, v2

    return v0
.end method

.method public final notifyDataSetChanged()V
    .locals 4

    .prologue
    .line 1447453
    invoke-super {p0}, LX/62C;->notifyDataSetChanged()V

    .line 1447454
    const/4 v0, 0x0

    .line 1447455
    iget-object v1, p0, LX/99d;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Qq;

    .line 1447456
    invoke-interface {v0}, LX/1Qr;->e()LX/1R4;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 1447457
    invoke-interface {v0}, LX/1Qr;->e()LX/1R4;

    move-result-object v0

    invoke-interface {v0}, LX/1R4;->a()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 1447458
    goto :goto_0

    .line 1447459
    :cond_0
    iput v1, p0, LX/99d;->d:I

    .line 1447460
    const/4 v0, 0x0

    .line 1447461
    move v1, v0

    move v2, v0

    .line 1447462
    :goto_2
    iget-object v0, p0, LX/99d;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1447463
    iget-object v0, p0, LX/99d;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Qq;

    invoke-interface {v0}, LX/1Qr;->d()I

    move-result v0

    add-int/2addr v2, v0

    .line 1447464
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1447465
    :cond_1
    iput v2, p0, LX/99d;->c:I

    .line 1447466
    return-void

    :cond_2
    move v0, v1

    goto :goto_1
.end method
