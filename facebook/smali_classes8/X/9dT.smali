.class public final enum LX/9dT;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9dT;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9dT;

.field public static final enum CLOCKWISE:LX/9dT;

.field public static final enum COUNTER_CLOCKWISE:LX/9dT;

.field public static final enum NO_ROTATION:LX/9dT;


# instance fields
.field private final mMultiplier:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1518168
    new-instance v0, LX/9dT;

    const-string v1, "CLOCKWISE"

    invoke-direct {v0, v1, v3, v4}, LX/9dT;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/9dT;->CLOCKWISE:LX/9dT;

    .line 1518169
    new-instance v0, LX/9dT;

    const-string v1, "COUNTER_CLOCKWISE"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v4, v2}, LX/9dT;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/9dT;->COUNTER_CLOCKWISE:LX/9dT;

    .line 1518170
    new-instance v0, LX/9dT;

    const-string v1, "NO_ROTATION"

    invoke-direct {v0, v1, v5, v3}, LX/9dT;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/9dT;->NO_ROTATION:LX/9dT;

    .line 1518171
    const/4 v0, 0x3

    new-array v0, v0, [LX/9dT;

    sget-object v1, LX/9dT;->CLOCKWISE:LX/9dT;

    aput-object v1, v0, v3

    sget-object v1, LX/9dT;->COUNTER_CLOCKWISE:LX/9dT;

    aput-object v1, v0, v4

    sget-object v1, LX/9dT;->NO_ROTATION:LX/9dT;

    aput-object v1, v0, v5

    sput-object v0, LX/9dT;->$VALUES:[LX/9dT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 1518164
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1518165
    iput p3, p0, LX/9dT;->mMultiplier:I

    .line 1518166
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9dT;
    .locals 1

    .prologue
    .line 1518167
    const-class v0, LX/9dT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9dT;

    return-object v0
.end method

.method public static values()[LX/9dT;
    .locals 1

    .prologue
    .line 1518163
    sget-object v0, LX/9dT;->$VALUES:[LX/9dT;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9dT;

    return-object v0
.end method


# virtual methods
.method public final getMultiplier()I
    .locals 1

    .prologue
    .line 1518162
    iget v0, p0, LX/9dT;->mMultiplier:I

    return v0
.end method
