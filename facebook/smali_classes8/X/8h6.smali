.class public final LX/8h6;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 25

    .prologue
    .line 1389035
    const/16 v21, 0x0

    .line 1389036
    const/16 v20, 0x0

    .line 1389037
    const/16 v19, 0x0

    .line 1389038
    const/16 v18, 0x0

    .line 1389039
    const/16 v17, 0x0

    .line 1389040
    const/16 v16, 0x0

    .line 1389041
    const/4 v15, 0x0

    .line 1389042
    const/4 v14, 0x0

    .line 1389043
    const/4 v13, 0x0

    .line 1389044
    const/4 v12, 0x0

    .line 1389045
    const/4 v11, 0x0

    .line 1389046
    const/4 v10, 0x0

    .line 1389047
    const/4 v9, 0x0

    .line 1389048
    const/4 v8, 0x0

    .line 1389049
    const/4 v7, 0x0

    .line 1389050
    const/4 v6, 0x0

    .line 1389051
    const/4 v5, 0x0

    .line 1389052
    const/4 v4, 0x0

    .line 1389053
    const/4 v3, 0x0

    .line 1389054
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v22

    sget-object v23, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    if-eq v0, v1, :cond_1

    .line 1389055
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1389056
    const/4 v3, 0x0

    .line 1389057
    :goto_0
    return v3

    .line 1389058
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1389059
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v22

    sget-object v23, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    if-eq v0, v1, :cond_10

    .line 1389060
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v22

    .line 1389061
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1389062
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v23

    sget-object v24, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-eq v0, v1, :cond_1

    if-eqz v22, :cond_1

    .line 1389063
    const-string v23, "__type__"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_2

    const-string v23, "__typename"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_3

    .line 1389064
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v21

    goto :goto_1

    .line 1389065
    :cond_3
    const-string v23, "can_viewer_follow"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_4

    .line 1389066
    const/4 v7, 0x1

    .line 1389067
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v20

    goto :goto_1

    .line 1389068
    :cond_4
    const-string v23, "can_viewer_like"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_5

    .line 1389069
    const/4 v6, 0x1

    .line 1389070
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v19

    goto :goto_1

    .line 1389071
    :cond_5
    const-string v23, "category_names"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_6

    .line 1389072
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v18

    goto :goto_1

    .line 1389073
    :cond_6
    const-string v23, "cover_photo"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_7

    .line 1389074
    invoke-static/range {p0 .. p1}, LX/8g0;->a(LX/15w;LX/186;)I

    move-result v17

    goto :goto_1

    .line 1389075
    :cond_7
    const-string v23, "does_viewer_like"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_8

    .line 1389076
    const/4 v5, 0x1

    .line 1389077
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v16

    goto/16 :goto_1

    .line 1389078
    :cond_8
    const-string v23, "id"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_9

    .line 1389079
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    goto/16 :goto_1

    .line 1389080
    :cond_9
    const-string v23, "is_verified"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_a

    .line 1389081
    const/4 v4, 0x1

    .line 1389082
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v14

    goto/16 :goto_1

    .line 1389083
    :cond_a
    const-string v23, "name"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_b

    .line 1389084
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    goto/16 :goto_1

    .line 1389085
    :cond_b
    const-string v23, "page_call_to_action"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_c

    .line 1389086
    invoke-static/range {p0 .. p1}, LX/8gz;->a(LX/15w;LX/186;)I

    move-result v12

    goto/16 :goto_1

    .line 1389087
    :cond_c
    const-string v23, "page_likers"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_d

    .line 1389088
    invoke-static/range {p0 .. p1}, LX/8h5;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 1389089
    :cond_d
    const-string v23, "profile_picture"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_e

    .line 1389090
    invoke-static/range {p0 .. p1}, LX/3lU;->a(LX/15w;LX/186;)I

    move-result v10

    goto/16 :goto_1

    .line 1389091
    :cond_e
    const-string v23, "verification_status"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_f

    .line 1389092
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    goto/16 :goto_1

    .line 1389093
    :cond_f
    const-string v23, "video_channel_is_viewer_following"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_0

    .line 1389094
    const/4 v3, 0x1

    .line 1389095
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v8

    goto/16 :goto_1

    .line 1389096
    :cond_10
    const/16 v22, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1389097
    const/16 v22, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v22

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1389098
    if-eqz v7, :cond_11

    .line 1389099
    const/4 v7, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 1389100
    :cond_11
    if-eqz v6, :cond_12

    .line 1389101
    const/4 v6, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 1389102
    :cond_12
    const/4 v6, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 1389103
    const/4 v6, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 1389104
    if-eqz v5, :cond_13

    .line 1389105
    const/4 v5, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 1389106
    :cond_13
    const/4 v5, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v15}, LX/186;->b(II)V

    .line 1389107
    if-eqz v4, :cond_14

    .line 1389108
    const/4 v4, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v14}, LX/186;->a(IZ)V

    .line 1389109
    :cond_14
    const/16 v4, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v13}, LX/186;->b(II)V

    .line 1389110
    const/16 v4, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12}, LX/186;->b(II)V

    .line 1389111
    const/16 v4, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->b(II)V

    .line 1389112
    const/16 v4, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->b(II)V

    .line 1389113
    const/16 v4, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v9}, LX/186;->b(II)V

    .line 1389114
    if-eqz v3, :cond_15

    .line 1389115
    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->a(IZ)V

    .line 1389116
    :cond_15
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method
