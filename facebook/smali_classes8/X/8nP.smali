.class public LX/8nP;
.super LX/8nB;
.source ""


# static fields
.field public static final a:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;


# instance fields
.field public final b:Ljava/util/concurrent/Executor;

.field private final c:LX/87o;

.field public final d:LX/3iT;

.field public final e:LX/87w;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1400963
    new-instance v0, Lcom/facebook/tagging/graphql/data/GoodFriendsTaggingTypeaheadDataSource$Metadata;

    invoke-direct {v0}, Lcom/facebook/tagging/graphql/data/GoodFriendsTaggingTypeaheadDataSource$Metadata;-><init>()V

    sput-object v0, LX/8nP;->a:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;LX/87o;LX/3iT;LX/87w;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1400964
    invoke-direct {p0}, LX/8nB;-><init>()V

    .line 1400965
    iput-object p1, p0, LX/8nP;->b:Ljava/util/concurrent/Executor;

    .line 1400966
    iput-object p2, p0, LX/8nP;->c:LX/87o;

    .line 1400967
    iput-object p3, p0, LX/8nP;->d:LX/3iT;

    .line 1400968
    iput-object p4, p0, LX/8nP;->e:LX/87w;

    .line 1400969
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;Ljava/lang/String;ZZZZZLX/8JX;)V
    .locals 3

    .prologue
    .line 1400970
    if-nez p1, :cond_0

    .line 1400971
    :goto_0
    return-void

    .line 1400972
    :cond_0
    iget-object v0, p0, LX/8nP;->c:LX/87o;

    const/4 v1, 0x0

    new-instance v2, LX/8nN;

    invoke-direct {v2, p0, p1, p8}, LX/8nN;-><init>(LX/8nP;Ljava/lang/CharSequence;LX/8JX;)V

    invoke-virtual {v0, v1, v2}, LX/87o;->a(ZLX/87n;)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1400973
    const/4 v0, 0x1

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1400974
    const-string v0, "goodfriends_fetcher"

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1400975
    sget-object v0, LX/8nE;->GOOD_FRIENDS:LX/8nE;

    invoke-virtual {v0}, LX/8nE;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
