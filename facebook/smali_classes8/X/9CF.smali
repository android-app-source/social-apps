.class public LX/9CF;
.super Landroid/graphics/drawable/Drawable;
.source ""

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;
.implements Landroid/graphics/drawable/Drawable$Callback;


# instance fields
.field private final a:Landroid/graphics/drawable/Drawable;

.field private final b:Landroid/graphics/drawable/Drawable;

.field public final c:Landroid/graphics/drawable/Drawable;

.field private d:Landroid/graphics/drawable/Drawable;

.field private e:Landroid/animation/ValueAnimator;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0wM;)V
    .locals 4

    .prologue
    .line 1453715
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 1453716
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 1453717
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x7f0105a8

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 1453718
    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    .line 1453719
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/9CF;->a:Landroid/graphics/drawable/Drawable;

    .line 1453720
    iget-object v0, p0, LX/9CF;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/9CF;->b:Landroid/graphics/drawable/Drawable;

    .line 1453721
    iget-object v0, p0, LX/9CF;->b:Landroid/graphics/drawable/Drawable;

    const v1, -0x161513

    invoke-virtual {p2, v1}, LX/0wM;->a(I)Landroid/graphics/ColorFilter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 1453722
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0202aa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/9CF;->c:Landroid/graphics/drawable/Drawable;

    .line 1453723
    invoke-virtual {p0}, LX/9CF;->invalidateSelf()V

    .line 1453724
    return-void
.end method

.method public static a(LX/9CF;Landroid/graphics/drawable/Drawable;Landroid/animation/ValueAnimator;)V
    .locals 1

    .prologue
    .line 1453708
    iput-object p1, p0, LX/9CF;->d:Landroid/graphics/drawable/Drawable;

    .line 1453709
    iput-object p2, p0, LX/9CF;->e:Landroid/animation/ValueAnimator;

    .line 1453710
    invoke-virtual {p2, p0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1453711
    invoke-virtual {p2, p0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1453712
    invoke-virtual {p2}, Landroid/animation/ValueAnimator;->isStarted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1453713
    invoke-virtual {p2}, Landroid/animation/ValueAnimator;->start()V

    .line 1453714
    :cond_0
    return-void
.end method

.method public static c()Landroid/animation/ValueAnimator;
    .locals 4

    .prologue
    .line 1453704
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 1453705
    const-wide/16 v2, 0xfa0

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1453706
    return-object v0

    .line 1453707
    :array_0
    .array-data 4
        0xff
        0x0
    .end array-data
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1453699
    invoke-virtual {p0}, LX/9CF;->b()V

    .line 1453700
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 1453701
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1453702
    iget-object v1, p0, LX/9CF;->b:Landroid/graphics/drawable/Drawable;

    invoke-static {p0, v1, v0}, LX/9CF;->a(LX/9CF;Landroid/graphics/drawable/Drawable;Landroid/animation/ValueAnimator;)V

    .line 1453703
    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0xff
    .end array-data
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1453693
    iput-object v1, p0, LX/9CF;->d:Landroid/graphics/drawable/Drawable;

    .line 1453694
    iget-object v0, p0, LX/9CF;->e:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 1453695
    iget-object v0, p0, LX/9CF;->e:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p0}, Landroid/animation/ValueAnimator;->removeUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1453696
    iput-object v1, p0, LX/9CF;->e:Landroid/animation/ValueAnimator;

    .line 1453697
    :cond_0
    invoke-virtual {p0}, LX/9CF;->invalidateSelf()V

    .line 1453698
    return-void
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 1453687
    iget-object v0, p0, LX/9CF;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, LX/9CF;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1453688
    iget-object v0, p0, LX/9CF;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1453689
    iget-object v0, p0, LX/9CF;->d:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 1453690
    iget-object v0, p0, LX/9CF;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, LX/9CF;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1453691
    iget-object v0, p0, LX/9CF;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1453692
    :cond_0
    return-void
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 1453686
    iget-object v0, p0, LX/9CF;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    move-result v0

    return v0
.end method

.method public final invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 1453684
    invoke-virtual {p0}, LX/9CF;->invalidateSelf()V

    .line 1453685
    return-void
.end method

.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 1453725
    return-void
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1453680
    iput-object v0, p0, LX/9CF;->d:Landroid/graphics/drawable/Drawable;

    .line 1453681
    iput-object v0, p0, LX/9CF;->e:Landroid/animation/ValueAnimator;

    .line 1453682
    invoke-virtual {p0}, LX/9CF;->invalidateSelf()V

    .line 1453683
    return-void
.end method

.method public final onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 1453679
    return-void
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 1453677
    invoke-virtual {p0}, LX/9CF;->invalidateSelf()V

    .line 1453678
    return-void
.end method

.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2

    .prologue
    .line 1453673
    iget-object v0, p0, LX/9CF;->d:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 1453674
    iget-object v1, p0, LX/9CF;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1453675
    invoke-virtual {p0}, LX/9CF;->invalidateSelf()V

    .line 1453676
    :cond_0
    return-void
.end method

.method public final scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V
    .locals 1

    .prologue
    .line 1453665
    invoke-virtual {p0, p2, p3, p4}, LX/9CF;->scheduleSelf(Ljava/lang/Runnable;J)V

    .line 1453666
    return-void
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 1453671
    iget-object v0, p0, LX/9CF;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1453672
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 1453669
    iget-object v0, p0, LX/9CF;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 1453670
    return-void
.end method

.method public final unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 1453667
    invoke-virtual {p0, p2}, LX/9CF;->unscheduleSelf(Ljava/lang/Runnable;)V

    .line 1453668
    return-void
.end method
