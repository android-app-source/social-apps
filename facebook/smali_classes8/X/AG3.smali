.class public final LX/AG3;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 17

    .prologue
    .line 1651193
    const/4 v11, 0x0

    .line 1651194
    const/4 v10, 0x0

    .line 1651195
    const/4 v9, 0x0

    .line 1651196
    const/4 v8, 0x0

    .line 1651197
    const/4 v5, 0x0

    .line 1651198
    const-wide/16 v6, 0x0

    .line 1651199
    const/4 v4, 0x0

    .line 1651200
    const/4 v3, 0x0

    .line 1651201
    const/4 v2, 0x0

    .line 1651202
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->START_OBJECT:LX/15z;

    if-eq v12, v13, :cond_b

    .line 1651203
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1651204
    const/4 v2, 0x0

    .line 1651205
    :goto_0
    return v2

    .line 1651206
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_8

    .line 1651207
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1651208
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1651209
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v14, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v14, :cond_0

    if-eqz v2, :cond_0

    .line 1651210
    const-string v6, "backstage_message"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1651211
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v13, v2

    goto :goto_1

    .line 1651212
    :cond_1
    const-string v6, "backstage_post_type"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1651213
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v12, v2

    goto :goto_1

    .line 1651214
    :cond_2
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1651215
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v11, v2

    goto :goto_1

    .line 1651216
    :cond_3
    const-string v6, "post_media"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1651217
    invoke-static/range {p0 .. p1}, LX/AG2;->a(LX/15w;LX/186;)I

    move-result v2

    move v10, v2

    goto :goto_1

    .line 1651218
    :cond_4
    const-string v6, "seen_by_users"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1651219
    invoke-static/range {p0 .. p1}, LX/AGN;->a(LX/15w;LX/186;)I

    move-result v2

    move v7, v2

    goto :goto_1

    .line 1651220
    :cond_5
    const-string v6, "time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1651221
    const/4 v2, 0x1

    .line 1651222
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 1651223
    :cond_6
    const-string v6, "timezone_offset_seconds"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1651224
    const/4 v2, 0x1

    .line 1651225
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v8, v2

    move v9, v6

    goto/16 :goto_1

    .line 1651226
    :cond_7
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1651227
    :cond_8
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1651228
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1651229
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1651230
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1651231
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1651232
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1651233
    if-eqz v3, :cond_9

    .line 1651234
    const/4 v3, 0x5

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1651235
    :cond_9
    if-eqz v8, :cond_a

    .line 1651236
    const/4 v2, 0x6

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9, v3}, LX/186;->a(III)V

    .line 1651237
    :cond_a
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_b
    move v12, v10

    move v13, v11

    move v10, v8

    move v11, v9

    move v8, v2

    move v9, v4

    move-wide v15, v6

    move v7, v5

    move-wide v4, v15

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 1651238
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1651239
    invoke-virtual {p0, p1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1651240
    if-eqz v0, :cond_0

    .line 1651241
    const-string v1, "backstage_message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1651242
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1651243
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1651244
    if-eqz v0, :cond_1

    .line 1651245
    const-string v1, "backstage_post_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1651246
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1651247
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1651248
    if-eqz v0, :cond_2

    .line 1651249
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1651250
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1651251
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1651252
    if-eqz v0, :cond_3

    .line 1651253
    const-string v1, "post_media"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1651254
    invoke-static {p0, v0, p2, p3}, LX/AG2;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1651255
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1651256
    if-eqz v0, :cond_4

    .line 1651257
    const-string v1, "seen_by_users"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1651258
    invoke-static {p0, v0, p2, p3}, LX/AGN;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1651259
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1651260
    cmp-long v2, v0, v4

    if-eqz v2, :cond_5

    .line 1651261
    const-string v2, "time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1651262
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1651263
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1651264
    if-eqz v0, :cond_6

    .line 1651265
    const-string v1, "timezone_offset_seconds"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1651266
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1651267
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1651268
    return-void
.end method
