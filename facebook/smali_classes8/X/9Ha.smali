.class public LX/9Ha;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9Hd;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/9Ha",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/9Hd;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1461550
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1461551
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/9Ha;->b:LX/0Zi;

    .line 1461552
    iput-object p1, p0, LX/9Ha;->a:LX/0Ot;

    .line 1461553
    return-void
.end method

.method public static a(LX/0QB;)LX/9Ha;
    .locals 4

    .prologue
    .line 1461539
    const-class v1, LX/9Ha;

    monitor-enter v1

    .line 1461540
    :try_start_0
    sget-object v0, LX/9Ha;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1461541
    sput-object v2, LX/9Ha;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1461542
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1461543
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1461544
    new-instance v3, LX/9Ha;

    const/16 p0, 0x1dd5

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/9Ha;-><init>(LX/0Ot;)V

    .line 1461545
    move-object v0, v3

    .line 1461546
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1461547
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/9Ha;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1461548
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1461549
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/View;LX/9Fa;ZZLX/1X1;)V
    .locals 7

    .prologue
    .line 1461536
    check-cast p5, LX/9HZ;

    .line 1461537
    iget-object v0, p0, LX/9Ha;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Hd;

    iget-object v2, p5, LX/9HZ;->a:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v3, p5, LX/9HZ;->d:LX/1Pq;

    move-object v1, p1

    move-object v4, p2

    move v5, p3

    move v6, p4

    invoke-virtual/range {v0 .. v6}, LX/9Hd;->onClick(Landroid/view/View;Lcom/facebook/graphql/model/GraphQLComment;LX/1Pq;LX/9Fa;ZZ)V

    .line 1461538
    return-void
.end method

.method public static onClick(LX/1De;LX/9Fa;ZZ)LX/1dQ;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/9Fa;",
            "ZZ)",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1461535
    const v0, -0x622216d

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method

.method public static onClick(LX/1X1;LX/9Fa;ZZ)LX/1dQ;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            "LX/9Fa;",
            "ZZ)",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1461534
    const v0, -0x622216d

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 13

    .prologue
    .line 1461481
    check-cast p2, LX/9HZ;

    .line 1461482
    iget-object v0, p0, LX/9Ha;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Hd;

    iget-object v2, p2, LX/9HZ;->a:Lcom/facebook/graphql/model/GraphQLComment;

    iget-boolean v3, p2, LX/9HZ;->b:Z

    iget-object v4, p2, LX/9HZ;->c:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    iget-object v5, p2, LX/9HZ;->d:LX/1Pq;

    move-object v1, p1

    const/4 v11, 0x1

    const/4 v12, 0x0

    const/4 v9, 0x0

    .line 1461483
    move-object v6, v5

    check-cast v6, LX/1Pr;

    new-instance v7, LX/9FZ;

    invoke-direct {v7, v2}, LX/9FZ;-><init>(Lcom/facebook/graphql/model/GraphQLComment;)V

    invoke-interface {v6, v7, v2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/9Fa;

    .line 1461484
    iget-boolean v7, v6, LX/9Fa;->b:Z

    move v7, v7

    .line 1461485
    if-eqz v7, :cond_2

    .line 1461486
    iget-object v7, v6, LX/9Fa;->d:Ljava/lang/CharSequence;

    move-object v7, v7

    .line 1461487
    :goto_0
    new-instance v8, LX/9Hb;

    invoke-direct {v8, v0, v2, v5, v6}, LX/9Hb;-><init>(LX/9Hd;Lcom/facebook/graphql/model/GraphQLComment;LX/1Pq;LX/9Fa;)V

    .line 1461488
    iget-boolean v10, v6, LX/9Fa;->a:Z

    move v10, v10

    .line 1461489
    if-nez v10, :cond_0

    if-nez v3, :cond_3

    :cond_0
    move-object v8, v9

    .line 1461490
    :goto_1
    invoke-static {v8}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_4

    move v10, v11

    .line 1461491
    :goto_2
    invoke-static {v2}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    move-result-object p0

    .line 1461492
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p1

    const/4 p2, 0x2

    invoke-interface {p1, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object p1

    if-nez v10, :cond_5

    :goto_3
    invoke-static {v1, v6, v10, v11}, LX/9Ha;->onClick(LX/1De;LX/9Fa;ZZ)LX/1dQ;

    move-result-object v11

    invoke-interface {p1, v11}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object p1

    .line 1461493
    iget-boolean v11, v6, LX/9Fa;->b:Z

    move v11, v11

    .line 1461494
    if-nez v11, :cond_6

    move-object v11, v9

    :goto_4
    invoke-interface {p1, v11}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v11

    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p1

    invoke-interface {p1, v12}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v12

    const/high16 p1, 0x3f800000    # 1.0f

    invoke-interface {v12, p1}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v12

    if-eqz v10, :cond_7

    :goto_5
    const/4 v3, 0x0

    .line 1461495
    if-nez v8, :cond_b

    .line 1461496
    const/4 v7, 0x0

    .line 1461497
    :goto_6
    move-object v7, v7

    .line 1461498
    invoke-interface {v12, v7}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v8

    .line 1461499
    iget-boolean v7, v6, LX/9Fa;->b:Z

    move v7, v7

    .line 1461500
    if-nez v7, :cond_8

    move-object v7, v9

    :goto_7
    invoke-interface {v8, v7}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v8

    .line 1461501
    iget-boolean v7, v6, LX/9Fa;->c:Z

    move v7, v7

    .line 1461502
    if-nez v7, :cond_9

    move-object v7, v9

    :goto_8
    invoke-interface {v8, v7}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v7

    .line 1461503
    iget-boolean v8, v6, LX/9Fa;->b:Z

    move v8, v8

    .line 1461504
    if-nez v8, :cond_1

    invoke-static {p0}, LX/9Hd;->a(Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;)Z

    move-result v8

    if-nez v8, :cond_a

    :cond_1
    :goto_9
    invoke-interface {v7, v9}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v6

    invoke-interface {v11, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6}, LX/1Di;->k()LX/1Dg;

    move-result-object v6

    move-object v0, v6

    .line 1461505
    return-object v0

    .line 1461506
    :cond_2
    iget-object v7, v0, LX/9Hd;->a:LX/9Hj;

    invoke-virtual {v7, v2, v4}, LX/9Hj;->a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)Ljava/lang/CharSequence;

    move-result-object v7

    goto/16 :goto_0

    .line 1461507
    :cond_3
    iget-object v10, v0, LX/9Hd;->c:LX/1Uf;

    invoke-virtual {v10, v7, v8}, LX/1Uf;->a(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/text/Spannable;

    move-result-object v8

    goto :goto_1

    :cond_4
    move v10, v12

    .line 1461508
    goto :goto_2

    :cond_5
    move v11, v12

    .line 1461509
    goto :goto_3

    .line 1461510
    :cond_6
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v11

    const p2, 0x7f0a0042

    invoke-interface {v11, p2}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v11

    const/4 p2, 0x1

    invoke-interface {v11, p2}, LX/1Dh;->H(I)LX/1Dh;

    move-result-object v11

    const/4 p2, 0x5

    const/4 v2, 0x6

    invoke-interface {v11, p2, v2}, LX/1Dh;->r(II)LX/1Dh;

    move-result-object v11

    invoke-interface {v11}, LX/1Di;->k()LX/1Dg;

    move-result-object v11

    move-object v11, v11

    .line 1461511
    goto :goto_4

    :cond_7
    move-object v8, v7

    goto :goto_5

    .line 1461512
    :cond_8
    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v10

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->SEE_CONVERSION:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    if-ne p0, v7, :cond_d

    const v7, 0x7f081203

    :goto_a
    invoke-virtual {v10, v7}, LX/1ne;->h(I)LX/1ne;

    move-result-object v7

    const v10, 0x7f0b004e

    invoke-virtual {v7, v10}, LX/1ne;->q(I)LX/1ne;

    move-result-object v7

    const v10, 0x7f0a0042

    invoke-virtual {v7, v10}, LX/1ne;->n(I)LX/1ne;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->d()LX/1X1;

    move-result-object v7

    move-object v7, v7

    .line 1461513
    goto :goto_7

    .line 1461514
    :cond_9
    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v7

    const v10, 0x7f081204

    invoke-virtual {v7, v10}, LX/1ne;->h(I)LX/1ne;

    move-result-object v7

    const v10, 0x7f0b0050

    invoke-virtual {v7, v10}, LX/1ne;->q(I)LX/1ne;

    move-result-object v7

    const v10, 0x7f0a0042

    invoke-virtual {v7, v10}, LX/1ne;->n(I)LX/1ne;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->d()LX/1X1;

    move-result-object v7

    move-object v7, v7

    .line 1461515
    goto/16 :goto_8

    :cond_a
    const/4 v10, 0x1

    .line 1461516
    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v8

    const v9, 0x7f081035

    invoke-virtual {v8, v9}, LX/1ne;->h(I)LX/1ne;

    move-result-object v8

    const v9, 0x7f0b0050

    invoke-virtual {v8, v9}, LX/1ne;->q(I)LX/1ne;

    move-result-object v8

    const v9, 0x7f0a0042

    invoke-virtual {v8, v9}, LX/1ne;->n(I)LX/1ne;

    move-result-object v8

    const v9, 0x7f0a0159

    invoke-virtual {v8, v9}, LX/1ne;->v(I)LX/1ne;

    move-result-object v8

    const v9, 0x3fa66666    # 1.3f

    invoke-virtual {v8, v9}, LX/1ne;->h(F)LX/1ne;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v8

    invoke-virtual {v8}, LX/1X5;->c()LX/1Di;

    move-result-object v8

    invoke-static {v1, v6, v10, v10}, LX/9Ha;->onClick(LX/1De;LX/9Fa;ZZ)LX/1dQ;

    move-result-object v9

    invoke-interface {v8, v9}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v8

    const/4 v9, 0x2

    invoke-interface {v8, v10, v9}, LX/1Di;->d(II)LX/1Di;

    move-result-object v8

    const/4 v9, 0x5

    const/16 v10, 0xc

    invoke-interface {v8, v9, v10}, LX/1Di;->d(II)LX/1Di;

    move-result-object v8

    invoke-interface {v8}, LX/1Di;->k()LX/1Dg;

    move-result-object v8

    move-object v9, v8

    .line 1461517
    goto/16 :goto_9

    .line 1461518
    :cond_b
    new-instance p2, Landroid/text/SpannableStringBuilder;

    invoke-direct {p2, v8}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1461519
    iget-object v7, v0, LX/9Hd;->b:LX/1zC;

    invoke-virtual {v1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const p1, 0x7f0b0050

    invoke-virtual {v10, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    invoke-virtual {v7, p2, v10}, LX/1zC;->a(Landroid/text/Editable;I)Z

    .line 1461520
    invoke-virtual {p2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    iget v10, v0, LX/9Hd;->e:I

    if-ge v7, v10, :cond_c

    .line 1461521
    const p1, 0x7f0b0058

    .line 1461522
    const v10, 0x400ccccd    # 2.2f

    .line 1461523
    const-string v7, "sans-serif-thin"

    invoke-static {v7, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v7

    .line 1461524
    :goto_b
    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object p2

    invoke-virtual {p2, p1}, LX/1ne;->q(I)LX/1ne;

    move-result-object p1

    const p2, 0x7f0105cf

    invoke-virtual {p1, p2}, LX/1ne;->o(I)LX/1ne;

    move-result-object p1

    invoke-virtual {p1, v7}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v7

    invoke-virtual {v7, v10}, LX/1ne;->i(F)LX/1ne;

    move-result-object v7

    invoke-virtual {v7, v3}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->d()LX/1X1;

    move-result-object v7

    goto/16 :goto_6

    .line 1461525
    :cond_c
    const p1, 0x7f0b0050

    .line 1461526
    const v10, 0x3fa66666    # 1.3f

    .line 1461527
    sget-object v7, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    goto :goto_b

    :cond_d
    const v7, 0x7f081202

    goto/16 :goto_a
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1461528
    invoke-static {}, LX/1dS;->b()V

    .line 1461529
    iget v0, p1, LX/1dQ;->b:I

    .line 1461530
    packed-switch v0, :pswitch_data_0

    .line 1461531
    :goto_0
    return-object v6

    .line 1461532
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1461533
    iget-object v1, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v2, 0x0

    aget-object v2, v0, v2

    check-cast v2, LX/9Fa;

    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v3, 0x1

    aget-object v0, v0, v3

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v4, 0x2

    aget-object v0, v0, v4

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    iget-object v5, p1, LX/1dQ;->a:LX/1X1;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, LX/9Ha;->a(Landroid/view/View;LX/9Fa;ZZLX/1X1;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x622216d
        :pswitch_0
    .end packed-switch
.end method
