.class public LX/8wv;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/3II;
.implements LX/7DM;


# instance fields
.field public final A:Landroid/os/Handler;

.field public B:Landroid/os/AsyncTask;

.field public C:LX/1bf;

.field public D:Lcom/facebook/common/callercontext/CallerContext;

.field public E:LX/8wt;

.field public F:LX/3IC;

.field public G:Landroid/view/GestureDetector;

.field public H:Landroid/widget/TextView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private I:J

.field public J:I

.field public final K:Landroid/view/TextureView$SurfaceTextureListener;

.field public a:LX/1xG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1HI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0yc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/attachments/photos/ui/Is360PhotoSpecDisplayEnabled;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

.field public i:LX/1bf;

.field public j:LX/7DL;

.field public k:Lcom/facebook/drawee/view/GenericDraweeView;

.field public l:Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;

.field public m:Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;

.field public n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

.field public o:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

.field public p:LX/7Dj;

.field public q:LX/1ca;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation
.end field

.field public r:Ljava/lang/String;

.field public s:Z

.field public t:Z

.field public u:Z

.field public v:Z

.field public w:Z

.field public x:Z

.field public final y:Ljava/lang/Runnable;

.field public final z:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/7Co;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1423137
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/8wv;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1423138
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1423139
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/8wv;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1423140
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1423141
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1423142
    iput-boolean v1, p0, LX/8wv;->s:Z

    .line 1423143
    iput-boolean v1, p0, LX/8wv;->t:Z

    .line 1423144
    iput-boolean v1, p0, LX/8wv;->u:Z

    .line 1423145
    iput-boolean v1, p0, LX/8wv;->v:Z

    .line 1423146
    iput-boolean v1, p0, LX/8wv;->w:Z

    .line 1423147
    iput-boolean v1, p0, LX/8wv;->x:Z

    .line 1423148
    new-instance v0, Lcom/facebook/attachments/photos/ui/Photo360View$1;

    invoke-direct {v0, p0}, Lcom/facebook/attachments/photos/ui/Photo360View$1;-><init>(LX/8wv;)V

    iput-object v0, p0, LX/8wv;->y:Ljava/lang/Runnable;

    .line 1423149
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/8wv;->z:Ljava/util/List;

    .line 1423150
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/8wv;->A:Landroid/os/Handler;

    .line 1423151
    iput v1, p0, LX/8wv;->J:I

    .line 1423152
    new-instance v0, LX/8wp;

    invoke-direct {v0, p0}, LX/8wp;-><init>(LX/8wv;)V

    iput-object v0, p0, LX/8wv;->K:Landroid/view/TextureView$SurfaceTextureListener;

    .line 1423153
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/8wv;

    invoke-static {v0}, LX/1xG;->b(LX/0QB;)LX/1xG;

    move-result-object v3

    check-cast v3, LX/1xG;

    invoke-static {v0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v4

    check-cast v4, LX/1HI;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v5

    check-cast v5, LX/0Sh;

    invoke-static {v0}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object p1

    check-cast p1, LX/0yc;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object p2

    check-cast p2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p2}, LX/0eG;->c(Lcom/facebook/prefs/shared/FbSharedPreferences;)Ljava/lang/Boolean;

    move-result-object p2

    move-object p2, p2

    check-cast p2, Ljava/lang/Boolean;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object p3

    check-cast p3, LX/0tX;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    iput-object v3, v2, LX/8wv;->a:LX/1xG;

    iput-object v4, v2, LX/8wv;->b:LX/1HI;

    iput-object v5, v2, LX/8wv;->c:LX/0Sh;

    iput-object p1, v2, LX/8wv;->d:LX/0yc;

    iput-object p2, v2, LX/8wv;->e:Ljava/lang/Boolean;

    iput-object p3, v2, LX/8wv;->f:LX/0tX;

    iput-object v0, v2, LX/8wv;->g:LX/0SG;

    .line 1423154
    const v0, 0x7f031397

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1423155
    const v0, 0x7f0d2d43

    invoke-virtual {p0, v0}, LX/8wv;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    iput-object v0, p0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    .line 1423156
    const v0, 0x7f0d2d44

    invoke-virtual {p0, v0}, LX/8wv;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/view/GenericDraweeView;

    iput-object v0, p0, LX/8wv;->k:Lcom/facebook/drawee/view/GenericDraweeView;

    .line 1423157
    iget-object v0, p0, LX/8wv;->k:Lcom/facebook/drawee/view/GenericDraweeView;

    new-instance v1, LX/1Uo;

    invoke-virtual {p0}, LX/8wv;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, LX/8wv;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a045d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 1423158
    iput-object v2, v1, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 1423159
    move-object v1, v1

    .line 1423160
    invoke-virtual {p0}, LX/8wv;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020aa6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1423161
    iput-object v2, v1, LX/1Uo;->h:Landroid/graphics/drawable/Drawable;

    .line 1423162
    move-object v1, v1

    .line 1423163
    sget-object v2, LX/1Up;->h:LX/1Up;

    invoke-virtual {v1, v2}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v1

    invoke-virtual {v1}, LX/1Uo;->u()LX/1af;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 1423164
    iget-object v0, p0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    iget-object v1, p0, LX/8wv;->K:Landroid/view/TextureView$SurfaceTextureListener;

    invoke-virtual {v0, v1}, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 1423165
    iget-object v0, p0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/2qW;->setZoomInteractionEnabled(Z)V

    .line 1423166
    const v0, 0x7f0d2d46

    invoke-virtual {p0, v0}, LX/8wv;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    iput-object v0, p0, LX/8wv;->o:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    .line 1423167
    iget-object v0, p0, LX/8wv;->o:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    invoke-virtual {p0}, LX/8wv;->getIndicatorBottomMargin()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->setBasePhoneAndNuxOffset(I)V

    .line 1423168
    iget-object v0, p0, LX/8wv;->o:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    invoke-static {p0, v0}, LX/8wv;->a(LX/8wv;LX/7Co;)V

    .line 1423169
    const v0, 0x7f0d2d45

    invoke-virtual {p0, v0}, LX/8wv;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;

    iput-object v0, p0, LX/8wv;->l:Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;

    .line 1423170
    const v0, 0x7f0d2d47

    invoke-virtual {p0, v0}, LX/8wv;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;

    iput-object v0, p0, LX/8wv;->m:Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;

    .line 1423171
    iget-object v0, p0, LX/8wv;->m:Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;

    iget-object v1, p0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    .line 1423172
    iput-object v1, v0, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    .line 1423173
    iget-object v0, p0, LX/8wv;->m:Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;

    iget-object v1, p0, LX/8wv;->o:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    .line 1423174
    iput-object v1, v0, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->b:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    .line 1423175
    iget-object v0, p0, LX/8wv;->m:Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;

    invoke-static {p0, v0}, LX/8wv;->a(LX/8wv;LX/7Co;)V

    .line 1423176
    new-instance v0, LX/8wt;

    invoke-direct {v0, p0}, LX/8wt;-><init>(LX/8wv;)V

    iput-object v0, p0, LX/8wv;->E:LX/8wt;

    .line 1423177
    new-instance v0, LX/3IC;

    invoke-virtual {p0}, LX/8wv;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/8wv;->E:LX/8wt;

    iget-object v3, p0, LX/8wv;->E:LX/8wt;

    invoke-direct {v0, v1, v2, v3}, LX/3IC;-><init>(Landroid/content/Context;LX/3IA;LX/3IB;)V

    iput-object v0, p0, LX/8wv;->F:LX/3IC;

    .line 1423178
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, LX/8wv;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, LX/8wu;

    invoke-direct {v2, p0}, LX/8wu;-><init>(LX/8wv;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, LX/8wv;->G:Landroid/view/GestureDetector;

    .line 1423179
    iget-object v0, p0, LX/8wv;->e:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1423180
    const/4 v2, -0x2

    .line 1423181
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, LX/8wv;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/8wv;->H:Landroid/widget/TextView;

    .line 1423182
    iget-object v0, p0, LX/8wv;->H:Landroid/widget/TextView;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1423183
    iget-object v0, p0, LX/8wv;->H:Landroid/widget/TextView;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, LX/8wv;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a05d9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1423184
    iget-object v0, p0, LX/8wv;->H:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, LX/8wv;->addView(Landroid/view/View;)V

    .line 1423185
    :cond_0
    return-void
.end method

.method public static a(LX/8wv;LX/7Co;)V
    .locals 1

    .prologue
    .line 1423186
    iget-object v0, p0, LX/8wv;->z:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1423187
    :goto_0
    return-void

    .line 1423188
    :cond_0
    iget-object v0, p0, LX/8wv;->z:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static r(LX/8wv;)V
    .locals 13

    .prologue
    const/4 v4, -0x1

    .line 1423189
    iget-object v0, p0, LX/8wv;->g:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, LX/8wv;->I:J

    sub-long/2addr v0, v2

    long-to-int v12, v0

    .line 1423190
    iget-object v0, p0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    .line 1423191
    iget-object v1, v0, LX/2qW;->c:LX/7Cy;

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/2qW;->c:LX/7Cy;

    iget-object v1, v1, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    if-eqz v1, :cond_1

    .line 1423192
    iget-object v1, v0, LX/2qW;->c:LX/7Cy;

    iget-object v1, v1, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    .line 1423193
    iget-object v0, v1, Lcom/facebook/spherical/GlMediaRenderThread;->b:LX/7D0;

    invoke-interface {v0}, LX/7D0;->e()LX/7DD;

    move-result-object v0

    move-object v1, v0

    .line 1423194
    :goto_0
    move-object v11, v1

    .line 1423195
    iget-object v0, p0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v0}, LX/2qW;->getRenderMethod()LX/7DC;

    move-result-object v3

    .line 1423196
    if-eqz v11, :cond_0

    iget-object v0, p0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v0}, LX/2qW;->getRenderMethod()LX/7DC;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v0, v11, LX/7DD;->g:I

    if-eq v0, v4, :cond_0

    iget v0, v11, LX/7DD;->h:I

    if-eq v0, v4, :cond_0

    .line 1423197
    iget-object v0, p0, LX/8wv;->a:LX/1xG;

    iget-object v1, p0, LX/8wv;->r:Ljava/lang/String;

    iget-object v2, p0, LX/8wv;->p:LX/7Dj;

    iget v4, v11, LX/7DD;->a:F

    iget v5, v11, LX/7DD;->b:F

    iget v6, v11, LX/7DD;->c:I

    iget v7, v11, LX/7DD;->d:I

    iget v8, v11, LX/7DD;->e:I

    iget v9, v11, LX/7DD;->f:I

    iget v10, v11, LX/7DD;->g:I

    iget v11, v11, LX/7DD;->h:I

    invoke-virtual/range {v0 .. v12}, LX/1xG;->a(Ljava/lang/String;LX/7Dj;LX/7DC;FFIIIIIII)V

    .line 1423198
    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/facebook/spherical/photo/model/SphericalPhotoParams;Lcom/facebook/common/callercontext/CallerContext;Ljava/lang/String;LX/7Dj;)V
    .locals 2

    .prologue
    .line 1423199
    iput-object p1, p0, LX/8wv;->h:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    .line 1423200
    iput-object p2, p0, LX/8wv;->D:Lcom/facebook/common/callercontext/CallerContext;

    .line 1423201
    iput-object p4, p0, LX/8wv;->p:LX/7Dj;

    .line 1423202
    iput-object p3, p0, LX/8wv;->r:Ljava/lang/String;

    .line 1423203
    iget-object v0, p0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    iget-object v1, p0, LX/8wv;->h:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    invoke-virtual {v0, v1}, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->setSphericalPhotoParams(Lcom/facebook/spherical/photo/model/SphericalPhotoParams;)V

    .line 1423204
    iget-object v0, p0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    iget-object v1, p0, LX/8wv;->h:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    invoke-virtual {v1}, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->b()F

    move-result v1

    invoke-virtual {v0, v1}, LX/2qW;->setMaxVerticalFOV(F)V

    .line 1423205
    iget-object v0, p0, LX/8wv;->m:Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;

    if-eqz v0, :cond_0

    .line 1423206
    iget-object v0, p0, LX/8wv;->m:Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;

    iget-object v1, p0, LX/8wv;->h:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    .line 1423207
    iput-object v1, v0, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->c:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    .line 1423208
    iget-object v0, p0, LX/8wv;->m:Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;

    iget-object v1, p0, LX/8wv;->r:Ljava/lang/String;

    invoke-virtual {v0, v1, p4}, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->a(Ljava/lang/String;LX/7Dj;)V

    .line 1423209
    :cond_0
    iget-object v0, p0, LX/8wv;->h:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    .line 1423210
    iget-object v1, v0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->m:Lcom/facebook/spherical/photo/model/PhotoVRCastParams;

    move-object v0, v1

    .line 1423211
    iget-object v1, p0, LX/8wv;->l:Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;

    iget-object p1, p0, LX/8wv;->r:Ljava/lang/String;

    iget-object p2, p0, LX/8wv;->p:LX/7Dj;

    invoke-virtual {v1, v0, p1, p2}, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->a(Lcom/facebook/spherical/photo/model/PhotoVRCastParams;Ljava/lang/String;LX/7Dj;)V

    .line 1423212
    const/high16 p1, 0x43340000    # 180.0f

    const/high16 v1, 0x42b40000    # 90.0f

    .line 1423213
    iget-object v0, p0, LX/8wv;->h:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    .line 1423214
    iget-object p2, v0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->n:Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;

    move-object v0, p2

    .line 1423215
    if-nez v0, :cond_1

    .line 1423216
    new-instance v0, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;

    invoke-direct {v0, p1, p1, v1, v1}, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;-><init>(FFFF)V

    .line 1423217
    :cond_1
    new-instance v1, LX/7DF;

    invoke-direct {v1}, LX/7DF;-><init>()V

    .line 1423218
    invoke-virtual {v1}, LX/7DF;->b()LX/7DF;

    move-result-object p1

    .line 1423219
    iget p2, v0, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->d:F

    move p2, p2

    .line 1423220
    iput p2, p1, LX/7DF;->d:F

    .line 1423221
    move-object p1, p1

    .line 1423222
    iget p2, v0, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->c:F

    move p2, p2

    .line 1423223
    neg-float p2, p2

    .line 1423224
    iput p2, p1, LX/7DF;->c:F

    .line 1423225
    iget p1, v0, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->a:F

    move p1, p1

    .line 1423226
    iget p2, v0, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->b:F

    move p2, p2

    .line 1423227
    add-float/2addr p1, p2

    .line 1423228
    const/high16 p2, 0x43af0000    # 350.0f

    cmpg-float p1, p1, p2

    if-gez p1, :cond_2

    .line 1423229
    invoke-virtual {v1}, LX/7DF;->a()LX/7DF;

    move-result-object p1

    .line 1423230
    iget p2, v0, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->b:F

    move p2, p2

    .line 1423231
    iput p2, p1, LX/7DF;->b:F

    .line 1423232
    move-object p1, p1

    .line 1423233
    iget p2, v0, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->a:F

    move p2, p2

    .line 1423234
    neg-float p2, p2

    .line 1423235
    iput p2, p1, LX/7DF;->a:F

    .line 1423236
    :cond_2
    iget-object p1, p0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    iget-object p2, p0, LX/8wv;->h:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    iget-object p2, p2, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->k:LX/19o;

    invoke-virtual {p1, p2}, LX/2qW;->setProjectionType(LX/19o;)V

    .line 1423237
    iget-object p1, p0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v1}, LX/7DF;->c()LX/7DG;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/2qW;->setRendererBounds(LX/7DG;)V

    .line 1423238
    iget-object v1, p0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v1, v0}, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->setPanoBounds(Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;)V

    .line 1423239
    sget-object v0, LX/19o;->CUBESTRIP:LX/19o;

    iget-object v1, p0, LX/8wv;->h:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    .line 1423240
    iget-object p1, v1, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->k:LX/19o;

    move-object v1, p1

    .line 1423241
    if-ne v0, v1, :cond_3

    .line 1423242
    iget-object v0, p0, LX/8wv;->h:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    iget-object v0, v0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->l:Lcom/facebook/spherical/photo/utils/CubemapsUtil$CubemapUris;

    .line 1423243
    iget-object v1, v0, Lcom/facebook/spherical/photo/utils/CubemapsUtil$CubemapUris;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1423244
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v1

    invoke-virtual {v1}, LX/1bX;->n()LX/1bf;

    move-result-object v1

    iput-object v1, p0, LX/8wv;->i:LX/1bf;

    .line 1423245
    iget-object v1, v0, Lcom/facebook/spherical/photo/utils/CubemapsUtil$CubemapUris;->b:Ljava/lang/String;

    move-object v0, v1

    .line 1423246
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    iput-object v0, p0, LX/8wv;->C:LX/1bf;

    .line 1423247
    :cond_3
    iget-object v0, p0, LX/8wv;->o:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    iget-object v1, p0, LX/8wv;->r:Ljava/lang/String;

    invoke-virtual {v0, v1, p4}, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->a(Ljava/lang/String;LX/7Dj;)V

    .line 1423248
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1423249
    iget-object v0, p0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1423250
    iget-object v0, p0, LX/8wv;->c:LX/0Sh;

    new-instance v1, Lcom/facebook/attachments/photos/ui/Photo360View$3;

    invoke-direct {v1, p0}, Lcom/facebook/attachments/photos/ui/Photo360View$3;-><init>(LX/8wv;)V

    invoke-virtual {v0, v1}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    .line 1423251
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1423252
    iget-object v0, p0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v0}, LX/2qW;->d()V

    .line 1423253
    return-void
.end method

.method public f()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1423254
    iget-boolean v0, p0, LX/8wv;->t:Z

    if-eqz v0, :cond_0

    .line 1423255
    :goto_0
    return-void

    .line 1423256
    :cond_0
    iput-boolean v2, p0, LX/8wv;->w:Z

    .line 1423257
    iget-object v0, p0, LX/8wv;->k:Lcom/facebook/drawee/view/GenericDraweeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/GenericDraweeView;->setVisibility(I)V

    .line 1423258
    iget-object v0, p0, LX/8wv;->m:Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;

    if-eqz v0, :cond_1

    .line 1423259
    iget-object v0, p0, LX/8wv;->m:Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;

    invoke-virtual {v0}, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->b()V

    .line 1423260
    :cond_1
    iget-object v0, p0, LX/8wv;->o:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    invoke-virtual {v0}, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->g()V

    .line 1423261
    iput-boolean v2, p0, LX/8wv;->s:Z

    .line 1423262
    iget-object v0, p0, LX/8wv;->o:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    invoke-virtual {v0}, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->i()V

    .line 1423263
    invoke-virtual {p0}, LX/8wv;->g()V

    .line 1423264
    iget-object v0, p0, LX/8wv;->h:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/8wv;->h:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    .line 1423265
    iget-object v1, v0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->k:LX/19o;

    move-object v0, v1

    .line 1423266
    sget-object v1, LX/19o;->CUBESTRIP:LX/19o;

    if-ne v0, v1, :cond_2

    .line 1423267
    iget-object v0, p0, LX/8wv;->b:LX/1HI;

    iget-object v1, p0, LX/8wv;->C:LX/1bf;

    iget-object v2, p0, LX/8wv;->D:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v0

    .line 1423268
    new-instance v1, LX/8wr;

    invoke-direct {v1, p0}, LX/8wr;-><init>(LX/8wv;)V

    invoke-static {}, LX/44p;->b()LX/44p;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 1423269
    :cond_2
    invoke-virtual {p0}, LX/8wv;->p()V

    goto :goto_0
.end method

.method public final g()V
    .locals 7

    .prologue
    .line 1423270
    iget-object v0, p0, LX/8wv;->g:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, LX/8wv;->I:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    .line 1423271
    iget-object v1, p0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v1}, LX/2qW;->getRenderMethod()LX/7DC;

    move-result-object v1

    .line 1423272
    if-eqz v1, :cond_0

    .line 1423273
    iget-object v2, p0, LX/8wv;->a:LX/1xG;

    iget-object v3, p0, LX/8wv;->r:Ljava/lang/String;

    iget-object v4, p0, LX/8wv;->p:LX/7Dj;

    .line 1423274
    new-instance v5, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v6, "spherical_photo_renderer_setup"

    invoke-direct {v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1423275
    sget-object v6, LX/7Di;->DT:LX/7Di;

    iget-object v6, v6, LX/7Di;->value:Ljava/lang/String;

    invoke-virtual {v5, v6, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1423276
    sget-object v6, LX/7Di;->RENDER_METHOD:LX/7Di;

    iget-object v6, v6, LX/7Di;->value:Ljava/lang/String;

    invoke-virtual {v1}, LX/7DC;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v5, v6, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1423277
    invoke-static {v2, v5, v3, v4}, LX/1xG;->a(LX/1xG;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/7Dj;)V

    .line 1423278
    :cond_0
    return-void
.end method

.method public get360TextureView()LX/2qW;
    .locals 1

    .prologue
    .line 1423279
    iget-object v0, p0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    return-object v0
.end method

.method public getIndicatorBottomMargin()I
    .locals 1

    .prologue
    .line 1423133
    const/16 v0, 0xc

    return v0
.end method

.method public getShouldShowPhoneAnimationInFullScreen()Z
    .locals 1

    .prologue
    .line 1423134
    iget-object v0, p0, LX/8wv;->o:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    .line 1423135
    iget-boolean p0, v0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->f:Z

    move v0, p0

    .line 1423136
    return v0
.end method

.method public final h()V
    .locals 12

    .prologue
    .line 1423055
    iget-boolean v0, p0, LX/8wv;->u:Z

    if-nez v0, :cond_0

    .line 1423056
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8wv;->v:Z

    .line 1423057
    :goto_0
    return-void

    .line 1423058
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/8wv;->v:Z

    .line 1423059
    iget-object v0, p0, LX/8wv;->g:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/8wv;->I:J

    .line 1423060
    iget-object v0, p0, LX/8wv;->h:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/8wv;->h:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    .line 1423061
    iget-object v1, v0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->k:LX/19o;

    move-object v0, v1

    .line 1423062
    sget-object v1, LX/19o;->TILED_CUBEMAP:LX/19o;

    if-ne v0, v1, :cond_3

    .line 1423063
    new-instance v2, LX/7Da;

    iget-object v3, p0, LX/8wv;->b:LX/1HI;

    iget-object v4, p0, LX/8wv;->f:LX/0tX;

    iget-object v5, p0, LX/8wv;->D:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v6, p0, LX/8wv;->h:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    iget-object v6, v6, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->j:Ljava/lang/String;

    iget-object v7, p0, LX/8wv;->h:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    iget v7, v7, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->e:I

    invoke-direct/range {v2 .. v7}, LX/7Da;-><init>(LX/1HI;LX/0tX;Lcom/facebook/common/callercontext/CallerContext;Ljava/lang/String;I)V

    .line 1423064
    iget-object v3, p0, LX/8wv;->h:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    iget-object v3, v3, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->o:LX/0Px;

    .line 1423065
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/spherical/photo/model/PhotoTile;

    .line 1423066
    new-instance v6, LX/7DZ;

    iget-object v7, v4, Lcom/facebook/spherical/photo/model/PhotoTile;->e:Ljava/lang/String;

    invoke-direct {v6, v7}, LX/7DZ;-><init>(Ljava/lang/String;)V

    .line 1423067
    iget-object v7, v2, LX/7Da;->a:Ljava/util/Map;

    .line 1423068
    new-instance v8, LX/7D7;

    iget v9, v4, Lcom/facebook/spherical/photo/model/PhotoTile;->a:I

    iget v10, v4, Lcom/facebook/spherical/photo/model/PhotoTile;->b:I

    iget v11, v4, Lcom/facebook/spherical/photo/model/PhotoTile;->c:I

    iget v3, v4, Lcom/facebook/spherical/photo/model/PhotoTile;->d:I

    invoke-direct {v8, v9, v10, v11, v3}, LX/7D7;-><init>(IIII)V

    move-object v4, v8

    .line 1423069
    invoke-interface {v7, v4, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1423070
    :cond_1
    iget-object v3, p0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v3, v2}, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->setTileProvider(LX/7Da;)V

    .line 1423071
    invoke-virtual {p0}, LX/8wv;->l()V

    .line 1423072
    const/4 v4, 0x0

    .line 1423073
    move v3, v4

    :goto_2
    const/4 v5, 0x6

    if-ge v3, v5, :cond_2

    .line 1423074
    new-instance v5, LX/7D7;

    invoke-direct {v5, v4, v3, v4, v4}, LX/7D7;-><init>(IIII)V

    .line 1423075
    invoke-virtual {v2, v5}, LX/7Da;->a(LX/7D7;)V

    .line 1423076
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1423077
    :cond_2
    goto :goto_0

    .line 1423078
    :cond_3
    iget v0, p0, LX/8wv;->J:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/8wv;->J:I

    .line 1423079
    iget-object v1, p0, LX/8wv;->b:LX/1HI;

    iget-object v2, p0, LX/8wv;->i:LX/1bf;

    iget-object v3, p0, LX/8wv;->D:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, v3}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v1

    iput-object v1, p0, LX/8wv;->q:LX/1ca;

    .line 1423080
    iget-object v1, p0, LX/8wv;->q:LX/1ca;

    new-instance v2, LX/8wq;

    invoke-direct {v2, p0, v0}, LX/8wq;-><init>(LX/8wv;I)V

    invoke-static {}, LX/44p;->b()LX/44p;

    move-result-object v0

    invoke-interface {v1, v2, v0}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 1423081
    goto/16 :goto_0
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 1423088
    iget-boolean v0, p0, LX/8wv;->w:Z

    if-eqz v0, :cond_0

    .line 1423089
    iget-object v0, p0, LX/8wv;->o:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    invoke-virtual {v0}, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->i()V

    .line 1423090
    :cond_0
    return-void
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 1423091
    iget-object v0, p0, LX/8wv;->o:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    invoke-virtual {v0}, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->j()V

    .line 1423092
    return-void
.end method

.method public final k()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1423093
    iget-object v0, p0, LX/8wv;->k:Lcom/facebook/drawee/view/GenericDraweeView;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/GenericDraweeView;->setVisibility(I)V

    .line 1423094
    iget-object v0, p0, LX/8wv;->o:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    invoke-virtual {v0, v1}, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->setVisibility(I)V

    .line 1423095
    iget-object v0, p0, LX/8wv;->o:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    invoke-virtual {v0}, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->d()V

    .line 1423096
    iget-object v0, p0, LX/8wv;->o:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    invoke-virtual {v0}, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->b()V

    .line 1423097
    return-void
.end method

.method public final l()V
    .locals 6

    .prologue
    .line 1423098
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8wv;->x:Z

    .line 1423099
    iget-object v0, p0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v0, p0}, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->setHasSphericalPhoto(LX/7DM;)V

    .line 1423100
    iget-object v0, p0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v0}, LX/2qW;->a()V

    .line 1423101
    iget-object v1, p0, LX/8wv;->A:Landroid/os/Handler;

    iget-object v2, p0, LX/8wv;->y:Ljava/lang/Runnable;

    const-wide/16 v3, 0x96

    const v5, -0xb0dd1a3

    invoke-static {v1, v2, v3, v4, v5}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1423102
    return-void
.end method

.method public final m()V
    .locals 1

    .prologue
    .line 1423103
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8wv;->v:Z

    .line 1423104
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/8wv;->x:Z

    .line 1423105
    iget-object v0, p0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v0}, LX/2qW;->c()V

    .line 1423106
    return-void
.end method

.method public final n()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1423082
    iput-boolean v0, p0, LX/8wv;->x:Z

    .line 1423083
    iput-boolean v0, p0, LX/8wv;->w:Z

    .line 1423084
    iput-boolean v0, p0, LX/8wv;->v:Z

    .line 1423085
    invoke-static {p0}, LX/8wv;->r(LX/8wv;)V

    .line 1423086
    iget-object v0, p0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v0}, LX/2qW;->d()V

    .line 1423087
    return-void
.end method

.method public onMeasure(II)V
    .locals 1

    .prologue
    .line 1423107
    iget-boolean v0, p0, LX/8wv;->t:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8wv;->d:LX/0yc;

    invoke-virtual {v0}, LX/0yc;->d()I

    move-result p2

    .line 1423108
    :cond_0
    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {p2, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-super {p0, p1, v0}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    .line 1423109
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x62352440

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1423110
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_0

    .line 1423111
    iget-object v2, p0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v2, v0}, LX/2qW;->a(Z)V

    .line 1423112
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v0, :cond_1

    .line 1423113
    iget-object v2, p0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/2qW;->a(Z)V

    .line 1423114
    :cond_1
    iget-object v2, p0, LX/8wv;->G:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    .line 1423115
    if-eqz v2, :cond_2

    .line 1423116
    const v2, 0x5ba90a61

    invoke-static {v4, v4, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1423117
    :goto_0
    return v0

    .line 1423118
    :cond_2
    iget-boolean v0, p0, LX/8wv;->s:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/8wv;->m:Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;

    if-eqz v0, :cond_3

    .line 1423119
    iget-object v0, p0, LX/8wv;->m:Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;

    invoke-virtual {v0}, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->d()V

    .line 1423120
    :cond_3
    iget-object v0, p0, LX/8wv;->F:LX/3IC;

    invoke-virtual {v0, p1}, LX/3IC;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, 0x1037b51b

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public final p()V
    .locals 3

    .prologue
    .line 1423121
    iget-object v0, p0, LX/8wv;->e:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8wv;->H:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 1423122
    iget-object v0, p0, LX/8wv;->H:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Field of View: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v2}, LX/2qW;->getFov()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1423123
    :cond_0
    return-void
.end method

.method public setHasFullScreenUFI(LX/7DL;)V
    .locals 0

    .prologue
    .line 1423124
    iput-object p1, p0, LX/8wv;->j:LX/7DL;

    .line 1423125
    return-void
.end method

.method public setPreviewPhotoDraweeController(LX/1aZ;)V
    .locals 1

    .prologue
    .line 1423126
    iget-object v0, p0, LX/8wv;->k:Lcom/facebook/drawee/view/GenericDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1423127
    return-void
.end method

.method public setRotatedRenderAxis(LX/7Cm;)V
    .locals 1

    .prologue
    .line 1423128
    iget-object v0, p0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    if-eqz v0, :cond_0

    .line 1423129
    iget-object v0, p0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v0, p1}, LX/2qW;->setRenderAxisRotation(LX/7Cm;)V

    .line 1423130
    :cond_0
    return-void
.end method

.method public setShouldShowPhoneAnimationInFullScreen(Z)V
    .locals 1

    .prologue
    .line 1423131
    iget-object v0, p0, LX/8wv;->o:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    invoke-virtual {v0, p1}, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->setShouldShowPhoneAnimationInFullScreen(Z)V

    .line 1423132
    return-void
.end method
