.class public final LX/AND;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/cameracore/ui/CameraCoreFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V
    .locals 0

    .prologue
    .line 1667532
    iput-object p1, p0, LX/AND;->a:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x1d8aabe8

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1667533
    iget-object v1, p0, LX/AND;->a:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    const/4 v4, 0x0

    .line 1667534
    iget-boolean v3, v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->X:Z

    if-nez v3, :cond_1

    const/4 v3, 0x1

    :goto_0
    iput-boolean v3, v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->X:Z

    .line 1667535
    iget-boolean v3, v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->X:Z

    if-eqz v3, :cond_2

    .line 1667536
    iget-object v3, v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->S:Lcom/facebook/fbui/glyph/GlyphButton;

    const v5, 0x7f020818

    invoke-virtual {v3, v5}, Lcom/facebook/fbui/glyph/GlyphButton;->setImageResource(I)V

    .line 1667537
    iget-object v3, v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->Y:LX/ANt;

    const/4 p1, 0x0

    .line 1667538
    iget-object v5, v3, LX/ANt;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v5, p1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 1667539
    iget-object v5, v3, LX/ANt;->p:LX/ANo;

    if-eqz v5, :cond_0

    .line 1667540
    iget-object v5, v3, LX/ANt;->p:LX/ANo;

    iget-object p0, v3, LX/ANt;->h:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

    invoke-interface {v5, p0}, LX/ANo;->a(Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;)V

    .line 1667541
    iget-object v5, v3, LX/ANt;->k:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v5, p1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 1667542
    :cond_0
    iget-object v3, v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->T:Landroid/view/ViewGroup;

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1667543
    :goto_1
    const v1, -0x31c7fe45

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_1
    move v3, v4

    .line 1667544
    goto :goto_0

    .line 1667545
    :cond_2
    iget-object v3, v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->S:Lcom/facebook/fbui/glyph/GlyphButton;

    const v4, 0x7f020919

    invoke-virtual {v3, v4}, Lcom/facebook/fbui/glyph/GlyphButton;->setImageResource(I)V

    .line 1667546
    iget-object v3, v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->Y:LX/ANt;

    const/16 p0, 0x8

    .line 1667547
    iget-object v4, v3, LX/ANt;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v4, p0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 1667548
    iget-object v4, v3, LX/ANt;->p:LX/ANo;

    if-eqz v4, :cond_3

    .line 1667549
    iget-object v4, v3, LX/ANt;->p:LX/ANo;

    iget-object v5, v3, LX/ANt;->h:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

    invoke-interface {v4, v5}, LX/ANo;->b(Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;)V

    .line 1667550
    :cond_3
    iget-object v4, v3, LX/ANt;->k:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v4, p0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 1667551
    iget-object v3, v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->T:Landroid/view/ViewGroup;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_1
.end method
