.class public final LX/9El;
.super LX/451;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/451",
        "<",
        "LX/6BE",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        ">;>;"
    }
.end annotation


# instance fields
.field public a:LX/0Ve;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ve",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic b:Lcom/facebook/ufiservices/flyout/FeedbackParams;

.field public final synthetic c:[LX/9Bw;

.field public final synthetic d:Lcom/facebook/feedback/ui/SingletonFeedbackController;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/SingletonFeedbackController;Lcom/facebook/ufiservices/flyout/FeedbackParams;[LX/9Bw;)V
    .locals 3

    .prologue
    .line 1457352
    iput-object p1, p0, LX/9El;->d:Lcom/facebook/feedback/ui/SingletonFeedbackController;

    iput-object p2, p0, LX/9El;->b:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    iput-object p3, p0, LX/9El;->c:[LX/9Bw;

    invoke-direct {p0}, LX/451;-><init>()V

    .line 1457353
    iget-object v0, p0, LX/9El;->d:Lcom/facebook/feedback/ui/SingletonFeedbackController;

    iget-object v1, p0, LX/9El;->b:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    iget-object v2, p0, LX/9El;->c:[LX/9Bw;

    invoke-static {v0, v1, v2}, Lcom/facebook/feedback/ui/SingletonFeedbackController;->a$redex0(Lcom/facebook/feedback/ui/SingletonFeedbackController;Lcom/facebook/ufiservices/flyout/FeedbackParams;[LX/9Bw;)LX/0Ve;

    move-result-object v0

    iput-object v0, p0, LX/9El;->a:LX/0Ve;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1457356
    check-cast p1, LX/6BE;

    .line 1457357
    iget-object v0, p0, LX/9El;->a:LX/0Ve;

    .line 1457358
    iget-object v1, p1, LX/6BE;->a:Ljava/lang/Object;

    move-object v1, v1

    .line 1457359
    invoke-interface {v0, v1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 1457360
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1457354
    iget-object v0, p0, LX/9El;->a:LX/0Ve;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 1457355
    return-void
.end method
