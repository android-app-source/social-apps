.class public final LX/9Sq;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1494685
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1494686
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1494687
    :goto_0
    return v1

    .line 1494688
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1494689
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_3

    .line 1494690
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1494691
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1494692
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 1494693
    const-string v3, "nodes"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1494694
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1494695
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->START_ARRAY:LX/15z;

    if-ne v2, v3, :cond_2

    .line 1494696
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_ARRAY:LX/15z;

    if-eq v2, v3, :cond_2

    .line 1494697
    invoke-static {p0, p1}, LX/9Sp;->b(LX/15w;LX/186;)I

    move-result v2

    .line 1494698
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1494699
    :cond_2
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    move v0, v0

    .line 1494700
    goto :goto_1

    .line 1494701
    :cond_3
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1494702
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1494703
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1494704
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1494705
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1494706
    if-eqz v0, :cond_1

    .line 1494707
    const-string v1, "nodes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1494708
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1494709
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result p1

    if-ge v1, p1, :cond_0

    .line 1494710
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result p1

    invoke-static {p0, p1, p2}, LX/9Sp;->a(LX/15i;ILX/0nX;)V

    .line 1494711
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1494712
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1494713
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1494714
    return-void
.end method
