.class public final LX/9bT;
.super LX/1ci;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1ci",
        "<",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/support/v4/app/DialogFragment;

.field public final synthetic b:Landroid/support/v4/app/FragmentActivity;

.field public final synthetic c:J

.field public final synthetic d:J

.field public final synthetic e:Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;Landroid/support/v4/app/DialogFragment;Landroid/support/v4/app/FragmentActivity;JJ)V
    .locals 0

    .prologue
    .line 1514947
    iput-object p1, p0, LX/9bT;->e:Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    iput-object p2, p0, LX/9bT;->a:Landroid/support/v4/app/DialogFragment;

    iput-object p3, p0, LX/9bT;->b:Landroid/support/v4/app/FragmentActivity;

    iput-wide p4, p0, LX/9bT;->c:J

    iput-wide p6, p0, LX/9bT;->d:J

    invoke-direct {p0}, LX/1ci;-><init>()V

    return-void
.end method


# virtual methods
.method public final e(LX/1ca;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1514948
    invoke-interface {p1}, LX/1ca;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1514949
    :goto_0
    return-void

    .line 1514950
    :cond_0
    invoke-interface {p1}, LX/1ca;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    .line 1514951
    if-eqz v0, :cond_1

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, LX/1lm;

    if-nez v1, :cond_2

    .line 1514952
    :cond_1
    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_0

    .line 1514953
    :cond_2
    new-instance v1, LX/9bS;

    invoke-direct {v1, p0}, LX/9bS;-><init>(LX/9bT;)V

    .line 1514954
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_3

    .line 1514955
    iget-object v2, p0, LX/9bT;->e:Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    iget-object v2, v2, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->h:Ljava/util/concurrent/ExecutorService;

    new-array v3, v5, [LX/1FJ;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, LX/3nE;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 1514956
    :cond_3
    new-array v2, v5, [LX/1FJ;

    aput-object v0, v2, v4

    invoke-virtual {v1, v2}, LX/3nE;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public final f(LX/1ca;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1514957
    iget-object v0, p0, LX/9bT;->b:Landroid/support/v4/app/FragmentActivity;

    .line 1514958
    const p0, 0x7f081200

    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p0

    invoke-virtual {p0}, Landroid/widget/Toast;->show()V

    .line 1514959
    return-void
.end method
