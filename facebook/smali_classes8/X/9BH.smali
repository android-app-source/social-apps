.class public final LX/9BH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:LX/1Cw;

.field public final synthetic b:LX/1zt;

.field public final synthetic c:LX/9BI;


# direct methods
.method public constructor <init>(LX/9BI;LX/1Cw;LX/1zt;)V
    .locals 0

    .prologue
    .line 1451475
    iput-object p1, p0, LX/9BH;->c:LX/9BI;

    iput-object p2, p0, LX/9BH;->a:LX/1Cw;

    iput-object p3, p0, LX/9BH;->b:LX/1zt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1451476
    iget-object v0, p0, LX/9BH;->a:LX/1Cw;

    invoke-interface {v0, p3}, LX/1Cw;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 1451477
    iget-object v1, p0, LX/9BH;->c:LX/9BI;

    iget-object v1, v1, LX/9BI;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    invoke-virtual {v1, v0}, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->a(Ljava/lang/Object;)V

    .line 1451478
    check-cast v0, LX/9Al;

    .line 1451479
    iget-object v1, v0, LX/9Al;->a:Lcom/facebook/graphql/model/GraphQLActor;

    move-object v0, v1

    .line 1451480
    iget-object v1, p0, LX/9BH;->c:LX/9BI;

    iget-object v1, v1, LX/9BI;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v1, v1, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->d:LX/82m;

    iget-object v2, p0, LX/9BH;->b:LX/1zt;

    .line 1451481
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object p0

    const-string p1, "reactor_subtitle"

    .line 1451482
    sget-object p2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->E()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ay()I

    move-result p2

    if-lez p2, :cond_0

    .line 1451483
    const-string p2, "unread_count"

    .line 1451484
    :goto_0
    move-object p2, p2

    .line 1451485
    invoke-virtual {p0, p1, p2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object p0

    const-string p1, "reactor_is_friend"

    sget-object p2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->E()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result p2

    invoke-virtual {p0, p1, p2}, LX/1rQ;->a(Ljava/lang/String;Z)LX/1rQ;

    move-result-object p0

    .line 1451486
    iget-object p1, v1, LX/82m;->b:LX/0if;

    sget-object p2, LX/82m;->a:LX/0ih;

    const-string p3, "profile_click"

    new-instance p4, Ljava/lang/StringBuilder;

    const-string p5, "reaction_"

    invoke-direct {p4, p5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1451487
    iget p5, v2, LX/1zt;->e:I

    move p5, p5

    .line 1451488
    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object p4

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p1, p2, p3, p4, p0}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 1451489
    return-void

    .line 1451490
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aa()Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    move-result-object p2

    if-eqz p2, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aa()Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;->a()I

    move-result p2

    if-lez p2, :cond_1

    .line 1451491
    const-string p2, "mutual_friends_count"

    goto :goto_0

    .line 1451492
    :cond_1
    const-string p2, "none"

    goto :goto_0
.end method
