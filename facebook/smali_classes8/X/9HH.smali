.class public LX/9HH;
.super LX/9Gx;
.source ""


# instance fields
.field public final b:Landroid/widget/TextView;

.field public final c:Landroid/view/View;

.field public final d:Landroid/widget/TextView;

.field public final e:Landroid/view/View;

.field public final f:Landroid/widget/TextView;

.field public final g:Landroid/view/View;

.field public final h:Landroid/widget/TextView;

.field public final i:Landroid/view/View;

.field public final j:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1460999
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/9HH;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1461000
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1461001
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/9HH;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1461002
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1461003
    invoke-direct {p0, p1, p2, p3}, LX/9Gx;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1461004
    const v0, 0x7f0302e4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1461005
    const v0, 0x7f0d09f6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/9HH;->b:Landroid/widget/TextView;

    .line 1461006
    const v0, 0x7f0d09f7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/9HH;->c:Landroid/view/View;

    .line 1461007
    const v0, 0x7f0d09f8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/9HH;->d:Landroid/widget/TextView;

    .line 1461008
    const v0, 0x7f0d09f9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/9HH;->e:Landroid/view/View;

    .line 1461009
    const v0, 0x7f0d09fa

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/9HH;->f:Landroid/widget/TextView;

    .line 1461010
    const v0, 0x7f0d09fb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/9HH;->g:Landroid/view/View;

    .line 1461011
    const v0, 0x7f0d09fc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/9HH;->h:Landroid/widget/TextView;

    .line 1461012
    const v0, 0x7f0d09fe

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/9HH;->i:Landroid/view/View;

    .line 1461013
    const v0, 0x7f0d09fd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/9HH;->j:Landroid/view/View;

    .line 1461014
    return-void
.end method

.method public static a(Landroid/view/View;Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1461015
    invoke-static {p2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1461016
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1461017
    :goto_0
    return-void

    .line 1461018
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1461019
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
