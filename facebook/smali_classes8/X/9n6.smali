.class public final LX/9n6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/Choreographer$FrameCallback;


# instance fields
.field public final synthetic a:LX/9n7;

.field private final b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/facebook/react/bridge/ExecutorToken;",
            "LX/5pD;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/9n7;)V
    .locals 1

    .prologue
    .line 1536699
    iput-object p1, p0, LX/9n6;->a:LX/9n7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1536700
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/9n6;->b:Ljava/util/HashMap;

    return-void
.end method

.method public synthetic constructor <init>(LX/9n7;B)V
    .locals 0

    .prologue
    .line 1536698
    invoke-direct {p0, p1}, LX/9n6;-><init>(LX/9n7;)V

    return-void
.end method


# virtual methods
.method public final doFrame(J)V
    .locals 10

    .prologue
    .line 1536672
    iget-object v0, p0, LX/9n6;->a:LX/9n7;

    iget-object v0, v0, LX/9n7;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9n6;->a:LX/9n7;

    iget-object v0, v0, LX/9n7;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1536673
    :goto_0
    return-void

    .line 1536674
    :cond_0
    const-wide/32 v0, 0xf4240

    div-long v2, p1, v0

    .line 1536675
    iget-object v0, p0, LX/9n6;->a:LX/9n7;

    iget-object v4, v0, LX/9n7;->b:Ljava/lang/Object;

    monitor-enter v4

    .line 1536676
    :cond_1
    :goto_1
    :try_start_0
    iget-object v0, p0, LX/9n6;->a:LX/9n7;

    iget-object v0, v0, LX/9n7;->d:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, LX/9n6;->a:LX/9n7;

    iget-object v0, v0, LX/9n7;->d:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9n5;

    iget-wide v0, v0, LX/9n5;->e:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_4

    .line 1536677
    iget-object v0, p0, LX/9n6;->a:LX/9n7;

    iget-object v0, v0, LX/9n7;->d:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9n5;

    .line 1536678
    iget-object v1, p0, LX/9n6;->b:Ljava/util/HashMap;

    iget-object v5, v0, LX/9n5;->a:Lcom/facebook/react/bridge/ExecutorToken;

    invoke-virtual {v1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/5pD;

    .line 1536679
    if-nez v1, :cond_2

    .line 1536680
    invoke-static {}, LX/5op;->a()LX/5pD;

    move-result-object v1

    .line 1536681
    iget-object v5, p0, LX/9n6;->b:Ljava/util/HashMap;

    iget-object v6, v0, LX/9n5;->a:Lcom/facebook/react/bridge/ExecutorToken;

    invoke-virtual {v5, v6, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1536682
    :cond_2
    iget v5, v0, LX/9n5;->b:I

    invoke-interface {v1, v5}, LX/5pD;->pushInt(I)V

    .line 1536683
    iget-boolean v1, v0, LX/9n5;->c:Z

    if-eqz v1, :cond_3

    .line 1536684
    iget v1, v0, LX/9n5;->d:I

    int-to-long v6, v1

    add-long/2addr v6, v2

    .line 1536685
    iput-wide v6, v0, LX/9n5;->e:J

    .line 1536686
    iget-object v1, p0, LX/9n6;->a:LX/9n7;

    iget-object v1, v1, LX/9n7;->d:Ljava/util/PriorityQueue;

    invoke-virtual {v1, v0}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1536687
    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1536688
    :cond_3
    :try_start_1
    iget-object v1, p0, LX/9n6;->a:LX/9n7;

    iget-object v1, v1, LX/9n7;->e:Ljava/util/Map;

    iget-object v5, v0, LX/9n5;->a:Lcom/facebook/react/bridge/ExecutorToken;

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/SparseArray;

    .line 1536689
    if-eqz v1, :cond_1

    .line 1536690
    iget v5, v0, LX/9n5;->b:I

    invoke-virtual {v1, v5}, Landroid/util/SparseArray;->remove(I)V

    .line 1536691
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 1536692
    iget-object v1, p0, LX/9n6;->a:LX/9n7;

    iget-object v1, v1, LX/9n7;->e:Ljava/util/Map;

    iget-object v0, v0, LX/9n5;->a:Lcom/facebook/react/bridge/ExecutorToken;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1536693
    :cond_4
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1536694
    iget-object v0, p0, LX/9n6;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1536695
    iget-object v1, p0, LX/9n6;->a:LX/9n7;

    invoke-static {v1}, LX/9n7;->f(LX/9n7;)LX/5pY;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/react/bridge/ExecutorToken;

    const-class v4, Lcom/facebook/react/modules/core/JSTimersExecution;

    invoke-virtual {v3, v1, v4}, LX/5pX;->a(Lcom/facebook/react/bridge/ExecutorToken;Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v1

    check-cast v1, Lcom/facebook/react/modules/core/JSTimersExecution;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5pD;

    invoke-interface {v1, v0}, Lcom/facebook/react/modules/core/JSTimersExecution;->callTimers(LX/5pD;)V

    goto :goto_2

    .line 1536696
    :cond_5
    iget-object v0, p0, LX/9n6;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 1536697
    iget-object v0, p0, LX/9n6;->a:LX/9n7;

    iget-object v0, v0, LX/9n7;->k:LX/5r6;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5r6;

    sget-object v1, LX/5r4;->TIMERS_EVENTS:LX/5r4;

    invoke-virtual {v0, v1, p0}, LX/5r6;->a(LX/5r4;Landroid/view/Choreographer$FrameCallback;)V

    goto/16 :goto_0
.end method
