.class public LX/9V2;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1498862
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 1498863
    return-void
.end method

.method public static a(Landroid/content/Context;LX/0pi;LX/0pq;)LX/1Gf;
    .locals 5
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation build Lcom/facebook/livephotos/downloader/LivePhotosFileCache;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 1498865
    invoke-static {p0}, LX/1Gf;->a(Landroid/content/Context;)LX/1Gg;

    move-result-object v0

    const/4 v1, 0x1

    .line 1498866
    iput v1, v0, LX/1Gg;->a:I

    .line 1498867
    move-object v0, v0

    .line 1498868
    const-string v1, "LivePhotos"

    .line 1498869
    iput-object v1, v0, LX/1Gg;->b:Ljava/lang/String;

    .line 1498870
    move-object v0, v0

    .line 1498871
    new-instance v1, LX/9V1;

    invoke-direct {v1, p0}, LX/9V1;-><init>(Landroid/content/Context;)V

    .line 1498872
    iput-object v1, v0, LX/1Gg;->c:LX/1Gd;

    .line 1498873
    move-object v0, v0

    .line 1498874
    const-wide/32 v2, 0x400000

    .line 1498875
    iput-wide v2, v0, LX/1Gg;->f:J

    .line 1498876
    move-object v0, v0

    .line 1498877
    const-wide/32 v2, 0xa00000

    .line 1498878
    iput-wide v2, v0, LX/1Gg;->e:J

    .line 1498879
    move-object v0, v0

    .line 1498880
    const-wide/32 v2, 0xf00000

    .line 1498881
    iput-wide v2, v0, LX/1Gg;->d:J

    .line 1498882
    move-object v0, v0

    .line 1498883
    const-string v1, "LivePhotos_file"

    invoke-virtual {p1, v1}, LX/0pi;->a(Ljava/lang/String;)LX/1GE;

    move-result-object v1

    .line 1498884
    iput-object v1, v0, LX/1Gg;->i:LX/1GE;

    .line 1498885
    move-object v0, v0

    .line 1498886
    iput-object p2, v0, LX/1Gg;->j:LX/0pr;

    .line 1498887
    move-object v0, v0

    .line 1498888
    invoke-virtual {v0}, LX/1Gg;->a()LX/1Gf;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/1Gf;LX/1Ft;)LX/1Hk;
    .locals 14
    .param p0    # LX/1Gf;
        .annotation build Lcom/facebook/livephotos/downloader/LivePhotosFileCache;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation build Lcom/facebook/livephotos/downloader/LivePhotosFileCache;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 1498889
    new-instance v0, LX/1Hf;

    invoke-direct {v0}, LX/1Hf;-><init>()V

    invoke-virtual {v0, p0}, LX/1Hf;->a(LX/1Gf;)LX/1Hh;

    move-result-object v0

    .line 1498890
    new-instance v1, LX/1Hj;

    .line 1498891
    iget-wide v12, p0, LX/1Gf;->f:J

    move-wide v2, v12

    .line 1498892
    iget-wide v12, p0, LX/1Gf;->e:J

    move-wide v4, v12

    .line 1498893
    iget-wide v12, p0, LX/1Gf;->d:J

    move-wide v6, v12

    .line 1498894
    invoke-direct/range {v1 .. v7}, LX/1Hj;-><init>(JJJ)V

    .line 1498895
    new-instance v2, LX/1Hk;

    .line 1498896
    iget-object v3, p0, LX/1Gf;->g:LX/1GU;

    move-object v4, v3

    .line 1498897
    iget-object v3, p0, LX/1Gf;->i:LX/1GE;

    move-object v6, v3

    .line 1498898
    iget-object v3, p0, LX/1Gf;->h:LX/1GQ;

    move-object v7, v3

    .line 1498899
    iget-object v3, p0, LX/1Gf;->j:LX/0pr;

    move-object v8, v3

    .line 1498900
    iget-object v3, p0, LX/1Gf;->k:Landroid/content/Context;

    move-object v9, v3

    .line 1498901
    invoke-interface {p1}, LX/1Ft;->a()Ljava/util/concurrent/Executor;

    move-result-object v10

    const/4 v11, 0x0

    move-object v3, v0

    move-object v5, v1

    invoke-direct/range {v2 .. v11}, LX/1Hk;-><init>(LX/1Hh;LX/1GU;LX/1Hj;LX/1GE;LX/1GQ;LX/0pr;Landroid/content/Context;Ljava/util/concurrent/Executor;Z)V

    return-object v2
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 1498864
    return-void
.end method
