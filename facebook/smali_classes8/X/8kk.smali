.class public final enum LX/8kk;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8kk;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8kk;

.field public static final enum DOWNLOADED:LX/8kk;

.field public static final enum DOWNLOAD_PREVIEW:LX/8kk;

.field public static final enum PROMOTED:LX/8kk;

.field public static final enum PULSING_DOWNLOAD_PREVIEW:LX/8kk;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1396769
    new-instance v0, LX/8kk;

    const-string v1, "DOWNLOADED"

    invoke-direct {v0, v1, v2}, LX/8kk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8kk;->DOWNLOADED:LX/8kk;

    .line 1396770
    new-instance v0, LX/8kk;

    const-string v1, "DOWNLOAD_PREVIEW"

    invoke-direct {v0, v1, v3}, LX/8kk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8kk;->DOWNLOAD_PREVIEW:LX/8kk;

    .line 1396771
    new-instance v0, LX/8kk;

    const-string v1, "PULSING_DOWNLOAD_PREVIEW"

    invoke-direct {v0, v1, v4}, LX/8kk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8kk;->PULSING_DOWNLOAD_PREVIEW:LX/8kk;

    .line 1396772
    new-instance v0, LX/8kk;

    const-string v1, "PROMOTED"

    invoke-direct {v0, v1, v5}, LX/8kk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8kk;->PROMOTED:LX/8kk;

    .line 1396773
    const/4 v0, 0x4

    new-array v0, v0, [LX/8kk;

    sget-object v1, LX/8kk;->DOWNLOADED:LX/8kk;

    aput-object v1, v0, v2

    sget-object v1, LX/8kk;->DOWNLOAD_PREVIEW:LX/8kk;

    aput-object v1, v0, v3

    sget-object v1, LX/8kk;->PULSING_DOWNLOAD_PREVIEW:LX/8kk;

    aput-object v1, v0, v4

    sget-object v1, LX/8kk;->PROMOTED:LX/8kk;

    aput-object v1, v0, v5

    sput-object v0, LX/8kk;->$VALUES:[LX/8kk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1396774
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8kk;
    .locals 1

    .prologue
    .line 1396775
    const-class v0, LX/8kk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8kk;

    return-object v0
.end method

.method public static values()[LX/8kk;
    .locals 1

    .prologue
    .line 1396776
    sget-object v0, LX/8kk;->$VALUES:[LX/8kk;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8kk;

    return-object v0
.end method
