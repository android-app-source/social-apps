.class public final LX/8mV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8mX;

.field public final synthetic b:LX/3do;

.field public final synthetic c:Lcom/facebook/stickers/store/StickerStoreFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/store/StickerStoreFragment;LX/8mX;LX/3do;)V
    .locals 0

    .prologue
    .line 1398871
    iput-object p1, p0, LX/8mV;->c:Lcom/facebook/stickers/store/StickerStoreFragment;

    iput-object p2, p0, LX/8mV;->a:LX/8mX;

    iput-object p3, p0, LX/8mV;->b:LX/3do;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 1398872
    iget-object v0, p0, LX/8mV;->c:Lcom/facebook/stickers/store/StickerStoreFragment;

    iget-object v0, v0, Lcom/facebook/stickers/store/StickerStoreFragment;->C:LX/8mX;

    iget-object v1, p0, LX/8mV;->a:LX/8mX;

    if-eq v0, v1, :cond_0

    .line 1398873
    :goto_0
    return-void

    .line 1398874
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/service/FetchStickerPacksResult;

    .line 1398875
    iget-object v1, v0, Lcom/facebook/stickers/service/FetchStickerPacksResult;->b:LX/0am;

    move-object v0, v1

    .line 1398876
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 1398877
    iget-object v1, p0, LX/8mV;->b:LX/3do;

    sget-object v2, LX/3do;->DOWNLOADED_PACKS:LX/3do;

    if-ne v1, v2, :cond_2

    .line 1398878
    iget-object v1, p0, LX/8mV;->c:Lcom/facebook/stickers/store/StickerStoreFragment;

    iget-object v1, v1, Lcom/facebook/stickers/store/StickerStoreFragment;->z:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->clear()V

    .line 1398879
    iget-object v1, p0, LX/8mV;->c:Lcom/facebook/stickers/store/StickerStoreFragment;

    iget-object v1, v1, Lcom/facebook/stickers/store/StickerStoreFragment;->A:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->clear()V

    .line 1398880
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v5

    move v4, v3

    :goto_1
    if-ge v4, v5, :cond_2

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/stickers/model/StickerPack;

    .line 1398881
    iget-object v2, v1, Lcom/facebook/stickers/model/StickerPack;->r:Lcom/facebook/stickers/model/StickerCapabilities;

    move-object v6, v2

    .line 1398882
    iget-object v2, p0, LX/8mV;->c:Lcom/facebook/stickers/store/StickerStoreFragment;

    iget-object v2, v2, Lcom/facebook/stickers/store/StickerStoreFragment;->F:LX/0am;

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/4m4;

    invoke-virtual {v6, v2}, Lcom/facebook/stickers/model/StickerCapabilities;->a(LX/4m4;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1398883
    iget-object v2, p0, LX/8mV;->c:Lcom/facebook/stickers/store/StickerStoreFragment;

    iget-object v2, v2, Lcom/facebook/stickers/store/StickerStoreFragment;->z:Ljava/util/LinkedHashMap;

    .line 1398884
    iget-object v6, v1, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v6, v6

    .line 1398885
    invoke-virtual {v2, v6, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1398886
    :goto_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_1

    .line 1398887
    :cond_1
    iget-object v2, p0, LX/8mV;->c:Lcom/facebook/stickers/store/StickerStoreFragment;

    iget-object v2, v2, Lcom/facebook/stickers/store/StickerStoreFragment;->A:Ljava/util/LinkedHashMap;

    .line 1398888
    iget-object v6, v1, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v6, v6

    .line 1398889
    invoke-virtual {v2, v6, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 1398890
    :cond_2
    iget-object v1, p0, LX/8mV;->a:LX/8mX;

    sget-object v2, LX/8mX;->FEATURED:LX/8mX;

    if-eq v1, v2, :cond_3

    iget-object v1, p0, LX/8mV;->a:LX/8mX;

    sget-object v2, LX/8mX;->AVAILABLE:LX/8mX;

    if-ne v1, v2, :cond_8

    .line 1398891
    :cond_3
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 1398892
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 1398893
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v6

    :goto_3
    if-ge v3, v6, :cond_7

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/stickers/model/StickerPack;

    .line 1398894
    iget-object v2, p0, LX/8mV;->a:LX/8mX;

    sget-object v7, LX/8mX;->AVAILABLE:LX/8mX;

    if-eq v2, v7, :cond_4

    .line 1398895
    iget-boolean v2, v1, Lcom/facebook/stickers/model/StickerPack;->m:Z

    move v2, v2

    .line 1398896
    if-eqz v2, :cond_5

    .line 1398897
    :cond_4
    iget-object v2, v1, Lcom/facebook/stickers/model/StickerPack;->r:Lcom/facebook/stickers/model/StickerCapabilities;

    move-object v7, v2

    .line 1398898
    iget-object v2, p0, LX/8mV;->c:Lcom/facebook/stickers/store/StickerStoreFragment;

    iget-object v2, v2, Lcom/facebook/stickers/store/StickerStoreFragment;->F:LX/0am;

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/4m4;

    invoke-virtual {v7, v2}, Lcom/facebook/stickers/model/StickerCapabilities;->a(LX/4m4;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1398899
    invoke-virtual {v4, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1398900
    :cond_5
    :goto_4
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_3

    .line 1398901
    :cond_6
    invoke-virtual {v5, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_4

    .line 1398902
    :cond_7
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1398903
    :cond_8
    iget-object v1, p0, LX/8mV;->c:Lcom/facebook/stickers/store/StickerStoreFragment;

    .line 1398904
    invoke-static {v1, v0}, Lcom/facebook/stickers/store/StickerStoreFragment;->a$redex0(Lcom/facebook/stickers/store/StickerStoreFragment;Ljava/util/List;)V

    .line 1398905
    iget-object v0, p0, LX/8mV;->c:Lcom/facebook/stickers/store/StickerStoreFragment;

    iget-object v1, p0, LX/8mV;->a:LX/8mX;

    .line 1398906
    iput-object v1, v0, Lcom/facebook/stickers/store/StickerStoreFragment;->D:LX/8mX;

    .line 1398907
    goto/16 :goto_0
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1398908
    sget-object v0, Lcom/facebook/stickers/store/StickerStoreFragment;->a:Ljava/lang/Class;

    const-string v1, "Fetching store packs tab failed: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/8mV;->a:LX/8mX;

    aput-object v4, v2, v3

    invoke-static {v0, p1, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1398909
    iget-object v0, p0, LX/8mV;->c:Lcom/facebook/stickers/store/StickerStoreFragment;

    iget-object v0, v0, Lcom/facebook/stickers/store/StickerStoreFragment;->d:LX/03V;

    sget-object v1, Lcom/facebook/stickers/store/StickerStoreFragment;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Fetching store packs tab failed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/8mV;->a:LX/8mX;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1398910
    iget-object v0, p0, LX/8mV;->c:Lcom/facebook/stickers/store/StickerStoreFragment;

    iget-object v1, p0, LX/8mV;->a:LX/8mX;

    invoke-virtual {v0, v1}, Lcom/facebook/stickers/store/StickerStoreFragment;->a(LX/8mX;)V

    .line 1398911
    iget-object v0, p0, LX/8mV;->c:Lcom/facebook/stickers/store/StickerStoreFragment;

    .line 1398912
    invoke-static {v0, p1}, Lcom/facebook/stickers/store/StickerStoreFragment;->a$redex0(Lcom/facebook/stickers/store/StickerStoreFragment;Ljava/lang/Throwable;)V

    .line 1398913
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1398914
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    invoke-direct {p0, p1}, LX/8mV;->a(Lcom/facebook/fbservice/service/OperationResult;)V

    return-void
.end method
