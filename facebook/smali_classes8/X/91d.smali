.class public final LX/91d;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/91f;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/91e;

.field private b:[Ljava/lang/String;

.field private c:I

.field private d:Ljava/util/BitSet;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 1431199
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1431200
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "minutiaeFeelingsBinder"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "searchBarPrompt"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "state"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/91d;->b:[Ljava/lang/String;

    .line 1431201
    iput v3, p0, LX/91d;->c:I

    .line 1431202
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/91d;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/91d;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/91d;LX/1De;IILX/91e;)V
    .locals 1

    .prologue
    .line 1431164
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1431165
    iput-object p4, p0, LX/91d;->a:LX/91e;

    .line 1431166
    iget-object v0, p0, LX/91d;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1431167
    return-void
.end method


# virtual methods
.method public final a(LX/1OX;)LX/91d;
    .locals 1

    .prologue
    .line 1431197
    iget-object v0, p0, LX/91d;->a:LX/91e;

    iput-object p1, v0, LX/91e;->e:LX/1OX;

    .line 1431198
    return-object p0
.end method

.method public final a(LX/8zz;)LX/91d;
    .locals 1

    .prologue
    .line 1431195
    iget-object v0, p0, LX/91d;->a:LX/91e;

    iput-object p1, v0, LX/91e;->h:LX/8zz;

    .line 1431196
    return-object p0
.end method

.method public final a(LX/903;)LX/91d;
    .locals 1

    .prologue
    .line 1431193
    iget-object v0, p0, LX/91d;->a:LX/91e;

    iput-object p1, v0, LX/91e;->g:LX/903;

    .line 1431194
    return-object p0
.end method

.method public final a(LX/91g;)LX/91d;
    .locals 2

    .prologue
    .line 1431190
    iget-object v0, p0, LX/91d;->a:LX/91e;

    iput-object p1, v0, LX/91e;->c:LX/91g;

    .line 1431191
    iget-object v0, p0, LX/91d;->d:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1431192
    return-object p0
.end method

.method public final a(LX/91k;)LX/91d;
    .locals 2

    .prologue
    .line 1431187
    iget-object v0, p0, LX/91d;->a:LX/91e;

    iput-object p1, v0, LX/91e;->a:LX/91k;

    .line 1431188
    iget-object v0, p0, LX/91d;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1431189
    return-object p0
.end method

.method public final a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)LX/91d;
    .locals 1

    .prologue
    .line 1431185
    iget-object v0, p0, LX/91d;->a:LX/91e;

    iput-object p1, v0, LX/91e;->f:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1431186
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1431181
    invoke-super {p0}, LX/1X5;->a()V

    .line 1431182
    const/4 v0, 0x0

    iput-object v0, p0, LX/91d;->a:LX/91e;

    .line 1431183
    sget-object v0, LX/91f;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1431184
    return-void
.end method

.method public final b(Ljava/lang/String;)LX/91d;
    .locals 2

    .prologue
    .line 1431178
    iget-object v0, p0, LX/91d;->a:LX/91e;

    iput-object p1, v0, LX/91e;->b:Ljava/lang/String;

    .line 1431179
    iget-object v0, p0, LX/91d;->d:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1431180
    return-object p0
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/91f;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1431168
    iget-object v1, p0, LX/91d;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/91d;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/91d;->c:I

    if-ge v1, v2, :cond_2

    .line 1431169
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1431170
    :goto_0
    iget v2, p0, LX/91d;->c:I

    if-ge v0, v2, :cond_1

    .line 1431171
    iget-object v2, p0, LX/91d;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1431172
    iget-object v2, p0, LX/91d;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1431173
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1431174
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1431175
    :cond_2
    iget-object v0, p0, LX/91d;->a:LX/91e;

    .line 1431176
    invoke-virtual {p0}, LX/91d;->a()V

    .line 1431177
    return-object v0
.end method
