.class public final LX/9fK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/9fL;


# direct methods
.method public constructor <init>(LX/9fL;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1521742
    iput-object p1, p0, LX/9fK;->b:LX/9fL;

    iput-object p2, p0, LX/9fK;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x7e3d79c3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1521743
    iget-object v1, p0, LX/9fK;->b:LX/9fL;

    iget-object v1, v1, LX/9fL;->l:Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;->e:LX/9f1;

    invoke-virtual {v1}, LX/9f1;->a()LX/9d5;

    move-result-object v1

    iget-object v2, p0, LX/9fK;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/9d5;->a(Ljava/lang/String;)V

    .line 1521744
    iget-object v1, p0, LX/9fK;->b:LX/9fL;

    iget-object v1, v1, LX/9fL;->l:Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;->l:LX/9fL;

    if-eqz v1, :cond_0

    .line 1521745
    iget-object v1, p0, LX/9fK;->b:LX/9fL;

    iget-object v1, v1, LX/9fL;->l:Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;->l:LX/9fL;

    invoke-virtual {v1}, LX/9fL;->x()V

    .line 1521746
    :cond_0
    iget-object v1, p0, LX/9fK;->b:LX/9fL;

    iget-object v1, v1, LX/9fL;->l:Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;

    iget-object v2, p0, LX/9fK;->b:LX/9fL;

    .line 1521747
    iget-object v4, v2, LX/9fL;->m:Lcom/facebook/resources/ui/FbTextView;

    iget-object p0, v2, LX/9fL;->l:Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;

    iget-object p0, p0, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;->b:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v4, p0}, Lcom/facebook/resources/ui/FbTextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1521748
    iget-object v4, v2, LX/9fL;->m:Lcom/facebook/resources/ui/FbTextView;

    iget-object p0, v2, LX/9fL;->l:Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;

    iget-object p0, p0, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;->d:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p1, 0x7f0a00d5

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p0

    invoke-virtual {v4, p0}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1521749
    move-object v2, v2

    .line 1521750
    iput-object v2, v1, Lcom/facebook/photos/editgallery/FilterPickerScrollAdapter;->l:LX/9fL;

    .line 1521751
    const v1, 0x2298f8d4

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
