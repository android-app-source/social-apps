.class public LX/9Wa;
.super Landroid/widget/Filter;
.source ""


# instance fields
.field private a:LX/0Sh;

.field private b:LX/9WV;

.field public c:LX/9WZ;


# direct methods
.method public constructor <init>(LX/0Sh;LX/9WV;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1501376
    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    .line 1501377
    iput-object p1, p0, LX/9Wa;->a:LX/0Sh;

    .line 1501378
    iput-object p2, p0, LX/9Wa;->b:LX/9WV;

    .line 1501379
    return-void
.end method


# virtual methods
.method public final performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 2

    .prologue
    .line 1501380
    iget-object v0, p0, LX/9Wa;->a:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 1501381
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1501382
    iget-object v0, p0, LX/9Wa;->b:LX/9WV;

    .line 1501383
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 1501384
    if-nez p1, :cond_0

    .line 1501385
    :goto_0
    move-object v1, v1

    .line 1501386
    move-object v0, v1

    .line 1501387
    invoke-static {v0}, Lcom/facebook/tagging/model/TaggingProfile;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 1501388
    new-instance v1, Landroid/widget/Filter$FilterResults;

    invoke-direct {v1}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 1501389
    iput-object v0, v1, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 1501390
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, v1, Landroid/widget/Filter$FilterResults;->count:I

    .line 1501391
    return-object v1

    .line 1501392
    :cond_0
    invoke-static {v0, p1}, LX/9WV;->c(LX/9WV;Ljava/lang/CharSequence;)Ljava/util/List;

    move-result-object v1

    goto :goto_0
.end method

.method public final publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1501393
    if-eqz p2, :cond_0

    iget-object v0, p0, LX/9Wa;->c:LX/9WZ;

    if-nez v0, :cond_1

    .line 1501394
    :cond_0
    :goto_0
    return-void

    .line 1501395
    :cond_1
    iget-object v1, p0, LX/9Wa;->c:LX/9WZ;

    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-interface {v1, v0}, LX/9WZ;->a(Ljava/util/List;)V

    goto :goto_0
.end method
