.class public final LX/AAP;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1637545
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 1637546
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1637547
    :goto_0
    return v1

    .line 1637548
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_5

    .line 1637549
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1637550
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1637551
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_0

    if-eqz v7, :cond_0

    .line 1637552
    const-string v8, "display_reactions"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1637553
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v6, v0

    move v0, v2

    goto :goto_1

    .line 1637554
    :cond_1
    const-string v8, "reactors"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1637555
    invoke-static {p0, p1}, LX/AAM;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1637556
    :cond_2
    const-string v8, "reshares"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1637557
    invoke-static {p0, p1}, LX/AAN;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1637558
    :cond_3
    const-string v8, "top_level_comments"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1637559
    invoke-static {p0, p1}, LX/AAO;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1637560
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1637561
    :cond_5
    const/4 v7, 0x4

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1637562
    if-eqz v0, :cond_6

    .line 1637563
    invoke-virtual {p1, v1, v6}, LX/186;->a(IZ)V

    .line 1637564
    :cond_6
    invoke-virtual {p1, v2, v5}, LX/186;->b(II)V

    .line 1637565
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1637566
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1637567
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1637568
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1637569
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1637570
    if-eqz v0, :cond_0

    .line 1637571
    const-string v1, "display_reactions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1637572
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1637573
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1637574
    if-eqz v0, :cond_1

    .line 1637575
    const-string v1, "reactors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1637576
    invoke-static {p0, v0, p2}, LX/AAM;->a(LX/15i;ILX/0nX;)V

    .line 1637577
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1637578
    if-eqz v0, :cond_2

    .line 1637579
    const-string v1, "reshares"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1637580
    invoke-static {p0, v0, p2}, LX/AAN;->a(LX/15i;ILX/0nX;)V

    .line 1637581
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1637582
    if-eqz v0, :cond_3

    .line 1637583
    const-string v1, "top_level_comments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1637584
    invoke-static {p0, v0, p2}, LX/AAO;->a(LX/15i;ILX/0nX;)V

    .line 1637585
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1637586
    return-void
.end method
