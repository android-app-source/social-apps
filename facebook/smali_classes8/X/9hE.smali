.class public final LX/9hE;
.super LX/9hD;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/9hD",
        "<",
        "LX/5kD;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;)V
    .locals 0

    .prologue
    .line 1526281
    invoke-direct {p0, p1}, LX/9hD;-><init>(Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;)V

    .line 1526282
    return-void
.end method


# virtual methods
.method public final b(LX/0Px;)LX/9hE;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "LX/1U8;",
            ">;)",
            "LX/9hE;"
        }
    .end annotation

    .prologue
    .line 1526274
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1526275
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 1526276
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1U8;

    .line 1526277
    invoke-static {v0}, LX/5k9;->a(LX/1U8;)LX/5kD;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1526278
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1526279
    :cond_0
    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/9hD;->a(LX/0Px;)LX/9hD;

    .line 1526280
    return-object p0
.end method
