.class public LX/9Av;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1Ck;

.field private final b:LX/3H7;

.field public c:Z

.field public d:LX/9BB;

.field public e:LX/9BJ;


# direct methods
.method public constructor <init>(LX/1Ck;LX/3H7;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1450871
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1450872
    iput-object v0, p0, LX/9Av;->d:LX/9BB;

    .line 1450873
    iput-object v0, p0, LX/9Av;->e:LX/9BJ;

    .line 1450874
    iput-object p1, p0, LX/9Av;->a:LX/1Ck;

    .line 1450875
    iput-object p2, p0, LX/9Av;->b:LX/3H7;

    .line 1450876
    return-void
.end method

.method public static a$redex0(LX/9Av;Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1450836
    iget-object v0, p0, LX/9Av;->d:LX/9BB;

    if-eqz v0, :cond_0

    .line 1450837
    iget-object v0, p0, LX/9Av;->d:LX/9BB;

    const/4 p0, 0x1

    .line 1450838
    invoke-static {p1}, Lcom/facebook/fbservice/service/ServiceException;->forException(Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/ServiceException;

    move-result-object v1

    .line 1450839
    iget-object v2, v0, LX/9BB;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v2, v2, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->e:LX/1CW;

    invoke-virtual {v2, v1, p0, p0}, LX/1CW;->a(Lcom/facebook/fbservice/service/ServiceException;ZZ)Ljava/lang/String;

    move-result-object v1

    .line 1450840
    iget-object v2, v0, LX/9BB;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    .line 1450841
    iget-object p0, v2, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->u:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    if-nez p0, :cond_1

    .line 1450842
    :cond_0
    :goto_0
    return-void

    .line 1450843
    :cond_1
    iget-object p0, v2, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->u:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    new-instance v0, LX/9BC;

    invoke-direct {v0, v2}, LX/9BC;-><init>(Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;)V

    invoke-virtual {p0, v1, v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/9Av;
    .locals 3

    .prologue
    .line 1450869
    new-instance v2, LX/9Av;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v0

    check-cast v0, LX/1Ck;

    invoke-static {p0}, LX/3H7;->a(LX/0QB;)LX/3H7;

    move-result-object v1

    check-cast v1, LX/3H7;

    invoke-direct {v2, v0, v1}, LX/9Av;-><init>(LX/1Ck;LX/3H7;)V

    .line 1450870
    return-object v2
.end method

.method private b()V
    .locals 1

    .prologue
    .line 1450864
    iget-object v0, p0, LX/9Av;->d:LX/9BB;

    if-eqz v0, :cond_0

    .line 1450865
    iget-object v0, p0, LX/9Av;->d:LX/9BB;

    .line 1450866
    iget-object p0, v0, LX/9BB;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object p0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->u:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    if-eqz p0, :cond_0

    .line 1450867
    iget-object p0, v0, LX/9BB;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object p0, p0, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->u:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {p0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 1450868
    :cond_0
    return-void
.end method

.method private c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1450863
    iget-object v0, p0, LX/9Av;->e:LX/9BJ;

    invoke-virtual {v0}, LX/9BJ;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Z)V
    .locals 6

    .prologue
    .line 1450844
    iget-boolean v0, p0, LX/9Av;->c:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, LX/9Av;->c()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1450845
    :cond_0
    :goto_0
    return-void

    .line 1450846
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/9Av;->c:Z

    .line 1450847
    invoke-direct {p0}, LX/9Av;->b()V

    .line 1450848
    if-eqz p1, :cond_2

    sget-object v0, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    :goto_1
    move-object v0, v0

    .line 1450849
    iget-object v1, p0, LX/9Av;->b:LX/3H7;

    invoke-direct {p0}, LX/9Av;->c()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/9Av;->e:LX/9BJ;

    invoke-virtual {v3}, LX/9BJ;->b()Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    .line 1450850
    new-instance v4, LX/5Cu;

    invoke-direct {v4}, LX/5Cu;-><init>()V

    move-object v4, v4

    .line 1450851
    const-string v5, "feedback_id"

    invoke-virtual {v4, v5, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1450852
    const-class v5, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v5}, LX/0w5;->b(Ljava/lang/Class;)LX/0w5;

    move-result-object v5

    invoke-static {v4, v5}, LX/0zO;->a(LX/0gW;LX/0w5;)LX/0zO;

    move-result-object v4

    .line 1450853
    iput-object v3, v4, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 1450854
    move-object v4, v4

    .line 1450855
    sget-object v5, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v4, v5}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v4

    .line 1450856
    sget-object v5, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    if-ne v0, v5, :cond_3

    .line 1450857
    sget-object v5, LX/0zS;->d:LX/0zS;

    invoke-virtual {v4, v5}, LX/0zO;->a(LX/0zS;)LX/0zO;

    .line 1450858
    :goto_2
    iget-object v5, v1, LX/3H7;->j:LX/0tX;

    invoke-virtual {v5, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    invoke-static {v4}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v0, v4

    .line 1450859
    iget-object v1, p0, LX/9Av;->a:LX/1Ck;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "task_fetch_reactions_counts"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, LX/9Av;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1450860
    new-instance v3, LX/9Au;

    invoke-direct {v3, p0, p1}, LX/9Au;-><init>(LX/9Av;Z)V

    move-object v3, v3

    .line 1450861
    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0

    :cond_2
    sget-object v0, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    goto :goto_1

    .line 1450862
    :cond_3
    sget-object v5, LX/0zS;->a:LX/0zS;

    invoke-virtual {v4, v5}, LX/0zO;->a(LX/0zS;)LX/0zO;

    goto :goto_2
.end method
