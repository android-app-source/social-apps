.class public final LX/9jq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/9jN;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/9jG;

.field public final synthetic b:LX/9ju;


# direct methods
.method public constructor <init>(LX/9ju;LX/9jG;)V
    .locals 0

    .prologue
    .line 1530461
    iput-object p1, p0, LX/9jq;->b:LX/9ju;

    iput-object p2, p0, LX/9jq;->a:LX/9jG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1530462
    iget-object v0, p0, LX/9jq;->b:LX/9ju;

    iget-object v0, v0, LX/9ju;->l:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 1530463
    iget-object v0, p0, LX/9jq;->b:LX/9ju;

    iget-object v0, v0, LX/9ju;->h:Landroid/os/Handler;

    iget-object v1, p0, LX/9jq;->b:LX/9ju;

    iget-object v1, v1, LX/9ju;->l:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1530464
    iget-object v0, p0, LX/9jq;->b:LX/9ju;

    const/4 v1, 0x0

    .line 1530465
    iput-object v1, v0, LX/9ju;->l:Ljava/lang/Runnable;

    .line 1530466
    :cond_0
    sget-object v0, LX/9ju;->a:Ljava/lang/String;

    const-string v1, "Error getting nearby places"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1530467
    iget-object v0, p0, LX/9jq;->b:LX/9ju;

    iget-object v0, v0, LX/9ju;->d:LX/9j5;

    .line 1530468
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/9j5;->r:Z

    .line 1530469
    iget-object v1, v0, LX/9j5;->a:LX/0Zb;

    const-string p1, "place_picker_error_network_failure"

    invoke-static {v0, p1}, LX/9j5;->h(LX/9j5;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-static {v0, p1}, LX/9j5;->a(LX/9j5;Lcom/facebook/analytics/logger/HoneyClientEvent;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-interface {v1, p1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1530470
    iget-object v0, p0, LX/9jq;->b:LX/9ju;

    iget-object v0, v0, LX/9ju;->b:LX/9j7;

    invoke-virtual {v0}, LX/9j7;->a()V

    .line 1530471
    iget-object v0, p0, LX/9jq;->b:LX/9ju;

    iget-object v0, v0, LX/9ju;->j:LX/90X;

    invoke-interface {v0}, LX/90X;->b()V

    .line 1530472
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1530473
    check-cast p1, LX/9jN;

    .line 1530474
    iget-object v0, p0, LX/9jq;->b:LX/9ju;

    iget-object v0, v0, LX/9ju;->f:LX/9kB;

    sget-object v1, LX/9jt;->MOST_RECENT:LX/9jt;

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 1530475
    iget-object v0, p0, LX/9jq;->b:LX/9ju;

    iget-object v0, v0, LX/9ju;->l:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 1530476
    iget-object v0, p0, LX/9jq;->b:LX/9ju;

    iget-object v0, v0, LX/9ju;->h:Landroid/os/Handler;

    iget-object v1, p0, LX/9jq;->b:LX/9ju;

    iget-object v1, v1, LX/9ju;->l:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1530477
    iget-object v0, p0, LX/9jq;->b:LX/9ju;

    const/4 v1, 0x0

    .line 1530478
    iput-object v1, v0, LX/9ju;->l:Ljava/lang/Runnable;

    .line 1530479
    :cond_0
    iget-object v0, p0, LX/9jq;->b:LX/9ju;

    iget-object v0, v0, LX/9ju;->b:LX/9j7;

    iget-object v1, p0, LX/9jq;->a:LX/9jG;

    const v5, 0x150019

    .line 1530480
    iget-object v2, v0, LX/9j7;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x150018

    const/4 v4, 0x2

    invoke-interface {v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 1530481
    iget-object v2, v0, LX/9j7;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v2, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 1530482
    invoke-static {v0, v5, v1}, LX/9j7;->a(LX/9j7;ILX/9jG;)V

    .line 1530483
    iget-object v0, p0, LX/9jq;->b:LX/9ju;

    iget-object v0, v0, LX/9ju;->j:LX/90X;

    invoke-interface {v0, p1}, LX/90X;->a(LX/9jN;)V

    .line 1530484
    iget-object v0, p0, LX/9jq;->b:LX/9ju;

    iget-object v0, v0, LX/9ju;->j:LX/90X;

    invoke-interface {v0}, LX/90X;->a()V

    .line 1530485
    return-void
.end method
