.class public LX/9nC;
.super Ljava/net/CookieHandler;
.source ""


# static fields
.field public static final a:Z


# instance fields
.field public final b:LX/9nB;

.field private final c:LX/5pX;

.field private d:Landroid/webkit/CookieManager;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1536876
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, LX/9nC;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(LX/5pX;)V
    .locals 1

    .prologue
    .line 1536881
    invoke-direct {p0}, Ljava/net/CookieHandler;-><init>()V

    .line 1536882
    iput-object p1, p0, LX/9nC;->c:LX/5pX;

    .line 1536883
    new-instance v0, LX/9nB;

    invoke-direct {v0, p0}, LX/9nB;-><init>(LX/9nC;)V

    iput-object v0, p0, LX/9nC;->b:LX/9nB;

    .line 1536884
    return-void
.end method

.method private static a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1536877
    sget-boolean v0, LX/9nC;->a:Z

    if-eqz v0, :cond_0

    .line 1536878
    invoke-static {p0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    move-result-object v0

    .line 1536879
    invoke-virtual {v0}, Landroid/webkit/CookieSyncManager;->sync()V

    .line 1536880
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 1536874
    invoke-static {p0}, LX/9nC;->b(LX/9nC;)Landroid/webkit/CookieManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Landroid/webkit/CookieManager;->setCookie(Ljava/lang/String;Ljava/lang/String;Landroid/webkit/ValueCallback;)V

    .line 1536875
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1536885
    sget-boolean v0, LX/9nC;->a:Z

    if-eqz v0, :cond_0

    .line 1536886
    new-instance v0, Lcom/facebook/react/modules/network/ForwardingCookieHandler$3;

    invoke-direct {v0, p0, p2, p1}, Lcom/facebook/react/modules/network/ForwardingCookieHandler$3;-><init>(LX/9nC;Ljava/util/List;Ljava/lang/String;)V

    invoke-static {p0, v0}, LX/9nC;->a$redex0(LX/9nC;Ljava/lang/Runnable;)V

    .line 1536887
    :goto_0
    return-void

    .line 1536888
    :cond_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1536889
    invoke-direct {p0, p1, v0}, LX/9nC;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1536890
    :cond_1
    iget-object v0, p0, LX/9nC;->b:LX/9nB;

    invoke-virtual {v0}, LX/9nB;->a()V

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1536873
    const-string v0, "Set-cookie"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Set-cookie2"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/9nC;Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 1536871
    new-instance v0, LX/9n9;

    iget-object v1, p0, LX/9nC;->c:LX/5pX;

    invoke-direct {v0, p0, v1, p1}, LX/9n9;-><init>(LX/9nC;LX/5pX;Ljava/lang/Runnable;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, LX/9n9;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1536872
    return-void
.end method

.method public static b(LX/9nC;)Landroid/webkit/CookieManager;
    .locals 1

    .prologue
    .line 1536855
    iget-object v0, p0, LX/9nC;->d:Landroid/webkit/CookieManager;

    if-nez v0, :cond_0

    .line 1536856
    iget-object v0, p0, LX/9nC;->c:LX/5pX;

    invoke-static {v0}, LX/9nC;->a(Landroid/content/Context;)V

    .line 1536857
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    iput-object v0, p0, LX/9nC;->d:Landroid/webkit/CookieManager;

    .line 1536858
    sget-boolean v0, LX/9nC;->a:Z

    if-eqz v0, :cond_0

    .line 1536859
    iget-object v0, p0, LX/9nC;->d:Landroid/webkit/CookieManager;

    invoke-virtual {v0}, Landroid/webkit/CookieManager;->removeExpiredCookie()V

    .line 1536860
    :cond_0
    iget-object v0, p0, LX/9nC;->d:Landroid/webkit/CookieManager;

    return-object v0
.end method


# virtual methods
.method public final get(Ljava/net/URI;Ljava/util/Map;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/net/URI;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1536867
    invoke-static {p0}, LX/9nC;->b(LX/9nC;)Landroid/webkit/CookieManager;

    move-result-object v0

    invoke-virtual {p1}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/CookieManager;->getCookie(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1536868
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1536869
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    .line 1536870
    :goto_0
    return-object v0

    :cond_0
    const-string v1, "Cookie"

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v1, v0}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method

.method public final put(Ljava/net/URI;Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/net/URI;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1536861
    invoke-virtual {p1}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1536862
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1536863
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1536864
    if-eqz v1, :cond_0

    invoke-static {v1}, LX/9nC;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1536865
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-direct {p0, v2, v0}, LX/9nC;->a(Ljava/lang/String;Ljava/util/List;)V

    goto :goto_0

    .line 1536866
    :cond_1
    return-void
.end method
