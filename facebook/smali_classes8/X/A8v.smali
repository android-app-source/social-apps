.class public final enum LX/A8v;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/A8v;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/A8v;

.field public static final enum PAUSE:LX/A8v;

.field public static final enum RESUME:LX/A8v;

.field public static final enum UPDATE_BUDGET:LX/A8v;


# instance fields
.field private final mCampaignStatus:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1628597
    new-instance v0, LX/A8v;

    const-string v1, "UPDATE_BUDGET"

    const-string v2, "RESUME"

    invoke-direct {v0, v1, v3, v2}, LX/A8v;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/A8v;->UPDATE_BUDGET:LX/A8v;

    .line 1628598
    new-instance v0, LX/A8v;

    const-string v1, "RESUME"

    const-string v2, "RESUME"

    invoke-direct {v0, v1, v4, v2}, LX/A8v;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/A8v;->RESUME:LX/A8v;

    .line 1628599
    new-instance v0, LX/A8v;

    const-string v1, "PAUSE"

    const-string v2, "STOP"

    invoke-direct {v0, v1, v5, v2}, LX/A8v;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/A8v;->PAUSE:LX/A8v;

    .line 1628600
    const/4 v0, 0x3

    new-array v0, v0, [LX/A8v;

    sget-object v1, LX/A8v;->UPDATE_BUDGET:LX/A8v;

    aput-object v1, v0, v3

    sget-object v1, LX/A8v;->RESUME:LX/A8v;

    aput-object v1, v0, v4

    sget-object v1, LX/A8v;->PAUSE:LX/A8v;

    aput-object v1, v0, v5

    sput-object v0, LX/A8v;->$VALUES:[LX/A8v;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1628601
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1628602
    iput-object p3, p0, LX/A8v;->mCampaignStatus:Ljava/lang/String;

    .line 1628603
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/A8v;
    .locals 1

    .prologue
    .line 1628604
    const-class v0, LX/A8v;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/A8v;

    return-object v0
.end method

.method public static values()[LX/A8v;
    .locals 1

    .prologue
    .line 1628605
    sget-object v0, LX/A8v;->$VALUES:[LX/A8v;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/A8v;

    return-object v0
.end method


# virtual methods
.method public final getCampaignStatusString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1628606
    iget-object v0, p0, LX/A8v;->mCampaignStatus:Ljava/lang/String;

    return-object v0
.end method
