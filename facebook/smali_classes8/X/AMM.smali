.class public final LX/AMM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AML;


# instance fields
.field public final synthetic a:LX/0Pz;

.field public final synthetic b:LX/0Pz;

.field public final synthetic c:Ljava/util/concurrent/atomic/AtomicInteger;

.field public final synthetic d:Ljava/util/concurrent/atomic/AtomicInteger;

.field public final synthetic e:LX/AMN;

.field public final synthetic f:LX/AMO;


# direct methods
.method public constructor <init>(LX/AMO;LX/0Pz;LX/0Pz;Ljava/util/concurrent/atomic/AtomicInteger;Ljava/util/concurrent/atomic/AtomicInteger;LX/AMN;)V
    .locals 0

    .prologue
    .line 1666084
    iput-object p1, p0, LX/AMM;->f:LX/AMO;

    iput-object p2, p0, LX/AMM;->a:LX/0Pz;

    iput-object p3, p0, LX/AMM;->b:LX/0Pz;

    iput-object p4, p0, LX/AMM;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    iput-object p5, p0, LX/AMM;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    iput-object p6, p0, LX/AMM;->e:LX/AMN;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/AMT;Ljava/io/File;)V
    .locals 5

    .prologue
    .line 1666085
    iget-object v0, p1, LX/AMT;->b:LX/AMS;

    move-object v0, v0

    .line 1666086
    sget-object v1, LX/AMS;->EFFECT:LX/AMS;

    if-ne v0, v1, :cond_1

    .line 1666087
    iget-object v0, p1, LX/AMT;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1666088
    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/AN1;->a(Ljava/lang/String;Ljava/lang/String;)LX/AN1;

    move-result-object v0

    .line 1666089
    if-eqz v0, :cond_0

    .line 1666090
    iget-object v1, p0, LX/AMM;->a:LX/0Pz;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1666091
    :cond_0
    :goto_0
    iget-object v0, p0, LX/AMM;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    iget-object v1, p0, LX/AMM;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    iget-object v2, p0, LX/AMM;->a:LX/0Pz;

    iget-object v3, p0, LX/AMM;->b:LX/0Pz;

    iget-object v4, p0, LX/AMM;->e:LX/AMN;

    invoke-static {v0, v1, v2, v3, v4}, LX/AMO;->b(Ljava/util/concurrent/atomic/AtomicInteger;Ljava/util/concurrent/atomic/AtomicInteger;LX/0Pz;LX/0Pz;LX/AMN;)V

    .line 1666092
    return-void

    .line 1666093
    :cond_1
    iget-object v0, p0, LX/AMM;->b:LX/0Pz;

    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1666094
    sget-object v0, LX/AMO;->a:Ljava/lang/String;

    const-string v1, "MSQRD asset download failed."

    invoke-static {v0, v1, p1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1666095
    iget-object v0, p0, LX/AMM;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 1666096
    iget-object v0, p0, LX/AMM;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    iget-object v1, p0, LX/AMM;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    iget-object v2, p0, LX/AMM;->a:LX/0Pz;

    iget-object v3, p0, LX/AMM;->b:LX/0Pz;

    iget-object v4, p0, LX/AMM;->e:LX/AMN;

    invoke-static {v0, v1, v2, v3, v4}, LX/AMO;->b(Ljava/util/concurrent/atomic/AtomicInteger;Ljava/util/concurrent/atomic/AtomicInteger;LX/0Pz;LX/0Pz;LX/AMN;)V

    .line 1666097
    return-void
.end method
