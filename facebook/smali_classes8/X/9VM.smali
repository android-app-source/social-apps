.class public LX/9VM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;
.implements LX/0KW;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:LX/9Ux;

.field public c:LX/9Uv;

.field public d:Landroid/content/Context;

.field public e:Landroid/os/Handler;

.field private f:LX/9VV;

.field public g:LX/0Kx;

.field public h:LX/9VW;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1499277
    const-class v0, LX/9VM;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/9VM;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;LX/9Ux;)V
    .locals 1

    .prologue
    .line 1499271
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1499272
    sget-object v0, LX/9Uv;->IDLE:LX/9Uv;

    iput-object v0, p0, LX/9VM;->c:LX/9Uv;

    .line 1499273
    iput-object p1, p0, LX/9VM;->d:Landroid/content/Context;

    .line 1499274
    iput-object p2, p0, LX/9VM;->e:Landroid/os/Handler;

    .line 1499275
    iput-object p3, p0, LX/9VM;->b:LX/9Ux;

    .line 1499276
    return-void
.end method

.method public static a(LX/9VM;I)V
    .locals 1

    .prologue
    .line 1499266
    packed-switch p1, :pswitch_data_0

    .line 1499267
    :goto_0
    return-void

    .line 1499268
    :pswitch_0
    sget-object v0, LX/9Uv;->PLAYING:LX/9Uv;

    invoke-static {p0, v0}, LX/9VM;->a(LX/9VM;LX/9Uv;)V

    goto :goto_0

    .line 1499269
    :pswitch_1
    invoke-virtual {p0}, LX/9VM;->b()V

    .line 1499270
    sget-object v0, LX/9Uv;->IDLE:LX/9Uv;

    invoke-static {p0, v0}, LX/9VM;->a(LX/9VM;LX/9Uv;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(LX/9VM;LX/9Uv;)V
    .locals 1

    .prologue
    .line 1499263
    iput-object p1, p0, LX/9VM;->c:LX/9Uv;

    .line 1499264
    iget-object v0, p0, LX/9VM;->b:LX/9Ux;

    invoke-interface {v0, p1}, LX/9Ux;->a(LX/9Uv;)V

    .line 1499265
    return-void
.end method

.method public static c(LX/9VM;)V
    .locals 4

    .prologue
    .line 1499258
    iget-object v0, p0, LX/9VM;->g:LX/0Kx;

    .line 1499259
    iget-object v1, p0, LX/9VM;->h:LX/9VW;

    .line 1499260
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 1499261
    const/4 v2, 0x2

    iget-object v3, p0, LX/9VM;->f:LX/9VV;

    invoke-interface {v0, v1, v2, v3}, LX/0Kx;->a(LX/0GS;ILjava/lang/Object;)V

    .line 1499262
    :cond_0
    return-void
.end method

.method private static d()Z
    .locals 2

    .prologue
    .line 1499278
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0Kv;)V
    .locals 3

    .prologue
    .line 1499255
    invoke-static {}, LX/9VM;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1499256
    iget-object v0, p0, LX/9VM;->e:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/livephotos/player/LivePhotoVideoPlayer$2;

    invoke-direct {v1, p0, p1}, Lcom/facebook/livephotos/player/LivePhotoVideoPlayer$2;-><init>(LX/9VM;LX/0Kv;)V

    const v2, 0x14187592

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1499257
    :cond_0
    return-void
.end method

.method public final a(ZI)V
    .locals 3

    .prologue
    .line 1499251
    invoke-static {}, LX/9VM;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1499252
    invoke-static {p0, p2}, LX/9VM;->a(LX/9VM;I)V

    .line 1499253
    :goto_0
    return-void

    .line 1499254
    :cond_0
    iget-object v0, p0, LX/9VM;->e:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/livephotos/player/LivePhotoVideoPlayer$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/livephotos/player/LivePhotoVideoPlayer$1;-><init>(LX/9VM;ZI)V

    const v2, -0x1d5677d0

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1499243
    iget-object v0, p0, LX/9VM;->g:LX/0Kx;

    if-eqz v0, :cond_0

    .line 1499244
    iget-object v0, p0, LX/9VM;->g:LX/0Kx;

    invoke-interface {v0, p0}, LX/0Kx;->b(LX/0KW;)V

    .line 1499245
    iget-object v0, p0, LX/9VM;->g:LX/0Kx;

    invoke-interface {v0}, LX/0Kx;->c()V

    .line 1499246
    iget-object v0, p0, LX/9VM;->g:LX/0Kx;

    invoke-interface {v0}, LX/0Kx;->d()V

    .line 1499247
    iput-object v1, p0, LX/9VM;->g:LX/0Kx;

    .line 1499248
    iput-object v1, p0, LX/9VM;->h:LX/9VW;

    .line 1499249
    sget-object v0, LX/9Uv;->IDLE:LX/9Uv;

    invoke-static {p0, v0}, LX/9VM;->a(LX/9VM;LX/9Uv;)V

    .line 1499250
    :cond_0
    return-void
.end method

.method public final declared-synchronized surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 1

    .prologue
    .line 1499239
    monitor-enter p0

    :try_start_0
    new-instance v0, LX/9VV;

    invoke-direct {v0, p1, p3, p4}, LX/9VV;-><init>(Landroid/view/SurfaceHolder;II)V

    iput-object v0, p0, LX/9VM;->f:LX/9VV;

    .line 1499240
    invoke-static {p0}, LX/9VM;->c(LX/9VM;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1499241
    monitor-exit p0

    return-void

    .line 1499242
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 0

    .prologue
    .line 1499238
    monitor-enter p0

    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 1

    .prologue
    .line 1499234
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LX/9VM;->f:LX/9VV;

    .line 1499235
    invoke-static {p0}, LX/9VM;->c(LX/9VM;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1499236
    monitor-exit p0

    return-void

    .line 1499237
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
