.class public final LX/9kg;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryPickerChildrenModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;

.field public final synthetic b:LX/9ki;


# direct methods
.method public constructor <init>(LX/9ki;Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;)V
    .locals 0

    .prologue
    .line 1532049
    iput-object p1, p0, LX/9kg;->b:LX/9ki;

    iput-object p2, p0, LX/9kg;->a:Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1532050
    iget-object v0, p0, LX/9kg;->b:LX/9ki;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    .line 1532051
    iput-object v1, v0, LX/9ki;->a:LX/0am;

    .line 1532052
    iget-object v0, p0, LX/9kg;->b:LX/9ki;

    invoke-virtual {v0}, LX/9kh;->b()V

    .line 1532053
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 1532054
    check-cast p1, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryPickerChildrenModel;

    .line 1532055
    :try_start_0
    iget-object v0, p0, LX/9kg;->b:LX/9ki;

    iget-object v1, p0, LX/9kg;->a:Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;

    iget-object v1, v1, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;->a:Ljava/lang/String;

    const/4 v7, 0x0

    .line 1532056
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1532057
    invoke-virtual {p1}, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryPickerChildrenModel;->a()LX/1vs;

    move-result-object v2

    iget-object v4, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v5, -0x741e820a

    invoke-static {v4, v2, v7, v5}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v2

    .line 1532058
    if-eqz v2, :cond_0

    invoke-static {v2}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v2

    :goto_0
    invoke-virtual {v2}, LX/3Sa;->e()LX/3Sh;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, LX/2sN;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v4}, LX/2sN;->b()LX/1vs;

    move-result-object v2

    iget-object v5, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 1532059
    const-class v6, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryRowModel;

    invoke-virtual {v5, v2, v7, v6}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryRowModel;

    invoke-static {v2}, LX/9kh;->a(Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryRowModel;)Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1532060
    :cond_0
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v2

    goto :goto_0

    .line 1532061
    :cond_1
    iget-object v2, v0, LX/9ki;->b:Ljava/util/Map;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1532062
    iget-object v0, p0, LX/9kg;->b:LX/9ki;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    .line 1532063
    iput-object v1, v0, LX/9ki;->a:LX/0am;

    .line 1532064
    iget-object v0, p0, LX/9kg;->b:LX/9ki;

    invoke-virtual {v0}, LX/9kh;->b()V
    :try_end_0
    .catch LX/4BK; {:try_start_0 .. :try_end_0} :catch_0

    .line 1532065
    :goto_2
    return-void

    .line 1532066
    :catch_0
    iget-object v0, p0, LX/9kg;->b:LX/9ki;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    .line 1532067
    iput-object v1, v0, LX/9ki;->a:LX/0am;

    .line 1532068
    iget-object v0, p0, LX/9kg;->b:LX/9ki;

    invoke-virtual {v0}, LX/9kh;->b()V

    goto :goto_2
.end method
