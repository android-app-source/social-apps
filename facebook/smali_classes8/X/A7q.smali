.class public final LX/A7q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/universalfeedback/graphql/UniversalFeedbackSubmissionMutationModels$FBUniversalFeedbackSubmissionMutationFragmentModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/A7r;


# direct methods
.method public constructor <init>(LX/A7r;)V
    .locals 0

    .prologue
    .line 1626757
    iput-object p1, p0, LX/A7q;->a:LX/A7r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1626758
    const-class v0, LX/A7t;

    const-string v1, "UF mutation failure: %d, \'%s\'"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/A7q;->a:LX/A7r;

    iget-object v4, v4, LX/A7r;->a:LX/A7t;

    iget v4, v4, LX/A7t;->i:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, LX/A7q;->a:LX/A7r;

    iget-object v4, v4, LX/A7r;->a:LX/A7t;

    iget-object v4, v4, LX/A7t;->j:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1626759
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1626760
    return-void
.end method
