.class public final LX/9iP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7UK;


# instance fields
.field public final synthetic a:LX/9iQ;


# direct methods
.method public constructor <init>(LX/9iQ;)V
    .locals 0

    .prologue
    .line 1527738
    iput-object p1, p0, LX/9iP;->a:LX/9iQ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/7UH;)V
    .locals 2

    .prologue
    .line 1527739
    sget-object v0, LX/7UH;->LOADED_IMAGE:LX/7UH;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, LX/9iP;->a:LX/9iQ;

    iget-object v0, v0, LX/9iQ;->b:LX/74w;

    instance-of v0, v0, Lcom/facebook/photos/base/photos/LocalPhoto;

    if-eqz v0, :cond_0

    .line 1527740
    iget-object v0, p0, LX/9iP;->a:LX/9iQ;

    invoke-virtual {v0}, LX/9iQ;->getZoomableImageView()Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    move-result-object v1

    iget-object v0, p0, LX/9iP;->a:LX/9iQ;

    iget-object v0, v0, LX/9iQ;->b:LX/74w;

    check-cast v0, Lcom/facebook/photos/base/photos/LocalPhoto;

    .line 1527741
    iget p0, v0, Lcom/facebook/photos/base/photos/LocalPhoto;->e:I

    move v0, p0

    .line 1527742
    invoke-virtual {v1, v0}, LX/7UQ;->setRotation(I)V

    .line 1527743
    :cond_0
    return-void
.end method

.method public final b(LX/7UH;)V
    .locals 1

    .prologue
    .line 1527744
    sget-object v0, LX/7UH;->LOADED_IMAGE:LX/7UH;

    if-ne p1, v0, :cond_0

    .line 1527745
    iget-object v0, p0, LX/9iP;->a:LX/9iQ;

    invoke-virtual {v0}, LX/9iQ;->getZoomableImageView()Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->f()V

    .line 1527746
    :cond_0
    return-void
.end method
