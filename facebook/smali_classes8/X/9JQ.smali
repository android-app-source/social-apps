.class public LX/9JQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "LX/9JQ;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/9JR;

.field public final b:LX/9JR;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/lang/String;

.field public final d:I

.field public final e:LX/0w5;

.field public final f:I

.field public final g:J

.field public final h:J

.field public final i:[I

.field public final j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;ILX/9JR;LX/9JR;LX/0w5;IJJ[ILjava/lang/String;)V
    .locals 1
    .param p4    # LX/9JR;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1464727
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1464728
    iput-object p1, p0, LX/9JQ;->c:Ljava/lang/String;

    .line 1464729
    iput p2, p0, LX/9JQ;->d:I

    .line 1464730
    iput-object p3, p0, LX/9JQ;->a:LX/9JR;

    .line 1464731
    iput-object p4, p0, LX/9JQ;->b:LX/9JR;

    .line 1464732
    iput-object p5, p0, LX/9JQ;->e:LX/0w5;

    .line 1464733
    iput p6, p0, LX/9JQ;->f:I

    .line 1464734
    iput-wide p7, p0, LX/9JQ;->g:J

    .line 1464735
    iput-wide p9, p0, LX/9JQ;->h:J

    .line 1464736
    iput-object p11, p0, LX/9JQ;->i:[I

    .line 1464737
    iput-object p12, p0, LX/9JQ;->j:Ljava/lang/String;

    .line 1464738
    return-void
.end method


# virtual methods
.method public final b()I
    .locals 1

    .prologue
    .line 1464750
    iget v0, p0, LX/9JQ;->d:I

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 1464751
    iget-object v0, p0, LX/9JQ;->b:LX/9JR;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9JQ;->b:LX/9JR;

    .line 1464752
    iget p0, v0, LX/9JR;->a:I

    move v0, p0

    .line 1464753
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/9JQ;->a:LX/9JR;

    .line 1464754
    iget p0, v0, LX/9JR;->a:I

    move v0, p0

    .line 1464755
    goto :goto_0
.end method

.method public final compareTo(Ljava/lang/Object;)I
    .locals 8

    .prologue
    .line 1464757
    check-cast p1, LX/9JQ;

    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 1464758
    iget-wide v2, p0, LX/9JQ;->g:J

    .line 1464759
    iget-wide v6, p1, LX/9JQ;->g:J

    move-wide v4, v6

    .line 1464760
    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 1464761
    iget-wide v2, p0, LX/9JQ;->g:J

    .line 1464762
    iget-wide v6, p1, LX/9JQ;->g:J

    move-wide v4, v6

    .line 1464763
    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 1464764
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 1464765
    goto :goto_0

    .line 1464766
    :cond_2
    iget-wide v2, p0, LX/9JQ;->h:J

    .line 1464767
    iget-wide v6, p1, LX/9JQ;->h:J

    move-wide v4, v6

    .line 1464768
    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    .line 1464769
    iget-wide v2, p0, LX/9JQ;->h:J

    .line 1464770
    iget-wide v6, p1, LX/9JQ;->h:J

    move-wide v4, v6

    .line 1464771
    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 1464772
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1464756
    invoke-virtual {p0}, LX/9JQ;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 1464739
    iget-object v0, p0, LX/9JQ;->b:LX/9JR;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9JQ;->b:LX/9JR;

    .line 1464740
    iget p0, v0, LX/9JR;->b:I

    move v0, p0

    .line 1464741
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/9JQ;->a:LX/9JR;

    .line 1464742
    iget p0, v0, LX/9JR;->b:I

    move v0, p0

    .line 1464743
    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1464744
    instance-of v0, p1, LX/9JQ;

    if-nez v0, :cond_0

    move v0, v1

    .line 1464745
    :goto_0
    return v0

    :cond_0
    iget-wide v2, p0, LX/9JQ;->g:J

    move-object v0, p1

    check-cast v0, LX/9JQ;

    .line 1464746
    iget-wide v6, v0, LX/9JQ;->g:J

    move-wide v4, v6

    .line 1464747
    cmp-long v0, v2, v4

    if-nez v0, :cond_1

    iget-wide v2, p0, LX/9JQ;->h:J

    check-cast p1, LX/9JQ;

    .line 1464748
    iget-wide v6, p1, LX/9JQ;->h:J

    move-wide v4, v6

    .line 1464749
    cmp-long v0, v2, v4

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final g()LX/0w5;
    .locals 1

    .prologue
    .line 1464724
    iget-object v0, p0, LX/9JQ;->e:LX/0w5;

    return-object v0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 1464723
    iget-wide v0, p0, LX/9JQ;->g:J

    iget-wide v2, p0, LX/9JQ;->h:J

    add-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public final i()J
    .locals 2

    .prologue
    .line 1464722
    iget-wide v0, p0, LX/9JQ;->g:J

    return-wide v0
.end method

.method public final j()J
    .locals 2

    .prologue
    .line 1464725
    iget-wide v0, p0, LX/9JQ;->h:J

    return-wide v0
.end method

.method public final k()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1464726
    const-string v0, "%016x%016x"

    iget-wide v2, p0, LX/9JQ;->g:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-wide v2, p0, LX/9JQ;->h:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
