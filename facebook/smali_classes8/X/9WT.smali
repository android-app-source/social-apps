.class public LX/9WT;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/9Vy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/widget/CustomLinearLayout;",
        "LX/9Vy",
        "<",
        "Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryInterfaces$NegativeFeedbackPromptQueryFragment;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Landroid/widget/ListView;

.field public b:Lcom/facebook/resources/ui/FbTextView;

.field public c:Landroid/widget/FrameLayout;

.field public d:LX/9WR;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1501200
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1501201
    const/4 p1, 0x0

    .line 1501202
    invoke-static {p0}, LX/9W0;->a(Lcom/facebook/widget/CustomLinearLayout;)V

    .line 1501203
    const v0, 0x7f030bdc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1501204
    const v0, 0x7f0d1d86

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, LX/9WT;->a:Landroid/widget/ListView;

    .line 1501205
    const v0, 0x7f0d1d85

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, LX/9WT;->c:Landroid/widget/FrameLayout;

    .line 1501206
    invoke-virtual {p0}, LX/9WT;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0303c6

    invoke-virtual {v0, v1, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1501207
    iget-object v1, p0, LX/9WT;->a:Landroid/widget/ListView;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, p1, v2}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 1501208
    invoke-virtual {p0}, LX/9WT;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030bdd

    invoke-virtual {v0, v1, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1501209
    iget-object v1, p0, LX/9WT;->a:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 1501210
    const v1, 0x7f0d1d78

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/9WT;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 1501211
    new-instance v0, LX/9WR;

    invoke-virtual {p0}, LX/9WT;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d1d84

    invoke-direct {v0, v1, v2}, LX/9WR;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, LX/9WT;->d:LX/9WR;

    .line 1501212
    iget-object v0, p0, LX/9WT;->a:Landroid/widget/ListView;

    iget-object v1, p0, LX/9WT;->d:LX/9WR;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1501213
    iget-object v0, p0, LX/9WT;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, LX/9WT;->a:Landroid/widget/ListView;

    invoke-static {v1, p0}, LX/9W0;->a(Landroid/widget/ListView;Lcom/facebook/widget/CustomLinearLayout;)Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1501214
    return-void
.end method


# virtual methods
.method public setProgressBarVisibility(Z)V
    .locals 4

    .prologue
    .line 1501215
    if-eqz p1, :cond_0

    .line 1501216
    iget-object v0, p0, LX/9WT;->c:Landroid/widget/FrameLayout;

    iget-object v1, p0, LX/9WT;->a:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getHeight()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setMinimumHeight(I)V

    .line 1501217
    new-instance v0, Lcom/facebook/negativefeedback/ui/NegativeFeedbackResponsesView$1;

    invoke-direct {v0, p0}, Lcom/facebook/negativefeedback/ui/NegativeFeedbackResponsesView$1;-><init>(LX/9WT;)V

    const-wide/16 v2, 0x64

    invoke-virtual {p0, v0, v2, v3}, LX/9WT;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1501218
    :goto_0
    return-void

    .line 1501219
    :cond_0
    iget-object v0, p0, LX/9WT;->c:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1501220
    iget-object v0, p0, LX/9WT;->a:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_0
.end method
