.class public final LX/8gd;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 28

    .prologue
    .line 1387675
    const/16 v23, 0x0

    .line 1387676
    const/16 v22, 0x0

    .line 1387677
    const/16 v19, 0x0

    .line 1387678
    const-wide/16 v20, 0x0

    .line 1387679
    const/16 v18, 0x0

    .line 1387680
    const/16 v17, 0x0

    .line 1387681
    const/16 v16, 0x0

    .line 1387682
    const/4 v15, 0x0

    .line 1387683
    const/4 v14, 0x0

    .line 1387684
    const/4 v13, 0x0

    .line 1387685
    const/4 v12, 0x0

    .line 1387686
    const-wide/16 v10, 0x0

    .line 1387687
    const/4 v9, 0x0

    .line 1387688
    const/4 v8, 0x0

    .line 1387689
    const/4 v7, 0x0

    .line 1387690
    const/4 v6, 0x0

    .line 1387691
    const/4 v5, 0x0

    .line 1387692
    const/4 v4, 0x0

    .line 1387693
    const/4 v3, 0x0

    .line 1387694
    const/4 v2, 0x0

    .line 1387695
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v24

    sget-object v25, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    if-eq v0, v1, :cond_17

    .line 1387696
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1387697
    const/4 v2, 0x0

    .line 1387698
    :goto_0
    return v2

    .line 1387699
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v7, :cond_11

    .line 1387700
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1387701
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1387702
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v26, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v26

    if-eq v7, v0, :cond_0

    if-eqz v2, :cond_0

    .line 1387703
    const-string v7, "__type__"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    const-string v7, "__typename"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1387704
    :cond_1
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v2

    move/from16 v25, v2

    goto :goto_1

    .line 1387705
    :cond_2
    const-string v7, "can_viewer_join"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1387706
    const/4 v2, 0x1

    .line 1387707
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move/from16 v24, v6

    move v6, v2

    goto :goto_1

    .line 1387708
    :cond_3
    const-string v7, "connection_style"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1387709
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v23, v2

    goto :goto_1

    .line 1387710
    :cond_4
    const-string v7, "end_timestamp"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1387711
    const/4 v2, 0x1

    .line 1387712
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 1387713
    :cond_5
    const-string v7, "eventSocialContext"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 1387714
    invoke-static/range {p0 .. p1}, LX/8gc;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v22, v2

    goto :goto_1

    .line 1387715
    :cond_6
    const-string v7, "event_place"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 1387716
    invoke-static/range {p0 .. p1}, LX/8gb;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v21, v2

    goto/16 :goto_1

    .line 1387717
    :cond_7
    const-string v7, "id"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 1387718
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 1387719
    :cond_8
    const-string v7, "is_all_day"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 1387720
    const/4 v2, 0x1

    .line 1387721
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move v10, v2

    move/from16 v19, v7

    goto/16 :goto_1

    .line 1387722
    :cond_9
    const-string v7, "is_verified"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 1387723
    const/4 v2, 0x1

    .line 1387724
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move v9, v2

    move/from16 v18, v7

    goto/16 :goto_1

    .line 1387725
    :cond_a
    const-string v7, "name"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 1387726
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 1387727
    :cond_b
    const-string v7, "profile_picture"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 1387728
    invoke-static/range {p0 .. p1}, LX/3lU;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v16, v2

    goto/16 :goto_1

    .line 1387729
    :cond_c
    const-string v7, "start_timestamp"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 1387730
    const/4 v2, 0x1

    .line 1387731
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v14

    move v8, v2

    goto/16 :goto_1

    .line 1387732
    :cond_d
    const-string v7, "time_range_sentence"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_e

    .line 1387733
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 1387734
    :cond_e
    const-string v7, "viewer_guest_status"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_f

    .line 1387735
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 1387736
    :cond_f
    const-string v7, "viewer_watch_status"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 1387737
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 1387738
    :cond_10
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1387739
    :cond_11
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1387740
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1387741
    if-eqz v6, :cond_12

    .line 1387742
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1387743
    :cond_12
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1387744
    if-eqz v3, :cond_13

    .line 1387745
    const/4 v3, 0x3

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1387746
    :cond_13
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1387747
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1387748
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1387749
    if-eqz v10, :cond_14

    .line 1387750
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1387751
    :cond_14
    if-eqz v9, :cond_15

    .line 1387752
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1387753
    :cond_15
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1387754
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1387755
    if-eqz v8, :cond_16

    .line 1387756
    const/16 v3, 0xb

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v14

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1387757
    :cond_16
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1387758
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1387759
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1387760
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_17
    move/from16 v24, v22

    move/from16 v25, v23

    move/from16 v22, v18

    move/from16 v23, v19

    move/from16 v18, v14

    move/from16 v19, v15

    move-wide v14, v10

    move v11, v7

    move v10, v4

    move/from16 v27, v13

    move v13, v9

    move v9, v3

    move v3, v5

    move-wide/from16 v4, v20

    move/from16 v21, v17

    move/from16 v20, v16

    move/from16 v16, v12

    move/from16 v17, v27

    move v12, v8

    move v8, v2

    goto/16 :goto_1
.end method
