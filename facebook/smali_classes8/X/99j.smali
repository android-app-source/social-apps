.class public LX/99j;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/99j;


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:LX/0hB;

.field private final c:LX/0gy;

.field private final d:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0hB;LX/0gy;Ljava/lang/Boolean;)V
    .locals 0
    .param p4    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/tablet/IsTablet;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1447586
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1447587
    iput-object p1, p0, LX/99j;->a:Landroid/content/Context;

    .line 1447588
    iput-object p2, p0, LX/99j;->b:LX/0hB;

    .line 1447589
    iput-object p3, p0, LX/99j;->c:LX/0gy;

    .line 1447590
    iput-object p4, p0, LX/99j;->d:Ljava/lang/Boolean;

    .line 1447591
    return-void
.end method

.method public static a(LX/0QB;)LX/99j;
    .locals 7

    .prologue
    .line 1447573
    sget-object v0, LX/99j;->e:LX/99j;

    if-nez v0, :cond_1

    .line 1447574
    const-class v1, LX/99j;

    monitor-enter v1

    .line 1447575
    :try_start_0
    sget-object v0, LX/99j;->e:LX/99j;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1447576
    if-eqz v2, :cond_0

    .line 1447577
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1447578
    new-instance p0, LX/99j;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v4

    check-cast v4, LX/0hB;

    invoke-static {v0}, LX/0gy;->a(LX/0QB;)LX/0gy;

    move-result-object v5

    check-cast v5, LX/0gy;

    invoke-static {v0}, LX/0rr;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    invoke-direct {p0, v3, v4, v5, v6}, LX/99j;-><init>(Landroid/content/Context;LX/0hB;LX/0gy;Ljava/lang/Boolean;)V

    .line 1447579
    move-object v0, p0

    .line 1447580
    sput-object v0, LX/99j;->e:LX/99j;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1447581
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1447582
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1447583
    :cond_1
    sget-object v0, LX/99j;->e:LX/99j;

    return-object v0

    .line 1447584
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1447585
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static d(LX/99j;)F
    .locals 2

    .prologue
    .line 1447592
    iget-object v0, p0, LX/99j;->d:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/99j;->c:LX/0gy;

    invoke-virtual {v0}, LX/0gy;->c()I

    move-result v0

    .line 1447593
    :goto_0
    iget-object v1, p0, LX/99j;->a:Landroid/content/Context;

    int-to-float v0, v0

    invoke-static {v1, v0}, LX/0tP;->c(Landroid/content/Context;F)I

    move-result v0

    int-to-float v0, v0

    .line 1447594
    return v0

    .line 1447595
    :cond_0
    iget-object v0, p0, LX/99j;->b:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final b()F
    .locals 2

    .prologue
    .line 1447570
    invoke-static {p0}, LX/99j;->d(LX/99j;)F

    move-result v0

    .line 1447571
    const v1, 0x3e23d70a    # 0.16f

    mul-float/2addr v1, v0

    .line 1447572
    sub-float/2addr v0, v1

    const/high16 v1, 0x41a00000    # 20.0f

    sub-float/2addr v0, v1

    return v0
.end method
