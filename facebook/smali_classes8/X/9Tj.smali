.class public final enum LX/9Tj;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9Tj;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9Tj;

.field public static final enum ADD_FRIENDS_MANAGE:LX/9Tj;

.field public static final enum CANCELED:LX/9Tj;

.field public static final enum COMPLETED:LX/9Tj;

.field public static final enum FETCH_FROM_CCU:LX/9Tj;

.field public static final enum FIND_FRIENDS_CLICKED:LX/9Tj;

.field public static final enum FIND_FRIENDS_SHOWN:LX/9Tj;

.field public static final enum FIRST_RESULTS_READY:LX/9Tj;

.field public static final enum FRIENDABLE_CONTACTS_FETCH_FAILED:LX/9Tj;

.field public static final enum FRIENDABLE_CONTACTS_PAGE_FETCHED:LX/9Tj;

.field public static final enum FRIENDABLE_CONTACTS_START_FETCHING:LX/9Tj;

.field public static final enum FRIEND_FINDER_FIRST_TIME_SEEN:LX/9Tj;

.field public static final enum HOW_MANY_SEEN:LX/9Tj;

.field public static final enum INVITES_FETCH_FAILED:LX/9Tj;

.field public static final enum INVITES_PAGE_FETCHED:LX/9Tj;

.field public static final enum INVITES_START_FETCHING:LX/9Tj;

.field public static final enum LEARN_MORE_MANAGE:LX/9Tj;

.field public static final enum LEGAL_GET_STARTED:LX/9Tj;

.field public static final enum LEGAL_LEARN_MORE:LX/9Tj;

.field public static final enum LEGAL_MANAGE:LX/9Tj;

.field public static final enum LEGAL_OPENED:LX/9Tj;

.field public static final enum OPENED:LX/9Tj;

.field public static final enum PHONEBOOK_READ:LX/9Tj;

.field public static final enum PYMK_FETCHED:LX/9Tj;

.field public static final enum PYMK_FETCH_FAILED:LX/9Tj;

.field public static final enum PYMK_START_FETCHING:LX/9Tj;

.field public static final enum SEND_INVITE:LX/9Tj;

.field public static final enum SEND_INVITE_ALL:LX/9Tj;

.field public static final enum TURN_OFF_CONTINUOUS_SYNC:LX/9Tj;

.field public static final enum TURN_ON_CONTINUOUS_SYNC:LX/9Tj;

.field public static final enum UNDO_CLICKED:LX/9Tj;


# instance fields
.field private final mEventType:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1496644
    new-instance v0, LX/9Tj;

    const-string v1, "FIND_FRIENDS_SHOWN"

    const-string v2, "find_friends_view_shown"

    invoke-direct {v0, v1, v4, v2}, LX/9Tj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Tj;->FIND_FRIENDS_SHOWN:LX/9Tj;

    .line 1496645
    new-instance v0, LX/9Tj;

    const-string v1, "FIND_FRIENDS_CLICKED"

    const-string v2, "find_friends_view_clicked"

    invoke-direct {v0, v1, v5, v2}, LX/9Tj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Tj;->FIND_FRIENDS_CLICKED:LX/9Tj;

    .line 1496646
    new-instance v0, LX/9Tj;

    const-string v1, "LEGAL_OPENED"

    const-string v2, "friend_finder_legal_opened"

    invoke-direct {v0, v1, v6, v2}, LX/9Tj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Tj;->LEGAL_OPENED:LX/9Tj;

    .line 1496647
    new-instance v0, LX/9Tj;

    const-string v1, "LEGAL_GET_STARTED"

    const-string v2, "friend_finder_legal_get_started"

    invoke-direct {v0, v1, v7, v2}, LX/9Tj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Tj;->LEGAL_GET_STARTED:LX/9Tj;

    .line 1496648
    new-instance v0, LX/9Tj;

    const-string v1, "LEGAL_MANAGE"

    const-string v2, "friend_finder_legal_manage"

    invoke-direct {v0, v1, v8, v2}, LX/9Tj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Tj;->LEGAL_MANAGE:LX/9Tj;

    .line 1496649
    new-instance v0, LX/9Tj;

    const-string v1, "LEGAL_LEARN_MORE"

    const/4 v2, 0x5

    const-string v3, "friend_finder_legal_learn_more"

    invoke-direct {v0, v1, v2, v3}, LX/9Tj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Tj;->LEGAL_LEARN_MORE:LX/9Tj;

    .line 1496650
    new-instance v0, LX/9Tj;

    const-string v1, "LEARN_MORE_MANAGE"

    const/4 v2, 0x6

    const-string v3, "friend_finder_learn_more_manage"

    invoke-direct {v0, v1, v2, v3}, LX/9Tj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Tj;->LEARN_MORE_MANAGE:LX/9Tj;

    .line 1496651
    new-instance v0, LX/9Tj;

    const-string v1, "OPENED"

    const/4 v2, 0x7

    const-string v3, "friend_finder_opened"

    invoke-direct {v0, v1, v2, v3}, LX/9Tj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Tj;->OPENED:LX/9Tj;

    .line 1496652
    new-instance v0, LX/9Tj;

    const-string v1, "CANCELED"

    const/16 v2, 0x8

    const-string v3, "friend_finder_canceled"

    invoke-direct {v0, v1, v2, v3}, LX/9Tj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Tj;->CANCELED:LX/9Tj;

    .line 1496653
    new-instance v0, LX/9Tj;

    const-string v1, "PHONEBOOK_READ"

    const/16 v2, 0x9

    const-string v3, "friend_finder_phonebook_read"

    invoke-direct {v0, v1, v2, v3}, LX/9Tj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Tj;->PHONEBOOK_READ:LX/9Tj;

    .line 1496654
    new-instance v0, LX/9Tj;

    const-string v1, "FIRST_RESULTS_READY"

    const/16 v2, 0xa

    const-string v3, "friend_finder_first_results_ready"

    invoke-direct {v0, v1, v2, v3}, LX/9Tj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Tj;->FIRST_RESULTS_READY:LX/9Tj;

    .line 1496655
    new-instance v0, LX/9Tj;

    const-string v1, "COMPLETED"

    const/16 v2, 0xb

    const-string v3, "friend_finder_completed"

    invoke-direct {v0, v1, v2, v3}, LX/9Tj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Tj;->COMPLETED:LX/9Tj;

    .line 1496656
    new-instance v0, LX/9Tj;

    const-string v1, "HOW_MANY_SEEN"

    const/16 v2, 0xc

    const-string v3, "friend_finder_how_many_seen"

    invoke-direct {v0, v1, v2, v3}, LX/9Tj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Tj;->HOW_MANY_SEEN:LX/9Tj;

    .line 1496657
    new-instance v0, LX/9Tj;

    const-string v1, "ADD_FRIENDS_MANAGE"

    const/16 v2, 0xd

    const-string v3, "friend_finder_add_friends_manage"

    invoke-direct {v0, v1, v2, v3}, LX/9Tj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Tj;->ADD_FRIENDS_MANAGE:LX/9Tj;

    .line 1496658
    new-instance v0, LX/9Tj;

    const-string v1, "FRIEND_FINDER_FIRST_TIME_SEEN"

    const/16 v2, 0xe

    const-string v3, "friend_finder_first_time_seen"

    invoke-direct {v0, v1, v2, v3}, LX/9Tj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Tj;->FRIEND_FINDER_FIRST_TIME_SEEN:LX/9Tj;

    .line 1496659
    new-instance v0, LX/9Tj;

    const-string v1, "TURN_ON_CONTINUOUS_SYNC"

    const/16 v2, 0xf

    const-string v3, "friend_finder_turn_on_continuous_contacts_upload"

    invoke-direct {v0, v1, v2, v3}, LX/9Tj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Tj;->TURN_ON_CONTINUOUS_SYNC:LX/9Tj;

    .line 1496660
    new-instance v0, LX/9Tj;

    const-string v1, "TURN_OFF_CONTINUOUS_SYNC"

    const/16 v2, 0x10

    const-string v3, "friend_finder_turn_off_continuous_contacts_upload"

    invoke-direct {v0, v1, v2, v3}, LX/9Tj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Tj;->TURN_OFF_CONTINUOUS_SYNC:LX/9Tj;

    .line 1496661
    new-instance v0, LX/9Tj;

    const-string v1, "FRIENDABLE_CONTACTS_START_FETCHING"

    const/16 v2, 0x11

    const-string v3, "friend_finder_friendable_contacts_start_fetching"

    invoke-direct {v0, v1, v2, v3}, LX/9Tj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Tj;->FRIENDABLE_CONTACTS_START_FETCHING:LX/9Tj;

    .line 1496662
    new-instance v0, LX/9Tj;

    const-string v1, "FRIENDABLE_CONTACTS_PAGE_FETCHED"

    const/16 v2, 0x12

    const-string v3, "friend_finder_friendable_contacts_page_fetched"

    invoke-direct {v0, v1, v2, v3}, LX/9Tj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Tj;->FRIENDABLE_CONTACTS_PAGE_FETCHED:LX/9Tj;

    .line 1496663
    new-instance v0, LX/9Tj;

    const-string v1, "FRIENDABLE_CONTACTS_FETCH_FAILED"

    const/16 v2, 0x13

    const-string v3, "friend_finder_friendable_contacts_fetch_failed"

    invoke-direct {v0, v1, v2, v3}, LX/9Tj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Tj;->FRIENDABLE_CONTACTS_FETCH_FAILED:LX/9Tj;

    .line 1496664
    new-instance v0, LX/9Tj;

    const-string v1, "PYMK_START_FETCHING"

    const/16 v2, 0x14

    const-string v3, "friend_finder_friendable_contacts_pymk_start_fetching"

    invoke-direct {v0, v1, v2, v3}, LX/9Tj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Tj;->PYMK_START_FETCHING:LX/9Tj;

    .line 1496665
    new-instance v0, LX/9Tj;

    const-string v1, "PYMK_FETCHED"

    const/16 v2, 0x15

    const-string v3, "friend_finder_friendable_contacts_pymk_fetched"

    invoke-direct {v0, v1, v2, v3}, LX/9Tj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Tj;->PYMK_FETCHED:LX/9Tj;

    .line 1496666
    new-instance v0, LX/9Tj;

    const-string v1, "PYMK_FETCH_FAILED"

    const/16 v2, 0x16

    const-string v3, "friend_finder_friendable_contacts_pymk_fetch_failed"

    invoke-direct {v0, v1, v2, v3}, LX/9Tj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Tj;->PYMK_FETCH_FAILED:LX/9Tj;

    .line 1496667
    new-instance v0, LX/9Tj;

    const-string v1, "FETCH_FROM_CCU"

    const/16 v2, 0x17

    const-string v3, "friend_finder_fetch_from_ccu"

    invoke-direct {v0, v1, v2, v3}, LX/9Tj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Tj;->FETCH_FROM_CCU:LX/9Tj;

    .line 1496668
    new-instance v0, LX/9Tj;

    const-string v1, "SEND_INVITE"

    const/16 v2, 0x18

    const-string v3, "friend_finder_send_invite"

    invoke-direct {v0, v1, v2, v3}, LX/9Tj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Tj;->SEND_INVITE:LX/9Tj;

    .line 1496669
    new-instance v0, LX/9Tj;

    const-string v1, "SEND_INVITE_ALL"

    const/16 v2, 0x19

    const-string v3, "friend_finder_send_invite_all"

    invoke-direct {v0, v1, v2, v3}, LX/9Tj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Tj;->SEND_INVITE_ALL:LX/9Tj;

    .line 1496670
    new-instance v0, LX/9Tj;

    const-string v1, "UNDO_CLICKED"

    const/16 v2, 0x1a

    const-string v3, "friend_finder_undo_invite_clicked"

    invoke-direct {v0, v1, v2, v3}, LX/9Tj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Tj;->UNDO_CLICKED:LX/9Tj;

    .line 1496671
    new-instance v0, LX/9Tj;

    const-string v1, "INVITES_START_FETCHING"

    const/16 v2, 0x1b

    const-string v3, "friend_finder_invitable_contacts_start_fetching"

    invoke-direct {v0, v1, v2, v3}, LX/9Tj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Tj;->INVITES_START_FETCHING:LX/9Tj;

    .line 1496672
    new-instance v0, LX/9Tj;

    const-string v1, "INVITES_PAGE_FETCHED"

    const/16 v2, 0x1c

    const-string v3, "friend_finder_invitable_contacts_page_fetched"

    invoke-direct {v0, v1, v2, v3}, LX/9Tj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Tj;->INVITES_PAGE_FETCHED:LX/9Tj;

    .line 1496673
    new-instance v0, LX/9Tj;

    const-string v1, "INVITES_FETCH_FAILED"

    const/16 v2, 0x1d

    const-string v3, "friend_finder_invitable_contacts_fetch_failed"

    invoke-direct {v0, v1, v2, v3}, LX/9Tj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Tj;->INVITES_FETCH_FAILED:LX/9Tj;

    .line 1496674
    const/16 v0, 0x1e

    new-array v0, v0, [LX/9Tj;

    sget-object v1, LX/9Tj;->FIND_FRIENDS_SHOWN:LX/9Tj;

    aput-object v1, v0, v4

    sget-object v1, LX/9Tj;->FIND_FRIENDS_CLICKED:LX/9Tj;

    aput-object v1, v0, v5

    sget-object v1, LX/9Tj;->LEGAL_OPENED:LX/9Tj;

    aput-object v1, v0, v6

    sget-object v1, LX/9Tj;->LEGAL_GET_STARTED:LX/9Tj;

    aput-object v1, v0, v7

    sget-object v1, LX/9Tj;->LEGAL_MANAGE:LX/9Tj;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/9Tj;->LEGAL_LEARN_MORE:LX/9Tj;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/9Tj;->LEARN_MORE_MANAGE:LX/9Tj;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/9Tj;->OPENED:LX/9Tj;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/9Tj;->CANCELED:LX/9Tj;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/9Tj;->PHONEBOOK_READ:LX/9Tj;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/9Tj;->FIRST_RESULTS_READY:LX/9Tj;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/9Tj;->COMPLETED:LX/9Tj;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/9Tj;->HOW_MANY_SEEN:LX/9Tj;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/9Tj;->ADD_FRIENDS_MANAGE:LX/9Tj;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/9Tj;->FRIEND_FINDER_FIRST_TIME_SEEN:LX/9Tj;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/9Tj;->TURN_ON_CONTINUOUS_SYNC:LX/9Tj;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/9Tj;->TURN_OFF_CONTINUOUS_SYNC:LX/9Tj;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/9Tj;->FRIENDABLE_CONTACTS_START_FETCHING:LX/9Tj;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/9Tj;->FRIENDABLE_CONTACTS_PAGE_FETCHED:LX/9Tj;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/9Tj;->FRIENDABLE_CONTACTS_FETCH_FAILED:LX/9Tj;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/9Tj;->PYMK_START_FETCHING:LX/9Tj;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/9Tj;->PYMK_FETCHED:LX/9Tj;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/9Tj;->PYMK_FETCH_FAILED:LX/9Tj;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/9Tj;->FETCH_FROM_CCU:LX/9Tj;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/9Tj;->SEND_INVITE:LX/9Tj;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/9Tj;->SEND_INVITE_ALL:LX/9Tj;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/9Tj;->UNDO_CLICKED:LX/9Tj;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/9Tj;->INVITES_START_FETCHING:LX/9Tj;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/9Tj;->INVITES_PAGE_FETCHED:LX/9Tj;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/9Tj;->INVITES_FETCH_FAILED:LX/9Tj;

    aput-object v2, v0, v1

    sput-object v0, LX/9Tj;->$VALUES:[LX/9Tj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1496675
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1496676
    iput-object p3, p0, LX/9Tj;->mEventType:Ljava/lang/String;

    .line 1496677
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9Tj;
    .locals 1

    .prologue
    .line 1496678
    const-class v0, LX/9Tj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9Tj;

    return-object v0
.end method

.method public static values()[LX/9Tj;
    .locals 1

    .prologue
    .line 1496679
    sget-object v0, LX/9Tj;->$VALUES:[LX/9Tj;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9Tj;

    return-object v0
.end method


# virtual methods
.method public final getEventName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1496680
    iget-object v0, p0, LX/9Tj;->mEventType:Ljava/lang/String;

    return-object v0
.end method
