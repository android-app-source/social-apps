.class public LX/8rW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/2Ip;


# direct methods
.method public constructor <init>(LX/2do;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1408888
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1408889
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/8rW;->a:Ljava/util/Map;

    .line 1408890
    new-instance v0, LX/8rV;

    invoke-direct {v0, p0}, LX/8rV;-><init>(LX/8rW;)V

    iput-object v0, p0, LX/8rW;->b:LX/2Ip;

    .line 1408891
    iget-object v0, p0, LX/8rW;->b:LX/2Ip;

    invoke-virtual {p1, v0}, LX/0b4;->a(LX/0b2;)Z

    .line 1408892
    return-void
.end method

.method public static a(LX/0QB;)LX/8rW;
    .locals 4

    .prologue
    .line 1408893
    const-class v1, LX/8rW;

    monitor-enter v1

    .line 1408894
    :try_start_0
    sget-object v0, LX/8rW;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1408895
    sput-object v2, LX/8rW;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1408896
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1408897
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1408898
    new-instance p0, LX/8rW;

    invoke-static {v0}, LX/2do;->a(LX/0QB;)LX/2do;

    move-result-object v3

    check-cast v3, LX/2do;

    invoke-direct {p0, v3}, LX/8rW;-><init>(LX/2do;)V

    .line 1408899
    move-object v0, p0

    .line 1408900
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1408901
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8rW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1408902
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1408903
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
