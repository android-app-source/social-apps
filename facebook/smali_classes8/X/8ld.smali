.class public final LX/8ld;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Mb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3Mb",
        "<",
        "LX/8li;",
        "LX/8lj;",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/List;

.field public final synthetic b:Lcom/facebook/stickers/search/StickerSearchContainer;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/search/StickerSearchContainer;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 1397859
    iput-object p1, p0, LX/8ld;->b:Lcom/facebook/stickers/search/StickerSearchContainer;

    iput-object p2, p0, LX/8ld;->a:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 0

    .prologue
    .line 1397860
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1397861
    check-cast p1, LX/8li;

    check-cast p2, LX/8lj;

    .line 1397862
    iget-object v0, p0, LX/8ld;->b:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-object v0, v0, Lcom/facebook/stickers/search/StickerSearchContainer;->L:LX/8lg;

    sget-object v1, LX/8lg;->WAIT_FOR_SEARCH_RESULTS:LX/8lg;

    if-eq v0, v1, :cond_0

    .line 1397863
    iget-object v0, p0, LX/8ld;->b:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-object v0, v0, Lcom/facebook/stickers/search/StickerSearchContainer;->t:LX/8j9;

    iget-object v1, p1, LX/8li;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/8j9;->c(Ljava/lang/String;)V

    .line 1397864
    :goto_0
    return-void

    .line 1397865
    :cond_0
    iget-object v0, p0, LX/8ld;->a:Ljava/util/List;

    iget-object v1, p2, LX/8lj;->a:LX/0Px;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1397866
    check-cast p1, LX/8li;

    .line 1397867
    iget-object v0, p0, LX/8ld;->b:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-object v0, v0, Lcom/facebook/stickers/search/StickerSearchContainer;->L:LX/8lg;

    sget-object v1, LX/8lg;->WAIT_FOR_SEARCH_RESULTS:LX/8lg;

    if-eq v0, v1, :cond_0

    .line 1397868
    iget-object v0, p0, LX/8ld;->b:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-object v0, v0, Lcom/facebook/stickers/search/StickerSearchContainer;->t:LX/8j9;

    iget-object v1, p1, LX/8li;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/8j9;->c(Ljava/lang/String;)V

    .line 1397869
    :goto_0
    return-void

    .line 1397870
    :cond_0
    const/4 v0, 0x0

    .line 1397871
    iget-object v1, p0, LX/8ld;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/Sticker;

    .line 1397872
    iget-object v3, p0, LX/8ld;->b:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-object v3, v3, Lcom/facebook/stickers/search/StickerSearchContainer;->v:LX/0Rf;

    iget-object v0, v0, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-virtual {v3, v0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1397873
    add-int/lit8 v0, v1, 0x1

    :goto_2
    move v1, v0

    .line 1397874
    goto :goto_1

    .line 1397875
    :cond_1
    iget-object v0, p0, LX/8ld;->b:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-object v0, v0, Lcom/facebook/stickers/search/StickerSearchContainer;->t:LX/8j9;

    iget-object v2, p1, LX/8li;->a:Ljava/lang/String;

    iget-object v3, p0, LX/8ld;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    .line 1397876
    const-string v4, "search"

    invoke-static {v0, v4}, LX/8j9;->e(LX/8j9;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 1397877
    const-string v5, "search_query"

    invoke-virtual {v4, v5, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1397878
    const-string v5, "operation_status"

    sget-object p2, LX/8j8;->COMPLETED:LX/8j8;

    invoke-virtual {v4, v5, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1397879
    const-string v5, "tray_sticker_matches_shown"

    invoke-virtual {v4, v5, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1397880
    const-string v5, "store_sticker_matches_shown"

    invoke-virtual {v4, v5, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1397881
    iget-object v5, v0, LX/8j9;->a:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1397882
    iget-object v0, p0, LX/8ld;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1397883
    iget-object v0, p0, LX/8ld;->b:Lcom/facebook/stickers/search/StickerSearchContainer;

    sget-object v1, LX/8lg;->SEARCH_FINISHED_NO_RESULTS:LX/8lg;

    invoke-static {v0, v1}, Lcom/facebook/stickers/search/StickerSearchContainer;->setCurrentState(Lcom/facebook/stickers/search/StickerSearchContainer;LX/8lg;)V

    .line 1397884
    :goto_3
    iget-object v0, p0, LX/8ld;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_0

    .line 1397885
    :cond_2
    iget-object v0, p0, LX/8ld;->b:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-object v1, p0, LX/8ld;->a:Ljava/util/List;

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    new-instance v2, LX/8lc;

    invoke-direct {v2, p0, p1}, LX/8lc;-><init>(LX/8ld;LX/8li;)V

    invoke-static {v0, v1, v2}, Lcom/facebook/stickers/search/StickerSearchContainer;->a$redex0(Lcom/facebook/stickers/search/StickerSearchContainer;LX/0Px;LX/8lZ;)V

    goto :goto_3

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1397886
    check-cast p1, LX/8li;

    check-cast p2, Ljava/lang/Throwable;

    .line 1397887
    iget-object v0, p0, LX/8ld;->b:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-object v0, v0, Lcom/facebook/stickers/search/StickerSearchContainer;->L:LX/8lg;

    sget-object v1, LX/8lg;->WAIT_FOR_SEARCH_RESULTS:LX/8lg;

    if-ne v0, v1, :cond_0

    .line 1397888
    iget-object v0, p0, LX/8ld;->b:Lcom/facebook/stickers/search/StickerSearchContainer;

    sget-object v1, LX/8lg;->ERROR:LX/8lg;

    invoke-static {v0, v1}, Lcom/facebook/stickers/search/StickerSearchContainer;->setCurrentState(Lcom/facebook/stickers/search/StickerSearchContainer;LX/8lg;)V

    .line 1397889
    :cond_0
    iget-object v0, p0, LX/8ld;->b:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-object v0, v0, Lcom/facebook/stickers/search/StickerSearchContainer;->s:LX/03V;

    sget-object v1, Lcom/facebook/stickers/search/StickerSearchContainer;->b:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Sticker query search failed"

    invoke-virtual {v0, v1, v2, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1397890
    iget-object v0, p0, LX/8ld;->b:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-object v0, v0, Lcom/facebook/stickers/search/StickerSearchContainer;->t:LX/8j9;

    iget-object v1, p1, LX/8li;->a:Ljava/lang/String;

    .line 1397891
    const-string v2, "search"

    invoke-static {v0, v2}, LX/8j9;->e(LX/8j9;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 1397892
    const-string p0, "search_query"

    invoke-virtual {v2, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1397893
    const-string p0, "operation_status"

    sget-object p1, LX/8j8;->FAILED:LX/8j8;

    invoke-virtual {v2, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1397894
    iget-object p0, v0, LX/8j9;->a:LX/0Zb;

    invoke-interface {p0, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1397895
    return-void
.end method
