.class public abstract LX/8vj;
.super LX/8vi;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
.implements Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/8vi",
        "<",
        "Landroid/widget/ListAdapter;",
        ">;",
        "Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;",
        "Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;"
    }
.end annotation


# static fields
.field public static final T:Landroid/view/animation/Interpolator;

.field public static final U:[I


# instance fields
.field public A:I

.field public B:I

.field public C:I

.field public D:I

.field public E:I

.field public F:I

.field public G:I

.field public H:I

.field public I:Lit/sephiroth/android/library/widget/AbsHListView$PositionScroller;

.field public J:I

.field public K:Z

.field public L:Z

.field public M:I

.field public N:I

.field public O:Ljava/lang/Runnable;

.field public final P:[Z

.field public Q:I

.field public R:I

.field public S:Z

.field public a:LX/8vS;

.field private aA:I

.field public aB:Lit/sephiroth/android/library/widget/AbsHListView$CheckForLongPress;

.field private aC:Ljava/lang/Runnable;

.field public aD:Lit/sephiroth/android/library/widget/AbsHListView$CheckForKeyLongPress;

.field private aE:Lit/sephiroth/android/library/widget/AbsHListView$PerformClick;

.field private aF:Ljava/lang/Runnable;

.field public aG:I

.field private aH:I

.field private aI:Z

.field private aJ:I

.field private aK:I

.field private aL:Ljava/lang/Runnable;

.field public aM:I

.field public aN:I

.field private aO:F

.field public aP:I

.field public aQ:LX/8vn;

.field public aR:LX/8vn;

.field private aS:I

.field private aT:I

.field private aU:I

.field private aV:Z

.field private aW:I

.field private aX:I

.field private aY:LX/8vd;

.field private aZ:I

.field public au:Landroid/view/VelocityTracker;

.field private av:Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

.field private aw:LX/8ve;

.field public ax:Z

.field private ay:Landroid/graphics/Rect;

.field private az:Landroid/view/ContextMenu$ContextMenuInfo;

.field public b:I

.field private ba:I

.field private bb:I

.field private bc:Lit/sephiroth/android/library/widget/AbsHListView$SavedState;

.field private bd:F

.field public c:Ljava/lang/Object;

.field public d:Ljava/lang/Object;

.field public e:I

.field public f:Landroid/util/SparseBooleanArray;

.field public g:LX/0tf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0tf",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public h:I

.field public i:LX/8va;

.field public j:Landroid/widget/ListAdapter;

.field public k:Z

.field public l:Z

.field public m:Landroid/graphics/drawable/Drawable;

.field public n:I

.field public o:Landroid/graphics/Rect;

.field public final p:LX/8vf;

.field public q:I

.field public r:I

.field public s:I

.field public t:I

.field public u:Landroid/graphics/Rect;

.field public v:I

.field public w:Landroid/view/View;

.field public x:Landroid/view/View;

.field public y:Z

.field public z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1419516
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    sput-object v0, LX/8vj;->T:Landroid/view/animation/Interpolator;

    .line 1419517
    const/4 v0, 0x1

    new-array v0, v0, [I

    aput v1, v0, v1

    sput-object v0, LX/8vj;->U:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 1418941
    invoke-direct {p0, p1}, LX/8vi;-><init>(Landroid/content/Context;)V

    .line 1418942
    iput v1, p0, LX/8vj;->b:I

    .line 1418943
    iput v1, p0, LX/8vj;->h:I

    .line 1418944
    iput-boolean v1, p0, LX/8vj;->l:Z

    .line 1418945
    iput v2, p0, LX/8vj;->n:I

    .line 1418946
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/8vj;->o:Landroid/graphics/Rect;

    .line 1418947
    new-instance v0, LX/8vf;

    invoke-direct {v0, p0}, LX/8vf;-><init>(LX/8vj;)V

    iput-object v0, p0, LX/8vj;->p:LX/8vf;

    .line 1418948
    iput v1, p0, LX/8vj;->q:I

    .line 1418949
    iput v1, p0, LX/8vj;->r:I

    .line 1418950
    iput v1, p0, LX/8vj;->s:I

    .line 1418951
    iput v1, p0, LX/8vj;->t:I

    .line 1418952
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/8vj;->u:Landroid/graphics/Rect;

    .line 1418953
    iput v1, p0, LX/8vj;->v:I

    .line 1418954
    iput v2, p0, LX/8vj;->F:I

    .line 1418955
    iput v1, p0, LX/8vj;->J:I

    .line 1418956
    iput-boolean v3, p0, LX/8vj;->ax:Z

    .line 1418957
    iput v2, p0, LX/8vj;->M:I

    .line 1418958
    const/4 v0, 0x0

    iput-object v0, p0, LX/8vj;->az:Landroid/view/ContextMenu$ContextMenuInfo;

    .line 1418959
    iput v2, p0, LX/8vj;->aA:I

    .line 1418960
    iput v1, p0, LX/8vj;->aJ:I

    .line 1418961
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, LX/8vj;->aO:F

    .line 1418962
    new-array v0, v3, [Z

    iput-object v0, p0, LX/8vj;->P:[Z

    .line 1418963
    iput v2, p0, LX/8vj;->aP:I

    .line 1418964
    iput v1, p0, LX/8vj;->aU:I

    .line 1418965
    invoke-direct {p0}, LX/8vj;->p()V

    .line 1418966
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1418967
    const v0, 0x7f010763

    invoke-direct {p0, p1, p2, v0}, LX/8vj;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1418968
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v4, -0x1

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1418969
    invoke-direct {p0, p1, p2, p3}, LX/8vi;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1418970
    iput v0, p0, LX/8vj;->b:I

    .line 1418971
    iput v0, p0, LX/8vj;->h:I

    .line 1418972
    iput-boolean v0, p0, LX/8vj;->l:Z

    .line 1418973
    iput v4, p0, LX/8vj;->n:I

    .line 1418974
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    iput-object v3, p0, LX/8vj;->o:Landroid/graphics/Rect;

    .line 1418975
    new-instance v3, LX/8vf;

    invoke-direct {v3, p0}, LX/8vf;-><init>(LX/8vj;)V

    iput-object v3, p0, LX/8vj;->p:LX/8vf;

    .line 1418976
    iput v0, p0, LX/8vj;->q:I

    .line 1418977
    iput v0, p0, LX/8vj;->r:I

    .line 1418978
    iput v0, p0, LX/8vj;->s:I

    .line 1418979
    iput v0, p0, LX/8vj;->t:I

    .line 1418980
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    iput-object v3, p0, LX/8vj;->u:Landroid/graphics/Rect;

    .line 1418981
    iput v0, p0, LX/8vj;->v:I

    .line 1418982
    iput v4, p0, LX/8vj;->F:I

    .line 1418983
    iput v0, p0, LX/8vj;->J:I

    .line 1418984
    iput-boolean v1, p0, LX/8vj;->ax:Z

    .line 1418985
    iput v4, p0, LX/8vj;->M:I

    .line 1418986
    iput-object v2, p0, LX/8vj;->az:Landroid/view/ContextMenu$ContextMenuInfo;

    .line 1418987
    iput v4, p0, LX/8vj;->aA:I

    .line 1418988
    iput v0, p0, LX/8vj;->aJ:I

    .line 1418989
    const/high16 v3, 0x3f800000    # 1.0f

    iput v3, p0, LX/8vj;->aO:F

    .line 1418990
    new-array v3, v1, [Z

    iput-object v3, p0, LX/8vj;->P:[Z

    .line 1418991
    iput v4, p0, LX/8vj;->aP:I

    .line 1418992
    iput v0, p0, LX/8vj;->aU:I

    .line 1418993
    invoke-direct {p0}, LX/8vj;->p()V

    .line 1418994
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    .line 1418995
    sget-object v4, LX/KCg;->AbsHListView:[I

    invoke-virtual {v3, p2, v4, p3, v0}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v8

    .line 1418996
    if-eqz v8, :cond_2

    .line 1418997
    const/16 v2, 0x0

    invoke-virtual {v8, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 1418998
    const/16 v2, 0x1

    invoke-virtual {v8, v2, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v6

    .line 1418999
    const/16 v2, 0x6

    invoke-virtual {v8, v2, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v5

    .line 1419000
    const/16 v2, 0x2

    invoke-virtual {v8, v2, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v4

    .line 1419001
    const/16 v2, 0x7

    invoke-virtual {v8, v2, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    .line 1419002
    const/16 v2, 0x3

    invoke-virtual {v8, v2, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    .line 1419003
    const/16 v9, 0x5

    invoke-virtual {v8, v9, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    .line 1419004
    const/16 v9, 0x4

    invoke-virtual {v8, v9, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 1419005
    invoke-virtual {v8}, Landroid/content/res/TypedArray;->recycle()V

    .line 1419006
    :goto_0
    if-eqz v7, :cond_0

    .line 1419007
    invoke-virtual {p0, v7}, LX/8vj;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 1419008
    :cond_0
    iput-boolean v6, p0, LX/8vj;->l:Z

    .line 1419009
    iget-boolean v6, p0, LX/8vj;->K:Z

    if-eq v6, v5, :cond_1

    .line 1419010
    iput-boolean v5, p0, LX/8vj;->K:Z

    .line 1419011
    invoke-virtual {p0}, LX/8vj;->getChildCount()I

    move-result v6

    if-lez v6, :cond_1

    .line 1419012
    invoke-virtual {p0}, LX/8vj;->c()V

    .line 1419013
    invoke-virtual {p0}, LX/8vj;->requestLayout()V

    .line 1419014
    invoke-virtual {p0}, LX/8vj;->invalidate()V

    .line 1419015
    :cond_1
    invoke-virtual {p0, v4}, LX/8vj;->setScrollingCacheEnabled(Z)V

    .line 1419016
    iput v3, p0, LX/8vj;->aG:I

    .line 1419017
    invoke-virtual {p0, v2}, LX/8vj;->setCacheColorHint(I)V

    .line 1419018
    iput-boolean v1, p0, LX/8vj;->ax:Z

    .line 1419019
    invoke-virtual {p0, v0}, LX/8vj;->setChoiceMode(I)V

    .line 1419020
    return-void

    :cond_2
    move v3, v0

    move v4, v1

    move v5, v0

    move v6, v0

    move-object v7, v2

    move v2, v0

    goto :goto_0
.end method

.method private A()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1419021
    iget-boolean v0, p0, LX/8vj;->L:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/8vj;->y:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/8vj;->a:LX/8vS;

    invoke-virtual {v0}, LX/8vS;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1419022
    invoke-virtual {p0, v1}, LX/8vj;->setChildrenDrawnWithCacheEnabled(Z)V

    .line 1419023
    invoke-virtual {p0, v1}, LX/8vj;->setChildrenDrawingCacheEnabled(Z)V

    .line 1419024
    iput-boolean v1, p0, LX/8vj;->z:Z

    iput-boolean v1, p0, LX/8vj;->y:Z

    .line 1419025
    :cond_0
    return-void
.end method

.method public static B(LX/8vj;)V
    .locals 1

    .prologue
    .line 1419026
    iget-object v0, p0, LX/8vj;->a:LX/8vS;

    invoke-virtual {v0}, LX/8vS;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1419027
    iget-object v0, p0, LX/8vj;->aL:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    .line 1419028
    new-instance v0, Lit/sephiroth/android/library/widget/AbsHListView$2;

    invoke-direct {v0, p0}, Lit/sephiroth/android/library/widget/AbsHListView$2;-><init>(LX/8vj;)V

    iput-object v0, p0, LX/8vj;->aL:Ljava/lang/Runnable;

    .line 1419029
    :cond_0
    iget-object v0, p0, LX/8vj;->aL:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, LX/8vj;->post(Ljava/lang/Runnable;)Z

    .line 1419030
    :cond_1
    return-void
.end method

.method private C()Z
    .locals 14

    .prologue
    const/4 v3, 0x1

    const/4 v6, -0x1

    const/4 v4, 0x0

    .line 1419031
    invoke-virtual {p0}, LX/8vj;->getChildCount()I

    move-result v9

    .line 1419032
    if-gtz v9, :cond_1

    .line 1419033
    :cond_0
    :goto_0
    return v4

    .line 1419034
    :cond_1
    iget-object v0, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v5, v0, Landroid/graphics/Rect;->left:I

    .line 1419035
    invoke-virtual {p0}, LX/8vj;->getRight()I

    move-result v0

    invoke-virtual {p0}, LX/8vj;->getLeft()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    sub-int v7, v0, v1

    .line 1419036
    iget v2, p0, LX/8vi;->V:I

    .line 1419037
    iget v1, p0, LX/8vj;->M:I

    .line 1419038
    if-lt v1, v2, :cond_5

    add-int v0, v2, v9

    if-ge v1, v0, :cond_5

    .line 1419039
    iget v0, p0, LX/8vi;->V:I

    sub-int v0, v1, v0

    invoke-virtual {p0, v0}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 1419040
    invoke-virtual {v8}, Landroid/view/View;->getLeft()I

    move-result v0

    .line 1419041
    invoke-virtual {v8}, Landroid/view/View;->getRight()I

    move-result v9

    .line 1419042
    if-ge v0, v5, :cond_3

    .line 1419043
    invoke-virtual {p0}, LX/8vj;->getHorizontalFadingEdgeLength()I

    move-result v0

    add-int/2addr v0, v5

    move v5, v0

    move v0, v1

    move v1, v3

    .line 1419044
    :goto_1
    iput v6, p0, LX/8vj;->M:I

    .line 1419045
    iget-object v7, p0, LX/8vj;->av:Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    invoke-virtual {p0, v7}, LX/8vj;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1419046
    iget-object v7, p0, LX/8vj;->I:Lit/sephiroth/android/library/widget/AbsHListView$PositionScroller;

    if-eqz v7, :cond_2

    .line 1419047
    iget-object v7, p0, LX/8vj;->I:Lit/sephiroth/android/library/widget/AbsHListView$PositionScroller;

    invoke-virtual {v7}, Lit/sephiroth/android/library/widget/AbsHListView$PositionScroller;->a()V

    .line 1419048
    :cond_2
    iput v6, p0, LX/8vj;->F:I

    .line 1419049
    invoke-static {p0}, LX/8vj;->B(LX/8vj;)V

    .line 1419050
    iput v5, p0, LX/8vj;->W:I

    .line 1419051
    invoke-virtual {p0, v0, v1}, LX/8vi;->a(IZ)I

    move-result v0

    .line 1419052
    if-lt v0, v2, :cond_c

    invoke-virtual {p0}, LX/8vi;->getLastVisiblePosition()I

    move-result v1

    if-gt v0, v1, :cond_c

    .line 1419053
    const/4 v1, 0x4

    iput v1, p0, LX/8vj;->h:I

    .line 1419054
    invoke-direct {p0}, LX/8vj;->v()V

    .line 1419055
    invoke-virtual {p0, v0}, LX/8vj;->setSelectionInt(I)V

    .line 1419056
    invoke-virtual {p0}, LX/8vj;->b()V

    .line 1419057
    :goto_2
    invoke-virtual {p0, v4}, LX/8vj;->a(I)V

    .line 1419058
    if-ltz v0, :cond_0

    move v4, v3

    goto :goto_0

    .line 1419059
    :cond_3
    if-le v9, v7, :cond_4

    .line 1419060
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    sub-int v0, v7, v0

    invoke-virtual {p0}, LX/8vj;->getHorizontalFadingEdgeLength()I

    move-result v5

    sub-int/2addr v0, v5

    :cond_4
    move v5, v0

    move v0, v1

    move v1, v3

    .line 1419061
    goto :goto_1

    .line 1419062
    :cond_5
    if-ge v1, v2, :cond_9

    move v7, v4

    move v0, v4

    .line 1419063
    :goto_3
    if-ge v7, v9, :cond_8

    .line 1419064
    invoke-virtual {p0, v7}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1419065
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    .line 1419066
    if-nez v7, :cond_11

    .line 1419067
    if-gtz v2, :cond_6

    if-ge v1, v5, :cond_10

    .line 1419068
    :cond_6
    invoke-virtual {p0}, LX/8vj;->getHorizontalFadingEdgeLength()I

    move-result v0

    add-int/2addr v0, v5

    move v5, v1

    .line 1419069
    :goto_4
    if-lt v1, v0, :cond_7

    .line 1419070
    add-int v0, v2, v7

    move v5, v1

    move v1, v3

    .line 1419071
    goto :goto_1

    .line 1419072
    :cond_7
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    move v13, v0

    move v0, v5

    move v5, v13

    goto :goto_3

    :cond_8
    move v1, v3

    move v5, v0

    move v0, v2

    goto :goto_1

    .line 1419073
    :cond_9
    iget v10, p0, LX/8vi;->ao:I

    .line 1419074
    add-int v0, v2, v9

    add-int/lit8 v0, v0, -0x1

    .line 1419075
    add-int/lit8 v1, v9, -0x1

    move v8, v1

    move v5, v4

    :goto_5
    if-ltz v8, :cond_f

    .line 1419076
    invoke-virtual {p0, v8}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v11

    .line 1419077
    invoke-virtual {v11}, Landroid/view/View;->getLeft()I

    move-result v1

    .line 1419078
    invoke-virtual {v11}, Landroid/view/View;->getRight()I

    move-result v11

    .line 1419079
    add-int/lit8 v12, v9, -0x1

    if-ne v8, v12, :cond_e

    .line 1419080
    add-int v5, v2, v9

    if-lt v5, v10, :cond_a

    if-le v11, v7, :cond_d

    .line 1419081
    :cond_a
    invoke-virtual {p0}, LX/8vj;->getHorizontalFadingEdgeLength()I

    move-result v5

    sub-int v5, v7, v5

    move v7, v1

    .line 1419082
    :goto_6
    if-gt v11, v5, :cond_b

    .line 1419083
    add-int v0, v2, v8

    move v5, v1

    move v1, v4

    .line 1419084
    goto/16 :goto_1

    .line 1419085
    :cond_b
    add-int/lit8 v1, v8, -0x1

    move v8, v1

    move v13, v5

    move v5, v7

    move v7, v13

    goto :goto_5

    :cond_c
    move v0, v6

    .line 1419086
    goto :goto_2

    :cond_d
    move v5, v7

    move v7, v1

    goto :goto_6

    :cond_e
    move v13, v7

    move v7, v5

    move v5, v13

    goto :goto_6

    :cond_f
    move v1, v4

    goto/16 :goto_1

    :cond_10
    move v0, v5

    move v5, v1

    goto :goto_4

    :cond_11
    move v13, v5

    move v5, v0

    move v0, v13

    goto :goto_4
.end method

.method private D()V
    .locals 13

    .prologue
    const/16 v12, 0xb

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1419087
    iget-object v0, p0, LX/8vj;->f:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->clear()V

    move v1, v6

    move v2, v6

    .line 1419088
    :goto_0
    iget-object v0, p0, LX/8vj;->g:LX/0tf;

    invoke-virtual {v0}, LX/0tf;->a()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 1419089
    iget-object v0, p0, LX/8vj;->g:LX/0tf;

    invoke-virtual {v0, v1}, LX/0tf;->b(I)J

    move-result-wide v4

    .line 1419090
    iget-object v0, p0, LX/8vj;->g:LX/0tf;

    invoke-virtual {v0, v1}, LX/0tf;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 1419091
    iget-object v0, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    invoke-interface {v0, v3}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v8

    .line 1419092
    cmp-long v0, v4, v8

    if-eqz v0, :cond_4

    .line 1419093
    add-int/lit8 v0, v3, -0x14

    invoke-static {v6, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1419094
    add-int/lit8 v8, v3, 0x14

    iget v9, p0, LX/8vi;->ao:I

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v8

    .line 1419095
    :goto_1
    if-ge v0, v8, :cond_7

    .line 1419096
    iget-object v9, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    invoke-interface {v9, v0}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v10

    .line 1419097
    cmp-long v9, v4, v10

    if-nez v9, :cond_3

    .line 1419098
    iget-object v8, p0, LX/8vj;->f:Landroid/util/SparseBooleanArray;

    invoke-virtual {v8, v0, v7}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 1419099
    iget-object v8, p0, LX/8vj;->g:LX/0tf;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 1419100
    iget-boolean v9, v8, LX/0tf;->b:Z

    if-eqz v9, :cond_0

    .line 1419101
    invoke-static {v8}, LX/0tf;->d(LX/0tf;)V

    .line 1419102
    :cond_0
    iget-object v9, v8, LX/0tf;->d:[Ljava/lang/Object;

    aput-object v0, v9, v1

    .line 1419103
    move v0, v7

    .line 1419104
    :goto_2
    if-nez v0, :cond_2

    .line 1419105
    iget-object v0, p0, LX/8vj;->g:LX/0tf;

    invoke-virtual {v0, v4, v5}, LX/0tf;->b(J)V

    .line 1419106
    add-int/lit8 v0, v1, -0x1

    .line 1419107
    iget v1, p0, LX/8vj;->e:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, LX/8vj;->e:I

    .line 1419108
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-le v1, v12, :cond_1

    .line 1419109
    iget-object v1, p0, LX/8vj;->c:Ljava/lang/Object;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/8vj;->d:Ljava/lang/Object;

    if-eqz v1, :cond_1

    .line 1419110
    iget-object v1, p0, LX/8vj;->d:Ljava/lang/Object;

    check-cast v1, LX/8vW;

    iget-object v2, p0, LX/8vj;->c:Ljava/lang/Object;

    check-cast v2, Landroid/view/ActionMode;

    invoke-virtual/range {v1 .. v6}, LX/8vW;->a(Landroid/view/ActionMode;IJZ)V

    :cond_1
    move v1, v0

    move v2, v7

    :cond_2
    move v0, v1

    move v1, v2

    .line 1419111
    :goto_3
    add-int/lit8 v0, v0, 0x1

    move v2, v1

    move v1, v0

    goto/16 :goto_0

    .line 1419112
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1419113
    :cond_4
    iget-object v0, p0, LX/8vj;->f:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, v3, v7}, Landroid/util/SparseBooleanArray;->put(IZ)V

    move v0, v1

    move v1, v2

    goto :goto_3

    .line 1419114
    :cond_5
    if-eqz v2, :cond_6

    iget-object v0, p0, LX/8vj;->c:Ljava/lang/Object;

    if-eqz v0, :cond_6

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-le v0, v12, :cond_6

    .line 1419115
    iget-object v0, p0, LX/8vj;->c:Ljava/lang/Object;

    check-cast v0, Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->invalidate()V

    .line 1419116
    :cond_6
    return-void

    :cond_7
    move v0, v6

    goto :goto_2
.end method

.method private E()V
    .locals 1

    .prologue
    .line 1418936
    iget-object v0, p0, LX/8vj;->aQ:LX/8vn;

    if-eqz v0, :cond_0

    .line 1418937
    iget-object v0, p0, LX/8vj;->aQ:LX/8vn;

    invoke-virtual {v0}, LX/8vn;->b()V

    .line 1418938
    iget-object v0, p0, LX/8vj;->aR:LX/8vn;

    invoke-virtual {v0}, LX/8vn;->b()V

    .line 1418939
    :cond_0
    return-void
.end method

.method public static synthetic a(LX/8vj;)I
    .locals 1

    .prologue
    .line 1419122
    invoke-virtual {p0}, LX/8vj;->getWindowAttachCount()I

    move-result v0

    return v0
.end method

.method public static a(Landroid/graphics/Rect;Landroid/graphics/Rect;I)I
    .locals 5

    .prologue
    .line 1419123
    sparse-switch p2, :sswitch_data_0

    .line 1419124
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT, FOCUS_FORWARD, FOCUS_BACKWARD}."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1419125
    :sswitch_0
    iget v3, p0, Landroid/graphics/Rect;->right:I

    .line 1419126
    iget v0, p0, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int v2, v0, v1

    .line 1419127
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 1419128
    iget v0, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v0, v4

    .line 1419129
    :goto_0
    sub-int/2addr v1, v3

    .line 1419130
    sub-int/2addr v0, v2

    .line 1419131
    mul-int/2addr v0, v0

    mul-int/2addr v1, v1

    add-int/2addr v0, v1

    return v0

    .line 1419132
    :sswitch_1
    iget v0, p0, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int v3, v0, v1

    .line 1419133
    iget v2, p0, Landroid/graphics/Rect;->bottom:I

    .line 1419134
    iget v0, p1, Landroid/graphics/Rect;->left:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v0

    .line 1419135
    iget v0, p1, Landroid/graphics/Rect;->top:I

    goto :goto_0

    .line 1419136
    :sswitch_2
    iget v3, p0, Landroid/graphics/Rect;->left:I

    .line 1419137
    iget v0, p0, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int v2, v0, v1

    .line 1419138
    iget v1, p1, Landroid/graphics/Rect;->right:I

    .line 1419139
    iget v0, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v0, v4

    .line 1419140
    goto :goto_0

    .line 1419141
    :sswitch_3
    iget v0, p0, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int v3, v0, v1

    .line 1419142
    iget v2, p0, Landroid/graphics/Rect;->top:I

    .line 1419143
    iget v0, p1, Landroid/graphics/Rect;->left:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v0

    .line 1419144
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    goto :goto_0

    .line 1419145
    :sswitch_4
    iget v0, p0, Landroid/graphics/Rect;->right:I

    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int v3, v0, v1

    .line 1419146
    iget v0, p0, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int v2, v0, v1

    .line 1419147
    iget v0, p1, Landroid/graphics/Rect;->left:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v0

    .line 1419148
    iget v0, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v0, v4

    .line 1419149
    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_4
        0x2 -> :sswitch_4
        0x11 -> :sswitch_2
        0x21 -> :sswitch_3
        0x42 -> :sswitch_0
        0x82 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(Ljava/util/ArrayList;I)Landroid/view/View;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;I)",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .line 1419150
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 1419151
    if-lez v3, :cond_2

    .line 1419152
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_1

    .line 1419153
    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1419154
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, LX/8vc;

    iget v1, v1, LX/8vc;->d:I

    if-ne v1, p1, :cond_0

    .line 1419155
    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1419156
    :goto_1
    return-object v0

    .line 1419157
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1419158
    :cond_1
    add-int/lit8 v0, v3, -0x1

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_1

    .line 1419159
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static synthetic a(LX/8vj;Landroid/view/View;Z)V
    .locals 0

    .prologue
    .line 1419160
    invoke-virtual {p0, p1, p2}, LX/8vj;->removeDetachedView(Landroid/view/View;Z)V

    return-void
.end method

.method public static synthetic a(LX/8vj;Z)V
    .locals 0

    .prologue
    .line 1419161
    invoke-virtual {p0, p1}, LX/8vj;->setChildrenDrawnWithCacheEnabled(Z)V

    return-void
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 1419162
    iget-object v0, p0, LX/8vj;->o:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1419163
    iget-object v0, p0, LX/8vj;->m:Landroid/graphics/drawable/Drawable;

    .line 1419164
    iget-object v1, p0, LX/8vj;->o:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1419165
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1419166
    :cond_0
    return-void
.end method

.method public static a(LX/8vj;FFI)Z
    .locals 4

    .prologue
    .line 1419167
    float-to-int v0, p1

    float-to-int v1, p2

    invoke-direct {p0, v0, v1}, LX/8vj;->b(II)I

    move-result v0

    .line 1419168
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1419169
    iget-object v1, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    invoke-interface {v1, v0}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v2

    .line 1419170
    iget v1, p0, LX/8vi;->V:I

    sub-int v1, v0, v1

    invoke-virtual {p0, v1}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1419171
    if-eqz v1, :cond_0

    .line 1419172
    invoke-static {v1, v0, v2, v3}, LX/8vj;->c(Landroid/view/View;IJ)Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v0

    iput-object v0, p0, LX/8vj;->az:Landroid/view/ContextMenu$ContextMenuInfo;

    .line 1419173
    invoke-super {p0, p0}, LX/8vi;->showContextMenuForChild(Landroid/view/View;)Z

    move-result v0

    .line 1419174
    :goto_0
    return v0

    :cond_0
    invoke-static {p0, p1, p2, p3}, LX/8vj;->a(LX/8vj;FFI)Z

    move-result v0

    goto :goto_0
.end method

.method public static synthetic a(LX/8vj;IIIIIIIIZ)Z
    .locals 1

    .prologue
    .line 1418797
    invoke-virtual/range {p0 .. p9}, LX/8vj;->overScrollBy(IIIIIIIIZ)Z

    move-result v0

    return v0
.end method

.method private b(II)I
    .locals 4

    .prologue
    .line 1418724
    iget-object v0, p0, LX/8vj;->ay:Landroid/graphics/Rect;

    .line 1418725
    if-nez v0, :cond_0

    .line 1418726
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/8vj;->ay:Landroid/graphics/Rect;

    .line 1418727
    iget-object v0, p0, LX/8vj;->ay:Landroid/graphics/Rect;

    .line 1418728
    :cond_0
    invoke-virtual {p0}, LX/8vj;->getChildCount()I

    move-result v1

    .line 1418729
    add-int/lit8 v1, v1, -0x1

    :goto_0
    if-ltz v1, :cond_2

    .line 1418730
    invoke-virtual {p0, v1}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1418731
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_1

    .line 1418732
    invoke-virtual {v2, v0}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 1418733
    invoke-virtual {v0, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1418734
    iget v0, p0, LX/8vi;->V:I

    add-int/2addr v0, v1

    .line 1418735
    :goto_1
    return v0

    .line 1418736
    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 1418737
    :cond_2
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public static synthetic b(LX/8vj;)I
    .locals 1

    .prologue
    .line 1418738
    invoke-virtual {p0}, LX/8vj;->getWindowAttachCount()I

    move-result v0

    return v0
.end method

.method private b(IZ)V
    .locals 8

    .prologue
    const/16 v4, 0xb

    const/4 v3, 0x3

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 1418739
    iget v0, p0, LX/8vj;->b:I

    if-nez v0, :cond_1

    .line 1418740
    :cond_0
    :goto_0
    return-void

    .line 1418741
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v4, :cond_4

    .line 1418742
    if-eqz p2, :cond_4

    iget v0, p0, LX/8vj;->b:I

    if-ne v0, v3, :cond_4

    iget-object v0, p0, LX/8vj;->c:Ljava/lang/Object;

    if-nez v0, :cond_4

    .line 1418743
    iget-object v0, p0, LX/8vj;->d:Ljava/lang/Object;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/8vj;->d:Ljava/lang/Object;

    check-cast v0, LX/8vW;

    .line 1418744
    iget-object v2, v0, LX/8vW;->a:LX/8vV;

    if-eqz v2, :cond_13

    const/4 v2, 0x1

    :goto_1
    move v0, v2

    .line 1418745
    if-nez v0, :cond_3

    .line 1418746
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "AbsListView: attempted to start selection mode for CHOICE_MODE_MULTIPLE_MODAL but no choice mode callback was supplied. Call setMultiChoiceModeListener to set a callback."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1418747
    :cond_3
    iget-object v0, p0, LX/8vj;->d:Ljava/lang/Object;

    check-cast v0, LX/8vW;

    invoke-virtual {p0, v0}, LX/8vj;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v0

    iput-object v0, p0, LX/8vj;->c:Ljava/lang/Object;

    .line 1418748
    :cond_4
    iget v0, p0, LX/8vj;->b:I

    const/4 v2, 0x2

    if-eq v0, v2, :cond_5

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v4, :cond_b

    iget v0, p0, LX/8vj;->b:I

    if-ne v0, v3, :cond_b

    .line 1418749
    :cond_5
    iget-object v0, p0, LX/8vj;->f:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseBooleanArray;->get(IZ)Z

    move-result v0

    .line 1418750
    iget-object v1, p0, LX/8vj;->f:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, p1, p2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 1418751
    iget-object v1, p0, LX/8vj;->g:LX/0tf;

    if-eqz v1, :cond_6

    iget-object v1, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1418752
    if-eqz p2, :cond_9

    .line 1418753
    iget-object v1, p0, LX/8vj;->g:LX/0tf;

    iget-object v2, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    invoke-interface {v2, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, LX/0tf;->b(JLjava/lang/Object;)V

    .line 1418754
    :cond_6
    :goto_2
    if-eq v0, p2, :cond_7

    .line 1418755
    if-eqz p2, :cond_a

    .line 1418756
    iget v0, p0, LX/8vj;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/8vj;->e:I

    .line 1418757
    :cond_7
    :goto_3
    iget-object v0, p0, LX/8vj;->c:Ljava/lang/Object;

    if-eqz v0, :cond_8

    .line 1418758
    iget-object v0, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    .line 1418759
    iget-object v1, p0, LX/8vj;->d:Ljava/lang/Object;

    check-cast v1, LX/8vW;

    iget-object v2, p0, LX/8vj;->c:Ljava/lang/Object;

    check-cast v2, Landroid/view/ActionMode;

    move v3, p1

    move v6, p2

    invoke-virtual/range {v1 .. v6}, LX/8vW;->a(Landroid/view/ActionMode;IJZ)V

    .line 1418760
    :cond_8
    :goto_4
    iget-boolean v0, p0, LX/8vi;->af:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/8vi;->at:Z

    if-nez v0, :cond_0

    .line 1418761
    iput-boolean v7, p0, LX/8vj;->aj:Z

    .line 1418762
    invoke-virtual {p0}, LX/8vi;->o()V

    .line 1418763
    invoke-virtual {p0}, LX/8vj;->requestLayout()V

    goto/16 :goto_0

    .line 1418764
    :cond_9
    iget-object v1, p0, LX/8vj;->g:LX/0tf;

    iget-object v2, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    invoke-interface {v2, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, LX/0tf;->b(J)V

    goto :goto_2

    .line 1418765
    :cond_a
    iget v0, p0, LX/8vj;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/8vj;->e:I

    goto :goto_3

    .line 1418766
    :cond_b
    iget-object v0, p0, LX/8vj;->g:LX/0tf;

    if-eqz v0, :cond_10

    iget-object v0, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v0

    if-eqz v0, :cond_10

    move v0, v7

    .line 1418767
    :goto_5
    if-nez p2, :cond_d

    const/4 v2, 0x0

    .line 1418768
    iget v3, p0, LX/8vj;->b:I

    if-eqz v3, :cond_c

    iget-object v3, p0, LX/8vj;->f:Landroid/util/SparseBooleanArray;

    if-eqz v3, :cond_c

    .line 1418769
    iget-object v3, p0, LX/8vj;->f:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, p1, v2}, Landroid/util/SparseBooleanArray;->get(IZ)Z

    move-result v2

    .line 1418770
    :cond_c
    move v2, v2

    .line 1418771
    if-eqz v2, :cond_e

    .line 1418772
    :cond_d
    iget-object v2, p0, LX/8vj;->f:Landroid/util/SparseBooleanArray;

    invoke-virtual {v2}, Landroid/util/SparseBooleanArray;->clear()V

    .line 1418773
    if-eqz v0, :cond_e

    .line 1418774
    iget-object v2, p0, LX/8vj;->g:LX/0tf;

    invoke-virtual {v2}, LX/0tf;->b()V

    .line 1418775
    :cond_e
    if-eqz p2, :cond_11

    .line 1418776
    iget-object v1, p0, LX/8vj;->f:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, p1, v7}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 1418777
    if-eqz v0, :cond_f

    .line 1418778
    iget-object v0, p0, LX/8vj;->g:LX/0tf;

    iget-object v1, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    invoke-interface {v1, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v1}, LX/0tf;->b(JLjava/lang/Object;)V

    .line 1418779
    :cond_f
    iput v7, p0, LX/8vj;->e:I

    goto :goto_4

    :cond_10
    move v0, v1

    .line 1418780
    goto :goto_5

    .line 1418781
    :cond_11
    iget-object v0, p0, LX/8vj;->f:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, LX/8vj;->f:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v0

    if-nez v0, :cond_8

    .line 1418782
    :cond_12
    iput v1, p0, LX/8vj;->e:I

    goto/16 :goto_4

    :cond_13
    const/4 v2, 0x0

    goto/16 :goto_1
.end method

.method public static synthetic b(LX/8vj;Landroid/view/View;Z)V
    .locals 0

    .prologue
    .line 1418783
    invoke-virtual {p0, p1, p2}, LX/8vj;->removeDetachedView(Landroid/view/View;Z)V

    return-void
.end method

.method public static synthetic b(LX/8vj;Z)V
    .locals 0

    .prologue
    .line 1418784
    invoke-virtual {p0, p1}, LX/8vj;->setChildrenDrawingCacheEnabled(Z)V

    return-void
.end method

.method private b(Landroid/view/MotionEvent;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1418785
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const v2, 0xff00

    and-int/2addr v0, v2

    shr-int/lit8 v0, v0, 0x8

    .line 1418786
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v2

    .line 1418787
    iget v3, p0, LX/8vj;->aP:I

    if-ne v2, v3, :cond_0

    .line 1418788
    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 1418789
    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, LX/8vj;->D:I

    .line 1418790
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, LX/8vj;->E:I

    .line 1418791
    iput v1, p0, LX/8vj;->H:I

    .line 1418792
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, LX/8vj;->aP:I

    .line 1418793
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 1418794
    goto :goto_0
.end method

.method public static synthetic b(LX/8vj;IIIIIIIIZ)Z
    .locals 1

    .prologue
    .line 1418795
    invoke-virtual/range {p0 .. p9}, LX/8vj;->overScrollBy(IIIIIIIIZ)Z

    move-result v0

    return v0
.end method

.method private static c(Landroid/view/View;IJ)Landroid/view/ContextMenu$ContextMenuInfo;
    .locals 2

    .prologue
    .line 1418796
    new-instance v0, LX/8vk;

    invoke-direct {v0, p0, p1, p2, p3}, LX/8vk;-><init>(Landroid/view/View;IJ)V

    return-object v0
.end method

.method private c(II)V
    .locals 1

    .prologue
    .line 1418722
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/8vj;->a(IIZ)V

    .line 1418723
    return-void
.end method

.method public static synthetic c(LX/8vj;Landroid/view/View;Z)V
    .locals 0

    .prologue
    .line 1418798
    invoke-virtual {p0, p1, p2}, LX/8vj;->removeDetachedView(Landroid/view/View;Z)V

    return-void
.end method

.method public static synthetic c(LX/8vj;IIIIIIIIZ)Z
    .locals 1

    .prologue
    .line 1418799
    invoke-virtual/range {p0 .. p9}, LX/8vj;->overScrollBy(IIIIIIIIZ)Z

    move-result v0

    return v0
.end method

.method public static synthetic d(LX/8vj;Landroid/view/View;Z)V
    .locals 0

    .prologue
    .line 1418800
    invoke-virtual {p0, p1, p2}, LX/8vj;->removeDetachedView(Landroid/view/View;Z)V

    return-void
.end method

.method public static synthetic e(LX/8vj;Landroid/view/View;Z)V
    .locals 0

    .prologue
    .line 1418801
    invoke-virtual {p0, p1, p2}, LX/8vj;->removeDetachedView(Landroid/view/View;Z)V

    return-void
.end method

.method private f(I)Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1418802
    iget v0, p0, LX/8vj;->D:I

    sub-int v2, p1, v0

    .line 1418803
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v4

    .line 1418804
    invoke-virtual {p0}, LX/8vj;->getScrollX()I

    move-result v0

    if-eqz v0, :cond_5

    move v0, v3

    .line 1418805
    :goto_0
    if-nez v0, :cond_0

    iget v5, p0, LX/8vj;->aK:I

    if-le v4, v5, :cond_4

    .line 1418806
    :cond_0
    invoke-direct {p0}, LX/8vj;->A()V

    .line 1418807
    if-eqz v0, :cond_6

    .line 1418808
    const/4 v0, 0x5

    iput v0, p0, LX/8vj;->F:I

    move v0, v1

    move-object v2, p0

    .line 1418809
    :goto_1
    iput v0, v2, LX/8vj;->H:I

    .line 1418810
    invoke-virtual {p0}, LX/8vj;->getHandler()Landroid/os/Handler;

    move-result-object v0

    .line 1418811
    if-eqz v0, :cond_1

    .line 1418812
    iget-object v2, p0, LX/8vj;->aB:Lit/sephiroth/android/library/widget/AbsHListView$CheckForLongPress;

    invoke-static {v0, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1418813
    :cond_1
    invoke-virtual {p0, v1}, LX/8vj;->setPressed(Z)V

    .line 1418814
    iget v0, p0, LX/8vj;->A:I

    iget v2, p0, LX/8vi;->V:I

    sub-int/2addr v0, v2

    invoke-virtual {p0, v0}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1418815
    if-eqz v0, :cond_2

    .line 1418816
    invoke-virtual {v0, v1}, Landroid/view/View;->setPressed(Z)V

    .line 1418817
    :cond_2
    invoke-virtual {p0, v3}, LX/8vj;->a(I)V

    .line 1418818
    invoke-virtual {p0}, LX/8vj;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 1418819
    if-eqz v0, :cond_3

    .line 1418820
    invoke-interface {v0, v3}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 1418821
    :cond_3
    invoke-direct {p0, p1}, LX/8vj;->g(I)V

    move v1, v3

    .line 1418822
    :cond_4
    return v1

    :cond_5
    move v0, v1

    .line 1418823
    goto :goto_0

    .line 1418824
    :cond_6
    const/4 v0, 0x3

    iput v0, p0, LX/8vj;->F:I

    .line 1418825
    if-lez v2, :cond_7

    iget v0, p0, LX/8vj;->aK:I

    move-object v2, p0

    goto :goto_1

    :cond_7
    iget v0, p0, LX/8vj;->aK:I

    neg-int v0, v0

    move-object v2, p0

    goto :goto_1
.end method

.method private g(I)V
    .locals 14

    .prologue
    const/4 v10, 0x5

    const/4 v13, 0x3

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 1418826
    iget v0, p0, LX/8vj;->D:I

    sub-int v12, p1, v0

    .line 1418827
    iget v0, p0, LX/8vj;->H:I

    sub-int v1, v12, v0

    .line 1418828
    iget v0, p0, LX/8vj;->G:I

    const/high16 v3, -0x80000000

    if-eq v0, v3, :cond_7

    iget v0, p0, LX/8vj;->G:I

    sub-int v0, p1, v0

    .line 1418829
    :goto_0
    iget v3, p0, LX/8vj;->F:I

    if-ne v3, v13, :cond_b

    .line 1418830
    iget v3, p0, LX/8vj;->G:I

    if-eq p1, v3, :cond_6

    .line 1418831
    invoke-static {v12}, Ljava/lang/Math;->abs(I)I

    move-result v3

    iget v4, p0, LX/8vj;->aK:I

    if-le v3, v4, :cond_0

    .line 1418832
    invoke-virtual {p0}, LX/8vj;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    .line 1418833
    if-eqz v3, :cond_0

    .line 1418834
    invoke-interface {v3, v9}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 1418835
    :cond_0
    iget v3, p0, LX/8vj;->A:I

    if-ltz v3, :cond_8

    .line 1418836
    iget v3, p0, LX/8vj;->A:I

    iget v4, p0, LX/8vi;->V:I

    sub-int/2addr v3, v4

    .line 1418837
    :goto_1
    invoke-virtual {p0, v3}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 1418838
    if-eqz v4, :cond_1a

    .line 1418839
    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v4

    .line 1418840
    :goto_2
    if-eqz v0, :cond_19

    .line 1418841
    invoke-virtual {p0, v1, v0}, LX/8vj;->a(II)Z

    move-result v1

    .line 1418842
    :goto_3
    invoke-virtual {p0, v3}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1418843
    if-eqz v3, :cond_5

    .line 1418844
    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v3

    .line 1418845
    if-eqz v1, :cond_4

    .line 1418846
    neg-int v0, v0

    sub-int v1, v3, v4

    sub-int v1, v0, v1

    .line 1418847
    invoke-virtual {p0}, LX/8vj;->getScrollX()I

    move-result v3

    iget v7, p0, LX/8vj;->Q:I

    move-object v0, p0

    move v4, v2

    move v5, v2

    move v6, v2

    move v8, v2

    invoke-virtual/range {v0 .. v9}, LX/8vj;->overScrollBy(IIIIIIIIZ)Z

    .line 1418848
    iget v0, p0, LX/8vj;->Q:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    invoke-virtual {p0}, LX/8vj;->getScrollX()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    if-ne v0, v3, :cond_1

    .line 1418849
    iget-object v0, p0, LX/8vj;->au:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_1

    .line 1418850
    iget-object v0, p0, LX/8vj;->au:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    .line 1418851
    :cond_1
    invoke-virtual {p0}, LX/8vj;->getOverScrollMode()I

    move-result v0

    .line 1418852
    if-eqz v0, :cond_2

    if-ne v0, v9, :cond_4

    invoke-static {p0}, LX/8vj;->r(LX/8vj;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1418853
    :cond_2
    iput v2, p0, LX/8vj;->aU:I

    .line 1418854
    iput v10, p0, LX/8vj;->F:I

    .line 1418855
    if-lez v12, :cond_9

    .line 1418856
    iget-object v0, p0, LX/8vj;->aQ:LX/8vn;

    int-to-float v1, v1

    invoke-virtual {p0}, LX/8vj;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v1, v3

    invoke-virtual {v0, v1}, LX/8vn;->a(F)V

    .line 1418857
    iget-object v0, p0, LX/8vj;->aR:LX/8vn;

    invoke-virtual {v0}, LX/8vn;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1418858
    iget-object v0, p0, LX/8vj;->aR:LX/8vn;

    invoke-virtual {v0}, LX/8vn;->c()V

    .line 1418859
    :cond_3
    iget-object v0, p0, LX/8vj;->aQ:LX/8vn;

    invoke-virtual {v0, v2}, LX/8vn;->a(Z)Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/8vj;->invalidate(Landroid/graphics/Rect;)V

    .line 1418860
    :cond_4
    :goto_4
    iput p1, p0, LX/8vj;->D:I

    .line 1418861
    :cond_5
    iput p1, p0, LX/8vj;->G:I

    .line 1418862
    :cond_6
    :goto_5
    return-void

    :cond_7
    move v0, v1

    .line 1418863
    goto/16 :goto_0

    .line 1418864
    :cond_8
    invoke-virtual {p0}, LX/8vj;->getChildCount()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    goto/16 :goto_1

    .line 1418865
    :cond_9
    if-gez v12, :cond_4

    .line 1418866
    iget-object v0, p0, LX/8vj;->aR:LX/8vn;

    int-to-float v1, v1

    invoke-virtual {p0}, LX/8vj;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-virtual {v0, v1}, LX/8vn;->a(F)V

    .line 1418867
    iget-object v0, p0, LX/8vj;->aQ:LX/8vn;

    invoke-virtual {v0}, LX/8vn;->a()Z

    move-result v0

    if-nez v0, :cond_a

    .line 1418868
    iget-object v0, p0, LX/8vj;->aQ:LX/8vn;

    invoke-virtual {v0}, LX/8vn;->c()V

    .line 1418869
    :cond_a
    iget-object v0, p0, LX/8vj;->aR:LX/8vn;

    invoke-virtual {v0, v9}, LX/8vn;->a(Z)Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/8vj;->invalidate(Landroid/graphics/Rect;)V

    goto :goto_4

    .line 1418870
    :cond_b
    iget v1, p0, LX/8vj;->F:I

    if-ne v1, v10, :cond_6

    .line 1418871
    iget v1, p0, LX/8vj;->G:I

    if-eq p1, v1, :cond_6

    .line 1418872
    invoke-virtual {p0}, LX/8vj;->getScrollX()I

    move-result v3

    .line 1418873
    sub-int v4, v3, v0

    .line 1418874
    iget v1, p0, LX/8vj;->G:I

    if-le p1, v1, :cond_15

    move v10, v9

    .line 1418875
    :goto_6
    iget v1, p0, LX/8vj;->aU:I

    if-nez v1, :cond_c

    .line 1418876
    iput v10, p0, LX/8vj;->aU:I

    .line 1418877
    :cond_c
    neg-int v1, v0

    .line 1418878
    if-gez v4, :cond_d

    if-gez v3, :cond_e

    :cond_d
    if-lez v4, :cond_16

    if-gtz v3, :cond_16

    .line 1418879
    :cond_e
    neg-int v1, v3

    .line 1418880
    add-int/2addr v0, v1

    move v11, v0

    .line 1418881
    :goto_7
    if-eqz v1, :cond_11

    .line 1418882
    invoke-virtual {p0}, LX/8vj;->getScrollX()I

    move-result v3

    iget v7, p0, LX/8vj;->Q:I

    move-object v0, p0

    move v4, v2

    move v5, v2

    move v6, v2

    move v8, v2

    invoke-virtual/range {v0 .. v9}, LX/8vj;->overScrollBy(IIIIIIIIZ)Z

    .line 1418883
    invoke-virtual {p0}, LX/8vj;->getOverScrollMode()I

    move-result v0

    .line 1418884
    if-eqz v0, :cond_f

    if-ne v0, v9, :cond_11

    invoke-static {p0}, LX/8vj;->r(LX/8vj;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 1418885
    :cond_f
    if-lez v12, :cond_17

    .line 1418886
    iget-object v0, p0, LX/8vj;->aQ:LX/8vn;

    int-to-float v1, v1

    invoke-virtual {p0}, LX/8vj;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v1, v3

    invoke-virtual {v0, v1}, LX/8vn;->a(F)V

    .line 1418887
    iget-object v0, p0, LX/8vj;->aR:LX/8vn;

    invoke-virtual {v0}, LX/8vn;->a()Z

    move-result v0

    if-nez v0, :cond_10

    .line 1418888
    iget-object v0, p0, LX/8vj;->aR:LX/8vn;

    invoke-virtual {v0}, LX/8vn;->c()V

    .line 1418889
    :cond_10
    iget-object v0, p0, LX/8vj;->aQ:LX/8vn;

    invoke-virtual {v0, v2}, LX/8vn;->a(Z)Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/8vj;->invalidate(Landroid/graphics/Rect;)V

    .line 1418890
    :cond_11
    :goto_8
    if-eqz v11, :cond_14

    .line 1418891
    invoke-virtual {p0}, LX/8vj;->getScrollX()I

    move-result v0

    if-eqz v0, :cond_12

    .line 1418892
    iget-object v0, p0, LX/8vj;->a:LX/8vS;

    invoke-virtual {v0, v2}, LX/8vS;->a(I)V

    .line 1418893
    invoke-direct {p0}, LX/8vj;->w()V

    .line 1418894
    :cond_12
    invoke-virtual {p0, v11, v11}, LX/8vj;->a(II)Z

    .line 1418895
    iput v13, p0, LX/8vj;->F:I

    .line 1418896
    const/4 v0, -0x1

    .line 1418897
    invoke-virtual {p0}, LX/8vj;->getChildCount()I

    move-result v3

    .line 1418898
    if-nez v3, :cond_1b

    .line 1418899
    :goto_9
    move v0, v0

    .line 1418900
    iput v2, p0, LX/8vj;->H:I

    .line 1418901
    iget v1, p0, LX/8vi;->V:I

    sub-int v1, v0, v1

    invoke-virtual {p0, v1}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1418902
    if-eqz v1, :cond_13

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    :cond_13
    iput v2, p0, LX/8vj;->B:I

    .line 1418903
    iput p1, p0, LX/8vj;->D:I

    .line 1418904
    iput v0, p0, LX/8vj;->A:I

    .line 1418905
    :cond_14
    iput p1, p0, LX/8vj;->G:I

    .line 1418906
    iput v10, p0, LX/8vj;->aU:I

    goto/16 :goto_5

    .line 1418907
    :cond_15
    const/4 v1, -0x1

    move v10, v1

    goto/16 :goto_6

    :cond_16
    move v11, v2

    .line 1418908
    goto :goto_7

    .line 1418909
    :cond_17
    if-gez v12, :cond_11

    .line 1418910
    iget-object v0, p0, LX/8vj;->aR:LX/8vn;

    int-to-float v1, v1

    invoke-virtual {p0}, LX/8vj;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v1, v3

    invoke-virtual {v0, v1}, LX/8vn;->a(F)V

    .line 1418911
    iget-object v0, p0, LX/8vj;->aQ:LX/8vn;

    invoke-virtual {v0}, LX/8vn;->a()Z

    move-result v0

    if-nez v0, :cond_18

    .line 1418912
    iget-object v0, p0, LX/8vj;->aQ:LX/8vn;

    invoke-virtual {v0}, LX/8vn;->c()V

    .line 1418913
    :cond_18
    iget-object v0, p0, LX/8vj;->aR:LX/8vn;

    invoke-virtual {v0, v9}, LX/8vn;->a(Z)Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/8vj;->invalidate(Landroid/graphics/Rect;)V

    goto :goto_8

    :cond_19
    move v1, v2

    goto/16 :goto_3

    :cond_1a
    move v4, v2

    goto/16 :goto_2

    .line 1418914
    :cond_1b
    invoke-virtual {p0, p1}, LX/8vj;->c(I)I

    move-result v1

    .line 1418915
    if-eq v1, v0, :cond_1c

    move v0, v1

    goto :goto_9

    :cond_1c
    iget v0, p0, LX/8vi;->V:I

    add-int/2addr v0, v3

    add-int/lit8 v0, v0, -0x1

    goto :goto_9
.end method

.method private p()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1418916
    invoke-virtual {p0, v0}, LX/8vj;->setClickable(Z)V

    .line 1418917
    invoke-virtual {p0, v0}, LX/8vj;->setFocusableInTouchMode(Z)V

    .line 1418918
    invoke-virtual {p0, v1}, LX/8vj;->setWillNotDraw(Z)V

    .line 1418919
    invoke-virtual {p0, v1}, LX/8vj;->setAlwaysDrawnWithCacheEnabled(Z)V

    .line 1418920
    invoke-virtual {p0, v0}, LX/8vj;->setScrollingCacheEnabled(Z)V

    .line 1418921
    invoke-virtual {p0}, LX/8vj;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 1418922
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, LX/8vj;->aK:I

    .line 1418923
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v1

    iput v1, p0, LX/8vj;->aM:I

    .line 1418924
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v1

    iput v1, p0, LX/8vj;->aN:I

    .line 1418925
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledOverscrollDistance()I

    move-result v1

    iput v1, p0, LX/8vj;->Q:I

    .line 1418926
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledOverflingDistance()I

    move-result v0

    iput v0, p0, LX/8vj;->R:I

    .line 1418927
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 1418928
    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 1418929
    new-instance v0, LX/8vY;

    invoke-direct {v0, p0}, LX/8vY;-><init>(Landroid/view/View;)V

    .line 1418930
    :goto_0
    move-object v0, v0

    .line 1418931
    iput-object v0, p0, LX/8vj;->a:LX/8vS;

    .line 1418932
    return-void

    .line 1418933
    :cond_0
    const/16 v1, 0xe

    if-lt v0, v1, :cond_1

    .line 1418934
    new-instance v0, LX/8vX;

    invoke-direct {v0, p0}, LX/8vX;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 1418935
    :cond_1
    new-instance v0, LX/8vT;

    invoke-direct {v0, p0}, LX/8vT;-><init>(Landroid/view/View;)V

    goto :goto_0
.end method

.method public static r(LX/8vj;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1419117
    invoke-virtual {p0}, LX/8vj;->getChildCount()I

    move-result v2

    .line 1419118
    if-nez v2, :cond_1

    .line 1419119
    :cond_0
    :goto_0
    return v0

    .line 1419120
    :cond_1
    iget v3, p0, LX/8vi;->ao:I

    if-eq v2, v3, :cond_2

    move v0, v1

    goto :goto_0

    .line 1419121
    :cond_2
    invoke-virtual {p0, v1}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v3

    iget-object v4, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    if-lt v3, v4, :cond_3

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p0, v2}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v2

    invoke-virtual {p0}, LX/8vj;->getWidth()I

    move-result v3

    iget-object v4, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    sub-int/2addr v3, v4

    if-le v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public static u(LX/8vj;)Z
    .locals 1

    .prologue
    .line 1419176
    iget v0, p0, LX/8vj;->F:I

    packed-switch v0, :pswitch_data_0

    .line 1419177
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1419178
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private v()V
    .locals 2

    .prologue
    .line 1419319
    iget-object v0, p0, LX/8vj;->m:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 1419320
    invoke-virtual {p0}, LX/8vj;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1419321
    iget-object v0, p0, LX/8vj;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, LX/8vj;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 1419322
    :cond_0
    :goto_0
    return-void

    .line 1419323
    :cond_1
    iget-object v0, p0, LX/8vj;->m:Landroid/graphics/drawable/Drawable;

    sget-object v1, LX/8vj;->U:[I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    goto :goto_0
.end method

.method private w()V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 1419324
    iget-object v0, p0, LX/8vj;->a:LX/8vS;

    invoke-virtual {v0}, LX/8vS;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/8vj;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1419325
    invoke-virtual {p0}, LX/8vj;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 1419326
    :cond_0
    return-void
.end method

.method private y()V
    .locals 1

    .prologue
    .line 1419327
    iget-object v0, p0, LX/8vj;->au:Landroid/view/VelocityTracker;

    if-nez v0, :cond_0

    .line 1419328
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, LX/8vj;->au:Landroid/view/VelocityTracker;

    .line 1419329
    :cond_0
    return-void
.end method

.method private z()V
    .locals 1

    .prologue
    .line 1419330
    iget-object v0, p0, LX/8vj;->au:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    .line 1419331
    iget-object v0, p0, LX/8vj;->au:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 1419332
    const/4 v0, 0x0

    iput-object v0, p0, LX/8vj;->au:Landroid/view/VelocityTracker;

    .line 1419333
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I[Z)Landroid/view/View;
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/16 v5, 0x10

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1419334
    aput-boolean v3, p2, v3

    .line 1419335
    iget-object v0, p0, LX/8vj;->p:LX/8vf;

    const/4 v1, 0x0

    .line 1419336
    iget-object v2, v0, LX/8vf;->i:LX/0YU;

    if-nez v2, :cond_b

    .line 1419337
    :cond_0
    :goto_0
    move-object v1, v1

    .line 1419338
    if-eqz v1, :cond_2

    .line 1419339
    :cond_1
    :goto_1
    return-object v1

    .line 1419340
    :cond_2
    iget-object v0, p0, LX/8vj;->p:LX/8vf;

    .line 1419341
    iget v1, v0, LX/8vf;->f:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_c

    .line 1419342
    iget-object v1, v0, LX/8vf;->g:Ljava/util/ArrayList;

    invoke-static {v1, p1}, LX/8vj;->a(Ljava/util/ArrayList;I)Landroid/view/View;

    move-result-object v1

    .line 1419343
    :goto_2
    move-object v1, v1

    .line 1419344
    if-eqz v1, :cond_6

    .line 1419345
    iget-object v0, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    invoke-interface {v0, p1, v1, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1419346
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v2, v5, :cond_3

    .line 1419347
    invoke-virtual {v0}, Landroid/view/View;->getImportantForAccessibility()I

    move-result v2

    if-nez v2, :cond_3

    .line 1419348
    invoke-virtual {v0, v4}, Landroid/view/View;->setImportantForAccessibility(I)V

    .line 1419349
    :cond_3
    if-eq v0, v1, :cond_5

    .line 1419350
    iget-object v2, p0, LX/8vj;->p:LX/8vf;

    invoke-virtual {v2, v1, p1}, LX/8vf;->a(Landroid/view/View;I)V

    .line 1419351
    iget v1, p0, LX/8vj;->aH:I

    if-eqz v1, :cond_8

    .line 1419352
    iget v1, p0, LX/8vj;->aH:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheBackgroundColor(I)V

    move-object v1, v0

    .line 1419353
    :goto_3
    iget-boolean v0, p0, LX/8vj;->k:Z

    if-eqz v0, :cond_4

    .line 1419354
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1419355
    if-nez v0, :cond_9

    .line 1419356
    invoke-virtual {p0}, LX/8vj;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/8vc;

    .line 1419357
    :goto_4
    iget-object v2, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    invoke-interface {v2, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v2

    iput-wide v2, v0, LX/8vc;->e:J

    .line 1419358
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1419359
    :cond_4
    iget-object v0, p0, LX/8vi;->aq:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1419360
    iget-object v0, p0, LX/8vj;->aY:LX/8vd;

    if-nez v0, :cond_1

    .line 1419361
    new-instance v0, LX/8vd;

    invoke-direct {v0, p0}, LX/8vd;-><init>(LX/8vj;)V

    iput-object v0, p0, LX/8vj;->aY:LX/8vd;

    goto :goto_1

    .line 1419362
    :cond_5
    aput-boolean v4, p2, v3

    .line 1419363
    invoke-virtual {v0}, Landroid/view/View;->onFinishTemporaryDetach()V

    move-object v1, v0

    goto :goto_3

    .line 1419364
    :cond_6
    iget-object v0, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1419365
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v5, :cond_7

    .line 1419366
    invoke-virtual {v0}, Landroid/view/View;->getImportantForAccessibility()I

    move-result v1

    if-nez v1, :cond_7

    .line 1419367
    invoke-virtual {v0, v4}, Landroid/view/View;->setImportantForAccessibility(I)V

    .line 1419368
    :cond_7
    iget v1, p0, LX/8vj;->aH:I

    if-eqz v1, :cond_8

    .line 1419369
    iget v1, p0, LX/8vj;->aH:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheBackgroundColor(I)V

    :cond_8
    move-object v1, v0

    goto :goto_3

    .line 1419370
    :cond_9
    invoke-virtual {p0, v0}, LX/8vj;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 1419371
    invoke-virtual {p0, v0}, LX/8vj;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/8vc;

    goto :goto_4

    .line 1419372
    :cond_a
    check-cast v0, LX/8vc;

    goto :goto_4

    .line 1419373
    :cond_b
    iget-object v2, v0, LX/8vf;->i:LX/0YU;

    invoke-virtual {v2, p1}, LX/0YU;->g(I)I

    move-result v2

    .line 1419374
    if-ltz v2, :cond_0

    .line 1419375
    iget-object v1, v0, LX/8vf;->i:LX/0YU;

    invoke-virtual {v1, v2}, LX/0YU;->f(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 1419376
    iget-object v6, v0, LX/8vf;->i:LX/0YU;

    invoke-virtual {v6, v2}, LX/0YU;->d(I)V

    goto/16 :goto_0

    .line 1419377
    :cond_c
    iget-object v1, v0, LX/8vf;->a:LX/8vj;

    iget-object v1, v1, LX/8vj;->j:Landroid/widget/ListAdapter;

    invoke-interface {v1, p1}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v1

    .line 1419378
    if-ltz v1, :cond_d

    iget-object v2, v0, LX/8vf;->e:[Ljava/util/ArrayList;

    array-length v2, v2

    if-ge v1, v2, :cond_d

    .line 1419379
    iget-object v2, v0, LX/8vf;->e:[Ljava/util/ArrayList;

    aget-object v1, v2, v1

    invoke-static {v1, p1}, LX/8vj;->a(Ljava/util/ArrayList;I)Landroid/view/View;

    move-result-object v1

    goto/16 :goto_2

    .line 1419380
    :cond_d
    const/4 v1, 0x0

    goto/16 :goto_2
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1419381
    iget-object v0, p0, LX/8vj;->f:Landroid/util/SparseBooleanArray;

    if-eqz v0, :cond_0

    .line 1419382
    iget-object v0, p0, LX/8vj;->f:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->clear()V

    .line 1419383
    :cond_0
    iget-object v0, p0, LX/8vj;->g:LX/0tf;

    if-eqz v0, :cond_1

    .line 1419384
    iget-object v0, p0, LX/8vj;->g:LX/0tf;

    invoke-virtual {v0}, LX/0tf;->b()V

    .line 1419385
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, LX/8vj;->e:I

    .line 1419386
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 1419387
    iget v0, p0, LX/8vj;->aJ:I

    if-eq p1, v0, :cond_0

    .line 1419388
    iget-object v0, p0, LX/8vj;->aw:LX/8ve;

    if-eqz v0, :cond_0

    .line 1419389
    iput p1, p0, LX/8vj;->aJ:I

    .line 1419390
    :cond_0
    return-void
.end method

.method public final a(IIZ)V
    .locals 6

    .prologue
    .line 1419391
    iget-object v0, p0, LX/8vj;->av:Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    if-nez v0, :cond_0

    .line 1419392
    new-instance v0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    invoke-direct {v0, p0}, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;-><init>(LX/8vj;)V

    iput-object v0, p0, LX/8vj;->av:Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    .line 1419393
    :cond_0
    iget v0, p0, LX/8vi;->V:I

    .line 1419394
    invoke-virtual {p0}, LX/8vj;->getChildCount()I

    move-result v1

    .line 1419395
    add-int v2, v0, v1

    .line 1419396
    invoke-virtual {p0}, LX/8vj;->getPaddingLeft()I

    move-result v3

    .line 1419397
    invoke-virtual {p0}, LX/8vj;->getWidth()I

    move-result v4

    invoke-virtual {p0}, LX/8vj;->getPaddingRight()I

    move-result v5

    sub-int/2addr v4, v5

    .line 1419398
    if-eqz p1, :cond_2

    iget v5, p0, LX/8vi;->ao:I

    if-eqz v5, :cond_2

    if-eqz v1, :cond_2

    if-nez v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    if-ne v0, v3, :cond_1

    if-ltz p1, :cond_2

    :cond_1
    iget v0, p0, LX/8vi;->ao:I

    if-ne v2, v0, :cond_4

    add-int/lit8 v0, v1, -0x1

    invoke-virtual {p0, v0}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    if-ne v0, v4, :cond_4

    if-lez p1, :cond_4

    .line 1419399
    :cond_2
    iget-object v0, p0, LX/8vj;->av:Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    invoke-virtual {v0}, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->b()V

    .line 1419400
    iget-object v0, p0, LX/8vj;->I:Lit/sephiroth/android/library/widget/AbsHListView$PositionScroller;

    if-eqz v0, :cond_3

    .line 1419401
    iget-object v0, p0, LX/8vj;->I:Lit/sephiroth/android/library/widget/AbsHListView$PositionScroller;

    invoke-virtual {v0}, Lit/sephiroth/android/library/widget/AbsHListView$PositionScroller;->a()V

    .line 1419402
    :cond_3
    :goto_0
    return-void

    .line 1419403
    :cond_4
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, LX/8vj;->a(I)V

    .line 1419404
    iget-object v0, p0, LX/8vj;->av:Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    const/4 v3, 0x0

    .line 1419405
    if-gez p1, :cond_5

    const v2, 0x7fffffff

    .line 1419406
    :goto_1
    iput v2, v0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->c:I

    .line 1419407
    iget-object v4, v0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->b:LX/8vs;

    if-eqz p3, :cond_6

    sget-object v1, LX/8vj;->T:Landroid/view/animation/Interpolator;

    .line 1419408
    :goto_2
    iput-object v1, v4, LX/8vs;->d:Landroid/view/animation/Interpolator;

    .line 1419409
    iget-object v1, v0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->b:LX/8vs;

    move v4, p1

    move v5, v3

    move p0, p2

    .line 1419410
    const/4 p1, 0x0

    iput p1, v1, LX/8vs;->a:I

    .line 1419411
    iget-object p1, v1, LX/8vs;->b:LX/8vr;

    invoke-virtual {p1, v2, v4, p0}, LX/8vr;->a(III)V

    .line 1419412
    iget-object p1, v1, LX/8vs;->c:LX/8vr;

    invoke-virtual {p1, v3, v5, p0}, LX/8vr;->a(III)V

    .line 1419413
    iget-object v1, v0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    const/4 v2, 0x4

    iput v2, v1, LX/8vj;->F:I

    .line 1419414
    iget-object v1, v0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a:LX/8vj;

    iget-object v1, v1, LX/8vj;->a:LX/8vS;

    invoke-virtual {v1, v0}, LX/8vS;->a(Ljava/lang/Runnable;)V

    .line 1419415
    goto :goto_0

    :cond_5
    move v2, v3

    .line 1419416
    goto :goto_1

    .line 1419417
    :cond_6
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public final a(ILandroid/view/View;)V
    .locals 9

    .prologue
    const/4 v5, -0x1

    .line 1419418
    if-eq p1, v5, :cond_0

    .line 1419419
    iput p1, p0, LX/8vj;->n:I

    .line 1419420
    :cond_0
    iget-object v0, p0, LX/8vj;->o:Landroid/graphics/Rect;

    .line 1419421
    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v1

    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {p2}, Landroid/view/View;->getRight()I

    move-result v3

    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 1419422
    iget v1, v0, Landroid/graphics/Rect;->left:I

    iget v2, v0, Landroid/graphics/Rect;->top:I

    iget v3, v0, Landroid/graphics/Rect;->right:I

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 1419423
    iget-object v4, p0, LX/8vj;->o:Landroid/graphics/Rect;

    iget v6, p0, LX/8vj;->q:I

    sub-int v6, v1, v6

    iget v7, p0, LX/8vj;->r:I

    sub-int v7, v2, v7

    iget v8, p0, LX/8vj;->s:I

    add-int/2addr v8, v3

    iget p1, p0, LX/8vj;->t:I

    add-int/2addr p1, v0

    invoke-virtual {v4, v6, v7, v8, p1}, Landroid/graphics/Rect;->set(IIII)V

    .line 1419424
    iget-boolean v0, p0, LX/8vj;->aI:Z

    .line 1419425
    invoke-virtual {p2}, Landroid/view/View;->isEnabled()Z

    move-result v1

    if-eq v1, v0, :cond_1

    .line 1419426
    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/8vj;->aI:Z

    .line 1419427
    iget v0, p0, LX/8vi;->ak:I

    move v0, v0

    .line 1419428
    if-eq v0, v5, :cond_1

    .line 1419429
    invoke-virtual {p0}, LX/8vj;->refreshDrawableState()V

    .line 1419430
    :cond_1
    return-void

    .line 1419431
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract a(Z)V
.end method

.method public final a(II)Z
    .locals 20

    .prologue
    .line 1419432
    invoke-virtual/range {p0 .. p0}, LX/8vj;->getChildCount()I

    move-result v9

    .line 1419433
    if-nez v9, :cond_0

    .line 1419434
    const/4 v2, 0x1

    .line 1419435
    :goto_0
    return v2

    .line 1419436
    :cond_0
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v3

    .line 1419437
    add-int/lit8 v2, v9, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v4

    .line 1419438
    move-object/from16 v0, p0

    iget-object v5, v0, LX/8vj;->u:Landroid/graphics/Rect;

    .line 1419439
    rsub-int/lit8 v10, v3, 0x0

    .line 1419440
    invoke-virtual/range {p0 .. p0}, LX/8vj;->getWidth()I

    move-result v2

    add-int/lit8 v2, v2, 0x0

    .line 1419441
    sub-int v11, v4, v2

    .line 1419442
    invoke-virtual/range {p0 .. p0}, LX/8vj;->getWidth()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, LX/8vj;->getPaddingRight()I

    move-result v6

    sub-int/2addr v2, v6

    invoke-virtual/range {p0 .. p0}, LX/8vj;->getPaddingLeft()I

    move-result v6

    sub-int v6, v2, v6

    .line 1419443
    if-gez p1, :cond_2

    .line 1419444
    add-int/lit8 v2, v6, -0x1

    neg-int v2, v2

    move/from16 v0, p1

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v2

    move v8, v2

    .line 1419445
    :goto_1
    if-gez p2, :cond_3

    .line 1419446
    add-int/lit8 v2, v6, -0x1

    neg-int v2, v2

    move/from16 v0, p2

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 1419447
    :goto_2
    move-object/from16 v0, p0

    iget v12, v0, LX/8vi;->V:I

    .line 1419448
    if-nez v12, :cond_4

    .line 1419449
    iget v6, v5, Landroid/graphics/Rect;->left:I

    sub-int v6, v3, v6

    move-object/from16 v0, p0

    iput v6, v0, LX/8vj;->aS:I

    .line 1419450
    :goto_3
    add-int v6, v12, v9

    move-object/from16 v0, p0

    iget v7, v0, LX/8vi;->ao:I

    if-ne v6, v7, :cond_5

    .line 1419451
    iget v6, v5, Landroid/graphics/Rect;->right:I

    add-int/2addr v6, v4

    move-object/from16 v0, p0

    iput v6, v0, LX/8vj;->aT:I

    .line 1419452
    :goto_4
    if-nez v12, :cond_6

    iget v6, v5, Landroid/graphics/Rect;->left:I

    if-lt v3, v6, :cond_6

    if-ltz v2, :cond_6

    const/4 v3, 0x1

    .line 1419453
    :goto_5
    add-int v6, v12, v9

    move-object/from16 v0, p0

    iget v7, v0, LX/8vi;->ao:I

    if-ne v6, v7, :cond_7

    invoke-virtual/range {p0 .. p0}, LX/8vj;->getWidth()I

    move-result v6

    iget v5, v5, Landroid/graphics/Rect;->right:I

    sub-int v5, v6, v5

    if-gt v4, v5, :cond_7

    if-gtz v2, :cond_7

    const/4 v4, 0x1

    .line 1419454
    :goto_6
    if-nez v3, :cond_1

    if-eqz v4, :cond_9

    .line 1419455
    :cond_1
    if-eqz v2, :cond_8

    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1419456
    :cond_2
    add-int/lit8 v2, v6, -0x1

    move/from16 v0, p1

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v2

    move v8, v2

    goto :goto_1

    .line 1419457
    :cond_3
    add-int/lit8 v2, v6, -0x1

    move/from16 v0, p2

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v2

    goto :goto_2

    .line 1419458
    :cond_4
    move-object/from16 v0, p0

    iget v6, v0, LX/8vj;->aS:I

    add-int/2addr v6, v2

    move-object/from16 v0, p0

    iput v6, v0, LX/8vj;->aS:I

    goto :goto_3

    .line 1419459
    :cond_5
    move-object/from16 v0, p0

    iget v6, v0, LX/8vj;->aT:I

    add-int/2addr v6, v2

    move-object/from16 v0, p0

    iput v6, v0, LX/8vj;->aT:I

    goto :goto_4

    .line 1419460
    :cond_6
    const/4 v3, 0x0

    goto :goto_5

    .line 1419461
    :cond_7
    const/4 v4, 0x0

    goto :goto_6

    .line 1419462
    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1419463
    :cond_9
    if-gez v2, :cond_c

    const/4 v3, 0x1

    .line 1419464
    :goto_7
    invoke-virtual/range {p0 .. p0}, LX/8vj;->isInTouchMode()Z

    move-result v13

    .line 1419465
    if-eqz v13, :cond_a

    .line 1419466
    invoke-virtual/range {p0 .. p0}, LX/8vj;->h()V

    .line 1419467
    :cond_a
    invoke-virtual/range {p0 .. p0}, LX/8vj;->getHeaderViewsCount()I

    move-result v14

    .line 1419468
    move-object/from16 v0, p0

    iget v4, v0, LX/8vi;->ao:I

    invoke-virtual/range {p0 .. p0}, LX/8vj;->getFooterViewsCount()I

    move-result v5

    sub-int v15, v4, v5

    .line 1419469
    const/4 v5, 0x0

    .line 1419470
    const/4 v6, 0x0

    .line 1419471
    if-eqz v3, :cond_d

    .line 1419472
    neg-int v0, v2

    move/from16 v16, v0

    .line 1419473
    const/4 v4, 0x0

    move/from16 v19, v4

    move v4, v6

    move/from16 v6, v19

    :goto_8
    if-ge v6, v9, :cond_f

    .line 1419474
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v17

    .line 1419475
    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getRight()I

    move-result v7

    move/from16 v0, v16

    if-ge v7, v0, :cond_f

    .line 1419476
    add-int/lit8 v7, v4, 0x1

    .line 1419477
    add-int v4, v12, v6

    .line 1419478
    if-lt v4, v14, :cond_b

    if-ge v4, v15, :cond_b

    .line 1419479
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8vj;->p:LX/8vf;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v4}, LX/8vf;->a(Landroid/view/View;I)V

    .line 1419480
    :cond_b
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    move v4, v7

    goto :goto_8

    .line 1419481
    :cond_c
    const/4 v3, 0x0

    goto :goto_7

    .line 1419482
    :cond_d
    invoke-virtual/range {p0 .. p0}, LX/8vj;->getWidth()I

    move-result v4

    sub-int v7, v4, v2

    .line 1419483
    add-int/lit8 v4, v9, -0x1

    move/from16 v19, v4

    move v4, v6

    move/from16 v6, v19

    :goto_9
    if-ltz v6, :cond_f

    .line 1419484
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    .line 1419485
    invoke-virtual {v9}, Landroid/view/View;->getLeft()I

    move-result v16

    move/from16 v0, v16

    if-le v0, v7, :cond_f

    .line 1419486
    add-int/lit8 v5, v4, 0x1

    .line 1419487
    add-int v4, v12, v6

    .line 1419488
    if-lt v4, v14, :cond_e

    if-ge v4, v15, :cond_e

    .line 1419489
    move-object/from16 v0, p0

    iget-object v0, v0, LX/8vj;->p:LX/8vf;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v9, v4}, LX/8vf;->a(Landroid/view/View;I)V

    .line 1419490
    :cond_e
    add-int/lit8 v4, v6, -0x1

    move/from16 v19, v4

    move v4, v5

    move v5, v6

    move/from16 v6, v19

    goto :goto_9

    .line 1419491
    :cond_f
    move-object/from16 v0, p0

    iget v6, v0, LX/8vj;->B:I

    add-int/2addr v6, v8

    move-object/from16 v0, p0

    iput v6, v0, LX/8vj;->C:I

    .line 1419492
    const/4 v6, 0x1

    move-object/from16 v0, p0

    iput-boolean v6, v0, LX/8vj;->at:Z

    .line 1419493
    if-lez v4, :cond_10

    .line 1419494
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v4}, LX/8vj;->detachViewsFromParent(II)V

    .line 1419495
    move-object/from16 v0, p0

    iget-object v5, v0, LX/8vj;->p:LX/8vf;

    invoke-virtual {v5}, LX/8vf;->d()V

    .line 1419496
    :cond_10
    invoke-virtual/range {p0 .. p0}, LX/8vj;->awakenScrollBars()Z

    move-result v5

    if-nez v5, :cond_11

    .line 1419497
    invoke-virtual/range {p0 .. p0}, LX/8vj;->invalidate()V

    .line 1419498
    :cond_11
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/8vj;->b(I)V

    .line 1419499
    if-eqz v3, :cond_12

    .line 1419500
    move-object/from16 v0, p0

    iget v5, v0, LX/8vi;->V:I

    add-int/2addr v4, v5

    move-object/from16 v0, p0

    iput v4, v0, LX/8vj;->V:I

    .line 1419501
    :cond_12
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 1419502
    if-lt v10, v2, :cond_13

    if-ge v11, v2, :cond_14

    .line 1419503
    :cond_13
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LX/8vj;->a(Z)V

    .line 1419504
    :cond_14
    if-nez v13, :cond_16

    move-object/from16 v0, p0

    iget v2, v0, LX/8vi;->am:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_16

    .line 1419505
    move-object/from16 v0, p0

    iget v2, v0, LX/8vi;->am:I

    move-object/from16 v0, p0

    iget v3, v0, LX/8vi;->V:I

    sub-int/2addr v2, v3

    .line 1419506
    if-ltz v2, :cond_15

    invoke-virtual/range {p0 .. p0}, LX/8vj;->getChildCount()I

    move-result v3

    if-ge v2, v3, :cond_15

    .line 1419507
    move-object/from16 v0, p0

    iget v3, v0, LX/8vi;->am:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v2}, LX/8vj;->a(ILandroid/view/View;)V

    .line 1419508
    :cond_15
    :goto_a
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, LX/8vj;->at:Z

    .line 1419509
    invoke-virtual/range {p0 .. p0}, LX/8vj;->b()V

    .line 1419510
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1419511
    :cond_16
    move-object/from16 v0, p0

    iget v2, v0, LX/8vj;->n:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_17

    .line 1419512
    move-object/from16 v0, p0

    iget v2, v0, LX/8vj;->n:I

    move-object/from16 v0, p0

    iget v3, v0, LX/8vi;->V:I

    sub-int/2addr v2, v3

    .line 1419513
    if-ltz v2, :cond_15

    invoke-virtual/range {p0 .. p0}, LX/8vj;->getChildCount()I

    move-result v3

    if-ge v2, v3, :cond_15

    .line 1419514
    const/4 v3, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v2}, LX/8vj;->a(ILandroid/view/View;)V

    goto :goto_a

    .line 1419515
    :cond_17
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8vj;->o:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->setEmpty()V

    goto :goto_a
.end method

.method public final a(Landroid/view/View;IJ)Z
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/4 v7, 0x0

    .line 1419518
    iget v1, p0, LX/8vj;->b:I

    if-eqz v1, :cond_12

    .line 1419519
    iget v1, p0, LX/8vj;->b:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_a

    iget v1, p0, LX/8vj;->b:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_a

    iget-object v1, p0, LX/8vj;->c:Ljava/lang/Object;

    if-eqz v1, :cond_a

    .line 1419520
    :cond_0
    iget-object v1, p0, LX/8vj;->f:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, p2, v7}, Landroid/util/SparseBooleanArray;->get(IZ)Z

    move-result v1

    if-nez v1, :cond_7

    move v6, v0

    .line 1419521
    :goto_0
    iget-object v1, p0, LX/8vj;->f:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, p2, v6}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 1419522
    iget-object v1, p0, LX/8vj;->g:LX/0tf;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1419523
    if-eqz v6, :cond_8

    .line 1419524
    iget-object v1, p0, LX/8vj;->g:LX/0tf;

    iget-object v2, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    invoke-interface {v2, p2}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, LX/0tf;->b(JLjava/lang/Object;)V

    .line 1419525
    :cond_1
    :goto_1
    if-eqz v6, :cond_9

    .line 1419526
    iget v1, p0, LX/8vj;->e:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/8vj;->e:I

    .line 1419527
    :goto_2
    iget-object v1, p0, LX/8vj;->c:Ljava/lang/Object;

    if-eqz v1, :cond_11

    .line 1419528
    iget-object v1, p0, LX/8vj;->d:Ljava/lang/Object;

    check-cast v1, LX/8vW;

    iget-object v2, p0, LX/8vj;->c:Ljava/lang/Object;

    check-cast v2, Landroid/view/ActionMode;

    move v3, p2

    move-wide v4, p3

    invoke-virtual/range {v1 .. v6}, LX/8vW;->a(Landroid/view/ActionMode;IJZ)V

    :goto_3
    move v1, v7

    move v7, v0

    .line 1419529
    :goto_4
    if-eqz v7, :cond_5

    .line 1419530
    const/4 v4, 0x0

    .line 1419531
    iget v6, p0, LX/8vi;->V:I

    .line 1419532
    invoke-virtual {p0}, LX/8vj;->getChildCount()I

    move-result v7

    .line 1419533
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_3

    const/4 v2, 0x1

    move v3, v2

    :goto_5
    move v5, v4

    .line 1419534
    :goto_6
    if-ge v5, v7, :cond_5

    .line 1419535
    invoke-virtual {p0, v5}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1419536
    add-int v8, v6, v5

    .line 1419537
    instance-of v9, v2, Landroid/widget/Checkable;

    if-eqz v9, :cond_4

    .line 1419538
    check-cast v2, Landroid/widget/Checkable;

    iget-object v9, p0, LX/8vj;->f:Landroid/util/SparseBooleanArray;

    invoke-virtual {v9, v8, v4}, Landroid/util/SparseBooleanArray;->get(IZ)Z

    move-result v8

    invoke-interface {v2, v8}, Landroid/widget/Checkable;->setChecked(Z)V

    .line 1419539
    :cond_2
    :goto_7
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_6

    :cond_3
    move v3, v4

    .line 1419540
    goto :goto_5

    .line 1419541
    :cond_4
    if-eqz v3, :cond_2

    .line 1419542
    iget-object v9, p0, LX/8vj;->f:Landroid/util/SparseBooleanArray;

    invoke-virtual {v9, v8, v4}, Landroid/util/SparseBooleanArray;->get(IZ)Z

    move-result v8

    invoke-virtual {v2, v8}, Landroid/view/View;->setActivated(Z)V

    goto :goto_7

    .line 1419543
    :cond_5
    :goto_8
    if-eqz v1, :cond_6

    .line 1419544
    invoke-super {p0, p1, p2, p3, p4}, LX/8vi;->a(Landroid/view/View;IJ)Z

    move-result v1

    or-int/2addr v0, v1

    .line 1419545
    :cond_6
    return v0

    :cond_7
    move v6, v7

    .line 1419546
    goto/16 :goto_0

    .line 1419547
    :cond_8
    iget-object v1, p0, LX/8vj;->g:LX/0tf;

    iget-object v2, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    invoke-interface {v2, p2}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, LX/0tf;->b(J)V

    goto :goto_1

    .line 1419548
    :cond_9
    iget v1, p0, LX/8vj;->e:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, LX/8vj;->e:I

    goto :goto_2

    .line 1419549
    :cond_a
    iget v1, p0, LX/8vj;->b:I

    if-ne v1, v0, :cond_10

    .line 1419550
    iget-object v1, p0, LX/8vj;->f:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, p2, v7}, Landroid/util/SparseBooleanArray;->get(IZ)Z

    move-result v1

    if-nez v1, :cond_d

    move v1, v0

    .line 1419551
    :goto_9
    if-eqz v1, :cond_e

    .line 1419552
    iget-object v1, p0, LX/8vj;->f:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1}, Landroid/util/SparseBooleanArray;->clear()V

    .line 1419553
    iget-object v1, p0, LX/8vj;->f:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, p2, v0}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 1419554
    iget-object v1, p0, LX/8vj;->g:LX/0tf;

    if-eqz v1, :cond_b

    iget-object v1, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 1419555
    iget-object v1, p0, LX/8vj;->g:LX/0tf;

    invoke-virtual {v1}, LX/0tf;->b()V

    .line 1419556
    iget-object v1, p0, LX/8vj;->g:LX/0tf;

    iget-object v2, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    invoke-interface {v2, p2}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, LX/0tf;->b(JLjava/lang/Object;)V

    .line 1419557
    :cond_b
    iput v0, p0, LX/8vj;->e:I

    :cond_c
    :goto_a
    move v7, v0

    move v1, v0

    .line 1419558
    goto/16 :goto_4

    :cond_d
    move v1, v7

    .line 1419559
    goto :goto_9

    .line 1419560
    :cond_e
    iget-object v1, p0, LX/8vj;->f:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1}, Landroid/util/SparseBooleanArray;->size()I

    move-result v1

    if-eqz v1, :cond_f

    iget-object v1, p0, LX/8vj;->f:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, v7}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v1

    if-nez v1, :cond_c

    .line 1419561
    :cond_f
    iput v7, p0, LX/8vj;->e:I

    goto :goto_a

    :cond_10
    move v1, v0

    goto/16 :goto_4

    :cond_11
    move v7, v0

    goto/16 :goto_3

    :cond_12
    move v1, v0

    move v0, v7

    goto :goto_8
.end method

.method public final addTouchables(Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1419562
    invoke-virtual {p0}, LX/8vj;->getChildCount()I

    move-result v1

    .line 1419563
    iget v2, p0, LX/8vi;->V:I

    .line 1419564
    iget-object v3, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    .line 1419565
    if-nez v3, :cond_1

    .line 1419566
    :cond_0
    return-void

    .line 1419567
    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 1419568
    invoke-virtual {p0, v0}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 1419569
    add-int v5, v2, v0

    invoke-interface {v3, v5}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1419570
    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1419571
    :cond_2
    invoke-virtual {v4, p1}, Landroid/view/View;->addTouchables(Ljava/util/ArrayList;)V

    .line 1419572
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1419315
    iget-object v0, p0, LX/8vj;->aw:LX/8ve;

    if-eqz v0, :cond_0

    .line 1419316
    invoke-virtual {p0}, LX/8vj;->getChildCount()I

    .line 1419317
    :cond_0
    invoke-virtual {p0, v1, v1, v1, v1}, LX/8vj;->onScrollChanged(IIII)V

    .line 1419318
    return-void
.end method

.method public final b(I)V
    .locals 3

    .prologue
    .line 1419573
    invoke-virtual {p0}, LX/8vj;->getChildCount()I

    move-result v1

    .line 1419574
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 1419575
    invoke-virtual {p0, v0}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1419576
    invoke-virtual {v2, p1}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 1419577
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1419578
    :cond_0
    return-void
.end method

.method public final b(Landroid/view/View;IJ)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1419235
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v0, v3, :cond_2

    .line 1419236
    iget v0, p0, LX/8vj;->b:I

    const/4 v3, 0x3

    if-ne v0, v3, :cond_2

    .line 1419237
    iget-object v0, p0, LX/8vj;->c:Ljava/lang/Object;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/8vj;->d:Ljava/lang/Object;

    check-cast v0, LX/8vW;

    invoke-virtual {p0, v0}, LX/8vj;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v0

    iput-object v0, p0, LX/8vj;->c:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 1419238
    invoke-direct {p0, p2, v1}, LX/8vj;->b(IZ)V

    .line 1419239
    invoke-virtual {p0, v2}, LX/8vj;->performHapticFeedback(I)Z

    :cond_0
    move v0, v1

    .line 1419240
    :cond_1
    :goto_0
    return v0

    .line 1419241
    :cond_2
    iget-object v0, p0, LX/8vi;->ai:LX/8vl;

    if-eqz v0, :cond_4

    .line 1419242
    iget-object v0, p0, LX/8vi;->ai:LX/8vl;

    invoke-interface {v0}, LX/8vl;->a()Z

    move-result v0

    .line 1419243
    :goto_1
    if-nez v0, :cond_3

    .line 1419244
    invoke-static {p1, p2, p3, p4}, LX/8vj;->c(Landroid/view/View;IJ)Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v0

    iput-object v0, p0, LX/8vj;->az:Landroid/view/ContextMenu$ContextMenuInfo;

    .line 1419245
    invoke-super {p0, p0}, LX/8vi;->showContextMenuForChild(Landroid/view/View;)Z

    move-result v0

    .line 1419246
    :cond_3
    if-eqz v0, :cond_1

    .line 1419247
    invoke-virtual {p0, v2}, LX/8vj;->performHapticFeedback(I)Z

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method public abstract c(I)I
.end method

.method public c()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 1419179
    invoke-virtual {p0}, LX/8vj;->removeAllViewsInLayout()V

    .line 1419180
    iput v3, p0, LX/8vj;->V:I

    .line 1419181
    iput-boolean v3, p0, LX/8vj;->aj:Z

    .line 1419182
    iput-object v0, p0, LX/8vj;->O:Ljava/lang/Runnable;

    .line 1419183
    iput-boolean v3, p0, LX/8vj;->ad:Z

    .line 1419184
    iput-object v0, p0, LX/8vj;->bc:Lit/sephiroth/android/library/widget/AbsHListView$SavedState;

    .line 1419185
    iput v2, p0, LX/8vj;->ar:I

    .line 1419186
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, LX/8vj;->as:J

    .line 1419187
    invoke-virtual {p0, v2}, LX/8vi;->setSelectedPositionInt(I)V

    .line 1419188
    invoke-virtual {p0, v2}, LX/8vi;->setNextSelectedPositionInt(I)V

    .line 1419189
    iput v3, p0, LX/8vj;->J:I

    .line 1419190
    iput v2, p0, LX/8vj;->n:I

    .line 1419191
    iget-object v0, p0, LX/8vj;->o:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 1419192
    invoke-virtual {p0}, LX/8vj;->invalidate()V

    .line 1419193
    return-void
.end method

.method public final checkInputConnectionProxy(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 1419194
    const/4 v0, 0x0

    return v0
.end method

.method public final checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 1419195
    instance-of v0, p1, LX/8vc;

    return v0
.end method

.method public final computeHorizontalScrollExtent()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1419196
    invoke-virtual {p0}, LX/8vj;->getChildCount()I

    move-result v2

    .line 1419197
    if-lez v2, :cond_3

    .line 1419198
    iget-boolean v0, p0, LX/8vj;->ax:Z

    if-eqz v0, :cond_2

    .line 1419199
    mul-int/lit8 v0, v2, 0x64

    .line 1419200
    invoke-virtual {p0, v1}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1419201
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v3

    .line 1419202
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    .line 1419203
    if-lez v1, :cond_0

    .line 1419204
    mul-int/lit8 v3, v3, 0x64

    div-int v1, v3, v1

    add-int/2addr v0, v1

    .line 1419205
    :cond_0
    add-int/lit8 v1, v2, -0x1

    invoke-virtual {p0, v1}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1419206
    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v2

    .line 1419207
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    .line 1419208
    if-lez v1, :cond_1

    .line 1419209
    invoke-virtual {p0}, LX/8vj;->getWidth()I

    move-result v3

    sub-int/2addr v2, v3

    mul-int/lit8 v2, v2, 0x64

    div-int v1, v2, v1

    sub-int/2addr v0, v1

    .line 1419210
    :cond_1
    :goto_0
    return v0

    .line 1419211
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 1419212
    goto :goto_0
.end method

.method public final computeHorizontalScrollOffset()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1419213
    iget v2, p0, LX/8vi;->V:I

    .line 1419214
    invoke-virtual {p0}, LX/8vj;->getChildCount()I

    move-result v3

    .line 1419215
    if-ltz v2, :cond_0

    if-lez v3, :cond_0

    .line 1419216
    iget-boolean v1, p0, LX/8vj;->ax:Z

    if-eqz v1, :cond_1

    .line 1419217
    invoke-virtual {p0, v0}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1419218
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v3

    .line 1419219
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    .line 1419220
    if-lez v1, :cond_0

    .line 1419221
    mul-int/lit8 v2, v2, 0x64

    mul-int/lit8 v3, v3, 0x64

    div-int v1, v3, v1

    sub-int v1, v2, v1

    invoke-virtual {p0}, LX/8vj;->getScrollX()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, LX/8vj;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    iget v3, p0, LX/8vi;->ao:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const/high16 v3, 0x42c80000    # 100.0f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    add-int/2addr v1, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1419222
    :cond_0
    :goto_0
    return v0

    .line 1419223
    :cond_1
    iget v1, p0, LX/8vi;->ao:I

    .line 1419224
    if-nez v2, :cond_2

    .line 1419225
    :goto_1
    int-to-float v2, v2

    int-to-float v3, v3

    int-to-float v0, v0

    int-to-float v1, v1

    div-float/2addr v0, v1

    mul-float/2addr v0, v3

    add-float/2addr v0, v2

    float-to-int v0, v0

    goto :goto_0

    .line 1419226
    :cond_2
    add-int v0, v2, v3

    if-ne v0, v1, :cond_3

    move v0, v1

    .line 1419227
    goto :goto_1

    .line 1419228
    :cond_3
    div-int/lit8 v0, v3, 0x2

    add-int/2addr v0, v2

    goto :goto_1
.end method

.method public final computeHorizontalScrollRange()I
    .locals 3

    .prologue
    .line 1419229
    iget-boolean v0, p0, LX/8vj;->ax:Z

    if-eqz v0, :cond_1

    .line 1419230
    iget v0, p0, LX/8vi;->ao:I

    mul-int/lit8 v0, v0, 0x64

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1419231
    invoke-virtual {p0}, LX/8vj;->getScrollX()I

    move-result v1

    if-eqz v1, :cond_0

    .line 1419232
    invoke-virtual {p0}, LX/8vj;->getScrollX()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, LX/8vj;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    iget v2, p0, LX/8vi;->ao:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 1419233
    :cond_0
    :goto_0
    return v0

    .line 1419234
    :cond_1
    iget v0, p0, LX/8vi;->ao:I

    goto :goto_0
.end method

.method public d()V
    .locals 0

    .prologue
    .line 1419175
    return-void
.end method

.method public dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1419248
    iget-boolean v0, p0, LX/8vj;->l:Z

    .line 1419249
    if-nez v0, :cond_0

    .line 1419250
    invoke-direct {p0, p1}, LX/8vj;->a(Landroid/graphics/Canvas;)V

    .line 1419251
    :cond_0
    invoke-super {p0, p1}, LX/8vi;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 1419252
    if-eqz v0, :cond_1

    .line 1419253
    invoke-direct {p0, p1}, LX/8vj;->a(Landroid/graphics/Canvas;)V

    .line 1419254
    :cond_1
    return-void
.end method

.method public final dispatchSetPressed(Z)V
    .locals 0

    .prologue
    .line 1419255
    return-void
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    .line 1419256
    invoke-super {p0, p1}, LX/8vi;->draw(Landroid/graphics/Canvas;)V

    .line 1419257
    iget-object v0, p0, LX/8vj;->aQ:LX/8vn;

    if-eqz v0, :cond_3

    .line 1419258
    invoke-virtual {p0}, LX/8vj;->getScrollX()I

    move-result v0

    .line 1419259
    iget-object v1, p0, LX/8vj;->aQ:LX/8vn;

    invoke-virtual {v1}, LX/8vn;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1419260
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 1419261
    iget-object v2, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget v3, p0, LX/8vj;->aW:I

    add-int/2addr v2, v3

    .line 1419262
    iget-object v3, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    iget v4, p0, LX/8vj;->aX:I

    add-int/2addr v3, v4

    .line 1419263
    invoke-virtual {p0}, LX/8vj;->getHeight()I

    move-result v4

    sub-int/2addr v4, v2

    sub-int v3, v4, v3

    .line 1419264
    const/4 v4, 0x0

    iget v5, p0, LX/8vj;->aS:I

    add-int/2addr v5, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 1419265
    const/high16 v5, -0x3d4c0000    # -90.0f

    invoke-virtual {p1, v5}, Landroid/graphics/Canvas;->rotate(F)V

    .line 1419266
    invoke-virtual {p0}, LX/8vj;->getHeight()I

    move-result v5

    neg-int v5, v5

    add-int/2addr v5, v2

    int-to-float v5, v5

    int-to-float v6, v4

    invoke-virtual {p1, v5, v6}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1419267
    iget-object v5, p0, LX/8vj;->aQ:LX/8vn;

    invoke-virtual {v5, v3, v3}, LX/8vn;->a(II)V

    .line 1419268
    iget-object v3, p0, LX/8vj;->aQ:LX/8vn;

    invoke-virtual {v3, p1}, LX/8vn;->a(Landroid/graphics/Canvas;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1419269
    iget-object v3, p0, LX/8vj;->aQ:LX/8vn;

    .line 1419270
    iput v4, v3, LX/8vn;->f:I

    .line 1419271
    iput v2, v3, LX/8vn;->g:I

    .line 1419272
    invoke-virtual {p0}, LX/8vj;->invalidate()V

    .line 1419273
    :cond_0
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 1419274
    :cond_1
    iget-object v1, p0, LX/8vj;->aR:LX/8vn;

    invoke-virtual {v1}, LX/8vn;->a()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1419275
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 1419276
    iget-object v2, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget v3, p0, LX/8vj;->aW:I

    add-int/2addr v2, v3

    .line 1419277
    iget-object v3, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget v4, p0, LX/8vj;->aX:I

    add-int/2addr v3, v4

    .line 1419278
    invoke-virtual {p0}, LX/8vj;->getHeight()I

    move-result v4

    sub-int/2addr v4, v2

    sub-int v3, v4, v3

    .line 1419279
    invoke-virtual {p0}, LX/8vj;->getWidth()I

    move-result v4

    .line 1419280
    iget v5, p0, LX/8vj;->aT:I

    add-int/2addr v0, v5

    invoke-static {v4, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1419281
    const/high16 v4, 0x42b40000    # 90.0f

    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->rotate(F)V

    .line 1419282
    neg-int v2, v2

    int-to-float v2, v2

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1, v2, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1419283
    iget-object v0, p0, LX/8vj;->aR:LX/8vn;

    invoke-virtual {v0, v3, v3}, LX/8vn;->a(II)V

    .line 1419284
    iget-object v0, p0, LX/8vj;->aR:LX/8vn;

    invoke-virtual {v0, p1}, LX/8vn;->a(Landroid/graphics/Canvas;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1419285
    invoke-virtual {p0}, LX/8vj;->invalidate()V

    .line 1419286
    :cond_2
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 1419287
    :cond_3
    return-void
.end method

.method public final drawableStateChanged()V
    .locals 0

    .prologue
    .line 1419288
    invoke-super {p0}, LX/8vi;->drawableStateChanged()V

    .line 1419289
    invoke-direct {p0}, LX/8vj;->v()V

    .line 1419290
    return-void
.end method

.method public final e()V
    .locals 6

    .prologue
    const/4 v3, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1419291
    iget-object v0, p0, LX/8vj;->w:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 1419292
    iget v0, p0, LX/8vi;->V:I

    if-lez v0, :cond_3

    move v0, v1

    .line 1419293
    :goto_0
    if-nez v0, :cond_0

    .line 1419294
    invoke-virtual {p0}, LX/8vj;->getChildCount()I

    move-result v4

    if-lez v4, :cond_0

    .line 1419295
    invoke-virtual {p0, v2}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1419296
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    iget-object v4, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    if-ge v0, v4, :cond_4

    move v0, v1

    .line 1419297
    :cond_0
    :goto_1
    iget-object v4, p0, LX/8vj;->w:Landroid/view/View;

    if-eqz v0, :cond_5

    move v0, v2

    :goto_2
    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1419298
    :cond_1
    iget-object v0, p0, LX/8vj;->x:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 1419299
    invoke-virtual {p0}, LX/8vj;->getChildCount()I

    move-result v4

    .line 1419300
    iget v0, p0, LX/8vi;->V:I

    add-int/2addr v0, v4

    iget v5, p0, LX/8vi;->ao:I

    if-ge v0, v5, :cond_6

    move v0, v1

    .line 1419301
    :goto_3
    if-nez v0, :cond_9

    if-lez v4, :cond_9

    .line 1419302
    add-int/lit8 v0, v4, -0x1

    invoke-virtual {p0, v0}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1419303
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    invoke-virtual {p0}, LX/8vj;->getRight()I

    move-result v4

    iget-object v5, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    sub-int/2addr v4, v5

    if-le v0, v4, :cond_7

    .line 1419304
    :goto_4
    iget-object v0, p0, LX/8vj;->x:Landroid/view/View;

    if-eqz v1, :cond_8

    :goto_5
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1419305
    :cond_2
    return-void

    :cond_3
    move v0, v2

    .line 1419306
    goto :goto_0

    :cond_4
    move v0, v2

    .line 1419307
    goto :goto_1

    :cond_5
    move v0, v3

    .line 1419308
    goto :goto_2

    :cond_6
    move v0, v2

    .line 1419309
    goto :goto_3

    :cond_7
    move v1, v2

    .line 1419310
    goto :goto_4

    :cond_8
    move v2, v3

    .line 1419311
    goto :goto_5

    :cond_9
    move v1, v0

    goto :goto_4
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 1419312
    invoke-virtual {p0}, LX/8vj;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/8vj;->isInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-static {p0}, LX/8vj;->u(LX/8vj;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 4

    .prologue
    .line 1419313
    new-instance v0, LX/8vc;

    const/4 v1, -0x2

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, LX/8vc;-><init>(III)V

    return-object v0
.end method

.method public final generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    .line 1419314
    new-instance v0, LX/8vc;

    invoke-virtual {p0}, LX/8vj;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, LX/8vc;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public final generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 1418940
    new-instance v0, LX/8vc;

    invoke-direct {v0, p1}, LX/8vc;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public getCacheColorHint()I
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "drawing"
    .end annotation

    .prologue
    .line 1418074
    iget v0, p0, LX/8vj;->aH:I

    return v0
.end method

.method public getCheckedItemIds()[J
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1418096
    iget v0, p0, LX/8vj;->b:I

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8vj;->g:LX/0tf;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    if-nez v0, :cond_2

    .line 1418097
    :cond_0
    new-array v0, v1, [J

    .line 1418098
    :cond_1
    return-object v0

    .line 1418099
    :cond_2
    iget-object v2, p0, LX/8vj;->g:LX/0tf;

    .line 1418100
    invoke-virtual {v2}, LX/0tf;->a()I

    move-result v3

    .line 1418101
    new-array v0, v3, [J

    .line 1418102
    :goto_0
    if-ge v1, v3, :cond_1

    .line 1418103
    invoke-virtual {v2, v1}, LX/0tf;->b(I)J

    move-result-wide v4

    aput-wide v4, v0, v1

    .line 1418104
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getCheckedItemPosition()I
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1418105
    iget v0, p0, LX/8vj;->b:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/8vj;->f:Landroid/util/SparseBooleanArray;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8vj;->f:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 1418106
    iget-object v0, p0, LX/8vj;->f:Landroid/util/SparseBooleanArray;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v0

    .line 1418107
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getCheckedItemPositions()Landroid/util/SparseBooleanArray;
    .locals 1

    .prologue
    .line 1418108
    iget v0, p0, LX/8vj;->b:I

    if-eqz v0, :cond_0

    .line 1418109
    iget-object v0, p0, LX/8vj;->f:Landroid/util/SparseBooleanArray;

    .line 1418110
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getChoiceMode()I
    .locals 1

    .prologue
    .line 1418111
    iget v0, p0, LX/8vj;->b:I

    return v0
.end method

.method public getContextMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;
    .locals 1

    .prologue
    .line 1418112
    iget-object v0, p0, LX/8vj;->az:Landroid/view/ContextMenu$ContextMenuInfo;

    return-object v0
.end method

.method public final getFocusedRect(Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    .line 1418113
    invoke-virtual {p0}, LX/8vi;->getSelectedView()Landroid/view/View;

    move-result-object v0

    .line 1418114
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-ne v1, p0, :cond_0

    .line 1418115
    invoke-virtual {v0, p1}, Landroid/view/View;->getFocusedRect(Landroid/graphics/Rect;)V

    .line 1418116
    invoke-virtual {p0, v0, p1}, LX/8vj;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 1418117
    :goto_0
    return-void

    .line 1418118
    :cond_0
    invoke-super {p0, p1}, LX/8vi;->getFocusedRect(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public getFooterViewsCount()I
    .locals 1

    .prologue
    .line 1418119
    const/4 v0, 0x0

    return v0
.end method

.method public getHeaderViewsCount()I
    .locals 1

    .prologue
    .line 1418086
    const/4 v0, 0x0

    return v0
.end method

.method public getHorizontalScrollFactor()F
    .locals 4

    .prologue
    .line 1418130
    iget v0, p0, LX/8vj;->bd:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 1418131
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 1418132
    invoke-virtual {p0}, LX/8vj;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x7f010764

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v1

    .line 1418133
    if-eqz v1, :cond_1

    .line 1418134
    invoke-virtual {p0}, LX/8vj;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/TypedValue;->getDimension(Landroid/util/DisplayMetrics;)F

    move-result v0

    iput v0, p0, LX/8vj;->bd:F

    .line 1418135
    :cond_0
    iget v0, p0, LX/8vj;->bd:F

    return v0

    .line 1418136
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Expected theme to define hlv_listPreferredItemWidth."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getHorizontalScrollbarHeight()I
    .locals 1

    .prologue
    .line 1418137
    invoke-super {p0}, LX/8vi;->getHorizontalScrollbarHeight()I

    move-result v0

    return v0
.end method

.method public getLeftFadingEdgeStrength()F
    .locals 4

    .prologue
    .line 1418138
    invoke-virtual {p0}, LX/8vj;->getChildCount()I

    move-result v1

    .line 1418139
    invoke-super {p0}, LX/8vi;->getLeftFadingEdgeStrength()F

    move-result v0

    .line 1418140
    if-nez v1, :cond_1

    .line 1418141
    :cond_0
    :goto_0
    return v0

    .line 1418142
    :cond_1
    iget v1, p0, LX/8vi;->V:I

    if-lez v1, :cond_2

    .line 1418143
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0

    .line 1418144
    :cond_2
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    .line 1418145
    invoke-virtual {p0}, LX/8vj;->getHorizontalFadingEdgeLength()I

    move-result v2

    int-to-float v2, v2

    .line 1418146
    invoke-virtual {p0}, LX/8vj;->getPaddingLeft()I

    move-result v3

    if-ge v1, v3, :cond_0

    invoke-virtual {p0}, LX/8vj;->getPaddingLeft()I

    move-result v0

    sub-int v0, v1, v0

    neg-int v0, v0

    int-to-float v0, v0

    div-float/2addr v0, v2

    goto :goto_0
.end method

.method public getListPaddingBottom()I
    .locals 1

    .prologue
    .line 1418147
    iget-object v0, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    return v0
.end method

.method public getListPaddingLeft()I
    .locals 1

    .prologue
    .line 1418148
    iget-object v0, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    return v0
.end method

.method public getListPaddingRight()I
    .locals 1

    .prologue
    .line 1418149
    iget-object v0, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    return v0
.end method

.method public getListPaddingTop()I
    .locals 1

    .prologue
    .line 1418150
    iget-object v0, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    return v0
.end method

.method public getRightFadingEdgeStrength()F
    .locals 5

    .prologue
    .line 1418120
    invoke-virtual {p0}, LX/8vj;->getChildCount()I

    move-result v1

    .line 1418121
    invoke-super {p0}, LX/8vi;->getRightFadingEdgeStrength()F

    move-result v0

    .line 1418122
    if-nez v1, :cond_1

    .line 1418123
    :cond_0
    :goto_0
    return v0

    .line 1418124
    :cond_1
    iget v2, p0, LX/8vi;->V:I

    add-int/2addr v2, v1

    add-int/lit8 v2, v2, -0x1

    iget v3, p0, LX/8vi;->ao:I

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_2

    .line 1418125
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0

    .line 1418126
    :cond_2
    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v1

    .line 1418127
    invoke-virtual {p0}, LX/8vj;->getWidth()I

    move-result v2

    .line 1418128
    invoke-virtual {p0}, LX/8vj;->getHorizontalFadingEdgeLength()I

    move-result v3

    int-to-float v3, v3

    .line 1418129
    invoke-virtual {p0}, LX/8vj;->getPaddingRight()I

    move-result v4

    sub-int v4, v2, v4

    if-le v1, v4, :cond_0

    sub-int v0, v1, v2

    invoke-virtual {p0}, LX/8vj;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    int-to-float v0, v0

    div-float/2addr v0, v3

    goto :goto_0
.end method

.method public getSelectedView()Landroid/view/View;
    .locals 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation

    .prologue
    .line 1417928
    iget v0, p0, LX/8vi;->ao:I

    if-lez v0, :cond_0

    iget v0, p0, LX/8vi;->am:I

    if-ltz v0, :cond_0

    .line 1417929
    iget v0, p0, LX/8vi;->am:I

    iget v1, p0, LX/8vi;->V:I

    sub-int/2addr v0, v1

    invoke-virtual {p0, v0}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1417930
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSelector()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 1417931
    iget-object v0, p0, LX/8vj;->m:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getSolidColor()I
    .locals 1

    .prologue
    .line 1417932
    iget v0, p0, LX/8vj;->aH:I

    return v0
.end method

.method public getTranscriptMode()I
    .locals 1

    .prologue
    .line 1417933
    iget v0, p0, LX/8vj;->aG:I

    return v0
.end method

.method public final h()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 1418087
    iget v0, p0, LX/8vi;->am:I

    if-eq v0, v2, :cond_2

    .line 1418088
    iget v0, p0, LX/8vj;->h:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 1418089
    iget v0, p0, LX/8vi;->am:I

    iput v0, p0, LX/8vj;->M:I

    .line 1418090
    :cond_0
    iget v0, p0, LX/8vi;->ak:I

    if-ltz v0, :cond_1

    iget v0, p0, LX/8vi;->ak:I

    iget v1, p0, LX/8vi;->am:I

    if-eq v0, v1, :cond_1

    .line 1418091
    iget v0, p0, LX/8vi;->ak:I

    iput v0, p0, LX/8vj;->M:I

    .line 1418092
    :cond_1
    invoke-virtual {p0, v2}, LX/8vi;->setSelectedPositionInt(I)V

    .line 1418093
    invoke-virtual {p0, v2}, LX/8vi;->setNextSelectedPositionInt(I)V

    .line 1418094
    const/4 v0, 0x0

    iput v0, p0, LX/8vj;->J:I

    .line 1418095
    :cond_2
    return-void
.end method

.method public final i()I
    .locals 2

    .prologue
    .line 1417934
    iget v0, p0, LX/8vi;->am:I

    .line 1417935
    if-gez v0, :cond_0

    .line 1417936
    iget v0, p0, LX/8vj;->M:I

    .line 1417937
    :cond_0
    const/4 v1, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1417938
    iget v1, p0, LX/8vi;->ao:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1417939
    return v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 1417940
    iget v0, p0, LX/8vi;->am:I

    if-gez v0, :cond_0

    invoke-direct {p0}, LX/8vj;->C()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1417941
    invoke-direct {p0}, LX/8vj;->v()V

    .line 1417942
    const/4 v0, 0x1

    .line 1417943
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final jumpDrawablesToCurrentState()V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 1417944
    invoke-super {p0}, LX/8vi;->jumpDrawablesToCurrentState()V

    .line 1417945
    iget-object v0, p0, LX/8vj;->m:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8vj;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    .line 1417946
    :cond_0
    return-void
.end method

.method public final k()V
    .locals 11

    .prologue
    const/4 v10, 0x5

    const/4 v9, -0x1

    const/4 v3, 0x3

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 1417947
    iget v5, p0, LX/8vi;->ao:I

    .line 1417948
    iget v6, p0, LX/8vj;->bb:I

    .line 1417949
    iget v0, p0, LX/8vi;->ao:I

    iput v0, p0, LX/8vj;->bb:I

    .line 1417950
    iget v0, p0, LX/8vj;->b:I

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1417951
    invoke-direct {p0}, LX/8vj;->D()V

    .line 1417952
    :cond_0
    iget-object v0, p0, LX/8vj;->p:LX/8vf;

    .line 1417953
    iget-object v1, v0, LX/8vf;->i:LX/0YU;

    if-eqz v1, :cond_1

    .line 1417954
    iget-object v1, v0, LX/8vf;->i:LX/0YU;

    invoke-virtual {v1}, LX/0YU;->b()V

    .line 1417955
    :cond_1
    if-lez v5, :cond_f

    .line 1417956
    iget-boolean v0, p0, LX/8vi;->ad:Z

    if-eqz v0, :cond_8

    .line 1417957
    iput-boolean v2, p0, LX/8vj;->ad:Z

    .line 1417958
    const/4 v0, 0x0

    iput-object v0, p0, LX/8vj;->bc:Lit/sephiroth/android/library/widget/AbsHListView$SavedState;

    .line 1417959
    iget v0, p0, LX/8vj;->aG:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 1417960
    iput v3, p0, LX/8vj;->h:I

    .line 1417961
    :cond_2
    :goto_0
    return-void

    .line 1417962
    :cond_3
    iget v0, p0, LX/8vj;->aG:I

    if-ne v0, v4, :cond_7

    .line 1417963
    iget-boolean v0, p0, LX/8vj;->aV:Z

    if-eqz v0, :cond_4

    .line 1417964
    iput-boolean v2, p0, LX/8vj;->aV:Z

    .line 1417965
    iput v3, p0, LX/8vj;->h:I

    goto :goto_0

    .line 1417966
    :cond_4
    invoke-virtual {p0}, LX/8vj;->getChildCount()I

    move-result v7

    .line 1417967
    invoke-virtual {p0}, LX/8vj;->getWidth()I

    move-result v0

    invoke-virtual {p0}, LX/8vj;->getPaddingRight()I

    move-result v1

    sub-int v1, v0, v1

    .line 1417968
    add-int/lit8 v0, v7, -0x1

    invoke-virtual {p0, v0}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1417969
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    .line 1417970
    :goto_1
    iget v8, p0, LX/8vi;->V:I

    add-int/2addr v7, v8

    if-lt v7, v6, :cond_6

    if-gt v0, v1, :cond_6

    .line 1417971
    iput v3, p0, LX/8vj;->h:I

    goto :goto_0

    :cond_5
    move v0, v1

    .line 1417972
    goto :goto_1

    .line 1417973
    :cond_6
    invoke-virtual {p0}, LX/8vj;->awakenScrollBars()Z

    .line 1417974
    :cond_7
    iget v0, p0, LX/8vi;->ae:I

    packed-switch v0, :pswitch_data_0

    .line 1417975
    :cond_8
    invoke-virtual {p0}, LX/8vj;->isInTouchMode()Z

    move-result v0

    if-nez v0, :cond_e

    .line 1417976
    iget v0, p0, LX/8vi;->ak:I

    move v0, v0

    .line 1417977
    if-lt v0, v5, :cond_9

    .line 1417978
    add-int/lit8 v0, v5, -0x1

    .line 1417979
    :cond_9
    if-gez v0, :cond_a

    move v0, v2

    .line 1417980
    :cond_a
    invoke-virtual {p0, v0, v4}, LX/8vi;->a(IZ)I

    move-result v1

    .line 1417981
    if-ltz v1, :cond_d

    .line 1417982
    invoke-virtual {p0, v1}, LX/8vi;->setNextSelectedPositionInt(I)V

    goto :goto_0

    .line 1417983
    :pswitch_0
    invoke-virtual {p0}, LX/8vj;->isInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1417984
    iput v10, p0, LX/8vj;->h:I

    .line 1417985
    iget v0, p0, LX/8vi;->aa:I

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/lit8 v1, v5, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, LX/8vj;->aa:I

    goto :goto_0

    .line 1417986
    :cond_b
    invoke-virtual {p0}, LX/8vi;->n()I

    move-result v0

    .line 1417987
    if-ltz v0, :cond_8

    .line 1417988
    invoke-virtual {p0, v0, v4}, LX/8vi;->a(IZ)I

    move-result v1

    .line 1417989
    if-ne v1, v0, :cond_8

    .line 1417990
    iput v0, p0, LX/8vj;->aa:I

    .line 1417991
    iget-wide v2, p0, LX/8vi;->ac:J

    invoke-virtual {p0}, LX/8vj;->getWidth()I

    move-result v1

    int-to-long v4, v1

    cmp-long v1, v2, v4

    if-nez v1, :cond_c

    .line 1417992
    iput v10, p0, LX/8vj;->h:I

    .line 1417993
    :goto_2
    invoke-virtual {p0, v0}, LX/8vi;->setNextSelectedPositionInt(I)V

    goto/16 :goto_0

    .line 1417994
    :cond_c
    const/4 v1, 0x2

    iput v1, p0, LX/8vj;->h:I

    goto :goto_2

    .line 1417995
    :pswitch_1
    iput v10, p0, LX/8vj;->h:I

    .line 1417996
    iget v0, p0, LX/8vi;->aa:I

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/lit8 v1, v5, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, LX/8vj;->aa:I

    goto/16 :goto_0

    .line 1417997
    :cond_d
    invoke-virtual {p0, v0, v2}, LX/8vi;->a(IZ)I

    move-result v0

    .line 1417998
    if-ltz v0, :cond_f

    .line 1417999
    invoke-virtual {p0, v0}, LX/8vi;->setNextSelectedPositionInt(I)V

    goto/16 :goto_0

    .line 1418000
    :cond_e
    iget v0, p0, LX/8vj;->M:I

    if-gez v0, :cond_2

    .line 1418001
    :cond_f
    iget-boolean v0, p0, LX/8vj;->K:Z

    if-eqz v0, :cond_10

    move v0, v3

    :goto_3
    iput v0, p0, LX/8vj;->h:I

    .line 1418002
    iput v9, p0, LX/8vj;->am:I

    .line 1418003
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, LX/8vj;->an:J

    .line 1418004
    iput v9, p0, LX/8vj;->ak:I

    .line 1418005
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, LX/8vj;->al:J

    .line 1418006
    iput-boolean v2, p0, LX/8vj;->ad:Z

    .line 1418007
    const/4 v0, 0x0

    iput-object v0, p0, LX/8vj;->bc:Lit/sephiroth/android/library/widget/AbsHListView$SavedState;

    .line 1418008
    iput v9, p0, LX/8vj;->n:I

    .line 1418009
    invoke-virtual {p0}, LX/8vi;->m()V

    goto/16 :goto_0

    :cond_10
    move v0, v4

    .line 1418010
    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onAttachedToWindow()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/16 v0, 0x2c

    const v1, 0x3c828154

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1418011
    invoke-super {p0}, LX/8vi;->onAttachedToWindow()V

    .line 1418012
    invoke-virtual {p0}, LX/8vj;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    .line 1418013
    invoke-virtual {v1, p0}, Landroid/view/ViewTreeObserver;->addOnTouchModeChangeListener(Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;)V

    .line 1418014
    iget-object v1, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/8vj;->i:LX/8va;

    if-nez v1, :cond_0

    .line 1418015
    new-instance v1, LX/8va;

    invoke-direct {v1, p0}, LX/8va;-><init>(LX/8vj;)V

    iput-object v1, p0, LX/8vj;->i:LX/8va;

    .line 1418016
    iget-object v1, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    iget-object v2, p0, LX/8vj;->i:LX/8va;

    invoke-interface {v1, v2}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 1418017
    iput-boolean v3, p0, LX/8vj;->aj:Z

    .line 1418018
    iget v1, p0, LX/8vi;->ao:I

    iput v1, p0, LX/8vj;->ap:I

    .line 1418019
    iget-object v1, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    iput v1, p0, LX/8vj;->ao:I

    .line 1418020
    :cond_0
    iput-boolean v3, p0, LX/8vj;->S:Z

    .line 1418021
    const/16 v1, 0x2d

    const v2, -0x4608ebb0

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateDrawableState(I)[I
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "Override"
        }
    .end annotation

    .prologue
    .line 1418022
    iget-boolean v0, p0, LX/8vj;->aI:Z

    if-eqz v0, :cond_1

    .line 1418023
    invoke-super {p0, p1}, LX/8vi;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 1418024
    :cond_0
    :goto_0
    return-object v0

    .line 1418025
    :cond_1
    sget-object v0, Landroid/view/View;->ENABLED_STATE_SET:[I

    const/4 v1, 0x0

    aget v3, v0, v1

    .line 1418026
    add-int/lit8 v0, p1, 0x1

    invoke-super {p0, v0}, LX/8vi;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 1418027
    const/4 v2, -0x1

    .line 1418028
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    :goto_1
    if-ltz v1, :cond_3

    .line 1418029
    aget v4, v0, v1

    if-ne v4, v3, :cond_2

    .line 1418030
    :goto_2
    if-ltz v1, :cond_0

    .line 1418031
    add-int/lit8 v2, v1, 0x1

    array-length v3, v0

    sub-int/2addr v3, v1

    add-int/lit8 v3, v3, -0x1

    invoke-static {v0, v2, v0, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    .line 1418032
    :cond_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2
.end method

.method public final onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 1

    .prologue
    .line 1418033
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onDetachedFromWindow()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x893c46d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1418034
    invoke-super {p0}, LX/8vi;->onDetachedFromWindow()V

    .line 1418035
    iget-object v1, p0, LX/8vj;->p:LX/8vf;

    invoke-virtual {v1}, LX/8vf;->b()V

    .line 1418036
    invoke-virtual {p0}, LX/8vj;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    .line 1418037
    invoke-virtual {v1, p0}, Landroid/view/ViewTreeObserver;->removeOnTouchModeChangeListener(Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;)V

    .line 1418038
    iget-object v1, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/8vj;->i:LX/8va;

    if-eqz v1, :cond_0

    .line 1418039
    iget-object v1, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    iget-object v2, p0, LX/8vj;->i:LX/8va;

    invoke-interface {v1, v2}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 1418040
    iput-object v4, p0, LX/8vj;->i:LX/8va;

    .line 1418041
    :cond_0
    iget-object v1, p0, LX/8vj;->av:Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    if-eqz v1, :cond_1

    .line 1418042
    iget-object v1, p0, LX/8vj;->av:Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    invoke-virtual {p0, v1}, LX/8vj;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1418043
    :cond_1
    iget-object v1, p0, LX/8vj;->I:Lit/sephiroth/android/library/widget/AbsHListView$PositionScroller;

    if-eqz v1, :cond_2

    .line 1418044
    iget-object v1, p0, LX/8vj;->I:Lit/sephiroth/android/library/widget/AbsHListView$PositionScroller;

    invoke-virtual {v1}, Lit/sephiroth/android/library/widget/AbsHListView$PositionScroller;->a()V

    .line 1418045
    :cond_2
    iget-object v1, p0, LX/8vj;->aL:Ljava/lang/Runnable;

    if-eqz v1, :cond_3

    .line 1418046
    iget-object v1, p0, LX/8vj;->aL:Ljava/lang/Runnable;

    invoke-virtual {p0, v1}, LX/8vj;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1418047
    :cond_3
    iget-object v1, p0, LX/8vj;->aE:Lit/sephiroth/android/library/widget/AbsHListView$PerformClick;

    if-eqz v1, :cond_4

    .line 1418048
    iget-object v1, p0, LX/8vj;->aE:Lit/sephiroth/android/library/widget/AbsHListView$PerformClick;

    invoke-virtual {p0, v1}, LX/8vj;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1418049
    :cond_4
    iget-object v1, p0, LX/8vj;->aF:Ljava/lang/Runnable;

    if-eqz v1, :cond_5

    .line 1418050
    iget-object v1, p0, LX/8vj;->aF:Ljava/lang/Runnable;

    invoke-virtual {p0, v1}, LX/8vj;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1418051
    iput-object v4, p0, LX/8vj;->aF:Ljava/lang/Runnable;

    .line 1418052
    :cond_5
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/8vj;->S:Z

    .line 1418053
    const/16 v1, 0x2d

    const v2, 0x1a2db9ae

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x7c7567f9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1418054
    invoke-super {p0, p1, p2, p3}, LX/8vi;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 1418055
    if-eqz p1, :cond_1

    iget v1, p0, LX/8vi;->am:I

    if-gez v1, :cond_1

    invoke-virtual {p0}, LX/8vj;->isInTouchMode()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1418056
    iget-boolean v1, p0, LX/8vj;->S:Z

    if-nez v1, :cond_0

    iget-object v1, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_0

    .line 1418057
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/8vj;->aj:Z

    .line 1418058
    iget v1, p0, LX/8vi;->ao:I

    iput v1, p0, LX/8vj;->ap:I

    .line 1418059
    iget-object v1, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    iput v1, p0, LX/8vj;->ao:I

    .line 1418060
    :cond_0
    invoke-direct {p0}, LX/8vj;->C()Z

    .line 1418061
    :cond_1
    const/16 v1, 0x2d

    const v2, 0xccce882

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xc
    .end annotation

    .prologue
    .line 1418062
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v0

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    .line 1418063
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1418064
    :cond_0
    invoke-super {p0, p1}, LX/8vi;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 1418065
    :pswitch_0
    iget v0, p0, LX/8vj;->F:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1418066
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v0

    .line 1418067
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_0

    .line 1418068
    invoke-virtual {p0}, LX/8vj;->getHorizontalScrollFactor()F

    move-result v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 1418069
    invoke-virtual {p0, v0, v0}, LX/8vj;->a(II)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1418070
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
    .end packed-switch
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 1418071
    invoke-super {p0, p1}, LX/8vi;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1418072
    const-class v0, LX/8vj;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 1418073
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "Override"
        }
    .end annotation

    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 1418075
    invoke-super {p0, p1}, LX/8vi;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 1418076
    const-class v0, LX/8vj;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 1418077
    invoke-virtual {p0}, LX/8vj;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1418078
    iget v0, p0, LX/8vi;->V:I

    move v0, v0

    .line 1418079
    if-lez v0, :cond_0

    .line 1418080
    const/16 v0, 0x2000

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 1418081
    :cond_0
    invoke-virtual {p0}, LX/8vi;->getLastVisiblePosition()I

    move-result v0

    .line 1418082
    iget v1, p0, LX/8vi;->ao:I

    move v1, v1

    .line 1418083
    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_1

    .line 1418084
    const/16 v0, 0x1000

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 1418085
    :cond_1
    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v2, 0x1

    const/4 v4, -0x1

    const/4 v1, 0x0

    .line 1418269
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 1418270
    iget-object v3, p0, LX/8vj;->I:Lit/sephiroth/android/library/widget/AbsHListView$PositionScroller;

    if-eqz v3, :cond_0

    .line 1418271
    iget-object v3, p0, LX/8vj;->I:Lit/sephiroth/android/library/widget/AbsHListView$PositionScroller;

    invoke-virtual {v3}, Lit/sephiroth/android/library/widget/AbsHListView$PositionScroller;->a()V

    .line 1418272
    :cond_0
    iget-boolean v3, p0, LX/8vj;->S:Z

    if-nez v3, :cond_2

    .line 1418273
    :cond_1
    :goto_0
    return v1

    .line 1418274
    :cond_2
    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 1418275
    :pswitch_1
    iget v0, p0, LX/8vj;->F:I

    .line 1418276
    const/4 v3, 0x6

    if-eq v0, v3, :cond_3

    const/4 v3, 0x5

    if-ne v0, v3, :cond_4

    .line 1418277
    :cond_3
    iput v1, p0, LX/8vj;->H:I

    move v1, v2

    .line 1418278
    goto :goto_0

    .line 1418279
    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    .line 1418280
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    .line 1418281
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v5

    iput v5, p0, LX/8vj;->aP:I

    .line 1418282
    invoke-virtual {p0, v3}, LX/8vj;->c(I)I

    move-result v5

    .line 1418283
    if-eq v0, v7, :cond_5

    if-ltz v5, :cond_5

    .line 1418284
    iget v6, p0, LX/8vi;->V:I

    sub-int v6, v5, v6

    invoke-virtual {p0, v6}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 1418285
    invoke-virtual {v6}, Landroid/view/View;->getLeft()I

    move-result v6

    iput v6, p0, LX/8vj;->B:I

    .line 1418286
    iput v3, p0, LX/8vj;->D:I

    .line 1418287
    iput v4, p0, LX/8vj;->E:I

    .line 1418288
    iput v5, p0, LX/8vj;->A:I

    .line 1418289
    iput v1, p0, LX/8vj;->F:I

    .line 1418290
    invoke-static {p0}, LX/8vj;->B(LX/8vj;)V

    .line 1418291
    :cond_5
    const/high16 v3, -0x80000000

    iput v3, p0, LX/8vj;->G:I

    .line 1418292
    iget-object v3, p0, LX/8vj;->au:Landroid/view/VelocityTracker;

    if-nez v3, :cond_7

    .line 1418293
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v3

    iput-object v3, p0, LX/8vj;->au:Landroid/view/VelocityTracker;

    .line 1418294
    :goto_1
    iget-object v3, p0, LX/8vj;->au:Landroid/view/VelocityTracker;

    invoke-virtual {v3, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 1418295
    if-ne v0, v7, :cond_1

    move v1, v2

    .line 1418296
    goto :goto_0

    .line 1418297
    :pswitch_2
    iget v0, p0, LX/8vj;->F:I

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 1418298
    :pswitch_3
    iget v0, p0, LX/8vj;->aP:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    .line 1418299
    if-ne v0, v4, :cond_6

    .line 1418300
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, LX/8vj;->aP:I

    move v0, v1

    .line 1418301
    :cond_6
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    float-to-int v0, v0

    .line 1418302
    invoke-direct {p0}, LX/8vj;->y()V

    .line 1418303
    iget-object v3, p0, LX/8vj;->au:Landroid/view/VelocityTracker;

    invoke-virtual {v3, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 1418304
    invoke-direct {p0, v0}, LX/8vj;->f(I)Z

    move-result v0

    if-eqz v0, :cond_1

    move v1, v2

    .line 1418305
    goto/16 :goto_0

    .line 1418306
    :pswitch_4
    iput v4, p0, LX/8vj;->F:I

    .line 1418307
    iput v4, p0, LX/8vj;->aP:I

    .line 1418308
    invoke-direct {p0}, LX/8vj;->z()V

    .line 1418309
    invoke-virtual {p0, v1}, LX/8vj;->a(I)V

    goto/16 :goto_0

    .line 1418310
    :pswitch_5
    invoke-direct {p0, p1}, LX/8vj;->b(Landroid/view/MotionEvent;)V

    goto/16 :goto_0

    .line 1418311
    :cond_7
    iget-object v3, p0, LX/8vj;->au:Landroid/view/VelocityTracker;

    invoke-virtual {v3}, Landroid/view/VelocityTracker;->clear()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_4
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
    .end packed-switch
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 1418721
    const/4 v0, 0x0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 1418712
    sparse-switch p1, :sswitch_data_0

    .line 1418713
    :cond_0
    invoke-super {p0, p1, p2}, LX/8vi;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    :cond_1
    :goto_0
    return v0

    .line 1418714
    :sswitch_0
    invoke-virtual {p0}, LX/8vj;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1418715
    invoke-virtual {p0}, LX/8vj;->isClickable()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, LX/8vj;->isPressed()Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, LX/8vi;->am:I

    if-ltz v1, :cond_0

    iget-object v1, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_0

    iget v1, p0, LX/8vi;->am:I

    iget-object v2, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 1418716
    iget v1, p0, LX/8vi;->am:I

    iget v2, p0, LX/8vi;->V:I

    sub-int/2addr v1, v2

    invoke-virtual {p0, v1}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1418717
    if-eqz v1, :cond_2

    .line 1418718
    iget v2, p0, LX/8vi;->am:I

    iget-wide v4, p0, LX/8vi;->an:J

    invoke-virtual {p0, v1, v2, v4, v5}, LX/8vi;->a(Landroid/view/View;IJ)Z

    .line 1418719
    invoke-virtual {v1, v3}, Landroid/view/View;->setPressed(Z)V

    .line 1418720
    :cond_2
    invoke-virtual {p0, v3}, LX/8vj;->setPressed(Z)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch
.end method

.method public final onLayout(ZIIII)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1418681
    invoke-super/range {p0 .. p5}, LX/8vi;->onLayout(ZIIII)V

    .line 1418682
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8vj;->af:Z

    .line 1418683
    if-eqz p1, :cond_4

    .line 1418684
    invoke-virtual {p0}, LX/8vj;->getChildCount()I

    move-result v2

    move v0, v1

    .line 1418685
    :goto_0
    if-ge v0, v2, :cond_0

    .line 1418686
    invoke-virtual {p0, v0}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->forceLayout()V

    .line 1418687
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1418688
    :cond_0
    iget-object v0, p0, LX/8vj;->p:LX/8vf;

    const/4 v3, 0x0

    .line 1418689
    iget v2, v0, LX/8vf;->f:I

    const/4 v4, 0x1

    if-ne v2, v4, :cond_1

    .line 1418690
    iget-object v5, v0, LX/8vf;->g:Ljava/util/ArrayList;

    .line 1418691
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result p1

    move v4, v3

    .line 1418692
    :goto_1
    if-ge v4, p1, :cond_3

    .line 1418693
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->forceLayout()V

    .line 1418694
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1

    .line 1418695
    :cond_1
    iget p1, v0, LX/8vf;->f:I

    move v5, v3

    .line 1418696
    :goto_2
    if-ge v5, p1, :cond_3

    .line 1418697
    iget-object v2, v0, LX/8vf;->e:[Ljava/util/ArrayList;

    aget-object p3, v2, v5

    .line 1418698
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result p5

    move v4, v3

    .line 1418699
    :goto_3
    if-ge v4, p5, :cond_2

    .line 1418700
    invoke-virtual {p3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->forceLayout()V

    .line 1418701
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_3

    .line 1418702
    :cond_2
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_2

    .line 1418703
    :cond_3
    iget-object v2, v0, LX/8vf;->i:LX/0YU;

    if-eqz v2, :cond_4

    .line 1418704
    iget-object v2, v0, LX/8vf;->i:LX/0YU;

    invoke-virtual {v2}, LX/0YU;->a()I

    move-result v4

    .line 1418705
    :goto_4
    if-ge v3, v4, :cond_4

    .line 1418706
    iget-object v2, v0, LX/8vf;->i:LX/0YU;

    invoke-virtual {v2, v3}, LX/0YU;->f(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->forceLayout()V

    .line 1418707
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_4

    .line 1418708
    :cond_4
    invoke-virtual {p0}, LX/8vj;->d()V

    .line 1418709
    iput-boolean v1, p0, LX/8vj;->af:Z

    .line 1418710
    sub-int v0, p4, p2

    div-int/lit8 v0, v0, 0x3

    iput v0, p0, LX/8vj;->N:I

    .line 1418711
    return-void
.end method

.method public onMeasure(II)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 1418665
    iget-object v0, p0, LX/8vj;->m:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 1418666
    invoke-virtual {p0}, LX/8vj;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x1080062

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/8vj;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 1418667
    :cond_0
    iget-object v0, p0, LX/8vj;->u:Landroid/graphics/Rect;

    .line 1418668
    iget v1, p0, LX/8vj;->q:I

    invoke-virtual {p0}, LX/8vj;->getPaddingLeft()I

    move-result v3

    add-int/2addr v1, v3

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 1418669
    iget v1, p0, LX/8vj;->r:I

    invoke-virtual {p0}, LX/8vj;->getPaddingTop()I

    move-result v3

    add-int/2addr v1, v3

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 1418670
    iget v1, p0, LX/8vj;->s:I

    invoke-virtual {p0}, LX/8vj;->getPaddingRight()I

    move-result v3

    add-int/2addr v1, v3

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 1418671
    iget v1, p0, LX/8vj;->t:I

    invoke-virtual {p0}, LX/8vj;->getPaddingBottom()I

    move-result v3

    add-int/2addr v1, v3

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 1418672
    iget v0, p0, LX/8vj;->aG:I

    if-ne v0, v2, :cond_1

    .line 1418673
    invoke-virtual {p0}, LX/8vj;->getChildCount()I

    move-result v3

    .line 1418674
    invoke-virtual {p0}, LX/8vj;->getWidth()I

    move-result v0

    invoke-virtual {p0}, LX/8vj;->getPaddingRight()I

    move-result v1

    sub-int v1, v0, v1

    .line 1418675
    add-int/lit8 v0, v3, -0x1

    invoke-virtual {p0, v0}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1418676
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    .line 1418677
    :goto_0
    iget v4, p0, LX/8vi;->V:I

    add-int/2addr v3, v4

    iget v4, p0, LX/8vj;->bb:I

    if-lt v3, v4, :cond_3

    if-gt v0, v1, :cond_3

    move v0, v2

    :goto_1
    iput-boolean v0, p0, LX/8vj;->aV:Z

    .line 1418678
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 1418679
    goto :goto_0

    .line 1418680
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final onOverScrolled(IIZZ)V
    .locals 3

    .prologue
    .line 1418659
    invoke-virtual {p0}, LX/8vj;->getScrollX()I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 1418660
    invoke-virtual {p0}, LX/8vj;->getScrollY()I

    move-result v0

    invoke-virtual {p0}, LX/8vj;->getScrollX()I

    move-result v1

    invoke-virtual {p0}, LX/8vj;->getScrollY()I

    move-result v2

    invoke-virtual {p0, p1, v0, v1, v2}, LX/8vj;->onScrollChanged(IIII)V

    .line 1418661
    iget-object v0, p0, LX/8vj;->a:LX/8vS;

    invoke-virtual {v0, p1}, LX/8vS;->a(I)V

    .line 1418662
    invoke-direct {p0}, LX/8vj;->w()V

    .line 1418663
    invoke-virtual {p0}, LX/8vj;->awakenScrollBars()Z

    .line 1418664
    :cond_0
    return-void
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x1

    .line 1418628
    check-cast p1, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;

    .line 1418629
    invoke-virtual {p1}, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, LX/8vi;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1418630
    iput-boolean v2, p0, LX/8vj;->aj:Z

    .line 1418631
    iget v0, p1, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->e:I

    int-to-long v0, v0

    iput-wide v0, p0, LX/8vj;->ac:J

    .line 1418632
    iget-wide v0, p1, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->a:J

    cmp-long v0, v0, v4

    if-ltz v0, :cond_4

    .line 1418633
    iput-boolean v2, p0, LX/8vj;->ad:Z

    .line 1418634
    iput-object p1, p0, LX/8vj;->bc:Lit/sephiroth/android/library/widget/AbsHListView$SavedState;

    .line 1418635
    iget-wide v0, p1, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->a:J

    iput-wide v0, p0, LX/8vj;->ab:J

    .line 1418636
    iget v0, p1, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->d:I

    iput v0, p0, LX/8vj;->aa:I

    .line 1418637
    iget v0, p1, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->c:I

    iput v0, p0, LX/8vj;->W:I

    .line 1418638
    const/4 v0, 0x0

    iput v0, p0, LX/8vj;->ae:I

    .line 1418639
    :cond_0
    :goto_0
    iget-object v0, p1, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->i:Landroid/util/SparseBooleanArray;

    if-eqz v0, :cond_1

    .line 1418640
    iget-object v0, p1, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->i:Landroid/util/SparseBooleanArray;

    iput-object v0, p0, LX/8vj;->f:Landroid/util/SparseBooleanArray;

    .line 1418641
    :cond_1
    iget-object v0, p1, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->j:LX/0tf;

    if-eqz v0, :cond_2

    .line 1418642
    iget-object v0, p1, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->j:LX/0tf;

    iput-object v0, p0, LX/8vj;->g:LX/0tf;

    .line 1418643
    :cond_2
    iget v0, p1, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->h:I

    iput v0, p0, LX/8vj;->e:I

    .line 1418644
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_3

    .line 1418645
    iget-boolean v0, p1, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->g:Z

    if-eqz v0, :cond_3

    iget v0, p0, LX/8vj;->b:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    iget-object v0, p0, LX/8vj;->d:Ljava/lang/Object;

    if-eqz v0, :cond_3

    .line 1418646
    iget-object v0, p0, LX/8vj;->d:Ljava/lang/Object;

    check-cast v0, LX/8vW;

    invoke-virtual {p0, v0}, LX/8vj;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v0

    iput-object v0, p0, LX/8vj;->c:Ljava/lang/Object;

    .line 1418647
    :cond_3
    invoke-virtual {p0}, LX/8vj;->requestLayout()V

    .line 1418648
    return-void

    .line 1418649
    :cond_4
    iget-wide v0, p1, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->b:J

    cmp-long v0, v0, v4

    if-ltz v0, :cond_0

    .line 1418650
    invoke-virtual {p0, v3}, LX/8vi;->setSelectedPositionInt(I)V

    .line 1418651
    invoke-virtual {p0, v3}, LX/8vi;->setNextSelectedPositionInt(I)V

    .line 1418652
    iput v3, p0, LX/8vj;->n:I

    .line 1418653
    iput-boolean v2, p0, LX/8vj;->ad:Z

    .line 1418654
    iput-object p1, p0, LX/8vj;->bc:Lit/sephiroth/android/library/widget/AbsHListView$SavedState;

    .line 1418655
    iget-wide v0, p1, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->b:J

    iput-wide v0, p0, LX/8vj;->ab:J

    .line 1418656
    iget v0, p1, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->d:I

    iput v0, p0, LX/8vj;->aa:I

    .line 1418657
    iget v0, p1, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->c:I

    iput v0, p0, LX/8vj;->W:I

    .line 1418658
    iput v2, p0, LX/8vj;->ae:I

    goto :goto_0
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 12

    .prologue
    const-wide/16 v8, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1418576
    invoke-super {p0}, LX/8vi;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 1418577
    new-instance v3, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;

    invoke-direct {v3, v0}, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 1418578
    iget-object v0, p0, LX/8vj;->bc:Lit/sephiroth/android/library/widget/AbsHListView$SavedState;

    if-eqz v0, :cond_0

    .line 1418579
    iget-object v0, p0, LX/8vj;->bc:Lit/sephiroth/android/library/widget/AbsHListView$SavedState;

    iget-wide v0, v0, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->a:J

    iput-wide v0, v3, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->a:J

    .line 1418580
    iget-object v0, p0, LX/8vj;->bc:Lit/sephiroth/android/library/widget/AbsHListView$SavedState;

    iget-wide v0, v0, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->b:J

    iput-wide v0, v3, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->b:J

    .line 1418581
    iget-object v0, p0, LX/8vj;->bc:Lit/sephiroth/android/library/widget/AbsHListView$SavedState;

    iget v0, v0, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->c:I

    iput v0, v3, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->c:I

    .line 1418582
    iget-object v0, p0, LX/8vj;->bc:Lit/sephiroth/android/library/widget/AbsHListView$SavedState;

    iget v0, v0, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->d:I

    iput v0, v3, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->d:I

    .line 1418583
    iget-object v0, p0, LX/8vj;->bc:Lit/sephiroth/android/library/widget/AbsHListView$SavedState;

    iget v0, v0, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->e:I

    iput v0, v3, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->e:I

    .line 1418584
    iget-object v0, p0, LX/8vj;->bc:Lit/sephiroth/android/library/widget/AbsHListView$SavedState;

    iget-object v0, v0, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->f:Ljava/lang/String;

    iput-object v0, v3, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->f:Ljava/lang/String;

    .line 1418585
    iget-object v0, p0, LX/8vj;->bc:Lit/sephiroth/android/library/widget/AbsHListView$SavedState;

    iget-boolean v0, v0, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->g:Z

    iput-boolean v0, v3, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->g:Z

    .line 1418586
    iget-object v0, p0, LX/8vj;->bc:Lit/sephiroth/android/library/widget/AbsHListView$SavedState;

    iget v0, v0, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->h:I

    iput v0, v3, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->h:I

    .line 1418587
    iget-object v0, p0, LX/8vj;->bc:Lit/sephiroth/android/library/widget/AbsHListView$SavedState;

    iget-object v0, v0, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->i:Landroid/util/SparseBooleanArray;

    iput-object v0, v3, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->i:Landroid/util/SparseBooleanArray;

    .line 1418588
    iget-object v0, p0, LX/8vj;->bc:Lit/sephiroth/android/library/widget/AbsHListView$SavedState;

    iget-object v0, v0, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->j:LX/0tf;

    iput-object v0, v3, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->j:LX/0tf;

    move-object v0, v3

    .line 1418589
    :goto_0
    return-object v0

    .line 1418590
    :cond_0
    invoke-virtual {p0}, LX/8vj;->getChildCount()I

    move-result v0

    if-lez v0, :cond_2

    iget v0, p0, LX/8vi;->ao:I

    if-lez v0, :cond_2

    move v0, v1

    .line 1418591
    :goto_1
    iget-wide v10, p0, LX/8vi;->al:J

    move-wide v4, v10

    .line 1418592
    iput-wide v4, v3, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->a:J

    .line 1418593
    invoke-virtual {p0}, LX/8vj;->getWidth()I

    move-result v6

    iput v6, v3, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->e:I

    .line 1418594
    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-ltz v4, :cond_3

    .line 1418595
    iget v0, p0, LX/8vj;->J:I

    iput v0, v3, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->c:I

    .line 1418596
    iget v0, p0, LX/8vi;->ak:I

    move v0, v0

    .line 1418597
    iput v0, v3, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->d:I

    .line 1418598
    iput-wide v8, v3, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->b:J

    .line 1418599
    :goto_2
    const/4 v0, 0x0

    iput-object v0, v3, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->f:Ljava/lang/String;

    .line 1418600
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-lt v0, v4, :cond_6

    iget v0, p0, LX/8vj;->b:I

    const/4 v4, 0x3

    if-ne v0, v4, :cond_6

    iget-object v0, p0, LX/8vj;->c:Ljava/lang/Object;

    if-eqz v0, :cond_6

    :goto_3
    iput-boolean v1, v3, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->g:Z

    .line 1418601
    iget-object v0, p0, LX/8vj;->f:Landroid/util/SparseBooleanArray;

    if-eqz v0, :cond_1

    .line 1418602
    :try_start_0
    iget-object v0, p0, LX/8vj;->f:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->clone()Landroid/util/SparseBooleanArray;

    move-result-object v0

    iput-object v0, v3, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->i:Landroid/util/SparseBooleanArray;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1418603
    :cond_1
    :goto_4
    iget-object v0, p0, LX/8vj;->g:LX/0tf;

    if-eqz v0, :cond_8

    .line 1418604
    new-instance v0, LX/0tf;

    invoke-direct {v0}, LX/0tf;-><init>()V

    .line 1418605
    iget-object v1, p0, LX/8vj;->g:LX/0tf;

    invoke-virtual {v1}, LX/0tf;->a()I

    move-result v1

    .line 1418606
    :goto_5
    if-ge v2, v1, :cond_7

    .line 1418607
    iget-object v4, p0, LX/8vj;->g:LX/0tf;

    invoke-virtual {v4, v2}, LX/0tf;->b(I)J

    move-result-wide v4

    iget-object v6, p0, LX/8vj;->g:LX/0tf;

    invoke-virtual {v6, v2}, LX/0tf;->c(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v0, v4, v5, v6}, LX/0tf;->b(JLjava/lang/Object;)V

    .line 1418608
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_2
    move v0, v2

    .line 1418609
    goto :goto_1

    .line 1418610
    :cond_3
    if-eqz v0, :cond_5

    iget v0, p0, LX/8vi;->V:I

    if-lez v0, :cond_5

    .line 1418611
    invoke-virtual {p0, v2}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1418612
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    iput v0, v3, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->c:I

    .line 1418613
    iget v0, p0, LX/8vi;->V:I

    .line 1418614
    iget v4, p0, LX/8vi;->ao:I

    if-lt v0, v4, :cond_4

    .line 1418615
    iget v0, p0, LX/8vi;->ao:I

    add-int/lit8 v0, v0, -0x1

    .line 1418616
    :cond_4
    iput v0, v3, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->d:I

    .line 1418617
    iget-object v4, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    invoke-interface {v4, v0}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    iput-wide v4, v3, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->b:J

    goto :goto_2

    .line 1418618
    :cond_5
    iput v2, v3, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->c:I

    .line 1418619
    iput-wide v8, v3, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->b:J

    .line 1418620
    iput v2, v3, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->d:I

    goto :goto_2

    :cond_6
    move v1, v2

    .line 1418621
    goto :goto_3

    .line 1418622
    :catch_0
    move-exception v0

    .line 1418623
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodError;->printStackTrace()V

    .line 1418624
    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v0, v3, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->i:Landroid/util/SparseBooleanArray;

    goto :goto_4

    .line 1418625
    :cond_7
    iput-object v0, v3, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->j:LX/0tf;

    .line 1418626
    :cond_8
    iget v0, p0, LX/8vj;->e:I

    iput v0, v3, Lit/sephiroth/android/library/widget/AbsHListView$SavedState;->h:I

    move-object v0, v3

    .line 1418627
    goto/16 :goto_0
.end method

.method public onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x514a6fec

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1418572
    invoke-virtual {p0}, LX/8vj;->getChildCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 1418573
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/8vj;->aj:Z

    .line 1418574
    invoke-virtual {p0}, LX/8vi;->o()V

    .line 1418575
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x5f754ab

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "Override"
        }
    .end annotation

    .prologue
    const/4 v7, 0x4

    const/4 v12, 0x2

    const/4 v2, 0x1

    const/4 v11, -0x1

    const/4 v1, 0x0

    const v0, 0x26ff62e0

    invoke-static {v12, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v4

    .line 1418381
    invoke-virtual {p0}, LX/8vj;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1418382
    invoke-virtual {p0}, LX/8vj;->isClickable()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/8vj;->isLongClickable()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    const v1, 0x6766fc95

    invoke-static {v1, v4}, LX/02F;->a(II)V

    move v2, v0

    .line 1418383
    :goto_1
    return v2

    :cond_1
    move v0, v1

    .line 1418384
    goto :goto_0

    .line 1418385
    :cond_2
    iget-object v0, p0, LX/8vj;->I:Lit/sephiroth/android/library/widget/AbsHListView$PositionScroller;

    if-eqz v0, :cond_3

    .line 1418386
    iget-object v0, p0, LX/8vj;->I:Lit/sephiroth/android/library/widget/AbsHListView$PositionScroller;

    invoke-virtual {v0}, Lit/sephiroth/android/library/widget/AbsHListView$PositionScroller;->a()V

    .line 1418387
    :cond_3
    iget-boolean v0, p0, LX/8vj;->S:Z

    if-nez v0, :cond_4

    .line 1418388
    const v0, 0x1beb98c6

    invoke-static {v0, v4}, LX/02F;->a(II)V

    move v2, v1

    goto :goto_1

    .line 1418389
    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 1418390
    invoke-direct {p0}, LX/8vj;->y()V

    .line 1418391
    iget-object v3, p0, LX/8vj;->au:Landroid/view/VelocityTracker;

    invoke-virtual {v3, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 1418392
    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    .line 1418393
    :cond_5
    :goto_2
    :pswitch_0
    const v0, -0x2b81fbc7

    invoke-static {v0, v4}, LX/02F;->a(II)V

    goto :goto_1

    .line 1418394
    :pswitch_1
    iget v0, p0, LX/8vj;->F:I

    packed-switch v0, :pswitch_data_1

    .line 1418395
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, LX/8vj;->aP:I

    .line 1418396
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v5, v0

    .line 1418397
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v6, v0

    .line 1418398
    invoke-direct {p0, v5, v6}, LX/8vj;->b(II)I

    move-result v3

    .line 1418399
    iget-boolean v0, p0, LX/8vi;->aj:Z

    if-nez v0, :cond_29

    .line 1418400
    iget v0, p0, LX/8vj;->F:I

    if-eq v0, v7, :cond_9

    if-ltz v3, :cond_9

    invoke-virtual {p0}, LX/8vi;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    invoke-interface {v0, v3}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1418401
    iput v1, p0, LX/8vj;->F:I

    .line 1418402
    iget-object v0, p0, LX/8vj;->aC:Ljava/lang/Runnable;

    if-nez v0, :cond_6

    .line 1418403
    new-instance v0, Lit/sephiroth/android/library/widget/AbsHListView$CheckForTap;

    invoke-direct {v0, p0}, Lit/sephiroth/android/library/widget/AbsHListView$CheckForTap;-><init>(LX/8vj;)V

    iput-object v0, p0, LX/8vj;->aC:Ljava/lang/Runnable;

    .line 1418404
    :cond_6
    iget-object v0, p0, LX/8vj;->aC:Ljava/lang/Runnable;

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v1

    int-to-long v8, v1

    invoke-virtual {p0, v0, v8, v9}, LX/8vj;->postDelayed(Ljava/lang/Runnable;J)Z

    move v0, v3

    .line 1418405
    :goto_3
    if-ltz v0, :cond_7

    .line 1418406
    iget v1, p0, LX/8vi;->V:I

    sub-int v1, v0, v1

    invoke-virtual {p0, v1}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1418407
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    iput v1, p0, LX/8vj;->B:I

    .line 1418408
    :cond_7
    iput v5, p0, LX/8vj;->D:I

    .line 1418409
    iput v6, p0, LX/8vj;->E:I

    .line 1418410
    iput v0, p0, LX/8vj;->A:I

    .line 1418411
    const/high16 v0, -0x80000000

    iput v0, p0, LX/8vj;->G:I

    .line 1418412
    :goto_4
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_2a

    .line 1418413
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getButtonState()I

    move-result v0

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2a

    .line 1418414
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getMetaState()I

    move-result v3

    invoke-static {p0, v0, v1, v3}, LX/8vj;->a(LX/8vj;FFI)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 1418415
    const/4 v0, 0x1

    .line 1418416
    :goto_5
    move v0, v0

    .line 1418417
    if-eqz v0, :cond_5

    .line 1418418
    iget v0, p0, LX/8vj;->F:I

    if-nez v0, :cond_5

    .line 1418419
    iget-object v0, p0, LX/8vj;->aC:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, LX/8vj;->removeCallbacks(Ljava/lang/Runnable;)Z

    goto/16 :goto_2

    .line 1418420
    :pswitch_2
    iget-object v0, p0, LX/8vj;->av:Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    invoke-virtual {v0}, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->b()V

    .line 1418421
    iget-object v0, p0, LX/8vj;->I:Lit/sephiroth/android/library/widget/AbsHListView$PositionScroller;

    if-eqz v0, :cond_8

    .line 1418422
    iget-object v0, p0, LX/8vj;->I:Lit/sephiroth/android/library/widget/AbsHListView$PositionScroller;

    invoke-virtual {v0}, Lit/sephiroth/android/library/widget/AbsHListView$PositionScroller;->a()V

    .line 1418423
    :cond_8
    const/4 v0, 0x5

    iput v0, p0, LX/8vj;->F:I

    .line 1418424
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, LX/8vj;->E:I

    .line 1418425
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, LX/8vj;->G:I

    iput v0, p0, LX/8vj;->D:I

    .line 1418426
    iput v1, p0, LX/8vj;->H:I

    .line 1418427
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, LX/8vj;->aP:I

    .line 1418428
    iput v1, p0, LX/8vj;->aU:I

    goto :goto_4

    .line 1418429
    :cond_9
    iget v0, p0, LX/8vj;->F:I

    if-ne v0, v7, :cond_29

    .line 1418430
    invoke-direct {p0}, LX/8vj;->A()V

    .line 1418431
    const/4 v0, 0x3

    iput v0, p0, LX/8vj;->F:I

    .line 1418432
    iput v1, p0, LX/8vj;->H:I

    .line 1418433
    invoke-virtual {p0, v5}, LX/8vj;->c(I)I

    move-result v0

    .line 1418434
    iget-object v1, p0, LX/8vj;->av:Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    invoke-virtual {v1}, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->c()V

    goto/16 :goto_3

    .line 1418435
    :pswitch_3
    iget v0, p0, LX/8vj;->aP:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    .line 1418436
    if-ne v0, v11, :cond_28

    .line 1418437
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, LX/8vj;->aP:I

    .line 1418438
    :goto_6
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    float-to-int v0, v0

    .line 1418439
    iget-boolean v1, p0, LX/8vi;->aj:Z

    if-eqz v1, :cond_a

    .line 1418440
    invoke-virtual {p0}, LX/8vj;->d()V

    .line 1418441
    :cond_a
    iget v1, p0, LX/8vj;->F:I

    packed-switch v1, :pswitch_data_2

    :pswitch_4
    goto/16 :goto_2

    .line 1418442
    :pswitch_5
    invoke-direct {p0, v0}, LX/8vj;->f(I)Z

    goto/16 :goto_2

    .line 1418443
    :pswitch_6
    invoke-direct {p0, v0}, LX/8vj;->g(I)V

    goto/16 :goto_2

    .line 1418444
    :pswitch_7
    iget v0, p0, LX/8vj;->F:I

    packed-switch v0, :pswitch_data_3

    .line 1418445
    :cond_b
    :goto_7
    :pswitch_8
    invoke-virtual {p0, v1}, LX/8vj;->setPressed(Z)V

    .line 1418446
    iget-object v0, p0, LX/8vj;->aQ:LX/8vn;

    if-eqz v0, :cond_c

    .line 1418447
    iget-object v0, p0, LX/8vj;->aQ:LX/8vn;

    invoke-virtual {v0}, LX/8vn;->c()V

    .line 1418448
    iget-object v0, p0, LX/8vj;->aR:LX/8vn;

    invoke-virtual {v0}, LX/8vn;->c()V

    .line 1418449
    :cond_c
    invoke-virtual {p0}, LX/8vj;->invalidate()V

    .line 1418450
    invoke-virtual {p0}, LX/8vj;->getHandler()Landroid/os/Handler;

    move-result-object v0

    .line 1418451
    if-eqz v0, :cond_d

    .line 1418452
    iget-object v1, p0, LX/8vj;->aB:Lit/sephiroth/android/library/widget/AbsHListView$CheckForLongPress;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1418453
    :cond_d
    invoke-direct {p0}, LX/8vj;->z()V

    .line 1418454
    iput v11, p0, LX/8vj;->aP:I

    goto/16 :goto_2

    .line 1418455
    :pswitch_9
    iget v3, p0, LX/8vj;->A:I

    .line 1418456
    iget v0, p0, LX/8vi;->V:I

    sub-int v0, v3, v0

    invoke-virtual {p0, v0}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 1418457
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 1418458
    iget-object v6, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    int-to-float v6, v6

    cmpl-float v6, v0, v6

    if-lez v6, :cond_14

    invoke-virtual {p0}, LX/8vj;->getWidth()I

    move-result v6

    iget-object v7, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    sub-int/2addr v6, v7

    int-to-float v6, v6

    cmpg-float v0, v0, v6

    if-gez v0, :cond_14

    move v0, v2

    .line 1418459
    :goto_8
    if-eqz v5, :cond_18

    invoke-virtual {v5}, Landroid/view/View;->hasFocusable()Z

    move-result v6

    if-nez v6, :cond_18

    if-eqz v0, :cond_18

    .line 1418460
    iget v0, p0, LX/8vj;->F:I

    if-eqz v0, :cond_e

    .line 1418461
    invoke-virtual {v5, v1}, Landroid/view/View;->setPressed(Z)V

    .line 1418462
    :cond_e
    iget-object v0, p0, LX/8vj;->aE:Lit/sephiroth/android/library/widget/AbsHListView$PerformClick;

    if-nez v0, :cond_f

    .line 1418463
    new-instance v0, Lit/sephiroth/android/library/widget/AbsHListView$PerformClick;

    invoke-direct {v0, p0}, Lit/sephiroth/android/library/widget/AbsHListView$PerformClick;-><init>(LX/8vj;)V

    iput-object v0, p0, LX/8vj;->aE:Lit/sephiroth/android/library/widget/AbsHListView$PerformClick;

    .line 1418464
    :cond_f
    iget-object v6, p0, LX/8vj;->aE:Lit/sephiroth/android/library/widget/AbsHListView$PerformClick;

    .line 1418465
    iput v3, v6, Lit/sephiroth/android/library/widget/AbsHListView$PerformClick;->a:I

    .line 1418466
    invoke-virtual {v6}, LX/8vb;->a()V

    .line 1418467
    iput v3, p0, LX/8vj;->M:I

    .line 1418468
    iget v0, p0, LX/8vj;->F:I

    if-eqz v0, :cond_10

    iget v0, p0, LX/8vj;->F:I

    if-ne v0, v2, :cond_17

    .line 1418469
    :cond_10
    invoke-virtual {p0}, LX/8vj;->getHandler()Landroid/os/Handler;

    move-result-object v7

    .line 1418470
    if-eqz v7, :cond_11

    .line 1418471
    iget v0, p0, LX/8vj;->F:I

    if-nez v0, :cond_15

    iget-object v0, p0, LX/8vj;->aC:Ljava/lang/Runnable;

    :goto_9
    invoke-static {v7, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1418472
    :cond_11
    iput v1, p0, LX/8vj;->h:I

    .line 1418473
    iget-boolean v0, p0, LX/8vi;->aj:Z

    if-nez v0, :cond_16

    iget-object v0, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    invoke-interface {v0, v3}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 1418474
    iput v2, p0, LX/8vj;->F:I

    .line 1418475
    iget v0, p0, LX/8vj;->A:I

    invoke-virtual {p0, v0}, LX/8vi;->setSelectedPositionInt(I)V

    .line 1418476
    invoke-virtual {p0}, LX/8vj;->d()V

    .line 1418477
    invoke-virtual {v5, v2}, Landroid/view/View;->setPressed(Z)V

    .line 1418478
    iget v0, p0, LX/8vj;->A:I

    invoke-virtual {p0, v0, v5}, LX/8vj;->a(ILandroid/view/View;)V

    .line 1418479
    invoke-virtual {p0, v2}, LX/8vj;->setPressed(Z)V

    .line 1418480
    iget-object v0, p0, LX/8vj;->m:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_12

    .line 1418481
    iget-object v0, p0, LX/8vj;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1418482
    if-eqz v0, :cond_12

    instance-of v1, v0, Landroid/graphics/drawable/TransitionDrawable;

    if-eqz v1, :cond_12

    .line 1418483
    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/TransitionDrawable;->resetTransition()V

    .line 1418484
    :cond_12
    iget-object v0, p0, LX/8vj;->aF:Ljava/lang/Runnable;

    if-eqz v0, :cond_13

    .line 1418485
    iget-object v0, p0, LX/8vj;->aF:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, LX/8vj;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1418486
    :cond_13
    new-instance v0, Lit/sephiroth/android/library/widget/AbsHListView$1;

    invoke-direct {v0, p0, v5, v6}, Lit/sephiroth/android/library/widget/AbsHListView$1;-><init>(LX/8vj;Landroid/view/View;Lit/sephiroth/android/library/widget/AbsHListView$PerformClick;)V

    iput-object v0, p0, LX/8vj;->aF:Ljava/lang/Runnable;

    .line 1418487
    iget-object v0, p0, LX/8vj;->aF:Ljava/lang/Runnable;

    invoke-static {}, Landroid/view/ViewConfiguration;->getPressedStateDuration()I

    move-result v1

    int-to-long v6, v1

    invoke-virtual {p0, v0, v6, v7}, LX/8vj;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1418488
    :goto_a
    const v0, -0x4ecddd8e    # -2.592199E-9f

    invoke-static {v0, v4}, LX/02F;->a(II)V

    goto/16 :goto_1

    :cond_14
    move v0, v1

    .line 1418489
    goto/16 :goto_8

    .line 1418490
    :cond_15
    iget-object v0, p0, LX/8vj;->aB:Lit/sephiroth/android/library/widget/AbsHListView$CheckForLongPress;

    goto :goto_9

    .line 1418491
    :cond_16
    iput v11, p0, LX/8vj;->F:I

    .line 1418492
    invoke-direct {p0}, LX/8vj;->v()V

    goto :goto_a

    .line 1418493
    :cond_17
    iget-boolean v0, p0, LX/8vi;->aj:Z

    if-nez v0, :cond_18

    iget-object v0, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    invoke-interface {v0, v3}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 1418494
    invoke-virtual {v6}, Lit/sephiroth/android/library/widget/AbsHListView$PerformClick;->run()V

    .line 1418495
    :cond_18
    iput v11, p0, LX/8vj;->F:I

    .line 1418496
    invoke-direct {p0}, LX/8vj;->v()V

    goto/16 :goto_7

    .line 1418497
    :pswitch_a
    invoke-virtual {p0}, LX/8vj;->getChildCount()I

    move-result v0

    .line 1418498
    if-lez v0, :cond_1f

    .line 1418499
    invoke-virtual {p0, v1}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v3

    .line 1418500
    add-int/lit8 v5, v0, -0x1

    invoke-virtual {p0, v5}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    move-result v5

    .line 1418501
    iget-object v6, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    .line 1418502
    invoke-virtual {p0}, LX/8vj;->getWidth()I

    move-result v7

    iget-object v8, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->right:I

    sub-int/2addr v7, v8

    .line 1418503
    iget v8, p0, LX/8vi;->V:I

    if-nez v8, :cond_19

    if-lt v3, v6, :cond_19

    iget v8, p0, LX/8vi;->V:I

    add-int/2addr v8, v0

    iget v9, p0, LX/8vi;->ao:I

    if-ge v8, v9, :cond_19

    invoke-virtual {p0}, LX/8vj;->getWidth()I

    move-result v8

    sub-int/2addr v8, v7

    if-gt v5, v8, :cond_19

    .line 1418504
    iput v11, p0, LX/8vj;->F:I

    .line 1418505
    invoke-virtual {p0, v1}, LX/8vj;->a(I)V

    goto/16 :goto_7

    .line 1418506
    :cond_19
    iget-object v8, p0, LX/8vj;->au:Landroid/view/VelocityTracker;

    .line 1418507
    const/16 v9, 0x3e8

    iget v10, p0, LX/8vj;->aN:I

    int-to-float v10, v10

    invoke-virtual {v8, v9, v10}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 1418508
    iget v9, p0, LX/8vj;->aP:I

    invoke-virtual {v8, v9}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v8

    iget v9, p0, LX/8vj;->aO:F

    mul-float/2addr v8, v9

    float-to-int v8, v8

    .line 1418509
    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v9

    iget v10, p0, LX/8vj;->aM:I

    if-le v9, v10, :cond_1d

    iget v9, p0, LX/8vi;->V:I

    if-nez v9, :cond_1a

    iget v9, p0, LX/8vj;->Q:I

    sub-int/2addr v6, v9

    if-eq v3, v6, :cond_1d

    :cond_1a
    iget v3, p0, LX/8vi;->V:I

    add-int/2addr v0, v3

    iget v3, p0, LX/8vi;->ao:I

    if-ne v0, v3, :cond_1b

    iget v0, p0, LX/8vj;->Q:I

    add-int/2addr v0, v7

    if-eq v5, v0, :cond_1d

    .line 1418510
    :cond_1b
    iget-object v0, p0, LX/8vj;->av:Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    if-nez v0, :cond_1c

    .line 1418511
    new-instance v0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    invoke-direct {v0, p0}, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;-><init>(LX/8vj;)V

    iput-object v0, p0, LX/8vj;->av:Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    .line 1418512
    :cond_1c
    invoke-virtual {p0, v12}, LX/8vj;->a(I)V

    .line 1418513
    iget-object v0, p0, LX/8vj;->av:Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    neg-int v3, v8

    invoke-virtual {v0, v3}, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a(I)V

    goto/16 :goto_7

    .line 1418514
    :cond_1d
    iput v11, p0, LX/8vj;->F:I

    .line 1418515
    invoke-virtual {p0, v1}, LX/8vj;->a(I)V

    .line 1418516
    iget-object v0, p0, LX/8vj;->av:Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    if-eqz v0, :cond_1e

    .line 1418517
    iget-object v0, p0, LX/8vj;->av:Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    invoke-virtual {v0}, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->b()V

    .line 1418518
    :cond_1e
    iget-object v0, p0, LX/8vj;->I:Lit/sephiroth/android/library/widget/AbsHListView$PositionScroller;

    if-eqz v0, :cond_b

    .line 1418519
    iget-object v0, p0, LX/8vj;->I:Lit/sephiroth/android/library/widget/AbsHListView$PositionScroller;

    invoke-virtual {v0}, Lit/sephiroth/android/library/widget/AbsHListView$PositionScroller;->a()V

    goto/16 :goto_7

    .line 1418520
    :cond_1f
    iput v11, p0, LX/8vj;->F:I

    .line 1418521
    invoke-virtual {p0, v1}, LX/8vj;->a(I)V

    goto/16 :goto_7

    .line 1418522
    :pswitch_b
    iget-object v0, p0, LX/8vj;->av:Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    if-nez v0, :cond_20

    .line 1418523
    new-instance v0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    invoke-direct {v0, p0}, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;-><init>(LX/8vj;)V

    iput-object v0, p0, LX/8vj;->av:Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    .line 1418524
    :cond_20
    iget-object v0, p0, LX/8vj;->au:Landroid/view/VelocityTracker;

    .line 1418525
    const/16 v3, 0x3e8

    iget v5, p0, LX/8vj;->aN:I

    int-to-float v5, v5

    invoke-virtual {v0, v3, v5}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 1418526
    iget v3, p0, LX/8vj;->aP:I

    invoke-virtual {v0, v3}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v0

    float-to-int v0, v0

    .line 1418527
    invoke-virtual {p0, v12}, LX/8vj;->a(I)V

    .line 1418528
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v3

    iget v5, p0, LX/8vj;->aM:I

    if-le v3, v5, :cond_21

    .line 1418529
    iget-object v3, p0, LX/8vj;->av:Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    neg-int v0, v0

    invoke-virtual {v3, v0}, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->b(I)V

    goto/16 :goto_7

    .line 1418530
    :cond_21
    iget-object v0, p0, LX/8vj;->av:Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    invoke-virtual {v0}, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a()V

    goto/16 :goto_7

    .line 1418531
    :pswitch_c
    iget v0, p0, LX/8vj;->F:I

    packed-switch v0, :pswitch_data_4

    .line 1418532
    iput v11, p0, LX/8vj;->F:I

    .line 1418533
    invoke-virtual {p0, v1}, LX/8vj;->setPressed(Z)V

    .line 1418534
    iget v0, p0, LX/8vj;->A:I

    iget v3, p0, LX/8vi;->V:I

    sub-int/2addr v0, v3

    invoke-virtual {p0, v0}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1418535
    if-eqz v0, :cond_22

    .line 1418536
    invoke-virtual {v0, v1}, Landroid/view/View;->setPressed(Z)V

    .line 1418537
    :cond_22
    invoke-static {p0}, LX/8vj;->B(LX/8vj;)V

    .line 1418538
    invoke-virtual {p0}, LX/8vj;->getHandler()Landroid/os/Handler;

    move-result-object v0

    .line 1418539
    if-eqz v0, :cond_23

    .line 1418540
    iget-object v1, p0, LX/8vj;->aB:Lit/sephiroth/android/library/widget/AbsHListView$CheckForLongPress;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1418541
    :cond_23
    invoke-direct {p0}, LX/8vj;->z()V

    .line 1418542
    :goto_b
    :pswitch_d
    iget-object v0, p0, LX/8vj;->aQ:LX/8vn;

    if-eqz v0, :cond_24

    .line 1418543
    iget-object v0, p0, LX/8vj;->aQ:LX/8vn;

    invoke-virtual {v0}, LX/8vn;->c()V

    .line 1418544
    iget-object v0, p0, LX/8vj;->aR:LX/8vn;

    invoke-virtual {v0}, LX/8vn;->c()V

    .line 1418545
    :cond_24
    iput v11, p0, LX/8vj;->aP:I

    goto/16 :goto_2

    .line 1418546
    :pswitch_e
    iget-object v0, p0, LX/8vj;->av:Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    if-nez v0, :cond_25

    .line 1418547
    new-instance v0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    invoke-direct {v0, p0}, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;-><init>(LX/8vj;)V

    iput-object v0, p0, LX/8vj;->av:Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    .line 1418548
    :cond_25
    iget-object v0, p0, LX/8vj;->av:Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    invoke-virtual {v0}, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->a()V

    goto :goto_b

    .line 1418549
    :pswitch_f
    invoke-direct {p0, p1}, LX/8vj;->b(Landroid/view/MotionEvent;)V

    .line 1418550
    iget v0, p0, LX/8vj;->D:I

    .line 1418551
    iget v1, p0, LX/8vj;->E:I

    .line 1418552
    invoke-direct {p0, v0, v1}, LX/8vj;->b(II)I

    move-result v1

    .line 1418553
    if-ltz v1, :cond_26

    .line 1418554
    iget v3, p0, LX/8vi;->V:I

    sub-int v3, v1, v3

    invoke-virtual {p0, v3}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1418555
    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v3

    iput v3, p0, LX/8vj;->B:I

    .line 1418556
    iput v1, p0, LX/8vj;->A:I

    .line 1418557
    :cond_26
    iput v0, p0, LX/8vj;->G:I

    goto/16 :goto_2

    .line 1418558
    :pswitch_10
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    .line 1418559
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v3

    .line 1418560
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v5

    float-to-int v5, v5

    .line 1418561
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    float-to-int v0, v0

    .line 1418562
    iput v1, p0, LX/8vj;->H:I

    .line 1418563
    iput v3, p0, LX/8vj;->aP:I

    .line 1418564
    iput v5, p0, LX/8vj;->D:I

    .line 1418565
    iput v0, p0, LX/8vj;->E:I

    .line 1418566
    invoke-direct {p0, v5, v0}, LX/8vj;->b(II)I

    move-result v0

    .line 1418567
    if-ltz v0, :cond_27

    .line 1418568
    iget v1, p0, LX/8vi;->V:I

    sub-int v1, v0, v1

    invoke-virtual {p0, v1}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1418569
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    iput v1, p0, LX/8vj;->B:I

    .line 1418570
    iput v0, p0, LX/8vj;->A:I

    .line 1418571
    :cond_27
    iput v5, p0, LX/8vj;->G:I

    goto/16 :goto_2

    :cond_28
    move v1, v0

    goto/16 :goto_6

    :cond_29
    move v0, v3

    goto/16 :goto_3

    :cond_2a
    const/4 v0, 0x0

    goto/16 :goto_5

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_7
        :pswitch_3
        :pswitch_c
        :pswitch_0
        :pswitch_10
        :pswitch_f
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x6
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_4
        :pswitch_6
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_a
        :pswitch_8
        :pswitch_b
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x5
        :pswitch_e
        :pswitch_d
    .end packed-switch
.end method

.method public final onTouchModeChanged(Z)V
    .locals 2

    .prologue
    .line 1418365
    if-eqz p1, :cond_2

    .line 1418366
    invoke-virtual {p0}, LX/8vj;->h()V

    .line 1418367
    invoke-virtual {p0}, LX/8vj;->getWidth()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, LX/8vj;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 1418368
    invoke-virtual {p0}, LX/8vj;->d()V

    .line 1418369
    :cond_0
    invoke-direct {p0}, LX/8vj;->v()V

    .line 1418370
    :cond_1
    :goto_0
    return-void

    .line 1418371
    :cond_2
    iget v0, p0, LX/8vj;->F:I

    .line 1418372
    const/4 v1, 0x5

    if-eq v0, v1, :cond_3

    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    .line 1418373
    :cond_3
    iget-object v0, p0, LX/8vj;->av:Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    if-eqz v0, :cond_4

    .line 1418374
    iget-object v0, p0, LX/8vj;->av:Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    invoke-virtual {v0}, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->b()V

    .line 1418375
    :cond_4
    iget-object v0, p0, LX/8vj;->I:Lit/sephiroth/android/library/widget/AbsHListView$PositionScroller;

    if-eqz v0, :cond_5

    .line 1418376
    iget-object v0, p0, LX/8vj;->I:Lit/sephiroth/android/library/widget/AbsHListView$PositionScroller;

    invoke-virtual {v0}, Lit/sephiroth/android/library/widget/AbsHListView$PositionScroller;->a()V

    .line 1418377
    :cond_5
    invoke-virtual {p0}, LX/8vj;->getScrollX()I

    move-result v0

    if-eqz v0, :cond_1

    .line 1418378
    iget-object v0, p0, LX/8vj;->a:LX/8vS;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/8vS;->a(I)V

    .line 1418379
    invoke-direct {p0}, LX/8vj;->E()V

    .line 1418380
    invoke-virtual {p0}, LX/8vj;->invalidate()V

    goto :goto_0
.end method

.method public final onWindowFocusChanged(Z)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x2

    const/16 v3, 0x2c

    const v4, 0x6aba268a

    invoke-static {v0, v3, v4}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 1418341
    invoke-super {p0, p1}, LX/8vi;->onWindowFocusChanged(Z)V

    .line 1418342
    invoke-virtual {p0}, LX/8vj;->isInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 1418343
    :goto_0
    if-nez p1, :cond_4

    .line 1418344
    invoke-virtual {p0, v1}, LX/8vj;->setChildrenDrawingCacheEnabled(Z)V

    .line 1418345
    iget-object v4, p0, LX/8vj;->av:Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    if-eqz v4, :cond_1

    .line 1418346
    iget-object v4, p0, LX/8vj;->av:Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    invoke-virtual {p0, v4}, LX/8vj;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1418347
    iget-object v4, p0, LX/8vj;->av:Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    invoke-virtual {v4}, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->b()V

    .line 1418348
    iget-object v4, p0, LX/8vj;->I:Lit/sephiroth/android/library/widget/AbsHListView$PositionScroller;

    if-eqz v4, :cond_0

    .line 1418349
    iget-object v4, p0, LX/8vj;->I:Lit/sephiroth/android/library/widget/AbsHListView$PositionScroller;

    invoke-virtual {v4}, Lit/sephiroth/android/library/widget/AbsHListView$PositionScroller;->a()V

    .line 1418350
    :cond_0
    invoke-virtual {p0}, LX/8vj;->getScrollX()I

    move-result v4

    if-eqz v4, :cond_1

    .line 1418351
    iget-object v4, p0, LX/8vj;->a:LX/8vS;

    invoke-virtual {v4, v1}, LX/8vS;->a(I)V

    .line 1418352
    invoke-direct {p0}, LX/8vj;->E()V

    .line 1418353
    invoke-virtual {p0}, LX/8vj;->invalidate()V

    .line 1418354
    :cond_1
    if-ne v0, v2, :cond_2

    .line 1418355
    iget v1, p0, LX/8vi;->am:I

    iput v1, p0, LX/8vj;->M:I

    .line 1418356
    :cond_2
    :goto_1
    iput v0, p0, LX/8vj;->aA:I

    .line 1418357
    const v0, -0x4b0c50fd

    invoke-static {v0, v3}, LX/02F;->g(II)V

    return-void

    :cond_3
    move v0, v2

    .line 1418358
    goto :goto_0

    .line 1418359
    :cond_4
    iget v4, p0, LX/8vj;->aA:I

    if-eq v0, v4, :cond_2

    iget v4, p0, LX/8vj;->aA:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_2

    .line 1418360
    if-ne v0, v2, :cond_5

    .line 1418361
    invoke-direct {p0}, LX/8vj;->C()Z

    goto :goto_1

    .line 1418362
    :cond_5
    invoke-virtual {p0}, LX/8vj;->h()V

    .line 1418363
    iput v1, p0, LX/8vj;->h:I

    .line 1418364
    invoke-virtual {p0}, LX/8vj;->d()V

    goto :goto_1
.end method

.method public final performAccessibilityAction(ILandroid/os/Bundle;)Z
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/16 v4, 0xc8

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1418327
    invoke-super {p0, p1, p2}, LX/8vi;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1418328
    :goto_0
    return v0

    .line 1418329
    :cond_0
    sparse-switch p1, :sswitch_data_0

    move v0, v1

    .line 1418330
    goto :goto_0

    .line 1418331
    :sswitch_0
    invoke-virtual {p0}, LX/8vj;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, LX/8vi;->getLastVisiblePosition()I

    move-result v2

    .line 1418332
    iget v3, p0, LX/8vi;->ao:I

    move v3, v3

    .line 1418333
    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_1

    .line 1418334
    invoke-virtual {p0}, LX/8vj;->getWidth()I

    move-result v1

    iget-object v2, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v2

    iget-object v2, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v1, v2

    .line 1418335
    invoke-direct {p0, v1, v4}, LX/8vj;->c(II)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1418336
    goto :goto_0

    .line 1418337
    :sswitch_1
    invoke-virtual {p0}, LX/8vj;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_2

    iget v2, p0, LX/8vi;->V:I

    if-lez v2, :cond_2

    .line 1418338
    invoke-virtual {p0}, LX/8vj;->getWidth()I

    move-result v1

    iget-object v2, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v2

    iget-object v2, p0, LX/8vj;->u:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v1, v2

    .line 1418339
    neg-int v1, v1

    invoke-direct {p0, v1, v4}, LX/8vj;->c(II)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 1418340
    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1000 -> :sswitch_0
        0x2000 -> :sswitch_1
    .end sparse-switch
.end method

.method public final requestDisallowInterceptTouchEvent(Z)V
    .locals 0

    .prologue
    .line 1418323
    if-eqz p1, :cond_0

    .line 1418324
    invoke-direct {p0}, LX/8vj;->z()V

    .line 1418325
    :cond_0
    invoke-super {p0, p1}, LX/8vi;->requestDisallowInterceptTouchEvent(Z)V

    .line 1418326
    return-void
.end method

.method public final requestLayout()V
    .locals 1

    .prologue
    .line 1418320
    iget-boolean v0, p0, LX/8vi;->at:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/8vi;->af:Z

    if-nez v0, :cond_0

    .line 1418321
    invoke-super {p0}, LX/8vi;->requestLayout()V

    .line 1418322
    :cond_0
    return-void
.end method

.method public final sendAccessibilityEvent(I)V
    .locals 3

    .prologue
    .line 1418312
    const/16 v0, 0x1000

    if-ne p1, v0, :cond_1

    .line 1418313
    iget v0, p0, LX/8vi;->V:I

    move v0, v0

    .line 1418314
    invoke-virtual {p0}, LX/8vi;->getLastVisiblePosition()I

    move-result v1

    .line 1418315
    iget v2, p0, LX/8vj;->aZ:I

    if-ne v2, v0, :cond_0

    iget v2, p0, LX/8vj;->ba:I

    if-ne v2, v1, :cond_0

    .line 1418316
    :goto_0
    return-void

    .line 1418317
    :cond_0
    iput v0, p0, LX/8vj;->aZ:I

    .line 1418318
    iput v1, p0, LX/8vj;->ba:I

    .line 1418319
    :cond_1
    invoke-super {p0, p1}, LX/8vi;->sendAccessibilityEvent(I)V

    goto :goto_0
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0

    .prologue
    .line 1418237
    check-cast p1, Landroid/widget/ListAdapter;

    invoke-virtual {p0, p1}, LX/8vj;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 1

    .prologue
    .line 1418154
    if-eqz p1, :cond_0

    .line 1418155
    iget-object v0, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v0

    iput-boolean v0, p0, LX/8vj;->k:Z

    .line 1418156
    iget v0, p0, LX/8vj;->b:I

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/8vj;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8vj;->g:LX/0tf;

    if-nez v0, :cond_0

    .line 1418157
    new-instance v0, LX/0tf;

    invoke-direct {v0}, LX/0tf;-><init>()V

    iput-object v0, p0, LX/8vj;->g:LX/0tf;

    .line 1418158
    :cond_0
    iget-object v0, p0, LX/8vj;->f:Landroid/util/SparseBooleanArray;

    if-eqz v0, :cond_1

    .line 1418159
    iget-object v0, p0, LX/8vj;->f:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->clear()V

    .line 1418160
    :cond_1
    iget-object v0, p0, LX/8vj;->g:LX/0tf;

    if-eqz v0, :cond_2

    .line 1418161
    iget-object v0, p0, LX/8vj;->g:LX/0tf;

    invoke-virtual {v0}, LX/0tf;->b()V

    .line 1418162
    :cond_2
    return-void
.end method

.method public setCacheColorHint(I)V
    .locals 7

    .prologue
    .line 1418163
    iget v0, p0, LX/8vj;->aH:I

    if-eq p1, v0, :cond_5

    .line 1418164
    iput p1, p0, LX/8vj;->aH:I

    .line 1418165
    invoke-virtual {p0}, LX/8vj;->getChildCount()I

    move-result v1

    .line 1418166
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 1418167
    invoke-virtual {p0, v0}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->setDrawingCacheBackgroundColor(I)V

    .line 1418168
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1418169
    :cond_0
    iget-object v0, p0, LX/8vj;->p:LX/8vf;

    const/4 v2, 0x0

    .line 1418170
    iget v1, v0, LX/8vf;->f:I

    const/4 v3, 0x1

    if-ne v1, v3, :cond_1

    .line 1418171
    iget-object v4, v0, LX/8vf;->g:Ljava/util/ArrayList;

    .line 1418172
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v2

    .line 1418173
    :goto_1
    if-ge v3, v5, :cond_3

    .line 1418174
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1, p1}, Landroid/view/View;->setDrawingCacheBackgroundColor(I)V

    .line 1418175
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    .line 1418176
    :cond_1
    iget v5, v0, LX/8vf;->f:I

    move v4, v2

    .line 1418177
    :goto_2
    if-ge v4, v5, :cond_3

    .line 1418178
    iget-object v1, v0, LX/8vf;->e:[Ljava/util/ArrayList;

    aget-object v6, v1, v4

    .line 1418179
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result p0

    move v3, v2

    .line 1418180
    :goto_3
    if-ge v3, p0, :cond_2

    .line 1418181
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1, p1}, Landroid/view/View;->setDrawingCacheBackgroundColor(I)V

    .line 1418182
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_3

    .line 1418183
    :cond_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_2

    .line 1418184
    :cond_3
    iget-object v3, v0, LX/8vf;->d:[Landroid/view/View;

    .line 1418185
    array-length v4, v3

    move v1, v2

    .line 1418186
    :goto_4
    if-ge v1, v4, :cond_5

    .line 1418187
    aget-object v2, v3, v1

    .line 1418188
    if-eqz v2, :cond_4

    .line 1418189
    invoke-virtual {v2, p1}, Landroid/view/View;->setDrawingCacheBackgroundColor(I)V

    .line 1418190
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1418191
    :cond_5
    return-void
.end method

.method public setChoiceMode(I)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/16 v1, 0xb

    .line 1418192
    iput p1, p0, LX/8vj;->b:I

    .line 1418193
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v1, :cond_1

    .line 1418194
    iget-object v0, p0, LX/8vj;->c:Ljava/lang/Object;

    if-eqz v0, :cond_1

    .line 1418195
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v1, :cond_0

    .line 1418196
    iget-object v0, p0, LX/8vj;->c:Ljava/lang/Object;

    check-cast v0, Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    .line 1418197
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/8vj;->c:Ljava/lang/Object;

    .line 1418198
    :cond_1
    iget v0, p0, LX/8vj;->b:I

    if-eqz v0, :cond_4

    .line 1418199
    iget-object v0, p0, LX/8vj;->f:Landroid/util/SparseBooleanArray;

    if-nez v0, :cond_2

    .line 1418200
    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v0, p0, LX/8vj;->f:Landroid/util/SparseBooleanArray;

    .line 1418201
    :cond_2
    iget-object v0, p0, LX/8vj;->g:LX/0tf;

    if-nez v0, :cond_3

    iget-object v0, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1418202
    new-instance v0, LX/0tf;

    invoke-direct {v0}, LX/0tf;-><init>()V

    iput-object v0, p0, LX/8vj;->g:LX/0tf;

    .line 1418203
    :cond_3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v1, :cond_4

    .line 1418204
    iget v0, p0, LX/8vj;->b:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    .line 1418205
    invoke-virtual {p0}, LX/8vj;->a()V

    .line 1418206
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/8vj;->setLongClickable(Z)V

    .line 1418207
    :cond_4
    return-void
.end method

.method public setDrawSelectorOnTop(Z)V
    .locals 0

    .prologue
    .line 1418208
    iput-boolean p1, p0, LX/8vj;->l:Z

    .line 1418209
    return-void
.end method

.method public setFriction(F)V
    .locals 1

    .prologue
    .line 1418210
    iget-object v0, p0, LX/8vj;->av:Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    if-nez v0, :cond_0

    .line 1418211
    new-instance v0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    invoke-direct {v0, p0}, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;-><init>(LX/8vj;)V

    iput-object v0, p0, LX/8vj;->av:Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    .line 1418212
    :cond_0
    iget-object v0, p0, LX/8vj;->av:Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;

    iget-object v0, v0, Lit/sephiroth/android/library/widget/AbsHListView$FlingRunnable;->b:LX/8vs;

    .line 1418213
    iget-object p0, v0, LX/8vs;->b:LX/8vr;

    .line 1418214
    iput p1, p0, LX/8vr;->m:F

    .line 1418215
    iget-object p0, v0, LX/8vs;->c:LX/8vr;

    .line 1418216
    iput p1, p0, LX/8vr;->m:F

    .line 1418217
    return-void
.end method

.method public setMultiChoiceModeListener(LX/8vV;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 1418218
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    .line 1418219
    iget-object v0, p0, LX/8vj;->d:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 1418220
    new-instance v0, LX/8vW;

    invoke-direct {v0, p0}, LX/8vW;-><init>(LX/8vj;)V

    iput-object v0, p0, LX/8vj;->d:Ljava/lang/Object;

    .line 1418221
    :cond_0
    iget-object v0, p0, LX/8vj;->d:Ljava/lang/Object;

    check-cast v0, LX/8vW;

    .line 1418222
    iput-object p1, v0, LX/8vW;->a:LX/8vV;

    .line 1418223
    :goto_0
    return-void

    .line 1418224
    :cond_1
    const-string v0, "AbsListView"

    const-string v1, "setMultiChoiceModeListener not supported for this version of Android"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setOnScrollListener(LX/8ve;)V
    .locals 0

    .prologue
    .line 1418225
    iput-object p1, p0, LX/8vj;->aw:LX/8ve;

    .line 1418226
    invoke-virtual {p0}, LX/8vj;->b()V

    .line 1418227
    return-void
.end method

.method public setOverScrollMode(I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1418228
    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    .line 1418229
    iget-object v0, p0, LX/8vj;->aQ:LX/8vn;

    if-nez v0, :cond_0

    .line 1418230
    invoke-virtual {p0}, LX/8vj;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1418231
    new-instance v1, LX/8vn;

    invoke-direct {v1, v0, v2}, LX/8vn;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, LX/8vj;->aQ:LX/8vn;

    .line 1418232
    new-instance v1, LX/8vn;

    invoke-direct {v1, v0, v2}, LX/8vn;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, LX/8vj;->aR:LX/8vn;

    .line 1418233
    :cond_0
    :goto_0
    invoke-super {p0, p1}, LX/8vi;->setOverScrollMode(I)V

    .line 1418234
    return-void

    .line 1418235
    :cond_1
    iput-object v1, p0, LX/8vj;->aQ:LX/8vn;

    .line 1418236
    iput-object v1, p0, LX/8vj;->aR:LX/8vn;

    goto :goto_0
.end method

.method public setRecyclerListener(LX/8vg;)V
    .locals 1

    .prologue
    .line 1418151
    iget-object v0, p0, LX/8vj;->p:LX/8vf;

    .line 1418152
    iput-object p1, v0, LX/8vf;->b:LX/8vg;

    .line 1418153
    return-void
.end method

.method public setScrollingCacheEnabled(Z)V
    .locals 1

    .prologue
    .line 1418238
    iget-boolean v0, p0, LX/8vj;->L:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 1418239
    invoke-static {p0}, LX/8vj;->B(LX/8vj;)V

    .line 1418240
    :cond_0
    iput-boolean p1, p0, LX/8vj;->L:Z

    .line 1418241
    return-void
.end method

.method public abstract setSelectionInt(I)V
.end method

.method public setSelector(I)V
    .locals 1

    .prologue
    .line 1418242
    invoke-virtual {p0}, LX/8vj;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/8vj;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 1418243
    return-void
.end method

.method public setSelector(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 1418244
    iget-object v0, p0, LX/8vj;->m:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 1418245
    iget-object v0, p0, LX/8vj;->m:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1418246
    iget-object v0, p0, LX/8vj;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, LX/8vj;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1418247
    :cond_0
    iput-object p1, p0, LX/8vj;->m:Landroid/graphics/drawable/Drawable;

    .line 1418248
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 1418249
    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 1418250
    iget v1, v0, Landroid/graphics/Rect;->left:I

    iput v1, p0, LX/8vj;->q:I

    .line 1418251
    iget v1, v0, Landroid/graphics/Rect;->top:I

    iput v1, p0, LX/8vj;->r:I

    .line 1418252
    iget v1, v0, Landroid/graphics/Rect;->right:I

    iput v1, p0, LX/8vj;->s:I

    .line 1418253
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iput v0, p0, LX/8vj;->t:I

    .line 1418254
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1418255
    invoke-direct {p0}, LX/8vj;->v()V

    .line 1418256
    return-void
.end method

.method public setVelocityScale(F)V
    .locals 0

    .prologue
    .line 1418257
    iput p1, p0, LX/8vj;->aO:F

    .line 1418258
    return-void
.end method

.method public final showContextMenuForChild(Landroid/view/View;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1418259
    invoke-virtual {p0, p1}, LX/8vi;->a(Landroid/view/View;)I

    move-result v1

    .line 1418260
    if-ltz v1, :cond_1

    .line 1418261
    iget-object v2, p0, LX/8vj;->j:Landroid/widget/ListAdapter;

    invoke-interface {v2, v1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v2

    .line 1418262
    iget-object v4, p0, LX/8vi;->ai:LX/8vl;

    if-eqz v4, :cond_0

    .line 1418263
    iget-object v0, p0, LX/8vi;->ai:LX/8vl;

    invoke-interface {v0}, LX/8vl;->a()Z

    move-result v0

    .line 1418264
    :cond_0
    if-nez v0, :cond_1

    .line 1418265
    iget v0, p0, LX/8vi;->V:I

    sub-int v0, v1, v0

    invoke-virtual {p0, v0}, LX/8vj;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0, v1, v2, v3}, LX/8vj;->c(Landroid/view/View;IJ)Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v0

    iput-object v0, p0, LX/8vj;->az:Landroid/view/ContextMenu$ContextMenuInfo;

    .line 1418266
    invoke-super {p0, p1}, LX/8vi;->showContextMenuForChild(Landroid/view/View;)Z

    move-result v0

    .line 1418267
    :cond_1
    return v0
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 1418268
    iget-object v0, p0, LX/8vj;->m:Landroid/graphics/drawable/Drawable;

    if-eq v0, p1, :cond_0

    invoke-super {p0, p1}, LX/8vi;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
