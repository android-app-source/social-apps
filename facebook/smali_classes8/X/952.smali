.class public final LX/952;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/contacts/upload/graphql/FetchPhonebookHashesGraphQLModels$FetchPhonebookHashesQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/contacts/upload/ContinuousContactUploadClient;


# direct methods
.method public constructor <init>(Lcom/facebook/contacts/upload/ContinuousContactUploadClient;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1436368
    iput-object p1, p0, LX/952;->c:Lcom/facebook/contacts/upload/ContinuousContactUploadClient;

    iput-object p2, p0, LX/952;->a:Ljava/lang/String;

    iput-object p3, p0, LX/952;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 1436364
    iget-object v0, p0, LX/952;->c:Lcom/facebook/contacts/upload/ContinuousContactUploadClient;

    iget-object v1, v0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->e:LX/2UP;

    iget-object v5, p0, LX/952;->b:Ljava/lang/String;

    iget-object v0, p0, LX/952;->c:Lcom/facebook/contacts/upload/ContinuousContactUploadClient;

    iget-wide v6, v0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->o:J

    iget-object v0, p0, LX/952;->c:Lcom/facebook/contacts/upload/ContinuousContactUploadClient;

    invoke-static {v0}, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->b(Lcom/facebook/contacts/upload/ContinuousContactUploadClient;)J

    move-result-wide v8

    move v3, v2

    move v4, v2

    invoke-virtual/range {v1 .. v9}, LX/2UP;->a(ZZZLjava/lang/String;JJ)V

    .line 1436365
    iget-object v0, p0, LX/952;->c:Lcom/facebook/contacts/upload/ContinuousContactUploadClient;

    iget-object v0, v0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->e:LX/2UP;

    sget-object v1, LX/95K;->SYNC_CHECK_SERVER_RESPONSE_NOT_RECEIVED:LX/95K;

    invoke-virtual {v0, v1}, LX/2UP;->a(LX/95K;)V

    .line 1436366
    iget-object v0, p0, LX/952;->c:Lcom/facebook/contacts/upload/ContinuousContactUploadClient;

    iget-object v0, v0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->e:LX/2UP;

    invoke-virtual {v0}, LX/2UP;->b()V

    .line 1436367
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1436361
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1436362
    iget-object v0, p0, LX/952;->c:Lcom/facebook/contacts/upload/ContinuousContactUploadClient;

    iget-object v1, p0, LX/952;->a:Ljava/lang/String;

    iget-object v2, p0, LX/952;->b:Ljava/lang/String;

    invoke-virtual {v0, p1, v1, v2}, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->a(Lcom/facebook/graphql/executor/GraphQLResult;Ljava/lang/String;Ljava/lang/String;)V

    .line 1436363
    return-void
.end method
