.class public LX/9IY;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/9IW;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9Ia;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1463246
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/9IY;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/9Ia;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1463247
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1463248
    iput-object p1, p0, LX/9IY;->b:LX/0Ot;

    .line 1463249
    return-void
.end method

.method public static a(LX/0QB;)LX/9IY;
    .locals 4

    .prologue
    .line 1463250
    const-class v1, LX/9IY;

    monitor-enter v1

    .line 1463251
    :try_start_0
    sget-object v0, LX/9IY;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1463252
    sput-object v2, LX/9IY;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1463253
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1463254
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1463255
    new-instance v3, LX/9IY;

    const/16 p0, 0x1df0

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/9IY;-><init>(LX/0Ot;)V

    .line 1463256
    move-object v0, v3

    .line 1463257
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1463258
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/9IY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1463259
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1463260
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;LX/9Gn;)LX/1dQ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            "LX/9Gn;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1463261
    const v0, -0x5cbf0416

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 13

    .prologue
    .line 1463262
    check-cast p2, LX/9IX;

    .line 1463263
    iget-object v0, p0, LX/9IY;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Ia;

    iget-object v2, p2, LX/9IX;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    iget-object v3, p2, LX/9IX;->b:LX/5Gs;

    iget v4, p2, LX/9IX;->c:I

    iget-object v5, p2, LX/9IX;->d:Ljava/lang/String;

    iget-object v6, p2, LX/9IX;->e:LX/9FA;

    move-object v1, p1

    const/16 p0, 0x8

    const/4 p2, 0x3

    const/4 v9, 0x0

    const/4 p1, 0x2

    const/4 v10, 0x1

    .line 1463264
    new-instance v7, LX/9Gm;

    invoke-direct {v7, v2, v3}, LX/9Gm;-><init>(Lcom/facebook/graphql/model/GraphQLFeedback;LX/5Gs;)V

    new-instance v8, LX/4Vj;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v8, v11}, LX/4Vj;-><init>(Ljava/lang/String;)V

    invoke-interface {v6, v7, v8}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/9Gn;

    .line 1463265
    iget-boolean v8, v7, LX/9Gn;->a:Z

    move v8, v8

    .line 1463266
    if-eqz v8, :cond_3

    invoke-virtual {v1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v11, 0x7f081019

    invoke-virtual {v8, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1463267
    :goto_0
    iget-object v11, v6, LX/9FA;->f:LX/9Ce;

    move-object v11, v11

    .line 1463268
    if-eqz v11, :cond_0

    .line 1463269
    iget-object v12, v11, LX/9Ce;->a:Ljava/lang/String;

    move-object v12, v12

    .line 1463270
    if-eqz v12, :cond_0

    .line 1463271
    iget-object v12, v11, LX/9Ce;->a:Ljava/lang/String;

    move-object v11, v12

    .line 1463272
    invoke-virtual {v11, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_0

    move v9, v10

    .line 1463273
    :cond_0
    iget-object v11, v0, LX/9Ia;->c:LX/0ad;

    sget-short v12, LX/0wn;->o:S

    invoke-interface {v11, v12, v10}, LX/0ad;->a(SZ)Z

    move-result v11

    if-eqz v11, :cond_5

    iget-object v11, v0, LX/9Ia;->b:LX/0tF;

    invoke-virtual {v11}, LX/0tF;->b()Z

    move-result v11

    if-nez v11, :cond_5

    .line 1463274
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v11

    invoke-interface {v11, p1}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v11

    invoke-interface {v11, p1}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v12

    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v11

    const/16 p0, 0x24

    invoke-interface {v11, p0}, LX/1Dh;->H(I)LX/1Dh;

    move-result-object v11

    const/16 p0, 0x30

    invoke-interface {v11, p0}, LX/1Dh;->K(I)LX/1Dh;

    move-result-object v11

    invoke-interface {v11, p1}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v11

    invoke-interface {v11, v10}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object p0

    .line 1463275
    iget-boolean v11, v7, LX/9Gn;->a:Z

    move v11, v11

    .line 1463276
    if-eqz v11, :cond_4

    invoke-static {v1}, LX/5Jt;->c(LX/1De;)LX/5Jr;

    move-result-object v11

    invoke-virtual {v11}, LX/1X5;->c()LX/1Di;

    move-result-object v11

    const/16 p1, 0x18

    invoke-interface {v11, p1}, LX/1Di;->j(I)LX/1Di;

    move-result-object v11

    const/16 p1, 0x18

    invoke-interface {v11, p1}, LX/1Di;->r(I)LX/1Di;

    move-result-object v11

    invoke-interface {v11}, LX/1Di;->k()LX/1Dg;

    move-result-object v11

    :goto_1
    invoke-interface {p0, v11}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v11

    invoke-interface {v12, v11}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v11

    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v12

    invoke-virtual {v12, v8}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v8

    const v12, 0x7f0b0050

    invoke-virtual {v8, v12}, LX/1ne;->q(I)LX/1ne;

    move-result-object v8

    const v12, 0x7f0a042a

    invoke-virtual {v8, v12}, LX/1ne;->n(I)LX/1ne;

    move-result-object v8

    invoke-virtual {v8, v10}, LX/1ne;->t(I)LX/1ne;

    move-result-object v8

    invoke-virtual {v8}, LX/1X5;->c()LX/1Di;

    move-result-object v8

    const v10, 0x7f0219d6

    invoke-interface {v8, v10}, LX/1Di;->x(I)LX/1Di;

    move-result-object v8

    const/4 v10, 0x4

    invoke-interface {v8, p2, v10}, LX/1Di;->h(II)LX/1Di;

    move-result-object v8

    const/4 v10, 0x6

    const/16 v12, 0xc

    invoke-interface {v8, v10, v12}, LX/1Di;->d(II)LX/1Di;

    move-result-object v8

    invoke-interface {v11, v8}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v8

    .line 1463277
    :goto_2
    if-eqz v9, :cond_1

    .line 1463278
    iget-object v9, v0, LX/9Ia;->a:LX/9Hp;

    invoke-virtual {v9, v1}, LX/9Hp;->c(LX/1De;)LX/9Hn;

    move-result-object v9

    invoke-virtual {v9, v6}, LX/9Hn;->a(LX/9FA;)LX/9Hn;

    move-result-object v9

    invoke-interface {v8, v9}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    .line 1463279
    :cond_1
    iget-boolean v9, v7, LX/9Gn;->a:Z

    move v9, v9

    .line 1463280
    if-nez v9, :cond_2

    .line 1463281
    const v9, -0x5cbf0416

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v7, v10, v11

    invoke-static {v1, v9, v10}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v9

    move-object v7, v9

    .line 1463282
    invoke-interface {v8, v7}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    .line 1463283
    :cond_2
    invoke-interface {v8}, LX/1Di;->k()LX/1Dg;

    move-result-object v7

    move-object v0, v7

    .line 1463284
    return-object v0

    .line 1463285
    :cond_3
    invoke-virtual {v1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-static {v2}, LX/21y;->getOrder(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/21y;

    move-result-object v11

    invoke-static {v8, v11, v3, v4}, LX/9Ii;->a(Landroid/content/res/Resources;LX/21y;LX/5Gs;I)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_0

    .line 1463286
    :cond_4
    invoke-static {v1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v11

    const p1, 0x7f0219d5

    invoke-virtual {v11, p1}, LX/1o5;->h(I)LX/1o5;

    move-result-object v11

    invoke-virtual {v11}, LX/1X5;->c()LX/1Di;

    move-result-object v11

    invoke-interface {v11}, LX/1Di;->k()LX/1Dg;

    move-result-object v11

    goto/16 :goto_1

    .line 1463287
    :cond_5
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v11

    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v12

    invoke-virtual {v12, v8}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v8

    const v12, 0x7f0b0050

    invoke-virtual {v8, v12}, LX/1ne;->q(I)LX/1ne;

    move-result-object v8

    const v12, 0x7f0a008b

    invoke-virtual {v8, v12}, LX/1ne;->n(I)LX/1ne;

    move-result-object v8

    invoke-virtual {v8}, LX/1X5;->c()LX/1Di;

    move-result-object v8

    invoke-interface {v8, v10, p0}, LX/1Di;->h(II)LX/1Di;

    move-result-object v8

    invoke-interface {v8, p2, p0}, LX/1Di;->h(II)LX/1Di;

    move-result-object v8

    invoke-interface {v11, v8}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v8

    goto :goto_2
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 1463288
    invoke-static {}, LX/1dS;->b()V

    .line 1463289
    iget v0, p1, LX/1dQ;->b:I

    .line 1463290
    packed-switch v0, :pswitch_data_0

    .line 1463291
    :goto_0
    return-object v3

    .line 1463292
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1463293
    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    check-cast v0, LX/9Gn;

    iget-object v2, p1, LX/1dQ;->a:LX/1X1;

    .line 1463294
    check-cast v2, LX/9IX;

    .line 1463295
    iget-object v4, p0, LX/9IY;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v5, v2, LX/9IX;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    iget-object v6, v2, LX/9IX;->b:LX/5Gs;

    iget-object v7, v2, LX/9IX;->f:LX/9Fi;

    iget-object p1, v2, LX/9IX;->e:LX/9FA;

    move-object p2, v0

    .line 1463296
    invoke-static {v5, v6, v7, p1, p2}, LX/9Ia;->a(Lcom/facebook/graphql/model/GraphQLFeedback;LX/5Gs;LX/9Fi;LX/9FA;LX/9Gn;)V

    .line 1463297
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x5cbf0416
        :pswitch_0
    .end packed-switch
.end method
