.class public final LX/8jH;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:LX/0Xv;

.field public final synthetic b:LX/8jI;


# direct methods
.method public constructor <init>(LX/8jI;LX/0Xv;)V
    .locals 0

    .prologue
    .line 1392262
    iput-object p1, p0, LX/8jH;->b:LX/8jI;

    iput-object p2, p0, LX/8jH;->a:LX/0Xv;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 4

    .prologue
    .line 1392263
    sget-object v0, LX/8jI;->a:Ljava/lang/Class;

    const-string v1, "Error fetching stickers"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1392264
    iget-object v0, p0, LX/8jH;->a:LX/0Xv;

    invoke-interface {v0}, LX/0Xu;->i()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/SettableFuture;

    .line 1392265
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Failed to fetch sticker"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    goto :goto_0

    .line 1392266
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 1392267
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    const/4 v3, 0x0

    .line 1392268
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/service/FetchStickersResult;

    .line 1392269
    iget-object v1, v0, Lcom/facebook/stickers/service/FetchStickersResult;->a:LX/0Px;

    move-object v4, v1

    .line 1392270
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v3

    :goto_0
    if-ge v2, v5, :cond_1

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/Sticker;

    .line 1392271
    iget-object v1, p0, LX/8jH;->a:LX/0Xv;

    iget-object v6, v0, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-interface {v1, v6}, LX/0Xv;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 1392272
    if-eqz v1, :cond_0

    .line 1392273
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/common/util/concurrent/SettableFuture;

    .line 1392274
    const v7, -0x64452550

    invoke-static {v1, v0, v7}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    goto :goto_1

    .line 1392275
    :cond_0
    iget-object v1, p0, LX/8jH;->a:LX/0Xv;

    iget-object v0, v0, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-interface {v1, v0}, LX/0Xv;->b(Ljava/lang/Object;)Ljava/util/List;

    .line 1392276
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1392277
    :cond_1
    iget-object v0, p0, LX/8jH;->a:LX/0Xv;

    invoke-interface {v0}, LX/0Xu;->n()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1392278
    sget-object v0, LX/8jI;->a:Ljava/lang/Class;

    const-string v1, "did not receive results for stickers: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v4, p0, LX/8jH;->a:LX/0Xv;

    invoke-interface {v4}, LX/0Xu;->p()Ljava/util/Set;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1392279
    :cond_2
    iget-object v0, p0, LX/8jH;->a:LX/0Xv;

    invoke-interface {v0}, LX/0Xu;->i()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/SettableFuture;

    .line 1392280
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Failed to fetch sticker"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    goto :goto_2

    .line 1392281
    :cond_3
    return-void
.end method
