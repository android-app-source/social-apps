.class public final enum LX/8lD;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8lD;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8lD;

.field public static final enum RECENTS:LX/8lD;

.field public static final enum SEARCH:LX/8lD;

.field public static final enum STICKER_PACK:LX/8lD;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1397428
    new-instance v0, LX/8lD;

    const-string v1, "SEARCH"

    invoke-direct {v0, v1, v2}, LX/8lD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8lD;->SEARCH:LX/8lD;

    .line 1397429
    new-instance v0, LX/8lD;

    const-string v1, "RECENTS"

    invoke-direct {v0, v1, v3}, LX/8lD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8lD;->RECENTS:LX/8lD;

    .line 1397430
    new-instance v0, LX/8lD;

    const-string v1, "STICKER_PACK"

    invoke-direct {v0, v1, v4}, LX/8lD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8lD;->STICKER_PACK:LX/8lD;

    .line 1397431
    const/4 v0, 0x3

    new-array v0, v0, [LX/8lD;

    sget-object v1, LX/8lD;->SEARCH:LX/8lD;

    aput-object v1, v0, v2

    sget-object v1, LX/8lD;->RECENTS:LX/8lD;

    aput-object v1, v0, v3

    sget-object v1, LX/8lD;->STICKER_PACK:LX/8lD;

    aput-object v1, v0, v4

    sput-object v0, LX/8lD;->$VALUES:[LX/8lD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1397425
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8lD;
    .locals 1

    .prologue
    .line 1397426
    const-class v0, LX/8lD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8lD;

    return-object v0
.end method

.method public static values()[LX/8lD;
    .locals 1

    .prologue
    .line 1397427
    sget-object v0, LX/8lD;->$VALUES:[LX/8lD;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8lD;

    return-object v0
.end method
