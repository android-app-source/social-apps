.class public final LX/AFI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AEs;


# instance fields
.field public final synthetic a:Ljava/util/Set;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/audience/model/ReplyThread;

.field public final synthetic d:LX/AFK;

.field public final synthetic e:LX/AFJ;


# direct methods
.method public constructor <init>(LX/AFJ;Ljava/util/Set;Ljava/lang/String;Lcom/facebook/audience/model/ReplyThread;LX/AFK;)V
    .locals 0

    .prologue
    .line 1647522
    iput-object p1, p0, LX/AFI;->e:LX/AFJ;

    iput-object p2, p0, LX/AFI;->a:Ljava/util/Set;

    iput-object p3, p0, LX/AFI;->b:Ljava/lang/String;

    iput-object p4, p0, LX/AFI;->c:Lcom/facebook/audience/model/ReplyThread;

    iput-object p5, p0, LX/AFI;->d:LX/AFK;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Map;)V
    .locals 11
    .param p1    # Ljava/util/Map;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/os/Parcel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1647523
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 1647524
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1647525
    iget-object v1, p0, LX/AFI;->a:Ljava/util/Set;

    invoke-static {v1, p1, v6, v0}, LX/AFJ;->b(Ljava/util/Set;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V

    .line 1647526
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v2, v3

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/model/ReplayableSession;

    .line 1647527
    iget-object v1, v0, Lcom/facebook/audience/direct/model/ReplayableSession;->b:LX/0Px;

    move-object v8, v1

    .line 1647528
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v5, v3

    :goto_1
    if-ge v5, v9, :cond_0

    invoke-virtual {v8, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1647529
    iget-object v10, p0, LX/AFI;->b:Ljava/lang/String;

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1647530
    iget-object v1, p0, LX/AFI;->b:Ljava/lang/String;

    invoke-static {v1, v6, v0}, LX/AFJ;->b(Ljava/lang/String;Ljava/util/Map;Lcom/facebook/audience/direct/model/ReplayableSession;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v2, v4

    .line 1647531
    goto :goto_0

    .line 1647532
    :cond_1
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_1

    .line 1647533
    :cond_2
    iget-object v0, p0, LX/AFI;->c:Lcom/facebook/audience/model/ReplyThread;

    invoke-static {v0}, Lcom/facebook/audience/model/ReplyThread;->a(Lcom/facebook/audience/model/ReplyThread;)LX/7gs;

    move-result-object v0

    if-eqz v2, :cond_4

    iget-object v1, p0, LX/AFI;->c:Lcom/facebook/audience/model/ReplyThread;

    .line 1647534
    iget-boolean v2, v1, Lcom/facebook/audience/model/ReplyThread;->c:Z

    move v1, v2

    .line 1647535
    if-eqz v1, :cond_4

    .line 1647536
    :goto_2
    iput-boolean v4, v0, LX/7gs;->b:Z

    .line 1647537
    move-object v0, v0

    .line 1647538
    invoke-virtual {v0}, LX/7gs;->a()Lcom/facebook/audience/model/ReplyThread;

    move-result-object v0

    .line 1647539
    iget-object v1, p0, LX/AFI;->d:LX/AFK;

    .line 1647540
    iget-object v2, v1, LX/AFK;->a:LX/AFL;

    iget-object v2, v2, LX/AFL;->a:LX/AFM;

    iget-object v2, v2, LX/AFM;->a:LX/AFO;

    .line 1647541
    iget-object v3, v0, Lcom/facebook/audience/model/ReplyThread;->e:LX/0Px;

    move-object v5, v3

    .line 1647542
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v3, 0x0

    move v4, v3

    :goto_3
    if-ge v4, v6, :cond_3

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/audience/model/Reply;

    .line 1647543
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_3

    .line 1647544
    :cond_3
    iget-object v3, v2, LX/AFO;->e:LX/7gh;

    invoke-virtual {v3}, LX/7gh;->a()Ljava/util/List;

    move-result-object v3

    .line 1647545
    iget-object v4, v2, LX/AFO;->f:Ljava/util/concurrent/ExecutorService;

    new-instance v5, Lcom/facebook/audience/direct/data/ReplyDataProvider$3;

    invoke-direct {v5, v2, v3, v0}, Lcom/facebook/audience/direct/data/ReplyDataProvider$3;-><init>(LX/AFO;Ljava/util/List;Lcom/facebook/audience/model/ReplyThread;)V

    const v3, -0x79418120

    invoke-static {v4, v5, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1647546
    return-void

    :cond_4
    move v4, v3

    .line 1647547
    goto :goto_2
.end method
