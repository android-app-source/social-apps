.class public final LX/9DB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9DA;


# instance fields
.field public final synthetic a:LX/9DG;


# direct methods
.method public constructor <init>(LX/9DG;)V
    .locals 0

    .prologue
    .line 1454809
    iput-object p1, p0, LX/9DB;->a:LX/9DG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 1454815
    iget-object v0, p0, LX/9DB;->a:LX/9DG;

    invoke-static {v0}, LX/9DG;->n(LX/9DG;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1454816
    iget-object v0, p0, LX/9DB;->a:LX/9DG;

    iget-object v0, v0, LX/9DG;->D:LX/9F7;

    invoke-virtual {v0, p1}, LX/9F7;->a(I)V

    .line 1454817
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 2

    .prologue
    .line 1454810
    iget-object v0, p0, LX/9DB;->a:LX/9DG;

    iget-object v0, v0, LX/9DG;->j:LX/9Do;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9DB;->a:LX/9DG;

    iget-object v0, v0, LX/9DG;->b:LX/9CC;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9DB;->a:LX/9DG;

    iget-boolean v0, v0, LX/9DG;->z:Z

    if-eqz v0, :cond_0

    .line 1454811
    iget-object v0, p0, LX/9DB;->a:LX/9DG;

    iget-object v0, v0, LX/9DG;->j:LX/9Do;

    invoke-virtual {v0, p1}, LX/9Do;->a(Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 1454812
    iget-object v0, p0, LX/9DB;->a:LX/9DG;

    iget-object v0, v0, LX/9DG;->b:LX/9CC;

    invoke-virtual {v0, p1}, LX/9CC;->a(Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 1454813
    iget-object v0, p0, LX/9DB;->a:LX/9DG;

    iget-object v0, v0, LX/9DG;->C:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3RX;

    const-string v1, "live_comment"

    invoke-virtual {v0, v1}, LX/3RX;->a(Ljava/lang/String;)V

    .line 1454814
    :cond_0
    return-void
.end method
