.class public final LX/94p;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/94q;


# direct methods
.method public constructor <init>(LX/94q;)V
    .locals 0

    .prologue
    .line 1435663
    iput-object p1, p0, LX/94p;->a:LX/94q;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1435664
    iget-object v0, p0, LX/94p;->a:LX/94q;

    const/4 v1, 0x0

    .line 1435665
    iput-object v1, v0, LX/94q;->m:LX/1ML;

    .line 1435666
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 10

    .prologue
    .line 1435667
    sget-object v0, LX/94q;->a:Ljava/lang/String;

    const-string v1, "Contacts upload failed: "

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1435668
    invoke-direct {p0}, LX/94p;->a()V

    .line 1435669
    iget-object v0, p0, LX/94p;->a:LX/94q;

    iget-object v0, v0, LX/94q;->e:LX/0Zb;

    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "contacts_upload_failed"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/94p;->a:LX/94q;

    invoke-static {v2}, LX/94q;->a(LX/94q;)Ljava/lang/String;

    move-result-object v2

    .line 1435670
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1435671
    move-object v1, v1

    .line 1435672
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1435673
    iget-object v0, p0, LX/94p;->a:LX/94q;

    iget-object v0, v0, LX/94q;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/32V;->d:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1435674
    instance-of v0, p1, Lcom/facebook/fbservice/service/ServiceException;

    if-eqz v0, :cond_0

    .line 1435675
    iget-object v0, p0, LX/94p;->a:LX/94q;

    iget-object v1, p0, LX/94p;->a:LX/94q;

    invoke-virtual {v1}, LX/94q;->b()Lcom/facebook/contacts/upload/ContactsUploadState;

    move-result-object v1

    check-cast p1, Lcom/facebook/fbservice/service/ServiceException;

    .line 1435676
    new-instance v3, Lcom/facebook/contacts/upload/ContactsUploadState;

    sget-object v4, LX/94z;->FAILED:LX/94z;

    .line 1435677
    iget v5, v1, Lcom/facebook/contacts/upload/ContactsUploadState;->b:I

    move v5, v5

    .line 1435678
    iget v6, v1, Lcom/facebook/contacts/upload/ContactsUploadState;->c:I

    move v6, v6

    .line 1435679
    iget v7, v1, Lcom/facebook/contacts/upload/ContactsUploadState;->d:I

    move v7, v7

    .line 1435680
    const/4 v8, 0x0

    move-object v9, p1

    invoke-direct/range {v3 .. v9}, Lcom/facebook/contacts/upload/ContactsUploadState;-><init>(LX/94z;IIILcom/facebook/fbservice/service/OperationResult;Lcom/facebook/fbservice/service/ServiceException;)V

    move-object v1, v3

    .line 1435681
    invoke-static {v0, v1}, LX/94q;->a$redex0(LX/94q;Lcom/facebook/contacts/upload/ContactsUploadState;)V

    .line 1435682
    :goto_0
    return-void

    .line 1435683
    :cond_0
    iget-object v0, p0, LX/94p;->a:LX/94q;

    const/4 v8, 0x0

    const/4 v5, 0x0

    .line 1435684
    new-instance v3, Lcom/facebook/contacts/upload/ContactsUploadState;

    sget-object v4, LX/94z;->FAILED:LX/94z;

    move v6, v5

    move v7, v5

    move-object v9, v8

    invoke-direct/range {v3 .. v9}, Lcom/facebook/contacts/upload/ContactsUploadState;-><init>(LX/94z;IIILcom/facebook/fbservice/service/OperationResult;Lcom/facebook/fbservice/service/ServiceException;)V

    move-object v1, v3

    .line 1435685
    invoke-static {v0, v1}, LX/94q;->a$redex0(LX/94q;Lcom/facebook/contacts/upload/ContactsUploadState;)V

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 11

    .prologue
    .line 1435686
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 1435687
    invoke-direct {p0}, LX/94p;->a()V

    .line 1435688
    iget-object v0, p0, LX/94p;->a:LX/94q;

    iget-object v0, v0, LX/94q;->e:LX/0Zb;

    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "contacts_upload_succeeded"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/94p;->a:LX/94q;

    invoke-static {v2}, LX/94q;->a(LX/94q;)Ljava/lang/String;

    move-result-object v2

    .line 1435689
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1435690
    move-object v1, v1

    .line 1435691
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1435692
    iget-object v0, p0, LX/94p;->a:LX/94q;

    iget-object v0, v0, LX/94q;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/32V;->d:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1435693
    iget-object v0, p0, LX/94p;->a:LX/94q;

    iget-object v0, v0, LX/94q;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/32V;->g:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1435694
    iget-object v0, p0, LX/94p;->a:LX/94q;

    iget-object v0, v0, LX/94q;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/32V;->g:LX/0Tn;

    iget-object v2, p0, LX/94p;->a:LX/94q;

    iget-object v2, v2, LX/94q;->h:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1435695
    :cond_0
    iget-object v0, p0, LX/94p;->a:LX/94q;

    invoke-virtual {v0}, LX/94q;->b()Lcom/facebook/contacts/upload/ContactsUploadState;

    move-result-object v0

    .line 1435696
    new-instance v4, Lcom/facebook/contacts/upload/ContactsUploadState;

    sget-object v5, LX/94z;->SUCCEEDED:LX/94z;

    .line 1435697
    iget v6, v0, Lcom/facebook/contacts/upload/ContactsUploadState;->b:I

    move v6, v6

    .line 1435698
    iget v7, v0, Lcom/facebook/contacts/upload/ContactsUploadState;->c:I

    move v7, v7

    .line 1435699
    iget v8, v0, Lcom/facebook/contacts/upload/ContactsUploadState;->d:I

    move v8, v8

    .line 1435700
    const/4 v10, 0x0

    move-object v9, p1

    invoke-direct/range {v4 .. v10}, Lcom/facebook/contacts/upload/ContactsUploadState;-><init>(LX/94z;IIILcom/facebook/fbservice/service/OperationResult;Lcom/facebook/fbservice/service/ServiceException;)V

    move-object v0, v4

    .line 1435701
    iget-object v1, p0, LX/94p;->a:LX/94q;

    invoke-static {v1, v0}, LX/94q;->a$redex0(LX/94q;Lcom/facebook/contacts/upload/ContactsUploadState;)V

    .line 1435702
    return-void
.end method
