.class public LX/8iE;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/8iE;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1391275
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/8iE;
    .locals 3

    .prologue
    .line 1391276
    sget-object v0, LX/8iE;->a:LX/8iE;

    if-nez v0, :cond_1

    .line 1391277
    const-class v1, LX/8iE;

    monitor-enter v1

    .line 1391278
    :try_start_0
    sget-object v0, LX/8iE;->a:LX/8iE;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1391279
    if-eqz v2, :cond_0

    .line 1391280
    :try_start_1
    new-instance v0, LX/8iE;

    invoke-direct {v0}, LX/8iE;-><init>()V

    .line 1391281
    move-object v0, v0

    .line 1391282
    sput-object v0, LX/8iE;->a:LX/8iE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1391283
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1391284
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1391285
    :cond_1
    sget-object v0, LX/8iE;->a:LX/8iE;

    return-object v0

    .line 1391286
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1391287
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/8iA;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v11, -0x1

    .line 1391288
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 1391289
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1391290
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\s+"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 1391291
    new-instance v0, LX/8iD;

    invoke-direct {v0}, LX/8iD;-><init>()V

    invoke-static {v5, v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 1391292
    array-length v6, v5

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v6, :cond_4

    aget-object v7, v5, v2

    .line 1391293
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 1391294
    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8, v4}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/CharSequence;)V

    .line 1391295
    :goto_1
    if-eq v1, v11, :cond_2

    .line 1391296
    if-eqz v1, :cond_0

    add-int/lit8 v0, v1, -0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    const/16 v9, 0x20

    if-ne v0, v9, :cond_5

    :cond_0
    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 1391297
    if-nez v0, :cond_2

    move v0, v1

    .line 1391298
    :goto_3
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v9, v1

    if-ge v0, v9, :cond_1

    .line 1391299
    add-int/lit8 v9, v0, 0x1

    const-string v10, " "

    invoke-virtual {v8, v0, v9, v10}, Ljava/lang/StringBuffer;->replace(IILjava/lang/String;)Ljava/lang/StringBuffer;

    .line 1391300
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 1391301
    :cond_1
    invoke-virtual {v8, v7}, Ljava/lang/StringBuffer;->indexOf(Ljava/lang/String;)I

    move-result v1

    goto :goto_1

    .line 1391302
    :cond_2
    if-eq v1, v11, :cond_3

    .line 1391303
    new-instance v0, LX/8iA;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    invoke-direct {v0, v1, v8}, LX/8iA;-><init>(II)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    .line 1391304
    :goto_4
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v8, v1

    if-ge v0, v8, :cond_3

    .line 1391305
    add-int/lit8 v8, v0, 0x1

    const-string v9, " "

    invoke-virtual {v4, v0, v8, v9}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 1391306
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1391307
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1391308
    :cond_4
    return-object v3

    :cond_5
    const/4 v0, 0x0

    goto :goto_2
.end method

.method private static a(Landroid/text/SpannableString;LX/0Px;II)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/text/SpannableString;",
            "LX/0Px",
            "<",
            "LX/8iB;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 1391309
    invoke-static {p0, p2, p3}, LX/8iE;->a(Ljava/lang/CharSequence;II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1391310
    :cond_0
    return-void

    .line 1391311
    :cond_1
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8iB;

    .line 1391312
    invoke-interface {v0}, LX/8iB;->a()Ljava/lang/Object;

    move-result-object v0

    const/16 v3, 0x11

    invoke-virtual {p0, v0, p2, p3, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1391313
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/CharSequence;II)Z
    .locals 1

    .prologue
    .line 1391314
    :goto_0
    if-ge p1, p2, :cond_1

    .line 1391315
    invoke-interface {p0, p1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1391316
    const/4 v0, 0x0

    .line 1391317
    :goto_1
    return v0

    .line 1391318
    :cond_0
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 1391319
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;LX/0Px;LX/0Px;)Ljava/lang/CharSequence;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "LX/8iB;",
            ">;",
            "LX/0Px",
            "<",
            "LX/8iB;",
            ">;)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .prologue
    .line 1391320
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1391321
    :goto_0
    return-object p1

    .line 1391322
    :cond_0
    new-instance v2, Landroid/text/SpannableString;

    invoke-direct {v2, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1391323
    invoke-static {p1, p2}, LX/8iE;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 1391324
    new-instance v0, LX/8iC;

    invoke-direct {v0, p0}, LX/8iC;-><init>(LX/8iE;)V

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1391325
    const/4 v0, 0x0

    .line 1391326
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8iA;

    .line 1391327
    iget v4, v0, LX/8iA;->a:I

    if-eqz v4, :cond_1

    .line 1391328
    iget v4, v0, LX/8iA;->a:I

    invoke-static {v2, p4, v1, v4}, LX/8iE;->a(Landroid/text/SpannableString;LX/0Px;II)V

    .line 1391329
    iget v4, v0, LX/8iA;->a:I

    sub-int/2addr v4, v1

    add-int/2addr v1, v4

    .line 1391330
    :cond_1
    iget v4, v0, LX/8iA;->a:I

    iget v5, v0, LX/8iA;->a:I

    iget v6, v0, LX/8iA;->b:I

    add-int/2addr v5, v6

    invoke-static {v2, p3, v4, v5}, LX/8iE;->a(Landroid/text/SpannableString;LX/0Px;II)V

    .line 1391331
    iget v0, v0, LX/8iA;->b:I

    add-int/2addr v0, v1

    move v1, v0

    .line 1391332
    goto :goto_1

    .line 1391333
    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-static {v2, p4, v1, v0}, LX/8iE;->a(Landroid/text/SpannableString;LX/0Px;II)V

    move-object p1, v2

    .line 1391334
    goto :goto_0
.end method
