.class public LX/AK2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/AK2;


# instance fields
.field private final a:LX/AKe;

.field public final b:Ljava/util/concurrent/ExecutorService;

.field public final c:LX/7gh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7gh",
            "<",
            "Lcom/facebook/audience/snacks/data/SnacksStoryDetailProvider$SnacksStoryDetailProviderObserver;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/AK1;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/AKe;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1662519
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1662520
    new-instance v0, LX/7gh;

    invoke-direct {v0}, LX/7gh;-><init>()V

    iput-object v0, p0, LX/AK2;->c:LX/7gh;

    .line 1662521
    new-instance v0, LX/AK1;

    invoke-direct {v0, p0}, LX/AK1;-><init>(LX/AK2;)V

    iput-object v0, p0, LX/AK2;->d:LX/AK1;

    .line 1662522
    iput-object p1, p0, LX/AK2;->b:Ljava/util/concurrent/ExecutorService;

    .line 1662523
    iput-object p2, p0, LX/AK2;->a:LX/AKe;

    .line 1662524
    return-void
.end method

.method public static a(LX/0QB;)LX/AK2;
    .locals 8

    .prologue
    .line 1662525
    sget-object v0, LX/AK2;->e:LX/AK2;

    if-nez v0, :cond_1

    .line 1662526
    const-class v1, LX/AK2;

    monitor-enter v1

    .line 1662527
    :try_start_0
    sget-object v0, LX/AK2;->e:LX/AK2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1662528
    if-eqz v2, :cond_0

    .line 1662529
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1662530
    new-instance v5, LX/AK2;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    .line 1662531
    new-instance v7, LX/AKe;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    const/16 p0, 0x12cb

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v7, v4, v6, p0}, LX/AKe;-><init>(LX/0tX;Ljava/util/concurrent/ExecutorService;LX/0Or;)V

    .line 1662532
    move-object v4, v7

    .line 1662533
    check-cast v4, LX/AKe;

    invoke-direct {v5, v3, v4}, LX/AK2;-><init>(Ljava/util/concurrent/ExecutorService;LX/AKe;)V

    .line 1662534
    move-object v0, v5

    .line 1662535
    sput-object v0, LX/AK2;->e:LX/AK2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1662536
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1662537
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1662538
    :cond_1
    sget-object v0, LX/AK2;->e:LX/AK2;

    return-object v0

    .line 1662539
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1662540
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/AK2;ZLjava/lang/String;)V
    .locals 4

    .prologue
    .line 1662541
    iget-object v0, p0, LX/AK2;->a:LX/AKe;

    iget-object v1, p0, LX/AK2;->d:LX/AK1;

    .line 1662542
    if-eqz p1, :cond_0

    sget-object v2, LX/0zS;->d:LX/0zS;

    .line 1662543
    :goto_0
    new-instance v3, LX/AKR;

    invoke-direct {v3}, LX/AKR;-><init>()V

    move-object v3, v3

    .line 1662544
    const-string p0, "0"

    invoke-virtual {v3, p0, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1662545
    const-string p0, "1"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object p1

    invoke-virtual {v3, p0, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 1662546
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v3

    .line 1662547
    iget-object p0, v0, LX/AKe;->c:LX/0tX;

    invoke-virtual {p0, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    move-object v3, v3

    .line 1662548
    new-instance p0, LX/AKd;

    invoke-direct {p0, v0, v1, p2}, LX/AKd;-><init>(LX/AKe;LX/AK1;Ljava/lang/String;)V

    iget-object p1, v0, LX/AKe;->d:Ljava/util/concurrent/ExecutorService;

    invoke-static {v3, p0, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1662549
    return-void

    .line 1662550
    :cond_0
    sget-object v2, LX/0zS;->b:LX/0zS;

    goto :goto_0
.end method
