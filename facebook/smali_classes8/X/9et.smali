.class public final LX/9et;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V
    .locals 0

    .prologue
    .line 1520663
    iput-object p1, p0, LX/9et;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 2

    .prologue
    .line 1520664
    iget-object v0, p0, LX/9et;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->N:LX/9eb;

    invoke-interface {v0}, LX/9ea;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1520665
    iget-object v0, p0, LX/9et;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v1, p0, LX/9et;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    .line 1520666
    iget-object p0, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->o:Ljava/lang/String;

    move-object v1, p0

    .line 1520667
    invoke-virtual {v0, v1}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->a(Ljava/lang/String;)V

    .line 1520668
    :goto_0
    return-void

    .line 1520669
    :cond_0
    iget-object v0, p0, LX/9et;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-boolean v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->j:Z

    if-nez v0, :cond_1

    iget-object v0, p0, LX/9et;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->N:LX/9eb;

    invoke-interface {v0}, LX/9eb;->l()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1520670
    :cond_1
    iget-object v0, p0, LX/9et;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v1, p0, LX/9et;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    .line 1520671
    iget-object p1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->o:Ljava/lang/String;

    move-object v1, p1

    .line 1520672
    invoke-virtual {v0, v1}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->a(Ljava/lang/String;)V

    .line 1520673
    iget-object v0, p0, LX/9et;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    invoke-static {v0}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->u$redex0(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V

    goto :goto_0

    .line 1520674
    :cond_2
    iget-object v0, p0, LX/9et;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->b(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;Z)V

    goto :goto_0
.end method
