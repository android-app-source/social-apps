.class public final LX/AAj;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1638450
    const-class v1, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;

    const v0, 0x226bde9e

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x3

    const-string v5, "BoostedComponentDataQuery"

    const-string v6, "6a5a11a253573793c7da139cf6d76f29"

    const-string v7, "page"

    const-string v8, "10155261854851729"

    const/4 v9, 0x0

    const-string v0, "short_term_cache_key_pyml"

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v10

    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1638451
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1638452
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1638453
    sparse-switch v0, :sswitch_data_0

    .line 1638454
    :goto_0
    return-object p1

    .line 1638455
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1638456
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1638457
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1638458
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1638459
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1638460
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 1638461
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 1638462
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 1638463
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 1638464
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 1638465
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 1638466
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 1638467
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 1638468
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 1638469
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 1638470
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    .line 1638471
    :sswitch_10
    const-string p1, "16"

    goto :goto_0

    .line 1638472
    :sswitch_11
    const-string p1, "17"

    goto :goto_0

    .line 1638473
    :sswitch_12
    const-string p1, "18"

    goto :goto_0

    .line 1638474
    :sswitch_13
    const-string p1, "19"

    goto :goto_0

    .line 1638475
    :sswitch_14
    const-string p1, "20"

    goto :goto_0

    .line 1638476
    :sswitch_15
    const-string p1, "21"

    goto :goto_0

    .line 1638477
    :sswitch_16
    const-string p1, "22"

    goto :goto_0

    .line 1638478
    :sswitch_17
    const-string p1, "23"

    goto :goto_0

    .line 1638479
    :sswitch_18
    const-string p1, "24"

    goto :goto_0

    .line 1638480
    :sswitch_19
    const-string p1, "25"

    goto :goto_0

    .line 1638481
    :sswitch_1a
    const-string p1, "26"

    goto :goto_0

    .line 1638482
    :sswitch_1b
    const-string p1, "27"

    goto :goto_0

    .line 1638483
    :sswitch_1c
    const-string p1, "28"

    goto :goto_0

    .line 1638484
    :sswitch_1d
    const-string p1, "29"

    goto :goto_0

    .line 1638485
    :sswitch_1e
    const-string p1, "30"

    goto :goto_0

    .line 1638486
    :sswitch_1f
    const-string p1, "31"

    goto :goto_0

    .line 1638487
    :sswitch_20
    const-string p1, "32"

    goto :goto_0

    .line 1638488
    :sswitch_21
    const-string p1, "33"

    goto :goto_0

    .line 1638489
    :sswitch_22
    const-string p1, "34"

    goto :goto_0

    .line 1638490
    :sswitch_23
    const-string p1, "35"

    goto :goto_0

    .line 1638491
    :sswitch_24
    const-string p1, "36"

    goto :goto_0

    .line 1638492
    :sswitch_25
    const-string p1, "37"

    goto :goto_0

    .line 1638493
    :sswitch_26
    const-string p1, "38"

    goto :goto_0

    .line 1638494
    :sswitch_27
    const-string p1, "39"

    goto :goto_0

    .line 1638495
    :sswitch_28
    const-string p1, "40"

    goto :goto_0

    .line 1638496
    :sswitch_29
    const-string p1, "41"

    goto :goto_0

    .line 1638497
    :sswitch_2a
    const-string p1, "42"

    goto/16 :goto_0

    .line 1638498
    :sswitch_2b
    const-string p1, "43"

    goto/16 :goto_0

    .line 1638499
    :sswitch_2c
    const-string p1, "44"

    goto/16 :goto_0

    .line 1638500
    :sswitch_2d
    const-string p1, "45"

    goto/16 :goto_0

    .line 1638501
    :sswitch_2e
    const-string p1, "46"

    goto/16 :goto_0

    .line 1638502
    :sswitch_2f
    const-string p1, "47"

    goto/16 :goto_0

    .line 1638503
    :sswitch_30
    const-string p1, "48"

    goto/16 :goto_0

    .line 1638504
    :sswitch_31
    const-string p1, "49"

    goto/16 :goto_0

    .line 1638505
    :sswitch_32
    const-string p1, "50"

    goto/16 :goto_0

    .line 1638506
    :sswitch_33
    const-string p1, "51"

    goto/16 :goto_0

    .line 1638507
    :sswitch_34
    const-string p1, "52"

    goto/16 :goto_0

    .line 1638508
    :sswitch_35
    const-string p1, "53"

    goto/16 :goto_0

    .line 1638509
    :sswitch_36
    const-string p1, "54"

    goto/16 :goto_0

    .line 1638510
    :sswitch_37
    const-string p1, "55"

    goto/16 :goto_0

    .line 1638511
    :sswitch_38
    const-string p1, "56"

    goto/16 :goto_0

    .line 1638512
    :sswitch_39
    const-string p1, "57"

    goto/16 :goto_0

    .line 1638513
    :sswitch_3a
    const-string p1, "58"

    goto/16 :goto_0

    .line 1638514
    :sswitch_3b
    const-string p1, "59"

    goto/16 :goto_0

    .line 1638515
    :sswitch_3c
    const-string p1, "60"

    goto/16 :goto_0

    .line 1638516
    :sswitch_3d
    const-string p1, "61"

    goto/16 :goto_0

    .line 1638517
    :sswitch_3e
    const-string p1, "62"

    goto/16 :goto_0

    .line 1638518
    :sswitch_3f
    const-string p1, "63"

    goto/16 :goto_0

    .line 1638519
    :sswitch_40
    const-string p1, "64"

    goto/16 :goto_0

    .line 1638520
    :sswitch_41
    const-string p1, "65"

    goto/16 :goto_0

    .line 1638521
    :sswitch_42
    const-string p1, "66"

    goto/16 :goto_0

    .line 1638522
    :sswitch_43
    const-string p1, "67"

    goto/16 :goto_0

    .line 1638523
    :sswitch_44
    const-string p1, "68"

    goto/16 :goto_0

    .line 1638524
    :sswitch_45
    const-string p1, "69"

    goto/16 :goto_0

    .line 1638525
    :sswitch_46
    const-string p1, "70"

    goto/16 :goto_0

    .line 1638526
    :sswitch_47
    const-string p1, "71"

    goto/16 :goto_0

    .line 1638527
    :sswitch_48
    const-string p1, "72"

    goto/16 :goto_0

    .line 1638528
    :sswitch_49
    const-string p1, "73"

    goto/16 :goto_0

    .line 1638529
    :sswitch_4a
    const-string p1, "74"

    goto/16 :goto_0

    .line 1638530
    :sswitch_4b
    const-string p1, "75"

    goto/16 :goto_0

    .line 1638531
    :sswitch_4c
    const-string p1, "76"

    goto/16 :goto_0

    .line 1638532
    :sswitch_4d
    const-string p1, "77"

    goto/16 :goto_0

    .line 1638533
    :sswitch_4e
    const-string p1, "78"

    goto/16 :goto_0

    .line 1638534
    :sswitch_4f
    const-string p1, "79"

    goto/16 :goto_0

    .line 1638535
    :sswitch_50
    const-string p1, "80"

    goto/16 :goto_0

    .line 1638536
    :sswitch_51
    const-string p1, "81"

    goto/16 :goto_0

    .line 1638537
    :sswitch_52
    const-string p1, "82"

    goto/16 :goto_0

    .line 1638538
    :sswitch_53
    const-string p1, "83"

    goto/16 :goto_0

    .line 1638539
    :sswitch_54
    const-string p1, "84"

    goto/16 :goto_0

    .line 1638540
    :sswitch_55
    const-string p1, "85"

    goto/16 :goto_0

    .line 1638541
    :sswitch_56
    const-string p1, "86"

    goto/16 :goto_0

    .line 1638542
    :sswitch_57
    const-string p1, "87"

    goto/16 :goto_0

    .line 1638543
    :sswitch_58
    const-string p1, "88"

    goto/16 :goto_0

    .line 1638544
    :sswitch_59
    const-string p1, "89"

    goto/16 :goto_0

    .line 1638545
    :sswitch_5a
    const-string p1, "90"

    goto/16 :goto_0

    .line 1638546
    :sswitch_5b
    const-string p1, "91"

    goto/16 :goto_0

    .line 1638547
    :sswitch_5c
    const-string p1, "92"

    goto/16 :goto_0

    .line 1638548
    :sswitch_5d
    const-string p1, "93"

    goto/16 :goto_0

    .line 1638549
    :sswitch_5e
    const-string p1, "94"

    goto/16 :goto_0

    .line 1638550
    :sswitch_5f
    const-string p1, "95"

    goto/16 :goto_0

    .line 1638551
    :sswitch_60
    const-string p1, "96"

    goto/16 :goto_0

    .line 1638552
    :sswitch_61
    const-string p1, "97"

    goto/16 :goto_0

    .line 1638553
    :sswitch_62
    const-string p1, "98"

    goto/16 :goto_0

    .line 1638554
    :sswitch_63
    const-string p1, "99"

    goto/16 :goto_0

    .line 1638555
    :sswitch_64
    const-string p1, "100"

    goto/16 :goto_0

    .line 1638556
    :sswitch_65
    const-string p1, "101"

    goto/16 :goto_0

    .line 1638557
    :sswitch_66
    const-string p1, "102"

    goto/16 :goto_0

    .line 1638558
    :sswitch_67
    const-string p1, "103"

    goto/16 :goto_0

    .line 1638559
    :sswitch_68
    const-string p1, "104"

    goto/16 :goto_0

    .line 1638560
    :sswitch_69
    const-string p1, "105"

    goto/16 :goto_0

    .line 1638561
    :sswitch_6a
    const-string p1, "106"

    goto/16 :goto_0

    .line 1638562
    :sswitch_6b
    const-string p1, "107"

    goto/16 :goto_0

    .line 1638563
    :sswitch_6c
    const-string p1, "108"

    goto/16 :goto_0

    .line 1638564
    :sswitch_6d
    const-string p1, "109"

    goto/16 :goto_0

    .line 1638565
    :sswitch_6e
    const-string p1, "110"

    goto/16 :goto_0

    .line 1638566
    :sswitch_6f
    const-string p1, "111"

    goto/16 :goto_0

    .line 1638567
    :sswitch_70
    const-string p1, "112"

    goto/16 :goto_0

    .line 1638568
    :sswitch_71
    const-string p1, "113"

    goto/16 :goto_0

    .line 1638569
    :sswitch_72
    const-string p1, "114"

    goto/16 :goto_0

    .line 1638570
    :sswitch_73
    const-string p1, "115"

    goto/16 :goto_0

    .line 1638571
    :sswitch_74
    const-string p1, "116"

    goto/16 :goto_0

    .line 1638572
    :sswitch_75
    const-string p1, "117"

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x7f566515 -> :sswitch_34
        -0x7e998586 -> :sswitch_e
        -0x7b752021 -> :sswitch_2
        -0x76bbb26c -> :sswitch_1e
        -0x7531a756 -> :sswitch_27
        -0x7354b836 -> :sswitch_72
        -0x6e3ba572 -> :sswitch_13
        -0x6c1bed17 -> :sswitch_6c
        -0x6a24640d -> :sswitch_58
        -0x6a02a4f4 -> :sswitch_3f
        -0x69f19a9a -> :sswitch_1
        -0x680de62a -> :sswitch_2d
        -0x6326fdb3 -> :sswitch_2a
        -0x626f1062 -> :sswitch_10
        -0x5e743804 -> :sswitch_b
        -0x58c946c7 -> :sswitch_73
        -0x57984ae8 -> :sswitch_4d
        -0x5709d77d -> :sswitch_60
        -0x55ff6f9b -> :sswitch_31
        -0x5349037c -> :sswitch_3c
        -0x51484e72 -> :sswitch_1d
        -0x513764de -> :sswitch_59
        -0x50cab1c8 -> :sswitch_7
        -0x504c617a -> :sswitch_6f
        -0x4f1f32b4 -> :sswitch_65
        -0x4eea3afb -> :sswitch_a
        -0x4aeac1fa -> :sswitch_66
        -0x4ae70342 -> :sswitch_8
        -0x48fcb87a -> :sswitch_74
        -0x46beb116 -> :sswitch_24
        -0x4651e685 -> :sswitch_35
        -0x4496acc9 -> :sswitch_2e
        -0x438a99f9 -> :sswitch_39
        -0x41a91745 -> :sswitch_4b
        -0x41143822 -> :sswitch_1c
        -0x3c54de38 -> :sswitch_33
        -0x3b85b241 -> :sswitch_5e
        -0x39e54905 -> :sswitch_48
        -0x35995a97 -> :sswitch_57
        -0x30b65c8f -> :sswitch_21
        -0x309c8f4f -> :sswitch_5b
        -0x2fe52f35 -> :sswitch_46
        -0x2fab0379 -> :sswitch_5d
        -0x2f1c601a -> :sswitch_28
        -0x28e3f038 -> :sswitch_3b
        -0x25a646c8 -> :sswitch_20
        -0x2511c384 -> :sswitch_32
        -0x24e1906f -> :sswitch_3
        -0x2177e47b -> :sswitch_22
        -0x210fd44e -> :sswitch_25
        -0x201d08e7 -> :sswitch_44
        -0x1d6ce0bf -> :sswitch_42
        -0x1b87b280 -> :sswitch_29
        -0x197a9d8e -> :sswitch_37
        -0x17e48248 -> :sswitch_4
        -0x15db59af -> :sswitch_6e
        -0x1536bbc0 -> :sswitch_54
        -0x14283bca -> :sswitch_43
        -0x12efdeb3 -> :sswitch_2f
        -0xf5ea1c3 -> :sswitch_70
        -0xebb8fc7 -> :sswitch_55
        -0x8cf3c8a -> :sswitch_19
        -0x8ca6426 -> :sswitch_6
        -0x6fe61e8 -> :sswitch_67
        -0x587d3fa -> :sswitch_2b
        -0x3e446ed -> :sswitch_23
        -0x12603b3 -> :sswitch_53
        0x180aba4 -> :sswitch_1b
        0x23413c3 -> :sswitch_3e
        0x4cb0e44 -> :sswitch_71
        0xa1fa812 -> :sswitch_12
        0xae3442d -> :sswitch_45
        0xc168ff8 -> :sswitch_9
        0xc830c19 -> :sswitch_5f
        0xe50e2a0 -> :sswitch_69
        0x11850e88 -> :sswitch_4f
        0x15888c51 -> :sswitch_68
        0x18ce3dbb -> :sswitch_d
        0x214100e0 -> :sswitch_30
        0x2292beef -> :sswitch_56
        0x244e76e6 -> :sswitch_52
        0x26d0c0ff -> :sswitch_4e
        0x27208b4a -> :sswitch_50
        0x291d8de0 -> :sswitch_4c
        0x2f8b060e -> :sswitch_62
        0x3052e0ff -> :sswitch_15
        0x326dc744 -> :sswitch_4a
        0x32e05a5f -> :sswitch_75
        0x34e16755 -> :sswitch_0
        0x367c1936 -> :sswitch_17
        0x3a26ea04 -> :sswitch_16
        0x410878b1 -> :sswitch_47
        0x420eb51c -> :sswitch_14
        0x427d4f1d -> :sswitch_26
        0x43ee5105 -> :sswitch_61
        0x44431ea4 -> :sswitch_f
        0x52da44cc -> :sswitch_36
        0x54ace343 -> :sswitch_51
        0x54df6484 -> :sswitch_c
        0x5aa53d79 -> :sswitch_41
        0x5e7957c4 -> :sswitch_1f
        0x5eacdfdf -> :sswitch_6b
        0x5f424068 -> :sswitch_1a
        0x5f559782 -> :sswitch_64
        0x6109525b -> :sswitch_38
        0x63c03b07 -> :sswitch_2c
        0x65baffdf -> :sswitch_18
        0x6662d8a5 -> :sswitch_5a
        0x670b906a -> :sswitch_6a
        0x6771e9f5 -> :sswitch_3d
        0x687cca6b -> :sswitch_49
        0x70f836af -> :sswitch_6d
        0x72068209 -> :sswitch_11
        0x732c0b14 -> :sswitch_63
        0x73a026b5 -> :sswitch_40
        0x7506f93c -> :sswitch_5c
        0x7c6b80b3 -> :sswitch_5
        0x7cf1f032 -> :sswitch_3a
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 7

    .prologue
    const/16 v5, 0xa

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 1638573
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 1638574
    :goto_1
    return v0

    .line 1638575
    :sswitch_0
    const-string v6, "2"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    move v1, v0

    goto :goto_0

    :sswitch_1
    const-string v6, "104"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    move v1, v2

    goto :goto_0

    :sswitch_2
    const-string v6, "21"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    move v1, v3

    goto :goto_0

    :sswitch_3
    const-string v6, "117"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    :sswitch_4
    const-string v6, "31"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    move v1, v4

    goto :goto_0

    :sswitch_5
    const-string v6, "14"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v1, 0x5

    goto :goto_0

    :sswitch_6
    const-string v6, "33"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v1, 0x6

    goto :goto_0

    :sswitch_7
    const-string v6, "13"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v1, 0x7

    goto :goto_0

    :sswitch_8
    const-string v6, "36"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v1, 0x8

    goto :goto_0

    :sswitch_9
    const-string v6, "113"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v1, 0x9

    goto :goto_0

    :sswitch_a
    const-string v6, "9"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    move v1, v5

    goto :goto_0

    :sswitch_b
    const-string v6, "1"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v1, 0xb

    goto :goto_0

    :sswitch_c
    const-string v6, "50"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v1, 0xc

    goto/16 :goto_0

    :sswitch_d
    const-string v6, "114"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v1, 0xd

    goto/16 :goto_0

    :sswitch_e
    const-string v6, "74"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v1, 0xe

    goto/16 :goto_0

    :sswitch_f
    const-string v6, "52"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v1, 0xf

    goto/16 :goto_0

    :sswitch_10
    const-string v6, "112"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v1, 0x10

    goto/16 :goto_0

    :sswitch_11
    const-string v6, "0"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v1, 0x11

    goto/16 :goto_0

    :sswitch_12
    const-string v6, "59"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v1, 0x12

    goto/16 :goto_0

    :sswitch_13
    const-string v6, "61"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v1, 0x13

    goto/16 :goto_0

    :sswitch_14
    const-string v6, "12"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v1, 0x14

    goto/16 :goto_0

    :sswitch_15
    const-string v6, "10"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v1, 0x15

    goto/16 :goto_0

    :sswitch_16
    const-string v6, "4"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v1, 0x16

    goto/16 :goto_0

    :sswitch_17
    const-string v6, "11"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v1, 0x17

    goto/16 :goto_0

    :sswitch_18
    const-string v6, "3"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v1, 0x18

    goto/16 :goto_0

    :sswitch_19
    const-string v6, "101"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v1, 0x19

    goto/16 :goto_0

    :sswitch_1a
    const-string v6, "100"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v1, 0x1a

    goto/16 :goto_0

    :sswitch_1b
    const-string v6, "116"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v1, 0x1b

    goto/16 :goto_0

    :sswitch_1c
    const-string v6, "89"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v1, 0x1c

    goto/16 :goto_0

    :sswitch_1d
    const-string v6, "110"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v1, 0x1d

    goto/16 :goto_0

    :sswitch_1e
    const-string v6, "8"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v1, 0x1e

    goto/16 :goto_0

    :sswitch_1f
    const-string v6, "7"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v1, 0x1f

    goto/16 :goto_0

    :sswitch_20
    const-string v6, "6"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v1, 0x20

    goto/16 :goto_0

    :sswitch_21
    const-string v6, "5"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v1, 0x21

    goto/16 :goto_0

    :sswitch_22
    const-string v6, "93"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v1, 0x22

    goto/16 :goto_0

    :sswitch_23
    const-string v6, "94"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v1, 0x23

    goto/16 :goto_0

    :sswitch_24
    const-string v6, "77"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v1, 0x24

    goto/16 :goto_0

    .line 1638576
    :pswitch_0
    const-string v0, "%s"

    invoke-static {p2, v2, v0}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 1638577
    :pswitch_1
    const-string v0, "image/x-auto"

    invoke-static {p2, v0}, LX/0wE;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 1638578
    :pswitch_2
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 1638579
    :pswitch_3
    const-string v0, "%s"

    invoke-static {p2, v5, v0}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 1638580
    :pswitch_4
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 1638581
    :pswitch_5
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 1638582
    :pswitch_6
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 1638583
    :pswitch_7
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 1638584
    :pswitch_8
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 1638585
    :pswitch_9
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 1638586
    :pswitch_a
    const-string v0, "%s"

    invoke-static {p2, v2, v0}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 1638587
    :pswitch_b
    const-string v0, "%s"

    invoke-static {p2, v2, v0}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 1638588
    :pswitch_c
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 1638589
    :pswitch_d
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 1638590
    :pswitch_e
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 1638591
    :pswitch_f
    const-string v0, "undefined"

    invoke-static {p2, v0}, LX/0wE;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 1638592
    :pswitch_10
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 1638593
    :pswitch_11
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 1638594
    :pswitch_12
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 1638595
    :pswitch_13
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 1638596
    :pswitch_14
    const-string v0, "%s"

    invoke-static {p2, v2, v0}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 1638597
    :pswitch_15
    const-string v0, "FUSE_BIG"

    invoke-static {p2, v0}, LX/0wE;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 1638598
    :pswitch_16
    const-string v0, "undefined"

    invoke-static {p2, v0}, LX/0wE;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 1638599
    :pswitch_17
    const-string v0, "%s"

    invoke-static {p2, v4, v0}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 1638600
    :pswitch_18
    const-string v0, "%s"

    invoke-static {p2, v3, v0}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 1638601
    :pswitch_19
    const-string v0, "%s"

    invoke-static {p2, v2, v0}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 1638602
    :pswitch_1a
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 1638603
    :pswitch_1b
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 1638604
    :pswitch_1c
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 1638605
    :pswitch_1d
    const-string v0, "mobile"

    invoke-static {p2, v0}, LX/0wE;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 1638606
    :pswitch_1e
    const-string v0, "%s"

    invoke-static {p2, v2, v0}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 1638607
    :pswitch_1f
    const-string v0, "%s"

    invoke-static {p2, v2, v0}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 1638608
    :pswitch_20
    const/16 v0, 0x78

    const-string v1, "%s"

    invoke-static {p2, v0, v1}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 1638609
    :pswitch_21
    const/16 v0, 0x5a

    const-string v1, "%s"

    invoke-static {p2, v0, v1}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 1638610
    :pswitch_22
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 1638611
    :pswitch_23
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 1638612
    :pswitch_24
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x30 -> :sswitch_11
        0x31 -> :sswitch_b
        0x32 -> :sswitch_0
        0x33 -> :sswitch_18
        0x34 -> :sswitch_16
        0x35 -> :sswitch_21
        0x36 -> :sswitch_20
        0x37 -> :sswitch_1f
        0x38 -> :sswitch_1e
        0x39 -> :sswitch_a
        0x61f -> :sswitch_15
        0x620 -> :sswitch_17
        0x621 -> :sswitch_14
        0x622 -> :sswitch_7
        0x623 -> :sswitch_5
        0x63f -> :sswitch_2
        0x65e -> :sswitch_4
        0x660 -> :sswitch_6
        0x663 -> :sswitch_8
        0x69b -> :sswitch_c
        0x69d -> :sswitch_f
        0x6a4 -> :sswitch_12
        0x6bb -> :sswitch_13
        0x6dd -> :sswitch_e
        0x6e0 -> :sswitch_24
        0x701 -> :sswitch_1c
        0x71a -> :sswitch_22
        0x71b -> :sswitch_23
        0xbdf1 -> :sswitch_1a
        0xbdf2 -> :sswitch_19
        0xbdf5 -> :sswitch_1
        0xbe10 -> :sswitch_1d
        0xbe12 -> :sswitch_10
        0xbe13 -> :sswitch_9
        0xbe14 -> :sswitch_d
        0xbe16 -> :sswitch_1b
        0xbe17 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
    .end packed-switch
.end method
