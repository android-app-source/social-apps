.class public final LX/91N;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/91P;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/91O;

.field private b:[Ljava/lang/String;

.field private c:I

.field private d:Ljava/util/BitSet;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1430602
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1430603
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "minutiaeObject"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/91N;->b:[Ljava/lang/String;

    .line 1430604
    iput v3, p0, LX/91N;->c:I

    .line 1430605
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/91N;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/91N;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/91N;LX/1De;IILX/91O;)V
    .locals 1

    .prologue
    .line 1430606
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1430607
    iput-object p4, p0, LX/91N;->a:LX/91O;

    .line 1430608
    iget-object v0, p0, LX/91N;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1430609
    return-void
.end method


# virtual methods
.method public final a(LX/903;)LX/91N;
    .locals 1

    .prologue
    .line 1430610
    iget-object v0, p0, LX/91N;->a:LX/91O;

    iput-object p1, v0, LX/91O;->b:LX/903;

    .line 1430611
    return-object p0
.end method

.method public final a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)LX/91N;
    .locals 2

    .prologue
    .line 1430612
    iget-object v0, p0, LX/91N;->a:LX/91O;

    iput-object p1, v0, LX/91O;->a:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1430613
    iget-object v0, p0, LX/91N;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1430614
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1430615
    invoke-super {p0}, LX/1X5;->a()V

    .line 1430616
    const/4 v0, 0x0

    iput-object v0, p0, LX/91N;->a:LX/91O;

    .line 1430617
    sget-object v0, LX/91P;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1430618
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/91P;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1430619
    iget-object v1, p0, LX/91N;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/91N;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/91N;->c:I

    if-ge v1, v2, :cond_2

    .line 1430620
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1430621
    :goto_0
    iget v2, p0, LX/91N;->c:I

    if-ge v0, v2, :cond_1

    .line 1430622
    iget-object v2, p0, LX/91N;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1430623
    iget-object v2, p0, LX/91N;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1430624
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1430625
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1430626
    :cond_2
    iget-object v0, p0, LX/91N;->a:LX/91O;

    .line 1430627
    invoke-virtual {p0}, LX/91N;->a()V

    .line 1430628
    return-object v0
.end method
