.class public final LX/ACb;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/util/List",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;)V
    .locals 0

    .prologue
    .line 1642867
    iput-object p1, p0, LX/ACb;->a:Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1642868
    iget-object v0, p0, LX/ACb;->a:Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;

    iget-object v0, v0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->g:Landroid/widget/TextView;

    const v1, 0x7f080039

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1642869
    iget-object v0, p0, LX/ACb;->a:Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;

    iget-object v0, v0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->h:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1642870
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1642871
    check-cast p1, Ljava/util/List;

    .line 1642872
    iget-object v0, p0, LX/ACb;->a:Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;

    invoke-static {v0, p1}, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->c(Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;Ljava/util/List;)V

    .line 1642873
    iget-object v0, p0, LX/ACb;->a:Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->a$redex0(Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;Z)V

    .line 1642874
    iget-object v0, p0, LX/ACb;->a:Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;

    iget-object v0, v0, Lcom/facebook/adinterfaces/ui/selector/BaseTargetingSelectorFragment;->h:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1642875
    return-void
.end method
