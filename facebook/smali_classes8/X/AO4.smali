.class public LX/AO4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ANi;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Landroid/net/Uri;

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/AN1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1668691
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1668692
    invoke-direct {p0, p1, v0}, LX/AO4;-><init>(Landroid/content/Context;LX/0Px;)V

    .line 1668693
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "LX/AN1;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1668694
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1668695
    const v0, 0x7f08276a

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/AO4;->a:Ljava/lang/String;

    .line 1668696
    const v0, 0x7f020767

    invoke-static {p1, v0}, LX/AOb;->a(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, LX/AO4;->b:Landroid/net/Uri;

    .line 1668697
    iput-object p2, p0, LX/AO4;->c:LX/0Px;

    .line 1668698
    return-void
.end method


# virtual methods
.method public final a()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1668690
    iget-object v0, p0, LX/AO4;->b:Landroid/net/Uri;

    return-object v0
.end method
