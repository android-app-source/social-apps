.class public LX/9ff;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9eb;


# static fields
.field private static final a:Landroid/graphics/RectF;


# instance fields
.field private final b:LX/9e6;

.field private final c:Landroid/net/Uri;

.field private final d:LX/434;

.field public final e:Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

.field private final f:Landroid/content/Context;

.field private final g:LX/9ee;

.field private final h:LX/9e5;

.field private final i:Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8GX;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/9cK;

.field private final l:LX/9fn;

.field private m:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

.field private n:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

.field public o:LX/9f4;

.field public p:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 1522443
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v1, v1, v2, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    sput-object v0, LX/9ff;->a:Landroid/graphics/RectF;

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Lcom/facebook/photos/creativeediting/RotatingFrameLayout;LX/9f4;Ljava/lang/String;LX/2Qx;LX/0Ot;LX/9cK;LX/9fo;Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/net/Uri;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/photos/creativeediting/RotatingFrameLayout;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/9f4;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Lcom/facebook/photos/creativeediting/RotatingFrameLayout;",
            "Lcom/facebook/photos/editgallery/EditGalleryFragmentController$FileEditingListener;",
            "Ljava/lang/String;",
            "LX/2Qx;",
            "LX/0Ot",
            "<",
            "LX/8GX;",
            ">;",
            "LX/9cK;",
            "LX/9fo;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1522426
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1522427
    new-instance v0, LX/9fa;

    invoke-direct {v0, p0}, LX/9fa;-><init>(LX/9ff;)V

    iput-object v0, p0, LX/9ff;->b:LX/9e6;

    .line 1522428
    iput-object p1, p0, LX/9ff;->c:Landroid/net/Uri;

    .line 1522429
    iput-object p2, p0, LX/9ff;->e:Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    .line 1522430
    iput-object p3, p0, LX/9ff;->o:LX/9f4;

    .line 1522431
    iput-object p6, p0, LX/9ff;->j:LX/0Ot;

    .line 1522432
    iput-object p7, p0, LX/9ff;->k:LX/9cK;

    .line 1522433
    invoke-virtual {p8, p4}, LX/9fo;->a(Ljava/lang/String;)LX/9fn;

    move-result-object v0

    iput-object v0, p0, LX/9ff;->l:LX/9fn;

    .line 1522434
    iput-object p9, p0, LX/9ff;->f:Landroid/content/Context;

    .line 1522435
    iget-object v0, p0, LX/9ff;->c:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/2Qx;->a(Ljava/lang/String;)LX/434;

    move-result-object v0

    iput-object v0, p0, LX/9ff;->d:LX/434;

    .line 1522436
    new-instance v0, LX/9e5;

    invoke-direct {v0, p9}, LX/9e5;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/9ff;->h:LX/9e5;

    .line 1522437
    iget-object v0, p0, LX/9ff;->h:LX/9e5;

    const v1, 0x7f0d00d4

    invoke-virtual {v0, v1}, LX/9e5;->setId(I)V

    .line 1522438
    new-instance v0, LX/9ee;

    iget-object v1, p0, LX/9ff;->f:Landroid/content/Context;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, LX/9ee;-><init>(Landroid/content/Context;Z)V

    iput-object v0, p0, LX/9ff;->g:LX/9ee;

    .line 1522439
    iget-object v0, p0, LX/9ff;->g:LX/9ee;

    const v1, 0x7f0d00d5

    invoke-virtual {v0, v1}, LX/9ee;->setId(I)V

    .line 1522440
    new-instance v0, Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;

    invoke-direct {v0, p9}, Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/9ff;->i:Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;

    .line 1522441
    iget-object v0, p0, LX/9ff;->i:Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;

    const v1, 0x7f0d00d6

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;->setId(I)V

    .line 1522442
    return-void
.end method

.method private static a(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z
    .locals 2

    .prologue
    .line 1522444
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1522445
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1522446
    iget v0, p0, Landroid/graphics/RectF;->right:F

    iget v1, p1, Landroid/graphics/RectF;->right:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget v0, p0, Landroid/graphics/RectF;->left:F

    iget v1, p1, Landroid/graphics/RectF;->left:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget v0, p0, Landroid/graphics/RectF;->top:F

    iget v1, p1, Landroid/graphics/RectF;->top:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget v0, p0, Landroid/graphics/RectF;->bottom:F

    iget v1, p1, Landroid/graphics/RectF;->bottom:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private o()Landroid/graphics/RectF;
    .locals 9

    .prologue
    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v7, 0x0

    .line 1522447
    iget-object v0, p0, LX/9ff;->g:LX/9ee;

    .line 1522448
    iget-object v1, v0, LX/9e3;->c:Landroid/graphics/RectF;

    move-object v1, v1

    .line 1522449
    iget-object v0, p0, LX/9ff;->h:LX/9e5;

    .line 1522450
    iget-object v2, v0, LX/9e5;->h:Landroid/graphics/RectF;

    move-object v2, v2

    .line 1522451
    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v0

    cmpl-float v0, v0, v7

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v0

    cmpl-float v0, v0, v7

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1522452
    new-instance v0, Landroid/graphics/RectF;

    iget v3, v1, Landroid/graphics/RectF;->left:F

    iget v4, v2, Landroid/graphics/RectF;->left:F

    sub-float/2addr v3, v4

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v4

    div-float/2addr v3, v4

    iget v4, v1, Landroid/graphics/RectF;->top:F

    iget v5, v2, Landroid/graphics/RectF;->top:F

    sub-float/2addr v4, v5

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v5

    div-float/2addr v4, v5

    iget v5, v1, Landroid/graphics/RectF;->right:F

    iget v6, v2, Landroid/graphics/RectF;->left:F

    sub-float/2addr v5, v6

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v6

    div-float/2addr v5, v6

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    iget v6, v2, Landroid/graphics/RectF;->top:F

    sub-float/2addr v1, v6

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    div-float/2addr v1, v2

    invoke-direct {v0, v3, v4, v5, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1522453
    invoke-virtual {v0, v7, v7, v8, v8}, Landroid/graphics/RectF;->intersect(FFFF)Z

    .line 1522454
    return-object v0

    .line 1522455
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static p(LX/9ff;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 1522457
    iget-object v0, p0, LX/9ff;->n:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9ff;->n:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-static {v0}, LX/5iB;->c(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1522458
    :cond_0
    iget-object v0, p0, LX/9ff;->k:LX/9cK;

    invoke-virtual {v0}, LX/9cK;->d()V

    .line 1522459
    :goto_0
    return-void

    .line 1522460
    :cond_1
    iget-object v0, p0, LX/9ff;->h:LX/9e5;

    .line 1522461
    iget-object v2, v0, LX/9e5;->h:Landroid/graphics/RectF;

    move-object v3, v2

    .line 1522462
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_2

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_3

    .line 1522463
    :cond_2
    iput-boolean v9, p0, LX/9ff;->p:Z

    goto :goto_0

    .line 1522464
    :cond_3
    iput-boolean v6, p0, LX/9ff;->p:Z

    .line 1522465
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v2

    float-to-int v2, v2

    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1522466
    iget v1, v3, Landroid/graphics/RectF;->top:F

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 1522467
    iget v1, v3, Landroid/graphics/RectF;->left:F

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 1522468
    iget-object v1, p0, LX/9ff;->i:Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;

    invoke-virtual {v1, v0}, Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1522469
    iget-object v0, p0, LX/9ff;->e:Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    const v1, 0x7f0d00d6

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_4

    .line 1522470
    iget-object v0, p0, LX/9ff;->e:Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    iget-object v1, p0, LX/9ff;->i:Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->addView(Landroid/view/View;)V

    .line 1522471
    iget-object v0, p0, LX/9ff;->g:LX/9ee;

    invoke-virtual {v0}, LX/9ee;->bringToFront()V

    .line 1522472
    :cond_4
    iget-object v0, p0, LX/9ff;->k:LX/9cK;

    .line 1522473
    invoke-virtual {v0}, LX/9cK;->b()V

    .line 1522474
    iget-object v0, p0, LX/9ff;->k:LX/9cK;

    iget-object v1, p0, LX/9ff;->n:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    float-to-int v3, v3

    iget-object v4, p0, LX/9ff;->e:Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    .line 1522475
    iget-boolean v5, v4, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->g:Z

    move v4, v5

    .line 1522476
    if-eqz v4, :cond_5

    iget-object v4, p0, LX/9ff;->e:Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    invoke-virtual {v4}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->getFinalRotation()I

    move-result v4

    :goto_1
    iget-object v5, p0, LX/9ff;->i:Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;

    const/4 v7, 0x3

    new-array v7, v7, [LX/9cJ;

    sget-object v8, LX/9cJ;->STICKERS:LX/9cJ;

    aput-object v8, v7, v6

    sget-object v8, LX/9cJ;->TEXTS:LX/9cJ;

    aput-object v8, v7, v9

    const/4 v8, 0x2

    sget-object v9, LX/9cJ;->DOODLE:LX/9cJ;

    aput-object v9, v7, v8

    invoke-virtual/range {v0 .. v7}, LX/9cK;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;IIILandroid/view/View;Z[LX/9cJ;)V

    goto/16 :goto_0

    :cond_5
    iget-object v4, p0, LX/9ff;->j:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/8GX;

    iget-object v5, p0, LX/9ff;->c:Landroid/net/Uri;

    invoke-virtual {v4, v5}, LX/8GX;->a(Landroid/net/Uri;)I

    move-result v4

    goto :goto_1
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1522456
    iget-object v0, p0, LX/9ff;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08241b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 1522478
    return-void
.end method

.method public final a(Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1522479
    iget-object v0, p1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v0, v0

    .line 1522480
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1522481
    iput-object p1, p0, LX/9ff;->m:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    .line 1522482
    iget-object v0, p1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v0, v0

    .line 1522483
    iput-object v0, p0, LX/9ff;->n:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1522484
    iget-object v0, p0, LX/9ff;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8GX;

    iget-object v3, p0, LX/9ff;->c:Landroid/net/Uri;

    invoke-virtual {v0, v3}, LX/8GX;->a(Landroid/net/Uri;)I

    move-result v3

    .line 1522485
    iget-object v0, p1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->n:Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;

    move-object v4, v0

    .line 1522486
    iget-object v0, v4, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->g:LX/434;

    move-object v5, v0

    .line 1522487
    sget-object v0, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->b:LX/434;

    invoke-virtual {v0, v5}, LX/434;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    .line 1522488
    :goto_0
    rem-int/lit8 v6, v3, 0x5a

    if-nez v6, :cond_8

    :goto_1
    const-string v6, "rotation must be multiple of 90 degree"

    invoke-static {v1, v6}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1522489
    iget-object v1, p0, LX/9ff;->g:LX/9ee;

    .line 1522490
    iget v6, v4, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->e:F

    move v6, v6

    .line 1522491
    iput v6, v1, LX/9e3;->d:F

    .line 1522492
    iget-object v1, p0, LX/9ff;->g:LX/9ee;

    .line 1522493
    iget v6, v4, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->f:F

    move v6, v6

    .line 1522494
    iput v6, v1, LX/9e3;->e:F

    .line 1522495
    iget-object v1, p0, LX/9ff;->h:LX/9e5;

    .line 1522496
    iget v6, v4, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->e:F

    move v6, v6

    .line 1522497
    iget p1, v1, LX/9e5;->m:F

    cmpl-float p1, p1, v6

    if-eqz p1, :cond_0

    .line 1522498
    iput v6, v1, LX/9e5;->m:F

    .line 1522499
    invoke-static {v1}, LX/9e5;->c(LX/9e5;)V

    .line 1522500
    :cond_0
    iget-object v1, p0, LX/9ff;->h:LX/9e5;

    .line 1522501
    iget v6, v4, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->f:F

    move v6, v6

    .line 1522502
    iget p1, v1, LX/9e5;->n:F

    cmpl-float p1, p1, v6

    if-eqz p1, :cond_1

    .line 1522503
    iput v6, v1, LX/9e5;->n:F

    .line 1522504
    invoke-static {v1}, LX/9e5;->c(LX/9e5;)V

    .line 1522505
    :cond_1
    iget-object v1, p0, LX/9ff;->h:LX/9e5;

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual {v1, v6}, LX/9e5;->setAlpha(F)V

    .line 1522506
    iget-object v1, p0, LX/9ff;->h:LX/9e5;

    invoke-virtual {v1, v2}, LX/9e5;->setVisibility(I)V

    .line 1522507
    iget-object v1, p0, LX/9ff;->h:LX/9e5;

    iget-object v6, p0, LX/9ff;->c:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/graphics/drawable/Drawable;->createFromPath(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v1, v6}, LX/9e5;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1522508
    if-eqz v0, :cond_2

    .line 1522509
    iget-object v0, p0, LX/9ff;->h:LX/9e5;

    iget-object v1, p0, LX/9ff;->d:LX/434;

    iget v1, v1, LX/434;->b:I

    int-to-float v1, v1

    iget v6, v5, LX/434;->b:I

    int-to-float v6, v6

    div-float/2addr v1, v6

    iget-object v6, p0, LX/9ff;->d:LX/434;

    iget v6, v6, LX/434;->a:I

    int-to-float v6, v6

    iget v5, v5, LX/434;->a:I

    int-to-float v5, v5

    div-float v5, v6, v5

    invoke-static {v1, v5}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 1522510
    iput v1, v0, LX/9e5;->p:F

    .line 1522511
    invoke-static {v0}, LX/9e5;->c(LX/9e5;)V

    .line 1522512
    :cond_2
    iget-object v0, p0, LX/9ff;->h:LX/9e5;

    invoke-static {v3}, LX/9e4;->valueOf(I)LX/9e4;

    move-result-object v1

    .line 1522513
    iget-object v3, v0, LX/9e5;->a:LX/9e4;

    if-eq v3, v1, :cond_3

    .line 1522514
    iput-object v1, v0, LX/9e5;->a:LX/9e4;

    .line 1522515
    invoke-static {v0}, LX/9e5;->b(LX/9e5;)V

    .line 1522516
    invoke-static {v0}, LX/9e5;->c(LX/9e5;)V

    .line 1522517
    :cond_3
    iget-object v0, p0, LX/9ff;->h:LX/9e5;

    new-instance v1, LX/9e8;

    iget-object v3, p0, LX/9ff;->h:LX/9e5;

    iget-object v5, p0, LX/9ff;->b:LX/9e6;

    invoke-direct {v1, v3, v5}, LX/9e8;-><init>(LX/9e5;LX/9e6;)V

    invoke-virtual {v0, v1}, LX/9e5;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1522518
    iget-object v0, p0, LX/9ff;->h:LX/9e5;

    .line 1522519
    iget-object v1, v4, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->c:Landroid/graphics/RectF;

    move-object v1, v1

    .line 1522520
    iput-object v1, v0, LX/9e5;->j:Landroid/graphics/RectF;

    .line 1522521
    invoke-static {v0}, LX/9e5;->c(LX/9e5;)V

    .line 1522522
    iget-object v0, p0, LX/9ff;->h:LX/9e5;

    new-instance v1, LX/9fb;

    invoke-direct {v1, p0}, LX/9fb;-><init>(LX/9ff;)V

    invoke-virtual {v0, v1}, LX/9e5;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 1522523
    iget-object v0, p0, LX/9ff;->e:Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    invoke-virtual {v0, v2}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->setVisibility(I)V

    .line 1522524
    iget-object v0, p0, LX/9ff;->e:Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    const v1, 0x7f0d00d4

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_4

    .line 1522525
    iget-object v0, p0, LX/9ff;->e:Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    iget-object v1, p0, LX/9ff;->h:LX/9e5;

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->addView(Landroid/view/View;)V

    .line 1522526
    :cond_4
    iget-object v0, p0, LX/9ff;->e:Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    const v1, 0x7f0d00d5

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_6

    .line 1522527
    iget-object v0, v4, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->d:Lcom/facebook/photos/creativeediting/model/StickerParams;

    move-object v0, v0

    .line 1522528
    if-eqz v0, :cond_5

    .line 1522529
    iget-object v0, p0, LX/9ff;->g:LX/9ee;

    .line 1522530
    iget-object v1, v4, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->d:Lcom/facebook/photos/creativeediting/model/StickerParams;

    move-object v1, v1

    .line 1522531
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 1522532
    iget-object v3, v0, LX/9ee;->c:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->j()V

    .line 1522533
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/5i8;

    .line 1522534
    iget-object v5, v0, LX/9ee;->c:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    invoke-virtual {v5, v3, v0}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->a(LX/5i8;Landroid/graphics/drawable/Drawable$Callback;)V

    goto :goto_2

    .line 1522535
    :cond_5
    iget-object v0, p0, LX/9ff;->g:LX/9ee;

    invoke-virtual {v0, v2}, LX/9ee;->setVisibility(I)V

    .line 1522536
    iget-object v0, p0, LX/9ff;->e:Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    iget-object v1, p0, LX/9ff;->g:LX/9ee;

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->addView(Landroid/view/View;)V

    .line 1522537
    :cond_6
    invoke-static {p0}, LX/9ff;->p(LX/9ff;)V

    .line 1522538
    iget-object v0, p0, LX/9ff;->i:Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;

    iget-object v1, p0, LX/9ff;->k:LX/9cK;

    .line 1522539
    iput-object v1, v0, Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;->a:LX/9cG;

    .line 1522540
    return-void

    :cond_7
    move v0, v2

    .line 1522541
    goto/16 :goto_0

    :cond_8
    move v1, v2

    .line 1522542
    goto/16 :goto_1
.end method

.method public final a(Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;)V
    .locals 0

    .prologue
    .line 1522477
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 1522422
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1522423
    iget-object v0, p0, LX/9ff;->e:Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->removeAllViews()V

    .line 1522424
    iget-object v0, p0, LX/9ff;->e:Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->setVisibility(I)V

    .line 1522425
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 1522374
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1522375
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1522376
    const/4 v0, 0x0

    return v0
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 1522377
    invoke-static {p0}, LX/9ff;->p(LX/9ff;)V

    .line 1522378
    return-void
.end method

.method public final g()V
    .locals 0

    .prologue
    .line 1522379
    return-void
.end method

.method public final h()V
    .locals 0

    .prologue
    .line 1522380
    return-void
.end method

.method public final i()V
    .locals 0

    .prologue
    .line 1522381
    return-void
.end method

.method public final j()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1522382
    sget-object v0, LX/5Rr;->CROP:LX/5Rr;

    return-object v0
.end method

.method public final k()LX/9ek;
    .locals 1

    .prologue
    .line 1522383
    sget-object v0, LX/9ek;->NONE:LX/9ek;

    return-object v0
.end method

.method public final l()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1522411
    iget-object v0, p0, LX/9ff;->h:LX/9e5;

    .line 1522412
    iget-object v2, v0, LX/9e5;->h:Landroid/graphics/RectF;

    move-object v0, v2

    .line 1522413
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9ff;->h:LX/9e5;

    .line 1522414
    iget-object v2, v0, LX/9e5;->h:Landroid/graphics/RectF;

    move-object v0, v2

    .line 1522415
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 1522416
    :cond_0
    const/4 v0, 0x0

    .line 1522417
    :goto_0
    return v0

    .line 1522418
    :cond_1
    invoke-direct {p0}, LX/9ff;->o()Landroid/graphics/RectF;

    move-result-object v0

    .line 1522419
    iget-object v1, p0, LX/9ff;->n:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v1

    if-nez v1, :cond_2

    .line 1522420
    sget-object v1, LX/9ff;->a:Landroid/graphics/RectF;

    invoke-static {v0, v1}, LX/9ff;->a(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    move-result v0

    goto :goto_0

    .line 1522421
    :cond_2
    iget-object v1, p0, LX/9ff;->n:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v1

    invoke-static {v1}, LX/63w;->c(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Landroid/graphics/RectF;

    move-result-object v1

    invoke-static {v0, v1}, LX/9ff;->a(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    move-result v0

    goto :goto_0
.end method

.method public final m()Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;
    .locals 6

    .prologue
    .line 1522384
    iget-object v0, p0, LX/9ff;->m:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    new-instance v1, LX/5Ry;

    iget-object v2, p0, LX/9ff;->m:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    .line 1522385
    iget-object v3, v2, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->n:Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;

    move-object v2, v3

    .line 1522386
    invoke-direct {v1, v2}, LX/5Ry;-><init>(Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;)V

    invoke-direct {p0}, LX/9ff;->o()Landroid/graphics/RectF;

    move-result-object v2

    .line 1522387
    iput-object v2, v1, LX/5Ry;->a:Landroid/graphics/RectF;

    .line 1522388
    move-object v1, v1

    .line 1522389
    invoke-virtual {v1}, LX/5Ry;->a()Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;

    move-result-object v1

    .line 1522390
    iput-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->n:Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;

    .line 1522391
    iget-object v0, p0, LX/9ff;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8GX;

    iget-object v1, p0, LX/9ff;->c:Landroid/net/Uri;

    invoke-virtual {v0, v1}, LX/8GX;->a(Landroid/net/Uri;)I

    move-result v0

    .line 1522392
    invoke-direct {p0}, LX/9ff;->o()Landroid/graphics/RectF;

    move-result-object v4

    .line 1522393
    if-nez v0, :cond_0

    .line 1522394
    iget-object v0, p0, LX/9ff;->n:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-static {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v0

    invoke-static {v4}, LX/63w;->a(Landroid/graphics/RectF;)Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setCropBox(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    iput-object v0, p0, LX/9ff;->n:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1522395
    iget-object v0, p0, LX/9ff;->m:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v1, p0, LX/9ff;->n:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1522396
    iput-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1522397
    iget-object v0, p0, LX/9ff;->o:LX/9f4;

    invoke-virtual {v0}, LX/9f4;->a()V

    .line 1522398
    iget-object v0, p0, LX/9ff;->l:LX/9fn;

    iget-object v1, p0, LX/9ff;->n:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    iget-object v2, p0, LX/9ff;->c:Landroid/net/Uri;

    iget-object v3, p0, LX/9ff;->d:LX/434;

    new-instance v5, LX/9fd;

    invoke-direct {v5, p0}, LX/9fd;-><init>(LX/9ff;)V

    invoke-virtual/range {v0 .. v5}, LX/9fn;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;Landroid/net/Uri;LX/434;Landroid/graphics/RectF;LX/9fc;)V

    .line 1522399
    iget-object v0, p0, LX/9ff;->m:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    .line 1522400
    :goto_0
    return-object v0

    .line 1522401
    :cond_0
    invoke-static {v0}, LX/8Ga;->a(I)I

    move-result v1

    rsub-int/lit8 v1, v1, 0x4

    .line 1522402
    rem-int/lit16 v2, v0, 0x168

    .line 1522403
    iget-object v0, p0, LX/9ff;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8GX;

    iget-object v3, p0, LX/9ff;->c:Landroid/net/Uri;

    invoke-virtual {v0, v3, v2}, LX/8GX;->a(Landroid/net/Uri;I)V

    .line 1522404
    invoke-static {v4, v1}, LX/8Ga;->a(Landroid/graphics/RectF;I)Landroid/graphics/RectF;

    move-result-object v4

    .line 1522405
    iget-object v0, p0, LX/9ff;->n:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-static {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v0

    invoke-static {v4}, LX/63w;->a(Landroid/graphics/RectF;)Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setCropBox(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    iput-object v0, p0, LX/9ff;->n:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1522406
    iget-object v0, p0, LX/9ff;->m:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v1, p0, LX/9ff;->n:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1522407
    iput-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1522408
    iget-object v0, p0, LX/9ff;->o:LX/9f4;

    invoke-virtual {v0}, LX/9f4;->a()V

    .line 1522409
    iget-object v0, p0, LX/9ff;->l:LX/9fn;

    iget-object v1, p0, LX/9ff;->n:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    iget-object v2, p0, LX/9ff;->c:Landroid/net/Uri;

    iget-object v3, p0, LX/9ff;->d:LX/434;

    new-instance v5, LX/9fe;

    invoke-direct {v5, p0}, LX/9fe;-><init>(LX/9ff;)V

    invoke-virtual/range {v0 .. v5}, LX/9fn;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;Landroid/net/Uri;LX/434;Landroid/graphics/RectF;LX/9fc;)V

    .line 1522410
    iget-object v0, p0, LX/9ff;->m:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    goto :goto_0
.end method
