.class public LX/8zA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8z3;


# instance fields
.field private final a:LX/8z4;

.field private final b:LX/8zJ;

.field private final c:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(LX/8z4;LX/8zJ;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1426955
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1426956
    iput-object p1, p0, LX/8zA;->a:LX/8z4;

    .line 1426957
    iput-object p2, p0, LX/8zA;->b:LX/8zJ;

    .line 1426958
    iput-object p3, p0, LX/8zA;->c:Landroid/content/res/Resources;

    .line 1426959
    return-void
.end method

.method public static a(LX/0QB;)LX/8zA;
    .locals 1

    .prologue
    .line 1426969
    invoke-static {p0}, LX/8zA;->b(LX/0QB;)LX/8zA;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/8zA;
    .locals 4

    .prologue
    .line 1426964
    new-instance v3, LX/8zA;

    .line 1426965
    new-instance v1, LX/8z4;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-direct {v1, v0}, LX/8z4;-><init>(Landroid/content/res/Resources;)V

    .line 1426966
    move-object v0, v1

    .line 1426967
    check-cast v0, LX/8z4;

    invoke-static {p0}, LX/8zJ;->b(LX/0QB;)LX/8zJ;

    move-result-object v1

    check-cast v1, LX/8zJ;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    invoke-direct {v3, v0, v1, v2}, LX/8zA;-><init>(LX/8z4;LX/8zJ;Landroid/content/res/Resources;)V

    .line 1426968
    return-object v3
.end method


# virtual methods
.method public final a(LX/8z5;)Landroid/text/SpannableStringBuilder;
    .locals 2

    .prologue
    .line 1426963
    new-instance v0, LX/8z8;

    iget-object v1, p0, LX/8zA;->c:Landroid/content/res/Resources;

    invoke-direct {v0, v1}, LX/8z8;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v0}, LX/8z8;->a()LX/8z7;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LX/8zA;->a(LX/8z5;LX/8z7;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/8z5;LX/8z7;)Landroid/text/SpannableStringBuilder;
    .locals 1

    .prologue
    .line 1426960
    iget-object v0, p1, LX/8z5;->a:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    if-nez v0, :cond_0

    .line 1426961
    iget-object v0, p0, LX/8zA;->a:LX/8z4;

    invoke-virtual {v0, p1, p2}, LX/8z4;->a(LX/8z5;LX/8z7;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 1426962
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/8zA;->b:LX/8zJ;

    invoke-virtual {v0, p1, p2}, LX/8zJ;->a(LX/8z5;LX/8z7;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    goto :goto_0
.end method
