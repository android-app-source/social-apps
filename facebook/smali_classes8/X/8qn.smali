.class public final LX/8qn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/8qp;


# direct methods
.method public constructor <init>(LX/8qp;)V
    .locals 0

    .prologue
    .line 1407962
    iput-object p1, p0, LX/8qn;->a:LX/8qp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x76c8cab1

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1407963
    const v0, 0x7f0d0073

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1407964
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1407965
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1407966
    iget-object v0, p0, LX/8qn;->a:LX/8qp;

    iget-object v0, v0, LX/8qp;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/8qn;->a:LX/8qp;

    iget-object v3, v3, LX/8qp;->a:Landroid/content/Context;

    invoke-interface {v0, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1407967
    iget-object v0, p0, LX/8qn;->a:LX/8qp;

    iget-object v2, v0, LX/8qp;->c:LX/0Zb;

    const v0, 0x7f0d006a

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-interface {v2, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1407968
    const v0, 0x75a2b265

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
