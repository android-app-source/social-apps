.class public LX/9dU;
.super LX/9dK;
.source ""


# instance fields
.field private c:Landroid/widget/ImageView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:Landroid/widget/TextView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/8Gc;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1518172
    invoke-direct {p0, p1, p2}, LX/9dK;-><init>(Landroid/content/Context;LX/8Gc;)V

    .line 1518173
    return-void
.end method


# virtual methods
.method public final a(F)F
    .locals 1

    .prologue
    .line 1518174
    iget-object v0, p0, LX/9dU;->c:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v0

    int-to-float v0, v0

    sub-float v0, p1, v0

    return v0
.end method

.method public final a()Landroid/widget/ImageView;
    .locals 3

    .prologue
    .line 1518175
    iget-object v0, p0, LX/9dU;->c:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 1518176
    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, LX/9dK;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/9dU;->c:Landroid/widget/ImageView;

    .line 1518177
    iget-object v0, p0, LX/9dU;->c:Landroid/widget/ImageView;

    iget-object v1, p0, LX/9dK;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020402

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1518178
    iget-object v0, p0, LX/9dU;->c:Landroid/widget/ImageView;

    const/high16 v1, -0x40800000    # -1.0f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 1518179
    iget-object v0, p0, LX/9dU;->c:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 1518180
    :cond_0
    iget-object v0, p0, LX/9dU;->c:Landroid/widget/ImageView;

    return-object v0
.end method

.method public final b()Landroid/widget/TextView;
    .locals 4

    .prologue
    .line 1518181
    iget-object v0, p0, LX/9dU;->d:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 1518182
    new-instance v0, Landroid/widget/TextView;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    iget-object v2, p0, LX/9dK;->a:Landroid/content/Context;

    const v3, 0x7f0e0643

    invoke-direct {v1, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/9dU;->d:Landroid/widget/TextView;

    .line 1518183
    iget-object v0, p0, LX/9dU;->d:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 1518184
    iget-object v0, p0, LX/9dU;->d:Landroid/widget/TextView;

    const v1, 0x7f0813cd

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1518185
    iget-object v0, p0, LX/9dU;->d:Landroid/widget/TextView;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 1518186
    iget-object v0, p0, LX/9dU;->d:Landroid/widget/TextView;

    iget-object v1, p0, LX/9dK;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x106000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1518187
    iget-object v0, p0, LX/9dU;->d:Landroid/widget/TextView;

    const/4 v1, 0x2

    const/high16 v2, 0x41700000    # 15.0f

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1518188
    :cond_0
    iget-object v0, p0, LX/9dU;->d:Landroid/widget/TextView;

    return-object v0
.end method

.method public final c()F
    .locals 1

    .prologue
    .line 1518189
    iget-object v0, p0, LX/9dK;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method public final d()F
    .locals 2

    .prologue
    .line 1518190
    const/high16 v0, 0x40400000    # 3.0f

    iget-object v1, p0, LX/9dK;->b:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    const/high16 v1, 0x40800000    # 4.0f

    div-float/2addr v0, v1

    return v0
.end method

.method public final e()LX/9dT;
    .locals 1

    .prologue
    .line 1518191
    sget-object v0, LX/9dT;->COUNTER_CLOCKWISE:LX/9dT;

    return-object v0
.end method

.method public final f()F
    .locals 1

    .prologue
    .line 1518192
    iget-object v0, p0, LX/9dK;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method public final g()F
    .locals 2

    .prologue
    .line 1518193
    iget-object v0, p0, LX/9dU;->c:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x40800000    # 4.0f

    mul-float/2addr v0, v1

    const/high16 v1, -0x40800000    # -1.0f

    mul-float/2addr v0, v1

    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 1518194
    const/4 v0, 0x0

    return v0
.end method
