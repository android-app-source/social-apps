.class public final LX/9Cb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLComment;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public final synthetic c:LX/9Cd;


# direct methods
.method public constructor <init>(LX/9Cd;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 0

    .prologue
    .line 1454193
    iput-object p1, p0, LX/9Cb;->c:LX/9Cd;

    iput-object p2, p0, LX/9Cb;->a:Lcom/facebook/graphql/model/GraphQLComment;

    iput-object p3, p0, LX/9Cb;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1454197
    iget-object v0, p0, LX/9Cb;->c:LX/9Cd;

    iget-object v0, v0, LX/9Cd;->d:LX/3iL;

    const v1, 0x7f080039

    const-string v2, "An error occurred while trying to delete an offline comment"

    .line 1454198
    iget-object p0, v0, LX/3iL;->e:LX/0kL;

    new-instance p1, LX/27k;

    invoke-direct {p1, v1}, LX/27k;-><init>(I)V

    invoke-virtual {p0, p1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 1454199
    if-eqz v2, :cond_0

    .line 1454200
    iget-object p0, v0, LX/3iL;->d:LX/03V;

    sget-object p1, LX/3iL;->a:Ljava/lang/String;

    invoke-virtual {p0, p1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1454201
    :cond_0
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1454194
    iget-object v0, p0, LX/9Cb;->c:LX/9Cd;

    iget-object v0, v0, LX/9Cd;->k:LX/3iP;

    iget-object v1, p0, LX/9Cb;->a:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v0, v1}, LX/3iP;->c(Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 1454195
    iget-object v0, p0, LX/9Cb;->c:LX/9Cd;

    iget-object v0, v0, LX/9Cd;->b:LX/1K9;

    new-instance v1, LX/8py;

    iget-object v2, p0, LX/9Cb;->a:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v3, p0, LX/9Cb;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/8py;-><init>(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/1K9;->a(LX/1KJ;)V

    .line 1454196
    return-void
.end method
