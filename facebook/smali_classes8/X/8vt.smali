.class public final LX/8vt;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/opengl/EGLDisplay;

.field private b:Landroid/opengl/EGLContext;

.field private c:Landroid/opengl/EGLConfig;

.field private d:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1421445
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LX/8vt;-><init>(Landroid/opengl/EGLContext;I)V

    .line 1421446
    return-void
.end method

.method private constructor <init>(Landroid/opengl/EGLContext;I)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x2

    const/4 v5, 0x3

    const/4 v4, 0x0

    .line 1421486
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1421487
    sget-object v0, Landroid/opengl/EGL14;->EGL_NO_DISPLAY:Landroid/opengl/EGLDisplay;

    iput-object v0, p0, LX/8vt;->a:Landroid/opengl/EGLDisplay;

    .line 1421488
    sget-object v0, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    iput-object v0, p0, LX/8vt;->b:Landroid/opengl/EGLContext;

    .line 1421489
    iput-object v2, p0, LX/8vt;->c:Landroid/opengl/EGLConfig;

    .line 1421490
    const/4 v0, -0x1

    iput v0, p0, LX/8vt;->d:I

    .line 1421491
    iget-object v0, p0, LX/8vt;->a:Landroid/opengl/EGLDisplay;

    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_DISPLAY:Landroid/opengl/EGLDisplay;

    if-eq v0, v1, :cond_0

    .line 1421492
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "EGL already set up"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1421493
    :cond_0
    if-nez p1, :cond_1

    .line 1421494
    sget-object p1, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    .line 1421495
    :cond_1
    invoke-static {v4}, Landroid/opengl/EGL14;->eglGetDisplay(I)Landroid/opengl/EGLDisplay;

    move-result-object v0

    iput-object v0, p0, LX/8vt;->a:Landroid/opengl/EGLDisplay;

    .line 1421496
    iget-object v0, p0, LX/8vt;->a:Landroid/opengl/EGLDisplay;

    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_DISPLAY:Landroid/opengl/EGLDisplay;

    if-ne v0, v1, :cond_2

    .line 1421497
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "unable to get EGL14 display"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1421498
    :cond_2
    new-array v0, v6, [I

    .line 1421499
    iget-object v1, p0, LX/8vt;->a:Landroid/opengl/EGLDisplay;

    invoke-static {v1, v0, v4, v0, v7}, Landroid/opengl/EGL14;->eglInitialize(Landroid/opengl/EGLDisplay;[II[II)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1421500
    iput-object v2, p0, LX/8vt;->a:Landroid/opengl/EGLDisplay;

    .line 1421501
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "unable to initialize EGL14"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1421502
    :cond_3
    and-int/lit8 v0, p2, 0x2

    if-eqz v0, :cond_4

    .line 1421503
    invoke-direct {p0, p2, v5}, LX/8vt;->b(II)Landroid/opengl/EGLConfig;

    move-result-object v0

    .line 1421504
    if-eqz v0, :cond_4

    .line 1421505
    new-array v1, v5, [I

    fill-array-data v1, :array_0

    .line 1421506
    iget-object v2, p0, LX/8vt;->a:Landroid/opengl/EGLDisplay;

    invoke-static {v2, v0, p1, v1, v4}, Landroid/opengl/EGL14;->eglCreateContext(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLConfig;Landroid/opengl/EGLContext;[II)Landroid/opengl/EGLContext;

    move-result-object v1

    .line 1421507
    invoke-static {}, Landroid/opengl/EGL14;->eglGetError()I

    move-result v2

    const/16 v3, 0x3000

    if-ne v2, v3, :cond_4

    .line 1421508
    iput-object v0, p0, LX/8vt;->c:Landroid/opengl/EGLConfig;

    .line 1421509
    iput-object v1, p0, LX/8vt;->b:Landroid/opengl/EGLContext;

    .line 1421510
    iput v5, p0, LX/8vt;->d:I

    .line 1421511
    :cond_4
    iget-object v0, p0, LX/8vt;->b:Landroid/opengl/EGLContext;

    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    if-ne v0, v1, :cond_6

    .line 1421512
    invoke-direct {p0, p2, v6}, LX/8vt;->b(II)Landroid/opengl/EGLConfig;

    move-result-object v0

    .line 1421513
    if-nez v0, :cond_5

    .line 1421514
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unable to find a suitable EGLConfig"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1421515
    :cond_5
    new-array v1, v5, [I

    fill-array-data v1, :array_1

    .line 1421516
    iget-object v2, p0, LX/8vt;->a:Landroid/opengl/EGLDisplay;

    invoke-static {v2, v0, p1, v1, v4}, Landroid/opengl/EGL14;->eglCreateContext(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLConfig;Landroid/opengl/EGLContext;[II)Landroid/opengl/EGLContext;

    move-result-object v1

    .line 1421517
    const-string v2, "eglCreateContext"

    invoke-static {v2}, LX/8vt;->a(Ljava/lang/String;)V

    .line 1421518
    iput-object v0, p0, LX/8vt;->c:Landroid/opengl/EGLConfig;

    .line 1421519
    iput-object v1, p0, LX/8vt;->b:Landroid/opengl/EGLContext;

    .line 1421520
    iput v6, p0, LX/8vt;->d:I

    .line 1421521
    :cond_6
    new-array v0, v7, [I

    .line 1421522
    iget-object v1, p0, LX/8vt;->a:Landroid/opengl/EGLDisplay;

    iget-object v2, p0, LX/8vt;->b:Landroid/opengl/EGLContext;

    const/16 v3, 0x3098

    invoke-static {v1, v2, v3, v0, v4}, Landroid/opengl/EGL14;->eglQueryContext(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLContext;I[II)Z

    .line 1421523
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "EGLContext created, client version "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget v0, v0, v4

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1421524
    return-void

    .line 1421525
    :array_0
    .array-data 4
        0x3098
        0x3
        0x3038
    .end array-data

    .line 1421526
    :array_1
    .array-data 4
        0x3098
        0x2
        0x3038
    .end array-data
.end method

.method private static a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1421527
    invoke-static {}, Landroid/opengl/EGL14;->eglGetError()I

    move-result v0

    const/16 v1, 0x3000

    if-eq v0, v1, :cond_0

    .line 1421528
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": EGL error: 0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1421529
    :cond_0
    return-void
.end method

.method private b(II)Landroid/opengl/EGLConfig;
    .locals 9

    .prologue
    const/4 v3, 0x4

    const/4 v8, 0x3

    const/16 v7, 0x8

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 1421468
    if-lt p2, v8, :cond_2

    .line 1421469
    const/16 v0, 0x44

    .line 1421470
    :goto_0
    const/16 v1, 0xf

    new-array v1, v1, [I

    const/16 v4, 0x3024

    aput v4, v1, v2

    aput v7, v1, v5

    const/4 v4, 0x2

    const/16 v6, 0x3023

    aput v6, v1, v4

    aput v7, v1, v8

    const/16 v4, 0x3022

    aput v4, v1, v3

    const/4 v3, 0x5

    aput v7, v1, v3

    const/4 v3, 0x6

    const/16 v4, 0x3021

    aput v4, v1, v3

    const/4 v3, 0x7

    aput v7, v1, v3

    const/16 v3, 0x3025

    aput v3, v1, v7

    const/16 v3, 0x9

    const/16 v4, 0x18

    aput v4, v1, v3

    const/16 v3, 0xa

    const/16 v4, 0x3040

    aput v4, v1, v3

    const/16 v3, 0xb

    aput v0, v1, v3

    const/16 v0, 0xc

    const/16 v3, 0x3038

    aput v3, v1, v0

    const/16 v0, 0xd

    aput v2, v1, v0

    const/16 v0, 0xe

    const/16 v3, 0x3038

    aput v3, v1, v0

    .line 1421471
    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_0

    .line 1421472
    const/16 v0, 0xc

    const/16 v3, 0x3142

    aput v3, v1, v0

    .line 1421473
    const/16 v0, 0xd

    aput v5, v1, v0

    .line 1421474
    :cond_0
    new-array v3, v5, [Landroid/opengl/EGLConfig;

    .line 1421475
    new-array v6, v5, [I

    .line 1421476
    iget-object v0, p0, LX/8vt;->a:Landroid/opengl/EGLDisplay;

    move v4, v2

    move v7, v2

    invoke-static/range {v0 .. v7}, Landroid/opengl/EGL14;->eglChooseConfig(Landroid/opengl/EGLDisplay;[II[Landroid/opengl/EGLConfig;II[II)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1421477
    const-string v0, "GLUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unable to find RGB8888 / "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " EGLConfig"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1421478
    const/4 v0, 0x0

    .line 1421479
    :goto_1
    return-object v0

    :cond_1
    aget-object v0, v3, v2

    goto :goto_1

    :cond_2
    move v0, v3

    goto :goto_0
.end method


# virtual methods
.method public final a(II)Landroid/opengl/EGLSurface;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1421480
    const/4 v0, 0x5

    new-array v0, v0, [I

    const/16 v1, 0x3057

    aput v1, v0, v3

    const/4 v1, 0x1

    aput p1, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0x3056

    aput v2, v0, v1

    const/4 v1, 0x3

    aput p2, v0, v1

    const/4 v1, 0x4

    const/16 v2, 0x3038

    aput v2, v0, v1

    .line 1421481
    iget-object v1, p0, LX/8vt;->a:Landroid/opengl/EGLDisplay;

    iget-object v2, p0, LX/8vt;->c:Landroid/opengl/EGLConfig;

    invoke-static {v1, v2, v0, v3}, Landroid/opengl/EGL14;->eglCreatePbufferSurface(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLConfig;[II)Landroid/opengl/EGLSurface;

    move-result-object v0

    .line 1421482
    const-string v1, "eglCreatePbufferSurface"

    invoke-static {v1}, LX/8vt;->a(Ljava/lang/String;)V

    .line 1421483
    if-nez v0, :cond_0

    .line 1421484
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "surface was null"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1421485
    :cond_0
    return-object v0
.end method

.method public final a()V
    .locals 4

    .prologue
    .line 1421459
    iget-object v0, p0, LX/8vt;->a:Landroid/opengl/EGLDisplay;

    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_DISPLAY:Landroid/opengl/EGLDisplay;

    if-eq v0, v1, :cond_0

    .line 1421460
    iget-object v0, p0, LX/8vt;->a:Landroid/opengl/EGLDisplay;

    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    sget-object v2, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    sget-object v3, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/EGL14;->eglMakeCurrent(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;)Z

    .line 1421461
    iget-object v0, p0, LX/8vt;->a:Landroid/opengl/EGLDisplay;

    iget-object v1, p0, LX/8vt;->b:Landroid/opengl/EGLContext;

    invoke-static {v0, v1}, Landroid/opengl/EGL14;->eglDestroyContext(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLContext;)Z

    .line 1421462
    invoke-static {}, Landroid/opengl/EGL14;->eglReleaseThread()Z

    .line 1421463
    iget-object v0, p0, LX/8vt;->a:Landroid/opengl/EGLDisplay;

    invoke-static {v0}, Landroid/opengl/EGL14;->eglTerminate(Landroid/opengl/EGLDisplay;)Z

    .line 1421464
    :cond_0
    sget-object v0, Landroid/opengl/EGL14;->EGL_NO_DISPLAY:Landroid/opengl/EGLDisplay;

    iput-object v0, p0, LX/8vt;->a:Landroid/opengl/EGLDisplay;

    .line 1421465
    sget-object v0, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    iput-object v0, p0, LX/8vt;->b:Landroid/opengl/EGLContext;

    .line 1421466
    const/4 v0, 0x0

    iput-object v0, p0, LX/8vt;->c:Landroid/opengl/EGLConfig;

    .line 1421467
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 1421456
    iget-object v0, p0, LX/8vt;->a:Landroid/opengl/EGLDisplay;

    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    sget-object v2, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    sget-object v3, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/EGL14;->eglMakeCurrent(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1421457
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "eglMakeCurrent failed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1421458
    :cond_0
    return-void
.end method

.method public final b(Landroid/opengl/EGLSurface;)V
    .locals 2

    .prologue
    .line 1421453
    iget-object v0, p0, LX/8vt;->a:Landroid/opengl/EGLDisplay;

    iget-object v1, p0, LX/8vt;->b:Landroid/opengl/EGLContext;

    invoke-static {v0, p1, p1, v1}, Landroid/opengl/EGL14;->eglMakeCurrent(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1421454
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "eglMakeCurrent failed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1421455
    :cond_0
    return-void
.end method

.method public final finalize()V
    .locals 2

    .prologue
    .line 1421447
    :try_start_0
    iget-object v0, p0, LX/8vt;->a:Landroid/opengl/EGLDisplay;

    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_DISPLAY:Landroid/opengl/EGLDisplay;

    if-eq v0, v1, :cond_0

    .line 1421448
    const-string v0, "GLUtil"

    const-string v1, "WARNING: EglCore was not explicitly released -- state may be leaked"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1421449
    invoke-virtual {p0}, LX/8vt;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1421450
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 1421451
    return-void

    .line 1421452
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method
