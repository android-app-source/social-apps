.class public LX/8u4;
.super LX/8tw;
.source ""


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1414444
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/8tw;-><init>(Ljava/lang/String;)V

    .line 1414445
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/8u4;->a:Z

    .line 1414446
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;IILandroid/text/Editable;)V
    .locals 3

    .prologue
    const/16 v2, 0x21

    .line 1414467
    const-string v0, "pre"

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1414468
    invoke-static {p4}, LX/33Q;->a(Landroid/text/Editable;)Landroid/text/Editable;

    .line 1414469
    new-instance v0, LX/8u9;

    invoke-direct {v0}, LX/8u9;-><init>()V

    const v1, -0x777778

    invoke-virtual {v0, v1}, LX/8u8;->a(I)LX/8u8;

    move-result-object v0

    invoke-interface {p4}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-interface {p4, v0, p2, v1, v2}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    .line 1414470
    :cond_0
    :goto_0
    return-void

    .line 1414471
    :cond_1
    const-string v0, "code"

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1414472
    iget-boolean v0, p0, LX/8u4;->a:Z

    if-nez v0, :cond_2

    .line 1414473
    new-instance v0, Landroid/text/style/BackgroundColorSpan;

    const v1, -0x111112

    invoke-direct {v0, v1}, Landroid/text/style/BackgroundColorSpan;-><init>(I)V

    invoke-interface {p4, v0, p2, p3, v2}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    .line 1414474
    :cond_2
    new-instance v0, Landroid/text/style/TypefaceSpan;

    const-string v1, "monospace"

    invoke-direct {v0, v1}, Landroid/text/style/TypefaceSpan;-><init>(Ljava/lang/String;)V

    invoke-interface {p4, v0, p2, p3, v2}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Landroid/text/Editable;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1414460
    const-string v2, "pre"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1414461
    const-string v2, "pre"

    invoke-virtual {p0, p2, v2}, LX/8tw;->b(Landroid/text/Editable;Ljava/lang/Object;)V

    .line 1414462
    iput-boolean v1, p0, LX/8u4;->a:Z

    .line 1414463
    :goto_0
    return v0

    .line 1414464
    :cond_0
    const-string v2, "code"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1414465
    const-string v1, "code"

    invoke-virtual {p0, p2, v1}, LX/8tw;->b(Landroid/text/Editable;Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1414466
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lorg/xml/sax/Attributes;Landroid/text/Editable;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1414452
    const-string v1, "pre"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1414453
    invoke-static {p3}, LX/33Q;->a(Landroid/text/Editable;)Landroid/text/Editable;

    .line 1414454
    const-string v1, "pre"

    invoke-static {p3, v1}, LX/8tw;->a(Landroid/text/Editable;Ljava/lang/Object;)V

    .line 1414455
    iput-boolean v0, p0, LX/8u4;->a:Z

    .line 1414456
    :goto_0
    return v0

    .line 1414457
    :cond_0
    const-string v1, "code"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1414458
    const-string v1, "code"

    invoke-static {p3, v1}, LX/8tw;->a(Landroid/text/Editable;Ljava/lang/Object;)V

    goto :goto_0

    .line 1414459
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a([CIILandroid/text/Editable;)Z
    .locals 1

    .prologue
    .line 1414447
    iget-boolean v0, p0, LX/8u4;->a:Z

    if-nez v0, :cond_0

    .line 1414448
    const/4 v0, 0x0

    .line 1414449
    :goto_0
    return v0

    .line 1414450
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1, p2, p3}, Ljava/lang/String;-><init>([CII)V

    invoke-interface {p4, v0}, Landroid/text/Editable;->append(Ljava/lang/CharSequence;)Landroid/text/Editable;

    .line 1414451
    const/4 v0, 0x1

    goto :goto_0
.end method
