.class public final enum LX/8wV;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8wV;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8wV;

.field public static final enum BOOSTED_EVENT_MOBILE_MODULE:LX/8wV;

.field public static final enum BOOSTED_POST_MOBILE_MODULE:LX/8wV;

.field public static final enum PROMOTE_CTA_MOBILE_MODULE:LX/8wV;

.field public static final enum PROMOTE_PAGE_MOBILE_MODULE:LX/8wV;


# instance fields
.field private final module:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1422740
    new-instance v0, LX/8wV;

    const-string v1, "BOOSTED_POST_MOBILE_MODULE"

    const-string v2, "boosted_post_mobile"

    invoke-direct {v0, v1, v3, v2}, LX/8wV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wV;->BOOSTED_POST_MOBILE_MODULE:LX/8wV;

    .line 1422741
    new-instance v0, LX/8wV;

    const-string v1, "BOOSTED_EVENT_MOBILE_MODULE"

    const-string v2, "boosted_event_mobile"

    invoke-direct {v0, v1, v4, v2}, LX/8wV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wV;->BOOSTED_EVENT_MOBILE_MODULE:LX/8wV;

    .line 1422742
    new-instance v0, LX/8wV;

    const-string v1, "PROMOTE_PAGE_MOBILE_MODULE"

    const-string v2, "boosted_page_like_mobile"

    invoke-direct {v0, v1, v5, v2}, LX/8wV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wV;->PROMOTE_PAGE_MOBILE_MODULE:LX/8wV;

    .line 1422743
    new-instance v0, LX/8wV;

    const-string v1, "PROMOTE_CTA_MOBILE_MODULE"

    const-string v2, "boosted_cta_mobile"

    invoke-direct {v0, v1, v6, v2}, LX/8wV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wV;->PROMOTE_CTA_MOBILE_MODULE:LX/8wV;

    .line 1422744
    const/4 v0, 0x4

    new-array v0, v0, [LX/8wV;

    sget-object v1, LX/8wV;->BOOSTED_POST_MOBILE_MODULE:LX/8wV;

    aput-object v1, v0, v3

    sget-object v1, LX/8wV;->BOOSTED_EVENT_MOBILE_MODULE:LX/8wV;

    aput-object v1, v0, v4

    sget-object v1, LX/8wV;->PROMOTE_PAGE_MOBILE_MODULE:LX/8wV;

    aput-object v1, v0, v5

    sget-object v1, LX/8wV;->PROMOTE_CTA_MOBILE_MODULE:LX/8wV;

    aput-object v1, v0, v6

    sput-object v0, LX/8wV;->$VALUES:[LX/8wV;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1422734
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1422735
    iput-object p3, p0, LX/8wV;->module:Ljava/lang/String;

    .line 1422736
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8wV;
    .locals 1

    .prologue
    .line 1422739
    const-class v0, LX/8wV;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8wV;

    return-object v0
.end method

.method public static values()[LX/8wV;
    .locals 1

    .prologue
    .line 1422738
    sget-object v0, LX/8wV;->$VALUES:[LX/8wV;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8wV;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1422737
    iget-object v0, p0, LX/8wV;->module:Ljava/lang/String;

    return-object v0
.end method
