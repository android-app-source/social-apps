.class public final LX/A6D;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 25

    .prologue
    .line 1623884
    const/16 v21, 0x0

    .line 1623885
    const/16 v20, 0x0

    .line 1623886
    const/16 v19, 0x0

    .line 1623887
    const/16 v18, 0x0

    .line 1623888
    const/16 v17, 0x0

    .line 1623889
    const/16 v16, 0x0

    .line 1623890
    const/4 v15, 0x0

    .line 1623891
    const/4 v14, 0x0

    .line 1623892
    const/4 v13, 0x0

    .line 1623893
    const/4 v12, 0x0

    .line 1623894
    const/4 v11, 0x0

    .line 1623895
    const/4 v10, 0x0

    .line 1623896
    const/4 v9, 0x0

    .line 1623897
    const/4 v8, 0x0

    .line 1623898
    const/4 v7, 0x0

    .line 1623899
    const/4 v6, 0x0

    .line 1623900
    const/4 v5, 0x0

    .line 1623901
    const/4 v4, 0x0

    .line 1623902
    const/4 v3, 0x0

    .line 1623903
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v22

    sget-object v23, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    if-eq v0, v1, :cond_1

    .line 1623904
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1623905
    const/4 v3, 0x0

    .line 1623906
    :goto_0
    return v3

    .line 1623907
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1623908
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v22

    sget-object v23, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    if-eq v0, v1, :cond_e

    .line 1623909
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v22

    .line 1623910
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1623911
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v23

    sget-object v24, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-eq v0, v1, :cond_1

    if-eqz v22, :cond_1

    .line 1623912
    const-string v23, "composer_actions"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_2

    .line 1623913
    invoke-static/range {p0 .. p1}, LX/A69;->a(LX/15w;LX/186;)I

    move-result v21

    goto :goto_1

    .line 1623914
    :cond_2
    const-string v23, "composer_placeholder_text"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_3

    .line 1623915
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v20

    goto :goto_1

    .line 1623916
    :cond_3
    const-string v23, "customizable"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_4

    .line 1623917
    const/4 v8, 0x1

    .line 1623918
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v19

    goto :goto_1

    .line 1623919
    :cond_4
    const-string v23, "disabled_favorite_icon"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_5

    .line 1623920
    invoke-static/range {p0 .. p1}, LX/A6A;->a(LX/15w;LX/186;)I

    move-result v18

    goto :goto_1

    .line 1623921
    :cond_5
    const-string v23, "enabled_favorite_icon"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_6

    .line 1623922
    invoke-static/range {p0 .. p1}, LX/A6B;->a(LX/15w;LX/186;)I

    move-result v17

    goto :goto_1

    .line 1623923
    :cond_6
    const-string v23, "header_image"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_7

    .line 1623924
    invoke-static/range {p0 .. p1}, LX/A6C;->a(LX/15w;LX/186;)I

    move-result v16

    goto :goto_1

    .line 1623925
    :cond_7
    const-string v23, "id"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_8

    .line 1623926
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    goto/16 :goto_1

    .line 1623927
    :cond_8
    const-string v23, "is_favorited"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_9

    .line 1623928
    const/4 v7, 0x1

    .line 1623929
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v14

    goto/16 :goto_1

    .line 1623930
    :cond_9
    const-string v23, "live_video_count"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_a

    .line 1623931
    const/4 v6, 0x1

    .line 1623932
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v13

    goto/16 :goto_1

    .line 1623933
    :cond_a
    const-string v23, "name"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_b

    .line 1623934
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto/16 :goto_1

    .line 1623935
    :cond_b
    const-string v23, "searchable"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_c

    .line 1623936
    const/4 v5, 0x1

    .line 1623937
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    goto/16 :goto_1

    .line 1623938
    :cond_c
    const-string v23, "should_prefetch"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_d

    .line 1623939
    const/4 v4, 0x1

    .line 1623940
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    goto/16 :goto_1

    .line 1623941
    :cond_d
    const-string v23, "show_audience_header"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_0

    .line 1623942
    const/4 v3, 0x1

    .line 1623943
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    goto/16 :goto_1

    .line 1623944
    :cond_e
    const/16 v22, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1623945
    const/16 v22, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v22

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1623946
    const/16 v21, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1623947
    if-eqz v8, :cond_f

    .line 1623948
    const/4 v8, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 1623949
    :cond_f
    const/4 v8, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 1623950
    const/4 v8, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 1623951
    const/4 v8, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 1623952
    const/4 v8, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v15}, LX/186;->b(II)V

    .line 1623953
    if-eqz v7, :cond_10

    .line 1623954
    const/4 v7, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v14}, LX/186;->a(IZ)V

    .line 1623955
    :cond_10
    if-eqz v6, :cond_11

    .line 1623956
    const/16 v6, 0x8

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v13, v7}, LX/186;->a(III)V

    .line 1623957
    :cond_11
    const/16 v6, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v12}, LX/186;->b(II)V

    .line 1623958
    if-eqz v5, :cond_12

    .line 1623959
    const/16 v5, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v11}, LX/186;->a(IZ)V

    .line 1623960
    :cond_12
    if-eqz v4, :cond_13

    .line 1623961
    const/16 v4, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->a(IZ)V

    .line 1623962
    :cond_13
    if-eqz v3, :cond_14

    .line 1623963
    const/16 v3, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->a(IZ)V

    .line 1623964
    :cond_14
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1623965
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1623966
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1623967
    if-eqz v0, :cond_0

    .line 1623968
    const-string v1, "composer_actions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1623969
    invoke-static {p0, v0, p2, p3}, LX/A69;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1623970
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1623971
    if-eqz v0, :cond_1

    .line 1623972
    const-string v1, "composer_placeholder_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1623973
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1623974
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1623975
    if-eqz v0, :cond_2

    .line 1623976
    const-string v1, "customizable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1623977
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1623978
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1623979
    if-eqz v0, :cond_3

    .line 1623980
    const-string v1, "disabled_favorite_icon"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1623981
    invoke-static {p0, v0, p2}, LX/A6A;->a(LX/15i;ILX/0nX;)V

    .line 1623982
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1623983
    if-eqz v0, :cond_4

    .line 1623984
    const-string v1, "enabled_favorite_icon"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1623985
    invoke-static {p0, v0, p2}, LX/A6B;->a(LX/15i;ILX/0nX;)V

    .line 1623986
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1623987
    if-eqz v0, :cond_5

    .line 1623988
    const-string v1, "header_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1623989
    invoke-static {p0, v0, p2}, LX/A6C;->a(LX/15i;ILX/0nX;)V

    .line 1623990
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1623991
    if-eqz v0, :cond_6

    .line 1623992
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1623993
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1623994
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1623995
    if-eqz v0, :cond_7

    .line 1623996
    const-string v1, "is_favorited"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1623997
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1623998
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1623999
    if-eqz v0, :cond_8

    .line 1624000
    const-string v1, "live_video_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1624001
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1624002
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1624003
    if-eqz v0, :cond_9

    .line 1624004
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1624005
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1624006
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1624007
    if-eqz v0, :cond_a

    .line 1624008
    const-string v1, "searchable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1624009
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1624010
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1624011
    if-eqz v0, :cond_b

    .line 1624012
    const-string v1, "should_prefetch"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1624013
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1624014
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1624015
    if-eqz v0, :cond_c

    .line 1624016
    const-string v1, "show_audience_header"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1624017
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1624018
    :cond_c
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1624019
    return-void
.end method
