.class public final LX/AJy;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/AJz;

.field public final synthetic c:LX/AK0;


# direct methods
.method public constructor <init>(LX/AK0;ZLX/AJz;)V
    .locals 0

    .prologue
    .line 1662359
    iput-object p1, p0, LX/AJy;->c:LX/AK0;

    iput-boolean p2, p0, LX/AJy;->a:Z

    iput-object p3, p0, LX/AJy;->b:LX/AJz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0Px;LX/0Px;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/snacks/model/SnackThreadMetadata;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/snacks/model/SnackThreadMetadata;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1662360
    iget-object v0, p0, LX/AJy;->c:LX/AK0;

    iget-object v0, v0, LX/AK0;->f:LX/AKN;

    .line 1662361
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 1662362
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;

    .line 1662363
    iget-boolean v5, v1, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->c:Z

    move v5, v5

    .line 1662364
    if-eqz v5, :cond_0

    iget-object v5, v0, LX/AKN;->a:Ljava/util/Set;

    .line 1662365
    iget-object v6, v1, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->b:Ljava/lang/String;

    move-object v6, v6

    .line 1662366
    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1662367
    iget-object v5, v0, LX/AKN;->a:Ljava/util/Set;

    .line 1662368
    iget-object v6, v1, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->b:Ljava/lang/String;

    move-object v6, v6

    .line 1662369
    invoke-interface {v5, v6}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1662370
    :cond_0
    iget-object v5, v0, LX/AKN;->a:Ljava/util/Set;

    .line 1662371
    iget-object v6, v1, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->b:Ljava/lang/String;

    move-object v6, v6

    .line 1662372
    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_8

    .line 1662373
    :goto_1
    move-object v1, v1

    .line 1662374
    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1662375
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1662376
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v1, v1

    .line 1662377
    iget-object v0, p0, LX/AJy;->c:LX/AK0;

    iget-object v0, v0, LX/AK0;->i:LX/AKO;

    invoke-virtual {v0, p2}, LX/AKO;->a(LX/0Px;)LX/0Px;

    move-result-object v2

    .line 1662378
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    if-ne v0, v3, :cond_7

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1662379
    iget-object v0, p0, LX/AJy;->c:LX/AK0;

    iget-object v0, v0, LX/AK0;->e:LX/AK4;

    iget-object v3, p0, LX/AJy;->c:LX/AK0;

    invoke-static {v3, v1}, LX/AK0;->a$redex0(LX/AK0;LX/0Px;)LX/0Px;

    move-result-object v1

    .line 1662380
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1662381
    const/4 v4, 0x0

    .line 1662382
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v7

    const/4 v3, 0x0

    move v5, v3

    :goto_3
    if-ge v5, v7, :cond_3

    invoke-virtual {v1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;

    .line 1662383
    if-eqz v4, :cond_2

    .line 1662384
    iget-object p1, v3, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->d:Lcom/facebook/audience/model/AudienceControlData;

    move-object p1, p1

    .line 1662385
    invoke-virtual {p1}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_9

    .line 1662386
    :cond_2
    iget-object v4, v3, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->d:Lcom/facebook/audience/model/AudienceControlData;

    move-object v3, v4

    .line 1662387
    invoke-virtual {v3}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v3

    .line 1662388
    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1662389
    :goto_4
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move-object v4, v3

    goto :goto_3

    .line 1662390
    :cond_3
    move-object v3, v6

    .line 1662391
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 1662392
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result p1

    const/4 v4, 0x0

    move v6, v4

    :goto_5
    if-ge v6, p1, :cond_5

    invoke-virtual {v1, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;

    .line 1662393
    iget-object v5, v4, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->d:Lcom/facebook/audience/model/AudienceControlData;

    move-object v5, v5

    .line 1662394
    invoke-virtual {v5}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v5

    .line 1662395
    invoke-interface {v7, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_4

    .line 1662396
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v7, v5, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1662397
    :cond_4
    invoke-interface {v7, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1662398
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_5

    .line 1662399
    :cond_5
    move-object v4, v7

    .line 1662400
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1662401
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_6
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1662402
    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-static {v0, v3}, LX/AK4;->c(LX/AK4;Ljava/util/List;)Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;

    move-result-object v3

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 1662403
    :cond_6
    invoke-static {v5}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    move-object v0, v3

    .line 1662404
    iget-object v1, p0, LX/AJy;->c:LX/AK0;

    iget-object v1, v1, LX/AK0;->e:LX/AK4;

    iget-object v3, p0, LX/AJy;->c:LX/AK0;

    invoke-static {v3, v2}, LX/AK0;->a$redex0(LX/AK0;LX/0Px;)LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/AK4;->a(LX/0Px;)Lcom/facebook/audience/snacks/model/SnacksMyUserThread;

    move-result-object v1

    .line 1662405
    iget-object v2, p0, LX/AJy;->c:LX/AK0;

    iget-object v2, v2, LX/AK0;->g:LX/AJv;

    .line 1662406
    iput-object v0, v2, LX/AJv;->a:LX/0Px;

    .line 1662407
    iput-object v1, v2, LX/AJv;->b:Lcom/facebook/audience/snacks/model/SnacksMyUserThread;

    .line 1662408
    iget-object v2, p0, LX/AJy;->c:LX/AK0;

    iget-boolean v3, p0, LX/AJy;->a:Z

    iget-object v4, p0, LX/AJy;->b:LX/AJz;

    .line 1662409
    iget-object v5, v2, LX/AK0;->d:LX/7gh;

    invoke-virtual {v5}, LX/7gh;->a()Ljava/util/List;

    move-result-object v7

    .line 1662410
    iget-object v12, v2, LX/AK0;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v5, Lcom/facebook/audience/snacks/data/SnacksDataProvider$4;

    move-object v6, v2

    move-object v8, v0

    move-object v9, v1

    move v10, v3

    move-object v11, v4

    invoke-direct/range {v5 .. v11}, Lcom/facebook/audience/snacks/data/SnacksDataProvider$4;-><init>(LX/AK0;Ljava/util/List;LX/0Px;Lcom/facebook/audience/snacks/model/SnacksMyUserThread;ZLX/AJz;)V

    const v6, 0x74a3788e

    invoke-static {v12, v5, v6}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1662411
    return-void

    .line 1662412
    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_8
    invoke-static {v1}, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->a(Lcom/facebook/audience/snacks/model/SnackThreadMetadata;)LX/AK6;

    move-result-object v5

    const/4 v6, 0x1

    .line 1662413
    iput-boolean v6, v5, LX/AK6;->b:Z

    .line 1662414
    move-object v5, v5

    .line 1662415
    invoke-virtual {v5}, LX/AK6;->a()Lcom/facebook/audience/snacks/model/SnackThreadMetadata;

    move-result-object v1

    goto/16 :goto_1

    :cond_9
    move-object v3, v4

    goto/16 :goto_4
.end method
