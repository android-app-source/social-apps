.class public final LX/AHY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/AFM;

.field public final synthetic c:LX/AHZ;


# direct methods
.method public constructor <init>(LX/AHZ;Ljava/lang/String;LX/AFM;)V
    .locals 0

    .prologue
    .line 1655724
    iput-object p1, p0, LX/AHY;->c:LX/AHZ;

    iput-object p2, p0, LX/AHY;->a:Ljava/lang/String;

    iput-object p3, p0, LX/AHY;->b:LX/AFM;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1655722
    sget-object v0, LX/AHZ;->a:Ljava/lang/String;

    const-string v1, "Reply result not available"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1655723
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1655714
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1655715
    if-nez p1, :cond_0

    .line 1655716
    sget-object v0, LX/AHZ;->a:Ljava/lang/String;

    const-string v1, "null result."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1655717
    :goto_0
    return-void

    .line 1655718
    :cond_0
    new-instance v1, LX/AHd;

    sget-object v2, LX/AHc;->POST:LX/AHc;

    iget-object v0, p0, LX/AHY;->c:LX/AHZ;

    iget-object v3, v0, LX/AHZ;->d:Ljava/lang/String;

    iget-object v4, p0, LX/AHY;->a:Ljava/lang/String;

    .line 1655719
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1655720
    check-cast v0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;

    invoke-direct {v1, v2, v3, v4, v0}, LX/AHd;-><init>(LX/AHc;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;)V

    .line 1655721
    iget-object v0, p0, LX/AHY;->b:LX/AFM;

    invoke-virtual {v1}, LX/AHa;->a()Lcom/facebook/audience/model/ReplyThread;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/AFM;->a(Lcom/facebook/audience/model/ReplyThread;)V

    goto :goto_0
.end method
