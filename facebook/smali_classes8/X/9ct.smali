.class public final LX/9ct;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9cr;


# instance fields
.field public final synthetic a:LX/9d5;


# direct methods
.method public constructor <init>(LX/9d5;)V
    .locals 0

    .prologue
    .line 1516892
    iput-object p1, p0, LX/9ct;->a:LX/9d5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1516881
    iget-object v0, p0, LX/9ct;->a:LX/9d5;

    iget-object v0, v0, LX/9d5;->z:LX/5iG;

    if-eqz v0, :cond_0

    .line 1516882
    iget-object v0, p0, LX/9ct;->a:LX/9d5;

    iget-object v1, p0, LX/9ct;->a:LX/9d5;

    iget-object v1, v1, LX/9d5;->z:LX/5iG;

    invoke-static {v0, v1, v3}, LX/9d5;->b$redex0(LX/9d5;LX/5iG;I)V

    .line 1516883
    :cond_0
    iget-object v0, p0, LX/9ct;->a:LX/9d5;

    iget-object v0, v0, LX/9d5;->A:LX/5iG;

    if-eqz v0, :cond_1

    .line 1516884
    iget-object v0, p0, LX/9ct;->a:LX/9d5;

    iget-object v1, p0, LX/9ct;->a:LX/9d5;

    iget-object v1, v1, LX/9d5;->A:LX/5iG;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, LX/9d5;->b$redex0(LX/9d5;LX/5iG;I)V

    .line 1516885
    :cond_1
    iget-object v0, p0, LX/9ct;->a:LX/9d5;

    iget-object v0, v0, LX/9d5;->B:LX/5iG;

    if-eqz v0, :cond_2

    .line 1516886
    iget-object v0, p0, LX/9ct;->a:LX/9d5;

    iget-object v1, p0, LX/9ct;->a:LX/9d5;

    iget-object v1, v1, LX/9d5;->B:LX/5iG;

    const/4 v2, 0x2

    invoke-static {v0, v1, v2}, LX/9d5;->b$redex0(LX/9d5;LX/5iG;I)V

    .line 1516887
    :cond_2
    iget-object v0, p0, LX/9ct;->a:LX/9d5;

    .line 1516888
    iput-boolean v3, v0, LX/9d5;->x:Z

    .line 1516889
    iget-object v0, p0, LX/9ct;->a:LX/9d5;

    iget-object v0, v0, LX/9d5;->s:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->f()V

    .line 1516890
    iget-object v0, p0, LX/9ct;->a:LX/9d5;

    invoke-virtual {v0}, LX/9d5;->e()V

    .line 1516891
    return-void
.end method

.method public final a(LX/5iG;)V
    .locals 1

    .prologue
    .line 1516877
    iget-object v0, p0, LX/9ct;->a:LX/9d5;

    iget-boolean v0, v0, LX/9d5;->w:Z

    if-eqz v0, :cond_0

    .line 1516878
    iget-object v0, p0, LX/9ct;->a:LX/9d5;

    iget-object v0, v0, LX/9d5;->s:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->a()V

    .line 1516879
    iget-object v0, p0, LX/9ct;->a:LX/9d5;

    invoke-virtual {v0}, LX/9d5;->e()V

    .line 1516880
    :cond_0
    return-void
.end method

.method public final a(LX/5iG;LX/5iG;LX/5iG;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1516824
    iget-object v0, p0, LX/9ct;->a:LX/9d5;

    iget-boolean v0, v0, LX/9d5;->w:Z

    if-eqz v0, :cond_3

    .line 1516825
    iget-object v0, p0, LX/9ct;->a:LX/9d5;

    .line 1516826
    iput-object p2, v0, LX/9d5;->A:LX/5iG;

    .line 1516827
    if-eqz p1, :cond_0

    .line 1516828
    iget-object v0, p1, LX/5iG;->d:LX/1aX;

    move-object v0, v0

    .line 1516829
    if-eqz v0, :cond_4

    .line 1516830
    iget-object v0, p1, LX/5iG;->d:LX/1aX;

    move-object v0, v0

    .line 1516831
    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_0
    const-string v3, "left item\'s drawable hierarchy was not properly set up"

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1516832
    iget-object v0, p0, LX/9ct;->a:LX/9d5;

    iget-object v0, v0, LX/9d5;->s:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    invoke-virtual {p1, v0}, LX/5iG;->a(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1516833
    iget-object v0, p0, LX/9ct;->a:LX/9d5;

    invoke-static {v0, p1, v2}, LX/9d5;->a$redex0(LX/9d5;LX/5iG;I)V

    .line 1516834
    :cond_0
    if-eqz p2, :cond_1

    .line 1516835
    iget-object v0, p2, LX/5iG;->d:LX/1aX;

    move-object v0, v0

    .line 1516836
    if-eqz v0, :cond_5

    .line 1516837
    iget-object v0, p2, LX/5iG;->d:LX/1aX;

    move-object v0, v0

    .line 1516838
    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_1
    const-string v3, "center item\'s drawable hierarchy was not properly set up"

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1516839
    iget-object v0, p0, LX/9ct;->a:LX/9d5;

    iget-object v0, v0, LX/9d5;->s:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    invoke-virtual {p2, v0}, LX/5iG;->a(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1516840
    iget-object v0, p0, LX/9ct;->a:LX/9d5;

    invoke-static {v0, p2, v1}, LX/9d5;->a$redex0(LX/9d5;LX/5iG;I)V

    .line 1516841
    :cond_1
    if-eqz p3, :cond_2

    .line 1516842
    iget-object v0, p3, LX/5iG;->d:LX/1aX;

    move-object v0, v0

    .line 1516843
    if-eqz v0, :cond_6

    .line 1516844
    iget-object v0, p3, LX/5iG;->d:LX/1aX;

    move-object v0, v0

    .line 1516845
    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_6

    :goto_2
    const-string v0, "right item\'s drawable hierarchy was not properly set up"

    invoke-static {v1, v0}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1516846
    iget-object v0, p0, LX/9ct;->a:LX/9d5;

    iget-object v0, v0, LX/9d5;->s:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    invoke-virtual {p3, v0}, LX/5iG;->a(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1516847
    iget-object v0, p0, LX/9ct;->a:LX/9d5;

    const/4 v1, 0x2

    invoke-static {v0, p3, v1}, LX/9d5;->a$redex0(LX/9d5;LX/5iG;I)V

    .line 1516848
    :cond_2
    iget-object v0, p0, LX/9ct;->a:LX/9d5;

    iget-object v1, p0, LX/9ct;->a:LX/9d5;

    iget-boolean v1, v1, LX/9d5;->y:Z

    invoke-virtual {v0, v1}, LX/9d5;->b(Z)V

    .line 1516849
    :cond_3
    return-void

    :cond_4
    move v0, v2

    .line 1516850
    goto :goto_0

    :cond_5
    move v0, v2

    .line 1516851
    goto :goto_1

    :cond_6
    move v1, v2

    .line 1516852
    goto :goto_2
.end method

.method public final b(LX/5iG;LX/5iG;LX/5iG;)V
    .locals 5

    .prologue
    .line 1516853
    iget-object v0, p0, LX/9ct;->a:LX/9d5;

    iget-boolean v0, v0, LX/9d5;->w:Z

    if-eqz v0, :cond_1

    .line 1516854
    iget-object v0, p0, LX/9ct;->a:LX/9d5;

    .line 1516855
    iput-object p1, v0, LX/9d5;->z:LX/5iG;

    .line 1516856
    iget-object v0, p0, LX/9ct;->a:LX/9d5;

    .line 1516857
    iput-object p2, v0, LX/9d5;->A:LX/5iG;

    .line 1516858
    iget-object v0, p0, LX/9ct;->a:LX/9d5;

    .line 1516859
    iput-object p3, v0, LX/9d5;->B:LX/5iG;

    .line 1516860
    iget-object v0, p0, LX/9ct;->a:LX/9d5;

    invoke-static {v0}, LX/9d5;->p(LX/9d5;)V

    .line 1516861
    iget-object v0, p0, LX/9ct;->a:LX/9d5;

    iget-object v0, v0, LX/9d5;->s:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->a()V

    .line 1516862
    iget-object v0, p0, LX/9ct;->a:LX/9d5;

    iget-object v0, v0, LX/9d5;->s:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->b()V

    .line 1516863
    iget-object v0, p0, LX/9ct;->a:LX/9d5;

    iget-boolean v0, v0, LX/9d5;->N:Z

    if-eqz v0, :cond_0

    .line 1516864
    iget-object v0, p0, LX/9ct;->a:LX/9d5;

    const/4 v1, 0x0

    .line 1516865
    iput-boolean v1, v0, LX/9d5;->N:Z

    .line 1516866
    iget-object v0, p0, LX/9ct;->a:LX/9d5;

    iget-object v0, v0, LX/9d5;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8GH;

    .line 1516867
    iget-object v2, p0, LX/9ct;->a:LX/9d5;

    iget-object v2, v2, LX/9d5;->D:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    iget-object v3, p0, LX/9ct;->a:LX/9d5;

    iget-object v3, v3, LX/9d5;->L:LX/9dI;

    .line 1516868
    iget-boolean v4, v3, LX/9dI;->c:Z

    move v3, v4

    .line 1516869
    iget-object v4, p0, LX/9ct;->a:LX/9d5;

    iget v4, v4, LX/9d5;->O:I

    invoke-interface {v0, v2, v3, v4}, LX/8GH;->a(Lcom/facebook/photos/creativeediting/model/SwipeableParams;ZI)V

    goto :goto_0

    .line 1516870
    :cond_0
    iget-object v0, p0, LX/9ct;->a:LX/9d5;

    invoke-virtual {v0}, LX/9d5;->e()V

    .line 1516871
    iget-object v0, p0, LX/9ct;->a:LX/9d5;

    iget-boolean v0, v0, LX/9d5;->I:Z

    if-nez v0, :cond_1

    iget-object v0, p0, LX/9ct;->a:LX/9d5;

    iget-object v0, v0, LX/9d5;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1516872
    iget-object v0, p0, LX/9ct;->a:LX/9d5;

    const/4 v1, 0x1

    .line 1516873
    iput-boolean v1, v0, LX/9d5;->I:Z

    .line 1516874
    iget-object v0, p0, LX/9ct;->a:LX/9d5;

    iget-object v0, v0, LX/9d5;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8GH;

    .line 1516875
    iget-object v2, p0, LX/9ct;->a:LX/9d5;

    invoke-static {v2}, LX/9d5;->t(LX/9d5;)Z

    move-result v2

    invoke-interface {v0, v2}, LX/8GH;->a(Z)V

    goto :goto_1

    .line 1516876
    :cond_1
    return-void
.end method
