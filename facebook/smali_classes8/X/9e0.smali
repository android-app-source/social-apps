.class public final LX/9e0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8CF;


# instance fields
.field public final synthetic a:LX/9e1;


# direct methods
.method public constructor <init>(LX/9e1;)V
    .locals 0

    .prologue
    .line 1518906
    iput-object p1, p0, LX/9e0;->a:LX/9e1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 1518907
    iget-object v0, p0, LX/9e0;->a:LX/9e1;

    iget-object v0, v0, LX/9e1;->c:Lcom/facebook/messaging/doodle/ColourIndicator;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/doodle/ColourIndicator;->setColour(I)V

    .line 1518908
    iget-object v0, p0, LX/9e0;->a:LX/9e1;

    iget-object v0, v0, LX/9e1;->c:Lcom/facebook/messaging/doodle/ColourIndicator;

    invoke-virtual {v0}, Lcom/facebook/messaging/doodle/ColourIndicator;->a()V

    .line 1518909
    iget-object v0, p0, LX/9e0;->a:LX/9e1;

    iget-object v0, v0, LX/9e1;->b:Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->setTextColor(I)V

    .line 1518910
    iget-object v0, p0, LX/9e0;->a:LX/9e1;

    const/4 v1, 0x1

    .line 1518911
    iput-boolean v1, v0, LX/9e1;->i:Z

    .line 1518912
    return-void
.end method

.method public final a(IFFF)V
    .locals 1

    .prologue
    .line 1518913
    iget-object v0, p0, LX/9e0;->a:LX/9e1;

    iget-object v0, v0, LX/9e1;->c:Lcom/facebook/messaging/doodle/ColourIndicator;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/facebook/messaging/doodle/ColourIndicator;->a(IFFF)V

    .line 1518914
    iget-object v0, p0, LX/9e0;->a:LX/9e1;

    iget-object v0, v0, LX/9e1;->b:Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->setTextColor(I)V

    .line 1518915
    return-void
.end method
