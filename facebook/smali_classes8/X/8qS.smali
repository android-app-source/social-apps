.class public final LX/8qS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "LX/36L;",
        "Lcom/facebook/graphql/model/GraphQLActor;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8qT;


# direct methods
.method public constructor <init>(LX/8qT;)V
    .locals 0

    .prologue
    .line 1407206
    iput-object p1, p0, LX/8qS;->a:LX/8qT;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1407156
    check-cast p1, LX/36L;

    .line 1407157
    if-nez p1, :cond_0

    .line 1407158
    const/4 v0, 0x0

    .line 1407159
    :goto_0
    move-object v0, v0

    .line 1407160
    return-object v0

    .line 1407161
    :cond_0
    new-instance v0, LX/3dL;

    invoke-direct {v0}, LX/3dL;-><init>()V

    .line 1407162
    invoke-interface {p1}, LX/36L;->c()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    .line 1407163
    iput-object v1, v0, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1407164
    invoke-interface {p1}, LX/36L;->d()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    .line 1407165
    iput-object v1, v0, LX/3dL;->B:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1407166
    invoke-interface {p1}, LX/36L;->e()Ljava/lang/String;

    move-result-object v1

    .line 1407167
    iput-object v1, v0, LX/3dL;->E:Ljava/lang/String;

    .line 1407168
    invoke-interface {p1}, LX/36L;->b()Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;

    move-result-object v1

    .line 1407169
    if-nez v1, :cond_1

    .line 1407170
    const/4 v2, 0x0

    .line 1407171
    :goto_1
    move-object v1, v2

    .line 1407172
    iput-object v1, v0, LX/3dL;->af:Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    .line 1407173
    invoke-interface {p1}, LX/36L;->v_()Ljava/lang/String;

    move-result-object v1

    .line 1407174
    iput-object v1, v0, LX/3dL;->ag:Ljava/lang/String;

    .line 1407175
    invoke-interface {p1}, LX/36L;->j()Lcom/facebook/ufiservices/flyout/UFIProfileListFragmentGraphQLModels$UFIProfileListFragmentGraphQLModel$ProfilePictureModel;

    move-result-object v1

    .line 1407176
    if-nez v1, :cond_2

    .line 1407177
    const/4 v2, 0x0

    .line 1407178
    :goto_2
    move-object v1, v2

    .line 1407179
    iput-object v1, v0, LX/3dL;->as:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1407180
    invoke-interface {p1}, LX/36L;->k()I

    move-result v1

    .line 1407181
    iput v1, v0, LX/3dL;->aI:I

    .line 1407182
    invoke-interface {p1}, LX/36L;->l()Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$UnseenStoriesModel;

    move-result-object v1

    .line 1407183
    if-nez v1, :cond_3

    .line 1407184
    const/4 v2, 0x0

    .line 1407185
    :goto_3
    move-object v1, v2

    .line 1407186
    iput-object v1, v0, LX/3dL;->aJ:Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    .line 1407187
    invoke-interface {p1}, LX/36L;->m()Ljava/lang/String;

    move-result-object v1

    .line 1407188
    iput-object v1, v0, LX/3dL;->aK:Ljava/lang/String;

    .line 1407189
    invoke-virtual {v0}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    goto :goto_0

    .line 1407190
    :cond_1
    new-instance v2, LX/4XM;

    invoke-direct {v2}, LX/4XM;-><init>()V

    .line 1407191
    invoke-virtual {v1}, Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;->a()I

    move-result p0

    .line 1407192
    iput p0, v2, LX/4XM;->b:I

    .line 1407193
    invoke-virtual {v2}, LX/4XM;->a()Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    move-result-object v2

    goto :goto_1

    .line 1407194
    :cond_2
    new-instance v2, LX/2dc;

    invoke-direct {v2}, LX/2dc;-><init>()V

    .line 1407195
    invoke-virtual {v1}, Lcom/facebook/ufiservices/flyout/UFIProfileListFragmentGraphQLModels$UFIProfileListFragmentGraphQLModel$ProfilePictureModel;->a()I

    move-result p0

    .line 1407196
    iput p0, v2, LX/2dc;->c:I

    .line 1407197
    invoke-virtual {v1}, Lcom/facebook/ufiservices/flyout/UFIProfileListFragmentGraphQLModels$UFIProfileListFragmentGraphQLModel$ProfilePictureModel;->b()Ljava/lang/String;

    move-result-object p0

    .line 1407198
    iput-object p0, v2, LX/2dc;->h:Ljava/lang/String;

    .line 1407199
    invoke-virtual {v1}, Lcom/facebook/ufiservices/flyout/UFIProfileListFragmentGraphQLModels$UFIProfileListFragmentGraphQLModel$ProfilePictureModel;->c()I

    move-result p0

    .line 1407200
    iput p0, v2, LX/2dc;->i:I

    .line 1407201
    invoke-virtual {v2}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    goto :goto_2

    .line 1407202
    :cond_3
    new-instance v2, LX/4ZL;

    invoke-direct {v2}, LX/4ZL;-><init>()V

    .line 1407203
    invoke-virtual {v1}, Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$UnseenStoriesModel;->a()I

    move-result p0

    .line 1407204
    iput p0, v2, LX/4ZL;->b:I

    .line 1407205
    invoke-virtual {v2}, LX/4ZL;->a()Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    move-result-object v2

    goto :goto_3
.end method
