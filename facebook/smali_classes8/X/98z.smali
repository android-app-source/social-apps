.class public final LX/98z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/1pN;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8xc;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/fbreact/persistedqueries/Fb4aPersistedQueryRequestSender;


# direct methods
.method public constructor <init>(Lcom/facebook/fbreact/persistedqueries/Fb4aPersistedQueryRequestSender;LX/8xc;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1446595
    iput-object p1, p0, LX/98z;->c:Lcom/facebook/fbreact/persistedqueries/Fb4aPersistedQueryRequestSender;

    iput-object p2, p0, LX/98z;->a:LX/8xc;

    iput-object p3, p0, LX/98z;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1446596
    iget-object v0, p0, LX/98z;->a:LX/8xc;

    iget-object v1, p0, LX/98z;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, LX/8xc;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1446597
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1446598
    check-cast p1, LX/1pN;

    .line 1446599
    :try_start_0
    iget-object v0, p1, LX/1pN;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1446600
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;
    :try_end_0
    .catch LX/2Oo; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1446601
    iget-object v1, p0, LX/98z;->a:LX/8xc;

    iget-object v2, p0, LX/98z;->b:Ljava/lang/String;

    .line 1446602
    iget-object p0, v1, LX/8xc;->a:LX/8xd;

    invoke-static {p0, v2, v0}, LX/8xd;->a$redex0(LX/8xd;Ljava/lang/String;Ljava/lang/String;)V

    .line 1446603
    iget-object p0, v1, LX/8xc;->a:LX/8xd;

    invoke-static {p0}, LX/8xd;->b(LX/8xd;)V

    .line 1446604
    :goto_0
    return-void

    .line 1446605
    :catch_0
    move-exception v0

    .line 1446606
    iget-object v1, p0, LX/98z;->a:LX/8xc;

    iget-object v2, p0, LX/98z;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/8xc;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
