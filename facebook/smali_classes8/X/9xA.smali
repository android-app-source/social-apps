.class public final LX/9xA;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1589080
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_b

    .line 1589081
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1589082
    :goto_0
    return v1

    .line 1589083
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_7

    .line 1589084
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1589085
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1589086
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_0

    if-eqz v11, :cond_0

    .line 1589087
    const-string v12, "can_viewer_comment"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 1589088
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v4

    move v10, v4

    move v4, v2

    goto :goto_1

    .line 1589089
    :cond_1
    const-string v12, "can_viewer_like"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 1589090
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v9, v3

    move v3, v2

    goto :goto_1

    .line 1589091
    :cond_2
    const-string v12, "does_viewer_like"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 1589092
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v8, v0

    move v0, v2

    goto :goto_1

    .line 1589093
    :cond_3
    const-string v12, "id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 1589094
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 1589095
    :cond_4
    const-string v12, "legacy_api_post_id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 1589096
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 1589097
    :cond_5
    const-string v12, "likers"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 1589098
    invoke-static {p0, p1}, LX/9x9;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1589099
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1589100
    :cond_7
    const/4 v11, 0x6

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1589101
    if-eqz v4, :cond_8

    .line 1589102
    invoke-virtual {p1, v1, v10}, LX/186;->a(IZ)V

    .line 1589103
    :cond_8
    if-eqz v3, :cond_9

    .line 1589104
    invoke-virtual {p1, v2, v9}, LX/186;->a(IZ)V

    .line 1589105
    :cond_9
    if-eqz v0, :cond_a

    .line 1589106
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v8}, LX/186;->a(IZ)V

    .line 1589107
    :cond_a
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1589108
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1589109
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1589110
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_b
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    move v10, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1589111
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1589112
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1589113
    if-eqz v0, :cond_0

    .line 1589114
    const-string v1, "can_viewer_comment"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589115
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1589116
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1589117
    if-eqz v0, :cond_1

    .line 1589118
    const-string v1, "can_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589119
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1589120
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1589121
    if-eqz v0, :cond_2

    .line 1589122
    const-string v1, "does_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589123
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1589124
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1589125
    if-eqz v0, :cond_3

    .line 1589126
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589127
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1589128
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1589129
    if-eqz v0, :cond_4

    .line 1589130
    const-string v1, "legacy_api_post_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589131
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1589132
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1589133
    if-eqz v0, :cond_5

    .line 1589134
    const-string v1, "likers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589135
    invoke-static {p0, v0, p2}, LX/9x9;->a(LX/15i;ILX/0nX;)V

    .line 1589136
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1589137
    return-void
.end method
