.class public LX/9FM;
.super LX/16T;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field public static final b:Ljava/lang/String;

.field private static volatile f:LX/9FM;


# instance fields
.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/20q;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9D6;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1458520
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->UFI_LONG_PRESSED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/9FM;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 1458521
    const-class v0, LX/9FM;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/9FM;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1458493
    invoke-direct {p0}, LX/16T;-><init>()V

    .line 1458494
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1458495
    iput-object v0, p0, LX/9FM;->c:LX/0Ot;

    .line 1458496
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1458497
    iput-object v0, p0, LX/9FM;->d:LX/0Ot;

    .line 1458498
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1458499
    iput-object v0, p0, LX/9FM;->e:LX/0Ot;

    .line 1458500
    return-void
.end method

.method public static a(LX/0QB;)LX/9FM;
    .locals 6

    .prologue
    .line 1458505
    sget-object v0, LX/9FM;->f:LX/9FM;

    if-nez v0, :cond_1

    .line 1458506
    const-class v1, LX/9FM;

    monitor-enter v1

    .line 1458507
    :try_start_0
    sget-object v0, LX/9FM;->f:LX/9FM;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1458508
    if-eqz v2, :cond_0

    .line 1458509
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1458510
    new-instance v3, LX/9FM;

    invoke-direct {v3}, LX/9FM;-><init>()V

    .line 1458511
    const/16 v4, 0x798

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x1d95

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0x259

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 1458512
    iput-object v4, v3, LX/9FM;->c:LX/0Ot;

    iput-object v5, v3, LX/9FM;->d:LX/0Ot;

    iput-object p0, v3, LX/9FM;->e:LX/0Ot;

    .line 1458513
    move-object v0, v3

    .line 1458514
    sput-object v0, LX/9FM;->f:LX/9FM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1458515
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1458516
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1458517
    :cond_1
    sget-object v0, LX/9FM;->f:LX/9FM;

    return-object v0

    .line 1458518
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1458519
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 1458504
    const-wide/32 v0, 0x5265c00

    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 1

    .prologue
    .line 1458503
    iget-object v0, p0, LX/9FM;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20q;

    invoke-virtual {v0}, LX/20q;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9FM;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9D6;

    invoke-virtual {v0}, LX/9D6;->a()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1458502
    const-string v0, "4293"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1458501
    sget-object v0, LX/9FM;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
