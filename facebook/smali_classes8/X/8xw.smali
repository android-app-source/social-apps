.class public LX/8xw;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0tX;

.field public final b:LX/1Ck;

.field private final c:LX/0sa;


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;LX/0sa;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1424918
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1424919
    iput-object p1, p0, LX/8xw;->a:LX/0tX;

    .line 1424920
    iput-object p2, p0, LX/8xw;->b:LX/1Ck;

    .line 1424921
    iput-object p3, p0, LX/8xw;->c:LX/0sa;

    .line 1424922
    return-void
.end method

.method public static a(LX/8xw;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0TF;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/checkin/socialsearch/utils/ConvertToPlaceListMutator$ConversionEntryPoints;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1424923
    new-instance v0, LX/4IR;

    invoke-direct {v0}, LX/4IR;-><init>()V

    .line 1424924
    const-string v1, "story_id"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1424925
    move-object v0, v0

    .line 1424926
    const-string v1, "entrypoint"

    invoke-virtual {v0, v1, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1424927
    move-object v0, v0

    .line 1424928
    if-eqz p3, :cond_0

    .line 1424929
    const-string v1, "location_id"

    invoke-virtual {v0, v1, p3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1424930
    :cond_0
    new-instance v1, LX/5Hc;

    invoke-direct {v1}, LX/5Hc;-><init>()V

    move-object v1, v1

    .line 1424931
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1424932
    const-string v0, "angora_attachment_profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1424933
    iget-object v0, p0, LX/8xw;->b:LX/1Ck;

    const-string v2, "convert_to_place_list"

    iget-object v3, p0, LX/8xw;->a:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v3, LX/8xv;

    invoke-direct {v3, p0, p4}, LX/8xv;-><init>(LX/8xw;LX/0TF;)V

    invoke-virtual {v0, v2, v1, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1424934
    return-void
.end method

.method public static b(LX/0QB;)LX/8xw;
    .locals 4

    .prologue
    .line 1424935
    new-instance v3, LX/8xw;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v1

    check-cast v1, LX/1Ck;

    invoke-static {p0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v2

    check-cast v2, LX/0sa;

    invoke-direct {v3, v0, v1, v2}, LX/8xw;-><init>(LX/0tX;LX/1Ck;LX/0sa;)V

    .line 1424936
    return-object v3
.end method
