.class public final synthetic LX/99q;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1447806
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->values()[Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/99q;->b:[I

    :try_start_0
    sget-object v0, LX/99q;->b:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->COMMENT:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_0
    :try_start_1
    sget-object v0, LX/99q;->b:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->SHARE:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    .line 1447807
    :goto_1
    invoke-static {}, LX/99r;->values()[LX/99r;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/99q;->a:[I

    :try_start_2
    sget-object v0, LX/99q;->a:[I

    sget-object v1, LX/99r;->PivotOnClick:LX/99r;

    invoke-virtual {v1}, LX/99r;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_2
    :try_start_3
    sget-object v0, LX/99q;->a:[I

    sget-object v1, LX/99r;->PivotOnComment:LX/99r;

    invoke-virtual {v1}, LX/99r;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_3
    :try_start_4
    sget-object v0, LX/99q;->a:[I

    sget-object v1, LX/99r;->PivotOnShare:LX/99r;

    invoke-virtual {v1}, LX/99r;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_4
    return-void

    :catch_0
    goto :goto_4

    :catch_1
    goto :goto_3

    :catch_2
    goto :goto_2

    :catch_3
    goto :goto_1

    :catch_4
    goto :goto_0
.end method
