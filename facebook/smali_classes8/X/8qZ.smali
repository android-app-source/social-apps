.class public final LX/8qZ;
.super LX/8qY;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;)V
    .locals 0

    .prologue
    .line 1407429
    iput-object p1, p0, LX/8qZ;->b:Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;

    invoke-direct {p0, p1}, LX/8qY;-><init>(Lcom/facebook/widget/popover/PopoverFragment;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1407430
    iget-object v0, p0, LX/8qZ;->b:Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;

    iget-object v0, v0, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->r:LX/195;

    invoke-virtual {v0}, LX/195;->a()V

    .line 1407431
    invoke-super {p0}, LX/8qY;->a()V

    .line 1407432
    iget-object v0, p0, LX/8qZ;->b:Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;

    invoke-virtual {v0}, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->z()LX/8qC;

    move-result-object v0

    .line 1407433
    if-eqz v0, :cond_1

    .line 1407434
    iget-object v1, p0, LX/8qZ;->b:Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;

    .line 1407435
    const/high16 p0, 0x3f800000    # 1.0f

    .line 1407436
    iget-object v2, v1, Lcom/facebook/widget/popover/PopoverFragment;->p:Landroid/view/Window;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/facebook/widget/popover/PopoverFragment;->r:Landroid/view/View;

    if-nez v2, :cond_2

    .line 1407437
    :cond_0
    :goto_0
    invoke-interface {v0}, LX/8qC;->gj_()V

    .line 1407438
    :cond_1
    return-void

    .line 1407439
    :cond_2
    iget-object v2, v1, Lcom/facebook/widget/popover/PopoverFragment;->r:Landroid/view/View;

    invoke-virtual {v2, p0}, Landroid/view/View;->setScaleX(F)V

    .line 1407440
    iget-object v2, v1, Lcom/facebook/widget/popover/PopoverFragment;->r:Landroid/view/View;

    invoke-virtual {v2, p0}, Landroid/view/View;->setScaleY(F)V

    goto :goto_0
.end method

.method public final a(FFLX/31M;)Z
    .locals 1

    .prologue
    .line 1407441
    iget-object v0, p0, LX/8qZ;->b:Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;

    invoke-virtual {v0}, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->z()LX/8qC;

    move-result-object v0

    .line 1407442
    if-eqz v0, :cond_0

    .line 1407443
    invoke-interface {v0, p1, p2, p3}, LX/8qC;->a(FFLX/31M;)Z

    move-result v0

    .line 1407444
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1407445
    iget-object v0, p0, LX/8qZ;->b:Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;

    iget-object v0, v0, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->r:LX/195;

    invoke-virtual {v0}, LX/195;->b()V

    .line 1407446
    iget-object v0, p0, LX/8qZ;->b:Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;

    invoke-virtual {v0}, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->z()LX/8qC;

    move-result-object v0

    .line 1407447
    if-eqz v0, :cond_0

    .line 1407448
    invoke-interface {v0}, LX/8qC;->gk_()V

    .line 1407449
    :cond_0
    invoke-super {p0}, LX/8qY;->b()V

    .line 1407450
    iget-object v0, p0, LX/8qZ;->b:Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;

    iget-object v0, v0, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->u:LX/1nJ;

    invoke-virtual {v0}, LX/1nJ;->c()V

    .line 1407451
    return-void
.end method
