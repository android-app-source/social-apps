.class public LX/9A1;
.super Landroid/os/Handler;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile m:LX/9A1;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1EZ;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0SG;

.field private final g:LX/1CJ;

.field public final h:LX/0qn;

.field private final i:LX/0bH;

.field private final j:LX/8Mb;

.field private final k:LX/0gd;

.field private final l:LX/0kb;


# direct methods
.method public constructor <init>(LX/0bH;Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;LX/0Ot;LX/0SG;LX/1CJ;LX/0qn;LX/8Mb;LX/0gd;LX/0kb;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0bH;",
            "Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;",
            "LX/0Ot",
            "<",
            "LX/1EZ;",
            ">;",
            "LX/0SG;",
            "LX/1CJ;",
            "LX/0qn;",
            "LX/8Mb;",
            "LX/0gd;",
            "LX/0kb;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1448113
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 1448114
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/9A1;->a:Ljava/util/Map;

    .line 1448115
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/9A1;->b:Ljava/util/Map;

    .line 1448116
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/9A1;->c:Ljava/util/Set;

    .line 1448117
    iput-object p1, p0, LX/9A1;->i:LX/0bH;

    .line 1448118
    iput-object p2, p0, LX/9A1;->d:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    .line 1448119
    iput-object p3, p0, LX/9A1;->e:LX/0Ot;

    .line 1448120
    iput-object p4, p0, LX/9A1;->f:LX/0SG;

    .line 1448121
    iput-object p5, p0, LX/9A1;->g:LX/1CJ;

    .line 1448122
    iput-object p6, p0, LX/9A1;->h:LX/0qn;

    .line 1448123
    iput-object p7, p0, LX/9A1;->j:LX/8Mb;

    .line 1448124
    iput-object p8, p0, LX/9A1;->k:LX/0gd;

    .line 1448125
    iput-object p9, p0, LX/9A1;->l:LX/0kb;

    .line 1448126
    return-void
.end method

.method public static a(LX/9A1;Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 1448111
    iget-object v0, p0, LX/9A1;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1448112
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/9A1;
    .locals 13

    .prologue
    .line 1448065
    sget-object v0, LX/9A1;->m:LX/9A1;

    if-nez v0, :cond_1

    .line 1448066
    const-class v1, LX/9A1;

    monitor-enter v1

    .line 1448067
    :try_start_0
    sget-object v0, LX/9A1;->m:LX/9A1;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1448068
    if-eqz v2, :cond_0

    .line 1448069
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1448070
    new-instance v3, LX/9A1;

    invoke-static {v0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v4

    check-cast v4, LX/0bH;

    invoke-static {v0}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a(LX/0QB;)Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    move-result-object v5

    check-cast v5, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    const/16 v6, 0xf39

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    invoke-static {v0}, LX/1CJ;->a(LX/0QB;)LX/1CJ;

    move-result-object v8

    check-cast v8, LX/1CJ;

    invoke-static {v0}, LX/0qn;->a(LX/0QB;)LX/0qn;

    move-result-object v9

    check-cast v9, LX/0qn;

    invoke-static {v0}, LX/8Mb;->b(LX/0QB;)LX/8Mb;

    move-result-object v10

    check-cast v10, LX/8Mb;

    invoke-static {v0}, LX/0gd;->a(LX/0QB;)LX/0gd;

    move-result-object v11

    check-cast v11, LX/0gd;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v12

    check-cast v12, LX/0kb;

    invoke-direct/range {v3 .. v12}, LX/9A1;-><init>(LX/0bH;Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;LX/0Ot;LX/0SG;LX/1CJ;LX/0qn;LX/8Mb;LX/0gd;LX/0kb;)V

    .line 1448071
    move-object v0, v3

    .line 1448072
    sput-object v0, LX/9A1;->m:LX/9A1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1448073
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1448074
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1448075
    :cond_1
    sget-object v0, LX/9A1;->m:LX/9A1;

    return-object v0

    .line 1448076
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1448077
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a()Landroid/view/View$OnTouchListener;
    .locals 1

    .prologue
    .line 1448110
    new-instance v0, LX/9A0;

    invoke-direct {v0}, LX/9A0;-><init>()V

    return-object v0
.end method

.method public static b(LX/9A1;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1448104
    iget-object v0, p0, LX/9A1;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1448105
    iget-object v0, p0, LX/9A1;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1448106
    iget-object v0, p0, LX/9A1;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 1448107
    if-eqz v0, :cond_0

    .line 1448108
    invoke-static {p0, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1448109
    :cond_0
    return-void
.end method

.method public static g(LX/9A1;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 2

    .prologue
    .line 1448102
    iget-object v0, p0, LX/9A1;->i:LX/0bH;

    new-instance v1, LX/1Ne;

    invoke-direct {v1, p1}, LX/1Ne;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1448103
    return-void
.end method

.method public static h(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1448127
    if-nez p0, :cond_0

    .line 1448128
    const/4 v0, 0x0

    .line 1448129
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;J)V
    .locals 4

    .prologue
    const/4 v0, 0x3

    .line 1448095
    invoke-virtual {p0, v0, p1}, LX/9A1;->removeMessages(ILjava/lang/Object;)V

    .line 1448096
    invoke-virtual {p0, v0, p1}, LX/9A1;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 1448097
    new-instance v1, Landroid/os/Bundle;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/os/Bundle;-><init>(I)V

    .line 1448098
    const-string v2, "story_key"

    invoke-static {v1, v2, p1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1448099
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1448100
    invoke-virtual {p0, v0, p2, p3}, LX/9A1;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1448101
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;LX/5Ro;)V
    .locals 2
    .param p1    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1448091
    iget-object v0, p0, LX/9A1;->h:LX/0qn;

    invoke-virtual {v0, p1}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v0

    .line 1448092
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->SUCCESS:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v0, v1, :cond_0

    .line 1448093
    iget-object v0, p0, LX/9A1;->j:LX/8Mb;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, LX/8Mb;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/5Ro;Z)V

    .line 1448094
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/ref/WeakReference;J)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Landroid/view/View;",
            ":",
            "LX/9A2;",
            ">(",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Ljava/lang/ref/WeakReference",
            "<TV;>;J)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 1448084
    invoke-virtual {p0, v2, p2}, LX/9A1;->removeMessages(ILjava/lang/Object;)V

    .line 1448085
    invoke-virtual {p0, v2, p2}, LX/9A1;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 1448086
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1, v2}, Landroid/os/Bundle;-><init>(I)V

    .line 1448087
    const-string v2, "story_key"

    invoke-static {v1, v2, p1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1448088
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1448089
    invoke-virtual {p0, v0, p3, p4}, LX/9A1;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1448090
    return-void
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLStory;LX/5Ro;)Z
    .locals 2

    .prologue
    .line 1448078
    iget-object v0, p0, LX/9A1;->l:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1448079
    invoke-virtual {p0, p1, p2}, LX/9A1;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/5Ro;)V

    .line 1448080
    const/4 v0, 0x1

    .line 1448081
    :goto_0
    return v0

    .line 1448082
    :cond_0
    iget-object v0, p0, LX/9A1;->k:LX/0gd;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, LX/0gd;->a(Ljava/lang/String;LX/5Ro;)V

    .line 1448083
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 1

    .prologue
    .line 1448063
    iget-object v0, p0, LX/9A1;->j:LX/8Mb;

    invoke-virtual {v0, p1}, LX/8Mb;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1448064
    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 12

    .prologue
    .line 1448002
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "story_key"

    invoke-static {v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1448003
    iget-object v1, p0, LX/9A1;->g:LX/1CJ;

    invoke-virtual {v1, v0}, LX/1CJ;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/4Zb;

    move-result-object v4

    .line 1448004
    iget-object v1, p0, LX/9A1;->d:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->d(Ljava/lang/String;)Lcom/facebook/composer/publish/common/PendingStory;

    move-result-object v2

    .line 1448005
    iget-object v1, p0, LX/9A1;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1EZ;

    invoke-virtual {v1}, LX/1EZ;->b()V

    .line 1448006
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 1448007
    :cond_0
    :goto_0
    return-void

    .line 1448008
    :pswitch_0
    if-nez v2, :cond_1

    .line 1448009
    iget-object v1, p0, LX/9A1;->h:LX/0qn;

    invoke-virtual {v1, v0}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->SUCCESS:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v1, v2, :cond_0

    .line 1448010
    invoke-static {p0, v0}, LX/9A1;->g(LX/9A1;Lcom/facebook/graphql/model/GraphQLStory;)V

    goto :goto_0

    .line 1448011
    :cond_1
    iget-object v1, p0, LX/9A1;->h:LX/0qn;

    invoke-virtual {v1, v0}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->FAILED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v1, v3, :cond_2

    invoke-virtual {v2}, Lcom/facebook/composer/publish/common/PendingStory;->j()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1448012
    iget-object v1, p0, LX/9A1;->f:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v4

    invoke-static {v0}, LX/17E;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    invoke-virtual {v2, v4, v5, v1}, Lcom/facebook/composer/publish/common/PendingStory;->b(JZ)V

    .line 1448013
    :cond_2
    invoke-static {p0, v0}, LX/9A1;->g(LX/9A1;Lcom/facebook/graphql/model/GraphQLStory;)V

    goto :goto_0

    .line 1448014
    :pswitch_1
    if-nez v2, :cond_4

    .line 1448015
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 1448016
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9A2;

    .line 1448017
    :goto_1
    if-eqz v0, :cond_0

    .line 1448018
    invoke-interface {v0}, LX/9A2;->a()V

    goto :goto_0

    .line 1448019
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 1448020
    :cond_4
    iget-object v1, p0, LX/9A1;->h:LX/0qn;

    invoke-virtual {v1, v0}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->TRANSCODING_FAILED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-ne v1, v3, :cond_5

    .line 1448021
    invoke-static {p0, v0}, LX/9A1;->g(LX/9A1;Lcom/facebook/graphql/model/GraphQLStory;)V

    goto :goto_0

    .line 1448022
    :cond_5
    invoke-virtual {v2}, Lcom/facebook/composer/publish/common/PendingStory;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v8

    if-nez v8, :cond_f

    .line 1448023
    :cond_6
    :goto_2
    iget-object v1, p0, LX/9A1;->h:LX/0qn;

    invoke-virtual {v1, v0}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->FAILED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v1, v3, :cond_7

    invoke-virtual {v2}, Lcom/facebook/composer/publish/common/PendingStory;->j()Z

    move-result v1

    if-nez v1, :cond_7

    .line 1448024
    iget-object v1, p0, LX/9A1;->f:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v6

    invoke-static {v0}, LX/17E;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    invoke-virtual {v2, v6, v7, v1}, Lcom/facebook/composer/publish/common/PendingStory;->b(JZ)V

    .line 1448025
    :cond_7
    invoke-virtual {v2}, Lcom/facebook/composer/publish/common/PendingStory;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-static {v1}, LX/9A1;->h(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v3

    .line 1448026
    iget-object v1, p0, LX/9A1;->f:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Lcom/facebook/composer/publish/common/PendingStory;->a(J)I

    move-result v5

    .line 1448027
    invoke-static {p0, v3}, LX/9A1;->a(LX/9A1;Ljava/lang/String;)I

    move-result v1

    .line 1448028
    if-le v5, v1, :cond_8

    .line 1448029
    iget-object v1, p0, LX/9A1;->b:Ljava/util/Map;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v1, v3, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1448030
    iget-object v1, p0, LX/9A1;->a:Ljava/util/Map;

    invoke-interface {v1, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 1448031
    iget-object v1, p0, LX/9A1;->a:Ljava/util/Map;

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    .line 1448032
    :goto_3
    invoke-static {p0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1448033
    const/16 v6, 0x3e8

    if-ge v5, v6, :cond_c

    .line 1448034
    const-wide/32 v6, 0xea60

    const v3, -0x17813fe6

    invoke-static {p0, v1, v6, v7, v3}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1448035
    :cond_8
    :goto_4
    invoke-virtual {v2}, Lcom/facebook/composer/publish/common/PendingStory;->k()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1448036
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/ref/WeakReference;

    .line 1448037
    if-eqz v1, :cond_0

    .line 1448038
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/9A2;

    .line 1448039
    if-eqz v2, :cond_e

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v3

    .line 1448040
    iget-object v5, v4, LX/4Zb;->b:Ljava/util/Set;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    move v3, v5

    .line 1448041
    if-eqz v3, :cond_e

    .line 1448042
    iget-object v3, p0, LX/9A1;->e:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1EZ;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/1EZ;->e(Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v3

    .line 1448043
    if-eqz v3, :cond_9

    .line 1448044
    iget-object v5, p0, LX/9A1;->l:LX/0kb;

    invoke-virtual {v5}, LX/0kb;->v()Z

    move-result v5

    if-nez v5, :cond_d

    .line 1448045
    iget-boolean v5, v3, Lcom/facebook/photos/upload/operation/UploadOperation;->au:Z

    move v3, v5

    .line 1448046
    if-eqz v3, :cond_d

    const/4 v3, 0x1

    :goto_5
    invoke-interface {v2, v3}, LX/9A2;->setStoryIsWaitingForWifi(Z)V

    .line 1448047
    :cond_9
    invoke-interface {v2, v0}, LX/9A2;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1448048
    :goto_6
    iget-object v0, v4, LX/4Zb;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_10

    const/4 v0, 0x1

    :goto_7
    move v0, v0

    .line 1448049
    if-eqz v0, :cond_a

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1448050
    :cond_a
    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v0

    const-wide/16 v2, 0x32

    invoke-virtual {p0, v0, v2, v3}, LX/9A1;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    .line 1448051
    :cond_b
    invoke-virtual {v2}, Lcom/facebook/composer/publish/common/PendingStory;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 1448052
    new-instance v6, Lcom/facebook/feed/util/composer/OfflinePostHeaderController$1;

    invoke-direct {v6, p0, v1}, Lcom/facebook/feed/util/composer/OfflinePostHeaderController$1;-><init>(LX/9A1;Lcom/facebook/graphql/model/GraphQLStory;)V

    move-object v1, v6

    .line 1448053
    iget-object v6, p0, LX/9A1;->a:Ljava/util/Map;

    invoke-interface {v6, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_3

    .line 1448054
    :cond_c
    invoke-static {p0, v3}, LX/9A1;->b(LX/9A1;Ljava/lang/String;)V

    goto :goto_4

    .line 1448055
    :cond_d
    const/4 v3, 0x0

    goto :goto_5

    .line 1448056
    :cond_e
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->clear()V

    goto :goto_6

    .line 1448057
    :pswitch_2
    sget-object v1, LX/5Ro;->AUTOMATIC:LX/5Ro;

    invoke-virtual {p0, v0, v1}, LX/9A1;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/5Ro;)V

    goto/16 :goto_0

    .line 1448058
    :cond_f
    invoke-virtual {v2}, Lcom/facebook/composer/publish/common/PendingStory;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v8

    invoke-static {v8}, LX/9A1;->h(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v8

    .line 1448059
    invoke-static {p0, v8}, LX/9A1;->a(LX/9A1;Ljava/lang/String;)I

    move-result v9

    .line 1448060
    iget-object v10, p0, LX/9A1;->f:LX/0SG;

    invoke-interface {v10}, LX/0SG;->a()J

    move-result-wide v10

    invoke-virtual {v2, v10, v11}, Lcom/facebook/composer/publish/common/PendingStory;->a(J)I

    move-result v10

    if-ge v9, v10, :cond_6

    .line 1448061
    iget-object v9, p0, LX/9A1;->c:Ljava/util/Set;

    invoke-interface {v9, v8}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 1448062
    invoke-static {p0, v0}, LX/9A1;->g(LX/9A1;Lcom/facebook/graphql/model/GraphQLStory;)V

    goto/16 :goto_2

    :cond_10
    const/4 v0, 0x0

    goto :goto_7

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
