.class public LX/AJo;
.super LX/AJl;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1662151
    invoke-direct {p0}, LX/AJl;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/3sU;)V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 1662149
    invoke-virtual {p1, v1}, LX/3sU;->d(F)LX/3sU;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/3sU;->e(F)LX/3sU;

    .line 1662150
    return-void
.end method

.method public final a(LX/3sU;II)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1662144
    if-eqz p2, :cond_0

    .line 1662145
    invoke-virtual {p1, v0}, LX/3sU;->b(F)LX/3sU;

    .line 1662146
    :cond_0
    if-eqz p3, :cond_1

    .line 1662147
    invoke-virtual {p1, v0}, LX/3sU;->c(F)LX/3sU;

    .line 1662148
    :cond_1
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1662141
    invoke-static {p1, v0}, LX/0vv;->d(Landroid/view/View;F)V

    .line 1662142
    invoke-static {p1, v0}, LX/0vv;->e(Landroid/view/View;F)V

    .line 1662143
    return-void
.end method

.method public final a(Landroid/view/View;II)V
    .locals 1

    .prologue
    .line 1662136
    if-eqz p2, :cond_0

    .line 1662137
    neg-int v0, p2

    int-to-float v0, v0

    invoke-static {p1, v0}, LX/0vv;->a(Landroid/view/View;F)V

    .line 1662138
    :cond_0
    if-eqz p3, :cond_1

    .line 1662139
    neg-int v0, p3

    int-to-float v0, v0

    invoke-static {p1, v0}, LX/0vv;->b(Landroid/view/View;F)V

    .line 1662140
    :cond_1
    return-void
.end method

.method public final b(LX/3sU;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1662117
    invoke-virtual {p1, v1}, LX/3sU;->d(F)LX/3sU;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/3sU;->e(F)LX/3sU;

    .line 1662118
    return-void
.end method

.method public final b(Landroid/view/View;)V
    .locals 1

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 1662133
    invoke-static {p1, v0}, LX/0vv;->d(Landroid/view/View;F)V

    .line 1662134
    invoke-static {p1, v0}, LX/0vv;->e(Landroid/view/View;F)V

    .line 1662135
    return-void
.end method

.method public final b(Landroid/view/View;II)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1662128
    if-eqz p2, :cond_0

    .line 1662129
    invoke-static {p1, v0}, LX/0vv;->a(Landroid/view/View;F)V

    .line 1662130
    :cond_0
    if-eqz p3, :cond_1

    .line 1662131
    invoke-static {p1, v0}, LX/0vv;->b(Landroid/view/View;F)V

    .line 1662132
    :cond_1
    return-void
.end method

.method public final c(Landroid/view/View;)V
    .locals 1

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 1662125
    invoke-static {p1, v0}, LX/0vv;->d(Landroid/view/View;F)V

    .line 1662126
    invoke-static {p1, v0}, LX/0vv;->e(Landroid/view/View;F)V

    .line 1662127
    return-void
.end method

.method public final d(Landroid/view/View;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1662122
    invoke-static {p1, v0}, LX/0vv;->d(Landroid/view/View;F)V

    .line 1662123
    invoke-static {p1, v0}, LX/0vv;->e(Landroid/view/View;F)V

    .line 1662124
    return-void
.end method

.method public final e(Landroid/view/View;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1662119
    invoke-static {p1, v0}, LX/0vv;->a(Landroid/view/View;F)V

    .line 1662120
    invoke-static {p1, v0}, LX/0vv;->b(Landroid/view/View;F)V

    .line 1662121
    return-void
.end method
