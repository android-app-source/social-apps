.class public LX/8zZ;
.super LX/5Jl;
.source ""


# instance fields
.field private final a:LX/1P0;

.field private final b:LX/8zW;

.field private final c:LX/90B;

.field private final d:LX/90D;

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLInterfaces$TaggableObjectEdge;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/5LG;

.field public g:I

.field public h:Z

.field public i:Ljava/lang/Runnable;

.field public j:LX/5Lv;


# direct methods
.method public constructor <init>(LX/1P0;LX/90B;LX/90D;LX/5LG;LX/8zW;Landroid/content/Context;)V
    .locals 1
    .param p1    # LX/1P0;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/90B;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/90D;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/5LG;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1427671
    invoke-direct {p0, p6, p1}, LX/5Jl;-><init>(Landroid/content/Context;LX/1P1;)V

    .line 1427672
    iput-object p1, p0, LX/8zZ;->a:LX/1P0;

    .line 1427673
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1427674
    iput-object v0, p0, LX/8zZ;->e:LX/0Px;

    .line 1427675
    iput-object p4, p0, LX/8zZ;->f:LX/5LG;

    .line 1427676
    iput-object p5, p0, LX/8zZ;->b:LX/8zW;

    .line 1427677
    iput-object p2, p0, LX/8zZ;->c:LX/90B;

    .line 1427678
    iput-object p3, p0, LX/8zZ;->d:LX/90D;

    .line 1427679
    const/4 v0, -0x1

    iput v0, p0, LX/8zZ;->g:I

    .line 1427680
    new-instance v0, LX/5Lv;

    invoke-direct {v0}, LX/5Lv;-><init>()V

    iput-object v0, p0, LX/8zZ;->j:LX/5Lv;

    .line 1427681
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1427682
    iget-object v0, p0, LX/8zZ;->a:LX/1P0;

    invoke-virtual {v0}, LX/1P1;->l()I

    move-result v0

    return v0
.end method

.method public final a(LX/1De;I)LX/1X1;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "I)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1427656
    iget-object v0, p0, LX/8zZ;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lt p2, v0, :cond_0

    iget-boolean v0, p0, LX/8zZ;->h:Z

    if-eqz v0, :cond_0

    .line 1427657
    new-instance v0, LX/8zY;

    invoke-direct {v0, p0}, LX/8zY;-><init>(LX/8zZ;)V

    .line 1427658
    invoke-static {p1}, LX/91S;->c(LX/1De;)LX/91R;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/91R;->a(Landroid/view/View$OnClickListener;)LX/91R;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 1427659
    :goto_0
    return-object v0

    .line 1427660
    :cond_0
    iget-object v0, p0, LX/8zZ;->e:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    .line 1427661
    iget v1, p0, LX/8zZ;->g:I

    if-ne p2, v1, :cond_1

    .line 1427662
    iget-object v1, p0, LX/8zZ;->b:LX/8zW;

    invoke-virtual {v1, p1}, LX/8zW;->c(LX/1De;)LX/8zU;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/8zU;->a(Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;)LX/8zU;

    move-result-object v0

    iget-object v1, p0, LX/8zZ;->f:LX/5LG;

    invoke-virtual {v0, v1}, LX/8zU;->a(LX/5LG;)LX/8zU;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/8zU;->a(Z)LX/8zU;

    move-result-object v0

    iget-object v1, p0, LX/8zZ;->d:LX/90D;

    .line 1427663
    iget-object p0, v0, LX/8zU;->a:LX/8zV;

    iput-object v1, p0, LX/8zV;->e:LX/90D;

    .line 1427664
    move-object v0, v0

    .line 1427665
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    goto :goto_0

    .line 1427666
    :cond_1
    iget-object v1, p0, LX/8zZ;->b:LX/8zW;

    invoke-virtual {v1, p1}, LX/8zW;->c(LX/1De;)LX/8zU;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/8zU;->a(Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;)LX/8zU;

    move-result-object v0

    iget-object v1, p0, LX/8zZ;->f:LX/5LG;

    invoke-virtual {v0, v1}, LX/8zU;->a(LX/5LG;)LX/8zU;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/8zU;->a(Z)LX/8zU;

    move-result-object v0

    iget-object v1, p0, LX/8zZ;->c:LX/90B;

    .line 1427667
    iget-object p0, v0, LX/8zU;->a:LX/8zV;

    iput-object v1, p0, LX/8zV;->d:LX/90B;

    .line 1427668
    move-object v0, v0

    .line 1427669
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    goto :goto_0
.end method

.method public final e()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1427653
    iget-object v1, p0, LX/8zZ;->e:LX/0Px;

    if-eqz v1, :cond_1

    .line 1427654
    iget-object v1, p0, LX/8zZ;->e:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    iget-boolean v2, p0, LX/8zZ;->h:Z

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    add-int/2addr v0, v1

    .line 1427655
    :cond_1
    return v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 1427670
    const/4 v0, 0x1

    return v0
.end method

.method public final k()LX/1OR;
    .locals 1

    .prologue
    .line 1427652
    iget-object v0, p0, LX/8zZ;->a:LX/1P0;

    return-object v0
.end method

.method public final l()I
    .locals 1

    .prologue
    .line 1427651
    iget-object v0, p0, LX/8zZ;->j:LX/5Lv;

    invoke-virtual {v0}, LX/5Lv;->b()I

    move-result v0

    return v0
.end method

.method public final n()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1427644
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 1427645
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, LX/8zZ;->j:LX/5Lv;

    invoke-virtual {v2}, LX/5Lv;->b()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 1427646
    iget-object v2, p0, LX/8zZ;->j:LX/5Lv;

    invoke-virtual {v2, v0}, LX/5Lv;->a(I)Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->e()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1427647
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1427648
    :cond_0
    iget v0, p0, LX/8zZ;->g:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    .line 1427649
    const-string v0, "0"

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1427650
    :cond_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method
