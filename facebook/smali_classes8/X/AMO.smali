.class public LX/AMO;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/AMW;

.field public final c:Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1666154
    const-class v0, LX/AMO;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AMO;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/AMW;Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1666150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1666151
    iput-object p1, p0, LX/AMO;->b:LX/AMW;

    .line 1666152
    iput-object p2, p0, LX/AMO;->c:Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;

    .line 1666153
    return-void
.end method

.method public static b(LX/0QB;)LX/AMO;
    .locals 4

    .prologue
    .line 1666122
    new-instance v2, LX/AMO;

    .line 1666123
    new-instance v3, LX/AMW;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v1

    check-cast v1, LX/1Ck;

    invoke-direct {v3, v0, v1}, LX/AMW;-><init>(LX/0tX;LX/1Ck;)V

    .line 1666124
    move-object v0, v3

    .line 1666125
    check-cast v0, LX/AMW;

    invoke-static {p0}, Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;->b(LX/0QB;)Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;

    move-result-object v1

    check-cast v1, Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;

    invoke-direct {v2, v0, v1}, LX/AMO;-><init>(LX/AMW;Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;)V

    .line 1666126
    return-object v2
.end method

.method public static b(Ljava/util/concurrent/atomic/AtomicInteger;Ljava/util/concurrent/atomic/AtomicInteger;LX/0Pz;LX/0Pz;LX/AMN;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/atomic/AtomicInteger;",
            "Ljava/util/concurrent/atomic/AtomicInteger;",
            "LX/0Pz",
            "<",
            "LX/AN1;",
            ">;",
            "LX/0Pz",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/AMN;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1666141
    invoke-virtual {p0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-nez v0, :cond_1

    .line 1666142
    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    .line 1666143
    if-lez v0, :cond_0

    .line 1666144
    sget-object v1, LX/AMO;->a:Ljava/lang/String;

    const-string v2, "%d downloads failed"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1666145
    :cond_0
    invoke-virtual {p3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/AN2;->a(LX/0Px;)LX/AN2;

    move-result-object v0

    .line 1666146
    if-eqz v0, :cond_2

    .line 1666147
    invoke-virtual {p2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-interface {p4, v1, v0}, LX/AMN;->a(LX/0Px;LX/AN2;)V

    .line 1666148
    :cond_1
    :goto_0
    return-void

    .line 1666149
    :cond_2
    new-instance v0, Ljava/lang/Throwable;

    const-string v1, "Invalid set of support assets returned"

    invoke-direct {v0, v1}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-interface {p4, v0}, LX/AMN;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1666155
    iget-object v0, p0, LX/AMO;->b:LX/AMW;

    .line 1666156
    iget-object v1, v0, LX/AMW;->b:LX/1Ck;

    sget-object v2, LX/AMV;->FETCH_DOWNLOADABLE_MSQRD_MASKS:LX/AMV;

    invoke-virtual {v1, v2}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 1666157
    iget-object v0, p0, LX/AMO;->c:Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;

    .line 1666158
    iget-object v1, v0, Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;->c:LX/1Ck;

    sget-object v2, LX/AMh;->DOWNLOAD_MSQRD_MASKS:LX/AMh;

    invoke-virtual {v1, v2}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 1666159
    iget-object v1, v0, Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;->d:LX/AMf;

    .line 1666160
    iget-object v2, v1, LX/AMf;->b:LX/1Ck;

    sget-object v0, LX/AMd;->UNZIP_TO_CACHE:LX/AMd;

    invoke-virtual {v2, v0}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 1666161
    return-void
.end method

.method public final a(LX/0Px;LX/AMN;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/AMT;",
            ">;",
            "LX/AMN;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1666139
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/AMO;->a(LX/0Px;LX/AMN;LX/AZo;)V

    .line 1666140
    return-void
.end method

.method public final a(LX/0Px;LX/AMN;LX/AZo;)V
    .locals 12
    .param p3    # LX/AZo;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/AMT;",
            ">;",
            "LX/AMN;",
            "Lcom/facebook/cameracore/assets/MsqrdMaskAssetManager$ProgressCallback;",
            ")V"
        }
    .end annotation

    .prologue
    const-wide/16 v10, 0x0

    .line 1666127
    new-instance v6, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    invoke-direct {v6, v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 1666128
    new-instance v8, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v0, 0x0

    invoke-direct {v8, v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 1666129
    const/4 v0, 0x0

    .line 1666130
    if-eqz p3, :cond_0

    .line 1666131
    new-instance v4, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v4, v10, v11}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    .line 1666132
    new-instance v3, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v3, v10, v11}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    .line 1666133
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v2

    .line 1666134
    new-instance v0, LX/AMK;

    move-object v1, p0

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LX/AMK;-><init>(LX/AMO;Ljava/util/Map;Ljava/util/concurrent/atomic/AtomicLong;Ljava/util/concurrent/atomic/AtomicLong;LX/AZo;)V

    move-object v7, v0

    .line 1666135
    :goto_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1666136
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1666137
    iget-object v9, p0, LX/AMO;->c:Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;

    new-instance v0, LX/AMM;

    move-object v1, p0

    move-object v4, v6

    move-object v5, v8

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, LX/AMM;-><init>(LX/AMO;LX/0Pz;LX/0Pz;Ljava/util/concurrent/atomic/AtomicInteger;Ljava/util/concurrent/atomic/AtomicInteger;LX/AMN;)V

    invoke-virtual {v9, p1, v0, v7}, Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;->a(LX/0Px;LX/AML;LX/AMK;)V

    .line 1666138
    return-void

    :cond_0
    move-object v7, v0

    goto :goto_0
.end method

.method public final a(ZLjava/lang/String;LX/AMN;LX/AZo;)V
    .locals 10
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/AZo;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1666105
    iget-object v0, p0, LX/AMO;->b:LX/AMW;

    new-instance v1, LX/AMJ;

    invoke-direct {v1, p0, p3, p4}, LX/AMJ;-><init>(LX/AMO;LX/AMN;LX/AZo;)V

    .line 1666106
    if-eqz p1, :cond_1

    sget-object v2, LX/0zS;->d:LX/0zS;

    .line 1666107
    :goto_0
    iget-object v3, v0, LX/AMW;->a:LX/0tX;

    .line 1666108
    new-instance v6, LX/AMl;

    invoke-direct {v6}, LX/AMl;-><init>()V

    move-object v6, v6

    .line 1666109
    const-string v7, "mask_model_version"

    const-wide/16 v8, 0x5

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1666110
    const-string v7, "mask_sdk_version"

    const/4 v8, 0x4

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1666111
    const-string v7, "mask_capabilities"

    invoke-static {}, LX/8Cg;->a()Ljava/util/List;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 1666112
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1666113
    const-string v7, "mask_name_contains"

    invoke-virtual {v6, v7, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1666114
    :cond_0
    invoke-static {v6}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v6

    invoke-virtual {v6, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v6

    const-wide/16 v8, 0x384

    invoke-virtual {v6, v8, v9}, LX/0zO;->a(J)LX/0zO;

    move-result-object v6

    const/4 v7, 0x1

    .line 1666115
    iput-boolean v7, v6, LX/0zO;->p:Z

    .line 1666116
    move-object v6, v6

    .line 1666117
    move-object v2, v6

    .line 1666118
    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    .line 1666119
    iget-object v3, v0, LX/AMW;->b:LX/1Ck;

    sget-object v4, LX/AMV;->FETCH_DOWNLOADABLE_MSQRD_MASKS:LX/AMV;

    new-instance v5, LX/AMU;

    invoke-direct {v5, v0, v1}, LX/AMU;-><init>(LX/AMW;LX/AMJ;)V

    invoke-virtual {v3, v4, v2, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1666120
    return-void

    .line 1666121
    :cond_1
    sget-object v2, LX/0zS;->a:LX/0zS;

    goto :goto_0
.end method

.method public final c(LX/0Px;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/AMT;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1666098
    iget-object v0, p0, LX/AMO;->c:Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;

    const/4 v2, 0x0

    .line 1666099
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result p0

    move v3, v2

    :goto_0
    if-ge v3, p0, :cond_1

    invoke-virtual {p1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/AMT;

    .line 1666100
    invoke-static {v0, v1}, Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;->b(Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;LX/AMT;)Ljava/io/File;

    move-result-object v1

    if-nez v1, :cond_0

    move v1, v2

    .line 1666101
    :goto_1
    move v0, v1

    .line 1666102
    return v0

    .line 1666103
    :cond_0
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    .line 1666104
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method
