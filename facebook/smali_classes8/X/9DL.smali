.class public LX/9DL;
.super LX/5pb;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "FBCommentGroupCommercePredictiveComments"
.end annotation


# instance fields
.field private final a:LX/9Cv;


# direct methods
.method public constructor <init>(LX/5pY;LX/9Cv;)V
    .locals 0
    .param p1    # LX/5pY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1455524
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 1455525
    iput-object p2, p0, LX/9DL;->a:LX/9Cv;

    .line 1455526
    return-void
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1455527
    const-string v0, "FBCommentGroupCommercePredictiveComments"

    return-object v0
.end method

.method public predictiveCommentSelected(Ljava/lang/String;I)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1455528
    iget-object v0, p0, LX/9DL;->a:LX/9Cv;

    .line 1455529
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1455530
    const-string p0, "CommentInlineBannerListener.SET_INPUT_TEXT"

    invoke-virtual {v1, p0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1455531
    const-string p0, "extra_input_text"

    invoke-virtual {v1, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1455532
    const-string p0, "extra_react_tag"

    invoke-virtual {v1, p0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1455533
    iget-object p0, v0, LX/9Cv;->a:LX/0Xl;

    invoke-interface {p0, v1}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 1455534
    return-void
.end method
