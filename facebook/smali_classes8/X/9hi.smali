.class public LX/9hi;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1526826
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLPlace;
    .locals 2
    .param p0    # Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1526827
    if-eqz p0, :cond_0

    .line 1526828
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/9hi;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    .line 1526829
    :goto_0
    return-object v0

    .line 1526830
    :cond_0
    if-eqz p1, :cond_1

    .line 1526831
    const-string v0, "0"

    invoke-static {v0, p1}, LX/9hi;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    goto :goto_0

    .line 1526832
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLPlace;
    .locals 1

    .prologue
    .line 1526833
    new-instance v0, LX/4Y6;

    invoke-direct {v0}, LX/4Y6;-><init>()V

    .line 1526834
    iput-object p0, v0, LX/4Y6;->n:Ljava/lang/String;

    .line 1526835
    move-object v0, v0

    .line 1526836
    iput-object p1, v0, LX/4Y6;->r:Ljava/lang/String;

    .line 1526837
    move-object v0, v0

    .line 1526838
    invoke-virtual {v0}, LX/4Y6;->a()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Ljava/lang/String;)Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;
    .locals 2
    .param p0    # Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1526839
    if-eqz p0, :cond_0

    .line 1526840
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/9hi;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;

    move-result-object v0

    .line 1526841
    :goto_0
    return-object v0

    .line 1526842
    :cond_0
    if-eqz p1, :cond_1

    .line 1526843
    const-string v0, "0"

    invoke-static {v0, p1}, LX/9hi;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;

    move-result-object v0

    goto :goto_0

    .line 1526844
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;
    .locals 9

    .prologue
    .line 1526845
    new-instance v0, LX/5kQ;

    invoke-direct {v0}, LX/5kQ;-><init>()V

    .line 1526846
    iput-object p0, v0, LX/5kQ;->b:Ljava/lang/String;

    .line 1526847
    move-object v0, v0

    .line 1526848
    iput-object p1, v0, LX/5kQ;->c:Ljava/lang/String;

    .line 1526849
    move-object v0, v0

    .line 1526850
    const/4 v5, 0x1

    const/4 v8, 0x0

    const/4 v3, 0x0

    .line 1526851
    new-instance v1, LX/186;

    const/16 v2, 0x80

    invoke-direct {v1, v2}, LX/186;-><init>(I)V

    .line 1526852
    iget-object v2, v0, LX/5kQ;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1526853
    iget-object v4, v0, LX/5kQ;->b:Ljava/lang/String;

    invoke-virtual {v1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1526854
    iget-object v6, v0, LX/5kQ;->c:Ljava/lang/String;

    invoke-virtual {v1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1526855
    const/4 v7, 0x3

    invoke-virtual {v1, v7}, LX/186;->c(I)V

    .line 1526856
    invoke-virtual {v1, v8, v2}, LX/186;->b(II)V

    .line 1526857
    invoke-virtual {v1, v5, v4}, LX/186;->b(II)V

    .line 1526858
    const/4 v2, 0x2

    invoke-virtual {v1, v2, v6}, LX/186;->b(II)V

    .line 1526859
    invoke-virtual {v1}, LX/186;->d()I

    move-result v2

    .line 1526860
    invoke-virtual {v1, v2}, LX/186;->d(I)V

    .line 1526861
    invoke-virtual {v1}, LX/186;->e()[B

    move-result-object v1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 1526862
    invoke-virtual {v2, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1526863
    new-instance v1, LX/15i;

    move-object v4, v3

    move-object v6, v3

    invoke-direct/range {v1 .. v6}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1526864
    new-instance v2, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;

    invoke-direct {v2, v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;-><init>(LX/15i;)V

    .line 1526865
    move-object v0, v2

    .line 1526866
    return-object v0
.end method

.method public static c(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Ljava/lang/String;)Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$PendingPlaceModel;
    .locals 2
    .param p0    # Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1526867
    if-eqz p0, :cond_0

    .line 1526868
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/9hi;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$PendingPlaceModel;

    move-result-object v0

    .line 1526869
    :goto_0
    return-object v0

    .line 1526870
    :cond_0
    if-eqz p1, :cond_1

    .line 1526871
    const-string v0, "0"

    invoke-static {v0, p1}, LX/9hi;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$PendingPlaceModel;

    move-result-object v0

    goto :goto_0

    .line 1526872
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$PendingPlaceModel;
    .locals 9

    .prologue
    .line 1526873
    new-instance v0, LX/5kT;

    invoke-direct {v0}, LX/5kT;-><init>()V

    .line 1526874
    iput-object p0, v0, LX/5kT;->b:Ljava/lang/String;

    .line 1526875
    move-object v0, v0

    .line 1526876
    iput-object p1, v0, LX/5kT;->c:Ljava/lang/String;

    .line 1526877
    move-object v0, v0

    .line 1526878
    const/4 v5, 0x1

    const/4 v8, 0x0

    const/4 v3, 0x0

    .line 1526879
    new-instance v1, LX/186;

    const/16 v2, 0x80

    invoke-direct {v1, v2}, LX/186;-><init>(I)V

    .line 1526880
    iget-object v2, v0, LX/5kT;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1526881
    iget-object v4, v0, LX/5kT;->b:Ljava/lang/String;

    invoke-virtual {v1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1526882
    iget-object v6, v0, LX/5kT;->c:Ljava/lang/String;

    invoke-virtual {v1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1526883
    const/4 v7, 0x3

    invoke-virtual {v1, v7}, LX/186;->c(I)V

    .line 1526884
    invoke-virtual {v1, v8, v2}, LX/186;->b(II)V

    .line 1526885
    invoke-virtual {v1, v5, v4}, LX/186;->b(II)V

    .line 1526886
    const/4 v2, 0x2

    invoke-virtual {v1, v2, v6}, LX/186;->b(II)V

    .line 1526887
    invoke-virtual {v1}, LX/186;->d()I

    move-result v2

    .line 1526888
    invoke-virtual {v1, v2}, LX/186;->d(I)V

    .line 1526889
    invoke-virtual {v1}, LX/186;->e()[B

    move-result-object v1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 1526890
    invoke-virtual {v2, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1526891
    new-instance v1, LX/15i;

    move-object v4, v3

    move-object v6, v3

    invoke-direct/range {v1 .. v6}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1526892
    new-instance v2, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$PendingPlaceModel;

    invoke-direct {v2, v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$PendingPlaceModel;-><init>(LX/15i;)V

    .line 1526893
    move-object v0, v2

    .line 1526894
    return-object v0
.end method
