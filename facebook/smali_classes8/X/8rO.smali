.class public LX/8rO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

.field public b:I

.field public c:Ljava/lang/Boolean;

.field public d:Z

.field public e:I

.field public f:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

.field public g:I

.field public h:LX/8rM;


# direct methods
.method public constructor <init>(LX/8rM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1408840
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1408841
    iput-object p1, p0, LX/8rO;->h:LX/8rM;

    .line 1408842
    invoke-virtual {p0}, LX/8rO;->c()V

    .line 1408843
    return-void
.end method

.method public static a(LX/8rO;I)Z
    .locals 1

    .prologue
    .line 1408820
    iget v0, p0, LX/8rO;->e:I

    if-gt v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLComposedBlockType;)Z
    .locals 1

    .prologue
    .line 1408839
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->UNORDERED_LIST_ITEM:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->ORDERED_LIST_ITEM:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final c()V
    .locals 2

    .prologue
    const v1, 0x7fffffff

    .line 1408828
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    iput-object v0, p0, LX/8rO;->a:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    .line 1408829
    const/4 v0, -0x1

    iput v0, p0, LX/8rO;->b:I

    .line 1408830
    const/4 v0, 0x0

    iput-object v0, p0, LX/8rO;->c:Ljava/lang/Boolean;

    .line 1408831
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/8rO;->d:Z

    .line 1408832
    iput v1, p0, LX/8rO;->e:I

    .line 1408833
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    iput-object v0, p0, LX/8rO;->f:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    .line 1408834
    iput v1, p0, LX/8rO;->g:I

    .line 1408835
    iget-object v0, p0, LX/8rO;->h:LX/8rM;

    const/4 p0, 0x0

    .line 1408836
    const/4 v1, -0x1

    iput v1, v0, LX/8rM;->d:I

    .line 1408837
    iget-object v1, v0, LX/8rM;->c:[I

    aput p0, v1, p0

    .line 1408838
    return-void
.end method

.method public final f()I
    .locals 2

    .prologue
    .line 1408821
    iget-object v0, p0, LX/8rO;->h:LX/8rM;

    invoke-virtual {v0}, LX/8rM;->c()I

    move-result v0

    rem-int/lit8 v0, v0, 0x3

    .line 1408822
    packed-switch v0, :pswitch_data_0

    .line 1408823
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bad count of unordered list styles."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1408824
    :pswitch_0
    const/4 v0, 0x1

    .line 1408825
    :goto_0
    return v0

    .line 1408826
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 1408827
    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
