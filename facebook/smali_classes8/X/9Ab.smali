.class public final LX/9Ab;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/9Ac;


# direct methods
.method public constructor <init>(LX/9Ac;)V
    .locals 0

    .prologue
    .line 1450231
    iput-object p1, p0, LX/9Ab;->a:LX/9Ac;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/62M;III)V
    .locals 1

    .prologue
    .line 1450232
    iget-object v0, p0, LX/9Ab;->a:LX/9Ac;

    iget-object v0, v0, LX/9Ac;->d:LX/1Jr;

    .line 1450233
    iget-object p0, v0, LX/1Jr;->a:LX/1Jp;

    iget-boolean p0, p0, LX/1Jp;->i:Z

    const-string p1, "Listeners must be registered before calling onScroll"

    invoke-static {p0, p1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1450234
    iget-object p0, v0, LX/1Jr;->a:LX/1Jp;

    iget-object p0, p0, LX/1Jp;->h:LX/A8f;

    const/4 p1, -0x1

    .line 1450235
    if-nez p2, :cond_1

    if-nez p3, :cond_1

    .line 1450236
    iput p1, p0, LX/A8f;->c:I

    .line 1450237
    iput p1, p0, LX/A8f;->d:I

    .line 1450238
    :goto_0
    iget-object p0, v0, LX/1Jr;->a:LX/1Jp;

    iget-object p0, p0, LX/1Jp;->d:LX/1Ju;

    iget-object p1, v0, LX/1Jr;->a:LX/1Jp;

    iget-object p1, p1, LX/1Jp;->h:LX/A8f;

    invoke-virtual {p0, p1}, LX/1Ju;->a(LX/9Ad;)V

    .line 1450239
    iget-object p0, v0, LX/1Jr;->a:LX/1Jp;

    iget-object p0, p0, LX/1Jp;->c:LX/0ad;

    invoke-static {p0}, LX/1Sr;->b(LX/0ad;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 1450240
    iget-object p0, v0, LX/1Jr;->a:LX/1Jp;

    iget-object p0, p0, LX/1Jp;->g:LX/9Af;

    iget-object p1, v0, LX/1Jr;->a:LX/1Jp;

    iget-object p1, p1, LX/1Jp;->k:LX/1Jt;

    .line 1450241
    invoke-static {p2, p3}, LX/9Af;->c(II)Z

    move-result p4

    if-eqz p4, :cond_2

    const/4 p4, 0x0

    .line 1450242
    :goto_1
    invoke-static {p0, p2, p4, p1}, LX/9Af;->c(LX/9Af;IILX/1Jt;)V

    .line 1450243
    iget-object p0, v0, LX/1Jr;->a:LX/1Jp;

    iget-object p0, p0, LX/1Jp;->e:LX/1Ju;

    iget-object p1, v0, LX/1Jr;->a:LX/1Jp;

    iget-object p1, p1, LX/1Jp;->g:LX/9Af;

    invoke-virtual {p0, p1}, LX/1Ju;->a(LX/9Ad;)V

    .line 1450244
    :cond_0
    return-void

    .line 1450245
    :cond_1
    iput p2, p0, LX/A8f;->c:I

    .line 1450246
    add-int p1, p2, p3

    add-int/lit8 p1, p1, -0x1

    iput p1, p0, LX/A8f;->d:I

    .line 1450247
    invoke-static {p0}, LX/A8f;->e(LX/A8f;)V

    goto :goto_0

    .line 1450248
    :cond_2
    add-int p4, p2, p3

    add-int/lit8 p4, p4, -0x1

    goto :goto_1
.end method
