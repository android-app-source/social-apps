.class public final LX/9QA;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1486610
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 1486611
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1486612
    :goto_0
    return v1

    .line 1486613
    :cond_0
    const-string v8, "length"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1486614
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v5, v3

    move v3, v2

    .line 1486615
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_4

    .line 1486616
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1486617
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1486618
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 1486619
    const-string v8, "entity"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1486620
    const/4 v7, 0x0

    .line 1486621
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v6, v8, :cond_d

    .line 1486622
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1486623
    :goto_2
    move v6, v7

    .line 1486624
    goto :goto_1

    .line 1486625
    :cond_2
    const-string v8, "offset"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1486626
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 1486627
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1486628
    :cond_4
    const/4 v7, 0x3

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1486629
    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 1486630
    if-eqz v3, :cond_5

    .line 1486631
    invoke-virtual {p1, v2, v5, v1}, LX/186;->a(III)V

    .line 1486632
    :cond_5
    if-eqz v0, :cond_6

    .line 1486633
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v4, v1}, LX/186;->a(III)V

    .line 1486634
    :cond_6
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto :goto_1

    .line 1486635
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1486636
    :cond_9
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_c

    .line 1486637
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1486638
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1486639
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_9

    if-eqz v9, :cond_9

    .line 1486640
    const-string v10, "__type__"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_a

    const-string v10, "__typename"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 1486641
    :cond_a
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v8

    goto :goto_3

    .line 1486642
    :cond_b
    const-string v10, "url"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 1486643
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_3

    .line 1486644
    :cond_c
    const/4 v9, 0x2

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1486645
    invoke-virtual {p1, v7, v8}, LX/186;->b(II)V

    .line 1486646
    const/4 v7, 0x1

    invoke-virtual {p1, v7, v6}, LX/186;->b(II)V

    .line 1486647
    invoke-virtual {p1}, LX/186;->d()I

    move-result v7

    goto/16 :goto_2

    :cond_d
    move v6, v7

    move v8, v7

    goto :goto_3
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1486648
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1486649
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1486650
    if-eqz v0, :cond_2

    .line 1486651
    const-string v1, "entity"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1486652
    const/4 p3, 0x0

    .line 1486653
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1486654
    invoke-virtual {p0, v0, p3}, LX/15i;->g(II)I

    move-result v1

    .line 1486655
    if-eqz v1, :cond_0

    .line 1486656
    const-string v1, "__type__"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1486657
    invoke-static {p0, v0, p3, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1486658
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1486659
    if-eqz v1, :cond_1

    .line 1486660
    const-string p3, "url"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1486661
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1486662
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1486663
    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1486664
    if-eqz v0, :cond_3

    .line 1486665
    const-string v1, "length"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1486666
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1486667
    :cond_3
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1486668
    if-eqz v0, :cond_4

    .line 1486669
    const-string v1, "offset"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1486670
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1486671
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1486672
    return-void
.end method
