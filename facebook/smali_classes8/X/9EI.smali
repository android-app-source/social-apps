.class public LX/9EI;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public b:Landroid/widget/EditText;

.field public c:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

.field public d:Landroid/view/View$OnFocusChangeListener;

.field public e:Landroid/text/TextWatcher;

.field public f:LX/8nB;

.field public g:Lcom/facebook/tagging/model/TaggingProfile;

.field public h:Ljava/lang/Long;


# direct methods
.method public constructor <init>(Landroid/widget/EditText;)V
    .locals 2

    .prologue
    .line 1456843
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1456844
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/9EI;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 1456845
    instance-of v0, p1, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    if-eqz v0, :cond_0

    .line 1456846
    iget-object v0, p0, LX/9EI;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1456847
    check-cast p1, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    iput-object p1, p0, LX/9EI;->c:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    .line 1456848
    :goto_0
    return-void

    .line 1456849
    :cond_0
    iput-object p1, p0, LX/9EI;->b:Landroid/widget/EditText;

    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 1456850
    iget-object v0, p0, LX/9EI;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1456851
    iget-object v0, p0, LX/9EI;->c:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    .line 1456852
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/9EI;->b:Landroid/widget/EditText;

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1456853
    iget-object v0, p0, LX/9EI;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1456854
    iget-object v0, p0, LX/9EI;->c:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getEncodedText()Ljava/lang/String;

    move-result-object v0

    .line 1456855
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/9EI;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
