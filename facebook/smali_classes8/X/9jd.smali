.class public LX/9jd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:LX/6Wc;

.field public static final b:LX/6Wc;

.field public static final c:LX/6Wc;

.field public static final d:LX/6Wc;

.field public static final e:LX/6Wc;


# instance fields
.field public final f:LX/6Wr;

.field public final g:LX/0TD;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1530295
    new-instance v0, LX/6Wc;

    const-string v1, "abbr_name"

    invoke-direct {v0, v1}, LX/6Wc;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/9jd;->a:LX/6Wc;

    .line 1530296
    new-instance v0, LX/6Wc;

    const-string v1, "type"

    invoke-direct {v0, v1}, LX/6Wc;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/9jd;->b:LX/6Wc;

    .line 1530297
    new-instance v0, LX/6Wc;

    const-string v1, "page_fbid"

    invoke-direct {v0, v1}, LX/6Wc;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/9jd;->c:LX/6Wc;

    .line 1530298
    new-instance v0, LX/6Wc;

    const-string v1, "latitude"

    invoke-direct {v0, v1}, LX/6Wc;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/9jd;->d:LX/6Wc;

    .line 1530299
    new-instance v0, LX/6Wc;

    const-string v1, "longitude"

    invoke-direct {v0, v1}, LX/6Wc;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/9jd;->e:LX/6Wc;

    return-void
.end method

.method public constructor <init>(LX/6Wr;LX/0TD;)V
    .locals 0
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1530300
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1530301
    iput-object p1, p0, LX/9jd;->f:LX/6Wr;

    .line 1530302
    iput-object p2, p0, LX/9jd;->g:LX/0TD;

    .line 1530303
    return-void
.end method
