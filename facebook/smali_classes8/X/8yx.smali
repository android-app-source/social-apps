.class public LX/8yx;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1426742
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1426743
    return-void
.end method

.method public static a(LX/1De;IILX/1X1;I)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/1X1",
            "<*>;I)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1426744
    new-instance v2, LX/1no;

    invoke-direct {v2}, LX/1no;-><init>()V

    .line 1426745
    if-nez p4, :cond_2

    .line 1426746
    invoke-static {p1}, LX/1mh;->a(I)I

    move-result v3

    .line 1426747
    if-nez v3, :cond_1

    .line 1426748
    :cond_0
    :goto_0
    return v0

    .line 1426749
    :cond_1
    invoke-static {v1, v1}, LX/1mh;->a(II)I

    move-result v3

    invoke-virtual {p3, p0, v3, p2, v2}, LX/1X1;->a(LX/1De;IILX/1no;)V

    .line 1426750
    iget v2, v2, LX/1no;->a:I

    invoke-static {p1}, LX/1mh;->b(I)I

    move-result v3

    if-le v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    .line 1426751
    :cond_2
    if-ne p4, v0, :cond_3

    .line 1426752
    invoke-static {p2}, LX/1mh;->a(I)I

    move-result v3

    .line 1426753
    if-eqz v3, :cond_0

    .line 1426754
    invoke-static {v1, v1}, LX/1mh;->a(II)I

    move-result v3

    invoke-virtual {p3, p0, p1, v3, v2}, LX/1X1;->a(LX/1De;IILX/1no;)V

    .line 1426755
    iget v2, v2, LX/1no;->b:I

    invoke-static {p2}, LX/1mh;->b(I)I

    move-result v3

    if-le v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    .line 1426756
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Supplied dimension "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was unexpected"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
