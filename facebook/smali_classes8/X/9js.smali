.class public final LX/9js;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/9jN;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/9ju;


# direct methods
.method public constructor <init>(LX/9ju;)V
    .locals 0

    .prologue
    .line 1530495
    iput-object p1, p0, LX/9js;->a:LX/9ju;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1530496
    iget-object v0, p0, LX/9js;->a:LX/9ju;

    iget-object v0, v0, LX/9ju;->j:LX/90X;

    invoke-interface {v0}, LX/90X;->a()V

    .line 1530497
    sget-object v0, LX/9ju;->a:Ljava/lang/String;

    const-string v1, "Error getting checkin history"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1530498
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1530499
    check-cast p1, LX/9jN;

    .line 1530500
    iget-object v0, p0, LX/9js;->a:LX/9ju;

    iget-object v0, v0, LX/9ju;->d:LX/9j5;

    const/4 v1, 0x0

    .line 1530501
    iget-boolean v2, v0, LX/9j5;->q:Z

    if-nez v2, :cond_0

    .line 1530502
    iget-object v2, v0, LX/9j5;->a:LX/0Zb;

    const-string v3, "place_picker_past_places_shown_in_main_list"

    invoke-static {v0, v3}, LX/9j5;->h(LX/9j5;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "is_most_visited"

    invoke-virtual {v3, v4, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1530503
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, v0, LX/9j5;->q:Z

    .line 1530504
    iget-object v0, p0, LX/9js;->a:LX/9ju;

    iget-object v0, v0, LX/9ju;->b:LX/9j7;

    invoke-virtual {v0}, LX/9j7;->a()V

    .line 1530505
    iget-object v0, p0, LX/9js;->a:LX/9ju;

    iget-object v0, v0, LX/9ju;->j:LX/90X;

    invoke-interface {v0, p1}, LX/90X;->a(LX/9jN;)V

    .line 1530506
    iget-object v0, p0, LX/9js;->a:LX/9ju;

    iget-object v0, v0, LX/9ju;->j:LX/90X;

    invoke-interface {v0}, LX/90X;->a()V

    .line 1530507
    return-void
.end method
