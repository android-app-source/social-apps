.class public final LX/9Wm;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 1501643
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 1501644
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1501645
    :goto_0
    return v6

    .line 1501646
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_4

    .line 1501647
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1501648
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1501649
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_0

    if-eqz v10, :cond_0

    .line 1501650
    const-string v11, "is_opted_in"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 1501651
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v7

    move v9, v7

    move v7, v1

    goto :goto_1

    .line 1501652
    :cond_1
    const-string v11, "opted_in_time"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 1501653
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v0, v1

    goto :goto_1

    .line 1501654
    :cond_2
    const-string v11, "owner_id"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 1501655
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 1501656
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1501657
    :cond_4
    const/4 v10, 0x3

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1501658
    if-eqz v7, :cond_5

    .line 1501659
    invoke-virtual {p1, v6, v9}, LX/186;->a(IZ)V

    .line 1501660
    :cond_5
    if-eqz v0, :cond_6

    move-object v0, p1

    .line 1501661
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1501662
    :cond_6
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1501663
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto :goto_0

    :cond_7
    move v0, v6

    move v7, v6

    move v8, v6

    move-wide v2, v4

    move v9, v6

    goto :goto_1
.end method
