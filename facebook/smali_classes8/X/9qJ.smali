.class public final LX/9qJ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 22

    .prologue
    .line 1546749
    const/16 v18, 0x0

    .line 1546750
    const/16 v17, 0x0

    .line 1546751
    const/16 v16, 0x0

    .line 1546752
    const/4 v15, 0x0

    .line 1546753
    const/4 v14, 0x0

    .line 1546754
    const/4 v13, 0x0

    .line 1546755
    const/4 v12, 0x0

    .line 1546756
    const/4 v11, 0x0

    .line 1546757
    const/4 v10, 0x0

    .line 1546758
    const/4 v9, 0x0

    .line 1546759
    const/4 v8, 0x0

    .line 1546760
    const/4 v7, 0x0

    .line 1546761
    const/4 v6, 0x0

    .line 1546762
    const/4 v5, 0x0

    .line 1546763
    const/4 v4, 0x0

    .line 1546764
    const/4 v3, 0x0

    .line 1546765
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_1

    .line 1546766
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1546767
    const/4 v3, 0x0

    .line 1546768
    :goto_0
    return v3

    .line 1546769
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1546770
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_c

    .line 1546771
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v19

    .line 1546772
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1546773
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v20

    sget-object v21, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-eq v0, v1, :cond_1

    if-eqz v19, :cond_1

    .line 1546774
    const-string v20, "core_attribute_alignment"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_2

    .line 1546775
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v18

    goto :goto_1

    .line 1546776
    :cond_2
    const-string v20, "core_attribute_boldness"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_3

    .line 1546777
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v17

    goto :goto_1

    .line 1546778
    :cond_3
    const-string v20, "core_attribute_chevron"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_4

    .line 1546779
    const/4 v7, 0x1

    .line 1546780
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v16

    goto :goto_1

    .line 1546781
    :cond_4
    const-string v20, "core_attribute_color"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_5

    .line 1546782
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    goto :goto_1

    .line 1546783
    :cond_5
    const-string v20, "core_attribute_line_spacing"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_6

    .line 1546784
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionCoreTextLineSpacing;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v14

    goto/16 :goto_1

    .line 1546785
    :cond_6
    const-string v20, "core_attribute_line_wrap"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_7

    .line 1546786
    const/4 v6, 0x1

    .line 1546787
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v13

    goto/16 :goto_1

    .line 1546788
    :cond_7
    const-string v20, "core_attribute_text_size"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_8

    .line 1546789
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v12

    goto/16 :goto_1

    .line 1546790
    :cond_8
    const-string v20, "core_attribute_truncation_lines"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_9

    .line 1546791
    const/4 v5, 0x1

    .line 1546792
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v11

    goto/16 :goto_1

    .line 1546793
    :cond_9
    const-string v20, "core_attribute_truncation_min_lines"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_a

    .line 1546794
    const/4 v4, 0x1

    .line 1546795
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v10

    goto/16 :goto_1

    .line 1546796
    :cond_a
    const-string v20, "core_attribute_truncation_text"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_b

    .line 1546797
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto/16 :goto_1

    .line 1546798
    :cond_b
    const-string v20, "core_attribute_uppercase_transform"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_0

    .line 1546799
    const/4 v3, 0x1

    .line 1546800
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v8

    goto/16 :goto_1

    .line 1546801
    :cond_c
    const/16 v19, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1546802
    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1546803
    const/16 v18, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1546804
    if-eqz v7, :cond_d

    .line 1546805
    const/4 v7, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 1546806
    :cond_d
    const/4 v7, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v15}, LX/186;->b(II)V

    .line 1546807
    const/4 v7, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v14}, LX/186;->b(II)V

    .line 1546808
    if-eqz v6, :cond_e

    .line 1546809
    const/4 v6, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v13}, LX/186;->a(IZ)V

    .line 1546810
    :cond_e
    const/4 v6, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v12}, LX/186;->b(II)V

    .line 1546811
    if-eqz v5, :cond_f

    .line 1546812
    const/4 v5, 0x7

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v11, v6}, LX/186;->a(III)V

    .line 1546813
    :cond_f
    if-eqz v4, :cond_10

    .line 1546814
    const/16 v4, 0x8

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10, v5}, LX/186;->a(III)V

    .line 1546815
    :cond_10
    const/16 v4, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v9}, LX/186;->b(II)V

    .line 1546816
    if-eqz v3, :cond_11

    .line 1546817
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->a(IZ)V

    .line 1546818
    :cond_11
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 5

    .prologue
    const/4 v4, 0x6

    const/4 v3, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1546819
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1546820
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1546821
    if-eqz v0, :cond_0

    .line 1546822
    const-string v0, "core_attribute_alignment"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1546823
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1546824
    :cond_0
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1546825
    if-eqz v0, :cond_1

    .line 1546826
    const-string v0, "core_attribute_boldness"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1546827
    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1546828
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1546829
    if-eqz v0, :cond_2

    .line 1546830
    const-string v1, "core_attribute_chevron"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1546831
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1546832
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1546833
    if-eqz v0, :cond_3

    .line 1546834
    const-string v1, "core_attribute_color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1546835
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1546836
    :cond_3
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1546837
    if-eqz v0, :cond_4

    .line 1546838
    const-string v0, "core_attribute_line_spacing"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1546839
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1546840
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1546841
    if-eqz v0, :cond_5

    .line 1546842
    const-string v1, "core_attribute_line_wrap"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1546843
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1546844
    :cond_5
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 1546845
    if-eqz v0, :cond_6

    .line 1546846
    const-string v0, "core_attribute_text_size"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1546847
    invoke-virtual {p0, p1, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1546848
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1546849
    if-eqz v0, :cond_7

    .line 1546850
    const-string v1, "core_attribute_truncation_lines"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1546851
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1546852
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1546853
    if-eqz v0, :cond_8

    .line 1546854
    const-string v1, "core_attribute_truncation_min_lines"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1546855
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1546856
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1546857
    if-eqz v0, :cond_9

    .line 1546858
    const-string v1, "core_attribute_truncation_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1546859
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1546860
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1546861
    if-eqz v0, :cond_a

    .line 1546862
    const-string v1, "core_attribute_uppercase_transform"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1546863
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1546864
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1546865
    return-void
.end method
