.class public LX/8hw;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1391070
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->USERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/8hw;->a:LX/0Rf;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1391068
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0Uh;LX/0ad;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1391069
    sget-object v1, LX/8hw;->a:LX/0Rf;

    invoke-virtual {v1, p2}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-short v1, LX/100;->F:S

    invoke-interface {p1, v1, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    sget v1, LX/2SU;->C:I

    invoke-virtual {p0, v1, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :cond_2
    return v0
.end method
