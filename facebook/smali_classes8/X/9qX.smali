.class public interface abstract LX/9qX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9qV;


# virtual methods
.method public abstract b()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract c()Lcom/facebook/graphql/enums/GraphQLReactionUnitCollapseState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract d()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract e()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract gV_()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract gW_()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract k()I
.end method

.method public abstract l()Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract m()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract n()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract o()LX/1U8;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method
