.class public LX/AJU;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1661186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel;)LX/0Px;
    .locals 13
    .param p0    # Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/model/AudienceControlData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1661187
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel;->a()Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel$DirectShareSheetRankedFriendsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel;->a()Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel$DirectShareSheetRankedFriendsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel$DirectShareSheetRankedFriendsModel;->a()LX/0Px;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1661188
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1661189
    :goto_0
    return-object v0

    .line 1661190
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 1661191
    const/4 v1, 0x1

    .line 1661192
    invoke-virtual {p0}, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel;->a()Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel$DirectShareSheetRankedFriendsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel$DirectShareSheetRankedFriendsModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    .line 1661193
    const/4 v0, 0x0

    move v2, v1

    move v1, v0

    :goto_1
    if-ge v1, v6, :cond_2

    invoke-virtual {v5, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel$DirectShareSheetRankedFriendsModel$NodesModel;

    .line 1661194
    add-int/lit8 v3, v2, 0x1

    const/4 p0, 0x0

    .line 1661195
    invoke-virtual {v0}, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel$DirectShareSheetRankedFriendsModel$NodesModel;->l()LX/1vs;

    move-result-object v7

    iget-object v8, v7, LX/1vs;->a:LX/15i;

    iget v7, v7, LX/1vs;->b:I

    .line 1661196
    invoke-virtual {v0}, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel$DirectShareSheetRankedFriendsModel$NodesModel;->l()LX/1vs;

    move-result-object v9

    iget-object v10, v9, LX/1vs;->a:LX/15i;

    iget v9, v9, LX/1vs;->b:I

    .line 1661197
    invoke-static {}, Lcom/facebook/audience/model/AudienceControlData;->newBuilder()Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v11

    invoke-virtual {v0}, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel$DirectShareSheetRankedFriendsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setId(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v11

    invoke-virtual {v0}, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel$DirectShareSheetRankedFriendsModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setName(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v11

    invoke-virtual {v8, v7, p0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v11, v7}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setProfileUri(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v7

    invoke-virtual {v10, v9, p0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setLowResProfileUri(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v7

    invoke-virtual {v7, v2}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setRanking(I)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v7

    sget-object v8, LX/7gf;->RANKED_FRIENDS:LX/7gf;

    invoke-virtual {v7, v8}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setAudienceType(LX/7gf;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/audience/model/AudienceControlData$Builder;->a()Lcom/facebook/audience/model/AudienceControlData;

    move-result-object v7

    move-object v0, v7

    .line 1661198
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1661199
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v2, v3

    goto :goto_1

    .line 1661200
    :cond_2
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method
