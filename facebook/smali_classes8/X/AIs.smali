.class public LX/AIs;
.super LX/AIq;
.source ""


# instance fields
.field private final m:LX/0fO;

.field private n:Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;

.field private o:Lcom/facebook/audience/sharesheet/ui/SharesheetPrivacyView;


# direct methods
.method public constructor <init>(LX/0fO;Landroid/view/View;)V
    .locals 2
    .param p2    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1660443
    invoke-direct {p0, p2}, LX/AIq;-><init>(Landroid/view/View;)V

    .line 1660444
    iput-object p1, p0, LX/AIs;->m:LX/0fO;

    .line 1660445
    const v0, 0x7f0d2c58

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;

    iput-object v0, p0, LX/AIs;->n:Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;

    .line 1660446
    iget-object v0, p0, LX/AIs;->n:Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;

    invoke-virtual {v0, p2}, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;->setParentForHeightAnimation(Landroid/view/View;)V

    .line 1660447
    iget-object v0, p0, LX/AIs;->n:Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;

    const v1, 0x7f0d2c5b

    invoke-virtual {v0, v1}, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/sharesheet/ui/SharesheetPrivacyView;

    iput-object v0, p0, LX/AIs;->o:Lcom/facebook/audience/sharesheet/ui/SharesheetPrivacyView;

    .line 1660448
    return-void
.end method


# virtual methods
.method public final a(LX/AJ2;LX/AJ8;Lcom/facebook/privacy/model/SelectablePrivacyData;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1660449
    invoke-virtual {p2}, LX/AJ8;->b()Z

    move-result v1

    invoke-super {p0, v1, p1, p2}, LX/AIq;->a(ZLX/AJ2;LX/AJ8;)V

    .line 1660450
    iget-object v1, p0, LX/AIs;->n:Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;

    invoke-virtual {p2}, LX/AJ8;->b()Z

    move-result v2

    .line 1660451
    iput-boolean v2, v1, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;->g:Z

    .line 1660452
    iget-object v1, p0, LX/AIs;->m:LX/0fO;

    invoke-virtual {v1}, LX/0fO;->u()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1660453
    iget-object v1, p0, LX/AIs;->o:Lcom/facebook/audience/sharesheet/ui/SharesheetPrivacyView;

    invoke-virtual {v1}, Lcom/facebook/audience/sharesheet/ui/SharesheetPrivacyView;->a()V

    .line 1660454
    iget-object v1, p0, LX/AIs;->o:Lcom/facebook/audience/sharesheet/ui/SharesheetPrivacyView;

    invoke-virtual {v1, v0}, Lcom/facebook/audience/sharesheet/ui/SharesheetPrivacyView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1660455
    :goto_0
    return-void

    .line 1660456
    :cond_0
    iget-object v1, p0, LX/AIs;->o:Lcom/facebook/audience/sharesheet/ui/SharesheetPrivacyView;

    if-nez p3, :cond_1

    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/audience/sharesheet/ui/SharesheetPrivacyView;->setPrivacy(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)V

    .line 1660457
    iget-object v0, p0, LX/AIs;->o:Lcom/facebook/audience/sharesheet/ui/SharesheetPrivacyView;

    new-instance v1, LX/AIr;

    invoke-direct {v1, p0}, LX/AIr;-><init>(LX/AIs;)V

    invoke-virtual {v0, v1}, Lcom/facebook/audience/sharesheet/ui/SharesheetPrivacyView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 1660458
    :cond_1
    iget-object v0, p3, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v0

    .line 1660459
    goto :goto_1
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 1660460
    invoke-super {p0, p1}, LX/AIq;->b(Z)V

    .line 1660461
    iget-object v0, p0, LX/AIs;->n:Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;

    invoke-virtual {v0, p1}, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;->b(Z)V

    .line 1660462
    return-void
.end method
