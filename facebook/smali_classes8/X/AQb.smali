.class public LX/AQb;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/3CE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3CE",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$TypeAheadSuggestionFieldsModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1671477
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1671478
    sget-object v0, LX/4xT;->a:LX/4xT;

    move-object v0, v0

    .line 1671479
    iput-object v0, p0, LX/AQb;->a:LX/3CE;

    .line 1671480
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$TypeAheadSuggestionFieldsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1671481
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1671482
    new-instance v0, LX/4yA;

    invoke-direct {v0}, LX/4yA;-><init>()V

    iget-object v1, p0, LX/AQb;->a:LX/3CE;

    invoke-virtual {v0, v1}, LX/4yA;->a(LX/0Xu;)LX/4yA;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LX/4yA;->a(Ljava/lang/Object;Ljava/lang/Iterable;)LX/4yA;

    move-result-object v0

    invoke-virtual {v0}, LX/4yA;->a()LX/3CE;

    move-result-object v0

    iput-object v0, p0, LX/AQb;->a:LX/3CE;

    .line 1671483
    return-void

    .line 1671484
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)LX/0Px;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$TypeAheadSuggestionFieldsModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1671485
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1671486
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1671487
    :goto_0
    return-object v0

    .line 1671488
    :cond_0
    iget-object v0, p0, LX/AQb;->a:LX/3CE;

    invoke-virtual {v0, p1}, LX/3CE;->e(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    .line 1671489
    iget-object v0, p0, LX/AQb;->a:LX/3CE;

    invoke-virtual {v0}, LX/18f;->x()LX/0Py;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$TypeAheadSuggestionFieldsModel;

    .line 1671490
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1671491
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$TypeAheadSuggestionFieldsModel;

    .line 1671492
    invoke-virtual {v1}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$TypeAheadSuggestionFieldsModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$TypeAheadSuggestionFieldsModel;->k()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1671493
    const/4 v1, 0x1

    .line 1671494
    :goto_2
    move v1, v1

    .line 1671495
    if-nez v1, :cond_1

    .line 1671496
    invoke-virtual {v0}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$TypeAheadSuggestionFieldsModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/1fg;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1671497
    invoke-static {p1}, LX/1fg;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1671498
    const-string v1, "\\s"

    invoke-virtual {v4, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    array-length v7, v6

    const/4 v1, 0x0

    :goto_3
    if-ge v1, v7, :cond_1

    aget-object v8, v6, v1

    .line 1671499
    invoke-virtual {v8, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_3

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1671500
    :cond_3
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1671501
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1671502
    :cond_5
    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_0

    :cond_6
    const/4 v1, 0x0

    goto :goto_2
.end method
