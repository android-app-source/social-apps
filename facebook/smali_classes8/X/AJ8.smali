.class public final LX/AJ8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/audience/sharesheet/app/SharesheetFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/audience/sharesheet/app/SharesheetFragment;)V
    .locals 0

    .prologue
    .line 1660645
    iput-object p1, p0, LX/AJ8;->a:Lcom/facebook/audience/sharesheet/app/SharesheetFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/audience/model/AudienceControlData;Z)V
    .locals 3

    .prologue
    .line 1660652
    if-eqz p2, :cond_2

    .line 1660653
    iget-object v0, p0, LX/AJ8;->a:Lcom/facebook/audience/sharesheet/app/SharesheetFragment;

    iget-object v0, v0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->u:LX/AJZ;

    .line 1660654
    iget-object v1, v0, LX/AJZ;->a:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1660655
    iget-object v1, v0, LX/AJZ;->c:Ljava/util/List;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object p2

    invoke-interface {v1, v2, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1660656
    iget-object v1, v0, LX/AJZ;->b:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1660657
    iget-object v1, v0, LX/AJZ;->d:LX/7gT;

    .line 1660658
    iget-boolean v2, v1, LX/7gT;->k:Z

    if-nez v2, :cond_0

    .line 1660659
    sget-object v2, LX/7gR;->SELECT_FIRST_FRIEND:LX/7gR;

    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object p2

    invoke-virtual {v1, v2, p2}, LX/7gT;->a(LX/7gR;LX/0Px;)V

    .line 1660660
    const/4 v2, 0x1

    iput-boolean v2, v1, LX/7gT;->k:Z

    .line 1660661
    :cond_0
    iget-object v0, p0, LX/AJ8;->a:Lcom/facebook/audience/sharesheet/app/SharesheetFragment;

    iget-object v0, v0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->q:LX/AJt;

    invoke-virtual {v0}, LX/AJt;->f()V

    .line 1660662
    :goto_0
    iget-object v0, p0, LX/AJ8;->a:Lcom/facebook/audience/sharesheet/app/SharesheetFragment;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->b(Lcom/facebook/audience/sharesheet/app/SharesheetFragment;Z)V

    .line 1660663
    iget-object v0, p0, LX/AJ8;->a:Lcom/facebook/audience/sharesheet/app/SharesheetFragment;

    iget-object v0, v0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->m:Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;

    .line 1660664
    iget-boolean v1, v0, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->f:Z

    move v0, v1

    .line 1660665
    if-eqz v0, :cond_1

    .line 1660666
    iget-object v0, p0, LX/AJ8;->a:Lcom/facebook/audience/sharesheet/app/SharesheetFragment;

    iget-object v0, v0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->s:LX/AJH;

    .line 1660667
    iget-object v1, v0, LX/AJH;->i:Ljava/lang/String;

    move-object v0, v1

    .line 1660668
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1660669
    iget-object v0, p0, LX/AJ8;->a:Lcom/facebook/audience/sharesheet/app/SharesheetFragment;

    iget-object v0, v0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->m:Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->setSearchText(Ljava/lang/String;)V

    .line 1660670
    :cond_1
    :goto_1
    return-void

    .line 1660671
    :cond_2
    iget-object v0, p0, LX/AJ8;->a:Lcom/facebook/audience/sharesheet/app/SharesheetFragment;

    iget-object v0, v0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->u:LX/AJZ;

    .line 1660672
    iget-object v1, v0, LX/AJZ;->a:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1660673
    iget-object v1, v0, LX/AJZ;->c:Ljava/util/List;

    invoke-virtual {p1}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 1660674
    if-ltz v1, :cond_3

    .line 1660675
    iget-object v2, v0, LX/AJZ;->c:Ljava/util/List;

    invoke-virtual {p1}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object p2

    invoke-interface {v2, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1660676
    :cond_3
    iget-object v2, v0, LX/AJZ;->d:LX/7gT;

    .line 1660677
    iget-boolean p2, v2, LX/7gT;->l:Z

    if-nez p2, :cond_4

    .line 1660678
    sget-object p2, LX/7gR;->DESELECT_FIRST_FRIEND:LX/7gR;

    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {v2, p2, v0}, LX/7gT;->a(LX/7gR;LX/0Px;)V

    .line 1660679
    const/4 p2, 0x1

    iput-boolean p2, v2, LX/7gT;->l:Z

    .line 1660680
    :cond_4
    move v0, v1

    .line 1660681
    iget-object v1, p0, LX/AJ8;->a:Lcom/facebook/audience/sharesheet/app/SharesheetFragment;

    iget-object v1, v1, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->q:LX/AJt;

    invoke-virtual {v1, v0}, LX/AJt;->e(I)V

    goto :goto_0

    .line 1660682
    :cond_5
    iget-object v0, p0, LX/AJ8;->a:Lcom/facebook/audience/sharesheet/app/SharesheetFragment;

    iget-object v0, v0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->m:Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;

    invoke-virtual {v0}, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->b()V

    goto :goto_1
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1660649
    iget-object v0, p0, LX/AJ8;->a:Lcom/facebook/audience/sharesheet/app/SharesheetFragment;

    iget-object v0, v0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->u:LX/AJZ;

    .line 1660650
    iget-boolean p0, v0, LX/AJZ;->e:Z

    move v0, p0

    .line 1660651
    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1660646
    iget-object v0, p0, LX/AJ8;->a:Lcom/facebook/audience/sharesheet/app/SharesheetFragment;

    iget-object v0, v0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->u:LX/AJZ;

    .line 1660647
    iget-boolean p0, v0, LX/AJZ;->f:Z

    move v0, p0

    .line 1660648
    return v0
.end method
