.class public final enum LX/9bV;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9bV;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9bV;

.field public static final enum BEST_AVAILABLE_IMAGE_URI_QUERY:LX/9bV;

.field public static final enum DOWNLOAD_COVER_PHOTO:LX/9bV;

.field public static final enum FETCH_FACEBOOK_PHOTO:LX/9bV;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1514978
    new-instance v0, LX/9bV;

    const-string v1, "FETCH_FACEBOOK_PHOTO"

    invoke-direct {v0, v1, v2}, LX/9bV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9bV;->FETCH_FACEBOOK_PHOTO:LX/9bV;

    .line 1514979
    new-instance v0, LX/9bV;

    const-string v1, "DOWNLOAD_COVER_PHOTO"

    invoke-direct {v0, v1, v3}, LX/9bV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9bV;->DOWNLOAD_COVER_PHOTO:LX/9bV;

    .line 1514980
    new-instance v0, LX/9bV;

    const-string v1, "BEST_AVAILABLE_IMAGE_URI_QUERY"

    invoke-direct {v0, v1, v4}, LX/9bV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9bV;->BEST_AVAILABLE_IMAGE_URI_QUERY:LX/9bV;

    .line 1514981
    const/4 v0, 0x3

    new-array v0, v0, [LX/9bV;

    sget-object v1, LX/9bV;->FETCH_FACEBOOK_PHOTO:LX/9bV;

    aput-object v1, v0, v2

    sget-object v1, LX/9bV;->DOWNLOAD_COVER_PHOTO:LX/9bV;

    aput-object v1, v0, v3

    sget-object v1, LX/9bV;->BEST_AVAILABLE_IMAGE_URI_QUERY:LX/9bV;

    aput-object v1, v0, v4

    sput-object v0, LX/9bV;->$VALUES:[LX/9bV;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1514977
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9bV;
    .locals 1

    .prologue
    .line 1514975
    const-class v0, LX/9bV;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9bV;

    return-object v0
.end method

.method public static values()[LX/9bV;
    .locals 1

    .prologue
    .line 1514976
    sget-object v0, LX/9bV;->$VALUES:[LX/9bV;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9bV;

    return-object v0
.end method
