.class public LX/AEg;
.super LX/2y5;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1PW;",
        ">",
        "LX/2y5",
        "<TE;>;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:LX/14x;

.field public final b:Ljava/lang/String;

.field private final c:LX/1Nt;

.field public final d:LX/AEi;

.field private final e:LX/2yS;

.field private final f:LX/2yT;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/14x;LX/AEi;LX/2yS;LX/2yT;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1646927
    invoke-direct {p0}, LX/2y5;-><init>()V

    .line 1646928
    iput-object p2, p0, LX/AEg;->a:LX/14x;

    .line 1646929
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08108e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/AEg;->b:Ljava/lang/String;

    .line 1646930
    new-instance v0, Lcom/facebook/attachments/angora/actionbutton/MessagePageActionButton$MessagePageActionButtonPartDefinition;

    invoke-direct {v0, p0}, Lcom/facebook/attachments/angora/actionbutton/MessagePageActionButton$MessagePageActionButtonPartDefinition;-><init>(LX/AEg;)V

    iput-object v0, p0, LX/AEg;->c:LX/1Nt;

    .line 1646931
    iput-object p3, p0, LX/AEg;->d:LX/AEi;

    .line 1646932
    iput-object p4, p0, LX/AEg;->e:LX/2yS;

    .line 1646933
    iput-object p5, p0, LX/AEg;->f:LX/2yT;

    .line 1646934
    return-void
.end method

.method public static a(LX/0QB;)LX/AEg;
    .locals 9

    .prologue
    .line 1646935
    const-class v1, LX/AEg;

    monitor-enter v1

    .line 1646936
    :try_start_0
    sget-object v0, LX/AEg;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1646937
    sput-object v2, LX/AEg;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1646938
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1646939
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1646940
    new-instance v3, LX/AEg;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/14x;->a(LX/0QB;)LX/14x;

    move-result-object v5

    check-cast v5, LX/14x;

    invoke-static {v0}, LX/AEi;->a(LX/0QB;)LX/AEi;

    move-result-object v6

    check-cast v6, LX/AEi;

    invoke-static {v0}, LX/2yS;->a(LX/0QB;)LX/2yS;

    move-result-object v7

    check-cast v7, LX/2yS;

    const-class v8, LX/2yT;

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/2yT;

    invoke-direct/range {v3 .. v8}, LX/AEg;-><init>(Landroid/content/Context;LX/14x;LX/AEi;LX/2yS;LX/2yT;)V

    .line 1646941
    move-object v0, v3

    .line 1646942
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1646943
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/AEg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1646944
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1646945
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/14x;)Z
    .locals 1

    .prologue
    .line 1646946
    const-string v0, "16.0"

    invoke-virtual {p0, v0}, LX/14x;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/14x;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b$redex0(LX/AEg;)I
    .locals 1

    .prologue
    .line 1646947
    iget-object v0, p0, LX/AEg;->a:LX/14x;

    invoke-static {v0}, LX/AEg;->a(LX/14x;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f020740

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f02092c

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Nt;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Landroid/view/View;",
            ":",
            "LX/35p;",
            ">()",
            "LX/1Nt",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;*TE;TV;>;"
        }
    .end annotation

    .prologue
    .line 1646948
    iget-object v0, p0, LX/AEg;->c:LX/1Nt;

    return-object v0
.end method

.method public final a(LX/1De;LX/1PW;Lcom/facebook/feed/rows/core/props/FeedProps;Z)LX/AE0;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "TE;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;Z)",
            "LX/AE0;"
        }
    .end annotation

    .prologue
    .line 1646949
    iget-object v0, p0, LX/AEg;->f:LX/2yT;

    const/4 v1, 0x3

    iget-object v2, p0, LX/AEg;->e:LX/2yS;

    invoke-virtual {v0, p4, v1, v2}, LX/2yT;->a(ZILX/2yS;)LX/AE0;

    move-result-object v0

    const/4 v1, 0x1

    .line 1646950
    iput-boolean v1, v0, LX/AE0;->l:Z

    .line 1646951
    move-object v0, v0

    .line 1646952
    invoke-static {p0}, LX/AEg;->b$redex0(LX/AEg;)I

    move-result v1

    invoke-virtual {v0, v1}, LX/AE0;->a(I)LX/AE0;

    move-result-object v0

    const v1, 0x7f020a8d

    invoke-virtual {v0, v1}, LX/AE0;->b(I)LX/AE0;

    move-result-object v0

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00a6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, LX/AE0;->d(I)LX/AE0;

    move-result-object v0

    iget-object v1, p0, LX/AEg;->b:Ljava/lang/String;

    .line 1646953
    iput-object v1, v0, LX/AE0;->g:Ljava/lang/CharSequence;

    .line 1646954
    move-object v0, v0

    .line 1646955
    iget-object v1, p0, LX/AEg;->d:LX/AEi;

    invoke-virtual {v1, p3}, LX/AEi;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/AEh;

    move-result-object v1

    .line 1646956
    iput-object v1, v0, LX/AE0;->o:Landroid/view/View$OnClickListener;

    .line 1646957
    move-object v0, v0

    .line 1646958
    return-object v0
.end method
