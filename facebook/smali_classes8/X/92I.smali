.class public LX/92I;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1432177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/5LG;ILjava/lang/String;ZLandroid/location/Location;)LX/92H;
    .locals 8
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Landroid/location/Location;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 1432178
    const-string v5, "composer"

    move-object v0, p0

    move-object v2, v1

    move v3, p1

    move-object v4, p2

    move v6, p3

    move-object v7, p4

    invoke-static/range {v0 .. v7}, LX/92I;->a(LX/5LG;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZLandroid/location/Location;)LX/92H;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/5LG;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLandroid/location/Location;)LX/92H;
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Landroid/location/Location;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1432179
    invoke-interface {p0}, LX/5LG;->v()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 1432180
    :goto_0
    new-instance v1, LX/92G;

    invoke-direct {v1}, LX/92G;-><init>()V

    invoke-interface {p0}, LX/5LG;->j()Ljava/lang/String;

    move-result-object v2

    .line 1432181
    iput-object v2, v1, LX/92G;->g:Ljava/lang/String;

    .line 1432182
    move-object v1, v1

    .line 1432183
    invoke-interface {p0}, LX/5LG;->r()Z

    move-result v2

    .line 1432184
    iput-boolean v2, v1, LX/92G;->k:Z

    .line 1432185
    move-object v1, v1

    .line 1432186
    const/16 v2, 0xa

    .line 1432187
    iput v2, v1, LX/92G;->j:I

    .line 1432188
    move-object v1, v1

    .line 1432189
    iput-object p1, v1, LX/92G;->h:Ljava/lang/String;

    .line 1432190
    move-object v1, v1

    .line 1432191
    iput-object p2, v1, LX/92G;->i:Ljava/lang/String;

    .line 1432192
    move-object v1, v1

    .line 1432193
    iput p3, v1, LX/92G;->a:I

    .line 1432194
    move-object v1, v1

    .line 1432195
    iput-object p4, v1, LX/92G;->c:Ljava/lang/String;

    .line 1432196
    move-object v1, v1

    .line 1432197
    iput-object p5, v1, LX/92G;->b:Ljava/lang/String;

    .line 1432198
    move-object v1, v1

    .line 1432199
    iput-boolean v0, v1, LX/92G;->l:Z

    .line 1432200
    move-object v0, v1

    .line 1432201
    iput-object p6, v0, LX/92G;->d:Ljava/lang/String;

    .line 1432202
    move-object v0, v0

    .line 1432203
    iput-boolean p7, v0, LX/92G;->e:Z

    .line 1432204
    move-object v0, v0

    .line 1432205
    iput-object p8, v0, LX/92G;->f:Landroid/location/Location;

    .line 1432206
    move-object v0, v0

    .line 1432207
    invoke-virtual {v0}, LX/92G;->a()LX/92H;

    move-result-object v0

    .line 1432208
    return-object v0

    .line 1432209
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(LX/5LG;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZLandroid/location/Location;)LX/92H;
    .locals 9
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Landroid/location/Location;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1432210
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v6, p5

    move v7, p6

    move-object/from16 v8, p7

    invoke-static/range {v0 .. v8}, LX/92I;->a(LX/5LG;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLandroid/location/Location;)LX/92H;

    move-result-object v0

    return-object v0
.end method
