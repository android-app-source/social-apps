.class public final LX/9CU;
.super LX/0Vd;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLComment;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public final synthetic c:LX/9Cd;


# direct methods
.method public constructor <init>(LX/9Cd;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 0

    .prologue
    .line 1454142
    iput-object p1, p0, LX/9CU;->c:LX/9Cd;

    iput-object p2, p0, LX/9CU;->a:Lcom/facebook/graphql/model/GraphQLComment;

    iput-object p3, p0, LX/9CU;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1454143
    iget-object v0, p0, LX/9CU;->c:LX/9Cd;

    iget-object v0, v0, LX/9Cd;->b:LX/1K9;

    new-instance v1, LX/8q4;

    iget-object v2, p0, LX/9CU;->a:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v3, p0, LX/9CU;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/8q4;-><init>(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/1K9;->a(LX/1KJ;)V

    .line 1454144
    iget-object v0, p0, LX/9CU;->c:LX/9Cd;

    iget-object v0, v0, LX/9Cd;->d:LX/3iL;

    invoke-virtual {v0, p1}, LX/3iL;->a(Ljava/lang/Throwable;)V

    .line 1454145
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1454146
    return-void
.end method
