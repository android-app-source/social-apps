.class public final enum LX/AM3;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/AM3;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/AM3;

.field public static final enum NONE:LX/AM3;

.field public static final enum PRESSING:LX/AM3;

.field public static final enum UNPRESSING:LX/AM3;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1665781
    new-instance v0, LX/AM3;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LX/AM3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AM3;->NONE:LX/AM3;

    .line 1665782
    new-instance v0, LX/AM3;

    const-string v1, "PRESSING"

    invoke-direct {v0, v1, v3}, LX/AM3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AM3;->PRESSING:LX/AM3;

    .line 1665783
    new-instance v0, LX/AM3;

    const-string v1, "UNPRESSING"

    invoke-direct {v0, v1, v4}, LX/AM3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AM3;->UNPRESSING:LX/AM3;

    .line 1665784
    const/4 v0, 0x3

    new-array v0, v0, [LX/AM3;

    sget-object v1, LX/AM3;->NONE:LX/AM3;

    aput-object v1, v0, v2

    sget-object v1, LX/AM3;->PRESSING:LX/AM3;

    aput-object v1, v0, v3

    sget-object v1, LX/AM3;->UNPRESSING:LX/AM3;

    aput-object v1, v0, v4

    sput-object v0, LX/AM3;->$VALUES:[LX/AM3;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1665785
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/AM3;
    .locals 1

    .prologue
    .line 1665780
    const-class v0, LX/AM3;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/AM3;

    return-object v0
.end method

.method public static values()[LX/AM3;
    .locals 1

    .prologue
    .line 1665779
    sget-object v0, LX/AM3;->$VALUES:[LX/AM3;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/AM3;

    return-object v0
.end method
