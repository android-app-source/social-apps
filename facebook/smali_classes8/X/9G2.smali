.class public final LX/9G2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/9G3;


# direct methods
.method public constructor <init>(LX/9G3;)V
    .locals 0

    .prologue
    .line 1459255
    iput-object p1, p0, LX/9G2;->a:LX/9G3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1459256
    new-instance v0, LX/27k;

    const-string v1, "Error adding Place to Map"

    invoke-direct {v0, v1}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    .line 1459257
    iget-object v1, p0, LX/9G2;->a:LX/9G3;

    iget-object v1, v1, LX/9G3;->d:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAddPendingPlaceClickListenerPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoAddPendingPlaceClickListenerPartDefinition;->g:LX/0kL;

    invoke-virtual {v1, v0}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 1459258
    return-void
.end method

.method public final bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1459254
    return-void
.end method
