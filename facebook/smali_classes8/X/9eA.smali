.class public final LX/9eA;
.super Landroid/animation/AnimatorListenerAdapter;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)V
    .locals 0

    .prologue
    .line 1519317
    iput-object p1, p0, LX/9eA;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3

    .prologue
    .line 1519318
    iget-object v0, p0, LX/9eA;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v0, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    sget-object v1, LX/9eK;->ANIMATE_IN:LX/9eK;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mDefaultShowAnimator onAnimationEnd invalid state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/9eA;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v2, v2, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1519319
    iget-object v0, p0, LX/9eA;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    invoke-static {v0}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->u(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)V

    .line 1519320
    iget-object v0, p0, LX/9eA;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    const/4 v1, 0x0

    .line 1519321
    iput-object v1, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->X:Landroid/animation/ValueAnimator;

    .line 1519322
    return-void

    .line 1519323
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .locals 1

    .prologue
    .line 1519324
    iget-object v0, p0, LX/9eA;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v0, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->t:LX/23g;

    invoke-virtual {v0}, LX/23g;->c()V

    .line 1519325
    return-void
.end method
