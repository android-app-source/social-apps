.class public LX/APp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0iK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0iw;",
        ":",
        "LX/0j3;",
        "DerivedData:",
        "Ljava/lang/Object;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
        ":",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicSetters$SetsIsFeedOnlyPost",
        "<TMutation;>;Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;:",
        "LX/0im",
        "<TMutation;>;>",
        "Ljava/lang/Object;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;"
    }
.end annotation


# static fields
.field public static final a:LX/0jK;


# instance fields
.field private final b:Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;

.field private final c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1670460
    const-class v0, LX/APp;

    invoke-static {v0}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v0

    sput-object v0, LX/APp;->a:LX/0jK;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0ad;Landroid/view/ViewStub;LX/0il;)V
    .locals 5
    .param p3    # Landroid/view/ViewStub;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0ad;",
            "Landroid/view/ViewStub;",
            "TServices;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1670461
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1670462
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/APp;->c:Ljava/lang/ref/WeakReference;

    .line 1670463
    sget-short v0, LX/1EB;->aa:S

    const/4 v1, 0x0

    invoke-interface {p2, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iw;

    check-cast v0, LX/0j3;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    const/4 v1, 0x0

    .line 1670464
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1670465
    :cond_0
    :goto_0
    move v0, v1

    .line 1670466
    if-eqz v0, :cond_1

    .line 1670467
    sget-char v0, LX/1EB;->Z:C

    const v1, 0x7f0814a8

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-interface {p2, v0, v1, v2}, LX/0ad;->a(CILandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    .line 1670468
    sget-char v0, LX/1EB;->ac:C

    const v2, 0x7f0814a9

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-interface {p2, v0, v2, v3}, LX/0ad;->a(CILandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v2

    .line 1670469
    sget-char v0, LX/1EB;->ab:C

    const v3, 0x7f0814aa

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-interface {p2, v0, v3, v4}, LX/0ad;->a(CILandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v3

    .line 1670470
    invoke-virtual {p3}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;

    iput-object v0, p0, LX/APp;->b:Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;

    .line 1670471
    iget-object v0, p0, LX/APp;->b:Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;

    invoke-virtual {v0, v1}, Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;->setTitleText(Ljava/lang/String;)V

    .line 1670472
    iget-object v0, p0, LX/APp;->b:Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;

    invoke-virtual {v0, v2, v3}, Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1670473
    iget-object v0, p0, LX/APp;->b:Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;

    new-instance v1, LX/APo;

    invoke-direct {v1, p0, p4}, LX/APo;-><init>(LX/APp;LX/0il;)V

    invoke-virtual {v0, v1}, Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1670474
    :goto_1
    return-void

    .line 1670475
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, LX/APp;->b:Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;

    goto :goto_1

    .line 1670476
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getAllowFeedOnlyPost()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1670477
    const/4 v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/5L2;)V
    .locals 2

    .prologue
    .line 1670478
    iget-object v0, p0, LX/APp;->b:Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;

    if-nez v0, :cond_1

    .line 1670479
    :cond_0
    :goto_0
    return-void

    .line 1670480
    :cond_1
    iget-object v0, p0, LX/APp;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1670481
    sget-object v1, LX/5L2;->ON_FIRST_DRAW:LX/5L2;

    if-ne p1, v1, :cond_0

    .line 1670482
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iw;

    invoke-interface {v0}, LX/0iw;->isFeedOnlyPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1670483
    iget-object v0, p0, LX/APp;->b:Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;->setToggleChecked(Z)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1670484
    iget-object v0, p0, LX/APp;->b:Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;

    if-nez v0, :cond_0

    .line 1670485
    :goto_0
    return-void

    .line 1670486
    :cond_0
    iget-object v0, p0, LX/APp;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1670487
    iget-object v1, p0, LX/APp;->b:Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iw;

    invoke-interface {v0}, LX/0iw;->isFeedOnlyPost()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/composer/ui/feedonly/ComposerFeedOnlyPostToggleView;->setToggleChecked(Z)V

    goto :goto_0
.end method
