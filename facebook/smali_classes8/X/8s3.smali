.class public final LX/8s3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:LX/9Hi;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLComment;

.field public final synthetic c:LX/8sB;


# direct methods
.method public constructor <init>(LX/8sB;LX/9Hi;Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 0

    .prologue
    .line 1409725
    iput-object p1, p0, LX/8s3;->c:LX/8sB;

    iput-object p2, p0, LX/8s3;->a:LX/9Hi;

    iput-object p3, p0, LX/8s3;->b:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 1409726
    iget-object v0, p0, LX/8s3;->c:LX/8sB;

    iget-object v0, v0, LX/8sB;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1409727
    iget-boolean v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v0, v1

    .line 1409728
    if-nez v0, :cond_0

    .line 1409729
    iget-object v0, p0, LX/8s3;->c:LX/8sB;

    iget-object v0, v0, LX/8sB;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Fail to log banning user actions in comments list: not Page Viewer Context"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1409730
    :goto_0
    return-void

    .line 1409731
    :cond_0
    iget-object v1, p0, LX/8s3;->a:LX/9Hi;

    iget-object v0, p0, LX/8s3;->c:LX/8sB;

    iget-object v0, v0, LX/8sB;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1409732
    iget-object v2, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v2

    .line 1409733
    iget-object v2, p0, LX/8s3;->b:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v1, v0, v2}, LX/9Hi;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 1409734
    iget-object v0, p0, LX/8s3;->a:LX/9Hi;

    iget-object v1, p0, LX/8s3;->b:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v0, v1}, LX/9Hi;->a(Lcom/facebook/graphql/model/GraphQLComment;)V

    goto :goto_0
.end method
