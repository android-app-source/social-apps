.class public final LX/9eM;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field private c:I

.field private d:LX/74S;

.field private e:LX/31M;

.field public f:I

.field public g:I

.field private h:Z


# direct methods
.method public constructor <init>(LX/74S;)V
    .locals 1

    .prologue
    .line 1519477
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1519478
    const/4 v0, -0x1

    iput v0, p0, LX/9eM;->c:I

    .line 1519479
    const/4 v0, 0x0

    iput-object v0, p0, LX/9eM;->d:LX/74S;

    .line 1519480
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/9eM;->h:Z

    .line 1519481
    iput-object p1, p0, LX/9eM;->d:LX/74S;

    .line 1519482
    return-void
.end method

.method public constructor <init>(Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;)V
    .locals 1

    .prologue
    .line 1519483
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1519484
    const/4 v0, -0x1

    iput v0, p0, LX/9eM;->c:I

    .line 1519485
    const/4 v0, 0x0

    iput-object v0, p0, LX/9eM;->d:LX/74S;

    .line 1519486
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/9eM;->h:Z

    .line 1519487
    iget-object v0, p1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->a:Ljava/lang/String;

    iput-object v0, p0, LX/9eM;->a:Ljava/lang/String;

    .line 1519488
    iget-object v0, p1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->f:Ljava/lang/String;

    iput-object v0, p0, LX/9eM;->b:Ljava/lang/String;

    .line 1519489
    iget-object v0, p1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->q:LX/74S;

    iput-object v0, p0, LX/9eM;->d:LX/74S;

    .line 1519490
    iget v0, p1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->p:I

    iput v0, p0, LX/9eM;->c:I

    .line 1519491
    iget-object v0, p1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->s:LX/31M;

    iput-object v0, p0, LX/9eM;->e:LX/31M;

    .line 1519492
    iget v0, p1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->t:I

    iput v0, p0, LX/9eM;->f:I

    .line 1519493
    const/4 v0, 0x0

    iput v0, p0, LX/9eM;->g:I

    .line 1519494
    return-void
.end method


# virtual methods
.method public final a(LX/31M;)LX/9eM;
    .locals 1

    .prologue
    .line 1519495
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/31M;

    iput-object v0, p0, LX/9eM;->e:LX/31M;

    .line 1519496
    return-object p0
.end method

.method public final a()Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 1519497
    iget-object v0, p0, LX/9eM;->d:LX/74S;

    const-string v1, "must set gallery source"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1519498
    iget-object v0, p0, LX/9eM;->e:LX/31M;

    const-string v1, "must set dismiss direction"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1519499
    iget-boolean v0, p0, LX/9eM;->h:Z

    if-eqz v0, :cond_0

    .line 1519500
    iget v0, p0, LX/9eM;->f:I

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "must set swipe dismiss direction flags"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1519501
    :cond_0
    new-instance v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;

    iget-object v1, p0, LX/9eM;->a:Ljava/lang/String;

    iget-object v2, p0, LX/9eM;->b:Ljava/lang/String;

    iget v3, p0, LX/9eM;->c:I

    iget-object v4, p0, LX/9eM;->d:LX/74S;

    iget-object v5, p0, LX/9eM;->e:LX/31M;

    iget v6, p0, LX/9eM;->f:I

    iget v7, p0, LX/9eM;->g:I

    iget-boolean v8, p0, LX/9eM;->h:Z

    invoke-direct/range {v0 .. v9}, Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;-><init>(Ljava/lang/String;Ljava/lang/String;ILX/74S;LX/31M;IIZB)V

    return-object v0

    :cond_1
    move v0, v9

    .line 1519502
    goto :goto_0
.end method
