.class public final LX/AI8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AI9;

.field public final synthetic b:LX/1Fm;


# direct methods
.method public constructor <init>(LX/1Fm;LX/AI9;)V
    .locals 0

    .prologue
    .line 1657438
    iput-object p1, p0, LX/AI8;->b:LX/1Fm;

    iput-object p2, p0, LX/AI8;->a:LX/AI9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1657439
    sget-object v0, LX/1Fm;->a:Ljava/lang/String;

    const-string v1, "error while trying to send message"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1657440
    iget-object v0, p0, LX/AI8;->a:LX/AI9;

    invoke-interface {v0}, LX/AI9;->a()V

    .line 1657441
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1657442
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1657443
    if-eqz p1, :cond_0

    .line 1657444
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1657445
    check-cast v0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel;

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel;->a()Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1657446
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1657447
    check-cast v0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel;

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel;->a()Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel;->a()Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel$LatestMessageModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1657448
    :cond_0
    iget-object v0, p0, LX/AI8;->a:LX/AI9;

    invoke-interface {v0}, LX/AI9;->a()V

    .line 1657449
    :goto_0
    return-void

    .line 1657450
    :cond_1
    iget-object v1, p0, LX/AI8;->a:LX/AI9;

    .line 1657451
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1657452
    check-cast v0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel;

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel;->a()Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel;->a()Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel$LatestMessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$DirectMessageReplyMutationModel$ThreadModel$LatestMessageModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, LX/AI9;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
