.class public final enum LX/9bv;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9bv;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9bv;

.field public static final enum FREE_FORM:LX/9bv;

.field public static final enum SQUARE:LX/9bv;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1515439
    new-instance v0, LX/9bv;

    const-string v1, "FREE_FORM"

    invoke-direct {v0, v1, v2}, LX/9bv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9bv;->FREE_FORM:LX/9bv;

    .line 1515440
    new-instance v0, LX/9bv;

    const-string v1, "SQUARE"

    invoke-direct {v0, v1, v3}, LX/9bv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9bv;->SQUARE:LX/9bv;

    .line 1515441
    const/4 v0, 0x2

    new-array v0, v0, [LX/9bv;

    sget-object v1, LX/9bv;->FREE_FORM:LX/9bv;

    aput-object v1, v0, v2

    sget-object v1, LX/9bv;->SQUARE:LX/9bv;

    aput-object v1, v0, v3

    sput-object v0, LX/9bv;->$VALUES:[LX/9bv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1515442
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9bv;
    .locals 1

    .prologue
    .line 1515443
    const-class v0, LX/9bv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9bv;

    return-object v0
.end method

.method public static values()[LX/9bv;
    .locals 1

    .prologue
    .line 1515444
    sget-object v0, LX/9bv;->$VALUES:[LX/9bv;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9bv;

    return-object v0
.end method
