.class public final enum LX/AMV;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/AMV;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/AMV;

.field public static final enum FETCH_DOWNLOADABLE_MSQRD_MASKS:LX/AMV;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1666284
    new-instance v0, LX/AMV;

    const-string v1, "FETCH_DOWNLOADABLE_MSQRD_MASKS"

    invoke-direct {v0, v1, v2}, LX/AMV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AMV;->FETCH_DOWNLOADABLE_MSQRD_MASKS:LX/AMV;

    .line 1666285
    const/4 v0, 0x1

    new-array v0, v0, [LX/AMV;

    sget-object v1, LX/AMV;->FETCH_DOWNLOADABLE_MSQRD_MASKS:LX/AMV;

    aput-object v1, v0, v2

    sput-object v0, LX/AMV;->$VALUES:[LX/AMV;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1666287
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/AMV;
    .locals 1

    .prologue
    .line 1666288
    const-class v0, LX/AMV;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/AMV;

    return-object v0
.end method

.method public static values()[LX/AMV;
    .locals 1

    .prologue
    .line 1666286
    sget-object v0, LX/AMV;->$VALUES:[LX/AMV;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/AMV;

    return-object v0
.end method
