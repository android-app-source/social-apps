.class public LX/9F2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9Bi;


# instance fields
.field private final a:LX/9Bj;

.field private final b:LX/9DG;


# direct methods
.method public constructor <init>(LX/9Bj;LX/9DG;)V
    .locals 0
    .param p1    # LX/9Bj;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/9DG;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1457965
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1457966
    iput-object p1, p0, LX/9F2;->a:LX/9Bj;

    .line 1457967
    iput-object p2, p0, LX/9F2;->b:LX/9DG;

    .line 1457968
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 1

    .prologue
    .line 1457963
    iget-object v0, p0, LX/9F2;->a:LX/9Bj;

    invoke-virtual {v0, p1}, LX/9Bj;->a(Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 1457964
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLComment;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1457961
    iget-object v0, p0, LX/9F2;->a:LX/9Bj;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/9Bj;->a(Lcom/facebook/graphql/model/GraphQLComment;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1457962
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V
    .locals 0

    .prologue
    .line 1457969
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 1

    .prologue
    .line 1457959
    iget-object v0, p0, LX/9F2;->a:LX/9Bj;

    invoke-virtual {v0, p1, p2}, LX/9Bj;->a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1457960
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V
    .locals 1

    .prologue
    .line 1457957
    iget-object v0, p0, LX/9F2;->b:LX/9DG;

    invoke-virtual {v0}, LX/9DG;->g()Z

    .line 1457958
    return-void
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 1

    .prologue
    .line 1457953
    iget-object v0, p0, LX/9F2;->a:LX/9Bj;

    invoke-virtual {v0, p1}, LX/9Bj;->b(Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 1457954
    return-void
.end method

.method public final c(Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 1

    .prologue
    .line 1457955
    iget-object v0, p0, LX/9F2;->a:LX/9Bj;

    invoke-virtual {v0, p1}, LX/9Bj;->c(Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 1457956
    return-void
.end method
