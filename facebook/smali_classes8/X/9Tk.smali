.class public LX/9Tk;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/0Zb;

.field public final b:LX/0if;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0if;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1496704
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1496705
    iput-object p1, p0, LX/9Tk;->a:LX/0Zb;

    .line 1496706
    iput-object p2, p0, LX/9Tk;->b:LX/0if;

    .line 1496707
    return-void
.end method

.method public static a(LX/0QB;)LX/9Tk;
    .locals 5

    .prologue
    .line 1496708
    const-class v1, LX/9Tk;

    monitor-enter v1

    .line 1496709
    :try_start_0
    sget-object v0, LX/9Tk;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1496710
    sput-object v2, LX/9Tk;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1496711
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1496712
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1496713
    new-instance p0, LX/9Tk;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v4

    check-cast v4, LX/0if;

    invoke-direct {p0, v3, v4}, LX/9Tk;-><init>(LX/0Zb;LX/0if;)V

    .line 1496714
    move-object v0, p0

    .line 1496715
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1496716
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/9Tk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1496717
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1496718
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/9Tk;LX/9Tj;)V
    .locals 1

    .prologue
    .line 1496719
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, LX/9Tk;->a(LX/9Tk;LX/9Tj;Ljava/util/Map;)V

    .line 1496720
    return-void
.end method

.method public static a(LX/9Tk;LX/9Tj;Ljava/util/Map;)V
    .locals 3
    .param p1    # LX/9Tj;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/9Tj;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 1496725
    iget-object v0, p0, LX/9Tk;->a:LX/0Zb;

    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-virtual {p1}, LX/9Tj;->getEventName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "friend_finder"

    .line 1496726
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1496727
    move-object v1, v1

    .line 1496728
    invoke-virtual {v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1496729
    return-void
.end method

.method public static b(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 1496721
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "friend_finder"

    .line 1496722
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1496723
    move-object v0, v0

    .line 1496724
    return-object v0
.end method


# virtual methods
.method public final a(LX/9Ti;Ljava/lang/String;JI)V
    .locals 5

    .prologue
    .line 1496732
    iget-object v0, p0, LX/9Tk;->a:LX/0Zb;

    sget-object v1, LX/9Tj;->COMPLETED:LX/9Tj;

    invoke-virtual {v1}, LX/9Tj;->getEventName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/9Tk;->b(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "api"

    invoke-virtual {p1}, LX/9Ti;->getApiName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "ci_flow"

    invoke-virtual {v1, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "time_since_creation"

    invoke-virtual {v1, v2, p3, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "matches"

    invoke-virtual {v1, v2, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1496733
    return-void
.end method

.method public final a(Ljava/lang/String;IIIILjava/lang/String;JII)V
    .locals 5

    .prologue
    .line 1496730
    iget-object v0, p0, LX/9Tk;->a:LX/0Zb;

    sget-object v1, LX/9Tj;->HOW_MANY_SEEN:LX/9Tj;

    invoke-virtual {v1}, LX/9Tj;->getEventName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/9Tk;->b(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "api"

    sget-object v3, LX/9Ti;->FRIEND_FINDER_API:LX/9Ti;

    invoke-virtual {v3}, LX/9Ti;->getApiName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "ci_flow"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "how_many_seen"

    invoke-virtual {v1, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "total_candidates"

    invoke-virtual {v1, v2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "batch_size"

    invoke-virtual {v1, v2, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "pagination_size"

    invoke-virtual {v1, v2, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "session_id"

    invoke-virtual {v1, v2, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "time_since_creation"

    invoke-virtual {v1, v2, p7, p8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "friendable_count"

    invoke-virtual {v1, v2, p9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "pymk_count"

    invoke-virtual {v1, v2, p10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1496731
    return-void
.end method

.method public final a(Ljava/lang/String;ILX/9Ti;)V
    .locals 7

    .prologue
    .line 1496700
    sget-object v6, LX/9Tj;->SEND_INVITE_ALL:LX/9Tj;

    const-string v0, "ci_flow"

    const-string v2, "total_candidates"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "api"

    move-object v1, p1

    move-object v5, p3

    invoke-static/range {v0 .. v5}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    invoke-static {p0, v6, v0}, LX/9Tk;->a(LX/9Tk;LX/9Tj;Ljava/util/Map;)V

    .line 1496701
    return-void
.end method

.method public final a(Ljava/lang/String;JIJ)V
    .locals 4

    .prologue
    .line 1496702
    iget-object v0, p0, LX/9Tk;->a:LX/0Zb;

    sget-object v1, LX/9Tj;->FIRST_RESULTS_READY:LX/9Tj;

    invoke-virtual {v1}, LX/9Tj;->getEventName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/9Tk;->b(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "api"

    sget-object v3, LX/9Ti;->FRIENDABLE_CONTACTS_API:LX/9Ti;

    invoke-virtual {v3}, LX/9Ti;->getApiName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "ci_flow"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "time_since_creation"

    invoke-virtual {v1, v2, p2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "result_size"

    invoke-virtual {v1, v2, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "fetch_time"

    invoke-virtual {v1, v2, p5, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1496703
    return-void
.end method

.method public final a(Ljava/lang/String;JIJLX/9Tj;)V
    .locals 3

    .prologue
    .line 1496687
    iget-object v0, p0, LX/9Tk;->a:LX/0Zb;

    invoke-virtual {p7}, LX/9Tj;->getEventName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/9Tk;->b(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "ci_flow"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "time_since_creation"

    invoke-virtual {v1, v2, p2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "result_size"

    invoke-virtual {v1, v2, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "fetch_time"

    invoke-virtual {v1, v2, p5, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1496688
    return-void
.end method

.method public final a(Ljava/lang/String;JILX/9Tj;)V
    .locals 4

    .prologue
    .line 1496681
    iget-object v0, p0, LX/9Tk;->a:LX/0Zb;

    invoke-virtual {p5}, LX/9Tj;->getEventName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/9Tk;->b(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "ci_flow"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "time_since_creation"

    invoke-virtual {v1, v2, p2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "fetched_candidates_size"

    invoke-virtual {v1, v2, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1496682
    return-void
.end method

.method public final a(Ljava/lang/String;JJII)V
    .locals 4

    .prologue
    .line 1496683
    iget-object v0, p0, LX/9Tk;->a:LX/0Zb;

    sget-object v1, LX/9Tj;->PYMK_FETCHED:LX/9Tj;

    invoke-virtual {v1}, LX/9Tj;->getEventName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/9Tk;->b(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "ci_flow"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "time_since_creation"

    invoke-virtual {v1, v2, p2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "fetch_time"

    invoke-virtual {v1, v2, p4, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "result_size"

    invoke-virtual {v1, v2, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "fetched_candidates_size"

    invoke-virtual {v1, v2, p7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1496684
    return-void
.end method

.method public final a(Ljava/lang/String;JLX/9Tj;)V
    .locals 4

    .prologue
    .line 1496685
    iget-object v0, p0, LX/9Tk;->a:LX/0Zb;

    invoke-virtual {p4}, LX/9Tj;->getEventName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/9Tk;->b(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "ci_flow"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "time_since_creation"

    invoke-virtual {v1, v2, p2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1496686
    return-void
.end method

.method public final a(Ljava/lang/String;LX/9Ti;)V
    .locals 3

    .prologue
    .line 1496689
    sget-object v0, LX/9Tj;->SEND_INVITE:LX/9Tj;

    const-string v1, "ci_flow"

    const-string v2, "api"

    invoke-static {v1, p1, v2, p2}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v1

    invoke-static {p0, v0, v1}, LX/9Tk;->a(LX/9Tk;LX/9Tj;Ljava/util/Map;)V

    .line 1496690
    return-void
.end method

.method public final a(Ljava/lang/String;LX/9Ti;JIIZ)V
    .locals 5

    .prologue
    .line 1496698
    iget-object v0, p0, LX/9Tk;->a:LX/0Zb;

    sget-object v1, LX/9Tj;->FRIEND_FINDER_FIRST_TIME_SEEN:LX/9Tj;

    invoke-virtual {v1}, LX/9Tj;->getEventName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/9Tk;->b(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "ci_flow"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "api"

    invoke-virtual {p2}, LX/9Ti;->getApiName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "time_since_creation"

    invoke-virtual {v1, v2, p3, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "friendable_count"

    invoke-virtual {v1, v2, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "pymk_count"

    invoke-virtual {v1, v2, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "fetch_complete"

    invoke-virtual {v1, v2, p7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1496699
    return-void
.end method

.method public final a(Ljava/lang/String;LX/9Ti;Ljava/lang/String;ZZ)V
    .locals 4

    .prologue
    .line 1496691
    iget-object v0, p0, LX/9Tk;->a:LX/0Zb;

    sget-object v1, LX/9Tj;->FETCH_FROM_CCU:LX/9Tj;

    invoke-virtual {v1}, LX/9Tj;->getEventName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/9Tk;->b(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "ci_flow"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "api"

    invoke-virtual {p2}, LX/9Ti;->getApiName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "ccu_cache_status"

    invoke-virtual {v1, v2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "first_time_fetch"

    invoke-virtual {v1, v2, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "has_seen_page"

    invoke-virtual {v1, v2, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1496692
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;)V
    .locals 11

    .prologue
    .line 1496693
    sget-object v10, LX/9Tj;->LEGAL_OPENED:LX/9Tj;

    const-string v0, "ci_flow"

    const-string v2, "ccu_ref"

    const-string v4, "ccu_turned_on"

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const-string v6, "should_show_term"

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    const-string v8, "skip_term_fc_gk"

    move-object v1, p1

    move-object v3, p2

    move-object/from16 v9, p5

    invoke-static/range {v0 .. v9}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    invoke-static {p0, v10, v0}, LX/9Tk;->a(LX/9Tk;LX/9Tj;Ljava/util/Map;)V

    .line 1496694
    iget-object v0, p0, LX/9Tk;->b:LX/0if;

    sget-object v1, LX/0ig;->b:LX/0ih;

    sget-object v2, LX/9Tj;->LEGAL_OPENED:LX/9Tj;

    invoke-virtual {v2}, LX/9Tj;->getEventName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1496695
    return-void
.end method

.method public final b(Ljava/lang/String;LX/9Ti;)V
    .locals 3

    .prologue
    .line 1496696
    sget-object v0, LX/9Tj;->UNDO_CLICKED:LX/9Tj;

    const-string v1, "ci_flow"

    const-string v2, "api"

    invoke-static {v1, p1, v2, p2}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v1

    invoke-static {p0, v0, v1}, LX/9Tk;->a(LX/9Tk;LX/9Tj;Ljava/util/Map;)V

    .line 1496697
    return-void
.end method
