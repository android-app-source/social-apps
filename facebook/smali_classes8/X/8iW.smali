.class public final LX/8iW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field public final synthetic a:Lcom/facebook/sounds/configurator/AudioConfigurator;


# direct methods
.method public constructor <init>(Lcom/facebook/sounds/configurator/AudioConfigurator;)V
    .locals 0

    .prologue
    .line 1391530
    iput-object p1, p0, LX/8iW;->a:Lcom/facebook/sounds/configurator/AudioConfigurator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/sounds/configurator/AudioConfigurator;B)V
    .locals 0

    .prologue
    .line 1391531
    invoke-direct {p0, p1}, LX/8iW;-><init>(Lcom/facebook/sounds/configurator/AudioConfigurator;)V

    return-void
.end method


# virtual methods
.method public final onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1391532
    iget-object v1, p0, LX/8iW;->a:Lcom/facebook/sounds/configurator/AudioConfigurator;

    iget-object v0, p0, LX/8iW;->a:Lcom/facebook/sounds/configurator/AudioConfigurator;

    iget-object v0, v0, Lcom/facebook/sounds/configurator/AudioConfigurator;->d:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8iO;

    .line 1391533
    iput-object v0, v1, Lcom/facebook/sounds/configurator/AudioConfigurator;->k:LX/8iO;

    .line 1391534
    iget-object v0, p0, LX/8iW;->a:Lcom/facebook/sounds/configurator/AudioConfigurator;

    iget-object v1, p0, LX/8iW;->a:Lcom/facebook/sounds/configurator/AudioConfigurator;

    iget-object v1, v1, Lcom/facebook/sounds/configurator/AudioConfigurator;->k:LX/8iO;

    .line 1391535
    invoke-static {v0, v1}, Lcom/facebook/sounds/configurator/AudioConfigurator;->a$redex0(Lcom/facebook/sounds/configurator/AudioConfigurator;LX/8iO;)V

    .line 1391536
    sget-object v0, Lcom/facebook/sounds/configurator/AudioConfigurator;->b:Ljava/text/DecimalFormat;

    iget-object v1, p0, LX/8iW;->a:Lcom/facebook/sounds/configurator/AudioConfigurator;

    iget-object v1, v1, Lcom/facebook/sounds/configurator/AudioConfigurator;->k:LX/8iO;

    .line 1391537
    iget v2, v1, LX/8iO;->c:F

    move v1, v2

    .line 1391538
    float-to-double v2, v1

    invoke-virtual {v0, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    .line 1391539
    iget-object v1, p0, LX/8iW;->a:Lcom/facebook/sounds/configurator/AudioConfigurator;

    iget-object v1, v1, Lcom/facebook/sounds/configurator/AudioConfigurator;->l:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1391540
    iget-object v0, p0, LX/8iW;->a:Lcom/facebook/sounds/configurator/AudioConfigurator;

    invoke-static {v0}, Lcom/facebook/sounds/configurator/AudioConfigurator;->e(Lcom/facebook/sounds/configurator/AudioConfigurator;)V

    .line 1391541
    return-void
.end method

.method public final onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 1391542
    return-void
.end method
