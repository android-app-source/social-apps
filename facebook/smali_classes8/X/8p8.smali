.class public LX/8p8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Uh;

.field private final b:LX/0ad;


# direct methods
.method public constructor <init>(LX/0Uh;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1405507
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1405508
    iput-object p1, p0, LX/8p8;->a:LX/0Uh;

    .line 1405509
    iput-object p2, p0, LX/8p8;->b:LX/0ad;

    .line 1405510
    return-void
.end method

.method public static a(LX/0QB;)LX/8p8;
    .locals 1

    .prologue
    .line 1405506
    invoke-static {p0}, LX/8p8;->b(LX/0QB;)LX/8p8;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/8p8;
    .locals 3

    .prologue
    .line 1405504
    new-instance v2, LX/8p8;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-direct {v2, v0, v1}, LX/8p8;-><init>(LX/0Uh;LX/0ad;)V

    .line 1405505
    return-object v2
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1405503
    iget-object v1, p0, LX/8p8;->a:LX/0Uh;

    const/16 v2, 0x42f

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/8p8;->b:LX/0ad;

    sget-short v2, LX/0wf;->E:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 1405502
    iget-object v0, p0, LX/8p8;->b:LX/0ad;

    sget-short v1, LX/0wf;->U:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1405499
    invoke-virtual {p0}, LX/8p8;->d()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/8p8;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 3

    .prologue
    .line 1405501
    iget-object v0, p0, LX/8p8;->b:LX/0ad;

    sget-short v1, LX/0wf;->T:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final e()Z
    .locals 3

    .prologue
    .line 1405500
    iget-object v0, p0, LX/8p8;->b:LX/0ad;

    sget-short v1, LX/0wf;->V:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method
