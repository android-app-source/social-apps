.class public LX/9HD;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/3Wr;
.implements LX/3Ws;


# instance fields
.field public a:LX/9HC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/8xP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final c:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

.field private final d:LX/9HB;

.field private final e:LX/9HK;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1460955
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/9HD;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1460956
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1460957
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/9HD;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1460958
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 1460959
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1460960
    const-class v0, LX/9HD;

    invoke-static {v0, p0}, LX/9HD;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1460961
    const v0, 0x7f030128

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1460962
    iget-object v0, p0, LX/9HD;->a:LX/9HC;

    invoke-virtual {v0, p0}, LX/9HC;->a(Landroid/view/View;)LX/9HB;

    move-result-object v0

    iput-object v0, p0, LX/9HD;->d:LX/9HB;

    .line 1460963
    iget-object v0, p0, LX/9HD;->d:LX/9HB;

    invoke-virtual {v0}, LX/9HB;->a()V

    .line 1460964
    new-instance v0, LX/9HK;

    invoke-direct {v0, p1}, LX/9HK;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/9HD;->e:LX/9HK;

    .line 1460965
    invoke-virtual {p0}, LX/9HD;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b18aa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1460966
    invoke-virtual {p0, v2, v0, v2, v0}, LX/9HD;->setPadding(IIII)V

    .line 1460967
    const v0, 0x7f0d05d6

    invoke-virtual {p0, v0}, LX/9HD;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iput-object v0, p0, LX/9HD;->c:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 1460968
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/9HD;

    const-class v1, LX/9HC;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/9HC;

    invoke-static {p0}, LX/8xP;->b(LX/0QB;)LX/8xP;

    move-result-object p0

    check-cast p0, LX/8xP;

    iput-object v1, p1, LX/9HD;->a:LX/9HC;

    iput-object p0, p1, LX/9HD;->b:LX/8xP;

    return-void
.end method


# virtual methods
.method public final a(LX/9CP;)V
    .locals 1

    .prologue
    .line 1460940
    iget-object v0, p0, LX/9HD;->e:LX/9HK;

    invoke-virtual {v0, p1}, LX/9HK;->a(LX/9CP;)V

    .line 1460941
    return-void
.end method

.method public final a(Landroid/animation/ValueAnimator;)V
    .locals 1

    .prologue
    .line 1460953
    iget-object v0, p0, LX/9HD;->d:LX/9HB;

    invoke-virtual {v0, p1}, LX/9HB;->a(Landroid/animation/ValueAnimator;)V

    .line 1460954
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1460951
    iget-object v0, p0, LX/9HD;->e:LX/9HK;

    invoke-virtual {v0}, LX/9HK;->a()V

    .line 1460952
    return-void
.end method

.method public final dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1460950
    iget-object v0, p0, LX/9HD;->e:LX/9HK;

    invoke-virtual {v0, p1}, LX/9HK;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1460947
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->draw(Landroid/graphics/Canvas;)V

    .line 1460948
    iget-object v0, p0, LX/9HD;->e:LX/9HK;

    invoke-virtual {v0, p1}, LX/9HK;->a(Landroid/graphics/Canvas;)V

    .line 1460949
    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1460946
    iget-object v0, p0, LX/9HD;->d:LX/9HB;

    invoke-virtual {v0, p1}, LX/9HB;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 1

    .prologue
    .line 1460942
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1460943
    iget-object v0, p0, LX/9HD;->d:LX/9HB;

    .line 1460944
    iput-object p1, v0, LX/9HB;->c:Landroid/view/View$OnTouchListener;

    .line 1460945
    return-void
.end method
