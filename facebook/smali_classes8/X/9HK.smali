.class public LX/9HK;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:I


# instance fields
.field public final b:I

.field private c:LX/9CP;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:F

.field public e:F

.field private f:Z

.field private g:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0xff

    .line 1461051
    const/16 v0, 0xc8

    invoke-static {v0, v1, v1, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    sput v0, LX/9HK;->a:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1461052
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1461053
    if-eqz p1, :cond_0

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    :goto_0
    iput v0, p0, LX/9HK;->b:I

    .line 1461054
    return-void

    .line 1461055
    :cond_0
    invoke-static {}, Landroid/view/ViewConfiguration;->getTouchSlop()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1461056
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/9HK;->f:Z

    .line 1461057
    const/4 v0, 0x0

    iput-object v0, p0, LX/9HK;->c:LX/9CP;

    .line 1461058
    return-void
.end method

.method public final a(LX/9CP;)V
    .locals 1
    .param p1    # LX/9CP;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1461059
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/9HK;->f:Z

    .line 1461060
    iput-object p1, p0, LX/9HK;->c:LX/9CP;

    .line 1461061
    return-void
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1461062
    iget-boolean v0, p0, LX/9HK;->f:Z

    if-nez v0, :cond_0

    .line 1461063
    :goto_0
    return-void

    .line 1461064
    :cond_0
    sget v0, LX/9HK;->a:I

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    goto :goto_0
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1461065
    iget-boolean v2, p0, LX/9HK;->f:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/9HK;->c:LX/9CP;

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    .line 1461066
    :cond_1
    :goto_0
    return v0

    .line 1461067
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    and-int/lit16 v2, v2, 0xff

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 1461068
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iput v1, p0, LX/9HK;->d:F

    .line 1461069
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iput v1, p0, LX/9HK;->e:F

    .line 1461070
    iput-boolean v0, p0, LX/9HK;->g:Z

    goto :goto_0

    .line 1461071
    :pswitch_1
    iget-boolean v1, p0, LX/9HK;->g:Z

    if-eqz v1, :cond_1

    .line 1461072
    iget-object v1, p0, LX/9HK;->c:LX/9CP;

    invoke-virtual {v1}, LX/9CP;->b()V

    goto :goto_0

    .line 1461073
    :pswitch_2
    iget-boolean v2, p0, LX/9HK;->g:Z

    if-eqz v2, :cond_1

    .line 1461074
    iget v2, p0, LX/9HK;->d:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, p0, LX/9HK;->b:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-gtz v2, :cond_3

    iget v2, p0, LX/9HK;->e:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, p0, LX/9HK;->b:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_4

    :cond_3
    const/4 v2, 0x1

    :goto_1
    move v2, v2

    .line 1461075
    if-eqz v2, :cond_1

    .line 1461076
    iput-boolean v1, p0, LX/9HK;->g:Z

    goto :goto_0

    :cond_4
    const/4 v2, 0x0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
