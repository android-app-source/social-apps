.class public final LX/8mL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/stickers/store/StickerStoreFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/store/StickerStoreFragment;)V
    .locals 0

    .prologue
    .line 1398776
    iput-object p1, p0, LX/8mL;->a:Lcom/facebook/stickers/store/StickerStoreFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1398777
    iget-object v0, p0, LX/8mL;->a:Lcom/facebook/stickers/store/StickerStoreFragment;

    invoke-virtual {v0}, Lcom/facebook/stickers/store/StickerStoreFragment;->b()V

    .line 1398778
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1398779
    iget-object v0, p0, LX/8mL;->a:Lcom/facebook/stickers/store/StickerStoreFragment;

    iget-object v0, v0, Lcom/facebook/stickers/store/StickerStoreFragment;->n:LX/1CX;

    iget-object v1, p0, LX/8mL;->a:Lcom/facebook/stickers/store/StickerStoreFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, LX/4mm;->a(Landroid/content/res/Resources;)LX/4mn;

    move-result-object v1

    iget-object v2, p0, LX/8mL;->a:Lcom/facebook/stickers/store/StickerStoreFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2}, LX/5MF;->b(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v2

    .line 1398780
    iput-object v2, v1, LX/4mn;->b:Ljava/lang/String;

    .line 1398781
    move-object v1, v1

    .line 1398782
    const v2, 0x7f080039

    invoke-virtual {v1, v2}, LX/4mn;->b(I)LX/4mn;

    move-result-object v1

    invoke-virtual {v1}, LX/4mn;->l()LX/4mm;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1CX;->a(LX/4mm;)LX/2EJ;

    .line 1398783
    iget-object v0, p0, LX/8mL;->a:Lcom/facebook/stickers/store/StickerStoreFragment;

    iget-object v0, v0, Lcom/facebook/stickers/store/StickerStoreFragment;->d:LX/03V;

    sget-object v1, Lcom/facebook/stickers/store/StickerStoreFragment;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Reordering downloaded sticker pack failed"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1398784
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1398785
    invoke-direct {p0}, LX/8mL;->a()V

    return-void
.end method
