.class public final synthetic LX/94u;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I

.field public static final synthetic c:[I

.field public static final synthetic d:[I

.field public static final synthetic e:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1435798
    invoke-static {}, LX/6OP;->values()[LX/6OP;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/94u;->e:[I

    :try_start_0
    sget-object v0, LX/94u;->e:[I

    sget-object v1, LX/6OP;->HIGH:LX/6OP;

    invoke-virtual {v1}, LX/6OP;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_c

    :goto_0
    :try_start_1
    sget-object v0, LX/94u;->e:[I

    sget-object v1, LX/6OP;->MEDIUM:LX/6OP;

    invoke-virtual {v1}, LX/6OP;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_b

    .line 1435799
    :goto_1
    invoke-static {}, LX/94v;->values()[LX/94v;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/94u;->d:[I

    :try_start_2
    sget-object v0, LX/94u;->d:[I

    sget-object v1, LX/94v;->LOCAL_CONTACT_IDS:LX/94v;

    invoke-virtual {v1}, LX/94v;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_a

    :goto_2
    :try_start_3
    sget-object v0, LX/94u;->d:[I

    sget-object v1, LX/94v;->REMOTE_CONTACT_IDS:LX/94v;

    invoke-virtual {v1}, LX/94v;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_9

    .line 1435800
    :goto_3
    invoke-static {}, LX/6OQ;->values()[LX/6OQ;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/94u;->c:[I

    :try_start_4
    sget-object v0, LX/94u;->c:[I

    sget-object v1, LX/6OQ;->ADD:LX/6OQ;

    invoke-virtual {v1}, LX/6OQ;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_8

    :goto_4
    :try_start_5
    sget-object v0, LX/94u;->c:[I

    sget-object v1, LX/6OQ;->NONE:LX/6OQ;

    invoke-virtual {v1}, LX/6OQ;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_7

    :goto_5
    :try_start_6
    sget-object v0, LX/94u;->c:[I

    sget-object v1, LX/6OQ;->MODIFY:LX/6OQ;

    invoke-virtual {v1}, LX/6OQ;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    :goto_6
    :try_start_7
    sget-object v0, LX/94u;->c:[I

    sget-object v1, LX/6OQ;->REMOVE:LX/6OQ;

    invoke-virtual {v1}, LX/6OQ;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_5

    .line 1435801
    :goto_7
    invoke-static {}, Lcom/facebook/contacts/ContactSurface;->values()[Lcom/facebook/contacts/ContactSurface;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/94u;->b:[I

    :try_start_8
    sget-object v0, LX/94u;->b:[I

    sget-object v1, Lcom/facebook/contacts/ContactSurface;->GROWTH_CONTACT_IMPORTER:Lcom/facebook/contacts/ContactSurface;

    invoke-virtual {v1}, Lcom/facebook/contacts/ContactSurface;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_4

    :goto_8
    :try_start_9
    sget-object v0, LX/94u;->b:[I

    sget-object v1, Lcom/facebook/contacts/ContactSurface;->MESSENGER:Lcom/facebook/contacts/ContactSurface;

    invoke-virtual {v1}, Lcom/facebook/contacts/ContactSurface;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_3

    .line 1435802
    :goto_9
    invoke-static {}, LX/6ON;->values()[LX/6ON;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/94u;->a:[I

    :try_start_a
    sget-object v0, LX/94u;->a:[I

    sget-object v1, LX/6ON;->ADD:LX/6ON;

    invoke-virtual {v1}, LX/6ON;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_2

    :goto_a
    :try_start_b
    sget-object v0, LX/94u;->a:[I

    sget-object v1, LX/6ON;->MODIFY:LX/6ON;

    invoke-virtual {v1}, LX/6ON;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_1

    :goto_b
    :try_start_c
    sget-object v0, LX/94u;->a:[I

    sget-object v1, LX/6ON;->DELETE:LX/6ON;

    invoke-virtual {v1}, LX/6ON;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_0

    :goto_c
    return-void

    :catch_0
    goto :goto_c

    :catch_1
    goto :goto_b

    :catch_2
    goto :goto_a

    :catch_3
    goto :goto_9

    :catch_4
    goto :goto_8

    :catch_5
    goto :goto_7

    :catch_6
    goto :goto_6

    :catch_7
    goto :goto_5

    :catch_8
    goto :goto_4

    :catch_9
    goto/16 :goto_3

    :catch_a
    goto/16 :goto_2

    :catch_b
    goto/16 :goto_1

    :catch_c
    goto/16 :goto_0
.end method
