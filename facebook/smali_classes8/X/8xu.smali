.class public LX/8xu;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:LX/0Zb;

.field private final c:LX/0kv;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Zb;LX/0kv;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1424907
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1424908
    iput-object p1, p0, LX/8xu;->a:Landroid/content/Context;

    .line 1424909
    iput-object p2, p0, LX/8xu;->b:LX/0Zb;

    .line 1424910
    iput-object p3, p0, LX/8xu;->c:LX/0kv;

    .line 1424911
    return-void
.end method

.method public static a(LX/8xu;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;LX/0oG;)LX/0oG;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Ljava/lang/String;",
            "LX/0oG;",
            ")",
            "LX/0oG;"
        }
    .end annotation

    .prologue
    .line 1424893
    invoke-static {p1}, LX/1WF;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    .line 1424894
    invoke-static {p1}, LX/1WF;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 1424895
    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    .line 1424896
    :cond_0
    :goto_0
    return-object p3

    .line 1424897
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    .line 1424898
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v0

    .line 1424899
    const-string v2, "social_search"

    invoke-virtual {p3, v2}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 1424900
    iget-object v2, p0, LX/8xu;->c:LX/0kv;

    iget-object v3, p0, LX/8xu;->a:Landroid/content/Context;

    invoke-virtual {v2, v3}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    .line 1424901
    const-string v2, "person_id"

    invoke-virtual {p3, v2, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1424902
    invoke-virtual {p3, v1}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    .line 1424903
    const-string v2, "story_graphql_id"

    invoke-virtual {p3, v2, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1424904
    const-string v1, "comment_graphql_id"

    invoke-virtual {p3, v1, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/8xu;
    .locals 4

    .prologue
    .line 1424905
    new-instance v3, LX/8xu;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-static {p0}, LX/0kv;->a(LX/0QB;)LX/0kv;

    move-result-object v2

    check-cast v2, LX/0kv;

    invoke-direct {v3, v0, v1, v2}, LX/8xu;-><init>(Landroid/content/Context;LX/0Zb;LX/0kv;)V

    .line 1424906
    return-object v3
.end method
