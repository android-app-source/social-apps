.class public LX/9hj;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1526970
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/String;DDLjava/lang/String;)Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;
    .locals 6

    .prologue
    const v2, 0x285feb

    const/4 v1, 0x1

    .line 1526895
    invoke-static {p0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1526896
    new-instance v0, LX/5kg;

    invoke-direct {v0}, LX/5kg;-><init>()V

    new-instance v3, LX/5kh;

    invoke-direct {v3}, LX/5kh;-><init>()V

    .line 1526897
    iput-object p5, v3, LX/5kh;->b:Ljava/lang/String;

    .line 1526898
    move-object v3, v3

    .line 1526899
    new-instance v4, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v4, v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 1526900
    iput-object v4, v3, LX/5kh;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1526901
    move-object v3, v3

    .line 1526902
    invoke-virtual {v3}, LX/5kh;->a()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoModel$TaggerModel;

    move-result-object v3

    .line 1526903
    iput-object v3, v0, LX/5kg;->e:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoModel$TaggerModel;

    .line 1526904
    move-object v0, v0

    .line 1526905
    iput-boolean v1, v0, LX/5kg;->a:Z

    .line 1526906
    move-object v0, v0

    .line 1526907
    new-instance v1, LX/4a8;

    invoke-direct {v1}, LX/4a8;-><init>()V

    .line 1526908
    iput-wide p1, v1, LX/4a8;->a:D

    .line 1526909
    move-object v1, v1

    .line 1526910
    iput-wide p3, v1, LX/4a8;->b:D

    .line 1526911
    move-object v1, v1

    .line 1526912
    invoke-virtual {v1}, LX/4a8;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v1

    .line 1526913
    iput-object v1, v0, LX/5kg;->c:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    .line 1526914
    move-object v0, v0

    .line 1526915
    invoke-virtual {v0}, LX/5kg;->a()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoModel;

    move-result-object v1

    .line 1526916
    const-string v0, "-1"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, -0x2e4d6bd6

    .line 1526917
    :goto_1
    new-instance v2, LX/5kk;

    invoke-direct {v2}, LX/5kk;-><init>()V

    .line 1526918
    iput-object p0, v2, LX/5kk;->a:Ljava/lang/String;

    .line 1526919
    move-object v2, v2

    .line 1526920
    new-instance v3, LX/5kl;

    invoke-direct {v3}, LX/5kl;-><init>()V

    .line 1526921
    iput-object p0, v3, LX/5kl;->b:Ljava/lang/String;

    .line 1526922
    move-object v3, v3

    .line 1526923
    iput-object p5, v3, LX/5kl;->d:Ljava/lang/String;

    .line 1526924
    move-object v3, v3

    .line 1526925
    new-instance v4, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v4, v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 1526926
    iput-object v4, v3, LX/5kl;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1526927
    move-object v0, v3

    .line 1526928
    invoke-virtual {v0}, LX/5kl;->a()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    move-result-object v0

    .line 1526929
    iput-object v0, v2, LX/5kk;->b:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    .line 1526930
    move-object v0, v2

    .line 1526931
    iput-object v1, v0, LX/5kk;->c:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoModel;

    .line 1526932
    move-object v0, v0

    .line 1526933
    invoke-virtual {v0}, LX/5kk;->a()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;

    move-result-object v0

    .line 1526934
    return-object v0

    .line 1526935
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 1526936
    goto :goto_1
.end method

.method public static a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;)Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;
    .locals 8

    .prologue
    .line 1526955
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1526956
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->aN()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_3

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;

    .line 1526957
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->j()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    move-result-object v5

    const/4 v6, 0x0

    .line 1526958
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1526959
    if-eqz v5, :cond_1

    .line 1526960
    invoke-virtual {v5}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->e()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->e()Ljava/lang/String;

    move-result-object p0

    invoke-static {v7, p0}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1526961
    invoke-virtual {v5}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->c()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->c()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    invoke-virtual {p1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->c()Ljava/lang/String;

    move-result-object v7

    const-string p0, "-1"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    :cond_0
    const/4 v6, 0x1

    .line 1526962
    :cond_1
    move v5, v6

    .line 1526963
    if-nez v5, :cond_2

    .line 1526964
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1526965
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1526966
    :cond_3
    new-instance v0, LX/5kj;

    invoke-direct {v0}, LX/5kj;-><init>()V

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1526967
    iput-object v1, v0, LX/5kj;->a:LX/0Px;

    .line 1526968
    move-object v0, v0

    .line 1526969
    invoke-virtual {v0}, LX/5kj;->a()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;Ljava/lang/String;Ljava/lang/String;DD)Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;
    .locals 7

    .prologue
    .line 1526947
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1526948
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->aN()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1526949
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->aN()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    :cond_0
    move-object v1, p1

    move-wide v2, p3

    move-wide v4, p5

    move-object v6, p2

    .line 1526950
    invoke-static/range {v1 .. v6}, LX/9hj;->a(Ljava/lang/String;DDLjava/lang/String;)Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1526951
    new-instance v1, LX/5kj;

    invoke-direct {v1}, LX/5kj;-><init>()V

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1526952
    iput-object v0, v1, LX/5kj;->a:LX/0Px;

    .line 1526953
    move-object v0, v1

    .line 1526954
    invoke-virtual {v0}, LX/5kj;->a()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 1526937
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->aN()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1526938
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->aN()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;

    .line 1526939
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->j()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 1526940
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->j()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->c()Ljava/lang/String;

    move-result-object v5

    .line 1526941
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->j()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->e()Ljava/lang/String;

    move-result-object v0

    .line 1526942
    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz v5, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1526943
    const/4 v0, 0x1

    .line 1526944
    :goto_1
    return v0

    .line 1526945
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1526946
    goto :goto_1
.end method
