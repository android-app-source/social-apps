.class public LX/9F4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field private final a:LX/21q;

.field public b:Z


# direct methods
.method public constructor <init>(LX/21q;)V
    .locals 1

    .prologue
    .line 1458029
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1458030
    iput-object p1, p0, LX/9F4;->a:LX/21q;

    .line 1458031
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/9F4;->b:Z

    .line 1458032
    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 1

    .prologue
    .line 1458025
    iget-boolean v0, p0, LX/9F4;->b:Z

    move v0, v0

    .line 1458026
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9F4;->a:LX/21q;

    if-nez v0, :cond_1

    .line 1458027
    :cond_0
    :goto_0
    return-void

    .line 1458028
    :cond_1
    iget-object v0, p0, LX/9F4;->a:LX/21q;

    invoke-interface {v0}, LX/21q;->p()V

    goto :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1458024
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 1458016
    iget-boolean v0, p0, LX/9F4;->b:Z

    move v0, v0

    .line 1458017
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9F4;->a:LX/21q;

    if-nez v0, :cond_1

    .line 1458018
    :cond_0
    :goto_0
    return-void

    .line 1458019
    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_2

    if-nez p2, :cond_2

    if-nez p3, :cond_2

    if-eqz p4, :cond_0

    .line 1458020
    :cond_2
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_3

    .line 1458021
    iget-object v0, p0, LX/9F4;->a:LX/21q;

    invoke-interface {v0}, LX/21q;->o()V

    .line 1458022
    iget-object v0, p0, LX/9F4;->a:LX/21q;

    invoke-interface {v0}, LX/21q;->n()V

    goto :goto_0

    .line 1458023
    :cond_3
    iget-object v0, p0, LX/9F4;->a:LX/21q;

    invoke-interface {v0, p1, p2, p3, p4}, LX/21q;->a(Ljava/lang/CharSequence;III)V

    goto :goto_0
.end method
