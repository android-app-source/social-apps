.class public LX/A6s;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/A6r;


# instance fields
.field private a:LX/A6u;

.field private b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Double;",
            ">;>;"
        }
    .end annotation
.end field

.field private c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private d:[[D

.field private e:I

.field private f:I

.field public g:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/A78;I)V
    .locals 7
    .param p2    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1624997
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1624998
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "keyboard_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LX/A78;->a(LX/A78;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 1624999
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1625000
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, LX/A6s;->g:Ljava/util/LinkedHashMap;

    .line 1625001
    const-string v0, "characters"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 1625002
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1625003
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 1625004
    invoke-virtual {v3}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v4

    .line 1625005
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1625006
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1625007
    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1625008
    iget-object v6, p0, LX/A6s;->g:Ljava/util/LinkedHashMap;

    invoke-virtual {v6, v0, v5}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1625009
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1625010
    :cond_1
    invoke-direct {p0, p1, p2}, LX/A6s;->a(LX/A78;I)V

    .line 1625011
    new-instance v0, LX/A6u;

    iget-object v1, p0, LX/A6s;->b:Ljava/util/Map;

    iget-object v2, p0, LX/A6s;->c:Ljava/util/Map;

    iget-object v3, p0, LX/A6s;->d:[[D

    iget v4, p0, LX/A6s;->e:I

    invoke-direct {v0, v1, v2, v3, v4}, LX/A6u;-><init>(Ljava/util/Map;Ljava/util/Map;[[DI)V

    iput-object v0, p0, LX/A6s;->a:LX/A6u;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1625012
    :goto_2
    return-void

    .line 1625013
    :catch_0
    move-exception v0

    .line 1625014
    const-string v1, "TRANSLITERATION"

    const-string v2, "Could not parse or load Bigram Model files"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method private a(LX/A78;I)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 1625015
    const/4 v0, 0x1

    invoke-virtual {p1, v0, p2}, LX/A78;->a(II)Ljava/lang/String;

    move-result-object v0

    .line 1625016
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1625017
    const-string v0, "version"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/A6s;->f:I

    .line 1625018
    const-string v0, "model"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 1625019
    const-string v0, "max_phoneme_length"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/A6s;->e:I

    .line 1625020
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/A6s;->c:Ljava/util/Map;

    .line 1625021
    const-string v0, "character_index"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 1625022
    invoke-virtual {v2}, Lorg/json/JSONObject;->length()I

    move-result v4

    .line 1625023
    invoke-virtual {v2}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v5

    .line 1625024
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1625025
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1625026
    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 1625027
    iget-object v7, p0, LX/A6s;->c:Ljava/util/Map;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v7, v0, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1625028
    :cond_0
    filled-new-array {v4, v4}, [I

    move-result-object v0

    sget-object v2, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    invoke-static {v2, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[D

    iput-object v0, p0, LX/A6s;->d:[[D

    .line 1625029
    const-string v0, "bigram_probability"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    move v0, v1

    .line 1625030
    :goto_1
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 1625031
    invoke-virtual {v4, v0}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v5

    move v2, v1

    .line 1625032
    :goto_2
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v2, v6, :cond_1

    .line 1625033
    iget-object v6, p0, LX/A6s;->d:[[D

    aget-object v6, v6, v0

    invoke-virtual {v5, v2}, Lorg/json/JSONArray;->getDouble(I)D

    move-result-wide v8

    aput-wide v8, v6, v2

    .line 1625034
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1625035
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1625036
    :cond_2
    const-string v0, "phoneme_character_map"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 1625037
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/A6s;->b:Ljava/util/Map;

    .line 1625038
    invoke-virtual {v2}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v3

    .line 1625039
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1625040
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1625041
    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 1625042
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 1625043
    invoke-virtual {v4}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v6

    .line 1625044
    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1625045
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1625046
    invoke-virtual {v4, v1}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v8

    .line 1625047
    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    invoke-virtual {v5, v1, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 1625048
    :cond_3
    iget-object v1, p0, LX/A6s;->b:Ljava/util/Map;

    invoke-interface {v1, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 1625049
    :cond_4
    return-void
.end method


# virtual methods
.method public final a()LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1625050
    iget-object v0, p0, LX/A6s;->g:Ljava/util/LinkedHashMap;

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;I)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1625051
    iget-object v0, p0, LX/A6s;->a:LX/A6u;

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p1, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 1625052
    const/4 v5, 0x0

    .line 1625053
    invoke-static {v1}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v2

    .line 1625054
    iget-object v3, v0, LX/A6u;->f:Ljava/lang/String;

    invoke-static {v3}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v3

    .line 1625055
    if-le v3, v2, :cond_4

    iget-object v4, v0, LX/A6u;->f:Ljava/lang/String;

    invoke-virtual {v4, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1625056
    :goto_0
    move v2, v2

    .line 1625057
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 1625058
    invoke-static {v1}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v6

    .line 1625059
    iget-object v3, v0, LX/A6u;->g:Ljava/util/ArrayList;

    iget-object v4, v0, LX/A6u;->g:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v2, v4}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 1625060
    :goto_1
    if-gt v2, v6, :cond_2

    .line 1625061
    sget-object v3, LX/1zb;->a:LX/1zb;

    move-object v3, v3

    .line 1625062
    invoke-static {v3}, LX/508;->a(Ljava/util/Comparator;)LX/504;

    move-result-object v3

    mul-int/lit8 v4, p2, 0x2

    invoke-virtual {v3, v4}, LX/504;->a(I)LX/504;

    move-result-object v3

    invoke-virtual {v3}, LX/504;->a()LX/508;

    move-result-object v3

    .line 1625063
    iget-object v4, v0, LX/A6u;->g:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1625064
    invoke-interface {v5}, Ljava/util/Map;->clear()V

    .line 1625065
    const/4 v3, 0x1

    move v4, v3

    .line 1625066
    :goto_2
    iget v3, v0, LX/A6u;->d:I

    if-gt v4, v3, :cond_1

    sub-int v3, v2, v4

    if-ltz v3, :cond_1

    .line 1625067
    sub-int v3, v2, v4

    .line 1625068
    invoke-virtual {v1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 1625069
    iget-object p1, v0, LX/A6u;->a:Ljava/util/Map;

    invoke-interface {p1, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 1625070
    iget-object p1, v0, LX/A6u;->g:Ljava/util/ArrayList;

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/508;

    invoke-virtual {v3}, LX/508;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/A6v;

    .line 1625071
    invoke-static {v0, v3, p0, v5}, LX/A6u;->a(LX/A6u;LX/A6v;Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_3

    .line 1625072
    :cond_0
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_2

    .line 1625073
    :cond_1
    iget-object v3, v0, LX/A6u;->g:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/508;

    invoke-interface {v5}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/508;->addAll(Ljava/util/Collection;)Z

    .line 1625074
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1625075
    :cond_2
    invoke-static {v0, v6, p2}, LX/A6u;->a(LX/A6u;II)LX/508;

    move-result-object v3

    move-object v4, v3

    .line 1625076
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1625077
    const/4 v2, 0x0

    move v3, v2

    :goto_4
    if-ge v3, p2, :cond_3

    invoke-virtual {v4}, LX/508;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1625078
    invoke-virtual {v4}, LX/508;->poll()Ljava/lang/Object;

    move-result-object v2

    move-object v2, v2

    .line 1625079
    check-cast v2, LX/A6v;

    .line 1625080
    iget-object v6, v2, LX/A6v;->a:Ljava/lang/String;

    move-object v2, v6

    .line 1625081
    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1625082
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_4

    .line 1625083
    :cond_3
    iput-object v1, v0, LX/A6u;->f:Ljava/lang/String;

    .line 1625084
    move-object v0, v5

    .line 1625085
    return-object v0

    .line 1625086
    :cond_4
    if-ge v3, v2, :cond_5

    invoke-virtual {v1, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1625087
    add-int/lit8 v2, v3, 0x1

    goto/16 :goto_0

    .line 1625088
    :cond_5
    const/4 v2, 0x1

    goto/16 :goto_0
.end method
