.class public final LX/A0c;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:I

.field public c:Z

.field public d:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1606177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;)LX/A0c;
    .locals 0
    .param p1    # Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1606178
    iput-object p1, p0, LX/A0c;->a:Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;

    .line 1606179
    return-object p0
.end method

.method public final a(Z)LX/A0c;
    .locals 0

    .prologue
    .line 1606180
    iput-boolean p1, p0, LX/A0c;->c:Z

    .line 1606181
    return-object p0
.end method

.method public final a()Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 1606182
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1606183
    iget-object v1, p0, LX/A0c;->a:Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1606184
    const/4 v3, 0x4

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1606185
    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 1606186
    iget v1, p0, LX/A0c;->b:I

    invoke-virtual {v0, v4, v1, v5}, LX/186;->a(III)V

    .line 1606187
    const/4 v1, 0x2

    iget-boolean v3, p0, LX/A0c;->c:Z

    invoke-virtual {v0, v1, v3}, LX/186;->a(IZ)V

    .line 1606188
    const/4 v1, 0x3

    iget-boolean v3, p0, LX/A0c;->d:Z

    invoke-virtual {v0, v1, v3}, LX/186;->a(IZ)V

    .line 1606189
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1606190
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1606191
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1606192
    invoke-virtual {v1, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1606193
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1606194
    new-instance v1, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;

    invoke-direct {v1, v0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;-><init>(LX/15i;)V

    .line 1606195
    return-object v1
.end method
