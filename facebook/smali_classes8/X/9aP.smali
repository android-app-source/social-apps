.class public final LX/9aP;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source ""


# instance fields
.field public final synthetic a:LX/9aQ;


# direct methods
.method public constructor <init>(LX/9aQ;)V
    .locals 0

    .prologue
    .line 1513603
    iput-object p1, p0, LX/9aP;->a:LX/9aQ;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDown(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    .line 1513604
    iget-object v0, p0, LX/9aP;->a:LX/9aQ;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-static {v0, v1, v2}, LX/9aQ;->a$redex0(LX/9aQ;FF)LX/9aL;

    move-result-object v0

    .line 1513605
    if-eqz v0, :cond_0

    .line 1513606
    iget-object v1, p0, LX/9aP;->a:LX/9aQ;

    .line 1513607
    iput-object v0, v1, LX/9aQ;->k:LX/9aL;

    .line 1513608
    iget-object v0, p0, LX/9aP;->a:LX/9aQ;

    iget-object v0, v0, LX/9aQ;->k:LX/9aL;

    const/4 v5, 0x0

    .line 1513609
    const-wide/16 v3, 0x0

    iput-wide v3, v0, LX/9aL;->n:J

    .line 1513610
    iput v5, v0, LX/9aL;->f:F

    .line 1513611
    iput v5, v0, LX/9aL;->g:F

    .line 1513612
    const/4 v0, 0x1

    .line 1513613
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    .prologue
    .line 1513614
    iget-object v0, p0, LX/9aP;->a:LX/9aQ;

    iget-object v0, v0, LX/9aQ;->k:LX/9aL;

    .line 1513615
    iput p3, v0, LX/9aL;->f:F

    .line 1513616
    iget-object v0, p0, LX/9aP;->a:LX/9aQ;

    iget-object v0, v0, LX/9aQ;->k:LX/9aL;

    .line 1513617
    iput p4, v0, LX/9aL;->g:F

    .line 1513618
    const/4 v0, 0x1

    return v0
.end method

.method public final onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 2

    .prologue
    .line 1513619
    iget-object v0, p0, LX/9aP;->a:LX/9aQ;

    iget-object v0, v0, LX/9aQ;->k:LX/9aL;

    iget-object v1, p0, LX/9aP;->a:LX/9aQ;

    iget-object v1, v1, LX/9aQ;->k:LX/9aL;

    .line 1513620
    iget p1, v1, LX/9aL;->c:F

    move v1, p1

    .line 1513621
    sub-float/2addr v1, p3

    .line 1513622
    iput v1, v0, LX/9aL;->c:F

    .line 1513623
    iget-object v0, p0, LX/9aP;->a:LX/9aQ;

    iget-object v0, v0, LX/9aQ;->k:LX/9aL;

    iget-object v1, p0, LX/9aP;->a:LX/9aQ;

    iget-object v1, v1, LX/9aQ;->k:LX/9aL;

    .line 1513624
    iget p0, v1, LX/9aL;->d:F

    move v1, p0

    .line 1513625
    sub-float/2addr v1, p4

    .line 1513626
    iput v1, v0, LX/9aL;->d:F

    .line 1513627
    const/4 v0, 0x1

    return v0
.end method
