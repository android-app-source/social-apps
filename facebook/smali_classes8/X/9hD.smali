.class public LX/9hD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<M:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private A:LX/21D;

.field public B:Z

.field public C:Z

.field private D:LX/1Up;

.field private E:Landroid/widget/ImageView$ScaleType;

.field private a:Ljava/lang/String;

.field private b:Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<TM;>;"
        }
    .end annotation
.end field

.field private d:LX/9g5;

.field private e:LX/8Hh;

.field private f:Ljava/lang/String;

.field private g:LX/1bf;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Z

.field public m:Z

.field public n:Z

.field public o:Z

.field private p:I

.field private q:LX/74S;

.field public r:Lcom/facebook/photos/galleryutil/GalleryDeepLinkBinder$DeepLinkBinderConfig;

.field private s:LX/31M;

.field private t:I

.field public u:LX/9hM;

.field private v:I

.field private w:Landroid/content/res/Resources;

.field public x:I

.field public y:Ljava/lang/String;

.field private z:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1526251
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1526252
    iput-boolean v1, p0, LX/9hD;->m:Z

    .line 1526253
    iput-boolean v2, p0, LX/9hD;->n:Z

    .line 1526254
    iput-boolean v1, p0, LX/9hD;->o:Z

    .line 1526255
    const/4 v0, -0x1

    iput v0, p0, LX/9hD;->p:I

    .line 1526256
    iput-object v3, p0, LX/9hD;->q:LX/74S;

    .line 1526257
    iput-object v3, p0, LX/9hD;->r:Lcom/facebook/photos/galleryutil/GalleryDeepLinkBinder$DeepLinkBinderConfig;

    .line 1526258
    const/16 v0, 0xa

    iput v0, p0, LX/9hD;->v:I

    .line 1526259
    sget-object v0, LX/21D;->MEDIA_GALLERY_DO_NOT_USE:LX/21D;

    iput-object v0, p0, LX/9hD;->A:LX/21D;

    .line 1526260
    iput-boolean v1, p0, LX/9hD;->B:Z

    .line 1526261
    iput-boolean v2, p0, LX/9hD;->C:Z

    .line 1526262
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    iput-object v0, p0, LX/9hD;->b:Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    .line 1526263
    return-void
.end method

.method public constructor <init>(Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1526208
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1526209
    iput-boolean v1, p0, LX/9hD;->m:Z

    .line 1526210
    iput-boolean v2, p0, LX/9hD;->n:Z

    .line 1526211
    iput-boolean v1, p0, LX/9hD;->o:Z

    .line 1526212
    const/4 v0, -0x1

    iput v0, p0, LX/9hD;->p:I

    .line 1526213
    iput-object v3, p0, LX/9hD;->q:LX/74S;

    .line 1526214
    iput-object v3, p0, LX/9hD;->r:Lcom/facebook/photos/galleryutil/GalleryDeepLinkBinder$DeepLinkBinderConfig;

    .line 1526215
    const/16 v0, 0xa

    iput v0, p0, LX/9hD;->v:I

    .line 1526216
    sget-object v0, LX/21D;->MEDIA_GALLERY_DO_NOT_USE:LX/21D;

    iput-object v0, p0, LX/9hD;->A:LX/21D;

    .line 1526217
    iput-boolean v1, p0, LX/9hD;->B:Z

    .line 1526218
    iput-boolean v2, p0, LX/9hD;->C:Z

    .line 1526219
    iget-object v0, p1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->a:Ljava/lang/String;

    iput-object v0, p0, LX/9hD;->a:Ljava/lang/String;

    .line 1526220
    iget-object v0, p1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->b:Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    iput-object v0, p0, LX/9hD;->b:Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    .line 1526221
    iget-object v0, p1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->c:LX/0Px;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/9hD;->c:LX/0Px;

    .line 1526222
    iget-object v0, p1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->d:LX/9g5;

    iput-object v0, p0, LX/9hD;->d:LX/9g5;

    .line 1526223
    iget-object v0, p1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->e:LX/8Hh;

    iput-object v0, p0, LX/9hD;->e:LX/8Hh;

    .line 1526224
    iget-object v0, p1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->f:Ljava/lang/String;

    iput-object v0, p0, LX/9hD;->f:Ljava/lang/String;

    .line 1526225
    iget-object v0, p1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->g:LX/1bf;

    iput-object v0, p0, LX/9hD;->g:LX/1bf;

    .line 1526226
    iget-object v0, p1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->i:Ljava/lang/String;

    iput-object v0, p0, LX/9hD;->h:Ljava/lang/String;

    .line 1526227
    iget-object v0, p1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->j:Ljava/lang/String;

    iput-object v0, p0, LX/9hD;->i:Ljava/lang/String;

    .line 1526228
    iget-boolean v0, p1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->m:Z

    iput-boolean v0, p0, LX/9hD;->m:Z

    .line 1526229
    iget-boolean v0, p1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->n:Z

    iput-boolean v0, p0, LX/9hD;->n:Z

    .line 1526230
    iget-object v0, p1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->h:Ljava/lang/String;

    iput-object v0, p0, LX/9hD;->j:Ljava/lang/String;

    .line 1526231
    iget-object v0, p1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->k:Ljava/lang/String;

    iput-object v0, p0, LX/9hD;->k:Ljava/lang/String;

    .line 1526232
    iget-boolean v0, p1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->l:Z

    iput-boolean v0, p0, LX/9hD;->l:Z

    .line 1526233
    iget-boolean v0, p1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->o:Z

    iput-boolean v0, p0, LX/9hD;->o:Z

    .line 1526234
    iget-object v0, p1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->q:LX/74S;

    iput-object v0, p0, LX/9hD;->q:LX/74S;

    .line 1526235
    iget-object v0, p1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->r:Lcom/facebook/photos/galleryutil/GalleryDeepLinkBinder$DeepLinkBinderConfig;

    iput-object v0, p0, LX/9hD;->r:Lcom/facebook/photos/galleryutil/GalleryDeepLinkBinder$DeepLinkBinderConfig;

    .line 1526236
    iget v0, p1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->p:I

    iput v0, p0, LX/9hD;->p:I

    .line 1526237
    iget-object v0, p1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->s:LX/31M;

    iput-object v0, p0, LX/9hD;->s:LX/31M;

    .line 1526238
    iget v0, p1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->t:I

    iput v0, p0, LX/9hD;->t:I

    .line 1526239
    iget-object v0, p1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->u:LX/9hM;

    iput-object v0, p0, LX/9hD;->u:LX/9hM;

    .line 1526240
    iget v0, p1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->v:I

    iput v0, p0, LX/9hD;->v:I

    .line 1526241
    iget-object v0, p1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->w:Landroid/content/res/Resources;

    iput-object v0, p0, LX/9hD;->w:Landroid/content/res/Resources;

    .line 1526242
    iget v0, p1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->x:I

    iput v0, p0, LX/9hD;->x:I

    .line 1526243
    iget-object v0, p1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->y:Ljava/lang/String;

    iput-object v0, p0, LX/9hD;->y:Ljava/lang/String;

    .line 1526244
    iget-object v0, p1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->z:Ljava/lang/String;

    iput-object v0, p0, LX/9hD;->z:Ljava/lang/String;

    .line 1526245
    iget-object v0, p1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->A:LX/21D;

    iput-object v0, p0, LX/9hD;->A:LX/21D;

    .line 1526246
    iget-boolean v0, p1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->B:Z

    iput-boolean v0, p0, LX/9hD;->B:Z

    .line 1526247
    iget-boolean v0, p1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->C:Z

    iput-boolean v0, p0, LX/9hD;->C:Z

    .line 1526248
    iget-object v0, p1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->D:LX/1Up;

    iput-object v0, p0, LX/9hD;->D:LX/1Up;

    .line 1526249
    iget-object v0, p1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->E:Landroid/widget/ImageView$ScaleType;

    iput-object v0, p0, LX/9hD;->E:Landroid/widget/ImageView$ScaleType;

    .line 1526250
    return-void
.end method


# virtual methods
.method public final a(I)LX/9hD;
    .locals 0

    .prologue
    .line 1526206
    iput p1, p0, LX/9hD;->p:I

    .line 1526207
    return-object p0
.end method

.method public final a(LX/0Px;)LX/9hD;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<TM;>;)",
            "LX/9hD;"
        }
    .end annotation

    .prologue
    .line 1526201
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1526202
    iget-object v0, p0, LX/9hD;->c:LX/0Px;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Multiple sources set"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1526203
    iput-object p1, p0, LX/9hD;->c:LX/0Px;

    .line 1526204
    return-object p0

    .line 1526205
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/1bf;)LX/9hD;
    .locals 2

    .prologue
    .line 1526198
    iget-object v0, p0, LX/9hD;->f:Ljava/lang/String;

    const-string v1, "StartMediaId must be set in order to use this feature"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1526199
    iput-object p1, p0, LX/9hD;->g:LX/1bf;

    .line 1526200
    return-object p0
.end method

.method public final a(LX/74S;)LX/9hD;
    .locals 1

    .prologue
    .line 1526196
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/74S;

    iput-object v0, p0, LX/9hD;->q:LX/74S;

    .line 1526197
    return-object p0
.end method

.method public final a(LX/8Hh;)LX/9hD;
    .locals 1

    .prologue
    .line 1526194
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Hh;

    iput-object v0, p0, LX/9hD;->e:LX/8Hh;

    .line 1526195
    return-object p0
.end method

.method public final a(LX/9hM;)LX/9hD;
    .locals 0

    .prologue
    .line 1526192
    iput-object p1, p0, LX/9hD;->u:LX/9hM;

    .line 1526193
    return-object p0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/9hD;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/9hD;"
        }
    .end annotation

    .prologue
    .line 1526264
    if-nez p1, :cond_1

    .line 1526265
    :cond_0
    :goto_0
    return-object p0

    .line 1526266
    :cond_1
    invoke-static {p1}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    iput-boolean v0, p0, LX/9hD;->l:Z

    .line 1526267
    invoke-static {p1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    invoke-virtual {v0}, LX/162;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/9hD;->j:Ljava/lang/String;

    .line 1526268
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1526269
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1526270
    if-eqz v0, :cond_0

    .line 1526271
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/9hD;->k:Ljava/lang/String;

    .line 1526272
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/9hD;->h:Ljava/lang/String;

    .line 1526273
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/9hD;->i:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/photos/galleryutil/GalleryDeepLinkBinder$DeepLinkBinderConfig;)LX/9hD;
    .locals 0
    .param p1    # Lcom/facebook/photos/galleryutil/GalleryDeepLinkBinder$DeepLinkBinderConfig;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1526190
    iput-object p1, p0, LX/9hD;->r:Lcom/facebook/photos/galleryutil/GalleryDeepLinkBinder$DeepLinkBinderConfig;

    .line 1526191
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/9hD;
    .locals 2

    .prologue
    .line 1526186
    iget-object v0, p0, LX/9hD;->f:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Multiple start id\'s set"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1526187
    iput-object p1, p0, LX/9hD;->f:Ljava/lang/String;

    .line 1526188
    return-object p0

    .line 1526189
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)LX/9hD;
    .locals 0

    .prologue
    .line 1526184
    iput p1, p0, LX/9hD;->v:I

    .line 1526185
    return-object p0
.end method

.method public final b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;
    .locals 34

    .prologue
    .line 1526181
    move-object/from16 v0, p0

    iget-object v1, v0, LX/9hD;->b:Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    const-string v2, "no source specified"

    invoke-static {v1, v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1526182
    move-object/from16 v0, p0

    iget-object v1, v0, LX/9hD;->q:LX/74S;

    const-string v2, "must set gallery source"

    invoke-static {v1, v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1526183
    new-instance v1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/9hD;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/9hD;->b:Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/9hD;->c:LX/0Px;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/9hD;->d:LX/9g5;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/9hD;->e:LX/8Hh;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/9hD;->f:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/9hD;->g:LX/1bf;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/9hD;->h:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/9hD;->i:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/9hD;->j:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/9hD;->k:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v13, v0, LX/9hD;->l:Z

    move-object/from16 v0, p0

    iget-boolean v14, v0, LX/9hD;->m:Z

    move-object/from16 v0, p0

    iget-boolean v15, v0, LX/9hD;->n:Z

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/9hD;->o:Z

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/9hD;->p:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/9hD;->q:LX/74S;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/9hD;->r:Lcom/facebook/photos/galleryutil/GalleryDeepLinkBinder$DeepLinkBinderConfig;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/9hD;->s:LX/31M;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/9hD;->t:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/9hD;->u:LX/9hM;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/9hD;->v:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/9hD;->w:Landroid/content/res/Resources;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/9hD;->x:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/9hD;->y:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/9hD;->z:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/9hD;->A:LX/21D;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/9hD;->B:Z

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/9hD;->C:Z

    move/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/9hD;->D:LX/1Up;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/9hD;->E:Landroid/widget/ImageView$ScaleType;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-direct/range {v1 .. v33}, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;-><init>(Ljava/lang/String;Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;LX/0Px;LX/9g5;LX/8Hh;Ljava/lang/String;LX/1bf;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZILX/74S;Lcom/facebook/photos/galleryutil/GalleryDeepLinkBinder$DeepLinkBinderConfig;LX/31M;ILX/9hM;ILandroid/content/res/Resources;ILjava/lang/String;Ljava/lang/String;LX/21D;ZZLX/1Up;Landroid/widget/ImageView$ScaleType;B)V

    return-object v1
.end method

.method public final c(I)LX/9hD;
    .locals 0

    .prologue
    .line 1526171
    iput p1, p0, LX/9hD;->x:I

    .line 1526172
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/9hD;
    .locals 0

    .prologue
    .line 1526179
    iput-object p1, p0, LX/9hD;->y:Ljava/lang/String;

    .line 1526180
    return-object p0
.end method

.method public final c(Z)LX/9hD;
    .locals 0

    .prologue
    .line 1526177
    iput-boolean p1, p0, LX/9hD;->o:Z

    .line 1526178
    return-object p0
.end method

.method public final d(Ljava/lang/String;)LX/9hD;
    .locals 0

    .prologue
    .line 1526175
    iput-object p1, p0, LX/9hD;->z:Ljava/lang/String;

    .line 1526176
    return-object p0
.end method

.method public final e(Z)LX/9hD;
    .locals 0

    .prologue
    .line 1526173
    iput-boolean p1, p0, LX/9hD;->C:Z

    .line 1526174
    return-object p0
.end method
