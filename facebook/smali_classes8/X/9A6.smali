.class public LX/9A6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/9A8;

.field private final c:LX/43M;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Er;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/1FZ;

.field private g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public i:Z

.field public j:Z

.field public k:Lcom/facebook/productionprompts/logging/PromptAnalytics;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLAlbum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:I

.field public n:I


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Ot;LX/9A8;LX/43M;LX/0Ot;LX/1FZ;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;",
            "LX/9A8;",
            "LX/43M;",
            "LX/0Ot",
            "<",
            "LX/1Er;",
            ">;",
            "LX/1FZ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1448854
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1448855
    const/16 v0, 0x200

    iput v0, p0, LX/9A6;->m:I

    .line 1448856
    const/16 v0, 0x140

    iput v0, p0, LX/9A6;->n:I

    .line 1448857
    iput-object p1, p0, LX/9A6;->a:Landroid/content/Context;

    .line 1448858
    iput-object p2, p0, LX/9A6;->e:LX/0Ot;

    .line 1448859
    iput-object p3, p0, LX/9A6;->b:LX/9A8;

    .line 1448860
    iput-object p4, p0, LX/9A6;->c:LX/43M;

    .line 1448861
    iput-object p5, p0, LX/9A6;->d:LX/0Ot;

    .line 1448862
    iput-object p6, p0, LX/9A6;->f:LX/1FZ;

    .line 1448863
    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLImage;I)Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3

    .prologue
    .line 1448840
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v0

    .line 1448841
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v1

    .line 1448842
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 1448843
    :cond_0
    invoke-static {p0}, LX/2dc;->a(Lcom/facebook/graphql/model/GraphQLImage;)LX/2dc;

    move-result-object v0

    invoke-virtual {v0}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 1448844
    :goto_0
    return-object v0

    .line 1448845
    :cond_1
    if-le v0, v1, :cond_2

    .line 1448846
    mul-int/2addr v1, p1

    div-int v0, v1, v0

    move v2, v0

    move v0, p1

    move p1, v2

    .line 1448847
    :goto_1
    invoke-static {p0}, LX/2dc;->a(Lcom/facebook/graphql/model/GraphQLImage;)LX/2dc;

    move-result-object v1

    .line 1448848
    iput v0, v1, LX/2dc;->i:I

    .line 1448849
    move-object v0, v1

    .line 1448850
    iput p1, v0, LX/2dc;->c:I

    .line 1448851
    move-object v0, v0

    .line 1448852
    invoke-virtual {v0}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_0

    .line 1448853
    :cond_2
    mul-int/2addr v0, p1

    div-int/2addr v0, v1

    goto :goto_1
.end method

.method private a(Lcom/facebook/ipc/media/MediaItem;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLMedia;
    .locals 13

    .prologue
    const/4 v1, 0x0

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 1448724
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object v0

    sget-object v2, LX/4gF;->PHOTO:LX/4gF;

    if-eq v0, v2, :cond_0

    sget-object v0, Lcom/facebook/ipc/media/MediaItem;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1448725
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object v0

    sget-object v2, LX/4gF;->PHOTO:LX/4gF;

    if-ne v0, v2, :cond_7

    invoke-static {p2}, Landroid/webkit/URLUtil;->isValidUrl(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v0

    move-object v7, v0

    .line 1448726
    :goto_0
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object p2

    .line 1448727
    :cond_1
    iget-object v0, p0, LX/9A6;->a:Landroid/content/Context;

    invoke-static {v0, v7}, LX/2Qx;->a(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v0

    .line 1448728
    const/4 v2, -0x1

    if-ne v0, v2, :cond_14

    move v8, v1

    .line 1448729
    :goto_1
    instance-of v0, p1, Lcom/facebook/photos/base/media/PhotoItem;

    if-eqz v0, :cond_2

    move-object v6, p1

    .line 1448730
    check-cast v6, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1448731
    iget-boolean v0, v6, Lcom/facebook/photos/base/media/PhotoItem;->d:Z

    move v0, v0

    .line 1448732
    if-eqz v0, :cond_2

    invoke-virtual {v6}, Lcom/facebook/photos/base/media/PhotoItem;->v()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1448733
    iget-object v0, v6, Lcom/facebook/photos/base/media/PhotoItem;->j:Landroid/net/Uri;

    move-object v0, v0

    .line 1448734
    if-eqz v0, :cond_8

    .line 1448735
    iget-object v0, v6, Lcom/facebook/photos/base/media/PhotoItem;->j:Landroid/net/Uri;

    move-object v7, v0

    .line 1448736
    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p2

    move v1, v9

    .line 1448737
    :cond_2
    :goto_2
    invoke-static {p2}, LX/2Qx;->a(Ljava/lang/String;)LX/434;

    move-result-object v2

    .line 1448738
    rem-int/lit16 v0, v8, 0xb4

    if-nez v0, :cond_a

    iget v0, v2, LX/434;->b:I

    .line 1448739
    :goto_3
    rem-int/lit16 v3, v8, 0xb4

    if-nez v3, :cond_b

    iget v2, v2, LX/434;->a:I

    .line 1448740
    :goto_4
    new-instance v3, LX/2dc;

    invoke-direct {v3}, LX/2dc;-><init>()V

    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1448741
    iput-object v4, v3, LX/2dc;->h:Ljava/lang/String;

    .line 1448742
    move-object v3, v3

    .line 1448743
    iput v0, v3, LX/2dc;->i:I

    .line 1448744
    move-object v0, v3

    .line 1448745
    iput v2, v0, LX/2dc;->c:I

    .line 1448746
    move-object v0, v0

    .line 1448747
    invoke-virtual {v0}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    .line 1448748
    iget v0, p0, LX/9A6;->m:I

    invoke-static {v3, v0}, LX/9A6;->a(Lcom/facebook/graphql/model/GraphQLImage;I)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    .line 1448749
    iget v0, p0, LX/9A6;->n:I

    invoke-static {v3, v0}, LX/9A6;->a(Lcom/facebook/graphql/model/GraphQLImage;I)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 1448750
    :goto_5
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object v4

    sget-object v5, LX/4gF;->VIDEO:LX/4gF;

    if-ne v4, v5, :cond_16

    .line 1448751
    sget-object v4, Lcom/facebook/ipc/media/MediaItem;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->i()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_15

    const v4, 0x68aef5ca

    .line 1448752
    :goto_6
    move v4, v4

    .line 1448753
    new-instance v5, LX/4XB;

    invoke-direct {v5}, LX/4XB;-><init>()V

    .line 1448754
    iput-object v3, v5, LX/4XB;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1448755
    move-object v5, v5

    .line 1448756
    iput-object v2, v5, LX/4XB;->V:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1448757
    move-object v2, v5

    .line 1448758
    iput-object v0, v2, LX/4XB;->ac:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1448759
    move-object v0, v2

    .line 1448760
    new-instance v2, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v2, v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 1448761
    iput-object v2, v0, LX/4XB;->bQ:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1448762
    move-object v2, v0

    .line 1448763
    if-eqz v3, :cond_10

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v0

    .line 1448764
    :goto_7
    iput v0, v2, LX/4XB;->S:I

    .line 1448765
    move-object v0, v2

    .line 1448766
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v9

    .line 1448767
    :cond_3
    iput v9, v0, LX/4XB;->bO:I

    .line 1448768
    move-object v0, v0

    .line 1448769
    iget-object v2, p1, Lcom/facebook/ipc/media/MediaItem;->c:Lcom/facebook/ipc/media/data/LocalMediaData;

    move-object v2, v2

    .line 1448770
    if-eqz v2, :cond_4

    .line 1448771
    iget-object v2, p1, Lcom/facebook/ipc/media/MediaItem;->c:Lcom/facebook/ipc/media/data/LocalMediaData;

    move-object v2, v2

    .line 1448772
    iget-wide v11, v2, Lcom/facebook/ipc/media/data/LocalMediaData;->mMediaStoreId:J

    move-wide v6, v11

    .line 1448773
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    :cond_4
    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1448774
    iput-object v2, v0, LX/4XB;->T:Ljava/lang/String;

    .line 1448775
    move-object v0, v0

    .line 1448776
    iput-boolean v1, v0, LX/4XB;->ax:Z

    .line 1448777
    move-object v0, v0

    .line 1448778
    sget-object v1, Lcom/facebook/ipc/media/MediaItem;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1448779
    iput-object v3, v0, LX/4XB;->d:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1448780
    move-object v1, v0

    .line 1448781
    iput-object v3, v1, LX/4XB;->V:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1448782
    :cond_5
    const v1, 0x4ed245b

    if-ne v4, v1, :cond_6

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 1448783
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1448784
    iput-object v1, v0, LX/4XB;->bc:Ljava/lang/String;

    .line 1448785
    :cond_6
    invoke-virtual {v0}, LX/4XB;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    return-object v0

    .line 1448786
    :cond_7
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    move-object v7, v0

    goto/16 :goto_0

    .line 1448787
    :cond_8
    const-string v0, "equirectangular"

    .line 1448788
    iget-object v1, v6, Lcom/facebook/photos/base/media/PhotoItem;->e:Lcom/facebook/bitmaps/SphericalPhotoMetadata;

    move-object v1, v1

    .line 1448789
    iget-object v2, v1, Lcom/facebook/bitmaps/SphericalPhotoMetadata;->mProjectionType:Ljava/lang/String;

    move-object v1, v2

    .line 1448790
    invoke-static {v0, v1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    const/4 v0, 0x3

    .line 1448791
    :goto_8
    iget-object v1, p0, LX/9A6;->c:LX/43M;

    new-instance v2, LX/431;

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->g()I

    move-result v3

    mul-int/lit16 v4, v0, 0x200

    invoke-direct {v2, v3, v4}, LX/431;-><init>(II)V

    invoke-virtual {v1, p1, v2}, LX/43M;->a(Lcom/facebook/ipc/media/MediaItem;LX/431;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1448792
    if-eqz v1, :cond_13

    .line 1448793
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    div-int v4, v2, v0

    .line 1448794
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    div-int/lit8 v2, v4, 0x2

    sub-int v2, v0, v2

    .line 1448795
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    div-int/lit8 v3, v4, 0x2

    sub-int v3, v0, v3

    .line 1448796
    iget-object v0, p0, LX/9A6;->f:LX/1FZ;

    move v5, v4

    invoke-virtual/range {v0 .. v5}, LX/1FZ;->a(Landroid/graphics/Bitmap;IIII)LX/1FJ;

    move-result-object v2

    .line 1448797
    invoke-virtual {v2}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 1448798
    iget-object v1, p0, LX/9A6;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Er;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "FB_V_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/1t3;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "."

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {v5}, Landroid/graphics/Bitmap$CompressFormat;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    invoke-virtual {v1, v3, v4, v5}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;

    move-result-object v1

    .line 1448799
    :try_start_0
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x50

    invoke-static {v0, v3, v4, v1}, LX/2Qx;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;ILjava/io/File;)V

    .line 1448800
    invoke-virtual {v6, v1}, Lcom/facebook/photos/base/media/PhotoItem;->a(Ljava/io/File;)V

    .line 1448801
    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v7

    .line 1448802
    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;
    :try_end_0
    .catch LX/42w; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object p2

    .line 1448803
    invoke-virtual {v2}, LX/1FJ;->close()V

    move v1, v9

    .line 1448804
    goto/16 :goto_2

    :cond_9
    move v0, v9

    .line 1448805
    goto/16 :goto_8

    .line 1448806
    :catch_0
    invoke-virtual {v2}, LX/1FJ;->close()V

    move v1, v9

    .line 1448807
    goto/16 :goto_2

    .line 1448808
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/1FJ;->close()V

    throw v0

    .line 1448809
    :cond_a
    iget v0, v2, LX/434;->a:I

    goto/16 :goto_3

    .line 1448810
    :cond_b
    iget v2, v2, LX/434;->b:I

    goto/16 :goto_4

    .line 1448811
    :cond_c
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object v0

    sget-object v2, LX/4gF;->VIDEO:LX/4gF;

    if-ne v0, v2, :cond_12

    .line 1448812
    iget-object v0, p0, LX/9A6;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b0c52

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    move-object v0, p1

    .line 1448813
    check-cast v0, Lcom/facebook/photos/base/media/VideoItem;

    .line 1448814
    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->k()F

    move-result v2

    .line 1448815
    const/high16 v4, 0x7fc00000    # NaNf

    cmpl-float v4, v2, v4

    if-eqz v4, :cond_11

    const/4 v4, 0x0

    cmpl-float v4, v2, v4

    if-lez v4, :cond_11

    .line 1448816
    int-to-float v4, v3

    div-float v2, v4, v2

    float-to-int v2, v2

    .line 1448817
    :goto_9
    invoke-virtual {v0}, Lcom/facebook/photos/base/media/VideoItem;->s()Landroid/net/Uri;

    move-result-object v4

    if-eqz v4, :cond_e

    .line 1448818
    invoke-virtual {v0}, Lcom/facebook/photos/base/media/VideoItem;->s()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p2

    .line 1448819
    :cond_d
    :goto_a
    new-instance v0, LX/2dc;

    invoke-direct {v0}, LX/2dc;-><init>()V

    .line 1448820
    iput-object p2, v0, LX/2dc;->h:Ljava/lang/String;

    .line 1448821
    move-object v0, v0

    .line 1448822
    iput v3, v0, LX/2dc;->i:I

    .line 1448823
    move-object v0, v0

    .line 1448824
    iput v2, v0, LX/2dc;->c:I

    .line 1448825
    move-object v0, v0

    .line 1448826
    invoke-virtual {v0}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    move-object v2, v10

    move-object v3, v0

    move-object v0, v10

    goto/16 :goto_5

    .line 1448827
    :cond_e
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 1448828
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/2Qx;->a(Ljava/lang/String;)LX/434;

    move-result-object v0

    .line 1448829
    iget v4, v0, LX/434;->b:I

    if-lez v4, :cond_d

    iget v4, v0, LX/434;->a:I

    if-lez v4, :cond_d

    .line 1448830
    iget v2, v0, LX/434;->a:I

    mul-int/2addr v2, v3

    iget v0, v0, LX/434;->b:I

    div-int/2addr v2, v0

    goto :goto_a

    .line 1448831
    :cond_f
    iget-object v0, p0, LX/9A6;->c:LX/43M;

    new-instance v4, LX/431;

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->g()I

    move-result v5

    invoke-direct {v4, v5, v3}, LX/431;-><init>(II)V

    invoke-virtual {v0, p1, v4}, LX/43M;->a(Lcom/facebook/ipc/media/MediaItem;LX/431;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 1448832
    iget-object v0, p0, LX/9A6;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Er;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "FB_V_"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/1t3;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "."

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v7, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {v7}, Landroid/graphics/Bitmap$CompressFormat;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    sget-object v7, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    invoke-virtual {v0, v5, v6, v7}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;

    move-result-object v0

    .line 1448833
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p2

    .line 1448834
    if-eqz v4, :cond_d

    .line 1448835
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    mul-int/2addr v2, v3

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    div-int/2addr v2, v5

    .line 1448836
    :try_start_1
    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v6, 0x50

    invoke-static {v4, v5, v6, v0}, LX/2Qx;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;ILjava/io/File;)V
    :try_end_1
    .catch LX/42w; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_a

    :catch_1
    goto/16 :goto_a

    :cond_10
    move v0, v9

    .line 1448837
    goto/16 :goto_7

    :cond_11
    move v2, v3

    goto/16 :goto_9

    :cond_12
    move-object v0, v10

    move-object v2, v10

    move-object v3, v10

    goto/16 :goto_5

    :cond_13
    move v1, v9

    goto/16 :goto_2

    :cond_14
    move v8, v0

    goto/16 :goto_1

    .line 1448838
    :cond_15
    const v4, 0x4ed245b

    goto/16 :goto_6

    .line 1448839
    :cond_16
    const v4, 0x4984e12

    goto/16 :goto_6
.end method

.method public static b(LX/0QB;)LX/9A6;
    .locals 13

    .prologue
    .line 1448586
    new-instance v0, LX/9A6;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    const/16 v2, 0x1032

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    .line 1448587
    new-instance v4, LX/9A8;

    invoke-static {p0}, LX/99L;->a(LX/0QB;)LX/99L;

    move-result-object v3

    check-cast v3, LX/99L;

    invoke-direct {v4, v3}, LX/9A8;-><init>(LX/99L;)V

    .line 1448588
    move-object v3, v4

    .line 1448589
    check-cast v3, LX/9A8;

    .line 1448590
    new-instance v7, LX/43M;

    const-class v8, Landroid/content/Context;

    invoke-interface {p0, v8}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    invoke-static {p0}, LX/43D;->b(LX/0QB;)LX/43C;

    move-result-object v9

    check-cast v9, LX/43C;

    invoke-static {p0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v10

    check-cast v10, LX/11i;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v11

    check-cast v11, LX/0So;

    invoke-static {p0}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v12

    check-cast v12, Ljava/util/Random;

    invoke-direct/range {v7 .. v12}, LX/43M;-><init>(Landroid/content/Context;LX/43C;LX/11i;LX/0So;Ljava/util/Random;)V

    .line 1448591
    move-object v4, v7

    .line 1448592
    check-cast v4, LX/43M;

    const/16 v5, 0x2d9

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {p0}, LX/1Et;->a(LX/0QB;)LX/1FZ;

    move-result-object v6

    check-cast v6, LX/1FZ;

    invoke-direct/range {v0 .. v6}, LX/9A6;-><init>(Landroid/content/Context;LX/0Ot;LX/9A8;LX/43M;LX/0Ot;LX/1FZ;)V

    .line 1448593
    return-object v0
.end method


# virtual methods
.method public final a(LX/0Px;)LX/9A6;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)",
            "LX/9A6;"
        }
    .end annotation

    .prologue
    .line 1448705
    invoke-static {p1}, LX/7kq;->e(LX/0Px;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/9A6;->g:LX/0Px;

    .line 1448706
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1448707
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_6

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1448708
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 1448709
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->a()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object v4

    sget-object v5, LX/4gF;->PHOTO:LX/4gF;

    if-ne v4, v5, :cond_3

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 1448710
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getDisplayUri()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 1448711
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getDisplayUri()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1448712
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1448713
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 1448714
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1448715
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1448716
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->a()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object v4

    sget-object v5, LX/4gF;->VIDEO:LX/4gF;

    if-ne v4, v5, :cond_5

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->f()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 1448717
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->f()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getDisplayUri()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 1448718
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->f()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getDisplayUri()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1448719
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1448720
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1448721
    :cond_6
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 1448722
    iput-object v0, p0, LX/9A6;->h:LX/0Px;

    .line 1448723
    return-object p0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .locals 14
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1448595
    iget-object v0, p0, LX/9A6;->g:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v5

    .line 1448596
    :goto_0
    return-object v0

    .line 1448597
    :cond_0
    iget-object v0, p0, LX/9A6;->h:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    iget-object v1, p0, LX/9A6;->g:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ne v0, v1, :cond_1

    move v0, v2

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1448598
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    move v4, v3

    .line 1448599
    :goto_2
    iget-object v0, p0, LX/9A6;->g:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v4, v0, :cond_2

    .line 1448600
    iget-object v0, p0, LX/9A6;->g:LX/0Px;

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    iget-object v1, p0, LX/9A6;->h:LX/0Px;

    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {p0, v0, v1}, LX/9A6;->a(Lcom/facebook/ipc/media/MediaItem;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 1448601
    invoke-virtual {v6, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1448602
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_2

    :cond_1
    move v0, v3

    .line 1448603
    goto :goto_1

    .line 1448604
    :cond_2
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v8

    .line 1448605
    iget-boolean v0, p0, LX/9A6;->j:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/9A6;->g:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-le v0, v2, :cond_4

    move v7, v2

    .line 1448606
    :goto_3
    if-eqz v7, :cond_e

    .line 1448607
    iget-object v0, p0, LX/9A6;->b:LX/9A8;

    invoke-virtual {v0, v8}, LX/9A8;->a(LX/0Px;)LX/99K;

    move-result-object v0

    move-object v1, v0

    .line 1448608
    :goto_4
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    move v4, v3

    .line 1448609
    :goto_5
    iget-object v0, p0, LX/9A6;->g:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v4, v0, :cond_5

    .line 1448610
    if-eqz v7, :cond_d

    .line 1448611
    if-nez v1, :cond_f

    .line 1448612
    const/4 v0, 0x0

    .line 1448613
    :goto_6
    move-object v0, v0

    .line 1448614
    move-object v6, v0

    .line 1448615
    :goto_7
    iget-object v0, p0, LX/9A6;->g:LX/0Px;

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 1448616
    iget-object v10, p0, LX/9A6;->k:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    if-eqz v10, :cond_13

    iget-object v10, p0, LX/9A6;->k:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    iget-object v10, v10, Lcom/facebook/productionprompts/logging/PromptAnalytics;->promptId:Ljava/lang/String;

    if-eqz v10, :cond_13

    .line 1448617
    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object v10

    sget-object v11, LX/4gF;->VIDEO:LX/4gF;

    if-ne v10, v11, :cond_12

    sget-object v10, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->INSPIRATION_VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    :goto_8
    invoke-static {v10}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v10

    .line 1448618
    :goto_9
    move-object v10, v10

    .line 1448619
    invoke-virtual {v8, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 1448620
    new-instance v11, LX/39x;

    invoke-direct {v11}, LX/39x;-><init>()V

    const/4 v12, 0x1

    .line 1448621
    iput-boolean v12, v11, LX/39x;->i:Z

    .line 1448622
    move-object v11, v11

    .line 1448623
    const-string v12, "unknown"

    .line 1448624
    iput-object v12, v11, LX/39x;->e:Ljava/lang/String;

    .line 1448625
    move-object v11, v11

    .line 1448626
    iput-object v0, v11, LX/39x;->j:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 1448627
    move-object v11, v11

    .line 1448628
    sget-object v12, LX/0Q7;->a:LX/0Px;

    move-object v12, v12

    .line 1448629
    iput-object v12, v11, LX/39x;->b:LX/0Px;

    .line 1448630
    move-object v11, v11

    .line 1448631
    iput-object v10, v11, LX/39x;->p:LX/0Px;

    .line 1448632
    move-object v11, v11

    .line 1448633
    if-eqz v6, :cond_3

    .line 1448634
    invoke-static {v6}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v12

    .line 1448635
    iput-object v12, v11, LX/39x;->o:LX/0Px;

    .line 1448636
    :cond_3
    invoke-virtual {v11}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v11

    move-object v0, v11

    .line 1448637
    invoke-virtual {v9, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1448638
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_5

    :cond_4
    move v7, v3

    .line 1448639
    goto :goto_3

    .line 1448640
    :cond_5
    iget-object v0, p0, LX/9A6;->l:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-nez v0, :cond_6

    iget-object v0, p0, LX/9A6;->g:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v2, :cond_6

    .line 1448641
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    goto/16 :goto_0

    .line 1448642
    :cond_6
    const-string v0, ""

    .line 1448643
    iget-object v1, p0, LX/9A6;->l:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-eqz v1, :cond_c

    iget-object v1, p0, LX/9A6;->l:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbum;->x()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    if-eqz v1, :cond_c

    .line 1448644
    iget-object v0, p0, LX/9A6;->l:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->x()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 1448645
    :goto_a
    iget-object v0, p0, LX/9A6;->g:LX/0Px;

    invoke-static {v0}, LX/7kz;->f(LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1448646
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    goto/16 :goto_0

    .line 1448647
    :cond_7
    iget-object v0, p0, LX/9A6;->k:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    if-eqz v0, :cond_9

    iget-object v0, p0, LX/9A6;->k:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    iget-object v0, v0, Lcom/facebook/productionprompts/logging/PromptAnalytics;->promptId:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 1448648
    iget-object v0, p0, LX/9A6;->g:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object v0

    sget-object v4, LX/4gF;->VIDEO:LX/4gF;

    if-ne v0, v4, :cond_8

    .line 1448649
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->INSPIRATION_VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 1448650
    :goto_b
    new-instance v4, LX/39x;

    invoke-direct {v4}, LX/39x;-><init>()V

    .line 1448651
    iput-boolean v2, v4, LX/39x;->i:Z

    .line 1448652
    move-object v4, v4

    .line 1448653
    iget-object v6, p0, LX/9A6;->l:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-eqz v6, :cond_b

    .line 1448654
    :goto_c
    iput-boolean v2, v4, LX/39x;->h:Z

    .line 1448655
    move-object v2, v4

    .line 1448656
    iput-object v1, v2, LX/39x;->k:Ljava/lang/String;

    .line 1448657
    move-object v1, v2

    .line 1448658
    const-string v2, "unknown"

    .line 1448659
    iput-object v2, v1, LX/39x;->e:Ljava/lang/String;

    .line 1448660
    move-object v1, v1

    .line 1448661
    iput-object v5, v1, LX/39x;->j:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 1448662
    move-object v1, v1

    .line 1448663
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 1448664
    iput-object v2, v1, LX/39x;->b:LX/0Px;

    .line 1448665
    move-object v1, v1

    .line 1448666
    iput-object v0, v1, LX/39x;->p:LX/0Px;

    .line 1448667
    move-object v0, v1

    .line 1448668
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1448669
    iput-object v1, v0, LX/39x;->q:LX/0Px;

    .line 1448670
    move-object v0, v0

    .line 1448671
    invoke-virtual {v0}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    goto/16 :goto_0

    .line 1448672
    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->INSPIRATION_PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_b

    .line 1448673
    :cond_9
    iget-boolean v0, p0, LX/9A6;->i:Z

    if-eqz v0, :cond_a

    .line 1448674
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SLIDESHOW:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_b

    .line 1448675
    :cond_a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ALBUM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_b

    :cond_b
    move v2, v3

    .line 1448676
    goto :goto_c

    :cond_c
    move-object v1, v0

    goto :goto_a

    :cond_d
    move-object v6, v5

    goto/16 :goto_7

    :cond_e
    move-object v1, v5

    goto/16 :goto_4

    .line 1448677
    :cond_f
    iget-object v0, v1, LX/99K;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lt v4, v0, :cond_11

    .line 1448678
    const/4 v0, 0x0

    .line 1448679
    :goto_d
    move-object v0, v0

    .line 1448680
    if-eqz v0, :cond_10

    .line 1448681
    new-instance v6, LX/4Yt;

    invoke-direct {v6}, LX/4Yt;-><init>()V

    iget v10, v0, LX/99J;->c:I

    .line 1448682
    iput v10, v6, LX/4Yt;->D:I

    .line 1448683
    move-object v6, v6

    .line 1448684
    iget v10, v0, LX/99J;->d:I

    .line 1448685
    iput v10, v6, LX/4Yt;->C:I

    .line 1448686
    move-object v6, v6

    .line 1448687
    iget v10, v0, LX/99J;->a:I

    .line 1448688
    iput v10, v6, LX/4Yt;->E:I

    .line 1448689
    move-object v6, v6

    .line 1448690
    iget v0, v0, LX/99J;->b:I

    .line 1448691
    iput v0, v6, LX/4Yt;->F:I

    .line 1448692
    move-object v0, v6

    .line 1448693
    invoke-virtual {v0}, LX/4Yt;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    move-result-object v0

    goto/16 :goto_6

    .line 1448694
    :cond_10
    new-instance v0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    invoke-direct {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;-><init>()V

    goto/16 :goto_6

    :cond_11
    iget-object v0, v1, LX/99K;->b:LX/0Px;

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/99J;

    goto :goto_d

    .line 1448695
    :cond_12
    sget-object v10, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->INSPIRATION_PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto/16 :goto_8

    .line 1448696
    :cond_13
    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object v10

    sget-object v11, LX/4gF;->VIDEO:LX/4gF;

    if-ne v10, v11, :cond_16

    .line 1448697
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v11

    .line 1448698
    sget-object v10, Lcom/facebook/ipc/media/MediaItem;->a:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->i()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_15

    .line 1448699
    sget-object v10, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ANIMATED_IMAGE_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {v11, v10}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1448700
    :cond_14
    :goto_e
    sget-object v10, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {v11, v10}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1448701
    invoke-virtual {v11}, LX/0Pz;->b()LX/0Px;

    move-result-object v10

    goto/16 :goto_9

    .line 1448702
    :cond_15
    iget-object v10, p0, LX/9A6;->e:LX/0Ot;

    invoke-interface {v10}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/0ad;

    sget-short v12, LX/0fe;->I:S

    const/4 v13, 0x0

    invoke-interface {v10, v12, v13}, LX/0ad;->a(SZ)Z

    move-result v10

    if-eqz v10, :cond_14

    .line 1448703
    sget-object v10, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {v11, v10}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_e

    .line 1448704
    :cond_16
    sget-object v10, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v10}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v10

    goto/16 :goto_9
.end method

.method public final b()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1448594
    iget-object v0, p0, LX/9A6;->g:LX/0Px;

    return-object v0
.end method
