.class public abstract LX/9dK;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final c:Ljava/util/concurrent/atomic/AtomicBoolean;


# instance fields
.field public final a:Landroid/content/Context;

.field public b:Landroid/view/ViewGroup;

.field public d:Z

.field public e:F

.field public final f:LX/8Gc;

.field public g:Landroid/widget/ImageView;

.field public h:Landroid/widget/TextView;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public i:LX/9cz;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1518072
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v0, LX/9dK;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/8Gc;)V
    .locals 0

    .prologue
    .line 1518068
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1518069
    iput-object p1, p0, LX/9dK;->a:Landroid/content/Context;

    .line 1518070
    iput-object p2, p0, LX/9dK;->f:LX/8Gc;

    .line 1518071
    return-void
.end method

.method public static a(LX/9dK;FF)Landroid/animation/Animator;
    .locals 2

    .prologue
    .line 1518065
    const/4 v0, 0x2

    new-array v0, v0, [F

    const/4 v1, 0x0

    aput p1, v0, v1

    const/4 v1, 0x1

    aput p2, v0, v1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 1518066
    new-instance v1, LX/9dQ;

    invoke-direct {v1, p0}, LX/9dQ;-><init>(LX/9dK;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1518067
    return-object v0
.end method

.method public static b(LX/9dK;F)F
    .locals 2

    .prologue
    .line 1518064
    invoke-virtual {p0, p1}, LX/9dK;->a(F)F

    move-result v0

    iget v1, p0, LX/9dK;->e:F

    add-float/2addr v0, v1

    return v0
.end method

.method public static p(LX/9dK;)Landroid/animation/Animator;
    .locals 4

    .prologue
    .line 1518058
    const-wide/16 v0, 0xf

    invoke-virtual {p0}, LX/9dK;->e()LX/9dT;

    move-result-object v2

    invoke-virtual {v2}, LX/9dT;->getMultiplier()I

    move-result v2

    int-to-long v2, v2

    mul-long/2addr v0, v2

    long-to-float v0, v0

    .line 1518059
    const/4 v1, 0x2

    new-array v1, v1, [F

    const/4 v2, 0x0

    invoke-virtual {p0}, LX/9dK;->c()F

    move-result v3

    aput v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, LX/9dK;->d()F

    move-result v3

    aput v3, v1, v2

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    invoke-virtual {p0}, LX/9dK;->i()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v1

    .line 1518060
    new-instance v2, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v3, 0x40000000    # 2.0f

    invoke-direct {v2, v3}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1518061
    new-instance v2, LX/9dR;

    invoke-direct {v2, p0, v0}, LX/9dR;-><init>(LX/9dK;F)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1518062
    new-instance v0, LX/9dS;

    invoke-direct {v0, p0}, LX/9dS;-><init>(LX/9dK;)V

    invoke-virtual {v1, v0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1518063
    return-object v1
.end method


# virtual methods
.method public abstract a(F)F
.end method

.method public abstract a()Landroid/widget/ImageView;
.end method

.method public abstract b()Landroid/widget/TextView;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract c()F
.end method

.method public abstract d()F
.end method

.method public abstract e()LX/9dT;
.end method

.method public abstract f()F
.end method

.method public abstract g()F
.end method

.method public abstract h()Z
.end method

.method public i()J
    .locals 2

    .prologue
    .line 1518057
    const-wide/16 v0, 0x640

    return-wide v0
.end method

.method public j()J
    .locals 2

    .prologue
    .line 1518056
    const-wide/16 v0, 0x0

    return-wide v0
.end method
