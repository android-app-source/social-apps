.class public LX/9fn;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/0TD;

.field public final c:LX/8GT;

.field public final d:LX/1HI;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Lcom/facebook/bitmaps/NativeImageProcessor;

.field public final g:LX/0kL;

.field public h:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/0TD;LX/8GT;LX/1HI;LX/0Ot;Lcom/facebook/bitmaps/NativeImageProcessor;LX/0kL;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0TD;",
            "LX/8GT;",
            "LX/1HI;",
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;",
            "Lcom/facebook/bitmaps/NativeImageProcessor;",
            "LX/0kL;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1522648
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1522649
    iput-object p1, p0, LX/9fn;->a:Ljava/lang/String;

    .line 1522650
    iput-object p2, p0, LX/9fn;->b:LX/0TD;

    .line 1522651
    iput-object p3, p0, LX/9fn;->c:LX/8GT;

    .line 1522652
    iput-object p4, p0, LX/9fn;->d:LX/1HI;

    .line 1522653
    iput-object p5, p0, LX/9fn;->e:LX/0Ot;

    .line 1522654
    iput-object p6, p0, LX/9fn;->f:Lcom/facebook/bitmaps/NativeImageProcessor;

    .line 1522655
    iput-object p7, p0, LX/9fn;->g:LX/0kL;

    .line 1522656
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;Landroid/net/Uri;LX/434;Landroid/graphics/RectF;LX/9fc;)V
    .locals 10

    .prologue
    .line 1522657
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1522658
    iget-object v0, p0, LX/9fn;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    const-string v1, "crop_task"

    .line 1522659
    new-instance v4, LX/9fm;

    move-object v5, p0

    move-object v6, p1

    move-object v7, p2

    move-object v8, p4

    move-object v9, p3

    invoke-direct/range {v4 .. v9}, LX/9fm;-><init>(LX/9fn;Lcom/facebook/photos/creativeediting/model/CreativeEditingData;Landroid/net/Uri;Landroid/graphics/RectF;LX/434;)V

    move-object v2, v4

    .line 1522660
    new-instance v3, LX/9fl;

    invoke-direct {v3, p0, p1, p4, p5}, LX/9fl;-><init>(LX/9fn;Lcom/facebook/photos/creativeediting/model/CreativeEditingData;Landroid/graphics/RectF;LX/9fc;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1522661
    return-void
.end method
