.class public LX/9Ao;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1zx;


# instance fields
.field private final a:LX/1zl;

.field private final b:I

.field private final c:LX/1zx;


# direct methods
.method public constructor <init>(LX/1zl;ILX/1zx;)V
    .locals 0

    .prologue
    .line 1450463
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1450464
    iput-object p1, p0, LX/9Ao;->a:LX/1zl;

    .line 1450465
    iput p2, p0, LX/9Ao;->b:I

    .line 1450466
    iput-object p3, p0, LX/9Ao;->c:LX/1zx;

    .line 1450467
    return-void
.end method


# virtual methods
.method public final a()Landroid/graphics/drawable/Drawable;
    .locals 3

    .prologue
    .line 1450468
    iget-object v0, p0, LX/9Ao;->a:LX/1zl;

    iget v1, p0, LX/9Ao;->b:I

    invoke-virtual {v0, v1}, LX/1zl;->a(I)LX/3K3;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1450469
    iget-object v0, p0, LX/9Ao;->c:LX/1zx;

    invoke-interface {v0}, LX/1zx;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1450470
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/9Uk;

    invoke-direct {v0}, LX/9Uk;-><init>()V

    iget-object v1, p0, LX/9Ao;->a:LX/1zl;

    iget v2, p0, LX/9Ao;->b:I

    invoke-virtual {v1, v2}, LX/1zl;->a(I)LX/3K3;

    move-result-object v1

    .line 1450471
    iput-object v1, v0, LX/9Uk;->a:LX/3K3;

    .line 1450472
    move-object v0, v0

    .line 1450473
    invoke-virtual {v0}, LX/9Uk;->a()LX/9Ug;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 1450474
    iget-object v0, p0, LX/9Ao;->c:LX/1zx;

    invoke-interface {v0}, LX/1zx;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1450475
    iget-object v0, p0, LX/9Ao;->c:LX/1zx;

    invoke-interface {v0}, LX/1zx;->c()Z

    move-result v0

    return v0
.end method
