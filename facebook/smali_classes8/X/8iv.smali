.class public LX/8iv;
.super LX/8iN;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1392042
    invoke-direct {p0}, LX/8iN;-><init>()V

    .line 1392043
    return-void
.end method


# virtual methods
.method public final c(LX/8ic;)LX/8id;
    .locals 13

    .prologue
    const v12, 0x3d75c28f    # 0.06f

    const v11, 0x3d3c6a7f    # 0.046f

    const v10, 0x3eb33333    # 0.35f

    const v9, 0x3f5c28f6    # 0.86f

    const v8, 0x3da3d70a    # 0.08f

    .line 1392044
    sget-object v0, LX/8iu;->a:[I

    invoke-virtual {p1}, LX/8ic;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1392045
    new-instance v0, LX/8id;

    const v1, 0x3f4ccccd    # 0.8f

    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v2

    const v3, 0x7f070011

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3e051eb8    # 0.13f

    const-string v6, "comment"

    const v7, 0x7f070011

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070038

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const/high16 v5, 0x3f800000    # 1.0f

    const-string v6, "like_comment"

    const v7, 0x7f070038

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070039

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3f7d70a4    # 0.99f

    const-string v6, "like_main"

    const v7, 0x7f070039

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070055

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3e5a1cac    # 0.213f

    const-string v6, "post_comment"

    const v7, 0x7f070055

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070056

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3f29374c    # 0.661f

    const-string v6, "post_main"

    const v7, 0x7f070056

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07007d

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3e0b4396    # 0.136f

    const-string v6, "share"

    const v7, 0x7f07007d

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070059

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3e54fdf4    # 0.208f

    const-string v6, "pull_to_refresh_fast"

    const v7, 0x7f070059

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07005a

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3e54fdf4    # 0.208f

    const-string v6, "pull_to_refresh_medium"

    const v7, 0x7f07005a

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07005b

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3e54fdf4    # 0.208f

    const-string v6, "pull_to_refresh_slow"

    const v7, 0x7f07005b

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07000e

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3e4ccccd    # 0.2f

    const-string v6, "collapse_after_refresh"

    const v7, 0x7f07000e

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07003a

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3e99999a    # 0.3f

    const-string v6, "live_comment"

    const v7, 0x7f07003a

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07008f

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3dcccccd    # 0.1f

    const-string v6, "typing_indicator"

    const v7, 0x7f07008f

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07005f

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_cancel"

    const v6, 0x7f07005f

    invoke-direct {v4, v12, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070060

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_appear"

    const v6, 0x7f070060

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070061

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_away"

    const v6, 0x7f070061

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070062

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_select_1"

    const v6, 0x7f070062

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070063

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_select_2"

    const v6, 0x7f070063

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070064

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_select_3"

    const v6, 0x7f070064

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070065

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_select_4"

    const v6, 0x7f070065

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070066

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_select_5"

    const v6, 0x7f070066

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070067

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_select_6"

    const v6, 0x7f070067

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070068

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_like_down"

    const v6, 0x7f070068

    invoke-direct {v4, v9, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070069

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_like_up"

    const v6, 0x7f070069

    invoke-direct {v4, v9, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07006a

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_ui_choose_love_down"

    const v6, 0x7f07006a

    invoke-direct {v4, v9, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07005d

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "qp_alert_notify_1"

    const v6, 0x7f07005d

    invoke-direct {v4, v10, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07005e

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3e2e147b    # 0.17f

    const-string v6, "qp_alert_notify_4"

    const v7, 0x7f07005e

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/8id;-><init>(FLjava/util/Map;)V

    :goto_0
    return-object v0

    .line 1392046
    :pswitch_0
    new-instance v0, LX/8id;

    const v1, 0x3f4ccccd    # 0.8f

    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v2

    const v3, 0x7f070011

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const/high16 v5, 0x3e800000    # 0.25f

    const-string v6, "comment"

    const v7, 0x7f070011

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070038

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3f2ac083    # 0.667f

    const-string v6, "like_comment"

    const v7, 0x7f070038

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070039

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3f266666    # 0.65f

    const-string v6, "like_main"

    const v7, 0x7f070039

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070055

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "post_comment"

    const v6, 0x7f070055

    invoke-direct {v4, v10, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070056

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const/high16 v5, 0x3f600000    # 0.875f

    const-string v6, "post_main"

    const v7, 0x7f070056

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07007d

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3e6c8b44    # 0.231f

    const-string v6, "share"

    const v7, 0x7f07007d

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070059

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3e54fdf4    # 0.208f

    const-string v6, "pull_to_refresh_fast"

    const v7, 0x7f070059

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07005a

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3e54fdf4    # 0.208f

    const-string v6, "pull_to_refresh_medium"

    const v7, 0x7f07005a

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07005b

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3e54fdf4    # 0.208f

    const-string v6, "pull_to_refresh_slow"

    const v7, 0x7f07005b

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07000e

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3e4ccccd    # 0.2f

    const-string v6, "collapse_after_refresh"

    const v7, 0x7f07000e

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07005f

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_cancel"

    const v6, 0x7f07005f

    invoke-direct {v4, v12, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070060

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_appear"

    const v6, 0x7f070060

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070061

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_away"

    const v6, 0x7f070061

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070062

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_select_1"

    const v6, 0x7f070062

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070063

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_select_2"

    const v6, 0x7f070063

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070064

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_select_3"

    const v6, 0x7f070064

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070065

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_select_4"

    const v6, 0x7f070065

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070066

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_select_5"

    const v6, 0x7f070066

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070067

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_select_6"

    const v6, 0x7f070067

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070068

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_like_down"

    const v6, 0x7f070068

    invoke-direct {v4, v9, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070069

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_like_up"

    const v6, 0x7f070069

    invoke-direct {v4, v9, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07006a

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_ui_choose_love_down"

    const v6, 0x7f07006a

    invoke-direct {v4, v9, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07005d

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "qp_alert_notify_1"

    const v6, 0x7f07005d

    invoke-direct {v4, v10, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07005e

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3e2e147b    # 0.17f

    const-string v6, "qp_alert_notify_4"

    const v7, 0x7f07005e

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/8id;-><init>(FLjava/util/Map;)V

    goto/16 :goto_0

    .line 1392047
    :pswitch_1
    new-instance v0, LX/8id;

    const v1, 0x3f4ccccd    # 0.8f

    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v2

    const v3, 0x7f070011

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3e8dd2f2    # 0.277f

    const-string v6, "comment"

    const v7, 0x7f070011

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070038

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3edc28f6    # 0.43f

    const-string v6, "like_comment"

    const v7, 0x7f070038

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070039

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3ef2b021    # 0.474f

    const-string v6, "like_main"

    const v7, 0x7f070039

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070055

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3e83126f    # 0.256f

    const-string v6, "post_comment"

    const v7, 0x7f070055

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070056

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3f051eb8    # 0.52f

    const-string v6, "post_main"

    const v7, 0x7f070056

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07007d

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3d9374bc    # 0.072f

    const-string v6, "share"

    const v7, 0x7f07007d

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070059

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "pull_to_refresh_fast"

    const v6, 0x7f070059

    invoke-direct {v4, v11, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07005a

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "pull_to_refresh_medium"

    const v6, 0x7f07005a

    invoke-direct {v4, v11, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07005b

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "pull_to_refresh_slow"

    const v6, 0x7f07005b

    invoke-direct {v4, v11, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07000e

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3d4ccccd    # 0.05f

    const-string v6, "collapse_after_refresh"

    const v7, 0x7f07000e

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07005f

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_cancel"

    const v6, 0x7f07005f

    invoke-direct {v4, v12, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070060

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_appear"

    const v6, 0x7f070060

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070061

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_away"

    const v6, 0x7f070061

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070062

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_select_1"

    const v6, 0x7f070062

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070063

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_select_2"

    const v6, 0x7f070063

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070064

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_select_3"

    const v6, 0x7f070064

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070065

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_select_4"

    const v6, 0x7f070065

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070066

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_select_5"

    const v6, 0x7f070066

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070067

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_select_6"

    const v6, 0x7f070067

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070068

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_like_down"

    const v6, 0x7f070068

    invoke-direct {v4, v9, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070069

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_like_up"

    const v6, 0x7f070069

    invoke-direct {v4, v9, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07006a

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_ui_choose_love_down"

    const v6, 0x7f07006a

    invoke-direct {v4, v9, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07005d

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "qp_alert_notify_1"

    const v6, 0x7f07005d

    invoke-direct {v4, v10, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07005e

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3e2e147b    # 0.17f

    const-string v6, "qp_alert_notify_4"

    const v7, 0x7f07005e

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/8id;-><init>(FLjava/util/Map;)V

    goto/16 :goto_0

    .line 1392048
    :pswitch_2
    new-instance v0, LX/8id;

    const v1, 0x3f4ccccd    # 0.8f

    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v2

    const v3, 0x7f070011

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3e0f5c29    # 0.14f

    const-string v6, "comment"

    const v7, 0x7f070011

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070038

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3f3f7cee    # 0.748f

    const-string v6, "like_comment"

    const v7, 0x7f070038

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070039

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3ef5c28f    # 0.48f

    const-string v6, "like_main"

    const v7, 0x7f070039

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070055

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3e841893    # 0.258f

    const-string v6, "post_comment"

    const v7, 0x7f070055

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070056

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3f2147ae    # 0.63f

    const-string v6, "post_main"

    const v7, 0x7f070056

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07007d

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3de76c8b    # 0.113f

    const-string v6, "share"

    const v7, 0x7f07007d

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070059

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3d656042    # 0.056f

    const-string v6, "pull_to_refresh_fast"

    const v7, 0x7f070059

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07005a

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3d656042    # 0.056f

    const-string v6, "pull_to_refresh_medium"

    const v7, 0x7f07005a

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07005b

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3d656042    # 0.056f

    const-string v6, "pull_to_refresh_slow"

    const v7, 0x7f07005b

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07000e

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3d4ccccd    # 0.05f

    const-string v6, "collapse_after_refresh"

    const v7, 0x7f07000e

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07005f

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_cancel"

    const v6, 0x7f07005f

    invoke-direct {v4, v12, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070060

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_appear"

    const v6, 0x7f070060

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070061

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_away"

    const v6, 0x7f070061

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070062

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_select_1"

    const v6, 0x7f070062

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070063

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_select_2"

    const v6, 0x7f070063

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070064

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_select_3"

    const v6, 0x7f070064

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070065

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_select_4"

    const v6, 0x7f070065

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070066

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_select_5"

    const v6, 0x7f070066

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070067

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_select_6"

    const v6, 0x7f070067

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070068

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_like_down"

    const v6, 0x7f070068

    invoke-direct {v4, v9, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070069

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_like_up"

    const v6, 0x7f070069

    invoke-direct {v4, v9, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07006a

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_ui_choose_love_down"

    const v6, 0x7f07006a

    invoke-direct {v4, v9, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07005d

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "qp_alert_notify_1"

    const v6, 0x7f07005d

    invoke-direct {v4, v10, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07005e

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3e2e147b    # 0.17f

    const-string v6, "qp_alert_notify_4"

    const v7, 0x7f07005e

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/8id;-><init>(FLjava/util/Map;)V

    goto/16 :goto_0

    .line 1392049
    :pswitch_3
    new-instance v0, LX/8id;

    const v1, 0x3f4ccccd    # 0.8f

    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v2

    const v3, 0x7f070011

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3e676c8b    # 0.226f

    const-string v6, "comment"

    const v7, 0x7f070011

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070038

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3f43d70a    # 0.765f

    const-string v6, "like_comment"

    const v7, 0x7f070038

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070039

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3f266666    # 0.65f

    const-string v6, "like_main"

    const v7, 0x7f070039

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070055

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3e851eb8    # 0.26f

    const-string v6, "post_comment"

    const v7, 0x7f070055

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070056

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3f5e76c9    # 0.869f

    const-string v6, "post_main"

    const v7, 0x7f070056

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07007d

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3e1ba5e3    # 0.152f

    const-string v6, "share"

    const v7, 0x7f07007d

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070059

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3d343958    # 0.044f

    const-string v6, "pull_to_refresh_fast"

    const v7, 0x7f070059

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07005a

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3d343958    # 0.044f

    const-string v6, "pull_to_refresh_medium"

    const v7, 0x7f07005a

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07005b

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3d343958    # 0.044f

    const-string v6, "pull_to_refresh_slow"

    const v7, 0x7f07005b

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07000e

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3e051eb8    # 0.13f

    const-string v6, "collapse_after_refresh"

    const v7, 0x7f07000e

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07005f

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_cancel"

    const v6, 0x7f07005f

    invoke-direct {v4, v12, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070060

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_appear"

    const v6, 0x7f070060

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070061

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_away"

    const v6, 0x7f070061

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070062

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_select_1"

    const v6, 0x7f070062

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070063

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_select_2"

    const v6, 0x7f070063

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070064

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_select_3"

    const v6, 0x7f070064

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070065

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_select_4"

    const v6, 0x7f070065

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070066

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_select_5"

    const v6, 0x7f070066

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070067

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_select_6"

    const v6, 0x7f070067

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070068

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_like_down"

    const v6, 0x7f070068

    invoke-direct {v4, v9, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070069

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_like_up"

    const v6, 0x7f070069

    invoke-direct {v4, v9, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07006a

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_ui_choose_love_down"

    const v6, 0x7f07006a

    invoke-direct {v4, v9, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07005d

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "qp_alert_notify_1"

    const v6, 0x7f07005d

    invoke-direct {v4, v10, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07005e

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3e2e147b    # 0.17f

    const-string v6, "qp_alert_notify_4"

    const v7, 0x7f07005e

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/8id;-><init>(FLjava/util/Map;)V

    goto/16 :goto_0

    .line 1392050
    :pswitch_4
    new-instance v0, LX/8id;

    const v1, 0x3f4ccccd    # 0.8f

    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v2

    const v3, 0x7f070011

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3e6b851f    # 0.23f

    const-string v6, "comment"

    const v7, 0x7f070011

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070038

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3f3a1cac    # 0.727f

    const-string v6, "like_comment"

    const v7, 0x7f070038

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070039

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const/high16 v5, 0x3f400000    # 0.75f

    const-string v6, "like_main"

    const v7, 0x7f070039

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070055

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3e851eb8    # 0.26f

    const-string v6, "post_comment"

    const v7, 0x7f070055

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070056

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3f3be76d    # 0.734f

    const-string v6, "post_main"

    const v7, 0x7f070056

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07007d

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3e25e354    # 0.162f

    const-string v6, "share"

    const v7, 0x7f07007d

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070059

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "pull_to_refresh_fast"

    const v6, 0x7f070059

    invoke-direct {v4, v11, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07005a

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "pull_to_refresh_medium"

    const v6, 0x7f07005a

    invoke-direct {v4, v11, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07005b

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "pull_to_refresh_slow"

    const v6, 0x7f07005b

    invoke-direct {v4, v11, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07000e

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3d7df3b6    # 0.062f

    const-string v6, "collapse_after_refresh"

    const v7, 0x7f07000e

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07005f

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_cancel"

    const v6, 0x7f07005f

    invoke-direct {v4, v12, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070060

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_appear"

    const v6, 0x7f070060

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070061

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_away"

    const v6, 0x7f070061

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070062

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_select_1"

    const v6, 0x7f070062

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070063

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_select_2"

    const v6, 0x7f070063

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070064

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_select_3"

    const v6, 0x7f070064

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070065

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_select_4"

    const v6, 0x7f070065

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070066

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_select_5"

    const v6, 0x7f070066

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070067

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_dock_select_6"

    const v6, 0x7f070067

    invoke-direct {v4, v8, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070068

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_like_down"

    const v6, 0x7f070068

    invoke-direct {v4, v9, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f070069

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_like_up"

    const v6, 0x7f070069

    invoke-direct {v4, v9, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07006a

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "reactions_ui_choose_love_down"

    const v6, 0x7f07006a

    invoke-direct {v4, v9, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07005d

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const-string v5, "qp_alert_notify_1"

    const v6, 0x7f07005d

    invoke-direct {v4, v10, v5, v6}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const v3, 0x7f07005e

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, LX/8iO;

    const v5, 0x3e2e147b    # 0.17f

    const-string v6, "qp_alert_notify_4"

    const v7, 0x7f07005e

    invoke-direct {v4, v5, v6, v7}, LX/8iO;-><init>(FLjava/lang/String;I)V

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/8id;-><init>(FLjava/util/Map;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
