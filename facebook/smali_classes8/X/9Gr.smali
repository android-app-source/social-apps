.class public final LX/9Gr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/9FG;

.field public final synthetic b:LX/9FA;

.field public final synthetic c:LX/9Gt;

.field public final synthetic d:Lcom/facebook/feedback/ui/rows/ThreadedCommentListGroupPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/rows/ThreadedCommentListGroupPartDefinition;LX/9FG;LX/9FA;LX/9Gt;)V
    .locals 0

    .prologue
    .line 1460351
    iput-object p1, p0, LX/9Gr;->d:Lcom/facebook/feedback/ui/rows/ThreadedCommentListGroupPartDefinition;

    iput-object p2, p0, LX/9Gr;->a:LX/9FG;

    iput-object p3, p0, LX/9Gr;->b:LX/9FA;

    iput-object p4, p0, LX/9Gr;->c:LX/9Gt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x2f663a5b

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1460342
    iget-object v1, p0, LX/9Gr;->a:LX/9FG;

    .line 1460343
    iget-object v6, v1, LX/9FG;->a:LX/0if;

    sget-object v7, LX/0ig;->j:LX/0ih;

    iget-wide v8, v1, LX/9FG;->b:J

    const-string v10, "view_more_replies_clicked"

    invoke-virtual {v6, v7, v8, v9, v10}, LX/0if;->b(LX/0ih;JLjava/lang/String;)V

    .line 1460344
    iget-object v1, p0, LX/9Gr;->d:Lcom/facebook/feedback/ui/rows/ThreadedCommentListGroupPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedback/ui/rows/ThreadedCommentListGroupPartDefinition;->d:LX/3iR;

    iget-object v2, p0, LX/9Gr;->b:LX/9FA;

    .line 1460345
    iget-object v3, v2, LX/9FA;->k:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v2, v3

    .line 1460346
    const-string v3, "comment_view_more_replies_clicked"

    invoke-static {v1, v3, v2}, LX/3iR;->b(LX/3iR;Ljava/lang/String;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)LX/0oG;

    move-result-object v3

    .line 1460347
    invoke-virtual {v3}, LX/0oG;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    if-eqz v2, :cond_0

    .line 1460348
    invoke-virtual {v3}, LX/0oG;->d()V

    .line 1460349
    :cond_0
    iget-object v1, p0, LX/9Gr;->b:LX/9FA;

    iget-object v2, p0, LX/9Gr;->c:LX/9Gt;

    iget-object v2, v2, LX/9Gt;->a:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v3, p0, LX/9Gr;->c:LX/9Gt;

    iget-object v3, v3, LX/9Gt;->a:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v4, p0, LX/9Gr;->c:LX/9Gt;

    iget-object v4, v4, LX/9Gt;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v1, v2, v3, v4}, LX/9FA;->a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1460350
    const v1, 0x6e76e9b5

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
