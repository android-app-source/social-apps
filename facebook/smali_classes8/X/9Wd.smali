.class public final enum LX/9Wd;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9Wd;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9Wd;

.field public static final enum COMMENTS:LX/9Wd;

.field public static final enum MESSAGE_SENDER_CONTEXT:LX/9Wd;

.field public static final enum MESSAGE_THREAD:LX/9Wd;

.field public static final enum REACTORS:LX/9Wd;


# instance fields
.field private mEventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1501439
    new-instance v0, LX/9Wd;

    const-string v1, "COMMENTS"

    const-string v2, "comments"

    invoke-direct {v0, v1, v3, v2}, LX/9Wd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Wd;->COMMENTS:LX/9Wd;

    .line 1501440
    new-instance v0, LX/9Wd;

    const-string v1, "REACTORS"

    const-string v2, "reactors"

    invoke-direct {v0, v1, v4, v2}, LX/9Wd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Wd;->REACTORS:LX/9Wd;

    .line 1501441
    new-instance v0, LX/9Wd;

    const-string v1, "MESSAGE_THREAD"

    const-string v2, "message_thread"

    invoke-direct {v0, v1, v5, v2}, LX/9Wd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Wd;->MESSAGE_THREAD:LX/9Wd;

    .line 1501442
    new-instance v0, LX/9Wd;

    const-string v1, "MESSAGE_SENDER_CONTEXT"

    const-string v2, "message_sender_context"

    invoke-direct {v0, v1, v6, v2}, LX/9Wd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9Wd;->MESSAGE_SENDER_CONTEXT:LX/9Wd;

    .line 1501443
    const/4 v0, 0x4

    new-array v0, v0, [LX/9Wd;

    sget-object v1, LX/9Wd;->COMMENTS:LX/9Wd;

    aput-object v1, v0, v3

    sget-object v1, LX/9Wd;->REACTORS:LX/9Wd;

    aput-object v1, v0, v4

    sget-object v1, LX/9Wd;->MESSAGE_THREAD:LX/9Wd;

    aput-object v1, v0, v5

    sget-object v1, LX/9Wd;->MESSAGE_SENDER_CONTEXT:LX/9Wd;

    aput-object v1, v0, v6

    sput-object v0, LX/9Wd;->$VALUES:[LX/9Wd;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1501444
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1501445
    iput-object p3, p0, LX/9Wd;->mEventName:Ljava/lang/String;

    .line 1501446
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9Wd;
    .locals 1

    .prologue
    .line 1501447
    const-class v0, LX/9Wd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9Wd;

    return-object v0
.end method

.method public static values()[LX/9Wd;
    .locals 1

    .prologue
    .line 1501448
    sget-object v0, LX/9Wd;->$VALUES:[LX/9Wd;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9Wd;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1501449
    iget-object v0, p0, LX/9Wd;->mEventName:Ljava/lang/String;

    return-object v0
.end method
