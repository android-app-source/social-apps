.class public final LX/9Kb;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 1468530
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 1468531
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1468532
    :goto_0
    return v1

    .line 1468533
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1468534
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_4

    .line 1468535
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1468536
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1468537
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1468538
    const-string v4, "attachment_properties"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1468539
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1468540
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_ARRAY:LX/15z;

    if-ne v3, v4, :cond_2

    .line 1468541
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_ARRAY:LX/15z;

    if-eq v3, v4, :cond_2

    .line 1468542
    const/4 v4, 0x0

    .line 1468543
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v5, :cond_9

    .line 1468544
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1468545
    :goto_3
    move v3, v4

    .line 1468546
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1468547
    :cond_2
    invoke-static {v2, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 1468548
    goto :goto_1

    .line 1468549
    :cond_3
    const-string v4, "target"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1468550
    invoke-static {p0, p1}, LX/9Ka;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1468551
    :cond_4
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1468552
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1468553
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1468554
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 1468555
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1468556
    :cond_7
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_8

    .line 1468557
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1468558
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1468559
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_7

    if-eqz v5, :cond_7

    .line 1468560
    const-string v6, "value"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1468561
    const/4 v5, 0x0

    .line 1468562
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v6, :cond_d

    .line 1468563
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1468564
    :goto_5
    move v3, v5

    .line 1468565
    goto :goto_4

    .line 1468566
    :cond_8
    const/4 v5, 0x1

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1468567
    invoke-virtual {p1, v4, v3}, LX/186;->b(II)V

    .line 1468568
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto :goto_3

    :cond_9
    move v3, v4

    goto :goto_4

    .line 1468569
    :cond_a
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1468570
    :cond_b
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_c

    .line 1468571
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1468572
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1468573
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_b

    if-eqz v6, :cond_b

    .line 1468574
    const-string v7, "text"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1468575
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_6

    .line 1468576
    :cond_c
    const/4 v6, 0x1

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1468577
    invoke-virtual {p1, v5, v3}, LX/186;->b(II)V

    .line 1468578
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto :goto_5

    :cond_d
    move v3, v5

    goto :goto_6
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 1468579
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1468580
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1468581
    if-eqz v0, :cond_3

    .line 1468582
    const-string v1, "attachment_properties"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1468583
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1468584
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 1468585
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    .line 1468586
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1468587
    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1468588
    if-eqz v3, :cond_1

    .line 1468589
    const-string v4, "value"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1468590
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1468591
    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1468592
    if-eqz v4, :cond_0

    .line 1468593
    const-string v2, "text"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1468594
    invoke-virtual {p2, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1468595
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1468596
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1468597
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1468598
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1468599
    :cond_3
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1468600
    if-eqz v0, :cond_4

    .line 1468601
    const-string v1, "target"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1468602
    invoke-static {p0, v0, p2}, LX/9Ka;->a(LX/15i;ILX/0nX;)V

    .line 1468603
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1468604
    return-void
.end method
