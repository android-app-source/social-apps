.class public LX/93m;
.super LX/93Q;
.source ""


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(LX/93q;LX/03V;LX/1Ck;Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;)V
    .locals 0
    .param p1    # LX/93q;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1434182
    invoke-direct {p0, p1, p2, p3}, LX/93Q;-><init>(LX/93q;LX/03V;LX/1Ck;)V

    .line 1434183
    iput-object p4, p0, LX/93m;->a:Ljava/lang/String;

    .line 1434184
    iput-object p5, p0, LX/93m;->b:Ljava/lang/String;

    .line 1434185
    iput-object p6, p0, LX/93m;->c:Landroid/content/res/Resources;

    .line 1434186
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1434187
    invoke-super {p0}, LX/93Q;->a()V

    .line 1434188
    new-instance v0, LX/7lN;

    invoke-direct {v0}, LX/7lN;-><init>()V

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->EVERYONE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    .line 1434189
    iput-object v1, v0, LX/7lN;->a:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    .line 1434190
    move-object v0, v0

    .line 1434191
    iget-object v1, p0, LX/93m;->c:Landroid/content/res/Resources;

    const v2, 0x7f08131d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1434192
    iput-object v1, v0, LX/7lN;->b:Ljava/lang/String;

    .line 1434193
    move-object v0, v0

    .line 1434194
    iget-object v1, p0, LX/93m;->c:Landroid/content/res/Resources;

    const v2, 0x7f081327

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1434195
    iput-object v1, v0, LX/7lN;->c:Ljava/lang/String;

    .line 1434196
    move-object v0, v0

    .line 1434197
    const-string v1, "{\"value\":\"EVERYONE\"}"

    .line 1434198
    iput-object v1, v0, LX/7lN;->d:Ljava/lang/String;

    .line 1434199
    move-object v0, v0

    .line 1434200
    invoke-virtual {v0}, LX/7lN;->a()Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    move-result-object v0

    .line 1434201
    new-instance v1, LX/7lP;

    invoke-direct {v1}, LX/7lP;-><init>()V

    const/4 v2, 0x1

    .line 1434202
    iput-boolean v2, v1, LX/7lP;->a:Z

    .line 1434203
    move-object v1, v1

    .line 1434204
    sget-object v2, Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;->b:Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;

    .line 1434205
    iput-object v2, v1, LX/7lP;->d:Lcom/facebook/ipc/composer/model/OptimisticPostPrivacy;

    .line 1434206
    move-object v1, v1

    .line 1434207
    invoke-virtual {v1, v0}, LX/7lP;->a(Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;)LX/7lP;

    move-result-object v0

    invoke-virtual {v0}, LX/7lP;->a()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    .line 1434208
    invoke-virtual {p0, v0}, LX/93Q;->a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    .line 1434209
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1434210
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "pageadmin:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/93m;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
