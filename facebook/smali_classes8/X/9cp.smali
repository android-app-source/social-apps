.class public LX/9cp;
.super LX/0gG;
.source ""


# instance fields
.field private final a:Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9cP;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lcom/facebook/stickers/search/StickerSearchContainer;

.field public d:LX/4m4;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;LX/4m4;LX/0Ot;)V
    .locals 0
    .param p1    # Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/4m4;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;",
            "LX/4m4;",
            "LX/0Ot",
            "<",
            "LX/9cP;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1516789
    invoke-direct {p0}, LX/0gG;-><init>()V

    .line 1516790
    iput-object p1, p0, LX/9cp;->a:Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;

    .line 1516791
    iput-object p2, p0, LX/9cp;->d:LX/4m4;

    .line 1516792
    iput-object p3, p0, LX/9cp;->b:LX/0Ot;

    .line 1516793
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v0, -0x1

    .line 1516794
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v0, v0}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 1516795
    iget-object v0, p0, LX/9cp;->a:Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;

    invoke-virtual {v0, p2}, Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 1516796
    sget-object v2, Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;->a:Ljava/lang/Object;

    if-ne v0, v2, :cond_1

    .line 1516797
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1516798
    iget-object v2, p0, LX/9cp;->c:Lcom/facebook/stickers/search/StickerSearchContainer;

    if-nez v2, :cond_0

    .line 1516799
    const v2, 0x7f0e0644

    const v3, 0x7f0e0644

    invoke-static {v0, v2, v3}, LX/0WH;->a(Landroid/content/Context;II)Landroid/content/Context;

    move-result-object v2

    .line 1516800
    new-instance v3, Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-object p2, p0, LX/9cp;->d:LX/4m4;

    invoke-direct {v3, v2, p2}, Lcom/facebook/stickers/search/StickerSearchContainer;-><init>(Landroid/content/Context;LX/4m4;)V

    iput-object v3, p0, LX/9cp;->c:Lcom/facebook/stickers/search/StickerSearchContainer;

    .line 1516801
    iget-object v2, p0, LX/9cp;->c:Lcom/facebook/stickers/search/StickerSearchContainer;

    new-instance v3, LX/9co;

    invoke-direct {v3, p0, v0}, LX/9co;-><init>(LX/9cp;Landroid/content/Context;)V

    invoke-virtual {v2, v3}, Lcom/facebook/stickers/search/StickerSearchContainer;->setStickerSearchListener(LX/8l6;)V

    .line 1516802
    :cond_0
    iget-object v2, p0, LX/9cp;->c:Lcom/facebook/stickers/search/StickerSearchContainer;

    move-object v0, v2

    .line 1516803
    :goto_0
    invoke-virtual {p1, v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1516804
    return-object v0

    .line 1516805
    :cond_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    check-cast v0, Lcom/facebook/stickers/model/StickerPack;

    .line 1516806
    new-instance v3, LX/9ck;

    invoke-direct {v3, v2}, LX/9ck;-><init>(Landroid/content/Context;)V

    .line 1516807
    iget-object p2, p0, LX/9cp;->d:LX/4m4;

    .line 1516808
    iget-object p0, v3, LX/9ck;->d:LX/8mq;

    invoke-virtual {p0, v3, p2}, LX/8mq;->a(Landroid/widget/GridView;LX/4m4;)LX/8mp;

    move-result-object p0

    iput-object p0, v3, LX/9ck;->e:LX/8mp;

    .line 1516809
    iput-object v0, v3, LX/9ck;->g:Lcom/facebook/stickers/model/StickerPack;

    .line 1516810
    const/4 p2, 0x0

    invoke-virtual {v3, p2}, LX/9ck;->setClipToPadding(Z)V

    .line 1516811
    move-object v0, v3

    .line 1516812
    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 1516813
    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1516814
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1516815
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1516816
    iget-object v0, p0, LX/9cp;->a:Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/stickers/stickers/StickersTrayAdapter;->getCount()I

    move-result v0

    return v0
.end method
