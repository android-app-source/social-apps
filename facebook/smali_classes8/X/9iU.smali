.class public LX/9iU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile g:LX/9iU;


# instance fields
.field private final b:LX/0Sh;

.field private final c:LX/1Er;

.field private final d:LX/03V;

.field private final e:Landroid/content/Context;

.field private final f:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1527920
    const-class v0, LX/9iU;

    sput-object v0, LX/9iU;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Uh;LX/0Sh;LX/1Er;LX/03V;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1527913
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1527914
    iput-object p3, p0, LX/9iU;->b:LX/0Sh;

    .line 1527915
    iput-object p4, p0, LX/9iU;->c:LX/1Er;

    .line 1527916
    iput-object p5, p0, LX/9iU;->d:LX/03V;

    .line 1527917
    iput-object p1, p0, LX/9iU;->e:Landroid/content/Context;

    .line 1527918
    iput-object p2, p0, LX/9iU;->f:LX/0Uh;

    .line 1527919
    return-void
.end method

.method public static a(LX/0QB;)LX/9iU;
    .locals 9

    .prologue
    .line 1527900
    sget-object v0, LX/9iU;->g:LX/9iU;

    if-nez v0, :cond_1

    .line 1527901
    const-class v1, LX/9iU;

    monitor-enter v1

    .line 1527902
    :try_start_0
    sget-object v0, LX/9iU;->g:LX/9iU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1527903
    if-eqz v2, :cond_0

    .line 1527904
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1527905
    new-instance v3, LX/9iU;

    const-class v4, Landroid/content/Context;

    const-class v5, Lcom/facebook/inject/ForAppContext;

    invoke-interface {v0, v4, v5}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v6

    check-cast v6, LX/0Sh;

    invoke-static {v0}, LX/1Er;->a(LX/0QB;)LX/1Er;

    move-result-object v7

    check-cast v7, LX/1Er;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-direct/range {v3 .. v8}, LX/9iU;-><init>(Landroid/content/Context;LX/0Uh;LX/0Sh;LX/1Er;LX/03V;)V

    .line 1527906
    move-object v0, v3

    .line 1527907
    sput-object v0, LX/9iU;->g:LX/9iU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1527908
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1527909
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1527910
    :cond_1
    sget-object v0, LX/9iU;->g:LX/9iU;

    return-object v0

    .line 1527911
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1527912
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Z)Ljava/io/File;
    .locals 6

    .prologue
    .line 1527893
    iget-object v1, p0, LX/9iU;->c:LX/1Er;

    const-string v2, ".facebook_"

    const-string v3, ".jpg"

    if-eqz p1, :cond_0

    sget-object v0, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;

    move-result-object v0

    .line 1527894
    if-nez v0, :cond_2

    .line 1527895
    iget-object v1, p0, LX/9iU;->d:LX/03V;

    sget-object v0, LX/9iU;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Failed to create temp file: %s %s %s"

    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    const-string v5, ".facebook_"

    aput-object v5, v4, v0

    const/4 v0, 0x1

    const-string v5, ".jpg"

    aput-object v5, v4, v0

    const/4 v5, 0x2

    if-eqz p1, :cond_1

    sget-object v0, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    invoke-virtual {v0}, LX/46h;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    aput-object v0, v4, v5

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1527896
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unable to create temporary file"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1527897
    :cond_0
    sget-object v0, LX/46h;->PREFER_SDCARD:LX/46h;

    goto :goto_0

    .line 1527898
    :cond_1
    sget-object v0, LX/46h;->PREFER_SDCARD:LX/46h;

    invoke-virtual {v0}, LX/46h;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1527899
    :cond_2
    return-object v0
.end method

.method private static b(Ljava/io/InputStream;Ljava/io/File;)V
    .locals 2

    .prologue
    .line 1527885
    const/4 v1, 0x0

    .line 1527886
    const/4 v0, 0x0

    :try_start_0
    new-array v0, v0, [LX/3AS;

    invoke-static {p1, v0}, LX/1t3;->a(Ljava/io/File;[LX/3AS;)LX/3AU;

    move-result-object v0

    invoke-virtual {v0}, LX/3AU;->a()Ljava/io/OutputStream;

    move-result-object v1

    .line 1527887
    invoke-static {p0, v1}, LX/0hW;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1527888
    if-eqz v1, :cond_0

    .line 1527889
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 1527890
    :cond_0
    return-void

    .line 1527891
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    .line 1527892
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    :cond_1
    throw v0
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;Ljava/io/File;)Landroid/net/Uri;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1527859
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1527860
    iget-object v0, p0, LX/9iU;->f:LX/0Uh;

    const/16 v2, 0x268

    invoke-virtual {v0, v2, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 1527861
    if-nez p2, :cond_0

    .line 1527862
    invoke-direct {p0, v0}, LX/9iU;->a(Z)Ljava/io/File;

    move-result-object p2

    .line 1527863
    :cond_0
    const/4 v2, 0x0

    .line 1527864
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1527865
    :try_start_1
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x50

    invoke-virtual {p1, v2, v3, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1527866
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 1527867
    :goto_1
    if-eqz v0, :cond_3

    .line 1527868
    iget-object v0, p0, LX/9iU;->e:Landroid/content/Context;

    invoke-static {v0, p2}, Lcom/facebook/content/FbFileProvider;->a(Landroid/content/Context;Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 1527869
    :goto_2
    return-object v0

    :cond_1
    move v0, v1

    .line 1527870
    goto :goto_0

    .line 1527871
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_3
    if-eqz v1, :cond_2

    .line 1527872
    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 1527873
    :cond_2
    :goto_4
    throw v0

    .line 1527874
    :cond_3
    invoke-static {p2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_2

    .line 1527875
    :catch_0
    goto :goto_1

    :catch_1
    goto :goto_4

    .line 1527876
    :catchall_1
    move-exception v0

    goto :goto_3
.end method

.method public final a(Ljava/io/InputStream;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1527884
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/9iU;->a(Ljava/io/InputStream;Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/io/InputStream;Ljava/io/File;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 1527877
    iget-object v0, p0, LX/9iU;->f:LX/0Uh;

    const/16 v1, 0x268

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 1527878
    if-nez p2, :cond_0

    .line 1527879
    invoke-direct {p0, v0}, LX/9iU;->a(Z)Ljava/io/File;

    move-result-object p2

    .line 1527880
    :cond_0
    invoke-static {p1, p2}, LX/9iU;->b(Ljava/io/InputStream;Ljava/io/File;)V

    .line 1527881
    if-eqz v0, :cond_1

    .line 1527882
    iget-object v0, p0, LX/9iU;->e:Landroid/content/Context;

    invoke-static {v0, p2}, Lcom/facebook/content/FbFileProvider;->a(Landroid/content/Context;Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 1527883
    :goto_0
    return-object v0

    :cond_1
    invoke-static {p2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method
