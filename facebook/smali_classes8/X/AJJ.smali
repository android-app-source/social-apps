.class public final enum LX/AJJ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/AJJ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/AJJ;

.field public static final enum DID_CHANGE_FEED_PRIVACY:LX/AJJ;

.field public static final enum DID_SHARE_DIRECT:LX/AJJ;

.field public static final enum DID_SHARE_TO_FEED:LX/AJJ;

.field public static final enum FRIENDS_FBIDS:LX/AJJ;

.field public static final enum NUMBER_OF_FRIENDS:LX/AJJ;

.field public static final enum PROMPT_ID:LX/AJJ;

.field public static final enum SELECTED_FEED_PRIVACY:LX/AJJ;

.field public static final enum SESSION_ID:LX/AJJ;


# instance fields
.field private final mName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1660993
    new-instance v0, LX/AJJ;

    const-string v1, "DID_SHARE_TO_FEED"

    const-string v2, "did_share_to_feed"

    invoke-direct {v0, v1, v4, v2}, LX/AJJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AJJ;->DID_SHARE_TO_FEED:LX/AJJ;

    .line 1660994
    new-instance v0, LX/AJJ;

    const-string v1, "DID_SHARE_DIRECT"

    const-string v2, "did_share_direct"

    invoke-direct {v0, v1, v5, v2}, LX/AJJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AJJ;->DID_SHARE_DIRECT:LX/AJJ;

    .line 1660995
    new-instance v0, LX/AJJ;

    const-string v1, "DID_CHANGE_FEED_PRIVACY"

    const-string v2, "did_change_feed_privacy"

    invoke-direct {v0, v1, v6, v2}, LX/AJJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AJJ;->DID_CHANGE_FEED_PRIVACY:LX/AJJ;

    .line 1660996
    new-instance v0, LX/AJJ;

    const-string v1, "NUMBER_OF_FRIENDS"

    const-string v2, "number_of_friends"

    invoke-direct {v0, v1, v7, v2}, LX/AJJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AJJ;->NUMBER_OF_FRIENDS:LX/AJJ;

    .line 1660997
    new-instance v0, LX/AJJ;

    const-string v1, "SELECTED_FEED_PRIVACY"

    const-string v2, "selected_feed_privacy"

    invoke-direct {v0, v1, v8, v2}, LX/AJJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AJJ;->SELECTED_FEED_PRIVACY:LX/AJJ;

    .line 1660998
    new-instance v0, LX/AJJ;

    const-string v1, "FRIENDS_FBIDS"

    const/4 v2, 0x5

    const-string v3, "friend_fbids"

    invoke-direct {v0, v1, v2, v3}, LX/AJJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AJJ;->FRIENDS_FBIDS:LX/AJJ;

    .line 1660999
    new-instance v0, LX/AJJ;

    const-string v1, "SESSION_ID"

    const/4 v2, 0x6

    const-string v3, "sharesheet_session_id"

    invoke-direct {v0, v1, v2, v3}, LX/AJJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AJJ;->SESSION_ID:LX/AJJ;

    .line 1661000
    new-instance v0, LX/AJJ;

    const-string v1, "PROMPT_ID"

    const/4 v2, 0x7

    const-string v3, "prompt_id"

    invoke-direct {v0, v1, v2, v3}, LX/AJJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AJJ;->PROMPT_ID:LX/AJJ;

    .line 1661001
    const/16 v0, 0x8

    new-array v0, v0, [LX/AJJ;

    sget-object v1, LX/AJJ;->DID_SHARE_TO_FEED:LX/AJJ;

    aput-object v1, v0, v4

    sget-object v1, LX/AJJ;->DID_SHARE_DIRECT:LX/AJJ;

    aput-object v1, v0, v5

    sget-object v1, LX/AJJ;->DID_CHANGE_FEED_PRIVACY:LX/AJJ;

    aput-object v1, v0, v6

    sget-object v1, LX/AJJ;->NUMBER_OF_FRIENDS:LX/AJJ;

    aput-object v1, v0, v7

    sget-object v1, LX/AJJ;->SELECTED_FEED_PRIVACY:LX/AJJ;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/AJJ;->FRIENDS_FBIDS:LX/AJJ;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/AJJ;->SESSION_ID:LX/AJJ;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/AJJ;->PROMPT_ID:LX/AJJ;

    aput-object v2, v0, v1

    sput-object v0, LX/AJJ;->$VALUES:[LX/AJJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1660990
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1660991
    iput-object p3, p0, LX/AJJ;->mName:Ljava/lang/String;

    .line 1660992
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/AJJ;
    .locals 1

    .prologue
    .line 1661002
    const-class v0, LX/AJJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/AJJ;

    return-object v0
.end method

.method public static values()[LX/AJJ;
    .locals 1

    .prologue
    .line 1660989
    sget-object v0, LX/AJJ;->$VALUES:[LX/AJJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/AJJ;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1660988
    iget-object v0, p0, LX/AJJ;->mName:Ljava/lang/String;

    return-object v0
.end method
