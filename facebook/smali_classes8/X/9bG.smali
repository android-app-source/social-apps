.class public final LX/9bG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/9bJ;


# direct methods
.method public constructor <init>(LX/9bJ;)V
    .locals 0

    .prologue
    .line 1514621
    iput-object p1, p0, LX/9bG;->a:LX/9bJ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v0, 0x2

    const v1, -0x438c3302

    invoke-static {v0, v6, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1514622
    iget-object v0, p0, LX/9bG;->a:LX/9bJ;

    iget-boolean v0, v0, LX/9bJ;->y:Z

    if-eqz v0, :cond_0

    .line 1514623
    iget-object v0, p0, LX/9bG;->a:LX/9bJ;

    iget-object v0, v0, LX/9bJ;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9XE;

    iget-object v1, p0, LX/9bG;->a:LX/9bJ;

    iget-object v1, v1, LX/9bJ;->x:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    const-string v1, "pandora_albums_grid"

    invoke-virtual {v0, v4, v5, v1}, LX/9XE;->f(JLjava/lang/String;)V

    .line 1514624
    :cond_0
    iget-object v0, p0, LX/9bG;->a:LX/9bJ;

    iget-object v0, v0, LX/9bJ;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9at;

    sget-object v3, LX/9au;->ALBUMSTAB:LX/9au;

    iget-object v1, p0, LX/9bG;->a:LX/9bJ;

    iget-object v1, v1, LX/9bJ;->t:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1514625
    iget-boolean v4, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v1, v4

    .line 1514626
    if-eqz v1, :cond_1

    iget-object v1, p0, LX/9bG;->a:LX/9bJ;

    iget-object v1, v1, LX/9bJ;->t:Lcom/facebook/auth/viewercontext/ViewerContext;

    :goto_0
    invoke-virtual {v0, v3, v1}, LX/9at;->a(LX/9au;Lcom/facebook/auth/viewercontext/ViewerContext;)Landroid/content/Intent;

    move-result-object v3

    .line 1514627
    iget-object v0, p0, LX/9bG;->a:LX/9bJ;

    invoke-virtual {v0}, LX/9bJ;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1514628
    iget-object v1, p0, LX/9bG;->a:LX/9bJ;

    iget-object v1, v1, LX/9bJ;->q:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v3, v6, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1514629
    const v0, 0x17a6793c

    invoke-static {v0, v2}, LX/02F;->a(II)V

    return-void

    .line 1514630
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
