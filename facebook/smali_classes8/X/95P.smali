.class public final LX/95P;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<TTResponseModel;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/3DR;

.field public final synthetic b:Ljava/lang/Object;

.field public final synthetic c:LX/2kW;


# direct methods
.method public constructor <init>(LX/2kW;LX/3DR;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1436812
    iput-object p1, p0, LX/95P;->c:LX/2kW;

    iput-object p2, p0, LX/95P;->a:LX/3DR;

    iput-object p3, p0, LX/95P;->b:Ljava/lang/Object;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCancel(Ljava/util/concurrent/CancellationException;)V
    .locals 3

    .prologue
    .line 1436813
    iget-object v0, p0, LX/95P;->c:LX/2kW;

    iget-object v0, v0, LX/2kW;->a:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/controller/connectioncontroller/ConnectionControllerImpl$2$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/controller/connectioncontroller/ConnectionControllerImpl$2$1;-><init>(LX/95P;Ljava/util/concurrent/CancellationException;)V

    const v2, -0x7eb4e98f

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1436814
    return-void
.end method

.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1436815
    iget-object v0, p0, LX/95P;->c:LX/2kW;

    iget-object v0, v0, LX/2kW;->a:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/controller/connectioncontroller/ConnectionControllerImpl$2$2;

    invoke-direct {v1, p0, p1}, Lcom/facebook/controller/connectioncontroller/ConnectionControllerImpl$2$2;-><init>(LX/95P;Ljava/lang/Throwable;)V

    const v2, 0x43baf24d

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1436816
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 11

    .prologue
    .line 1436817
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1436818
    iget-wide v8, p1, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v4, v8

    .line 1436819
    iget-object v0, p0, LX/95P;->c:LX/2kW;

    iget-object v0, v0, LX/2kW;->k:LX/1s3;

    if-eqz v0, :cond_0

    .line 1436820
    iget-object v0, p0, LX/95P;->c:LX/2kW;

    iget-object v0, v0, LX/2kW;->k:LX/1s3;

    iget-object v1, p0, LX/95P;->c:LX/2kW;

    .line 1436821
    iget-object v2, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v2, v2

    .line 1436822
    if-eqz v2, :cond_0

    .line 1436823
    iget-object v2, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v2, v2

    .line 1436824
    instance-of v2, v2, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel;

    if-nez v2, :cond_2

    .line 1436825
    :cond_0
    :goto_0
    :try_start_0
    iget-object v0, p0, LX/95P;->c:LX/2kW;

    iget-object v0, v0, LX/2kW;->e:LX/1rs;

    invoke-interface {v0, p1}, LX/1rs;->a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/5Mb;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 1436826
    :try_start_1
    iget-object v0, p0, LX/95P;->c:LX/2kW;

    iget-object v0, v0, LX/2kW;->h:LX/2jy;

    iget-object v1, p0, LX/95P;->a:LX/3DR;

    .line 1436827
    iget-object v2, v1, LX/3DR;->a:LX/2nj;

    move-object v1, v2

    .line 1436828
    iget-object v2, p0, LX/95P;->a:LX/3DR;

    .line 1436829
    iget-object v6, v2, LX/3DR;->b:LX/3DP;

    move-object v2, v6

    .line 1436830
    iget-object v6, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v6, v6

    .line 1436831
    sget-object v7, LX/0ta;->FROM_SERVER:LX/0ta;

    if-ne v6, v7, :cond_1

    sget-object v6, LX/3Cb;->NETWORK:LX/3Cb;

    :goto_1
    invoke-interface/range {v0 .. v6}, LX/2jy;->a(LX/2nj;LX/3DP;LX/5Mb;JLX/3Cb;)V
    :try_end_1
    .catch LX/5Md; {:try_start_1 .. :try_end_1} :catch_1

    .line 1436832
    :goto_2
    return-void

    .line 1436833
    :catch_0
    move-exception v0

    .line 1436834
    new-instance v1, LX/5Me;

    const-string v2, "Error in extracting GraphQL result!"

    invoke-direct {v1, v2, v0}, LX/5Me;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1436835
    iget-object v0, p0, LX/95P;->c:LX/2kW;

    iget-object v0, v0, LX/2kW;->d:LX/03V;

    const-string v2, "ConnectionController"

    invoke-virtual {v0, v2, v1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1436836
    invoke-virtual {p0, v1}, LX/0Vd;->onFailure(Ljava/lang/Throwable;)V

    goto :goto_2

    .line 1436837
    :cond_1
    :try_start_2
    sget-object v6, LX/3Cb;->DISK:LX/3Cb;
    :try_end_2
    .catch LX/5Md; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 1436838
    :catch_1
    move-exception v0

    .line 1436839
    invoke-virtual {p0, v0}, LX/95P;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    goto :goto_2

    .line 1436840
    :cond_2
    iget-object v2, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v2, v2

    .line 1436841
    check-cast v2, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel;

    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel$NotificationStoriesModel;

    move-result-object v2

    .line 1436842
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel$NotificationStoriesModel;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaConnectionFieldsModel;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1436843
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 1436844
    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel$NotificationStoriesModel;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaConnectionFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaConnectionFieldsModel;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    .line 1436845
    const/4 v2, 0x0

    move v3, v2

    :goto_3
    if-ge v3, v8, :cond_5

    invoke-virtual {v7, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaFieldsModel;

    .line 1436846
    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaFieldsModel;->e()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 1436847
    new-instance v9, LX/BE4;

    invoke-direct {v9, v0, v2}, LX/BE4;-><init>(LX/1s3;Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaFieldsModel;)V

    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaFieldsModel;->e()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1, v9, v10}, LX/2kW;->a(LX/0Rl;Ljava/lang/String;)V

    .line 1436848
    :cond_3
    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaFieldsModel;->j()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;

    move-result-object v9

    if-eqz v9, :cond_4

    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaFieldsModel;->j()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->hZ_()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_4

    .line 1436849
    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaFieldsModel;->j()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->hZ_()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaFieldsModel;->j()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;

    move-result-object v2

    invoke-direct {v10, v2}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;-><init>(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;)V

    invoke-interface {v6, v9, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1436850
    :cond_4
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_3

    .line 1436851
    :cond_5
    invoke-interface {v6}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1436852
    iget-object v2, v0, LX/1s3;->a:LX/1dy;

    new-instance v3, LX/BE5;

    invoke-direct {v3, v6}, LX/BE5;-><init>(Ljava/util/Map;)V

    invoke-virtual {v2, v3}, LX/1dy;->a(LX/4VT;)V

    goto/16 :goto_0
.end method
