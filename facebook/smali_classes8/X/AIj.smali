.class public final LX/AIj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Efx;

.field public final synthetic b:LX/AIk;


# direct methods
.method public constructor <init>(LX/AIk;LX/Efx;)V
    .locals 0

    .prologue
    .line 1660252
    iput-object p1, p0, LX/AIj;->b:LX/AIk;

    iput-object p2, p0, LX/AIj;->a:LX/Efx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1660253
    sget-object v0, LX/AIk;->a:Ljava/lang/String;

    const-string v1, "error while trying to send reaction"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1660254
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1660255
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1660256
    if-eqz p1, :cond_0

    .line 1660257
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1660258
    check-cast v0, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel;

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel;->a()Lcom/facebook/audience/protocol/DirectMutationFragmentsModels$FBSnacksReactionCreateMutationModel$ThreadModel;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1660259
    :cond_0
    return-void
.end method
