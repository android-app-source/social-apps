.class public final LX/9fl;
.super LX/0Vd;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

.field public final synthetic b:Landroid/graphics/RectF;

.field public final synthetic c:LX/9fc;

.field public final synthetic d:LX/9fn;


# direct methods
.method public constructor <init>(LX/9fn;Lcom/facebook/photos/creativeediting/model/CreativeEditingData;Landroid/graphics/RectF;LX/9fc;)V
    .locals 0

    .prologue
    .line 1522618
    iput-object p1, p0, LX/9fl;->d:LX/9fn;

    iput-object p2, p0, LX/9fl;->a:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    iput-object p3, p0, LX/9fl;->b:Landroid/graphics/RectF;

    iput-object p4, p0, LX/9fl;->c:LX/9fc;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1522619
    iget-object v0, p0, LX/9fl;->c:LX/9fc;

    iget-object v1, p0, LX/9fl;->a:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-interface {v0, p1, v1}, LX/9fc;->a(Ljava/lang/Throwable;Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)V

    .line 1522620
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1522621
    iget-object v0, p0, LX/9fl;->a:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-static {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v1

    iget-object v0, p0, LX/9fl;->d:LX/9fn;

    iget-object v0, v0, LX/9fn;->h:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9fl;->d:LX/9fn;

    iget-object v0, v0, LX/9fn;->h:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setEditedUri(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v0

    iget-object v1, p0, LX/9fl;->b:Landroid/graphics/RectF;

    invoke-static {v1}, LX/63w;->a(Landroid/graphics/RectF;)Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setCropBox(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    .line 1522622
    iget-object v1, p0, LX/9fl;->c:LX/9fc;

    invoke-interface {v1, v0}, LX/9fc;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)V

    .line 1522623
    return-void

    .line 1522624
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
