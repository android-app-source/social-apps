.class public abstract LX/9Bj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9Bi;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:LX/0hG;

.field private final c:Landroid/content/Context;

.field private final d:LX/1nL;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/0hG;LX/1nL;)V
    .locals 1

    .prologue
    .line 1452462
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, LX/9Bj;-><init>(Ljava/lang/String;LX/0hG;Landroid/content/Context;LX/1nL;)V

    .line 1452463
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;LX/0hG;Landroid/content/Context;LX/1nL;)V
    .locals 0

    .prologue
    .line 1452456
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1452457
    iput-object p1, p0, LX/9Bj;->a:Ljava/lang/String;

    .line 1452458
    iput-object p2, p0, LX/9Bj;->b:LX/0hG;

    .line 1452459
    iput-object p3, p0, LX/9Bj;->c:Landroid/content/Context;

    .line 1452460
    iput-object p4, p0, LX/9Bj;->d:LX/1nL;

    .line 1452461
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;LX/1nL;)V
    .locals 1

    .prologue
    .line 1452422
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2, p3}, LX/9Bj;-><init>(Ljava/lang/String;LX/0hG;Landroid/content/Context;LX/1nL;)V

    .line 1452423
    return-void
.end method

.method private a(LX/8qC;)V
    .locals 2

    .prologue
    .line 1452452
    iget-object v0, p0, LX/9Bj;->b:LX/0hG;

    if-eqz v0, :cond_0

    .line 1452453
    iget-object v0, p0, LX/9Bj;->b:LX/0hG;

    invoke-interface {v0, p1}, LX/0hG;->a(LX/8qC;)V

    .line 1452454
    :goto_0
    return-void

    .line 1452455
    :cond_0
    iget-object v0, p0, LX/9Bj;->d:LX/1nL;

    iget-object v1, p0, LX/9Bj;->c:Landroid/content/Context;

    invoke-virtual {v0, p1, v1}, LX/1nL;->a(LX/8qC;Landroid/content/Context;)V

    goto :goto_0
.end method

.method private a()Z
    .locals 1

    .prologue
    .line 1452451
    iget-object v0, p0, LX/9Bj;->b:LX/0hG;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 5

    .prologue
    .line 1452439
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Cannot show edit history for comment without an ID"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1452440
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/9Bj;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, LX/9Bj;->a()Z

    move-result v2

    .line 1452441
    new-instance v3, Lcom/facebook/ufiservices/flyout/history/CommentEditHistoryFragment;

    invoke-direct {v3}, Lcom/facebook/ufiservices/flyout/history/CommentEditHistoryFragment;-><init>()V

    .line 1452442
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 1452443
    const-string p1, "node_id"

    invoke-virtual {v4, p1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1452444
    const-string p1, "module"

    invoke-virtual {v4, p1, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1452445
    const-string p1, "standalone"

    invoke-virtual {v4, p1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1452446
    invoke-virtual {v3, v4}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1452447
    move-object v0, v3

    .line 1452448
    invoke-direct {p0, v0}, LX/9Bj;->a(LX/8qC;)V

    .line 1452449
    return-void

    .line 1452450
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 6

    .prologue
    .line 1452424
    new-instance v0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;

    invoke-direct {v0}, Lcom/facebook/ufiservices/flyout/EditCommentFragment;-><init>()V

    .line 1452425
    new-instance v1, LX/8qX;

    invoke-direct {v1}, LX/8qX;-><init>()V

    iget-object v2, p0, LX/9Bj;->a:Ljava/lang/String;

    .line 1452426
    iget-object v3, v1, LX/8qX;->a:Landroid/os/Bundle;

    const-string v4, "moduleName"

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1452427
    move-object v1, v1

    .line 1452428
    iget-object v2, v1, LX/8qX;->a:Landroid/os/Bundle;

    const-string v3, "feedback"

    invoke-static {v2, v3, p2}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1452429
    move-object v1, v1

    .line 1452430
    iget-object v2, v1, LX/8qX;->a:Landroid/os/Bundle;

    const-string v3, "comment"

    invoke-static {v2, v3, p1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1452431
    move-object v1, v1

    .line 1452432
    invoke-direct {p0}, LX/9Bj;->a()Z

    move-result v2

    .line 1452433
    iget-object v3, v1, LX/8qX;->a:Landroid/os/Bundle;

    const-string v4, "standalone"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1452434
    move-object v1, v1

    .line 1452435
    iget-object v2, v1, LX/8qX;->a:Landroid/os/Bundle;

    move-object v1, v2

    .line 1452436
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1452437
    invoke-direct {p0, v0}, LX/9Bj;->a(LX/8qC;)V

    .line 1452438
    return-void
.end method
