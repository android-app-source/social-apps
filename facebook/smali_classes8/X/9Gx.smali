.class public abstract LX/9Gx;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/3Wr;
.implements LX/3Ws;


# instance fields
.field public a:LX/9HC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:LX/9HB;

.field private final c:LX/9HK;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1460541
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1460542
    const-class v0, LX/9Gx;

    invoke-static {v0, p0}, LX/9Gx;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1460543
    iget-object v0, p0, LX/9Gx;->a:LX/9HC;

    invoke-virtual {v0, p0}, LX/9HC;->a(Landroid/view/View;)LX/9HB;

    move-result-object v0

    iput-object v0, p0, LX/9Gx;->b:LX/9HB;

    .line 1460544
    iget-object v0, p0, LX/9Gx;->b:LX/9HB;

    invoke-virtual {v0}, LX/9HB;->a()V

    .line 1460545
    new-instance v0, LX/9HK;

    invoke-direct {v0, p1}, LX/9HK;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/9Gx;->c:LX/9HK;

    .line 1460546
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/9Gx;

    const-class p0, LX/9HC;

    invoke-interface {v1, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/9HC;

    iput-object v1, p1, LX/9Gx;->a:LX/9HC;

    return-void
.end method


# virtual methods
.method public final a(LX/9CP;)V
    .locals 1

    .prologue
    .line 1460564
    iget-object v0, p0, LX/9Gx;->c:LX/9HK;

    invoke-virtual {v0, p1}, LX/9HK;->a(LX/9CP;)V

    .line 1460565
    return-void
.end method

.method public final a(Landroid/animation/ValueAnimator;)V
    .locals 1

    .prologue
    .line 1460562
    iget-object v0, p0, LX/9Gx;->b:LX/9HB;

    invoke-virtual {v0, p1}, LX/9HB;->a(Landroid/animation/ValueAnimator;)V

    .line 1460563
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1460560
    iget-object v0, p0, LX/9Gx;->c:LX/9HK;

    invoke-virtual {v0}, LX/9HK;->a()V

    .line 1460561
    return-void
.end method

.method public final dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1460559
    iget-object v0, p0, LX/9Gx;->c:LX/9HK;

    invoke-virtual {v0, p1}, LX/9HK;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1460556
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->draw(Landroid/graphics/Canvas;)V

    .line 1460557
    iget-object v0, p0, LX/9Gx;->c:LX/9HK;

    invoke-virtual {v0, p1}, LX/9HK;->a(Landroid/graphics/Canvas;)V

    .line 1460558
    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1460555
    iget-object v0, p0, LX/9Gx;->b:LX/9HB;

    invoke-virtual {v0, p1}, LX/9HB;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1460551
    iput v0, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1460552
    iput v0, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1460553
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1460554
    return-void
.end method

.method public setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 1

    .prologue
    .line 1460547
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1460548
    iget-object v0, p0, LX/9Gx;->b:LX/9HB;

    .line 1460549
    iput-object p1, v0, LX/9HB;->c:Landroid/view/View$OnTouchListener;

    .line 1460550
    return-void
.end method
