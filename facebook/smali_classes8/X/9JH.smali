.class public LX/9JH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/95b;


# instance fields
.field private a:[I

.field private b:[LX/0w5;

.field private c:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private e:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private f:Landroid/util/SparseIntArray;

.field private g:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0x20

    .line 1464520
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1464521
    const/4 v0, 0x0

    iput v0, p0, LX/9JH;->g:I

    .line 1464522
    new-array v0, v1, [I

    iput-object v0, p0, LX/9JH;->a:[I

    .line 1464523
    new-array v0, v1, [LX/0w5;

    iput-object v0, p0, LX/9JH;->b:[LX/0w5;

    .line 1464524
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 1464515
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1464516
    const/4 v0, 0x0

    iput v0, p0, LX/9JH;->g:I

    .line 1464517
    new-array v0, p1, [I

    iput-object v0, p0, LX/9JH;->a:[I

    .line 1464518
    new-array v0, p1, [LX/0w5;

    iput-object v0, p0, LX/9JH;->b:[LX/0w5;

    .line 1464519
    return-void
.end method

.method public static a(Lcom/facebook/flatbuffers/MutableFlattenable;)Lcom/facebook/flatbuffers/MutableFlattenable;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/flatbuffers/MutableFlattenable;",
            ">(TT;)TT;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1464509
    invoke-interface {p0}, Lcom/facebook/flatbuffers/MutableFlattenable;->q_()LX/15i;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1464510
    :goto_0
    return-object p0

    .line 1464511
    :cond_0
    invoke-static {p0}, LX/186;->b(Lcom/facebook/flatbuffers/Flattenable;)[B

    move-result-object v1

    .line 1464512
    new-instance v0, LX/15i;

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1464513
    const-string v1, "BufferRowArrayList"

    invoke-virtual {v0, v1}, LX/15i;->a(Ljava/lang/String;)V

    .line 1464514
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/15i;->a(Ljava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/MutableFlattenable;

    move-object p0, v0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1464508
    iget v0, p0, LX/9JH;->g:I

    return v0
.end method

.method public final a(I)I
    .locals 1

    .prologue
    .line 1464505
    iget v0, p0, LX/9JH;->g:I

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1464506
    iget-object v0, p0, LX/9JH;->a:[I

    aget v0, v0, p1

    return v0

    .line 1464507
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(ILX/0w5;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/0w5",
            "<+",
            "Lcom/facebook/flatbuffers/MutableFlattenable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1464495
    if-lez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1464496
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1464497
    iget-object v0, p0, LX/9JH;->a:[I

    array-length v0, v0

    iget v1, p0, LX/9JH;->g:I

    if-gt v0, v1, :cond_0

    .line 1464498
    iget-object v0, p0, LX/9JH;->a:[I

    iget-object v1, p0, LX/9JH;->a:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    iput-object v0, p0, LX/9JH;->a:[I

    .line 1464499
    iget-object v0, p0, LX/9JH;->b:[LX/0w5;

    iget-object v1, p0, LX/9JH;->b:[LX/0w5;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0w5;

    iput-object v0, p0, LX/9JH;->b:[LX/0w5;

    .line 1464500
    :cond_0
    iget-object v0, p0, LX/9JH;->a:[I

    iget v1, p0, LX/9JH;->g:I

    aput p1, v0, v1

    .line 1464501
    iget-object v0, p0, LX/9JH;->b:[LX/0w5;

    iget v1, p0, LX/9JH;->g:I

    aput-object p2, v0, v1

    .line 1464502
    iget v0, p0, LX/9JH;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/9JH;->g:I

    .line 1464503
    return-void

    .line 1464504
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(ILX/0w5;Ljava/util/Collection;I)V
    .locals 2
    .param p3    # Ljava/util/Collection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/0w5",
            "<+",
            "Lcom/facebook/flatbuffers/MutableFlattenable;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 1464485
    if-eqz p4, :cond_1

    .line 1464486
    iget-object v0, p0, LX/9JH;->f:Landroid/util/SparseIntArray;

    if-nez v0, :cond_0

    .line 1464487
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, LX/9JH;->f:Landroid/util/SparseIntArray;

    .line 1464488
    :cond_0
    iget-object v0, p0, LX/9JH;->f:Landroid/util/SparseIntArray;

    iget v1, p0, LX/9JH;->g:I

    invoke-virtual {v0, v1, p4}, Landroid/util/SparseIntArray;->append(II)V

    .line 1464489
    :cond_1
    if-eqz p3, :cond_3

    invoke-interface {p3}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1464490
    iget-object v0, p0, LX/9JH;->d:Landroid/util/SparseArray;

    if-nez v0, :cond_2

    .line 1464491
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/9JH;->d:Landroid/util/SparseArray;

    .line 1464492
    :cond_2
    iget-object v0, p0, LX/9JH;->d:Landroid/util/SparseArray;

    iget v1, p0, LX/9JH;->g:I

    invoke-virtual {v0, v1, p3}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 1464493
    :cond_3
    invoke-virtual {p0, p1, p2}, LX/9JH;->a(ILX/0w5;)V

    .line 1464494
    return-void
.end method

.method public final b(I)LX/0w5;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/0w5",
            "<+",
            "Lcom/facebook/flatbuffers/MutableFlattenable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1464469
    iget v0, p0, LX/9JH;->g:I

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1464470
    iget-object v0, p0, LX/9JH;->b:[LX/0w5;

    aget-object v0, v0, p1

    return-object v0

    .line 1464471
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1464484
    iget v0, p0, LX/9JH;->g:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1464472
    iget-object v0, p0, LX/9JH;->c:Landroid/util/SparseArray;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1464473
    iget-object v0, p0, LX/9JH;->c:Landroid/util/SparseArray;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0

    .line 1464474
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(I)Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1464475
    iget-object v1, p0, LX/9JH;->d:Landroid/util/SparseArray;

    if-nez v1, :cond_0

    .line 1464476
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/9JH;->d:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    goto :goto_0
.end method

.method public final e(I)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1464477
    iget-object v1, p0, LX/9JH;->e:Landroid/util/SparseArray;

    if-nez v1, :cond_0

    .line 1464478
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/9JH;->e:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public final f(I)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1464479
    iget-object v1, p0, LX/9JH;->f:Landroid/util/SparseIntArray;

    if-nez v1, :cond_0

    .line 1464480
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, LX/9JH;->f:Landroid/util/SparseIntArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    goto :goto_0
.end method

.method public final g(I)I
    .locals 1

    .prologue
    .line 1464481
    iget v0, p0, LX/9JH;->g:I

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1464482
    iget-object v0, p0, LX/9JH;->a:[I

    aget v0, v0, p1

    return v0

    .line 1464483
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
