.class public LX/96B;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/96B;


# instance fields
.field public final a:LX/0Zb;

.field private final b:LX/31g;


# direct methods
.method public constructor <init>(LX/0Zb;LX/31g;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1438268
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1438269
    iput-object p1, p0, LX/96B;->a:LX/0Zb;

    .line 1438270
    iput-object p2, p0, LX/96B;->b:LX/31g;

    .line 1438271
    return-void
.end method

.method public static a(LX/0QB;)LX/96B;
    .locals 5

    .prologue
    .line 1438272
    sget-object v0, LX/96B;->c:LX/96B;

    if-nez v0, :cond_1

    .line 1438273
    const-class v1, LX/96B;

    monitor-enter v1

    .line 1438274
    :try_start_0
    sget-object v0, LX/96B;->c:LX/96B;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1438275
    if-eqz v2, :cond_0

    .line 1438276
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1438277
    new-instance p0, LX/96B;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/31g;->a(LX/0QB;)LX/31g;

    move-result-object v4

    check-cast v4, LX/31g;

    invoke-direct {p0, v3, v4}, LX/96B;-><init>(LX/0Zb;LX/31g;)V

    .line 1438278
    move-object v0, p0

    .line 1438279
    sput-object v0, LX/96B;->c:LX/96B;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1438280
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1438281
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1438282
    :cond_1
    sget-object v0, LX/96B;->c:LX/96B;

    return-object v0

    .line 1438283
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1438284
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/96B;Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 3

    .prologue
    .line 1438285
    iget-object v0, p1, Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;->a:Ljava/lang/String;

    invoke-static {p0, v0, p2}, LX/96B;->a(LX/96B;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "endpoint"

    iget-object v2, p1, Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/96B;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 4

    .prologue
    .line 1438286
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "place_creation_session"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "crowdsourcing_create"

    .line 1438287
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1438288
    move-object v0, v0

    .line 1438289
    const-string v1, "event"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "entry_point"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "session_id"

    iget-object v2, p0, LX/96B;->b:LX/31g;

    invoke-virtual {v2}, LX/31g;->a()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;LX/969;)V
    .locals 4

    .prologue
    .line 1438290
    iget-object v0, p0, LX/96B;->a:LX/0Zb;

    const-string v1, "field_edited"

    invoke-static {p0, p1, v1}, LX/96B;->a(LX/96B;Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "field_type_name"

    iget-object v3, p2, LX/969;->name:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1438291
    return-void
.end method

.method public final a(Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;LX/96A;J)V
    .locals 5

    .prologue
    .line 1438292
    iget-object v0, p0, LX/96B;->a:LX/0Zb;

    const-string v1, "existing_place_selected"

    invoke-static {p0, p1, v1}, LX/96B;->a(LX/96B;Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "event_obj_id"

    invoke-virtual {v1, v2, p3, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "view_name"

    iget-object v3, p2, LX/96A;->logValue:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1438293
    return-void
.end method

.method public final b(Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;LX/96A;)V
    .locals 4

    .prologue
    .line 1438294
    iget-object v0, p0, LX/96B;->a:LX/0Zb;

    const-string v1, "view_changed"

    invoke-static {p0, p1, v1}, LX/96B;->a(LX/96B;Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "view_name"

    iget-object v3, p2, LX/96A;->logValue:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1438295
    return-void
.end method

.method public final c(Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;J)V
    .locals 4

    .prologue
    .line 1438296
    iget-object v0, p0, LX/96B;->a:LX/0Zb;

    const-string v1, "created_place"

    invoke-static {p0, p1, v1}, LX/96B;->a(LX/96B;Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "event_obj_id"

    invoke-virtual {v1, v2, p2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1438297
    return-void
.end method
