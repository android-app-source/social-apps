.class public LX/9Dg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9Df;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field private final c:Lcom/facebook/content/SecureContextHelper;

.field private final d:Lcom/facebook/base/fragment/FbFragment;

.field private final e:LX/0SI;

.field private final f:LX/03V;

.field private final g:LX/0tH;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1456043
    const-class v0, LX/9Dg;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/9Dg;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/intent/feed/IFeedIntentBuilder;Lcom/facebook/content/SecureContextHelper;LX/0SI;LX/03V;LX/0tH;Lcom/facebook/base/fragment/FbFragment;)V
    .locals 0
    .param p6    # Lcom/facebook/base/fragment/FbFragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1456044
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1456045
    iput-object p1, p0, LX/9Dg;->b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 1456046
    iput-object p2, p0, LX/9Dg;->c:Lcom/facebook/content/SecureContextHelper;

    .line 1456047
    iput-object p6, p0, LX/9Dg;->d:Lcom/facebook/base/fragment/FbFragment;

    .line 1456048
    iput-object p3, p0, LX/9Dg;->e:LX/0SI;

    .line 1456049
    iput-object p4, p0, LX/9Dg;->f:LX/03V;

    .line 1456050
    iput-object p5, p0, LX/9Dg;->g:LX/0tH;

    .line 1456051
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 4

    .prologue
    .line 1456025
    iget-object v0, p0, LX/9Dg;->g:LX/0tH;

    invoke-virtual {v0}, LX/0tH;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1456026
    invoke-static {p1}, LX/9Ap;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/feedback/reactions/ui/ReactorsListFragment;

    move-result-object v1

    .line 1456027
    iget-object v0, p0, LX/9Dg;->d:Lcom/facebook/base/fragment/FbFragment;

    instance-of v0, v0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    if-eqz v0, :cond_1

    .line 1456028
    iget-object v0, p0, LX/9Dg;->d:Lcom/facebook/base/fragment/FbFragment;

    check-cast v0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    .line 1456029
    iget-object v2, v0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->x:LX/0hG;

    if-eqz v2, :cond_0

    .line 1456030
    iget-object v2, v0, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->x:LX/0hG;

    invoke-interface {v2, v1}, LX/0hG;->a(LX/8qC;)V

    .line 1456031
    :cond_0
    :goto_0
    return-void

    .line 1456032
    :cond_1
    iget-object v0, p0, LX/9Dg;->d:Lcom/facebook/base/fragment/FbFragment;

    instance-of v0, v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;

    if-eqz v0, :cond_0

    .line 1456033
    iget-object v0, p0, LX/9Dg;->d:Lcom/facebook/base/fragment/FbFragment;

    check-cast v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;

    .line 1456034
    iget-object v2, v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->H:LX/0hG;

    if-eqz v2, :cond_2

    .line 1456035
    iget-object v2, v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->H:LX/0hG;

    invoke-interface {v2, v1}, LX/0hG;->a(LX/8qC;)V

    .line 1456036
    :cond_2
    goto :goto_0

    .line 1456037
    :cond_3
    iget-object v0, p0, LX/9Dg;->b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    sget-object v1, LX/8s1;->ACTIVITY_RESULT:LX/8s1;

    invoke-static {p1, v0, v1}, LX/9Ap;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/8s1;)Landroid/content/Intent;

    move-result-object v0

    .line 1456038
    iget-object v1, p0, LX/9Dg;->e:LX/0SI;

    invoke-interface {v1}, LX/0SI;->b()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    if-eqz v1, :cond_4

    iget-object v1, p0, LX/9Dg;->e:LX/0SI;

    invoke-interface {v1}, LX/0SI;->b()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    .line 1456039
    iget-boolean v2, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v1, v2

    .line 1456040
    if-eqz v1, :cond_4

    .line 1456041
    const-string v1, "com.facebook.orca.auth.OVERRIDDEN_VIEWER_CONTEXT"

    iget-object v2, p0, LX/9Dg;->e:LX/0SI;

    invoke-interface {v2}, LX/0SI;->b()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1456042
    :cond_4
    iget-object v1, p0, LX/9Dg;->c:Lcom/facebook/content/SecureContextHelper;

    const v2, 0xb256

    iget-object v3, p0, LX/9Dg;->d:Lcom/facebook/base/fragment/FbFragment;

    invoke-interface {v1, v0, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    goto :goto_0
.end method
