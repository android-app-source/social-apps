.class public final LX/9Hc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Wt;


# instance fields
.field public final synthetic a:LX/9Fa;

.field public final synthetic b:LX/1Pq;

.field public final synthetic c:LX/9Hd;


# direct methods
.method public constructor <init>(LX/9Hd;LX/9Fa;LX/1Pq;)V
    .locals 0

    .prologue
    .line 1461557
    iput-object p1, p0, LX/9Hc;->c:LX/9Hd;

    iput-object p2, p0, LX/9Hc;->a:LX/9Fa;

    iput-object p3, p0, LX/9Hc;->b:LX/1Pq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 1461558
    invoke-static {p1}, LX/47i;->a(Ljava/util/Map;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1461559
    invoke-virtual {p0}, LX/9Hc;->hv_()V

    .line 1461560
    :goto_0
    return-void

    .line 1461561
    :cond_0
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1461562
    iget-object v1, p0, LX/9Hc;->c:LX/9Hd;

    iget-object v1, v1, LX/9Hd;->c:LX/1Uf;

    invoke-static {v0}, LX/9JZ;->a(LX/175;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-static {v0}, LX/1eD;->c(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/1eE;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v3, v2}, LX/1Uf;->a(LX/1eE;ZLX/0lF;)Landroid/text/Spannable;

    move-result-object v0

    invoke-static {v0}, LX/1Uf;->a(Landroid/text/Spannable;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 1461563
    iget-object v1, p0, LX/9Hc;->a:LX/9Fa;

    .line 1461564
    iput-boolean v3, v1, LX/9Fa;->b:Z

    .line 1461565
    iget-object v1, p0, LX/9Hc;->a:LX/9Fa;

    const/4 v2, 0x0

    .line 1461566
    iput-boolean v2, v1, LX/9Fa;->c:Z

    .line 1461567
    iget-object v1, p0, LX/9Hc;->a:LX/9Fa;

    .line 1461568
    iput-object v0, v1, LX/9Fa;->d:Ljava/lang/CharSequence;

    .line 1461569
    iget-object v0, p0, LX/9Hc;->b:LX/1Pq;

    invoke-interface {v0}, LX/1Pq;->iN_()V

    goto :goto_0
.end method

.method public final hv_()V
    .locals 2

    .prologue
    .line 1461570
    iget-object v0, p0, LX/9Hc;->a:LX/9Fa;

    const/4 v1, 0x0

    .line 1461571
    iput-boolean v1, v0, LX/9Fa;->b:Z

    .line 1461572
    iget-object v0, p0, LX/9Hc;->a:LX/9Fa;

    const/4 v1, 0x1

    .line 1461573
    iput-boolean v1, v0, LX/9Fa;->c:Z

    .line 1461574
    iget-object v0, p0, LX/9Hc;->a:LX/9Fa;

    const/4 v1, 0x0

    .line 1461575
    iput-object v1, v0, LX/9Fa;->d:Ljava/lang/CharSequence;

    .line 1461576
    iget-object v0, p0, LX/9Hc;->b:LX/1Pq;

    invoke-interface {v0}, LX/1Pq;->iN_()V

    .line 1461577
    return-void
.end method
