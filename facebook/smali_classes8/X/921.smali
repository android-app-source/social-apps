.class public final LX/921;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;)V
    .locals 0

    .prologue
    .line 1431856
    iput-object p1, p0, LX/921;->a:Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 3

    .prologue
    .line 1431850
    iget-object v0, p0, LX/921;->a:Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;

    .line 1431851
    iget-object v1, v0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->d:Lcom/facebook/widget/gridview/BetterGridView;

    invoke-virtual {v1}, Lcom/facebook/widget/gridview/BetterGridView;->getChildCount()I

    move-result v1

    if-nez v1, :cond_0

    .line 1431852
    :goto_0
    return-void

    .line 1431853
    :cond_0
    iget-object v1, v0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->d:Lcom/facebook/widget/gridview/BetterGridView;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p0, 0x7f0b0c6a

    invoke-virtual {v2, p0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/widget/gridview/BetterGridView;->a(I)I

    move-result v2

    .line 1431854
    iget-object v1, v0, Lcom/facebook/composer/minutiae/iconpicker/MinutiaeIconPickerFragment;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 1431855
    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    goto :goto_0
.end method
