.class public LX/9Dd;
.super LX/1Cv;
.source ""

# interfaces
.implements LX/21l;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1Cv;",
        "LX/21l",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/3kW;

.field private b:Lcom/facebook/graphql/model/GraphQLStory;

.field private c:Z


# direct methods
.method public constructor <init>(LX/3kW;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1455903
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 1455904
    iput-object p1, p0, LX/9Dd;->a:LX/3kW;

    .line 1455905
    return-void
.end method

.method public static a(LX/0QB;)LX/9Dd;
    .locals 2

    .prologue
    .line 1455900
    new-instance v1, LX/9Dd;

    invoke-static {p0}, LX/3kW;->a(LX/0QB;)LX/3kW;

    move-result-object v0

    check-cast v0, LX/3kW;

    invoke-direct {v1, v0}, LX/9Dd;-><init>(LX/3kW;)V

    .line 1455901
    move-object v0, v1

    .line 1455902
    return-object v0
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1455887
    invoke-static {}, LX/9Dc;->values()[LX/9Dc;

    move-result-object v1

    aget-object v1, v1, p1

    .line 1455888
    sget-object v2, LX/9Db;->a:[I

    invoke-virtual {v1}, LX/9Dc;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 1455889
    :cond_0
    :goto_0
    return-object v0

    .line 1455890
    :pswitch_0
    iget-boolean v1, p0, LX/9Dd;->c:Z

    if-eqz v1, :cond_0

    .line 1455891
    new-instance v0, LX/8SX;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/8SX;-><init>(Landroid/content/Context;)V

    .line 1455892
    const/4 p1, 0x0

    .line 1455893
    iget-object v1, v0, LX/8SX;->f:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1455894
    const/16 v2, 0xb

    invoke-virtual {v1, v2, p1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1455895
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p0, 0x11

    if-lt v2, p0, :cond_1

    .line 1455896
    const/16 v2, 0x15

    invoke-virtual {v1, v2, p1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1455897
    :cond_1
    const/16 v2, 0xe

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1455898
    iget-object v2, v0, LX/8SX;->f:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1455899
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 1455881
    invoke-static {}, LX/9Dc;->values()[LX/9Dc;

    move-result-object v0

    aget-object v0, v0, p4

    .line 1455882
    sget-object v1, LX/9Db;->a:[I

    invoke-virtual {v0}, LX/9Dc;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1455883
    :cond_0
    :goto_0
    return-void

    .line 1455884
    :pswitch_0
    iget-object v0, p0, LX/9Dd;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/0sa;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1455885
    check-cast p3, LX/8SX;

    .line 1455886
    iget-object v0, p0, LX/9Dd;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/0sa;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->k()Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;

    move-result-object v0

    iget-object v1, p0, LX/9Dd;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, LX/8SX;->a(Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1455906
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1455907
    iput-object p1, p0, LX/9Dd;->b:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1455908
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, LX/3kW;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1455909
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/9Dd;->c:Z

    .line 1455910
    :goto_0
    return-void

    .line 1455911
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/9Dd;->c:Z

    goto :goto_0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 1455880
    iget-boolean v0, p0, LX/9Dd;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1455879
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1455878
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1455875
    iget-boolean v0, p0, LX/9Dd;->c:Z

    if-eqz v0, :cond_0

    .line 1455876
    sget-object v0, LX/9Dc;->PRIVACY_EDUCATION:LX/9Dc;

    invoke-virtual {v0}, LX/9Dc;->ordinal()I

    move-result v0

    .line 1455877
    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/9Dc;->UNKNOWN:LX/9Dc;

    invoke-virtual {v0}, LX/9Dc;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 1455874
    invoke-static {}, LX/9Dc;->values()[LX/9Dc;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
