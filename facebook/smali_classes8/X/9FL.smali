.class public LX/9FL;
.super LX/16T;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/9FL;


# instance fields
.field private final a:LX/20l;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/9Aw;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/20l;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/20l;",
            "LX/0Or",
            "<",
            "LX/9Aw;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1458457
    invoke-direct {p0}, LX/16T;-><init>()V

    .line 1458458
    iput-object p1, p0, LX/9FL;->a:LX/20l;

    .line 1458459
    iput-object p2, p0, LX/9FL;->b:LX/0Or;

    .line 1458460
    return-void
.end method

.method public static a(LX/0QB;)LX/9FL;
    .locals 5

    .prologue
    .line 1458461
    sget-object v0, LX/9FL;->c:LX/9FL;

    if-nez v0, :cond_1

    .line 1458462
    const-class v1, LX/9FL;

    monitor-enter v1

    .line 1458463
    :try_start_0
    sget-object v0, LX/9FL;->c:LX/9FL;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1458464
    if-eqz v2, :cond_0

    .line 1458465
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1458466
    new-instance v4, LX/9FL;

    invoke-static {v0}, LX/20l;->a(LX/0QB;)LX/20l;

    move-result-object v3

    check-cast v3, LX/20l;

    const/16 p0, 0x1d84

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/9FL;-><init>(LX/20l;LX/0Or;)V

    .line 1458467
    move-object v0, v4

    .line 1458468
    sput-object v0, LX/9FL;->c:LX/9FL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1458469
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1458470
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1458471
    :cond_1
    sget-object v0, LX/9FL;->c:LX/9FL;

    return-object v0

    .line 1458472
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1458473
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 1458474
    const-wide/32 v0, 0x5265c00

    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 1

    .prologue
    .line 1458475
    iget-object v0, p0, LX/9FL;->a:LX/20l;

    invoke-virtual {v0}, LX/20l;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final a(Landroid/view/View;Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 4

    .prologue
    .line 1458476
    new-instance v1, LX/9FK;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v2, 0x1

    invoke-direct {v1, v0, v2}, LX/9FK;-><init>(Landroid/content/Context;I)V

    .line 1458477
    invoke-virtual {v1}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1458478
    const/4 v2, -0x1

    .line 1458479
    iput v2, v1, LX/0hs;->t:I

    .line 1458480
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08198e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 1458481
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f08198f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 1458482
    const v0, 0x3e4ccccd    # 0.2f

    invoke-virtual {v1, v0}, LX/0ht;->b(F)V

    .line 1458483
    const-string v0, " "

    invoke-virtual {v1, v0}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 1458484
    iget-object v0, p0, LX/9FL;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Aw;

    .line 1458485
    invoke-virtual {v0, p2}, LX/9Aw;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1458486
    const/4 p0, 0x0

    .line 1458487
    iget-object v2, v1, LX/0ht;->g:LX/5OY;

    const v3, 0x7f0d0941

    invoke-virtual {v2, v3}, LX/5OY;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1458488
    invoke-virtual {v2, v0, p0, p0, p0}, Landroid/widget/TextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1458489
    invoke-virtual {v1, p1}, LX/0ht;->a(Landroid/view/View;)V

    .line 1458490
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1458491
    const-string v0, "4239"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1458492
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->COMMENT_UFI_LIKE_CLICKED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
