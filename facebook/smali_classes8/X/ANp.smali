.class public LX/ANp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ANo;


# instance fields
.field public final a:LX/6Jt;

.field public final b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public final c:Landroid/content/Context;

.field public final d:LX/ANk;

.field public final e:LX/7S6;

.field public final f:LX/6KY;

.field public g:LX/AOI;

.field private h:Landroid/view/View$OnTouchListener;

.field public i:LX/ANj;


# direct methods
.method public constructor <init>(LX/6Jt;Lcom/facebook/widget/recyclerview/BetterRecyclerView;Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1668400
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1668401
    new-instance v0, LX/ANl;

    invoke-direct {v0, p0}, LX/ANl;-><init>(LX/ANp;)V

    iput-object v0, p0, LX/ANp;->d:LX/ANk;

    .line 1668402
    iput-object p1, p0, LX/ANp;->a:LX/6Jt;

    .line 1668403
    iput-object p2, p0, LX/ANp;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1668404
    iput-object p3, p0, LX/ANp;->c:Landroid/content/Context;

    .line 1668405
    new-instance v0, LX/3rW;

    new-instance v1, LX/ANn;

    invoke-direct {v1, p0}, LX/ANn;-><init>(LX/ANp;)V

    invoke-direct {v0, p3, v1}, LX/3rW;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    .line 1668406
    new-instance v1, LX/ANm;

    invoke-direct {v1, p0, v0}, LX/ANm;-><init>(LX/ANp;LX/3rW;)V

    iput-object v1, p0, LX/ANp;->h:Landroid/view/View$OnTouchListener;

    .line 1668407
    new-instance v0, LX/7S6;

    invoke-direct {v0}, LX/7S6;-><init>()V

    iput-object v0, p0, LX/ANp;->e:LX/7S6;

    .line 1668408
    new-instance v0, LX/6KY;

    iget-object v1, p0, LX/ANp;->e:LX/7S6;

    invoke-direct {v0, v1}, LX/6KY;-><init>(LX/61B;)V

    iput-object v0, p0, LX/ANp;->f:LX/6KY;

    .line 1668409
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 1

    .prologue
    .line 1668399
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;)V
    .locals 2

    .prologue
    .line 1668393
    iget-object v0, p0, LX/ANp;->h:Landroid/view/View$OnTouchListener;

    invoke-virtual {p1, v0}, Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;->a(Landroid/view/View$OnTouchListener;)V

    .line 1668394
    iget-object v0, p0, LX/ANp;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, LX/ANp;->g:LX/AOI;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1668395
    return-void
.end method

.method public final b(Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;)V
    .locals 2

    .prologue
    .line 1668396
    iget-object v0, p0, LX/ANp;->h:Landroid/view/View$OnTouchListener;

    invoke-virtual {p1, v0}, Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;->b(Landroid/view/View$OnTouchListener;)V

    .line 1668397
    iget-object v0, p0, LX/ANp;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1668398
    return-void
.end method
