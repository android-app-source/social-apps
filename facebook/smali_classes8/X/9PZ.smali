.class public final LX/9PZ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 1485156
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_9

    .line 1485157
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1485158
    :goto_0
    return v1

    .line 1485159
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1485160
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_8

    .line 1485161
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1485162
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1485163
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_1

    if-eqz v6, :cond_1

    .line 1485164
    const-string v7, "__type__"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    const-string v7, "__typename"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1485165
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v5

    goto :goto_1

    .line 1485166
    :cond_3
    const-string v7, "id"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1485167
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 1485168
    :cond_4
    const-string v7, "item_price"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1485169
    const/4 v6, 0x0

    .line 1485170
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v7, :cond_d

    .line 1485171
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1485172
    :goto_2
    move v3, v6

    .line 1485173
    goto :goto_1

    .line 1485174
    :cond_5
    const-string v7, "photos"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 1485175
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1485176
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->START_ARRAY:LX/15z;

    if-ne v6, v7, :cond_6

    .line 1485177
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_ARRAY:LX/15z;

    if-eq v6, v7, :cond_6

    .line 1485178
    const/4 v7, 0x0

    .line 1485179
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v6, v8, :cond_11

    .line 1485180
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1485181
    :goto_4
    move v6, v7

    .line 1485182
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1485183
    :cond_6
    invoke-static {v2, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 1485184
    goto/16 :goto_1

    .line 1485185
    :cond_7
    const-string v7, "story"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1485186
    invoke-static {p0, p1}, LX/9PY;->a(LX/15w;LX/186;)I

    move-result v0

    goto/16 :goto_1

    .line 1485187
    :cond_8
    const/4 v6, 0x5

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1485188
    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1485189
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1485190
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1485191
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1485192
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1485193
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_9
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto/16 :goto_1

    .line 1485194
    :cond_a
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1485195
    :cond_b
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_c

    .line 1485196
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1485197
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1485198
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_b

    if-eqz v7, :cond_b

    .line 1485199
    const-string v8, "formatted"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 1485200
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_5

    .line 1485201
    :cond_c
    const/4 v7, 0x1

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1485202
    invoke-virtual {p1, v6, v3}, LX/186;->b(II)V

    .line 1485203
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto/16 :goto_2

    :cond_d
    move v3, v6

    goto :goto_5

    .line 1485204
    :cond_e
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1485205
    :cond_f
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_10

    .line 1485206
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1485207
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1485208
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_f

    if-eqz v8, :cond_f

    .line 1485209
    const-string v9, "image"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_e

    .line 1485210
    const/4 v8, 0x0

    .line 1485211
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v6, v9, :cond_15

    .line 1485212
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1485213
    :goto_7
    move v6, v8

    .line 1485214
    goto :goto_6

    .line 1485215
    :cond_10
    const/4 v8, 0x1

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1485216
    invoke-virtual {p1, v7, v6}, LX/186;->b(II)V

    .line 1485217
    invoke-virtual {p1}, LX/186;->d()I

    move-result v7

    goto/16 :goto_4

    :cond_11
    move v6, v7

    goto :goto_6

    .line 1485218
    :cond_12
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1485219
    :cond_13
    :goto_8
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_14

    .line 1485220
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1485221
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1485222
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_13

    if-eqz v9, :cond_13

    .line 1485223
    const-string v10, "uri"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_12

    .line 1485224
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_8

    .line 1485225
    :cond_14
    const/4 v9, 0x1

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1485226
    invoke-virtual {p1, v8, v6}, LX/186;->b(II)V

    .line 1485227
    invoke-virtual {p1}, LX/186;->d()I

    move-result v8

    goto :goto_7

    :cond_15
    move v6, v8

    goto :goto_8
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1485228
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1485229
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1485230
    if-eqz v0, :cond_0

    .line 1485231
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1485232
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1485233
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1485234
    if-eqz v0, :cond_1

    .line 1485235
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1485236
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1485237
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1485238
    if-eqz v0, :cond_3

    .line 1485239
    const-string v1, "item_price"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1485240
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1485241
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1485242
    if-eqz v1, :cond_2

    .line 1485243
    const-string v2, "formatted"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1485244
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1485245
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1485246
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1485247
    if-eqz v0, :cond_7

    .line 1485248
    const-string v1, "photos"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1485249
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1485250
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_6

    .line 1485251
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    .line 1485252
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1485253
    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1485254
    if-eqz v3, :cond_5

    .line 1485255
    const-string v4, "image"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1485256
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1485257
    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1485258
    if-eqz v4, :cond_4

    .line 1485259
    const-string v2, "uri"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1485260
    invoke-virtual {p2, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1485261
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1485262
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1485263
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1485264
    :cond_6
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1485265
    :cond_7
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1485266
    if-eqz v0, :cond_8

    .line 1485267
    const-string v1, "story"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1485268
    invoke-static {p0, v0, p2}, LX/9PY;->a(LX/15i;ILX/0nX;)V

    .line 1485269
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1485270
    return-void
.end method
