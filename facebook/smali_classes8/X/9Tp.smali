.class public LX/9Tp;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;",
            ">;"
        }
    .end annotation
.end field

.field public static final e:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 14

    .prologue
    const/4 v13, 0x4

    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 1496763
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->BLOCK_ACTOR:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->BLOCK_PAGE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->DELETE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_ADVERTISER:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_AD:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->MARK_AS_SPAM:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const/16 v6, 0xb

    new-array v6, v6, [Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REPORT_CONTENT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v7, v6, v9

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->SPAM_CLEANUP_CHECKPOINT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v7, v6, v10

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNFRIEND:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v7, v6, v11

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNLIKE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v7, v6, v12

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v7, v6, v13

    const/4 v7, 0x5

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_OWNER:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v8, v6, v7

    const/4 v7, 0x6

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_PAGE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v8, v6, v7

    const/4 v7, 0x7

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_RESHARER:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v8, v6, v7

    const/16 v7, 0x8

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_ATTACHED_STORY_ACTOR:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v8, v6, v7

    const/16 v7, 0x9

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_DIRECTED_TARGET:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v8, v6, v7

    const/16 v7, 0xa

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNTAG:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v8, v6, v7

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/9Tp;->a:LX/0Rf;

    .line 1496764
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->BLOCK_ACTOR:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->BLOCK_APP:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->BLOCK_PAGE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->DELETE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_ADVERTISER:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_AD:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const/16 v6, 0xf

    new-array v6, v6, [Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->MARK_AS_SPAM:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v7, v6, v9

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->MESSAGE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v7, v6, v10

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REDIRECT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v7, v6, v11

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REPORT_CONTENT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v7, v6, v12

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REPORT_IP_VIOLATION:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v7, v6, v13

    const/4 v7, 0x5

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->SPAM_CLEANUP_CHECKPOINT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v8, v6, v7

    const/4 v7, 0x6

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNFRIEND:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v8, v6, v7

    const/4 v7, 0x7

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNLIKE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v8, v6, v7

    const/16 v7, 0x8

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v8, v6, v7

    const/16 v7, 0x9

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_OWNER:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v8, v6, v7

    const/16 v7, 0xa

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_PAGE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v8, v6, v7

    const/16 v7, 0xb

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_RESHARER:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v8, v6, v7

    const/16 v7, 0xc

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_ATTACHED_STORY_ACTOR:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v8, v6, v7

    const/16 v7, 0xd

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_DIRECTED_TARGET:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v8, v6, v7

    const/16 v7, 0xe

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNTAG:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    aput-object v8, v6, v7

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/9Tp;->b:LX/0Rf;

    .line 1496765
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REDIRECT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REPORT_IP_VIOLATION:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/9Tp;->c:LX/0Rf;

    .line 1496766
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->REPORT_CONTENT:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/9Tp;->d:LX/0Rf;

    .line 1496767
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->BLOCK_ACTOR:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const v2, 0x7f08242b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->BLOCK_APP:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const v2, 0x7f08242b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->BLOCK_PAGE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const v2, 0x7f08242b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->DELETE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const v2, 0x7f08242c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNFRIEND:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const v2, 0x7f08242d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNLIKE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const v2, 0x7f08242e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const v2, 0x7f08242f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_OWNER:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const v2, 0x7f08242f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_PAGE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const v2, 0x7f08242f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_RESHARER:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const v2, 0x7f08242f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_ATTACHED_STORY_ACTOR:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const v2, 0x7f08242f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSUBSCRIBE_DIRECTED_TARGET:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const v2, 0x7f08242f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNTAG:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const v2, 0x7f082430

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/9Tp;->e:LX/0P1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1496768
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1496769
    return-void
.end method

.method public static a(ZLcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;)I
    .locals 2

    .prologue
    .line 1496770
    if-eqz p0, :cond_0

    .line 1496771
    const v0, 0x7f0207db

    .line 1496772
    :goto_0
    return v0

    .line 1496773
    :cond_0
    sget-object v0, LX/9Tn;->a:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1496774
    const v0, 0x7f020737

    goto :goto_0

    .line 1496775
    :pswitch_0
    const v0, 0x7f020888

    goto :goto_0

    .line 1496776
    :pswitch_1
    const v0, 0x7f02080f

    goto :goto_0

    .line 1496777
    :pswitch_2
    const v0, 0x7f020a0b

    goto :goto_0

    .line 1496778
    :pswitch_3
    const v0, 0x7f0208ed

    goto :goto_0

    .line 1496779
    :pswitch_4
    const v0, 0x7f0209ae

    goto :goto_0

    .line 1496780
    :pswitch_5
    const v0, 0x7f020809

    goto :goto_0

    .line 1496781
    :pswitch_6
    const v0, 0x7f020945

    goto :goto_0

    .line 1496782
    :pswitch_7
    const v0, 0x7f0209ae

    goto :goto_0

    .line 1496783
    :pswitch_8
    const v0, 0x7f020945

    goto :goto_0

    .line 1496784
    :pswitch_9
    const v0, 0x7f0208ac

    goto :goto_0

    .line 1496785
    :pswitch_a
    const v0, 0x7f02089e

    goto :goto_0

    .line 1496786
    :pswitch_b
    const v0, 0x7f020743

    goto :goto_0

    .line 1496787
    :pswitch_c
    const v0, 0x7f020a1d

    goto :goto_0

    .line 1496788
    :pswitch_d
    const v0, 0x7f0209f2

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method
