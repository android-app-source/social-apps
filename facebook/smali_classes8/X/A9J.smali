.class public final LX/A9J;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 1634100
    const/4 v5, 0x0

    .line 1634101
    const-wide/16 v6, 0x0

    .line 1634102
    const/4 v4, 0x0

    .line 1634103
    const/4 v3, 0x0

    .line 1634104
    const/4 v2, 0x0

    .line 1634105
    const/4 v1, 0x0

    .line 1634106
    const/4 v0, 0x0

    .line 1634107
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v8, v9, :cond_9

    .line 1634108
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1634109
    const/4 v0, 0x0

    .line 1634110
    :goto_0
    return v0

    .line 1634111
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v0, v5, :cond_5

    .line 1634112
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    .line 1634113
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1634114
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v10, :cond_0

    if-eqz v0, :cond_0

    .line 1634115
    const-string v5, "height"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1634116
    const/4 v0, 0x1

    .line 1634117
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v9, v4

    move v4, v0

    goto :goto_1

    .line 1634118
    :cond_1
    const-string v5, "scale"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1634119
    const/4 v0, 0x1

    .line 1634120
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v1, v0

    goto :goto_1

    .line 1634121
    :cond_2
    const-string v5, "uri"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1634122
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    move v8, v0

    goto :goto_1

    .line 1634123
    :cond_3
    const-string v5, "width"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1634124
    const/4 v0, 0x1

    .line 1634125
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v5

    move v6, v0

    move v7, v5

    goto :goto_1

    .line 1634126
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1634127
    :cond_5
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1634128
    if-eqz v4, :cond_6

    .line 1634129
    const/4 v0, 0x0

    const/4 v4, 0x0

    invoke-virtual {p1, v0, v9, v4}, LX/186;->a(III)V

    .line 1634130
    :cond_6
    if-eqz v1, :cond_7

    .line 1634131
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1634132
    :cond_7
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1634133
    if-eqz v6, :cond_8

    .line 1634134
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v7, v1}, LX/186;->a(III)V

    .line 1634135
    :cond_8
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_9
    move v8, v4

    move v9, v5

    move v4, v2

    move-wide v11, v6

    move v6, v0

    move v7, v3

    move-wide v2, v11

    goto/16 :goto_1
.end method
