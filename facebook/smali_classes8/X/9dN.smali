.class public LX/9dN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9dM;


# instance fields
.field private final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/SwipeableParams;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/9dL;

.field private c:I


# direct methods
.method public constructor <init>(LX/0Px;LX/9dL;)V
    .locals 1
    .param p1    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/SwipeableParams;",
            ">;",
            "LX/9dL;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1518088
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1518089
    const/4 v0, 0x0

    iput v0, p0, LX/9dN;->c:I

    .line 1518090
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, LX/9dN;->a:LX/0Px;

    .line 1518091
    iput-object p2, p0, LX/9dN;->b:LX/9dL;

    .line 1518092
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1518093
    const/4 v0, 0x1

    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1518094
    iget v0, p0, LX/9dN;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/9dN;->c:I

    .line 1518095
    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1518096
    iget v0, p0, LX/9dN;->c:I

    iget-object v1, p0, LX/9dN;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()LX/9dK;
    .locals 1

    .prologue
    .line 1518097
    iget-object v0, p0, LX/9dN;->b:LX/9dL;

    return-object v0
.end method
