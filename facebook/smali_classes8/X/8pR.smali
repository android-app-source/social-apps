.class public LX/8pR;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/content/Context;

.field public b:LX/8pV;

.field public c:LX/15i;

.field public d:I

.field private e:Landroid/content/res/Resources;

.field public f:LX/3AE;

.field public g:LX/8pQ;

.field public h:Ljava/lang/Boolean;

.field private i:Ljava/lang/Boolean;

.field public j:LX/17W;


# direct methods
.method public constructor <init>(Lcom/facebook/translation/TranslatabilityForViewerGraphQLModels$TranslatabilityGraphQLModel;Landroid/content/Context;LX/3AE;Ljava/lang/Boolean;LX/17W;)V
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "<init>"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 1405870
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1405871
    iput-object p2, p0, LX/8pR;->a:Landroid/content/Context;

    .line 1405872
    invoke-virtual {p1}, Lcom/facebook/translation/TranslatabilityForViewerGraphQLModels$TranslatabilityGraphQLModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iput-object v1, p0, LX/8pR;->c:LX/15i;

    iput v0, p0, LX/8pR;->d:I

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1405873
    new-instance v0, LX/8pV;

    iget-object v1, p0, LX/8pR;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/8pV;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/8pR;->b:LX/8pV;

    .line 1405874
    iput-object p3, p0, LX/8pR;->f:LX/3AE;

    .line 1405875
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/8pR;->h:Ljava/lang/Boolean;

    .line 1405876
    iput-object p4, p0, LX/8pR;->i:Ljava/lang/Boolean;

    .line 1405877
    iput-object p5, p0, LX/8pR;->j:LX/17W;

    .line 1405878
    return-void

    .line 1405879
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static a(LX/8pR;LX/5OG;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1405909
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v2, p0, LX/8pR;->d:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_1

    .line 1405910
    :goto_0
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->AUTO_TRANSLATION:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/8pR;->i:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1405911
    iget-object v0, p0, LX/8pR;->h:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f08120a

    invoke-virtual {p1, v0}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v0

    .line 1405912
    :goto_1
    const v1, 0x7f0209b7

    invoke-virtual {v0, v1}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 1405913
    invoke-static {p0}, LX/8pR;->d(LX/8pR;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3Ai;->a(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1405914
    new-instance v1, LX/8pN;

    invoke-direct {v1, p0}, LX/8pN;-><init>(LX/8pR;)V

    invoke-virtual {v0, v1}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1405915
    :cond_0
    invoke-static {p0}, LX/8pR;->e(LX/8pR;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v0

    .line 1405916
    const v1, 0x7f020a9f

    invoke-virtual {v0, v1}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 1405917
    new-instance v1, LX/8pL;

    invoke-direct {v1, p0}, LX/8pL;-><init>(LX/8pR;)V

    invoke-virtual {v0, v1}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1405918
    invoke-static {p0}, LX/8pR;->f(LX/8pR;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v0

    .line 1405919
    const v1, 0x7f021811

    invoke-virtual {v0, v1}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 1405920
    new-instance v1, LX/8pM;

    invoke-direct {v1, p0}, LX/8pM;-><init>(LX/8pR;)V

    invoke-virtual {v0, v1}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1405921
    return-void

    .line 1405922
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1405923
    :cond_1
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_2
    iget-object v2, p0, LX/8pR;->c:LX/15i;

    iget v3, p0, LX/8pR;->d:I

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1405924
    const/4 v1, 0x4

    const-class v4, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    invoke-virtual {v2, v3, v1, v4, v0}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    goto :goto_0

    .line 1405925
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 1405926
    :cond_2
    const v0, 0x7f08120b

    invoke-virtual {p1, v0}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v0

    goto :goto_1
.end method

.method public static d(LX/8pR;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1405904
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LX/8pR;->c:LX/15i;

    iget v2, p0, LX/8pR;->d:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1405905
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_1
    iget-object v3, p0, LX/8pR;->c:LX/15i;

    iget v4, p0, LX/8pR;->d:I

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1405906
    iget-object v1, p0, LX/8pR;->e:Landroid/content/res/Resources;

    const v5, 0x7f08120c

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v5, 0x1

    invoke-virtual {v0, v2, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x3

    invoke-virtual {v3, v4, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1405907
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1405908
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static e(LX/8pR;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1405901
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LX/8pR;->c:LX/15i;

    iget v2, p0, LX/8pR;->d:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1405902
    iget-object v1, p0, LX/8pR;->e:Landroid/content/res/Resources;

    const v3, 0x7f08120d

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1405903
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static f(LX/8pR;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1405898
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LX/8pR;->c:LX/15i;

    iget v2, p0, LX/8pR;->d:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1405899
    iget-object v1, p0, LX/8pR;->e:Landroid/content/res/Resources;

    const v3, 0x7f081206

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1405900
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static g(LX/8pR;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1405895
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LX/8pR;->c:LX/15i;

    iget v2, p0, LX/8pR;->d:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1405896
    iget-object v1, p0, LX/8pR;->e:Landroid/content/res/Resources;

    const v3, 0x7f081210

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1405897
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1405882
    iget-object v0, p0, LX/8pR;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, LX/8pR;->e:Landroid/content/res/Resources;

    .line 1405883
    new-instance v0, LX/5OM;

    iget-object v2, p0, LX/8pR;->a:Landroid/content/Context;

    invoke-direct {v0, v2}, LX/5OM;-><init>(Landroid/content/Context;)V

    .line 1405884
    invoke-virtual {v0}, LX/5OM;->c()LX/5OG;

    move-result-object v2

    .line 1405885
    invoke-static {p0, v2}, LX/8pR;->a(LX/8pR;LX/5OG;)V

    .line 1405886
    invoke-virtual {v0, v2}, LX/5OM;->a(LX/5OG;)V

    .line 1405887
    iget-object v2, p0, LX/8pR;->b:LX/8pV;

    .line 1405888
    iput-object v2, v0, LX/5OM;->o:Landroid/view/View;

    .line 1405889
    move-object v0, v0

    .line 1405890
    iput-boolean v1, v0, LX/0ht;->e:Z

    .line 1405891
    invoke-virtual {v0, v1}, LX/0ht;->c(Z)V

    .line 1405892
    invoke-virtual {v0, p1}, LX/0ht;->c(Landroid/view/View;)V

    .line 1405893
    invoke-virtual {v0}, LX/0ht;->d()V

    .line 1405894
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 1405880
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/8pR;->h:Ljava/lang/Boolean;

    .line 1405881
    return-void
.end method
