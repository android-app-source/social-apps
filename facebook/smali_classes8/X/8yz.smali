.class public LX/8yz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8yy;


# instance fields
.field private a:Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;

.field private b:I

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1426757
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1426758
    iput v0, p0, LX/8yz;->c:I

    .line 1426759
    iput v0, p0, LX/8yz;->d:I

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .prologue
    .line 1426760
    iget-object v0, p0, LX/8yz;->a:Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;

    if-eqz v0, :cond_2

    .line 1426761
    iget v0, p0, LX/8yz;->d:I

    if-eqz v0, :cond_0

    iget v0, p0, LX/8yz;->d:I

    iget v1, p0, LX/8yz;->b:I

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_3

    :cond_0
    const/4 v0, 0x1

    .line 1426762
    :goto_0
    iget v1, p0, LX/8yz;->c:I

    if-le p1, v1, :cond_4

    iget v1, p0, LX/8yz;->d:I

    iget v2, p0, LX/8yz;->b:I

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_4

    .line 1426763
    iget v1, p0, LX/8yz;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/8yz;->d:I

    .line 1426764
    :cond_1
    :goto_1
    iput p1, p0, LX/8yz;->c:I

    .line 1426765
    iget-object v1, p0, LX/8yz;->a:Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;

    iget v2, p0, LX/8yz;->d:I

    invoke-virtual {v1, p1, v2, v0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->a(IIZ)V

    .line 1426766
    :cond_2
    return-void

    .line 1426767
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 1426768
    :cond_4
    iget v1, p0, LX/8yz;->c:I

    if-ge p1, v1, :cond_1

    iget v1, p0, LX/8yz;->d:I

    if-lez v1, :cond_1

    .line 1426769
    iget v1, p0, LX/8yz;->d:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, LX/8yz;->d:I

    goto :goto_1
.end method

.method public final a(II)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1426770
    invoke-virtual {p0, p1}, LX/8yz;->a(I)V

    .line 1426771
    return-void
.end method
