.class public final LX/8pO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:LX/8pR;


# direct methods
.method public constructor <init>(LX/8pR;)V
    .locals 0

    .prologue
    .line 1405855
    iput-object p1, p0, LX/8pO;->a:LX/8pR;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 1405856
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LX/8pO;->a:LX/8pR;

    iget-object v0, v0, LX/8pR;->c:LX/15i;

    iget-object v2, p0, LX/8pO;->a:LX/8pR;

    iget v2, v2, LX/8pR;->d:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1405857
    iget-object v1, p0, LX/8pO;->a:LX/8pR;

    iget-object v1, v1, LX/8pR;->f:LX/3AE;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 1405858
    new-instance v2, LX/4HL;

    invoke-direct {v2}, LX/4HL;-><init>()V

    .line 1405859
    const-string v3, "language_dialect"

    invoke-virtual {v2, v3, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1405860
    move-object v2, v2

    .line 1405861
    new-instance v3, LX/7fj;

    invoke-direct {v3}, LX/7fj;-><init>()V

    move-object v3, v3

    .line 1405862
    const-string p0, "input"

    invoke-virtual {v3, p0, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1405863
    invoke-static {v3}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v2

    .line 1405864
    iget-object v3, v1, LX/3AE;->a:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1405865
    return-void

    .line 1405866
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
