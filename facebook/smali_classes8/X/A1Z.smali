.class public final LX/A1Z;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/search/protocol/nullstate/FetchSearchCategorySubTopicsGraphQLModels$FetchSearchCategorySubtopicsModel;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1611219
    const-class v1, Lcom/facebook/search/protocol/nullstate/FetchSearchCategorySubTopicsGraphQLModels$FetchSearchCategorySubtopicsModel;

    const v0, -0x4e8ca1e3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x2

    const-string v5, "FetchSearchCategorySubtopics"

    const-string v6, "1b699bd2076c2beff6fe6a357ab8619d"

    const-string v7, "nodes"

    const-string v8, "10155069965876729"

    const/4 v9, 0x0

    .line 1611220
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1611221
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1611222
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1611223
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1611224
    sparse-switch v0, :sswitch_data_0

    .line 1611225
    :goto_0
    return-object p1

    .line 1611226
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1611227
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x123cb666 -> :sswitch_1
        0x172b63a8 -> :sswitch_0
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1611228
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 1611229
    :goto_1
    return v0

    .line 1611230
    :pswitch_0
    const-string v2, "1"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 1611231
    :pswitch_1
    const/16 v0, 0xa

    const-string v1, "%s"

    invoke-static {p2, v0, v1}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x31
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method

.method public final l()Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;
    .locals 2

    .prologue
    .line 1611232
    new-instance v0, Lcom/facebook/search/protocol/nullstate/FetchSearchCategorySubTopicsGraphQL$FetchSearchCategorySubtopicsString$1;

    const-class v1, Lcom/facebook/search/protocol/nullstate/FetchSearchCategorySubTopicsGraphQLModels$FetchSearchCategorySubtopicsModel;

    invoke-direct {v0, p0, v1}, Lcom/facebook/search/protocol/nullstate/FetchSearchCategorySubTopicsGraphQL$FetchSearchCategorySubtopicsString$1;-><init>(LX/A1Z;Ljava/lang/Class;)V

    return-object v0
.end method
