.class public final enum LX/ASW;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ASW;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ASW;

.field public static final enum COMPOSER_TAG_EXPANSION_PILL_NUX:LX/ASW;

.field public static final enum FACECAST_ICON_NUX:LX/ASW;

.field public static final enum INTERSTITIAL_NUX:LX/ASW;

.field public static final enum LIVE_TOPIC_COMPOSER_NUX:LX/ASW;

.field public static final enum TAG_PEOPLE_FOR_CHECKIN:LX/ASW;

.field public static final enum TAG_PLACE_AFTER_PHOTO:LX/ASW;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1674024
    new-instance v0, LX/ASW;

    const-string v1, "INTERSTITIAL_NUX"

    invoke-direct {v0, v1, v3}, LX/ASW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ASW;->INTERSTITIAL_NUX:LX/ASW;

    .line 1674025
    new-instance v0, LX/ASW;

    const-string v1, "TAG_PEOPLE_FOR_CHECKIN"

    invoke-direct {v0, v1, v4}, LX/ASW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ASW;->TAG_PEOPLE_FOR_CHECKIN:LX/ASW;

    .line 1674026
    new-instance v0, LX/ASW;

    const-string v1, "TAG_PLACE_AFTER_PHOTO"

    invoke-direct {v0, v1, v5}, LX/ASW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ASW;->TAG_PLACE_AFTER_PHOTO:LX/ASW;

    .line 1674027
    new-instance v0, LX/ASW;

    const-string v1, "FACECAST_ICON_NUX"

    invoke-direct {v0, v1, v6}, LX/ASW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ASW;->FACECAST_ICON_NUX:LX/ASW;

    .line 1674028
    new-instance v0, LX/ASW;

    const-string v1, "LIVE_TOPIC_COMPOSER_NUX"

    invoke-direct {v0, v1, v7}, LX/ASW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ASW;->LIVE_TOPIC_COMPOSER_NUX:LX/ASW;

    .line 1674029
    new-instance v0, LX/ASW;

    const-string v1, "COMPOSER_TAG_EXPANSION_PILL_NUX"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/ASW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ASW;->COMPOSER_TAG_EXPANSION_PILL_NUX:LX/ASW;

    .line 1674030
    const/4 v0, 0x6

    new-array v0, v0, [LX/ASW;

    sget-object v1, LX/ASW;->INTERSTITIAL_NUX:LX/ASW;

    aput-object v1, v0, v3

    sget-object v1, LX/ASW;->TAG_PEOPLE_FOR_CHECKIN:LX/ASW;

    aput-object v1, v0, v4

    sget-object v1, LX/ASW;->TAG_PLACE_AFTER_PHOTO:LX/ASW;

    aput-object v1, v0, v5

    sget-object v1, LX/ASW;->FACECAST_ICON_NUX:LX/ASW;

    aput-object v1, v0, v6

    sget-object v1, LX/ASW;->LIVE_TOPIC_COMPOSER_NUX:LX/ASW;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/ASW;->COMPOSER_TAG_EXPANSION_PILL_NUX:LX/ASW;

    aput-object v2, v0, v1

    sput-object v0, LX/ASW;->$VALUES:[LX/ASW;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1674031
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ASW;
    .locals 1

    .prologue
    .line 1674032
    const-class v0, LX/ASW;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ASW;

    return-object v0
.end method

.method public static values()[LX/ASW;
    .locals 1

    .prologue
    .line 1674033
    sget-object v0, LX/ASW;->$VALUES:[LX/ASW;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ASW;

    return-object v0
.end method
