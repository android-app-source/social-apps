.class public final enum LX/9ek;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9ek;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9ek;

.field public static final enum NONE:LX/9ek;

.field public static final enum SHOW_EDITED_URI:LX/9ek;

.field public static final enum SHOW_ORIGINAL_URI:LX/9ek;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1520289
    new-instance v0, LX/9ek;

    const-string v1, "SHOW_ORIGINAL_URI"

    invoke-direct {v0, v1, v2}, LX/9ek;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9ek;->SHOW_ORIGINAL_URI:LX/9ek;

    .line 1520290
    new-instance v0, LX/9ek;

    const-string v1, "SHOW_EDITED_URI"

    invoke-direct {v0, v1, v3}, LX/9ek;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9ek;->SHOW_EDITED_URI:LX/9ek;

    .line 1520291
    new-instance v0, LX/9ek;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v4}, LX/9ek;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9ek;->NONE:LX/9ek;

    .line 1520292
    const/4 v0, 0x3

    new-array v0, v0, [LX/9ek;

    sget-object v1, LX/9ek;->SHOW_ORIGINAL_URI:LX/9ek;

    aput-object v1, v0, v2

    sget-object v1, LX/9ek;->SHOW_EDITED_URI:LX/9ek;

    aput-object v1, v0, v3

    sget-object v1, LX/9ek;->NONE:LX/9ek;

    aput-object v1, v0, v4

    sput-object v0, LX/9ek;->$VALUES:[LX/9ek;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1520295
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9ek;
    .locals 1

    .prologue
    .line 1520294
    const-class v0, LX/9ek;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9ek;

    return-object v0
.end method

.method public static values()[LX/9ek;
    .locals 1

    .prologue
    .line 1520293
    sget-object v0, LX/9ek;->$VALUES:[LX/9ek;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9ek;

    return-object v0
.end method
