.class public LX/9ke;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9kZ;


# instance fields
.field private final a:LX/96B;

.field private final b:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;


# direct methods
.method public constructor <init>(LX/96B;Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;)V
    .locals 0
    .param p2    # Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1532031
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1532032
    iput-object p1, p0, LX/9ke;->a:LX/96B;

    .line 1532033
    iput-object p2, p0, LX/9ke;->b:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    .line 1532034
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/ipc/model/PageTopic;)V
    .locals 6

    .prologue
    .line 1532044
    iget-object v0, p0, LX/9ke;->a:LX/96B;

    iget-object v1, p0, LX/9ke;->b:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    iget-wide v2, p1, Lcom/facebook/ipc/model/PageTopic;->id:J

    .line 1532045
    iget-object v4, v0, LX/96B;->a:LX/0Zb;

    const-string v5, "hierarchy_result_tapped"

    invoke-static {v0, v1, v5}, LX/96B;->a(LX/96B;Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string p0, "event_obj_id"

    invoke-virtual {v5, p0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-interface {v4, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1532046
    return-void
.end method

.method public final a(Lcom/facebook/ipc/model/PageTopic;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 1532041
    iget-object v1, p0, LX/9ke;->a:LX/96B;

    iget-object v2, p0, LX/9ke;->b:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    sget-object v3, LX/969;->PLACE_CATEGORY:LX/969;

    iget-wide v4, p1, Lcom/facebook/ipc/model/PageTopic;->id:J

    move-object v6, p2

    .line 1532042
    iget-object v0, v1, LX/96B;->a:LX/0Zb;

    const-string v7, "search_result_tapped"

    invoke-static {v1, v2, v7}, LX/96B;->a(LX/96B;Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "field_type_name"

    iget-object p1, v3, LX/969;->name:Ljava/lang/String;

    invoke-virtual {v7, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "event_obj_id"

    invoke-virtual {v7, p0, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "query"

    invoke-virtual {v7, p0, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    invoke-interface {v0, v7}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1532043
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1532038
    iget-object v0, p0, LX/9ke;->a:LX/96B;

    iget-object v1, p0, LX/9ke;->b:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    sget-object v2, LX/969;->PLACE_CATEGORY:LX/969;

    .line 1532039
    iget-object v3, v0, LX/96B;->a:LX/0Zb;

    const-string v4, "no_results_found"

    invoke-static {v0, v1, v4}, LX/96B;->a(LX/96B;Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "field_type_name"

    iget-object p0, v2, LX/969;->name:Ljava/lang/String;

    invoke-virtual {v4, v5, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "query"

    invoke-virtual {v4, v5, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-interface {v3, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1532040
    return-void
.end method

.method public final b(Lcom/facebook/ipc/model/PageTopic;)V
    .locals 6

    .prologue
    .line 1532035
    iget-object v0, p0, LX/9ke;->a:LX/96B;

    iget-object v1, p0, LX/9ke;->b:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    iget-wide v2, p1, Lcom/facebook/ipc/model/PageTopic;->id:J

    .line 1532036
    iget-object v4, v0, LX/96B;->a:LX/0Zb;

    const-string v5, "typeahead_parent_category_viewed"

    invoke-static {v0, v1, v5}, LX/96B;->a(LX/96B;Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string p0, "event_obj_id"

    invoke-virtual {v5, p0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-interface {v4, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1532037
    return-void
.end method
