.class public final LX/9lU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/399;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;

.field public final synthetic b:LX/9lb;


# direct methods
.method public constructor <init>(LX/9lb;Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;)V
    .locals 0

    .prologue
    .line 1532933
    iput-object p1, p0, LX/9lU;->b:LX/9lb;

    iput-object p2, p0, LX/9lU;->a:Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1532934
    iget-object v0, p0, LX/9lU;->b:LX/9lb;

    iget-object v1, p0, LX/9lU;->a:Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;

    sget-object v2, LX/9lY;->NETWORK_ERROR:LX/9lY;

    invoke-static {v0, v1, v2}, LX/9lb;->a$redex0(LX/9lb;Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;LX/9lY;)V

    .line 1532935
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1532936
    check-cast p1, LX/399;

    .line 1532937
    if-nez p1, :cond_0

    .line 1532938
    iget-object v0, p0, LX/9lU;->b:LX/9lb;

    iget-object v1, p0, LX/9lU;->a:Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;

    sget-object v2, LX/9lY;->SERVER_ERROR:LX/9lY;

    invoke-static {v0, v1, v2}, LX/9lb;->a$redex0(LX/9lb;Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;LX/9lY;)V

    .line 1532939
    :goto_0
    return-void

    .line 1532940
    :cond_0
    iget-object v0, p0, LX/9lU;->b:LX/9lb;

    iget-object v1, p0, LX/9lU;->a:Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;

    .line 1532941
    iget-object v2, v0, LX/9lb;->b:LX/1Ck;

    sget-object v3, LX/9la;->ADDITIONAL_DATA_REPORT:LX/9la;

    iget-object v4, v0, LX/9lb;->f:LX/0tX;

    invoke-virtual {v4, p1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    new-instance p0, LX/9lV;

    invoke-direct {p0, v0, v1}, LX/9lV;-><init>(LX/9lb;Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;)V

    invoke-static {p0}, LX/0Vd;->of(LX/0TF;)LX/0Vd;

    move-result-object p0

    invoke-virtual {v2, v3, v4, p0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1532942
    goto :goto_0
.end method
