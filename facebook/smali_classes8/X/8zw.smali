.class public LX/8zw;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/8zu;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8zy;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1428328
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/8zw;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/8zy;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1428325
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1428326
    iput-object p1, p0, LX/8zw;->b:LX/0Ot;

    .line 1428327
    return-void
.end method

.method public static a(LX/0QB;)LX/8zw;
    .locals 4

    .prologue
    .line 1428329
    const-class v1, LX/8zw;

    monitor-enter v1

    .line 1428330
    :try_start_0
    sget-object v0, LX/8zw;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1428331
    sput-object v2, LX/8zw;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1428332
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1428333
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1428334
    new-instance v3, LX/8zw;

    const/16 p0, 0x1997

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/8zw;-><init>(LX/0Ot;)V

    .line 1428335
    move-object v0, v3

    .line 1428336
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1428337
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8zw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1428338
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1428339
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 1428314
    check-cast p2, LX/8zv;

    .line 1428315
    iget-object v0, p0, LX/8zw;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8zy;

    iget-object v2, p2, LX/8zv;->a:LX/8zj;

    iget-object v3, p2, LX/8zv;->b:Ljava/lang/String;

    iget-object v4, p2, LX/8zv;->c:LX/8zx;

    iget-object v5, p2, LX/8zv;->d:Landroid/view/View$OnClickListener;

    iget-object v6, p2, LX/8zv;->e:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    iget-object v7, p2, LX/8zv;->f:LX/903;

    move-object v1, p1

    const/4 p0, 0x0

    const/4 p2, 0x0

    .line 1428316
    sget-object v8, LX/8zx;->NETWORK_ERROR:LX/8zx;

    if-ne v4, v8, :cond_0

    .line 1428317
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v8

    iget-object p0, v0, LX/8zy;->b:LX/91K;

    invoke-virtual {p0, v1}, LX/91K;->c(LX/1De;)LX/91J;

    move-result-object p0

    const p1, 0x7f08003b

    invoke-virtual {p0, p1}, LX/91J;->h(I)LX/91J;

    move-result-object p0

    invoke-virtual {p0, v5}, LX/91J;->a(Landroid/view/View$OnClickListener;)LX/91J;

    move-result-object p0

    invoke-interface {v8, p0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v8

    invoke-interface {v8}, LX/1Di;->k()LX/1Dg;

    move-result-object v8

    .line 1428318
    :goto_0
    move-object v0, v8

    .line 1428319
    return-object v0

    .line 1428320
    :cond_0
    sget-object v8, LX/8zx;->GENERIC_ERROR:LX/8zx;

    if-ne v4, v8, :cond_1

    .line 1428321
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v8

    iget-object p0, v0, LX/8zy;->b:LX/91K;

    invoke-virtual {p0, v1}, LX/91K;->c(LX/1De;)LX/91J;

    move-result-object p0

    invoke-virtual {p0, v5}, LX/91J;->a(Landroid/view/View$OnClickListener;)LX/91J;

    move-result-object p0

    invoke-interface {v8, p0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v8

    invoke-interface {v8}, LX/1Di;->k()LX/1Dg;

    move-result-object v8

    goto :goto_0

    .line 1428322
    :cond_1
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v8

    const/4 p1, 0x4

    invoke-interface {v8, p1}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v8

    invoke-interface {v8, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object p1

    if-nez v6, :cond_2

    move-object v8, p0

    :goto_1
    invoke-interface {p1, v8}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v8

    if-eqz v6, :cond_3

    :goto_2
    invoke-interface {v8, p0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v8

    invoke-static {v1}, LX/5K1;->c(LX/1De;)LX/5Jz;

    move-result-object p0

    invoke-virtual {p0, v2}, LX/5Jz;->a(LX/5Je;)LX/5Jz;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    invoke-interface {p0, p2}, LX/1Di;->a(I)LX/1Di;

    move-result-object p0

    const/high16 p1, 0x3f800000    # 1.0f

    invoke-interface {p0, p1}, LX/1Di;->a(F)LX/1Di;

    move-result-object p0

    invoke-interface {v8, p0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v8

    invoke-interface {v8}, LX/1Di;->k()LX/1Dg;

    move-result-object v8

    goto :goto_0

    :cond_2
    iget-object v8, v0, LX/8zy;->a:LX/91P;

    invoke-virtual {v8, v1}, LX/91P;->c(LX/1De;)LX/91N;

    move-result-object v8

    invoke-virtual {v8, v6}, LX/91N;->a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)LX/91N;

    move-result-object v8

    invoke-virtual {v8, v7}, LX/91N;->a(LX/903;)LX/91N;

    move-result-object v8

    goto :goto_1

    :cond_3
    invoke-static {v1}, LX/91W;->c(LX/1De;)LX/91U;

    move-result-object p0

    invoke-virtual {p0, v3}, LX/91U;->a(Ljava/lang/CharSequence;)LX/91U;

    move-result-object p0

    .line 1428323
    const p1, 0x242331e6

    const/4 v0, 0x0

    invoke-static {v1, p1, v0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p1

    move-object p1, p1

    .line 1428324
    invoke-virtual {p0, p1}, LX/91U;->a(LX/1dQ;)LX/91U;

    move-result-object p0

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, LX/91U;->h(I)LX/91U;

    move-result-object p0

    goto :goto_2
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1428295
    invoke-static {}, LX/1dS;->b()V

    .line 1428296
    iget v0, p1, LX/1dQ;->b:I

    .line 1428297
    packed-switch v0, :pswitch_data_0

    .line 1428298
    :goto_0
    return-object v2

    .line 1428299
    :pswitch_0
    check-cast p2, LX/91c;

    .line 1428300
    iget-object v0, p2, LX/91c;->a:Ljava/lang/String;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1428301
    check-cast v1, LX/8zv;

    .line 1428302
    iget-object p1, p0, LX/8zw;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, LX/8zv;->g:LX/8zz;

    .line 1428303
    if-eqz p1, :cond_0

    .line 1428304
    invoke-interface {p1, v0}, LX/8zz;->a(Ljava/lang/String;)V

    .line 1428305
    :cond_0
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x242331e6
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/8zu;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1428306
    new-instance v1, LX/8zv;

    invoke-direct {v1, p0}, LX/8zv;-><init>(LX/8zw;)V

    .line 1428307
    sget-object v2, LX/8zw;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8zu;

    .line 1428308
    if-nez v2, :cond_0

    .line 1428309
    new-instance v2, LX/8zu;

    invoke-direct {v2}, LX/8zu;-><init>()V

    .line 1428310
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/8zu;->a$redex0(LX/8zu;LX/1De;IILX/8zv;)V

    .line 1428311
    move-object v1, v2

    .line 1428312
    move-object v0, v1

    .line 1428313
    return-object v0
.end method
