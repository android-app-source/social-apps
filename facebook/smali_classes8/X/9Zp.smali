.class public final LX/9Zp;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 28

    .prologue
    .line 1511897
    const/16 v24, 0x0

    .line 1511898
    const/16 v23, 0x0

    .line 1511899
    const/16 v22, 0x0

    .line 1511900
    const/16 v21, 0x0

    .line 1511901
    const/16 v20, 0x0

    .line 1511902
    const/16 v19, 0x0

    .line 1511903
    const/16 v18, 0x0

    .line 1511904
    const/16 v17, 0x0

    .line 1511905
    const/16 v16, 0x0

    .line 1511906
    const/4 v15, 0x0

    .line 1511907
    const/4 v14, 0x0

    .line 1511908
    const/4 v13, 0x0

    .line 1511909
    const/4 v12, 0x0

    .line 1511910
    const/4 v11, 0x0

    .line 1511911
    const/4 v10, 0x0

    .line 1511912
    const/4 v9, 0x0

    .line 1511913
    const/4 v8, 0x0

    .line 1511914
    const/4 v7, 0x0

    .line 1511915
    const/4 v6, 0x0

    .line 1511916
    const/4 v5, 0x0

    .line 1511917
    const/4 v4, 0x0

    .line 1511918
    const/4 v3, 0x0

    .line 1511919
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v25

    sget-object v26, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-eq v0, v1, :cond_1

    .line 1511920
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1511921
    const/4 v3, 0x0

    .line 1511922
    :goto_0
    return v3

    .line 1511923
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1511924
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v25

    sget-object v26, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-eq v0, v1, :cond_13

    .line 1511925
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v25

    .line 1511926
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1511927
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v26

    sget-object v27, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    if-eq v0, v1, :cond_1

    if-eqz v25, :cond_1

    .line 1511928
    const-string v26, "attribution"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_2

    .line 1511929
    invoke-static/range {p0 .. p1}, LX/9Zh;->a(LX/15w;LX/186;)I

    move-result v24

    goto :goto_1

    .line 1511930
    :cond_2
    const-string v26, "business_info"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_3

    .line 1511931
    invoke-static/range {p0 .. p1}, LX/9Zk;->a(LX/15w;LX/186;)I

    move-result v23

    goto :goto_1

    .line 1511932
    :cond_3
    const-string v26, "email_addresses"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_4

    .line 1511933
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v22

    goto :goto_1

    .line 1511934
    :cond_4
    const-string v26, "expressed_as_place"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_5

    .line 1511935
    const/4 v6, 0x1

    .line 1511936
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v21

    goto :goto_1

    .line 1511937
    :cond_5
    const-string v26, "hours"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_6

    .line 1511938
    invoke-static/range {p0 .. p1}, LX/4aY;->a(LX/15w;LX/186;)I

    move-result v20

    goto :goto_1

    .line 1511939
    :cond_6
    const-string v26, "id"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_7

    .line 1511940
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v19

    goto :goto_1

    .line 1511941
    :cond_7
    const-string v26, "is_always_open"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_8

    .line 1511942
    const/4 v5, 0x1

    .line 1511943
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v18

    goto/16 :goto_1

    .line 1511944
    :cond_8
    const-string v26, "is_owned"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_9

    .line 1511945
    const/4 v4, 0x1

    .line 1511946
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v17

    goto/16 :goto_1

    .line 1511947
    :cond_9
    const-string v26, "is_permanently_closed"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_a

    .line 1511948
    const/4 v3, 0x1

    .line 1511949
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v16

    goto/16 :goto_1

    .line 1511950
    :cond_a
    const-string v26, "location"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_b

    .line 1511951
    invoke-static/range {p0 .. p1}, LX/9Zl;->a(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 1511952
    :cond_b
    const-string v26, "name"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_c

    .line 1511953
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    goto/16 :goto_1

    .line 1511954
    :cond_c
    const-string v26, "page_featured_admin_info"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_d

    .line 1511955
    invoke-static/range {p0 .. p1}, LX/9Zn;->a(LX/15w;LX/186;)I

    move-result v13

    goto/16 :goto_1

    .line 1511956
    :cond_d
    const-string v26, "page_info_sections"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_e

    .line 1511957
    invoke-static/range {p0 .. p1}, LX/9Zo;->a(LX/15w;LX/186;)I

    move-result v12

    goto/16 :goto_1

    .line 1511958
    :cond_e
    const-string v26, "page_payment_options"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_f

    .line 1511959
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 1511960
    :cond_f
    const-string v26, "permanently_closed_status"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_10

    .line 1511961
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v10

    goto/16 :goto_1

    .line 1511962
    :cond_10
    const-string v26, "place_type"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_11

    .line 1511963
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/facebook/graphql/enums/GraphQLPlaceType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    goto/16 :goto_1

    .line 1511964
    :cond_11
    const-string v26, "viewer_profile_permissions"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_12

    .line 1511965
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v8

    goto/16 :goto_1

    .line 1511966
    :cond_12
    const-string v26, "websites"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_0

    .line 1511967
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v7

    goto/16 :goto_1

    .line 1511968
    :cond_13
    const/16 v25, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1511969
    const/16 v25, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v25

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1511970
    const/16 v24, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v24

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1511971
    const/16 v23, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v23

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1511972
    if-eqz v6, :cond_14

    .line 1511973
    const/4 v6, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 1511974
    :cond_14
    const/4 v6, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 1511975
    const/4 v6, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 1511976
    if-eqz v5, :cond_15

    .line 1511977
    const/4 v5, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 1511978
    :cond_15
    if-eqz v4, :cond_16

    .line 1511979
    const/4 v4, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 1511980
    :cond_16
    if-eqz v3, :cond_17

    .line 1511981
    const/16 v3, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->a(IZ)V

    .line 1511982
    :cond_17
    const/16 v3, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->b(II)V

    .line 1511983
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->b(II)V

    .line 1511984
    const/16 v3, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 1511985
    const/16 v3, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 1511986
    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 1511987
    const/16 v3, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 1511988
    const/16 v3, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 1511989
    const/16 v3, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 1511990
    const/16 v3, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 1511991
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method
