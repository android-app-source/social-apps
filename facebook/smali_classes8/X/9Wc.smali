.class public LX/9Wc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/9Wc;


# instance fields
.field public final a:LX/0Zb;

.field private final b:LX/01T;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Zb;LX/01T;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "LX/01T;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1501434
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1501435
    iput-object p1, p0, LX/9Wc;->a:LX/0Zb;

    .line 1501436
    iput-object p2, p0, LX/9Wc;->b:LX/01T;

    .line 1501437
    iput-object p3, p0, LX/9Wc;->c:LX/0Ot;

    .line 1501438
    return-void
.end method

.method public static a(LX/0QB;)LX/9Wc;
    .locals 6

    .prologue
    .line 1501421
    sget-object v0, LX/9Wc;->d:LX/9Wc;

    if-nez v0, :cond_1

    .line 1501422
    const-class v1, LX/9Wc;

    monitor-enter v1

    .line 1501423
    :try_start_0
    sget-object v0, LX/9Wc;->d:LX/9Wc;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1501424
    if-eqz v2, :cond_0

    .line 1501425
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1501426
    new-instance v5, LX/9Wc;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v4

    check-cast v4, LX/01T;

    const/16 p0, 0x259

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v5, v3, v4, p0}, LX/9Wc;-><init>(LX/0Zb;LX/01T;LX/0Ot;)V

    .line 1501427
    move-object v0, v5

    .line 1501428
    sput-object v0, LX/9Wc;->d:LX/9Wc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1501429
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1501430
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1501431
    :cond_1
    sget-object v0, LX/9Wc;->d:LX/9Wc;

    return-object v0

    .line 1501432
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1501433
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/9Wb;J)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 3

    .prologue
    .line 1501405
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-virtual {p0}, LX/9Wb;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "pages_public_view"

    .line 1501406
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1501407
    move-object v0, v0

    .line 1501408
    const-string v1, "page_id"

    invoke-virtual {v0, v1, p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/9Wd;)V
    .locals 6

    .prologue
    .line 1501409
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1501410
    iget-object v0, p0, LX/9Wc;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "PagesBanUserAnalytics"

    const-string v2, "Fail to log banning user actions in reactors list: page id is null."

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1501411
    :goto_0
    return-void

    .line 1501412
    :cond_0
    iget-object v0, p0, LX/9Wc;->b:LX/01T;

    sget-object v1, LX/01T;->PAA:LX/01T;

    if-ne v0, v1, :cond_1

    .line 1501413
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 1501414
    iget-object v2, p0, LX/9Wc;->a:LX/0Zb;

    sget-object v3, LX/9Wb;->PMA_BAN_USER_ACTIONS:LX/9Wb;

    invoke-static {v3, v0, v1}, LX/9Wc;->a(LX/9Wb;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "referrer"

    invoke-virtual {p2}, LX/9Wd;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1501415
    goto :goto_0

    .line 1501416
    :cond_1
    iget-object v0, p0, LX/9Wc;->b:LX/01T;

    sget-object v1, LX/01T;->FB4A:LX/01T;

    if-ne v0, v1, :cond_2

    .line 1501417
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 1501418
    iget-object v2, p0, LX/9Wc;->a:LX/0Zb;

    sget-object v3, LX/9Wb;->FB4A_BAN_USER_ACTIONS:LX/9Wb;

    invoke-static {v3, v0, v1}, LX/9Wc;->a(LX/9Wb;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "referrer"

    invoke-virtual {p2}, LX/9Wd;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1501419
    goto :goto_0

    .line 1501420
    :cond_2
    iget-object v0, p0, LX/9Wc;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "PagesBanUserAnalytics"

    const-string v2, "Fail to log banning user actions in reactors list: not in PMA or FB4A."

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
