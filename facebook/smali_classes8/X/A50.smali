.class public final LX/A50;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 57

    .prologue
    .line 1620240
    const-wide/16 v50, 0x0

    .line 1620241
    const/16 v48, 0x0

    .line 1620242
    const/16 v45, 0x0

    .line 1620243
    const-wide/16 v46, 0x0

    .line 1620244
    const/16 v44, 0x0

    .line 1620245
    const/16 v43, 0x0

    .line 1620246
    const/16 v42, 0x0

    .line 1620247
    const/16 v41, 0x0

    .line 1620248
    const/16 v40, 0x0

    .line 1620249
    const/16 v39, 0x0

    .line 1620250
    const/16 v38, 0x0

    .line 1620251
    const/16 v37, 0x0

    .line 1620252
    const/16 v36, 0x0

    .line 1620253
    const/16 v35, 0x0

    .line 1620254
    const/16 v34, 0x0

    .line 1620255
    const-wide/16 v32, 0x0

    .line 1620256
    const/16 v31, 0x0

    .line 1620257
    const/16 v30, 0x0

    .line 1620258
    const/16 v29, 0x0

    .line 1620259
    const/16 v28, 0x0

    .line 1620260
    const-wide/16 v26, 0x0

    .line 1620261
    const-wide/16 v24, 0x0

    .line 1620262
    const/16 v23, 0x0

    .line 1620263
    const/16 v22, 0x0

    .line 1620264
    const/16 v21, 0x0

    .line 1620265
    const/16 v20, 0x0

    .line 1620266
    const/16 v19, 0x0

    .line 1620267
    const/16 v18, 0x0

    .line 1620268
    const/16 v17, 0x0

    .line 1620269
    const/16 v16, 0x0

    .line 1620270
    const/4 v15, 0x0

    .line 1620271
    const/4 v14, 0x0

    .line 1620272
    const/4 v13, 0x0

    .line 1620273
    const/4 v12, 0x0

    .line 1620274
    const/4 v11, 0x0

    .line 1620275
    const/4 v10, 0x0

    .line 1620276
    const/4 v9, 0x0

    .line 1620277
    const/4 v8, 0x0

    .line 1620278
    const/4 v7, 0x0

    .line 1620279
    const/4 v6, 0x0

    .line 1620280
    const/4 v5, 0x0

    .line 1620281
    const/4 v4, 0x0

    .line 1620282
    const/4 v3, 0x0

    .line 1620283
    const/4 v2, 0x0

    .line 1620284
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v49

    sget-object v52, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v49

    move-object/from16 v1, v52

    if-eq v0, v1, :cond_2e

    .line 1620285
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1620286
    const/4 v2, 0x0

    .line 1620287
    :goto_0
    return v2

    .line 1620288
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_1b

    .line 1620289
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1620290
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1620291
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 1620292
    const-string v6, "created_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1620293
    const/4 v2, 0x1

    .line 1620294
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 1620295
    :cond_1
    const-string v6, "creation_story"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1620296
    invoke-static/range {p0 .. p1}, LX/2aD;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v53, v2

    goto :goto_1

    .line 1620297
    :cond_2
    const-string v6, "enable_focus"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1620298
    const/4 v2, 0x1

    .line 1620299
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move/from16 v24, v2

    move/from16 v52, v6

    goto :goto_1

    .line 1620300
    :cond_3
    const-string v6, "focus_width_degrees"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1620301
    const/4 v2, 0x1

    .line 1620302
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move/from16 v23, v2

    move-wide/from16 v50, v6

    goto :goto_1

    .line 1620303
    :cond_4
    const-string v6, "guided_tour"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1620304
    invoke-static/range {p0 .. p1}, LX/5CB;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v49, v2

    goto :goto_1

    .line 1620305
    :cond_5
    const-string v6, "has_viewer_watched_video"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1620306
    const/4 v2, 0x1

    .line 1620307
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move/from16 v22, v2

    move/from16 v48, v6

    goto :goto_1

    .line 1620308
    :cond_6
    const-string v6, "height"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1620309
    const/4 v2, 0x1

    .line 1620310
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move/from16 v21, v2

    move/from16 v47, v6

    goto/16 :goto_1

    .line 1620311
    :cond_7
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1620312
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v46, v2

    goto/16 :goto_1

    .line 1620313
    :cond_8
    const-string v6, "initial_view_heading_degrees"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 1620314
    const/4 v2, 0x1

    .line 1620315
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move/from16 v20, v2

    move/from16 v45, v6

    goto/16 :goto_1

    .line 1620316
    :cond_9
    const-string v6, "initial_view_pitch_degrees"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1620317
    const/4 v2, 0x1

    .line 1620318
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move/from16 v19, v2

    move/from16 v44, v6

    goto/16 :goto_1

    .line 1620319
    :cond_a
    const-string v6, "initial_view_roll_degrees"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 1620320
    const/4 v2, 0x1

    .line 1620321
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move/from16 v18, v2

    move/from16 v43, v6

    goto/16 :goto_1

    .line 1620322
    :cond_b
    const-string v6, "is_live_streaming"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 1620323
    const/4 v2, 0x1

    .line 1620324
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move/from16 v17, v2

    move/from16 v42, v6

    goto/16 :goto_1

    .line 1620325
    :cond_c
    const-string v6, "is_spherical"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 1620326
    const/4 v2, 0x1

    .line 1620327
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move/from16 v16, v2

    move/from16 v41, v6

    goto/16 :goto_1

    .line 1620328
    :cond_d
    const-string v6, "live_viewer_count_read_only"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 1620329
    const/4 v2, 0x1

    .line 1620330
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v15, v2

    move/from16 v40, v6

    goto/16 :goto_1

    .line 1620331
    :cond_e
    const-string v6, "message"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 1620332
    invoke-static/range {p0 .. p1}, LX/4ap;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v37, v2

    goto/16 :goto_1

    .line 1620333
    :cond_f
    const-string v6, "off_focus_level"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 1620334
    const/4 v2, 0x1

    .line 1620335
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v14, v2

    move-wide/from16 v38, v6

    goto/16 :goto_1

    .line 1620336
    :cond_10
    const-string v6, "owner"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_11

    .line 1620337
    invoke-static/range {p0 .. p1}, LX/A4z;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v36, v2

    goto/16 :goto_1

    .line 1620338
    :cond_11
    const-string v6, "play_count"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_12

    .line 1620339
    const/4 v2, 0x1

    .line 1620340
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v13, v2

    move/from16 v35, v6

    goto/16 :goto_1

    .line 1620341
    :cond_12
    const-string v6, "playable_duration"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_13

    .line 1620342
    const/4 v2, 0x1

    .line 1620343
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v12, v2

    move/from16 v34, v6

    goto/16 :goto_1

    .line 1620344
    :cond_13
    const-string v6, "projection_type"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_14

    .line 1620345
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v29, v2

    goto/16 :goto_1

    .line 1620346
    :cond_14
    const-string v6, "sphericalFullscreenAspectRatio"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_15

    .line 1620347
    const/4 v2, 0x1

    .line 1620348
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v11, v2

    move-wide/from16 v32, v6

    goto/16 :goto_1

    .line 1620349
    :cond_15
    const-string v6, "sphericalInlineAspectRatio"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_16

    .line 1620350
    const/4 v2, 0x1

    .line 1620351
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v10, v2

    move-wide/from16 v30, v6

    goto/16 :goto_1

    .line 1620352
    :cond_16
    const-string v6, "sphericalPlayableUrlHdString"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_17

    .line 1620353
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v28, v2

    goto/16 :goto_1

    .line 1620354
    :cond_17
    const-string v6, "sphericalPlayableUrlSdString"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_18

    .line 1620355
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v27, v2

    goto/16 :goto_1

    .line 1620356
    :cond_18
    const-string v6, "sphericalPreferredFov"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_19

    .line 1620357
    const/4 v2, 0x1

    .line 1620358
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v9, v2

    move/from16 v26, v6

    goto/16 :goto_1

    .line 1620359
    :cond_19
    const-string v6, "width"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 1620360
    const/4 v2, 0x1

    .line 1620361
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v8, v2

    move/from16 v25, v6

    goto/16 :goto_1

    .line 1620362
    :cond_1a
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1620363
    :cond_1b
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1620364
    if-eqz v3, :cond_1c

    .line 1620365
    const/4 v3, 0x0

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1620366
    :cond_1c
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v53

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1620367
    if-eqz v24, :cond_1d

    .line 1620368
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v52

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1620369
    :cond_1d
    if-eqz v23, :cond_1e

    .line 1620370
    const/4 v3, 0x3

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v50

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1620371
    :cond_1e
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v49

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1620372
    if-eqz v22, :cond_1f

    .line 1620373
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v48

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1620374
    :cond_1f
    if-eqz v21, :cond_20

    .line 1620375
    const/4 v2, 0x6

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 1620376
    :cond_20
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1620377
    if-eqz v20, :cond_21

    .line 1620378
    const/16 v2, 0x8

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 1620379
    :cond_21
    if-eqz v19, :cond_22

    .line 1620380
    const/16 v2, 0x9

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 1620381
    :cond_22
    if-eqz v18, :cond_23

    .line 1620382
    const/16 v2, 0xa

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 1620383
    :cond_23
    if-eqz v17, :cond_24

    .line 1620384
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1620385
    :cond_24
    if-eqz v16, :cond_25

    .line 1620386
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1620387
    :cond_25
    if-eqz v15, :cond_26

    .line 1620388
    const/16 v2, 0xd

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 1620389
    :cond_26
    const/16 v2, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1620390
    if-eqz v14, :cond_27

    .line 1620391
    const/16 v3, 0xf

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v38

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1620392
    :cond_27
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1620393
    if-eqz v13, :cond_28

    .line 1620394
    const/16 v2, 0x11

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 1620395
    :cond_28
    if-eqz v12, :cond_29

    .line 1620396
    const/16 v2, 0x12

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 1620397
    :cond_29
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1620398
    if-eqz v11, :cond_2a

    .line 1620399
    const/16 v3, 0x14

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v32

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1620400
    :cond_2a
    if-eqz v10, :cond_2b

    .line 1620401
    const/16 v3, 0x15

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v30

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1620402
    :cond_2b
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1620403
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1620404
    if-eqz v9, :cond_2c

    .line 1620405
    const/16 v2, 0x18

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 1620406
    :cond_2c
    if-eqz v8, :cond_2d

    .line 1620407
    const/16 v2, 0x19

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 1620408
    :cond_2d
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_2e
    move/from16 v49, v44

    move/from16 v52, v45

    move/from16 v53, v48

    move/from16 v44, v39

    move/from16 v45, v40

    move/from16 v48, v43

    move/from16 v40, v35

    move/from16 v43, v38

    move/from16 v35, v30

    move-wide/from16 v38, v32

    move-wide/from16 v32, v26

    move/from16 v26, v21

    move/from16 v27, v22

    move/from16 v21, v15

    move/from16 v22, v16

    move/from16 v16, v10

    move v15, v9

    move v9, v3

    move v10, v4

    move/from16 v3, v19

    move/from16 v19, v13

    move v13, v7

    move-wide/from16 v54, v24

    move/from16 v24, v18

    move/from16 v25, v20

    move/from16 v20, v14

    move/from16 v18, v12

    move v14, v8

    move v12, v6

    move v8, v2

    move/from16 v56, v34

    move/from16 v34, v29

    move/from16 v29, v28

    move/from16 v28, v23

    move/from16 v23, v17

    move/from16 v17, v11

    move v11, v5

    move-wide/from16 v4, v50

    move-wide/from16 v50, v46

    move/from16 v47, v42

    move/from16 v46, v41

    move/from16 v41, v36

    move/from16 v42, v37

    move/from16 v37, v56

    move/from16 v36, v31

    move-wide/from16 v30, v54

    goto/16 :goto_1
.end method
