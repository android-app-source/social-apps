.class public LX/9CQ;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/9CP;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1454067
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1454068
    return-void
.end method


# virtual methods
.method public final a(LX/9FA;LX/9D1;LX/9Ce;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;Z)LX/9CP;
    .locals 7

    .prologue
    .line 1454069
    new-instance v0, LX/9CP;

    const-class v1, LX/9FH;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/9FH;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, LX/9CP;-><init>(LX/9FA;LX/9D1;LX/9Ce;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;ZLX/9FH;)V

    .line 1454070
    const-class v1, LX/3iG;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/3iG;

    const/16 v2, 0x3741

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x1d9c

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x79a

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x259

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    .line 1454071
    iput-object v1, v0, LX/9CP;->b:LX/3iG;

    iput-object v2, v0, LX/9CP;->c:LX/0Ot;

    iput-object v3, v0, LX/9CP;->d:LX/0Ot;

    iput-object v4, v0, LX/9CP;->e:LX/0Ot;

    iput-object v5, v0, LX/9CP;->f:LX/0Ot;

    .line 1454072
    return-object v0
.end method
