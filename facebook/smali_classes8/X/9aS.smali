.class public LX/9aS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9aM;


# static fields
.field private static final a:LX/9aT;


# instance fields
.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/9aT;

.field public d:LX/9aT;

.field public e:LX/9aT;

.field public f:Z

.field public g:LX/9aT;

.field public h:LX/9aT;

.field public i:LX/9aT;

.field public j:LX/9aT;

.field public k:J

.field public l:LX/9aT;

.field private m:J


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1513792
    new-instance v0, LX/9aV;

    const v1, -0x41333333    # -0.4f

    const v2, 0x3ecccccd    # 0.4f

    invoke-direct {v0, v1, v2}, LX/9aV;-><init>(FF)V

    sput-object v0, LX/9aS;->a:LX/9aT;

    return-void
.end method

.method public constructor <init>(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 1513790
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, LX/9aS;-><init>(Ljava/util/List;)V

    .line 1513791
    return-void
.end method

.method private constructor <init>(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1513778
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1513779
    sget-object v0, LX/9aS;->a:LX/9aT;

    iput-object v0, p0, LX/9aS;->c:LX/9aT;

    .line 1513780
    sget-object v0, LX/9aU;->a:LX/9aU;

    iput-object v0, p0, LX/9aS;->d:LX/9aT;

    .line 1513781
    sget-object v0, LX/9aU;->a:LX/9aU;

    iput-object v0, p0, LX/9aS;->e:LX/9aT;

    .line 1513782
    sget-object v0, LX/9aU;->a:LX/9aU;

    iput-object v0, p0, LX/9aS;->g:LX/9aT;

    .line 1513783
    sget-object v0, LX/9aU;->a:LX/9aU;

    iput-object v0, p0, LX/9aS;->h:LX/9aT;

    .line 1513784
    sget-object v0, LX/9aU;->a:LX/9aU;

    iput-object v0, p0, LX/9aS;->i:LX/9aT;

    .line 1513785
    sget-object v0, LX/9aU;->b:LX/9aU;

    iput-object v0, p0, LX/9aS;->j:LX/9aT;

    .line 1513786
    sget-object v0, LX/9aU;->a:LX/9aU;

    iput-object v0, p0, LX/9aS;->l:LX/9aT;

    .line 1513787
    const-wide/16 v0, 0x32

    iput-wide v0, p0, LX/9aS;->m:J

    .line 1513788
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/9aS;->b:Ljava/util/List;

    .line 1513789
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 1513793
    iget-wide v0, p0, LX/9aS;->m:J

    return-wide v0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 1513777
    iget-object v0, p0, LX/9aS;->b:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    return v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 1513776
    iget-object v0, p0, LX/9aS;->b:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    return v0
.end method
