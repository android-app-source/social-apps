.class public LX/9kz;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/9l2;

.field private final b:Ljava/util/concurrent/ScheduledExecutorService;

.field private final c:LX/0SG;

.field public final d:Ljava/lang/String;

.field private final e:Ljava/lang/Runnable;

.field private final f:Ljava/lang/Runnable;

.field private g:Ljava/util/concurrent/ScheduledFuture;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private h:Ljava/util/concurrent/ScheduledFuture;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private i:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/9l2;Ljava/util/concurrent/ScheduledExecutorService;LX/0SG;Ljava/lang/String;)V
    .locals 1
    .param p1    # LX/9l2;
        .annotation runtime Lcom/facebook/presence/annotations/FeedbackComments;
        .end annotation
    .end param
    .param p2    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1532334
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1532335
    iput-object p1, p0, LX/9kz;->a:LX/9l2;

    .line 1532336
    iput-object p2, p0, LX/9kz;->b:Ljava/util/concurrent/ScheduledExecutorService;

    .line 1532337
    iput-object p3, p0, LX/9kz;->c:LX/0SG;

    .line 1532338
    iput-object p4, p0, LX/9kz;->d:Ljava/lang/String;

    .line 1532339
    new-instance v0, Lcom/facebook/presence/CommentTypingContext$1;

    invoke-direct {v0, p0}, Lcom/facebook/presence/CommentTypingContext$1;-><init>(LX/9kz;)V

    iput-object v0, p0, LX/9kz;->e:Ljava/lang/Runnable;

    .line 1532340
    new-instance v0, Lcom/facebook/presence/CommentTypingContext$2;

    invoke-direct {v0, p0}, Lcom/facebook/presence/CommentTypingContext$2;-><init>(LX/9kz;)V

    iput-object v0, p0, LX/9kz;->f:Ljava/lang/Runnable;

    .line 1532341
    return-void
.end method

.method private static d(LX/9kz;)Z
    .locals 1

    .prologue
    .line 1532374
    iget-object v0, p0, LX/9kz;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(LX/9kz;)V
    .locals 3

    .prologue
    .line 1532368
    monitor-enter p0

    .line 1532369
    :try_start_0
    iget-object v0, p0, LX/9kz;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/9kz;->i:J

    .line 1532370
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1532371
    iget-object v0, p0, LX/9kz;->a:LX/9l2;

    iget-object v1, p0, LX/9kz;->d:Ljava/lang/String;

    sget-object v2, LX/9lB;->ACTIVE:LX/9lB;

    invoke-interface {v0, v1, v2}, LX/9l2;->a(Ljava/lang/String;LX/9lB;)V

    .line 1532372
    return-void

    .line 1532373
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static f(LX/9kz;)V
    .locals 3

    .prologue
    .line 1532360
    monitor-enter p0

    .line 1532361
    const-wide/16 v0, 0x0

    :try_start_0
    iput-wide v0, p0, LX/9kz;->i:J

    .line 1532362
    iget-object v0, p0, LX/9kz;->g:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    .line 1532363
    iget-object v0, p0, LX/9kz;->g:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 1532364
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1532365
    iget-object v0, p0, LX/9kz;->a:LX/9l2;

    iget-object v1, p0, LX/9kz;->d:Ljava/lang/String;

    sget-object v2, LX/9lB;->INACTIVE:LX/9lB;

    invoke-interface {v0, v1, v2}, LX/9l2;->a(Ljava/lang/String;LX/9lB;)V

    .line 1532366
    return-void

    .line 1532367
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized b()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x2710

    .line 1532350
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/9kz;->d(LX/9kz;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 1532351
    :goto_0
    monitor-exit p0

    return-void

    .line 1532352
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/9kz;->g:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/9kz;->g:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v0}, Ljava/util/concurrent/ScheduledFuture;->isDone()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1532353
    :cond_1
    iget-object v0, p0, LX/9kz;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, LX/9kz;->i:J

    sub-long/2addr v0, v2

    .line 1532354
    const-wide/16 v2, 0x0

    sub-long v0, v4, v0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 1532355
    iget-object v2, p0, LX/9kz;->b:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v3, p0, LX/9kz;->e:Ljava/lang/Runnable;

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v3, v0, v1, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/9kz;->g:Ljava/util/concurrent/ScheduledFuture;

    .line 1532356
    :cond_2
    iget-object v0, p0, LX/9kz;->h:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_3

    .line 1532357
    iget-object v0, p0, LX/9kz;->h:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 1532358
    :cond_3
    iget-object v0, p0, LX/9kz;->b:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v1, p0, LX/9kz;->f:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2710

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/9kz;->h:Ljava/util/concurrent/ScheduledFuture;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1532359
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 5

    .prologue
    .line 1532342
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/9kz;->d(LX/9kz;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 1532343
    :goto_0
    monitor-exit p0

    return-void

    .line 1532344
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/9kz;->g:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_1

    .line 1532345
    iget-object v0, p0, LX/9kz;->g:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 1532346
    :cond_1
    iget-object v0, p0, LX/9kz;->h:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_2

    .line 1532347
    iget-object v0, p0, LX/9kz;->h:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 1532348
    :cond_2
    iget-object v0, p0, LX/9kz;->b:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v1, p0, LX/9kz;->f:Ljava/lang/Runnable;

    const-wide/16 v2, 0x0

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/9kz;->h:Ljava/util/concurrent/ScheduledFuture;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1532349
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
