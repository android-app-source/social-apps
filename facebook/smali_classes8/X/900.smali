.class public final LX/900;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8zz;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;)V
    .locals 0

    .prologue
    .line 1428362
    iput-object p1, p0, LX/900;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1428363
    iget-object v0, p0, LX/900;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->h:LX/90G;

    invoke-virtual {v0}, LX/90G;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1428364
    iget-object v0, p0, LX/900;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->r:LX/91D;

    iget-object v1, p0, LX/900;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;

    invoke-static {v1}, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->l(Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;)Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    move-result-object v1

    .line 1428365
    iget-object v2, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v1, v2

    .line 1428366
    iget-object v2, p0, LX/900;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;

    iget-object v2, v2, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->c:LX/91v;

    .line 1428367
    iget-object v3, v2, LX/91v;->g:LX/5LG;

    move-object v2, v3

    .line 1428368
    invoke-interface {v2}, LX/5LG;->l()Ljava/lang/String;

    move-result-object v2

    .line 1428369
    const-string v3, "feeling_selector_first_keystroke"

    invoke-static {v3, v1}, LX/91D;->b(Ljava/lang/String;Ljava/lang/String;)LX/8yQ;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/8yQ;->a(Ljava/lang/String;)LX/8yQ;

    move-result-object v3

    .line 1428370
    iget-object v4, v3, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v3, v4

    .line 1428371
    iget-object v4, v0, LX/91D;->a:LX/0Zb;

    invoke-interface {v4, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1428372
    :cond_0
    iget-object v0, p0, LX/900;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->h:LX/90G;

    invoke-virtual {v0}, LX/90G;->a()V

    .line 1428373
    iget-object v0, p0, LX/900;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->c:LX/91v;

    invoke-virtual {v0, p1}, LX/91v;->a(Ljava/lang/String;)V

    .line 1428374
    return-void
.end method
