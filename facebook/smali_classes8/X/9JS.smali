.class public LX/9JS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field public final a:Ljava/lang/String;

.field private b:Ljava/io/File;

.field public c:LX/9JP;

.field public final d:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/9JQ;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<[I>;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LX/0w5;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/0w5;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private i:I

.field public final j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/graphql/cursor/edgestore/PageInfo;",
            ">;"
        }
    .end annotation
.end field

.field public k:I

.field public l:I

.field private m:I

.field public final n:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 1464885
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1464886
    iput-object v0, p0, LX/9JS;->b:Ljava/io/File;

    .line 1464887
    iput-object v0, p0, LX/9JS;->c:LX/9JP;

    .line 1464888
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/9JS;->d:Landroid/util/SparseArray;

    .line 1464889
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/9JS;->e:Landroid/util/SparseArray;

    .line 1464890
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/9JS;->f:Ljava/util/Map;

    .line 1464891
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/9JS;->g:Ljava/util/Map;

    .line 1464892
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/9JS;->h:Ljava/util/Map;

    .line 1464893
    iput v1, p0, LX/9JS;->i:I

    .line 1464894
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/9JS;->j:Ljava/util/ArrayList;

    .line 1464895
    const/4 v0, -0x1

    iput v0, p0, LX/9JS;->k:I

    .line 1464896
    iput v1, p0, LX/9JS;->l:I

    .line 1464897
    const/4 v0, 0x1

    iput v0, p0, LX/9JS;->m:I

    .line 1464898
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, LX/9JS;->n:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 1464899
    iput-object p1, p0, LX/9JS;->a:Ljava/lang/String;

    .line 1464900
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/io/File;)LX/9JS;
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 1464901
    new-instance v1, LX/9JS;

    invoke-direct {v1, p0}, LX/9JS;-><init>(Ljava/lang/String;)V

    .line 1464902
    invoke-virtual {p1}, Ljava/io/File;->isFile()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1464903
    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->isFile()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1464904
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 1464905
    new-instance v2, LX/9JN;

    new-instance v5, LX/9JM;

    invoke-virtual {v0}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    invoke-direct {v5, p1, v0}, LX/9JM;-><init>(Ljava/io/File;Ljava/nio/channels/FileChannel;)V

    invoke-direct {v2, v5}, LX/9JN;-><init>(LX/9JM;)V

    move-object v5, v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 1464906
    const/4 v0, 0x1

    .line 1464907
    :cond_0
    :try_start_1
    invoke-virtual {v5}, LX/9JN;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1464908
    iget-object v2, v5, LX/9JN;->b:Ljava/nio/ByteBuffer;

    move-object v2, v2

    .line 1464909
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1464910
    :try_start_2
    invoke-static {v2}, LX/9JT;->a(Ljava/nio/ByteBuffer;)LX/9JT;
    :try_end_2
    .catch Ljava/nio/BufferUnderflowException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v2

    .line 1464911
    :try_start_3
    invoke-static {v1, v2}, LX/9JS;->c(LX/9JS;LX/9JT;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v4

    .line 1464912
    :cond_1
    :goto_0
    if-eqz v0, :cond_7

    .line 1464913
    invoke-static {v5}, LX/9JP;->a(LX/9JN;)LX/9JP;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v0

    .line 1464914
    :goto_1
    if-eqz v5, :cond_2

    :try_start_4
    invoke-virtual {v5}, LX/9JN;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 1464915
    :cond_2
    :goto_2
    if-nez v0, :cond_3

    .line 1464916
    new-instance v1, LX/9JS;

    invoke-direct {v1, p0}, LX/9JS;-><init>(Ljava/lang/String;)V

    .line 1464917
    invoke-static {p1}, LX/9JP;->a(Ljava/io/File;)LX/9JP;

    move-result-object v0

    .line 1464918
    :cond_3
    iput-object p1, v1, LX/9JS;->b:Ljava/io/File;

    .line 1464919
    iput-object v0, v1, LX/9JS;->c:LX/9JP;

    .line 1464920
    return-object v1

    .line 1464921
    :catch_0
    move v0, v4

    .line 1464922
    goto :goto_0

    .line 1464923
    :catch_1
    move-exception v0

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1464924
    :catchall_0
    move-exception v2

    move-object v6, v2

    move-object v2, v0

    move-object v0, v6

    :goto_3
    if-eqz v5, :cond_4

    if-eqz v2, :cond_6

    :try_start_6
    invoke-virtual {v5}, LX/9JN;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    :cond_4
    :goto_4
    :try_start_7
    throw v0
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    :catch_2
    move-exception v0

    .line 1464925
    const-string v2, "SimpleEdgeStore"

    const-string v5, "Unable to recover session. Starting with an empty session"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v0, v5, v4}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_5
    move-object v0, v3

    goto :goto_2

    .line 1464926
    :catch_3
    move-exception v5

    :try_start_8
    invoke-static {v2, v5}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_6
    invoke-virtual {v5}, LX/9JN;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2

    goto :goto_4

    :catchall_1
    move-exception v0

    move-object v2, v3

    goto :goto_3

    :cond_7
    move-object v0, v3

    goto :goto_1
.end method

.method private static a(LX/9JS;ILX/0w5;)V
    .locals 2

    .prologue
    .line 1464927
    iget-object v0, p0, LX/9JS;->h:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1464928
    iget-object v0, p0, LX/9JS;->g:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1464929
    iget v0, p0, LX/9JS;->i:I

    if-gt v0, p1, :cond_0

    .line 1464930
    add-int/lit8 v0, p1, 0x1

    iput v0, p0, LX/9JS;->i:I

    .line 1464931
    :cond_0
    return-void
.end method

.method private static a(LX/9JS;LX/9JQ;)V
    .locals 14

    .prologue
    .line 1464932
    iget-object v0, p0, LX/9JS;->c:LX/9JP;

    if-eqz v0, :cond_1

    .line 1464933
    iget-object v0, p0, LX/9JS;->h:Ljava/util/Map;

    .line 1464934
    iget-object v1, p1, LX/9JQ;->e:LX/0w5;

    move-object v1, v1

    .line 1464935
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1464936
    if-nez v0, :cond_0

    .line 1464937
    iget v0, p0, LX/9JS;->i:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 1464938
    iget v1, p0, LX/9JS;->i:I

    .line 1464939
    iget-object v2, p1, LX/9JQ;->e:LX/0w5;

    move-object v2, v2

    .line 1464940
    invoke-virtual {v2}, LX/0w5;->e()[B

    move-result-object v2

    invoke-static {v1, v2}, LX/9JT;->c(I[B)LX/9JT;

    move-result-object v1

    invoke-static {p0, v1}, LX/9JS;->b(LX/9JS;LX/9JT;)V

    .line 1464941
    iget v1, p0, LX/9JS;->i:I

    .line 1464942
    iget-object v2, p1, LX/9JQ;->e:LX/0w5;

    move-object v2, v2

    .line 1464943
    invoke-static {p0, v1, v2}, LX/9JS;->a(LX/9JS;ILX/0w5;)V

    .line 1464944
    :cond_0
    iget v1, p1, LX/9JQ;->d:I

    move v1, v1

    .line 1464945
    invoke-virtual {p1}, LX/9JQ;->c()I

    move-result v2

    invoke-virtual {p1}, LX/9JQ;->e()I

    move-result v3

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 1464946
    iget v0, p1, LX/9JQ;->f:I

    move v5, v0

    .line 1464947
    iget-wide v12, p1, LX/9JQ;->g:J

    move-wide v6, v12

    .line 1464948
    iget-wide v12, p1, LX/9JQ;->h:J

    move-wide v8, v12

    .line 1464949
    iget-object v0, p1, LX/9JQ;->i:[I

    move-object v10, v0

    .line 1464950
    iget-object v0, p1, LX/9JQ;->j:Ljava/lang/String;

    move-object v11, v0

    .line 1464951
    invoke-static/range {v1 .. v11}, LX/9JT;->a(IIIIIJJ[ILjava/lang/String;)LX/9JT;

    move-result-object v0

    invoke-static {p0, v0}, LX/9JS;->b(LX/9JS;LX/9JT;)V

    .line 1464952
    :cond_1
    invoke-static {p0, p1}, LX/9JS;->b(LX/9JS;LX/9JQ;)V

    .line 1464953
    return-void
.end method

.method private static b(LX/9JS;LX/9JQ;)V
    .locals 10

    .prologue
    .line 1464954
    iget v0, p1, LX/9JQ;->d:I

    move v0, v0

    .line 1464955
    iget v1, p0, LX/9JS;->m:I

    if-lt v0, v1, :cond_0

    .line 1464956
    iget v0, p1, LX/9JQ;->d:I

    move v0, v0

    .line 1464957
    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/9JS;->m:I

    .line 1464958
    :cond_0
    iget-object v0, p0, LX/9JS;->d:Landroid/util/SparseArray;

    .line 1464959
    iget v1, p1, LX/9JQ;->d:I

    move v1, v1

    .line 1464960
    invoke-virtual {v0, v1, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1464961
    iget-object v0, p1, LX/9JQ;->i:[I

    move-object v0, v0

    .line 1464962
    if-eqz v0, :cond_1

    .line 1464963
    iget-object v0, p1, LX/9JQ;->i:[I

    move-object v1, v0

    .line 1464964
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget v3, v1, v0

    .line 1464965
    iget v4, p1, LX/9JQ;->d:I

    move v4, v4

    .line 1464966
    const/4 v8, -0x1

    const/4 v7, 0x0

    .line 1464967
    iget-object v5, p0, LX/9JS;->e:Landroid/util/SparseArray;

    invoke-virtual {v5, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [I

    .line 1464968
    if-nez v5, :cond_3

    .line 1464969
    iget-object v5, p0, LX/9JS;->e:Landroid/util/SparseArray;

    const/4 v6, 0x1

    new-array v6, v6, [I

    aput v4, v6, v7

    invoke-virtual {v5, v3, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1464970
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1464971
    :cond_1
    iget-object v0, p1, LX/9JQ;->j:Ljava/lang/String;

    move-object v0, v0

    .line 1464972
    if-eqz v0, :cond_2

    .line 1464973
    iget-object v0, p0, LX/9JS;->f:Ljava/util/Map;

    .line 1464974
    iget-object v1, p1, LX/9JQ;->j:Ljava/lang/String;

    move-object v1, v1

    .line 1464975
    iget v2, p1, LX/9JQ;->d:I

    move v2, v2

    .line 1464976
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1464977
    :cond_2
    return-void

    :cond_3
    move v6, v7

    .line 1464978
    :goto_2
    array-length v9, v5

    if-ge v6, v9, :cond_5

    .line 1464979
    aget v9, v5, v6

    if-ne v9, v8, :cond_4

    .line 1464980
    aput v4, v5, v6

    goto :goto_1

    .line 1464981
    :cond_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 1464982
    :cond_5
    array-length v6, v5

    mul-int/lit8 v6, v6, 0x2

    new-array v9, v6, [I

    .line 1464983
    :goto_3
    array-length v6, v9

    if-ge v7, v6, :cond_7

    .line 1464984
    array-length v6, v5

    if-ge v7, v6, :cond_6

    aget v6, v5, v7

    :goto_4
    aput v6, v9, v7

    .line 1464985
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    :cond_6
    move v6, v8

    .line 1464986
    goto :goto_4

    .line 1464987
    :cond_7
    array-length v5, v5

    aput v4, v9, v5

    .line 1464988
    iget-object v5, p0, LX/9JS;->e:Landroid/util/SparseArray;

    invoke-virtual {v5, v3, v9}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_1
.end method

.method public static b(LX/9JS;LX/9JT;)V
    .locals 2

    .prologue
    .line 1464989
    iget-object v0, p0, LX/9JS;->c:LX/9JP;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1464990
    invoke-virtual {p1}, LX/9JT;->o()Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 1464991
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 1464992
    iget-object v1, p0, LX/9JS;->c:LX/9JP;

    invoke-virtual {v1, v0}, LX/9JP;->a(Ljava/nio/ByteBuffer;)V

    .line 1464993
    return-void

    .line 1464994
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(LX/9JS;Lcom/facebook/graphql/cursor/edgestore/PageInfo;)V
    .locals 14

    .prologue
    const/4 v4, 0x0

    .line 1464995
    iget-object v0, p0, LX/9JS;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 1464996
    iget-object v0, p0, LX/9JS;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1464997
    :goto_0
    return-void

    .line 1464998
    :cond_0
    iget-object v0, p0, LX/9JS;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    .line 1464999
    iget-boolean v0, v5, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->e:Z

    if-nez v0, :cond_1

    iget-object v0, p1, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->a:Ljava/lang/String;

    iget-object v1, v5, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_1

    .line 1465000
    iget-object v8, p0, LX/9JS;->j:Ljava/util/ArrayList;

    iget-object v0, p1, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->a:Ljava/lang/String;

    iget-object v1, v5, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->c:Ljava/lang/String;

    iget-object v3, v5, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->d:Ljava/lang/String;

    iget-boolean v5, v5, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->f:Z

    iget-wide v6, p1, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->h:J

    invoke-static/range {v0 .. v7}, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZJ)Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    move-result-object v0

    invoke-virtual {v8, v4, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1465001
    :cond_1
    iget-object v0, p0, LX/9JS;->j:Ljava/util/ArrayList;

    iget-object v1, p0, LX/9JS;->j:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    .line 1465002
    iget-boolean v1, v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->f:Z

    if-nez v1, :cond_3

    iget-object v1, p1, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->a:Ljava/lang/String;

    iget-object v2, v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-gtz v1, :cond_3

    .line 1465003
    iget-object v1, p0, LX/9JS;->j:Ljava/util/ArrayList;

    iget-object v2, p0, LX/9JS;->j:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    iget-object v6, v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->a:Ljava/lang/String;

    iget-object v7, p1, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->b:Ljava/lang/String;

    iget-object v8, v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->c:Ljava/lang/String;

    iget-object v9, p1, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->d:Ljava/lang/String;

    iget-boolean v10, v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->e:Z

    iget-wide v12, p1, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->h:J

    move v11, v4

    invoke-static/range {v6 .. v13}, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZJ)Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1465004
    :cond_2
    add-int/lit8 v4, v4, 0x1

    :cond_3
    iget-object v0, p0, LX/9JS;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v4, v0, :cond_4

    .line 1465005
    iget-object v1, p1, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->a:Ljava/lang/String;

    iget-object v0, p0, LX/9JS;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    iget-object v0, v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_2

    .line 1465006
    iget-object v0, p0, LX/9JS;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v4, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 1465007
    :cond_4
    iget-object v0, p0, LX/9JS;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method public static c(LX/9JS;LX/9JT;)Z
    .locals 14

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x0

    .line 1465008
    iget v1, p1, LX/9JT;->a:I

    move v1, v1

    .line 1465009
    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    const/4 v2, 0x4

    if-eq v1, v2, :cond_0

    const/4 v2, 0x5

    if-eq v1, v2, :cond_0

    const/4 v2, 0x6

    if-eq v1, v2, :cond_0

    const/4 v2, 0x7

    if-ne v1, v2, :cond_1

    .line 1465010
    :cond_0
    invoke-direct {p0, p1}, LX/9JS;->d(LX/9JT;)Z

    move-result v0

    .line 1465011
    :goto_0
    return v0

    .line 1465012
    :cond_1
    packed-switch v1, :pswitch_data_0

    .line 1465013
    :pswitch_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1465014
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 1465015
    :pswitch_1
    iget-object v1, p0, LX/9JS;->g:Ljava/util/Map;

    .line 1465016
    iget v2, p1, LX/9JT;->f:I

    move v2, v2

    .line 1465017
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0w5;

    .line 1465018
    if-nez v6, :cond_2

    .line 1465019
    const-string v1, "SimpleEdgeStore"

    const-string v2, "Missing entry in the modelType map!"

    invoke-static {v1, v2}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1465020
    :cond_2
    new-instance v1, LX/9JQ;

    iget-object v2, p0, LX/9JS;->a:Ljava/lang/String;

    .line 1465021
    iget v0, p1, LX/9JT;->b:I

    move v3, v0

    .line 1465022
    new-instance v4, LX/9JR;

    .line 1465023
    iget v0, p1, LX/9JT;->c:I

    move v0, v0

    .line 1465024
    iget v7, p1, LX/9JT;->d:I

    move v7, v7

    .line 1465025
    invoke-direct {v4, v0, v7, v5}, LX/9JR;-><init>(II[B)V

    .line 1465026
    iget v0, p1, LX/9JT;->h:I

    move v7, v0

    .line 1465027
    invoke-virtual {p1}, LX/9JT;->i()J

    move-result-wide v8

    invoke-virtual {p1}, LX/9JT;->j()J

    move-result-wide v10

    .line 1465028
    iget-object v0, p1, LX/9JT;->k:[I

    move-object v12, v0

    .line 1465029
    iget-object v0, p1, LX/9JT;->l:Ljava/lang/String;

    move-object v13, v0

    .line 1465030
    invoke-direct/range {v1 .. v13}, LX/9JQ;-><init>(Ljava/lang/String;ILX/9JR;LX/9JR;LX/0w5;IJJ[ILjava/lang/String;)V

    invoke-static {p0, v1}, LX/9JS;->b(LX/9JS;LX/9JQ;)V

    goto :goto_1

    .line 1465031
    :pswitch_2
    iget v0, p1, LX/9JT;->b:I

    move v0, v0

    .line 1465032
    invoke-direct {p0, v0}, LX/9JS;->e(I)V

    goto :goto_1

    .line 1465033
    :pswitch_3
    iget-object v0, p1, LX/9JT;->m:Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    move-object v0, v0

    .line 1465034
    invoke-static {p0, v0}, LX/9JS;->b(LX/9JS;Lcom/facebook/graphql/cursor/edgestore/PageInfo;)V

    goto :goto_1

    .line 1465035
    :pswitch_4
    iget v0, p1, LX/9JT;->n:I

    move v0, v0

    .line 1465036
    iput v0, p0, LX/9JS;->k:I

    .line 1465037
    goto :goto_1

    .line 1465038
    :pswitch_5
    :try_start_0
    iget-object v1, p1, LX/9JT;->g:[B

    move-object v1, v1

    .line 1465039
    invoke-static {v1}, LX/0w5;->a([B)LX/0w5;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1465040
    iget v1, p1, LX/9JT;->f:I

    move v1, v1

    .line 1465041
    invoke-static {p0, v1, v0}, LX/9JS;->a(LX/9JS;ILX/0w5;)V

    goto :goto_1

    .line 1465042
    :catch_0
    move-exception v1

    .line 1465043
    const-string v2, "SimpleEdgeStore"

    const-string v3, "Class not found"

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private d(LX/9JT;)Z
    .locals 14

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1465044
    iget-object v0, p0, LX/9JS;->d:Landroid/util/SparseArray;

    .line 1465045
    iget v3, p1, LX/9JT;->b:I

    move v3, v3

    .line 1465046
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9JQ;

    .line 1465047
    if-nez v0, :cond_0

    .line 1465048
    const-string v0, "SimpleEdgeStore"

    const-string v1, "Trying to apply update to non-existant model"

    invoke-static {v0, v1}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 1465049
    :goto_0
    return v0

    .line 1465050
    :cond_0
    iget-object v3, v0, LX/9JQ;->a:LX/9JR;

    move-object v4, v3

    .line 1465051
    iget-object v3, v0, LX/9JQ;->b:LX/9JR;

    move-object v5, v3

    .line 1465052
    iget v3, p1, LX/9JT;->a:I

    move v3, v3

    .line 1465053
    packed-switch v3, :pswitch_data_0

    .line 1465054
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 1465055
    :goto_1
    new-instance v1, LX/9JQ;

    iget-object v2, p0, LX/9JS;->a:Ljava/lang/String;

    .line 1465056
    iget v3, v0, LX/9JQ;->d:I

    move v3, v3

    .line 1465057
    iget-object v6, v0, LX/9JQ;->e:LX/0w5;

    move-object v6, v6

    .line 1465058
    iget v7, v0, LX/9JQ;->f:I

    move v7, v7

    .line 1465059
    invoke-virtual {v0}, LX/9JQ;->i()J

    move-result-wide v8

    invoke-virtual {v0}, LX/9JQ;->j()J

    move-result-wide v10

    .line 1465060
    iget-object v12, v0, LX/9JQ;->i:[I

    move-object v12, v12

    .line 1465061
    iget-object v13, v0, LX/9JQ;->j:Ljava/lang/String;

    move-object v13, v13

    .line 1465062
    invoke-direct/range {v1 .. v13}, LX/9JQ;-><init>(Ljava/lang/String;ILX/9JR;LX/9JR;LX/0w5;IJJ[ILjava/lang/String;)V

    .line 1465063
    iget-object v2, p0, LX/9JS;->d:Landroid/util/SparseArray;

    .line 1465064
    iget v3, v0, LX/9JQ;->d:I

    move v0, v3

    .line 1465065
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v0, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1465066
    const/4 v0, 0x1

    goto :goto_0

    .line 1465067
    :pswitch_0
    new-instance v4, LX/9JR;

    .line 1465068
    iget v2, p1, LX/9JT;->c:I

    move v2, v2

    .line 1465069
    iget v3, p1, LX/9JT;->d:I

    move v3, v3

    .line 1465070
    invoke-direct {v4, v2, v3, v1}, LX/9JR;-><init>(II[B)V

    goto :goto_1

    .line 1465071
    :pswitch_1
    new-instance v5, LX/9JR;

    .line 1465072
    iget v2, p1, LX/9JT;->c:I

    move v2, v2

    .line 1465073
    iget v3, p1, LX/9JT;->d:I

    move v3, v3

    .line 1465074
    invoke-direct {v5, v2, v3, v1}, LX/9JR;-><init>(II[B)V

    goto :goto_1

    .line 1465075
    :pswitch_2
    new-instance v1, LX/9JR;

    .line 1465076
    iget v2, v4, LX/9JR;->a:I

    move v2, v2

    .line 1465077
    iget v3, v4, LX/9JR;->b:I

    move v3, v3

    .line 1465078
    iget-object v4, p1, LX/9JT;->e:[B

    move-object v4, v4

    .line 1465079
    invoke-direct {v1, v2, v3, v4}, LX/9JR;-><init>(II[B)V

    move-object v4, v1

    .line 1465080
    goto :goto_1

    .line 1465081
    :pswitch_3
    new-instance v5, LX/9JR;

    invoke-virtual {v0}, LX/9JQ;->c()I

    move-result v1

    invoke-virtual {v0}, LX/9JQ;->e()I

    move-result v2

    .line 1465082
    iget-object v3, p1, LX/9JT;->e:[B

    move-object v3, v3

    .line 1465083
    invoke-direct {v5, v1, v2, v3}, LX/9JR;-><init>(II[B)V

    goto :goto_1

    :pswitch_4
    move-object v5, v1

    .line 1465084
    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private e(I)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1465085
    iget-object v0, p0, LX/9JS;->d:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9JQ;

    .line 1465086
    if-nez v0, :cond_0

    .line 1465087
    const-string v0, "SimpleEdgeStore"

    const-string v2, "Trying to delete a model that doesn\'t exist, row ID %d, session %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v1, 0x1

    iget-object v4, p0, LX/9JS;->a:Ljava/lang/String;

    aput-object v4, v3, v1

    invoke-static {v0, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1465088
    :goto_0
    return-void

    .line 1465089
    :cond_0
    iget-object v2, v0, LX/9JQ;->i:[I

    move-object v2, v2

    .line 1465090
    if-eqz v2, :cond_2

    .line 1465091
    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget v4, v2, v1

    .line 1465092
    iget-object v5, p0, LX/9JS;->e:Landroid/util/SparseArray;

    invoke-virtual {v5, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [I

    .line 1465093
    if-nez v5, :cond_4

    .line 1465094
    :cond_1
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1465095
    :cond_2
    iget-object v1, v0, LX/9JQ;->j:Ljava/lang/String;

    move-object v1, v1

    .line 1465096
    if-eqz v1, :cond_3

    .line 1465097
    iget-object v1, p0, LX/9JS;->f:Ljava/util/Map;

    .line 1465098
    iget-object v2, v0, LX/9JQ;->j:Ljava/lang/String;

    move-object v0, v2

    .line 1465099
    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1465100
    :cond_3
    iget-object v0, p0, LX/9JS;->d:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    goto :goto_0

    .line 1465101
    :cond_4
    const/4 v6, 0x0

    :goto_3
    array-length v7, v5

    if-ge v6, v7, :cond_1

    .line 1465102
    aget v7, v5, v6

    if-ne v7, p1, :cond_5

    .line 1465103
    const/4 v7, -0x1

    aput v7, v5, v6

    goto :goto_2

    .line 1465104
    :cond_5
    add-int/lit8 v6, v6, 0x1

    goto :goto_3
.end method

.method private o()Ljava/util/ArrayList;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "LX/9JT;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v13, 0x0

    .line 1465105
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 1465106
    iget-object v0, p0, LX/9JS;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1465107
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p0, LX/9JS;->g:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0w5;

    invoke-virtual {v0}, LX/0w5;->e()[B

    move-result-object v0

    invoke-static {v2, v0}, LX/9JT;->c(I[B)LX/9JT;

    move-result-object v0

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    move v12, v13

    .line 1465108
    :goto_1
    iget-object v0, p0, LX/9JS;->d:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v12, v0, :cond_3

    .line 1465109
    iget-object v0, p0, LX/9JS;->d:Landroid/util/SparseArray;

    invoke-virtual {v0, v12}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9JQ;

    .line 1465110
    iget v1, v0, LX/9JQ;->d:I

    move v1, v1

    .line 1465111
    iget-object v2, v0, LX/9JQ;->a:LX/9JR;

    move-object v2, v2

    .line 1465112
    iget v3, v2, LX/9JR;->a:I

    move v2, v3

    .line 1465113
    iget-object v3, v0, LX/9JQ;->a:LX/9JR;

    move-object v3, v3

    .line 1465114
    iget v4, v3, LX/9JR;->b:I

    move v3, v4

    .line 1465115
    iget-object v4, p0, LX/9JS;->h:Ljava/util/Map;

    .line 1465116
    iget-object v5, v0, LX/9JQ;->e:LX/0w5;

    move-object v5, v5

    .line 1465117
    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 1465118
    iget v5, v0, LX/9JQ;->f:I

    move v5, v5

    .line 1465119
    invoke-virtual {v0}, LX/9JQ;->i()J

    move-result-wide v6

    invoke-virtual {v0}, LX/9JQ;->j()J

    move-result-wide v8

    .line 1465120
    iget-object v10, v0, LX/9JQ;->i:[I

    move-object v10, v10

    .line 1465121
    iget-object v11, v0, LX/9JQ;->j:Ljava/lang/String;

    move-object v11, v11

    .line 1465122
    invoke-static/range {v1 .. v11}, LX/9JT;->a(IIIIIJJ[ILjava/lang/String;)LX/9JT;

    move-result-object v1

    invoke-virtual {v14, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1465123
    iget-object v1, v0, LX/9JQ;->a:LX/9JR;

    move-object v1, v1

    .line 1465124
    iget-object v2, v1, LX/9JR;->c:[B

    move-object v1, v2

    .line 1465125
    if-eqz v1, :cond_1

    .line 1465126
    iget v1, v0, LX/9JQ;->d:I

    move v1, v1

    .line 1465127
    iget-object v2, v0, LX/9JQ;->a:LX/9JR;

    move-object v2, v2

    .line 1465128
    iget-object v3, v2, LX/9JR;->c:[B

    move-object v2, v3

    .line 1465129
    invoke-static {v1, v2}, LX/9JT;->a(I[B)LX/9JT;

    move-result-object v1

    invoke-virtual {v14, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1465130
    :cond_1
    iget-object v1, v0, LX/9JQ;->b:LX/9JR;

    move-object v1, v1

    .line 1465131
    if-eqz v1, :cond_2

    .line 1465132
    iget v1, v0, LX/9JQ;->d:I

    move v1, v1

    .line 1465133
    iget-object v2, v0, LX/9JQ;->b:LX/9JR;

    move-object v2, v2

    .line 1465134
    iget v3, v2, LX/9JR;->a:I

    move v2, v3

    .line 1465135
    iget-object v3, v0, LX/9JQ;->b:LX/9JR;

    move-object v3, v3

    .line 1465136
    iget v4, v3, LX/9JR;->b:I

    move v3, v4

    .line 1465137
    invoke-static {v1, v2, v3}, LX/9JT;->b(III)LX/9JT;

    move-result-object v1

    invoke-virtual {v14, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1465138
    iget-object v1, v0, LX/9JQ;->b:LX/9JR;

    move-object v1, v1

    .line 1465139
    iget-object v2, v1, LX/9JR;->c:[B

    move-object v1, v2

    .line 1465140
    if-eqz v1, :cond_2

    .line 1465141
    iget v1, v0, LX/9JQ;->d:I

    move v1, v1

    .line 1465142
    iget-object v2, v0, LX/9JQ;->b:LX/9JR;

    move-object v0, v2

    .line 1465143
    iget-object v2, v0, LX/9JR;->c:[B

    move-object v0, v2

    .line 1465144
    invoke-static {v1, v0}, LX/9JT;->b(I[B)LX/9JT;

    move-result-object v0

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1465145
    :cond_2
    add-int/lit8 v0, v12, 0x1

    move v12, v0

    goto/16 :goto_1

    .line 1465146
    :cond_3
    iget-object v0, p0, LX/9JS;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    :goto_2
    if-ge v13, v1, :cond_4

    iget-object v0, p0, LX/9JS;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    .line 1465147
    invoke-static {v0}, LX/9JT;->a(Lcom/facebook/graphql/cursor/edgestore/PageInfo;)LX/9JT;

    move-result-object v0

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1465148
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 1465149
    :cond_4
    iget v0, p0, LX/9JS;->k:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_5

    .line 1465150
    iget v0, p0, LX/9JS;->k:I

    invoke-static {v0}, LX/9JT;->c(I)LX/9JT;

    move-result-object v0

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1465151
    :cond_5
    return-object v14
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 1464861
    iget-object v0, p0, LX/9JS;->b:Ljava/io/File;

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, LX/9JS;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    goto :goto_0
.end method

.method public final a(LX/95b;I)Ljava/util/ArrayList;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/95b;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "LX/9JQ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1464862
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 1464863
    const/4 v2, 0x0

    move/from16 v16, v2

    :goto_0
    invoke-interface/range {p1 .. p1}, LX/95b;->a()I

    move-result v2

    move/from16 v0, v16

    if-ge v0, v2, :cond_1

    .line 1464864
    move-object/from16 v0, p0

    iget v5, v0, LX/9JS;->m:I

    add-int/lit8 v2, v5, 0x1

    move-object/from16 v0, p0

    iput v2, v0, LX/9JS;->m:I

    .line 1464865
    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-interface {v0, v1}, LX/95b;->a(I)I

    move-result v7

    .line 1464866
    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-interface {v0, v1}, LX/95b;->b(I)LX/0w5;

    move-result-object v8

    .line 1464867
    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-interface {v0, v1}, LX/95b;->f(I)I

    move-result v9

    .line 1464868
    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-interface {v0, v1}, LX/95b;->c(I)Ljava/lang/String;

    move-result-object v6

    .line 1464869
    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-interface {v0, v1}, LX/95b;->d(I)Ljava/util/Collection;

    move-result-object v3

    .line 1464870
    const/4 v14, 0x0

    .line 1464871
    if-eqz v3, :cond_0

    .line 1464872
    invoke-interface {v3}, Ljava/util/Collection;->size()I

    move-result v2

    new-array v14, v2, [I

    .line 1464873
    const/4 v2, 0x0

    .line 1464874
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move v3, v2

    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1464875
    add-int/lit8 v4, v3, 0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    aput v2, v14, v3

    move v3, v4

    .line 1464876
    goto :goto_1

    .line 1464877
    :cond_0
    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-interface {v0, v1}, LX/95b;->e(I)Ljava/lang/String;

    move-result-object v15

    .line 1464878
    const/4 v2, 0x0

    const/16 v3, 0x10

    invoke-virtual {v6, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x10

    invoke-static {v2, v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v10

    .line 1464879
    const/16 v2, 0x10

    invoke-virtual {v6, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x10

    invoke-static {v2, v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v12

    .line 1464880
    new-instance v3, LX/9JQ;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/9JS;->a:Ljava/lang/String;

    new-instance v6, LX/9JR;

    const/4 v2, 0x0

    move/from16 v0, p2

    invoke-direct {v6, v0, v7, v2}, LX/9JR;-><init>(II[B)V

    const/4 v7, 0x0

    invoke-direct/range {v3 .. v15}, LX/9JQ;-><init>(Ljava/lang/String;ILX/9JR;LX/9JR;LX/0w5;IJJ[ILjava/lang/String;)V

    .line 1464881
    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1464882
    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/9JS;->a(LX/9JS;LX/9JQ;)V

    .line 1464883
    add-int/lit8 v2, v16, 0x1

    move/from16 v16, v2

    goto/16 :goto_0

    .line 1464884
    :cond_1
    return-object v17
.end method

.method public final a(Lcom/facebook/graphql/cursor/edgestore/PageInfo;)V
    .locals 1

    .prologue
    .line 1464779
    iget-object v0, p0, LX/9JS;->c:LX/9JP;

    if-eqz v0, :cond_0

    .line 1464780
    invoke-static {p1}, LX/9JT;->a(Lcom/facebook/graphql/cursor/edgestore/PageInfo;)LX/9JT;

    move-result-object v0

    invoke-static {p0, v0}, LX/9JS;->b(LX/9JS;LX/9JT;)V

    .line 1464781
    :cond_0
    invoke-static {p0, p1}, LX/9JS;->b(LX/9JS;Lcom/facebook/graphql/cursor/edgestore/PageInfo;)V

    .line 1464782
    return-void
.end method

.method public final b(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "LX/9JQ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1464787
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1464788
    iget-object v0, p0, LX/9JS;->e:Landroid/util/SparseArray;

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    .line 1464789
    if-eqz v0, :cond_1

    .line 1464790
    array-length v3, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget v4, v0, v1

    .line 1464791
    const/4 v5, -0x1

    if-eq v4, v5, :cond_0

    .line 1464792
    iget-object v5, p0, LX/9JS;->d:Landroid/util/SparseArray;

    invoke-virtual {v5, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1464793
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1464794
    :cond_1
    return-object v2
.end method

.method public final b()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1464795
    iget-object v0, p0, LX/9JS;->b:Ljava/io/File;

    if-nez v0, :cond_0

    .line 1464796
    :goto_0
    return-void

    .line 1464797
    :cond_0
    iget-object v0, p0, LX/9JS;->c:LX/9JP;

    if-eqz v0, :cond_1

    .line 1464798
    :try_start_0
    iget-object v0, p0, LX/9JS;->c:LX/9JP;

    invoke-virtual {v0}, LX/9JP;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1464799
    :cond_1
    :goto_1
    :try_start_1
    invoke-direct {p0}, LX/9JS;->o()Ljava/util/ArrayList;

    move-result-object v3

    .line 1464800
    new-instance v4, Ljava/io/File;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/9JS;->b:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".tmp"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1464801
    invoke-static {v4}, LX/9JP;->a(Ljava/io/File;)LX/9JP;

    move-result-object v0

    iput-object v0, p0, LX/9JS;->c:LX/9JP;

    .line 1464802
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v1, v2

    :goto_2
    if-ge v1, v5, :cond_2

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9JT;

    .line 1464803
    invoke-virtual {v0}, LX/9JT;->o()Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 1464804
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 1464805
    iget-object v6, p0, LX/9JS;->c:LX/9JP;

    invoke-virtual {v6, v0}, LX/9JP;->a(Ljava/nio/ByteBuffer;)V

    .line 1464806
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1464807
    :cond_2
    iget-object v0, p0, LX/9JS;->b:Ljava/io/File;

    invoke-virtual {v4, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1464808
    :catch_0
    move-exception v0

    .line 1464809
    const-string v1, "SimpleEdgeStore"

    const-string v3, "Unable to compact the log"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v3, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1464810
    iget-object v0, p0, LX/9JS;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1464811
    const/4 v0, 0x0

    iput-object v0, p0, LX/9JS;->c:LX/9JP;

    goto :goto_0

    :catch_1
    goto :goto_1
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 1464812
    iget-object v0, p0, LX/9JS;->c:LX/9JP;

    if-eqz v0, :cond_0

    .line 1464813
    invoke-static {p1}, LX/9JT;->a(I)LX/9JT;

    move-result-object v0

    invoke-static {p0, v0}, LX/9JS;->b(LX/9JS;LX/9JT;)V

    .line 1464814
    :cond_0
    invoke-direct {p0, p1}, LX/9JS;->e(I)V

    .line 1464815
    return-void
.end method

.method public final c(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 1464816
    iget-object v0, p0, LX/9JS;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1464817
    iget-object v0, p0, LX/9JS;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 1464783
    iget-object v0, p0, LX/9JS;->c:LX/9JP;

    if-eqz v0, :cond_0

    .line 1464784
    invoke-static {p1}, LX/9JT;->c(I)LX/9JT;

    move-result-object v0

    invoke-static {p0, v0}, LX/9JS;->b(LX/9JS;LX/9JT;)V

    .line 1464785
    :cond_0
    iput p1, p0, LX/9JS;->k:I

    .line 1464786
    return-void
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 1464818
    iget-object v0, p0, LX/9JS;->c:LX/9JP;

    if-eqz v0, :cond_0

    .line 1464819
    iget-object v0, p0, LX/9JS;->c:LX/9JP;

    invoke-virtual {v0}, LX/9JP;->close()V

    .line 1464820
    const/4 v0, 0x0

    iput-object v0, p0, LX/9JS;->c:LX/9JP;

    .line 1464821
    :cond_0
    iget-object v0, p0, LX/9JS;->d:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 1464822
    iget-object v0, p0, LX/9JS;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 1464823
    iget-object v0, p0, LX/9JS;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1464824
    return-void
.end method

.method public final d(I)V
    .locals 4

    .prologue
    .line 1464825
    invoke-virtual {p0}, LX/9JS;->k()Ljava/util/ArrayList;

    move-result-object v2

    .line 1464826
    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 1464827
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, p1, :cond_4

    .line 1464828
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-lt v1, p1, :cond_0

    .line 1464829
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9JQ;

    .line 1464830
    iget v3, v0, LX/9JQ;->d:I

    move v0, v3

    .line 1464831
    invoke-virtual {p0, v0}, LX/9JS;->b(I)V

    .line 1464832
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 1464833
    :cond_0
    const/4 v1, 0x0

    .line 1464834
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    iget-object v0, p0, LX/9JS;->d:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 1464835
    iget-object v0, p0, LX/9JS;->d:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9JQ;

    .line 1464836
    if-eqz v1, :cond_1

    invoke-virtual {v0}, LX/9JQ;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-lez v3, :cond_5

    .line 1464837
    :cond_1
    invoke-virtual {v0}, LX/9JQ;->k()Ljava/lang/String;

    move-result-object v0

    .line 1464838
    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_1

    .line 1464839
    :cond_2
    :goto_3
    iget-object v0, p0, LX/9JS;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    if-eqz v1, :cond_3

    iget-object v0, p0, LX/9JS;->j:Ljava/util/ArrayList;

    iget-object v2, p0, LX/9JS;->j:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    iget-object v0, v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_4

    .line 1464840
    :cond_3
    iget-object v0, p0, LX/9JS;->j:Ljava/util/ArrayList;

    iget-object v2, p0, LX/9JS;->j:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_3

    .line 1464841
    :cond_4
    return-void

    :cond_5
    move-object v0, v1

    goto :goto_2
.end method

.method public final g()[J
    .locals 4

    .prologue
    .line 1464842
    iget-object v0, p0, LX/9JS;->d:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    new-array v1, v0, [J

    .line 1464843
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 1464844
    iget-object v2, p0, LX/9JS;->d:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    int-to-long v2, v2

    aput-wide v2, v1, v0

    .line 1464845
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1464846
    :cond_0
    return-object v1
.end method

.method public final k()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "LX/9JQ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1464847
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, LX/9JS;->d:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1464848
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, LX/9JS;->d:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 1464849
    iget-object v2, p0, LX/9JS;->d:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1464850
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1464851
    :cond_0
    return-object v1
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 1464852
    iget-object v0, p0, LX/9JS;->c:LX/9JP;

    if-eqz v0, :cond_0

    .line 1464853
    iget-object v0, p0, LX/9JS;->c:LX/9JP;

    invoke-virtual {v0}, LX/9JP;->close()V

    .line 1464854
    iget-object v0, p0, LX/9JS;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1464855
    iget-object v0, p0, LX/9JS;->b:Ljava/io/File;

    invoke-static {v0}, LX/9JP;->a(Ljava/io/File;)LX/9JP;

    move-result-object v0

    iput-object v0, p0, LX/9JS;->c:LX/9JP;

    .line 1464856
    :cond_0
    iget-object v0, p0, LX/9JS;->d:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 1464857
    iget-object v0, p0, LX/9JS;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 1464858
    iget-object v0, p0, LX/9JS;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1464859
    return-void
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 1464860
    iget v0, p0, LX/9JS;->l:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/9JS;->l:I

    return v0
.end method
