.class public final enum LX/9cJ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9cJ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9cJ;

.field public static final enum DOODLE:LX/9cJ;

.field public static final enum FACEBOXES:LX/9cJ;

.field public static final enum FRAME:LX/9cJ;

.field public static final enum STICKERS:LX/9cJ;

.field public static final enum TAGS:LX/9cJ;

.field public static final enum TEXTS:LX/9cJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1516407
    new-instance v0, LX/9cJ;

    const-string v1, "STICKERS"

    invoke-direct {v0, v1, v3}, LX/9cJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9cJ;->STICKERS:LX/9cJ;

    .line 1516408
    new-instance v0, LX/9cJ;

    const-string v1, "TEXTS"

    invoke-direct {v0, v1, v4}, LX/9cJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9cJ;->TEXTS:LX/9cJ;

    .line 1516409
    new-instance v0, LX/9cJ;

    const-string v1, "DOODLE"

    invoke-direct {v0, v1, v5}, LX/9cJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9cJ;->DOODLE:LX/9cJ;

    .line 1516410
    new-instance v0, LX/9cJ;

    const-string v1, "FACEBOXES"

    invoke-direct {v0, v1, v6}, LX/9cJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9cJ;->FACEBOXES:LX/9cJ;

    .line 1516411
    new-instance v0, LX/9cJ;

    const-string v1, "TAGS"

    invoke-direct {v0, v1, v7}, LX/9cJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9cJ;->TAGS:LX/9cJ;

    .line 1516412
    new-instance v0, LX/9cJ;

    const-string v1, "FRAME"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/9cJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9cJ;->FRAME:LX/9cJ;

    .line 1516413
    const/4 v0, 0x6

    new-array v0, v0, [LX/9cJ;

    sget-object v1, LX/9cJ;->STICKERS:LX/9cJ;

    aput-object v1, v0, v3

    sget-object v1, LX/9cJ;->TEXTS:LX/9cJ;

    aput-object v1, v0, v4

    sget-object v1, LX/9cJ;->DOODLE:LX/9cJ;

    aput-object v1, v0, v5

    sget-object v1, LX/9cJ;->FACEBOXES:LX/9cJ;

    aput-object v1, v0, v6

    sget-object v1, LX/9cJ;->TAGS:LX/9cJ;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/9cJ;->FRAME:LX/9cJ;

    aput-object v2, v0, v1

    sput-object v0, LX/9cJ;->$VALUES:[LX/9cJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1516414
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9cJ;
    .locals 1

    .prologue
    .line 1516406
    const-class v0, LX/9cJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9cJ;

    return-object v0
.end method

.method public static values()[LX/9cJ;
    .locals 1

    .prologue
    .line 1516405
    sget-object v0, LX/9cJ;->$VALUES:[LX/9cJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9cJ;

    return-object v0
.end method
