.class public abstract LX/9gr;
.super LX/9g8;
.source ""

# interfaces
.implements LX/1rs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "Q:",
        "Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;",
        "E:",
        "Ljava/lang/Object;",
        ">",
        "LX/9g8",
        "<TQ;>;",
        "LX/1rs",
        "<TE;",
        "Ljava/lang/Void;",
        "TT;>;"
    }
.end annotation


# instance fields
.field public final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final c:Lcom/facebook/common/callercontext/CallerContext;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;Ljava/lang/Class;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 0
    .param p3    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TQ;",
            "Ljava/lang/Class",
            "<TE;>;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1525727
    invoke-direct {p0, p1}, LX/9g8;-><init>(Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;)V

    .line 1525728
    iput-object p2, p0, LX/9gr;->b:Ljava/lang/Class;

    .line 1525729
    iput-object p3, p0, LX/9gr;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 1525730
    return-void
.end method


# virtual methods
.method public abstract a(ILjava/lang/String;)LX/0gW;
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "LX/0gW",
            "<TT;>;"
        }
    .end annotation
.end method

.method public final a(LX/3DR;Ljava/lang/Object;)LX/0gW;
    .locals 2

    .prologue
    .line 1525724
    iget v0, p1, LX/3DR;->e:I

    move v0, v0

    .line 1525725
    iget-object v1, p1, LX/3DR;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1525726
    invoke-virtual {p0, v0, v1}, LX/9gr;->a(ILjava/lang/String;)LX/0gW;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/5Mb;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;)",
            "LX/5Mb",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1525731
    invoke-virtual {p0, p1}, LX/9gr;->b(Lcom/facebook/graphql/executor/GraphQLResult;)LX/9fz;

    move-result-object v1

    .line 1525732
    iget-object v0, v1, LX/9fz;->b:Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    if-nez v0, :cond_0

    sget-object v0, LX/5Mb;->a:LX/5Mb;

    :goto_0
    return-object v0

    .line 1525733
    :cond_0
    iget-object v0, v1, LX/9fz;->b:Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    invoke-virtual {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v2, v1, LX/9fz;->b:Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    invoke-virtual {v2}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;->b()Z

    move-result v2

    iget-object v3, v1, LX/9fz;->b:Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    invoke-virtual {v3}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;->c()Z

    move-result v3

    iget-object v4, v1, LX/9fz;->b:Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    invoke-virtual {v4}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;->p_()Ljava/lang/String;

    move-result-object v4

    new-instance v5, LX/186;

    const/16 v6, 0x400

    invoke-direct {v5, v6}, LX/186;-><init>(I)V

    invoke-virtual {v5, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v5, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, LX/186;->c(I)V

    const/4 v6, 0x0

    invoke-virtual {v5, v6, v0}, LX/186;->b(II)V

    const/4 v0, 0x1

    invoke-virtual {v5, v0, v2}, LX/186;->a(IZ)V

    const/4 v0, 0x2

    invoke-virtual {v5, v0, v3}, LX/186;->a(IZ)V

    const/4 v0, 0x3

    invoke-virtual {v5, v0, v4}, LX/186;->b(II)V

    const v0, 0x4e363642    # 7.6425229E8f

    invoke-static {v5, v0}, LX/1vs;->a(LX/186;I)LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    .line 1525734
    new-instance v0, LX/5Mb;

    iget-object v1, v1, LX/9fz;->a:LX/0Px;

    invoke-direct {v0, v1, v2, v3}, LX/5Mb;-><init>(LX/0Px;LX/15i;I)V

    goto :goto_0
.end method

.method public final b(ILjava/lang/String;)LX/0zO;
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "LX/0zO",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1525719
    invoke-virtual {p0, p1, p2}, LX/9gr;->a(ILjava/lang/String;)LX/0gW;

    move-result-object v0

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 1525720
    iget-object v1, p0, LX/9gr;->c:Lcom/facebook/common/callercontext/CallerContext;

    move-object v1, v1

    .line 1525721
    iput-object v1, v0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 1525722
    move-object v0, v0

    .line 1525723
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    return-object v0
.end method

.method public abstract b(Lcom/facebook/graphql/executor/GraphQLResult;)LX/9fz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;)",
            "LX/9fz",
            "<TE;>;"
        }
    .end annotation
.end method
