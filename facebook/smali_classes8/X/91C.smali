.class public LX/91C;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zb;

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1430218
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1430219
    iput-object p1, p0, LX/91C;->a:LX/0Zb;

    .line 1430220
    return-void
.end method

.method public static a(LX/91C;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 1

    .prologue
    .line 1430221
    iget-object v0, p0, LX/91C;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1430222
    iget-object v0, p0, LX/91C;->b:Ljava/lang/String;

    .line 1430223
    iput-object v0, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 1430224
    :cond_0
    iget-object v0, p0, LX/91C;->a:LX/0Zb;

    invoke-interface {v0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1430225
    return-void
.end method
