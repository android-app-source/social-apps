.class public final LX/8jV;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8jW;

.field public final synthetic b:LX/8jY;


# direct methods
.method public constructor <init>(LX/8jY;LX/8jW;)V
    .locals 0

    .prologue
    .line 1392571
    iput-object p1, p0, LX/8jV;->b:LX/8jY;

    iput-object p2, p0, LX/8jV;->a:LX/8jW;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1392572
    iget-object v0, p0, LX/8jV;->b:LX/8jY;

    iget-object v0, v0, LX/8jY;->e:LX/3Mb;

    if-eqz v0, :cond_0

    .line 1392573
    iget-object v0, p0, LX/8jV;->b:LX/8jY;

    iget-object v0, v0, LX/8jY;->e:LX/3Mb;

    iget-object v1, p0, LX/8jV;->a:LX/8jW;

    invoke-interface {v0, v1, p1}, LX/3Mb;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1392574
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1392575
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 1392576
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/service/FetchStickersResult;

    .line 1392577
    iget-object v1, p0, LX/8jV;->b:LX/8jY;

    iget-object v1, v1, LX/8jY;->e:LX/3Mb;

    if-eqz v1, :cond_0

    .line 1392578
    iget-object v1, p0, LX/8jV;->b:LX/8jY;

    iget-object v1, v1, LX/8jY;->e:LX/3Mb;

    iget-object v2, p0, LX/8jV;->a:LX/8jW;

    new-instance v3, LX/8jX;

    .line 1392579
    iget-object p0, v0, Lcom/facebook/stickers/service/FetchStickersResult;->a:LX/0Px;

    move-object v0, p0

    .line 1392580
    invoke-direct {v3, v0}, LX/8jX;-><init>(Ljava/util/List;)V

    invoke-interface {v1, v2, v3}, LX/3Mb;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1392581
    :cond_0
    return-void
.end method
