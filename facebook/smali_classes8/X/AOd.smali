.class public final LX/AOd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;)V
    .locals 0

    .prologue
    .line 1669125
    iput-object p1, p0, LX/AOd;->a:Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 3
    .param p1    # Lcom/facebook/graphql/executor/GraphQLResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1669126
    if-eqz p1, :cond_0

    .line 1669127
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1669128
    if-eqz v0, :cond_0

    .line 1669129
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1669130
    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel;->a()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel$ChangedPageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1669131
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1669132
    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel;->a()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel$ChangedPageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel$ChangedPageModel;->j()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1669133
    :cond_0
    :goto_0
    return-void

    .line 1669134
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1669135
    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel;->j()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForAddPlaceRecommendationModel;

    move-result-object v0

    invoke-static {v0}, LX/5Hb;->a(Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForAddPlaceRecommendationModel;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 1669136
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1669137
    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel;->a()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel$ChangedPageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel$ChangedPageModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/5JC;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLPlaceListItem;

    move-result-object v0

    .line 1669138
    if-eqz v0, :cond_0

    .line 1669139
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, LX/AOd;->a:Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;

    iget-object v2, v2, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->u:LX/1c9;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1669140
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1669141
    iget-object v0, p0, LX/AOd;->a:Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;

    .line 1669142
    invoke-static {v0, v1}, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->a$redex0(Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;Ljava/util/List;)V

    .line 1669143
    iget-object v0, p0, LX/AOd;->a:Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;

    const/4 v1, 0x1

    .line 1669144
    iput-boolean v1, v0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->A:Z

    .line 1669145
    goto :goto_0
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1669146
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1669147
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/AOd;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    return-void
.end method
