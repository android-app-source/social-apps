.class public LX/9CD;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedback/ui/rows/FeedbackRootGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedback/ui/rows/TypingIndicatorGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/9F6;

.field private final d:LX/9CE;

.field private final e:LX/9E9;

.field public final f:LX/1DS;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/9F6;LX/9CE;LX/9E9;LX/1DS;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedback/ui/rows/FeedbackRootGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedback/ui/rows/TypingIndicatorGroupPartDefinition;",
            ">;",
            "LX/9F6;",
            "LX/9CE;",
            "LX/9E9;",
            "LX/1DS;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1453629
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1453630
    iput-object p1, p0, LX/9CD;->a:LX/0Ot;

    .line 1453631
    iput-object p2, p0, LX/9CD;->b:LX/0Ot;

    .line 1453632
    iput-object p3, p0, LX/9CD;->c:LX/9F6;

    .line 1453633
    iput-object p4, p0, LX/9CD;->d:LX/9CE;

    .line 1453634
    iput-object p5, p0, LX/9CD;->e:LX/9E9;

    .line 1453635
    iput-object p6, p0, LX/9CD;->f:LX/1DS;

    .line 1453636
    return-void
.end method

.method public static a(LX/0QB;)LX/9CD;
    .locals 8

    .prologue
    .line 1453651
    new-instance v1, LX/9CD;

    const/16 v2, 0x1dc4

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x1dcd

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const-class v4, LX/9F6;

    invoke-interface {p0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/9F6;

    const-class v5, LX/9CE;

    invoke-interface {p0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/9CE;

    const-class v6, LX/9E9;

    invoke-interface {p0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/9E9;

    invoke-static {p0}, LX/1DS;->b(LX/0QB;)LX/1DS;

    move-result-object v7

    check-cast v7, LX/1DS;

    invoke-direct/range {v1 .. v7}, LX/9CD;-><init>(LX/0Ot;LX/0Ot;LX/9F6;LX/9CE;LX/9E9;LX/1DS;)V

    .line 1453652
    move-object v0, v1

    .line 1453653
    return-object v0
.end method

.method private static a(LX/9CD;LX/9FA;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/9E8;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/9E8;"
        }
    .end annotation

    .prologue
    .line 1453654
    new-instance v0, LX/9Di;

    invoke-direct {v0, p2}, LX/9Di;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1453655
    iget-object v1, p0, LX/9CD;->f:LX/1DS;

    iget-object v2, p0, LX/9CD;->a:LX/0Ot;

    invoke-virtual {v1, v2, v0}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v1

    .line 1453656
    iput-object p1, v1, LX/1Ql;->f:LX/1PW;

    .line 1453657
    move-object v1, v1

    .line 1453658
    invoke-virtual {v1}, LX/1Ql;->e()LX/1Qq;

    move-result-object v1

    .line 1453659
    iget-object v2, p0, LX/9CD;->e:LX/9E9;

    .line 1453660
    new-instance v3, LX/9E8;

    invoke-static {v2}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v7

    check-cast v7, LX/0Sh;

    invoke-static {v2}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-static {v2}, LX/9Dz;->a(LX/0QB;)LX/9Dz;

    move-result-object v9

    check-cast v9, LX/9Dz;

    move-object v4, v1

    move-object v5, v0

    move-object v6, p1

    invoke-direct/range {v3 .. v9}, LX/9E8;-><init>(LX/1Qq;LX/9Di;LX/9FA;LX/0Sh;LX/03V;LX/9Dz;)V

    .line 1453661
    move-object v0, v3

    .line 1453662
    return-object v0
.end method


# virtual methods
.method public final a(LX/9FA;Lcom/facebook/feed/rows/core/props/FeedProps;Z)LX/9CC;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;Z)",
            "LX/9CC;"
        }
    .end annotation

    .prologue
    .line 1453637
    iget-object v0, p0, LX/9CD;->d:LX/9CE;

    invoke-static {p0, p1, p2}, LX/9CD;->a(LX/9CD;LX/9FA;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/9E8;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p0, p1, v2}, LX/9CD;->a(LX/9CD;LX/9FA;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/9E8;

    move-result-object v2

    .line 1453638
    new-instance v3, LX/1Cq;

    invoke-direct {v3}, LX/1Cq;-><init>()V

    .line 1453639
    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 1453640
    iput-object v4, v3, LX/1Cq;->a:Ljava/lang/Object;

    .line 1453641
    iget-object v4, p0, LX/9CD;->f:LX/1DS;

    iget-object p2, p0, LX/9CD;->b:LX/0Ot;

    invoke-virtual {v4, p2, v3}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v4

    .line 1453642
    iput-object p1, v4, LX/1Ql;->f:LX/1PW;

    .line 1453643
    move-object v4, v4

    .line 1453644
    invoke-virtual {v4}, LX/1Ql;->e()LX/1Qq;

    move-result-object v4

    .line 1453645
    new-instance p2, LX/9F5;

    invoke-direct {p2, v4, v3}, LX/9F5;-><init>(LX/1Qq;LX/1Cq;)V

    .line 1453646
    move-object v3, p2

    .line 1453647
    move-object v3, v3

    .line 1453648
    new-instance v4, LX/9CC;

    invoke-static {v0}, LX/9Dz;->a(LX/0QB;)LX/9Dz;

    move-result-object v9

    check-cast v9, LX/9Dz;

    move-object v5, v1

    move-object v6, v2

    move-object v7, v3

    move v8, p3

    invoke-direct/range {v4 .. v9}, LX/9CC;-><init>(LX/9E8;LX/9E8;LX/9F5;ZLX/9Dz;)V

    .line 1453649
    move-object v0, v4

    .line 1453650
    return-object v0
.end method
