.class public LX/8hy;
.super Landroid/text/style/ReplacementSpan;
.source ""


# instance fields
.field private final a:F

.field private final b:I

.field private final c:I

.field private final d:F

.field private final e:F

.field private final f:Landroid/graphics/Paint$FontMetrics;


# direct methods
.method public constructor <init>(FIIFF)V
    .locals 1

    .prologue
    .line 1391077
    invoke-direct {p0}, Landroid/text/style/ReplacementSpan;-><init>()V

    .line 1391078
    new-instance v0, Landroid/graphics/Paint$FontMetrics;

    invoke-direct {v0}, Landroid/graphics/Paint$FontMetrics;-><init>()V

    iput-object v0, p0, LX/8hy;->f:Landroid/graphics/Paint$FontMetrics;

    .line 1391079
    iput p1, p0, LX/8hy;->a:F

    .line 1391080
    iput p2, p0, LX/8hy;->b:I

    .line 1391081
    iput p3, p0, LX/8hy;->c:I

    .line 1391082
    iput p4, p0, LX/8hy;->d:F

    .line 1391083
    iput p5, p0, LX/8hy;->e:F

    .line 1391084
    return-void
.end method

.method private a(Landroid/graphics/Paint;Ljava/lang/CharSequence;II)F
    .locals 3

    .prologue
    .line 1391085
    invoke-virtual {p1, p2, p3, p4}, Landroid/graphics/Paint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    iget v2, p0, LX/8hy;->e:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;Ljava/lang/CharSequence;IIFIIILandroid/graphics/Paint;)V
    .locals 8

    .prologue
    .line 1391086
    iget-object v1, p0, LX/8hy;->f:Landroid/graphics/Paint$FontMetrics;

    move-object/from16 v0, p9

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->getFontMetrics(Landroid/graphics/Paint$FontMetrics;)F

    .line 1391087
    add-int/lit8 v1, p7, 0x4

    .line 1391088
    iget-object v2, p0, LX/8hy;->f:Landroid/graphics/Paint$FontMetrics;

    iget v2, v2, Landroid/graphics/Paint$FontMetrics;->descent:F

    iget-object v3, p0, LX/8hy;->f:Landroid/graphics/Paint$FontMetrics;

    iget v3, v3, Landroid/graphics/Paint$FontMetrics;->ascent:F

    sub-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    iget v4, p0, LX/8hy;->d:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    .line 1391089
    int-to-float v3, v1

    sub-float/2addr v3, v2

    .line 1391090
    int-to-float v1, v1

    iget-object v4, p0, LX/8hy;->f:Landroid/graphics/Paint$FontMetrics;

    iget v4, v4, Landroid/graphics/Paint$FontMetrics;->descent:F

    sub-float/2addr v1, v4

    iget v4, p0, LX/8hy;->d:F

    sub-float v6, v1, v4

    .line 1391091
    new-instance v1, Landroid/graphics/RectF;

    move-object/from16 v0, p9

    invoke-direct {p0, v0, p2, p3, p4}, LX/8hy;->a(Landroid/graphics/Paint;Ljava/lang/CharSequence;II)F

    move-result v4

    add-float/2addr v4, p5

    add-float/2addr v2, v3

    invoke-direct {v1, p5, v3, v4, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1391092
    iget v2, p0, LX/8hy;->b:I

    move-object/from16 v0, p9

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1391093
    iget v2, p0, LX/8hy;->a:F

    iget v3, p0, LX/8hy;->a:F

    move-object/from16 v0, p9

    invoke-virtual {p1, v1, v2, v3, v0}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 1391094
    iget v1, p0, LX/8hy;->c:I

    move-object/from16 v0, p9

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1391095
    iget v1, p0, LX/8hy;->e:F

    add-float v5, p5, v1

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object/from16 v7, p9

    invoke-virtual/range {v1 .. v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    .line 1391096
    return-void
.end method

.method public final getSize(Landroid/graphics/Paint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;)I
    .locals 2

    .prologue
    .line 1391097
    invoke-direct {p0, p1, p2, p3, p4}, LX/8hy;->a(Landroid/graphics/Paint;Ljava/lang/CharSequence;II)F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method
