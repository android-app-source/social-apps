.class public final LX/93O;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;

.field public final synthetic b:LX/92e;

.field public final synthetic c:LX/93P;


# direct methods
.method public constructor <init>(LX/93P;Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;LX/92e;)V
    .locals 0

    .prologue
    .line 1433684
    iput-object p1, p0, LX/93O;->c:LX/93P;

    iput-object p2, p0, LX/93O;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;

    iput-object p3, p0, LX/93O;->b:LX/92e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x25e63d62

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1433685
    iget-object v1, p0, LX/93O;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;

    if-eqz v1, :cond_0

    .line 1433686
    iget-object v1, p0, LX/93O;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;

    iget-object v2, p0, LX/93O;->b:LX/92e;

    .line 1433687
    iget-object v4, v1, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->i:LX/92C;

    invoke-virtual {v4}, LX/92C;->a()V

    .line 1433688
    iget-object v4, v1, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->g:LX/919;

    iget-object v5, v1, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->r:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1433689
    iget-object v6, v5, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v5, v6

    .line 1433690
    iget-object v6, v2, LX/92e;->a:LX/5LG;

    move-object v6, v6

    .line 1433691
    invoke-interface {v6}, LX/5LG;->l()Ljava/lang/String;

    move-result-object v6

    .line 1433692
    iget-object v7, v2, LX/92e;->b:Ljava/lang/String;

    move-object v7, v7

    .line 1433693
    iget-object v8, v1, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->q:LX/0Px;

    invoke-virtual {v8, v2}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v8

    iget-object v9, v1, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->q:LX/0Px;

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v9

    iget-object v10, v1, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->b:LX/92o;

    invoke-virtual {v10}, LX/92o;->d()Ljava/lang/String;

    move-result-object v10

    .line 1433694
    const-string p0, "activity_picker_verb_selected"

    invoke-static {p0, v5}, LX/919;->d(Ljava/lang/String;Ljava/lang/String;)LX/8yQ;

    move-result-object p0

    invoke-virtual {p0, v6}, LX/8yQ;->a(Ljava/lang/String;)LX/8yQ;

    move-result-object p0

    invoke-virtual {p0, v7}, LX/8yQ;->g(Ljava/lang/String;)LX/8yQ;

    move-result-object p0

    invoke-virtual {p0, v8}, LX/8yQ;->c(I)LX/8yQ;

    move-result-object p0

    invoke-virtual {p0, v9}, LX/8yQ;->a(I)LX/8yQ;

    move-result-object p0

    invoke-virtual {p0, v10}, LX/8yQ;->e(Ljava/lang/String;)LX/8yQ;

    move-result-object p0

    .line 1433695
    iget-object p1, p0, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object p0, p1

    .line 1433696
    iget-object p1, v4, LX/919;->a:LX/0Zb;

    invoke-interface {p1, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1433697
    iget-object v4, v1, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->u:LX/90a;

    if-eqz v4, :cond_0

    .line 1433698
    iget-object v4, v1, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;->u:LX/90a;

    .line 1433699
    iget-object v5, v2, LX/92e;->a:LX/5LG;

    move-object v5, v5

    .line 1433700
    invoke-interface {v4, v5}, LX/90a;->a(LX/5LG;)V

    .line 1433701
    :cond_0
    const v1, 0x1d68ba7

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
