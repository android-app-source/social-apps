.class public final enum LX/92W;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/92W;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/92W;

.field public static final enum SEARCH:LX/92W;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1432340
    new-instance v0, LX/92W;

    const-string v1, "SEARCH"

    invoke-direct {v0, v1, v2}, LX/92W;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/92W;->SEARCH:LX/92W;

    .line 1432341
    const/4 v0, 0x1

    new-array v0, v0, [LX/92W;

    sget-object v1, LX/92W;->SEARCH:LX/92W;

    aput-object v1, v0, v2

    sput-object v0, LX/92W;->$VALUES:[LX/92W;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1432339
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/92W;
    .locals 1

    .prologue
    .line 1432342
    const-class v0, LX/92W;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/92W;

    return-object v0
.end method

.method public static values()[LX/92W;
    .locals 1

    .prologue
    .line 1432338
    sget-object v0, LX/92W;->$VALUES:[LX/92W;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/92W;

    return-object v0
.end method
