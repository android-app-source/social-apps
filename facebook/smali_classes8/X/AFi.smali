.class public final LX/AFi;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1648036
    const-class v1, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;

    const v0, 0x24c83e7e

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "SnacksReplyThreadQueryV2"

    const-string v6, "f980cda746002772ab9e9d2725f25985"

    const-string v7, "node"

    const-string v8, "10155247526456729"

    const-string v9, "10155259090171729"

    .line 1648037
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1648038
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1648039
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1648040
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1648041
    sparse-switch v0, :sswitch_data_0

    .line 1648042
    :goto_0
    return-object p1

    .line 1648043
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1648044
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1648045
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1648046
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1648047
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1648048
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0xd1b -> :sswitch_2
        0x35e001 -> :sswitch_1
        0x683094a -> :sswitch_3
        0x177da4cf -> :sswitch_0
        0x38abfb24 -> :sswitch_4
        0x683ab666 -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1648049
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    packed-switch v1, :pswitch_data_1

    .line 1648050
    :goto_1
    return v0

    .line 1648051
    :pswitch_1
    const-string v2, "0"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :pswitch_2
    const-string v2, "5"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :pswitch_3
    const-string v2, "1"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    .line 1648052
    :pswitch_4
    const/16 v0, 0x32

    const-string v1, "%s"

    invoke-static {p2, v0, v1}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 1648053
    :pswitch_5
    const/16 v0, 0x64

    const-string v1, "%s"

    invoke-static {p2, v0, v1}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 1648054
    :pswitch_6
    const/16 v0, 0xa0

    const-string v1, "%s"

    invoke-static {p2, v0, v1}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x30
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
