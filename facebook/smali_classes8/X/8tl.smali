.class public final LX/8tl;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:LX/8tn;


# direct methods
.method public constructor <init>(LX/8tn;)V
    .locals 0

    .prologue
    .line 1413805
    iput-object p1, p0, LX/8tl;->a:LX/8tn;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    .line 1413806
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    .line 1413807
    cmpg-double v2, v0, v4

    if-gez v2, :cond_1

    .line 1413808
    iget-object v0, p0, LX/8tl;->a:LX/8tn;

    iget-object v0, v0, LX/8tn;->u:LX/8tm;

    sget-object v1, LX/8tm;->BEING_DRAGGED:LX/8tm;

    if-eq v0, v1, :cond_0

    .line 1413809
    iget-object v0, p0, LX/8tl;->a:LX/8tn;

    iget-object v1, p0, LX/8tl;->a:LX/8tn;

    iget-object v1, v1, LX/8tn;->s:LX/31M;

    .line 1413810
    iput-object v1, v0, LX/8tn;->t:LX/31M;

    .line 1413811
    iget-object v0, p0, LX/8tl;->a:LX/8tn;

    iget-object v0, v0, LX/8tn;->f:LX/0wd;

    .line 1413812
    iget-object v6, p1, LX/0wd;->e:LX/0we;

    iget-wide v6, v6, LX/0we;->b:D

    move-wide v2, v6

    .line 1413813
    invoke-virtual {v0, v2, v3}, LX/0wd;->c(D)LX/0wd;

    move-result-object v0

    .line 1413814
    iget-wide v6, p1, LX/0wd;->i:D

    move-wide v2, v6

    .line 1413815
    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 1413816
    :cond_0
    invoke-virtual {p1, v4, v5}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, LX/0wd;->b(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    .line 1413817
    :goto_0
    return-void

    .line 1413818
    :cond_1
    iget-object v3, p0, LX/8tl;->a:LX/8tn;

    iget-object v3, v3, LX/8tn;->n:Landroid/view/ViewGroup;

    iget-object v4, p0, LX/8tl;->a:LX/8tn;

    iget-object v4, v4, LX/8tn;->s:LX/31M;

    double-to-int v0, v0

    .line 1413819
    invoke-static {v3, v4, v0}, LX/8tn;->a(Landroid/view/View;LX/31M;I)V

    .line 1413820
    goto :goto_0
.end method
