.class public final LX/9GF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/9GG;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/9FA;

.field public final synthetic e:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;LX/9GG;Ljava/lang/String;Ljava/lang/String;LX/9FA;)V
    .locals 0

    .prologue
    .line 1459600
    iput-object p1, p0, LX/9GF;->e:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;

    iput-object p2, p0, LX/9GF;->a:LX/9GG;

    iput-object p3, p0, LX/9GF;->b:Ljava/lang/String;

    iput-object p4, p0, LX/9GF;->c:Ljava/lang/String;

    iput-object p5, p0, LX/9GF;->d:LX/9FA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 13

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x740405b7

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 1459601
    iget-object v0, p0, LX/9GF;->e:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;

    iget-object v1, p0, LX/9GF;->a:LX/9GG;

    iget-object v1, v1, LX/9GG;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    iget-object v2, p0, LX/9GF;->a:LX/9GG;

    iget-object v2, v2, LX/9GG;->b:Lcom/facebook/graphql/model/GraphQLPage;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/9GF;->b:Ljava/lang/String;

    iget-object v4, p0, LX/9GF;->a:LX/9GG;

    iget-boolean v4, v4, LX/9GG;->c:Z

    .line 1459602
    if-nez v1, :cond_5

    .line 1459603
    const/4 v6, 0x0

    .line 1459604
    :goto_0
    move-object v8, v6

    .line 1459605
    iget-object v0, p0, LX/9GF;->e:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;

    iget-object v1, p0, LX/9GF;->a:LX/9GG;

    iget-object v1, v1, LX/9GG;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1}, LX/1WF;->h(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    iget-object v2, p0, LX/9GF;->a:LX/9GG;

    iget-object v2, v2, LX/9GG;->b:Lcom/facebook/graphql/model/GraphQLPage;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/9GF;->b:Ljava/lang/String;

    iget-object v4, p0, LX/9GF;->a:LX/9GG;

    iget-boolean v4, v4, LX/9GG;->c:Z

    .line 1459606
    if-nez v1, :cond_6

    .line 1459607
    const/4 v6, 0x0

    .line 1459608
    :goto_1
    move-object v9, v6

    .line 1459609
    iget-object v0, p0, LX/9GF;->e:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;->b:LX/8xz;

    iget-object v1, p0, LX/9GF;->b:Ljava/lang/String;

    iget-object v2, p0, LX/9GF;->a:LX/9GG;

    iget-object v2, v2, LX/9GG;->b:Lcom/facebook/graphql/model/GraphQLPage;

    iget-object v3, p0, LX/9GF;->c:Ljava/lang/String;

    if-eqz v8, :cond_1

    .line 1459610
    iget-object v4, v8, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 1459611
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    :goto_2
    if-eqz v9, :cond_0

    iget-object v5, v9, LX/6PS;->b:Lcom/facebook/graphql/model/GraphQLComment;

    :cond_0
    new-instance v6, LX/9GE;

    invoke-direct {v6, p0}, LX/9GE;-><init>(LX/9GF;)V

    .line 1459612
    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v10

    .line 1459613
    new-instance v11, LX/4DT;

    invoke-direct {v11}, LX/4DT;-><init>()V

    .line 1459614
    const-string v12, "comment_id"

    invoke-virtual {v11, v12, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1459615
    move-object v11, v11

    .line 1459616
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v12

    .line 1459617
    const-string p1, "place_id"

    invoke-virtual {v11, p1, v12}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1459618
    move-object v11, v11

    .line 1459619
    const-string v12, "recommendation_types"

    invoke-virtual {v11, v12, v10}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1459620
    move-object v10, v11

    .line 1459621
    new-instance v11, LX/5IJ;

    invoke-direct {v11}, LX/5IJ;-><init>()V

    move-object v11, v11

    .line 1459622
    const-string v12, "input"

    invoke-virtual {v11, v12, v10}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1459623
    const-string v10, "profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v11, v10, v12}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1459624
    invoke-static {v11}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v10

    invoke-static {v2, v5, v4}, LX/8xz;->a(Lcom/facebook/graphql/model/GraphQLPage;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$DeletePlaceRecommendationFromCommentMutationModel;

    move-result-object v11

    invoke-virtual {v10, v11}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v10

    .line 1459625
    iget-object v11, v0, LX/8xz;->c:LX/1Ck;

    const-string v12, "remove_place"

    iget-object p1, v0, LX/8xz;->a:LX/0tX;

    invoke-virtual {p1, v10}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v10

    invoke-static {v6}, LX/0Vd;->of(LX/0TF;)LX/0Vd;

    move-result-object p1

    invoke-virtual {v11, v12, v10, p1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1459626
    iget-object v0, p0, LX/9GF;->d:LX/9FA;

    .line 1459627
    iget-boolean v1, v0, LX/9FA;->c:Z

    move v0, v1

    .line 1459628
    if-eqz v0, :cond_3

    .line 1459629
    if-nez v8, :cond_2

    .line 1459630
    const v0, 0x267ccf29

    invoke-static {v0, v7}, LX/02F;->a(II)V

    .line 1459631
    :goto_3
    return-void

    :cond_1
    move-object v4, v5

    .line 1459632
    goto :goto_2

    .line 1459633
    :cond_2
    invoke-static {v8}, LX/182;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 1459634
    iget-object v1, p0, LX/9GF;->e:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;->e:LX/0bH;

    new-instance v2, LX/1Ne;

    invoke-direct {v2, v0}, LX/1Ne;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 1459635
    :goto_4
    const v0, -0x749f70af

    invoke-static {v0, v7}, LX/02F;->a(II)V

    goto :goto_3

    .line 1459636
    :cond_3
    if-nez v9, :cond_4

    .line 1459637
    const v0, -0x4437f3be

    invoke-static {v0, v7}, LX/02F;->a(II)V

    goto :goto_3

    .line 1459638
    :cond_4
    iget-object v0, p0, LX/9GF;->e:Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;->f:LX/1K9;

    new-instance v1, LX/8q4;

    iget-object v2, v9, LX/6PS;->b:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v3, v9, LX/6PS;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/8q4;-><init>(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/1K9;->a(LX/1KJ;)V

    goto :goto_4

    :cond_5
    iget-object v6, v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;->c:LX/189;

    invoke-virtual {v6, v1, v2, v3, v4}, LX/189;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;Z)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v6

    goto/16 :goto_0

    :cond_6
    iget-object v6, v0, Lcom/facebook/feedback/ui/rows/CommentPlaceInfoRemoveAttachmentClickListenerPartDefinition;->d:LX/20j;

    invoke-virtual {v6, v1, v3, v2, v4}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;Ljava/lang/String;Z)LX/6PS;

    move-result-object v6

    goto/16 :goto_1
.end method
