.class public final LX/A9p;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 1635894
    const/4 v9, 0x0

    .line 1635895
    const/4 v8, 0x0

    .line 1635896
    const/4 v7, 0x0

    .line 1635897
    const/4 v6, 0x0

    .line 1635898
    const/4 v5, 0x0

    .line 1635899
    const/4 v4, 0x0

    .line 1635900
    const/4 v3, 0x0

    .line 1635901
    const/4 v2, 0x0

    .line 1635902
    const/4 v1, 0x0

    .line 1635903
    const/4 v0, 0x0

    .line 1635904
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v10, v11, :cond_1

    .line 1635905
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1635906
    const/4 v0, 0x0

    .line 1635907
    :goto_0
    return v0

    .line 1635908
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1635909
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_a

    .line 1635910
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1635911
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1635912
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 1635913
    const-string v11, "ad_account"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 1635914
    invoke-static {p0, p1}, LX/A9i;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 1635915
    :cond_2
    const-string v11, "allowed_call_to_action_types"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1635916
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1635917
    :cond_3
    const-string v11, "available_audiences"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 1635918
    invoke-static {p0, p1}, LX/A9j;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1635919
    :cond_4
    const-string v11, "bid_amount"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 1635920
    const/4 v0, 0x1

    .line 1635921
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v6

    goto :goto_1

    .line 1635922
    :cond_5
    const-string v11, "call_to_action"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 1635923
    invoke-static {p0, p1}, LX/A9k;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1635924
    :cond_6
    const-string v11, "default_creative_spec"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 1635925
    invoke-static {p0, p1}, LX/A9l;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1635926
    :cond_7
    const-string v11, "default_target_spec"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 1635927
    invoke-static {p0, p1}, LX/AAU;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1635928
    :cond_8
    const-string v11, "duration_suggestions"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 1635929
    invoke-static {p0, p1}, LX/A9o;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 1635930
    :cond_9
    const-string v11, "pacing_type"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 1635931
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    goto/16 :goto_1

    .line 1635932
    :cond_a
    const/16 v10, 0x9

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1635933
    const/4 v10, 0x0

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 1635934
    const/4 v9, 0x1

    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 1635935
    const/4 v8, 0x2

    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 1635936
    if-eqz v0, :cond_b

    .line 1635937
    const/4 v0, 0x3

    const/4 v7, 0x0

    invoke-virtual {p1, v0, v6, v7}, LX/186;->a(III)V

    .line 1635938
    :cond_b
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1635939
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1635940
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1635941
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1635942
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1635943
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 7

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1635944
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1635945
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1635946
    if-eqz v0, :cond_0

    .line 1635947
    const-string v1, "ad_account"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635948
    invoke-static {p0, v0, p2}, LX/A9i;->a(LX/15i;ILX/0nX;)V

    .line 1635949
    :cond_0
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1635950
    if-eqz v0, :cond_1

    .line 1635951
    const-string v0, "allowed_call_to_action_types"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635952
    invoke-virtual {p0, p1, v3}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1635953
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1635954
    if-eqz v0, :cond_6

    .line 1635955
    const-string v1, "available_audiences"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635956
    const/4 v1, 0x0

    .line 1635957
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1635958
    invoke-virtual {p0, v0, v1, v1}, LX/15i;->a(III)I

    move-result v1

    .line 1635959
    if-eqz v1, :cond_2

    .line 1635960
    const-string v3, "count"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635961
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 1635962
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1635963
    if-eqz v1, :cond_5

    .line 1635964
    const-string v3, "edges"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635965
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1635966
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v5

    if-ge v3, v5, :cond_4

    .line 1635967
    invoke-virtual {p0, v1, v3}, LX/15i;->q(II)I

    move-result v5

    .line 1635968
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1635969
    const/4 v6, 0x0

    invoke-virtual {p0, v5, v6}, LX/15i;->g(II)I

    move-result v6

    .line 1635970
    if-eqz v6, :cond_3

    .line 1635971
    const-string v0, "node"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635972
    invoke-static {p0, v6, p2, p3}, LX/A9c;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1635973
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1635974
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1635975
    :cond_4
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1635976
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1635977
    :cond_6
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1635978
    if-eqz v0, :cond_7

    .line 1635979
    const-string v1, "bid_amount"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635980
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1635981
    :cond_7
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1635982
    if-eqz v0, :cond_8

    .line 1635983
    const-string v1, "call_to_action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635984
    invoke-static {p0, v0, p2}, LX/A9k;->a(LX/15i;ILX/0nX;)V

    .line 1635985
    :cond_8
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1635986
    if-eqz v0, :cond_12

    .line 1635987
    const-string v1, "default_creative_spec"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635988
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1635989
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1635990
    if-eqz v1, :cond_9

    .line 1635991
    const-string v2, "body"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635992
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1635993
    :cond_9
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1635994
    if-eqz v1, :cond_a

    .line 1635995
    const-string v2, "image_hash"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635996
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1635997
    :cond_a
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1635998
    if-eqz v1, :cond_b

    .line 1635999
    const-string v2, "image_url"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636000
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1636001
    :cond_b
    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1636002
    if-eqz v1, :cond_c

    .line 1636003
    const-string v2, "object_id"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636004
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1636005
    :cond_c
    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1636006
    if-eqz v1, :cond_11

    .line 1636007
    const-string v2, "object_story_spec"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636008
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1636009
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1636010
    if-eqz v2, :cond_10

    .line 1636011
    const-string v0, "link_data"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636012
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1636013
    const/4 v0, 0x0

    invoke-virtual {p0, v2, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1636014
    if-eqz v0, :cond_d

    .line 1636015
    const-string v1, "link"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636016
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1636017
    :cond_d
    const/4 v0, 0x1

    invoke-virtual {p0, v2, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1636018
    if-eqz v0, :cond_e

    .line 1636019
    const-string v1, "message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636020
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1636021
    :cond_e
    const/4 v0, 0x2

    invoke-virtual {p0, v2, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1636022
    if-eqz v0, :cond_f

    .line 1636023
    const-string v1, "picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636024
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1636025
    :cond_f
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1636026
    :cond_10
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1636027
    :cond_11
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1636028
    :cond_12
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1636029
    if-eqz v0, :cond_13

    .line 1636030
    const-string v1, "default_target_spec"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636031
    invoke-static {p0, v0, p2, p3}, LX/AAU;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1636032
    :cond_13
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1636033
    if-eqz v0, :cond_14

    .line 1636034
    const-string v1, "duration_suggestions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636035
    invoke-static {p0, v0, p2, p3}, LX/A9o;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1636036
    :cond_14
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 1636037
    if-eqz v0, :cond_15

    .line 1636038
    const-string v0, "pacing_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636039
    invoke-virtual {p0, p1, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1636040
    :cond_15
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1636041
    return-void
.end method
