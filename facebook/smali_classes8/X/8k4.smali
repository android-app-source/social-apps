.class public final LX/8k4;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1395413
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_3

    .line 1395414
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1395415
    :goto_0
    return v1

    .line 1395416
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1395417
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_2

    .line 1395418
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1395419
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1395420
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 1395421
    const-string v3, "owned_packs"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1395422
    const/4 v2, 0x0

    .line 1395423
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 1395424
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1395425
    :goto_2
    move v0, v2

    .line 1395426
    goto :goto_1

    .line 1395427
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1395428
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1395429
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    .line 1395430
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1395431
    :cond_5
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_6

    .line 1395432
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1395433
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1395434
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_5

    if-eqz v3, :cond_5

    .line 1395435
    const-string v4, "nodes"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1395436
    invoke-static {p0, p1}, LX/8kS;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_3

    .line 1395437
    :cond_6
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1395438
    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1395439
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_2

    :cond_7
    move v0, v2

    goto :goto_3
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1395440
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1395441
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1395442
    if-eqz v0, :cond_1

    .line 1395443
    const-string v1, "owned_packs"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1395444
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1395445
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1395446
    if-eqz v1, :cond_0

    .line 1395447
    const-string p1, "nodes"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1395448
    invoke-static {p0, v1, p2, p3}, LX/8kS;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1395449
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1395450
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1395451
    return-void
.end method
