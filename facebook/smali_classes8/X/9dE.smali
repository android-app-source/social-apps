.class public final LX/9dE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4oU;


# instance fields
.field public final synthetic a:LX/9dI;


# direct methods
.method public constructor <init>(LX/9dI;)V
    .locals 0

    .prologue
    .line 1517806
    iput-object p1, p0, LX/9dE;->a:LX/9dI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MotionEvent;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 1517818
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-eq v0, v2, :cond_0

    .line 1517819
    sget-object v0, LX/9dI;->a:Ljava/lang/String;

    const-string v1, "ScrollAwareScrollView::onTouchUp is triggered when action isn\'t cancel or up. action : %d"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1517820
    :cond_0
    iget-object v0, p0, LX/9dE;->a:LX/9dI;

    iget-object v0, v0, LX/9dI;->q:LX/5jK;

    invoke-virtual {v0}, LX/5jK;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1517821
    iget-object v0, p0, LX/9dE;->a:LX/9dI;

    invoke-virtual {v0, v4}, LX/9dI;->a(Z)V

    .line 1517822
    :cond_1
    return-void
.end method

.method public final a(Lcom/facebook/widget/ScrollingAwareScrollView;Landroid/view/MotionEvent;FF)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1517807
    iget-object v0, p0, LX/9dE;->a:LX/9dI;

    iget-object v0, v0, LX/9dI;->q:LX/5jK;

    invoke-virtual {v0}, LX/5jK;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1517808
    iget-object v0, p0, LX/9dE;->a:LX/9dI;

    sget-object v1, LX/5jJ;->NOT_SWIPING:LX/5jJ;

    invoke-static {v0, v1}, LX/9dI;->a$redex0(LX/9dI;LX/5jJ;)V

    .line 1517809
    :cond_0
    :goto_0
    return-void

    .line 1517810
    :cond_1
    iget-object v0, p0, LX/9dE;->a:LX/9dI;

    iget-object v0, v0, LX/9dI;->q:LX/5jK;

    invoke-virtual {v0}, LX/5jK;->h()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1517811
    iget-object v1, p0, LX/9dE;->a:LX/9dI;

    iget-object v0, p0, LX/9dE;->a:LX/9dI;

    iget-boolean v0, v0, LX/9dI;->t:Z

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    :goto_1
    iget-object v2, p0, LX/9dE;->a:LX/9dI;

    iget-boolean v2, v2, LX/9dI;->t:Z

    if-eqz v2, :cond_3

    :goto_2
    invoke-static {v1, v0, p3, v3, v3}, LX/9dI;->a$redex0(LX/9dI;FFZZ)V

    goto :goto_0

    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    goto :goto_1

    :cond_3
    move p3, p4

    goto :goto_2

    .line 1517812
    :cond_4
    iget-object v0, p0, LX/9dE;->a:LX/9dI;

    iget-object v0, v0, LX/9dI;->q:LX/5jK;

    invoke-virtual {v0}, LX/5jK;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1517813
    iget-boolean v0, p1, Lcom/facebook/widget/ScrollingAwareScrollView;->a:Z

    move v0, v0

    .line 1517814
    if-nez v0, :cond_0

    .line 1517815
    const/4 v0, 0x1

    .line 1517816
    iput-boolean v0, p1, Lcom/facebook/widget/ScrollingAwareScrollView;->a:Z

    .line 1517817
    goto :goto_0
.end method
