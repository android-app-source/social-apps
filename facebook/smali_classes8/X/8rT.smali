.class public final LX/8rT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/ufiservices/ui/CommentEditView;


# direct methods
.method public constructor <init>(Lcom/facebook/ufiservices/ui/CommentEditView;)V
    .locals 0

    .prologue
    .line 1408871
    iput-object p1, p0, LX/8rT;->a:Lcom/facebook/ufiservices/ui/CommentEditView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x54d65546

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1408872
    iget-object v1, p0, LX/8rT;->a:Lcom/facebook/ufiservices/ui/CommentEditView;

    iget-object v1, v1, Lcom/facebook/ufiservices/ui/CommentEditView;->g:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->clearFocus()V

    .line 1408873
    iget-object v1, p0, LX/8rT;->a:Lcom/facebook/ufiservices/ui/CommentEditView;

    iget-object v1, v1, Lcom/facebook/ufiservices/ui/CommentEditView;->a:Landroid/view/inputmethod/InputMethodManager;

    iget-object v2, p0, LX/8rT;->a:Lcom/facebook/ufiservices/ui/CommentEditView;

    invoke-virtual {v2}, Lcom/facebook/ufiservices/ui/CommentEditView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1408874
    iget-object v1, p0, LX/8rT;->a:Lcom/facebook/ufiservices/ui/CommentEditView;

    iget-object v1, v1, Lcom/facebook/ufiservices/ui/CommentEditView;->n:LX/8qE;

    iget-object v2, p0, LX/8rT;->a:Lcom/facebook/ufiservices/ui/CommentEditView;

    iget-object v2, v2, Lcom/facebook/ufiservices/ui/CommentEditView;->l:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v3, p0, LX/8rT;->a:Lcom/facebook/ufiservices/ui/CommentEditView;

    iget-object v3, v3, Lcom/facebook/ufiservices/ui/CommentEditView;->g:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-virtual {v3}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v3

    .line 1408875
    iget-object p0, v1, LX/8qE;->a:Lcom/facebook/ufiservices/flyout/EditCommentFragment;

    .line 1408876
    if-eqz v3, :cond_0

    .line 1408877
    invoke-static {p0, v2, v3}, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->a$redex0(Lcom/facebook/ufiservices/flyout/EditCommentFragment;Lcom/facebook/graphql/model/GraphQLComment;Landroid/text/Editable;)V

    .line 1408878
    :cond_0
    iget-object p0, v1, LX/8qE;->a:Lcom/facebook/ufiservices/flyout/EditCommentFragment;

    iget-object p0, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->w:LX/0hG;

    invoke-interface {p0}, LX/0hG;->S_()Z

    .line 1408879
    const v1, -0x94ce2c1

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
