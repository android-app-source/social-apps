.class public final LX/9XQ;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1502831
    const-class v1, Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel;

    const v0, -0x3dc09d54

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FetchAddableActionsQuery"

    const-string v6, "82cb5c336f89810d5f08f4035acb80dd"

    const-string v7, "node"

    const-string v8, "10155261262161729"

    const-string v9, "10155263176696729"

    .line 1502832
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1502833
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1502834
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1502835
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1502836
    sparse-switch v0, :sswitch_data_0

    .line 1502837
    :goto_0
    return-object p1

    .line 1502838
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1502839
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1502840
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1502841
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x71e86c6d -> :sswitch_1
        -0x2fe52f35 -> :sswitch_0
        0x41ee846d -> :sswitch_3
        0x78326898 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1502842
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 1502843
    :goto_1
    return v0

    .line 1502844
    :pswitch_0
    const-string v2, "3"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 1502845
    :pswitch_1
    const-string v0, "undefined"

    invoke-static {p2, v0}, LX/0wE;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x33
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
