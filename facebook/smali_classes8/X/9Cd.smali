.class public LX/9Cd;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;


# instance fields
.field public A:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

.field public B:Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

.field public final b:LX/1K9;

.field public final c:LX/3iK;

.field public final d:LX/3iL;

.field public final e:LX/0hx;

.field public final f:LX/0So;

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/sounds/FBSoundUtil;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/1Ck;

.field public final i:Landroid/support/v4/app/FragmentActivity;

.field public final j:LX/8DU;

.field public final k:LX/3iP;

.field public final l:LX/0TD;

.field public final m:Ljava/util/concurrent/Executor;

.field private final n:LX/2Yg;

.field public final o:LX/3iM;

.field public final p:LX/3iQ;

.field public final q:LX/2dj;

.field public final r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9Wc;",
            ">;"
        }
    .end annotation
.end field

.field public final s:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field public final t:LX/20i;

.field public final u:LX/20j;

.field public final v:LX/20h;

.field public final w:LX/1Kf;

.field public final x:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final y:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field

.field public final z:LX/9yI;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1454204
    const-class v0, LX/9Cd;

    sput-object v0, LX/9Cd;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/1K9;LX/3iG;LX/3iL;LX/0hx;LX/0So;LX/0Ot;LX/1Ck;Landroid/support/v4/app/FragmentActivity;LX/8DU;LX/3iP;LX/0TD;Ljava/util/concurrent/Executor;LX/2Yg;LX/3iM;LX/3iQ;LX/2dj;LX/0Ot;LX/0Ot;LX/20i;LX/20j;LX/20h;LX/1Kf;LX/0Ot;LX/0Ot;LX/9yI;)V
    .locals 2
    .param p11    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .param p12    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1K9;",
            "LX/3iG;",
            "LX/3iL;",
            "LX/0hx;",
            "LX/0So;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/sounds/FBSoundUtil;",
            ">;",
            "LX/1Ck;",
            "Landroid/support/v4/app/FragmentActivity;",
            "LX/8DU;",
            "LX/3iP;",
            "LX/0TD;",
            "Ljava/util/concurrent/Executor;",
            "LX/2Yg;",
            "LX/3iM;",
            "LX/3iQ;",
            "LX/2dj;",
            "LX/0Ot",
            "<",
            "LX/9Wc;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLActorCache;",
            "LX/20j;",
            "LX/20h;",
            "LX/1Kf;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;",
            "LX/9yI;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1454205
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1454206
    iput-object p1, p0, LX/9Cd;->b:LX/1K9;

    .line 1454207
    sget-object v1, Lcom/facebook/feedback/ui/FeedbackControllerParams;->a:Lcom/facebook/feedback/ui/FeedbackControllerParams;

    invoke-virtual {p2, v1}, LX/3iG;->a(Lcom/facebook/feedback/ui/FeedbackControllerParams;)LX/3iK;

    move-result-object v1

    iput-object v1, p0, LX/9Cd;->c:LX/3iK;

    .line 1454208
    iput-object p3, p0, LX/9Cd;->d:LX/3iL;

    .line 1454209
    iput-object p4, p0, LX/9Cd;->e:LX/0hx;

    .line 1454210
    iput-object p5, p0, LX/9Cd;->f:LX/0So;

    .line 1454211
    iput-object p6, p0, LX/9Cd;->g:LX/0Ot;

    .line 1454212
    iput-object p7, p0, LX/9Cd;->h:LX/1Ck;

    .line 1454213
    iput-object p8, p0, LX/9Cd;->i:Landroid/support/v4/app/FragmentActivity;

    .line 1454214
    iput-object p9, p0, LX/9Cd;->j:LX/8DU;

    .line 1454215
    iput-object p10, p0, LX/9Cd;->k:LX/3iP;

    .line 1454216
    iput-object p11, p0, LX/9Cd;->l:LX/0TD;

    .line 1454217
    iput-object p12, p0, LX/9Cd;->m:Ljava/util/concurrent/Executor;

    .line 1454218
    iput-object p13, p0, LX/9Cd;->n:LX/2Yg;

    .line 1454219
    move-object/from16 v0, p14

    iput-object v0, p0, LX/9Cd;->o:LX/3iM;

    .line 1454220
    move-object/from16 v0, p15

    iput-object v0, p0, LX/9Cd;->p:LX/3iQ;

    .line 1454221
    move-object/from16 v0, p16

    iput-object v0, p0, LX/9Cd;->q:LX/2dj;

    .line 1454222
    move-object/from16 v0, p17

    iput-object v0, p0, LX/9Cd;->r:LX/0Ot;

    .line 1454223
    move-object/from16 v0, p18

    iput-object v0, p0, LX/9Cd;->s:LX/0Ot;

    .line 1454224
    move-object/from16 v0, p19

    iput-object v0, p0, LX/9Cd;->t:LX/20i;

    .line 1454225
    move-object/from16 v0, p20

    iput-object v0, p0, LX/9Cd;->u:LX/20j;

    .line 1454226
    move-object/from16 v0, p21

    iput-object v0, p0, LX/9Cd;->v:LX/20h;

    .line 1454227
    move-object/from16 v0, p22

    iput-object v0, p0, LX/9Cd;->w:LX/1Kf;

    .line 1454228
    move-object/from16 v0, p23

    iput-object v0, p0, LX/9Cd;->x:LX/0Ot;

    .line 1454229
    move-object/from16 v0, p24

    iput-object v0, p0, LX/9Cd;->y:LX/0Ot;

    .line 1454230
    move-object/from16 v0, p25

    iput-object v0, p0, LX/9Cd;->z:LX/9yI;

    .line 1454231
    return-void
.end method

.method public static a(LX/9Cd;ILandroid/content/Context;LX/9CW;)LX/1L9;
    .locals 6
    .param p0    # LX/9Cd;
        .annotation build Landroid/support/annotation/AttrRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/content/Context;",
            "LX/9CW;",
            ")",
            "LX/1L9",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 1454232
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 1454233
    invoke-virtual {p2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    invoke-virtual {v1, p1, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 1454234
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1454235
    new-instance v2, Landroid/app/ProgressDialog;

    invoke-direct {v2, p2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 1454236
    invoke-virtual {v2, v0}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 1454237
    invoke-virtual {v2}, Landroid/app/ProgressDialog;->show()V

    .line 1454238
    iget-object v0, p0, LX/9Cd;->f:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    .line 1454239
    iget-object v0, p0, LX/9Cd;->e:LX/0hx;

    invoke-virtual {v0, v3}, LX/0hx;->a(Z)V

    .line 1454240
    new-instance v0, LX/9CV;

    move-object v1, p0

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, LX/9CV;-><init>(LX/9Cd;Landroid/app/ProgressDialog;LX/9CW;J)V

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLComment;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1454241
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/9Cd;
    .locals 28

    .prologue
    .line 1454242
    new-instance v2, LX/9Cd;

    invoke-static/range {p0 .. p0}, LX/1K9;->a(LX/0QB;)LX/1K9;

    move-result-object v3

    check-cast v3, LX/1K9;

    const-class v4, LX/3iG;

    move-object/from16 v0, p0

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/3iG;

    invoke-static/range {p0 .. p0}, LX/3iL;->a(LX/0QB;)LX/3iL;

    move-result-object v5

    check-cast v5, LX/3iL;

    invoke-static/range {p0 .. p0}, LX/0hx;->a(LX/0QB;)LX/0hx;

    move-result-object v6

    check-cast v6, LX/0hx;

    invoke-static/range {p0 .. p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v7

    check-cast v7, LX/0So;

    const/16 v8, 0x3567

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static/range {p0 .. p0}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v9

    check-cast v9, LX/1Ck;

    invoke-static/range {p0 .. p0}, LX/1No;->a(LX/0QB;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v10

    check-cast v10, Landroid/support/v4/app/FragmentActivity;

    invoke-static/range {p0 .. p0}, LX/8DU;->a(LX/0QB;)LX/8DU;

    move-result-object v11

    check-cast v11, LX/8DU;

    invoke-static/range {p0 .. p0}, LX/3iP;->a(LX/0QB;)LX/3iP;

    move-result-object v12

    check-cast v12, LX/3iP;

    invoke-static/range {p0 .. p0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v13

    check-cast v13, LX/0TD;

    invoke-static/range {p0 .. p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v14

    check-cast v14, Ljava/util/concurrent/Executor;

    invoke-static/range {p0 .. p0}, LX/2Yg;->a(LX/0QB;)LX/2Yg;

    move-result-object v15

    check-cast v15, LX/2Yg;

    invoke-static/range {p0 .. p0}, LX/3iM;->a(LX/0QB;)LX/3iM;

    move-result-object v16

    check-cast v16, LX/3iM;

    invoke-static/range {p0 .. p0}, LX/3iQ;->a(LX/0QB;)LX/3iQ;

    move-result-object v17

    check-cast v17, LX/3iQ;

    invoke-static/range {p0 .. p0}, LX/2dj;->a(LX/0QB;)LX/2dj;

    move-result-object v18

    check-cast v18, LX/2dj;

    const/16 v19, 0x2b4b

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v19

    const/16 v20, 0x19e

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v20

    invoke-static/range {p0 .. p0}, LX/20i;->a(LX/0QB;)LX/20i;

    move-result-object v21

    check-cast v21, LX/20i;

    invoke-static/range {p0 .. p0}, LX/20j;->a(LX/0QB;)LX/20j;

    move-result-object v22

    check-cast v22, LX/20j;

    invoke-static/range {p0 .. p0}, LX/20h;->a(LX/0QB;)LX/20h;

    move-result-object v23

    check-cast v23, LX/20h;

    invoke-static/range {p0 .. p0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v24

    check-cast v24, LX/1Kf;

    const/16 v25, 0x259

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v25

    const/16 v26, 0xbc

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v26

    invoke-static/range {p0 .. p0}, LX/9yI;->a(LX/0QB;)LX/9yI;

    move-result-object v27

    check-cast v27, LX/9yI;

    invoke-direct/range {v2 .. v27}, LX/9Cd;-><init>(LX/1K9;LX/3iG;LX/3iL;LX/0hx;LX/0So;LX/0Ot;LX/1Ck;Landroid/support/v4/app/FragmentActivity;LX/8DU;LX/3iP;LX/0TD;Ljava/util/concurrent/Executor;LX/2Yg;LX/3iM;LX/3iQ;LX/2dj;LX/0Ot;LX/0Ot;LX/20i;LX/20j;LX/20h;LX/1Kf;LX/0Ot;LX/0Ot;LX/9yI;)V

    .line 1454243
    return-object v2
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 6

    .prologue
    .line 1454244
    iget-object v0, p0, LX/9Cd;->p:LX/3iQ;

    iget-object v1, p0, LX/9Cd;->A:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    new-instance v2, LX/9CT;

    invoke-direct {v2, p0, p2}, LX/9CT;-><init>(LX/9Cd;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1454245
    iget-object v3, v0, LX/3iQ;->h:LX/20j;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    iget-object v5, v0, LX/3iQ;->b:LX/20i;

    invoke-virtual {v5}, LX/20i;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLActor;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    .line 1454246
    invoke-static {p1}, LX/4Vu;->a(Lcom/facebook/graphql/model/GraphQLComment;)LX/4Vu;

    move-result-object v4

    .line 1454247
    iput-object v3, v4, LX/4Vu;->p:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1454248
    move-object v4, v4

    .line 1454249
    invoke-virtual {v4}, LX/4Vu;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v4

    .line 1454250
    if-eqz v2, :cond_0

    .line 1454251
    invoke-interface {v2, v4}, LX/1L9;->a(Ljava/lang/Object;)V

    .line 1454252
    :cond_0
    invoke-static {}, Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;->a()LX/5HA;

    move-result-object v4

    .line 1454253
    iput-object v1, v4, LX/5HA;->d:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1454254
    move-object v4, v4

    .line 1454255
    iput-object v3, v4, LX/5HA;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1454256
    move-object v4, v4

    .line 1454257
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->s_()Z

    move-result v3

    .line 1454258
    iput-boolean v3, v4, LX/5HA;->b:Z

    .line 1454259
    move-object v3, v4

    .line 1454260
    iget-object v4, v0, LX/3iQ;->b:LX/20i;

    invoke-virtual {v4}, LX/20i;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v4

    .line 1454261
    iput-object v4, v3, LX/5HA;->c:Lcom/facebook/graphql/model/GraphQLActor;

    .line 1454262
    move-object v3, v3

    .line 1454263
    invoke-virtual {v3}, LX/5HA;->a()Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;

    move-result-object v3

    .line 1454264
    iget-object v4, v0, LX/3iQ;->d:LX/1Ck;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string p2, "toggle_comment_like_"

    invoke-direct {v5, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object p2, v0, LX/3iQ;->i:LX/3iV;

    invoke-virtual {p2, v3}, LX/3iV;->a(Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    new-instance p2, LX/95t;

    invoke-direct {p2, v0, v2, p1}, LX/95t;-><init>(LX/3iQ;LX/1L9;Lcom/facebook/graphql/model/GraphQLComment;)V

    invoke-virtual {v4, v5, v3, p2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1454265
    iget-object v3, v0, LX/3iQ;->a:LX/3iR;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    .line 1454266
    const-string v5, "comment_like"

    invoke-static {v3, v5, v4, v1}, LX/3iR;->a(LX/3iR;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V

    .line 1454267
    iget-object v3, v0, LX/3iQ;->l:LX/0ie;

    .line 1454268
    iget-object v4, v3, LX/0ie;->a:LX/0if;

    sget-object v5, LX/0ig;->ar:LX/0ih;

    sget-object p2, LX/8D1;->COMMENT_LIKE:LX/8D1;

    invoke-virtual {p2}, LX/8D1;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v4, v5, p2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1454269
    iget-object v0, p0, LX/9Cd;->B:Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    if-eqz v0, :cond_1

    .line 1454270
    iget-object v0, p0, LX/9Cd;->n:LX/2Yg;

    iget-object v1, p0, LX/9Cd;->B:Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    const-string v2, "beeper_caused_comment_like"

    invoke-virtual {v0, v1, v2}, LX/2Yg;->a(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;Ljava/lang/String;)V

    .line 1454271
    const/4 v0, 0x0

    iput-object v0, p0, LX/9Cd;->B:Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    .line 1454272
    :cond_1
    return-void
.end method
