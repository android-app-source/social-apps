.class public final LX/9Zk;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    .line 1511712
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1511713
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1511714
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1511715
    const/4 v2, 0x0

    .line 1511716
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_6

    .line 1511717
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1511718
    :goto_1
    move v1, v2

    .line 1511719
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1511720
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 1511721
    :cond_1
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1511722
    :cond_2
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_5

    .line 1511723
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1511724
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1511725
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_2

    if-eqz v5, :cond_2

    .line 1511726
    const-string v6, "label"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1511727
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_2

    .line 1511728
    :cond_3
    const-string v6, "type"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1511729
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    goto :goto_2

    .line 1511730
    :cond_4
    const-string v6, "value"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1511731
    invoke-static {p0, p1}, LX/9Zj;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_2

    .line 1511732
    :cond_5
    const/4 v5, 0x3

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1511733
    invoke-virtual {p1, v2, v4}, LX/186;->b(II)V

    .line 1511734
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v3}, LX/186;->b(II)V

    .line 1511735
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, LX/186;->b(II)V

    .line 1511736
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_1

    :cond_6
    move v1, v2

    move v3, v2

    move v4, v2

    goto :goto_2
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 1511737
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1511738
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1511739
    if-eqz v0, :cond_0

    .line 1511740
    const-string v1, "label"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1511741
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1511742
    :cond_0
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1511743
    if-eqz v0, :cond_1

    .line 1511744
    const-string v0, "type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1511745
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1511746
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1511747
    if-eqz v0, :cond_7

    .line 1511748
    const-string v1, "value"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1511749
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1511750
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1511751
    if-eqz v1, :cond_5

    .line 1511752
    const-string v2, "ranges"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1511753
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1511754
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v3

    if-ge v2, v3, :cond_4

    .line 1511755
    invoke-virtual {p0, v1, v2}, LX/15i;->q(II)I

    move-result v3

    const/4 p3, 0x0

    .line 1511756
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1511757
    invoke-virtual {p0, v3, p3, p3}, LX/15i;->a(III)I

    move-result v4

    .line 1511758
    if-eqz v4, :cond_2

    .line 1511759
    const-string p1, "length"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1511760
    invoke-virtual {p2, v4}, LX/0nX;->b(I)V

    .line 1511761
    :cond_2
    const/4 v4, 0x1

    invoke-virtual {p0, v3, v4, p3}, LX/15i;->a(III)I

    move-result v4

    .line 1511762
    if-eqz v4, :cond_3

    .line 1511763
    const-string p1, "offset"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1511764
    invoke-virtual {p2, v4}, LX/0nX;->b(I)V

    .line 1511765
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1511766
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1511767
    :cond_4
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1511768
    :cond_5
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1511769
    if-eqz v1, :cond_6

    .line 1511770
    const-string v2, "text"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1511771
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1511772
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1511773
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1511774
    return-void
.end method
