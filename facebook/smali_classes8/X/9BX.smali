.class public final LX/9BX;
.super LX/1cr;
.source ""


# instance fields
.field public final synthetic b:LX/9BY;


# direct methods
.method public constructor <init>(LX/9BY;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1451781
    iput-object p1, p0, LX/9BX;->b:LX/9BY;

    .line 1451782
    invoke-direct {p0, p2}, LX/1cr;-><init>(Landroid/view/View;)V

    .line 1451783
    return-void
.end method


# virtual methods
.method public final a(FF)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1451768
    const/4 v1, 0x2

    new-array v1, v1, [I

    .line 1451769
    iget-object v2, p0, LX/9BX;->b:LX/9BY;

    invoke-virtual {v2, v1}, LX/9BY;->getLocationOnScreen([I)V

    .line 1451770
    aget v2, v1, v0

    int-to-float v2, v2

    add-float/2addr v2, p1

    .line 1451771
    const/4 v3, 0x1

    aget v1, v1, v3

    int-to-float v1, v1

    add-float/2addr v1, p2

    .line 1451772
    iget-object v3, p0, LX/9BX;->b:LX/9BY;

    invoke-virtual {v3, v2, v1}, LX/9BY;->a(FF)V

    .line 1451773
    iget-object v1, p0, LX/9BX;->b:LX/9BY;

    invoke-virtual {v1}, LX/9BY;->getFaceConfigs()LX/0Px;

    move-result-object v2

    move v1, v0

    .line 1451774
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1451775
    iget-object v0, p0, LX/9BX;->b:LX/9BY;

    invoke-virtual {v0}, LX/9BY;->getCurrentReaction()LX/1zt;

    move-result-object v3

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9BW;

    .line 1451776
    iget-object p1, v0, LX/9BW;->c:LX/1zt;

    move-object v0, p1

    .line 1451777
    if-ne v3, v0, :cond_0

    .line 1451778
    :goto_1
    return v1

    .line 1451779
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1451780
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    goto :goto_1
.end method

.method public final a(ILX/3sp;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1451741
    const-class v0, LX/9Be;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/3sp;->b(Ljava/lang/CharSequence;)V

    .line 1451742
    iget-object v0, p0, LX/9BX;->b:LX/9BY;

    .line 1451743
    sget-object v1, LX/3sp;->a:LX/3sg;

    iget-object v4, p2, LX/3sp;->b:Ljava/lang/Object;

    invoke-interface {v1, v4, v0, p1}, LX/3sg;->c(Ljava/lang/Object;Landroid/view/View;I)V

    .line 1451744
    iget-object v0, p0, LX/9BX;->b:LX/9BY;

    invoke-virtual {v0}, LX/9BY;->getFaceConfigs()LX/0Px;

    move-result-object v0

    .line 1451745
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ne p1, v1, :cond_0

    .line 1451746
    iget-object v0, p0, LX/9BX;->b:LX/9BY;

    iget-object v0, v0, LX/9BY;->p:Ljava/lang/String;

    invoke-virtual {p2, v0}, LX/3sp;->d(Ljava/lang/CharSequence;)V

    .line 1451747
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v3, v3, v2, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p2, v0}, LX/3sp;->b(Landroid/graphics/Rect;)V

    .line 1451748
    :goto_0
    const/16 v0, 0x10

    invoke-virtual {p2, v0}, LX/3sp;->a(I)V

    .line 1451749
    invoke-virtual {p2, v2}, LX/3sp;->f(Z)V

    .line 1451750
    return-void

    .line 1451751
    :cond_0
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9BW;

    .line 1451752
    iget-object v1, v0, LX/9BW;->e:Ljava/lang/String;

    move-object v0, v1

    .line 1451753
    invoke-virtual {p2, v0}, LX/3sp;->d(Ljava/lang/CharSequence;)V

    .line 1451754
    iget-object v0, p0, LX/9BX;->b:LX/9BY;

    invoke-virtual {v0, p1}, LX/9BY;->a(I)Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/3sp;->b(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public final a(ILandroid/view/accessibility/AccessibilityEvent;)V
    .locals 2

    .prologue
    .line 1451761
    iget-object v0, p0, LX/9BX;->b:LX/9BY;

    invoke-virtual {v0}, LX/9BY;->getFaceConfigs()LX/0Px;

    move-result-object v0

    .line 1451762
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ne p1, v1, :cond_0

    .line 1451763
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, LX/9BX;->b:LX/9BY;

    iget-object v1, v1, LX/9BY;->p:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1451764
    :goto_0
    return-void

    .line 1451765
    :cond_0
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9BW;

    .line 1451766
    iget-object p0, v0, LX/9BW;->e:Ljava/lang/String;

    move-object v0, p0

    .line 1451767
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1451756
    iget-object v0, p0, LX/9BX;->b:LX/9BY;

    invoke-virtual {v0}, LX/9BY;->getFaceConfigs()LX/0Px;

    move-result-object v1

    .line 1451757
    const/4 v0, 0x0

    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    if-ge v0, v2, :cond_0

    .line 1451758
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1451759
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1451760
    :cond_0
    return-void
.end method

.method public final b(II)Z
    .locals 1

    .prologue
    .line 1451755
    const/4 v0, 0x0

    return v0
.end method
