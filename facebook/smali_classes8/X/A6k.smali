.class public final LX/A6k;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/transliteration/TransliterationFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/transliteration/TransliterationFragment;)V
    .locals 0

    .prologue
    .line 1624750
    iput-object p1, p0, LX/A6k;->a:Lcom/facebook/transliteration/TransliterationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x76cc9a32

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1624751
    iget-object v1, p0, LX/A6k;->a:Lcom/facebook/transliteration/TransliterationFragment;

    iget-object v1, v1, Lcom/facebook/transliteration/TransliterationFragment;->a:Lcom/facebook/transliteration/gridview/CharacterMapGridFragment;

    if-nez v1, :cond_0

    .line 1624752
    iget-object v1, p0, LX/A6k;->a:Lcom/facebook/transliteration/TransliterationFragment;

    iget-object v1, v1, Lcom/facebook/transliteration/TransliterationFragment;->f:LX/A6e;

    .line 1624753
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1624754
    iget-boolean v2, v1, LX/A6e;->b:Z

    if-nez v2, :cond_1

    move-object v2, v5

    .line 1624755
    :goto_0
    move-object v1, v2

    .line 1624756
    new-instance v2, LX/A6j;

    invoke-direct {v2, p0}, LX/A6j;-><init>(LX/A6k;)V

    .line 1624757
    iget-object v3, p0, LX/A6k;->a:Lcom/facebook/transliteration/TransliterationFragment;

    .line 1624758
    new-instance v5, Lcom/facebook/transliteration/gridview/CharacterMapGridFragment;

    invoke-direct {v5}, Lcom/facebook/transliteration/gridview/CharacterMapGridFragment;-><init>()V

    .line 1624759
    iput-object v1, v5, Lcom/facebook/transliteration/gridview/CharacterMapGridFragment;->o:Ljava/util/List;

    .line 1624760
    iput-object v2, v5, Lcom/facebook/transliteration/gridview/CharacterMapGridFragment;->p:Landroid/widget/AdapterView$OnItemClickListener;

    .line 1624761
    move-object v1, v5

    .line 1624762
    iput-object v1, v3, Lcom/facebook/transliteration/TransliterationFragment;->a:Lcom/facebook/transliteration/gridview/CharacterMapGridFragment;

    .line 1624763
    :cond_0
    iget-object v1, p0, LX/A6k;->a:Lcom/facebook/transliteration/TransliterationFragment;

    iget-object v1, v1, Lcom/facebook/transliteration/TransliterationFragment;->g:LX/A6X;

    invoke-virtual {v1}, LX/A6X;->a()V

    .line 1624764
    iget-object v1, p0, LX/A6k;->a:Lcom/facebook/transliteration/TransliterationFragment;

    iget-object v1, v1, Lcom/facebook/transliteration/TransliterationFragment;->a:Lcom/facebook/transliteration/gridview/CharacterMapGridFragment;

    iget-object v2, p0, LX/A6k;->a:Lcom/facebook/transliteration/TransliterationFragment;

    .line 1624765
    iget-object v3, v2, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v2, v3

    .line 1624766
    sget-object v3, Lcom/facebook/transliteration/gridview/CharacterMapGridFragment;->m:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1624767
    const v1, 0x1cdd3957

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1624768
    :cond_1
    iget-object v2, v1, LX/A6e;->d:LX/0P1;

    if-nez v2, :cond_2

    .line 1624769
    iget-object v2, v1, LX/A6e;->i:LX/A6r;

    invoke-interface {v2}, LX/A6r;->a()LX/0P1;

    move-result-object v2

    iput-object v2, v1, LX/A6e;->d:LX/0P1;

    .line 1624770
    :cond_2
    iget-object v2, v1, LX/A6e;->d:LX/0P1;

    invoke-virtual {v2}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v2

    invoke-virtual {v2}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 1624771
    new-instance p1, LX/A7C;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {p1, v3, v2}, LX/A7C;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    move-object v2, v5

    .line 1624772
    goto :goto_0
.end method
