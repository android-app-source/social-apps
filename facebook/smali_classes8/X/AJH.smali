.class public LX/AJH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/AJX;

.field public final c:LX/BMr;

.field public final d:LX/1Ck;

.field public final e:LX/7gT;

.field public final f:LX/AIu;

.field public final g:LX/0aG;

.field public final h:LX/0hg;

.field public i:Ljava/lang/String;

.field public final j:Landroid/text/TextWatcher;

.field public final k:LX/AJF;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1660972
    const-class v0, LX/AJH;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AJH;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/AIu;LX/AJX;LX/BMr;LX/1Ck;LX/7gT;LX/0aG;LX/0hg;)V
    .locals 1
    .param p1    # LX/AIu;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1660973
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1660974
    new-instance v0, LX/AJE;

    invoke-direct {v0, p0}, LX/AJE;-><init>(LX/AJH;)V

    iput-object v0, p0, LX/AJH;->j:Landroid/text/TextWatcher;

    .line 1660975
    new-instance v0, LX/AJF;

    invoke-direct {v0, p0}, LX/AJF;-><init>(LX/AJH;)V

    iput-object v0, p0, LX/AJH;->k:LX/AJF;

    .line 1660976
    iput-object p2, p0, LX/AJH;->b:LX/AJX;

    .line 1660977
    iput-object p3, p0, LX/AJH;->c:LX/BMr;

    .line 1660978
    iput-object p4, p0, LX/AJH;->d:LX/1Ck;

    .line 1660979
    iput-object p5, p0, LX/AJH;->e:LX/7gT;

    .line 1660980
    iput-object p1, p0, LX/AJH;->f:LX/AIu;

    .line 1660981
    iput-object p6, p0, LX/AJH;->g:LX/0aG;

    .line 1660982
    iput-object p7, p0, LX/AJH;->h:LX/0hg;

    .line 1660983
    return-void
.end method


# virtual methods
.method public final d()V
    .locals 1

    .prologue
    .line 1660984
    iget-object v0, p0, LX/AJH;->d:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 1660985
    return-void
.end method
