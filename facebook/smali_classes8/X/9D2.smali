.class public final LX/9D2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/api/ufiservices/FetchSingleCommentParams;

.field public final synthetic b:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

.field public final synthetic c:Lcom/facebook/feedback/ui/CommentPermalinkEscapeHatchView;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/CommentPermalinkEscapeHatchView;Lcom/facebook/api/ufiservices/FetchSingleCommentParams;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V
    .locals 0

    .prologue
    .line 1454675
    iput-object p1, p0, LX/9D2;->c:Lcom/facebook/feedback/ui/CommentPermalinkEscapeHatchView;

    iput-object p2, p0, LX/9D2;->a:Lcom/facebook/api/ufiservices/FetchSingleCommentParams;

    iput-object p3, p0, LX/9D2;->b:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0xd3d1c5

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1454676
    new-instance v1, LX/89k;

    invoke-direct {v1}, LX/89k;-><init>()V

    iget-object v2, p0, LX/9D2;->a:Lcom/facebook/api/ufiservices/FetchSingleCommentParams;

    .line 1454677
    iget-object v3, v2, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->d:Ljava/lang/String;

    move-object v2, v3

    .line 1454678
    iput-object v2, v1, LX/89k;->b:Ljava/lang/String;

    .line 1454679
    move-object v1, v1

    .line 1454680
    iget-object v2, p0, LX/9D2;->a:Lcom/facebook/api/ufiservices/FetchSingleCommentParams;

    .line 1454681
    iget-object v3, v2, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->e:Ljava/lang/String;

    move-object v2, v3

    .line 1454682
    iput-object v2, v1, LX/89k;->c:Ljava/lang/String;

    .line 1454683
    move-object v1, v1

    .line 1454684
    iget-object v2, p0, LX/9D2;->b:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1454685
    iput-object v2, v1, LX/89k;->p:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1454686
    move-object v1, v1

    .line 1454687
    invoke-virtual {v1}, LX/89k;->a()Lcom/facebook/ipc/feed/PermalinkStoryIdParams;

    move-result-object v1

    .line 1454688
    iget-object v2, p0, LX/9D2;->c:Lcom/facebook/feedback/ui/CommentPermalinkEscapeHatchView;

    iget-object v2, v2, Lcom/facebook/feedback/ui/CommentPermalinkEscapeHatchView;->c:LX/0hy;

    invoke-interface {v2, v1}, LX/0hy;->a(Lcom/facebook/ipc/feed/PermalinkStoryIdParams;)Landroid/content/Intent;

    move-result-object v1

    .line 1454689
    iget-object v2, p0, LX/9D2;->c:Lcom/facebook/feedback/ui/CommentPermalinkEscapeHatchView;

    iget-object v2, v2, Lcom/facebook/feedback/ui/CommentPermalinkEscapeHatchView;->a:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/9D2;->c:Lcom/facebook/feedback/ui/CommentPermalinkEscapeHatchView;

    invoke-virtual {v3}, Lcom/facebook/feedback/ui/CommentPermalinkEscapeHatchView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1454690
    const v1, -0xcae9232

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
