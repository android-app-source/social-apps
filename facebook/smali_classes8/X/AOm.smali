.class public LX/AOm;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/AOl;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPlaceListItem;",
            ">;"
        }
    .end annotation
.end field

.field public c:Z

.field public d:LX/AOi;

.field public final e:LX/9Er;

.field private final f:LX/1e2;


# direct methods
.method public constructor <init>(LX/9Er;LX/1e2;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1669366
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1669367
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/AOm;->b:Ljava/util/List;

    .line 1669368
    iput-object p1, p0, LX/AOm;->e:LX/9Er;

    .line 1669369
    iput-object p2, p0, LX/AOm;->f:LX/1e2;

    .line 1669370
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 6

    .prologue
    .line 1669371
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031386

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 1669372
    const v0, 0x7f0d2d1f

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 1669373
    new-instance v0, LX/AOl;

    const v3, 0x7f0d2d20

    invoke-virtual {v2, v3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f0d2d21

    invoke-virtual {v2, v4}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f0d2d22

    invoke-virtual {v2, v5}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    invoke-direct/range {v0 .. v5}, LX/AOl;-><init>(Landroid/widget/LinearLayout;Lcom/facebook/fbui/widget/layout/ImageBlockLayout;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;)V

    return-object v0
.end method

.method public final a(LX/1a1;I)V
    .locals 6

    .prologue
    .line 1669374
    check-cast p1, LX/AOl;

    .line 1669375
    iget-object v0, p0, LX/AOm;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlaceListItem;

    .line 1669376
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    .line 1669377
    iget-object v1, p1, LX/AOl;->m:Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1669378
    iget-object v1, p1, LX/AOl;->n:Landroid/widget/TextView;

    iget-object v3, p0, LX/AOm;->e:LX/9Er;

    const/4 v4, 0x1

    invoke-virtual {v3, v2, v4}, LX/9Er;->a(Lcom/facebook/graphql/model/GraphQLPage;I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1669379
    iget-object v1, p1, LX/AOl;->o:Landroid/widget/TextView;

    .line 1669380
    iget-object v3, p0, LX/AOm;->b:Ljava/util/List;

    invoke-interface {v3, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLPlaceListItem;

    .line 1669381
    iget-object v4, p0, LX/AOm;->e:LX/9Er;

    invoke-virtual {v4, v3}, LX/9Er;->a(Lcom/facebook/graphql/model/GraphQLPlaceListItem;)Ljava/lang/CharSequence;

    move-result-object v3

    move-object v3, v3

    .line 1669382
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1669383
    iget-object v3, p1, LX/AOl;->l:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-static {v1}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v1

    :goto_0
    invoke-virtual {v3, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 1669384
    const-string v1, "full_map_listview"

    .line 1669385
    iget-object v3, p1, LX/AOl;->l:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iget-object v4, p0, LX/AOm;->e:LX/9Er;

    iget-object v5, p0, LX/AOm;->a:Ljava/lang/String;

    invoke-virtual {v4, v2, v5, v1}, LX/9Er;->a(Lcom/facebook/graphql/model/GraphQLPage;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1669386
    iget-object v1, p0, LX/AOm;->f:LX/1e2;

    .line 1669387
    iget-object v2, v1, LX/1e2;->a:LX/0Uh;

    const/16 v3, 0x63b

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    move v1, v2

    .line 1669388
    if-eqz v1, :cond_0

    iget-boolean v1, p0, LX/AOm;->c:Z

    if-eqz v1, :cond_0

    .line 1669389
    iget-object v1, p1, LX/AOl;->l:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 1669390
    new-instance v2, LX/AOk;

    invoke-direct {v2, p0, v0}, LX/AOk;-><init>(LX/AOm;Lcom/facebook/graphql/model/GraphQLPlaceListItem;)V

    move-object v0, v2

    .line 1669391
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1669392
    :cond_0
    return-void

    .line 1669393
    :cond_1
    sget-object v1, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1669394
    iget-object v0, p0, LX/AOm;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
