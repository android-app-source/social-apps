.class public final LX/8zV;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/8zW;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

.field public b:LX/5LG;

.field public c:Z

.field public d:LX/90B;

.field public e:LX/90D;

.field public final synthetic f:LX/8zW;


# direct methods
.method public constructor <init>(LX/8zW;)V
    .locals 1

    .prologue
    .line 1427493
    iput-object p1, p0, LX/8zV;->f:LX/8zW;

    .line 1427494
    move-object v0, p1

    .line 1427495
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1427496
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1427497
    const-string v0, "MinutiaeObjectComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1427498
    if-ne p0, p1, :cond_1

    .line 1427499
    :cond_0
    :goto_0
    return v0

    .line 1427500
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1427501
    goto :goto_0

    .line 1427502
    :cond_3
    check-cast p1, LX/8zV;

    .line 1427503
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1427504
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1427505
    if-eq v2, v3, :cond_0

    .line 1427506
    iget-object v2, p0, LX/8zV;->a:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/8zV;->a:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    iget-object v3, p1, LX/8zV;->a:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1427507
    goto :goto_0

    .line 1427508
    :cond_5
    iget-object v2, p1, LX/8zV;->a:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    if-nez v2, :cond_4

    .line 1427509
    :cond_6
    iget-object v2, p0, LX/8zV;->b:LX/5LG;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/8zV;->b:LX/5LG;

    iget-object v3, p1, LX/8zV;->b:LX/5LG;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1427510
    goto :goto_0

    .line 1427511
    :cond_8
    iget-object v2, p1, LX/8zV;->b:LX/5LG;

    if-nez v2, :cond_7

    .line 1427512
    :cond_9
    iget-boolean v2, p0, LX/8zV;->c:Z

    iget-boolean v3, p1, LX/8zV;->c:Z

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 1427513
    goto :goto_0

    .line 1427514
    :cond_a
    iget-object v2, p0, LX/8zV;->d:LX/90B;

    if-eqz v2, :cond_c

    iget-object v2, p0, LX/8zV;->d:LX/90B;

    iget-object v3, p1, LX/8zV;->d:LX/90B;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    .line 1427515
    goto :goto_0

    .line 1427516
    :cond_c
    iget-object v2, p1, LX/8zV;->d:LX/90B;

    if-nez v2, :cond_b

    .line 1427517
    :cond_d
    iget-object v2, p0, LX/8zV;->e:LX/90D;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/8zV;->e:LX/90D;

    iget-object v3, p1, LX/8zV;->e:LX/90D;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1427518
    goto :goto_0

    .line 1427519
    :cond_e
    iget-object v2, p1, LX/8zV;->e:LX/90D;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
