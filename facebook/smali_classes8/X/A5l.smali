.class public final LX/A5l;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLInterfaces$FBPersonFriendTagSuggestionsQuery$;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0TF;

.field public final synthetic b:LX/0TF;

.field public final synthetic c:LX/A5m;


# direct methods
.method public constructor <init>(LX/A5m;LX/0TF;LX/0TF;)V
    .locals 0

    .prologue
    .line 1622223
    iput-object p1, p0, LX/A5l;->c:LX/A5m;

    iput-object p2, p0, LX/A5l;->a:LX/0TF;

    iput-object p3, p0, LX/A5l;->b:LX/0TF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1622248
    iget-object v0, p0, LX/A5l;->c:LX/A5m;

    const-string v1, "fail"

    invoke-static {v0, v1}, LX/A5m;->a$redex0(LX/A5m;Ljava/lang/String;)V

    .line 1622249
    iget-object v0, p0, LX/A5l;->c:LX/A5m;

    iget-object v0, v0, LX/A5m;->f:Landroid/os/Handler;

    iget-object v1, p0, LX/A5l;->c:LX/A5m;

    iget-object v1, v1, LX/A5m;->j:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1622250
    iget-object v0, p0, LX/A5l;->b:LX/0TF;

    if-eqz v0, :cond_0

    .line 1622251
    iget-object v0, p0, LX/A5l;->b:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 1622252
    :cond_0
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1622224
    check-cast p1, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel;

    .line 1622225
    iget-object v0, p0, LX/A5l;->c:LX/A5m;

    iget-object v0, v0, LX/A5m;->f:Landroid/os/Handler;

    iget-object v1, p0, LX/A5l;->c:LX/A5m;

    iget-object v1, v1, LX/A5m;->j:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1622226
    invoke-virtual {p1}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel;->a()LX/0Px;

    move-result-object v0

    .line 1622227
    if-eqz v0, :cond_0

    .line 1622228
    iget-object v1, p0, LX/A5l;->c:LX/A5m;

    invoke-static {v1, v0}, LX/A5m;->a(LX/A5m;LX/0Px;)LX/0Px;

    move-result-object v0

    .line 1622229
    iget-object v1, p0, LX/A5l;->c:LX/A5m;

    iget-object v1, v1, LX/A5m;->d:LX/A5k;

    .line 1622230
    iput-object v0, v1, LX/A5k;->b:LX/0Px;

    .line 1622231
    iget-object v3, v1, LX/A5k;->f:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v3

    iput-wide v3, v1, LX/A5k;->e:J

    .line 1622232
    iget-object v1, p0, LX/A5l;->a:LX/0TF;

    if-eqz v1, :cond_0

    .line 1622233
    iget-object v1, p0, LX/A5l;->a:LX/0TF;

    invoke-interface {v1, v0}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 1622234
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel;->b()Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1622235
    iget-object v0, p0, LX/A5l;->c:LX/A5m;

    const-string v1, "success"

    invoke-static {v0, v1}, LX/A5m;->a$redex0(LX/A5m;Ljava/lang/String;)V

    .line 1622236
    iget-object v0, p0, LX/A5l;->c:LX/A5m;

    invoke-static {v0, p1}, LX/A5m;->a$redex0(LX/A5m;Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel;)LX/0Px;

    move-result-object v0

    .line 1622237
    invoke-virtual {p1}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel;->b()Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 1622238
    iget-object v2, p0, LX/A5l;->c:LX/A5m;

    iget-object v2, v2, LX/A5m;->d:LX/A5k;

    .line 1622239
    iput-object v0, v2, LX/A5k;->a:LX/0Px;

    .line 1622240
    iput-object v1, v2, LX/A5k;->c:Ljava/lang/String;

    .line 1622241
    iget-object v3, v2, LX/A5k;->f:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v3

    iput-wide v3, v2, LX/A5k;->d:J

    .line 1622242
    iget-object v1, p0, LX/A5l;->b:LX/0TF;

    if-eqz v1, :cond_1

    .line 1622243
    iget-object v1, p0, LX/A5l;->b:LX/0TF;

    invoke-interface {v1, v0}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 1622244
    :cond_1
    :goto_0
    return-void

    .line 1622245
    :cond_2
    iget-object v0, p0, LX/A5l;->b:LX/0TF;

    if-eqz v0, :cond_1

    .line 1622246
    iget-object v0, p0, LX/A5l;->c:LX/A5m;

    const-string v1, "empty_list"

    invoke-static {v0, v1}, LX/A5m;->a$redex0(LX/A5m;Ljava/lang/String;)V

    .line 1622247
    iget-object v0, p0, LX/A5l;->b:LX/0TF;

    new-instance v1, Ljava/lang/Throwable;

    const-string v2, "Empty list retrieved"

    invoke-direct {v1, v2}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
