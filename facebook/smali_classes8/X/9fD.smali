.class public LX/9fD;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private c:LX/0gc;

.field private d:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1521320
    const-class v0, LX/9fD;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/9fD;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0gc;)V
    .locals 2

    .prologue
    .line 1521321
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1521322
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/9fD;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 1521323
    new-instance v0, Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/9fD;->d:Ljava/lang/ref/WeakReference;

    .line 1521324
    iput-object p1, p0, LX/9fD;->c:LX/0gc;

    .line 1521325
    return-void
.end method


# virtual methods
.method public final a(LX/9el;)V
    .locals 3

    .prologue
    .line 1521326
    new-instance v0, Ljava/lang/ref/WeakReference;

    iget-object v1, p0, LX/9fD;->c:LX/0gc;

    sget-object v2, LX/9fD;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/9fD;->d:Ljava/lang/ref/WeakReference;

    .line 1521327
    iget-object v0, p0, LX/9fD;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1521328
    iget-object v0, p0, LX/9fD;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    .line 1521329
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1521330
    iput-object p1, v0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->r:LX/9el;

    .line 1521331
    :cond_0
    return-void
.end method

.method public final a(Landroid/net/Uri;IILcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;LX/9el;Ljava/util/List;LX/9fh;)V
    .locals 8
    .param p6    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # LX/9fh;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "II",
            "Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;",
            "LX/9el;",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/RectF;",
            ">;",
            "LX/9fh;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1521332
    invoke-static {p5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1521333
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1521334
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1521335
    if-lez p2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1521336
    if-lez p3, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1521337
    iget-object v0, p0, LX/9fD;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1521338
    iget-object v0, p0, LX/9fD;->c:LX/0gc;

    sget-object v1, LX/9fD;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1521339
    iget-object v0, p0, LX/9fD;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1521340
    :cond_0
    :goto_2
    return-void

    .line 1521341
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1521342
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 1521343
    :cond_3
    new-instance v0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;

    invoke-direct {v0}, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;-><init>()V

    move-object v1, p4

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p5

    move-object v6, p6

    move-object v7, p7

    .line 1521344
    iput-object v5, v0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->r:LX/9el;

    .line 1521345
    invoke-virtual {v2}, Landroid/net/Uri;->isAbsolute()Z

    move-result p1

    if-eqz p1, :cond_4

    :goto_3
    iput-object v2, v0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->o:Landroid/net/Uri;

    .line 1521346
    iput v3, v0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->p:I

    .line 1521347
    iput v4, v0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->q:I

    .line 1521348
    iget-object p1, v1, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->k:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object p1, p1

    .line 1521349
    if-nez p1, :cond_5

    invoke-static {}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->newBuilder()Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object p1

    :goto_4
    iput-object p1, v0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->t:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1521350
    iput-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->L:Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    .line 1521351
    iput-object v6, v0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->J:Ljava/util/List;

    .line 1521352
    const/4 p1, 0x1

    iput-boolean p1, v0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->s:Z

    .line 1521353
    iput-object v7, v0, Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;->K:LX/9fh;

    .line 1521354
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, LX/9fD;->d:Ljava/lang/ref/WeakReference;

    .line 1521355
    iget-object v1, p0, LX/9fD;->c:LX/0gc;

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    sget-object v2, LX/9fD;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, LX/0hH;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->c()I

    .line 1521356
    iget-object v0, p0, LX/9fD;->c:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->b()Z

    .line 1521357
    iget-object v0, p0, LX/9fD;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_2

    .line 1521358
    :cond_4
    new-instance p1, Ljava/io/File;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    goto :goto_3

    .line 1521359
    :cond_5
    iget-object p1, v1, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->k:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object p1, p1

    .line 1521360
    goto :goto_4
.end method
