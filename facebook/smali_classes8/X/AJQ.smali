.class public LX/AJQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/93y;

.field public final b:LX/AJA;

.field public c:LX/93t;

.field public d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

.field public e:LX/0fO;

.field public f:LX/7gT;

.field public g:LX/AJK;

.field public final h:LX/93W;

.field public final i:LX/93q;

.field public final j:LX/93w;

.field public final k:LX/93x;


# direct methods
.method public constructor <init>(LX/0fO;LX/93y;LX/7gT;LX/AJK;LX/AJA;Lcom/facebook/privacy/model/SelectablePrivacyData;)V
    .locals 4
    .param p5    # LX/AJA;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/privacy/model/SelectablePrivacyData;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1661085
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1661086
    new-instance v0, LX/93W;

    new-instance v1, LX/AJL;

    invoke-direct {v1, p0}, LX/AJL;-><init>(LX/AJQ;)V

    new-instance v2, LX/AJM;

    invoke-direct {v2, p0}, LX/AJM;-><init>(LX/AJQ;)V

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, LX/93W;-><init>(LX/8Q5;LX/93X;LX/8Rm;)V

    iput-object v0, p0, LX/AJQ;->h:LX/93W;

    .line 1661087
    new-instance v0, LX/AJN;

    invoke-direct {v0, p0}, LX/AJN;-><init>(LX/AJQ;)V

    iput-object v0, p0, LX/AJQ;->i:LX/93q;

    .line 1661088
    new-instance v0, LX/AJO;

    invoke-direct {v0, p0}, LX/AJO;-><init>(LX/AJQ;)V

    iput-object v0, p0, LX/AJQ;->j:LX/93w;

    .line 1661089
    new-instance v0, LX/AJP;

    invoke-direct {v0, p0}, LX/AJP;-><init>(LX/AJQ;)V

    iput-object v0, p0, LX/AJQ;->k:LX/93x;

    .line 1661090
    iput-object p1, p0, LX/AJQ;->e:LX/0fO;

    .line 1661091
    iput-object p2, p0, LX/AJQ;->a:LX/93y;

    .line 1661092
    iput-object p3, p0, LX/AJQ;->f:LX/7gT;

    .line 1661093
    iput-object p4, p0, LX/AJQ;->g:LX/AJK;

    .line 1661094
    iput-object p5, p0, LX/AJQ;->b:LX/AJA;

    .line 1661095
    invoke-virtual {p0, p6}, LX/AJQ;->a(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    .line 1661096
    iget-object v1, p0, LX/AJQ;->a:LX/93y;

    iget-object v2, p0, LX/AJQ;->i:LX/93q;

    iget-object v3, p0, LX/AJQ;->j:LX/93w;

    iget-object v0, p0, LX/AJQ;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AJQ;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-object v0, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AJQ;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-object v0, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1661097
    iget-object p1, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, p1

    .line 1661098
    :goto_0
    iget-object p1, p0, LX/AJQ;->k:LX/93x;

    invoke-virtual {v1, v2, v3, v0, p1}, LX/93y;->a(LX/93q;LX/93w;Lcom/facebook/graphql/model/GraphQLPrivacyOption;Ljava/lang/Object;)LX/93t;

    move-result-object v0

    iput-object v0, p0, LX/AJQ;->c:LX/93t;

    .line 1661099
    iget-object v0, p0, LX/AJQ;->c:LX/93t;

    invoke-virtual {v0}, LX/93s;->f()V

    .line 1661100
    return-void

    .line 1661101
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/privacy/model/SelectablePrivacyData;)V
    .locals 2
    .param p1    # Lcom/facebook/privacy/model/SelectablePrivacyData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1661102
    iget-object v0, p0, LX/AJQ;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    if-nez v0, :cond_1

    .line 1661103
    new-instance v0, LX/7lP;

    invoke-direct {v0}, LX/7lP;-><init>()V

    .line 1661104
    :goto_0
    const/4 v1, 0x1

    .line 1661105
    iput-boolean v1, v0, LX/7lP;->a:Z

    .line 1661106
    move-object v0, v0

    .line 1661107
    invoke-virtual {v0, p1}, LX/7lP;->a(Lcom/facebook/privacy/model/SelectablePrivacyData;)LX/7lP;

    move-result-object v0

    invoke-virtual {v0}, LX/7lP;->a()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    iput-object v0, p0, LX/AJQ;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    .line 1661108
    iget-object v0, p0, LX/AJQ;->e:LX/0fO;

    invoke-virtual {v0}, LX/0fO;->u()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1661109
    iget-object v1, p0, LX/AJQ;->f:LX/7gT;

    const-string v0, "Only Me"

    .line 1661110
    :goto_1
    iget-object p0, v1, LX/7gT;->j:Ljava/lang/String;

    iput-object p0, v1, LX/7gT;->i:Ljava/lang/String;

    .line 1661111
    iput-object v0, v1, LX/7gT;->j:Ljava/lang/String;

    .line 1661112
    :cond_0
    return-void

    .line 1661113
    :cond_1
    new-instance v0, LX/7lP;

    iget-object v1, p0, LX/AJQ;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    invoke-direct {v0, v1}, LX/7lP;-><init>(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    goto :goto_0

    .line 1661114
    :cond_2
    if-eqz p1, :cond_0

    .line 1661115
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v0

    .line 1661116
    iget-object v1, p0, LX/AJQ;->f:LX/7gT;

    if-nez v0, :cond_3

    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final b()Lcom/facebook/privacy/model/SelectablePrivacyData;
    .locals 1

    .prologue
    .line 1661084
    iget-object v0, p0, LX/AJQ;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AJQ;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-object v0, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
