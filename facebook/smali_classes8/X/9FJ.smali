.class public LX/9FJ;
.super LX/19M;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile h:LX/9FJ;


# instance fields
.field public b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/1AM;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/6W5;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/0iA;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/0wL;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1458405
    const-class v0, LX/9FJ;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/9FJ;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1458438
    invoke-direct {p0}, LX/19M;-><init>()V

    .line 1458439
    return-void
.end method

.method public static a(LX/0QB;)LX/9FJ;
    .locals 8

    .prologue
    .line 1458440
    sget-object v0, LX/9FJ;->h:LX/9FJ;

    if-nez v0, :cond_1

    .line 1458441
    const-class v1, LX/9FJ;

    monitor-enter v1

    .line 1458442
    :try_start_0
    sget-object v0, LX/9FJ;->h:LX/9FJ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1458443
    if-eqz v2, :cond_0

    .line 1458444
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1458445
    new-instance v3, LX/9FJ;

    invoke-direct {v3}, LX/9FJ;-><init>()V

    .line 1458446
    invoke-static {v0}, LX/1AM;->a(LX/0QB;)LX/1AM;

    move-result-object v4

    check-cast v4, LX/1AM;

    invoke-static {v0}, LX/6W5;->a(LX/0QB;)LX/6W5;

    move-result-object v5

    check-cast v5, LX/6W5;

    invoke-static {v0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v6

    check-cast v6, LX/0iA;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {v0}, LX/0wL;->a(LX/0QB;)LX/0wL;

    move-result-object p0

    check-cast p0, LX/0wL;

    .line 1458447
    iput-object v4, v3, LX/9FJ;->c:LX/1AM;

    iput-object v5, v3, LX/9FJ;->d:LX/6W5;

    iput-object v6, v3, LX/9FJ;->e:LX/0iA;

    iput-object v7, v3, LX/9FJ;->f:LX/03V;

    iput-object p0, v3, LX/9FJ;->g:LX/0wL;

    .line 1458448
    move-object v0, v3

    .line 1458449
    sput-object v0, LX/9FJ;->h:LX/9FJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1458450
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1458451
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1458452
    :cond_1
    sget-object v0, LX/9FJ;->h:LX/9FJ;

    return-object v0

    .line 1458453
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1458454
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 7

    .prologue
    .line 1458406
    iget-object v0, p0, LX/9FJ;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1458407
    iget-object v1, p0, LX/9FJ;->e:LX/0iA;

    sget-object v2, LX/9FM;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v3, LX/9FM;

    invoke-virtual {v1, v2, v3}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v1

    check-cast v1, LX/9FM;

    .line 1458408
    if-nez v1, :cond_1

    .line 1458409
    :cond_0
    :goto_0
    iget-object v1, p0, LX/9FJ;->g:LX/0wL;

    iget-object v0, p0, LX/9FJ;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v1, v0}, LX/0wL;->b(Landroid/view/View;)V

    .line 1458410
    iget-object v0, p0, LX/9FJ;->c:LX/1AM;

    invoke-virtual {v0, p0}, LX/0b4;->b(LX/0b2;)Z

    .line 1458411
    const/4 v0, 0x0

    iput-object v0, p0, LX/9FJ;->b:Ljava/lang/ref/WeakReference;

    .line 1458412
    return-void

    .line 1458413
    :cond_1
    if-nez v0, :cond_2

    .line 1458414
    iget-object v1, p0, LX/9FJ;->f:LX/03V;

    sget-object v2, LX/9FJ;->a:Ljava/lang/String;

    const-string v3, "The view on which the NUX should be shown is null"

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1458415
    :cond_2
    iget-object v2, v1, LX/9FM;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/20q;

    invoke-virtual {v2}, LX/20q;->e()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, v1, LX/9FM;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/20q;

    invoke-virtual {v2}, LX/20q;->f()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_4

    .line 1458416
    :cond_3
    iget-object v2, v1, LX/9FM;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03V;

    sget-object v3, LX/9FM;->b:Ljava/lang/String;

    const-string v4, "Strings retrieved from QE for the NUX are null"

    invoke-virtual {v2, v3, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1458417
    const/4 v2, 0x0

    .line 1458418
    :goto_1
    move v2, v2

    .line 1458419
    if-eqz v2, :cond_0

    .line 1458420
    iget-object v2, p0, LX/9FJ;->d:LX/6W5;

    const-string v3, "comment_with_speech_nux"

    sget-object v4, LX/6W4;->FOOTER:LX/6W4;

    invoke-virtual {v1}, LX/9FM;->b()Ljava/lang/String;

    move-result-object v5

    .line 1458421
    new-instance p1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v6, "comment_with_speech_nux_interaction"

    invoke-direct {p1, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1458422
    iput-object v3, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1458423
    const-string v6, "comment_with_speech_nux_source"

    invoke-virtual {v4}, LX/6W4;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v6, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1458424
    const-string v6, "nux_id"

    invoke-virtual {p1, v6, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1458425
    iget-object v6, v2, LX/6W5;->a:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0Zb;

    invoke-interface {v6, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1458426
    iget-object v2, p0, LX/9FJ;->e:LX/0iA;

    invoke-virtual {v2}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v2

    invoke-virtual {v1}, LX/9FM;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 1458427
    goto/16 :goto_0

    .line 1458428
    :cond_4
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 1458429
    new-instance v4, LX/0hs;

    const/4 v3, 0x2

    invoke-direct {v4, v2, v3}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 1458430
    iget-object v3, v1, LX/9FM;->c:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/20q;

    invoke-virtual {v3}, LX/20q;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 1458431
    iget-object v3, v1, LX/9FM;->c:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/20q;

    invoke-virtual {v3}, LX/20q;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 1458432
    sget-object v3, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v4, v3}, LX/0ht;->a(LX/3AV;)V

    .line 1458433
    const/4 v3, -0x1

    .line 1458434
    iput v3, v4, LX/0hs;->t:I

    .line 1458435
    move-object v2, v4

    .line 1458436
    invoke-virtual {v2, v0}, LX/0ht;->f(Landroid/view/View;)V

    .line 1458437
    const/4 v2, 0x1

    goto :goto_1
.end method
