.class public final LX/9kM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/facebook/places/pagetopics/FetchPageTopicsResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/9kN;


# direct methods
.method public constructor <init>(LX/9kN;)V
    .locals 0

    .prologue
    .line 1531181
    iput-object p1, p0, LX/9kM;->a:LX/9kN;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1531178
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1531179
    iget-object v1, p0, LX/9kM;->a:LX/9kN;

    iget-object v1, v1, LX/9kN;->a:LX/0aG;

    const-string v2, "FetchPageTopics"

    const v3, 0x266f3511

    invoke-static {v1, v2, v0, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->startOnMainThread()LX/1ML;

    move-result-object v0

    const v1, -0x4e6d2bb3

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbservice/service/OperationResult;

    .line 1531180
    invoke-virtual {v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/pagetopics/FetchPageTopicsResult;

    return-object v0
.end method
