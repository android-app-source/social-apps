.class public final LX/95z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public final synthetic b:Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;

.field public final synthetic c:LX/961;


# direct methods
.method public constructor <init>(LX/961;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;)V
    .locals 0

    .prologue
    .line 1438066
    iput-object p1, p0, LX/95z;->c:LX/961;

    iput-object p2, p0, LX/95z;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object p3, p0, LX/95z;->b:Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1438067
    iget-object v0, p0, LX/95z;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-nez v0, :cond_0

    .line 1438068
    iget-object v0, p0, LX/95z;->c:LX/961;

    iget-object v0, v0, LX/961;->e:LX/3iV;

    iget-object v1, p0, LX/95z;->b:Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;

    .line 1438069
    iget-object p0, v1, Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;->a:Ljava/lang/String;

    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1438070
    iget-object p0, v1, Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-nez p0, :cond_1

    const/4 p0, 0x1

    :goto_0
    invoke-static {p0}, LX/0PB;->checkState(Z)V

    .line 1438071
    invoke-virtual {v1}, Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;->b()Lcom/facebook/api/ufiservices/common/ToggleLikeParams;

    move-result-object p0

    invoke-static {v0, p0}, LX/3iV;->a(LX/3iV;Lcom/facebook/api/ufiservices/common/ToggleLikeParams;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p0

    move-object v0, p0

    .line 1438072
    :goto_1
    return-object v0

    :cond_0
    iget-object v0, p0, LX/95z;->c:LX/961;

    iget-object v0, v0, LX/961;->f:LX/25G;

    iget-object v1, p0, LX/95z;->b:Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;

    invoke-virtual {v0, v1}, LX/25G;->a(Lcom/facebook/api/ufiservices/common/TogglePostLikeParams;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_1

    .line 1438073
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method
