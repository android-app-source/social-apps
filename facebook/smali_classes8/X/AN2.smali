.class public LX/AN2;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1667388
    const-class v0, LX/AN2;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AN2;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1667389
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1667390
    iput-object p1, p0, LX/AN2;->b:Ljava/lang/String;

    .line 1667391
    iput-object p2, p0, LX/AN2;->c:Ljava/lang/String;

    .line 1667392
    iput-object p3, p0, LX/AN2;->d:Ljava/lang/String;

    .line 1667393
    return-void
.end method

.method public static a(LX/0Px;)LX/AN2;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/AN2;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 1667394
    if-eqz p0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Tried to create with a null downloadable assets"

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1667395
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v9

    move v8, v2

    move-object v3, v7

    move-object v4, v7

    move-object v5, v7

    :goto_1
    if-ge v8, v9, :cond_5

    invoke-virtual {p0, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1667396
    if-eqz v0, :cond_1

    move v6, v1

    :goto_2
    const-string v10, "Null assetpath passed in"

    invoke-static {v6, v10}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1667397
    const-string v6, "face_align_model.bin"

    invoke-virtual {v0, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    move-object v12, v3

    move-object v3, v4

    move-object v4, v0

    move-object v0, v12

    .line 1667398
    :goto_3
    add-int/lit8 v5, v8, 0x1

    move v8, v5

    move-object v5, v4

    move-object v4, v3

    move-object v3, v0

    goto :goto_1

    :cond_0
    move v0, v2

    .line 1667399
    goto :goto_0

    :cond_1
    move v6, v2

    .line 1667400
    goto :goto_2

    .line 1667401
    :cond_2
    const-string v6, "face_detect_model.bin"

    invoke-virtual {v0, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    move-object v4, v5

    move-object v12, v0

    move-object v0, v3

    move-object v3, v12

    .line 1667402
    goto :goto_3

    .line 1667403
    :cond_3
    const-string v6, "pdm_multires.bin"

    invoke-virtual {v0, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    move-object v3, v4

    move-object v4, v5

    .line 1667404
    goto :goto_3

    .line 1667405
    :cond_4
    sget-object v6, LX/AN2;->a:Ljava/lang/String;

    const-string v10, "Unrecognized support asset: %s"

    new-array v11, v1, [Ljava/lang/Object;

    aput-object v0, v11, v2

    invoke-static {v6, v10, v11}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v3

    move-object v3, v4

    move-object v4, v5

    goto :goto_3

    .line 1667406
    :cond_5
    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 1667407
    new-instance v0, LX/AN2;

    invoke-direct {v0, v5, v4, v3}, LX/AN2;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1667408
    :goto_4
    return-object v0

    :cond_6
    move-object v0, v7

    goto :goto_4
.end method
