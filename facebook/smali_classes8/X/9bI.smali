.class public final enum LX/9bI;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9bI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9bI;

.field public static final enum LEFT:LX/9bI;

.field public static final enum NONE:LX/9bI;

.field public static final enum RIGHT:LX/9bI;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1514635
    new-instance v0, LX/9bI;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LX/9bI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9bI;->NONE:LX/9bI;

    .line 1514636
    new-instance v0, LX/9bI;

    const-string v1, "LEFT"

    invoke-direct {v0, v1, v3}, LX/9bI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9bI;->LEFT:LX/9bI;

    .line 1514637
    new-instance v0, LX/9bI;

    const-string v1, "RIGHT"

    invoke-direct {v0, v1, v4}, LX/9bI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9bI;->RIGHT:LX/9bI;

    .line 1514638
    const/4 v0, 0x3

    new-array v0, v0, [LX/9bI;

    sget-object v1, LX/9bI;->NONE:LX/9bI;

    aput-object v1, v0, v2

    sget-object v1, LX/9bI;->LEFT:LX/9bI;

    aput-object v1, v0, v3

    sget-object v1, LX/9bI;->RIGHT:LX/9bI;

    aput-object v1, v0, v4

    sput-object v0, LX/9bI;->$VALUES:[LX/9bI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1514632
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9bI;
    .locals 1

    .prologue
    .line 1514634
    const-class v0, LX/9bI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9bI;

    return-object v0
.end method

.method public static values()[LX/9bI;
    .locals 1

    .prologue
    .line 1514633
    sget-object v0, LX/9bI;->$VALUES:[LX/9bI;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9bI;

    return-object v0
.end method
