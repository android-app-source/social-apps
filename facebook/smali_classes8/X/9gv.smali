.class public LX/9gv;
.super LX/9gr;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/9gr",
        "<",
        "Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchPhotosByCategoryModel;",
        "Lcom/facebook/photos/mediafetcher/query/param/CategoryQueryParam;",
        "LX/5kD;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:LX/0tG;

.field private final c:LX/0tI;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/mediafetcher/query/param/CategoryQueryParam;Lcom/facebook/common/callercontext/CallerContext;LX/0tG;LX/0tI;)V
    .locals 1
    .param p1    # Lcom/facebook/photos/mediafetcher/query/param/CategoryQueryParam;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1525813
    const-class v0, LX/5kD;

    invoke-direct {p0, p1, v0, p2}, LX/9gr;-><init>(Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;Ljava/lang/Class;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1525814
    iput-object p3, p0, LX/9gv;->b:LX/0tG;

    .line 1525815
    iput-object p4, p0, LX/9gv;->c:LX/0tI;

    .line 1525816
    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/String;)LX/0gW;
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "LX/0gW",
            "<",
            "Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchPhotosByCategoryModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1525797
    new-instance v0, LX/9gJ;

    invoke-direct {v0}, LX/9gJ;-><init>()V

    move-object v1, v0

    .line 1525798
    const-string v0, "after_cursor"

    invoke-virtual {v1, v0, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "count"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "page_id"

    .line 1525799
    iget-object v0, p0, LX/9g8;->a:Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;

    move-object v0, v0

    .line 1525800
    check-cast v0, Lcom/facebook/photos/mediafetcher/query/param/CategoryQueryParam;

    iget-object v0, v0, Lcom/facebook/photos/mediafetcher/query/param/CategoryQueryParam;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "category"

    .line 1525801
    iget-object v0, p0, LX/9g8;->a:Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;

    move-object v0, v0

    .line 1525802
    check-cast v0, Lcom/facebook/photos/mediafetcher/query/param/CategoryQueryParam;

    iget-object v0, v0, Lcom/facebook/photos/mediafetcher/query/param/CategoryQueryParam;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "entry_point"

    .line 1525803
    iget-object v0, p0, LX/9g8;->a:Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;

    move-object v0, v0

    .line 1525804
    check-cast v0, Lcom/facebook/photos/mediafetcher/query/param/CategoryQueryParam;

    iget-object v0, v0, Lcom/facebook/photos/mediafetcher/query/param/CategoryQueryParam;->c:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1525805
    iget-object v0, p0, LX/9gv;->b:LX/0tG;

    invoke-virtual {v0, v1}, LX/0tG;->a(LX/0gW;)V

    .line 1525806
    iget-object v0, p0, LX/9gv;->c:LX/0tI;

    invoke-virtual {v0, v1}, LX/0tI;->a(LX/0gW;)V

    .line 1525807
    return-object v1
.end method

.method public final b(Lcom/facebook/graphql/executor/GraphQLResult;)LX/9fz;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchPhotosByCategoryModel;",
            ">;)",
            "LX/9fz",
            "<",
            "LX/5kD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1525808
    new-instance v1, LX/9fz;

    .line 1525809
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1525810
    check-cast v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchPhotosByCategoryModel;

    invoke-virtual {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchPhotosByCategoryModel;->a()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchPhotosByCategoryModel$PhotosByCategoryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchPhotosByCategoryModel$PhotosByCategoryModel;->a()LX/0Px;

    move-result-object v2

    .line 1525811
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1525812
    check-cast v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchPhotosByCategoryModel;

    invoke-virtual {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchPhotosByCategoryModel;->a()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchPhotosByCategoryModel$PhotosByCategoryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchPhotosByCategoryModel$PhotosByCategoryModel;->j()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    move-result-object v0

    invoke-direct {v1, v2, v0}, LX/9fz;-><init>(LX/0Px;Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;)V

    return-object v1
.end method
