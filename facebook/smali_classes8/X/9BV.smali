.class public final LX/9BV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:LX/20K;


# direct methods
.method public constructor <init>(LX/20K;)V
    .locals 0

    .prologue
    .line 1451719
    iput-object p1, p0, LX/9BV;->a:LX/20K;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 1451720
    iget-object v0, p0, LX/9BV;->a:LX/20K;

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1451721
    iget-object v4, v0, LX/20K;->m:LX/9BY;

    invoke-virtual {v4}, LX/9BY;->f()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1451722
    :cond_0
    :goto_0
    move v0, v1

    .line 1451723
    if-eqz v0, :cond_1

    .line 1451724
    iget-object v0, p0, LX/9BV;->a:LX/20K;

    const/4 v1, 0x0

    invoke-static {v0, v1, p2, v2}, LX/20K;->a$redex0(LX/20K;Landroid/view/View;Landroid/view/MotionEvent;Z)V

    .line 1451725
    :goto_1
    return v2

    .line 1451726
    :cond_1
    iget-object v0, p0, LX/9BV;->a:LX/20K;

    invoke-virtual {v0}, LX/20K;->b()V

    goto :goto_1

    .line 1451727
    :cond_2
    iget v4, v0, LX/20K;->r:I

    const/4 p1, -0x1

    if-eq v4, p1, :cond_3

    move v1, v3

    .line 1451728
    goto :goto_0

    .line 1451729
    :cond_3
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    if-nez v4, :cond_4

    iget-object v4, v0, LX/20K;->m:LX/9BY;

    invoke-virtual {v4, p2}, LX/9BY;->b(Landroid/view/MotionEvent;)Z

    move-result v4

    if-nez v4, :cond_4

    iget-object v4, v0, LX/20K;->g:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v4}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    :cond_4
    move v1, v3

    goto :goto_0
.end method
