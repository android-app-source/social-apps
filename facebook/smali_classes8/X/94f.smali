.class public LX/94f;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1435582
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(LX/0Px;)LX/0Rf;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;)",
            "LX/0Rf",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1435583
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v2

    .line 1435584
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    .line 1435585
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1435586
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1435587
    :cond_0
    invoke-virtual {v2}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;LX/0Px;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ipc/composer/intent/ComposerConfiguration;",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1435588
    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialTaggedUsers()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/94f;->a(LX/0Px;)LX/0Rf;

    move-result-object v0

    .line 1435589
    invoke-static {p1}, LX/94f;->a(LX/0Px;)LX/0Rf;

    move-result-object v1

    .line 1435590
    invoke-virtual {v0, v1}, LX/0Rf;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1435591
    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getMinutiaeObjectTag()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v2

    .line 1435592
    if-ne v2, p1, :cond_1

    .line 1435593
    :cond_0
    :goto_0
    return v0

    .line 1435594
    :cond_1
    if-eqz v2, :cond_2

    if-nez p1, :cond_3

    :cond_2
    move v0, v1

    .line 1435595
    goto :goto_0

    .line 1435596
    :cond_3
    invoke-virtual {v2, p1}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->b(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public static a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1435597
    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v2

    .line 1435598
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v3

    .line 1435599
    if-ne v2, v3, :cond_1

    .line 1435600
    :cond_0
    :goto_0
    return v0

    .line 1435601
    :cond_1
    if-eqz v2, :cond_2

    if-nez v3, :cond_3

    :cond_2
    move v0, v1

    .line 1435602
    goto :goto_0

    .line 1435603
    :cond_3
    invoke-virtual {v2}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method
