.class public final LX/AIh;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 17

    .prologue
    .line 1660179
    const/4 v13, 0x0

    .line 1660180
    const/4 v12, 0x0

    .line 1660181
    const/4 v5, 0x0

    .line 1660182
    const-wide/16 v10, 0x0

    .line 1660183
    const-wide/16 v8, 0x0

    .line 1660184
    const-wide/16 v6, 0x0

    .line 1660185
    const/4 v4, 0x0

    .line 1660186
    const/4 v3, 0x0

    .line 1660187
    const/4 v2, 0x0

    .line 1660188
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->START_OBJECT:LX/15z;

    if-eq v14, v15, :cond_b

    .line 1660189
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1660190
    const/4 v2, 0x0

    .line 1660191
    :goto_0
    return v2

    .line 1660192
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_7

    .line 1660193
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1660194
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1660195
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 1660196
    const-string v6, "feedback_reaction"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1660197
    invoke-static/range {p0 .. p1}, LX/AIg;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v16, v2

    goto :goto_1

    .line 1660198
    :cond_1
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1660199
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v15, v2

    goto :goto_1

    .line 1660200
    :cond_2
    const-string v6, "reaction_actor"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1660201
    invoke-static/range {p0 .. p1}, LX/AIe;->a(LX/15w;LX/186;)I

    move-result v2

    move v14, v2

    goto :goto_1

    .line 1660202
    :cond_3
    const-string v6, "time_position"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1660203
    const/4 v2, 0x1

    .line 1660204
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 1660205
    :cond_4
    const-string v6, "x_position"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1660206
    const/4 v2, 0x1

    .line 1660207
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v9, v2

    move-wide v12, v6

    goto :goto_1

    .line 1660208
    :cond_5
    const-string v6, "y_position"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1660209
    const/4 v2, 0x1

    .line 1660210
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v8, v2

    move-wide v10, v6

    goto :goto_1

    .line 1660211
    :cond_6
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1660212
    :cond_7
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1660213
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1660214
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1660215
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1660216
    if-eqz v3, :cond_8

    .line 1660217
    const/4 v3, 0x3

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1660218
    :cond_8
    if-eqz v9, :cond_9

    .line 1660219
    const/4 v3, 0x4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v12

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1660220
    :cond_9
    if-eqz v8, :cond_a

    .line 1660221
    const/4 v3, 0x5

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v10

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1660222
    :cond_a
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_b
    move v14, v5

    move v15, v12

    move/from16 v16, v13

    move-wide v12, v8

    move v8, v2

    move v9, v3

    move v3, v4

    move-wide v4, v10

    move-wide v10, v6

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1660223
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1660224
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1660225
    if-eqz v0, :cond_0

    .line 1660226
    const-string v1, "feedback_reaction"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1660227
    invoke-static {p0, v0, p2, p3}, LX/AIg;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1660228
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1660229
    if-eqz v0, :cond_1

    .line 1660230
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1660231
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1660232
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1660233
    if-eqz v0, :cond_2

    .line 1660234
    const-string v1, "reaction_actor"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1660235
    invoke-static {p0, v0, p2, p3}, LX/AIe;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1660236
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1660237
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_3

    .line 1660238
    const-string v2, "time_position"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1660239
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1660240
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1660241
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_4

    .line 1660242
    const-string v2, "x_position"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1660243
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1660244
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1660245
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_5

    .line 1660246
    const-string v2, "y_position"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1660247
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1660248
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1660249
    return-void
.end method
