.class public LX/8vq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/Filterable;
.implements Landroid/widget/WrapperListAdapter;


# static fields
.field public static final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/8vp;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/8vp;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/8vp;",
            ">;"
        }
    .end annotation
.end field

.field public d:Z

.field private final e:Landroid/widget/ListAdapter;

.field private final f:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1421176
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, LX/8vq;->c:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(Ljava/util/ArrayList;Ljava/util/ArrayList;Landroid/widget/ListAdapter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "LX/8vp;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "LX/8vp;",
            ">;",
            "Landroid/widget/ListAdapter;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1421164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1421165
    iput-object p3, p0, LX/8vq;->e:Landroid/widget/ListAdapter;

    .line 1421166
    instance-of v0, p3, Landroid/widget/Filterable;

    iput-boolean v0, p0, LX/8vq;->f:Z

    .line 1421167
    if-nez p1, :cond_0

    .line 1421168
    sget-object v0, LX/8vq;->c:Ljava/util/ArrayList;

    iput-object v0, p0, LX/8vq;->a:Ljava/util/ArrayList;

    .line 1421169
    :goto_0
    if-nez p2, :cond_1

    .line 1421170
    sget-object v0, LX/8vq;->c:Ljava/util/ArrayList;

    iput-object v0, p0, LX/8vq;->b:Ljava/util/ArrayList;

    .line 1421171
    :goto_1
    iget-object v0, p0, LX/8vq;->a:Ljava/util/ArrayList;

    invoke-static {v0}, LX/8vq;->a(Ljava/util/ArrayList;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/8vq;->b:Ljava/util/ArrayList;

    invoke-static {v0}, LX/8vq;->a(Ljava/util/ArrayList;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    iput-boolean v0, p0, LX/8vq;->d:Z

    .line 1421172
    return-void

    .line 1421173
    :cond_0
    iput-object p1, p0, LX/8vq;->a:Ljava/util/ArrayList;

    goto :goto_0

    .line 1421174
    :cond_1
    iput-object p2, p0, LX/8vq;->b:Ljava/util/ArrayList;

    goto :goto_1

    .line 1421175
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method private a()I
    .locals 1

    .prologue
    .line 1421102
    iget-object v0, p0, LX/8vq;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method private static a(Ljava/util/ArrayList;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "LX/8vp;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1421177
    if-eqz p0, :cond_1

    .line 1421178
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8vp;

    .line 1421179
    iget-boolean v0, v0, LX/8vp;->c:Z

    if-nez v0, :cond_0

    .line 1421180
    const/4 v0, 0x0

    .line 1421181
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private b()I
    .locals 1

    .prologue
    .line 1421182
    iget-object v0, p0, LX/8vq;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method


# virtual methods
.method public final areAllItemsEnabled()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1421183
    iget-object v1, p0, LX/8vq;->e:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_0

    .line 1421184
    iget-boolean v1, p0, LX/8vq;->d:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/8vq;->e:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->areAllItemsEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1421185
    :cond_0
    :goto_0
    return v0

    .line 1421186
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 1421187
    iget-object v0, p0, LX/8vq;->e:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    .line 1421188
    invoke-direct {p0}, LX/8vq;->b()I

    move-result v0

    invoke-direct {p0}, LX/8vq;->a()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LX/8vq;->e:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    add-int/2addr v0, v1

    .line 1421189
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, LX/8vq;->b()I

    move-result v0

    invoke-direct {p0}, LX/8vq;->a()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public final getFilter()Landroid/widget/Filter;
    .locals 1

    .prologue
    .line 1421190
    iget-boolean v0, p0, LX/8vq;->f:Z

    if-eqz v0, :cond_0

    .line 1421191
    iget-object v0, p0, LX/8vq;->e:Landroid/widget/ListAdapter;

    check-cast v0, Landroid/widget/Filterable;

    invoke-interface {v0}, Landroid/widget/Filterable;->getFilter()Landroid/widget/Filter;

    move-result-object v0

    .line 1421192
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1421146
    invoke-direct {p0}, LX/8vq;->a()I

    move-result v0

    .line 1421147
    if-ge p1, v0, :cond_0

    .line 1421148
    iget-object v0, p0, LX/8vq;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8vp;

    iget-object v0, v0, LX/8vp;->b:Ljava/lang/Object;

    .line 1421149
    :goto_0
    return-object v0

    .line 1421150
    :cond_0
    sub-int v1, p1, v0

    .line 1421151
    const/4 v0, 0x0

    .line 1421152
    iget-object v2, p0, LX/8vq;->e:Landroid/widget/ListAdapter;

    if-eqz v2, :cond_1

    .line 1421153
    iget-object v0, p0, LX/8vq;->e:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    .line 1421154
    if-ge v1, v0, :cond_1

    .line 1421155
    iget-object v0, p0, LX/8vq;->e:Landroid/widget/ListAdapter;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 1421156
    :cond_1
    iget-object v2, p0, LX/8vq;->b:Ljava/util/ArrayList;

    sub-int v0, v1, v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8vp;

    iget-object v0, v0, LX/8vp;->b:Ljava/lang/Object;

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1421157
    invoke-direct {p0}, LX/8vq;->a()I

    move-result v0

    .line 1421158
    iget-object v1, p0, LX/8vq;->e:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_0

    if-lt p1, v0, :cond_0

    .line 1421159
    sub-int v0, p1, v0

    .line 1421160
    iget-object v1, p0, LX/8vq;->e:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    .line 1421161
    if-ge v0, v1, :cond_0

    .line 1421162
    iget-object v1, p0, LX/8vq;->e:Landroid/widget/ListAdapter;

    invoke-interface {v1, v0}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v0

    .line 1421163
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 1421139
    invoke-direct {p0}, LX/8vq;->a()I

    move-result v0

    .line 1421140
    iget-object v1, p0, LX/8vq;->e:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_0

    if-lt p1, v0, :cond_0

    .line 1421141
    sub-int v0, p1, v0

    .line 1421142
    iget-object v1, p0, LX/8vq;->e:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    .line 1421143
    if-ge v0, v1, :cond_0

    .line 1421144
    iget-object v1, p0, LX/8vq;->e:Landroid/widget/ListAdapter;

    invoke-interface {v1, v0}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v0

    .line 1421145
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x2

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1421128
    invoke-direct {p0}, LX/8vq;->a()I

    move-result v0

    .line 1421129
    if-ge p1, v0, :cond_0

    .line 1421130
    iget-object v0, p0, LX/8vq;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8vp;

    iget-object v0, v0, LX/8vp;->a:Landroid/view/View;

    .line 1421131
    :goto_0
    return-object v0

    .line 1421132
    :cond_0
    sub-int v1, p1, v0

    .line 1421133
    const/4 v0, 0x0

    .line 1421134
    iget-object v2, p0, LX/8vq;->e:Landroid/widget/ListAdapter;

    if-eqz v2, :cond_1

    .line 1421135
    iget-object v0, p0, LX/8vq;->e:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    .line 1421136
    if-ge v1, v0, :cond_1

    .line 1421137
    iget-object v0, p0, LX/8vq;->e:Landroid/widget/ListAdapter;

    invoke-interface {v0, v1, p2, p3}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 1421138
    :cond_1
    iget-object v2, p0, LX/8vq;->b:Ljava/util/ArrayList;

    sub-int v0, v1, v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8vp;

    iget-object v0, v0, LX/8vp;->a:Landroid/view/View;

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 1421125
    iget-object v0, p0, LX/8vq;->e:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    .line 1421126
    iget-object v0, p0, LX/8vq;->e:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getViewTypeCount()I

    move-result v0

    .line 1421127
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final getWrappedAdapter()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 1421124
    iget-object v0, p0, LX/8vq;->e:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 1421121
    iget-object v0, p0, LX/8vq;->e:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    .line 1421122
    iget-object v0, p0, LX/8vq;->e:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v0

    .line 1421123
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 1421120
    iget-object v0, p0, LX/8vq;->e:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8vq;->e:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEnabled(I)Z
    .locals 3

    .prologue
    .line 1421109
    invoke-direct {p0}, LX/8vq;->a()I

    move-result v0

    .line 1421110
    if-ge p1, v0, :cond_0

    .line 1421111
    iget-object v0, p0, LX/8vq;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8vp;

    iget-boolean v0, v0, LX/8vp;->c:Z

    .line 1421112
    :goto_0
    return v0

    .line 1421113
    :cond_0
    sub-int v1, p1, v0

    .line 1421114
    const/4 v0, 0x0

    .line 1421115
    iget-object v2, p0, LX/8vq;->e:Landroid/widget/ListAdapter;

    if-eqz v2, :cond_1

    .line 1421116
    iget-object v0, p0, LX/8vq;->e:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    .line 1421117
    if-ge v1, v0, :cond_1

    .line 1421118
    iget-object v0, p0, LX/8vq;->e:Landroid/widget/ListAdapter;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v0

    goto :goto_0

    .line 1421119
    :cond_1
    iget-object v2, p0, LX/8vq;->b:Ljava/util/ArrayList;

    sub-int v0, v1, v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8vp;

    iget-boolean v0, v0, LX/8vp;->c:Z

    goto :goto_0
.end method

.method public final registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 1421106
    iget-object v0, p0, LX/8vq;->e:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    .line 1421107
    iget-object v0, p0, LX/8vq;->e:Landroid/widget/ListAdapter;

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 1421108
    :cond_0
    return-void
.end method

.method public final unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 1421103
    iget-object v0, p0, LX/8vq;->e:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    .line 1421104
    iget-object v0, p0, LX/8vq;->e:Landroid/widget/ListAdapter;

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 1421105
    :cond_0
    return-void
.end method
