.class public final LX/AEF;
.super LX/0Vd;
.source ""


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:LX/AEG;


# direct methods
.method public constructor <init>(LX/AEG;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1646284
    iput-object p1, p0, LX/AEF;->b:LX/AEG;

    iput-object p2, p0, LX/AEF;->a:Landroid/view/View;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1646285
    iget-object v0, p0, LX/AEF;->b:LX/AEG;

    iget-object v0, v0, LX/AEG;->a:Lcom/facebook/graphql/model/GraphQLCoupon;

    invoke-static {v0}, LX/AEI;->d(Lcom/facebook/graphql/model/GraphQLCoupon;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "claim"

    invoke-static {v0, v1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1646286
    iget-object v0, p0, LX/AEF;->b:LX/AEG;

    iget-object v0, v0, LX/AEG;->d:LX/AEI;

    iget-object v1, p0, LX/AEF;->b:LX/AEG;

    iget-object v1, v1, LX/AEG;->d:LX/AEI;

    iget-object v1, v1, LX/AEI;->a:Landroid/content/res/Resources;

    const v2, 0x7f081020

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/AEI;->a$redex0(LX/AEI;Ljava/lang/String;)V

    .line 1646287
    :cond_0
    :goto_0
    return-void

    .line 1646288
    :cond_1
    iget-object v0, p0, LX/AEF;->b:LX/AEG;

    iget-object v0, v0, LX/AEG;->a:Lcom/facebook/graphql/model/GraphQLCoupon;

    invoke-static {v0}, LX/AEI;->d(Lcom/facebook/graphql/model/GraphQLCoupon;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "resend"

    invoke-static {v0, v1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1646289
    iget-object v0, p0, LX/AEF;->b:LX/AEG;

    iget-object v0, v0, LX/AEG;->d:LX/AEI;

    iget-object v1, p0, LX/AEF;->b:LX/AEG;

    iget-object v1, v1, LX/AEG;->d:LX/AEI;

    iget-object v1, v1, LX/AEI;->a:Landroid/content/res/Resources;

    const v2, 0x7f081021

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/AEI;->a$redex0(LX/AEI;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1646290
    iget-object v0, p0, LX/AEF;->b:LX/AEG;

    iget-object v0, v0, LX/AEG;->a:Lcom/facebook/graphql/model/GraphQLCoupon;

    invoke-static {v0}, LX/AEI;->d(Lcom/facebook/graphql/model/GraphQLCoupon;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "claim"

    invoke-static {v0, v1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1646291
    iget-object v0, p0, LX/AEF;->b:LX/AEG;

    iget-object v0, v0, LX/AEG;->d:LX/AEI;

    iget-object v0, v0, LX/AEI;->i:LX/0Zb;

    const-string v1, "tap_coupon_attachment"

    invoke-interface {v0, v1, v4}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 1646292
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1646293
    const-string v1, "event_type"

    const-string v2, "claim_offer"

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "tracking"

    iget-object v3, p0, LX/AEF;->b:LX/AEG;

    iget-object v3, v3, LX/AEG;->b:LX/162;

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;LX/0lF;)LX/0oG;

    move-result-object v1

    iget-object v2, p0, LX/AEF;->b:LX/AEG;

    iget-boolean v2, v2, LX/AEG;->c:Z

    .line 1646294
    iget-object v3, v1, LX/0oG;->a:Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    .line 1646295
    iput-boolean v2, v3, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->k:Z

    .line 1646296
    const-string p1, "sponsored"

    invoke-virtual {v3, p1, v2}, Lcom/facebook/analytics/event/HoneyClientEventFastInternal;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/event/HoneyClientEventFastInternal;

    .line 1646297
    move-object v1, v1

    .line 1646298
    const-string v3, "native_newsfeed"

    move-object v3, v3

    .line 1646299
    move-object v2, v3

    .line 1646300
    invoke-virtual {v1, v2}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 1646301
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 1646302
    :cond_0
    iget-object v0, p0, LX/AEF;->b:LX/AEG;

    iget-object v0, v0, LX/AEG;->a:Lcom/facebook/graphql/model/GraphQLCoupon;

    .line 1646303
    invoke-virtual {v0, v4}, Lcom/facebook/graphql/model/GraphQLCoupon;->a(Z)V

    .line 1646304
    iget-object v0, p0, LX/AEF;->b:LX/AEG;

    iget-object v0, v0, LX/AEG;->d:LX/AEI;

    iget-object v0, v0, LX/AEI;->d:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iget-object v1, p0, LX/AEF;->b:LX/AEG;

    iget-object v1, v1, LX/AEG;->d:LX/AEI;

    iget-object v1, v1, LX/AEI;->e:Landroid/content/Context;

    sget-object v2, LX/0ax;->bh:Ljava/lang/String;

    iget-object v3, p0, LX/AEF;->b:LX/AEG;

    iget-object v3, v3, LX/AEG;->a:Lcom/facebook/graphql/model/GraphQLCoupon;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLCoupon;->k()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 1646305
    iget-object v0, p0, LX/AEF;->b:LX/AEG;

    iget-object v0, v0, LX/AEG;->d:LX/AEI;

    iget-object v0, v0, LX/AEI;->a:Landroid/content/res/Resources;

    iget-object v1, p0, LX/AEF;->b:LX/AEG;

    iget-object v1, v1, LX/AEG;->a:Lcom/facebook/graphql/model/GraphQLCoupon;

    invoke-static {v1}, LX/AEI;->c(Lcom/facebook/graphql/model/GraphQLCoupon;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1646306
    iget-object v0, p0, LX/AEF;->a:Landroid/view/View;

    instance-of v0, v0, Lcom/facebook/resources/ui/FbButton;

    if-eqz v0, :cond_1

    .line 1646307
    iget-object v0, p0, LX/AEF;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 1646308
    :goto_0
    return-void

    .line 1646309
    :cond_1
    iget-object v0, p0, LX/AEF;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
