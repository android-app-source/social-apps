.class public LX/A79;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public b:LX/A78;

.field private c:LX/0lB;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1625606
    const-class v0, LX/A79;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/A79;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/A78;LX/0lB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1625607
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1625608
    iput-object p1, p0, LX/A79;->b:LX/A78;

    .line 1625609
    iput-object p2, p0, LX/A79;->c:LX/0lB;

    .line 1625610
    return-void
.end method

.method public static a(LX/A79;Ljava/lang/String;)Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1625611
    :try_start_0
    iget-object v0, p0, LX/A79;->c:LX/0lB;

    invoke-virtual {v0}, LX/0lD;->b()LX/0lp;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0lp;->a(Ljava/lang/String;)LX/15w;
    :try_end_0
    .catch LX/2aQ; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 1625612
    :try_start_1
    const-class v0, Lcom/facebook/transliteration/datatypes/DictionaryItem;

    invoke-virtual {v2, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/transliteration/datatypes/DictionaryItem;
    :try_end_1
    .catch LX/2aQ; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1625613
    if-eqz v2, :cond_0

    .line 1625614
    :try_start_2
    invoke-virtual {v2}, LX/15w;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    .line 1625615
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    move-object v0, v1

    :goto_1
    return-object v0

    .line 1625616
    :catch_0
    move-exception v0

    move-object v2, v1

    .line 1625617
    :goto_2
    :try_start_3
    sget-object v3, LX/A79;->a:Ljava/lang/String;

    const-string v4, "Could not parse data"

    invoke-static {v3, v4, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1625618
    if-eqz v2, :cond_3

    .line 1625619
    :try_start_4
    invoke-virtual {v2}, LX/15w;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    move-object v0, v1

    .line 1625620
    goto :goto_0

    :catch_1
    move-object v0, v1

    goto :goto_0

    .line 1625621
    :catch_2
    move-exception v0

    move-object v2, v1

    .line 1625622
    :goto_3
    :try_start_5
    sget-object v3, LX/A79;->a:Ljava/lang/String;

    const-string v4, "Could not convert class"

    invoke-static {v3, v4, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1625623
    if-eqz v2, :cond_3

    .line 1625624
    :try_start_6
    invoke-virtual {v2}, LX/15w;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    move-object v0, v1

    .line 1625625
    goto :goto_0

    :catch_3
    move-object v0, v1

    goto :goto_0

    .line 1625626
    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_4
    if-eqz v2, :cond_1

    .line 1625627
    :try_start_7
    invoke-virtual {v2}, LX/15w;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_5

    .line 1625628
    :cond_1
    :goto_5
    throw v0

    .line 1625629
    :cond_2
    iget-object v0, v0, Lcom/facebook/transliteration/datatypes/DictionaryItem;->mDictionary:Ljava/util/Map;

    goto :goto_1

    .line 1625630
    :catch_4
    goto :goto_0

    :catch_5
    goto :goto_5

    .line 1625631
    :catchall_1
    move-exception v0

    goto :goto_4

    .line 1625632
    :catch_6
    move-exception v0

    goto :goto_3

    .line 1625633
    :catch_7
    move-exception v0

    goto :goto_2

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/A79;
    .locals 3

    .prologue
    .line 1625634
    new-instance v2, LX/A79;

    invoke-static {p0}, LX/A78;->b(LX/0QB;)LX/A78;

    move-result-object v0

    check-cast v0, LX/A78;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v1

    check-cast v1, LX/0lB;

    invoke-direct {v2, v0, v1}, LX/A79;-><init>(LX/A78;LX/0lB;)V

    .line 1625635
    return-object v2
.end method
