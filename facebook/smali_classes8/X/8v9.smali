.class public LX/8v9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/3Lx;

.field private final b:Landroid/content/res/Resources;

.field private final c:Z

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/3Lx;Landroid/content/res/Resources;Ljava/lang/Boolean;LX/0Ot;)V
    .locals 1
    .param p3    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .param p4    # LX/0Ot;
        .annotation runtime Lcom/facebook/work/config/community/WorkCommunityName;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3Lx;",
            "Landroid/content/res/Resources;",
            "Ljava/lang/Boolean;",
            "LX/0Ot",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1416685
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1416686
    iput-object p1, p0, LX/8v9;->a:LX/3Lx;

    .line 1416687
    iput-object p2, p0, LX/8v9;->b:Landroid/content/res/Resources;

    .line 1416688
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/8v9;->c:Z

    .line 1416689
    iput-object p4, p0, LX/8v9;->d:LX/0Ot;

    .line 1416690
    return-void
.end method
