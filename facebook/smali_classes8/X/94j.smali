.class public LX/94j;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final b:LX/0SG;

.field private final c:LX/30I;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/30I;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1435617
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1435618
    iput-object p1, p0, LX/94j;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1435619
    iput-object p2, p0, LX/94j;->b:LX/0SG;

    .line 1435620
    iput-object p3, p0, LX/94j;->c:LX/30I;

    .line 1435621
    return-void
.end method

.method public static b(LX/0QB;)LX/94j;
    .locals 4

    .prologue
    .line 1435622
    new-instance v3, LX/94j;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-static {p0}, LX/30I;->b(LX/0QB;)LX/30I;

    move-result-object v2

    check-cast v2, LX/30I;

    invoke-direct {v3, v0, v1, v2}, LX/94j;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/30I;)V

    .line 1435623
    return-object v3
.end method
