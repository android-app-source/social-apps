.class public LX/8ht;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:LX/8i0;

.field public static final e:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/103;",
            "LX/8i0;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile i:LX/8ht;


# instance fields
.field public final f:LX/0Uh;

.field private final g:LX/0ad;

.field public final h:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    .line 1390813
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/8ht;->a:LX/0Px;

    .line 1390814
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/8ht;->b:LX/0Px;

    .line 1390815
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED_VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/8ht;->c:LX/0Px;

    .line 1390816
    new-instance v0, LX/8hz;

    invoke-direct {v0}, LX/8hz;-><init>()V

    move-object v0, v0

    .line 1390817
    invoke-virtual {v0}, LX/8hz;->a()LX/8hz;

    move-result-object v0

    invoke-virtual {v0}, LX/8hz;->b()LX/8i0;

    move-result-object v0

    sput-object v0, LX/8ht;->d:LX/8i0;

    .line 1390818
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    sget-object v1, LX/103;->GROUP:LX/103;

    sget-object v2, LX/103;->GROUP:LX/103;

    invoke-static {v2}, LX/8i0;->a(LX/103;)LX/8hz;

    move-result-object v2

    invoke-virtual {v2}, LX/8hz;->a()LX/8hz;

    move-result-object v2

    invoke-virtual {v2}, LX/8hz;->b()LX/8i0;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/103;->USER:LX/103;

    sget-object v2, LX/103;->USER:LX/103;

    invoke-static {v2}, LX/8i0;->a(LX/103;)LX/8hz;

    move-result-object v2

    invoke-virtual {v2}, LX/8hz;->a()LX/8hz;

    move-result-object v2

    invoke-virtual {v2}, LX/8hz;->b()LX/8i0;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/103;->PAGE:LX/103;

    sget-object v2, LX/103;->PAGE:LX/103;

    invoke-static {v2}, LX/8i0;->a(LX/103;)LX/8hz;

    move-result-object v2

    invoke-virtual {v2}, LX/8hz;->a()LX/8hz;

    move-result-object v2

    sget v3, LX/2SU;->u:I

    .line 1390819
    iget v4, v2, LX/8hz;->b:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_0

    const/4 v4, 0x1

    :goto_0
    const-string v5, "Gk already specified"

    invoke-static {v4, v5}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1390820
    iput v3, v2, LX/8hz;->b:I

    .line 1390821
    move-object v2, v2

    .line 1390822
    invoke-virtual {v2}, LX/8hz;->b()LX/8i0;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/103;->URL:LX/103;

    sget-object v2, LX/103;->URL:LX/103;

    invoke-static {v2}, LX/8i0;->a(LX/103;)LX/8hz;

    move-result-object v2

    invoke-virtual {v2}, LX/8hz;->a()LX/8hz;

    move-result-object v2

    invoke-virtual {v2}, LX/8hz;->b()LX/8i0;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/103;->VIDEO:LX/103;

    sget-object v2, LX/103;->VIDEO:LX/103;

    invoke-static {v2}, LX/8i0;->a(LX/103;)LX/8hz;

    move-result-object v2

    invoke-virtual {v2}, LX/8hz;->b()LX/8i0;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/103;->MARKETPLACE:LX/103;

    sget-object v2, LX/103;->MARKETPLACE:LX/103;

    invoke-static {v2}, LX/8i0;->a(LX/103;)LX/8hz;

    move-result-object v2

    invoke-virtual {v2}, LX/8hz;->b()LX/8i0;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/103;->COMMERCE:LX/103;

    sget-object v2, LX/103;->COMMERCE:LX/103;

    invoke-static {v2}, LX/8i0;->a(LX/103;)LX/8hz;

    move-result-object v2

    invoke-virtual {v2}, LX/8hz;->b()LX/8i0;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/8ht;->e:LX/0P1;

    return-void

    .line 1390823
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public constructor <init>(LX/0Uh;LX/0ad;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1390908
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1390909
    iput-object p1, p0, LX/8ht;->f:LX/0Uh;

    .line 1390910
    iput-object p2, p0, LX/8ht;->g:LX/0ad;

    .line 1390911
    sget v0, LX/2SU;->e:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/8ht;->h:Z

    .line 1390912
    return-void
.end method

.method public static a(Lcom/facebook/search/api/GraphSearchQuery;)LX/0Px;
    .locals 3
    .param p0    # Lcom/facebook/search/api/GraphSearchQuery;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/api/GraphSearchQuery;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1390897
    if-nez p0, :cond_0

    .line 1390898
    sget-object v0, LX/8ht;->a:LX/0Px;

    .line 1390899
    :goto_0
    return-object v0

    .line 1390900
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v0, v0

    .line 1390901
    if-eqz v0, :cond_1

    .line 1390902
    iget-object v1, p0, Lcom/facebook/search/api/GraphSearchQuery;->j:LX/7B5;

    move-object v1, v1

    .line 1390903
    sget-object v2, LX/7B5;->TAB:LX/7B5;

    if-eq v1, v2, :cond_2

    .line 1390904
    :cond_1
    sget-object v0, LX/8ht;->a:LX/0Px;

    goto :goto_0

    .line 1390905
    :cond_2
    sget-object v1, LX/8hs;->a:[I

    invoke-virtual {v0}, LX/103;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1390906
    sget-object v0, LX/8ht;->b:LX/0Px;

    goto :goto_0

    .line 1390907
    :pswitch_0
    sget-object v0, LX/8ht;->c:LX/0Px;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/0QB;)LX/8ht;
    .locals 5

    .prologue
    .line 1390884
    sget-object v0, LX/8ht;->i:LX/8ht;

    if-nez v0, :cond_1

    .line 1390885
    const-class v1, LX/8ht;

    monitor-enter v1

    .line 1390886
    :try_start_0
    sget-object v0, LX/8ht;->i:LX/8ht;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1390887
    if-eqz v2, :cond_0

    .line 1390888
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1390889
    new-instance p0, LX/8ht;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-direct {p0, v3, v4}, LX/8ht;-><init>(LX/0Uh;LX/0ad;)V

    .line 1390890
    move-object v0, p0

    .line 1390891
    sput-object v0, LX/8ht;->i:LX/8ht;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1390892
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1390893
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1390894
    :cond_1
    sget-object v0, LX/8ht;->i:LX/8ht;

    return-object v0

    .line 1390895
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1390896
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/7B6;)Z
    .locals 1
    .param p0    # LX/7B6;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1390883
    instance-of v0, p0, Lcom/facebook/search/api/GraphSearchQuery;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/facebook/search/api/GraphSearchQuery;

    invoke-static {p0}, LX/8ht;->b(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/search/api/GraphSearchQuery;)Z
    .locals 2
    .param p0    # Lcom/facebook/search/api/GraphSearchQuery;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1390880
    if-eqz p0, :cond_0

    .line 1390881
    iget-object v0, p0, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v0, v0

    .line 1390882
    sget-object v1, LX/103;->VIDEO:LX/103;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/search/api/GraphSearchQuery;)Z
    .locals 2
    .param p0    # Lcom/facebook/search/api/GraphSearchQuery;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1390875
    if-eqz p0, :cond_1

    .line 1390876
    iget-object v0, p0, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v0, v0

    .line 1390877
    sget-object v1, LX/103;->MARKETPLACE:LX/103;

    if-eq v0, v1, :cond_0

    .line 1390878
    iget-object v0, p0, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v0, v0

    .line 1390879
    sget-object v1, LX/103;->COMMERCE:LX/103;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;LX/8ci;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1390874
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-eq p1, v1, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED_STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-ne p1, v1, :cond_1

    :cond_0
    sget-object v1, LX/8ci;->l:LX/8ci;

    if-eq p2, v1, :cond_1

    iget-object v1, p0, LX/8ht;->f:LX/0Uh;

    sget v2, LX/2SU;->h:I

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public final e(Lcom/facebook/search/api/GraphSearchQuery;)Z
    .locals 6
    .param p1    # Lcom/facebook/search/api/GraphSearchQuery;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1390846
    if-eqz p1, :cond_0

    .line 1390847
    iget-object v2, p1, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v2, v2

    .line 1390848
    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    .line 1390849
    :cond_1
    :goto_0
    return v0

    .line 1390850
    :cond_2
    invoke-static {p1}, LX/8ht;->c(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1390851
    iget-boolean v2, p0, LX/8ht;->h:Z

    if-eqz v2, :cond_5

    const/4 v2, 0x0

    .line 1390852
    if-nez p1, :cond_6

    .line 1390853
    :cond_3
    :goto_1
    move v2, v2

    .line 1390854
    if-eqz v2, :cond_5

    .line 1390855
    iget-object v2, p1, Lcom/facebook/search/api/GraphSearchQuery;->j:LX/7B5;

    move-object v2, v2

    .line 1390856
    sget-object v3, LX/7B5;->TAB:LX/7B5;

    if-eq v2, v3, :cond_4

    .line 1390857
    iget-object v2, p1, Lcom/facebook/search/api/GraphSearchQuery;->j:LX/7B5;

    move-object v2, v2

    .line 1390858
    sget-object v3, LX/7B5;->SCOPED_ONLY:LX/7B5;

    if-ne v2, v3, :cond_5

    .line 1390859
    :cond_4
    iget-boolean v2, p1, Lcom/facebook/search/api/GraphSearchQuery;->k:Z

    move v2, v2

    .line 1390860
    if-nez v2, :cond_1

    :cond_5
    move v0, v1

    goto :goto_0

    .line 1390861
    :cond_6
    iget-object v3, p1, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v3, v3

    .line 1390862
    if-eqz v3, :cond_3

    .line 1390863
    sget-object v2, LX/8ht;->e:LX/0P1;

    invoke-virtual {v2, v3}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    sget-object v2, LX/8ht;->e:LX/0P1;

    invoke-virtual {v2, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8i0;

    .line 1390864
    :goto_2
    iget-object v3, p0, LX/8ht;->f:LX/0Uh;

    const/4 v4, 0x0

    .line 1390865
    iget-object v5, v2, LX/8i0;->a:LX/103;

    if-eqz v5, :cond_9

    .line 1390866
    iget-object v5, p1, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v5, v5

    .line 1390867
    iget-object p0, v2, LX/8i0;->a:LX/103;

    if-eq v5, p0, :cond_9

    .line 1390868
    :cond_7
    :goto_3
    move v2, v4

    .line 1390869
    goto :goto_1

    .line 1390870
    :cond_8
    sget-object v2, LX/8ht;->d:LX/8i0;

    goto :goto_2

    .line 1390871
    :cond_9
    iget v5, v2, LX/8i0;->b:I

    const/4 p0, -0x1

    if-eq v5, p0, :cond_a

    iget v5, v2, LX/8i0;->b:I

    invoke-virtual {v3, v5, v4}, LX/0Uh;->a(IZ)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 1390872
    :cond_a
    iget-boolean v5, v2, LX/8i0;->c:Z

    if-eqz v5, :cond_b

    invoke-virtual {p1}, Lcom/facebook/search/api/GraphSearchQuery;->k()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 1390873
    :cond_b
    const/4 v4, 0x1

    goto :goto_3
.end method

.method public final f(Lcom/facebook/search/api/GraphSearchQuery;)Z
    .locals 2
    .param p1    # Lcom/facebook/search/api/GraphSearchQuery;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1390843
    invoke-virtual {p0, p1}, LX/8ht;->e(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 1390844
    iget-object v0, p1, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v0, v0

    .line 1390845
    sget-object v1, LX/103;->GROUP:LX/103;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g(Lcom/facebook/search/api/GraphSearchQuery;)Z
    .locals 5
    .param p1    # Lcom/facebook/search/api/GraphSearchQuery;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1390834
    invoke-virtual {p0, p1}, LX/8ht;->e(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz p1, :cond_3

    .line 1390835
    iget-object v0, p1, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v0, v0

    .line 1390836
    sget-object v3, LX/103;->USER:LX/103;

    if-ne v0, v3, :cond_3

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1390837
    if-eqz v0, :cond_2

    iget-object v0, p0, LX/8ht;->g:LX/0ad;

    sget-short v3, LX/100;->P:S

    invoke-interface {v0, v3, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 1390838
    :goto_1
    invoke-virtual {p0, p1}, LX/8ht;->e(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v3

    if-eqz v3, :cond_4

    if-eqz p1, :cond_4

    .line 1390839
    iget-object v3, p1, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v3, v3

    .line 1390840
    sget-object v4, LX/103;->PAGE:LX/103;

    if-ne v3, v4, :cond_4

    const/4 v3, 0x1

    :goto_2
    move v3, v3

    .line 1390841
    if-nez v3, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    return v2

    :cond_2
    move v0, v2

    .line 1390842
    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    const/4 v3, 0x0

    goto :goto_2
.end method

.method public final h(Lcom/facebook/search/api/GraphSearchQuery;)Z
    .locals 3
    .param p1    # Lcom/facebook/search/api/GraphSearchQuery;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 1390829
    iget-boolean v1, p0, LX/8ht;->h:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/8ht;->g:LX/0ad;

    sget-short v2, LX/100;->cu:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 1390830
    iget-object v1, p1, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v1, v1

    .line 1390831
    sget-object v2, LX/103;->URL:LX/103;

    if-ne v1, v2, :cond_0

    .line 1390832
    iget-object v1, p1, Lcom/facebook/search/api/GraphSearchQuery;->h:Ljava/lang/String;

    move-object v1, v1

    .line 1390833
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final i(Lcom/facebook/search/api/GraphSearchQuery;)Z
    .locals 2
    .param p1    # Lcom/facebook/search/api/GraphSearchQuery;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1390824
    iget-boolean v0, p0, LX/8ht;->h:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/search/api/GraphSearchQuery;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1390825
    iget-object v0, p1, Lcom/facebook/search/api/GraphSearchQuery;->j:LX/7B5;

    move-object v0, v0

    .line 1390826
    if-eqz v0, :cond_0

    .line 1390827
    iget-object v0, p1, Lcom/facebook/search/api/GraphSearchQuery;->j:LX/7B5;

    move-object v0, v0

    .line 1390828
    sget-object v1, LX/7B5;->SINGLE_STATE:LX/7B5;

    if-ne v0, v1, :cond_1

    :cond_0
    invoke-virtual {p0, p1}, LX/8ht;->h(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
