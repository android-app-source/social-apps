.class public LX/8vQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/16E;
.implements LX/0i1;


# instance fields
.field public final a:LX/8vP;

.field public b:Landroid/view/View;

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/lang/String;

.field public e:Z

.field public f:I

.field public g:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;


# direct methods
.method public constructor <init>(LX/8vP;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1417067
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1417068
    iput-object p1, p0, LX/8vQ;->a:LX/8vP;

    .line 1417069
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 1417063
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 1

    .prologue
    .line 1417070
    iget-object v0, p0, LX/8vQ;->b:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/8vQ;->c:LX/0Px;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/8vQ;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/8vQ;->g:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/8vQ;->a:LX/8vP;

    invoke-virtual {v0}, LX/8vP;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1417071
    if-eqz v0, :cond_0

    .line 1417072
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    .line 1417073
    :goto_1
    return-object v0

    :cond_0
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 1417064
    return-void
.end method

.method public final a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1417065
    iget-object v0, p0, LX/8vQ;->a:LX/8vP;

    invoke-virtual {v0}, LX/8vP;->b()V

    .line 1417066
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 1417061
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1417060
    const-string v0, "4327"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1417062
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->GROUP_MALL_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
