.class public final LX/9JV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2kb;


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation


# instance fields
.field public final a:LX/9JS;


# direct methods
.method public constructor <init>(LX/9JS;)V
    .locals 1

    .prologue
    .line 1465342
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1465343
    iput-object p1, p0, LX/9JV;->a:LX/9JS;

    .line 1465344
    iget-object v0, p0, LX/9JV;->a:LX/9JS;

    .line 1465345
    iget-object p0, v0, LX/9JS;->n:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {p0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 1465346
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1465352
    iget-object v0, p0, LX/9JV;->a:LX/9JS;

    .line 1465353
    iget-object p0, v0, LX/9JS;->a:Ljava/lang/String;

    move-object v0, p0

    .line 1465354
    return-object v0
.end method

.method public final b()LX/9JS;
    .locals 1

    .prologue
    .line 1465355
    iget-object v0, p0, LX/9JV;->a:LX/9JS;

    return-object v0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 1465347
    iget-object v0, p0, LX/9JV;->a:LX/9JS;

    .line 1465348
    iget-object p0, v0, LX/9JS;->n:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {p0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result p0

    move v0, p0

    .line 1465349
    if-nez v0, :cond_0

    .line 1465350
    invoke-static {}, LX/2kE;->a()V

    .line 1465351
    :cond_0
    return-void
.end method
