.class public final LX/9d0;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/9d5;


# direct methods
.method public constructor <init>(LX/9d5;)V
    .locals 0

    .prologue
    .line 1517042
    iput-object p1, p0, LX/9d0;->a:LX/9d5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/9dK;)V
    .locals 7

    .prologue
    .line 1517043
    iget-object v0, p0, LX/9d0;->a:LX/9d5;

    iget-object v0, v0, LX/9d5;->s:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    .line 1517044
    iget-object v1, v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->c:LX/0zw;

    move-object v0, v1

    .line 1517045
    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, LX/9d0;->a:LX/9d5;

    iget-object v1, v1, LX/9d5;->f:LX/9cz;

    const/4 p0, -0x2

    .line 1517046
    iput-object v0, p1, LX/9dK;->b:Landroid/view/ViewGroup;

    .line 1517047
    iput-object v1, p1, LX/9dK;->i:LX/9cz;

    .line 1517048
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 1517049
    invoke-virtual {p1}, LX/9dK;->a()Landroid/widget/ImageView;

    move-result-object v2

    iput-object v2, p1, LX/9dK;->g:Landroid/widget/ImageView;

    .line 1517050
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iget-object v3, p1, LX/9dK;->g:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1517051
    iget-object v2, p1, LX/9dK;->g:Landroid/widget/ImageView;

    invoke-static {v2, p0, p0}, LX/46y;->a(Landroid/view/View;II)V

    .line 1517052
    invoke-virtual {p1}, LX/9dK;->b()Landroid/widget/TextView;

    move-result-object v2

    iput-object v2, p1, LX/9dK;->h:Landroid/widget/TextView;

    .line 1517053
    iget-object v2, p1, LX/9dK;->h:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    .line 1517054
    iget-object v2, p1, LX/9dK;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1517055
    :cond_0
    invoke-virtual {p1}, LX/9dK;->g()F

    move-result v2

    iput v2, p1, LX/9dK;->e:F

    .line 1517056
    invoke-virtual {p1}, LX/9dK;->h()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1517057
    sget-object v3, LX/9dK;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1517058
    :goto_0
    return-void

    .line 1517059
    :cond_1
    sget-object v3, LX/9dK;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1517060
    :goto_1
    goto :goto_0

    .line 1517061
    :cond_2
    const/4 v3, 0x1

    iput-boolean v3, p1, LX/9dK;->d:Z

    .line 1517062
    iget-object v3, p1, LX/9dK;->b:Landroid/view/ViewGroup;

    new-instance v4, Lcom/facebook/photos/creativeediting/swipeable/composer/nux/SwipeableAnimatingNuxViewController$2;

    invoke-direct {v4, p1}, Lcom/facebook/photos/creativeediting/swipeable/composer/nux/SwipeableAnimatingNuxViewController$2;-><init>(LX/9dK;)V

    const-wide/16 v5, 0xc8

    invoke-virtual {v3, v4, v5, v6}, Landroid/view/ViewGroup;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 1517063
    :cond_3
    const/4 v3, 0x1

    iput-boolean v3, p1, LX/9dK;->d:Z

    .line 1517064
    iget-object v3, p1, LX/9dK;->b:Landroid/view/ViewGroup;

    new-instance v4, Lcom/facebook/photos/creativeediting/swipeable/composer/nux/SwipeableAnimatingNuxViewController$1;

    invoke-direct {v4, p1}, Lcom/facebook/photos/creativeediting/swipeable/composer/nux/SwipeableAnimatingNuxViewController$1;-><init>(LX/9dK;)V

    const-wide/16 v5, 0xc8

    invoke-virtual {v3, v4, v5, v6}, Landroid/view/ViewGroup;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1
.end method
