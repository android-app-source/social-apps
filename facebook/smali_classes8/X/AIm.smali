.class public LX/AIm;
.super LX/1OM;
.source ""

# interfaces
.implements LX/1OO;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;",
        "LX/1OO",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/AJ0;

.field private final b:LX/AJ8;

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/model/AudienceControlData;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/model/AudienceControlData;",
            ">;"
        }
    .end annotation
.end field

.field public e:Z


# direct methods
.method public constructor <init>(ZLX/AJ8;LX/AJ0;)V
    .locals 1
    .param p1    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/AJ8;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1660340
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1660341
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1660342
    iput-object v0, p0, LX/AIm;->c:LX/0Px;

    .line 1660343
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1660344
    iput-object v0, p0, LX/AIm;->d:LX/0Px;

    .line 1660345
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/AIm;->e:Z

    .line 1660346
    iput-boolean p1, p0, LX/AIm;->e:Z

    .line 1660347
    iput-object p2, p0, LX/AIm;->b:LX/AJ8;

    .line 1660348
    iput-object p3, p0, LX/AIm;->a:LX/AJ0;

    .line 1660349
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1660327
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1660328
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1660329
    packed-switch p2, :pswitch_data_0

    .line 1660330
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1660331
    :pswitch_0
    const v1, 0x7f031312

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1660332
    iget-object v1, p0, LX/AIm;->a:LX/AJ0;

    .line 1660333
    new-instance v2, LX/AIz;

    const/16 p0, 0xbca

    invoke-static {v1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v2, v0, p0}, LX/AIz;-><init>(Landroid/view/View;LX/0Ot;)V

    .line 1660334
    move-object v0, v2

    .line 1660335
    goto :goto_0

    .line 1660336
    :pswitch_1
    const v1, 0x7f03130f

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1660337
    new-instance v0, Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;

    iget-object v2, p0, LX/AIm;->b:LX/AJ8;

    invoke-direct {v0, v1, v2}, Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;-><init>(Landroid/view/View;LX/AJ8;)V

    goto :goto_0

    .line 1660338
    :pswitch_2
    const v1, 0x7f031314

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1660339
    new-instance v0, LX/AIl;

    invoke-direct {v0, p0, v1}, LX/AIl;-><init>(LX/AIm;Landroid/view/View;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(LX/1a1;I)V
    .locals 4

    .prologue
    .line 1660304
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    .line 1660305
    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 1660306
    check-cast p1, LX/AIz;

    .line 1660307
    const v0, 0x7f082885

    .line 1660308
    iget-object v1, p1, LX/AIz;->l:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1660309
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 1660310
    iget-object v2, p1, LX/AIz;->l:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbTextView;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 1660311
    iget v2, v1, Landroid/graphics/Rect;->left:I

    if-nez v2, :cond_0

    iget v2, v1, Landroid/graphics/Rect;->right:I

    if-nez v2, :cond_0

    iget v2, v1, Landroid/graphics/Rect;->top:I

    if-nez v2, :cond_0

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    if-eqz v1, :cond_3

    :cond_0
    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 1660312
    if-eqz v1, :cond_1

    .line 1660313
    iget-object v1, p1, LX/AIz;->m:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/15W;

    iget-object v2, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object p0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SHARESHEET_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v3, p0}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    const-class p0, LX/AJ7;

    iget-object p2, p1, LX/AIz;->l:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v2, v3, p0, p2}, LX/15W;->a(Landroid/content/Context;Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;Ljava/lang/Object;)Z

    .line 1660314
    :cond_1
    :goto_1
    return-void

    .line 1660315
    :cond_2
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 1660316
    check-cast p1, Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;

    .line 1660317
    invoke-virtual {p0, p2}, LX/AIm;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/model/AudienceControlData;

    .line 1660318
    iget-object v1, p0, LX/AIm;->b:LX/AJ8;

    .line 1660319
    iget-object v2, v1, LX/AJ8;->a:Lcom/facebook/audience/sharesheet/app/SharesheetFragment;

    iget-object v2, v2, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->u:LX/AJZ;

    .line 1660320
    iget-object v3, v2, LX/AJZ;->a:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    move v2, v3

    .line 1660321
    move v1, v2

    .line 1660322
    iput-object v0, p1, Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;->n:Lcom/facebook/audience/model/AudienceControlData;

    .line 1660323
    iget-object v2, p1, Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;->r:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/audience/model/AudienceControlData;->getProfileUri()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object p0, Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;->l:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1660324
    iget-object v2, p1, Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;->u:Lcom/facebook/resources/ui/FbCheckBox;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbCheckBox;->setChecked(Z)V

    .line 1660325
    invoke-virtual {v0}, Lcom/facebook/audience/model/AudienceControlData;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;->p:Ljava/lang/String;

    invoke-static {p1, v1, v2, v3}, Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;->a(Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;ZLjava/lang/String;Ljava/lang/String;)V

    .line 1660326
    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1660350
    iget-object v0, p0, LX/AIm;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1660301
    invoke-virtual {p0}, LX/AIm;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1660302
    iget-object v0, p0, LX/AIm;->d:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 1660303
    :goto_0
    return-object v0

    :cond_0
    if-nez p1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, LX/AIm;->c:LX/0Px;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/model/AudienceControlData;

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    const/4 v0, 0x2

    .line 1660295
    invoke-virtual {p0}, LX/AIm;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1660296
    :cond_0
    :goto_0
    return v0

    .line 1660297
    :cond_1
    if-nez p1, :cond_2

    .line 1660298
    const/4 v0, 0x1

    goto :goto_0

    .line 1660299
    :cond_2
    iget-boolean v1, p0, LX/AIm;->e:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne p1, v1, :cond_0

    .line 1660300
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public final ii_()I
    .locals 1

    .prologue
    .line 1660294
    const/4 v0, 0x3

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1660288
    invoke-virtual {p0}, LX/AIm;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1660289
    iget-object v0, p0, LX/AIm;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 1660290
    :goto_0
    return v0

    .line 1660291
    :cond_0
    iget-boolean v0, p0, LX/AIm;->e:Z

    if-nez v0, :cond_1

    .line 1660292
    iget-object v0, p0, LX/AIm;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 1660293
    :cond_1
    iget-object v0, p0, LX/AIm;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
