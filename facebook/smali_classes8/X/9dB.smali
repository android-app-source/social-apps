.class public final LX/9dB;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/9dC;


# direct methods
.method public constructor <init>(LX/9dC;)V
    .locals 0

    .prologue
    .line 1517657
    iput-object p1, p0, LX/9dB;->a:LX/9dC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 1517658
    monitor-enter p0

    .line 1517659
    :try_start_0
    iget-object v0, p0, LX/9dB;->a:LX/9dC;

    iget-object v0, v0, LX/9dC;->i:LX/1FJ;

    invoke-static {v0}, LX/1FJ;->b(LX/1FJ;)LX/1FJ;

    move-result-object v0

    .line 1517660
    if-eqz v0, :cond_0

    .line 1517661
    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    .line 1517662
    monitor-exit p0

    .line 1517663
    :goto_0
    return-void

    .line 1517664
    :cond_0
    iget-object v0, p0, LX/9dB;->a:LX/9dC;

    iget-object v0, v0, LX/9dC;->a:Lcom/facebook/photos/imageprocessing/FiltersEngine;

    invoke-virtual {v0, p1}, Lcom/facebook/photos/imageprocessing/FiltersEngine;->a(Landroid/graphics/Bitmap;)LX/8Ht;

    move-result-object v0

    .line 1517665
    iget-object v1, p0, LX/9dB;->a:LX/9dC;

    invoke-static {v0}, LX/1FJ;->a(Ljava/io/Closeable;)LX/1FJ;

    move-result-object v0

    .line 1517666
    iput-object v0, v1, LX/9dC;->i:LX/1FJ;

    .line 1517667
    iget-object v0, p0, LX/9dB;->a:LX/9dC;

    iget-object v0, v0, LX/9dC;->c:LX/9dD;

    iget-object v1, p0, LX/9dB;->a:LX/9dC;

    iget-object v1, v1, LX/9dC;->i:LX/1FJ;

    invoke-virtual {v0, v1}, LX/9dD;->a(LX/1FJ;)V

    .line 1517668
    iget-object v0, p0, LX/9dB;->a:LX/9dC;

    iget-object v0, v0, LX/9dC;->d:LX/9dD;

    iget-object v1, p0, LX/9dB;->a:LX/9dC;

    iget-object v1, v1, LX/9dC;->i:LX/1FJ;

    invoke-virtual {v0, v1}, LX/9dD;->a(LX/1FJ;)V

    .line 1517669
    iget-object v0, p0, LX/9dB;->a:LX/9dC;

    iget-object v0, v0, LX/9dC;->e:LX/9dD;

    iget-object v1, p0, LX/9dB;->a:LX/9dC;

    iget-object v1, v1, LX/9dC;->i:LX/1FJ;

    invoke-virtual {v0, v1}, LX/9dD;->a(LX/1FJ;)V

    .line 1517670
    iget-object v0, p0, LX/9dB;->a:LX/9dC;

    invoke-static {v0}, LX/9dC;->g$redex0(LX/9dC;)V

    .line 1517671
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
