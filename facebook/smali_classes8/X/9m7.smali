.class public final LX/9m7;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 16

    .prologue
    .line 1534914
    const/4 v12, 0x0

    .line 1534915
    const/4 v11, 0x0

    .line 1534916
    const/4 v10, 0x0

    .line 1534917
    const/4 v9, 0x0

    .line 1534918
    const/4 v8, 0x0

    .line 1534919
    const/4 v7, 0x0

    .line 1534920
    const/4 v6, 0x0

    .line 1534921
    const/4 v5, 0x0

    .line 1534922
    const/4 v4, 0x0

    .line 1534923
    const/4 v3, 0x0

    .line 1534924
    const/4 v2, 0x0

    .line 1534925
    const/4 v1, 0x0

    .line 1534926
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v13, v14, :cond_1

    .line 1534927
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1534928
    const/4 v1, 0x0

    .line 1534929
    :goto_0
    return v1

    .line 1534930
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1534931
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->END_OBJECT:LX/15z;

    if-eq v13, v14, :cond_a

    .line 1534932
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v13

    .line 1534933
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1534934
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v14, v15, :cond_1

    if-eqz v13, :cond_1

    .line 1534935
    const-string v14, "done_button_label"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 1534936
    invoke-static/range {p0 .. p1}, LX/9m0;->a(LX/15w;LX/186;)I

    move-result v12

    goto :goto_1

    .line 1534937
    :cond_2
    const-string v14, "empty_tags_allowed"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 1534938
    const/4 v3, 0x1

    .line 1534939
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    goto :goto_1

    .line 1534940
    :cond_3
    const-string v14, "enabled"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 1534941
    const/4 v2, 0x1

    .line 1534942
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    goto :goto_1

    .line 1534943
    :cond_4
    const-string v14, "id"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 1534944
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 1534945
    :cond_5
    const-string v14, "is_uf_eligible"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 1534946
    const/4 v1, 0x1

    .line 1534947
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v8

    goto :goto_1

    .line 1534948
    :cond_6
    const-string v14, "persistent_units"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 1534949
    invoke-static/range {p0 .. p1}, LX/9m2;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1534950
    :cond_7
    const-string v14, "report_tags"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 1534951
    invoke-static/range {p0 .. p1}, LX/9m3;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1534952
    :cond_8
    const-string v14, "subtitle"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 1534953
    invoke-static/range {p0 .. p1}, LX/9m5;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 1534954
    :cond_9
    const-string v14, "title"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 1534955
    invoke-static/range {p0 .. p1}, LX/9m6;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 1534956
    :cond_a
    const/16 v13, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 1534957
    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 1534958
    if-eqz v3, :cond_b

    .line 1534959
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->a(IZ)V

    .line 1534960
    :cond_b
    if-eqz v2, :cond_c

    .line 1534961
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->a(IZ)V

    .line 1534962
    :cond_c
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1534963
    if-eqz v1, :cond_d

    .line 1534964
    const/4 v1, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v8}, LX/186;->a(IZ)V

    .line 1534965
    :cond_d
    const/4 v1, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1534966
    const/4 v1, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1534967
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1534968
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1534969
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 7

    .prologue
    .line 1534970
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1534971
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1534972
    if-eqz v0, :cond_1

    .line 1534973
    const-string v1, "done_button_label"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1534974
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1534975
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1534976
    if-eqz v1, :cond_0

    .line 1534977
    const-string v2, "text"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1534978
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1534979
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1534980
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1534981
    if-eqz v0, :cond_2

    .line 1534982
    const-string v1, "empty_tags_allowed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1534983
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1534984
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1534985
    if-eqz v0, :cond_3

    .line 1534986
    const-string v1, "enabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1534987
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1534988
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1534989
    if-eqz v0, :cond_4

    .line 1534990
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1534991
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1534992
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1534993
    if-eqz v0, :cond_5

    .line 1534994
    const-string v1, "is_uf_eligible"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1534995
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1534996
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1534997
    if-eqz v0, :cond_10

    .line 1534998
    const-string v1, "persistent_units"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1534999
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1535000
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_f

    .line 1535001
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    const/4 v4, 0x0

    .line 1535002
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1535003
    invoke-virtual {p0, v2, v4}, LX/15i;->g(II)I

    move-result v3

    .line 1535004
    if-eqz v3, :cond_6

    .line 1535005
    const-string v3, "__type__"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1535006
    invoke-static {p0, v2, v4, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1535007
    :cond_6
    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, LX/15i;->b(II)Z

    move-result v3

    .line 1535008
    if-eqz v3, :cond_7

    .line 1535009
    const-string v4, "is_optional"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1535010
    invoke-virtual {p2, v3}, LX/0nX;->a(Z)V

    .line 1535011
    :cond_7
    const/4 v3, 0x2

    invoke-virtual {p0, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1535012
    if-eqz v3, :cond_b

    .line 1535013
    const-string v4, "message"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1535014
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1535015
    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, LX/15i;->g(II)I

    move-result v4

    .line 1535016
    if-eqz v4, :cond_9

    .line 1535017
    const-string v5, "ranges"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1535018
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1535019
    const/4 v5, 0x0

    :goto_1
    invoke-virtual {p0, v4}, LX/15i;->c(I)I

    move-result v6

    if-ge v5, v6, :cond_8

    .line 1535020
    invoke-virtual {p0, v4, v5}, LX/15i;->q(II)I

    move-result v6

    invoke-static {p0, v6, p2, p3}, LX/9m1;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1535021
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 1535022
    :cond_8
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1535023
    :cond_9
    const/4 v4, 0x1

    invoke-virtual {p0, v3, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1535024
    if-eqz v4, :cond_a

    .line 1535025
    const-string v5, "text"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1535026
    invoke-virtual {p2, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1535027
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1535028
    :cond_b
    const/4 v3, 0x3

    invoke-virtual {p0, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1535029
    if-eqz v3, :cond_c

    .line 1535030
    const-string v4, "message_type"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1535031
    invoke-virtual {p2, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1535032
    :cond_c
    const/4 v3, 0x4

    invoke-virtual {p0, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1535033
    if-eqz v3, :cond_e

    .line 1535034
    const-string v4, "placeholder_text"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1535035
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1535036
    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1535037
    if-eqz v4, :cond_d

    .line 1535038
    const-string v5, "text"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1535039
    invoke-virtual {p2, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1535040
    :cond_d
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1535041
    :cond_e
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1535042
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 1535043
    :cond_f
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1535044
    :cond_10
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1535045
    if-eqz v0, :cond_12

    .line 1535046
    const-string v1, "report_tags"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1535047
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1535048
    const/4 v1, 0x0

    :goto_2
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_11

    .line 1535049
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/9m3;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1535050
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1535051
    :cond_11
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1535052
    :cond_12
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1535053
    if-eqz v0, :cond_16

    .line 1535054
    const-string v1, "subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1535055
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1535056
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1535057
    if-eqz v1, :cond_14

    .line 1535058
    const-string v2, "ranges"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1535059
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1535060
    const/4 v2, 0x0

    :goto_3
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v3

    if-ge v2, v3, :cond_13

    .line 1535061
    invoke-virtual {p0, v1, v2}, LX/15i;->q(II)I

    move-result v3

    invoke-static {p0, v3, p2, p3}, LX/9m4;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1535062
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1535063
    :cond_13
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1535064
    :cond_14
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1535065
    if-eqz v1, :cond_15

    .line 1535066
    const-string v2, "text"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1535067
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1535068
    :cond_15
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1535069
    :cond_16
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1535070
    if-eqz v0, :cond_18

    .line 1535071
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1535072
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1535073
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1535074
    if-eqz v1, :cond_17

    .line 1535075
    const-string v2, "text"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1535076
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1535077
    :cond_17
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1535078
    :cond_18
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1535079
    return-void
.end method
