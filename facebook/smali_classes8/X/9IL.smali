.class public LX/9IL;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/9IJ;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9IN;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1462906
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/9IL;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/9IN;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1462907
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1462908
    iput-object p1, p0, LX/9IL;->b:LX/0Ot;

    .line 1462909
    return-void
.end method

.method public static a(LX/0QB;)LX/9IL;
    .locals 4

    .prologue
    .line 1462865
    const-class v1, LX/9IL;

    monitor-enter v1

    .line 1462866
    :try_start_0
    sget-object v0, LX/9IL;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1462867
    sput-object v2, LX/9IL;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1462868
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1462869
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1462870
    new-instance v3, LX/9IL;

    const/16 p0, 0x1dea

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/9IL;-><init>(LX/0Ot;)V

    .line 1462871
    move-object v0, v3

    .line 1462872
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1462873
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/9IL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1462874
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1462875
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1462905
    const v0, -0x700d51a1

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 1462899
    check-cast p2, LX/9IK;

    .line 1462900
    iget-object v0, p0, LX/9IL;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9IN;

    iget-object v1, p2, LX/9IK;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget-boolean v2, p2, LX/9IK;->b:Z

    const/16 p2, 0x18

    const/4 p0, 0x1

    const/4 v7, 0x0

    .line 1462901
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    iget-object v4, v0, LX/9IN;->e:LX/9IQ;

    invoke-virtual {v4, p1}, LX/9IQ;->c(LX/1De;)LX/9IO;

    move-result-object v4

    invoke-virtual {v4, v1}, LX/9IO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/9IO;

    move-result-object v4

    iget-object v5, v0, LX/9IN;->f:LX/0yI;

    iget-object v6, v0, LX/9IN;->g:LX/0ad;

    invoke-static {v5, v6}, LX/9IN;->a(LX/0yI;LX/0ad;)Z

    move-result v5

    invoke-virtual {v4, v5}, LX/9IO;->a(Z)LX/9IO;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    .line 1462902
    const v5, -0x700d51a1

    const/4 v6, 0x0

    invoke-static {p1, v5, v6}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v5, v5

    .line 1462903
    invoke-interface {v4, v5}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    if-eqz v2, :cond_0

    const/4 v3, 0x0

    :goto_0
    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1462904
    return-object v0

    :cond_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, p0}, LX/1Dh;->C(I)LX/1Dh;

    move-result-object v3

    const/16 v5, 0x8

    invoke-interface {v3, v5, v7}, LX/1Dh;->y(II)LX/1Dh;

    move-result-object v3

    const/4 v5, 0x2

    invoke-interface {v3, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, p0}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v3

    const v5, 0x7f0e01b6

    invoke-static {p1, v7, v5}, LX/5Jt;->a(LX/1De;II)LX/5Jr;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    invoke-interface {v5, p2}, LX/1Di;->j(I)LX/1Di;

    move-result-object v5

    invoke-interface {v5, p2}, LX/1Di;->r(I)LX/1Di;

    move-result-object v5

    invoke-interface {v3, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1462876
    invoke-static {}, LX/1dS;->b()V

    .line 1462877
    iget v0, p1, LX/1dQ;->b:I

    .line 1462878
    packed-switch v0, :pswitch_data_0

    .line 1462879
    :goto_0
    return-object v2

    .line 1462880
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1462881
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1462882
    check-cast v1, LX/9IK;

    .line 1462883
    iget-object v3, p0, LX/9IL;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/9IN;

    iget-object v4, v1, LX/9IK;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1462884
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object p2

    .line 1462885
    invoke-static {v4}, LX/1VO;->k(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1462886
    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    .line 1462887
    if-nez v0, :cond_0

    .line 1462888
    :goto_1
    goto :goto_0

    .line 1462889
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1462890
    sget-object v5, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v0, v5}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    goto :goto_1

    .line 1462891
    :cond_1
    sget-object v5, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v0, v5}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    goto :goto_1

    .line 1462892
    :cond_2
    iget-object v5, v3, LX/9IN;->d:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1nB;

    .line 1462893
    iget-object v6, v3, LX/9IN;->c:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/support/v4/app/FragmentActivity;

    .line 1462894
    iget-object v7, v3, LX/9IN;->b:LX/0Or;

    invoke-interface {v7}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    .line 1462895
    iget-object p1, v3, LX/9IN;->a:LX/0Or;

    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/121;

    .line 1462896
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLMedia;->aS()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v5, p2}, LX/1nB;->c(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    .line 1462897
    sget-object p2, LX/0yY;->VIDEO_PLAY_INTERSTITIAL:LX/0yY;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    const v1, 0x7f080e4a

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    new-instance v1, LX/9IM;

    invoke-direct {v1, v3, v7, v5, v0}, LX/9IM;-><init>(LX/9IN;Lcom/facebook/content/SecureContextHelper;Landroid/content/Intent;Landroid/view/View;)V

    invoke-virtual {p1, p2, p0, v1}, LX/121;->a(LX/0yY;Ljava/lang/String;LX/39A;)LX/121;

    .line 1462898
    sget-object v5, LX/0yY;->VIDEO_PLAY_INTERSTITIAL:LX/0yY;

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v6

    invoke-virtual {p1, v5, v6}, LX/121;->a(LX/0yY;LX/0gc;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch -0x700d51a1
        :pswitch_0
    .end packed-switch
.end method
