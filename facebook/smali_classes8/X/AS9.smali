.class public final LX/AS9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field public final synthetic a:LX/ASA;


# direct methods
.method public constructor <init>(LX/ASA;)V
    .locals 0

    .prologue
    .line 1673692
    iput-object p1, p0, LX/AS9;->a:LX/ASA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1673693
    sget-object v0, LX/AS4;->FEED_ONLY_POST_NUX:LX/AS4;

    invoke-virtual {v0}, LX/AS4;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1673694
    iget-object v0, p0, LX/AS9;->a:LX/ASA;

    iget-object v0, v0, LX/ASA;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/AS4;->FEED_ONLY_POST_NUX:LX/AS4;

    iget-object v1, v1, LX/AS4;->prefKey:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1673695
    iget-object v0, p0, LX/AS9;->a:LX/ASA;

    new-array v1, v4, [Ljava/lang/String;

    const/4 v2, 0x0

    sget-object v3, LX/AS4;->FEED_ONLY_POST_NUX:LX/AS4;

    iget-object v3, v3, LX/AS4;->interstitialId:Ljava/lang/String;

    aput-object v3, v1, v2

    .line 1673696
    new-instance v2, Lcom/facebook/interstitial/api/FetchInterstitialsParams;

    invoke-static {v1}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/facebook/interstitial/api/FetchInterstitialsParams;-><init>(LX/0Px;)V

    .line 1673697
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1673698
    const-string p0, "fetchAndUpdateInterstitialsParams"

    invoke-virtual {v3, p0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1673699
    iget-object v2, v0, LX/ASA;->b:LX/0aG;

    const-string p0, "interstitials_fetch_and_update"

    const p1, -0x55d832f1

    invoke-static {v2, p0, v3, p1}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v2

    invoke-interface {v2}, LX/1MF;->start()LX/1ML;

    .line 1673700
    :cond_0
    return v4
.end method
