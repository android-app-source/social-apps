.class public final LX/9I0;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/9I1;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public b:Z

.field public final synthetic c:LX/9I1;


# direct methods
.method public constructor <init>(LX/9I1;)V
    .locals 1

    .prologue
    .line 1462366
    iput-object p1, p0, LX/9I0;->c:LX/9I1;

    .line 1462367
    move-object v0, p1

    .line 1462368
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1462369
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1462370
    const-string v0, "CommentPhotoAttachmentComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1462371
    if-ne p0, p1, :cond_1

    .line 1462372
    :cond_0
    :goto_0
    return v0

    .line 1462373
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1462374
    goto :goto_0

    .line 1462375
    :cond_3
    check-cast p1, LX/9I0;

    .line 1462376
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1462377
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1462378
    if-eq v2, v3, :cond_0

    .line 1462379
    iget-object v2, p0, LX/9I0;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/9I0;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget-object v3, p1, LX/9I0;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1462380
    goto :goto_0

    .line 1462381
    :cond_5
    iget-object v2, p1, LX/9I0;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-nez v2, :cond_4

    .line 1462382
    :cond_6
    iget-boolean v2, p0, LX/9I0;->b:Z

    iget-boolean v3, p1, LX/9I0;->b:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1462383
    goto :goto_0
.end method
