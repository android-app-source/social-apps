.class public LX/9j2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private a:Landroid/view/GestureDetector;

.field public b:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

.field public c:LX/9iy;

.field public d:Z

.field public e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;LX/9iy;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1529019
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1529020
    iput-boolean v3, p0, LX/9j2;->d:Z

    .line 1529021
    iput-boolean v3, p0, LX/9j2;->e:Z

    .line 1529022
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, LX/9j1;

    invoke-direct {v1, p0}, LX/9j1;-><init>(LX/9j2;)V

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2, v3}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;Z)V

    iput-object v0, p0, LX/9j2;->a:Landroid/view/GestureDetector;

    .line 1529023
    iput-object p2, p0, LX/9j2;->b:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    .line 1529024
    iput-object p3, p0, LX/9j2;->c:LX/9iy;

    .line 1529025
    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 1529026
    invoke-virtual {p1}, Landroid/view/View;->getX()F

    move-result v0

    .line 1529027
    invoke-virtual {p1}, Landroid/view/View;->getY()F

    move-result v1

    .line 1529028
    iget-object v2, p0, LX/9j2;->a:Landroid/view/GestureDetector;

    invoke-virtual {v2, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1529029
    invoke-virtual {p2, v0, v1}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 1529030
    iget-object v0, p0, LX/9j2;->b:Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    invoke-virtual {v0, p2}, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1529031
    const/4 v0, 0x1

    return v0
.end method
