.class public final enum LX/9FF;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9FF;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9FF;

.field public static final enum REPLY:LX/9FF;

.field public static final enum TOP_LEVEL:LX/9FF;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1458350
    new-instance v0, LX/9FF;

    const-string v1, "TOP_LEVEL"

    invoke-direct {v0, v1, v2}, LX/9FF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9FF;->TOP_LEVEL:LX/9FF;

    .line 1458351
    new-instance v0, LX/9FF;

    const-string v1, "REPLY"

    invoke-direct {v0, v1, v3}, LX/9FF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9FF;->REPLY:LX/9FF;

    .line 1458352
    const/4 v0, 0x2

    new-array v0, v0, [LX/9FF;

    sget-object v1, LX/9FF;->TOP_LEVEL:LX/9FF;

    aput-object v1, v0, v2

    sget-object v1, LX/9FF;->REPLY:LX/9FF;

    aput-object v1, v0, v3

    sput-object v0, LX/9FF;->$VALUES:[LX/9FF;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1458353
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9FF;
    .locals 1

    .prologue
    .line 1458354
    const-class v0, LX/9FF;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9FF;

    return-object v0
.end method

.method public static values()[LX/9FF;
    .locals 1

    .prologue
    .line 1458355
    sget-object v0, LX/9FF;->$VALUES:[LX/9FF;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9FF;

    return-object v0
.end method
