.class public LX/AKm;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private a:Landroid/view/ViewStub;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private b:LX/AKl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/AKl",
            "<TV;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private c:Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/ViewStub;)V
    .locals 1
    .param p1    # Landroid/view/ViewStub;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 1664256
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1664257
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, LX/AKm;->a:Landroid/view/ViewStub;

    .line 1664258
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1664259
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/AKm;->c:Landroid/view/View;

    if-nez v0, :cond_0

    .line 1664260
    iget-object v0, p0, LX/AKm;->a:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    .line 1664261
    iput-object v0, p0, LX/AKm;->c:Landroid/view/View;

    .line 1664262
    const/4 v0, 0x0

    iput-object v0, p0, LX/AKm;->b:LX/AKl;

    .line 1664263
    const/4 v0, 0x0

    iput-object v0, p0, LX/AKm;->a:Landroid/view/ViewStub;

    .line 1664264
    :cond_0
    iget-object v0, p0, LX/AKm;->c:Landroid/view/View;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 1664265
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
