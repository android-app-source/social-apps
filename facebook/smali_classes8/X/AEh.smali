.class public final LX/AEh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/AEi;

.field private final b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/AEi;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1646959
    iput-object p1, p0, LX/AEh;->a:LX/AEi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1646960
    iput-object p2, p0, LX/AEh;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1646961
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x7adc9384

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1646962
    iget-object v0, p0, LX/AEh;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1646963
    iget-object v0, p0, LX/AEh;->a:LX/AEi;

    iget-object v0, v0, LX/AEi;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1646964
    iget-boolean v3, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v3, v3

    .line 1646965
    if-nez v3, :cond_0

    .line 1646966
    iget-object v0, p0, LX/AEh;->a:LX/AEi;

    invoke-static {v0, v1}, LX/AEi;->b(LX/AEi;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1646967
    const v0, 0x5d4195c2

    invoke-static {v4, v4, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1646968
    :goto_0
    return-void

    .line 1646969
    :cond_0
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v3

    .line 1646970
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    .line 1646971
    if-nez v1, :cond_1

    const/4 v1, 0x0

    .line 1646972
    :goto_1
    iget-object v3, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v3

    .line 1646973
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1646974
    iget-object v0, p0, LX/AEh;->a:LX/AEi;

    iget-object v0, v0, LX/AEi;->b:LX/0kL;

    new-instance v1, LX/27k;

    const v3, 0x7f081123

    invoke-direct {v1, v3}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1646975
    :goto_2
    const v0, 0x1be4ae67

    invoke-static {v0, v2}, LX/02F;->a(II)V

    goto :goto_0

    .line 1646976
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 1646977
    :cond_2
    iget-object v0, p0, LX/AEh;->a:LX/AEi;

    iget-object v0, v0, LX/AEi;->b:LX/0kL;

    new-instance v1, LX/27k;

    const v3, 0x7f081124

    invoke-direct {v1, v3}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    goto :goto_2
.end method
