.class public final LX/9C2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:Landroid/os/Bundle;

.field public final synthetic c:Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1452997
    iput-object p1, p0, LX/9C2;->c:Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;

    iput-object p2, p0, LX/9C2;->a:Landroid/content/Context;

    iput-object p3, p0, LX/9C2;->b:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 15

    .prologue
    .line 1452998
    iget-object v0, p0, LX/9C2;->c:Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;

    iget-object v1, p0, LX/9C2;->a:Landroid/content/Context;

    .line 1452999
    const/4 v9, 0x0

    .line 1453000
    iget-object v3, v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->ab:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1453001
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 1453002
    :cond_0
    iget-object v3, v0, Landroid/support/v4/app/Fragment;->mParentFragment:Landroid/support/v4/app/Fragment;

    move-object v3, v3

    .line 1453003
    check-cast v3, LX/0hG;

    iput-object v3, v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->H:LX/0hG;

    .line 1453004
    iget-object v3, v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->e:LX/9Dk;

    iget-object v4, v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->N:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    iget-object v5, v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->R:LX/9CA;

    invoke-virtual {v3, v4, v5}, LX/9Dk;->a(Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;LX/1DI;)LX/9Dj;

    move-result-object v3

    iput-object v3, v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->O:LX/9Dj;

    .line 1453005
    iget-object v3, v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->O:LX/9Dj;

    invoke-virtual {v3}, LX/62O;->a()V

    .line 1453006
    iget-object v3, v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->c:LX/9DH;

    iget-object v4, v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->H:LX/0hG;

    iget-object v5, v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1453007
    iget-object v6, v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->i:LX/9DO;

    invoke-virtual {v6, v4, v5}, LX/9DO;->a(LX/0hG;Lcom/facebook/ufiservices/flyout/FeedbackParams;)LX/9DN;

    move-result-object v6

    move-object v6, v6

    .line 1453008
    iget-object v4, v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1453009
    iget-object v5, v4, Lcom/facebook/ufiservices/flyout/FeedbackParams;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v7, v5

    .line 1453010
    iget-object v4, v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1453011
    iget-object v5, v4, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v8, v5

    .line 1453012
    const/4 v10, 0x1

    new-instance v12, LX/9C3;

    invoke-direct {v12, v0}, LX/9C3;-><init>(Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;)V

    new-instance v14, LX/9C4;

    invoke-direct {v14, v0}, LX/9C4;-><init>(Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;)V

    move-object v4, v0

    move-object v5, v1

    move v11, v9

    move v13, v9

    invoke-virtual/range {v3 .. v14}, LX/9DH;->a(Landroid/support/v4/app/Fragment;Landroid/content/Context;LX/9Bj;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;ZZZLX/9Br;ZLX/0QK;)LX/9DG;

    move-result-object v3

    iput-object v3, v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->J:LX/9DG;

    .line 1453013
    iget-object v3, v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1453014
    iget-object v4, v3, Lcom/facebook/ufiservices/flyout/FeedbackParams;->k:Ljava/lang/Long;

    move-object v3, v4

    .line 1453015
    if-eqz v3, :cond_1

    .line 1453016
    iget-object v3, v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->J:LX/9DG;

    iget-object v4, v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1453017
    iget-object v5, v4, Lcom/facebook/ufiservices/flyout/FeedbackParams;->k:Ljava/lang/Long;

    move-object v4, v5

    .line 1453018
    invoke-virtual {v3, v4}, LX/9DG;->a(Ljava/lang/Long;)V

    .line 1453019
    :cond_1
    iget-object v3, v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->P:Ljava/util/Set;

    iget-object v4, v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->J:LX/9DG;

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1453020
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1453021
    iget-object v4, v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->w:LX/9Dd;

    if-eqz v4, :cond_2

    .line 1453022
    iget-object v4, v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->w:LX/9Dd;

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1453023
    :cond_2
    iget-object v4, v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->J:LX/9DG;

    .line 1453024
    iget-object v5, v4, LX/9DG;->b:LX/9CC;

    move-object v4, v5

    .line 1453025
    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1453026
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-static {v3}, LX/62C;->b(Ljava/util/List;)LX/62C;

    move-result-object v3

    iput-object v3, v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->M:LX/62C;

    .line 1453027
    iget-object v3, v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->J:LX/9DG;

    iget-object v4, v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1453028
    iget-object v5, v4, Lcom/facebook/ufiservices/flyout/FeedbackParams;->l:Lcom/facebook/ipc/media/MediaItem;

    move-object v4, v5

    .line 1453029
    invoke-virtual {v3, v4}, LX/9DG;->a(Lcom/facebook/ipc/media/MediaItem;)V

    .line 1453030
    iget-object v3, v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->j:LX/3iR;

    iget-object v4, v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;->F:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    .line 1453031
    iget-object v5, v4, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v4, v5

    .line 1453032
    invoke-virtual {v3, v4}, LX/3iR;->b(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V

    goto/16 :goto_0
.end method
