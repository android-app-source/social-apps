.class public final LX/8kS;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 1396436
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1396437
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1396438
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1396439
    invoke-static {p0, p1}, LX/8kS;->b(LX/15w;LX/186;)I

    move-result v1

    .line 1396440
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1396441
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1396442
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1396443
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1396444
    if-eqz v0, :cond_0

    .line 1396445
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1396446
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1396447
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1396448
    if-eqz v0, :cond_1

    .line 1396449
    const-string v1, "is_composer_capable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1396450
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1396451
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1396452
    cmp-long v2, v0, v4

    if-eqz v2, :cond_2

    .line 1396453
    const-string v2, "published_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1396454
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1396455
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1396456
    cmp-long v2, v0, v4

    if-eqz v2, :cond_3

    .line 1396457
    const-string v2, "updated_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1396458
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1396459
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1396460
    return-void
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1396461
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1396462
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1396463
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2}, LX/8kS;->a(LX/15i;ILX/0nX;)V

    .line 1396464
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1396465
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1396466
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 12

    .prologue
    .line 1396467
    const/4 v8, 0x0

    .line 1396468
    const/4 v3, 0x0

    .line 1396469
    const-wide/16 v6, 0x0

    .line 1396470
    const-wide/16 v4, 0x0

    .line 1396471
    const/4 v2, 0x0

    .line 1396472
    const/4 v1, 0x0

    .line 1396473
    const/4 v0, 0x0

    .line 1396474
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v9, v10, :cond_9

    .line 1396475
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1396476
    const/4 v0, 0x0

    .line 1396477
    :goto_0
    return v0

    .line 1396478
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v0, v7, :cond_5

    .line 1396479
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    .line 1396480
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1396481
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v11, :cond_0

    if-eqz v0, :cond_0

    .line 1396482
    const-string v7, "id"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1396483
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    move v10, v0

    goto :goto_1

    .line 1396484
    :cond_1
    const-string v7, "is_composer_capable"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1396485
    const/4 v0, 0x1

    .line 1396486
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v4

    move v5, v4

    move v4, v0

    goto :goto_1

    .line 1396487
    :cond_2
    const-string v7, "published_time"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1396488
    const/4 v0, 0x1

    .line 1396489
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v1, v0

    goto :goto_1

    .line 1396490
    :cond_3
    const-string v7, "updated_time"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1396491
    const/4 v0, 0x1

    .line 1396492
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v6

    move-wide v8, v6

    move v6, v0

    goto :goto_1

    .line 1396493
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1396494
    :cond_5
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1396495
    const/4 v0, 0x0

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 1396496
    if-eqz v4, :cond_6

    .line 1396497
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v5}, LX/186;->a(IZ)V

    .line 1396498
    :cond_6
    if-eqz v1, :cond_7

    .line 1396499
    const/4 v1, 0x2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1396500
    :cond_7
    if-eqz v6, :cond_8

    .line 1396501
    const/4 v1, 0x3

    const-wide/16 v4, 0x0

    move-object v0, p1

    move-wide v2, v8

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1396502
    :cond_8
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_9
    move v10, v8

    move-wide v8, v4

    move v4, v2

    move v5, v3

    move-wide v2, v6

    move v6, v0

    goto/16 :goto_1
.end method
