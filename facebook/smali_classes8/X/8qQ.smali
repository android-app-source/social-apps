.class public final LX/8qQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/89l;

.field public e:Lcom/facebook/ipc/feed/ViewPermalinkParams;

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:LX/8s1;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1407088
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1407089
    sget-object v0, LX/89l;->UNKNOWN:LX/89l;

    iput-object v0, p0, LX/8qQ;->d:LX/89l;

    .line 1407090
    sget-object v0, LX/8s1;->NOT_SUPPORTED:LX/8s1;

    iput-object v0, p0, LX/8qQ;->j:LX/8s1;

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/ufiservices/flyout/ProfileListParams;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1407084
    iget-object v0, p0, LX/8qQ;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/8qQ;->b:Ljava/util/List;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/8qQ;->c:Ljava/util/List;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v2, "Either a feedback id, profile ids or actors list must be set"

    invoke-static {v0, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1407085
    new-instance v0, Lcom/facebook/ufiservices/flyout/ProfileListParams;

    invoke-direct {v0, p0}, Lcom/facebook/ufiservices/flyout/ProfileListParams;-><init>(LX/8qQ;)V

    .line 1407086
    return-object v0

    :cond_1
    move v0, v1

    .line 1407087
    goto :goto_0
.end method
