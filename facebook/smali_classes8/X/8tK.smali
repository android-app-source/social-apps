.class public LX/8tK;
.super LX/2Mj;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/8tK;


# direct methods
.method public constructor <init>(LX/7Ss;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1412923
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/52d;->a(Ljava/lang/Object;)LX/0Or;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/2Mj;-><init>(LX/2Md;LX/0Or;)V

    .line 1412924
    return-void

    .line 1412925
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/8tK;
    .locals 4

    .prologue
    .line 1412926
    sget-object v0, LX/8tK;->a:LX/8tK;

    if-nez v0, :cond_1

    .line 1412927
    const-class v1, LX/8tK;

    monitor-enter v1

    .line 1412928
    :try_start_0
    sget-object v0, LX/8tK;->a:LX/8tK;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1412929
    if-eqz v2, :cond_0

    .line 1412930
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1412931
    new-instance p0, LX/8tK;

    invoke-static {v0}, LX/7Ss;->a(LX/0QB;)LX/7Ss;

    move-result-object v3

    check-cast v3, LX/7Ss;

    invoke-direct {p0, v3}, LX/8tK;-><init>(LX/7Ss;)V

    .line 1412932
    move-object v0, p0

    .line 1412933
    sput-object v0, LX/8tK;->a:LX/8tK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1412934
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1412935
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1412936
    :cond_1
    sget-object v0, LX/8tK;->a:LX/8tK;

    return-object v0

    .line 1412937
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1412938
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
