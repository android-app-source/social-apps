.class public final LX/A5h;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;)V
    .locals 0

    .prologue
    .line 1621735
    iput-object p1, p0, LX/A5h;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1621737
    iget-object v0, p0, LX/A5h;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    iget-object v0, v0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->Q:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1621738
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1621739
    iget-object v0, p0, LX/A5h;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    iget-object v0, v0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->R:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    iget-object v0, p0, LX/A5h;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    iget-object v0, v0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->R:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QL;

    .line 1621740
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1621741
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1621742
    :cond_0
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QL;

    .line 1621743
    instance-of v5, v0, LX/8vB;

    if-nez v5, :cond_1

    .line 1621744
    invoke-virtual {v0}, LX/8QK;->b()Ljava/lang/String;

    move-result-object v5

    .line 1621745
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v6

    invoke-interface {p1, v1, v6}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 1621746
    iget-object v5, p0, LX/A5h;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    iget-object v5, v5, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->R:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1621747
    iget-object v5, p0, LX/A5h;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    invoke-static {v5, v0}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->a$redex0(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;LX/8QL;)V

    .line 1621748
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1621749
    :cond_2
    iget-object v0, p0, LX/A5h;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    iget-object v0, v0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->P:LX/8tC;

    invoke-virtual {v0}, LX/3Tf;->a()LX/333;

    move-result-object v0

    iget-object v2, p0, LX/A5h;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    iget-object v2, v2, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->Q:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getUserEnteredPlainText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v0, v2}, LX/333;->a(Ljava/lang/CharSequence;)V

    .line 1621750
    iget-object v0, p0, LX/A5h;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    invoke-static {v0}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->n(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;)V

    .line 1621751
    iget-object v0, p0, LX/A5h;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    iget-object v0, v0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->P:LX/8tC;

    const v2, -0x14853c9b

    invoke-static {v0, v2}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1621752
    iget-object v2, p0, LX/A5h;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    invoke-static {v2, v0}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->c(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;Z)V

    .line 1621753
    return-void

    :cond_3
    move v0, v1

    .line 1621754
    goto :goto_2
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1621755
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1621736
    return-void
.end method
