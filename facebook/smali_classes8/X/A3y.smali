.class public final LX/A3y;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 16

    .prologue
    .line 1617664
    const/4 v12, 0x0

    .line 1617665
    const/4 v11, 0x0

    .line 1617666
    const/4 v10, 0x0

    .line 1617667
    const/4 v9, 0x0

    .line 1617668
    const/4 v8, 0x0

    .line 1617669
    const/4 v7, 0x0

    .line 1617670
    const/4 v6, 0x0

    .line 1617671
    const/4 v5, 0x0

    .line 1617672
    const/4 v4, 0x0

    .line 1617673
    const/4 v3, 0x0

    .line 1617674
    const/4 v2, 0x0

    .line 1617675
    const/4 v1, 0x0

    .line 1617676
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v13, v14, :cond_1

    .line 1617677
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1617678
    const/4 v1, 0x0

    .line 1617679
    :goto_0
    return v1

    .line 1617680
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1617681
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->END_OBJECT:LX/15z;

    if-eq v13, v14, :cond_a

    .line 1617682
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v13

    .line 1617683
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1617684
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v14, v15, :cond_1

    if-eqz v13, :cond_1

    .line 1617685
    const-string v14, "count"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 1617686
    const/4 v3, 0x1

    .line 1617687
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v12

    goto :goto_1

    .line 1617688
    :cond_2
    const-string v14, "id"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 1617689
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto :goto_1

    .line 1617690
    :cond_3
    const-string v14, "more_posts_query"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 1617691
    invoke-static/range {p0 .. p1}, LX/A3w;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 1617692
    :cond_4
    const-string v14, "phrase"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 1617693
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 1617694
    :cond_5
    const-string v14, "phrase_length"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 1617695
    const/4 v2, 0x1

    .line 1617696
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v8

    goto :goto_1

    .line 1617697
    :cond_6
    const-string v14, "phrase_offset"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 1617698
    const/4 v1, 0x1

    .line 1617699
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v7

    goto :goto_1

    .line 1617700
    :cond_7
    const-string v14, "phrase_owner"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 1617701
    invoke-static/range {p0 .. p1}, LX/A3x;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 1617702
    :cond_8
    const-string v14, "sample_text"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 1617703
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto/16 :goto_1

    .line 1617704
    :cond_9
    const-string v14, "sentence"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 1617705
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_1

    .line 1617706
    :cond_a
    const/16 v13, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 1617707
    if-eqz v3, :cond_b

    .line 1617708
    const/4 v3, 0x0

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12, v13}, LX/186;->a(III)V

    .line 1617709
    :cond_b
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 1617710
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 1617711
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 1617712
    if-eqz v2, :cond_c

    .line 1617713
    const/4 v2, 0x4

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8, v3}, LX/186;->a(III)V

    .line 1617714
    :cond_c
    if-eqz v1, :cond_d

    .line 1617715
    const/4 v1, 0x5

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v7, v2}, LX/186;->a(III)V

    .line 1617716
    :cond_d
    const/4 v1, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1617717
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1617718
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1617719
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1617720
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1617721
    invoke-virtual {p0, p1, v2, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1617722
    if-eqz v0, :cond_0

    .line 1617723
    const-string v1, "count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1617724
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1617725
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1617726
    if-eqz v0, :cond_1

    .line 1617727
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1617728
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1617729
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1617730
    if-eqz v0, :cond_2

    .line 1617731
    const-string v1, "more_posts_query"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1617732
    invoke-static {p0, v0, p2, p3}, LX/A3w;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1617733
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1617734
    if-eqz v0, :cond_3

    .line 1617735
    const-string v1, "phrase"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1617736
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1617737
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1617738
    if-eqz v0, :cond_4

    .line 1617739
    const-string v1, "phrase_length"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1617740
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1617741
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1617742
    if-eqz v0, :cond_5

    .line 1617743
    const-string v1, "phrase_offset"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1617744
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1617745
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1617746
    if-eqz v0, :cond_6

    .line 1617747
    const-string v1, "phrase_owner"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1617748
    invoke-static {p0, v0, p2, p3}, LX/A3x;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1617749
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1617750
    if-eqz v0, :cond_7

    .line 1617751
    const-string v1, "sample_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1617752
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1617753
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1617754
    if-eqz v0, :cond_8

    .line 1617755
    const-string v1, "sentence"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1617756
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1617757
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1617758
    return-void
.end method
