.class public LX/9j4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/9j4;


# instance fields
.field public final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1529035
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1529036
    iput-object p1, p0, LX/9j4;->a:LX/0Zb;

    .line 1529037
    return-void
.end method

.method public static a(LX/0QB;)LX/9j4;
    .locals 4

    .prologue
    .line 1529050
    sget-object v0, LX/9j4;->b:LX/9j4;

    if-nez v0, :cond_1

    .line 1529051
    const-class v1, LX/9j4;

    monitor-enter v1

    .line 1529052
    :try_start_0
    sget-object v0, LX/9j4;->b:LX/9j4;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1529053
    if-eqz v2, :cond_0

    .line 1529054
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1529055
    new-instance p0, LX/9j4;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/9j4;-><init>(LX/0Zb;)V

    .line 1529056
    move-object v0, p0

    .line 1529057
    sput-object v0, LX/9j4;->b:LX/9j4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1529058
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1529059
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1529060
    :cond_1
    sget-object v0, LX/9j4;->b:LX/9j4;

    return-object v0

    .line 1529061
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1529062
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$CheckinPlace;",
            ">;)",
            "Lcom/facebook/analytics/logger/HoneyClientEvent;"
        }
    .end annotation

    .prologue
    .line 1529038
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1529039
    iput-object p1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 1529040
    move-object v0, v0

    .line 1529041
    const-string v1, "place_picker"

    .line 1529042
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1529043
    move-object v0, v0

    .line 1529044
    const-string v1, "query"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "search_type"

    sget-object v2, LX/9jG;->LIGHTWEIGHT_PLACE_PICKER:LX/9jG;

    invoke-virtual {v2}, LX/9jG;->toLegacyString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "place_picker_session_id"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "results_list_id"

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "results_fetched"

    .line 1529045
    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-virtual {v2}, LX/0mC;->b()LX/162;

    move-result-object p0

    .line 1529046
    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1529047
    invoke-virtual {v2}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/162;->g(Ljava/lang/String;)LX/162;

    goto :goto_0

    .line 1529048
    :cond_0
    move-object v2, p0

    .line 1529049
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    return-object v0
.end method
