.class public final LX/9WX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:LX/9WY;


# direct methods
.method public constructor <init>(LX/9WY;)V
    .locals 0

    .prologue
    .line 1501310
    iput-object p1, p0, LX/9WX;->a:LX/9WY;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 1501302
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1501304
    iget-object v0, p0, LX/9WX;->a:LX/9WY;

    iget-object v0, v0, LX/9WY;->a:Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    .line 1501305
    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v0

    const-class v3, Landroid/text/Annotation;

    invoke-interface {v2, v1, v0, v3}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/Annotation;

    .line 1501306
    array-length v3, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v0, v1

    .line 1501307
    invoke-interface {v2, v4}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    .line 1501308
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1501309
    :cond_0
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1501303
    return-void
.end method
