.class public final LX/9th;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 1562505
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1562506
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1562507
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1562508
    invoke-static {p0, p1}, LX/9th;->b(LX/15w;LX/186;)I

    move-result v1

    .line 1562509
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1562510
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1562511
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1562512
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1562513
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/9th;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1562514
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1562515
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1562516
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1562517
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_3

    .line 1562518
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1562519
    :goto_0
    return v1

    .line 1562520
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1562521
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_2

    .line 1562522
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1562523
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1562524
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 1562525
    const-string v3, "feedback"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1562526
    invoke-static {p0, p1}, LX/5tI;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1562527
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1562528
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1562529
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1562530
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1562531
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1562532
    if-eqz v0, :cond_0

    .line 1562533
    const-string v1, "feedback"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1562534
    invoke-static {p0, v0, p2}, LX/5tI;->a(LX/15i;ILX/0nX;)V

    .line 1562535
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1562536
    return-void
.end method
