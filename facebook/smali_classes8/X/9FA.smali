.class public LX/9FA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Pn;
.implements LX/1Pq;
.implements LX/1Pr;
.implements LX/1Pv;
.implements LX/21l;
.implements LX/1PW;
.implements LX/1Px;


# instance fields
.field public a:Landroid/content/Context;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:LX/9Bi;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Z

.field public final d:Z

.field public final e:LX/9Cd;

.field public f:LX/9Ce;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final g:LX/9E6;

.field public h:LX/1QR;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final i:LX/1QF;

.field private final j:LX/1QD;

.field public k:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

.field public l:Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:LX/9FD;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final n:LX/0tF;

.field public o:LX/9Im;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:LX/9CP;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/9Bi;LX/9Ce;Ljava/lang/Runnable;LX/9Im;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;ZZLX/9Cd;LX/9E6;LX/1QC;LX/1QF;LX/1QD;LX/0tF;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/9Bi;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/9Ce;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Runnable;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/9Im;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1458284
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1458285
    iput-object p1, p0, LX/9FA;->a:Landroid/content/Context;

    .line 1458286
    iput-object p2, p0, LX/9FA;->b:LX/9Bi;

    .line 1458287
    iput-object p3, p0, LX/9FA;->f:LX/9Ce;

    .line 1458288
    iput-boolean p7, p0, LX/9FA;->d:Z

    .line 1458289
    iput-boolean p8, p0, LX/9FA;->c:Z

    .line 1458290
    iput-object p9, p0, LX/9FA;->e:LX/9Cd;

    .line 1458291
    iput-object p10, p0, LX/9FA;->g:LX/9E6;

    .line 1458292
    invoke-static {p6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1458293
    iput-object p6, p0, LX/9FA;->k:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1458294
    invoke-static {p4}, LX/1QC;->a(Ljava/lang/Runnable;)LX/1QR;

    move-result-object v0

    iput-object v0, p0, LX/9FA;->h:LX/1QR;

    .line 1458295
    iput-object p12, p0, LX/9FA;->i:LX/1QF;

    .line 1458296
    iput-object p13, p0, LX/9FA;->j:LX/1QD;

    .line 1458297
    iput-object p5, p0, LX/9FA;->o:LX/9Im;

    .line 1458298
    iput-object p14, p0, LX/9FA;->n:LX/0tF;

    .line 1458299
    new-instance v0, LX/9FD;

    invoke-direct {v0, p1}, LX/9FD;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/9FA;->m:LX/9FD;

    .line 1458300
    return-void
.end method


# virtual methods
.method public final a(LX/1KL;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1KL",
            "<TK;TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 1458283
    iget-object v0, p0, LX/9FA;->i:LX/1QF;

    invoke-virtual {v0, p1}, LX/1QF;->a(LX/1KL;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1KL;LX/0jW;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1KL",
            "<TK;TT;>;",
            "LX/0jW;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 1458282
    iget-object v0, p0, LX/9FA;->i:LX/1QF;

    invoke-virtual {v0, p1, p2}, LX/1QF;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1R6;)V
    .locals 1
    .param p1    # LX/1R6;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1458279
    iget-object v0, p0, LX/9FA;->h:LX/1QR;

    if-eqz v0, :cond_0

    .line 1458280
    iget-object v0, p0, LX/9FA;->h:LX/1QR;

    invoke-virtual {v0, p1}, LX/1QR;->a(LX/1R6;)V

    .line 1458281
    :cond_0
    return-void
.end method

.method public final a(LX/9CP;)V
    .locals 0

    .prologue
    .line 1458276
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1458277
    iput-object p1, p0, LX/9FA;->p:LX/9CP;

    .line 1458278
    return-void
.end method

.method public final a(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V
    .locals 1

    .prologue
    .line 1458270
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1458271
    iput-object p1, p0, LX/9FA;->k:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1458272
    iget-object v0, p0, LX/9FA;->e:LX/9Cd;

    if-eqz v0, :cond_0

    .line 1458273
    iget-object v0, p0, LX/9FA;->e:LX/9Cd;

    .line 1458274
    iput-object p1, v0, LX/9Cd;->A:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1458275
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 8

    .prologue
    .line 1458221
    iget-object v0, p0, LX/9FA;->e:LX/9Cd;

    iget-object v1, p0, LX/9FA;->a:Landroid/content/Context;

    .line 1458222
    new-instance v2, LX/170;

    invoke-direct {v2}, LX/170;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->O()Ljava/lang/String;

    move-result-object v3

    .line 1458223
    iput-object v3, v2, LX/170;->o:Ljava/lang/String;

    .line 1458224
    move-object v2, v2

    .line 1458225
    new-instance v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v4, -0x642179c1

    invoke-direct {v3, v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 1458226
    iput-object v3, v2, LX/170;->ab:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1458227
    move-object v2, v2

    .line 1458228
    invoke-virtual {v2}, LX/170;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v2

    .line 1458229
    new-instance v3, LX/39x;

    invoke-direct {v3}, LX/39x;-><init>()V

    const/4 v5, 0x0

    const/4 p0, 0x0

    .line 1458230
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1458231
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    .line 1458232
    :goto_0
    move-object v4, v4

    .line 1458233
    iput-object v4, v3, LX/39x;->t:Ljava/lang/String;

    .line 1458234
    move-object v3, v3

    .line 1458235
    invoke-static {p1}, LX/9Cd;->a(Lcom/facebook/graphql/model/GraphQLComment;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/16z;->a(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    .line 1458236
    iput-object v4, v3, LX/39x;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1458237
    move-object v3, v3

    .line 1458238
    new-instance v4, LX/4XB;

    invoke-direct {v4}, LX/4XB;-><init>()V

    .line 1458239
    if-eqz p1, :cond_6

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v5

    if-eqz v5, :cond_6

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    :goto_1
    move-object v5, v5

    .line 1458240
    iput-object v5, v4, LX/4XB;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1458241
    move-object v4, v4

    .line 1458242
    invoke-virtual {v4}, LX/4XB;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    .line 1458243
    iput-object v4, v3, LX/39x;->j:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 1458244
    move-object v3, v3

    .line 1458245
    invoke-virtual {v3}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    .line 1458246
    invoke-static {v2}, LX/89G;->a(Lcom/facebook/graphql/model/GraphQLEntity;)LX/89G;

    move-result-object v2

    .line 1458247
    iput-object v3, v2, LX/89G;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1458248
    move-object v2, v2

    .line 1458249
    invoke-virtual {v2}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v2

    .line 1458250
    iget-object v3, v0, LX/9Cd;->A:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1458251
    iget-object v4, v3, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->i:LX/21D;

    move-object v3, v4

    .line 1458252
    const-string v4, "comment_share"

    invoke-static {v3, v4, v2}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    sget-object v3, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {v2, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v3

    .line 1458253
    iget-object v2, v0, LX/9Cd;->y:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Zb;

    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "comment_share_action_click"

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    invoke-interface {v2, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1458254
    iget-object v2, v0, LX/9Cd;->w:LX/1Kf;

    const/4 v4, 0x0

    invoke-interface {v2, v4, v3, v1}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Landroid/content/Context;)V

    .line 1458255
    return-void

    .line 1458256
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->p()LX/0Px;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->p()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_5

    .line 1458257
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->p()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v4}, LX/1VO;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1458258
    const v4, 0x7f08126c

    .line 1458259
    :goto_2
    invoke-static {p1}, LX/9Cd;->a(Lcom/facebook/graphql/model/GraphQLComment;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_4

    iget-object v5, v0, LX/9Cd;->i:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p1}, LX/9Cd;->a(Lcom/facebook/graphql/model/GraphQLComment;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, p0

    invoke-virtual {v5, v4, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .line 1458260
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->p()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v4}, LX/1VO;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1458261
    const v4, 0x7f08126d

    goto :goto_2

    .line 1458262
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->p()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1458263
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->STICKER:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v4, v6}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)Z

    move-result v6

    move v4, v6

    .line 1458264
    if-eqz v4, :cond_3

    .line 1458265
    const v4, 0x7f08126e

    goto :goto_2

    .line 1458266
    :cond_3
    iget-object v4, v0, LX/9Cd;->x:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/03V;

    sget-object v6, LX/9Cd;->a:Ljava/lang/Class;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Unrecognized comment attachment"

    invoke-virtual {v4, v6, v7}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v5

    .line 1458267
    goto/16 :goto_0

    :cond_4
    move-object v4, v5

    .line 1458268
    goto/16 :goto_0

    :cond_5
    move-object v4, v5

    .line 1458269
    goto/16 :goto_0

    :cond_6
    const/4 v5, 0x0

    goto/16 :goto_1
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 4

    .prologue
    .line 1458190
    iget-object v0, p0, LX/9FA;->p:LX/9CP;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/9FA;->n:LX/0tF;

    invoke-virtual {v0}, LX/0tF;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1458191
    iget-object v0, p0, LX/9FA;->p:LX/9CP;

    .line 1458192
    iget-object v1, v0, LX/9CP;->n:LX/21o;

    if-nez v1, :cond_2

    .line 1458193
    :cond_0
    :goto_0
    return-void

    .line 1458194
    :cond_1
    iget-object v0, p0, LX/9FA;->b:LX/9Bi;

    if-eqz v0, :cond_0

    .line 1458195
    iget-object v0, p0, LX/9FA;->b:LX/9Bi;

    iget-object v1, p0, LX/9FA;->k:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-interface {v0, p2, p3, v1}, LX/9Bi;->a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V

    goto :goto_0

    .line 1458196
    :cond_2
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    .line 1458197
    const/4 v3, 0x1

    .line 1458198
    iget-object v1, v0, LX/9CP;->n:LX/21o;

    if-nez v1, :cond_5

    .line 1458199
    iget-object v1, v0, LX/9CP;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03V;

    sget-object p0, LX/9CP;->a:Ljava/lang/String;

    const-string p3, "sticky comment composer cannot be null when requesting to show reply composer"

    invoke-virtual {v1, p0, p3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v3

    .line 1458200
    :goto_1
    move v1, v1

    .line 1458201
    if-nez v1, :cond_0

    .line 1458202
    invoke-static {v0}, LX/9CP;->s(LX/9CP;)Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x1

    :goto_2
    iput-boolean v1, v0, LX/9CP;->I:Z

    .line 1458203
    invoke-static {v0}, LX/9CP;->O(LX/9CP;)Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, v0, LX/9CP;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v1, :cond_8

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v1

    iget-object v3, v0, LX/9CP;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    const/4 v1, 0x1

    :goto_3
    move v1, v1

    .line 1458204
    if-eqz v1, :cond_4

    .line 1458205
    iget-object v1, v0, LX/9CP;->n:LX/21o;

    invoke-static {v0}, LX/9CP;->s(LX/9CP;)Z

    move-result v2

    invoke-interface {v1, v2}, LX/21o;->a(Z)V

    .line 1458206
    invoke-static {v0, p1, p2}, LX/9CP;->b(LX/9CP;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 1458207
    invoke-static {v0}, LX/9CP;->q(LX/9CP;)V

    .line 1458208
    invoke-static {v0}, LX/9CP;->x(LX/9CP;)V

    .line 1458209
    invoke-virtual {v0}, LX/9CP;->i()V

    goto :goto_0

    .line 1458210
    :cond_3
    const/4 v1, 0x0

    goto :goto_2

    .line 1458211
    :cond_4
    invoke-static {v0}, LX/9CP;->u(LX/9CP;)Z

    .line 1458212
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    iput-object v1, v0, LX/9CP;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1458213
    invoke-static {v0, p1, p2}, LX/9CP;->b(LX/9CP;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 1458214
    invoke-static {v0}, LX/9CP;->x(LX/9CP;)V

    .line 1458215
    invoke-static {v0}, LX/9CP;->v(LX/9CP;)V

    .line 1458216
    invoke-virtual {v0}, LX/9CP;->i()V

    goto :goto_0

    .line 1458217
    :cond_5
    if-eqz v2, :cond_6

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_7

    .line 1458218
    :cond_6
    iget-object v1, v0, LX/9CP;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03V;

    sget-object p0, LX/9CP;->a:Ljava/lang/String;

    const-string p3, "top level comment has null feedback or null feedback id"

    invoke-virtual {v1, p0, p3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v3

    .line 1458219
    goto :goto_1

    .line 1458220
    :cond_7
    const/4 v1, 0x0

    goto :goto_1

    :cond_8
    const/4 v1, 0x0

    goto :goto_3
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 1

    .prologue
    .line 1458188
    iget-object v0, p0, LX/9FA;->e:LX/9Cd;

    invoke-virtual {v0, p1, p2}, LX/9Cd;->a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1458189
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;LX/1zt;)V
    .locals 4

    .prologue
    .line 1458169
    iget-object v0, p0, LX/9FA;->e:LX/9Cd;

    .line 1458170
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-static {v2}, LX/1zt;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2, p3}, LX/20h;->a(Lcom/facebook/graphql/model/GraphQLFeedback;ILX/1zt;)Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel;

    move-result-object v1

    .line 1458171
    iget-object v2, v0, LX/9Cd;->u:LX/20j;

    iget-object v3, v0, LX/9Cd;->t:LX/20i;

    invoke-virtual {v3}, LX/20i;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object p0

    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel;->a()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;

    move-result-object v1

    invoke-virtual {v2, v3, p0, v1}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLActor;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 1458172
    new-instance v2, LX/4Vu;

    invoke-direct {v2}, LX/4Vu;-><init>()V

    invoke-static {p1}, LX/4Vu;->a(Lcom/facebook/graphql/model/GraphQLComment;)LX/4Vu;

    move-result-object v2

    .line 1458173
    iput-object v1, v2, LX/4Vu;->p:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1458174
    move-object v1, v2

    .line 1458175
    invoke-virtual {v1}, LX/4Vu;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v1

    .line 1458176
    iget-object v2, v0, LX/9Cd;->b:LX/1K9;

    new-instance v3, LX/8q4;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v3, v1, p0}, LX/8q4;-><init>(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/1K9;->a(LX/1KJ;)V

    .line 1458177
    iget-object v1, v0, LX/9Cd;->A:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/9Cd;->A:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1458178
    iget-object v2, v1, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->a:LX/162;

    move-object v1, v2

    .line 1458179
    if-eqz v1, :cond_0

    .line 1458180
    :goto_0
    iget-object v1, v0, LX/9Cd;->v:LX/20h;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    iget-object v3, v0, LX/9Cd;->A:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    new-instance p0, LX/9CU;

    invoke-direct {p0, v0, p1, p2}, LX/9CU;-><init>(LX/9Cd;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    invoke-virtual {v1, v2, p3, v3, p0}, LX/20h;->a(Lcom/facebook/graphql/model/GraphQLFeedback;LX/1zt;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;LX/0Ve;)V

    .line 1458181
    return-void

    .line 1458182
    :cond_0
    iget-object v1, v0, LX/9Cd;->A:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-static {v1}, LX/21A;->a(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)LX/21A;

    move-result-object v1

    .line 1458183
    iget-object v2, v1, LX/21A;->a:LX/162;

    move-object v2, v2

    .line 1458184
    if-nez v2, :cond_1

    .line 1458185
    invoke-static {}, LX/1fz;->a()LX/162;

    move-result-object v2

    .line 1458186
    iput-object v2, v1, LX/21A;->a:LX/162;

    .line 1458187
    :cond_1
    invoke-virtual {v1}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v1

    iput-object v1, v0, LX/9Cd;->A:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1458166
    iget-object v0, p0, LX/9FA;->b:LX/9Bi;

    if-eqz v0, :cond_0

    .line 1458167
    iget-object v0, p0, LX/9FA;->b:LX/9Bi;

    iget-object v1, p0, LX/9FA;->a:Landroid/content/Context;

    invoke-interface {v0, p1, v1, p2, p3}, LX/9Bi;->a(Lcom/facebook/graphql/model/GraphQLComment;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1458168
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;LX/5Gs;ILX/0TF;)V
    .locals 7
    .param p4    # LX/0TF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            "LX/5Gs;",
            "I",
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1458156
    iget-object v0, p0, LX/9FA;->g:LX/9E6;

    .line 1458157
    iget-object v1, v0, LX/9E6;->e:LX/1nK;

    const/4 p0, 0x1

    .line 1458158
    iget-object v2, v1, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    new-instance v3, LX/0Yj;

    const v4, 0x390019

    const-string v5, "UfiLoadMoreComments"

    invoke-direct {v3, v4, v5}, LX/0Yj;-><init>(ILjava/lang/String;)V

    new-array v4, p0, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "story_feedback_flyout"

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, LX/0Yj;->a([Ljava/lang/String;)LX/0Yj;

    move-result-object v3

    .line 1458159
    iput-boolean p0, v3, LX/0Yj;->n:Z

    .line 1458160
    move-object v3, v3

    .line 1458161
    invoke-interface {v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->c(LX/0Yj;)V

    .line 1458162
    sget-object v1, LX/5Gs;->LOAD_AFTER:LX/5Gs;

    if-ne p2, v1, :cond_0

    const-string v1, "fetch_after_comments_"

    .line 1458163
    :goto_0
    iget-object v2, v0, LX/9E6;->d:LX/1Ck;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v3, LX/9E3;

    invoke-direct {v3, v0, p1, p2, p3}, LX/9E3;-><init>(LX/9E6;Lcom/facebook/graphql/model/GraphQLFeedback;LX/5Gs;I)V

    new-instance v4, LX/9E4;

    invoke-direct {v4, v0, p1, p2, p4}, LX/9E4;-><init>(LX/9E6;Lcom/facebook/graphql/model/GraphQLFeedback;LX/5Gs;LX/0TF;)V

    invoke-virtual {v2, v1, v3, v4}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1458164
    return-void

    .line 1458165
    :cond_0
    const-string v1, "fetch_before_comments_"

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1458155
    return-void
.end method

.method public final varargs a([Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 1

    .prologue
    .line 1458301
    iget-object v0, p0, LX/9FA;->h:LX/1QR;

    if-eqz v0, :cond_0

    .line 1458302
    iget-object v0, p0, LX/9FA;->h:LX/1QR;

    invoke-virtual {v0, p1}, LX/1QR;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1458303
    :cond_0
    return-void
.end method

.method public final varargs a([Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1458105
    iget-object v0, p0, LX/9FA;->h:LX/1QR;

    if-eqz v0, :cond_0

    .line 1458106
    iget-object v0, p0, LX/9FA;->h:LX/1QR;

    invoke-virtual {v0, p1}, LX/1QR;->a([Ljava/lang/Object;)V

    .line 1458107
    :cond_0
    return-void
.end method

.method public final a(LX/1KL;Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1KL",
            "<TK;TT;>;TT;)Z"
        }
    .end annotation

    .prologue
    .line 1458111
    iget-object v0, p0, LX/9FA;->i:LX/1QF;

    invoke-virtual {v0, p1, p2}, LX/1QF;->a(LX/1KL;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(LX/1R6;)V
    .locals 1

    .prologue
    .line 1458112
    iget-object v0, p0, LX/9FA;->h:LX/1QR;

    if-eqz v0, :cond_0

    .line 1458113
    iget-object v0, p0, LX/9FA;->h:LX/1QR;

    invoke-virtual {v0, p1}, LX/1QR;->b(LX/1R6;)V

    .line 1458114
    :cond_0
    return-void
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 1

    .prologue
    .line 1458108
    iget-object v0, p0, LX/9FA;->b:LX/9Bi;

    if-eqz v0, :cond_0

    .line 1458109
    iget-object v0, p0, LX/9FA;->b:LX/9Bi;

    invoke-interface {v0, p1}, LX/9Bi;->a(Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 1458110
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1458115
    iget-object v0, p0, LX/9FA;->i:LX/1QF;

    invoke-virtual {v0, p1}, LX/1QF;->b(Ljava/lang/String;)V

    .line 1458116
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 1458117
    iget-object v0, p0, LX/9FA;->j:LX/1QD;

    invoke-virtual {v0, p1}, LX/1QD;->b(Z)V

    .line 1458118
    return-void
.end method

.method public final c(Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 1

    .prologue
    .line 1458119
    iget-object v0, p0, LX/9FA;->b:LX/9Bi;

    if-eqz v0, :cond_0

    .line 1458120
    iget-object v0, p0, LX/9FA;->b:LX/9Bi;

    invoke-interface {v0, p1}, LX/9Bi;->b(Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 1458121
    :cond_0
    return-void
.end method

.method public final c(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 10

    .prologue
    .line 1458122
    iget-object v0, p0, LX/9FA;->e:LX/9Cd;

    .line 1458123
    iget-object v1, v0, LX/9Cd;->c:LX/3iK;

    iget-object v2, v0, LX/9Cd;->A:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1458124
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->H()Ljava/lang/String;

    move-result-object v4

    .line 1458125
    iget-object v3, v1, LX/3iK;->i:LX/3iM;

    .line 1458126
    iget-object v5, v3, LX/3iM;->a:Ljava/util/Map;

    invoke-interface {v5, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;

    move-object v5, v5

    .line 1458127
    if-nez v5, :cond_0

    .line 1458128
    :goto_0
    return-void

    .line 1458129
    :cond_0
    iget-object v3, v1, LX/3iK;->m:LX/3iQ;

    const/4 v8, 0x1

    invoke-static {v1, v4, v5, p2}, LX/3iK;->a(LX/3iK;Ljava/lang/String;Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;Lcom/facebook/graphql/model/GraphQLFeedback;)LX/1L9;

    move-result-object v9

    move-object v6, p2

    move-object v7, v2

    invoke-virtual/range {v3 .. v9}, LX/3iQ;->a(Ljava/lang/String;Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;ZLX/1L9;)V

    goto :goto_0
.end method

.method public final d(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 10

    .prologue
    .line 1458130
    iget-object v0, p0, LX/9FA;->e:LX/9Cd;

    iget-object v1, p0, LX/9FA;->a:Landroid/content/Context;

    const/4 v5, 0x1

    .line 1458131
    iget-object v2, v0, LX/9Cd;->o:LX/3iM;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->H()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/3iM;->c(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->OFFLINE:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-ne v2, v3, :cond_0

    .line 1458132
    iget-object v2, v0, LX/9Cd;->l:LX/0TD;

    new-instance v3, LX/9Ca;

    invoke-direct {v3, v0, p1}, LX/9Ca;-><init>(LX/9Cd;Lcom/facebook/graphql/model/GraphQLComment;)V

    invoke-interface {v2, v3}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 1458133
    new-instance v3, LX/9Cb;

    invoke-direct {v3, v0, p1, p2}, LX/9Cb;-><init>(LX/9Cd;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    iget-object v4, v0, LX/9Cd;->m:Ljava/util/concurrent/Executor;

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1458134
    :goto_0
    return-void

    .line 1458135
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    .line 1458136
    iget-object v2, v0, LX/9Cd;->b:LX/1K9;

    new-instance v3, LX/8py;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, p1, v4}, LX/8py;-><init>(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/1K9;->a(LX/1KJ;)V

    goto :goto_0

    .line 1458137
    :cond_1
    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    .line 1458138
    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    const v4, 0x7f0105ae

    invoke-virtual {v3, v4, v2, v5}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 1458139
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget v2, v2, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1458140
    new-instance v4, Landroid/app/ProgressDialog;

    invoke-direct {v4, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 1458141
    invoke-virtual {v4, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 1458142
    invoke-virtual {v4}, Landroid/app/ProgressDialog;->show()V

    .line 1458143
    iget-object v2, v0, LX/9Cd;->f:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v6

    .line 1458144
    iget-object v2, v0, LX/9Cd;->e:LX/0hx;

    invoke-virtual {v2, v5}, LX/0hx;->a(Z)V

    .line 1458145
    iget-object v8, v0, LX/9Cd;->p:LX/3iQ;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v9

    new-instance v2, LX/9CZ;

    move-object v3, v0

    move-object v5, p2

    invoke-direct/range {v2 .. v7}, LX/9CZ;-><init>(LX/9Cd;Landroid/app/ProgressDialog;Lcom/facebook/graphql/model/GraphQLFeedback;J)V

    invoke-virtual {v8, p1, v9, v2}, LX/3iQ;->a(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;LX/1L9;)V

    goto :goto_0
.end method

.method public final g()LX/9Ce;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1458146
    iget-object v0, p0, LX/9FA;->f:LX/9Ce;

    return-object v0
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1458147
    iget-object v0, p0, LX/9FA;->a:Landroid/content/Context;

    return-object v0
.end method

.method public final iN_()V
    .locals 1

    .prologue
    .line 1458148
    iget-object v0, p0, LX/9FA;->h:LX/1QR;

    if-eqz v0, :cond_0

    .line 1458149
    iget-object v0, p0, LX/9FA;->h:LX/1QR;

    invoke-virtual {v0}, LX/1QR;->iN_()V

    .line 1458150
    :cond_0
    return-void
.end method

.method public final iO_()Z
    .locals 1

    .prologue
    .line 1458151
    iget-object v0, p0, LX/9FA;->j:LX/1QD;

    invoke-virtual {v0}, LX/1QD;->iO_()Z

    move-result v0

    return v0
.end method

.method public final m_(Z)V
    .locals 1

    .prologue
    .line 1458152
    iget-object v0, p0, LX/9FA;->h:LX/1QR;

    if-eqz v0, :cond_0

    .line 1458153
    iget-object v0, p0, LX/9FA;->h:LX/1QR;

    invoke-virtual {v0, p1}, LX/1QR;->m_(Z)V

    .line 1458154
    :cond_0
    return-void
.end method
