.class public final LX/9pQ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 42

    .prologue
    .line 1544378
    const/16 v38, 0x0

    .line 1544379
    const/16 v37, 0x0

    .line 1544380
    const/16 v36, 0x0

    .line 1544381
    const/16 v35, 0x0

    .line 1544382
    const/16 v34, 0x0

    .line 1544383
    const/16 v33, 0x0

    .line 1544384
    const/16 v32, 0x0

    .line 1544385
    const/16 v31, 0x0

    .line 1544386
    const/16 v30, 0x0

    .line 1544387
    const/16 v29, 0x0

    .line 1544388
    const/16 v28, 0x0

    .line 1544389
    const/16 v27, 0x0

    .line 1544390
    const/16 v26, 0x0

    .line 1544391
    const/16 v25, 0x0

    .line 1544392
    const/16 v24, 0x0

    .line 1544393
    const/16 v23, 0x0

    .line 1544394
    const/16 v22, 0x0

    .line 1544395
    const/16 v21, 0x0

    .line 1544396
    const/16 v20, 0x0

    .line 1544397
    const/16 v19, 0x0

    .line 1544398
    const/16 v18, 0x0

    .line 1544399
    const/16 v17, 0x0

    .line 1544400
    const/16 v16, 0x0

    .line 1544401
    const/4 v15, 0x0

    .line 1544402
    const/4 v14, 0x0

    .line 1544403
    const/4 v13, 0x0

    .line 1544404
    const/4 v12, 0x0

    .line 1544405
    const/4 v11, 0x0

    .line 1544406
    const/4 v10, 0x0

    .line 1544407
    const/4 v9, 0x0

    .line 1544408
    const/4 v8, 0x0

    .line 1544409
    const/4 v7, 0x0

    .line 1544410
    const/4 v6, 0x0

    .line 1544411
    const/4 v5, 0x0

    .line 1544412
    const/4 v4, 0x0

    .line 1544413
    const/4 v3, 0x0

    .line 1544414
    const/4 v2, 0x0

    .line 1544415
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v39

    sget-object v40, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v39

    move-object/from16 v1, v40

    if-eq v0, v1, :cond_1

    .line 1544416
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1544417
    const/4 v2, 0x0

    .line 1544418
    :goto_0
    return v2

    .line 1544419
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1544420
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v39

    sget-object v40, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v39

    move-object/from16 v1, v40

    if-eq v0, v1, :cond_1a

    .line 1544421
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v39

    .line 1544422
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1544423
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v40

    sget-object v41, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v40

    move-object/from16 v1, v41

    if-eq v0, v1, :cond_1

    if-eqz v39, :cond_1

    .line 1544424
    const-string v40, "can_page_viewer_invite_post_likers"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_2

    .line 1544425
    const/4 v13, 0x1

    .line 1544426
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v38

    goto :goto_1

    .line 1544427
    :cond_2
    const-string v40, "can_see_voice_switcher"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_3

    .line 1544428
    const/4 v12, 0x1

    .line 1544429
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v37

    goto :goto_1

    .line 1544430
    :cond_3
    const-string v40, "can_viewer_comment"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_4

    .line 1544431
    const/4 v11, 0x1

    .line 1544432
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v36

    goto :goto_1

    .line 1544433
    :cond_4
    const-string v40, "can_viewer_comment_with_photo"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_5

    .line 1544434
    const/4 v10, 0x1

    .line 1544435
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v35

    goto :goto_1

    .line 1544436
    :cond_5
    const-string v40, "can_viewer_comment_with_sticker"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_6

    .line 1544437
    const/4 v9, 0x1

    .line 1544438
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v34

    goto :goto_1

    .line 1544439
    :cond_6
    const-string v40, "can_viewer_comment_with_video"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_7

    .line 1544440
    const/4 v8, 0x1

    .line 1544441
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v33

    goto :goto_1

    .line 1544442
    :cond_7
    const-string v40, "can_viewer_like"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_8

    .line 1544443
    const/4 v7, 0x1

    .line 1544444
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v32

    goto/16 :goto_1

    .line 1544445
    :cond_8
    const-string v40, "can_viewer_react"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_9

    .line 1544446
    const/4 v6, 0x1

    .line 1544447
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v31

    goto/16 :goto_1

    .line 1544448
    :cond_9
    const-string v40, "can_viewer_subscribe"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_a

    .line 1544449
    const/4 v5, 0x1

    .line 1544450
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v30

    goto/16 :goto_1

    .line 1544451
    :cond_a
    const-string v40, "comments_mirroring_domain"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_b

    .line 1544452
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v29

    goto/16 :goto_1

    .line 1544453
    :cond_b
    const-string v40, "does_viewer_like"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_c

    .line 1544454
    const/4 v4, 0x1

    .line 1544455
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v28

    goto/16 :goto_1

    .line 1544456
    :cond_c
    const-string v40, "id"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_d

    .line 1544457
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v27

    goto/16 :goto_1

    .line 1544458
    :cond_d
    const-string v40, "important_reactors"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_e

    .line 1544459
    invoke-static/range {p0 .. p1}, LX/5DS;->a(LX/15w;LX/186;)I

    move-result v26

    goto/16 :goto_1

    .line 1544460
    :cond_e
    const-string v40, "is_viewer_subscribed"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_f

    .line 1544461
    const/4 v3, 0x1

    .line 1544462
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v25

    goto/16 :goto_1

    .line 1544463
    :cond_f
    const-string v40, "legacy_api_post_id"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_10

    .line 1544464
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v24

    goto/16 :goto_1

    .line 1544465
    :cond_10
    const-string v40, "likers"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_11

    .line 1544466
    invoke-static/range {p0 .. p1}, LX/9pN;->a(LX/15w;LX/186;)I

    move-result v23

    goto/16 :goto_1

    .line 1544467
    :cond_11
    const-string v40, "reactors"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_12

    .line 1544468
    invoke-static/range {p0 .. p1}, LX/5DL;->a(LX/15w;LX/186;)I

    move-result v22

    goto/16 :goto_1

    .line 1544469
    :cond_12
    const-string v40, "remixable_photo_uri"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_13

    .line 1544470
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v21

    goto/16 :goto_1

    .line 1544471
    :cond_13
    const-string v40, "reshares"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_14

    .line 1544472
    invoke-static/range {p0 .. p1}, LX/9pO;->a(LX/15w;LX/186;)I

    move-result v20

    goto/16 :goto_1

    .line 1544473
    :cond_14
    const-string v40, "supported_reactions"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_15

    .line 1544474
    invoke-static/range {p0 .. p1}, LX/5DM;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 1544475
    :cond_15
    const-string v40, "top_level_comments"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_16

    .line 1544476
    invoke-static/range {p0 .. p1}, LX/9pP;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 1544477
    :cond_16
    const-string v40, "top_reactions"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_17

    .line 1544478
    invoke-static/range {p0 .. p1}, LX/5DK;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 1544479
    :cond_17
    const-string v40, "viewer_acts_as_page"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_18

    .line 1544480
    invoke-static/range {p0 .. p1}, LX/5AX;->a(LX/15w;LX/186;)I

    move-result v16

    goto/16 :goto_1

    .line 1544481
    :cond_18
    const-string v40, "viewer_acts_as_person"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_19

    .line 1544482
    invoke-static/range {p0 .. p1}, LX/5DT;->a(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 1544483
    :cond_19
    const-string v40, "viewer_feedback_reaction_key"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v39

    if-eqz v39, :cond_0

    .line 1544484
    const/4 v2, 0x1

    .line 1544485
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v14

    goto/16 :goto_1

    .line 1544486
    :cond_1a
    const/16 v39, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1544487
    if-eqz v13, :cond_1b

    .line 1544488
    const/4 v13, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v13, v1}, LX/186;->a(IZ)V

    .line 1544489
    :cond_1b
    if-eqz v12, :cond_1c

    .line 1544490
    const/4 v12, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v12, v1}, LX/186;->a(IZ)V

    .line 1544491
    :cond_1c
    if-eqz v11, :cond_1d

    .line 1544492
    const/4 v11, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v11, v1}, LX/186;->a(IZ)V

    .line 1544493
    :cond_1d
    if-eqz v10, :cond_1e

    .line 1544494
    const/4 v10, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v10, v1}, LX/186;->a(IZ)V

    .line 1544495
    :cond_1e
    if-eqz v9, :cond_1f

    .line 1544496
    const/4 v9, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v9, v1}, LX/186;->a(IZ)V

    .line 1544497
    :cond_1f
    if-eqz v8, :cond_20

    .line 1544498
    const/4 v8, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 1544499
    :cond_20
    if-eqz v7, :cond_21

    .line 1544500
    const/4 v7, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 1544501
    :cond_21
    if-eqz v6, :cond_22

    .line 1544502
    const/4 v6, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 1544503
    :cond_22
    if-eqz v5, :cond_23

    .line 1544504
    const/16 v5, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 1544505
    :cond_23
    const/16 v5, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 1544506
    if-eqz v4, :cond_24

    .line 1544507
    const/16 v4, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 1544508
    :cond_24
    const/16 v4, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1544509
    const/16 v4, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1544510
    if-eqz v3, :cond_25

    .line 1544511
    const/16 v3, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v3, v1}, LX/186;->a(IZ)V

    .line 1544512
    :cond_25
    const/16 v3, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1544513
    const/16 v3, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1544514
    const/16 v3, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1544515
    const/16 v3, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1544516
    const/16 v3, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1544517
    const/16 v3, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1544518
    const/16 v3, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1544519
    const/16 v3, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1544520
    const/16 v3, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1544521
    const/16 v3, 0x17

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->b(II)V

    .line 1544522
    if-eqz v2, :cond_26

    .line 1544523
    const/16 v2, 0x18

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14, v3}, LX/186;->a(III)V

    .line 1544524
    :cond_26
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1544525
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1544526
    invoke-virtual {p0, p1, v2}, LX/15i;->b(II)Z

    move-result v0

    .line 1544527
    if-eqz v0, :cond_0

    .line 1544528
    const-string v1, "can_page_viewer_invite_post_likers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1544529
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1544530
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1544531
    if-eqz v0, :cond_1

    .line 1544532
    const-string v1, "can_see_voice_switcher"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1544533
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1544534
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1544535
    if-eqz v0, :cond_2

    .line 1544536
    const-string v1, "can_viewer_comment"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1544537
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1544538
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1544539
    if-eqz v0, :cond_3

    .line 1544540
    const-string v1, "can_viewer_comment_with_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1544541
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1544542
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1544543
    if-eqz v0, :cond_4

    .line 1544544
    const-string v1, "can_viewer_comment_with_sticker"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1544545
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1544546
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1544547
    if-eqz v0, :cond_5

    .line 1544548
    const-string v1, "can_viewer_comment_with_video"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1544549
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1544550
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1544551
    if-eqz v0, :cond_6

    .line 1544552
    const-string v1, "can_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1544553
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1544554
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1544555
    if-eqz v0, :cond_7

    .line 1544556
    const-string v1, "can_viewer_react"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1544557
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1544558
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1544559
    if-eqz v0, :cond_8

    .line 1544560
    const-string v1, "can_viewer_subscribe"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1544561
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1544562
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1544563
    if-eqz v0, :cond_9

    .line 1544564
    const-string v1, "comments_mirroring_domain"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1544565
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1544566
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1544567
    if-eqz v0, :cond_a

    .line 1544568
    const-string v1, "does_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1544569
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1544570
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1544571
    if-eqz v0, :cond_b

    .line 1544572
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1544573
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1544574
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1544575
    if-eqz v0, :cond_c

    .line 1544576
    const-string v1, "important_reactors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1544577
    invoke-static {p0, v0, p2, p3}, LX/5DS;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1544578
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1544579
    if-eqz v0, :cond_d

    .line 1544580
    const-string v1, "is_viewer_subscribed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1544581
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1544582
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1544583
    if-eqz v0, :cond_e

    .line 1544584
    const-string v1, "legacy_api_post_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1544585
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1544586
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1544587
    if-eqz v0, :cond_f

    .line 1544588
    const-string v1, "likers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1544589
    invoke-static {p0, v0, p2}, LX/9pN;->a(LX/15i;ILX/0nX;)V

    .line 1544590
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1544591
    if-eqz v0, :cond_10

    .line 1544592
    const-string v1, "reactors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1544593
    invoke-static {p0, v0, p2}, LX/5DL;->a(LX/15i;ILX/0nX;)V

    .line 1544594
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1544595
    if-eqz v0, :cond_11

    .line 1544596
    const-string v1, "remixable_photo_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1544597
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1544598
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1544599
    if-eqz v0, :cond_12

    .line 1544600
    const-string v1, "reshares"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1544601
    invoke-static {p0, v0, p2}, LX/9pO;->a(LX/15i;ILX/0nX;)V

    .line 1544602
    :cond_12
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1544603
    if-eqz v0, :cond_13

    .line 1544604
    const-string v1, "supported_reactions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1544605
    invoke-static {p0, v0, p2, p3}, LX/5DM;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1544606
    :cond_13
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1544607
    if-eqz v0, :cond_14

    .line 1544608
    const-string v1, "top_level_comments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1544609
    invoke-static {p0, v0, p2}, LX/9pP;->a(LX/15i;ILX/0nX;)V

    .line 1544610
    :cond_14
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1544611
    if-eqz v0, :cond_15

    .line 1544612
    const-string v1, "top_reactions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1544613
    invoke-static {p0, v0, p2, p3}, LX/5DK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1544614
    :cond_15
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1544615
    if-eqz v0, :cond_16

    .line 1544616
    const-string v1, "viewer_acts_as_page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1544617
    invoke-static {p0, v0, p2, p3}, LX/5AX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1544618
    :cond_16
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1544619
    if-eqz v0, :cond_17

    .line 1544620
    const-string v1, "viewer_acts_as_person"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1544621
    invoke-static {p0, v0, p2}, LX/5DT;->a(LX/15i;ILX/0nX;)V

    .line 1544622
    :cond_17
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1544623
    if-eqz v0, :cond_18

    .line 1544624
    const-string v1, "viewer_feedback_reaction_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1544625
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1544626
    :cond_18
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1544627
    return-void
.end method
