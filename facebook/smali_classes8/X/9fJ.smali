.class public LX/9fJ;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/9fI;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1521738
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1521739
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewStub;Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;Lcom/facebook/photos/editgallery/EditableOverlayContainerView;LX/9f1;LX/0am;Landroid/net/Uri;Landroid/view/View;)LX/9fI;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewStub;",
            "Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;",
            "Lcom/facebook/photos/editgallery/EditableOverlayContainerView;",
            "Lcom/facebook/photos/editgallery/EditGallerySwipeableLayoutController;",
            "LX/0am",
            "<",
            "LX/9c7;",
            ">;",
            "Landroid/net/Uri;",
            "Landroid/view/View;",
            ")",
            "LX/9fI;"
        }
    .end annotation

    .prologue
    .line 1521740
    new-instance v0, LX/9fI;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    const-class v1, LX/9fO;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/9fO;

    const/16 v1, 0x2e40

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v11

    check-cast v11, LX/0ad;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v11}, LX/9fI;-><init>(Landroid/view/ViewStub;Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;Lcom/facebook/photos/editgallery/EditableOverlayContainerView;LX/9f1;LX/0am;Landroid/net/Uri;Landroid/view/View;Landroid/content/Context;LX/9fO;LX/0Or;LX/0ad;)V

    .line 1521741
    return-object v0
.end method
