.class public final LX/9qi;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:I

.field public J:Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$ProductModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public M:D

.field public N:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PageMediaWithAttributionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public O:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public P:Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Q:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public R:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public S:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public T:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public U:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public V:Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel$StoryAdminPageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public W:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public X:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Y:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryTopicAttachmentFragmentModel$TopicModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Z:I

.field public a:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aa:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ab:I

.field public ac:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ad:I

.field public ae:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public af:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionAttachmentActionFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionAttributionFooterAttachmentFieldsModel$AttributionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:I

.field public m:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ExternalImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel$InviteeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Z

.field public t:Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPagePromotionAttachmentFieldsModel$ItemModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel$MessageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel$PageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1548989
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;
    .locals 55

    .prologue
    .line 1548990
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1548991
    move-object/from16 v0, p0

    iget-object v3, v0, LX/9qi;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1548992
    move-object/from16 v0, p0

    iget-object v4, v0, LX/9qi;->b:LX/0Px;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v4

    .line 1548993
    move-object/from16 v0, p0

    iget-object v5, v0, LX/9qi;->c:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-static {v2, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1548994
    move-object/from16 v0, p0

    iget-object v6, v0, LX/9qi;->d:Ljava/lang/String;

    invoke-virtual {v2, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1548995
    move-object/from16 v0, p0

    iget-object v7, v0, LX/9qi;->e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-static {v2, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1548996
    move-object/from16 v0, p0

    iget-object v8, v0, LX/9qi;->f:Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionAttributionFooterAttachmentFieldsModel$AttributionModel;

    invoke-static {v2, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1548997
    move-object/from16 v0, p0

    iget-object v9, v0, LX/9qi;->g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-static {v2, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1548998
    move-object/from16 v0, p0

    iget-object v10, v0, LX/9qi;->h:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-static {v2, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1548999
    move-object/from16 v0, p0

    iget-object v11, v0, LX/9qi;->i:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-static {v2, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1549000
    move-object/from16 v0, p0

    iget-object v12, v0, LX/9qi;->j:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-static {v2, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1549001
    move-object/from16 v0, p0

    iget-object v13, v0, LX/9qi;->k:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v2, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 1549002
    move-object/from16 v0, p0

    iget-object v14, v0, LX/9qi;->m:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;

    invoke-static {v2, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 1549003
    move-object/from16 v0, p0

    iget-object v15, v0, LX/9qi;->n:Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ExternalImageModel;

    invoke-static {v2, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 1549004
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9qi;->o:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 1549005
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9qi;->p:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 1549006
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9qi;->q:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 1549007
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9qi;->r:Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel$InviteeModel;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 1549008
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9qi;->t:Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPagePromotionAttachmentFieldsModel$ItemModel;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 1549009
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9qi;->u:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 1549010
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9qi;->v:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 1549011
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9qi;->w:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v23

    .line 1549012
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9qi;->x:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v24

    .line 1549013
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9qi;->y:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel$MessageModel;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v25

    .line 1549014
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9qi;->z:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel$PageModel;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v26

    .line 1549015
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9qi;->A:Lcom/facebook/graphql/model/GraphQLStory;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v27

    .line 1549016
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9qi;->B:Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v28

    .line 1549017
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9qi;->C:Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v29

    .line 1549018
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9qi;->D:Ljava/lang/String;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v30

    .line 1549019
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9qi;->E:Ljava/lang/String;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v31

    .line 1549020
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9qi;->F:Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v32

    .line 1549021
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9qi;->G:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v33

    .line 1549022
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9qi;->H:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v34

    .line 1549023
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9qi;->J:Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageCommerceAttachmentFieldsModel$ProductModel;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v35

    .line 1549024
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9qi;->K:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionProfileFieldsModel;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v36

    .line 1549025
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9qi;->L:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v37

    .line 1549026
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9qi;->N:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PageMediaWithAttributionModel;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v38

    .line 1549027
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9qi;->O:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v39

    .line 1549028
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9qi;->P:Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionCriticReviewAttachmentFieldsModel$ReviewerModel;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v40

    .line 1549029
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9qi;->Q:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v41

    .line 1549030
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9qi;->R:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-object/from16 v42, v0

    move-object/from16 v0, v42

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v42

    .line 1549031
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9qi;->S:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v43

    .line 1549032
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9qi;->T:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-object/from16 v44, v0

    move-object/from16 v0, v44

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v44

    .line 1549033
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9qi;->U:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v45

    .line 1549034
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9qi;->V:Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel$StoryAdminPageModel;

    move-object/from16 v46, v0

    move-object/from16 v0, v46

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v46

    .line 1549035
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9qi;->W:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-object/from16 v47, v0

    move-object/from16 v0, v47

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v47

    .line 1549036
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9qi;->X:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v48

    .line 1549037
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9qi;->Y:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryTopicAttachmentFragmentModel$TopicModel;

    move-object/from16 v49, v0

    move-object/from16 v0, v49

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v49

    .line 1549038
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9qi;->aa:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-object/from16 v50, v0

    move-object/from16 v0, v50

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v50

    .line 1549039
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9qi;->ac:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-object/from16 v51, v0

    move-object/from16 v0, v51

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v51

    .line 1549040
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9qi;->ae:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-object/from16 v52, v0

    move-object/from16 v0, v52

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v52

    .line 1549041
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9qi;->af:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    move-object/from16 v53, v0

    move-object/from16 v0, v53

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v53

    .line 1549042
    const/16 v54, 0x3a

    move/from16 v0, v54

    invoke-virtual {v2, v0}, LX/186;->c(I)V

    .line 1549043
    const/16 v54, 0x0

    move/from16 v0, v54

    invoke-virtual {v2, v0, v3}, LX/186;->b(II)V

    .line 1549044
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v4}, LX/186;->b(II)V

    .line 1549045
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v5}, LX/186;->b(II)V

    .line 1549046
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v6}, LX/186;->b(II)V

    .line 1549047
    const/4 v3, 0x4

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 1549048
    const/4 v3, 0x5

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 1549049
    const/4 v3, 0x6

    invoke-virtual {v2, v3, v9}, LX/186;->b(II)V

    .line 1549050
    const/4 v3, 0x7

    invoke-virtual {v2, v3, v10}, LX/186;->b(II)V

    .line 1549051
    const/16 v3, 0x8

    invoke-virtual {v2, v3, v11}, LX/186;->b(II)V

    .line 1549052
    const/16 v3, 0x9

    invoke-virtual {v2, v3, v12}, LX/186;->b(II)V

    .line 1549053
    const/16 v3, 0xa

    invoke-virtual {v2, v3, v13}, LX/186;->b(II)V

    .line 1549054
    const/16 v3, 0xb

    move-object/from16 v0, p0

    iget v4, v0, LX/9qi;->l:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 1549055
    const/16 v3, 0xc

    invoke-virtual {v2, v3, v14}, LX/186;->b(II)V

    .line 1549056
    const/16 v3, 0xd

    invoke-virtual {v2, v3, v15}, LX/186;->b(II)V

    .line 1549057
    const/16 v3, 0xe

    move/from16 v0, v16

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1549058
    const/16 v3, 0xf

    move/from16 v0, v17

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1549059
    const/16 v3, 0x10

    move/from16 v0, v18

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1549060
    const/16 v3, 0x11

    move/from16 v0, v19

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1549061
    const/16 v3, 0x12

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/9qi;->s:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1549062
    const/16 v3, 0x13

    move/from16 v0, v20

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1549063
    const/16 v3, 0x14

    move/from16 v0, v21

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1549064
    const/16 v3, 0x15

    move/from16 v0, v22

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1549065
    const/16 v3, 0x16

    move/from16 v0, v23

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1549066
    const/16 v3, 0x17

    move/from16 v0, v24

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1549067
    const/16 v3, 0x18

    move/from16 v0, v25

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1549068
    const/16 v3, 0x19

    move/from16 v0, v26

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1549069
    const/16 v3, 0x1a

    move/from16 v0, v27

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1549070
    const/16 v3, 0x1b

    move/from16 v0, v28

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1549071
    const/16 v3, 0x1c

    move/from16 v0, v29

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1549072
    const/16 v3, 0x1d

    move/from16 v0, v30

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1549073
    const/16 v3, 0x1e

    move/from16 v0, v31

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1549074
    const/16 v3, 0x1f

    move/from16 v0, v32

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1549075
    const/16 v3, 0x20

    move/from16 v0, v33

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1549076
    const/16 v3, 0x21

    move/from16 v0, v34

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1549077
    const/16 v3, 0x22

    move-object/from16 v0, p0

    iget v4, v0, LX/9qi;->I:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 1549078
    const/16 v3, 0x23

    move/from16 v0, v35

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1549079
    const/16 v3, 0x24

    move/from16 v0, v36

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1549080
    const/16 v3, 0x25

    move/from16 v0, v37

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1549081
    const/16 v3, 0x26

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/9qi;->M:D

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1549082
    const/16 v3, 0x27

    move/from16 v0, v38

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1549083
    const/16 v3, 0x28

    move/from16 v0, v39

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1549084
    const/16 v3, 0x29

    move/from16 v0, v40

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1549085
    const/16 v3, 0x2a

    move/from16 v0, v41

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1549086
    const/16 v3, 0x2b

    move/from16 v0, v42

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1549087
    const/16 v3, 0x2c

    move/from16 v0, v43

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1549088
    const/16 v3, 0x2d

    move/from16 v0, v44

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1549089
    const/16 v3, 0x2e

    move/from16 v0, v45

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1549090
    const/16 v3, 0x2f

    move/from16 v0, v46

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1549091
    const/16 v3, 0x30

    move/from16 v0, v47

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1549092
    const/16 v3, 0x31

    move/from16 v0, v48

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1549093
    const/16 v3, 0x32

    move/from16 v0, v49

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1549094
    const/16 v3, 0x33

    move-object/from16 v0, p0

    iget v4, v0, LX/9qi;->Z:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 1549095
    const/16 v3, 0x34

    move/from16 v0, v50

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1549096
    const/16 v3, 0x35

    move-object/from16 v0, p0

    iget v4, v0, LX/9qi;->ab:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 1549097
    const/16 v3, 0x36

    move/from16 v0, v51

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1549098
    const/16 v3, 0x37

    move-object/from16 v0, p0

    iget v4, v0, LX/9qi;->ad:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 1549099
    const/16 v3, 0x38

    move/from16 v0, v52

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1549100
    const/16 v3, 0x39

    move/from16 v0, v53

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1549101
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1549102
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1549103
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1549104
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1549105
    new-instance v2, LX/15i;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1549106
    new-instance v3, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    invoke-direct {v3, v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;-><init>(LX/15i;)V

    .line 1549107
    move-object/from16 v0, p0

    iget-object v2, v0, LX/9qi;->k:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v2, :cond_0

    .line 1549108
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, LX/9qi;->k:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->L_()LX/0x2;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/facebook/graphql/model/GraphQLStory;->a(LX/0x2;)V

    .line 1549109
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/9qi;->A:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v2, :cond_1

    .line 1549110
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->C()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, LX/9qi;->A:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->L_()LX/0x2;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/facebook/graphql/model/GraphQLStory;->a(LX/0x2;)V

    .line 1549111
    :cond_1
    return-object v3
.end method
