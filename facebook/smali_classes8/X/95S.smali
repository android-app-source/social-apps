.class public LX/95S;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TEdge:",
        "Ljava/lang/Object;",
        "TUserInfo:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public a:LX/2jj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2jj",
            "<TTEdge;TTUserInfo;*>;"
        }
    .end annotation
.end field

.field public b:LX/2kW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/controller/connectioncontroller/common/ConnectionController",
            "<TTEdge;TTUserInfo;>;"
        }
    .end annotation
.end field

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/95R;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0QK",
            "<",
            "Lcom/facebook/controller/connectioncontroller/common/ConnectionController",
            "<TTEdge;TTUserInfo;>;",
            "Ljava/lang/Void;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2jj;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2jj",
            "<TTEdge;TTUserInfo;*>;)V"
        }
    .end annotation

    .prologue
    .line 1436882
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1436883
    iput-object p1, p0, LX/95S;->a:LX/2jj;

    .line 1436884
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/95S;->c:Ljava/util/List;

    .line 1436885
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()LX/95R;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/95R",
            "<TTEdge;TTUserInfo;>;"
        }
    .end annotation

    .prologue
    .line 1436876
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/95S;->b:LX/2kW;

    if-nez v0, :cond_0

    .line 1436877
    iget-object v0, p0, LX/95S;->a:LX/2jj;

    invoke-virtual {v0}, LX/2jj;->a()LX/2kW;

    move-result-object v0

    iput-object v0, p0, LX/95S;->b:LX/2kW;

    .line 1436878
    :cond_0
    new-instance v0, LX/95R;

    invoke-direct {v0, p0}, LX/95R;-><init>(LX/95S;)V

    .line 1436879
    iget-object v1, p0, LX/95S;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1436880
    monitor-exit p0

    return-object v0

    .line 1436881
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/0QK;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QK",
            "<",
            "Lcom/facebook/controller/connectioncontroller/common/ConnectionController",
            "<TTEdge;TTUserInfo;>;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1436886
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/95S;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1436887
    iget-object v0, p0, LX/95S;->a:LX/2jj;

    .line 1436888
    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/2jj;->b(LX/2jj;Z)LX/2kW;

    move-result-object v1

    move-object v0, v1

    .line 1436889
    invoke-interface {p1, v0}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1436890
    :goto_0
    monitor-exit p0

    return-void

    .line 1436891
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/95S;->d:Ljava/util/List;

    if-nez v0, :cond_1

    .line 1436892
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/95S;->d:Ljava/util/List;

    .line 1436893
    :cond_1
    iget-object v0, p0, LX/95S;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1436894
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/95R;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/95R",
            "<TTEdge;TTUserInfo;>;)V"
        }
    .end annotation

    .prologue
    .line 1436866
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/95S;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1436867
    iget-object v0, p0, LX/95S;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1436868
    iget-object v0, p0, LX/95S;->d:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 1436869
    iget-object v0, p0, LX/95S;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0QK;

    .line 1436870
    iget-object v2, p0, LX/95S;->b:LX/2kW;

    invoke-interface {v0, v2}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1436871
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1436872
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/95S;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1436873
    :cond_1
    iget-object v0, p0, LX/95S;->b:LX/2kW;

    invoke-virtual {v0}, LX/2kW;->c()V

    .line 1436874
    const/4 v0, 0x0

    iput-object v0, p0, LX/95S;->b:LX/2kW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1436875
    :cond_2
    monitor-exit p0

    return-void
.end method

.method public final b()LX/2kW;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/controller/connectioncontroller/common/ConnectionController",
            "<TTEdge;TTUserInfo;>;"
        }
    .end annotation

    .prologue
    .line 1436863
    iget-object v0, p0, LX/95S;->b:LX/2kW;

    if-nez v0, :cond_0

    .line 1436864
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SharedConnectionControllerVendor asked for invalid connection controller"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1436865
    :cond_0
    iget-object v0, p0, LX/95S;->b:LX/2kW;

    return-object v0
.end method
