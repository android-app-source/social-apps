.class public LX/8x3;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/8x1;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8x4;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1423454
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/8x3;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/8x4;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1423455
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1423456
    iput-object p1, p0, LX/8x3;->b:LX/0Ot;

    .line 1423457
    return-void
.end method

.method private a(LX/1De;II)LX/8x1;
    .locals 2

    .prologue
    .line 1423440
    new-instance v0, LX/8x2;

    invoke-direct {v0, p0}, LX/8x2;-><init>(LX/8x3;)V

    invoke-static {p1, p2, p3, v0}, LX/8x3;->a(LX/1De;IILX/8x2;)LX/8x1;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/1De;IILX/8x2;)LX/8x1;
    .locals 1

    .prologue
    .line 1423449
    sget-object v0, LX/8x3;->a:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8x1;

    .line 1423450
    if-nez v0, :cond_0

    .line 1423451
    new-instance v0, LX/8x1;

    invoke-direct {v0}, LX/8x1;-><init>()V

    .line 1423452
    :cond_0
    invoke-static {v0, p0, p1, p2, p3}, LX/8x1;->a$redex0(LX/8x1;LX/1De;IILX/8x2;)V

    .line 1423453
    return-object v0
.end method

.method public static a(LX/0QB;)LX/8x3;
    .locals 3

    .prologue
    .line 1423458
    const-class v1, LX/8x3;

    monitor-enter v1

    .line 1423459
    :try_start_0
    sget-object v0, LX/8x3;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1423460
    sput-object v2, LX/8x3;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1423461
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1423462
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/8x3;->b(LX/0QB;)LX/8x3;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1423463
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8x3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1423464
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1423465
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/8x3;
    .locals 2

    .prologue
    .line 1423447
    new-instance v0, LX/8x3;

    const/16 v1, 0x17a2

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/8x3;-><init>(LX/0Ot;)V

    .line 1423448
    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 2

    .prologue
    .line 1423444
    check-cast p2, LX/8x2;

    .line 1423445
    iget-object v0, p0, LX/8x3;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget v0, p2, LX/8x2;->a:I

    iget v1, p2, LX/8x2;->b:I

    invoke-static {p1, v0, v1}, LX/8x4;->a(LX/1De;II)LX/1Dg;

    move-result-object v0

    .line 1423446
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1423442
    invoke-static {}, LX/1dS;->b()V

    .line 1423443
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/8x1;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1423441
    invoke-direct {p0, p1, v0, v0}, LX/8x3;->a(LX/1De;II)LX/8x1;

    move-result-object v0

    return-object v0
.end method
