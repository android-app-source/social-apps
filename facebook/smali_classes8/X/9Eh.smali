.class public final LX/9Eh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feedback/ui/SingleLineCommentComposerView;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/SingleLineCommentComposerView;)V
    .locals 0

    .prologue
    .line 1457293
    iput-object p1, p0, LX/9Eh;->a:Lcom/facebook/feedback/ui/SingleLineCommentComposerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x240b1761

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1457294
    iget-object v1, p0, LX/9Eh;->a:Lcom/facebook/feedback/ui/SingleLineCommentComposerView;

    iget-object v1, v1, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->N:LX/9CP;

    if-eqz v1, :cond_1

    .line 1457295
    iget-object v1, p0, LX/9Eh;->a:Lcom/facebook/feedback/ui/SingleLineCommentComposerView;

    iget-object v1, v1, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->N:LX/9CP;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v7, 0x1

    .line 1457296
    invoke-static {v1}, LX/9CP;->J(LX/9CP;)Z

    move-result v4

    if-eqz v4, :cond_0

    if-eqz v2, :cond_0

    iget-object v4, v1, LX/9CP;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-nez v4, :cond_2

    .line 1457297
    :cond_0
    iget-object v4, v1, LX/9CP;->g:LX/9FG;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, LX/9FG;->a(Z)V

    .line 1457298
    invoke-static {v1}, LX/9CP;->t(LX/9CP;)V

    .line 1457299
    :cond_1
    :goto_0
    const v1, 0xde84ee

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1457300
    :cond_2
    iget-object v4, v1, LX/9CP;->g:LX/9FG;

    invoke-virtual {v4, v7}, LX/9FG;->a(Z)V

    .line 1457301
    new-instance v5, LX/0ju;

    invoke-direct {v5, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    iget-object v4, v1, LX/9CP;->e:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0tF;

    .line 1457302
    iget-object v6, v4, LX/0tF;->a:LX/0ad;

    sget-char p0, LX/0wn;->Y:C

    const/4 p1, 0x0

    invoke-interface {v6, p0, p1}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1457303
    invoke-static {v6}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_3

    const v6, 0x7f081225

    invoke-virtual {v2, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    :cond_3
    move-object v4, v6

    .line 1457304
    invoke-virtual {v5, v4}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v4

    .line 1457305
    iget-object v5, v1, LX/9CP;->e:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0tF;

    .line 1457306
    iget-object v6, v5, LX/0tF;->a:LX/0ad;

    sget-char p0, LX/0wn;->W:C

    const/4 p1, 0x0

    invoke-interface {v6, p0, p1}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object v5, v6

    .line 1457307
    invoke-static {v5}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 1457308
    :goto_1
    move-object v5, v5

    .line 1457309
    invoke-virtual {v4, v5}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v5

    iget-object v4, v1, LX/9CP;->e:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0tF;

    .line 1457310
    iget-object v6, v4, LX/0tF;->a:LX/0ad;

    sget-char p0, LX/0wn;->Z:C

    const/4 p1, 0x0

    invoke-interface {v6, p0, p1}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1457311
    invoke-static {v6}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_4

    const v6, 0x7f080020

    invoke-virtual {v2, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    :cond_4
    move-object v4, v6

    .line 1457312
    new-instance v6, LX/9CK;

    invoke-direct {v6, v1}, LX/9CK;-><init>(LX/9CP;)V

    invoke-virtual {v5, v4, v6}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v5

    iget-object v4, v1, LX/9CP;->e:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0tF;

    .line 1457313
    iget-object v6, v4, LX/0tF;->a:LX/0ad;

    sget-char p0, LX/0wn;->X:C

    const/4 p1, 0x0

    invoke-interface {v6, p0, p1}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1457314
    invoke-static {v6}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_5

    const v6, 0x7f080017

    invoke-virtual {v2, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    :cond_5
    move-object v4, v6

    .line 1457315
    const/4 v6, 0x0

    invoke-virtual {v5, v4, v6}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v4

    invoke-virtual {v4, v7}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v4

    invoke-virtual {v4}, LX/0ju;->a()LX/2EJ;

    move-result-object v4

    invoke-virtual {v4}, LX/2EJ;->show()V

    goto/16 :goto_0

    .line 1457316
    :cond_6
    iget-object v5, v1, LX/9CP;->d:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/9Dp;

    iget-object v6, v1, LX/9CP;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v5, v6}, LX/9Dp;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)Ljava/lang/String;

    move-result-object v5

    .line 1457317
    iget-object v6, v1, LX/9CP;->r:Ljava/lang/String;

    invoke-static {v6, v5}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    const v5, 0x7f081227

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    :cond_7
    const v5, 0x7f081226

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 p0, 0x0

    iget-object p1, v1, LX/9CP;->q:Ljava/lang/String;

    aput-object p1, v6, p0

    invoke-virtual {v2, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1
.end method
