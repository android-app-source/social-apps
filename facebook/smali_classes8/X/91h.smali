.class public LX/91h;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/91P;

.field public final b:LX/91K;


# direct methods
.method public constructor <init>(LX/91P;LX/91K;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1431293
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1431294
    iput-object p1, p0, LX/91h;->a:LX/91P;

    .line 1431295
    iput-object p2, p0, LX/91h;->b:LX/91K;

    .line 1431296
    return-void
.end method

.method public static a(LX/0QB;)LX/91h;
    .locals 5

    .prologue
    .line 1431297
    const-class v1, LX/91h;

    monitor-enter v1

    .line 1431298
    :try_start_0
    sget-object v0, LX/91h;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1431299
    sput-object v2, LX/91h;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1431300
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1431301
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1431302
    new-instance p0, LX/91h;

    invoke-static {v0}, LX/91P;->a(LX/0QB;)LX/91P;

    move-result-object v3

    check-cast v3, LX/91P;

    invoke-static {v0}, LX/91K;->a(LX/0QB;)LX/91K;

    move-result-object v4

    check-cast v4, LX/91K;

    invoke-direct {p0, v3, v4}, LX/91h;-><init>(LX/91P;LX/91K;)V

    .line 1431303
    move-object v0, p0

    .line 1431304
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1431305
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/91h;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1431306
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1431307
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
