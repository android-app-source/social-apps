.class public LX/9dY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9dM;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9dX;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/8G7;

.field private final c:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

.field private final d:J

.field public e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:Z


# direct methods
.method public constructor <init>(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;LX/0Ot;LX/8G7;)V
    .locals 10
    .param p1    # Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$FramePack;",
            "LX/0Ot",
            "<",
            "LX/9dX;",
            ">;",
            "LX/8G7;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1518243
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1518244
    iput-object p2, p0, LX/9dY;->a:LX/0Ot;

    .line 1518245
    iput-object p3, p0, LX/9dY;->b:LX/8G7;

    .line 1518246
    iput-object p1, p0, LX/9dY;->c:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    .line 1518247
    sget-object v0, LX/0SF;->a:LX/0SF;

    move-object v0, v0

    .line 1518248
    invoke-virtual {v0}, LX/0SF;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/9dY;->d:J

    .line 1518249
    iget-object v0, p0, LX/9dY;->b:LX/8G7;

    .line 1518250
    iget-object v6, v0, LX/8G7;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v7, LX/8G7;->c:LX/0Tn;

    const-wide/16 v8, 0x0

    invoke-interface {v6, v7, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v6

    move-wide v0, v6

    .line 1518251
    iget-wide v2, p0, LX/9dY;->d:J

    const-wide/32 v4, 0x48190800

    sub-long/2addr v2, v4

    cmp-long v0, v2, v0

    if-lez v0, :cond_1

    .line 1518252
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/9dY;->f:Z

    .line 1518253
    :cond_0
    return-void

    .line 1518254
    :cond_1
    iget-object v0, p0, LX/9dY;->b:LX/8G7;

    .line 1518255
    iget-object v6, v0, LX/8G7;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v7, LX/8G7;->b:LX/0Tn;

    const-wide/16 v8, 0x0

    invoke-interface {v6, v7, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v6

    move-wide v0, v6

    .line 1518256
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->bd_()J

    move-result-wide v2

    cmp-long v0, v2, v0

    if-lez v0, :cond_0

    .line 1518257
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/9dY;->e:Ljava/util/Set;

    .line 1518258
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->c()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    .line 1518259
    iget-object v4, p0, LX/9dY;->e:Ljava/util/Set;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1518260
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1518242
    iget-boolean v0, p0, LX/9dY;->f:Z

    return v0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 1518236
    iget-object v0, p0, LX/9dY;->b:LX/8G7;

    iget-object v1, p0, LX/9dY;->c:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->bd_()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/8G7;->a(J)V

    .line 1518237
    iget-object v0, p0, LX/9dY;->b:LX/8G7;

    iget-wide v2, p0, LX/9dY;->d:J

    invoke-virtual {v0, v2, v3}, LX/8G7;->b(J)V

    .line 1518238
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/9dY;->f:Z

    .line 1518239
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1518241
    const/4 v0, 0x0

    return v0
.end method

.method public final d()LX/9dK;
    .locals 1

    .prologue
    .line 1518240
    iget-object v0, p0, LX/9dY;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9dK;

    return-object v0
.end method
