.class public final LX/9eo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V
    .locals 0

    .prologue
    .line 1520618
    iput-object p1, p0, LX/9eo;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2

    .prologue
    .line 1520614
    iget-object v0, p0, LX/9eo;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->N:LX/9eb;

    invoke-interface {v0}, LX/9ea;->h()V

    .line 1520615
    iget-object v0, p0, LX/9eo;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    const/4 v1, 0x0

    .line 1520616
    iput-boolean v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->Z:Z

    .line 1520617
    return-void
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 1520613
    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2

    .prologue
    .line 1520608
    iget-object v0, p0, LX/9eo;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->J:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1520609
    iget-object v0, p0, LX/9eo;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->N:LX/9eb;

    invoke-interface {v0}, LX/9ea;->i()V

    .line 1520610
    iget-object v0, p0, LX/9eo;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    const/4 v1, 0x1

    .line 1520611
    iput-boolean v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->Z:Z

    .line 1520612
    return-void
.end method
