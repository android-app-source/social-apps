.class public final enum LX/A8d;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/A8d;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/A8d;

.field public static final enum LEADING:LX/A8d;

.field public static final enum TRAILING:LX/A8d;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1627914
    new-instance v0, LX/A8d;

    const-string v1, "LEADING"

    invoke-direct {v0, v1, v2}, LX/A8d;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/A8d;->LEADING:LX/A8d;

    .line 1627915
    new-instance v0, LX/A8d;

    const-string v1, "TRAILING"

    invoke-direct {v0, v1, v3}, LX/A8d;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/A8d;->TRAILING:LX/A8d;

    .line 1627916
    const/4 v0, 0x2

    new-array v0, v0, [LX/A8d;

    sget-object v1, LX/A8d;->LEADING:LX/A8d;

    aput-object v1, v0, v2

    sget-object v1, LX/A8d;->TRAILING:LX/A8d;

    aput-object v1, v0, v3

    sput-object v0, LX/A8d;->$VALUES:[LX/A8d;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1627917
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/A8d;
    .locals 1

    .prologue
    .line 1627918
    const-class v0, LX/A8d;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/A8d;

    return-object v0
.end method

.method public static values()[LX/A8d;
    .locals 1

    .prologue
    .line 1627919
    sget-object v0, LX/A8d;->$VALUES:[LX/A8d;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/A8d;

    return-object v0
.end method
