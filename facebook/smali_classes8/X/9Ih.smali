.class public LX/9Ih;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1463513
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1463514
    return-void
.end method

.method public static a(LX/0QB;)LX/9Ih;
    .locals 3

    .prologue
    .line 1463515
    const-class v1, LX/9Ih;

    monitor-enter v1

    .line 1463516
    :try_start_0
    sget-object v0, LX/9Ih;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1463517
    sput-object v2, LX/9Ih;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1463518
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1463519
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1463520
    new-instance v0, LX/9Ih;

    invoke-direct {v0}, LX/9Ih;-><init>()V

    .line 1463521
    move-object v0, v0

    .line 1463522
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1463523
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/9Ih;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1463524
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1463525
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
