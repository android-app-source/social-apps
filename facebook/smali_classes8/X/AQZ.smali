.class public LX/AQZ;
.super LX/1Cv;
.source ""


# instance fields
.field private final a:Landroid/view/View$OnClickListener;

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$TypeAheadSuggestionFieldsModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnClickListener;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1671470
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 1671471
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1671472
    iput-object v0, p0, LX/AQZ;->b:LX/0Px;

    .line 1671473
    iput-object p1, p0, LX/AQZ;->a:Landroid/view/View$OnClickListener;

    .line 1671474
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1671465
    new-instance v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;-><init>(Landroid/content/Context;)V

    .line 1671466
    const v1, 0x7f0207fb

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setBackgroundResource(I)V

    .line 1671467
    iget-object v1, p0, LX/AQZ;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1671468
    sget-object v1, LX/6VF;->SMALL:LX/6VF;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 1671469
    return-object v0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 1671457
    check-cast p3, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 1671458
    check-cast p2, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$TypeAheadSuggestionFieldsModel;

    .line 1671459
    invoke-virtual {p2}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$TypeAheadSuggestionFieldsModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 1671460
    invoke-virtual {p3, p2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTag(Ljava/lang/Object;)V

    .line 1671461
    invoke-virtual {p2}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$TypeAheadSuggestionFieldsModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    .line 1671462
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p3, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 1671463
    return-void

    .line 1671464
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 1671454
    iget-object v0, p0, LX/AQZ;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1671456
    iget-object v0, p0, LX/AQZ;->b:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1671455
    int-to-long v0, p1

    return-wide v0
.end method
