.class public final LX/94Y;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

.field public c:LX/63W;

.field public d:LX/94c;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1435428
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1435429
    const-string v0, ""

    iput-object v0, p0, LX/94Y;->a:Ljava/lang/String;

    .line 1435430
    invoke-static {}, LX/94c;->c()LX/94c;

    move-result-object v0

    iput-object v0, p0, LX/94Y;->d:LX/94c;

    .line 1435431
    return-void
.end method

.method public constructor <init>(LX/94X;)V
    .locals 1

    .prologue
    .line 1435432
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1435433
    const-string v0, ""

    iput-object v0, p0, LX/94Y;->a:Ljava/lang/String;

    .line 1435434
    invoke-static {}, LX/94c;->c()LX/94c;

    move-result-object v0

    iput-object v0, p0, LX/94Y;->d:LX/94c;

    .line 1435435
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1435436
    iget-object v0, p1, LX/94X;->a:Ljava/lang/String;

    iput-object v0, p0, LX/94Y;->a:Ljava/lang/String;

    .line 1435437
    iget-object v0, p1, LX/94X;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    iput-object v0, p0, LX/94Y;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 1435438
    iget-object v0, p1, LX/94X;->c:LX/63W;

    iput-object v0, p0, LX/94Y;->c:LX/63W;

    .line 1435439
    iget-object v0, p1, LX/94X;->d:LX/94c;

    iput-object v0, p0, LX/94Y;->d:LX/94c;

    .line 1435440
    return-void
.end method


# virtual methods
.method public final a()LX/94X;
    .locals 2

    .prologue
    .line 1435427
    new-instance v0, LX/94X;

    invoke-direct {v0, p0}, LX/94X;-><init>(LX/94Y;)V

    return-object v0
.end method

.method public final a(Z)LX/94Y;
    .locals 1

    .prologue
    .line 1435423
    iget-object v0, p0, LX/94Y;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    if-eqz v0, :cond_0

    .line 1435424
    iget-object v0, p0, LX/94Y;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 1435425
    iput-boolean p1, v0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->u:Z

    .line 1435426
    :cond_0
    return-object p0
.end method
