.class public LX/95Y;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2jy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TEdge:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/2jy",
        "<TTEdge;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TTEdge;>;"
        }
    .end annotation
.end field

.field public final b:LX/2kI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2kI",
            "<TTEdge;>;"
        }
    .end annotation
.end field

.field private final c:LX/95X;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/95X",
            "<TTEdge;>;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/concurrent/Executor;

.field private final e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Z

.field private i:Z

.field public j:LX/2kM;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2kM",
            "<TTEdge;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(ZLjava/lang/String;LX/95Z;Ljava/util/concurrent/Executor;)V
    .locals 1
    .param p1    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1437075
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1437076
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/95Y;->a:Ljava/util/List;

    .line 1437077
    invoke-static {p0}, LX/95Y;->f(LX/95Y;)LX/2kM;

    move-result-object v0

    iput-object v0, p0, LX/95Y;->j:LX/2kM;

    .line 1437078
    new-instance v0, LX/2kI;

    invoke-direct {v0}, LX/2kI;-><init>()V

    iput-object v0, p0, LX/95Y;->b:LX/2kI;

    .line 1437079
    if-eqz p1, :cond_0

    .line 1437080
    new-instance v0, LX/95X;

    const/16 p1, 0xafc

    invoke-static {p3, p1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p1

    invoke-direct {v0, p0, p1}, LX/95X;-><init>(LX/2jy;LX/0Or;)V

    .line 1437081
    move-object v0, v0

    .line 1437082
    iput-object v0, p0, LX/95Y;->c:LX/95X;

    .line 1437083
    :goto_0
    iput-object p4, p0, LX/95Y;->d:Ljava/util/concurrent/Executor;

    .line 1437084
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/95Y;->e:Ljava/lang/String;

    .line 1437085
    return-void

    .line 1437086
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/95Y;->c:LX/95X;

    goto :goto_0
.end method

.method public static f(LX/95Y;)LX/2kM;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/2kM",
            "<TTEdge;>;"
        }
    .end annotation

    .prologue
    .line 1437069
    iget-object v0, p0, LX/95Y;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1437070
    sget-object v0, LX/2kL;->a:LX/2kM;

    .line 1437071
    :goto_0
    return-object v0

    .line 1437072
    :cond_0
    iget-object v0, p0, LX/95Y;->a:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 1437073
    new-instance v0, LX/2nm;

    new-instance v2, LX/2nj;

    iget-object v3, p0, LX/95Y;->f:Ljava/lang/String;

    sget-object v4, LX/2nk;->BEFORE:LX/2nk;

    iget-boolean v5, p0, LX/95Y;->h:Z

    invoke-direct {v2, v3, v4, v5}, LX/2nj;-><init>(Ljava/lang/String;LX/2nk;Z)V

    new-instance v3, LX/2nj;

    iget-object v4, p0, LX/95Y;->g:Ljava/lang/String;

    sget-object v5, LX/2nk;->AFTER:LX/2nk;

    iget-boolean v6, p0, LX/95Y;->i:Z

    invoke-direct {v3, v4, v5, v6}, LX/2nj;-><init>(Ljava/lang/String;LX/2nk;Z)V

    invoke-direct {v0, v2, v3}, LX/2nm;-><init>(LX/2nj;LX/2nj;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 1437074
    new-instance v0, LX/95T;

    invoke-direct {v0, p0, v1, v2}, LX/95T;-><init>(LX/95Y;LX/0Px;LX/0Px;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1437068
    iget-object v0, p0, LX/95Y;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/0Rl;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rl",
            "<TTEdge;>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1437067
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Cannot visit edges in simple connection store"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(LX/2kJ;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2kJ",
            "<TTEdge;>;)V"
        }
    .end annotation

    .prologue
    .line 1437065
    iget-object v0, p0, LX/95Y;->b:LX/2kI;

    invoke-virtual {v0, p1}, LX/2kI;->a(LX/2kJ;)V

    .line 1437066
    return-void
.end method

.method public final declared-synchronized a(LX/2nj;LX/3DP;LX/5Mb;JLX/3Cb;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2nj;",
            "LX/3DP;",
            "LX/5Mb",
            "<TTEdge;>;J",
            "LX/3Cb;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 1437006
    monitor-enter p0

    .line 1437007
    :try_start_0
    iget-object v1, p1, LX/2nj;->c:LX/2nk;

    move-object v1, v1

    .line 1437008
    iget-object v3, p0, LX/95Y;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    .line 1437009
    sget-object v3, LX/95U;->a:[I

    invoke-virtual {v1}, LX/2nk;->ordinal()I

    move-result v5

    aget v3, v3, v5

    packed-switch v3, :pswitch_data_0

    .line 1437010
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Not supported Location:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, LX/2nk;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1437011
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1437012
    :pswitch_0
    :try_start_1
    iget-object v1, p0, LX/95Y;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 1437013
    iget-object v1, p0, LX/95Y;->a:Ljava/util/List;

    .line 1437014
    iget-object v3, p3, LX/5Mb;->b:LX/0Px;

    move-object v3, v3

    .line 1437015
    invoke-interface {v1, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1437016
    iget-object v1, p3, LX/5Mb;->c:Ljava/lang/String;

    move-object v1, v1

    .line 1437017
    iput-object v1, p0, LX/95Y;->f:Ljava/lang/String;

    .line 1437018
    iget-object v1, p3, LX/5Mb;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1437019
    iput-object v1, p0, LX/95Y;->g:Ljava/lang/String;

    .line 1437020
    iget-boolean v1, p3, LX/5Mb;->e:Z

    move v1, v1

    .line 1437021
    iput-boolean v1, p0, LX/95Y;->h:Z

    .line 1437022
    iget-boolean v1, p3, LX/5Mb;->f:Z

    move v1, v1

    .line 1437023
    iput-boolean v1, p0, LX/95Y;->i:Z

    move v1, v2

    move v3, v0

    .line 1437024
    :goto_0
    if-nez v4, :cond_4

    if-eqz v1, :cond_4

    .line 1437025
    :goto_1
    iget-object v8, p0, LX/95Y;->d:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/facebook/controller/connectioncontroller/SimpleConnectionStore$1;

    move-object v1, p0

    move-object v4, p3

    move-object v5, p6

    move-object v6, p1

    move-object v7, p2

    invoke-direct/range {v0 .. v7}, Lcom/facebook/controller/connectioncontroller/SimpleConnectionStore$1;-><init>(LX/95Y;ZILX/5Mb;LX/3Cb;LX/2nj;LX/3DP;)V

    const v1, -0x47b3787f

    invoke-static {v8, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1437026
    monitor-exit p0

    return-void

    .line 1437027
    :pswitch_1
    :try_start_2
    iget-object v1, p0, LX/95Y;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v2

    :goto_2
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1437028
    iget-boolean v1, p3, LX/5Mb;->f:Z

    move v1, v1

    .line 1437029
    if-eqz v1, :cond_1

    .line 1437030
    iget-object v1, p0, LX/95Y;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 1437031
    iget-object v1, p0, LX/95Y;->a:Ljava/util/List;

    .line 1437032
    iget-object v3, p3, LX/5Mb;->b:LX/0Px;

    move-object v3, v3

    .line 1437033
    invoke-interface {v1, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1437034
    iget-object v1, p3, LX/5Mb;->c:Ljava/lang/String;

    move-object v1, v1

    .line 1437035
    iput-object v1, p0, LX/95Y;->f:Ljava/lang/String;

    .line 1437036
    iget-object v1, p3, LX/5Mb;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1437037
    iput-object v1, p0, LX/95Y;->g:Ljava/lang/String;

    .line 1437038
    iget-boolean v1, p3, LX/5Mb;->e:Z

    move v1, v1

    .line 1437039
    iput-boolean v1, p0, LX/95Y;->h:Z

    .line 1437040
    iget-boolean v1, p3, LX/5Mb;->f:Z

    move v1, v1

    .line 1437041
    iput-boolean v1, p0, LX/95Y;->i:Z

    move v1, v2

    move v3, v0

    goto :goto_0

    :cond_0
    move v1, v0

    .line 1437042
    goto :goto_2

    .line 1437043
    :cond_1
    iget-object v1, p0, LX/95Y;->a:Ljava/util/List;

    const/4 v3, 0x0

    .line 1437044
    iget-object v5, p3, LX/5Mb;->b:LX/0Px;

    move-object v5, v5

    .line 1437045
    invoke-interface {v1, v3, v5}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    .line 1437046
    iget-object v1, p3, LX/5Mb;->c:Ljava/lang/String;

    move-object v1, v1

    .line 1437047
    if-eqz v1, :cond_2

    .line 1437048
    iget-object v1, p3, LX/5Mb;->c:Ljava/lang/String;

    move-object v1, v1

    .line 1437049
    iput-object v1, p0, LX/95Y;->f:Ljava/lang/String;

    .line 1437050
    :cond_2
    iget-boolean v1, p3, LX/5Mb;->e:Z

    move v1, v1

    .line 1437051
    iput-boolean v1, p0, LX/95Y;->h:Z

    move v1, v0

    move v3, v0

    .line 1437052
    goto :goto_0

    .line 1437053
    :pswitch_2
    iget-object v1, p0, LX/95Y;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    move v1, v2

    :goto_3
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1437054
    iget-object v1, p0, LX/95Y;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    .line 1437055
    iget-object v1, p0, LX/95Y;->a:Ljava/util/List;

    .line 1437056
    iget-object v5, p3, LX/5Mb;->b:LX/0Px;

    move-object v5, v5

    .line 1437057
    invoke-interface {v1, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1437058
    iget-object v1, p3, LX/5Mb;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1437059
    iput-object v1, p0, LX/95Y;->g:Ljava/lang/String;

    .line 1437060
    iget-boolean v1, p3, LX/5Mb;->f:Z

    move v1, v1

    .line 1437061
    iput-boolean v1, p0, LX/95Y;->i:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v1, v0

    .line 1437062
    goto/16 :goto_0

    :cond_3
    move v1, v0

    .line 1437063
    goto :goto_3

    :cond_4
    move v2, v0

    .line 1437064
    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(LX/5Mb;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5Mb",
            "<TTEdge;>;)V"
        }
    .end annotation

    .prologue
    .line 1436969
    iget-object v0, p0, LX/95Y;->a:Ljava/util/List;

    .line 1436970
    iget-object v1, p1, LX/5Mb;->b:LX/0Px;

    move-object v1, v1

    .line 1436971
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1436972
    iget-object v0, p1, LX/5Mb;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1436973
    iput-object v0, p0, LX/95Y;->f:Ljava/lang/String;

    .line 1436974
    iget-object v0, p1, LX/5Mb;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1436975
    iput-object v0, p0, LX/95Y;->g:Ljava/lang/String;

    .line 1436976
    iget-boolean v0, p1, LX/5Mb;->e:Z

    move v0, v0

    .line 1436977
    iput-boolean v0, p0, LX/95Y;->h:Z

    .line 1436978
    iget-boolean v0, p1, LX/5Mb;->f:Z

    move v0, v0

    .line 1436979
    iput-boolean v0, p0, LX/95Y;->i:Z

    .line 1436980
    invoke-static {p0}, LX/95Y;->f(LX/95Y;)LX/2kM;

    move-result-object v0

    iput-object v0, p0, LX/95Y;->j:LX/2kM;

    .line 1436981
    iget-object v0, p0, LX/95Y;->c:LX/95X;

    if-eqz v0, :cond_0

    .line 1436982
    iget-object v0, p1, LX/5Mb;->b:LX/0Px;

    move-object v1, v0

    .line 1436983
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    .line 1436984
    iget-object v4, p0, LX/95Y;->c:LX/95X;

    invoke-static {v4, v3}, LX/95X;->a$redex0(LX/95X;Ljava/lang/Object;)V

    .line 1436985
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1436986
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;LX/3Cf;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/3Cf",
            "<TTEdge;>;)V"
        }
    .end annotation

    .prologue
    .line 1437005
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Cannot visit edges in simple connection store"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(LX/2nj;LX/3DP;I)Z
    .locals 1

    .prologue
    .line 1437004
    const/4 v0, 0x0

    return v0
.end method

.method public final declared-synchronized a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTEdge;TTEdge;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 1436996
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LX/95Y;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 1436997
    if-ltz v1, :cond_0

    .line 1436998
    iget-object v2, p0, LX/95Y;->a:Ljava/util/List;

    invoke-interface {v2, v1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1436999
    iget-object v2, p0, LX/95Y;->j:LX/2kM;

    .line 1437000
    invoke-static {p0}, LX/95Y;->f(LX/95Y;)LX/2kM;

    move-result-object v3

    iput-object v3, p0, LX/95Y;->j:LX/2kM;

    .line 1437001
    iget-object v3, p0, LX/95Y;->b:LX/2kI;

    const/4 v4, 0x1

    invoke-static {v1, v1, v4}, LX/3CY;->a(III)LX/3CY;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    sget-object v4, LX/3Cb;->DISK:LX/3Cb;

    iget-object v5, p0, LX/95Y;->j:LX/2kM;

    invoke-virtual {v3, v1, v4, v2, v5}, LX/2kI;->a(LX/0Px;LX/3Cb;LX/2kM;LX/2kM;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1437002
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1437003
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1436994
    iget-object v0, p0, LX/95Y;->b:LX/2kI;

    iget-object v1, p0, LX/95Y;->j:LX/2kM;

    invoke-virtual {v0, v1}, LX/2kI;->a(LX/2kM;)V

    .line 1436995
    return-void
.end method

.method public final b(LX/2kJ;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2kJ",
            "<TTEdge;>;)V"
        }
    .end annotation

    .prologue
    .line 1436992
    iget-object v0, p0, LX/95Y;->b:LX/2kI;

    invoke-virtual {v0, p1}, LX/2kI;->b(LX/2kJ;)V

    .line 1436993
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1436989
    iget-object v0, p0, LX/95Y;->c:LX/95X;

    if-eqz v0, :cond_0

    .line 1436990
    iget-object v0, p0, LX/95Y;->c:LX/95X;

    invoke-virtual {v0}, LX/95X;->a()V

    .line 1436991
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 1436988
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1436987
    const/4 v0, 0x0

    return v0
.end method
