.class public LX/ARv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0io;",
        ":",
        "LX/0j0;",
        ":",
        "LX/0j1;",
        ":",
        "LX/0j3;",
        ":",
        "LX/0j8;",
        ":",
        "LX/0j4;",
        ":",
        "LX/0j5;",
        ":",
        "LX/0jB;",
        ":",
        "LX/0jD;",
        ":",
        "LX/0j6;",
        ":",
        "LX/0ip;",
        ":",
        "LX/0jF;",
        "DerivedData::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesIsTargetMenuSupported;",
        ":",
        "LX/5RE;",
        "PluginData::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginGetters$ProvidesPluginAllowsAttachingToAlbumsGetter;",
        ":",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginGetters$ProvidesPluginAllowsCheckinGetter;",
        ":",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginGetters$ProvidesPluginAllowsMinutiaeGetter;",
        ":",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginGetters$ProvidesPluginAllowsPhotoGetter;",
        ":",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginGetters$ProvidesPluginAllowsTaggingPeopleGetter;",
        ":",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginGetters$ProvidesPluginTitleGetter;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;:",
        "LX/0in",
        "<TPluginData;>;>",
        "Ljava/lang/Object;",
        "Lcom/facebook/composer/controller/ComposerMenuCreator;"
    }
.end annotation


# static fields
.field public static final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/ARs;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/ARs;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/ARs;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/ARs;",
            ">;"
        }
    .end annotation
.end field

.field public static final e:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/ARs;",
            ">;"
        }
    .end annotation
.end field

.field public static final f:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/ARs;",
            ">;"
        }
    .end annotation
.end field

.field public static final g:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/ARs;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final h:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/composer/targetselection/ComposerTargetSelectorController$TargetSelectorClient;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field public final j:LX/7mI;

.field private final k:LX/APD;

.field public final l:LX/31w;

.field public final m:LX/APF;

.field private final n:LX/2sV;

.field private final o:LX/APL;

.field private final p:LX/AP6;

.field private final q:LX/APz;

.field private final r:LX/0gd;

.field public final s:LX/03V;

.field private final t:Landroid/content/res/Resources;

.field public final u:LX/ARx;

.field public final v:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/2rw;",
            "LX/ARo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1673497
    sget-object v0, LX/ARs;->FRIEND_TAGGING_NOT_ALLOWED:LX/ARs;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/ARv;->a:LX/0Rf;

    .line 1673498
    sget-object v0, LX/ARs;->LOCATION_TAGGING_NOT_ALLOWED:LX/ARs;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/ARv;->b:LX/0Rf;

    .line 1673499
    sget-object v0, LX/ARs;->MINUTIAE_NOT_ALLOWED:LX/ARs;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/ARv;->c:LX/0Rf;

    .line 1673500
    sget-object v0, LX/ARs;->FRIEND_TAGGING_NOT_ALLOWED:LX/ARs;

    sget-object v1, LX/ARs;->LOCATION_TAGGING_NOT_ALLOWED:LX/ARs;

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/ARv;->d:LX/0Rf;

    .line 1673501
    sget-object v0, LX/ARs;->FRIEND_TAGGING_NOT_ALLOWED:LX/ARs;

    sget-object v1, LX/ARs;->MINUTIAE_NOT_ALLOWED:LX/ARs;

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/ARv;->e:LX/0Rf;

    .line 1673502
    sget-object v0, LX/ARs;->LOCATION_TAGGING_NOT_ALLOWED:LX/ARs;

    sget-object v1, LX/ARs;->MINUTIAE_NOT_ALLOWED:LX/ARs;

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/ARv;->f:LX/0Rf;

    .line 1673503
    sget-object v0, LX/ARs;->FRIEND_TAGGING_NOT_ALLOWED:LX/ARs;

    sget-object v1, LX/ARs;->LOCATION_TAGGING_NOT_ALLOWED:LX/ARs;

    sget-object v2, LX/ARs;->MINUTIAE_NOT_ALLOWED:LX/ARs;

    invoke-static {v0, v1, v2}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/ARv;->g:LX/0Rf;

    return-void
.end method

.method public constructor <init>(LX/ARx;Ljava/util/Set;LX/7mI;LX/APD;LX/31w;LX/APF;LX/2sV;LX/APL;LX/AP6;LX/APz;LX/0gd;LX/03V;Landroid/content/res/Resources;LX/Hqt;LX/0il;)V
    .locals 6
    .param p14    # LX/Hqt;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p15    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/composer/targetselection/ComposerTargetTypesBuilder;",
            "Ljava/util/Set",
            "<",
            "LX/ARn;",
            ">;",
            "LX/7mI;",
            "LX/APD;",
            "LX/31w;",
            "LX/APF;",
            "LX/2sV;",
            "LX/APL;",
            "LX/AP6;",
            "LX/APz;",
            "LX/0gd;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Landroid/content/res/Resources;",
            "Lcom/facebook/composer/targetselection/ComposerTargetSelectorController$TargetSelectorClient;",
            "TServices;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1673476
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1673477
    iput-object p1, p0, LX/ARv;->u:LX/ARx;

    .line 1673478
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v2

    .line 1673479
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/ARn;

    .line 1673480
    invoke-interface {v1}, LX/ARn;->a()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/ARo;

    .line 1673481
    iget-object v5, v1, LX/ARo;->a:LX/2rw;

    invoke-virtual {v2, v5, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_0

    .line 1673482
    :cond_1
    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    iput-object v1, p0, LX/ARv;->v:LX/0P1;

    .line 1673483
    iput-object p3, p0, LX/ARv;->j:LX/7mI;

    .line 1673484
    iput-object p4, p0, LX/ARv;->k:LX/APD;

    .line 1673485
    iput-object p5, p0, LX/ARv;->l:LX/31w;

    .line 1673486
    iput-object p6, p0, LX/ARv;->m:LX/APF;

    .line 1673487
    iput-object p7, p0, LX/ARv;->n:LX/2sV;

    .line 1673488
    iput-object p8, p0, LX/ARv;->o:LX/APL;

    .line 1673489
    iput-object p9, p0, LX/ARv;->p:LX/AP6;

    .line 1673490
    move-object/from16 v0, p10

    iput-object v0, p0, LX/ARv;->q:LX/APz;

    .line 1673491
    move-object/from16 v0, p11

    iput-object v0, p0, LX/ARv;->r:LX/0gd;

    .line 1673492
    move-object/from16 v0, p12

    iput-object v0, p0, LX/ARv;->s:LX/03V;

    .line 1673493
    move-object/from16 v0, p13

    iput-object v0, p0, LX/ARv;->t:Landroid/content/res/Resources;

    .line 1673494
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-static/range {p14 .. p14}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, LX/ARv;->h:Ljava/lang/ref/WeakReference;

    .line 1673495
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-static/range {p15 .. p15}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, LX/ARv;->i:Ljava/lang/ref/WeakReference;

    .line 1673496
    return-void
.end method

.method public static a(LX/0P1;)LX/0Rf;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "LX/2rw;",
            "LX/ARu;",
            ">;)",
            "LX/0Rf",
            "<",
            "LX/2rw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1673470
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v2

    .line 1673471
    invoke-virtual {p0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2rw;

    .line 1673472
    invoke-virtual {p0, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/ARu;

    .line 1673473
    iget-boolean v4, v1, LX/ARu;->a:Z

    if-nez v4, :cond_0

    iget-object v1, v1, LX/ARu;->b:LX/0Rf;

    invoke-virtual {v1}, LX/0Rf;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1673474
    invoke-virtual {v2, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_0

    .line 1673475
    :cond_1
    invoke-virtual {v2}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/0il;Ljava/util/Map;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "Ljava/util/Map",
            "<",
            "LX/2rw;",
            "LX/ARt;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 1673446
    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0j5;

    invoke-interface {v0}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    if-eqz v0, :cond_a

    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0j5;

    invoke-interface {v0}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->shareable:Lcom/facebook/graphql/model/GraphQLEntity;

    if-eqz v0, :cond_a

    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0j5;

    invoke-interface {v0}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->shareable:Lcom/facebook/graphql/model/GraphQLEntity;

    invoke-static {v0}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLEntity;)I

    move-result v0

    const v1, 0x1eaef984

    if-ne v0, v1, :cond_a

    move v1, v2

    .line 1673447
    :goto_0
    sget-object v0, LX/2rw;->PAGE:LX/2rw;

    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0j3;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getAttachedStory()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0j3;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getAttachedStory()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {v0}, LX/185;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0j3;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getAttachedStory()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {v0}, LX/0sa;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v3, "everyone"

    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0j3;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getAttachedStory()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {v0}, LX/0sa;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->t()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0j4;

    invoke-interface {v0}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v0

    invoke-static {v0}, LX/2rm;->a(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Z

    move-result v0

    if-nez v0, :cond_0

    if-nez v1, :cond_0

    .line 1673448
    sget-object v0, LX/2rw;->PAGE:LX/2rw;

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ARt;

    sget-object v1, LX/ARs;->POST_NOT_PUBLIC:LX/ARs;

    invoke-virtual {v0, v1}, LX/ARt;->a(LX/ARs;)LX/ARt;

    .line 1673449
    :cond_0
    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-le v0, v2, :cond_1

    .line 1673450
    invoke-static {p0, p1, p2}, LX/ARv;->b(LX/ARv;LX/0il;Ljava/util/Map;)V

    .line 1673451
    :cond_1
    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->m(LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1673452
    iget-object v0, p0, LX/ARv;->u:LX/ARx;

    invoke-virtual {v0}, LX/ARx;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2rw;

    .line 1673453
    iget-object v2, p0, LX/ARv;->l:LX/31w;

    invoke-virtual {v2, v0}, LX/31w;->a(LX/2rw;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1673454
    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ARt;

    sget-object v2, LX/ARs;->MULTIMEDIA_NOT_ALLOWED:LX/ARs;

    invoke-virtual {v0, v2}, LX/ARt;->a(LX/ARs;)LX/ARt;

    goto :goto_1

    .line 1673455
    :cond_3
    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0ip;

    invoke-interface {v0}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 1673456
    iget-object v4, p0, LX/ARv;->u:LX/ARx;

    invoke-virtual {v4}, LX/ARx;->a()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_4
    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/2rw;

    .line 1673457
    iget-object v4, p0, LX/ARv;->m:LX/APF;

    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0io;

    check-cast v6, LX/0j3;

    invoke-interface {v6}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v6

    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0io;

    check-cast v7, LX/0j3;

    invoke-interface {v7}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEditTagEnabled()Z

    move-result v7

    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/0io;

    check-cast v8, LX/0j1;

    invoke-interface {v8}, LX/0j1;->getTargetAlbum()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v8

    if-eqz v8, :cond_5

    const/4 v8, 0x1

    :goto_3
    move-object v9, p1

    check-cast v9, LX/0in;

    invoke-interface {v9}, LX/0in;->d()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/AQ9;

    check-cast v9, LX/AQ9;

    .line 1673458
    iget-object v11, v9, LX/AQ9;->i:LX/ARN;

    move-object v9, v11

    .line 1673459
    invoke-virtual/range {v4 .. v9}, LX/APF;->a(LX/2rw;ZZZLX/ARN;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 1673460
    invoke-interface {p2, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/ARt;

    sget-object v5, LX/ARs;->MINUTIAE_NOT_ALLOWED:LX/ARs;

    invoke-virtual {v4, v5}, LX/ARt;->a(LX/ARs;)LX/ARt;

    goto :goto_2

    .line 1673461
    :cond_5
    const/4 v8, 0x0

    goto :goto_3

    .line 1673462
    :cond_6
    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0j8;

    invoke-interface {v0}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 1673463
    invoke-static {p0, p1, p2}, LX/ARv;->d(LX/ARv;LX/0il;Ljava/util/Map;)V

    .line 1673464
    :cond_7
    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0jD;

    invoke-interface {v0}, LX/0jD;->getTaggedUsers()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    .line 1673465
    invoke-static {p0, p1, p2}, LX/ARv;->e(LX/ARv;LX/0il;Ljava/util/Map;)V

    .line 1673466
    :cond_8
    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0j1;

    invoke-interface {v0}, LX/0j1;->getTargetAlbum()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 1673467
    invoke-static {p0, p1, p2}, LX/ARv;->f(LX/ARv;LX/0il;Ljava/util/Map;)V

    .line 1673468
    :cond_9
    return-void

    .line 1673469
    :cond_a
    const/4 v0, 0x0

    move v1, v0

    goto/16 :goto_0
.end method

.method private static b(LX/ARv;LX/0il;Ljava/util/Map;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "Ljava/util/Map",
            "<",
            "LX/2rw;",
            "LX/ARt;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1673504
    iget-object v0, p0, LX/ARv;->u:LX/ARx;

    invoke-virtual {v0}, LX/ARx;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_0
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2rw;

    .line 1673505
    sget-object v0, LX/2rw;->PAGE:LX/2rw;

    if-eq v2, v0, :cond_1

    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0j4;

    invoke-interface {v0}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v0

    invoke-static {v0}, LX/2rm;->a(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    move v10, v0

    .line 1673506
    :goto_1
    iget-object v0, p0, LX/ARv;->k:LX/APD;

    move-object v1, p1

    check-cast v1, LX/0in;

    invoke-interface {v1}, LX/0in;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/AQ9;

    check-cast v1, LX/AQ9;

    .line 1673507
    iget-object v3, v1, LX/AQ9;->c:LX/ARN;

    move-object v1, v3

    .line 1673508
    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0io;

    check-cast v3, LX/0jB;

    invoke-interface {v3}, LX/0jB;->getReferencedStickerData()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0io;

    check-cast v3, LX/0j5;

    invoke-interface {v3}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0io;

    check-cast v3, LX/0j5;

    invoke-interface {v3}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    if-eqz v3, :cond_3

    const/4 v3, 0x1

    :goto_2
    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0io;

    check-cast v4, LX/0j3;

    invoke-interface {v4}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->shouldDisablePhotos()Z

    move-result v4

    if-nez v4, :cond_4

    const/4 v4, 0x1

    :goto_3
    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0io;

    check-cast v5, LX/0j3;

    invoke-interface {v5}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v5

    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0io;

    check-cast v6, LX/0j3;

    invoke-interface {v6}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->canViewerEditPostMedia()Z

    move-result v6

    move-object v7, p1

    check-cast v7, LX/0ik;

    invoke-interface {v7}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/2zG;

    check-cast v7, LX/5RE;

    invoke-interface {v7}, LX/5RE;->I()LX/5RF;

    move-result-object v7

    sget-object v8, LX/5RF;->SLIDESHOW:LX/5RF;

    if-ne v7, v8, :cond_5

    const/4 v7, 0x1

    :goto_4
    move-object v8, p1

    check-cast v8, LX/0ik;

    invoke-interface {v8}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/2zG;

    check-cast v8, LX/5RE;

    invoke-interface {v8}, LX/5RE;->I()LX/5RF;

    move-result-object v8

    sget-object v9, LX/5RF;->STORYLINE:LX/5RF;

    if-ne v8, v9, :cond_6

    const/4 v8, 0x1

    :goto_5
    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/0io;

    invoke-interface {v9}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v9

    invoke-virtual/range {v0 .. v9}, LX/APD;->a(LX/ARN;LX/2rw;ZZZZZZLX/0Px;)Z

    move-result v0

    .line 1673509
    iget-object v1, p0, LX/ARv;->k:LX/APD;

    invoke-virtual {v1, v0, v2, v10}, LX/APD;->a(ZLX/2rw;Z)Z

    move-result v1

    .line 1673510
    if-eqz v0, :cond_0

    if-nez v1, :cond_0

    .line 1673511
    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ARt;

    sget-object v1, LX/ARs;->MULTI_PHOTOS_NOT_ALLOWED:LX/ARs;

    invoke-virtual {v0, v1}, LX/ARt;->a(LX/ARs;)LX/ARt;

    goto/16 :goto_0

    .line 1673512
    :cond_2
    const/4 v0, 0x0

    move v10, v0

    goto/16 :goto_1

    .line 1673513
    :cond_3
    const/4 v3, 0x0

    goto/16 :goto_2

    :cond_4
    const/4 v4, 0x0

    goto :goto_3

    :cond_5
    const/4 v7, 0x0

    goto :goto_4

    :cond_6
    const/4 v8, 0x0

    goto :goto_5

    .line 1673514
    :cond_7
    return-void
.end method

.method public static c(LX/ARv;)LX/0P1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "LX/2rw;",
            "LX/ARu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1673434
    iget-object v0, p0, LX/ARv;->i:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1673435
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v2

    .line 1673436
    iget-object v1, p0, LX/ARv;->u:LX/ARx;

    invoke-virtual {v1}, LX/ARx;->a()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2rw;

    .line 1673437
    new-instance v4, LX/ARt;

    invoke-direct {v4}, LX/ARt;-><init>()V

    invoke-interface {v2, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1673438
    :cond_0
    sget-object v1, LX/2rw;->PAGE:LX/2rw;

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/ARv;->j:LX/7mI;

    invoke-interface {v1}, LX/7mI;->a()LX/03R;

    move-result-object v1

    sget-object v3, LX/03R;->NO:LX/03R;

    if-ne v1, v3, :cond_1

    .line 1673439
    sget-object v1, LX/2rw;->PAGE:LX/2rw;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/ARt;

    .line 1673440
    const/4 v3, 0x1

    iput-boolean v3, v1, LX/ARt;->a:Z

    .line 1673441
    :cond_1
    invoke-direct {p0, v0, v2}, LX/ARv;->a(LX/0il;Ljava/util/Map;)V

    .line 1673442
    new-instance v0, LX/ARq;

    invoke-direct {v0, p0}, LX/ARq;-><init>(LX/ARv;)V

    invoke-static {v2, v0}, LX/0PM;->a(Ljava/util/Map;LX/4zq;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v1

    .line 1673443
    invoke-static {v1}, LX/ARv;->a(LX/0P1;)LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No active targets! Target states: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1673444
    return-object v1

    .line 1673445
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static d(LX/ARv;LX/0il;Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "Ljava/util/Map",
            "<",
            "LX/2rw;",
            "LX/ARt;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1673428
    iget-object v0, p0, LX/ARv;->u:LX/ARx;

    invoke-virtual {v0}, LX/ARx;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2rw;

    .line 1673429
    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j3;

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v3

    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j3;

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEditTagEnabled()Z

    move-result v4

    move-object v1, p1

    check-cast v1, LX/0in;

    invoke-interface {v1}, LX/0in;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/AQ9;

    check-cast v1, LX/AQ9;

    .line 1673430
    iget-object p0, v1, LX/AQ9;->n:LX/ARN;

    move-object v1, p0

    .line 1673431
    invoke-static {v3, v4, v1}, LX/2sV;->a(ZZLX/ARN;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1673432
    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ARt;

    sget-object v1, LX/ARs;->LOCATION_TAGGING_NOT_ALLOWED:LX/ARs;

    invoke-virtual {v0, v1}, LX/ARt;->a(LX/ARs;)LX/ARt;

    goto :goto_0

    .line 1673433
    :cond_1
    return-void
.end method

.method private static e(LX/ARv;LX/0il;Ljava/util/Map;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "Ljava/util/Map",
            "<",
            "LX/2rw;",
            "LX/ARt;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1673420
    iget-object v0, p0, LX/ARv;->u:LX/ARx;

    invoke-virtual {v0}, LX/ARx;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2rw;

    .line 1673421
    sget-object v1, LX/2rw;->PAGE:LX/2rw;

    if-eq v0, v1, :cond_1

    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j4;

    invoke-interface {v1}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v1

    invoke-static {v1}, LX/2rm;->a(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    const/4 v3, 0x1

    .line 1673422
    :goto_1
    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j3;

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v1

    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0io;

    check-cast v2, LX/0j3;

    invoke-interface {v2}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEditTagEnabled()Z

    move-result v2

    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0io;

    check-cast v4, LX/0j3;

    invoke-interface {v4}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->shouldDisableFriendTagging()Z

    move-result v4

    move-object v5, p1

    check-cast v5, LX/0in;

    invoke-interface {v5}, LX/0in;->d()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/AQ9;

    check-cast v5, LX/AQ9;

    .line 1673423
    iget-object p0, v5, LX/AQ9;->s:LX/ARN;

    move-object v5, p0

    .line 1673424
    invoke-static/range {v0 .. v5}, LX/APL;->a(LX/2rw;ZZZZLX/ARN;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1673425
    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ARt;

    sget-object v1, LX/ARs;->FRIEND_TAGGING_NOT_ALLOWED:LX/ARs;

    invoke-virtual {v0, v1}, LX/ARt;->a(LX/ARs;)LX/ARt;

    goto :goto_0

    .line 1673426
    :cond_2
    const/4 v3, 0x0

    goto :goto_1

    .line 1673427
    :cond_3
    return-void
.end method

.method private static f(LX/ARv;LX/0il;Ljava/util/Map;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "Ljava/util/Map",
            "<",
            "LX/2rw;",
            "LX/ARt;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1673413
    iget-object v0, p0, LX/ARv;->u:LX/ARx;

    invoke-virtual {v0}, LX/ARx;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_0
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2rw;

    .line 1673414
    iget-object v0, p0, LX/ARv;->p:LX/AP6;

    move-object v1, p1

    check-cast v1, LX/0in;

    invoke-interface {v1}, LX/0in;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/AQ9;

    .line 1673415
    iget-object v3, v1, LX/AQ9;->f:LX/ARN;

    move-object v1, v3

    .line 1673416
    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0io;

    check-cast v3, LX/0j6;

    invoke-interface {v3}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v3

    iget-wide v4, v3, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0io;

    check-cast v4, LX/0jF;

    invoke-interface {v4}, LX/0jF;->getPublishMode()LX/5Rn;

    move-result-object v4

    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0io;

    check-cast v5, LX/0j3;

    invoke-interface {v5}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v5

    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0io;

    check-cast v6, LX/0j3;

    invoke-interface {v6}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->shouldDisableAttachToAlbum()Z

    move-result v6

    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0io;

    invoke-interface {v7}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v7

    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/0io;

    check-cast v8, LX/0ip;

    invoke-interface {v8}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v8

    if-eqz v8, :cond_1

    const/4 v8, 0x1

    :goto_1
    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/0io;

    check-cast v9, LX/0j4;

    invoke-interface {v9}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v9

    invoke-static {v9}, LX/2rm;->a(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Z

    move-result v9

    invoke-virtual/range {v0 .. v9}, LX/AP6;->a(LX/ARN;LX/2rw;Ljava/lang/String;LX/5Rn;ZZLX/0Px;ZZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1673417
    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ARt;

    sget-object v1, LX/ARs;->ATTACH_TO_ALBUM_NOT_ALLOWED:LX/ARs;

    invoke-virtual {v0, v1}, LX/ARt;->a(LX/ARs;)LX/ARt;

    goto/16 :goto_0

    .line 1673418
    :cond_1
    const/4 v8, 0x0

    goto :goto_1

    .line 1673419
    :cond_2
    return-void
.end method


# virtual methods
.method public final a(LX/5OG;)V
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 1673350
    iget-object v0, p0, LX/ARv;->i:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1673351
    iget-object v2, p0, LX/ARv;->r:LX/0gd;

    sget-object v3, LX/0ge;->COMPOSER_OPENED_TARGET_SELECTOR:LX/0ge;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j0;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 1673352
    invoke-static {p0}, LX/ARv;->c(LX/ARv;)LX/0P1;

    move-result-object v6

    .line 1673353
    iget-object v1, p0, LX/ARv;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    const-string v2, "targetSelectorClient was garbage collected"

    invoke-static {v1, v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Hqt;

    .line 1673354
    invoke-virtual {p1}, LX/5OG;->clear()V

    .line 1673355
    invoke-virtual {v6}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v2

    invoke-virtual {v2}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2rw;

    .line 1673356
    invoke-virtual {v6, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/ARu;

    .line 1673357
    iget-boolean v4, v3, LX/ARu;->a:Z

    if-nez v4, :cond_0

    .line 1673358
    iget-object v4, p0, LX/ARv;->v:LX/0P1;

    invoke-virtual {v4, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/ARo;

    .line 1673359
    sget-object v5, LX/2rw;->UNDIRECTED:LX/2rw;

    if-ne v2, v5, :cond_3

    iget-object v8, p0, LX/ARv;->q:LX/APz;

    move-object v5, v0

    check-cast v5, LX/0in;

    invoke-interface {v5}, LX/0in;->d()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/AQ9;

    check-cast v5, LX/AQ9;

    .line 1673360
    iget-object v9, v5, LX/AQ9;->g:LX/AQ4;

    move-object v9, v9

    .line 1673361
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0io;

    check-cast v5, LX/0j3;

    invoke-interface {v5}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v10

    sget-object v11, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0io;

    check-cast v5, LX/0j4;

    invoke-interface {v5}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v5

    invoke-virtual {v8, v9, v10, v11, v5}, LX/APz;->a(LX/AQ4;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Lcom/facebook/ipc/composer/intent/ComposerTargetData;Lcom/facebook/ipc/composer/intent/ComposerPageData;)Ljava/lang/String;

    move-result-object v5

    .line 1673362
    :goto_1
    iget v8, v4, LX/ARo;->b:I

    invoke-virtual {p1, v12, v8, v5}, LX/5OG;->a(IILjava/lang/CharSequence;)LX/3Ai;

    move-result-object v5

    .line 1673363
    iget v4, v4, LX/ARo;->d:I

    invoke-virtual {v5, v4}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 1673364
    new-instance v4, LX/ARp;

    invoke-direct {v4, p0, v1, v2}, LX/ARp;-><init>(LX/ARv;LX/Hqt;LX/2rw;)V

    invoke-virtual {v5, v4}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1673365
    iget-object v4, v3, LX/ARu;->b:LX/0Rf;

    invoke-virtual {v4}, LX/0Rf;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1673366
    iget-object v3, v3, LX/ARu;->b:LX/0Rf;

    .line 1673367
    sget-object v4, LX/ARr;->a:[I

    invoke-virtual {v2}, LX/2rw;->ordinal()I

    move-result v8

    aget v4, v4, v8

    packed-switch v4, :pswitch_data_0

    .line 1673368
    :cond_1
    iget-object v4, p0, LX/ARv;->s:LX/03V;

    const-string v8, "composer_no_explanation_for_deactivated_target"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "target: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", deactivation: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v8, v9}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1673369
    const/4 v4, 0x0

    :goto_2
    move v2, v4

    .line 1673370
    if-eqz v2, :cond_2

    .line 1673371
    iget-object v3, p0, LX/ARv;->t:Landroid/content/res/Resources;

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, LX/3Ai;->a(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1673372
    :cond_2
    invoke-virtual {v5, v12}, LX/3Ai;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_0

    .line 1673373
    :cond_3
    iget-object v5, p0, LX/ARv;->t:Landroid/content/res/Resources;

    iget v8, v4, LX/ARo;->c:I

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 1673374
    :cond_4
    return-void

    .line 1673375
    :pswitch_0
    sget-object v4, LX/ARs;->MULTI_PHOTOS_NOT_ALLOWED:LX/ARs;

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1673376
    const v4, 0x7f0824cc

    goto :goto_2

    .line 1673377
    :cond_5
    sget-object v4, LX/ARs;->MULTIMEDIA_NOT_ALLOWED:LX/ARs;

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1673378
    const v4, 0x7f0824db

    goto :goto_2

    .line 1673379
    :pswitch_1
    sget-object v4, LX/ARs;->MULTIMEDIA_NOT_ALLOWED:LX/ARs;

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1673380
    const v4, 0x7f0824dc

    goto :goto_2

    .line 1673381
    :cond_6
    sget-object v4, LX/ARv;->c:LX/0Rf;

    invoke-virtual {v4, v3}, LX/0Rf;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1673382
    const v4, 0x7f0824cd

    goto :goto_2

    .line 1673383
    :cond_7
    sget-object v4, LX/ARv;->b:LX/0Rf;

    invoke-virtual {v4, v3}, LX/0Rf;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1673384
    const v4, 0x7f0824cf

    goto :goto_2

    .line 1673385
    :cond_8
    sget-object v4, LX/ARv;->a:LX/0Rf;

    invoke-virtual {v4, v3}, LX/0Rf;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 1673386
    const v4, 0x7f0824d1

    goto :goto_2

    .line 1673387
    :cond_9
    sget-object v4, LX/ARv;->e:LX/0Rf;

    invoke-virtual {v4, v3}, LX/0Rf;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 1673388
    const v4, 0x7f0824d3

    goto :goto_2

    .line 1673389
    :cond_a
    sget-object v4, LX/ARv;->f:LX/0Rf;

    invoke-virtual {v4, v3}, LX/0Rf;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 1673390
    const v4, 0x7f0824d5

    goto :goto_2

    .line 1673391
    :cond_b
    sget-object v4, LX/ARv;->d:LX/0Rf;

    invoke-virtual {v4, v3}, LX/0Rf;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 1673392
    const v4, 0x7f0824d7

    goto/16 :goto_2

    .line 1673393
    :cond_c
    sget-object v4, LX/ARv;->g:LX/0Rf;

    invoke-virtual {v4, v3}, LX/0Rf;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1673394
    const v4, 0x7f0824d9

    goto/16 :goto_2

    .line 1673395
    :pswitch_2
    sget-object v4, LX/ARs;->MULTIMEDIA_NOT_ALLOWED:LX/ARs;

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 1673396
    const v4, 0x7f0824dd

    goto/16 :goto_2

    .line 1673397
    :cond_d
    sget-object v4, LX/ARs;->POST_NOT_PUBLIC:LX/ARs;

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 1673398
    const v4, 0x7f0824cb

    goto/16 :goto_2

    .line 1673399
    :cond_e
    sget-object v4, LX/ARv;->c:LX/0Rf;

    invoke-virtual {v4, v3}, LX/0Rf;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 1673400
    const v4, 0x7f0824ce

    goto/16 :goto_2

    .line 1673401
    :cond_f
    sget-object v4, LX/ARv;->b:LX/0Rf;

    invoke-virtual {v4, v3}, LX/0Rf;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 1673402
    const v4, 0x7f0824d0

    goto/16 :goto_2

    .line 1673403
    :cond_10
    sget-object v4, LX/ARv;->a:LX/0Rf;

    invoke-virtual {v4, v3}, LX/0Rf;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 1673404
    const v4, 0x7f0824d2

    goto/16 :goto_2

    .line 1673405
    :cond_11
    sget-object v4, LX/ARv;->e:LX/0Rf;

    invoke-virtual {v4, v3}, LX/0Rf;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 1673406
    const v4, 0x7f0824d4

    goto/16 :goto_2

    .line 1673407
    :cond_12
    sget-object v4, LX/ARv;->f:LX/0Rf;

    invoke-virtual {v4, v3}, LX/0Rf;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_13

    .line 1673408
    const v4, 0x7f0824d6

    goto/16 :goto_2

    .line 1673409
    :cond_13
    sget-object v4, LX/ARv;->d:LX/0Rf;

    invoke-virtual {v4, v3}, LX/0Rf;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_14

    .line 1673410
    const v4, 0x7f0824d8

    goto/16 :goto_2

    .line 1673411
    :cond_14
    sget-object v4, LX/ARv;->g:LX/0Rf;

    invoke-virtual {v4, v3}, LX/0Rf;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1673412
    const v4, 0x7f0824da

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
