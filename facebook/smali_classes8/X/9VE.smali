.class public LX/9VE;
.super LX/9VC;
.source ""


# instance fields
.field public c:Landroid/opengl/EGLSurface;

.field private d:Z


# direct methods
.method public constructor <init>(LX/9VA;Landroid/view/Surface;)V
    .locals 4

    .prologue
    .line 1499158
    invoke-direct {p0, p1}, LX/9VC;-><init>(LX/9VA;)V

    .line 1499159
    iget-object v0, p1, LX/9VA;->f:Landroid/opengl/EGLDisplay;

    iget-object v1, p1, LX/9VA;->e:Landroid/opengl/EGLConfig;

    iget-object v2, p1, LX/9VA;->c:[I

    const/4 v3, 0x0

    invoke-static {v0, v1, p2, v2, v3}, Landroid/opengl/EGL14;->eglCreateWindowSurface(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLConfig;Ljava/lang/Object;[II)Landroid/opengl/EGLSurface;

    move-result-object v0

    .line 1499160
    const-string v1, "eglCreateWindowSurface"

    invoke-static {v1}, LX/9VA;->b(Ljava/lang/String;)V

    .line 1499161
    move-object v0, v0

    .line 1499162
    iput-object v0, p0, LX/9VE;->c:Landroid/opengl/EGLSurface;

    .line 1499163
    iget-object v0, p0, LX/9VE;->c:Landroid/opengl/EGLSurface;

    .line 1499164
    if-eqz v0, :cond_0

    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    if-eq v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 1499165
    iput-boolean v0, p0, LX/9VE;->d:Z

    .line 1499166
    iput-object p2, p0, LX/9VE;->b:Landroid/view/Surface;

    .line 1499167
    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static g(LX/9VE;)V
    .locals 2

    .prologue
    .line 1499168
    iget-object v0, p0, LX/9VE;->c:Landroid/opengl/EGLSurface;

    if-nez v0, :cond_0

    .line 1499169
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "mEglSurface must be set in the constructor"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1499170
    :cond_0
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 5

    .prologue
    .line 1499171
    iget-object v0, p0, LX/9VC;->a:LX/9VA;

    iget-object v1, p0, LX/9VE;->c:Landroid/opengl/EGLSurface;

    .line 1499172
    iget-object v2, v0, LX/9VA;->f:Landroid/opengl/EGLDisplay;

    sget-object v3, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    sget-object v4, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    sget-object p0, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    invoke-static {v2, v3, v4, p0}, Landroid/opengl/EGL14;->eglMakeCurrent(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;)Z

    .line 1499173
    iget-object v2, v0, LX/9VA;->f:Landroid/opengl/EGLDisplay;

    invoke-static {v2, v1}, Landroid/opengl/EGL14;->eglDestroySurface(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;)Z

    .line 1499174
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 1499175
    invoke-static {p0}, LX/9VE;->g(LX/9VE;)V

    .line 1499176
    iget-object v0, p0, LX/9VC;->a:LX/9VA;

    iget-object v1, p0, LX/9VE;->c:Landroid/opengl/EGLSurface;

    .line 1499177
    iget-object v2, v0, LX/9VA;->f:Landroid/opengl/EGLDisplay;

    iget-object p0, v0, LX/9VA;->g:Landroid/opengl/EGLContext;

    invoke-static {v2, v1, v1, p0}, Landroid/opengl/EGL14;->eglMakeCurrent(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;)Z

    .line 1499178
    return-void
.end method
