.class public LX/9JI;
.super Landroid/database/MatrixCursor;
.source ""

# interfaces
.implements LX/2nf;


# instance fields
.field private final a:LX/15i;

.field private final b:LX/95b;

.field private final c:[Lcom/facebook/flatbuffers/Flattenable;

.field private final d:Landroid/os/Bundle;

.field private e:LX/2kb;


# direct methods
.method public constructor <init>(Ljava/nio/ByteBuffer;LX/95b;I)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 1464543
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    invoke-direct {p0, v0, p3}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;I)V

    .line 1464544
    if-nez p1, :cond_0

    .line 1464545
    iput-object v2, p0, LX/9JI;->a:LX/15i;

    .line 1464546
    :goto_0
    iput-object p2, p0, LX/9JI;->b:LX/95b;

    .line 1464547
    new-array v0, p3, [Lcom/facebook/flatbuffers/Flattenable;

    iput-object v0, p0, LX/9JI;->c:[Lcom/facebook/flatbuffers/Flattenable;

    .line 1464548
    new-array v0, v6, [Ljava/lang/Object;

    aput-object v2, v0, v4

    .line 1464549
    :goto_1
    if-ge v4, p3, :cond_1

    .line 1464550
    invoke-super {p0, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 1464551
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1464552
    :cond_0
    new-instance v0, LX/15i;

    move-object v1, p1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    iput-object v0, p0, LX/9JI;->a:LX/15i;

    .line 1464553
    iget-object v0, p0, LX/9JI;->a:LX/15i;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "BufferRowsCursor["

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "]"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/15i;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1464554
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, LX/9JI;->d:Landroid/os/Bundle;

    .line 1464555
    iget-object v0, p0, LX/9JI;->d:Landroid/os/Bundle;

    const-string v1, "CHANGE_NUMBER"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1464556
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 1464542
    invoke-virtual {p0}, LX/9JI;->getPosition()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public final a(LX/2kb;)V
    .locals 1

    .prologue
    .line 1464538
    iget-object v0, p0, LX/9JI;->e:LX/2kb;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1464539
    iput-object p1, p0, LX/9JI;->e:LX/2kb;

    .line 1464540
    return-void

    .line 1464541
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 1464537
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final c()Lcom/facebook/flatbuffers/Flattenable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">()TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1464531
    invoke-virtual {p0}, LX/9JI;->getPosition()I

    move-result v1

    .line 1464532
    iget-object v0, p0, LX/9JI;->c:[Lcom/facebook/flatbuffers/Flattenable;

    aget-object v0, v0, v1

    if-eqz v0, :cond_0

    .line 1464533
    iget-object v0, p0, LX/9JI;->c:[Lcom/facebook/flatbuffers/Flattenable;

    aget-object v0, v0, v1

    .line 1464534
    :goto_0
    return-object v0

    .line 1464535
    :cond_0
    iget-object v0, p0, LX/9JI;->b:LX/95b;

    invoke-interface {v0, v1}, LX/95b;->b(I)LX/0w5;

    move-result-object v0

    iget-object v2, p0, LX/9JI;->a:LX/15i;

    iget-object v3, p0, LX/9JI;->b:LX/95b;

    invoke-interface {v3, v1}, LX/95b;->a(I)I

    move-result v3

    invoke-virtual {v0, v2, v3}, LX/0w5;->a(LX/15i;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    .line 1464536
    iget-object v2, p0, LX/9JI;->c:[Lcom/facebook/flatbuffers/Flattenable;

    aput-object v0, v2, v1

    goto :goto_0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 1464527
    iget-object v0, p0, LX/9JI;->e:LX/2kb;

    if-eqz v0, :cond_0

    .line 1464528
    iget-object v0, p0, LX/9JI;->e:LX/2kb;

    invoke-interface {v0}, LX/2kb;->close()V

    .line 1464529
    :cond_0
    invoke-super {p0}, Landroid/database/MatrixCursor;->close()V

    .line 1464530
    return-void
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 1464525
    iget-object v0, p0, LX/9JI;->b:LX/95b;

    invoke-virtual {p0}, LX/9JI;->getPosition()I

    move-result v1

    invoke-interface {v0, v1}, LX/95b;->f(I)I

    move-result v0

    return v0
.end method

.method public final getExtras()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 1464526
    iget-object v0, p0, LX/9JI;->d:Landroid/os/Bundle;

    return-object v0
.end method
