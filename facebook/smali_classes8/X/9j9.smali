.class public LX/9j9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Tn;

.field private final b:LX/0Tn;

.field public final c:LX/9jB;

.field private final d:LX/9jn;

.field public final e:LX/1e2;

.field public f:LX/9jN;

.field private final g:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "LX/9jN;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/9jB;LX/9jn;LX/1e2;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1529224
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1529225
    sget-object v0, LX/9jz;->a:LX/0Tn;

    const-string v1, "tag_people_to_place_time_of_last_skip"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/9j9;->a:LX/0Tn;

    .line 1529226
    sget-object v0, LX/9jz;->a:LX/0Tn;

    const-string v1, "tag_people_to_place_consecutive_num_times_skipped"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/9j9;->b:LX/0Tn;

    .line 1529227
    new-instance v0, LX/9j8;

    invoke-direct {v0, p0}, LX/9j8;-><init>(LX/9j9;)V

    iput-object v0, p0, LX/9j9;->g:LX/0TF;

    .line 1529228
    iput-object p1, p0, LX/9j9;->c:LX/9jB;

    .line 1529229
    iget-object v0, p0, LX/9j9;->c:LX/9jB;

    iget-object v1, p0, LX/9j9;->b:LX/0Tn;

    iget-object v2, p0, LX/9j9;->a:LX/0Tn;

    invoke-virtual {v0, v1, v2}, LX/9jB;->a(LX/0Tn;LX/0Tn;)V

    .line 1529230
    iput-object p2, p0, LX/9j9;->d:LX/9jn;

    .line 1529231
    iget-object v0, p0, LX/9j9;->d:LX/9jn;

    iget-object v1, p0, LX/9j9;->g:LX/0TF;

    invoke-virtual {v0, v1}, LX/9jn;->a(LX/0TF;)V

    .line 1529232
    iput-object p3, p0, LX/9j9;->e:LX/1e2;

    .line 1529233
    return-void
.end method

.method public static b(LX/0QB;)LX/9j9;
    .locals 4

    .prologue
    .line 1529234
    new-instance v3, LX/9j9;

    invoke-static {p0}, LX/9jB;->b(LX/0QB;)LX/9jB;

    move-result-object v0

    check-cast v0, LX/9jB;

    invoke-static {p0}, LX/9jn;->a(LX/0QB;)LX/9jn;

    move-result-object v1

    check-cast v1, LX/9jn;

    invoke-static {p0}, LX/1e2;->b(LX/0QB;)LX/1e2;

    move-result-object v2

    check-cast v2, LX/1e2;

    invoke-direct {v3, v0, v1, v2}, LX/9j9;-><init>(LX/9jB;LX/9jn;LX/1e2;)V

    .line 1529235
    return-object v3
.end method
