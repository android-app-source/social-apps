.class public final LX/AEG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLCoupon;

.field public final synthetic b:LX/162;

.field public final synthetic c:Z

.field public final synthetic d:LX/AEI;


# direct methods
.method public constructor <init>(LX/AEI;Lcom/facebook/graphql/model/GraphQLCoupon;LX/162;Z)V
    .locals 0

    .prologue
    .line 1646310
    iput-object p1, p0, LX/AEG;->d:LX/AEI;

    iput-object p2, p0, LX/AEG;->a:Lcom/facebook/graphql/model/GraphQLCoupon;

    iput-object p3, p0, LX/AEG;->b:LX/162;

    iput-boolean p4, p0, LX/AEG;->c:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, -0x6a5b5846

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1646311
    new-instance v1, Lcom/facebook/feed/protocol/coupons/ClaimCouponParams;

    iget-object v2, p0, LX/AEG;->a:Lcom/facebook/graphql/model/GraphQLCoupon;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLCoupon;->k()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/AEG;->a:Lcom/facebook/graphql/model/GraphQLCoupon;

    invoke-static {v3}, LX/AEI;->d(Lcom/facebook/graphql/model/GraphQLCoupon;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/facebook/feed/protocol/coupons/ClaimCouponParams;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1646312
    iget-object v2, p0, LX/AEG;->d:LX/AEI;

    iget-object v2, v2, LX/AEI;->c:LX/1Ck;

    const-string v3, "email_coupon"

    iget-object v4, p0, LX/AEG;->d:LX/AEI;

    iget-object v4, v4, LX/AEI;->b:LX/8pa;

    .line 1646313
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 1646314
    const-string v6, "claimCouponParams"

    invoke-virtual {v8, v6, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1646315
    iget-object v6, v4, LX/8pa;->a:LX/0aG;

    const-string v7, "feed_claim_coupon"

    sget-object v9, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    const/4 v10, 0x0

    const v11, -0x63c31168

    invoke-static/range {v6 .. v11}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v6

    invoke-interface {v6}, LX/1MF;->start()LX/1ML;

    move-result-object v6

    .line 1646316
    move-object v1, v6

    .line 1646317
    new-instance v4, LX/AEF;

    invoke-direct {v4, p0, p1}, LX/AEF;-><init>(LX/AEG;Landroid/view/View;)V

    invoke-virtual {v2, v3, v1, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1646318
    const v1, 0x70f94ddb

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
