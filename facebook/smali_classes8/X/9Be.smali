.class public LX/9Be;
.super LX/9BY;
.source ""


# instance fields
.field public C:LX/4lW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:LX/0wW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/sounds/FBSoundUtil;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public F:LX/0tH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public G:LX/20v;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public H:LX/0hL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final I:I

.field private final J:I

.field private final K:LX/0wd;

.field private final L:LX/0wd;

.field public final M:LX/9BZ;

.field public N:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/9Bd;",
            ">;"
        }
    .end annotation
.end field

.field public O:LX/4lV;

.field private P:LX/211;

.field public Q:LX/1zt;

.field private R:Landroid/graphics/Path;

.field private S:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1452248
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/9Be;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1452249
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1451935
    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, v0}, LX/9Be;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1451936
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 11
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    const-wide/high16 v8, 0x402e000000000000L    # 15.0

    const-wide/16 v6, 0x0

    const/4 v4, 0x1

    .line 1452228
    invoke-direct {p0, p1, p2, p3}, LX/9BY;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1452229
    sget-object v0, LX/211;->UNKNOWN:LX/211;

    iput-object v0, p0, LX/9Be;->P:LX/211;

    .line 1452230
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/9Be;

    const-class v3, LX/4lW;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/4lW;

    invoke-static {v0}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v5

    check-cast v5, LX/0wW;

    const/16 v10, 0x3567

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {v0}, LX/0tH;->a(LX/0QB;)LX/0tH;

    move-result-object p2

    check-cast p2, LX/0tH;

    invoke-static {v0}, LX/20v;->a(LX/0QB;)LX/20v;

    move-result-object p3

    check-cast p3, LX/20v;

    invoke-static {v0}, LX/0hL;->a(LX/0QB;)LX/0hL;

    move-result-object v0

    check-cast v0, LX/0hL;

    iput-object v3, v2, LX/9Be;->C:LX/4lW;

    iput-object v5, v2, LX/9Be;->D:LX/0wW;

    iput-object v10, v2, LX/9Be;->E:LX/0Ot;

    iput-object p2, v2, LX/9Be;->F:LX/0tH;

    iput-object p3, v2, LX/9Be;->G:LX/20v;

    iput-object v0, v2, LX/9Be;->H:LX/0hL;

    .line 1452231
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1452232
    const v1, 0x7f0b0fdf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, LX/9Be;->I:I

    .line 1452233
    const v1, 0x7f0b0fe0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/9Be;->J:I

    .line 1452234
    iget-object v0, p0, LX/9Be;->D:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    const-wide/high16 v2, 0x4014000000000000L    # 5.0

    invoke-static {v8, v9, v2, v3}, LX/0wT;->b(DD)LX/0wT;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    .line 1452235
    iput-boolean v4, v0, LX/0wd;->c:Z

    .line 1452236
    move-object v0, v0

    .line 1452237
    invoke-virtual {v0, v6, v7}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    move-result-object v0

    new-instance v1, LX/9Ba;

    invoke-direct {v1, p0}, LX/9Ba;-><init>(LX/9Be;)V

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v0

    iput-object v0, p0, LX/9Be;->K:LX/0wd;

    .line 1452238
    new-instance v0, LX/9BZ;

    invoke-direct {v0, p0}, LX/9BZ;-><init>(LX/9Be;)V

    iput-object v0, p0, LX/9Be;->M:LX/9BZ;

    .line 1452239
    iget-object v0, p0, LX/9Be;->D:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    const-wide/high16 v2, 0x4034000000000000L    # 20.0

    invoke-static {v2, v3, v8, v9}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    .line 1452240
    iput-boolean v4, v0, LX/0wd;->c:Z

    .line 1452241
    move-object v0, v0

    .line 1452242
    invoke-virtual {v0, v6, v7}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    move-result-object v0

    iget-object v1, p0, LX/9Be;->M:LX/9BZ;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v0

    iput-object v0, p0, LX/9Be;->L:LX/0wd;

    .line 1452243
    invoke-virtual {p0, v4}, LX/9Be;->setFocusable(Z)V

    .line 1452244
    new-instance v0, LX/9BX;

    invoke-direct {v0, p0, p0}, LX/9BX;-><init>(LX/9BY;Landroid/view/View;)V

    invoke-static {p0, v0}, LX/0vv;->a(Landroid/view/View;LX/0vn;)V

    .line 1452245
    iget-object v0, p0, LX/9Be;->F:LX/0tH;

    invoke-virtual {v0}, LX/0tH;->f()LX/1zu;

    move-result-object v0

    iget-boolean v0, v0, LX/1zu;->isVectorBased:Z

    if-eqz v0, :cond_0

    .line 1452246
    const/4 v0, 0x0

    invoke-virtual {p0, v4, v0}, LX/9Be;->setLayerType(ILandroid/graphics/Paint;)V

    .line 1452247
    :cond_0
    return-void
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 11

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    const/4 v10, 0x0

    .line 1452211
    iget-object v0, p0, LX/9BY;->E:Landroid/graphics/drawable/Drawable;

    move-object v7, v0

    .line 1452212
    invoke-virtual {p0}, LX/9BY;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1452213
    iget-object v0, p0, LX/9Be;->N:LX/0Px;

    iget-object v1, p0, LX/9Be;->O:LX/4lV;

    .line 1452214
    iget v3, v1, LX/4lV;->d:I

    move v1, v3

    .line 1452215
    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Bd;

    .line 1452216
    iget v1, v0, LX/9Bd;->e:F

    move v0, v1

    .line 1452217
    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget v1, p0, LX/9BY;->x:I

    int-to-float v1, v1

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    move v6, v0

    .line 1452218
    :goto_0
    const-wide v8, 0x406fe00000000000L    # 255.0

    iget-object v0, p0, LX/9Be;->O:LX/4lV;

    invoke-virtual {v0}, LX/4lV;->b()LX/0wd;

    move-result-object v0

    invoke-virtual {v0}, LX/0wd;->d()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    invoke-static/range {v0 .. v5}, LX/0xw;->a(DDD)D

    move-result-wide v0

    mul-double/2addr v0, v8

    double-to-int v0, v0

    invoke-virtual {v7, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1452219
    invoke-virtual {p1, v10, v6}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1452220
    invoke-virtual {v7, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1452221
    neg-float v0, v6

    invoke-virtual {p1, v10, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1452222
    return-void

    .line 1452223
    :cond_0
    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    iget v1, p0, LX/9BY;->x:I

    int-to-float v1, v1

    div-float/2addr v1, v2

    add-float/2addr v1, v0

    iget-object v0, p0, LX/9Be;->N:LX/0Px;

    iget-object v2, p0, LX/9Be;->O:LX/4lV;

    .line 1452224
    iget v3, v2, LX/4lV;->d:I

    move v2, v3

    .line 1452225
    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Bd;

    .line 1452226
    iget v2, v0, LX/9Bd;->e:F

    move v0, v2

    .line 1452227
    sub-float v0, v1, v0

    move v6, v0

    goto :goto_0
.end method

.method private a(Landroid/graphics/Canvas;LX/9Bd;)V
    .locals 12

    .prologue
    .line 1452186
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1452187
    invoke-virtual {p2}, LX/9Bd;->j()F

    move-result v0

    .line 1452188
    const/high16 v1, 0x3f800000    # 1.0f

    sub-float v1, v0, v1

    iget v2, p0, LX/9BY;->r:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    .line 1452189
    iget v2, p2, LX/9BW;->f:F

    move v2, v2

    .line 1452190
    iget v3, p0, LX/9BY;->m:I

    mul-int/lit8 v3, v3, 0x4

    int-to-float v3, v3

    add-float v6, v2, v3

    .line 1452191
    iget v2, p0, LX/9BY;->l:I

    iget v3, p0, LX/9BY;->m:I

    mul-int/lit8 v3, v3, 0x2

    add-int v7, v2, v3

    .line 1452192
    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v0, v2

    const/high16 v2, 0x3fa00000    # 1.25f

    div-float v8, v0, v2

    .line 1452193
    invoke-virtual {p0}, LX/9BY;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, LX/9BY;->n:I

    neg-int v0, v0

    sub-int/2addr v0, v7

    int-to-float v0, v0

    sub-float/2addr v0, v1

    .line 1452194
    :goto_0
    iget v1, p0, LX/9BY;->r:I

    int-to-float v1, v1

    sub-float/2addr v1, v6

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1452195
    const-wide v10, 0x406fe00000000000L    # 255.0

    float-to-double v0, v8

    const-wide/16 v2, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    invoke-static/range {v0 .. v5}, LX/0xw;->a(DDD)D

    move-result-wide v0

    mul-double/2addr v0, v10

    double-to-int v1, v0

    .line 1452196
    iget-object v0, p0, LX/9BY;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1452197
    iget-object v0, p0, LX/9BY;->d:Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/high16 v4, 0x3f000000    # 0.5f

    add-float/2addr v4, v6

    float-to-int v4, v4

    invoke-virtual {v0, v2, v3, v4, v7}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1452198
    const/high16 v0, 0x40000000    # 2.0f

    div-float v2, v6, v0

    invoke-virtual {p0}, LX/9BY;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, LX/9BY;->r:I

    int-to-float v0, v0

    .line 1452199
    iget v3, p2, LX/9Bd;->f:F

    move v3, v3

    .line 1452200
    mul-float/2addr v0, v3

    :goto_1
    invoke-virtual {p1, v8, v8, v2, v0}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 1452201
    iget-object v0, p0, LX/9BY;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1452202
    int-to-float v0, v7

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v0, v2

    iget-object v2, p0, LX/9BY;->e:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->descent()F

    move-result v2

    iget-object v3, p0, LX/9BY;->e:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->ascent()F

    move-result v3

    sub-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    add-float/2addr v0, v2

    iget-object v2, p0, LX/9BY;->e:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->descent()F

    move-result v2

    sub-float/2addr v0, v2

    .line 1452203
    iget v2, p0, LX/9BY;->m:I

    mul-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1452204
    iget-object v2, p0, LX/9BY;->e:Landroid/graphics/Paint;

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1452205
    iget-object v1, p2, LX/9BW;->e:Ljava/lang/String;

    move-object v1, v1

    .line 1452206
    const/4 v2, 0x0

    iget-object v3, p0, LX/9BY;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v0, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1452207
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1452208
    return-void

    .line 1452209
    :cond_0
    iget v0, p0, LX/9BY;->r:I

    int-to-float v0, v0

    add-float/2addr v0, v1

    iget v1, p0, LX/9BY;->n:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    goto/16 :goto_0

    .line 1452210
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static g(LX/9Be;)Z
    .locals 2

    .prologue
    .line 1452185
    iget-object v0, p0, LX/9Be;->F:LX/0tH;

    invoke-virtual {v0}, LX/0tH;->f()LX/1zu;

    move-result-object v0

    sget-object v1, LX/1zu;->VECTOR_ANIMATED_ALWAYS:LX/1zu;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setScaleForAllFaces(Z)V
    .locals 9

    .prologue
    .line 1452176
    iget-object v0, p0, LX/9Be;->N:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_2

    iget-object v0, p0, LX/9Be;->N:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Bd;

    .line 1452177
    iget v1, p0, LX/9BY;->A:F

    .line 1452178
    if-eqz p1, :cond_0

    .line 1452179
    iget-object v1, v0, LX/9BW;->c:LX/1zt;

    move-object v1, v1

    .line 1452180
    iget-object v4, p0, LX/9Be;->Q:LX/1zt;

    if-ne v1, v4, :cond_1

    const/high16 v1, 0x3f800000    # 1.0f

    .line 1452181
    :cond_0
    :goto_1
    iget-object v5, v0, LX/9Bd;->c:LX/0wd;

    float-to-double v7, v1

    invoke-virtual {v5, v7, v8}, LX/0wd;->b(D)LX/0wd;

    .line 1452182
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1452183
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 1452184
    :cond_2
    return-void
.end method


# virtual methods
.method public final a(I)Landroid/graphics/Rect;
    .locals 7

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    .line 1452168
    iget v0, p0, LX/9BY;->s:I

    .line 1452169
    iget-object v1, p0, LX/9BY;->E:Landroid/graphics/drawable/Drawable;

    move-object v1, v1

    .line 1452170
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v1

    iget-object v1, p0, LX/9BY;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v1

    int-to-float v1, v0

    .line 1452171
    iget-object v0, p0, LX/9Be;->N:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Bd;

    invoke-virtual {v0}, LX/9Bd;->i()F

    move-result v0

    iget v2, p0, LX/9BY;->r:I

    int-to-float v2, v2

    mul-float/2addr v0, v2

    iget v2, p0, LX/9BY;->t:I

    int-to-float v2, v2

    add-float/2addr v2, v0

    .line 1452172
    iget-object v0, p0, LX/9Be;->N:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Bd;

    .line 1452173
    iget v3, v0, LX/9Bd;->e:F

    move v0, v3

    .line 1452174
    int-to-float v3, p1

    mul-float/2addr v3, v2

    add-float/2addr v1, v3

    .line 1452175
    new-instance v3, Landroid/graphics/Rect;

    iget v4, p0, LX/9BY;->t:I

    int-to-float v4, v4

    sub-float v4, v1, v4

    float-to-int v4, v4

    div-float v5, v2, v6

    sub-float v5, v0, v5

    float-to-int v5, v5

    add-float/2addr v1, v2

    float-to-int v1, v1

    div-float/2addr v2, v6

    add-float/2addr v0, v2

    float-to-int v0, v0

    invoke-direct {v3, v4, v5, v1, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v3
.end method

.method public final a(FF)V
    .locals 12

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x1

    .line 1452135
    const/4 v0, 0x2

    new-array v2, v0, [I

    .line 1452136
    invoke-virtual {p0, v2}, LX/9Be;->getLocationOnScreen([I)V

    .line 1452137
    aget v0, v2, v6

    iget v1, p0, LX/9BY;->s:I

    add-int/2addr v0, v1

    .line 1452138
    iget-object v1, p0, LX/9BY;->E:Landroid/graphics/drawable/Drawable;

    move-object v1, v1

    .line 1452139
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v1

    iget-object v1, p0, LX/9BY;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v1

    int-to-float v4, v0

    .line 1452140
    invoke-virtual {p0}, LX/9BY;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    aget v0, v2, v3

    int-to-float v0, v0

    move v1, v0

    .line 1452141
    :goto_0
    invoke-virtual {p0}, LX/9BY;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    aget v0, v2, v3

    invoke-virtual {p0}, LX/9Be;->getHeight()I

    move-result v2

    add-int/2addr v0, v2

    iget v2, p0, LX/9BY;->o:I

    add-int/2addr v0, v2

    int-to-float v0, v0

    move v2, v0

    .line 1452142
    :goto_1
    iget-object v0, p0, LX/9Be;->N:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v8

    move v5, v6

    move v7, v4

    move v4, v6

    :goto_2
    if-ge v5, v8, :cond_7

    iget-object v0, p0, LX/9Be;->N:LX/0Px;

    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Bd;

    .line 1452143
    invoke-virtual {v0}, LX/9Bd;->i()F

    move-result v9

    iget v10, p0, LX/9BY;->r:I

    int-to-float v10, v10

    mul-float/2addr v9, v10

    iget v10, p0, LX/9BY;->t:I

    int-to-float v10, v10

    add-float/2addr v9, v10

    .line 1452144
    if-nez v4, :cond_2

    iget v10, p0, LX/9BY;->t:I

    int-to-float v10, v10

    sub-float v10, v7, v10

    cmpl-float v10, p1, v10

    if-ltz v10, :cond_2

    add-float v10, v7, v9

    cmpg-float v10, p1, v10

    if-gez v10, :cond_2

    cmpl-float v10, p2, v1

    if-ltz v10, :cond_2

    cmpg-float v10, p2, v2

    if-gtz v10, :cond_2

    .line 1452145
    invoke-virtual {v0, v3}, LX/9Bd;->a(Z)V

    .line 1452146
    iget-object v4, p0, LX/9Be;->K:LX/0wd;

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v4, v10, v11}, LX/0wd;->b(D)LX/0wd;

    .line 1452147
    iget-object v4, v0, LX/9BW;->c:LX/1zt;

    move-object v0, v4

    .line 1452148
    iput-object v0, p0, LX/9Be;->Q:LX/1zt;

    move v0, v3

    .line 1452149
    :goto_3
    add-float/2addr v7, v9

    .line 1452150
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move v4, v0

    goto :goto_2

    .line 1452151
    :cond_0
    aget v0, v2, v3

    iget v1, p0, LX/9BY;->o:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    move v1, v0

    goto :goto_0

    .line 1452152
    :cond_1
    aget v0, v2, v3

    invoke-virtual {p0}, LX/9Be;->getHeight()I

    move-result v2

    add-int/2addr v0, v2

    int-to-float v0, v0

    move v2, v0

    goto :goto_1

    .line 1452153
    :cond_2
    invoke-virtual {v0, v6}, LX/9Bd;->a(Z)V

    .line 1452154
    iget v0, p0, LX/9BY;->t:I

    int-to-float v0, v0

    sub-float v0, v7, v0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_3

    .line 1452155
    sget-object v0, LX/211;->LEFT:LX/211;

    iput-object v0, p0, LX/9Be;->P:LX/211;

    .line 1452156
    :cond_3
    add-float v0, v7, v9

    cmpl-float v0, p1, v0

    if-lez v0, :cond_4

    .line 1452157
    sget-object v0, LX/211;->RIGHT:LX/211;

    iput-object v0, p0, LX/9Be;->P:LX/211;

    .line 1452158
    :cond_4
    cmpg-float v0, p2, v1

    if-gez v0, :cond_5

    .line 1452159
    sget-object v0, LX/211;->ABOVE:LX/211;

    iput-object v0, p0, LX/9Be;->P:LX/211;

    .line 1452160
    :cond_5
    cmpl-float v0, p2, v2

    if-lez v0, :cond_6

    .line 1452161
    sget-object v0, LX/211;->BELOW:LX/211;

    iput-object v0, p0, LX/9Be;->P:LX/211;

    :cond_6
    move v0, v4

    goto :goto_3

    .line 1452162
    :cond_7
    if-eqz v4, :cond_8

    .line 1452163
    sget-object v0, LX/211;->OVER:LX/211;

    iput-object v0, p0, LX/9Be;->P:LX/211;

    .line 1452164
    :goto_4
    invoke-direct {p0, v4}, LX/9Be;->setScaleForAllFaces(Z)V

    .line 1452165
    return-void

    .line 1452166
    :cond_8
    iget-object v0, p0, LX/9Be;->K:LX/0wd;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 1452167
    sget-object v0, LX/1zt;->c:LX/1zt;

    iput-object v0, p0, LX/9Be;->Q:LX/1zt;

    goto :goto_4
.end method

.method public final b(Landroid/view/MotionEvent;)Z
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/high16 v2, 0x3f000000    # 0.5f

    .line 1452108
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    add-float/2addr v0, v2

    float-to-int v0, v0

    .line 1452109
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    add-float/2addr v1, v2

    float-to-int v1, v1

    .line 1452110
    const/4 v2, 0x2

    new-array v2, v2, [I

    .line 1452111
    new-instance v3, Landroid/graphics/Rect;

    .line 1452112
    iget-object v4, p0, LX/9BY;->E:Landroid/graphics/drawable/Drawable;

    move-object v4, v4

    .line 1452113
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 1452114
    invoke-virtual {p0, v2}, LX/9Be;->getLocationOnScreen([I)V

    .line 1452115
    iget v4, p0, LX/9BY;->f:I

    iget v5, p0, LX/9BY;->k:I

    add-int/2addr v4, v5

    .line 1452116
    iget v5, v3, Landroid/graphics/Rect;->left:I

    aget v6, v2, v7

    add-int/2addr v5, v6

    iput v5, v3, Landroid/graphics/Rect;->left:I

    .line 1452117
    iget v5, v3, Landroid/graphics/Rect;->right:I

    aget v6, v2, v7

    add-int/2addr v5, v6

    iput v5, v3, Landroid/graphics/Rect;->right:I

    .line 1452118
    iget v5, v3, Landroid/graphics/Rect;->top:I

    aget v6, v2, v8

    add-int/2addr v5, v6

    iput v5, v3, Landroid/graphics/Rect;->top:I

    .line 1452119
    iget v5, v3, Landroid/graphics/Rect;->bottom:I

    aget v2, v2, v8

    add-int/2addr v2, v5

    iput v2, v3, Landroid/graphics/Rect;->bottom:I

    .line 1452120
    invoke-virtual {p0}, LX/9BY;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1452121
    iget v2, v3, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v2, v4

    iput v2, v3, Landroid/graphics/Rect;->bottom:I

    .line 1452122
    :goto_0
    iget v2, v3, Landroid/graphics/Rect;->left:I

    if-ge v0, v2, :cond_1

    .line 1452123
    sget-object v2, LX/211;->LEFT:LX/211;

    .line 1452124
    :goto_1
    move-object v2, v2

    .line 1452125
    iput-object v2, p0, LX/9Be;->P:LX/211;

    .line 1452126
    invoke-virtual {v3, v0, v1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    return v0

    .line 1452127
    :cond_0
    iget v2, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v4

    iput v2, v3, Landroid/graphics/Rect;->top:I

    goto :goto_0

    .line 1452128
    :cond_1
    iget v2, v3, Landroid/graphics/Rect;->right:I

    if-le v0, v2, :cond_2

    .line 1452129
    sget-object v2, LX/211;->RIGHT:LX/211;

    goto :goto_1

    .line 1452130
    :cond_2
    iget v2, v3, Landroid/graphics/Rect;->top:I

    if-ge v1, v2, :cond_3

    .line 1452131
    sget-object v2, LX/211;->ABOVE:LX/211;

    goto :goto_1

    .line 1452132
    :cond_3
    iget v2, v3, Landroid/graphics/Rect;->bottom:I

    if-le v1, v2, :cond_4

    .line 1452133
    sget-object v2, LX/211;->BELOW:LX/211;

    goto :goto_1

    .line 1452134
    :cond_4
    sget-object v2, LX/211;->OVER:LX/211;

    goto :goto_1
.end method

.method public final c()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1452101
    sget-object v0, LX/1zt;->c:LX/1zt;

    iput-object v0, p0, LX/9Be;->Q:LX/1zt;

    .line 1452102
    sget-object v0, LX/211;->UNKNOWN:LX/211;

    iput-object v0, p0, LX/9Be;->P:LX/211;

    .line 1452103
    iget-object v0, p0, LX/9Be;->N:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, LX/9Be;->N:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Bd;

    .line 1452104
    invoke-virtual {v0, v2}, LX/9Bd;->a(Z)V

    .line 1452105
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1452106
    :cond_0
    invoke-direct {p0, v2}, LX/9Be;->setScaleForAllFaces(Z)V

    .line 1452107
    return-void
.end method

.method public final d()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1452083
    sget-object v0, LX/1zt;->c:LX/1zt;

    iput-object v0, p0, LX/9Be;->Q:LX/1zt;

    .line 1452084
    sget-object v0, LX/211;->UNKNOWN:LX/211;

    iput-object v0, p0, LX/9Be;->P:LX/211;

    .line 1452085
    iput-boolean v1, p0, LX/9Be;->S:Z

    .line 1452086
    iget-object v0, p0, LX/9Be;->O:LX/4lV;

    .line 1452087
    iget-object v2, v0, LX/4lV;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    move-object v0, v2

    .line 1452088
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0wd;

    .line 1452089
    iput-boolean v1, v0, LX/0wd;->c:Z

    .line 1452090
    goto :goto_0

    .line 1452091
    :cond_0
    iget-object v0, p0, LX/9Be;->O:LX/4lV;

    invoke-virtual {v0}, LX/4lV;->b()LX/0wd;

    move-result-object v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 1452092
    iget-object v0, p0, LX/9Be;->E:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3RX;

    const-string v2, "reactions_dock_appear"

    invoke-virtual {v0, v2}, LX/3RX;->a(Ljava/lang/String;)V

    .line 1452093
    invoke-static {p0}, LX/9Be;->g(LX/9Be;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1452094
    iget-object v0, p0, LX/9Be;->N:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    :goto_1
    if-ge v1, v2, :cond_2

    iget-object v0, p0, LX/9Be;->N:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Bd;

    .line 1452095
    iget-object v3, v0, LX/9BW;->d:Landroid/graphics/drawable/Drawable;

    move-object v3, v3

    .line 1452096
    instance-of v3, v3, LX/9Ug;

    if-eqz v3, :cond_1

    .line 1452097
    iget-object v3, v0, LX/9BW;->d:Landroid/graphics/drawable/Drawable;

    move-object v0, v3

    .line 1452098
    check-cast v0, LX/9Ug;

    invoke-virtual {v0}, LX/9Ug;->a()V

    .line 1452099
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1452100
    :cond_2
    return-void
.end method

.method public final e()V
    .locals 14

    .prologue
    const/high16 v11, 0x40000000    # 2.0f

    const/4 v2, 0x1

    const/4 v6, 0x0

    const-wide/16 v12, 0x0

    .line 1452047
    iget-boolean v0, p0, LX/9Be;->S:Z

    if-eqz v0, :cond_0

    .line 1452048
    :goto_0
    return-void

    .line 1452049
    :cond_0
    iput-boolean v2, p0, LX/9Be;->S:Z

    .line 1452050
    iget-object v0, p0, LX/9Be;->O:LX/4lV;

    .line 1452051
    iget-object v1, v0, LX/4lV;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    move-object v0, v1

    .line 1452052
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0wd;

    .line 1452053
    iput-boolean v2, v0, LX/0wd;->c:Z

    .line 1452054
    goto :goto_1

    .line 1452055
    :cond_1
    iget-object v0, p0, LX/9Be;->O:LX/4lV;

    invoke-virtual {v0}, LX/4lV;->b()LX/0wd;

    move-result-object v0

    invoke-virtual {v0, v12, v13}, LX/0wd;->b(D)LX/0wd;

    .line 1452056
    iget-object v0, p0, LX/9Be;->K:LX/0wd;

    invoke-virtual {v0, v12, v13}, LX/0wd;->b(D)LX/0wd;

    .line 1452057
    iget-object v0, p0, LX/9Be;->N:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v7

    move v5, v6

    :goto_2
    if-ge v5, v7, :cond_7

    iget-object v0, p0, LX/9Be;->N:LX/0Px;

    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Bd;

    .line 1452058
    iget-object v1, p0, LX/9Be;->Q:LX/1zt;

    sget-object v3, LX/1zt;->c:LX/1zt;

    if-eq v1, v3, :cond_3

    .line 1452059
    iget-object v1, v0, LX/9BW;->c:LX/1zt;

    move-object v1, v1

    .line 1452060
    iget-object v3, p0, LX/9Be;->Q:LX/1zt;

    if-ne v1, v3, :cond_3

    .line 1452061
    iget-object v1, p0, LX/9Be;->L:LX/0wd;

    invoke-virtual {v1, v12, v13}, LX/0wd;->a(D)LX/0wd;

    move-result-object v1

    invoke-virtual {v1}, LX/0wd;->j()LX/0wd;

    .line 1452062
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, LX/9Be;->R:Landroid/graphics/Path;

    .line 1452063
    invoke-virtual {v0}, LX/9Bd;->i()F

    move-result v1

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float/2addr v1, v3

    iget v3, p0, LX/9BY;->r:I

    int-to-float v3, v3

    mul-float/2addr v1, v3

    div-float v3, v1, v11

    .line 1452064
    iget-object v4, p0, LX/9Be;->R:Landroid/graphics/Path;

    .line 1452065
    iget v1, v0, LX/9Bd;->h:F

    move v8, v1

    .line 1452066
    iget v1, v0, LX/9Bd;->e:F

    move v9, v1

    .line 1452067
    invoke-virtual {p0}, LX/9BY;->a()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, -0x1

    :goto_3
    int-to-float v1, v1

    mul-float/2addr v1, v3

    add-float/2addr v1, v9

    invoke-virtual {v4, v8, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1452068
    iget v1, p0, LX/9Be;->I:I

    int-to-float v1, v1

    add-float/2addr v1, v3

    .line 1452069
    iget-object v3, p0, LX/9Be;->H:LX/0hL;

    invoke-virtual {v3}, LX/0hL;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1452070
    invoke-virtual {p0}, LX/9Be;->getWidth()I

    move-result v3

    int-to-float v3, v3

    sub-float v1, v3, v1

    .line 1452071
    :cond_2
    invoke-virtual {p0}, LX/9BY;->a()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {p0}, LX/9Be;->getHeight()I

    move-result v3

    iget v4, p0, LX/9Be;->J:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    .line 1452072
    :goto_4
    iget-object v8, p0, LX/9Be;->R:Landroid/graphics/Path;

    .line 1452073
    iget v4, v0, LX/9Bd;->h:F

    move v4, v4

    .line 1452074
    add-float/2addr v4, v1

    div-float v9, v4, v11

    invoke-virtual {p0}, LX/9BY;->a()Z

    move-result v4

    if-eqz v4, :cond_6

    iget v4, p0, LX/9Be;->J:I

    neg-int v4, v4

    int-to-float v4, v4

    :goto_5
    invoke-virtual {v8, v9, v4, v1, v3}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 1452075
    iget-object v1, p0, LX/9Be;->M:LX/9BZ;

    iget-object v3, p0, LX/9Be;->R:Landroid/graphics/Path;

    invoke-virtual {v1, v0, v3}, LX/9BZ;->a(LX/9Bd;Landroid/graphics/Path;)V

    .line 1452076
    iget-object v1, p0, LX/9Be;->L:LX/0wd;

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v1, v8, v9}, LX/0wd;->b(D)LX/0wd;

    .line 1452077
    :cond_3
    invoke-virtual {v0, v6}, LX/9Bd;->a(Z)V

    .line 1452078
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto/16 :goto_2

    :cond_4
    move v1, v2

    .line 1452079
    goto :goto_3

    .line 1452080
    :cond_5
    iget v3, p0, LX/9Be;->J:I

    int-to-float v3, v3

    goto :goto_4

    .line 1452081
    :cond_6
    invoke-virtual {p0}, LX/9Be;->getHeight()I

    move-result v4

    iget v10, p0, LX/9Be;->J:I

    add-int/2addr v4, v10

    int-to-float v4, v4

    goto :goto_5

    .line 1452082
    :cond_7
    invoke-direct {p0, v6}, LX/9Be;->setScaleForAllFaces(Z)V

    goto/16 :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 1452046
    iget-boolean v0, p0, LX/9Be;->S:Z

    return v0
.end method

.method public getCurrentReaction()LX/1zt;
    .locals 1

    .prologue
    .line 1452045
    iget-object v0, p0, LX/9Be;->Q:LX/1zt;

    return-object v0
.end method

.method public getFaceConfigs()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "LX/9BW;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1452044
    iget-object v0, p0, LX/9Be;->N:LX/0Px;

    return-object v0
.end method

.method public getPointerPosition()LX/211;
    .locals 1

    .prologue
    .line 1452043
    iget-object v0, p0, LX/9Be;->P:LX/211;

    return-object v0
.end method

.method public final invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 1452040
    invoke-virtual {p0, p1}, LX/9Be;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1452041
    invoke-virtual {p0}, LX/9Be;->invalidate()V

    .line 1452042
    :cond_0
    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 14

    .prologue
    .line 1451985
    invoke-super {p0, p1}, LX/9BY;->onDraw(Landroid/graphics/Canvas;)V

    .line 1451986
    invoke-direct {p0, p1}, LX/9Be;->a(Landroid/graphics/Canvas;)V

    .line 1451987
    iget v0, p0, LX/9BY;->r:I

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float v7, v0, v1

    .line 1451988
    iget-object v0, p0, LX/9BY;->E:Landroid/graphics/drawable/Drawable;

    move-object v0, v0

    .line 1451989
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, LX/9BY;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v1

    iget v1, p0, LX/9BY;->s:I

    add-int/2addr v0, v1

    int-to-float v1, v0

    .line 1451990
    iget-object v0, p0, LX/9Be;->N:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v8

    const/4 v0, 0x0

    move v6, v0

    :goto_0
    if-ge v6, v8, :cond_a

    iget-object v0, p0, LX/9Be;->N:LX/0Px;

    invoke-virtual {v0, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Bd;

    .line 1451991
    invoke-virtual {v0}, LX/9Bd;->i()F

    move-result v4

    .line 1451992
    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v2, v4, v2

    iget v3, p0, LX/9BY;->r:I

    int-to-float v3, v3

    mul-float v9, v2, v3

    .line 1451993
    const/4 v2, 0x0

    .line 1451994
    iget-object v3, p0, LX/9Be;->M:LX/9BZ;

    iget-object v3, v3, LX/9BZ;->c:LX/9Bd;

    if-ne v3, v0, :cond_1

    .line 1451995
    add-float v2, v7, v1

    iget-object v3, p0, LX/9Be;->M:LX/9BZ;

    iget-object v3, v3, LX/9BZ;->b:[F

    const/4 v5, 0x0

    aget v3, v3, v5

    sub-float/2addr v2, v3

    .line 1451996
    sub-float v3, v1, v2

    .line 1451997
    iget-object v1, p0, LX/9Be;->M:LX/9BZ;

    iget-object v1, v1, LX/9BZ;->b:[F

    const/4 v5, 0x1

    aget v1, v1, v5

    sub-float/2addr v1, v7

    .line 1451998
    :goto_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1451999
    invoke-virtual {p1, v3, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1452000
    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v1, v4, v1

    if-lez v1, :cond_0

    const/4 v1, 0x0

    cmpl-float v1, v2, v1

    if-nez v1, :cond_0

    .line 1452001
    invoke-direct {p0, p1, v0}, LX/9Be;->a(Landroid/graphics/Canvas;LX/9Bd;)V

    .line 1452002
    :cond_0
    const/4 v1, 0x0

    cmpl-float v1, v2, v1

    if-eqz v1, :cond_4

    .line 1452003
    iget-object v1, v0, LX/9BW;->d:Landroid/graphics/drawable/Drawable;

    move-object v1, v1

    .line 1452004
    instance-of v1, v1, LX/9Uc;

    if-eqz v1, :cond_3

    .line 1452005
    iget-object v1, v0, LX/9BW;->d:Landroid/graphics/drawable/Drawable;

    move-object v1, v1

    .line 1452006
    check-cast v1, LX/9Uc;

    const/high16 v5, 0x3f800000    # 1.0f

    const/high16 v10, 0x3f800000    # 1.0f

    iget-object v11, p0, LX/9Be;->L:LX/0wd;

    invoke-virtual {v11}, LX/0wd;->d()D

    move-result-wide v12

    double-to-float v11, v12

    sub-float/2addr v10, v11

    mul-float/2addr v10, v4

    invoke-virtual {p0}, LX/9BY;->a()Z

    move-result v4

    if-eqz v4, :cond_2

    sget-object v4, LX/9Ub;->UP:LX/9Ub;

    :goto_2
    invoke-interface {v1, v5, v10, v4}, LX/9Uc;->a(FFLX/9Ub;)V

    .line 1452007
    :goto_3
    iget-object v1, v0, LX/9BW;->d:Landroid/graphics/drawable/Drawable;

    move-object v1, v1

    .line 1452008
    const/high16 v4, 0x437f0000    # 255.0f

    const/high16 v5, 0x3f800000    # 1.0f

    iget-object v10, p0, LX/9Be;->L:LX/0wd;

    invoke-virtual {v10}, LX/0wd;->d()D

    move-result-wide v10

    double-to-float v10, v10

    sub-float/2addr v5, v10

    mul-float/2addr v4, v5

    float-to-int v4, v4

    invoke-virtual {v1, v4}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1452009
    :goto_4
    iget-object v1, v0, LX/9BW;->d:Landroid/graphics/drawable/Drawable;

    move-object v0, v1

    .line 1452010
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1452011
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1452012
    iget v0, p0, LX/9BY;->r:I

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v9, v1

    add-float/2addr v0, v1

    iget v1, p0, LX/9BY;->t:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    add-float v1, v0, v2

    const/4 v0, 0x0

    cmpl-float v0, v2, v0

    if-nez v0, :cond_9

    const/4 v0, 0x0

    :goto_5
    add-float/2addr v0, v1

    add-float v1, v3, v0

    .line 1452013
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto/16 :goto_0

    .line 1452014
    :cond_1
    const/high16 v3, 0x40000000    # 2.0f

    div-float v3, v9, v3

    add-float/2addr v3, v1

    .line 1452015
    iget v1, v0, LX/9Bd;->e:F

    move v1, v1

    .line 1452016
    sub-float/2addr v1, v7

    goto :goto_1

    .line 1452017
    :cond_2
    sget-object v4, LX/9Ub;->DOWN:LX/9Ub;

    goto :goto_2

    .line 1452018
    :cond_3
    const/high16 v1, 0x3f800000    # 1.0f

    iget-object v5, p0, LX/9Be;->L:LX/0wd;

    invoke-virtual {v5}, LX/0wd;->d()D

    move-result-wide v10

    double-to-float v5, v10

    sub-float/2addr v1, v5

    mul-float/2addr v1, v4

    const/high16 v5, 0x3f800000    # 1.0f

    iget-object v10, p0, LX/9Be;->L:LX/0wd;

    invoke-virtual {v10}, LX/0wd;->d()D

    move-result-wide v10

    double-to-float v10, v10

    sub-float/2addr v5, v10

    mul-float/2addr v4, v5

    invoke-virtual {p1, v1, v4, v7, v7}, Landroid/graphics/Canvas;->scale(FFFF)V

    goto :goto_3

    .line 1452019
    :cond_4
    iget-boolean v1, v0, LX/9BW;->g:Z

    move v1, v1

    .line 1452020
    if-eqz v1, :cond_5

    invoke-virtual {v0}, LX/9Bd;->j()F

    move-result v1

    move v4, v1

    .line 1452021
    :goto_6
    iget-object v1, v0, LX/9BW;->d:Landroid/graphics/drawable/Drawable;

    move-object v1, v1

    .line 1452022
    instance-of v1, v1, LX/9Uc;

    if-eqz v1, :cond_7

    .line 1452023
    iget-object v1, v0, LX/9BW;->d:Landroid/graphics/drawable/Drawable;

    move-object v1, v1

    .line 1452024
    check-cast v1, LX/9Uc;

    .line 1452025
    iget v5, v0, LX/9Bd;->f:F

    move v10, v5

    .line 1452026
    invoke-virtual {p0}, LX/9BY;->a()Z

    move-result v5

    if-eqz v5, :cond_6

    sget-object v5, LX/9Ub;->UP:LX/9Ub;

    :goto_7
    invoke-interface {v1, v10, v4, v5}, LX/9Uc;->a(FFLX/9Ub;)V

    .line 1452027
    :goto_8
    iget v1, p0, LX/9BY;->r:I

    int-to-float v1, v1

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v1, v4

    add-float/2addr v1, v3

    .line 1452028
    iput v1, v0, LX/9Bd;->h:F

    .line 1452029
    goto :goto_4

    .line 1452030
    :cond_5
    invoke-virtual {v0}, LX/9Bd;->i()F

    move-result v1

    move v4, v1

    goto :goto_6

    .line 1452031
    :cond_6
    sget-object v5, LX/9Ub;->DOWN:LX/9Ub;

    goto :goto_7

    .line 1452032
    :cond_7
    iget v1, v0, LX/9Bd;->f:F

    move v1, v1

    .line 1452033
    iget v5, v0, LX/9Bd;->f:F

    move v5, v5

    .line 1452034
    invoke-virtual {p1, v1, v5, v7, v7}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 1452035
    invoke-virtual {p0}, LX/9BY;->a()Z

    move-result v1

    if-eqz v1, :cond_8

    iget v1, p0, LX/9BY;->r:I

    int-to-float v1, v1

    .line 1452036
    iget v5, v0, LX/9Bd;->f:F

    move v5, v5

    .line 1452037
    mul-float/2addr v1, v5

    :goto_9
    invoke-virtual {p1, v4, v4, v7, v1}, Landroid/graphics/Canvas;->scale(FFFF)V

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    goto :goto_9

    .line 1452038
    :cond_9
    const/high16 v0, 0x40000000    # 2.0f

    div-float v0, v9, v0

    goto/16 :goto_5

    .line 1452039
    :cond_a
    return-void
.end method

.method public final onMeasure(II)V
    .locals 6

    .prologue
    .line 1451976
    iget v0, p0, LX/9BY;->x:I

    iget v1, p0, LX/9BY;->f:I

    add-int/2addr v0, v1

    iget v1, p0, LX/9BY;->k:I

    add-int/2addr v0, v1

    iget v1, p0, LX/9BY;->u:I

    add-int/2addr v0, v1

    .line 1451977
    iget v1, p0, LX/9BY;->w:I

    invoke-virtual {p0, v1, v0}, LX/9Be;->setMeasuredDimension(II)V

    .line 1451978
    iget v1, p0, LX/9BY;->w:I

    iget v2, p0, LX/9BY;->v:I

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    .line 1451979
    invoke-virtual {p0}, LX/9BY;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1451980
    iget-object v0, p0, LX/9BY;->E:Landroid/graphics/drawable/Drawable;

    move-object v0, v0

    .line 1451981
    iget v2, p0, LX/9BY;->u:I

    invoke-virtual {p0}, LX/9Be;->getMeasuredWidth()I

    move-result v3

    sub-int/2addr v3, v1

    iget v4, p0, LX/9BY;->u:I

    iget v5, p0, LX/9BY;->x:I

    add-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1451982
    :goto_0
    return-void

    .line 1451983
    :cond_0
    iget-object v2, p0, LX/9BY;->E:Landroid/graphics/drawable/Drawable;

    move-object v2, v2

    .line 1451984
    iget v3, p0, LX/9BY;->x:I

    sub-int v3, v0, v3

    iget v4, p0, LX/9BY;->u:I

    sub-int/2addr v3, v4

    invoke-virtual {p0}, LX/9Be;->getMeasuredWidth()I

    move-result v4

    sub-int/2addr v4, v1

    iget v5, p0, LX/9BY;->u:I

    sub-int/2addr v0, v5

    invoke-virtual {v2, v1, v3, v4, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto :goto_0
.end method

.method public setupReactionsImpl(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/1zt;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-wide/high16 v6, 0x4014000000000000L    # 5.0

    const/4 v1, 0x0

    .line 1451947
    iget-object v0, p0, LX/9Be;->N:LX/0Px;

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iget-object v2, p0, LX/9Be;->N:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ne v0, v2, :cond_1

    .line 1451948
    iget-object v0, p0, LX/9Be;->N:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_4

    .line 1451949
    iget-object v0, p0, LX/9Be;->N:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Bd;

    .line 1451950
    iget-object v4, v0, LX/9BW;->c:LX/1zt;

    move-object v0, v4

    .line 1451951
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    if-eq v0, v4, :cond_0

    .line 1451952
    const/4 v0, 0x1

    .line 1451953
    :goto_1
    if-nez v0, :cond_1

    .line 1451954
    :goto_2
    return-void

    .line 1451955
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1451956
    :cond_1
    iget-object v0, p0, LX/9Be;->C:LX/4lW;

    new-instance v2, LX/4lU;

    const-wide/high16 v4, 0x4034000000000000L    # 20.0

    invoke-static {v4, v5, v6, v7}, LX/0wT;->b(DD)LX/0wT;

    move-result-object v3

    const-wide/high16 v4, 0x403e000000000000L    # 30.0

    invoke-static {v4, v5, v6, v7}, LX/0wT;->b(DD)LX/0wT;

    move-result-object v4

    invoke-direct {v2, v3, v4}, LX/4lU;-><init>(LX/0wT;LX/0wT;)V

    .line 1451957
    new-instance v4, LX/4lV;

    invoke-static {v0}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v3

    check-cast v3, LX/0wW;

    invoke-direct {v4, v3, v2}, LX/4lV;-><init>(LX/0wW;LX/4lU;)V

    .line 1451958
    move-object v0, v4

    .line 1451959
    iput-object v0, p0, LX/9Be;->O:LX/4lV;

    .line 1451960
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1451961
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_2

    .line 1451962
    new-instance v5, LX/9Bd;

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1zt;

    invoke-direct {v5, p0, v2, v0}, LX/9Bd;-><init>(LX/9Be;ILX/1zt;)V

    invoke-virtual {v3, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1451963
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 1451964
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/9Be;->N:LX/0Px;

    .line 1451965
    iget-object v2, p0, LX/9Be;->O:LX/4lV;

    iget-object v0, p0, LX/9Be;->H:LX/0hL;

    invoke-virtual {v0}, LX/0hL;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/9Be;->N:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 1451966
    :goto_4
    iput v0, v2, LX/4lV;->d:I

    .line 1451967
    iget-object v1, v2, LX/4lV;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    iget v3, v2, LX/4lV;->d:I

    invoke-virtual {v1, v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0wd;

    .line 1451968
    if-nez v1, :cond_5

    .line 1451969
    :goto_5
    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_4

    :cond_4
    move v0, v1

    goto :goto_1

    .line 1451970
    :cond_5
    iget-object v1, v2, LX/4lV;->a:LX/0wW;

    .line 1451971
    iget-object v3, v1, LX/0wW;->a:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    move-object v4, v3

    .line 1451972
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v1, 0x0

    move v3, v1

    :goto_6
    if-ge v3, v5, :cond_6

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0wd;

    .line 1451973
    iget-object v6, v2, LX/4lV;->f:LX/0wT;

    invoke-virtual {v1, v6}, LX/0wd;->a(LX/0wT;)LX/0wd;

    .line 1451974
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_6

    .line 1451975
    :cond_6
    invoke-virtual {v2}, LX/4lV;->b()LX/0wd;

    move-result-object v1

    iget-object v3, v2, LX/4lV;->e:LX/0wT;

    invoke-virtual {v1, v3}, LX/0wd;->a(LX/0wT;)LX/0wd;

    goto :goto_5
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 5

    .prologue
    .line 1451937
    invoke-super {p0, p1}, LX/9BY;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    .line 1451938
    iget-object v0, p0, LX/9Be;->N:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_3

    .line 1451939
    iget-object v0, p0, LX/9Be;->N:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Bd;

    .line 1451940
    iget-object v4, v0, LX/9BW;->d:Landroid/graphics/drawable/Drawable;

    move-object v0, v4

    .line 1451941
    if-ne v0, p1, :cond_2

    .line 1451942
    const/4 v0, 0x1

    .line 1451943
    :goto_1
    move v0, v0

    .line 1451944
    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    .line 1451945
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_3
    move v0, v1

    .line 1451946
    goto :goto_1
.end method
