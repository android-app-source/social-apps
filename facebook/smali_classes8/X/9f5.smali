.class public final LX/9f5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V
    .locals 0

    .prologue
    .line 1520779
    iput-object p1, p0, LX/9f5;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x1

    .line 1520780
    const/4 v1, 0x4

    if-ne p2, v1, :cond_0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-eq v1, v0, :cond_2

    :cond_0
    move v0, v2

    .line 1520781
    :cond_1
    :goto_0
    return v0

    .line 1520782
    :cond_2
    iget-object v1, p0, LX/9f5;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-boolean v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->T:Z

    if-nez v1, :cond_1

    .line 1520783
    iget-object v1, p0, LX/9f5;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->L:LX/9fk;

    if-eqz v1, :cond_7

    .line 1520784
    iget-object v1, p0, LX/9f5;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->N:LX/9eb;

    invoke-interface {v1}, LX/9ea;->d()Z

    move-result v1

    .line 1520785
    :goto_1
    if-nez v1, :cond_6

    .line 1520786
    iget-object v1, p0, LX/9f5;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    .line 1520787
    iget-boolean v3, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->p:Z

    move v1, v3

    .line 1520788
    if-eqz v1, :cond_4

    iget-object v1, p0, LX/9f5;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-boolean v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->j:Z

    if-nez v1, :cond_3

    iget-object v1, p0, LX/9f5;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->N:LX/9eb;

    invoke-interface {v1}, LX/9eb;->l()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1520789
    :cond_3
    iget-object v1, p0, LX/9f5;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    invoke-static {v1}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->v$redex0(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V

    goto :goto_0

    .line 1520790
    :cond_4
    iget-object v1, p0, LX/9f5;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->m:LX/9el;

    if-eqz v1, :cond_5

    .line 1520791
    iget-object v1, p0, LX/9f5;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->N:LX/9eb;

    invoke-interface {v1}, LX/9ea;->b()V

    .line 1520792
    iget-object v1, p0, LX/9f5;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->m:LX/9el;

    iget-object v3, p0, LX/9f5;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v3, v3, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v3, v3, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;

    invoke-interface {v1, v3, v2}, LX/9el;->a(Lcom/facebook/photos/editgallery/EditGalleryFragmentManager$UsageParams;Z)V

    .line 1520793
    :cond_5
    iget-object v1, p0, LX/9f5;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    invoke-static {v1}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->t$redex0(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V

    goto :goto_0

    :cond_6
    move v0, v1

    .line 1520794
    goto :goto_0

    :cond_7
    move v1, v2

    goto :goto_1
.end method
