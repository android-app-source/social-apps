.class public final LX/AJF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/AJH;


# direct methods
.method public constructor <init>(LX/AJH;)V
    .locals 0

    .prologue
    .line 1660951
    iput-object p1, p0, LX/AJF;->a:LX/AJH;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0Px;Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/model/AudienceControlData;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 1660928
    iget-object v0, p0, LX/AJF;->a:LX/AJH;

    iget-object v0, v0, LX/AJH;->f:LX/AIu;

    .line 1660929
    iget-object v1, v0, LX/AIu;->b:LX/AIm;

    .line 1660930
    iput-boolean p2, v1, LX/AIm;->e:Z

    .line 1660931
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1660932
    :goto_0
    invoke-virtual {v1}, LX/1OM;->notifyDataSetChanged()V

    .line 1660933
    iget-object v0, p0, LX/AJF;->a:LX/AJH;

    iget-object v0, v0, LX/AJH;->e:LX/7gT;

    const/4 v1, 0x0

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    const/16 v3, 0x14

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-virtual {p1, v1, v2}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v1

    .line 1660934
    iget-boolean v2, v0, LX/7gT;->m:Z

    if-nez v2, :cond_0

    .line 1660935
    sget-object v2, LX/7gR;->PRIVATE_LISTS_SHOWN:LX/7gR;

    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v3

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/7gT;->a(LX/7gR;LX/0Px;)V

    .line 1660936
    const/4 v2, 0x1

    iput-boolean v2, v0, LX/7gT;->m:Z

    .line 1660937
    :cond_0
    iget-object v0, p0, LX/AJF;->a:LX/AJH;

    iget-object v0, v0, LX/AJH;->h:LX/0hg;

    const/4 v1, 0x2

    .line 1660938
    iget-boolean v2, v0, LX/0hg;->e:Z

    if-eqz v2, :cond_1

    .line 1660939
    iget-object v2, v0, LX/0hg;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0xbc0001

    invoke-interface {v2, v3, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 1660940
    const/4 v2, 0x0

    iput-boolean v2, v0, LX/0hg;->e:Z

    .line 1660941
    :cond_1
    return-void

    .line 1660942
    :cond_2
    iget-object v2, v1, LX/AIm;->c:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1660943
    iput-object p1, v1, LX/AIm;->c:LX/0Px;

    goto :goto_0

    .line 1660944
    :cond_3
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 1660945
    iget-object v2, v1, LX/AIm;->c:LX/0Px;

    invoke-virtual {v4, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1660946
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    if-ge v3, v0, :cond_5

    invoke-virtual {p1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/audience/model/AudienceControlData;

    .line 1660947
    iget-object p2, v1, LX/AIm;->c:LX/0Px;

    invoke-virtual {p2, v2}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_4

    .line 1660948
    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1660949
    :cond_4
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 1660950
    :cond_5
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    iput-object v2, v1, LX/AIm;->c:LX/0Px;

    goto :goto_0
.end method
