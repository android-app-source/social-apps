.class public LX/9e1;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public a:Lcom/facebook/photos/creativeediting/model/TextParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;

.field public c:Lcom/facebook/messaging/doodle/ColourIndicator;

.field public d:Lcom/facebook/messaging/doodle/ColourPicker;

.field public e:Landroid/widget/ImageView;

.field public f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public g:Landroid/view/View;

.field public h:Lcom/facebook/widget/CustomFrameLayout;

.field public i:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1518946
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1518947
    invoke-virtual {p0}, LX/9e1;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1518948
    const p1, 0x7f030f50

    invoke-virtual {v0, p1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 1518949
    const v0, 0x7f0d17ba

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomFrameLayout;

    iput-object v0, p0, LX/9e1;->h:Lcom/facebook/widget/CustomFrameLayout;

    .line 1518950
    const v0, 0x7f0d04fb

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;

    iput-object v0, p0, LX/9e1;->b:Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;

    .line 1518951
    const v0, 0x7f0d2509

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/doodle/ColourIndicator;

    iput-object v0, p0, LX/9e1;->c:Lcom/facebook/messaging/doodle/ColourIndicator;

    .line 1518952
    const v0, 0x7f0d250a

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/doodle/ColourPicker;

    iput-object v0, p0, LX/9e1;->d:Lcom/facebook/messaging/doodle/ColourPicker;

    .line 1518953
    const v0, 0x7f0d1b3f

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/9e1;->e:Landroid/widget/ImageView;

    .line 1518954
    const v0, 0x7f0d0d10

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/9e1;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1518955
    const v0, 0x7f0d2508

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/9e1;->g:Landroid/view/View;

    .line 1518956
    iget-object v0, p0, LX/9e1;->d:Lcom/facebook/messaging/doodle/ColourPicker;

    .line 1518957
    new-instance p1, LX/9e0;

    invoke-direct {p1, p0}, LX/9e0;-><init>(LX/9e1;)V

    move-object p1, p1

    .line 1518958
    iput-object p1, v0, Lcom/facebook/messaging/doodle/ColourPicker;->c:LX/8CF;

    .line 1518959
    new-instance v0, LX/9dy;

    invoke-direct {v0, p0}, LX/9dy;-><init>(LX/9e1;)V

    invoke-virtual {p0, v0}, LX/9e1;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1518960
    iget-object v0, p0, LX/9e1;->g:Landroid/view/View;

    new-instance p1, LX/9dz;

    invoke-direct {p1, p0}, LX/9dz;-><init>(LX/9e1;)V

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1518961
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 1518931
    const/4 v0, 0x0

    iput-object v0, p0, LX/9e1;->a:Lcom/facebook/photos/creativeediting/model/TextParams;

    .line 1518932
    iput-boolean v4, p0, LX/9e1;->i:Z

    .line 1518933
    iget-object v0, p0, LX/9e1;->b:Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1518934
    iget-object v0, p0, LX/9e1;->b:Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->setTextColor(I)V

    .line 1518935
    iget-object v0, p0, LX/9e1;->b:Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;

    invoke-virtual {p0}, LX/9e1;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0c03

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v4, v1}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->setTextSize(IF)V

    .line 1518936
    iget-object v0, p0, LX/9e1;->b:Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;

    invoke-virtual {v0, v3}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->setVisibility(I)V

    .line 1518937
    iget-object v0, p0, LX/9e1;->c:Lcom/facebook/messaging/doodle/ColourIndicator;

    invoke-virtual {v0, v3}, Lcom/facebook/messaging/doodle/ColourIndicator;->setVisibility(I)V

    .line 1518938
    iget-object v0, p0, LX/9e1;->c:Lcom/facebook/messaging/doodle/ColourIndicator;

    invoke-virtual {v0, v4}, Lcom/facebook/messaging/doodle/ColourIndicator;->setEnabled(Z)V

    .line 1518939
    iget-object v0, p0, LX/9e1;->d:Lcom/facebook/messaging/doodle/ColourPicker;

    invoke-virtual {v0, v3}, Lcom/facebook/messaging/doodle/ColourPicker;->setVisibility(I)V

    .line 1518940
    iget-object v0, p0, LX/9e1;->d:Lcom/facebook/messaging/doodle/ColourPicker;

    invoke-virtual {v0, v4}, Lcom/facebook/messaging/doodle/ColourPicker;->setEnabled(Z)V

    .line 1518941
    iget-object v0, p0, LX/9e1;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1518942
    iget-object v0, p0, LX/9e1;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1518943
    iget-object v0, p0, LX/9e1;->g:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1518944
    invoke-virtual {p0, v3}, LX/9e1;->setVisibility(I)V

    .line 1518945
    return-void
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1518930
    iget-object v0, p0, LX/9e1;->b:Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTextColor()I
    .locals 1

    .prologue
    .line 1518929
    iget-object v0, p0, LX/9e1;->b:Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->getCurrentTextColor()I

    move-result v0

    return v0
.end method

.method public getTextHeight()I
    .locals 1

    .prologue
    .line 1518962
    iget-object v0, p0, LX/9e1;->b:Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public getTextId()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1518928
    iget-object v0, p0, LX/9e1;->a:Lcom/facebook/photos/creativeediting/model/TextParams;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9e1;->a:Lcom/facebook/photos/creativeediting/model/TextParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/TextParams;->g()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTextWidth()I
    .locals 1

    .prologue
    .line 1518927
    iget-object v0, p0, LX/9e1;->b:Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->getMeasuredWidth()I

    move-result v0

    return v0
.end method

.method public setCallBack(LX/9de;)V
    .locals 1

    .prologue
    .line 1518924
    iget-object v0, p0, LX/9e1;->b:Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;

    .line 1518925
    iput-object p1, v0, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->g:LX/9de;

    .line 1518926
    return-void
.end method

.method public setTextParams(Lcom/facebook/photos/creativeediting/model/TextParams;)V
    .locals 2
    .param p1    # Lcom/facebook/photos/creativeediting/model/TextParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1518916
    iput-object p1, p0, LX/9e1;->a:Lcom/facebook/photos/creativeediting/model/TextParams;

    .line 1518917
    iget-object v0, p0, LX/9e1;->a:Lcom/facebook/photos/creativeediting/model/TextParams;

    if-eqz v0, :cond_0

    .line 1518918
    iget-object v0, p0, LX/9e1;->a:Lcom/facebook/photos/creativeediting/model/TextParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/TextParams;->m()I

    move-result v0

    .line 1518919
    iget-object p1, p0, LX/9e1;->b:Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;

    invoke-virtual {p1, v0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->setTextColor(I)V

    .line 1518920
    iget-object v0, p0, LX/9e1;->a:Lcom/facebook/photos/creativeediting/model/TextParams;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/TextParams;->l()Ljava/lang/String;

    move-result-object v0

    .line 1518921
    iget-object v1, p0, LX/9e1;->b:Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;

    invoke-virtual {v1, v0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1518922
    iget-object v1, p0, LX/9e1;->b:Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result p1

    invoke-virtual {v1, p1}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->setSelection(I)V

    .line 1518923
    :cond_0
    return-void
.end method
