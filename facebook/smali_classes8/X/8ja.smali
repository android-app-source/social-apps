.class public final LX/8ja;
.super LX/34X;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/34X",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/stickers/data/StickerAssetDownloader;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/data/StickerAssetDownloader;Landroid/net/Uri;LX/1uy;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 0

    .prologue
    .line 1392623
    iput-object p1, p0, LX/8ja;->a:Lcom/facebook/stickers/data/StickerAssetDownloader;

    invoke-direct {p0, p2, p3, p4}, LX/34X;-><init>(Landroid/net/Uri;LX/1uy;Lcom/facebook/common/callercontext/CallerContext;)V

    return-void
.end method


# virtual methods
.method public final a()Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 4

    .prologue
    .line 1392624
    invoke-super {p0}, LX/34X;->a()Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    .line 1392625
    new-instance v1, LX/1uv;

    iget-object v2, p0, LX/8ja;->a:Lcom/facebook/stickers/data/StickerAssetDownloader;

    iget-object v2, v2, Lcom/facebook/stickers/data/StickerAssetDownloader;->e:LX/00I;

    invoke-interface {v2}, LX/00I;->c()Ljava/lang/String;

    move-result-object v2

    const-string v3, "sticker_pack_download"

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/1uv;-><init>(Ljava/lang/String;LX/0Px;)V

    .line 1392626
    const-string v2, "Referer"

    invoke-virtual {v1}, LX/1uv;->a()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 1392627
    return-object v0
.end method
