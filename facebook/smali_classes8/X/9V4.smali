.class public LX/9V4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1bh;


# instance fields
.field private a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1498918
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1498919
    iput-object p1, p0, LX/9V4;->a:Ljava/lang/String;

    .line 1498920
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1498921
    iget-object v0, p0, LX/9V4;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Landroid/net/Uri;)Z
    .locals 1

    .prologue
    .line 1498922
    iget-object v0, p0, LX/9V4;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1498923
    instance-of v0, p1, LX/9V4;

    if-nez v0, :cond_0

    .line 1498924
    const/4 v0, 0x0

    .line 1498925
    :goto_0
    return v0

    .line 1498926
    :cond_0
    check-cast p1, LX/9V4;

    .line 1498927
    iget-object v0, p0, LX/9V4;->a:Ljava/lang/String;

    iget-object v1, p1, LX/9V4;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1498928
    iget-object v0, p0, LX/9V4;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1498929
    iget-object v0, p0, LX/9V4;->a:Ljava/lang/String;

    return-object v0
.end method
