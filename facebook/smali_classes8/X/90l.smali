.class public final LX/90l;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;)V
    .locals 0

    .prologue
    .line 1429404
    iput-object p1, p0, LX/90l;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 5

    .prologue
    .line 1429406
    iget-object v0, p0, LX/90l;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    .line 1429407
    iget-object v1, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->s:LX/90G;

    invoke-virtual {v1}, LX/90G;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1429408
    iget-object v1, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->j:LX/919;

    iget-object v2, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->t:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1429409
    iget-object v3, v2, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v2, v3

    .line 1429410
    iget-object v3, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->r:Ljava/lang/String;

    .line 1429411
    const-string v4, "activity_picker_first_keystroke"

    invoke-static {v4, v2}, LX/919;->d(Ljava/lang/String;Ljava/lang/String;)LX/8yQ;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/8yQ;->a(Ljava/lang/String;)LX/8yQ;

    move-result-object v4

    .line 1429412
    iget-object p0, v4, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v4, p0

    .line 1429413
    iget-object p0, v1, LX/919;->a:LX/0Zb;

    invoke-interface {p0, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1429414
    :cond_0
    iget-object v1, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->s:LX/90G;

    invoke-virtual {v1}, LX/90G;->a()V

    .line 1429415
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 1429416
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1429417
    const/4 v1, 0x0

    .line 1429418
    :cond_1
    iput-object v1, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->y:Ljava/lang/String;

    .line 1429419
    iget-object v1, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->y:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;->a$redex0(Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;Ljava/lang/String;)V

    .line 1429420
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1429421
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1429405
    return-void
.end method
