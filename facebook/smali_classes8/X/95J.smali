.class public LX/95J;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;

.field private static volatile c:LX/95J;


# instance fields
.field private final b:LX/2Iv;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1436723
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "local_contact_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "contact_hash"

    aput-object v2, v0, v1

    sput-object v0, LX/95J;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/2Iv;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1436724
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1436725
    iput-object p1, p0, LX/95J;->b:LX/2Iv;

    .line 1436726
    return-void
.end method

.method public static a(LX/0QB;)LX/95J;
    .locals 4

    .prologue
    .line 1436727
    sget-object v0, LX/95J;->c:LX/95J;

    if-nez v0, :cond_1

    .line 1436728
    const-class v1, LX/95J;

    monitor-enter v1

    .line 1436729
    :try_start_0
    sget-object v0, LX/95J;->c:LX/95J;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1436730
    if-eqz v2, :cond_0

    .line 1436731
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1436732
    new-instance p0, LX/95J;

    invoke-static {v0}, LX/2Iv;->a(LX/0QB;)LX/2Iv;

    move-result-object v3

    check-cast v3, LX/2Iv;

    invoke-direct {p0, v3}, LX/95J;-><init>(LX/2Iv;)V

    .line 1436733
    move-object v0, p0

    .line 1436734
    sput-object v0, LX/95J;->c:LX/95J;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1436735
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1436736
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1436737
    :cond_1
    sget-object v0, LX/95J;->c:LX/95J;

    return-object v0

    .line 1436738
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1436739
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/2Ta;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/2Ta",
            "<",
            "LX/95A;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1436740
    iget-object v0, p0, LX/95J;->b:LX/2Iv;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "phone_address_book_snapshot"

    sget-object v2, LX/95J;->a:[Ljava/lang/String;

    const-string v7, "local_contact_id"

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1436741
    new-instance v1, LX/95I;

    invoke-direct {v1, v0}, LX/95I;-><init>(Landroid/database/Cursor;)V

    return-object v1
.end method
