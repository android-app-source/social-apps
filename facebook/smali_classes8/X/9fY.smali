.class public final enum LX/9fY;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9fY;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9fY;

.field public static final enum TEXT_EDIT:LX/9fY;

.field public static final enum TEXT_ENTRY:LX/9fY;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1522121
    new-instance v0, LX/9fY;

    const-string v1, "TEXT_ENTRY"

    invoke-direct {v0, v1, v2}, LX/9fY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9fY;->TEXT_ENTRY:LX/9fY;

    .line 1522122
    new-instance v0, LX/9fY;

    const-string v1, "TEXT_EDIT"

    invoke-direct {v0, v1, v3}, LX/9fY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9fY;->TEXT_EDIT:LX/9fY;

    .line 1522123
    const/4 v0, 0x2

    new-array v0, v0, [LX/9fY;

    sget-object v1, LX/9fY;->TEXT_ENTRY:LX/9fY;

    aput-object v1, v0, v2

    sget-object v1, LX/9fY;->TEXT_EDIT:LX/9fY;

    aput-object v1, v0, v3

    sput-object v0, LX/9fY;->$VALUES:[LX/9fY;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1522124
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9fY;
    .locals 1

    .prologue
    .line 1522125
    const-class v0, LX/9fY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9fY;

    return-object v0
.end method

.method public static values()[LX/9fY;
    .locals 1

    .prologue
    .line 1522126
    sget-object v0, LX/9fY;->$VALUES:[LX/9fY;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9fY;

    return-object v0
.end method
