.class public LX/9iB;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1527354
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/9iB;
    .locals 1

    .prologue
    .line 1527355
    new-instance v0, LX/9iB;

    invoke-direct {v0}, LX/9iB;-><init>()V

    .line 1527356
    move-object v0, v0

    .line 1527357
    return-object v0
.end method

.method public static b(LX/5kD;)Ljava/lang/String;
    .locals 2
    .param p0    # LX/5kD;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1527358
    if-nez p0, :cond_1

    .line 1527359
    :cond_0
    :goto_0
    return-object v0

    .line 1527360
    :cond_1
    invoke-interface {p0}, LX/5kD;->ae()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel$PrivacyScopeModel;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-interface {p0}, LX/5kD;->ae()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel$PrivacyScopeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel$PrivacyScopeModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel$PrivacyScopeModel$IconImageModel;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-interface {p0}, LX/5kD;->ae()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel$PrivacyScopeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel$PrivacyScopeModel;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1527361
    invoke-interface {p0}, LX/5kD;->ae()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel$PrivacyScopeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel$PrivacyScopeModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel$PrivacyScopeModel$IconImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel$PrivacyScopeModel$IconImageModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1527362
    :cond_2
    invoke-interface {p0}, LX/5kD;->A()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, LX/5kD;->A()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;->e()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel$PrivacyScopeModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, LX/5kD;->A()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;->e()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel$PrivacyScopeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel$PrivacyScopeModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel$PrivacyScopeModel$IconImageModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, LX/5kD;->A()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;->e()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel$PrivacyScopeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel$PrivacyScopeModel;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1527363
    invoke-interface {p0}, LX/5kD;->A()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;->e()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel$PrivacyScopeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel$PrivacyScopeModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel$PrivacyScopeModel$IconImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel$PrivacyScopeModel$IconImageModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
