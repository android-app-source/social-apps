.class public LX/990;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;",
        "LX/1pN;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1446613
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1446614
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 1446615
    check-cast p1, Ljava/util/Map;

    .line 1446616
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 1446617
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "queries"

    const-string v0, "queries"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1446618
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "JSON"

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1446619
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "xhrEncoding"

    const-string v3, "gzip"

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1446620
    new-instance v2, LX/14O;

    invoke-direct {v2}, LX/14O;-><init>()V

    const-string v0, "entryName"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1446621
    iput-object v0, v2, LX/14O;->b:Ljava/lang/String;

    .line 1446622
    move-object v0, v2

    .line 1446623
    const-string v2, "GET"

    .line 1446624
    iput-object v2, v0, LX/14O;->c:Ljava/lang/String;

    .line 1446625
    move-object v0, v0

    .line 1446626
    const-string v2, "graphqlbatch?locale=user"

    .line 1446627
    iput-object v2, v0, LX/14O;->d:Ljava/lang/String;

    .line 1446628
    move-object v0, v0

    .line 1446629
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 1446630
    move-object v0, v0

    .line 1446631
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1446632
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1446633
    move-object v0, v0

    .line 1446634
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1446635
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1446636
    return-object p2
.end method
