.class public final LX/A5Q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/speech/SpeechRecognitionClient$ClientRecognitionListener;


# instance fields
.field public final synthetic a:Lcom/facebook/speech/SpeechRecognition;


# direct methods
.method public constructor <init>(Lcom/facebook/speech/SpeechRecognition;)V
    .locals 0

    .prologue
    .line 1621161
    iput-object p1, p0, LX/A5Q;->a:Lcom/facebook/speech/SpeechRecognition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onEndOfSpeech()V
    .locals 2

    .prologue
    .line 1621162
    iget-object v0, p0, LX/A5Q;->a:Lcom/facebook/speech/SpeechRecognition;

    const-string v1, "end_of_speech"

    invoke-static {v0, v1}, Lcom/facebook/speech/SpeechRecognition;->c(Lcom/facebook/speech/SpeechRecognition;Ljava/lang/String;)V

    .line 1621163
    iget-object v0, p0, LX/A5Q;->a:Lcom/facebook/speech/SpeechRecognition;

    invoke-static {v0}, Lcom/facebook/speech/SpeechRecognition;->g(Lcom/facebook/speech/SpeechRecognition;)V

    .line 1621164
    iget-object v0, p0, LX/A5Q;->a:Lcom/facebook/speech/SpeechRecognition;

    iget-object v0, v0, Lcom/facebook/speech/SpeechRecognition;->o:Lcom/facebook/speech/Result;

    invoke-virtual {p0, v0}, LX/A5Q;->onResult(Lcom/facebook/speech/Result;)V

    .line 1621165
    invoke-virtual {p0}, LX/A5Q;->onRecognitionEnd()V

    .line 1621166
    iget-object v0, p0, LX/A5Q;->a:Lcom/facebook/speech/SpeechRecognition;

    const/4 v1, 0x1

    .line 1621167
    iput-boolean v1, v0, Lcom/facebook/speech/SpeechRecognition;->p:Z

    .line 1621168
    return-void
.end method

.method public final declared-synchronized onError(I)V
    .locals 2

    .prologue
    .line 1621169
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/A5Q;->a:Lcom/facebook/speech/SpeechRecognition;

    const-string v1, "shortwave_error"

    invoke-static {v0, v1, p1}, Lcom/facebook/speech/SpeechRecognition;->a$redex0(Lcom/facebook/speech/SpeechRecognition;Ljava/lang/String;I)V

    .line 1621170
    iget-object v0, p0, LX/A5Q;->a:Lcom/facebook/speech/SpeechRecognition;

    invoke-static {v0}, Lcom/facebook/speech/SpeechRecognition;->f(Lcom/facebook/speech/SpeechRecognition;)V

    .line 1621171
    iget-object v0, p0, LX/A5Q;->a:Lcom/facebook/speech/SpeechRecognition;

    invoke-virtual {v0}, Lcom/facebook/speech/SpeechRecognition;->b()V

    .line 1621172
    iget-object v0, p0, LX/A5Q;->a:Lcom/facebook/speech/SpeechRecognition;

    iget-object v0, v0, Lcom/facebook/speech/SpeechRecognition;->a:LX/0Sh;

    new-instance v1, Lcom/facebook/speech/SpeechRecognition$4$3;

    invoke-direct {v1, p0, p1}, Lcom/facebook/speech/SpeechRecognition$4$3;-><init>(LX/A5Q;I)V

    invoke-virtual {v0, v1}, LX/0Sh;->b(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1621173
    monitor-exit p0

    return-void

    .line 1621174
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final onRecognitionEnd()V
    .locals 2

    .prologue
    .line 1621175
    iget-object v0, p0, LX/A5Q;->a:Lcom/facebook/speech/SpeechRecognition;

    const-string v1, "recognition_end"

    invoke-static {v0, v1}, Lcom/facebook/speech/SpeechRecognition;->c(Lcom/facebook/speech/SpeechRecognition;Ljava/lang/String;)V

    .line 1621176
    iget-object v0, p0, LX/A5Q;->a:Lcom/facebook/speech/SpeechRecognition;

    invoke-static {v0}, Lcom/facebook/speech/SpeechRecognition;->f(Lcom/facebook/speech/SpeechRecognition;)V

    .line 1621177
    iget-object v0, p0, LX/A5Q;->a:Lcom/facebook/speech/SpeechRecognition;

    iget-boolean v0, v0, Lcom/facebook/speech/SpeechRecognition;->p:Z

    if-eqz v0, :cond_0

    .line 1621178
    :goto_0
    return-void

    .line 1621179
    :cond_0
    iget-object v0, p0, LX/A5Q;->a:Lcom/facebook/speech/SpeechRecognition;

    iget-object v0, v0, Lcom/facebook/speech/SpeechRecognition;->a:LX/0Sh;

    new-instance v1, Lcom/facebook/speech/SpeechRecognition$4$2;

    invoke-direct {v1, p0}, Lcom/facebook/speech/SpeechRecognition$4$2;-><init>(LX/A5Q;)V

    invoke-virtual {v0, v1}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final onResult(Lcom/facebook/speech/Result;)V
    .locals 3

    .prologue
    .line 1621180
    if-nez p1, :cond_0

    .line 1621181
    iget-object v0, p0, LX/A5Q;->a:Lcom/facebook/speech/SpeechRecognition;

    const-string v1, "receive_result"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/facebook/speech/SpeechRecognition;->a$redex0(Lcom/facebook/speech/SpeechRecognition;Ljava/lang/String;Ljava/lang/String;)V

    .line 1621182
    :goto_0
    iget-object v0, p0, LX/A5Q;->a:Lcom/facebook/speech/SpeechRecognition;

    iget-boolean v0, v0, Lcom/facebook/speech/SpeechRecognition;->p:Z

    if-eqz v0, :cond_1

    .line 1621183
    :goto_1
    return-void

    .line 1621184
    :cond_0
    iget-object v0, p0, LX/A5Q;->a:Lcom/facebook/speech/SpeechRecognition;

    const-string v1, "receive_result"

    invoke-virtual {p1}, Lcom/facebook/speech/Result;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/speech/SpeechRecognition;->a$redex0(Lcom/facebook/speech/SpeechRecognition;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1621185
    :cond_1
    iget-object v0, p0, LX/A5Q;->a:Lcom/facebook/speech/SpeechRecognition;

    iget-object v0, v0, Lcom/facebook/speech/SpeechRecognition;->a:LX/0Sh;

    new-instance v1, Lcom/facebook/speech/SpeechRecognition$4$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/speech/SpeechRecognition$4$1;-><init>(LX/A5Q;Lcom/facebook/speech/Result;)V

    invoke-virtual {v0, v1}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    goto :goto_1
.end method
