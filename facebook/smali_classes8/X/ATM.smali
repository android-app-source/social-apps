.class public final LX/ATM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9bz;


# instance fields
.field public final synthetic a:LX/ATO;


# direct methods
.method public constructor <init>(LX/ATO;)V
    .locals 0

    .prologue
    .line 1675503
    iput-object p1, p0, LX/ATM;->a:LX/ATO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private b(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;
    .locals 2

    .prologue
    .line 1675504
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1675505
    iget-object v0, p0, LX/ATM;->a:LX/ATO;

    iget-object v0, v0, LX/ATO;->r:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1675506
    iget-object v0, p0, LX/ATM;->a:LX/ATO;

    iget-object v0, v0, LX/ATO;->r:Ljava/util/Map;

    invoke-static {}, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->newBuilder()LX/9cC;

    move-result-object v1

    invoke-virtual {v1}, LX/9cC;->a()Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1675507
    :cond_0
    iget-object v0, p0, LX/ATM;->a:LX/ATO;

    iget-object v0, v0, LX/ATO;->r:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;

    return-object v0

    .line 1675508
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1675509
    invoke-direct {p0, p1}, LX/ATM;->b(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;

    move-result-object v0

    .line 1675510
    iget-object v1, p0, LX/ATM;->a:LX/ATO;

    iget-object v1, v1, LX/ATO;->r:Ljava/util/Map;

    invoke-static {v0}, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->a(Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;)LX/9cC;

    move-result-object v2

    iget v0, v0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->d:I

    add-int/lit8 v0, v0, 0x1

    .line 1675511
    iput v0, v2, LX/9cC;->d:I

    .line 1675512
    move-object v0, v2

    .line 1675513
    invoke-virtual {v0}, LX/9cC;->a()Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;

    move-result-object v0

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1675514
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 1675515
    invoke-direct {p0, p1}, LX/ATM;->b(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;

    move-result-object v0

    .line 1675516
    iget-object v1, p0, LX/ATM;->a:LX/ATO;

    iget-object v1, v1, LX/ATO;->r:Ljava/util/Map;

    invoke-static {v0}, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->a(Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;)LX/9cC;

    move-result-object v2

    iget v0, v0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->f:I

    add-int/2addr v0, p2

    .line 1675517
    iput v0, v2, LX/9cC;->f:I

    .line 1675518
    move-object v0, v2

    .line 1675519
    invoke-virtual {v0}, LX/9cC;->a()Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;

    move-result-object v0

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1675520
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/photos/creativeediting/model/SwipeableParams;I)V
    .locals 3

    .prologue
    .line 1675521
    invoke-direct {p0, p1}, LX/ATM;->b(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;

    move-result-object v0

    .line 1675522
    iget-object v1, p0, LX/ATM;->a:LX/ATO;

    iget-object v1, v1, LX/ATO;->r:Ljava/util/Map;

    invoke-static {v0}, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->a(Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;)LX/9cC;

    move-result-object v2

    iget v0, v0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->i:I

    add-int/lit8 v0, v0, 0x1

    .line 1675523
    iput v0, v2, LX/9cC;->i:I

    .line 1675524
    move-object v0, v2

    .line 1675525
    iget-object v2, p2, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1675526
    iput-object v2, v0, LX/9cC;->n:Ljava/lang/String;

    .line 1675527
    move-object v0, v0

    .line 1675528
    invoke-virtual {v0}, LX/9cC;->a()Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;

    move-result-object v0

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1675529
    return-void
.end method

.method public final b(Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 1675530
    invoke-direct {p0, p1}, LX/ATM;->b(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;

    move-result-object v0

    .line 1675531
    iget-object v1, p0, LX/ATM;->a:LX/ATO;

    iget-object v1, v1, LX/ATO;->r:Ljava/util/Map;

    invoke-static {v0}, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->a(Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;)LX/9cC;

    move-result-object v2

    iget v0, v0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->e:I

    add-int/2addr v0, p2

    .line 1675532
    iput v0, v2, LX/9cC;->e:I

    .line 1675533
    move-object v0, v2

    .line 1675534
    invoke-virtual {v0}, LX/9cC;->a()Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;

    move-result-object v0

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1675535
    return-void
.end method

.method public final c(Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 1675536
    invoke-direct {p0, p1}, LX/ATM;->b(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;

    move-result-object v0

    .line 1675537
    iget-object v1, p0, LX/ATM;->a:LX/ATO;

    iget-object v1, v1, LX/ATO;->r:Ljava/util/Map;

    invoke-static {v0}, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->a(Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;)LX/9cC;

    move-result-object v2

    iget v0, v0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->g:I

    add-int/2addr v0, p2

    .line 1675538
    iput v0, v2, LX/9cC;->g:I

    .line 1675539
    move-object v0, v2

    .line 1675540
    invoke-virtual {v0}, LX/9cC;->a()Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;

    move-result-object v0

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1675541
    return-void
.end method

.method public final d(Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 1675542
    invoke-direct {p0, p1}, LX/ATM;->b(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;

    move-result-object v0

    .line 1675543
    iget-object v1, p0, LX/ATM;->a:LX/ATO;

    iget-object v1, v1, LX/ATO;->r:Ljava/util/Map;

    invoke-static {v0}, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->a(Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;)LX/9cC;

    move-result-object v2

    iget v0, v0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->h:I

    add-int/2addr v0, p2

    .line 1675544
    iput v0, v2, LX/9cC;->h:I

    .line 1675545
    move-object v0, v2

    .line 1675546
    invoke-virtual {v0}, LX/9cC;->a()Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;

    move-result-object v0

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1675547
    return-void
.end method

.method public final e(Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 1675548
    invoke-direct {p0, p1}, LX/ATM;->b(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;

    move-result-object v0

    .line 1675549
    iget-object v1, p0, LX/ATM;->a:LX/ATO;

    iget-object v1, v1, LX/ATO;->r:Ljava/util/Map;

    invoke-static {v0}, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->a(Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;)LX/9cC;

    move-result-object v2

    iget v0, v0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->m:I

    add-int/2addr v0, p2

    rem-int/lit16 v0, v0, 0x168

    .line 1675550
    iput v0, v2, LX/9cC;->m:I

    .line 1675551
    move-object v0, v2

    .line 1675552
    invoke-virtual {v0}, LX/9cC;->a()Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;

    move-result-object v0

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1675553
    return-void
.end method
