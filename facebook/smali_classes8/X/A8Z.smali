.class public LX/A8Z;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "LX/A8X;",
        ">",
        "LX/1OM",
        "<",
        "LX/A8Y;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/A8S;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/A8S",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1627811
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/A8Z;-><init>(LX/0Px;LX/A8X;)V

    .line 1627812
    return-void
.end method

.method private constructor <init>(LX/0Px;LX/A8X;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<TT;>;TT;)V"
        }
    .end annotation

    .prologue
    .line 1627809
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/A8Z;-><init>(LX/0Px;LX/A8X;Z)V

    .line 1627810
    return-void
.end method

.method private constructor <init>(LX/0Px;LX/A8X;Z)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<TT;>;TT;Z)V"
        }
    .end annotation

    .prologue
    .line 1627801
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1627802
    if-eqz p3, :cond_0

    .line 1627803
    new-instance v0, LX/A8S;

    const/4 v1, 0x1

    invoke-direct {v0, p1, p2, v1}, LX/A8S;-><init>(LX/0Px;LX/90h;Z)V

    move-object v0, v0

    .line 1627804
    :goto_0
    iput-object v0, p0, LX/A8Z;->a:LX/A8S;

    .line 1627805
    iget-object v0, p0, LX/A8Z;->a:LX/A8S;

    new-instance v1, LX/A8V;

    invoke-direct {v1, p0}, LX/A8V;-><init>(LX/A8Z;)V

    .line 1627806
    iput-object v1, v0, LX/A8S;->c:LX/A8O;

    .line 1627807
    return-void

    .line 1627808
    :cond_0
    new-instance v0, LX/A8S;

    invoke-direct {v0, p1, p2}, LX/A8S;-><init>(LX/0Px;LX/90h;)V

    goto :goto_0
.end method

.method public constructor <init>(LX/0Px;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<TT;>;Z)V"
        }
    .end annotation

    .prologue
    .line 1627799
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, LX/A8Z;-><init>(LX/0Px;LX/A8X;Z)V

    .line 1627800
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 2

    .prologue
    .line 1627813
    iget-object v0, p0, LX/A8Z;->a:LX/A8S;

    invoke-virtual {v0, p2}, LX/A8S;->a(I)LX/A8R;

    move-result-object v1

    .line 1627814
    iget-object v0, v1, LX/A8R;->b:LX/90h;

    check-cast v0, LX/A8X;

    iget v1, v1, LX/A8R;->a:I

    invoke-virtual {v0, p1, v1}, LX/A8X;->c(Landroid/view/ViewGroup;I)LX/A8Y;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1a1;)V
    .locals 1

    .prologue
    .line 1627796
    check-cast p1, LX/A8Y;

    .line 1627797
    iget-object v0, p1, LX/A8Y;->l:LX/A8X;

    invoke-virtual {v0, p1}, LX/1OM;->a(LX/1a1;)V

    .line 1627798
    return-void
.end method

.method public final a(LX/1a1;I)V
    .locals 2

    .prologue
    .line 1627792
    check-cast p1, LX/A8Y;

    .line 1627793
    invoke-virtual {p0, p2}, LX/A8Z;->e(I)LX/A8P;

    move-result-object v1

    .line 1627794
    iget-object v0, v1, LX/A8P;->b:LX/90h;

    check-cast v0, LX/A8X;

    iget v1, v1, LX/A8P;->a:I

    invoke-virtual {v0, p1, v1}, LX/A8X;->a(LX/A8Y;I)V

    .line 1627795
    return-void
.end method

.method public final e(I)LX/A8P;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/A8S",
            "<TT;>.Section",
            "LookUpResult",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1627791
    iget-object v0, p0, LX/A8Z;->a:LX/A8S;

    invoke-virtual {v0, p1}, LX/A8S;->b(I)LX/A8P;

    move-result-object v0

    return-object v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1627790
    iget-object v0, p0, LX/A8Z;->a:LX/A8S;

    invoke-virtual {v0, p1}, LX/A8S;->c(I)I

    move-result v0

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1627789
    iget-object v0, p0, LX/A8Z;->a:LX/A8S;

    invoke-virtual {v0}, LX/A8S;->a()I

    move-result v0

    return v0
.end method
