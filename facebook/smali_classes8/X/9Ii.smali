.class public LX/9Ii;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1463526
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/res/Resources;LX/21y;LX/5Gs;I)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1463527
    sget-object v0, LX/21y;->RANKED_ORDER:LX/21y;

    invoke-virtual {p1, v0}, LX/21y;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 1463528
    sget-object v1, LX/5Gs;->LOAD_AFTER:LX/5Gs;

    invoke-virtual {p2, v1}, LX/5Gs;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 1463529
    const/4 v2, -0x1

    if-ne p3, v2, :cond_1

    .line 1463530
    if-eq v0, v1, :cond_0

    const v0, 0x7f080fc3

    .line 1463531
    :goto_0
    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1463532
    :goto_1
    return-object v0

    .line 1463533
    :cond_0
    const v0, 0x7f080fc5

    goto :goto_0

    .line 1463534
    :cond_1
    if-eq v0, v1, :cond_2

    const v0, 0x7f0f005e

    .line 1463535
    :goto_2
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, p3, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1463536
    :cond_2
    const v0, 0x7f0f005f

    goto :goto_2
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLFeedback;LX/5Gs;)Z
    .locals 1

    .prologue
    .line 1463537
    sget-object v0, LX/5Gs;->LOAD_BEFORE:LX/5Gs;

    invoke-virtual {p1, v0}, LX/5Gs;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1463538
    invoke-static {p0}, LX/16z;->i(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v0

    .line 1463539
    :goto_0
    return v0

    .line 1463540
    :cond_0
    invoke-static {p0}, LX/16z;->g(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {p0}, LX/16z;->g(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 1463541
    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
