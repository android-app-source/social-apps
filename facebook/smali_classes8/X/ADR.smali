.class public LX/ADR;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/ADR;


# instance fields
.field private final a:LX/ADQ;


# direct methods
.method public constructor <init>(LX/ADQ;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1644572
    invoke-direct {p0}, LX/398;-><init>()V

    .line 1644573
    iput-object p1, p0, LX/ADR;->a:LX/ADQ;

    .line 1644574
    const-string v0, "{method}/?account={account}&ref={ref}&adgroup={adgroup}"

    invoke-static {v0}, LX/ADR;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/ADR;->a:LX/ADQ;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 1644575
    const-string v0, "{method}/?account={account}&ref={ref}&campaign={campaign}"

    invoke-static {v0}, LX/ADR;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/ADR;->a:LX/ADQ;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 1644576
    const-string v0, "{method}/?account={account}&ref={ref}"

    invoke-static {v0}, LX/ADR;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/ADR;->a:LX/ADQ;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 1644577
    return-void
.end method

.method public static a(LX/0QB;)LX/ADR;
    .locals 5

    .prologue
    .line 1644578
    sget-object v0, LX/ADR;->b:LX/ADR;

    if-nez v0, :cond_1

    .line 1644579
    const-class v1, LX/ADR;

    monitor-enter v1

    .line 1644580
    :try_start_0
    sget-object v0, LX/ADR;->b:LX/ADR;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1644581
    if-eqz v2, :cond_0

    .line 1644582
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1644583
    new-instance v4, LX/ADR;

    .line 1644584
    new-instance p0, LX/ADQ;

    invoke-static {v0}, LX/2lA;->a(LX/0QB;)LX/2lA;

    move-result-object v3

    check-cast v3, LX/2lA;

    invoke-direct {p0, v3}, LX/ADQ;-><init>(LX/2lA;)V

    .line 1644585
    move-object v3, p0

    .line 1644586
    check-cast v3, LX/ADQ;

    invoke-direct {v4, v3}, LX/ADR;-><init>(LX/ADQ;)V

    .line 1644587
    move-object v0, v4

    .line 1644588
    sput-object v0, LX/ADR;->b:LX/ADR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1644589
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1644590
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1644591
    :cond_1
    sget-object v0, LX/ADR;->b:LX/ADR;

    return-object v0

    .line 1644592
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1644593
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1644594
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "fb-ama://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1644595
    const/4 v0, 0x1

    return v0
.end method
