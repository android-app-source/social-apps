.class public final LX/9Fe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5OO;


# instance fields
.field public final synthetic a:LX/9FA;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLComment;

.field public final synthetic c:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public final synthetic d:Lcom/facebook/feedback/ui/rows/CommentFailedStatusPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/rows/CommentFailedStatusPartDefinition;LX/9FA;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 0

    .prologue
    .line 1458788
    iput-object p1, p0, LX/9Fe;->d:Lcom/facebook/feedback/ui/rows/CommentFailedStatusPartDefinition;

    iput-object p2, p0, LX/9Fe;->a:LX/9FA;

    iput-object p3, p0, LX/9Fe;->b:Lcom/facebook/graphql/model/GraphQLComment;

    iput-object p4, p0, LX/9Fe;->c:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 1458789
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 1458790
    const v1, 0x7f0d3264

    if-ne v0, v1, :cond_1

    .line 1458791
    iget-object v0, p0, LX/9Fe;->a:LX/9FA;

    iget-object v1, p0, LX/9Fe;->b:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v2, p0, LX/9Fe;->c:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0, v1, v2}, LX/9FA;->c(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1458792
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 1458793
    :cond_1
    const v1, 0x7f0d3265

    if-ne v0, v1, :cond_0

    .line 1458794
    iget-object v0, p0, LX/9Fe;->a:LX/9FA;

    iget-object v1, p0, LX/9Fe;->b:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v2, p0, LX/9Fe;->c:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0, v1, v2}, LX/9FA;->d(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    goto :goto_0
.end method
