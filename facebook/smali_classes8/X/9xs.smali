.class public final LX/9xs;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1593552
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1593553
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1593554
    :goto_0
    return v1

    .line 1593555
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1593556
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1593557
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1593558
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1593559
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1593560
    const-string v4, "icon_image"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1593561
    invoke-static {p0, p1}, LX/5tK;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1593562
    :cond_2
    const-string v4, "icon_sizing"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1593563
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLImageSizingStyle;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    goto :goto_1

    .line 1593564
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1593565
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1593566
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1593567
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1593568
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1593569
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1593570
    if-eqz v0, :cond_0

    .line 1593571
    const-string v1, "icon_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1593572
    invoke-static {p0, v0, p2}, LX/5tK;->a(LX/15i;ILX/0nX;)V

    .line 1593573
    :cond_0
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1593574
    if-eqz v0, :cond_1

    .line 1593575
    const-string v0, "icon_sizing"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1593576
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1593577
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1593578
    return-void
.end method
