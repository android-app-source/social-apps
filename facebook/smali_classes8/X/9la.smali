.class public final enum LX/9la;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9la;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9la;

.field public static final enum ADDITIONAL_DATA_REPORT:LX/9la;

.field public static final enum EXECUTE_GUIDED_ACTION:LX/9la;

.field public static final enum FETCH_ADDITIONAL_DATA:LX/9la;

.field public static final enum FETCH_METADATA:LX/9la;

.field public static final enum METADATA_REPORT:LX/9la;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1532963
    new-instance v0, LX/9la;

    const-string v1, "FETCH_METADATA"

    invoke-direct {v0, v1, v2}, LX/9la;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9la;->FETCH_METADATA:LX/9la;

    .line 1532964
    new-instance v0, LX/9la;

    const-string v1, "METADATA_REPORT"

    invoke-direct {v0, v1, v3}, LX/9la;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9la;->METADATA_REPORT:LX/9la;

    .line 1532965
    new-instance v0, LX/9la;

    const-string v1, "FETCH_ADDITIONAL_DATA"

    invoke-direct {v0, v1, v4}, LX/9la;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9la;->FETCH_ADDITIONAL_DATA:LX/9la;

    .line 1532966
    new-instance v0, LX/9la;

    const-string v1, "ADDITIONAL_DATA_REPORT"

    invoke-direct {v0, v1, v5}, LX/9la;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9la;->ADDITIONAL_DATA_REPORT:LX/9la;

    .line 1532967
    new-instance v0, LX/9la;

    const-string v1, "EXECUTE_GUIDED_ACTION"

    invoke-direct {v0, v1, v6}, LX/9la;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9la;->EXECUTE_GUIDED_ACTION:LX/9la;

    .line 1532968
    const/4 v0, 0x5

    new-array v0, v0, [LX/9la;

    sget-object v1, LX/9la;->FETCH_METADATA:LX/9la;

    aput-object v1, v0, v2

    sget-object v1, LX/9la;->METADATA_REPORT:LX/9la;

    aput-object v1, v0, v3

    sget-object v1, LX/9la;->FETCH_ADDITIONAL_DATA:LX/9la;

    aput-object v1, v0, v4

    sget-object v1, LX/9la;->ADDITIONAL_DATA_REPORT:LX/9la;

    aput-object v1, v0, v5

    sget-object v1, LX/9la;->EXECUTE_GUIDED_ACTION:LX/9la;

    aput-object v1, v0, v6

    sput-object v0, LX/9la;->$VALUES:[LX/9la;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1532969
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9la;
    .locals 1

    .prologue
    .line 1532970
    const-class v0, LX/9la;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9la;

    return-object v0
.end method

.method public static values()[LX/9la;
    .locals 1

    .prologue
    .line 1532971
    sget-object v0, LX/9la;->$VALUES:[LX/9la;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9la;

    return-object v0
.end method
