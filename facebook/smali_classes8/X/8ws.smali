.class public final LX/8ws;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8wv;


# direct methods
.method public constructor <init>(LX/8wv;)V
    .locals 0

    .prologue
    .line 1423023
    iput-object p1, p0, LX/8ws;->a:LX/8wv;

    invoke-direct {p0}, LX/3nE;-><init>()V

    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1423024
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onPostExecute(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1423025
    check-cast p1, Ljava/lang/Void;

    .line 1423026
    invoke-super {p0, p1}, LX/3nE;->onPostExecute(Ljava/lang/Object;)V

    .line 1423027
    iget-object v0, p0, LX/8ws;->a:LX/8wv;

    iget-object v0, v0, LX/8wv;->z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Co;

    .line 1423028
    iget-object v2, p0, LX/8ws;->a:LX/8wv;

    invoke-virtual {v2}, LX/8wv;->get360TextureView()LX/2qW;

    move-result-object v2

    invoke-virtual {v2}, LX/2qW;->getPitch()F

    move-result v2

    iget-object v3, p0, LX/8ws;->a:LX/8wv;

    invoke-virtual {v3}, LX/8wv;->get360TextureView()LX/2qW;

    move-result-object v3

    invoke-virtual {v3}, LX/2qW;->getYaw()F

    move-result v3

    iget-object v4, p0, LX/8ws;->a:LX/8wv;

    invoke-virtual {v4}, LX/8wv;->get360TextureView()LX/2qW;

    move-result-object v4

    invoke-virtual {v4}, LX/2qW;->getRoll()F

    iget-object v4, p0, LX/8ws;->a:LX/8wv;

    invoke-virtual {v4}, LX/8wv;->get360TextureView()LX/2qW;

    move-result-object v4

    invoke-virtual {v4}, LX/2qW;->getFov()F

    move-result v4

    invoke-interface {v0, v2, v3, v4}, LX/7Co;->a(FFF)V

    goto :goto_0

    .line 1423029
    :cond_0
    iget-object v0, p0, LX/8ws;->a:LX/8wv;

    iget-boolean v0, v0, LX/8wv;->x:Z

    if-eqz v0, :cond_1

    .line 1423030
    iget-object v0, p0, LX/8ws;->a:LX/8wv;

    iget-object v0, v0, LX/8wv;->A:Landroid/os/Handler;

    iget-object v1, p0, LX/8ws;->a:LX/8wv;

    iget-object v1, v1, LX/8wv;->y:Ljava/lang/Runnable;

    const-wide/16 v2, 0x96

    const v4, -0x19215a17

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1423031
    :cond_1
    return-void
.end method
