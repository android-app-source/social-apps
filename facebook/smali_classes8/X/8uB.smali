.class public LX/8uB;
.super Landroid/text/style/URLSpan;
.source ""


# instance fields
.field public a:I

.field public b:Z

.field public c:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1414555
    invoke-direct {p0, p1}, Landroid/text/style/URLSpan;-><init>(Ljava/lang/String;)V

    .line 1414556
    const v0, -0xffff01

    iput v0, p0, LX/8uB;->a:I

    .line 1414557
    iput-boolean v1, p0, LX/8uB;->b:Z

    .line 1414558
    iput-boolean v1, p0, LX/8uB;->c:Z

    .line 1414559
    return-void
.end method


# virtual methods
.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .prologue
    .line 1414560
    invoke-super {p0, p1}, Landroid/text/style/URLSpan;->updateDrawState(Landroid/text/TextPaint;)V

    .line 1414561
    iget v0, p0, LX/8uB;->a:I

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 1414562
    iget-boolean v0, p0, LX/8uB;->b:Z

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 1414563
    iget-boolean v0, p0, LX/8uB;->c:Z

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    .line 1414564
    return-void
.end method
