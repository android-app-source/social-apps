.class public LX/A8S;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<SECTION::",
        "LX/90h;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<TSECTION;>;"
        }
    .end annotation
.end field

.field private b:LX/90h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TSECTION;"
        }
    .end annotation
.end field

.field public c:LX/A8O;

.field private d:Z

.field private e:Z

.field public f:I

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/A8S",
            "<TSECTION;>.Section",
            "LookUpResult",
            "<TSECTION;>;>;"
        }
    .end annotation
.end field

.field private h:Landroid/util/SparseIntArray;

.field private i:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/A8R;",
            ">;"
        }
    .end annotation
.end field

.field public j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/90h;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public k:Z


# direct methods
.method public constructor <init>(LX/0Px;LX/90h;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<TSECTION;>;TSECTION;)V"
        }
    .end annotation

    .prologue
    .line 1627669
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/A8S;-><init>(LX/0Px;LX/90h;Z)V

    .line 1627670
    return-void
.end method

.method public constructor <init>(LX/0Px;LX/90h;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<TSECTION;>;TSECTION;Z)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1627671
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1627672
    iput-boolean v2, p0, LX/A8S;->k:Z

    .line 1627673
    iput-boolean p3, p0, LX/A8S;->e:Z

    .line 1627674
    iput-object p1, p0, LX/A8S;->a:LX/0Px;

    .line 1627675
    iput-object p2, p0, LX/A8S;->b:LX/90h;

    .line 1627676
    iget-object v0, p0, LX/A8S;->b:LX/90h;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, LX/A8S;->d:Z

    .line 1627677
    iget-boolean v0, p0, LX/A8S;->d:Z

    if-eqz v0, :cond_0

    .line 1627678
    iget-object v0, p0, LX/A8S;->b:LX/90h;

    invoke-interface {v0}, LX/90h;->getCount()I

    move-result v0

    iget-object v3, p0, LX/A8S;->a:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-ne v0, v3, :cond_2

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1627679
    :cond_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v1

    :goto_2
    if-ge v2, v1, :cond_3

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/90h;

    .line 1627680
    new-instance v3, LX/A8Q;

    invoke-direct {v3, p0, v0}, LX/A8Q;-><init>(LX/A8S;LX/90h;)V

    invoke-interface {v0, v3}, LX/90h;->a(LX/A8O;)V

    .line 1627681
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_1
    move v0, v2

    .line 1627682
    goto :goto_0

    :cond_2
    move v1, v2

    .line 1627683
    goto :goto_1

    .line 1627684
    :cond_3
    invoke-static {p0}, LX/A8S;->c(LX/A8S;)V

    .line 1627685
    return-void
.end method

.method public static c(LX/A8S;)V
    .locals 13

    .prologue
    const/4 v5, 0x0

    .line 1627686
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/A8S;->g:Ljava/util/List;

    .line 1627687
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/A8S;->i:Landroid/util/SparseArray;

    .line 1627688
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, LX/A8S;->h:Landroid/util/SparseIntArray;

    .line 1627689
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/A8S;->j:Ljava/util/Map;

    .line 1627690
    iput v5, p0, LX/A8S;->f:I

    .line 1627691
    iget-object v0, p0, LX/A8S;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    move v1, v5

    :goto_0
    if-ge v1, v2, :cond_2

    iget-object v0, p0, LX/A8S;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/90h;

    .line 1627692
    iget-boolean v3, p0, LX/A8S;->e:Z

    if-nez v3, :cond_1

    .line 1627693
    iget v3, p0, LX/A8S;->f:I

    invoke-interface {v0}, LX/90h;->getViewTypeCount()I

    move-result v0

    add-int/2addr v0, v3

    iput v0, p0, LX/A8S;->f:I

    .line 1627694
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1627695
    :cond_1
    invoke-interface {v0}, LX/90h;->getViewTypeCount()I

    move-result v3

    iget v4, p0, LX/A8S;->f:I

    if-le v3, v4, :cond_0

    .line 1627696
    invoke-interface {v0}, LX/90h;->getViewTypeCount()I

    move-result v0

    iput v0, p0, LX/A8S;->f:I

    goto :goto_1

    .line 1627697
    :cond_2
    iget-object v0, p0, LX/A8S;->i:Landroid/util/SparseArray;

    iget v1, p0, LX/A8S;->f:I

    new-instance v2, LX/A8R;

    iget-object v3, p0, LX/A8S;->b:LX/90h;

    invoke-direct {v2, p0, v5, v3}, LX/A8R;-><init>(LX/A8S;ILX/90h;)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    move v2, v5

    move v12, v5

    .line 1627698
    :goto_2
    iget-object v0, p0, LX/A8S;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_7

    .line 1627699
    iget-object v0, p0, LX/A8S;->a:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/90h;

    .line 1627700
    iget-boolean v0, p0, LX/A8S;->d:Z

    if-eqz v0, :cond_3

    invoke-interface {v9}, LX/90h;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    const-string v0, ""

    iget-object v1, p0, LX/A8S;->b:LX/90h;

    invoke-interface {v1, v2}, LX/90h;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1627701
    iget-object v6, p0, LX/A8S;->g:Ljava/util/List;

    new-instance v0, LX/A8P;

    iget-object v3, p0, LX/A8S;->b:LX/90h;

    move-object v1, p0

    move v4, v2

    invoke-direct/range {v0 .. v4}, LX/A8P;-><init>(LX/A8S;ILX/90h;I)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1627702
    iget-object v0, p0, LX/A8S;->h:Landroid/util/SparseIntArray;

    iget-object v1, p0, LX/A8S;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iget v3, p0, LX/A8S;->f:I

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 1627703
    :cond_3
    iget-object v0, p0, LX/A8S;->j:Ljava/util/Map;

    iget-object v1, p0, LX/A8S;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v9, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v8, v5

    .line 1627704
    :goto_3
    invoke-interface {v9}, LX/90h;->getCount()I

    move-result v0

    if-ge v8, v0, :cond_5

    .line 1627705
    invoke-interface {v9, v8}, LX/90h;->getItemViewType(I)I

    move-result v0

    .line 1627706
    add-int v1, v12, v0

    .line 1627707
    iget-object v3, p0, LX/A8S;->g:Ljava/util/List;

    new-instance v6, LX/A8P;

    move-object v7, p0

    move v10, v2

    invoke-direct/range {v6 .. v10}, LX/A8P;-><init>(LX/A8S;ILX/90h;I)V

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1627708
    iget-object v3, p0, LX/A8S;->h:Landroid/util/SparseIntArray;

    iget-object v4, p0, LX/A8S;->g:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4, v1}, Landroid/util/SparseIntArray;->put(II)V

    .line 1627709
    iget-object v3, p0, LX/A8S;->i:Landroid/util/SparseArray;

    invoke-virtual {v3, v1}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v3

    if-gez v3, :cond_4

    .line 1627710
    iget-object v3, p0, LX/A8S;->i:Landroid/util/SparseArray;

    new-instance v4, LX/A8R;

    invoke-direct {v4, p0, v0, v9}, LX/A8R;-><init>(LX/A8S;ILX/90h;)V

    invoke-virtual {v3, v1, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1627711
    :cond_4
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 1627712
    :cond_5
    iget-boolean v0, p0, LX/A8S;->e:Z

    if-eqz v0, :cond_6

    move v0, v5

    :goto_4
    add-int/2addr v0, v12

    .line 1627713
    add-int/lit8 v2, v2, 0x1

    move v12, v0

    goto/16 :goto_2

    .line 1627714
    :cond_6
    invoke-interface {v9}, LX/90h;->getViewTypeCount()I

    move-result v0

    goto :goto_4

    .line 1627715
    :cond_7
    return-void
.end method

.method public static d(LX/A8S;)V
    .locals 1

    .prologue
    .line 1627716
    iget-boolean v0, p0, LX/A8S;->k:Z

    if-eqz v0, :cond_0

    .line 1627717
    invoke-static {p0}, LX/A8S;->c(LX/A8S;)V

    .line 1627718
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/A8S;->k:Z

    .line 1627719
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1627720
    invoke-static {p0}, LX/A8S;->d(LX/A8S;)V

    .line 1627721
    iget-object v0, p0, LX/A8S;->h:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v0

    return v0
.end method

.method public final a(I)LX/A8R;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/A8S",
            "<TSECTION;>.ViewType",
            "LookUpResult",
            "<TSECTION;>;"
        }
    .end annotation

    .prologue
    .line 1627722
    invoke-static {p0}, LX/A8S;->d(LX/A8S;)V

    .line 1627723
    iget-object v0, p0, LX/A8S;->i:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/A8R;

    return-object v0
.end method

.method public final b(I)LX/A8P;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/A8S",
            "<TSECTION;>.Section",
            "LookUpResult",
            "<TSECTION;>;"
        }
    .end annotation

    .prologue
    .line 1627724
    invoke-static {p0}, LX/A8S;->d(LX/A8S;)V

    .line 1627725
    iget-object v0, p0, LX/A8S;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/A8P;

    return-object v0
.end method

.method public final c(I)I
    .locals 1

    .prologue
    .line 1627726
    invoke-static {p0}, LX/A8S;->d(LX/A8S;)V

    .line 1627727
    iget-object v0, p0, LX/A8S;->h:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    return v0
.end method
