.class public LX/AJK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final d:Ljava/lang/Object;


# instance fields
.field public a:Z

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1661003
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/AJK;->d:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1661004
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/AJK;
    .locals 7

    .prologue
    .line 1661005
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 1661006
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 1661007
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 1661008
    if-nez v1, :cond_0

    .line 1661009
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1661010
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 1661011
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 1661012
    sget-object v1, LX/AJK;->d:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1661013
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 1661014
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1661015
    :cond_1
    if-nez v1, :cond_4

    .line 1661016
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 1661017
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 1661018
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    .line 1661019
    new-instance v0, LX/AJK;

    invoke-direct {v0}, LX/AJK;-><init>()V

    .line 1661020
    move-object v1, v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1661021
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 1661022
    if-nez v1, :cond_2

    .line 1661023
    sget-object v0, LX/AJK;->d:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AJK;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1661024
    :goto_1
    if-eqz v0, :cond_3

    .line 1661025
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1661026
    :goto_3
    check-cast v0, LX/AJK;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1661027
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 1661028
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1661029
    :catchall_1
    move-exception v0

    .line 1661030
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1661031
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1661032
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 1661033
    :cond_2
    :try_start_8
    sget-object v0, LX/AJK;->d:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AJK;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static a(LX/AJK;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1661034
    iget-object v0, p0, LX/AJK;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1661035
    sget-object v0, LX/AJJ;->SESSION_ID:LX/AJJ;

    invoke-virtual {v0}, LX/AJJ;->getName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/AJK;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1661036
    :cond_0
    iget-object v0, p0, LX/AJK;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1661037
    sget-object v0, LX/AJJ;->PROMPT_ID:LX/AJJ;

    invoke-virtual {v0}, LX/AJJ;->getName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/AJK;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1661038
    :cond_1
    return-void
.end method

.method public static a(LX/AJK;Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1661039
    sget-object v0, LX/AJJ;->DID_CHANGE_FEED_PRIVACY:LX/AJJ;

    invoke-virtual {v0}, LX/AJJ;->getName()Ljava/lang/String;

    move-result-object v1

    iget-boolean v0, p0, LX/AJK;->a:Z

    if-eqz v0, :cond_1

    const-string v0, "1"

    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1661040
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1661041
    sget-object v0, LX/AJJ;->SELECTED_FEED_PRIVACY:LX/AJJ;

    invoke-virtual {v0}, LX/AJJ;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1661042
    :cond_0
    return-void

    .line 1661043
    :cond_1
    const-string v0, "0"

    goto :goto_0
.end method

.method public static a(Landroid/os/Bundle;LX/0Px;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/model/AudienceControlData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1661044
    sget-object v0, LX/AJJ;->NUMBER_OF_FRIENDS:LX/AJJ;

    invoke-virtual {v0}, LX/AJJ;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1661045
    sget-object v0, LX/AJJ;->FRIENDS_FBIDS:LX/AJJ;

    invoke-virtual {v0}, LX/AJJ;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, ", "

    .line 1661046
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1661047
    const/4 v2, 0x0

    move v3, v2

    :goto_0
    const/16 v2, 0x14

    if-ge v3, v2, :cond_0

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_0

    .line 1661048
    invoke-virtual {p1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/audience/model/AudienceControlData;

    invoke-virtual {v2}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1661049
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 1661050
    :cond_0
    move-object v2, v4

    .line 1661051
    invoke-static {v1, v2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1661052
    return-void
.end method
