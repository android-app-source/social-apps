.class public final LX/9WE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnShowListener;


# instance fields
.field public final synthetic a:LX/9WH;

.field public final synthetic b:Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;LX/9WH;)V
    .locals 0

    .prologue
    .line 1500894
    iput-object p1, p0, LX/9WE;->b:Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;

    iput-object p2, p0, LX/9WE;->a:LX/9WH;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onShow(Landroid/content/DialogInterface;)V
    .locals 14

    .prologue
    .line 1500895
    iget-object v0, p0, LX/9WE;->a:LX/9WH;

    iget-object v1, p0, LX/9WE;->b:Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;

    iget-object v1, v1, Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;->m:LX/2EJ;

    const/4 v2, -0x3

    invoke-virtual {v1, v2}, LX/2EJ;->a(I)Landroid/widget/Button;

    move-result-object v1

    .line 1500896
    iput-object v1, v0, LX/9WH;->a:Landroid/widget/Button;

    .line 1500897
    iget-object v0, p0, LX/9WE;->a:LX/9WH;

    iget-object v1, p0, LX/9WE;->b:Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;

    iget-object v1, v1, Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;->m:LX/2EJ;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, LX/2EJ;->a(I)Landroid/widget/Button;

    move-result-object v1

    .line 1500898
    iput-object v1, v0, LX/9WH;->b:Landroid/widget/Button;

    .line 1500899
    iget-object v0, p0, LX/9WE;->a:LX/9WH;

    iget-object v1, p0, LX/9WE;->b:Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;

    iget-object v1, v1, Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;->m:LX/2EJ;

    const/4 v2, -0x2

    invoke-virtual {v1, v2}, LX/2EJ;->a(I)Landroid/widget/Button;

    move-result-object v1

    .line 1500900
    iput-object v1, v0, LX/9WH;->c:Landroid/widget/Button;

    .line 1500901
    iget-object v0, p0, LX/9WE;->a:LX/9WH;

    .line 1500902
    iget-object v1, v0, LX/9WH;->c:Landroid/widget/Button;

    move-object v0, v1

    .line 1500903
    iget-object v1, p0, LX/9WE;->b:Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 1500904
    iget-object v0, p0, LX/9WE;->a:LX/9WH;

    invoke-virtual {v0}, LX/9WH;->c()V

    .line 1500905
    iget-object v0, p0, LX/9WE;->a:LX/9WH;

    invoke-virtual {v0}, LX/9WH;->g()V

    .line 1500906
    iget-object v0, p0, LX/9WE;->b:Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;

    .line 1500907
    iget-object v1, v0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v0, v1

    .line 1500908
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 1500909
    iget-object v0, p0, LX/9WE;->b:Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;

    iget-object v0, v0, Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;->q:LX/9WC;

    iget-object v1, p0, LX/9WE;->b:Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;

    iget-object v1, v1, Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;->m:LX/2EJ;

    .line 1500910
    iput-object v1, v0, LX/9WC;->g:LX/2EJ;

    .line 1500911
    iget-object v2, v0, LX/9WC;->g:LX/2EJ;

    const v3, 0x7f0d10d3

    invoke-virtual {v2, v3}, LX/2EJ;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    iput-object v2, v0, LX/9WC;->o:Landroid/widget/FrameLayout;

    .line 1500912
    iget-object v2, v0, LX/9WC;->g:LX/2EJ;

    const v3, 0x7f0d10c8

    invoke-virtual {v2, v3}, LX/2EJ;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, v0, LX/9WC;->p:Landroid/widget/LinearLayout;

    .line 1500913
    iget-object v0, p0, LX/9WE;->b:Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;

    iget-wide v0, v0, Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;->o:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 1500914
    iget-object v0, p0, LX/9WE;->b:Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;

    iget-object v0, v0, Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;->q:LX/9WC;

    iget-object v1, p0, LX/9WE;->b:Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;

    iget-object v1, v1, Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;->n:Ljava/lang/String;

    iget-object v2, p0, LX/9WE;->b:Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;

    iget-wide v2, v2, Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;->o:J

    iget-object v4, p0, LX/9WE;->b:Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;

    iget-object v4, v4, Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/9WC;->a(Ljava/lang/String;JLjava/lang/String;)V

    .line 1500915
    :goto_0
    return-void

    .line 1500916
    :cond_0
    iget-object v0, p0, LX/9WE;->b:Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;

    iget-object v0, v0, Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;->q:LX/9WC;

    iget-object v1, p0, LX/9WE;->b:Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;

    iget-object v1, v1, Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;->n:Ljava/lang/String;

    iget-object v2, p0, LX/9WE;->b:Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;

    iget-object v2, v2, Lcom/facebook/negativefeedback/ui/NegativeFeedbackDialogFragment;->p:Ljava/lang/String;

    .line 1500917
    new-instance v5, LX/9Vx;

    const-wide/16 v7, -0x1

    const/4 v9, 0x0

    const-string v10, ""

    const-string v11, ""

    const-string v12, ""

    const/4 v13, 0x0

    move-object v6, v1

    invoke-direct/range {v5 .. v13}, LX/9Vx;-><init>(Ljava/lang/String;JLcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    iput-object v5, v0, LX/9WC;->k:LX/9Vx;

    .line 1500918
    iget-object v5, v0, LX/9WC;->r:LX/9Ve;

    invoke-virtual {v5, v1, v2}, LX/9Ve;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1500919
    iput-object v2, v0, LX/9WC;->w:Ljava/lang/String;

    .line 1500920
    iget-object v5, v0, LX/9WC;->k:LX/9Vx;

    invoke-static {v0, v5}, LX/9WC;->a$redex0(LX/9WC;LX/9Vx;)V

    .line 1500921
    goto :goto_0
.end method
