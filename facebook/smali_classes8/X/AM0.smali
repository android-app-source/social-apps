.class public final LX/AM0;
.super Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/backstage/ui/ScalingToggleableEditText;

.field private b:F


# direct methods
.method public constructor <init>(Lcom/facebook/backstage/ui/ScalingToggleableEditText;)V
    .locals 0

    .prologue
    .line 1665685
    iput-object p1, p0, LX/AM0;->a:Lcom/facebook/backstage/ui/ScalingToggleableEditText;

    invoke-direct {p0}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/backstage/ui/ScalingToggleableEditText;B)V
    .locals 0

    .prologue
    .line 1665686
    invoke-direct {p0, p1}, LX/AM0;-><init>(Lcom/facebook/backstage/ui/ScalingToggleableEditText;)V

    return-void
.end method


# virtual methods
.method public final onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 4

    .prologue
    .line 1665687
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpan()F

    move-result v0

    .line 1665688
    iget v1, p0, LX/AM0;->b:F

    sub-float v1, v0, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const v2, 0x3a83126f    # 0.001f

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    .line 1665689
    iget v1, p0, LX/AM0;->b:F

    sub-float v1, v0, v1

    iget-object v2, p0, LX/AM0;->a:Lcom/facebook/backstage/ui/ScalingToggleableEditText;

    invoke-virtual {v2}, Lcom/facebook/backstage/ui/ScalingToggleableEditText;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    iget-object v2, p0, LX/AM0;->a:Lcom/facebook/backstage/ui/ScalingToggleableEditText;

    iget v2, v2, Lcom/facebook/backstage/ui/ScalingToggleableEditText;->d:F

    mul-float/2addr v1, v2

    iget-object v2, p0, LX/AM0;->a:Lcom/facebook/backstage/ui/ScalingToggleableEditText;

    iget v2, v2, Lcom/facebook/backstage/ui/ScalingToggleableEditText;->d:F

    add-float/2addr v1, v2

    .line 1665690
    const/high16 v2, 0x42c80000    # 100.0f

    const/high16 v3, 0x40800000    # 4.0f

    invoke-static {v3, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 1665691
    iget-object v2, p0, LX/AM0;->a:Lcom/facebook/backstage/ui/ScalingToggleableEditText;

    invoke-virtual {v2, v1}, Lcom/facebook/backstage/ui/ScalingToggleableEditText;->setTextSize(F)V

    .line 1665692
    :cond_0
    iput v0, p0, LX/AM0;->b:F

    .line 1665693
    const/4 v0, 0x1

    return v0
.end method

.method public final onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1665694
    iget-object v0, p0, LX/AM0;->a:Lcom/facebook/backstage/ui/ScalingToggleableEditText;

    invoke-virtual {v0}, Lcom/facebook/backstage/ui/ScalingToggleableEditText;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 1665695
    if-eqz v0, :cond_0

    .line 1665696
    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 1665697
    :cond_0
    iget-object v0, p0, LX/AM0;->a:Lcom/facebook/backstage/ui/ScalingToggleableEditText;

    .line 1665698
    iput-boolean v1, v0, Lcom/facebook/backstage/ui/ScalingToggleableEditText;->e:Z

    .line 1665699
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpan()F

    move-result v0

    iput v0, p0, LX/AM0;->b:F

    .line 1665700
    return v1
.end method

.method public final onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 2

    .prologue
    .line 1665701
    iget-object v0, p0, LX/AM0;->a:Lcom/facebook/backstage/ui/ScalingToggleableEditText;

    invoke-virtual {v0}, Lcom/facebook/backstage/ui/ScalingToggleableEditText;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 1665702
    if-eqz v0, :cond_0

    .line 1665703
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 1665704
    :cond_0
    return-void
.end method
