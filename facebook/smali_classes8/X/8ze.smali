.class public final LX/8ze;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/8zh;


# direct methods
.method public constructor <init>(LX/8zh;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1427830
    iput-object p1, p0, LX/8ze;->b:LX/8zh;

    iput-object p2, p0, LX/8ze;->a:Ljava/lang/String;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1427819
    iget-object v0, p0, LX/8ze;->b:LX/8zh;

    const/4 v1, 0x0

    .line 1427820
    iput-boolean v1, v0, LX/8zh;->r:Z

    .line 1427821
    new-instance v0, LX/8zd;

    invoke-direct {v0, p0}, LX/8zd;-><init>(LX/8ze;)V

    .line 1427822
    invoke-static {p1}, LX/3d6;->forException(Ljava/lang/Throwable;)LX/1nY;

    move-result-object v1

    sget-object v2, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    if-ne v1, v2, :cond_0

    .line 1427823
    iget-object v1, p0, LX/8ze;->b:LX/8zh;

    iget-object v1, v1, LX/8zh;->e:LX/909;

    invoke-interface {v1, v0}, LX/909;->a(LX/8zc;)V

    .line 1427824
    :goto_0
    iget-object v0, p0, LX/8ze;->b:LX/8zh;

    iget-object v0, v0, LX/8zh;->f:LX/918;

    iget-object v1, p0, LX/8ze;->b:LX/8zh;

    iget-object v1, v1, LX/8zh;->c:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1427825
    iget-object v2, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v1, v2

    .line 1427826
    iget-object v2, p0, LX/8ze;->b:LX/8zh;

    iget-object v2, v2, LX/8zh;->i:LX/5LG;

    invoke-interface {v2}, LX/5LG;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/918;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1427827
    iget-object v0, p0, LX/8ze;->b:LX/8zh;

    iget-object v0, v0, LX/8zh;->h:LX/92D;

    invoke-virtual {v0}, LX/92D;->f()V

    .line 1427828
    return-void

    .line 1427829
    :cond_0
    iget-object v1, p0, LX/8ze;->b:LX/8zh;

    iget-object v1, v1, LX/8zh;->e:LX/909;

    invoke-interface {v1, v0}, LX/909;->b(LX/8zc;)V

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 1427735
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1427736
    iget-object v0, p0, LX/8ze;->b:LX/8zh;

    iget-object v0, v0, LX/8zh;->f:LX/918;

    iget-object v1, p0, LX/8ze;->b:LX/8zh;

    iget-object v1, v1, LX/8zh;->c:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1427737
    iget-object v2, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v1, v2

    .line 1427738
    iget-object v2, p0, LX/8ze;->b:LX/8zh;

    iget-object v2, v2, LX/8zh;->i:LX/5LG;

    invoke-interface {v2}, LX/5LG;->l()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/8ze;->b:LX/8zh;

    iget v3, v3, LX/8zh;->m:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/8ze;->a:Ljava/lang/String;

    .line 1427739
    const-string v5, "activities_selector_objects_loaded"

    invoke-static {v5, v1}, LX/918;->d(Ljava/lang/String;Ljava/lang/String;)LX/8yQ;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/8yQ;->a(Ljava/lang/String;)LX/8yQ;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/8yQ;->e(Ljava/lang/String;)LX/8yQ;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/8yQ;->f(Ljava/lang/String;)LX/8yQ;

    move-result-object v5

    .line 1427740
    iget-object v6, v5, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v5, v6

    .line 1427741
    iget-object v6, v0, LX/918;->a:LX/0Zb;

    invoke-interface {v6, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1427742
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1427743
    check-cast v0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;

    .line 1427744
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;->o()Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1427745
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/8ze;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    .line 1427746
    :goto_0
    return-void

    .line 1427747
    :cond_0
    iget-object v1, p0, LX/8ze;->b:LX/8zh;

    .line 1427748
    iget-object v2, v1, LX/8zh;->i:LX/5LG;

    invoke-interface {v2}, LX/5LG;->v()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v2

    if-nez v2, :cond_1

    .line 1427749
    iget-object v2, v1, LX/8zh;->i:LX/5LG;

    check-cast v2, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    invoke-static {v2}, LX/5Lb;->a(Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;)LX/5Lb;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;->a()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v3

    .line 1427750
    iput-object v3, v2, LX/5Lb;->i:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1427751
    move-object v2, v2

    .line 1427752
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;->j()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v3

    .line 1427753
    iput-object v3, v2, LX/5Lb;->j:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1427754
    move-object v2, v2

    .line 1427755
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;->k()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v3

    .line 1427756
    iput-object v3, v2, LX/5Lb;->k:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1427757
    move-object v2, v2

    .line 1427758
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;->l()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v3

    .line 1427759
    iput-object v3, v2, LX/5Lb;->l:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1427760
    move-object v2, v2

    .line 1427761
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;->m()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v3

    .line 1427762
    iput-object v3, v2, LX/5Lb;->m:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1427763
    move-object v2, v2

    .line 1427764
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;->n()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v3

    .line 1427765
    iput-object v3, v2, LX/5Lb;->n:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1427766
    move-object v2, v2

    .line 1427767
    invoke-virtual {v2}, LX/5Lb;->a()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    move-result-object v2

    iput-object v2, v1, LX/8zh;->i:LX/5LG;

    .line 1427768
    :cond_1
    iget-object v1, p0, LX/8ze;->b:LX/8zh;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;->o()Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;->o()Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v0

    iget-object v4, p0, LX/8ze;->a:Ljava/lang/String;

    .line 1427769
    const/4 v6, 0x0

    .line 1427770
    iput-object v0, v1, LX/8zh;->j:LX/0ut;

    .line 1427771
    iget-object v5, v1, LX/8zh;->a:LX/8zZ;

    iget-object v7, v1, LX/8zh;->i:LX/5LG;

    .line 1427772
    iput-object v7, v5, LX/8zZ;->f:LX/5LG;

    .line 1427773
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v8

    move v7, v6

    :goto_1
    if-ge v7, v8, :cond_5

    invoke-virtual {v2, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    .line 1427774
    invoke-virtual {v5}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->e()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v5}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->e()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->v_()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v5}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->e()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->v_()Ljava/lang/String;

    move-result-object v5

    iget-object v3, v1, LX/8zh;->l:Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1427775
    const/4 v5, 0x1

    .line 1427776
    :goto_2
    iget-object v7, v1, LX/8zh;->l:Ljava/lang/String;

    invoke-static {v7}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_4

    if-nez v5, :cond_4

    iget-object v5, v1, LX/8zh;->i:LX/5LG;

    invoke-interface {v5}, LX/5LG;->q()Z

    move-result v5

    if-eqz v5, :cond_4

    iget-object v5, v1, LX/8zh;->i:LX/5LG;

    invoke-interface {v5}, LX/5LG;->z()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;

    move-result-object v5

    if-eqz v5, :cond_4

    iget-object v5, v1, LX/8zh;->i:LX/5LG;

    invoke-interface {v5}, LX/5LG;->A()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;

    move-result-object v5

    if-eqz v5, :cond_4

    .line 1427777
    new-instance v5, LX/5LL;

    invoke-direct {v5}, LX/5LL;-><init>()V

    iget-object v7, v1, LX/8zh;->l:Ljava/lang/String;

    .line 1427778
    iput-object v7, v5, LX/5LL;->d:Ljava/lang/String;

    .line 1427779
    move-object v5, v5

    .line 1427780
    iput-boolean v6, v5, LX/5LL;->h:Z

    .line 1427781
    move-object v5, v5

    .line 1427782
    new-instance v7, LX/4aM;

    invoke-direct {v7}, LX/4aM;-><init>()V

    iget-object v8, v1, LX/8zh;->i:LX/5LG;

    invoke-interface {v8}, LX/5LG;->z()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;->a()Ljava/lang/String;

    move-result-object v8

    .line 1427783
    iput-object v8, v7, LX/4aM;->b:Ljava/lang/String;

    .line 1427784
    move-object v7, v7

    .line 1427785
    invoke-virtual {v7}, LX/4aM;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v7

    .line 1427786
    iput-object v7, v5, LX/5LL;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1427787
    move-object v5, v5

    .line 1427788
    new-instance v7, LX/5Lc;

    invoke-direct {v7}, LX/5Lc;-><init>()V

    iget-object v8, v1, LX/8zh;->l:Ljava/lang/String;

    .line 1427789
    iput-object v8, v7, LX/5Lc;->d:Ljava/lang/String;

    .line 1427790
    move-object v7, v7

    .line 1427791
    iput-boolean v6, v7, LX/5Lc;->c:Z

    .line 1427792
    move-object v7, v7

    .line 1427793
    new-instance v8, LX/4aM;

    invoke-direct {v8}, LX/4aM;-><init>()V

    iget-object v3, v1, LX/8zh;->i:LX/5LG;

    invoke-interface {v3}, LX/5LG;->A()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 1427794
    iput-object v3, v8, LX/4aM;->b:Ljava/lang/String;

    .line 1427795
    move-object v8, v8

    .line 1427796
    invoke-virtual {v8}, LX/4aM;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v8

    .line 1427797
    iput-object v8, v7, LX/5Lc;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1427798
    move-object v7, v7

    .line 1427799
    invoke-virtual {v7}, LX/5Lc;->a()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v7

    .line 1427800
    iput-object v7, v5, LX/5LL;->g:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    .line 1427801
    move-object v5, v5

    .line 1427802
    invoke-virtual {v5}, LX/5LL;->a()Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    move-result-object v5

    .line 1427803
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    invoke-virtual {v7, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v5

    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    .line 1427804
    iget-object v7, v1, LX/8zh;->a:LX/8zZ;

    .line 1427805
    iput-object v5, v7, LX/8zZ;->e:LX/0Px;

    .line 1427806
    iput v6, v7, LX/8zZ;->g:I

    .line 1427807
    invoke-virtual {v7}, LX/3mY;->bE_()V

    .line 1427808
    :goto_3
    iput-boolean v6, v1, LX/8zh;->r:Z

    .line 1427809
    iget-boolean v5, v1, LX/8zh;->q:Z

    if-eqz v5, :cond_2

    .line 1427810
    invoke-static {v1}, LX/8zh;->f(LX/8zh;)V

    .line 1427811
    :cond_2
    iput-object v4, v1, LX/8zh;->n:Ljava/lang/String;

    .line 1427812
    iget-object v0, p0, LX/8ze;->b:LX/8zh;

    iget-object v0, v0, LX/8zh;->h:LX/92D;

    iget-object v1, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    invoke-virtual {v0, v1}, LX/92D;->a(LX/0ta;)V

    goto/16 :goto_0

    .line 1427813
    :cond_3
    add-int/lit8 v5, v7, 0x1

    move v7, v5

    goto/16 :goto_1

    .line 1427814
    :cond_4
    iget-object v5, v1, LX/8zh;->a:LX/8zZ;

    .line 1427815
    iput-object v2, v5, LX/8zZ;->e:LX/0Px;

    .line 1427816
    const/4 v7, -0x1

    iput v7, v5, LX/8zZ;->g:I

    .line 1427817
    invoke-virtual {v5}, LX/3mY;->bE_()V

    .line 1427818
    goto :goto_3

    :cond_5
    move v5, v6

    goto/16 :goto_2
.end method
