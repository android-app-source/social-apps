.class public final LX/9Au;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/9Av;


# direct methods
.method public constructor <init>(LX/9Av;Z)V
    .locals 0

    .prologue
    .line 1450771
    iput-object p1, p0, LX/9Au;->b:LX/9Av;

    iput-boolean p2, p0, LX/9Au;->a:Z

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1450772
    iget-object v0, p0, LX/9Au;->b:LX/9Av;

    .line 1450773
    iput-boolean v1, v0, LX/9Av;->c:Z

    .line 1450774
    iget-boolean v0, p0, LX/9Au;->a:Z

    if-eqz v0, :cond_0

    .line 1450775
    iget-object v0, p0, LX/9Au;->b:LX/9Av;

    invoke-virtual {v0, v1}, LX/9Av;->a(Z)V

    .line 1450776
    :cond_0
    iget-object v0, p0, LX/9Au;->b:LX/9Av;

    invoke-static {v0, p1}, LX/9Av;->a$redex0(LX/9Av;Ljava/lang/Throwable;)V

    .line 1450777
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 12

    .prologue
    .line 1450778
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1450779
    iget-object v0, p0, LX/9Au;->b:LX/9Av;

    const/4 v1, 0x0

    .line 1450780
    iput-boolean v1, v0, LX/9Av;->c:Z

    .line 1450781
    if-nez p1, :cond_0

    .line 1450782
    iget-object v0, p0, LX/9Au;->b:LX/9Av;

    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Null feedback received"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, LX/9Av;->a$redex0(LX/9Av;Ljava/lang/Throwable;)V

    .line 1450783
    :goto_0
    return-void

    .line 1450784
    :cond_0
    iget-object v0, p0, LX/9Au;->b:LX/9Av;

    .line 1450785
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->O()Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->O()Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;->a()LX/0Px;

    move-result-object v1

    if-nez v1, :cond_a

    .line 1450786
    :cond_1
    const/4 v1, 0x0

    .line 1450787
    :goto_1
    move-object v1, v1

    .line 1450788
    iget-object v2, v0, LX/9Av;->d:LX/9BB;

    if-eqz v2, :cond_9

    .line 1450789
    iget-object v2, v0, LX/9Av;->d:LX/9BB;

    .line 1450790
    iget-object v3, v2, LX/9BB;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    const/4 v8, 0x0

    .line 1450791
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 1450792
    if-nez v1, :cond_2

    .line 1450793
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1450794
    :cond_2
    iget-object v4, v3, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->i:LX/1zf;

    .line 1450795
    iget-object v5, v4, LX/1zf;->c:LX/0Px;

    move-object v10, v5

    .line 1450796
    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v11

    move v7, v8

    move v5, v8

    move v6, v8

    :goto_2
    if-ge v7, v11, :cond_4

    invoke-virtual {v10, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1zt;

    .line 1450797
    iget p0, v4, LX/1zt;->e:I

    move p0, p0

    .line 1450798
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1450799
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 1450800
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v9, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1450801
    add-int/2addr v6, v4

    .line 1450802
    if-lez v4, :cond_d

    .line 1450803
    add-int/lit8 v4, v5, 0x1

    :goto_3
    move v5, v6

    .line 1450804
    :goto_4
    iget-object v6, v3, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->z:Landroid/util/SparseArray;

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v6, p0, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1450805
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    move v6, v5

    move v5, v4

    goto :goto_2

    .line 1450806
    :cond_3
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v9, v4, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v4, v5

    move v5, v6

    goto :goto_4

    .line 1450807
    :cond_4
    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_5
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    .line 1450808
    iget-object v10, v3, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->i:LX/1zf;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v11

    invoke-virtual {v10, v11}, LX/1zf;->a(I)LX/1zt;

    move-result-object v10

    sget-object v11, LX/1zt;->c:LX/1zt;

    if-ne v10, v11, :cond_c

    .line 1450809
    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    add-int/2addr v4, v6

    :goto_6
    move v6, v4

    .line 1450810
    goto :goto_5

    .line 1450811
    :cond_5
    const/4 v4, 0x1

    if-eq v5, v4, :cond_6

    .line 1450812
    sget-object v4, LX/1zt;->c:LX/1zt;

    move-object v4, v4

    .line 1450813
    iget v7, v4, LX/1zt;->e:I

    move v4, v7

    .line 1450814
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v9, v7, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1450815
    iget-object v6, v3, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->z:Landroid/util/SparseArray;

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v6, v4, v7}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1450816
    :cond_6
    iput-object v9, v3, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->x:Ljava/util/HashMap;

    .line 1450817
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 1450818
    new-instance v7, Landroid/util/SparseArray;

    invoke-direct {v7}, Landroid/util/SparseArray;-><init>()V

    iput-object v7, v3, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->y:Landroid/util/SparseArray;

    .line 1450819
    if-eq v5, v4, :cond_e

    .line 1450820
    iget-object v7, v3, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->y:Landroid/util/SparseArray;

    .line 1450821
    sget-object v8, LX/1zt;->c:LX/1zt;

    move-object v8, v8

    .line 1450822
    iget v9, v8, LX/1zt;->e:I

    move v8, v9

    .line 1450823
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v7, v8, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1450824
    :goto_7
    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v6, v4

    :goto_8
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 1450825
    iget-object v9, v3, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->y:Landroid/util/SparseArray;

    add-int/lit8 v4, v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v9, v8, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    move v6, v4

    .line 1450826
    goto :goto_8

    .line 1450827
    :cond_7
    iget-object v3, v2, LX/9BB;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v3, v3, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->u:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    if-eqz v3, :cond_8

    .line 1450828
    iget-object v3, v2, LX/9BB;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    iget-object v3, v3, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->u:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v3}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 1450829
    :cond_8
    iget-object v3, v2, LX/9BB;->a:Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;

    invoke-static {v3}, Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;->r(Lcom/facebook/feedback/reactions/ui/TabbedReactorsListFragment;)V

    .line 1450830
    :cond_9
    goto/16 :goto_0

    .line 1450831
    :cond_a
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    .line 1450832
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->O()Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v1, 0x0

    move v3, v1

    :goto_9
    if-ge v3, v5, :cond_b

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;

    .line 1450833
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;->a()Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->j()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;->j()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, p0, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1450834
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_9

    :cond_b
    move-object v1, v2

    .line 1450835
    goto/16 :goto_1

    :cond_c
    move v4, v6

    goto/16 :goto_6

    :cond_d
    move v4, v5

    goto/16 :goto_3

    :cond_e
    move v4, v6

    goto :goto_7
.end method
