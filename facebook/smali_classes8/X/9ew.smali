.class public final LX/9ew;
.super Landroid/animation/AnimatorListenerAdapter;
.source ""


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1520684
    iput-object p1, p0, LX/9ew;->b:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iput-object p2, p0, LX/9ew;->a:Landroid/view/View;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1520685
    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 1520686
    iget-object v0, p0, LX/9ew;->a:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 1520687
    iget-object v0, p0, LX/9ew;->a:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1520688
    iget-object v0, p0, LX/9ew;->b:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->N:LX/9eb;

    invoke-interface {v0}, LX/9ea;->h()V

    .line 1520689
    iget-object v0, p0, LX/9ew;->b:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    .line 1520690
    iput-boolean v2, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->Z:Z

    .line 1520691
    return-void
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 1520692
    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationStart(Landroid/animation/Animator;)V

    .line 1520693
    iget-object v0, p0, LX/9ew;->b:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    const/4 v1, 0x0

    .line 1520694
    iput-boolean v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->Y:Z

    .line 1520695
    iget-object v0, p0, LX/9ew;->b:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->N:LX/9eb;

    invoke-interface {v0}, LX/9ea;->i()V

    .line 1520696
    iget-object v0, p0, LX/9ew;->b:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    const/4 v1, 0x1

    .line 1520697
    iput-boolean v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->Z:Z

    .line 1520698
    return-void
.end method
