.class public final LX/8xj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;)V
    .locals 0

    .prologue
    .line 1424558
    iput-object p1, p0, LX/8xj;->a:Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x6e94bc0

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1424559
    iget-object v1, p0, LX/8xj;->a:Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;

    invoke-static {v1}, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->a(Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1424560
    const v1, -0x807239e

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1424561
    :goto_0
    return-void

    .line 1424562
    :cond_0
    iget-object v1, p0, LX/8xj;->a:Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;

    iget-object v1, v1, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->u:Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;

    if-nez v1, :cond_1

    .line 1424563
    iget-object v1, p0, LX/8xj;->a:Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;

    invoke-static {v1}, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->p(Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;)V

    .line 1424564
    :goto_1
    const v1, -0xa8cbe62

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0

    .line 1424565
    :cond_1
    iget-object v1, p0, LX/8xj;->a:Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;

    invoke-static {v1}, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->o(Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;)V

    goto :goto_1
.end method
