.class public final LX/9FP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/21M;


# instance fields
.field public final synthetic a:LX/9FA;

.field public final synthetic b:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic c:Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;LX/9FA;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 1458545
    iput-object p1, p0, LX/9FP;->c:Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;

    iput-object p2, p0, LX/9FP;->a:LX/9FA;

    iput-object p3, p0, LX/9FP;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/1zt;LX/0Ve;)V
    .locals 3

    .prologue
    .line 1458546
    sget-object v0, LX/1zt;->d:LX/1zt;

    if-ne p2, v0, :cond_0

    .line 1458547
    :goto_0
    return-void

    .line 1458548
    :cond_0
    iget-object v0, p0, LX/9FP;->c:Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedback/ui/rows/CommentActionsWithReactionsPartDefinition;->l:LX/20l;

    .line 1458549
    iget-object v1, v0, LX/20l;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/1zs;->d:LX/0Tn;

    const/4 p1, 0x1

    invoke-interface {v1, v2, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 1458550
    iget-object v1, p0, LX/9FP;->a:LX/9FA;

    iget-object v0, p0, LX/9FP;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1458551
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 1458552
    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v2, p0, LX/9FP;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v2}, LX/5Op;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-virtual {v1, v0, v2, p2}, LX/9FA;->a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;LX/1zt;)V

    goto :goto_0
.end method
