.class public final enum LX/9jt;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9jt;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9jt;

.field public static final enum MOST_RECENT:LX/9jt;

.field public static final enum NEARBY:LX/9jt;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1530510
    new-instance v0, LX/9jt;

    const-string v1, "NEARBY"

    invoke-direct {v0, v1, v2}, LX/9jt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9jt;->NEARBY:LX/9jt;

    .line 1530511
    new-instance v0, LX/9jt;

    const-string v1, "MOST_RECENT"

    invoke-direct {v0, v1, v3}, LX/9jt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9jt;->MOST_RECENT:LX/9jt;

    .line 1530512
    const/4 v0, 0x2

    new-array v0, v0, [LX/9jt;

    sget-object v1, LX/9jt;->NEARBY:LX/9jt;

    aput-object v1, v0, v2

    sget-object v1, LX/9jt;->MOST_RECENT:LX/9jt;

    aput-object v1, v0, v3

    sput-object v0, LX/9jt;->$VALUES:[LX/9jt;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1530509
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9jt;
    .locals 1

    .prologue
    .line 1530513
    const-class v0, LX/9jt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9jt;

    return-object v0
.end method

.method public static values()[LX/9jt;
    .locals 1

    .prologue
    .line 1530508
    sget-object v0, LX/9jt;->$VALUES:[LX/9jt;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9jt;

    return-object v0
.end method
