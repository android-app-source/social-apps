.class public final enum LX/9Dw;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9Dw;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9Dw;

.field public static final enum FRIEND_AND_OTHERS:LX/9Dw;

.field public static final enum NO_FRIENDS:LX/9Dw;

.field public static final enum ONE_FRIEND_ONLY:LX/9Dw;

.field public static final enum TWO_FRIENDS_ONLY:LX/9Dw;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1456312
    new-instance v0, LX/9Dw;

    const-string v1, "ONE_FRIEND_ONLY"

    invoke-direct {v0, v1, v2}, LX/9Dw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9Dw;->ONE_FRIEND_ONLY:LX/9Dw;

    .line 1456313
    new-instance v0, LX/9Dw;

    const-string v1, "TWO_FRIENDS_ONLY"

    invoke-direct {v0, v1, v3}, LX/9Dw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9Dw;->TWO_FRIENDS_ONLY:LX/9Dw;

    .line 1456314
    new-instance v0, LX/9Dw;

    const-string v1, "NO_FRIENDS"

    invoke-direct {v0, v1, v4}, LX/9Dw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9Dw;->NO_FRIENDS:LX/9Dw;

    .line 1456315
    new-instance v0, LX/9Dw;

    const-string v1, "FRIEND_AND_OTHERS"

    invoke-direct {v0, v1, v5}, LX/9Dw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9Dw;->FRIEND_AND_OTHERS:LX/9Dw;

    .line 1456316
    const/4 v0, 0x4

    new-array v0, v0, [LX/9Dw;

    sget-object v1, LX/9Dw;->ONE_FRIEND_ONLY:LX/9Dw;

    aput-object v1, v0, v2

    sget-object v1, LX/9Dw;->TWO_FRIENDS_ONLY:LX/9Dw;

    aput-object v1, v0, v3

    sget-object v1, LX/9Dw;->NO_FRIENDS:LX/9Dw;

    aput-object v1, v0, v4

    sget-object v1, LX/9Dw;->FRIEND_AND_OTHERS:LX/9Dw;

    aput-object v1, v0, v5

    sput-object v0, LX/9Dw;->$VALUES:[LX/9Dw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1456317
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9Dw;
    .locals 1

    .prologue
    .line 1456318
    const-class v0, LX/9Dw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9Dw;

    return-object v0
.end method

.method public static values()[LX/9Dw;
    .locals 1

    .prologue
    .line 1456319
    sget-object v0, LX/9Dw;->$VALUES:[LX/9Dw;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9Dw;

    return-object v0
.end method
