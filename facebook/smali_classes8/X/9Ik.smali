.class public LX/9Ik;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1J7;


# instance fields
.field private final a:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feedback/ui/vpv_logging/CommentsViewportMonitor$CommentsViewportEventListener;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/os/Handler;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/5Os;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0g8;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Z


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1463558
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1463559
    new-instance v0, LX/01J;

    invoke-direct {v0}, LX/01J;-><init>()V

    iput-object v0, p0, LX/9Ik;->a:LX/01J;

    .line 1463560
    new-instance v0, LX/01J;

    invoke-direct {v0}, LX/01J;-><init>()V

    iput-object v0, p0, LX/9Ik;->b:LX/01J;

    .line 1463561
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/9Ik;->c:Ljava/util/List;

    .line 1463562
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1463563
    iput-object v0, p0, LX/9Ik;->d:LX/0Ot;

    .line 1463564
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1463565
    iput-object v0, p0, LX/9Ik;->e:LX/0Ot;

    .line 1463566
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/9Ik;->g:Z

    .line 1463567
    return-void
.end method

.method public static a(LX/9Ik;I)Lcom/facebook/graphql/model/GraphQLComment;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1463568
    iget-object v0, p0, LX/9Ik;->f:LX/0g8;

    invoke-interface {v0, p1}, LX/0g8;->f(I)Ljava/lang/Object;

    move-result-object v0

    .line 1463569
    instance-of v2, v0, LX/1Rk;

    if-nez v2, :cond_0

    move-object v0, v1

    .line 1463570
    :goto_0
    return-object v0

    .line 1463571
    :cond_0
    check-cast v0, LX/1Rk;

    .line 1463572
    iget-object v2, v0, LX/1Rk;->e:LX/0jW;

    move-object v0, v2

    .line 1463573
    instance-of v2, v0, Lcom/facebook/graphql/model/GraphQLComment;

    if-eqz v2, :cond_1

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method private a(IIII)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1463574
    invoke-static {p1, p3}, Ljava/lang/Math;->max(II)I

    move-result v2

    sub-int/2addr v2, p1

    .line 1463575
    invoke-static {p2, p4}, Ljava/lang/Math;->min(II)I

    move-result v3

    sub-int v4, v3, p1

    .line 1463576
    iget-object v3, p0, LX/9Ik;->f:LX/0g8;

    invoke-interface {v3}, LX/0g8;->d()I

    move-result v5

    .line 1463577
    if-lt p3, p1, :cond_3

    if-gt p4, p2, :cond_3

    .line 1463578
    iget-object v3, p0, LX/9Ik;->f:LX/0g8;

    invoke-interface {v3, v2}, LX/0g8;->e(I)Landroid/view/View;

    move-result-object v3

    .line 1463579
    iget-object v6, p0, LX/9Ik;->f:LX/0g8;

    invoke-interface {v6, v4}, LX/0g8;->e(I)Landroid/view/View;

    move-result-object v6

    .line 1463580
    if-eqz v3, :cond_0

    if-nez v6, :cond_2

    :cond_0
    move v0, v1

    .line 1463581
    :cond_1
    :goto_0
    return v0

    .line 1463582
    :cond_2
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v3

    if-ltz v3, :cond_3

    invoke-virtual {v6}, Landroid/view/View;->getBottom()I

    move-result v3

    if-le v3, v5, :cond_1

    .line 1463583
    :cond_3
    div-int/lit8 v6, v5, 0x2

    move v3, v1

    .line 1463584
    :goto_1
    if-gt v2, v4, :cond_5

    .line 1463585
    iget-object v7, p0, LX/9Ik;->f:LX/0g8;

    invoke-interface {v7, v2}, LX/0g8;->e(I)Landroid/view/View;

    move-result-object v7

    .line 1463586
    if-nez v7, :cond_4

    move v0, v1

    .line 1463587
    goto :goto_0

    .line 1463588
    :cond_4
    const/4 p3, 0x0

    .line 1463589
    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    move-result p1

    invoke-static {p3, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 1463590
    invoke-virtual {v7}, Landroid/view/View;->getBottom()I

    move-result p2

    invoke-static {v5, p2}, Ljava/lang/Math;->min(II)I

    move-result p2

    .line 1463591
    sub-int p1, p2, p1

    invoke-static {p3, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    move v7, p1

    .line 1463592
    add-int/2addr v3, v7

    .line 1463593
    if-ge v3, v6, :cond_1

    .line 1463594
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_5
    move v0, v1

    .line 1463595
    goto :goto_0
.end method

.method private b(Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 13

    .prologue
    .line 1463596
    iget-object v0, p0, LX/9Ik;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 1463597
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 1463598
    iget-object v0, p0, LX/9Ik;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Ij;

    .line 1463599
    iget-object v3, v0, LX/9Ij;->e:LX/01J;

    invoke-virtual {v3, p1}, LX/01J;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    .line 1463600
    if-nez v3, :cond_2

    .line 1463601
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1463602
    :cond_1
    return-void

    .line 1463603
    :cond_2
    iget-object v4, v0, LX/9Ij;->c:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v5

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    sub-long/2addr v5, v3

    .line 1463604
    const-wide/16 v3, 0x0

    cmp-long v3, v5, v3

    if-ltz v3, :cond_4

    iget-object v3, v0, LX/9Ij;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/5Os;

    .line 1463605
    iget-object v7, v3, LX/5Os;->d:Ljava/lang/Long;

    if-nez v7, :cond_3

    .line 1463606
    iget-object v7, v3, LX/5Os;->a:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0ad;

    sget-wide v9, LX/0wn;->A:J

    const-wide/16 v11, 0x64

    invoke-interface {v7, v9, v10, v11, v12}, LX/0ad;->a(JJ)J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    iput-object v7, v3, LX/5Os;->d:Ljava/lang/Long;

    .line 1463607
    :cond_3
    iget-object v7, v3, LX/5Os;->d:Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    move-wide v3, v7

    .line 1463608
    cmp-long v3, v5, v3

    if-ltz v3, :cond_0

    .line 1463609
    :cond_4
    const/4 v8, 0x0

    .line 1463610
    if-eqz p1, :cond_5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    iget-object v3, v0, LX/9Ij;->f:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    if-nez v3, :cond_6

    .line 1463611
    :cond_5
    :goto_2
    goto :goto_1

    .line 1463612
    :cond_6
    iget-object v3, v0, LX/9Ij;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0Zb;

    const-string v4, "comment_vpv_duration"

    invoke-interface {v3, v4, v8}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v4

    .line 1463613
    invoke-virtual {v4}, LX/0oG;->a()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1463614
    iget-object v3, v0, LX/9Ij;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/5Os;

    .line 1463615
    iget-object v7, v3, LX/5Os;->e:Ljava/lang/Boolean;

    if-nez v7, :cond_7

    .line 1463616
    iget-object v7, v3, LX/5Os;->a:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0ad;

    sget-short v9, LX/0wn;->y:S

    const/4 v10, 0x0

    invoke-interface {v7, v9, v10}, LX/0ad;->a(SZ)Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    iput-object v7, v3, LX/5Os;->e:Ljava/lang/Boolean;

    .line 1463617
    :cond_7
    iget-object v7, v3, LX/5Os;->e:Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    move v3, v7

    .line 1463618
    if-eqz v3, :cond_8

    .line 1463619
    const-string v3, "is_on_sponsored_story"

    iget-object v7, v0, LX/9Ij;->f:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1463620
    iget-boolean v9, v7, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->h:Z

    move v7, v9

    .line 1463621
    invoke-virtual {v4, v3, v7}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    .line 1463622
    :cond_8
    iget-object v3, v0, LX/9Ij;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/5Os;

    .line 1463623
    iget-object v7, v3, LX/5Os;->f:Ljava/lang/Boolean;

    if-nez v7, :cond_9

    .line 1463624
    iget-object v7, v3, LX/5Os;->a:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0ad;

    sget-short v9, LX/0wn;->z:S

    const/4 v10, 0x0

    invoke-interface {v7, v9, v10}, LX/0ad;->a(SZ)Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    iput-object v7, v3, LX/5Os;->f:Ljava/lang/Boolean;

    .line 1463625
    :cond_9
    iget-object v7, v3, LX/5Os;->f:Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    move v3, v7

    .line 1463626
    if-eqz v3, :cond_a

    .line 1463627
    const-string v3, "is_page_comment"

    invoke-static {p1}, LX/36l;->j(Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result v7

    invoke-virtual {v4, v3, v7}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    .line 1463628
    :cond_a
    iget-object v3, v0, LX/9Ij;->f:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1463629
    iget-object v7, v3, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->c:Ljava/lang/String;

    move-object v3, v7

    .line 1463630
    invoke-virtual {v4, v3}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string v4, "comment_id"

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v4, v7}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string v4, "duration_ms"

    invoke-virtual {v3, v4, v5, v6}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    move-result-object v3

    const-string v4, "referrer_source"

    iget-object v7, v0, LX/9Ij;->f:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1463631
    iget-object v9, v7, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->d:Ljava/lang/String;

    move-object v7, v9

    .line 1463632
    invoke-virtual {v3, v4, v7}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string v4, "tracking_codes"

    iget-object v7, v0, LX/9Ij;->f:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1463633
    iget-object v9, v7, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->a:LX/162;

    move-object v7, v9

    .line 1463634
    invoke-virtual {v3, v4, v7}, LX/0oG;->a(Ljava/lang/String;LX/0lF;)LX/0oG;

    move-result-object v3

    invoke-virtual {v3}, LX/0oG;->d()V

    .line 1463635
    const/4 v3, 0x7

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    const/4 v4, 0x1

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v3, v4

    const/4 v4, 0x2

    iget-object v7, v0, LX/9Ij;->f:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1463636
    iget-object v8, v7, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->c:Ljava/lang/String;

    move-object v7, v8

    .line 1463637
    aput-object v7, v3, v4

    const/4 v4, 0x3

    iget-object v7, v0, LX/9Ij;->f:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1463638
    iget-object v8, v7, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->d:Ljava/lang/String;

    move-object v7, v8

    .line 1463639
    aput-object v7, v3, v4

    const/4 v4, 0x4

    iget-object v7, v0, LX/9Ij;->f:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1463640
    iget-boolean v8, v7, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->h:Z

    move v7, v8

    .line 1463641
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v3, v4

    const/4 v4, 0x5

    invoke-static {p1}, LX/36l;->j(Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v3, v4

    const/4 v4, 0x6

    iget-object v7, v0, LX/9Ij;->f:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1463642
    iget-object v8, v7, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->a:LX/162;

    move-object v7, v8

    .line 1463643
    aput-object v7, v3, v4

    goto/16 :goto_2
.end method

.method public static d(LX/9Ik;)V
    .locals 15

    .prologue
    .line 1463644
    iget-object v0, p0, LX/9Ik;->f:LX/0g8;

    if-eqz v0, :cond_8

    iget-object v0, p0, LX/9Ik;->f:LX/0g8;

    invoke-interface {v0}, LX/0g8;->B()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, LX/9Ik;->f:LX/0g8;

    invoke-interface {v0}, LX/0g8;->d()I

    move-result v0

    if-lez v0, :cond_8

    iget-object v0, p0, LX/9Ik;->f:LX/0g8;

    invoke-interface {v0}, LX/0g8;->q()I

    move-result v0

    if-ltz v0, :cond_8

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1463645
    if-nez v0, :cond_0

    .line 1463646
    :goto_1
    return-void

    .line 1463647
    :cond_0
    iget-object v0, p0, LX/9Ik;->b:LX/01J;

    iget-object v1, p0, LX/9Ik;->a:LX/01J;

    invoke-virtual {v0, v1}, LX/01J;->a(LX/01J;)V

    .line 1463648
    iget-object v0, p0, LX/9Ik;->f:LX/0g8;

    invoke-interface {v0}, LX/0g8;->q()I

    move-result v1

    .line 1463649
    iget-object v0, p0, LX/9Ik;->f:LX/0g8;

    invoke-interface {v0}, LX/0g8;->r()I

    move-result v2

    .line 1463650
    iget-object v0, p0, LX/9Ik;->f:LX/0g8;

    invoke-interface {v0}, LX/0g8;->s()I

    move-result v3

    .line 1463651
    add-int/lit8 v0, v3, -0x1

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v4

    move v0, v1

    .line 1463652
    :cond_1
    :goto_2
    if-gt v0, v4, :cond_5

    .line 1463653
    invoke-static {p0, v0}, LX/9Ik;->a(LX/9Ik;I)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v5

    .line 1463654
    if-nez v5, :cond_2

    .line 1463655
    add-int/lit8 v0, v0, 0x1

    .line 1463656
    goto :goto_2

    .line 1463657
    :cond_2
    if-le v0, v1, :cond_9

    .line 1463658
    :goto_3
    move v0, v0

    .line 1463659
    add-int/lit8 v6, v0, 0x1

    .line 1463660
    :goto_4
    if-ge v6, v3, :cond_3

    invoke-static {p0, v6}, LX/9Ik;->a(LX/9Ik;I)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/facebook/graphql/model/GraphQLComment;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1463661
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    .line 1463662
    :cond_3
    add-int/lit8 v6, v6, -0x1

    move v6, v6

    .line 1463663
    invoke-direct {p0, v1, v2, v0, v6}, LX/9Ik;->a(IIII)Z

    move-result v7

    .line 1463664
    add-int/lit8 v0, v6, 0x1

    .line 1463665
    if-eqz v7, :cond_1

    .line 1463666
    iget-object v6, p0, LX/9Ik;->b:LX/01J;

    invoke-virtual {v6, v5}, LX/01J;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-nez v6, :cond_1

    .line 1463667
    iget-object v6, p0, LX/9Ik;->a:LX/01J;

    sget-object v7, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v6, v5, v7}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-nez v6, :cond_1

    .line 1463668
    iget-object v8, p0, LX/9Ik;->c:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v10

    .line 1463669
    const/4 v8, 0x0

    move v9, v8

    :goto_5
    if-ge v9, v10, :cond_4

    .line 1463670
    iget-object v8, p0, LX/9Ik;->c:Ljava/util/List;

    invoke-interface {v8, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/9Ij;

    .line 1463671
    iget-object v12, v8, LX/9Ij;->e:LX/01J;

    iget-object v11, v8, LX/9Ij;->c:LX/0Ot;

    invoke-interface {v11}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, LX/0SG;

    invoke-interface {v11}, LX/0SG;->a()J

    move-result-wide v13

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v12, v5, v11}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1463672
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    goto :goto_5

    .line 1463673
    :cond_4
    goto :goto_2

    .line 1463674
    :cond_5
    iget-object v0, p0, LX/9Ik;->b:LX/01J;

    invoke-virtual {v0}, LX/01J;->size()I

    move-result v2

    .line 1463675
    const/4 v0, 0x0

    move v1, v0

    :goto_6
    if-ge v1, v2, :cond_7

    .line 1463676
    iget-object v0, p0, LX/9Ik;->b:LX/01J;

    invoke-virtual {v0, v1}, LX/01J;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1463677
    iget-object v3, p0, LX/9Ik;->a:LX/01J;

    invoke-virtual {v3, v0}, LX/01J;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_6

    .line 1463678
    invoke-direct {p0, v0}, LX/9Ik;->b(Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 1463679
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 1463680
    :cond_7
    iget-object v0, p0, LX/9Ik;->b:LX/01J;

    invoke-virtual {v0}, LX/01J;->clear()V

    goto/16 :goto_1

    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1463681
    :cond_9
    add-int/lit8 v6, v0, -0x1

    .line 1463682
    :goto_7
    if-ltz v6, :cond_a

    invoke-static {p0, v6}, LX/9Ik;->a(LX/9Ik;I)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/facebook/graphql/model/GraphQLComment;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 1463683
    add-int/lit8 v6, v6, -0x1

    goto :goto_7

    .line 1463684
    :cond_a
    add-int/lit8 v0, v6, 0x1

    goto/16 :goto_3
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 1463685
    iget-object v0, p0, LX/9Ik;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5Os;

    .line 1463686
    iget-object v1, v0, LX/5Os;->c:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 1463687
    iget-object v1, v0, LX/5Os;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0ad;

    sget v2, LX/0wn;->w:I

    const/4 p0, -0x1

    invoke-interface {v1, v2, p0}, LX/0ad;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, LX/5Os;->c:Ljava/lang/Integer;

    .line 1463688
    :cond_0
    iget-object v1, v0, LX/5Os;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move v0, v1

    .line 1463689
    return v0
.end method

.method public final a(LX/0g8;I)V
    .locals 0

    .prologue
    .line 1463690
    return-void
.end method

.method public final a(LX/0g8;III)V
    .locals 1

    .prologue
    .line 1463691
    iget-boolean v0, p0, LX/9Ik;->g:Z

    if-nez v0, :cond_0

    .line 1463692
    :goto_0
    return-void

    .line 1463693
    :cond_0
    invoke-static {p0}, LX/9Ik;->d(LX/9Ik;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1463694
    iget-object v0, p0, LX/9Ik;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    new-instance v1, Lcom/facebook/feedback/ui/vpv_logging/CommentsViewportMonitor$1;

    invoke-direct {v1, p0}, Lcom/facebook/feedback/ui/vpv_logging/CommentsViewportMonitor$1;-><init>(LX/9Ik;)V

    const v2, 0x2677139e

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1463695
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 1463696
    iget-object v0, p0, LX/9Ik;->a:LX/01J;

    invoke-virtual {v0}, LX/01J;->size()I

    move-result v2

    .line 1463697
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 1463698
    iget-object v0, p0, LX/9Ik;->a:LX/01J;

    invoke-virtual {v0, v1}, LX/01J;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1463699
    invoke-direct {p0, v0}, LX/9Ik;->b(Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 1463700
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1463701
    :cond_0
    iget-object v0, p0, LX/9Ik;->a:LX/01J;

    invoke-virtual {v0}, LX/01J;->clear()V

    .line 1463702
    return-void
.end method
