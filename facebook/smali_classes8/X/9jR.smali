.class public LX/9jR;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public b:LX/0lB;

.field public c:LX/9k8;

.field public d:LX/0Sh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1529853
    const-class v0, LX/9jR;

    sput-object v0, LX/9jR;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0lB;LX/9k8;LX/0Sh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1529854
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1529855
    iput-object p1, p0, LX/9jR;->b:LX/0lB;

    .line 1529856
    iput-object p2, p0, LX/9jR;->c:LX/9k8;

    .line 1529857
    iput-object p3, p0, LX/9jR;->d:LX/0Sh;

    .line 1529858
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel;)V
    .locals 4

    .prologue
    .line 1529859
    iget-object v0, p0, LX/9jR;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 1529860
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1529861
    const/4 v0, 0x2

    .line 1529862
    :try_start_0
    iget-object v1, p0, LX/9jR;->b:LX/0lB;

    invoke-virtual {v1, p1}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1529863
    iget-object v2, p0, LX/9jR;->c:LX/9k8;

    invoke-virtual {v2, v0, v1}, LX/9k8;->a(ILjava/lang/String;)V
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    .line 1529864
    :goto_1
    return-void

    .line 1529865
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1529866
    :catch_0
    move-exception v1

    .line 1529867
    sget-object v2, LX/9jR;->a:Ljava/lang/Class;

    const-string v3, "failed to serialize model"

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
