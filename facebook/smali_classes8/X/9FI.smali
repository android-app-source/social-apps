.class public LX/9FI;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1458389
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1458390
    return-void
.end method

.method public static a(Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1458391
    iget-object v0, p0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->g:Lcom/facebook/ipc/media/StickerItem;

    if-eqz v0, :cond_0

    .line 1458392
    const-string v0, "sticker_comment_posted"

    .line 1458393
    :goto_0
    return-object v0

    .line 1458394
    :cond_0
    iget-object v0, p0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 1458395
    :goto_1
    iget-object v3, p0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->f:Lcom/facebook/ipc/media/MediaItem;

    if-eqz v3, :cond_2

    .line 1458396
    :goto_2
    if-eqz v1, :cond_3

    if-nez v0, :cond_3

    .line 1458397
    const-string v0, "photo_comment_without_text_posted"

    goto :goto_0

    :cond_1
    move v0, v2

    .line 1458398
    goto :goto_1

    :cond_2
    move v1, v2

    .line 1458399
    goto :goto_2

    .line 1458400
    :cond_3
    if-eqz v1, :cond_4

    .line 1458401
    const-string v0, "photo_comment_with_text_posted"

    goto :goto_0

    .line 1458402
    :cond_4
    if-eqz v0, :cond_5

    .line 1458403
    const-string v0, "text_only_comment_posted"

    goto :goto_0

    .line 1458404
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method
