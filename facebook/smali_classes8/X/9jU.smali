.class public final LX/9jU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/location/ImmutableLocation;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/9jo;

.field public final synthetic b:LX/9jW;


# direct methods
.method public constructor <init>(LX/9jW;LX/9jo;)V
    .locals 0

    .prologue
    .line 1529893
    iput-object p1, p0, LX/9jU;->b:LX/9jW;

    iput-object p2, p0, LX/9jU;->a:LX/9jo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1529894
    sget-object v0, LX/9jW;->a:Ljava/lang/Class;

    const-string v1, "Location not retrieved"

    invoke-static {v0, v1, p1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1529895
    iget-object v0, p0, LX/9jU;->b:LX/9jW;

    iget-object v1, p0, LX/9jU;->a:LX/9jo;

    invoke-static {v0, v1}, LX/9jW;->e(LX/9jW;LX/9jo;)V

    .line 1529896
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1529897
    check-cast p1, Lcom/facebook/location/ImmutableLocation;

    .line 1529898
    if-eqz p1, :cond_0

    .line 1529899
    sget-object v0, LX/9jW;->a:Ljava/lang/Class;

    const-string v1, "Location retrieved"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1529900
    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->l()Landroid/location/Location;

    move-result-object v0

    .line 1529901
    iget-object v1, p0, LX/9jU;->b:LX/9jW;

    iget-object v1, v1, LX/9jW;->c:LX/9jT;

    .line 1529902
    iput-object v0, v1, LX/9jT;->b:Landroid/location/Location;

    .line 1529903
    iget-object v1, p0, LX/9jU;->b:LX/9jW;

    iget-object v2, p0, LX/9jU;->a:LX/9jo;

    .line 1529904
    iput-object v0, v2, LX/9jo;->b:Landroid/location/Location;

    .line 1529905
    move-object v0, v2

    .line 1529906
    invoke-static {v1, v0}, LX/9jW;->d(LX/9jW;LX/9jo;)V

    .line 1529907
    :goto_0
    return-void

    .line 1529908
    :cond_0
    iget-object v0, p0, LX/9jU;->b:LX/9jW;

    iget-object v1, p0, LX/9jU;->a:LX/9jo;

    invoke-static {v0, v1}, LX/9jW;->e(LX/9jW;LX/9jo;)V

    goto :goto_0
.end method
