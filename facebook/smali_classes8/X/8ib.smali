.class public final LX/8ib;
.super Landroid/widget/BaseAdapter;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/sounds/configurator/AudioConfigurator;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/sounds/configurator/AudioConfigurator;)V
    .locals 1

    .prologue
    .line 1391609
    iput-object p1, p0, LX/8ib;->a:Lcom/facebook/sounds/configurator/AudioConfigurator;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 1391610
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/8ib;->b:Ljava/util/List;

    .line 1391611
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1391589
    iget-object v0, p0, LX/8ib;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1391590
    const v0, 0x2cbd702a

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1391591
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1391605
    iget-object v0, p0, LX/8ib;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1391606
    iget-object v0, p0, LX/8ib;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 1391607
    const v0, -0x5b9ce14c

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1391608
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 1391604
    iget-object v0, p0, LX/8ib;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1391603
    iget-object v0, p0, LX/8ib;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1391602
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1391592
    if-nez p2, :cond_0

    .line 1391593
    new-instance p2, Landroid/widget/TextView;

    iget-object v0, p0, LX/8ib;->a:Lcom/facebook/sounds/configurator/AudioConfigurator;

    invoke-virtual {v0}, Lcom/facebook/sounds/configurator/AudioConfigurator;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p2, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1391594
    new-instance v0, Landroid/widget/AbsListView$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    .line 1391595
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1391596
    const/high16 v0, 0x41400000    # 12.0f

    iget-object v1, p0, LX/8ib;->a:Lcom/facebook/sounds/configurator/AudioConfigurator;

    invoke-virtual {v1}, Lcom/facebook/sounds/configurator/AudioConfigurator;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v0, v1}, LX/8YO;->a(FLandroid/content/res/Resources;)I

    move-result v0

    .line 1391597
    invoke-virtual {p2, v0, v0, v0, v0}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1391598
    iget-object v0, p0, LX/8ib;->a:Lcom/facebook/sounds/configurator/AudioConfigurator;

    iget v0, v0, Lcom/facebook/sounds/configurator/AudioConfigurator;->h:I

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1391599
    :goto_0
    iget-object v0, p0, LX/8ib;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1391600
    return-object p2

    .line 1391601
    :cond_0
    check-cast p2, Landroid/widget/TextView;

    goto :goto_0
.end method
