.class public final LX/8lU;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/stickers/search/StickerSearchContainer;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/search/StickerSearchContainer;)V
    .locals 0

    .prologue
    .line 1397803
    iput-object p1, p0, LX/8lU;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 3

    .prologue
    .line 1397804
    iget-object v0, p0, LX/8lU;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-object v0, v0, Lcom/facebook/stickers/search/StickerSearchContainer;->y:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 1397805
    iget-object v0, p0, LX/8lU;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-object v0, v0, Lcom/facebook/stickers/search/StickerSearchContainer;->y:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTranslationY()F

    move-result v0

    int-to-float v2, p2

    sub-float/2addr v0, v2

    float-to-int v0, v0

    .line 1397806
    if-ge p1, v1, :cond_0

    neg-int v1, p1

    if-ge v0, v1, :cond_0

    .line 1397807
    neg-int v0, p1

    .line 1397808
    :cond_0
    iget-object v1, p0, LX/8lU;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    int-to-float v0, v0

    sget-object v2, LX/8lf;->JUMP_TO_VALUE:LX/8lf;

    invoke-static {v1, v0, v2}, Lcom/facebook/stickers/search/StickerSearchContainer;->a$redex0(Lcom/facebook/stickers/search/StickerSearchContainer;FLX/8lf;)V

    .line 1397809
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget-object v1, p0, LX/8lU;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget v1, v1, Lcom/facebook/stickers/search/StickerSearchContainer;->S:I

    if-le v0, v1, :cond_1

    .line 1397810
    iget-object v0, p0, LX/8lU;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-object v0, v0, Lcom/facebook/stickers/search/StickerSearchContainer;->h:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, LX/8lU;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    invoke-virtual {v1}, Lcom/facebook/stickers/search/StickerSearchContainer;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1397811
    :cond_1
    return-void
.end method
