.class public final LX/A5f;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/0P1",
        "<",
        "Ljava/lang/Long;",
        "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;)V
    .locals 0

    .prologue
    .line 1621680
    iput-object p1, p0, LX/A5f;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1621681
    iget-object v0, p0, LX/A5f;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    iget-object v0, v0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->B:LX/7kp;

    .line 1621682
    const-string v1, "QueryFriends"

    invoke-static {v0, v1}, LX/7kp;->a(LX/7kp;Ljava/lang/String;)V

    .line 1621683
    iget-object v0, p0, LX/A5f;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    iget-object v0, v0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->N:LX/2RQ;

    invoke-virtual {v0}, LX/2RQ;->a()LX/2RR;

    move-result-object v0

    iget-object v1, p0, LX/A5f;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    .line 1621684
    iget-object v2, v1, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->W:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    .line 1621685
    iget-boolean v3, v2, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->d:Z

    move v2, v3

    .line 1621686
    if-eqz v2, :cond_2

    iget-object v2, v1, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->G:LX/3Oq;

    sget-object v3, LX/3Oq;->ME:LX/3Oq;

    invoke-static {v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    :goto_0
    move-object v1, v2

    .line 1621687
    iput-object v1, v0, LX/2RR;->c:Ljava/util/Collection;

    .line 1621688
    move-object v0, v0

    .line 1621689
    sget-object v1, LX/2RS;->NAME:LX/2RS;

    .line 1621690
    iput-object v1, v0, LX/2RR;->n:LX/2RS;

    .line 1621691
    move-object v0, v0

    .line 1621692
    iget-object v1, p0, LX/A5f;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    iget-object v1, v1, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->M:LX/3LP;

    invoke-virtual {v1, v0}, LX/3LP;->a(LX/2RR;)LX/3On;

    move-result-object v1

    .line 1621693
    new-instance v0, LX/A5j;

    iget-object v2, p0, LX/A5f;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    invoke-direct {v0, v2}, LX/A5j;-><init>(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;)V

    .line 1621694
    :try_start_0
    iget-object v2, p0, LX/A5f;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    const/4 v3, 0x1

    .line 1621695
    invoke-static {v2, v0, v3}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->a(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;LX/A5j;Z)V

    .line 1621696
    :cond_0
    :goto_1
    invoke-interface {v1}, LX/3On;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1621697
    invoke-interface {v1}, LX/3On;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/user/model/User;

    invoke-static {v4}, LX/8uo;->a(Lcom/facebook/user/model/User;)Lcom/facebook/user/model/User;

    move-result-object v4

    .line 1621698
    new-instance v5, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    invoke-direct {v5, v4}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;-><init>(Lcom/facebook/user/model/User;)V

    .line 1621699
    iget-object v6, v4, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v6, v6

    .line 1621700
    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v0, v6, v5}, LX/A5j;->a(Ljava/lang/Long;Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;)V

    .line 1621701
    if-eqz v3, :cond_0

    iget-object v6, v2, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->W:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    .line 1621702
    iget-object v7, v6, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->j:LX/0Px;

    move-object v6, v7

    .line 1621703
    iget-object v7, v4, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v4, v7

    .line 1621704
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v6, v4}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1621705
    iget-object v4, v2, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->O:Ljava/util/Set;

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1621706
    :cond_1
    invoke-interface {v1}, LX/3On;->close()V

    .line 1621707
    iget-object v1, v0, LX/A5j;->b:Ljava/util/Map;

    invoke-static {v1}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v1

    move-object v0, v1

    .line 1621708
    iget-object v1, p0, LX/A5f;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    iget-object v1, v1, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->B:LX/7kp;

    .line 1621709
    const-string v2, "QueryFriends"

    invoke-static {v1, v2}, LX/7kp;->c(LX/7kp;Ljava/lang/String;)V

    .line 1621710
    return-object v0

    .line 1621711
    :catchall_0
    move-exception v0

    invoke-interface {v1}, LX/3On;->close()V

    throw v0

    :cond_2
    iget-object v2, v1, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->G:LX/3Oq;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    goto :goto_0
.end method
