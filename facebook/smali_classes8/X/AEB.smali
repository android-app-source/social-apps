.class public LX/AEB;
.super LX/2y5;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1PW;",
        ">",
        "LX/2y5",
        "<TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:LX/2yS;

.field private final b:LX/1nA;

.field private final c:LX/2v5;

.field private final d:LX/1Nt;

.field private final e:LX/2yT;


# direct methods
.method public constructor <init>(LX/2yS;Lcom/facebook/attachments/angora/actionbutton/AppOpenActionButtonPartDefinition;LX/1nA;LX/2v5;LX/2yT;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1646121
    invoke-direct {p0}, LX/2y5;-><init>()V

    .line 1646122
    iput-object p1, p0, LX/AEB;->a:LX/2yS;

    .line 1646123
    iput-object p3, p0, LX/AEB;->b:LX/1nA;

    .line 1646124
    iput-object p4, p0, LX/AEB;->c:LX/2v5;

    .line 1646125
    iput-object p2, p0, LX/AEB;->d:LX/1Nt;

    .line 1646126
    iput-object p5, p0, LX/AEB;->e:LX/2yT;

    .line 1646127
    return-void
.end method

.method public static a(LX/0QB;)LX/AEB;
    .locals 9

    .prologue
    .line 1646128
    const-class v1, LX/AEB;

    monitor-enter v1

    .line 1646129
    :try_start_0
    sget-object v0, LX/AEB;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1646130
    sput-object v2, LX/AEB;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1646131
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1646132
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1646133
    new-instance v3, LX/AEB;

    invoke-static {v0}, LX/2yS;->a(LX/0QB;)LX/2yS;

    move-result-object v4

    check-cast v4, LX/2yS;

    invoke-static {v0}, Lcom/facebook/attachments/angora/actionbutton/AppOpenActionButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/attachments/angora/actionbutton/AppOpenActionButtonPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/attachments/angora/actionbutton/AppOpenActionButtonPartDefinition;

    invoke-static {v0}, LX/1nA;->a(LX/0QB;)LX/1nA;

    move-result-object v6

    check-cast v6, LX/1nA;

    invoke-static {v0}, LX/2v5;->b(LX/0QB;)LX/2v5;

    move-result-object v7

    check-cast v7, LX/2v5;

    const-class v8, LX/2yT;

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/2yT;

    invoke-direct/range {v3 .. v8}, LX/AEB;-><init>(LX/2yS;Lcom/facebook/attachments/angora/actionbutton/AppOpenActionButtonPartDefinition;LX/1nA;LX/2v5;LX/2yT;)V

    .line 1646134
    move-object v0, v3

    .line 1646135
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1646136
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/AEB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1646137
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1646138
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/2v5;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1646139
    const/4 v0, 0x0

    .line 1646140
    if-nez p1, :cond_2

    .line 1646141
    :cond_0
    :goto_0
    move-object v0, v0

    .line 1646142
    if-nez v0, :cond_1

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v0

    :cond_1
    return-object v0

    .line 1646143
    :cond_2
    const v1, -0x1e53800c

    invoke-static {p1, v1}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 1646144
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->k()Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    move-result-object v2

    .line 1646145
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 1646146
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->o()Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/2v5;->a(Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;)Ljava/lang/String;

    move-result-object v0

    .line 1646147
    if-nez v0, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z
    .locals 2

    .prologue
    .line 1646148
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ae()Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->BUTTON_WITH_TEXT_ONLY:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ae()Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->VIDEO_DR_STYLE:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Nt;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Landroid/view/View;",
            ":",
            "LX/35p;",
            ">()",
            "LX/1Nt",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;*TE;TV;>;"
        }
    .end annotation

    .prologue
    .line 1646149
    iget-object v0, p0, LX/AEB;->d:LX/1Nt;

    return-object v0
.end method

.method public final a(LX/1De;LX/1PW;Lcom/facebook/feed/rows/core/props/FeedProps;Z)LX/AE0;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "TE;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;Z)",
            "LX/AE0;"
        }
    .end annotation

    .prologue
    .line 1646150
    iget-object v0, p3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1646151
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1646152
    const v1, -0x1e53800c

    invoke-static {v0, v1}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 1646153
    invoke-static {v1}, LX/AEB;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1646154
    const/4 v0, 0x0

    .line 1646155
    :goto_0
    return-object v0

    :cond_0
    iget-object v2, p0, LX/AEB;->e:LX/2yT;

    const/4 v3, 0x2

    iget-object v4, p0, LX/AEB;->a:LX/2yS;

    invoke-virtual {v2, p4, v3, v4}, LX/2yT;->a(ZILX/2yS;)LX/AE0;

    move-result-object v2

    iget-object v3, p0, LX/AEB;->c:LX/2v5;

    invoke-static {v3, v0, v1}, LX/AEB;->a(LX/2v5;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Ljava/lang/String;

    move-result-object v0

    .line 1646156
    iput-object v0, v2, LX/AE0;->f:Ljava/lang/CharSequence;

    .line 1646157
    move-object v0, v2

    .line 1646158
    iget-object v2, p0, LX/AEB;->b:LX/1nA;

    invoke-virtual {v2, p3, v1}, LX/1nA;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Landroid/view/View$OnClickListener;

    move-result-object v1

    .line 1646159
    iput-object v1, v0, LX/AE0;->o:Landroid/view/View$OnClickListener;

    .line 1646160
    move-object v0, v0

    .line 1646161
    goto :goto_0
.end method
