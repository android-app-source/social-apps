.class public final LX/9sr;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$GroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionCommonFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeJobDetailActionFieldsModel$JobOpeningModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPlaysActionFieldsModel$MatchPageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:Lcom/facebook/graphql/enums/GraphQLPagePhotoSourceType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public M:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenNearbyPlacesActionFieldsModel$PlacesQueryLocationPageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public N:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public O:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public P:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Q:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public R:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public S:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public T:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$RelatedUsersModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public U:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public V:Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public W:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public X:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Y:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Z:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public a:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aa:Z

.field public ab:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ac:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ad:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSendMessageAsPageFieldsModel$ThreadKeyModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ae:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public af:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ag:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Z

.field public h:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel$ActionOgObjectModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPageAlbumActionFragmentModel$AlbumModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Z

.field public m:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeePageCommerceProductsFieldsModel$CollectionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$CommentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/graphql/enums/GraphQLFundraiserSupportersConnectionType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$FriendModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewFundraiserSupportersActionFieldsModel$FundraiserModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1559247
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;)LX/9sr;
    .locals 2

    .prologue
    .line 1559248
    new-instance v0, LX/9sr;

    invoke-direct {v0}, LX/9sr;-><init>()V

    .line 1559249
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1559250
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aj()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->b:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1559251
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ak()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->c:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1559252
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->al()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->d:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    .line 1559253
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->am()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1559254
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->an()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->f:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1559255
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->hh_()Z

    move-result v1

    iput-boolean v1, v0, LX/9sr;->g:Z

    .line 1559256
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ao()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel$ActionOgObjectModel;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->h:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel$ActionOgObjectModel;

    .line 1559257
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->k()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->i:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    .line 1559258
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ap()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPageAlbumActionFragmentModel$AlbumModel;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->j:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPageAlbumActionFragmentModel$AlbumModel;

    .line 1559259
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aq()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->k:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;

    .line 1559260
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->n()Z

    move-result v1

    iput-boolean v1, v0, LX/9sr;->l:Z

    .line 1559261
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ar()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeePageCommerceProductsFieldsModel$CollectionModel;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->m:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeePageCommerceProductsFieldsModel$CollectionModel;

    .line 1559262
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->as()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$CommentModel;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->n:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$CommentModel;

    .line 1559263
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->q()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->o:Ljava/lang/String;

    .line 1559264
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->r()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->p:Ljava/lang/String;

    .line 1559265
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->at()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->q:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;

    .line 1559266
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->t()Lcom/facebook/graphql/enums/GraphQLFundraiserSupportersConnectionType;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->r:Lcom/facebook/graphql/enums/GraphQLFundraiserSupportersConnectionType;

    .line 1559267
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->u()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->s:Ljava/lang/String;

    .line 1559268
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->v()Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->t:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    .line 1559269
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->au()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->u:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;

    .line 1559270
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->av()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->v:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;

    .line 1559271
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aw()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$FriendModel;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->w:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$FriendModel;

    .line 1559272
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->z()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->x:Ljava/lang/String;

    .line 1559273
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ax()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewFundraiserSupportersActionFieldsModel$FundraiserModel;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->y:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewFundraiserSupportersActionFieldsModel$FundraiserModel;

    .line 1559274
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ay()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->z:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    .line 1559275
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->az()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$GroupModel;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->A:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$GroupModel;

    .line 1559276
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->D()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->B:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1559277
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ai()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->C:LX/0Px;

    .line 1559278
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aA()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeJobDetailActionFieldsModel$JobOpeningModel;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->D:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeJobDetailActionFieldsModel$JobOpeningModel;

    .line 1559279
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aB()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->E:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 1559280
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aC()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPlaysActionFieldsModel$MatchPageModel;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->F:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPlaysActionFieldsModel$MatchPageModel;

    .line 1559281
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->H()Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->G:Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    .line 1559282
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aD()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->H:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;

    .line 1559283
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aE()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->I:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    .line 1559284
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->K()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->J:Ljava/lang/String;

    .line 1559285
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->L()Lcom/facebook/graphql/enums/GraphQLPagePhotoSourceType;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->K:Lcom/facebook/graphql/enums/GraphQLPagePhotoSourceType;

    .line 1559286
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->M()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->L:Ljava/lang/String;

    .line 1559287
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aF()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenNearbyPlacesActionFieldsModel$PlacesQueryLocationPageModel;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->M:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenNearbyPlacesActionFieldsModel$PlacesQueryLocationPageModel;

    .line 1559288
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->O()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->N:Ljava/lang/String;

    .line 1559289
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->P()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->O:Ljava/lang/String;

    .line 1559290
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aG()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->P:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;

    .line 1559291
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aH()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->Q:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    .line 1559292
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->S()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->R:Ljava/lang/String;

    .line 1559293
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->T()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->S:Ljava/lang/String;

    .line 1559294
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->U()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->T:LX/0Px;

    .line 1559295
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aI()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->U:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel;

    .line 1559296
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aJ()Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->V:Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;

    .line 1559297
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->X()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->W:Ljava/lang/String;

    .line 1559298
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->Y()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->X:Ljava/lang/String;

    .line 1559299
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aK()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->Y:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;

    .line 1559300
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aL()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->Z:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;

    .line 1559301
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ab()Z

    move-result v1

    iput-boolean v1, v0, LX/9sr;->aa:Z

    .line 1559302
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ac()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->ab:Ljava/lang/String;

    .line 1559303
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aM()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->ac:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel;

    .line 1559304
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aN()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSendMessageAsPageFieldsModel$ThreadKeyModel;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->ad:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSendMessageAsPageFieldsModel$ThreadKeyModel;

    .line 1559305
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->af()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->ae:Ljava/lang/String;

    .line 1559306
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ag()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->af:Ljava/lang/String;

    .line 1559307
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aO()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-result-object v1

    iput-object v1, v0, LX/9sr;->ag:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    .line 1559308
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;
    .locals 59

    .prologue
    .line 1559309
    new-instance v1, LX/186;

    const/16 v2, 0x80

    invoke-direct {v1, v2}, LX/186;-><init>(I)V

    .line 1559310
    move-object/from16 v0, p0

    iget-object v2, v0, LX/9sr;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1559311
    move-object/from16 v0, p0

    iget-object v3, v0, LX/9sr;->b:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-static {v1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1559312
    move-object/from16 v0, p0

    iget-object v4, v0, LX/9sr;->c:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-static {v1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1559313
    move-object/from16 v0, p0

    iget-object v5, v0, LX/9sr;->d:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    invoke-static {v1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1559314
    move-object/from16 v0, p0

    iget-object v6, v0, LX/9sr;->e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-static {v1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1559315
    move-object/from16 v0, p0

    iget-object v7, v0, LX/9sr;->f:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-static {v1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1559316
    move-object/from16 v0, p0

    iget-object v8, v0, LX/9sr;->h:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel$ActionOgObjectModel;

    invoke-static {v1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1559317
    move-object/from16 v0, p0

    iget-object v9, v0, LX/9sr;->i:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    invoke-virtual {v1, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    .line 1559318
    move-object/from16 v0, p0

    iget-object v10, v0, LX/9sr;->j:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPageAlbumActionFragmentModel$AlbumModel;

    invoke-static {v1, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1559319
    move-object/from16 v0, p0

    iget-object v11, v0, LX/9sr;->k:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;

    invoke-static {v1, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1559320
    move-object/from16 v0, p0

    iget-object v12, v0, LX/9sr;->m:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeePageCommerceProductsFieldsModel$CollectionModel;

    invoke-static {v1, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1559321
    move-object/from16 v0, p0

    iget-object v13, v0, LX/9sr;->n:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$CommentModel;

    invoke-static {v1, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 1559322
    move-object/from16 v0, p0

    iget-object v14, v0, LX/9sr;->o:Ljava/lang/String;

    invoke-virtual {v1, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 1559323
    move-object/from16 v0, p0

    iget-object v15, v0, LX/9sr;->p:Ljava/lang/String;

    invoke-virtual {v1, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 1559324
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->q:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 1559325
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->r:Lcom/facebook/graphql/enums/GraphQLFundraiserSupportersConnectionType;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v17

    .line 1559326
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->s:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    .line 1559327
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->t:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v19

    .line 1559328
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->u:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 1559329
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->v:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 1559330
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->w:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$FriendModel;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 1559331
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->x:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v23

    .line 1559332
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->y:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewFundraiserSupportersActionFieldsModel$FundraiserModel;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v24

    .line 1559333
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->z:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v25

    .line 1559334
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->A:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$GroupModel;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v26

    .line 1559335
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->B:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-virtual {v1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v27

    .line 1559336
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->C:LX/0Px;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v28

    .line 1559337
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->D:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeJobDetailActionFieldsModel$JobOpeningModel;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v29

    .line 1559338
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->E:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v30

    .line 1559339
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->F:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPlaysActionFieldsModel$MatchPageModel;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v31

    .line 1559340
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->G:Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    invoke-virtual {v1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v32

    .line 1559341
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->H:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v33

    .line 1559342
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->I:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v34

    .line 1559343
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->J:Ljava/lang/String;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    invoke-virtual {v1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v35

    .line 1559344
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->K:Lcom/facebook/graphql/enums/GraphQLPagePhotoSourceType;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    invoke-virtual {v1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v36

    .line 1559345
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->L:Ljava/lang/String;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    invoke-virtual {v1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v37

    .line 1559346
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->M:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenNearbyPlacesActionFieldsModel$PlacesQueryLocationPageModel;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v38

    .line 1559347
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->N:Ljava/lang/String;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    invoke-virtual {v1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v39

    .line 1559348
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->O:Ljava/lang/String;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    invoke-virtual {v1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v40

    .line 1559349
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->P:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v41

    .line 1559350
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->Q:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-object/from16 v42, v0

    move-object/from16 v0, v42

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v42

    .line 1559351
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->R:Ljava/lang/String;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    invoke-virtual {v1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v43

    .line 1559352
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->S:Ljava/lang/String;

    move-object/from16 v44, v0

    move-object/from16 v0, v44

    invoke-virtual {v1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v44

    .line 1559353
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->T:LX/0Px;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v45

    .line 1559354
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->U:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel;

    move-object/from16 v46, v0

    move-object/from16 v0, v46

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v46

    .line 1559355
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->V:Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;

    move-object/from16 v47, v0

    move-object/from16 v0, v47

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v47

    .line 1559356
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->W:Ljava/lang/String;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    invoke-virtual {v1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v48

    .line 1559357
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->X:Ljava/lang/String;

    move-object/from16 v49, v0

    move-object/from16 v0, v49

    invoke-virtual {v1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v49

    .line 1559358
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->Y:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;

    move-object/from16 v50, v0

    move-object/from16 v0, v50

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v50

    .line 1559359
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->Z:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;

    move-object/from16 v51, v0

    move-object/from16 v0, v51

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v51

    .line 1559360
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->ab:Ljava/lang/String;

    move-object/from16 v52, v0

    move-object/from16 v0, v52

    invoke-virtual {v1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v52

    .line 1559361
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->ac:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel;

    move-object/from16 v53, v0

    move-object/from16 v0, v53

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v53

    .line 1559362
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->ad:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSendMessageAsPageFieldsModel$ThreadKeyModel;

    move-object/from16 v54, v0

    move-object/from16 v0, v54

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v54

    .line 1559363
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->ae:Ljava/lang/String;

    move-object/from16 v55, v0

    move-object/from16 v0, v55

    invoke-virtual {v1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v55

    .line 1559364
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->af:Ljava/lang/String;

    move-object/from16 v56, v0

    move-object/from16 v0, v56

    invoke-virtual {v1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v56

    .line 1559365
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sr;->ag:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-object/from16 v57, v0

    move-object/from16 v0, v57

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v57

    .line 1559366
    const/16 v58, 0x3b

    move/from16 v0, v58

    invoke-virtual {v1, v0}, LX/186;->c(I)V

    .line 1559367
    const/16 v58, 0x0

    move/from16 v0, v58

    invoke-virtual {v1, v0, v2}, LX/186;->b(II)V

    .line 1559368
    const/4 v2, 0x1

    invoke-virtual {v1, v2, v3}, LX/186;->b(II)V

    .line 1559369
    const/4 v2, 0x2

    invoke-virtual {v1, v2, v4}, LX/186;->b(II)V

    .line 1559370
    const/4 v2, 0x3

    invoke-virtual {v1, v2, v5}, LX/186;->b(II)V

    .line 1559371
    const/4 v2, 0x4

    invoke-virtual {v1, v2, v6}, LX/186;->b(II)V

    .line 1559372
    const/4 v2, 0x5

    invoke-virtual {v1, v2, v7}, LX/186;->b(II)V

    .line 1559373
    const/4 v2, 0x6

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/9sr;->g:Z

    invoke-virtual {v1, v2, v3}, LX/186;->a(IZ)V

    .line 1559374
    const/4 v2, 0x7

    invoke-virtual {v1, v2, v8}, LX/186;->b(II)V

    .line 1559375
    const/16 v2, 0x8

    invoke-virtual {v1, v2, v9}, LX/186;->b(II)V

    .line 1559376
    const/16 v2, 0x9

    invoke-virtual {v1, v2, v10}, LX/186;->b(II)V

    .line 1559377
    const/16 v2, 0xa

    invoke-virtual {v1, v2, v11}, LX/186;->b(II)V

    .line 1559378
    const/16 v2, 0xb

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/9sr;->l:Z

    invoke-virtual {v1, v2, v3}, LX/186;->a(IZ)V

    .line 1559379
    const/16 v2, 0xc

    invoke-virtual {v1, v2, v12}, LX/186;->b(II)V

    .line 1559380
    const/16 v2, 0xd

    invoke-virtual {v1, v2, v13}, LX/186;->b(II)V

    .line 1559381
    const/16 v2, 0xe

    invoke-virtual {v1, v2, v14}, LX/186;->b(II)V

    .line 1559382
    const/16 v2, 0xf

    invoke-virtual {v1, v2, v15}, LX/186;->b(II)V

    .line 1559383
    const/16 v2, 0x10

    move/from16 v0, v16

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559384
    const/16 v2, 0x11

    move/from16 v0, v17

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559385
    const/16 v2, 0x12

    move/from16 v0, v18

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559386
    const/16 v2, 0x13

    move/from16 v0, v19

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559387
    const/16 v2, 0x14

    move/from16 v0, v20

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559388
    const/16 v2, 0x15

    move/from16 v0, v21

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559389
    const/16 v2, 0x16

    move/from16 v0, v22

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559390
    const/16 v2, 0x17

    move/from16 v0, v23

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559391
    const/16 v2, 0x18

    move/from16 v0, v24

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559392
    const/16 v2, 0x19

    move/from16 v0, v25

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559393
    const/16 v2, 0x1a

    move/from16 v0, v26

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559394
    const/16 v2, 0x1b

    move/from16 v0, v27

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559395
    const/16 v2, 0x1c

    move/from16 v0, v28

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559396
    const/16 v2, 0x1d

    move/from16 v0, v29

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559397
    const/16 v2, 0x1e

    move/from16 v0, v30

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559398
    const/16 v2, 0x1f

    move/from16 v0, v31

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559399
    const/16 v2, 0x20

    move/from16 v0, v32

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559400
    const/16 v2, 0x21

    move/from16 v0, v33

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559401
    const/16 v2, 0x22

    move/from16 v0, v34

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559402
    const/16 v2, 0x23

    move/from16 v0, v35

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559403
    const/16 v2, 0x24

    move/from16 v0, v36

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559404
    const/16 v2, 0x25

    move/from16 v0, v37

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559405
    const/16 v2, 0x26

    move/from16 v0, v38

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559406
    const/16 v2, 0x27

    move/from16 v0, v39

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559407
    const/16 v2, 0x28

    move/from16 v0, v40

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559408
    const/16 v2, 0x29

    move/from16 v0, v41

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559409
    const/16 v2, 0x2a

    move/from16 v0, v42

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559410
    const/16 v2, 0x2b

    move/from16 v0, v43

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559411
    const/16 v2, 0x2c

    move/from16 v0, v44

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559412
    const/16 v2, 0x2d

    move/from16 v0, v45

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559413
    const/16 v2, 0x2e

    move/from16 v0, v46

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559414
    const/16 v2, 0x2f

    move/from16 v0, v47

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559415
    const/16 v2, 0x30

    move/from16 v0, v48

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559416
    const/16 v2, 0x31

    move/from16 v0, v49

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559417
    const/16 v2, 0x32

    move/from16 v0, v50

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559418
    const/16 v2, 0x33

    move/from16 v0, v51

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559419
    const/16 v2, 0x34

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/9sr;->aa:Z

    invoke-virtual {v1, v2, v3}, LX/186;->a(IZ)V

    .line 1559420
    const/16 v2, 0x35

    move/from16 v0, v52

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559421
    const/16 v2, 0x36

    move/from16 v0, v53

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559422
    const/16 v2, 0x37

    move/from16 v0, v54

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559423
    const/16 v2, 0x38

    move/from16 v0, v55

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559424
    const/16 v2, 0x39

    move/from16 v0, v56

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559425
    const/16 v2, 0x3a

    move/from16 v0, v57

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1559426
    invoke-virtual {v1}, LX/186;->d()I

    move-result v2

    .line 1559427
    invoke-virtual {v1, v2}, LX/186;->d(I)V

    .line 1559428
    invoke-virtual {v1}, LX/186;->e()[B

    move-result-object v1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 1559429
    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1559430
    new-instance v1, LX/15i;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1559431
    new-instance v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-direct {v2, v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;-><init>(LX/15i;)V

    .line 1559432
    return-object v2
.end method
