.class public final LX/AO9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field public final synthetic a:Landroid/widget/EditText;

.field public final synthetic b:LX/2EJ;

.field public final synthetic c:LX/AOB;


# direct methods
.method public constructor <init>(LX/AOB;Landroid/widget/EditText;LX/2EJ;)V
    .locals 0

    .prologue
    .line 1668735
    iput-object p1, p0, LX/AO9;->c:LX/AOB;

    iput-object p2, p0, LX/AO9;->a:Landroid/widget/EditText;

    iput-object p3, p0, LX/AO9;->b:LX/2EJ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1668736
    const/4 v0, 0x6

    if-ne p2, v0, :cond_0

    .line 1668737
    iget-object v0, p0, LX/AO9;->c:LX/AOB;

    iget-object v0, v0, LX/AOB;->f:Landroid/content/Context;

    const-string v2, "input_method"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1668738
    iget-object v2, p0, LX/AO9;->a:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1668739
    iget-object v0, p0, LX/AO9;->b:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->dismiss()V

    .line 1668740
    iget-object v0, p0, LX/AO9;->c:LX/AOB;

    iget-object v1, p0, LX/AO9;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/AOB;->a$redex0(LX/AOB;Ljava/lang/String;)V

    .line 1668741
    const/4 v0, 0x1

    .line 1668742
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method
