.class public LX/9n1;
.super LX/5pb;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "HeadlessJsTaskSupport"
.end annotation


# direct methods
.method public constructor <init>(LX/5pY;)V
    .locals 0

    .prologue
    .line 1536605
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 1536606
    return-void
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1536607
    const-string v0, "HeadlessJsTaskSupport"

    return-object v0
.end method

.method public notifyTaskFinished(I)V
    .locals 5
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1536608
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 1536609
    invoke-static {v0}, LX/9mv;->a(LX/5pX;)LX/9mv;

    move-result-object v0

    .line 1536610
    invoke-virtual {v0, p1}, LX/9mv;->b(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1536611
    invoke-virtual {v0, p1}, LX/9mv;->a(I)V

    .line 1536612
    :goto_0
    return-void

    .line 1536613
    :cond_0
    const-class v0, LX/9n1;

    const-string v1, "Tried to finish non-active task with id %d. Did it time out?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/03J;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
