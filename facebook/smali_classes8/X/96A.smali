.class public final enum LX/96A;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/96A;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/96A;

.field public static final enum CATEGORY_PICKER:LX/96A;

.field public static final enum CITY_PICKER:LX/96A;

.field public static final enum DEDUPER:LX/96A;

.field public static final enum FORM:LX/96A;

.field public static final enum HOME_CREATION:LX/96A;

.field public static final enum PHOTO_PICKER:LX/96A;


# instance fields
.field public final logValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1438261
    new-instance v0, LX/96A;

    const-string v1, "CATEGORY_PICKER"

    const-string v2, "category_picker"

    invoke-direct {v0, v1, v4, v2}, LX/96A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/96A;->CATEGORY_PICKER:LX/96A;

    .line 1438262
    new-instance v0, LX/96A;

    const-string v1, "CITY_PICKER"

    const-string v2, "city_picker"

    invoke-direct {v0, v1, v5, v2}, LX/96A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/96A;->CITY_PICKER:LX/96A;

    .line 1438263
    new-instance v0, LX/96A;

    const-string v1, "PHOTO_PICKER"

    const-string v2, "photo_picker"

    invoke-direct {v0, v1, v6, v2}, LX/96A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/96A;->PHOTO_PICKER:LX/96A;

    .line 1438264
    new-instance v0, LX/96A;

    const-string v1, "HOME_CREATION"

    const-string v2, "home_creation"

    invoke-direct {v0, v1, v7, v2}, LX/96A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/96A;->HOME_CREATION:LX/96A;

    .line 1438265
    new-instance v0, LX/96A;

    const-string v1, "DEDUPER"

    const-string v2, "deduper"

    invoke-direct {v0, v1, v8, v2}, LX/96A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/96A;->DEDUPER:LX/96A;

    .line 1438266
    new-instance v0, LX/96A;

    const-string v1, "FORM"

    const/4 v2, 0x5

    const-string v3, "form"

    invoke-direct {v0, v1, v2, v3}, LX/96A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/96A;->FORM:LX/96A;

    .line 1438267
    const/4 v0, 0x6

    new-array v0, v0, [LX/96A;

    sget-object v1, LX/96A;->CATEGORY_PICKER:LX/96A;

    aput-object v1, v0, v4

    sget-object v1, LX/96A;->CITY_PICKER:LX/96A;

    aput-object v1, v0, v5

    sget-object v1, LX/96A;->PHOTO_PICKER:LX/96A;

    aput-object v1, v0, v6

    sget-object v1, LX/96A;->HOME_CREATION:LX/96A;

    aput-object v1, v0, v7

    sget-object v1, LX/96A;->DEDUPER:LX/96A;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/96A;->FORM:LX/96A;

    aput-object v2, v0, v1

    sput-object v0, LX/96A;->$VALUES:[LX/96A;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1438256
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1438257
    iput-object p3, p0, LX/96A;->logValue:Ljava/lang/String;

    .line 1438258
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/96A;
    .locals 1

    .prologue
    .line 1438259
    const-class v0, LX/96A;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/96A;

    return-object v0
.end method

.method public static values()[LX/96A;
    .locals 1

    .prologue
    .line 1438260
    sget-object v0, LX/96A;->$VALUES:[LX/96A;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/96A;

    return-object v0
.end method
