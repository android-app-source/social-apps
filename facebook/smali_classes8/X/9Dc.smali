.class public final enum LX/9Dc;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9Dc;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9Dc;

.field public static final enum PRIVACY_EDUCATION:LX/9Dc;

.field public static final enum UNKNOWN:LX/9Dc;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1455868
    new-instance v0, LX/9Dc;

    const-string v1, "PRIVACY_EDUCATION"

    invoke-direct {v0, v1, v2}, LX/9Dc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9Dc;->PRIVACY_EDUCATION:LX/9Dc;

    .line 1455869
    new-instance v0, LX/9Dc;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, LX/9Dc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9Dc;->UNKNOWN:LX/9Dc;

    .line 1455870
    const/4 v0, 0x2

    new-array v0, v0, [LX/9Dc;

    sget-object v1, LX/9Dc;->PRIVACY_EDUCATION:LX/9Dc;

    aput-object v1, v0, v2

    sget-object v1, LX/9Dc;->UNKNOWN:LX/9Dc;

    aput-object v1, v0, v3

    sput-object v0, LX/9Dc;->$VALUES:[LX/9Dc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1455871
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9Dc;
    .locals 1

    .prologue
    .line 1455872
    const-class v0, LX/9Dc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9Dc;

    return-object v0
.end method

.method public static values()[LX/9Dc;
    .locals 1

    .prologue
    .line 1455873
    sget-object v0, LX/9Dc;->$VALUES:[LX/9Dc;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9Dc;

    return-object v0
.end method
