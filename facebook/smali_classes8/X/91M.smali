.class public LX/91M;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/91L;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1430537
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/91M;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1430538
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1430539
    iput-object p1, p0, LX/91M;->b:LX/0Ot;

    .line 1430540
    return-void
.end method

.method public static a(LX/0QB;)LX/91M;
    .locals 4

    .prologue
    .line 1430541
    const-class v1, LX/91M;

    monitor-enter v1

    .line 1430542
    :try_start_0
    sget-object v0, LX/91M;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1430543
    sput-object v2, LX/91M;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1430544
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1430545
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1430546
    new-instance v3, LX/91M;

    const/16 p0, 0x19a1

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/91M;-><init>(LX/0Ot;)V

    .line 1430547
    move-object v0, v3

    .line 1430548
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1430549
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/91M;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1430550
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1430551
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 23

    .prologue
    .line 1430552
    check-cast p2, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;

    .line 1430553
    move-object/from16 v0, p0

    iget-object v1, v0, LX/91M;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponentSpec;

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->a:Landroid/net/Uri;

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->b:Ljava/lang/CharSequence;

    move-object/from16 v0, p2

    iget v5, v0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->c:I

    move-object/from16 v0, p2

    iget v6, v0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->d:I

    move-object/from16 v0, p2

    iget v7, v0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->e:I

    move-object/from16 v0, p2

    iget-object v8, v0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->f:Ljava/lang/CharSequence;

    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->g:Ljava/lang/CharSequence;

    move-object/from16 v0, p2

    iget-object v10, v0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->h:LX/1dQ;

    move-object/from16 v0, p2

    iget-object v11, v0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->i:LX/1dQ;

    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->j:LX/1dQ;

    move-object/from16 v0, p2

    iget v13, v0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->k:I

    move-object/from16 v0, p2

    iget-object v14, v0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->l:Lcom/facebook/common/callercontext/CallerContext;

    move-object/from16 v0, p2

    iget-object v15, v0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->m:LX/4Ab;

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->n:LX/1dc;

    move-object/from16 v16, v0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->o:Landroid/net/Uri;

    move-object/from16 v17, v0

    move-object/from16 v0, p2

    iget v0, v0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->p:I

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->q:I

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->r:I

    move/from16 v20, v0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->s:Ljava/lang/CharSequence;

    move-object/from16 v21, v0

    move-object/from16 v0, p2

    iget-boolean v0, v0, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->t:Z

    move/from16 v22, v0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v22}, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponentSpec;->a(LX/1De;Landroid/net/Uri;Ljava/lang/CharSequence;IIILjava/lang/CharSequence;Ljava/lang/CharSequence;LX/1dQ;LX/1dQ;LX/1dQ;ILcom/facebook/common/callercontext/CallerContext;LX/4Ab;LX/1dc;Landroid/net/Uri;IIILjava/lang/CharSequence;Z)LX/1Dg;

    move-result-object v1

    .line 1430554
    return-object v1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1430555
    invoke-static {}, LX/1dS;->b()V

    .line 1430556
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/91L;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1430557
    new-instance v1, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;

    invoke-direct {v1, p0}, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;-><init>(LX/91M;)V

    .line 1430558
    sget-object v2, LX/91M;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/91L;

    .line 1430559
    if-nez v2, :cond_0

    .line 1430560
    new-instance v2, LX/91L;

    invoke-direct {v2}, LX/91L;-><init>()V

    .line 1430561
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/91L;->a$redex0(LX/91L;LX/1De;IILcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;)V

    .line 1430562
    move-object v1, v2

    .line 1430563
    move-object v0, v1

    .line 1430564
    return-object v0
.end method
