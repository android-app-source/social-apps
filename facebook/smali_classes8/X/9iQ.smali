.class public LX/9iQ;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "LX/74w;",
        ">",
        "Lcom/facebook/widget/CustomFrameLayout;"
    }
.end annotation


# instance fields
.field private final a:[Ljava/lang/Object;

.field public b:LX/74w;

.field public c:Lcom/facebook/widget/images/UrlImage;

.field public d:LX/75D;

.field public e:Z

.field public f:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1527808
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1527809
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, LX/9iQ;->a:[Ljava/lang/Object;

    .line 1527810
    const-class v0, LX/9iQ;

    invoke-static {v0, p0}, LX/9iQ;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1527811
    const v0, 0x7f030f52

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1527812
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, LX/9iQ;->f:Ljava/util/LinkedList;

    .line 1527813
    const v0, 0x7f0d1063

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/images/UrlImage;

    iput-object v0, p0, LX/9iQ;->c:Lcom/facebook/widget/images/UrlImage;

    .line 1527814
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    iput-object v0, p0, LX/9iQ;->g:Lcom/google/common/util/concurrent/SettableFuture;

    .line 1527815
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/9iQ;

    invoke-static {p0}, LX/75D;->b(LX/0QB;)LX/75D;

    move-result-object p0

    check-cast p0, LX/75D;

    iput-object p0, p1, LX/9iQ;->d:LX/75D;

    return-void
.end method

.method public static j(LX/9iQ;)V
    .locals 3

    .prologue
    .line 1527797
    iget-object v1, p0, LX/9iQ;->a:[Ljava/lang/Object;

    monitor-enter v1

    .line 1527798
    :try_start_0
    iget-boolean v0, p0, LX/9iQ;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9iQ;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1527799
    :cond_0
    monitor-exit v1

    .line 1527800
    :cond_1
    return-void

    .line 1527801
    :cond_2
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v0

    .line 1527802
    iget-object v2, p0, LX/9iQ;->f:Ljava/util/LinkedList;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1527803
    iget-object v2, p0, LX/9iQ;->f:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->clear()V

    .line 1527804
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1527805
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 1527806
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 1527807
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public a(LX/74w;)V
    .locals 6
    .param p1    # LX/74w;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 1527774
    iput-object p1, p0, LX/9iQ;->b:LX/74w;

    .line 1527775
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/9iQ;->e:Z

    .line 1527776
    const/4 v0, 0x0

    .line 1527777
    invoke-virtual {p0}, LX/9iQ;->getZoomableImageView()Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/7UQ;->setRotation(I)V

    .line 1527778
    iget-object v1, p0, LX/9iQ;->b:LX/74w;

    if-eqz v1, :cond_2

    .line 1527779
    iget-object v0, p0, LX/9iQ;->b:LX/74w;

    sget-object v1, LX/74z;->THUMBNAIL:LX/74z;

    invoke-virtual {v0, v1}, LX/74w;->a(LX/74z;)LX/4n9;

    move-result-object v1

    .line 1527780
    iget-object v0, p0, LX/9iQ;->b:LX/74w;

    sget-object v2, LX/74z;->SCREENNAIL:LX/74z;

    invoke-virtual {v0, v2}, LX/74w;->a(LX/74z;)LX/4n9;

    move-result-object v0

    .line 1527781
    :goto_0
    iget-object v2, p0, LX/9iQ;->b:LX/74w;

    if-eqz v2, :cond_1

    iget-object v2, p0, LX/9iQ;->d:LX/75D;

    iget-object v3, p0, LX/9iQ;->b:LX/74w;

    sget-object v4, LX/74z;->THUMBNAIL:LX/74z;

    .line 1527782
    invoke-static {v2, v3, v4}, LX/75D;->b(LX/75D;LX/74w;LX/74z;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 1527783
    instance-of v5, p1, Ljava/io/Closeable;

    if-eqz v5, :cond_0

    move-object v5, p1

    .line 1527784
    check-cast v5, Ljava/io/Closeable;

    invoke-static {v5}, LX/1pX;->a(Ljava/io/Closeable;)V

    .line 1527785
    :cond_0
    if-eqz p1, :cond_3

    const/4 v5, 0x1

    :goto_1
    move v2, v5

    .line 1527786
    if-eqz v2, :cond_1

    .line 1527787
    iget-object v2, p0, LX/9iQ;->c:Lcom/facebook/widget/images/UrlImage;

    sget-object v3, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v3}, Lcom/facebook/widget/images/UrlImage;->setPlaceHolderScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 1527788
    iget-object v2, p0, LX/9iQ;->c:Lcom/facebook/widget/images/UrlImage;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/images/UrlImage;->setPlaceholderImageParams(LX/4n9;)V

    .line 1527789
    :goto_2
    iget-object v1, p0, LX/9iQ;->c:Lcom/facebook/widget/images/UrlImage;

    new-instance v2, LX/9iO;

    invoke-direct {v2, p0}, LX/9iO;-><init>(LX/9iQ;)V

    .line 1527790
    iput-object v2, v1, Lcom/facebook/widget/images/UrlImage;->G:LX/7UJ;

    .line 1527791
    iget-object v1, p0, LX/9iQ;->c:Lcom/facebook/widget/images/UrlImage;

    new-instance v2, LX/9iP;

    invoke-direct {v2, p0}, LX/9iP;-><init>(LX/9iQ;)V

    .line 1527792
    iput-object v2, v1, Lcom/facebook/widget/images/UrlImage;->H:LX/7UK;

    .line 1527793
    invoke-virtual {p0, v0}, LX/9iQ;->setFetchParams(LX/4n9;)V

    .line 1527794
    return-void

    .line 1527795
    :cond_1
    iget-object v1, p0, LX/9iQ;->c:Lcom/facebook/widget/images/UrlImage;

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/images/UrlImage;->setPlaceHolderScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 1527796
    iget-object v1, p0, LX/9iQ;->c:Lcom/facebook/widget/images/UrlImage;

    const v2, 0x7f02145d

    invoke-virtual {v1, v2}, Lcom/facebook/widget/images/UrlImage;->setPlaceHolderResourceId(I)V

    goto :goto_2

    :cond_2
    move-object v1, v0

    goto :goto_0

    :cond_3
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public final a(Ljava/lang/Runnable;Z)V
    .locals 2

    .prologue
    .line 1527766
    iget-object v1, p0, LX/9iQ;->a:[Ljava/lang/Object;

    monitor-enter v1

    .line 1527767
    if-eqz p2, :cond_0

    .line 1527768
    :try_start_0
    iget-object v0, p0, LX/9iQ;->f:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 1527769
    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1527770
    invoke-static {p0}, LX/9iQ;->j(LX/9iQ;)V

    .line 1527771
    return-void

    .line 1527772
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/9iQ;->f:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    goto :goto_0

    .line 1527773
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1527747
    iget-object v0, p0, LX/9iQ;->c:Lcom/facebook/widget/images/UrlImage;

    .line 1527748
    iget-boolean p0, v0, Lcom/facebook/widget/images/UrlImage;->aj:Z

    move v0, p0

    .line 1527749
    return v0
.end method

.method public getBitmap()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1527762
    invoke-virtual {p0}, LX/9iQ;->getCachedBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1527763
    if-eqz v0, :cond_0

    .line 1527764
    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1527765
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/9iQ;->g:Lcom/google/common/util/concurrent/SettableFuture;

    goto :goto_0
.end method

.method public getCachedBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 1527760
    iget-object v0, p0, LX/9iQ;->c:Lcom/facebook/widget/images/UrlImage;

    move-object v0, v0

    .line 1527761
    invoke-virtual {v0}, Lcom/facebook/widget/images/UrlImage;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getZoomableImageView()Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;
    .locals 1

    .prologue
    .line 1527757
    iget-object v0, p0, LX/9iQ;->c:Lcom/facebook/widget/images/UrlImage;

    .line 1527758
    iget-object p0, v0, Lcom/facebook/widget/images/UrlImage;->z:Landroid/widget/ImageView;

    move-object v0, p0

    .line 1527759
    check-cast v0, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    return-object v0
.end method

.method public onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x48707966

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1527752
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 1527753
    iget-object v1, p0, LX/9iQ;->g:Lcom/google/common/util/concurrent/SettableFuture;

    if-eqz v1, :cond_0

    .line 1527754
    iget-object v1, p0, LX/9iQ;->g:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/0SQ;->cancel(Z)Z

    .line 1527755
    const/4 v1, 0x0

    iput-object v1, p0, LX/9iQ;->g:Lcom/google/common/util/concurrent/SettableFuture;

    .line 1527756
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x552f908b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setFetchParams(LX/4n9;)V
    .locals 1
    .param p1    # LX/4n9;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1527750
    iget-object v0, p0, LX/9iQ;->c:Lcom/facebook/widget/images/UrlImage;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/images/UrlImage;->setImageParams(LX/4n9;)V

    .line 1527751
    return-void
.end method
