.class public LX/9VB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# static fields
.field private static final c:[F

.field private static final d:[F

.field private static final e:[S


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:I

.field private final j:I

.field private final k:I

.field private final l:I

.field private final m:I

.field private final n:Ljava/nio/FloatBuffer;

.field private final o:Ljava/nio/FloatBuffer;

.field private final p:Ljava/nio/ShortBuffer;

.field private final q:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1499124
    const/16 v0, 0xc

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    sput-object v0, LX/9VB;->c:[F

    .line 1499125
    const/16 v0, 0x8

    new-array v0, v0, [F

    fill-array-data v0, :array_1

    sput-object v0, LX/9VB;->d:[F

    .line 1499126
    const/4 v0, 0x6

    new-array v0, v0, [S

    fill-array-data v0, :array_2

    sput-object v0, LX/9VB;->e:[S

    return-void

    nop

    :array_0
    .array-data 4
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        0x0
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x0
        0x3f800000    # 1.0f
        -0x40800000    # -1.0f
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 1499127
    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data

    .line 1499128
    :array_2
    .array-data 2
        0x0s
        0x1s
        0x2s
        0x0s
        0x2s
        0x3s
    .end array-data
.end method

.method public constructor <init>(LX/9VA;)V
    .locals 8

    .prologue
    .line 1499058
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1499059
    const-string v0, "attribute mediump vec4 aPosition;\nattribute mediump vec2 aTexCoord;\nuniform mediump float uScale;\nuniform mediump float uAlpha;\nuniform mediump int uBlurType;\nuniform mediump mat4 uTransformMatrix;\nvarying mediump float vAlpha;\nvarying mediump vec2 vBlurTexCoord[14];\nvarying mediump vec2 vTexCoord;\nvoid main() {\n vTexCoord = (uTransformMatrix * vec4(aTexCoord, 0.0, 1.0)).xy;\n vAlpha = uAlpha;\n if (uBlurType == 1) {\n  vBlurTexCoord[ 0] = vTexCoord + vec2(-0.028, 0.0);\n  vBlurTexCoord[ 1] = vTexCoord + vec2(-0.024, 0.0);\n  vBlurTexCoord[ 2] = vTexCoord + vec2(-0.020, 0.0);\n  vBlurTexCoord[ 3] = vTexCoord + vec2(-0.016, 0.0);\n  vBlurTexCoord[ 4] = vTexCoord + vec2(-0.012, 0.0);\n  vBlurTexCoord[ 5] = vTexCoord + vec2(-0.008, 0.0);\n  vBlurTexCoord[ 6] = vTexCoord + vec2(-0.004, 0.0);\n  vBlurTexCoord[ 7] = vTexCoord + vec2( 0.004, 0.0);\n  vBlurTexCoord[ 8] = vTexCoord + vec2( 0.008, 0.0);\n  vBlurTexCoord[ 9] = vTexCoord + vec2( 0.012, 0.0);\n  vBlurTexCoord[10] = vTexCoord + vec2( 0.016, 0.0);\n  vBlurTexCoord[11] = vTexCoord + vec2( 0.020, 0.0);\n  vBlurTexCoord[12] = vTexCoord + vec2( 0.024, 0.0);\n  vBlurTexCoord[13] = vTexCoord + vec2( 0.028, 0.0);\n }\n vec2 pos = aPosition.xy * uScale;\n gl_Position = vec4(pos, 0.0, 1.0);\n}\n"

    iput-object v0, p0, LX/9VB;->a:Ljava/lang/String;

    .line 1499060
    const-string v0, "#extension GL_OES_EGL_image_external : require\nvarying mediump vec2 vBlurTexCoord[14];\nvarying mediump vec2 vTexCoord;\nuniform mediump int uBlurType;\nvarying mediump float vAlpha;\nuniform samplerExternalOES uTexture;\nvoid main() {\n if (uBlurType == 0) {\n  gl_FragColor = vAlpha * texture2D(uTexture, vTexCoord);\n } else {\n gl_FragColor = vec4(0.0);\n gl_FragColor += vAlpha * texture2D(uTexture, vBlurTexCoord[ 0])*0.0044299121055113265;\n gl_FragColor += vAlpha * texture2D(uTexture, vBlurTexCoord[ 1])*0.00895781211794;\n gl_FragColor += vAlpha * texture2D(uTexture, vBlurTexCoord[ 2])*0.0215963866053;\n gl_FragColor += vAlpha * texture2D(uTexture, vBlurTexCoord[ 3])*0.0443683338718;\n gl_FragColor += vAlpha * texture2D(uTexture, vBlurTexCoord[ 4])*0.0776744219933;\n gl_FragColor += vAlpha * texture2D(uTexture, vBlurTexCoord[ 5])*0.115876621105;\n gl_FragColor += vAlpha * texture2D(uTexture, vBlurTexCoord[ 6])*0.147308056121;\n gl_FragColor += vAlpha * texture2D(uTexture, vTexCoord        )*0.159576912161;\n gl_FragColor += vAlpha * texture2D(uTexture, vBlurTexCoord[ 7])*0.147308056121;\n gl_FragColor += vAlpha * texture2D(uTexture, vBlurTexCoord[ 8])*0.115876621105;\n gl_FragColor += vAlpha * texture2D(uTexture, vBlurTexCoord[ 9])*0.0776744219933;\n gl_FragColor += vAlpha * texture2D(uTexture, vBlurTexCoord[10])*0.0443683338718;\n gl_FragColor += vAlpha * texture2D(uTexture, vBlurTexCoord[11])*0.0215963866053;\n gl_FragColor += vAlpha * texture2D(uTexture, vBlurTexCoord[12])*0.00895781211794;\n gl_FragColor += vAlpha * texture2D(uTexture, vBlurTexCoord[13])*0.0044299121055113265; }\n}\n"

    iput-object v0, p0, LX/9VB;->b:Ljava/lang/String;

    .line 1499061
    const-string v0, "attribute mediump vec4 aPosition;\nattribute mediump vec2 aTexCoord;\nuniform mediump float uScale;\nuniform mediump float uAlpha;\nuniform mediump int uBlurType;\nuniform mediump mat4 uTransformMatrix;\nvarying mediump float vAlpha;\nvarying mediump vec2 vBlurTexCoord[14];\nvarying mediump vec2 vTexCoord;\nvoid main() {\n vTexCoord = (uTransformMatrix * vec4(aTexCoord, 0.0, 1.0)).xy;\n vAlpha = uAlpha;\n if (uBlurType == 1) {\n  vBlurTexCoord[ 0] = vTexCoord + vec2(-0.028, 0.0);\n  vBlurTexCoord[ 1] = vTexCoord + vec2(-0.024, 0.0);\n  vBlurTexCoord[ 2] = vTexCoord + vec2(-0.020, 0.0);\n  vBlurTexCoord[ 3] = vTexCoord + vec2(-0.016, 0.0);\n  vBlurTexCoord[ 4] = vTexCoord + vec2(-0.012, 0.0);\n  vBlurTexCoord[ 5] = vTexCoord + vec2(-0.008, 0.0);\n  vBlurTexCoord[ 6] = vTexCoord + vec2(-0.004, 0.0);\n  vBlurTexCoord[ 7] = vTexCoord + vec2( 0.004, 0.0);\n  vBlurTexCoord[ 8] = vTexCoord + vec2( 0.008, 0.0);\n  vBlurTexCoord[ 9] = vTexCoord + vec2( 0.012, 0.0);\n  vBlurTexCoord[10] = vTexCoord + vec2( 0.016, 0.0);\n  vBlurTexCoord[11] = vTexCoord + vec2( 0.020, 0.0);\n  vBlurTexCoord[12] = vTexCoord + vec2( 0.024, 0.0);\n  vBlurTexCoord[13] = vTexCoord + vec2( 0.028, 0.0);\n }\n vec2 pos = aPosition.xy * uScale;\n gl_Position = vec4(pos, 0.0, 1.0);\n}\n"

    const-string v1, "#extension GL_OES_EGL_image_external : require\nvarying mediump vec2 vBlurTexCoord[14];\nvarying mediump vec2 vTexCoord;\nuniform mediump int uBlurType;\nvarying mediump float vAlpha;\nuniform samplerExternalOES uTexture;\nvoid main() {\n if (uBlurType == 0) {\n  gl_FragColor = vAlpha * texture2D(uTexture, vTexCoord);\n } else {\n gl_FragColor = vec4(0.0);\n gl_FragColor += vAlpha * texture2D(uTexture, vBlurTexCoord[ 0])*0.0044299121055113265;\n gl_FragColor += vAlpha * texture2D(uTexture, vBlurTexCoord[ 1])*0.00895781211794;\n gl_FragColor += vAlpha * texture2D(uTexture, vBlurTexCoord[ 2])*0.0215963866053;\n gl_FragColor += vAlpha * texture2D(uTexture, vBlurTexCoord[ 3])*0.0443683338718;\n gl_FragColor += vAlpha * texture2D(uTexture, vBlurTexCoord[ 4])*0.0776744219933;\n gl_FragColor += vAlpha * texture2D(uTexture, vBlurTexCoord[ 5])*0.115876621105;\n gl_FragColor += vAlpha * texture2D(uTexture, vBlurTexCoord[ 6])*0.147308056121;\n gl_FragColor += vAlpha * texture2D(uTexture, vTexCoord        )*0.159576912161;\n gl_FragColor += vAlpha * texture2D(uTexture, vBlurTexCoord[ 7])*0.147308056121;\n gl_FragColor += vAlpha * texture2D(uTexture, vBlurTexCoord[ 8])*0.115876621105;\n gl_FragColor += vAlpha * texture2D(uTexture, vBlurTexCoord[ 9])*0.0776744219933;\n gl_FragColor += vAlpha * texture2D(uTexture, vBlurTexCoord[10])*0.0443683338718;\n gl_FragColor += vAlpha * texture2D(uTexture, vBlurTexCoord[11])*0.0215963866053;\n gl_FragColor += vAlpha * texture2D(uTexture, vBlurTexCoord[12])*0.00895781211794;\n gl_FragColor += vAlpha * texture2D(uTexture, vBlurTexCoord[13])*0.0044299121055113265; }\n}\n"

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 1499062
    const v3, 0x8b31

    invoke-static {v3, v0}, LX/9VA;->a(ILjava/lang/String;)I

    move-result v4

    .line 1499063
    const v3, 0x8b30

    invoke-static {v3, v1}, LX/9VA;->a(ILjava/lang/String;)I

    move-result v5

    .line 1499064
    if-eqz v4, :cond_0

    if-nez v5, :cond_2

    .line 1499065
    :cond_0
    :goto_0
    move v0, v2

    .line 1499066
    iput v0, p0, LX/9VB;->f:I

    .line 1499067
    iget v0, p0, LX/9VB;->f:I

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1499068
    iget v0, p0, LX/9VB;->f:I

    const-string v1, "aPosition"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, LX/9VB;->g:I

    .line 1499069
    iget v0, p0, LX/9VB;->f:I

    const-string v1, "aTexCoord"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, LX/9VB;->i:I

    .line 1499070
    iget v0, p0, LX/9VB;->f:I

    const-string v1, "uTexture"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, LX/9VB;->h:I

    .line 1499071
    iget v0, p0, LX/9VB;->f:I

    const-string v1, "uScale"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, LX/9VB;->j:I

    .line 1499072
    iget v0, p0, LX/9VB;->f:I

    const-string v1, "uAlpha"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, LX/9VB;->k:I

    .line 1499073
    iget v0, p0, LX/9VB;->f:I

    const-string v1, "uBlurType"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, LX/9VB;->l:I

    .line 1499074
    iget v0, p0, LX/9VB;->f:I

    const-string v1, "uTransformMatrix"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, LX/9VB;->m:I

    .line 1499075
    sget-object v0, LX/9VB;->e:[S

    array-length v0, v0

    iput v0, p0, LX/9VB;->q:I

    .line 1499076
    sget-object v0, LX/9VB;->e:[S

    .line 1499077
    array-length v1, v0

    mul-int/lit8 v1, v1, 0x2

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->asShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object v1

    .line 1499078
    invoke-virtual {v1, v0}, Ljava/nio/ShortBuffer;->put([S)Ljava/nio/ShortBuffer;

    .line 1499079
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/nio/ShortBuffer;->position(I)Ljava/nio/Buffer;

    .line 1499080
    move-object v0, v1

    .line 1499081
    iput-object v0, p0, LX/9VB;->p:Ljava/nio/ShortBuffer;

    .line 1499082
    sget-object v0, LX/9VB;->d:[F

    invoke-static {v0}, LX/9VB;->a([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, LX/9VB;->o:Ljava/nio/FloatBuffer;

    .line 1499083
    sget-object v0, LX/9VB;->c:[F

    invoke-static {v0}, LX/9VB;->a([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, LX/9VB;->n:Ljava/nio/FloatBuffer;

    .line 1499084
    return-void

    .line 1499085
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1499086
    :cond_2
    invoke-static {}, Landroid/opengl/GLES20;->glCreateProgram()I

    move-result v3

    .line 1499087
    invoke-static {v3, v4}, Landroid/opengl/GLES20;->glAttachShader(II)V

    .line 1499088
    invoke-static {v3, v5}, Landroid/opengl/GLES20;->glAttachShader(II)V

    .line 1499089
    invoke-static {v3}, Landroid/opengl/GLES20;->glLinkProgram(I)V

    .line 1499090
    new-array v4, v6, [I

    .line 1499091
    const v5, 0x8b82

    invoke-static {v3, v5, v4, v2}, Landroid/opengl/GLES20;->glGetProgramiv(II[II)V

    .line 1499092
    aget v4, v4, v2

    if-nez v4, :cond_3

    .line 1499093
    sget-object v4, LX/9VA;->a:Ljava/lang/String;

    const-string v5, "createProgram failed:\n%s"

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v3}, Landroid/opengl/GLES20;->glGetProgramInfoLog(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-static {v4, v5, v6}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1499094
    invoke-static {v3}, Landroid/opengl/GLES20;->glDeleteProgram(I)V

    goto/16 :goto_0

    :cond_3
    move v2, v3

    .line 1499095
    goto/16 :goto_0
.end method

.method private static a([F)Ljava/nio/FloatBuffer;
    .locals 2

    .prologue
    .line 1499120
    array-length v0, p0

    mul-int/lit8 v0, v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    .line 1499121
    invoke-virtual {v0, p0}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    .line 1499122
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 1499123
    return-object v0
.end method

.method public static a(LX/9VB;[FIFFI)V
    .locals 8

    .prologue
    const/16 v2, 0x1406

    const/16 v7, 0xbe2

    const/16 v4, 0x303

    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 1499096
    invoke-static {v7}, Landroid/opengl/GLES20;->glEnable(I)V

    .line 1499097
    const/16 v0, 0xb71

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisable(I)V

    .line 1499098
    invoke-static {v3}, Landroid/opengl/GLES20;->glDepthMask(Z)V

    .line 1499099
    const v0, 0x8006

    const v1, 0x8006

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBlendEquationSeparate(II)V

    .line 1499100
    invoke-static {v6, v4, v6, v4}, Landroid/opengl/GLES20;->glBlendFuncSeparate(IIII)V

    .line 1499101
    iget v0, p0, LX/9VB;->f:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glUseProgram(I)V

    .line 1499102
    iget v0, p0, LX/9VB;->g:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 1499103
    iget v0, p0, LX/9VB;->i:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 1499104
    iget v0, p0, LX/9VB;->g:I

    const/4 v1, 0x3

    iget-object v5, p0, LX/9VB;->n:Ljava/nio/FloatBuffer;

    move v4, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 1499105
    iget v0, p0, LX/9VB;->i:I

    const/4 v1, 0x2

    iget-object v5, p0, LX/9VB;->o:Ljava/nio/FloatBuffer;

    move v4, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 1499106
    const v0, 0x84c0

    invoke-static {v0}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 1499107
    const v0, 0x8d65

    invoke-static {v0, p2}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 1499108
    iget v0, p0, LX/9VB;->h:I

    invoke-static {v0, v3}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 1499109
    iget v0, p0, LX/9VB;->j:I

    invoke-static {v0, p3}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 1499110
    iget v0, p0, LX/9VB;->k:I

    invoke-static {v0, p4}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 1499111
    iget v0, p0, LX/9VB;->l:I

    invoke-static {v0, p5}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 1499112
    iget v0, p0, LX/9VB;->m:I

    invoke-static {v0, v6, v3, p1, v3}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    .line 1499113
    const/4 v0, 0x4

    iget v1, p0, LX/9VB;->q:I

    const/16 v2, 0x1403

    iget-object v4, p0, LX/9VB;->p:Ljava/nio/ShortBuffer;

    invoke-static {v0, v1, v2, v4}, Landroid/opengl/GLES20;->glDrawElements(IIILjava/nio/Buffer;)V

    .line 1499114
    iget v0, p0, LX/9VB;->g:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    .line 1499115
    iget v0, p0, LX/9VB;->i:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    .line 1499116
    const v0, 0x8d65

    invoke-static {v0, v3}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 1499117
    invoke-static {v7}, Landroid/opengl/GLES20;->glDisable(I)V

    .line 1499118
    invoke-static {v3}, Landroid/opengl/GLES20;->glUseProgram(I)V

    .line 1499119
    return-void
.end method
