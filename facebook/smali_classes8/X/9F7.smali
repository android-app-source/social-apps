.class public LX/9F7;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/9Du;

.field public final b:LX/9D1;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:LX/9CC;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Landroid/os/Handler;

.field private final e:LX/0SG;

.field public final f:Ljava/lang/Runnable;

.field public final g:LX/0if;

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/sounds/FBSoundUtil;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Z

.field public final j:Z

.field public k:I


# direct methods
.method public constructor <init>(LX/9Du;LX/9D1;LX/9CC;Landroid/os/Handler;LX/0SG;LX/0if;LX/0Ot;LX/0ad;)V
    .locals 2
    .param p1    # LX/9Du;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/9D1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/9CC;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/9Du;",
            "LX/9D1;",
            "LX/9CC;",
            "Landroid/os/Handler;",
            "LX/0SG;",
            "Lcom/facebook/funnellogger/FunnelLogger;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/sounds/FBSoundUtil;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1458075
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1458076
    iput-object p2, p0, LX/9F7;->b:LX/9D1;

    .line 1458077
    iput-object p1, p0, LX/9F7;->a:LX/9Du;

    .line 1458078
    iput-object p3, p0, LX/9F7;->c:LX/9CC;

    .line 1458079
    iput-object p4, p0, LX/9F7;->d:Landroid/os/Handler;

    .line 1458080
    iput-object p5, p0, LX/9F7;->e:LX/0SG;

    .line 1458081
    iput-object p6, p0, LX/9F7;->g:LX/0if;

    .line 1458082
    new-instance v0, Lcom/facebook/feedback/ui/TypingIndicatorController$1;

    invoke-direct {v0, p0}, Lcom/facebook/feedback/ui/TypingIndicatorController$1;-><init>(LX/9F7;)V

    iput-object v0, p0, LX/9F7;->f:Ljava/lang/Runnable;

    .line 1458083
    iput-object p7, p0, LX/9F7;->h:LX/0Ot;

    .line 1458084
    sget-short v0, LX/0wn;->aJ:S

    invoke-interface {p8, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/9F7;->i:Z

    .line 1458085
    sget-short v0, LX/0wn;->aK:S

    invoke-interface {p8, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/9F7;->j:Z

    .line 1458086
    return-void
.end method

.method public static d(LX/9F7;)Z
    .locals 1

    .prologue
    .line 1458074
    iget-object v0, p0, LX/9F7;->c:LX/9CC;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9F7;->b:LX/9D1;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1458087
    iget-object v0, p0, LX/9F7;->g:LX/0if;

    sget-object v1, LX/0ig;->r:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->a(LX/0ih;)V

    .line 1458088
    iget-object v1, p0, LX/9F7;->g:LX/0if;

    sget-object v2, LX/0ig;->r:LX/0ih;

    iget-boolean v0, p0, LX/9F7;->i:Z

    if-eqz v0, :cond_0

    const-string v0, "Typing_Indicator_Cell_Shown_With_Text"

    :goto_0
    invoke-virtual {v1, v2, v0}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 1458089
    iget-object v0, p0, LX/9F7;->g:LX/0if;

    sget-object v1, LX/0ig;->r:LX/0ih;

    const-string v2, "Viewing_Comments"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1458090
    return-void

    .line 1458091
    :cond_0
    const-string v0, "Typing_Indicator_Cell_Shown_Without_Text"

    goto :goto_0
.end method

.method public final a(I)V
    .locals 4

    .prologue
    .line 1458049
    if-lez p1, :cond_3

    .line 1458050
    iget v0, p0, LX/9F7;->k:I

    if-ge v0, p1, :cond_1

    .line 1458051
    const/4 v0, 0x0

    .line 1458052
    invoke-static {p0}, LX/9F7;->d(LX/9F7;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1458053
    iget-object v0, p0, LX/9F7;->c:LX/9CC;

    invoke-virtual {v0, p1}, LX/9CC;->h(I)V

    .line 1458054
    iget-object v0, p0, LX/9F7;->b:LX/9D1;

    invoke-virtual {v0}, LX/9D1;->a()Z

    move-result v0

    .line 1458055
    :cond_0
    if-eqz v0, :cond_5

    .line 1458056
    iget-object v0, p0, LX/9F7;->b:LX/9D1;

    invoke-virtual {v0}, LX/9D1;->g()Z

    .line 1458057
    iget v0, p0, LX/9F7;->k:I

    if-nez v0, :cond_1

    .line 1458058
    iget-object v0, p0, LX/9F7;->g:LX/0if;

    sget-object v1, LX/0ig;->r:LX/0ih;

    const-string v2, "Typing_Indicator_Cell_Shown"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1458059
    iget-boolean v0, p0, LX/9F7;->j:Z

    if-eqz v0, :cond_1

    .line 1458060
    iget-object v0, p0, LX/9F7;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3RX;

    const-string v1, "typing_indicator"

    invoke-virtual {v0, v1}, LX/3RX;->a(Ljava/lang/String;)V

    .line 1458061
    :cond_1
    :goto_0
    iput p1, p0, LX/9F7;->k:I

    .line 1458062
    const-wide/16 v0, 0x4e84

    .line 1458063
    iget-object v2, p0, LX/9F7;->d:Landroid/os/Handler;

    iget-object v3, p0, LX/9F7;->f:Ljava/lang/Runnable;

    invoke-static {v2, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1458064
    iget-object v2, p0, LX/9F7;->d:Landroid/os/Handler;

    iget-object v3, p0, LX/9F7;->f:Ljava/lang/Runnable;

    const p1, 0x2e1482ae

    invoke-static {v2, v3, v0, v1, p1}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1458065
    :cond_2
    :goto_1
    return-void

    .line 1458066
    :cond_3
    iget v0, p0, LX/9F7;->k:I

    if-lez v0, :cond_2

    .line 1458067
    const/4 v0, 0x0

    iput v0, p0, LX/9F7;->k:I

    .line 1458068
    invoke-static {p0}, LX/9F7;->d(LX/9F7;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1458069
    iget-object v0, p0, LX/9F7;->c:LX/9CC;

    iget v1, p0, LX/9F7;->k:I

    invoke-virtual {v0, v1}, LX/9CC;->h(I)V

    .line 1458070
    :cond_4
    iget-object v0, p0, LX/9F7;->a:LX/9Du;

    iget v1, p0, LX/9F7;->k:I

    invoke-virtual {v0, v1}, LX/9Du;->a(I)Z

    .line 1458071
    goto :goto_1

    .line 1458072
    :cond_5
    iget-object v0, p0, LX/9F7;->a:LX/9Du;

    invoke-virtual {v0, p1}, LX/9Du;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1458073
    iget-object v0, p0, LX/9F7;->g:LX/0if;

    sget-object v1, LX/0ig;->r:LX/0ih;

    const-string v2, "Typing_Indicator_Pill_Shown"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1458046
    iget-object v0, p0, LX/9F7;->d:Landroid/os/Handler;

    iget-object v1, p0, LX/9F7;->f:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1458047
    iget-object v0, p0, LX/9F7;->g:LX/0if;

    sget-object v1, LX/0ig;->r:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->c(LX/0ih;)V

    .line 1458048
    return-void
.end method
