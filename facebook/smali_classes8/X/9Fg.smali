.class public final LX/9Fg;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source ""

# interfaces
.implements Landroid/view/View$OnLongClickListener;
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;

.field private final b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/graphql/model/GraphQLFeedback;

.field private final d:LX/9FA;

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/3rW;

.field public g:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/9FA;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;",
            "Lcom/facebook/feedback/ui/environment/CommentsEnvironment;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1458830
    iput-object p1, p0, LX/9Fg;->a:Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    .line 1458831
    iput-object p2, p0, LX/9Fg;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1458832
    iget-object v0, p0, LX/9Fg;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/5Op;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    iput-object v0, p0, LX/9Fg;->c:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1458833
    iput-object p3, p0, LX/9Fg;->d:LX/9FA;

    .line 1458834
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/9Fg;->e:Ljava/util/List;

    .line 1458835
    new-instance v0, LX/3rW;

    iget-object v1, p1, Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;->h:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, LX/3rW;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, LX/9Fg;->f:LX/3rW;

    .line 1458836
    return-void
.end method


# virtual methods
.method public final onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1458837
    const/4 v0, 0x1

    return v0
.end method

.method public final onLongClick(Landroid/view/View;)Z
    .locals 13

    .prologue
    .line 1458838
    invoke-virtual {p1}, Landroid/view/View;->cancelLongPress()V

    .line 1458839
    iget-object v0, p0, LX/9Fg;->a:Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;->i:LX/3iR;

    iget-object v1, p0, LX/9Fg;->d:LX/9FA;

    .line 1458840
    iget-object v2, v1, LX/9FA;->k:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v1, v2

    .line 1458841
    const-string v2, "comment_curation_menu_opened"

    invoke-static {v0, v2, v1}, LX/3iR;->b(LX/3iR;Ljava/lang/String;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)LX/0oG;

    move-result-object v2

    .line 1458842
    invoke-virtual {v2}, LX/0oG;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz v1, :cond_0

    .line 1458843
    invoke-virtual {v2}, LX/0oG;->d()V

    .line 1458844
    :cond_0
    iget-object v0, p0, LX/9Fg;->a:Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;

    iget-object v1, v0, Lcom/facebook/feedback/ui/rows/CommentGroupPartDefinition;->g:LX/8sB;

    iget-object v0, p0, LX/9Fg;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1458845
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 1458846
    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, LX/9Fg;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v5, p0, LX/9Fg;->d:LX/9FA;

    invoke-static {v3, v4, v5}, LX/9Hj;->a(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;LX/9FA;)LX/9Hi;

    move-result-object v3

    const/4 v11, 0x0

    .line 1458847
    move-object v6, v1

    move-object v7, v0

    move-object v8, p1

    move-object v9, v2

    move-object v10, v3

    move-object v12, v11

    invoke-virtual/range {v6 .. v12}, LX/8sB;->a(Lcom/facebook/graphql/model/GraphQLComment;Landroid/view/View;Landroid/content/Context;LX/9Hi;LX/2yQ;LX/2dD;)Z

    .line 1458848
    const/4 v0, 0x1

    return v0
.end method

.method public final onLongPress(Landroid/view/MotionEvent;)V
    .locals 2

    .prologue
    .line 1458849
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v1

    .line 1458850
    const/4 v0, 0x3

    invoke-virtual {v1, v0}, Landroid/view/MotionEvent;->setAction(I)V

    .line 1458851
    iget-object v0, p0, LX/9Fg;->g:Landroid/view/View;

    instance-of v0, v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 1458852
    iget-object v0, p0, LX/9Fg;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1458853
    :goto_0
    invoke-virtual {v1}, Landroid/view/MotionEvent;->recycle()V

    .line 1458854
    return-void

    .line 1458855
    :cond_0
    iget-object v0, p0, LX/9Fg;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0
.end method

.method public final onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1458856
    const/4 v0, 0x0

    return v0
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 1458857
    iput-object p1, p0, LX/9Fg;->g:Landroid/view/View;

    .line 1458858
    iget-object v0, p0, LX/9Fg;->f:LX/3rW;

    invoke-virtual {v0, p2}, LX/3rW;->a(Landroid/view/MotionEvent;)Z

    .line 1458859
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1458860
    :cond_0
    :pswitch_0
    const/4 v0, 0x0

    return v0

    .line 1458861
    :pswitch_1
    iget-object v0, p0, LX/9Fg;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1458862
    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {v0}, LX/9CG;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 1458863
    :pswitch_2
    iget-object v0, p0, LX/9Fg;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1458864
    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {v0}, LX/9CG;->b(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
