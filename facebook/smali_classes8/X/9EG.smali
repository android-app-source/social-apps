.class public LX/9EG;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/9EG;


# instance fields
.field private final a:LX/0ad;

.field public final b:LX/1EQ;

.field private final c:LX/1Uf;

.field private final d:LX/0if;

.field private final e:Lcom/facebook/content/SecureContextHelper;

.field private final f:Lcom/facebook/intent/feed/IFeedIntentBuilder;


# direct methods
.method public constructor <init>(LX/0ad;LX/1EQ;LX/1Uf;LX/0if;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/intent/feed/IFeedIntentBuilder;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1456786
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1456787
    iput-object p1, p0, LX/9EG;->a:LX/0ad;

    .line 1456788
    iput-object p2, p0, LX/9EG;->b:LX/1EQ;

    .line 1456789
    iput-object p3, p0, LX/9EG;->c:LX/1Uf;

    .line 1456790
    iput-object p4, p0, LX/9EG;->d:LX/0if;

    .line 1456791
    iput-object p5, p0, LX/9EG;->e:Lcom/facebook/content/SecureContextHelper;

    .line 1456792
    iput-object p6, p0, LX/9EG;->f:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 1456793
    return-void
.end method

.method public static a(LX/0QB;)LX/9EG;
    .locals 10

    .prologue
    .line 1456773
    sget-object v0, LX/9EG;->g:LX/9EG;

    if-nez v0, :cond_1

    .line 1456774
    const-class v1, LX/9EG;

    monitor-enter v1

    .line 1456775
    :try_start_0
    sget-object v0, LX/9EG;->g:LX/9EG;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1456776
    if-eqz v2, :cond_0

    .line 1456777
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1456778
    new-instance v3, LX/9EG;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {v0}, LX/1EQ;->b(LX/0QB;)LX/1EQ;

    move-result-object v5

    check-cast v5, LX/1EQ;

    invoke-static {v0}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v6

    check-cast v6, LX/1Uf;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v7

    check-cast v7, LX/0if;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v8

    check-cast v8, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v9

    check-cast v9, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-direct/range {v3 .. v9}, LX/9EG;-><init>(LX/0ad;LX/1EQ;LX/1Uf;LX/0if;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/intent/feed/IFeedIntentBuilder;)V

    .line 1456779
    move-object v0, v3

    .line 1456780
    sput-object v0, LX/9EG;->g:LX/9EG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1456781
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1456782
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1456783
    :cond_1
    sget-object v0, LX/9EG;->g:LX/9EG;

    return-object v0

    .line 1456784
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1456785
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static c(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/9EE;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/9EE;"
        }
    .end annotation

    .prologue
    .line 1456671
    if-nez p0, :cond_0

    .line 1456672
    sget-object v0, LX/9EE;->NONE:LX/9EE;

    .line 1456673
    :goto_0
    return-object v0

    .line 1456674
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1456675
    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v1, :cond_1

    .line 1456676
    sget-object v0, LX/9EE;->NONE:LX/9EE;

    goto :goto_0

    .line 1456677
    :cond_1
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1456678
    invoke-static {v0}, LX/16y;->e(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1456679
    sget-object v0, LX/9EE;->NONE:LX/9EE;

    goto :goto_0

    .line 1456680
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    .line 1456681
    if-eqz v1, :cond_3

    instance-of v2, v1, Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v2, :cond_3

    .line 1456682
    sget-object v0, LX/9EE;->NONE:LX/9EE;

    goto :goto_0

    .line 1456683
    :cond_3
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1456684
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 1456685
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aP()I

    move-result v3

    int-to-long v4, v3

    .line 1456686
    if-eqz v1, :cond_4

    invoke-static {v1}, LX/16y;->e(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 1456687
    :cond_4
    if-nez v2, :cond_5

    .line 1456688
    sget-object v0, LX/9EE;->NONE:LX/9EE;

    goto :goto_0

    .line 1456689
    :cond_5
    const-wide/16 v0, 0x0

    cmp-long v0, v4, v0

    if-nez v0, :cond_6

    .line 1456690
    sget-object v0, LX/9EE;->RESHARE_TOP:LX/9EE;

    goto :goto_0

    .line 1456691
    :cond_6
    sget-object v0, LX/9EE;->AGGREGATE_TOP:LX/9EE;

    goto :goto_0

    .line 1456692
    :cond_7
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-ne v2, v0, :cond_9

    .line 1456693
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->aP()I

    move-result v0

    if-lez v0, :cond_8

    sget-object v0, LX/9EE;->AGGREGATE_ORIGINAL_POST:LX/9EE;

    goto :goto_0

    :cond_8
    sget-object v0, LX/9EE;->ORIGINAL_POST:LX/9EE;

    goto :goto_0

    .line 1456694
    :cond_9
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-nez v0, :cond_a

    .line 1456695
    sget-object v0, LX/9EE;->NONE:LX/9EE;

    goto :goto_0

    .line 1456696
    :cond_a
    sget-object v0, LX/9EE;->AGGREGATE_RESHARE:LX/9EE;

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PT;)LX/9EF;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/1PT;",
            ")",
            "LX/9EF;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1456744
    if-nez p2, :cond_1

    const/4 v0, 0x0

    .line 1456745
    :goto_0
    sget-object v1, LX/1Qt;->VIDEO_CHANNEL:LX/1Qt;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/1Qt;->VIDEO_HOME:LX/1Qt;

    if-ne v0, v1, :cond_2

    :cond_0
    iget-object v0, p0, LX/9EG;->a:LX/0ad;

    sget-short v1, LX/0wn;->av:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1456746
    new-instance v0, LX/9EF;

    invoke-direct {v0, v3, p1}, LX/9EF;-><init>(ZLcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1456747
    :goto_1
    return-object v0

    .line 1456748
    :cond_1
    invoke-interface {p2}, LX/1PT;->a()LX/1Qt;

    move-result-object v0

    goto :goto_0

    .line 1456749
    :cond_2
    sget-object v0, LX/9ED;->a:[I

    invoke-static {p1}, LX/9EG;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/9EE;

    move-result-object v1

    invoke-virtual {v1}, LX/9EE;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1456750
    new-instance v0, LX/9EF;

    invoke-direct {v0, v3, p1}, LX/9EF;-><init>(ZLcom/facebook/feed/rows/core/props/FeedProps;)V

    goto :goto_1

    .line 1456751
    :pswitch_0
    iget-object v0, p0, LX/9EG;->a:LX/0ad;

    sget-short v1, LX/0wn;->au:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1456752
    new-instance v1, LX/9EF;

    .line 1456753
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1456754
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-direct {v1, v4, v0}, LX/9EF;-><init>(ZLcom/facebook/feed/rows/core/props/FeedProps;)V

    move-object v0, v1

    goto :goto_1

    .line 1456755
    :cond_3
    new-instance v0, LX/9EF;

    invoke-direct {v0, v3, p1}, LX/9EF;-><init>(ZLcom/facebook/feed/rows/core/props/FeedProps;)V

    goto :goto_1

    .line 1456756
    :pswitch_1
    iget-object v0, p0, LX/9EG;->a:LX/0ad;

    sget-short v1, LX/0wn;->at:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1456757
    new-instance v0, LX/9EF;

    .line 1456758
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v1

    .line 1456759
    invoke-direct {v0, v4, v1}, LX/9EF;-><init>(ZLcom/facebook/feed/rows/core/props/FeedProps;)V

    goto :goto_1

    .line 1456760
    :cond_4
    new-instance v0, LX/9EF;

    invoke-direct {v0, v3, p1}, LX/9EF;-><init>(ZLcom/facebook/feed/rows/core/props/FeedProps;)V

    goto :goto_1

    .line 1456761
    :pswitch_2
    new-instance v0, LX/9EF;

    invoke-direct {v0, v3, p1}, LX/9EF;-><init>(ZLcom/facebook/feed/rows/core/props/FeedProps;)V

    goto :goto_1

    .line 1456762
    :pswitch_3
    iget-object v0, p0, LX/9EG;->a:LX/0ad;

    sget-short v1, LX/0wn;->au:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1456763
    new-instance v1, LX/9EF;

    .line 1456764
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v2, v0

    .line 1456765
    invoke-virtual {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-direct {v1, v4, v0}, LX/9EF;-><init>(ZLcom/facebook/feed/rows/core/props/FeedProps;)V

    move-object v0, v1

    goto :goto_1

    .line 1456766
    :cond_5
    new-instance v0, LX/9EF;

    invoke-direct {v0, v3, p1}, LX/9EF;-><init>(ZLcom/facebook/feed/rows/core/props/FeedProps;)V

    goto/16 :goto_1

    .line 1456767
    :pswitch_4
    iget-object v0, p0, LX/9EG;->a:LX/0ad;

    sget-short v1, LX/0wn;->at:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1456768
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v2, v0

    .line 1456769
    new-instance v1, LX/9EF;

    .line 1456770
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1456771
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->E()Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-virtual {v2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-direct {v1, v4, v0}, LX/9EF;-><init>(ZLcom/facebook/feed/rows/core/props/FeedProps;)V

    move-object v0, v1

    goto/16 :goto_1

    .line 1456772
    :cond_6
    new-instance v0, LX/9EF;

    invoke-direct {v0, v3, p1}, LX/9EF;-><init>(ZLcom/facebook/feed/rows/core/props/FeedProps;)V

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final a(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/CharSequence;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ")",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1456729
    invoke-static {p3}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    .line 1456730
    iget-object v1, p0, LX/9EG;->c:LX/1Uf;

    invoke-static {v0}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLActor;)LX/1y5;

    move-result-object v0

    const/4 v2, 0x0

    sget v3, LX/1Uf;->a:I

    invoke-virtual {v1, v0, v2, v3, v4}, LX/1Uf;->a(LX/1y5;LX/0lF;IZ)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 1456731
    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLStory;->aP()I

    move-result v1

    if-lez v1, :cond_0

    .line 1456732
    const v1, 0x7f081261

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v5

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1456733
    iget-object v1, p0, LX/9EG;->d:LX/0if;

    sget-object v2, LX/0ig;->W:LX/0ih;

    const-string v3, "show_aggregate_post_button"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1456734
    :goto_0
    return-object v0

    .line 1456735
    :cond_0
    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1456736
    const v1, 0x7f081262

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v5

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1456737
    iget-object v1, p0, LX/9EG;->d:LX/0if;

    sget-object v2, LX/0ig;->W:LX/0ih;

    const-string v3, "show_reshare_post_button"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    goto :goto_0

    .line 1456738
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    if-ne v1, p3, :cond_3

    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 1456739
    if-eqz v1, :cond_2

    .line 1456740
    const v1, 0x7f081260

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v5

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1456741
    iget-object v1, p0, LX/9EG;->d:LX/0if;

    sget-object v2, LX/0ig;->W:LX/0ih;

    const-string v3, "show_original_post_button"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    goto :goto_0

    .line 1456742
    :cond_2
    const v1, 0x7f081262

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v5

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1456743
    iget-object v1, p0, LX/9EG;->d:LX/0if;

    sget-object v2, LX/0ig;->W:LX/0ih;

    const-string v3, "show_aggregate_reshare_post_button"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1456723
    sget-object v1, LX/9ED;->a:[I

    invoke-static {p1}, LX/9EG;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/9EE;

    move-result-object v2

    invoke-virtual {v2}, LX/9EE;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1456724
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 1456725
    :pswitch_1
    iget-object v1, p0, LX/9EG;->a:LX/0ad;

    sget-short v2, LX/0wn;->ay:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v0

    goto :goto_0

    .line 1456726
    :pswitch_2
    iget-object v1, p0, LX/9EG;->a:LX/0ad;

    sget-short v2, LX/0wn;->az:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v0

    goto :goto_0

    .line 1456727
    :pswitch_3
    iget-object v1, p0, LX/9EG;->a:LX/0ad;

    sget-short v2, LX/0wn;->ay:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v0

    goto :goto_0

    .line 1456728
    :pswitch_4
    iget-object v1, p0, LX/9EG;->a:LX/0ad;

    sget-short v2, LX/0wn;->az:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, LX/9EG;->a:LX/0ad;

    sget-short v2, LX/0wn;->aw:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Lcom/facebook/graphql/model/GraphQLStory;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1456711
    sget-object v0, LX/9ED;->a:[I

    invoke-static {p1}, LX/9EG;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/9EE;

    move-result-object v1

    invoke-virtual {v1}, LX/9EE;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1456712
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1456713
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    :goto_0
    return-object v0

    .line 1456714
    :pswitch_0
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1456715
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    goto :goto_0

    .line 1456716
    :pswitch_1
    invoke-virtual {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    goto :goto_0

    .line 1456717
    :pswitch_2
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1456718
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    goto :goto_0

    .line 1456719
    :pswitch_3
    invoke-virtual {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    goto :goto_0

    .line 1456720
    :pswitch_4
    iget-object v0, p0, LX/9EG;->a:LX/0ad;

    sget-short v1, LX/0wn;->au:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/9EG;->a:LX/0ad;

    sget-short v1, LX/0wn;->aw:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1456721
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    goto :goto_0

    .line 1456722
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->E()Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final b(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1456697
    iget-object v0, p0, LX/9EG;->f:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-interface {v0, p3}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Lcom/facebook/graphql/model/GraphQLStory;)Landroid/content/Intent;

    move-result-object v0

    .line 1456698
    const-string v1, "feedback_logging_params"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1456699
    invoke-static {p2}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v2

    .line 1456700
    invoke-static {v1}, LX/21A;->a(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)LX/21A;

    move-result-object v1

    .line 1456701
    iput-object v2, v1, LX/21A;->a:LX/162;

    .line 1456702
    move-object v2, v1

    .line 1456703
    iget-object p3, p0, LX/9EG;->b:LX/1EQ;

    .line 1456704
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1456705
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {p3, v1, v2}, LX/1EQ;->a(Lcom/facebook/graphql/model/FeedUnit;LX/21A;)V

    .line 1456706
    const-string v1, "feedback_logging_params"

    invoke-virtual {v2}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1456707
    const-string v1, "RESHARE_BUTTON_EXPERIMENT_CLICKED"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1456708
    iget-object v1, p0, LX/9EG;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1456709
    iget-object v0, p0, LX/9EG;->d:LX/0if;

    sget-object v1, LX/0ig;->W:LX/0ih;

    const-string v2, "click_view_post_button"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1456710
    return-void
.end method
