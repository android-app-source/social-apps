.class public LX/9T2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/B1W;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/B1W",
        "<",
        "Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsStoriesMallIdsModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/api/feedtype/FeedType;

.field private final b:LX/0pn;

.field public final c:LX/0Uh;


# direct methods
.method public constructor <init>(Lcom/facebook/api/feedtype/FeedType;LX/0pn;LX/0Uh;)V
    .locals 0
    .param p1    # Lcom/facebook/api/feedtype/FeedType;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1494871
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1494872
    iput-object p1, p0, LX/9T2;->a:Lcom/facebook/api/feedtype/FeedType;

    .line 1494873
    iput-object p2, p0, LX/9T2;->b:LX/0pn;

    .line 1494874
    iput-object p3, p0, LX/9T2;->c:LX/0Uh;

    .line 1494875
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/0Px;
    .locals 6

    .prologue
    .line 1494876
    check-cast p1, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsStoriesMallIdsModel;

    .line 1494877
    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsStoriesMallIdsModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsStoriesMallIdsModel$GroupStoriesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsStoriesMallIdsModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsStoriesMallIdsModel$GroupStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsStoriesMallIdsModel$GroupStoriesModel;->a()LX/0Px;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1494878
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1494879
    :goto_0
    return-object v0

    .line 1494880
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1494881
    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsStoriesMallIdsModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsStoriesMallIdsModel$GroupStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsStoriesMallIdsModel$GroupStoriesModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_4

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsStoriesMallIdsModel$GroupStoriesModel$NodesModel;

    .line 1494882
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsStoriesMallIdsModel$GroupStoriesModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 1494883
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsStoriesMallIdsModel$GroupStoriesModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1494884
    :cond_2
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1494885
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsStoriesMallIdsModel$GroupStoriesModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 1494886
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsStoriesMallIdsModel$GroupStoriesModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 1494887
    :cond_4
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()LX/0zO;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0zO",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsStoriesMallIdsModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1494888
    iget-object v0, p0, LX/9T2;->a:Lcom/facebook/api/feedtype/FeedType;

    .line 1494889
    iget-object v1, v0, Lcom/facebook/api/feedtype/FeedType;->e:Ljava/lang/Object;

    move-object v0, v1

    .line 1494890
    check-cast v0, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;

    .line 1494891
    iget-object v1, v0, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->a:Ljava/lang/String;

    move-object v2, v1

    .line 1494892
    new-instance v0, LX/9Rv;

    invoke-direct {v0}, LX/9Rv;-><init>()V

    move-object v3, v0

    .line 1494893
    iget-object v0, p0, LX/9T2;->b:LX/0pn;

    invoke-virtual {v0}, LX/0pn;->d()I

    move-result v0

    int-to-long v0, v0

    .line 1494894
    iget-object v4, p0, LX/9T2;->c:LX/0Uh;

    const/16 v5, 0x3c6

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, LX/0Uh;->a(IZ)Z

    move-result v4

    move v4, v4

    .line 1494895
    if-eqz v4, :cond_0

    .line 1494896
    iget-object v4, p0, LX/9T2;->b:LX/0pn;

    iget-object v5, p0, LX/9T2;->a:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v4, v5}, LX/0pn;->b(Lcom/facebook/api/feedtype/FeedType;)LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    int-to-long v4, v4

    .line 1494897
    const-wide/16 v6, 0xa

    add-long/2addr v4, v6

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 1494898
    :cond_0
    const-string v4, "group_id"

    invoke-virtual {v3, v4, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v4, "stories_to_fetch"

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1494899
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    return-object v0
.end method

.method public final b()LX/0TF;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsStoriesMallIdsModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1494900
    new-instance v0, LX/B1V;

    iget-object v1, p0, LX/9T2;->a:Lcom/facebook/api/feedtype/FeedType;

    iget-object v2, p0, LX/9T2;->b:LX/0pn;

    invoke-direct {v0, p0, v1, v2}, LX/B1V;-><init>(LX/B1W;Lcom/facebook/api/feedtype/FeedType;LX/0pn;)V

    return-object v0
.end method
