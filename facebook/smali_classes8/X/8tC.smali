.class public LX/8tC;
.super LX/8tB;
.source ""


# instance fields
.field private final e:LX/8tE;


# direct methods
.method public constructor <init>(Landroid/view/inputmethod/InputMethodManager;LX/8vM;LX/8tF;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1412609
    invoke-direct {p0, p1, p2}, LX/8tB;-><init>(Landroid/view/inputmethod/InputMethodManager;LX/8vM;)V

    .line 1412610
    invoke-virtual {p0}, LX/8tB;->e()LX/8vE;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/8tF;->a(LX/8vE;)LX/8tE;

    move-result-object v0

    iput-object v0, p0, LX/8tC;->e:LX/8tE;

    .line 1412611
    return-void
.end method

.method public static b(LX/0QB;)LX/8tC;
    .locals 1

    .prologue
    .line 1412612
    invoke-static {p0}, LX/8tC;->c(LX/0QB;)LX/8tC;

    move-result-object v0

    return-object v0
.end method

.method public static c(LX/0QB;)LX/8tC;
    .locals 4

    .prologue
    .line 1412607
    new-instance v3, LX/8tC;

    invoke-static {p0}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    const-class v1, LX/8vM;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/8vM;

    const-class v2, LX/8tF;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/8tF;

    invoke-direct {v3, v0, v1, v2}, LX/8tC;-><init>(Landroid/view/inputmethod/InputMethodManager;LX/8vM;LX/8tF;)V

    .line 1412608
    return-object v3
.end method


# virtual methods
.method public final a()LX/333;
    .locals 1

    .prologue
    .line 1412606
    iget-object v0, p0, LX/8tC;->e:LX/8tE;

    return-object v0
.end method
