.class public LX/8w5;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public b:I

.field public c:I

.field public d:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1421587
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1421588
    invoke-virtual {p0}, LX/8w5;->a()V

    .line 1421589
    return-void
.end method

.method public synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 1421609
    invoke-direct {p0}, LX/8w5;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/support/v7/widget/GridLayout;Landroid/view/View;LX/8vz;IZ)I
    .locals 2

    .prologue
    .line 1421608
    iget v0, p0, LX/8w5;->b:I

    invoke-static {p1}, LX/3sF;->a(Landroid/view/ViewGroup;)I

    move-result v1

    invoke-virtual {p3, p2, p4, v1}, LX/8vz;->a(Landroid/view/View;II)I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public a(Z)I
    .locals 2

    .prologue
    .line 1421604
    if-nez p1, :cond_0

    .line 1421605
    iget v0, p0, LX/8w5;->d:I

    invoke-static {v0}, Landroid/support/v7/widget/GridLayout;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1421606
    const v0, 0x186a0

    .line 1421607
    :goto_0
    return v0

    :cond_0
    iget v0, p0, LX/8w5;->b:I

    iget v1, p0, LX/8w5;->c:I

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 1421600
    iput v0, p0, LX/8w5;->b:I

    .line 1421601
    iput v0, p0, LX/8w5;->c:I

    .line 1421602
    const/4 v0, 0x2

    iput v0, p0, LX/8w5;->d:I

    .line 1421603
    return-void
.end method

.method public a(II)V
    .locals 1

    .prologue
    .line 1421597
    iget v0, p0, LX/8w5;->b:I

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LX/8w5;->b:I

    .line 1421598
    iget v0, p0, LX/8w5;->c:I

    invoke-static {v0, p2}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LX/8w5;->c:I

    .line 1421599
    return-void
.end method

.method public final a(Landroid/support/v7/widget/GridLayout;Landroid/view/View;LX/8wH;LX/8wC;I)V
    .locals 2

    .prologue
    .line 1421591
    iget v0, p0, LX/8w5;->d:I

    invoke-virtual {p3}, LX/8wH;->a()I

    move-result v1

    and-int/2addr v0, v1

    iput v0, p0, LX/8w5;->d:I

    .line 1421592
    iget-boolean v0, p4, LX/8wC;->a:Z

    .line 1421593
    iget-object v1, p3, LX/8wH;->d:LX/8vz;

    invoke-static {v1, v0}, Landroid/support/v7/widget/GridLayout;->a(LX/8vz;Z)LX/8vz;

    move-result-object v0

    .line 1421594
    invoke-static {p1}, LX/3sF;->a(Landroid/view/ViewGroup;)I

    move-result v1

    invoke-virtual {v0, p2, p5, v1}, LX/8vz;->a(Landroid/view/View;II)I

    move-result v0

    .line 1421595
    sub-int v1, p5, v0

    invoke-virtual {p0, v0, v1}, LX/8w5;->a(II)V

    .line 1421596
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1421590
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Bounds{before="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, LX/8w5;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", after="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/8w5;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
