.class public LX/9HS;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Lcom/facebook/feedback/ui/environment/HasCommentActions;",
        ":",
        "LX/1Pn;",
        ":",
        "Lcom/facebook/feedback/ui/environment/HasLoggingParams;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9HT;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/9HS",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/9HT;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1461293
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1461294
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/9HS;->b:LX/0Zi;

    .line 1461295
    iput-object p1, p0, LX/9HS;->a:LX/0Ot;

    .line 1461296
    return-void
.end method

.method public static a(LX/0QB;)LX/9HS;
    .locals 4

    .prologue
    .line 1461297
    const-class v1, LX/9HS;

    monitor-enter v1

    .line 1461298
    :try_start_0
    sget-object v0, LX/9HS;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1461299
    sput-object v2, LX/9HS;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1461300
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1461301
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1461302
    new-instance v3, LX/9HS;

    const/16 p0, 0x1dd1

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/9HS;-><init>(LX/0Ot;)V

    .line 1461303
    move-object v0, v3

    .line 1461304
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1461305
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/9HS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1461306
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1461307
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 1461308
    check-cast p2, LX/9HR;

    .line 1461309
    iget-object v0, p0, LX/9HS;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9HT;

    iget-object v1, p2, LX/9HR;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/9HR;->b:LX/9FA;

    .line 1461310
    iget-object v3, v0, LX/9HT;->a:LX/9Hj;

    invoke-virtual {v3, v1, v2}, LX/9Hj;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/9FA;)Ljava/util/List;

    move-result-object v3

    .line 1461311
    invoke-static {v3, p1}, LX/8qs;->a(Ljava/util/List;Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 1461312
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    const p0, 0x7f0b08cf

    invoke-virtual {v4, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/4 p0, 0x7

    const p2, 0x7f0b0af8

    invoke-interface {v4, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    invoke-interface {v4, v3}, LX/1Di;->a(Ljava/lang/CharSequence;)LX/1Di;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1461313
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1461314
    invoke-static {}, LX/1dS;->b()V

    .line 1461315
    const/4 v0, 0x0

    return-object v0
.end method
