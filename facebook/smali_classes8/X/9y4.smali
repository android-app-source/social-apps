.class public final LX/9y4;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 17

    .prologue
    .line 1593938
    const/4 v12, 0x0

    .line 1593939
    const/4 v11, 0x0

    .line 1593940
    const/4 v10, 0x0

    .line 1593941
    const-wide/16 v8, 0x0

    .line 1593942
    const/4 v5, 0x0

    .line 1593943
    const-wide/16 v6, 0x0

    .line 1593944
    const/4 v4, 0x0

    .line 1593945
    const/4 v3, 0x0

    .line 1593946
    const/4 v2, 0x0

    .line 1593947
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v13, v14, :cond_b

    .line 1593948
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1593949
    const/4 v2, 0x0

    .line 1593950
    :goto_0
    return v2

    .line 1593951
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_8

    .line 1593952
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1593953
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1593954
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 1593955
    const-string v6, "hour_ranges"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1593956
    invoke-static/range {p0 .. p1}, LX/4aY;->a(LX/15w;LX/186;)I

    move-result v2

    move v15, v2

    goto :goto_1

    .line 1593957
    :cond_1
    const-string v6, "place_category"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1593958
    invoke-static/range {p0 .. p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v2

    move v14, v2

    goto :goto_1

    .line 1593959
    :cond_2
    const-string v6, "place_info_blurb_breadcrumbs"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1593960
    invoke-static/range {p0 .. p1}, LX/9y3;->a(LX/15w;LX/186;)I

    move-result v2

    move v13, v2

    goto :goto_1

    .line 1593961
    :cond_3
    const-string v6, "place_info_blurb_rating"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1593962
    const/4 v2, 0x1

    .line 1593963
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 1593964
    :cond_4
    const-string v6, "price_range"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1593965
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v12, v2

    goto :goto_1

    .line 1593966
    :cond_5
    const-string v6, "rating_count"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1593967
    const/4 v2, 0x1

    .line 1593968
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v8, v2

    move-wide v10, v6

    goto :goto_1

    .line 1593969
    :cond_6
    const-string v6, "timezone"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1593970
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v9, v2

    goto/16 :goto_1

    .line 1593971
    :cond_7
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1593972
    :cond_8
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1593973
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1593974
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1593975
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1593976
    if-eqz v3, :cond_9

    .line 1593977
    const/4 v3, 0x3

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1593978
    :cond_9
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1593979
    if-eqz v8, :cond_a

    .line 1593980
    const/4 v3, 0x5

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v10

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1593981
    :cond_a
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1593982
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_b
    move v13, v10

    move v14, v11

    move v15, v12

    move v12, v5

    move-wide v10, v6

    move/from16 v16, v4

    move-wide v4, v8

    move v8, v2

    move/from16 v9, v16

    goto/16 :goto_1
.end method
