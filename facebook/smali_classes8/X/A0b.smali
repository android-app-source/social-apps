.class public final LX/A0b;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:LX/2uF;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "cards"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1606086
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/2uF;)LX/A0b;
    .locals 0
    .param p1    # LX/2uF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "cards"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 1606084
    iput-object p1, p0, LX/A0b;->b:LX/2uF;

    .line 1606085
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;)LX/A0b;
    .locals 0
    .param p1    # Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1606045
    iput-object p1, p0, LX/A0b;->a:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;

    .line 1606046
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/A0b;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1606082
    iput-object p1, p0, LX/A0b;->d:Ljava/lang/String;

    .line 1606083
    return-object p0
.end method

.method public final a()Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;
    .locals 13

    .prologue
    .line 1606051
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1606052
    iget-object v1, p0, LX/A0b;->a:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXButtonStyle;

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1606053
    iget-object v2, p0, LX/A0b;->b:LX/2uF;

    invoke-static {v2, v0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v2

    .line 1606054
    iget-object v3, p0, LX/A0b;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1606055
    iget-object v4, p0, LX/A0b;->d:Ljava/lang/String;

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1606056
    iget-object v5, p0, LX/A0b;->e:Ljava/lang/String;

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1606057
    iget-object v6, p0, LX/A0b;->f:Ljava/lang/String;

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1606058
    iget-object v7, p0, LX/A0b;->g:Ljava/lang/String;

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1606059
    iget-object v8, p0, LX/A0b;->h:Ljava/lang/String;

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1606060
    iget-object v9, p0, LX/A0b;->i:Ljava/lang/String;

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1606061
    iget-object v10, p0, LX/A0b;->j:Ljava/lang/String;

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1606062
    iget-object v11, p0, LX/A0b;->k:Ljava/lang/String;

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 1606063
    const/16 v12, 0xb

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 1606064
    const/4 v12, 0x0

    invoke-virtual {v0, v12, v1}, LX/186;->b(II)V

    .line 1606065
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1606066
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1606067
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1606068
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1606069
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1606070
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1606071
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1606072
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 1606073
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 1606074
    const/16 v1, 0xa

    invoke-virtual {v0, v1, v11}, LX/186;->b(II)V

    .line 1606075
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1606076
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1606077
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1606078
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1606079
    new-instance v0, LX/15i;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1606080
    new-instance v1, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;

    invoke-direct {v1, v0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;-><init>(LX/15i;)V

    .line 1606081
    return-object v1
.end method

.method public final b(Ljava/lang/String;)LX/A0b;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1606087
    iput-object p1, p0, LX/A0b;->e:Ljava/lang/String;

    .line 1606088
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/A0b;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1606049
    iput-object p1, p0, LX/A0b;->f:Ljava/lang/String;

    .line 1606050
    return-object p0
.end method

.method public final d(Ljava/lang/String;)LX/A0b;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1606047
    iput-object p1, p0, LX/A0b;->g:Ljava/lang/String;

    .line 1606048
    return-object p0
.end method

.method public final e(Ljava/lang/String;)LX/A0b;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1606043
    iput-object p1, p0, LX/A0b;->h:Ljava/lang/String;

    .line 1606044
    return-object p0
.end method

.method public final f(Ljava/lang/String;)LX/A0b;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1606041
    iput-object p1, p0, LX/A0b;->j:Ljava/lang/String;

    .line 1606042
    return-object p0
.end method
