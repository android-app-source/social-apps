.class public final LX/9kX;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1531941
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_8

    .line 1531942
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1531943
    :goto_0
    return v1

    .line 1531944
    :cond_0
    const-string v9, "is_choosable"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 1531945
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v4, v0

    move v0, v2

    .line 1531946
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_6

    .line 1531947
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1531948
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1531949
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 1531950
    const-string v9, "child_categories"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 1531951
    invoke-static {p0, p1}, LX/9kW;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1531952
    :cond_2
    const-string v9, "fbid"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1531953
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 1531954
    :cond_3
    const-string v9, "id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1531955
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 1531956
    :cond_4
    const-string v9, "name"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1531957
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 1531958
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1531959
    :cond_6
    const/4 v8, 0x5

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1531960
    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 1531961
    invoke-virtual {p1, v2, v6}, LX/186;->b(II)V

    .line 1531962
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1531963
    if-eqz v0, :cond_7

    .line 1531964
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v4}, LX/186;->a(IZ)V

    .line 1531965
    :cond_7
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1531966
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_8
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 1531967
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1531968
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1531969
    if-eqz v0, :cond_5

    .line 1531970
    const-string v1, "child_categories"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1531971
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1531972
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1531973
    if-eqz v1, :cond_4

    .line 1531974
    const-string v2, "edges"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1531975
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1531976
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v3

    if-ge v2, v3, :cond_3

    .line 1531977
    invoke-virtual {p0, v1, v2}, LX/15i;->q(II)I

    move-result v3

    .line 1531978
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1531979
    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, LX/15i;->g(II)I

    move-result v4

    .line 1531980
    if-eqz v4, :cond_2

    .line 1531981
    const-string v0, "node"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1531982
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1531983
    const/4 v0, 0x0

    invoke-virtual {p0, v4, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1531984
    if-eqz v0, :cond_0

    .line 1531985
    const-string v3, "name"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1531986
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1531987
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v4, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1531988
    if-eqz v0, :cond_1

    .line 1531989
    const-string v3, "page_id"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1531990
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1531991
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1531992
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1531993
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1531994
    :cond_3
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1531995
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1531996
    :cond_5
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1531997
    if-eqz v0, :cond_6

    .line 1531998
    const-string v1, "fbid"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1531999
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1532000
    :cond_6
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1532001
    if-eqz v0, :cond_7

    .line 1532002
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1532003
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1532004
    :cond_7
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1532005
    if-eqz v0, :cond_8

    .line 1532006
    const-string v1, "is_choosable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1532007
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1532008
    :cond_8
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1532009
    if-eqz v0, :cond_9

    .line 1532010
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1532011
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1532012
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1532013
    return-void
.end method
