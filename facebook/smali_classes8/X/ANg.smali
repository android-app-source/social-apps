.class public final LX/ANg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:Lcom/facebook/cameracore/ui/FocusView;


# direct methods
.method public constructor <init>(Lcom/facebook/cameracore/ui/FocusView;)V
    .locals 0

    .prologue
    .line 1668308
    iput-object p1, p0, LX/ANg;->a:Lcom/facebook/cameracore/ui/FocusView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 3

    .prologue
    .line 1668303
    iget-object v1, p0, LX/ANg;->a:Lcom/facebook/cameracore/ui/FocusView;

    iget-object v0, p0, LX/ANg;->a:Lcom/facebook/cameracore/ui/FocusView;

    iget v2, v0, Lcom/facebook/cameracore/ui/FocusView;->b:F

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    mul-float/2addr v0, v2

    const/high16 v2, 0x40400000    # 3.0f

    div-float/2addr v0, v2

    .line 1668304
    iput v0, v1, Lcom/facebook/cameracore/ui/FocusView;->e:F

    .line 1668305
    iget-object v0, p0, LX/ANg;->a:Lcom/facebook/cameracore/ui/FocusView;

    iget-object v0, v0, Lcom/facebook/cameracore/ui/FocusView;->a:Landroid/graphics/Paint;

    const/high16 v1, 0x437f0000    # 255.0f

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1668306
    iget-object v0, p0, LX/ANg;->a:Lcom/facebook/cameracore/ui/FocusView;

    invoke-virtual {v0}, Lcom/facebook/cameracore/ui/FocusView;->invalidate()V

    .line 1668307
    return-void
.end method
