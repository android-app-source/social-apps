.class public final LX/8jq;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchTrayTaggedStickersWithPreviewsQueryModel;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1392796
    const-class v1, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchTrayTaggedStickersWithPreviewsQueryModel;

    const v0, -0x7fdcef8c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x2

    const-string v5, "FetchTrayTaggedStickersWithPreviewsQuery"

    const-string v6, "4afefe8744d585a754a9eef93e8e2330"

    const-string v7, "nodes"

    const-string v8, "10155093175006729"

    const/4 v9, 0x0

    .line 1392797
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1392798
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1392799
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1392800
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1392801
    sparse-switch v0, :sswitch_data_0

    .line 1392802
    :goto_0
    return-object p1

    .line 1392803
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1392804
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1392805
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1392806
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1392807
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1392808
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x51392de8 -> :sswitch_3
        -0x46c3012f -> :sswitch_0
        0x1df56d39 -> :sswitch_1
        0x56cb4b91 -> :sswitch_5
        0x73a026b5 -> :sswitch_2
        0x763c4507 -> :sswitch_4
    .end sparse-switch
.end method

.method public final l()Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;
    .locals 2

    .prologue
    .line 1392809
    new-instance v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQL$FetchTrayTaggedStickersWithPreviewsQueryString$1;

    const-class v1, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchTrayTaggedStickersWithPreviewsQueryModel;

    invoke-direct {v0, p0, v1}, Lcom/facebook/stickers/graphql/FetchStickersGraphQL$FetchTrayTaggedStickersWithPreviewsQueryString$1;-><init>(LX/8jq;Ljava/lang/Class;)V

    return-object v0
.end method
