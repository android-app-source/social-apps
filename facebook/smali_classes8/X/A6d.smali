.class public final LX/A6d;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/A6e;


# direct methods
.method public constructor <init>(LX/A6e;)V
    .locals 0

    .prologue
    .line 1624564
    iput-object p1, p0, LX/A6d;->a:LX/A6e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1624565
    iget-object v0, p0, LX/A6d;->a:LX/A6e;

    iget-object v0, v0, LX/A6e;->j:LX/A6X;

    iget-object v1, p0, LX/A6d;->a:LX/A6e;

    iget-object v1, v1, LX/A6e;->a:LX/A6N;

    .line 1624566
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 1624567
    const-string p0, "algorithm"

    invoke-virtual {v1}, LX/A6N;->name()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v2, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1624568
    sget-object p0, LX/A6W;->DICTIONARY_LOAD_FAILED:LX/A6W;

    iget-object p0, p0, LX/A6W;->eventName:Ljava/lang/String;

    invoke-static {v0, p0, v2}, LX/A6X;->a(LX/A6X;Ljava/lang/String;Ljava/util/Map;)V

    .line 1624569
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1624570
    check-cast p1, Ljava/util/Map;

    .line 1624571
    iget-object v0, p0, LX/A6d;->a:LX/A6e;

    iget-object v0, v0, LX/A6e;->n:LX/A73;

    .line 1624572
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 1624573
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1624574
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1624575
    iget-object p0, v0, LX/A73;->b:LX/A72;

    invoke-virtual {p0, v2, v1}, LX/A72;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1624576
    :cond_0
    return-void
.end method
