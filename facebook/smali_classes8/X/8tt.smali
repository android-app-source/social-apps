.class public final LX/8tt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8pS;


# instance fields
.field public final synthetic a:Lcom/facebook/widget/ratingbar/AnimatedRatingBar;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/ratingbar/AnimatedRatingBar;)V
    .locals 0

    .prologue
    .line 1414072
    iput-object p1, p0, LX/8tt;->a:Lcom/facebook/widget/ratingbar/AnimatedRatingBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/widget/ratingbar/AnimatedRatingBar;B)V
    .locals 0

    .prologue
    .line 1414071
    invoke-direct {p0, p1}, LX/8tt;-><init>(Lcom/facebook/widget/ratingbar/AnimatedRatingBar;)V

    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1414067
    iget-object v0, p0, LX/8tt;->a:Lcom/facebook/widget/ratingbar/AnimatedRatingBar;

    invoke-virtual {v0}, Lcom/facebook/widget/ratingbar/AnimatedRatingBar;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f040097

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1414068
    invoke-virtual {p1}, Landroid/view/View;->clearAnimation()V

    .line 1414069
    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1414070
    return-void
.end method

.method private b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1414063
    iget-object v0, p0, LX/8tt;->a:Lcom/facebook/widget/ratingbar/AnimatedRatingBar;

    invoke-virtual {v0}, Lcom/facebook/widget/ratingbar/AnimatedRatingBar;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f040096

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1414064
    invoke-virtual {p1}, Landroid/view/View;->clearAnimation()V

    .line 1414065
    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1414066
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 4

    .prologue
    .line 1414049
    if-lez p1, :cond_0

    .line 1414050
    iget-object v0, p0, LX/8tt;->a:Lcom/facebook/widget/ratingbar/AnimatedRatingBar;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/ratingbar/AnimatedRatingBar;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, LX/8tt;->a(Landroid/view/View;)V

    .line 1414051
    :cond_0
    add-int/lit8 v0, p1, -0x2

    :goto_0
    if-ltz v0, :cond_1

    .line 1414052
    iget-object v1, p0, LX/8tt;->a:Lcom/facebook/widget/ratingbar/AnimatedRatingBar;

    invoke-virtual {v1}, Lcom/facebook/widget/ratingbar/AnimatedRatingBar;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f040098

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 1414053
    add-int/lit8 v2, p1, -0x1

    sub-int/2addr v2, v0

    mul-int/lit8 v2, v2, 0x32

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 1414054
    iget-object v2, p0, LX/8tt;->a:Lcom/facebook/widget/ratingbar/AnimatedRatingBar;

    invoke-virtual {v2, v0}, Lcom/facebook/widget/ratingbar/AnimatedRatingBar;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1414055
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1414056
    :cond_1
    return-void
.end method

.method public final a(II)V
    .locals 2

    .prologue
    .line 1414057
    if-lez p1, :cond_0

    .line 1414058
    iget-object v0, p0, LX/8tt;->a:Lcom/facebook/widget/ratingbar/AnimatedRatingBar;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/ratingbar/AnimatedRatingBar;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1414059
    invoke-direct {p0, v0}, LX/8tt;->a(Landroid/view/View;)V

    .line 1414060
    :cond_0
    iget-object v0, p0, LX/8tt;->a:Lcom/facebook/widget/ratingbar/AnimatedRatingBar;

    add-int/lit8 v1, p2, -0x1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/ratingbar/AnimatedRatingBar;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1414061
    invoke-direct {p0, v0}, LX/8tt;->b(Landroid/view/View;)V

    .line 1414062
    return-void
.end method
