.class public final LX/914;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/903;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;)V
    .locals 0

    .prologue
    .line 1430031
    iput-object p1, p0, LX/914;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 1430032
    iget-object v0, p0, LX/914;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;

    invoke-static {v0}, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->e(Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;)Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->a(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)LX/937;

    move-result-object v0

    .line 1430033
    iput-object v5, v0, LX/937;->k:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1430034
    move-object v0, v0

    .line 1430035
    invoke-virtual {v0}, LX/937;->a()Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    move-result-object v0

    .line 1430036
    iget-object v1, p0, LX/914;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;

    .line 1430037
    iget-object v2, v1, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->a:LX/90T;

    invoke-interface {v2, v0}, LX/90T;->a(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)V

    .line 1430038
    iget-object v1, p0, LX/914;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;

    invoke-virtual {v1}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, -0x1

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    const-string v4, "minutiae_object"

    .line 1430039
    iget-object v6, v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->j:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-object v0, v6

    .line 1430040
    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1430041
    iget-object v0, p0, LX/914;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;

    sget-object v1, LX/8zx;->NORMAL:LX/8zx;

    invoke-static {v0, v1, v5}, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->a$redex0(Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;LX/8zx;Landroid/view/View$OnClickListener;)V

    .line 1430042
    return-void
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 1430043
    iget-object v0, p0, LX/914;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;

    invoke-static {v0}, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->e(Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;)Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    move-result-object v0

    .line 1430044
    iget-object v1, v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->j:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-object v1, v1

    .line 1430045
    if-eqz v1, :cond_0

    .line 1430046
    iget-object v1, v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->j:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-object v1, v1

    .line 1430047
    invoke-virtual {v1}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->b()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1430048
    :cond_0
    :goto_0
    return-void

    .line 1430049
    :cond_1
    iget-object v2, p0, LX/914;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 1430050
    iget-object v1, v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v3, v1

    .line 1430051
    iget-object v1, v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->j:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-object v4, v1

    .line 1430052
    iget-object v1, v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->j:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-object v0, v1

    .line 1430053
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->b()LX/0Px;

    move-result-object v0

    invoke-static {v2, v3, v4, v0}, LX/925;->a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/composer/minutiae/model/MinutiaeObject;LX/0Px;)Landroid/content/Intent;

    move-result-object v1

    .line 1430054
    if-eqz v1, :cond_0

    .line 1430055
    iget-object v0, p0, LX/914;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x1

    iget-object v3, p0, LX/914;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1430056
    iget-object v0, p0, LX/914;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeVerbSelectorFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 1430057
    return-void
.end method
