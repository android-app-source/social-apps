.class public LX/998;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:Landroid/widget/Scroller;

.field public b:LX/4Bb;

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1446709
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1446710
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/998;->c:Z

    .line 1446711
    invoke-direct {p0}, LX/998;->a()V

    .line 1446712
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1446705
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1446706
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/998;->c:Z

    .line 1446707
    invoke-direct {p0}, LX/998;->a()V

    .line 1446708
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1446701
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1446702
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/998;->c:Z

    .line 1446703
    invoke-direct {p0}, LX/998;->a()V

    .line 1446704
    return-void
.end method

.method private final a()V
    .locals 5

    .prologue
    .line 1446699
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, LX/998;

    new-instance v1, LX/4Bb;

    const-class v2, Landroid/content/Context;

    invoke-interface {v3, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-direct {v1, v2}, LX/4Bb;-><init>(Landroid/content/Context;)V

    move-object v2, v1

    check-cast v2, LX/4Bb;

    new-instance v1, LX/31L;

    const-class v4, Landroid/content/Context;

    invoke-interface {v3, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    new-instance v0, LX/4Bc;

    invoke-direct {v0}, LX/4Bc;-><init>()V

    move-object v0, v0

    move-object v0, v0

    check-cast v0, Landroid/view/animation/Interpolator;

    invoke-direct {v1, v4, v0}, LX/31L;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    move-object v3, v1

    check-cast v3, Landroid/widget/Scroller;

    iput-object v2, p0, LX/998;->b:LX/4Bb;

    iput-object v3, p0, LX/998;->a:Landroid/widget/Scroller;

    .line 1446700
    return-void
.end method

.method private a(III)V
    .locals 7

    .prologue
    .line 1446688
    invoke-virtual {p0}, LX/998;->g()Z

    .line 1446689
    invoke-virtual {p0}, LX/998;->getScrollX()I

    move-result v1

    .line 1446690
    sub-int v3, p1, v1

    .line 1446691
    invoke-virtual {p0}, LX/998;->getScrollY()I

    move-result v2

    .line 1446692
    sub-int v4, p2, v2

    .line 1446693
    if-gez p3, :cond_0

    .line 1446694
    iget-object v0, p0, LX/998;->b:LX/4Bb;

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v5

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 1446695
    int-to-float v6, v5

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    iget p1, v0, LX/4Bb;->a:F

    mul-float/2addr v6, p1

    float-to-int v6, v6

    move v5, v6

    .line 1446696
    :goto_0
    iget-object v0, p0, LX/998;->a:Landroid/widget/Scroller;

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 1446697
    invoke-virtual {p0}, LX/998;->invalidate()V

    .line 1446698
    return-void

    :cond_0
    move v5, p3

    goto :goto_0
.end method


# virtual methods
.method public final a(II)V
    .locals 1

    .prologue
    .line 1446686
    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, v0}, LX/998;->a(III)V

    .line 1446687
    return-void
.end method

.method public a(IIZ)V
    .locals 1

    .prologue
    .line 1446679
    if-eqz p3, :cond_0

    .line 1446680
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/998;->c:Z

    .line 1446681
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;->scrollTo(II)V

    .line 1446682
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/998;->c:Z

    .line 1446683
    invoke-virtual {p0}, LX/998;->postInvalidate()V

    .line 1446684
    :goto_0
    return-void

    .line 1446685
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;->scrollTo(II)V

    goto :goto_0
.end method

.method public final computeScroll()V
    .locals 3

    .prologue
    .line 1446656
    iget-object v0, p0, LX/998;->a:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1446657
    iget-object v0, p0, LX/998;->a:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    .line 1446658
    iget-object v1, p0, LX/998;->a:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrY()I

    move-result v1

    .line 1446659
    iget-object v2, p0, LX/998;->a:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getFinalX()I

    move-result v2

    if-ne v0, v2, :cond_0

    iget-object v2, p0, LX/998;->a:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getFinalY()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 1446660
    iget-object v2, p0, LX/998;->a:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->abortAnimation()V

    .line 1446661
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, LX/998;->a(IIZ)V

    .line 1446662
    :cond_1
    return-void
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 1446678
    iget-object v0, p0, LX/998;->a:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 1446674
    invoke-virtual {p0}, LX/998;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1446675
    iget-object v0, p0, LX/998;->a:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 1446676
    const/4 v0, 0x1

    .line 1446677
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final invalidate()V
    .locals 1

    .prologue
    .line 1446671
    iget-boolean v0, p0, LX/998;->c:Z

    if-nez v0, :cond_0

    .line 1446672
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->invalidate()V

    .line 1446673
    :cond_0
    return-void
.end method

.method public final invalidate(IIII)V
    .locals 1

    .prologue
    .line 1446668
    iget-boolean v0, p0, LX/998;->c:Z

    if-nez v0, :cond_0

    .line 1446669
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/widget/CustomFrameLayout;->invalidate(IIII)V

    .line 1446670
    :cond_0
    return-void
.end method

.method public final invalidate(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 1446665
    iget-boolean v0, p0, LX/998;->c:Z

    if-nez v0, :cond_0

    .line 1446666
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->invalidate(Landroid/graphics/Rect;)V

    .line 1446667
    :cond_0
    return-void
.end method

.method public final scrollTo(II)V
    .locals 1

    .prologue
    .line 1446663
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/998;->a(IIZ)V

    .line 1446664
    return-void
.end method
