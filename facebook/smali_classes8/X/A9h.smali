.class public final LX/A9h;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1635561
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1635562
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1635563
    :goto_0
    return v1

    .line 1635564
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1635565
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_3

    .line 1635566
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1635567
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1635568
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 1635569
    const-string v3, "tips"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1635570
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1635571
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->START_ARRAY:LX/15z;

    if-ne v2, v3, :cond_2

    .line 1635572
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_ARRAY:LX/15z;

    if-eq v2, v3, :cond_2

    .line 1635573
    invoke-static {p0, p1}, LX/A9L;->b(LX/15w;LX/186;)I

    move-result v2

    .line 1635574
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1635575
    :cond_2
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    move v0, v0

    .line 1635576
    goto :goto_1

    .line 1635577
    :cond_3
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1635578
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1635579
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 12

    .prologue
    .line 1635580
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1635581
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1635582
    if-eqz v0, :cond_12

    .line 1635583
    const-string v1, "tips"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635584
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1635585
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v3

    if-ge v2, v3, :cond_11

    .line 1635586
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result v3

    .line 1635587
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1635588
    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1635589
    if-eqz v4, :cond_0

    .line 1635590
    const-string v5, "action_as_button_text"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635591
    invoke-virtual {p2, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1635592
    :cond_0
    const/4 v4, 0x1

    invoke-virtual {p0, v3, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1635593
    if-eqz v4, :cond_1

    .line 1635594
    const-string v5, "body_text"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635595
    invoke-virtual {p2, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1635596
    :cond_1
    const/4 v4, 0x2

    invoke-virtual {p0, v3, v4}, LX/15i;->g(II)I

    move-result v4

    .line 1635597
    if-eqz v4, :cond_6

    .line 1635598
    const-string v5, "image"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635599
    const-wide/16 v10, 0x0

    const/4 v9, 0x0

    .line 1635600
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1635601
    invoke-virtual {p0, v4, v9, v9}, LX/15i;->a(III)I

    move-result v6

    .line 1635602
    if-eqz v6, :cond_2

    .line 1635603
    const-string v7, "height"

    invoke-virtual {p2, v7}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635604
    invoke-virtual {p2, v6}, LX/0nX;->b(I)V

    .line 1635605
    :cond_2
    const/4 v6, 0x1

    invoke-virtual {p0, v4, v6, v10, v11}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 1635606
    cmpl-double v8, v6, v10

    if-eqz v8, :cond_3

    .line 1635607
    const-string v8, "scale"

    invoke-virtual {p2, v8}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635608
    invoke-virtual {p2, v6, v7}, LX/0nX;->a(D)V

    .line 1635609
    :cond_3
    const/4 v6, 0x2

    invoke-virtual {p0, v4, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v6

    .line 1635610
    if-eqz v6, :cond_4

    .line 1635611
    const-string v7, "uri"

    invoke-virtual {p2, v7}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635612
    invoke-virtual {p2, v6}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1635613
    :cond_4
    const/4 v6, 0x3

    invoke-virtual {p0, v4, v6, v9}, LX/15i;->a(III)I

    move-result v6

    .line 1635614
    if-eqz v6, :cond_5

    .line 1635615
    const-string v7, "width"

    invoke-virtual {p2, v7}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635616
    invoke-virtual {p2, v6}, LX/0nX;->b(I)V

    .line 1635617
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1635618
    :cond_6
    const/4 v4, 0x3

    invoke-virtual {p0, v3, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1635619
    if-eqz v4, :cond_7

    .line 1635620
    const-string v5, "image_uri"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635621
    invoke-virtual {p2, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1635622
    :cond_7
    const/4 v4, 0x4

    invoke-virtual {p0, v3, v4}, LX/15i;->b(II)Z

    move-result v4

    .line 1635623
    if-eqz v4, :cond_8

    .line 1635624
    const-string v5, "is_dismissible"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635625
    invoke-virtual {p2, v4}, LX/0nX;->a(Z)V

    .line 1635626
    :cond_8
    const/4 v4, 0x5

    invoke-virtual {p0, v3, v4}, LX/15i;->g(II)I

    move-result v4

    .line 1635627
    if-eqz v4, :cond_e

    .line 1635628
    const-string v5, "native_mobile_action"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635629
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1635630
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1635631
    invoke-virtual {p0, v4, v6}, LX/15i;->g(II)I

    move-result v5

    .line 1635632
    if-eqz v5, :cond_9

    .line 1635633
    const-string v5, "__type__"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635634
    invoke-static {p0, v4, v6, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1635635
    :cond_9
    invoke-virtual {p0, v4, v7}, LX/15i;->g(II)I

    move-result v5

    .line 1635636
    if-eqz v5, :cond_a

    .line 1635637
    const-string v5, "destination"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635638
    invoke-virtual {p0, v4, v7}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v5}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1635639
    :cond_a
    const/4 v5, 0x2

    invoke-virtual {p0, v4, v5}, LX/15i;->g(II)I

    move-result v5

    .line 1635640
    if-eqz v5, :cond_c

    .line 1635641
    const-string v6, "display_text"

    invoke-virtual {p2, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635642
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1635643
    const/4 v6, 0x0

    invoke-virtual {p0, v5, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v6

    .line 1635644
    if-eqz v6, :cond_b

    .line 1635645
    const-string v7, "text"

    invoke-virtual {p2, v7}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635646
    invoke-virtual {p2, v6}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1635647
    :cond_b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1635648
    :cond_c
    const/4 v5, 0x3

    invoke-virtual {p0, v4, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v5

    .line 1635649
    if-eqz v5, :cond_d

    .line 1635650
    const-string v6, "uri"

    invoke-virtual {p2, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635651
    invoke-virtual {p2, v5}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1635652
    :cond_d
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1635653
    :cond_e
    const/4 v4, 0x6

    invoke-virtual {p0, v3, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1635654
    if-eqz v4, :cond_f

    .line 1635655
    const-string v5, "secondary_action_uri"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635656
    invoke-virtual {p2, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1635657
    :cond_f
    const/4 v4, 0x7

    invoke-virtual {p0, v3, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1635658
    if-eqz v4, :cond_10

    .line 1635659
    const-string v5, "title_text"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635660
    invoke-virtual {p2, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1635661
    :cond_10
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1635662
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 1635663
    :cond_11
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1635664
    :cond_12
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1635665
    return-void
.end method
