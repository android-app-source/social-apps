.class public LX/9hk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Bq;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:[LX/3d2;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/9hl;)V
    .locals 4

    .prologue
    .line 1526977
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1526978
    iput-object p1, p0, LX/9hk;->b:Ljava/lang/String;

    .line 1526979
    const/4 v0, 0x3

    new-array v0, v0, [LX/3d2;

    const/4 v1, 0x0

    new-instance v2, LX/3d2;

    const-class v3, Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-direct {v2, v3, p2}, LX/3d2;-><init>(Ljava/lang/Class;LX/3d4;)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, LX/3d2;

    const-class v3, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;

    invoke-direct {v2, v3, p2}, LX/3d2;-><init>(Ljava/lang/Class;LX/3d4;)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, LX/3d2;

    const-class v3, Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;

    invoke-direct {v2, v3, p2}, LX/3d2;-><init>(Ljava/lang/Class;LX/3d4;)V

    aput-object v2, v0, v1

    iput-object v0, p0, LX/9hk;->c:[LX/3d2;

    .line 1526980
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Z)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;Z)TT;"
        }
    .end annotation

    .prologue
    .line 1526971
    iget-object v1, p0, LX/9hk;->c:[LX/3d2;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 1526972
    invoke-virtual {v3, p1}, LX/3d2;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    .line 1526973
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1526974
    :cond_0
    return-object p1
.end method

.method public final a()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1526975
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, LX/9hk;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1526976
    const-string v0, "MediaVisitor"

    return-object v0
.end method
