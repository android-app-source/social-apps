.class public final LX/9zG;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:I

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:I

.field public m:Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel$ResultsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1600263
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;
    .locals 15

    .prologue
    .line 1600264
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1600265
    iget-object v1, p0, LX/9zG;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/186;->c(Ljava/util/List;)I

    move-result v1

    .line 1600266
    iget-object v2, p0, LX/9zG;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1600267
    iget-object v3, p0, LX/9zG;->d:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1600268
    iget-object v4, p0, LX/9zG;->e:Ljava/lang/String;

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1600269
    iget-object v5, p0, LX/9zG;->j:Ljava/lang/String;

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1600270
    iget-object v6, p0, LX/9zG;->k:Ljava/lang/String;

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1600271
    iget-object v7, p0, LX/9zG;->m:Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel$ResultsModel;

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1600272
    iget-object v8, p0, LX/9zG;->n:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-virtual {v0, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 1600273
    iget-object v9, p0, LX/9zG;->o:Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1600274
    iget-object v10, p0, LX/9zG;->p:LX/0Px;

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/util/List;)I

    move-result v10

    .line 1600275
    iget-object v11, p0, LX/9zG;->q:Ljava/lang/String;

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 1600276
    iget-object v12, p0, LX/9zG;->r:Ljava/lang/String;

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 1600277
    const/16 v13, 0x12

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 1600278
    const/4 v13, 0x0

    invoke-virtual {v0, v13, v1}, LX/186;->b(II)V

    .line 1600279
    const/4 v1, 0x1

    iget v13, p0, LX/9zG;->b:I

    const/4 v14, 0x0

    invoke-virtual {v0, v1, v13, v14}, LX/186;->a(III)V

    .line 1600280
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1600281
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1600282
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1600283
    const/4 v1, 0x5

    iget-boolean v2, p0, LX/9zG;->f:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 1600284
    const/4 v1, 0x6

    iget-boolean v2, p0, LX/9zG;->g:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 1600285
    const/4 v1, 0x7

    iget-boolean v2, p0, LX/9zG;->h:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 1600286
    const/16 v1, 0x8

    iget-boolean v2, p0, LX/9zG;->i:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 1600287
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1600288
    const/16 v1, 0xa

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1600289
    const/16 v1, 0xb

    iget v2, p0, LX/9zG;->l:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/186;->a(III)V

    .line 1600290
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1600291
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1600292
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 1600293
    const/16 v1, 0xf

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 1600294
    const/16 v1, 0x10

    invoke-virtual {v0, v1, v11}, LX/186;->b(II)V

    .line 1600295
    const/16 v1, 0x11

    invoke-virtual {v0, v1, v12}, LX/186;->b(II)V

    .line 1600296
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1600297
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1600298
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1600299
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1600300
    new-instance v0, LX/15i;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1600301
    new-instance v1, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;

    invoke-direct {v1, v0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;-><init>(LX/15i;)V

    .line 1600302
    return-object v1
.end method
