.class public LX/9HP;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""


# instance fields
.field public j:LX/9HC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final m:LX/9HB;

.field public final n:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1461235
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/9HP;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1461236
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1461237
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/9HP;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1461238
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1461239
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1461240
    const-class v0, LX/9HP;

    invoke-static {v0, p0}, LX/9HP;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1461241
    iget-object v0, p0, LX/9HP;->j:LX/9HC;

    invoke-virtual {v0, p0}, LX/9HC;->a(Landroid/view/View;)LX/9HB;

    move-result-object v0

    iput-object v0, p0, LX/9HP;->m:LX/9HB;

    .line 1461242
    iget-object v0, p0, LX/9HP;->m:LX/9HB;

    invoke-virtual {v0}, LX/9HB;->a()V

    .line 1461243
    const v0, 0x7f031534

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1461244
    invoke-virtual {p0}, LX/9HP;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b08bd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailPadding(I)V

    .line 1461245
    const v0, 0x7f0d09cd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/9HP;->n:Landroid/widget/TextView;

    .line 1461246
    iget-object v0, p0, LX/9HP;->k:LX/0ad;

    sget-short v1, LX/0wn;->aI:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1461247
    iget-object v1, p0, LX/9HP;->n:Landroid/widget/TextView;

    iget-object v0, p0, LX/9HP;->l:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f081240

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 1461248
    :cond_0
    :goto_1
    return-void

    .line 1461249
    :cond_1
    const v0, 0x7f08123f

    goto :goto_0

    .line 1461250
    :cond_2
    iget-object v0, p0, LX/9HP;->k:LX/0ad;

    sget-short v1, LX/0wn;->aH:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1461251
    iget-object v0, p0, LX/9HP;->n:Landroid/widget/TextView;

    const v1, 0x7f081241

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/9HP;

    const-class v1, LX/9HC;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/9HC;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v2

    check-cast v2, LX/0ad;

    invoke-static {p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object p0

    check-cast p0, Ljava/lang/Boolean;

    iput-object v1, p1, LX/9HP;->j:LX/9HC;

    iput-object v2, p1, LX/9HP;->k:LX/0ad;

    iput-object p0, p1, LX/9HP;->l:Ljava/lang/Boolean;

    return-void
.end method
