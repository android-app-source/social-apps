.class public final LX/AE5;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic c:LX/AEA;


# direct methods
.method public constructor <init>(LX/AEA;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 1646000
    iput-object p1, p0, LX/AE5;->c:LX/AEA;

    iput-object p2, p0, LX/AE5;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p3, p0, LX/AE5;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 1646001
    iget-object v0, p0, LX/AE5;->c:LX/AEA;

    iget-object v0, v0, LX/AEA;->b:LX/2hZ;

    iget-object v1, p0, LX/AE5;->c:LX/AEA;

    iget-object v2, p0, LX/AE5;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1646002
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1646003
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1646004
    invoke-static {v2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    .line 1646005
    new-instance v5, LX/AE7;

    invoke-direct {v5, v1, v3, v4}, LX/AE7;-><init>(LX/AEA;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    move-object v1, v5

    .line 1646006
    invoke-virtual {v0, p1, v1}, LX/2hZ;->a(Ljava/lang/Throwable;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1646007
    iget-object v0, p0, LX/AE5;->c:LX/AEA;

    iget-object v0, v0, LX/AEA;->l:LX/1Sl;

    invoke-virtual {v0}, LX/1Sl;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1646008
    iget-object v0, p0, LX/AE5;->c:LX/AEA;

    iget-object v1, v0, LX/AEA;->c:LX/0bH;

    new-instance v2, LX/1Ne;

    iget-object v0, p0, LX/AE5;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1646009
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 1646010
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-direct {v2, v0}, LX/1Ne;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 1646011
    :cond_0
    return-void
.end method

.method public final bridge synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1646012
    return-void
.end method
