.class public LX/AHZ;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Ljava/util/concurrent/ExecutorService;

.field public final c:LX/0tX;

.field public final d:Ljava/lang/String;

.field public final e:LX/0gX;

.field public final f:LX/0gK;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1655725
    const-class v0, LX/AHZ;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AHZ;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0gX;LX/0Or;Ljava/util/concurrent/ExecutorService;LX/0tX;LX/0gK;)V
    .locals 1
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0gX;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0tX;",
            "LX/0gK;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1655726
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1655727
    iput-object p3, p0, LX/AHZ;->b:Ljava/util/concurrent/ExecutorService;

    .line 1655728
    iput-object p4, p0, LX/AHZ;->c:LX/0tX;

    .line 1655729
    invoke-interface {p2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/AHZ;->d:Ljava/lang/String;

    .line 1655730
    iput-object p1, p0, LX/AHZ;->e:LX/0gX;

    .line 1655731
    iput-object p5, p0, LX/AHZ;->f:LX/0gK;

    .line 1655732
    return-void
.end method
