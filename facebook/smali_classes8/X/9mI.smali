.class public LX/9mI;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/9mW;

.field public b:Lcom/facebook/rapidreporting/ui/GuidedActionItem;

.field private c:Landroid/widget/RelativeLayout;

.field private d:Lcom/facebook/fbui/glyph/GlyphView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/ProgressBar;

.field public h:LX/9mD;

.field private final i:Landroid/view/View$OnClickListener;

.field private final j:Landroid/view/View$OnClickListener;

.field private final k:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1535265
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1535266
    new-instance v0, LX/9mE;

    invoke-direct {v0, p0}, LX/9mE;-><init>(LX/9mI;)V

    iput-object v0, p0, LX/9mI;->i:Landroid/view/View$OnClickListener;

    .line 1535267
    new-instance v0, LX/9mF;

    invoke-direct {v0, p0}, LX/9mF;-><init>(LX/9mI;)V

    iput-object v0, p0, LX/9mI;->j:Landroid/view/View$OnClickListener;

    .line 1535268
    new-instance v0, LX/9mG;

    invoke-direct {v0, p0}, LX/9mG;-><init>(LX/9mI;)V

    iput-object v0, p0, LX/9mI;->k:Landroid/view/View$OnClickListener;

    .line 1535269
    const v0, 0x7f0310eb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1535270
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, LX/9mI;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1535271
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/9mI;->setOrientation(I)V

    .line 1535272
    const v0, 0x7f0d2830

    invoke-virtual {p0, v0}, LX/9mI;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, LX/9mI;->c:Landroid/widget/RelativeLayout;

    .line 1535273
    const v0, 0x7f0d2831

    invoke-virtual {p0, v0}, LX/9mI;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/9mI;->d:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1535274
    const v0, 0x7f0d2832

    invoke-virtual {p0, v0}, LX/9mI;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/9mI;->e:Landroid/widget/TextView;

    .line 1535275
    const v0, 0x7f0d2833

    invoke-virtual {p0, v0}, LX/9mI;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/9mI;->f:Landroid/widget/TextView;

    .line 1535276
    const v0, 0x7f0d282f

    invoke-virtual {p0, v0}, LX/9mI;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, LX/9mI;->g:Landroid/widget/ProgressBar;

    .line 1535277
    return-void
.end method

.method public static a$redex0(LX/9mI;LX/9To;Z)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1535278
    iget-object v0, p0, LX/9mI;->b:Lcom/facebook/rapidreporting/ui/GuidedActionItem;

    .line 1535279
    iput-object p1, v0, Lcom/facebook/rapidreporting/ui/GuidedActionItem;->i:LX/9To;

    .line 1535280
    sget-object v0, LX/9mH;->a:[I

    invoke-virtual {p1}, LX/9To;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1535281
    :cond_0
    :goto_0
    return-void

    .line 1535282
    :pswitch_0
    iget-object v0, p0, LX/9mI;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 1535283
    invoke-direct {p0, v3}, LX/9mI;->setProgressBarVisibility(Z)V

    .line 1535284
    iget-object v0, p0, LX/9mI;->e:Landroid/widget/TextView;

    iget-object v1, p0, LX/9mI;->b:Lcom/facebook/rapidreporting/ui/GuidedActionItem;

    .line 1535285
    iget-object v2, v1, Lcom/facebook/rapidreporting/ui/GuidedActionItem;->b:Ljava/lang/String;

    move-object v1, v2

    .line 1535286
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1535287
    iget-object v0, p0, LX/9mI;->f:Landroid/widget/TextView;

    iget-object v1, p0, LX/9mI;->b:Lcom/facebook/rapidreporting/ui/GuidedActionItem;

    .line 1535288
    iget-object v2, v1, Lcom/facebook/rapidreporting/ui/GuidedActionItem;->c:Ljava/lang/String;

    move-object v1, v2

    .line 1535289
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1535290
    :pswitch_1
    iget-object v0, p0, LX/9mI;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 1535291
    invoke-direct {p0, v3}, LX/9mI;->setProgressBarVisibility(Z)V

    .line 1535292
    iget-object v0, p0, LX/9mI;->e:Landroid/widget/TextView;

    invoke-virtual {p0}, LX/9mI;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08242a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1535293
    iget-object v0, p0, LX/9mI;->f:Landroid/widget/TextView;

    iget-object v1, p0, LX/9mI;->b:Lcom/facebook/rapidreporting/ui/GuidedActionItem;

    .line 1535294
    iget-object v2, v1, Lcom/facebook/rapidreporting/ui/GuidedActionItem;->c:Ljava/lang/String;

    move-object v1, v2

    .line 1535295
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1535296
    iget-object v0, p0, LX/9mI;->h:LX/9mD;

    invoke-virtual {v0, v3}, LX/9mD;->setVisibility(I)V

    .line 1535297
    if-eqz p2, :cond_0

    .line 1535298
    iget-object v0, p0, LX/9mI;->a:LX/9mW;

    .line 1535299
    iget-object v1, v0, LX/9mP;->a:Landroid/widget/ScrollView;

    new-instance v2, Lcom/facebook/rapidreporting/ui/RapidReportingThankYouView$2;

    invoke-direct {v2, v0, p0}, Lcom/facebook/rapidreporting/ui/RapidReportingThankYouView$2;-><init>(LX/9mW;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/widget/ScrollView;->post(Ljava/lang/Runnable;)Z

    .line 1535300
    goto :goto_0

    .line 1535301
    :pswitch_2
    iget-object v0, p0, LX/9mI;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 1535302
    invoke-direct {p0, v2}, LX/9mI;->setProgressBarVisibility(Z)V

    .line 1535303
    iget-object v0, p0, LX/9mI;->a:LX/9mW;

    iget-object v1, p0, LX/9mI;->b:Lcom/facebook/rapidreporting/ui/GuidedActionItem;

    .line 1535304
    iget-object v2, v0, LX/9mW;->f:Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;

    .line 1535305
    iget-object v3, v2, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->m:LX/9lb;

    .line 1535306
    iget-object p1, v3, LX/9lb;->h:LX/0Or;

    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/8wj;

    .line 1535307
    iget-object p2, v1, Lcom/facebook/rapidreporting/ui/GuidedActionItem;->a:Ljava/lang/String;

    move-object p2, p2

    .line 1535308
    invoke-virtual {p1, p2}, LX/8wj;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p1

    .line 1535309
    iget-object p2, v3, LX/9lb;->b:LX/1Ck;

    sget-object v0, LX/9la;->EXECUTE_GUIDED_ACTION:LX/9la;

    new-instance v2, LX/9lW;

    invoke-direct {v2, v3, p0}, LX/9lW;-><init>(LX/9lb;LX/9mI;)V

    invoke-static {v2}, LX/0Vd;->of(LX/0TF;)LX/0Vd;

    move-result-object v2

    invoke-virtual {p2, v0, p1, v2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1535310
    goto/16 :goto_0

    .line 1535311
    :pswitch_3
    iget-object v0, p0, LX/9mI;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 1535312
    invoke-direct {p0, v3}, LX/9mI;->setProgressBarVisibility(Z)V

    .line 1535313
    iget-object v0, p0, LX/9mI;->d:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, LX/9mI;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00e7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1535314
    iget-object v0, p0, LX/9mI;->e:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1535315
    iget-object v0, p0, LX/9mI;->f:Landroid/widget/TextView;

    iget-object v1, p0, LX/9mI;->b:Lcom/facebook/rapidreporting/ui/GuidedActionItem;

    .line 1535316
    iget-object v2, v1, Lcom/facebook/rapidreporting/ui/GuidedActionItem;->e:Ljava/lang/String;

    move-object v1, v2

    .line 1535317
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1535318
    iget-object v0, p0, LX/9mI;->f:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private setProgressBarVisibility(Z)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 1535319
    if-eqz p1, :cond_0

    .line 1535320
    iget-object v0, p0, LX/9mI;->g:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {p0}, LX/9mI;->getMeasuredHeight()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1535321
    iget-object v0, p0, LX/9mI;->g:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1535322
    iget-object v0, p0, LX/9mI;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1535323
    :goto_0
    return-void

    .line 1535324
    :cond_0
    iget-object v0, p0, LX/9mI;->g:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1535325
    iget-object v0, p0, LX/9mI;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/9mW;Lcom/facebook/rapidreporting/ui/GuidedActionItem;)V
    .locals 2

    .prologue
    .line 1535326
    iput-object p1, p0, LX/9mI;->a:LX/9mW;

    .line 1535327
    iput-object p2, p0, LX/9mI;->b:Lcom/facebook/rapidreporting/ui/GuidedActionItem;

    .line 1535328
    iget-object v0, p0, LX/9mI;->c:Landroid/widget/RelativeLayout;

    iget-object v1, p0, LX/9mI;->i:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1535329
    iget-object v0, p0, LX/9mI;->d:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v1, p0, LX/9mI;->b:Lcom/facebook/rapidreporting/ui/GuidedActionItem;

    .line 1535330
    const/4 p1, 0x0

    iget-object p2, v1, Lcom/facebook/rapidreporting/ui/GuidedActionItem;->g:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-static {p1, p2}, LX/9Tp;->a(ZLcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;)I

    move-result p1

    move v1, p1

    .line 1535331
    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 1535332
    new-instance v0, LX/9mD;

    invoke-virtual {p0}, LX/9mI;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/9mD;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/9mI;->h:LX/9mD;

    .line 1535333
    iget-object v0, p0, LX/9mI;->h:LX/9mD;

    .line 1535334
    iget-object v1, v0, LX/9mD;->a:Lcom/facebook/fig/button/FigButton;

    move-object v0, v1

    .line 1535335
    iget-object v1, p0, LX/9mI;->b:Lcom/facebook/rapidreporting/ui/GuidedActionItem;

    .line 1535336
    sget-object p1, LX/9Tp;->e:LX/0P1;

    iget-object p2, v1, Lcom/facebook/rapidreporting/ui/GuidedActionItem;->g:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-virtual {p1, p2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    move v1, p1

    .line 1535337
    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setText(I)V

    .line 1535338
    iget-object v0, p0, LX/9mI;->h:LX/9mD;

    .line 1535339
    iget-object v1, v0, LX/9mD;->a:Lcom/facebook/fig/button/FigButton;

    move-object v0, v1

    .line 1535340
    iget-object v1, p0, LX/9mI;->j:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1535341
    iget-object v0, p0, LX/9mI;->h:LX/9mD;

    .line 1535342
    iget-object v1, v0, LX/9mD;->b:Lcom/facebook/fig/button/FigButton;

    move-object v0, v1

    .line 1535343
    iget-object v1, p0, LX/9mI;->k:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1535344
    iget-object v0, p0, LX/9mI;->h:LX/9mD;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, LX/9mD;->setVisibility(I)V

    .line 1535345
    iget-object v0, p0, LX/9mI;->h:LX/9mD;

    invoke-virtual {p0, v0}, LX/9mI;->addView(Landroid/view/View;)V

    .line 1535346
    iget-object v0, p0, LX/9mI;->b:Lcom/facebook/rapidreporting/ui/GuidedActionItem;

    .line 1535347
    iget-object v1, v0, Lcom/facebook/rapidreporting/ui/GuidedActionItem;->i:LX/9To;

    move-object v0, v1

    .line 1535348
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/9mI;->a$redex0(LX/9mI;LX/9To;Z)V

    .line 1535349
    return-void
.end method
