.class public LX/9jW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final e:Ljava/lang/Object;


# instance fields
.field public final b:Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;

.field public final c:LX/9jT;

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/9jo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1529915
    const-class v0, LX/9jW;

    sput-object v0, LX/9jW;->a:Ljava/lang/Class;

    .line 1529916
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/9jW;->e:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/9jT;Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1529917
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1529918
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/9jW;->d:Ljava/util/List;

    .line 1529919
    iput-object p1, p0, LX/9jW;->c:LX/9jT;

    .line 1529920
    iput-object p2, p0, LX/9jW;->b:Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;

    .line 1529921
    return-void
.end method

.method public static a(LX/0QB;)LX/9jW;
    .locals 8

    .prologue
    .line 1529922
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 1529923
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 1529924
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 1529925
    if-nez v1, :cond_0

    .line 1529926
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1529927
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 1529928
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 1529929
    sget-object v1, LX/9jW;->e:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1529930
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 1529931
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1529932
    :cond_1
    if-nez v1, :cond_4

    .line 1529933
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 1529934
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 1529935
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 1529936
    new-instance p0, LX/9jW;

    invoke-static {v0}, LX/9jT;->a(LX/0QB;)LX/9jT;

    move-result-object v1

    check-cast v1, LX/9jT;

    invoke-static {v0}, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;->b(LX/0QB;)Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;

    move-result-object v7

    check-cast v7, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;

    invoke-direct {p0, v1, v7}, LX/9jW;-><init>(LX/9jT;Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;)V

    .line 1529937
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1529938
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 1529939
    if-nez v1, :cond_2

    .line 1529940
    sget-object v0, LX/9jW;->e:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9jW;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1529941
    :goto_1
    if-eqz v0, :cond_3

    .line 1529942
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1529943
    :goto_3
    check-cast v0, LX/9jW;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1529944
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 1529945
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1529946
    :catchall_1
    move-exception v0

    .line 1529947
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1529948
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1529949
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 1529950
    :cond_2
    :try_start_8
    sget-object v0, LX/9jW;->e:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9jW;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static d(LX/9jW;LX/9jo;)V
    .locals 2

    .prologue
    .line 1529951
    iget-object v0, p0, LX/9jW;->b:Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;

    new-instance v1, LX/9jV;

    invoke-direct {v1, p0, p1}, LX/9jV;-><init>(LX/9jW;LX/9jo;)V

    invoke-virtual {v0, p1, v1}, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;->a(LX/9jo;LX/0TF;)V

    .line 1529952
    return-void
.end method

.method public static e(LX/9jW;LX/9jo;)V
    .locals 1

    .prologue
    .line 1529953
    iget-object v0, p0, LX/9jW;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1529954
    return-void
.end method


# virtual methods
.method public final a(LX/9jo;)V
    .locals 4

    .prologue
    .line 1529955
    const/4 v1, 0x0

    .line 1529956
    iget-object v0, p0, LX/9jW;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9jo;

    .line 1529957
    iget-boolean v3, v0, LX/9jo;->d:Z

    move v3, v3

    .line 1529958
    if-nez v3, :cond_4

    .line 1529959
    iget-boolean v3, p1, LX/9jo;->d:Z

    move v3, v3

    .line 1529960
    if-nez v3, :cond_4

    move v0, v1

    .line 1529961
    :goto_0
    move v0, v0

    .line 1529962
    if-eqz v0, :cond_1

    .line 1529963
    iget-object v0, p0, LX/9jW;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1529964
    iget-object v0, p1, LX/9jo;->b:Landroid/location/Location;

    move-object v0, v0

    .line 1529965
    if-eqz v0, :cond_2

    .line 1529966
    invoke-static {p0, p1}, LX/9jW;->d(LX/9jW;LX/9jo;)V

    .line 1529967
    :cond_1
    :goto_1
    return-void

    .line 1529968
    :cond_2
    iget-object v0, p0, LX/9jW;->c:LX/9jT;

    invoke-virtual {v0}, LX/9jT;->a()Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1529969
    iget-object v0, p0, LX/9jW;->c:LX/9jT;

    invoke-virtual {v0}, LX/9jT;->a()Landroid/location/Location;

    move-result-object v0

    .line 1529970
    iput-object v0, p1, LX/9jo;->b:Landroid/location/Location;

    .line 1529971
    move-object v0, p1

    .line 1529972
    invoke-static {p0, v0}, LX/9jW;->d(LX/9jW;LX/9jo;)V

    goto :goto_1

    .line 1529973
    :cond_3
    iget-object v0, p0, LX/9jW;->b:Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;

    new-instance v1, LX/9jU;

    invoke-direct {v1, p0, p1}, LX/9jU;-><init>(LX/9jW;LX/9jo;)V

    invoke-virtual {v0, v1}, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;->a(LX/0TF;)V

    .line 1529974
    goto :goto_1

    .line 1529975
    :cond_4
    iget-object v3, v0, LX/9jo;->b:Landroid/location/Location;

    move-object v3, v3

    .line 1529976
    if-eqz v3, :cond_0

    .line 1529977
    iget-object v3, p1, LX/9jo;->b:Landroid/location/Location;

    move-object v3, v3

    .line 1529978
    if-eqz v3, :cond_0

    .line 1529979
    iget-object v3, v0, LX/9jo;->b:Landroid/location/Location;

    move-object v0, v3

    .line 1529980
    iget-object v3, p1, LX/9jo;->b:Landroid/location/Location;

    move-object v3, v3

    .line 1529981
    invoke-virtual {v0, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 1529982
    goto :goto_0

    .line 1529983
    :cond_5
    const/4 v0, 0x1

    goto :goto_0
.end method
