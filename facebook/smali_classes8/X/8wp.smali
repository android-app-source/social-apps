.class public final LX/8wp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# instance fields
.field public final synthetic a:LX/8wv;


# direct methods
.method public constructor <init>(LX/8wv;)V
    .locals 0

    .prologue
    .line 1422983
    iput-object p1, p0, LX/8wp;->a:LX/8wv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    .prologue
    .line 1422984
    iget-object v0, p0, LX/8wp;->a:LX/8wv;

    .line 1422985
    iget-boolean p0, v0, LX/8wv;->t:Z

    if-eqz p0, :cond_1

    .line 1422986
    iget-object p0, v0, LX/8wv;->o:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    const/16 p1, 0x8

    invoke-virtual {p0, p1}, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->setVisibility(I)V

    .line 1422987
    :cond_0
    :goto_0
    return-void

    .line 1422988
    :cond_1
    const/4 p0, 0x1

    iput-boolean p0, v0, LX/8wv;->u:Z

    .line 1422989
    iget-object p0, v0, LX/8wv;->o:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    invoke-virtual {p0}, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->f()V

    .line 1422990
    iget-object p0, v0, LX/8wv;->o:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    const/4 p1, 0x0

    .line 1422991
    iput-boolean p1, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->h:Z

    .line 1422992
    invoke-virtual {v0}, LX/8wv;->k()V

    .line 1422993
    iget-boolean p0, v0, LX/8wv;->v:Z

    if-eqz p0, :cond_0

    .line 1422994
    invoke-virtual {v0}, LX/8wv;->h()V

    goto :goto_0
.end method

.method public final onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 1

    .prologue
    .line 1422995
    const/4 v0, 0x1

    return v0
.end method

.method public final onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 0

    .prologue
    .line 1422996
    return-void
.end method

.method public final onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0

    .prologue
    .line 1422997
    return-void
.end method
