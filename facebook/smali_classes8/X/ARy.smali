.class public final LX/ARy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/ARn;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/ARn;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method private constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 1673526
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1673527
    iput-object p1, p0, LX/ARy;->a:LX/0QB;

    .line 1673528
    return-void
.end method

.method public static a(LX/0QB;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")",
            "Ljava/util/Set",
            "<",
            "LX/ARn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1673529
    new-instance v0, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    new-instance v2, LX/ARy;

    invoke-direct {v2, p0}, LX/ARy;-><init>(LX/0QB;)V

    invoke-direct {v0, v1, v2}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1673530
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/ARy;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1673531
    packed-switch p2, :pswitch_data_0

    .line 1673532
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1673533
    :pswitch_0
    new-instance v0, LX/Hrf;

    invoke-direct {v0}, LX/Hrf;-><init>()V

    .line 1673534
    move-object v0, v0

    .line 1673535
    move-object v0, v0

    .line 1673536
    :goto_0
    return-object v0

    .line 1673537
    :pswitch_1
    new-instance v0, LX/BEP;

    invoke-direct {v0}, LX/BEP;-><init>()V

    .line 1673538
    move-object v0, v0

    .line 1673539
    move-object v0, v0

    .line 1673540
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1673541
    const/4 v0, 0x2

    return v0
.end method
