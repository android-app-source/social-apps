.class public final LX/9bU;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/photos/data/protocol/FetchBestAvailableImageUriQueryModels$FetchBestAvailableImageUriQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;

.field public final synthetic c:Landroid/app/Activity;

.field public final synthetic d:Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;Ljava/lang/String;Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 1514960
    iput-object p1, p0, LX/9bU;->d:Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    iput-object p2, p0, LX/9bU;->a:Ljava/lang/String;

    iput-object p3, p0, LX/9bU;->b:Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;

    iput-object p4, p0, LX/9bU;->c:Landroid/app/Activity;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1514961
    iget-object v0, p0, LX/9bU;->d:Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    iget-object v0, v0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->l:LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "failed to fetch available image from server on fallback query"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1514962
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1514963
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/16 v5, 0xb4

    .line 1514964
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1514965
    check-cast v0, Lcom/facebook/photos/data/protocol/FetchBestAvailableImageUriQueryModels$FetchBestAvailableImageUriQueryModel;

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/FetchBestAvailableImageUriQueryModels$FetchBestAvailableImageUriQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 1514966
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1514967
    check-cast v0, Lcom/facebook/photos/data/protocol/FetchBestAvailableImageUriQueryModels$FetchBestAvailableImageUriQueryModel;

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/FetchBestAvailableImageUriQueryModels$FetchBestAvailableImageUriQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, LX/15i;->j(II)I

    move-result v2

    .line 1514968
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1514969
    check-cast v0, Lcom/facebook/photos/data/protocol/FetchBestAvailableImageUriQueryModels$FetchBestAvailableImageUriQueryModel;

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/FetchBestAvailableImageUriQueryModels$FetchBestAvailableImageUriQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v4, 0x2

    invoke-virtual {v3, v0, v4}, LX/15i;->j(II)I

    move-result v0

    .line 1514970
    if-lt v2, v5, :cond_0

    if-ge v0, v5, :cond_1

    .line 1514971
    :cond_0
    iget-object v0, p0, LX/9bU;->d:Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    iget-object v0, v0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->m:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f082428

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1514972
    :goto_0
    return-void

    .line 1514973
    :cond_1
    iget-object v0, p0, LX/9bU;->d:Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    iget-object v0, v0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->l:LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "failed to fetch available image from server on first query"

    invoke-virtual {v0, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1514974
    iget-object v0, p0, LX/9bU;->d:Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    iget-object v2, p0, LX/9bU;->a:Ljava/lang/String;

    iget-object v3, p0, LX/9bU;->b:Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;

    iget-object v4, p0, LX/9bU;->c:Landroid/app/Activity;

    invoke-static {v0, v2, v1, v3, v4}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a$redex0(Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;Landroid/app/Activity;)V

    goto :goto_0
.end method
