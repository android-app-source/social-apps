.class public LX/8j9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/8j9;


# instance fields
.field public final a:LX/0Zb;

.field private final b:LX/0SG;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1392221
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1392222
    iput-object p1, p0, LX/8j9;->a:LX/0Zb;

    .line 1392223
    iput-object p2, p0, LX/8j9;->b:LX/0SG;

    .line 1392224
    return-void
.end method

.method public static a(LX/0QB;)LX/8j9;
    .locals 5

    .prologue
    .line 1392208
    sget-object v0, LX/8j9;->c:LX/8j9;

    if-nez v0, :cond_1

    .line 1392209
    const-class v1, LX/8j9;

    monitor-enter v1

    .line 1392210
    :try_start_0
    sget-object v0, LX/8j9;->c:LX/8j9;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1392211
    if-eqz v2, :cond_0

    .line 1392212
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1392213
    new-instance p0, LX/8j9;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-direct {p0, v3, v4}, LX/8j9;-><init>(LX/0Zb;LX/0SG;)V

    .line 1392214
    move-object v0, p0

    .line 1392215
    sput-object v0, LX/8j9;->c:LX/8j9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1392216
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1392217
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1392218
    :cond_1
    sget-object v0, LX/8j9;->c:LX/8j9;

    return-object v0

    .line 1392219
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1392220
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static e(LX/8j9;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 4

    .prologue
    .line 1392204
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "sticker_search"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1392205
    const-string v1, "event_type"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1392206
    const-string v1, "timestamp"

    iget-object v2, p0, LX/8j9;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1392207
    return-object v0
.end method


# virtual methods
.method public final c(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1392199
    const-string v0, "search"

    invoke-static {p0, v0}, LX/8j9;->e(LX/8j9;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1392200
    const-string v1, "search_query"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1392201
    const-string v1, "operation_status"

    sget-object v2, LX/8j8;->CANCELLED:LX/8j8;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1392202
    iget-object v1, p0, LX/8j9;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1392203
    return-void
.end method
