.class public LX/A8f;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9Ae;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Item:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/9Ae",
        "<TItem;>;"
    }
.end annotation


# instance fields
.field public final a:LX/62M;

.field private final b:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Ljava/lang/Object;",
            "TItem;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:I

.field public d:I


# direct methods
.method public constructor <init>(LX/62M;LX/0QK;)V
    .locals 1
    .param p2    # LX/0QK;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/widget/listview/WorkingRangeScrollable;",
            "LX/0QK",
            "<",
            "Ljava/lang/Object;",
            "TItem;>;)V"
        }
    .end annotation

    .prologue
    const/high16 v0, -0x80000000

    .line 1627947
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1627948
    iput v0, p0, LX/A8f;->c:I

    .line 1627949
    iput v0, p0, LX/A8f;->d:I

    .line 1627950
    iput-object p1, p0, LX/A8f;->a:LX/62M;

    .line 1627951
    iput-object p2, p0, LX/A8f;->b:LX/0QK;

    .line 1627952
    return-void
.end method

.method public static e(LX/A8f;)V
    .locals 3

    .prologue
    .line 1627953
    iget-object v0, p0, LX/A8f;->a:LX/62M;

    .line 1627954
    iget-object v1, v0, LX/62M;->a:LX/0g8;

    invoke-interface {v1}, LX/0g8;->B()Z

    move-result v1

    move v0, v1

    .line 1627955
    if-eqz v0, :cond_0

    iget v0, p0, LX/A8f;->c:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1627956
    if-eqz v0, :cond_2

    .line 1627957
    const/high16 v0, -0x80000000

    .line 1627958
    iput v0, p0, LX/A8f;->c:I

    .line 1627959
    iput v0, p0, LX/A8f;->d:I

    .line 1627960
    :cond_1
    return-void

    .line 1627961
    :cond_2
    iget v0, p0, LX/A8f;->c:I

    iget v1, p0, LX/A8f;->d:I

    if-gt v0, v1, :cond_4

    iget v0, p0, LX/A8f;->c:I

    if-ltz v0, :cond_4

    iget v0, p0, LX/A8f;->d:I

    if-ltz v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 1627962
    if-nez v0, :cond_1

    .line 1627963
    const-string v0, "Provided visibility indices [%d, %d] are invalid"

    iget v1, p0, LX/A8f;->c:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v2, p0, LX/A8f;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1627964
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1627965
    iget v0, p0, LX/A8f;->c:I

    return v0
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TItem;"
        }
    .end annotation

    .prologue
    .line 1627966
    iget-object v0, p0, LX/A8f;->a:LX/62M;

    .line 1627967
    iget-object v1, v0, LX/62M;->a:LX/0g8;

    invoke-interface {v1, p1}, LX/0g8;->f(I)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    .line 1627968
    iget-object v1, p0, LX/A8f;->b:LX/0QK;

    if-nez v1, :cond_0

    .line 1627969
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/A8f;->b:LX/0QK;

    invoke-interface {v1, v0}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1627970
    iget v0, p0, LX/A8f;->d:I

    return v0
.end method
