.class public final LX/8lP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8l0;


# instance fields
.field public final synthetic a:Lcom/facebook/stickers/search/StickerSearchContainer;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/search/StickerSearchContainer;)V
    .locals 0

    .prologue
    .line 1397753
    iput-object p1, p0, LX/8lP;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/stickers/model/Sticker;)V
    .locals 5

    .prologue
    .line 1397754
    iget-object v0, p0, LX/8lP;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-object v0, v0, Lcom/facebook/stickers/search/StickerSearchContainer;->K:LX/8l6;

    if-eqz v0, :cond_0

    .line 1397755
    iget-object v0, p0, LX/8lP;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-object v0, v0, Lcom/facebook/stickers/search/StickerSearchContainer;->h:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, LX/8lP;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    invoke-virtual {v1}, Lcom/facebook/stickers/search/StickerSearchContainer;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1397756
    iget-object v0, p0, LX/8lP;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-object v0, v0, Lcom/facebook/stickers/search/StickerSearchContainer;->K:LX/8l6;

    invoke-interface {v0, p1}, LX/8l6;->a(Lcom/facebook/stickers/model/Sticker;)V

    .line 1397757
    iget-object v0, p0, LX/8lP;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-object v0, v0, Lcom/facebook/stickers/search/StickerSearchContainer;->t:LX/8j9;

    iget-object v1, p1, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    iget-object v2, p0, LX/8lP;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-object v2, v2, Lcom/facebook/stickers/search/StickerSearchContainer;->v:LX/0Rf;

    iget-object v3, p1, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v2

    iget-object v3, p0, LX/8lP;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-object v3, v3, Lcom/facebook/stickers/search/StickerSearchContainer;->L:LX/8lg;

    sget-object v4, LX/8lg;->TAG_RESULTS_SHOWN:LX/8lg;

    invoke-virtual {v3, v4}, LX/8lg;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 1397758
    const-string v4, "sticker_sent"

    invoke-static {v0, v4}, LX/8j9;->e(LX/8j9;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 1397759
    const-string p0, "sticker_id"

    invoke-virtual {v4, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1397760
    const-string p0, "is_in_tray"

    invoke-virtual {v4, p0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1397761
    const-string p0, "is_from_featured_tag"

    invoke-virtual {v4, p0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1397762
    iget-object p0, v0, LX/8j9;->a:LX/0Zb;

    invoke-interface {p0, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1397763
    :cond_0
    return-void
.end method
