.class public LX/9jT;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile d:LX/9jT;


# instance fields
.field public b:Landroid/location/Location;

.field private c:LX/0SG;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1529871
    const-class v0, LX/9jT;

    sput-object v0, LX/9jT;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1529872
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1529873
    iput-object p1, p0, LX/9jT;->c:LX/0SG;

    .line 1529874
    return-void
.end method

.method public static a(LX/0QB;)LX/9jT;
    .locals 4

    .prologue
    .line 1529875
    sget-object v0, LX/9jT;->d:LX/9jT;

    if-nez v0, :cond_1

    .line 1529876
    const-class v1, LX/9jT;

    monitor-enter v1

    .line 1529877
    :try_start_0
    sget-object v0, LX/9jT;->d:LX/9jT;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1529878
    if-eqz v2, :cond_0

    .line 1529879
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1529880
    new-instance p0, LX/9jT;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-direct {p0, v3}, LX/9jT;-><init>(LX/0SG;)V

    .line 1529881
    move-object v0, p0

    .line 1529882
    sput-object v0, LX/9jT;->d:LX/9jT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1529883
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1529884
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1529885
    :cond_1
    sget-object v0, LX/9jT;->d:LX/9jT;

    return-object v0

    .line 1529886
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1529887
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Landroid/location/Location;
    .locals 4

    .prologue
    .line 1529888
    iget-object v0, p0, LX/9jT;->b:Landroid/location/Location;

    if-eqz v0, :cond_0

    .line 1529889
    iget-object v0, p0, LX/9jT;->b:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v0

    const-wide/32 v2, 0x927c0

    add-long/2addr v0, v2

    iget-object v2, p0, LX/9jT;->c:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 1529890
    iget-object v0, p0, LX/9jT;->b:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    iget-object v0, p0, LX/9jT;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    .line 1529891
    const/4 v0, 0x0

    iput-object v0, p0, LX/9jT;->b:Landroid/location/Location;

    .line 1529892
    :cond_0
    iget-object v0, p0, LX/9jT;->b:Landroid/location/Location;

    return-object v0
.end method
