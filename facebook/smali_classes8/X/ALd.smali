.class public final LX/ALd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/ALe;


# direct methods
.method public constructor <init>(LX/ALe;)V
    .locals 0

    .prologue
    .line 1665173
    iput-object p1, p0, LX/ALd;->a:LX/ALe;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1665175
    iget-object v0, p0, LX/ALd;->a:LX/ALe;

    iget-object v0, v0, LX/ALe;->a:Lcom/facebook/backstage/camera/PreviewView;

    iget-object v0, v0, Lcom/facebook/backstage/camera/PreviewView;->u:LX/ALj;

    if-eqz v0, :cond_0

    .line 1665176
    :cond_0
    iget-object v0, p0, LX/ALd;->a:LX/ALe;

    iget-object v0, v0, LX/ALe;->a:Lcom/facebook/backstage/camera/PreviewView;

    invoke-virtual {v0}, Lcom/facebook/backstage/camera/PreviewView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f082873

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1665177
    iget-object v0, p0, LX/ALd;->a:LX/ALe;

    iget-object v0, v0, LX/ALe;->a:Lcom/facebook/backstage/camera/PreviewView;

    iget-object v0, v0, Lcom/facebook/backstage/camera/PreviewView;->q:Lcom/facebook/fbui/glyph/GlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1665178
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1665179
    iget-object v0, p0, LX/ALd;->a:LX/ALe;

    iget-object v0, v0, LX/ALe;->a:Lcom/facebook/backstage/camera/PreviewView;

    iget-object v0, v0, Lcom/facebook/backstage/camera/PreviewView;->q:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setEnabled(Z)V

    .line 1665180
    iget-object v0, p0, LX/ALd;->a:LX/ALe;

    iget-object v0, v0, LX/ALe;->a:Lcom/facebook/backstage/camera/PreviewView;

    invoke-virtual {v0}, Lcom/facebook/backstage/camera/PreviewView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f082872

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1665181
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1665174
    invoke-direct {p0}, LX/ALd;->a()V

    return-void
.end method
