.class public LX/9hl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3d4;


# instance fields
.field private a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 1526981
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1526982
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1526983
    iput-object p1, p0, LX/9hl;->a:Ljava/lang/String;

    .line 1526984
    return-void
.end method


# virtual methods
.method public a(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLMedia;
    .locals 0

    .prologue
    .line 1526985
    return-object p1
.end method

.method public a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;)Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;
    .locals 0

    .prologue
    .line 1526986
    return-object p1
.end method

.method public a(Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;)Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;
    .locals 0

    .prologue
    .line 1526987
    return-object p1
.end method

.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1526988
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLMedia;

    if-eqz v0, :cond_1

    .line 1526989
    iget-object v1, p0, LX/9hl;->a:Ljava/lang/String;

    move-object v0, p1

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1526990
    check-cast p1, Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {p0, p1}, LX/9hl;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object p1

    .line 1526991
    :cond_0
    :goto_0
    return-object p1

    .line 1526992
    :cond_1
    instance-of v0, p1, Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;

    if-eqz v0, :cond_2

    .line 1526993
    iget-object v1, p0, LX/9hl;->a:Ljava/lang/String;

    move-object v0, p1

    check-cast v0, Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;

    invoke-virtual {v0}, Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1526994
    check-cast p1, Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;

    invoke-virtual {p0, p1}, LX/9hl;->a(Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;)Lcom/facebook/photos/data/sizeawaremedia/PhotosDefaultsGraphQLModels$SizeAwareMediaModel;

    move-result-object p1

    goto :goto_0

    .line 1526995
    :cond_2
    instance-of v0, p1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;

    if-eqz v0, :cond_0

    .line 1526996
    iget-object v1, p0, LX/9hl;->a:Ljava/lang/String;

    move-object v0, p1

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1526997
    check-cast p1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;

    invoke-virtual {p0, p1}, LX/9hl;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;)Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;

    move-result-object p1

    goto :goto_0
.end method
