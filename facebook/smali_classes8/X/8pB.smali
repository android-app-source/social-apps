.class public final LX/8pB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel;",
        ">;",
        "Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1E3;


# direct methods
.method public constructor <init>(LX/1E3;)V
    .locals 0

    .prologue
    .line 1405536
    iput-object p1, p0, LX/8pB;->a:LX/1E3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1405537
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1405538
    if-eqz p1, :cond_0

    .line 1405539
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1405540
    if-eqz v0, :cond_0

    .line 1405541
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1405542
    check-cast v0, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel;->a()Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1405543
    :cond_0
    const/4 v0, 0x0

    .line 1405544
    :goto_0
    return-object v0

    .line 1405545
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1405546
    check-cast v0, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel;->a()Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;

    move-result-object v0

    .line 1405547
    iget-object v1, p0, LX/8pB;->a:LX/1E3;

    .line 1405548
    invoke-virtual {v0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v2

    .line 1405549
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1405550
    invoke-virtual {v0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 1405551
    new-instance v4, Lcom/facebook/user/model/PicSquareUrlWithSize;

    invoke-virtual {v0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->c()I

    move-result p0

    invoke-virtual {v0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v4, p0, p1}, Lcom/facebook/user/model/PicSquareUrlWithSize;-><init>(ILjava/lang/String;)V

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1405552
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 1405553
    new-instance v4, Lcom/facebook/user/model/PicSquareUrlWithSize;

    invoke-virtual {v0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->c()I

    move-result p0

    invoke-virtual {v0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v4, p0, p1}, Lcom/facebook/user/model/PicSquareUrlWithSize;-><init>(ILjava/lang/String;)V

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1405554
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 1405555
    new-instance v4, Lcom/facebook/user/model/PicSquareUrlWithSize;

    invoke-virtual {v0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->c()I

    move-result p0

    invoke-virtual {v0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v4, p0, p1}, Lcom/facebook/user/model/PicSquareUrlWithSize;-><init>(ILjava/lang/String;)V

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1405556
    :cond_4
    new-instance v4, Lcom/facebook/user/model/PicSquare;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-direct {v4, v3}, Lcom/facebook/user/model/PicSquare;-><init>(LX/0Px;)V

    move-object v3, v4

    .line 1405557
    iget-object v4, v1, LX/1E3;->b:LX/0WJ;

    invoke-virtual {v4}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v4

    .line 1405558
    if-eqz v4, :cond_6

    .line 1405559
    new-instance p0, LX/0XI;

    invoke-direct {p0}, LX/0XI;-><init>()V

    .line 1405560
    invoke-virtual {p0, v4}, LX/0XI;->a(Lcom/facebook/user/model/User;)LX/0XI;

    .line 1405561
    if-eqz v2, :cond_5

    .line 1405562
    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;->b()Ljava/lang/String;

    move-result-object v2

    .line 1405563
    iput-object v2, p0, LX/0XI;->n:Ljava/lang/String;

    .line 1405564
    :cond_5
    iput-object v3, p0, LX/0XI;->p:Lcom/facebook/user/model/PicSquare;

    .line 1405565
    iget-object v2, v1, LX/1E3;->b:LX/0WJ;

    invoke-virtual {p0}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0WJ;->c(Lcom/facebook/user/model/User;)V

    .line 1405566
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/timeline/profilemedia/sync/protocol/FetchProfilePicGraphQLModels$FetchProfilePicGraphQLModel$ActorModel;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    goto/16 :goto_0
.end method
