.class public LX/A6z;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/A78;

.field private b:LX/0lB;


# direct methods
.method public constructor <init>(LX/A78;LX/0lB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1625316
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1625317
    iput-object p1, p0, LX/A6z;->a:LX/A78;

    .line 1625318
    iput-object p2, p0, LX/A6z;->b:LX/0lB;

    .line 1625319
    return-void
.end method

.method public static a(LX/A6z;Ljava/lang/String;)LX/A6x;
    .locals 4

    .prologue
    .line 1625320
    const/4 v1, 0x0

    .line 1625321
    :try_start_0
    iget-object v0, p0, LX/A6z;->b:LX/0lB;

    invoke-virtual {v0}, LX/0lD;->b()LX/0lp;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0lp;->a(Ljava/lang/String;)LX/15w;

    move-result-object v0

    .line 1625322
    const-class v2, Lcom/facebook/transliteration/algorithms/unigram/UnigramLanguageModel;

    invoke-virtual {v0, v2}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/transliteration/algorithms/unigram/UnigramLanguageModel;
    :try_end_0
    .catch LX/2aQ; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1625323
    :goto_0
    return-object v0

    .line 1625324
    :catch_0
    move-exception v0

    .line 1625325
    const-string v2, "TRANSLITERATION"

    const-string v3, "Could not parse data"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 1625326
    goto :goto_0

    .line 1625327
    :catch_1
    move-exception v0

    .line 1625328
    const-string v2, "TRANSLITERATION"

    const-string v3, "Could not convert class"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    goto :goto_0
.end method
