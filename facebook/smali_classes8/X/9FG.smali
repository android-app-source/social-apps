.class public LX/9FG;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0if;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public final b:J

.field private c:Z


# direct methods
.method public constructor <init>(JLX/0Or;)V
    .locals 9
    .param p1    # J
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/common/random/InsecureRandom;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/0Or",
            "<",
            "Ljava/util/Random;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-wide/16 v6, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1458376
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1458377
    const-wide/16 v4, 0x0

    cmp-long v0, p1, v4

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "0 is an invalid instance id. Explicitly use NEW_COMMENTS_FUNNEL_LOGGER_INSTANCE."

    invoke-static {v0, v3}, LX/0Tp;->b(ZLjava/lang/String;)V

    .line 1458378
    cmp-long v0, p1, v6

    if-nez v0, :cond_2

    :goto_1
    iput-boolean v1, p0, LX/9FG;->c:Z

    .line 1458379
    cmp-long v0, p1, v6

    if-nez v0, :cond_0

    invoke-static {p3}, LX/9FG;->a(LX/0Or;)J

    move-result-wide p1

    :cond_0
    iput-wide p1, p0, LX/9FG;->b:J

    .line 1458380
    return-void

    :cond_1
    move v0, v2

    .line 1458381
    goto :goto_0

    :cond_2
    move v1, v2

    .line 1458382
    goto :goto_1
.end method

.method private static a(LX/0Or;)J
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/util/Random;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 1458369
    invoke-interface {p0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Random;

    .line 1458370
    :cond_0
    invoke-virtual {v0}, Ljava/util/Random;->nextLong()J

    move-result-wide v2

    .line 1458371
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 1458372
    return-wide v2
.end method


# virtual methods
.method public final a(Z)V
    .locals 6

    .prologue
    .line 1458373
    iget-object v0, p0, LX/9FG;->a:LX/0if;

    sget-object v1, LX/0ig;->j:LX/0ih;

    iget-wide v2, p0, LX/9FG;->b:J

    const-string v4, "close_reply_banner"

    if-eqz p1, :cond_0

    const-string v5, "alert_shown"

    :goto_0
    invoke-virtual/range {v0 .. v5}, LX/0if;->a(LX/0ih;JLjava/lang/String;Ljava/lang/String;)V

    .line 1458374
    return-void

    .line 1458375
    :cond_0
    const-string v5, "alert_not_shown"

    goto :goto_0
.end method

.method public final a(LX/9FF;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1458360
    iget-boolean v1, p0, LX/9FG;->c:Z

    if-nez v1, :cond_0

    .line 1458361
    :goto_0
    return v0

    .line 1458362
    :cond_0
    iput-boolean v0, p0, LX/9FG;->c:Z

    .line 1458363
    iget-object v0, p0, LX/9FG;->a:LX/0if;

    sget-object v1, LX/0ig;->j:LX/0ih;

    iget-wide v2, p0, LX/9FG;->b:J

    invoke-virtual {v0, v1, v2, v3}, LX/0if;->a(LX/0ih;J)V

    .line 1458364
    sget-object v0, LX/9FE;->a:[I

    invoke-virtual {p1}, LX/9FF;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1458365
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized enum value"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1458366
    :pswitch_0
    iget-object v0, p0, LX/9FG;->a:LX/0if;

    sget-object v1, LX/0ig;->j:LX/0ih;

    iget-wide v2, p0, LX/9FG;->b:J

    const-string v4, "started_in_top_level_mode"

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0if;->a(LX/0ih;JLjava/lang/String;)V

    .line 1458367
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 1458368
    :pswitch_1
    iget-object v0, p0, LX/9FG;->a:LX/0if;

    sget-object v1, LX/0ig;->j:LX/0ih;

    iget-wide v2, p0, LX/9FG;->b:J

    const-string v4, "started_in_reply_mode"

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0if;->a(LX/0ih;JLjava/lang/String;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b(LX/9FF;)V
    .locals 6

    .prologue
    .line 1458358
    iget-object v0, p0, LX/9FG;->a:LX/0if;

    sget-object v1, LX/0ig;->j:LX/0ih;

    iget-wide v2, p0, LX/9FG;->b:J

    const-string v4, "media_item_set"

    invoke-virtual {p1}, LX/9FF;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/0if;->a(LX/0ih;JLjava/lang/String;Ljava/lang/String;)V

    .line 1458359
    return-void
.end method

.method public final h()V
    .locals 5

    .prologue
    .line 1458356
    iget-object v0, p0, LX/9FG;->a:LX/0if;

    sget-object v1, LX/0ig;->j:LX/0ih;

    iget-wide v2, p0, LX/9FG;->b:J

    const-string v4, "reply_text_clicked"

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0if;->b(LX/0ih;JLjava/lang/String;)V

    .line 1458357
    return-void
.end method
