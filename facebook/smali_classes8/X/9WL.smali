.class public LX/9WL;
.super Landroid/widget/ArrayAdapter;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "LX/9WM;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0

    .prologue
    .line 1501053
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 1501054
    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1501055
    new-instance v1, LX/9WO;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, LX/9WO;-><init>(Landroid/content/Context;)V

    .line 1501056
    invoke-virtual {p0, p1}, LX/9WL;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9WM;

    .line 1501057
    iget-object p0, v1, LX/9WO;->b:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1501058
    invoke-static {v0}, LX/9WM;->h(LX/9WM;)Z

    move-result p1

    iget-object p2, v0, LX/9WM;->a:Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;

    invoke-virtual {p2}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->gl_()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object p2

    invoke-static {p1, p2}, LX/9Tp;->a(ZLcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;)I

    move-result p1

    move p1, p1

    .line 1501059
    invoke-virtual {p0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 1501060
    iget-object p0, v0, LX/9WM;->c:LX/9To;

    move-object p0, p0

    .line 1501061
    iput-object p0, v1, LX/9WO;->a:LX/9To;

    .line 1501062
    sget-object p0, LX/9Tp;->e:LX/0P1;

    invoke-virtual {v0}, LX/9WM;->c()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result p0

    move p0, p0

    .line 1501063
    iput-boolean p0, v1, LX/9WO;->f:Z

    .line 1501064
    iget-object p0, v1, LX/9WO;->a:LX/9To;

    sget-object p1, LX/9To;->INITIATED:LX/9To;

    if-ne p0, p1, :cond_2

    .line 1501065
    const/4 p0, 0x1

    invoke-static {v1, p0}, LX/9WO;->setProgressBarVisibility(LX/9WO;Z)V

    .line 1501066
    :goto_0
    iget-object p0, v1, LX/9WO;->a:LX/9To;

    sget-object p1, LX/9To;->ASK_TO_CONFIRM:LX/9To;

    if-eq p0, p1, :cond_0

    iget-object p0, v1, LX/9WO;->a:LX/9To;

    sget-object p1, LX/9To;->INITIATED:LX/9To;

    if-ne p0, p1, :cond_3

    iget-boolean p0, v1, LX/9WO;->f:Z

    if-eqz p0, :cond_3

    .line 1501067
    :cond_0
    iget-object p0, v1, LX/9WO;->c:Lcom/facebook/fbui/widget/contentview/ContentView;

    const p1, 0x7f08242a

    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(I)V

    .line 1501068
    iget-object p0, v1, LX/9WO;->c:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v0}, LX/9WM;->b()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 1501069
    iget-object p0, v1, LX/9WO;->e:LX/0zw;

    invoke-virtual {p0}, LX/0zw;->a()Landroid/view/View;

    move-result-object p0

    check-cast p0, Lcom/facebook/resources/ui/FbTextView;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1501070
    iget-object p0, v1, LX/9WO;->e:LX/0zw;

    invoke-virtual {p0}, LX/0zw;->a()Landroid/view/View;

    move-result-object p0

    check-cast p0, Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1}, LX/9WO;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget-object p1, LX/9Tp;->e:LX/0P1;

    invoke-virtual {v0}, LX/9WM;->c()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object p3

    invoke-virtual {p1, p3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {p2, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1501071
    iget-object p0, v1, LX/9WO;->e:LX/0zw;

    invoke-virtual {p0}, LX/0zw;->a()Landroid/view/View;

    move-result-object p0

    check-cast p0, Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1}, LX/9WO;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f0a00d2

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1501072
    iget-object p0, v1, LX/9WO;->c:Lcom/facebook/fbui/widget/contentview/ContentView;

    const p1, 0x7f0e09a6

    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleTextAppearance(I)V

    .line 1501073
    iget-object p0, v1, LX/9WO;->c:Lcom/facebook/fbui/widget/contentview/ContentView;

    const p1, 0x7f0e09a7

    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleTextAppearance(I)V

    .line 1501074
    iget-object p0, v1, LX/9WO;->b:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v1}, LX/9WO;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f0a00d2

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1501075
    :goto_1
    iget-object p0, v1, LX/9WO;->c:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v1}, LX/9WO;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f0b1943

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p1

    float-to-int p1, p1

    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailSize(I)V

    .line 1501076
    iget-object p0, v1, LX/9WO;->a:LX/9To;

    sget-object p1, LX/9To;->INITIATED:LX/9To;

    if-eq p0, p1, :cond_1

    .line 1501077
    invoke-virtual {v1}, LX/9WO;->refreshDrawableState()V

    .line 1501078
    :cond_1
    return-object v1

    .line 1501079
    :cond_2
    const/4 p0, 0x0

    invoke-static {v1, p0}, LX/9WO;->setProgressBarVisibility(LX/9WO;Z)V

    goto/16 :goto_0

    .line 1501080
    :cond_3
    iget-object p0, v1, LX/9WO;->c:Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 1501081
    invoke-static {v0}, LX/9WM;->h(LX/9WM;)Z

    move-result p1

    if-eqz p1, :cond_5

    .line 1501082
    iget-object p1, v0, LX/9WM;->a:Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;

    invoke-virtual {p1}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->d()Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel$CompletedTitleModel;

    move-result-object p1

    if-eqz p1, :cond_4

    iget-object p1, v0, LX/9WM;->a:Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;

    invoke-virtual {p1}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->d()Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel$CompletedTitleModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel$CompletedTitleModel;->a()Ljava/lang/String;

    move-result-object p1

    .line 1501083
    :goto_2
    move-object p1, p1

    .line 1501084
    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 1501085
    iget-object p0, v1, LX/9WO;->c:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v0}, LX/9WM;->b()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1501086
    :cond_4
    const-string p1, ""

    goto :goto_2

    .line 1501087
    :cond_5
    iget-object p1, v0, LX/9WM;->a:Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;

    invoke-virtual {p1}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->m()Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel$TitleModel;

    move-result-object p1

    if-eqz p1, :cond_6

    iget-object p1, v0, LX/9WM;->a:Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;

    invoke-virtual {p1}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->m()Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel$TitleModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel$TitleModel;->a()Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    :cond_6
    const-string p1, ""

    goto :goto_2
.end method
