.class public LX/91k;
.super LX/5Jf;
.source ""


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLInterfaces$TaggableObjectEdge;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLInterfaces$TaggableObjectEdge;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/91o;

.field private final d:LX/90B;

.field private final e:LX/90D;

.field public f:I

.field public g:Z

.field public h:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(LX/62T;LX/90B;LX/90D;LX/91o;Landroid/content/Context;)V
    .locals 1
    .param p1    # LX/62T;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/90B;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/90D;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1431319
    invoke-direct {p0, p5, p1}, LX/5Jf;-><init>(Landroid/content/Context;LX/3wu;)V

    .line 1431320
    new-instance v0, LX/91i;

    invoke-direct {v0, p0}, LX/91i;-><init>(LX/91k;)V

    .line 1431321
    iput-object v0, p1, LX/3wu;->h:LX/3wr;

    .line 1431322
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1431323
    iput-object v0, p0, LX/91k;->a:LX/0Px;

    .line 1431324
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/91k;->b:Ljava/util/Map;

    .line 1431325
    iput-object p4, p0, LX/91k;->c:LX/91o;

    .line 1431326
    iput-object p2, p0, LX/91k;->d:LX/90B;

    .line 1431327
    iput-object p3, p0, LX/91k;->e:LX/90D;

    .line 1431328
    const/4 v0, -0x1

    iput v0, p0, LX/91k;->f:I

    .line 1431329
    return-void
.end method


# virtual methods
.method public final a(LX/1De;I)LX/1X1;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "I)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1431330
    iget-object v0, p0, LX/91k;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lt p2, v0, :cond_0

    iget-boolean v0, p0, LX/91k;->g:Z

    if-eqz v0, :cond_0

    .line 1431331
    new-instance v0, LX/91j;

    invoke-direct {v0, p0}, LX/91j;-><init>(LX/91k;)V

    .line 1431332
    invoke-static {p1}, LX/91S;->c(LX/1De;)LX/91R;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/91R;->a(Landroid/view/View$OnClickListener;)LX/91R;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 1431333
    :goto_0
    return-object v0

    .line 1431334
    :cond_0
    iget-object v0, p0, LX/91k;->a:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    .line 1431335
    iget v1, p0, LX/91k;->f:I

    if-ne p2, v1, :cond_1

    .line 1431336
    iget-object v1, p0, LX/91k;->c:LX/91o;

    invoke-virtual {v1, p1}, LX/91o;->c(LX/1De;)LX/91m;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/91m;->a(Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;)LX/91m;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/91m;->a(Z)LX/91m;

    move-result-object v0

    iget-object v1, p0, LX/91k;->e:LX/90D;

    .line 1431337
    iget-object p0, v0, LX/91m;->a:LX/91n;

    iput-object v1, p0, LX/91n;->e:LX/90D;

    .line 1431338
    move-object v0, v0

    .line 1431339
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    goto :goto_0

    .line 1431340
    :cond_1
    iget-object v1, p0, LX/91k;->c:LX/91o;

    invoke-virtual {v1, p1}, LX/91o;->c(LX/1De;)LX/91m;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/91m;->a(Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;)LX/91m;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/91m;->a(Z)LX/91m;

    move-result-object v0

    iget-object v1, p0, LX/91k;->d:LX/90B;

    .line 1431341
    iget-object p0, v0, LX/91m;->a:LX/91n;

    iput-object v1, p0, LX/91n;->d:LX/90B;

    .line 1431342
    move-object v0, v0

    .line 1431343
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    goto :goto_0
.end method

.method public final e()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1431344
    iget-object v1, p0, LX/91k;->a:LX/0Px;

    if-eqz v1, :cond_1

    .line 1431345
    iget-object v1, p0, LX/91k;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    iget-boolean v2, p0, LX/91k;->g:Z

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    add-int/2addr v0, v1

    .line 1431346
    :cond_1
    return v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 1431347
    const/4 v0, 0x1

    return v0
.end method

.method public final m()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1431348
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 1431349
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/91k;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1431350
    iget-object v0, p0, LX/91k;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->e()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->e()Ljava/lang/String;

    move-result-object v0

    .line 1431351
    if-eqz v0, :cond_0

    .line 1431352
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1431353
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1431354
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method
