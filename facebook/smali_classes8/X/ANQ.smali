.class public final LX/ANQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Landroid/net/Uri;

.field public final synthetic b:Lcom/facebook/cameracore/ui/CameraCoreFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/cameracore/ui/CameraCoreFragment;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 1667680
    iput-object p1, p0, LX/ANQ;->b:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    iput-object p2, p0, LX/ANQ;->a:Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1667681
    sget-object v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->c:Ljava/lang/String;

    const-string v1, "Unable to fetch image from URI: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, LX/ANQ;->a:Landroid/net/Uri;

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1667682
    iget-object v0, p0, LX/ANQ;->b:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f08277d

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1667683
    iget-object v0, p0, LX/ANQ;->b:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    iget-object v0, v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->s:LX/ANZ;

    .line 1667684
    iget-object v1, p0, LX/ANQ;->b:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    sget-object v2, LX/ANZ;->PHOTO:LX/ANZ;

    .line 1667685
    iput-object v2, v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->s:LX/ANZ;

    .line 1667686
    iget-object v1, p0, LX/ANQ;->b:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    iget-object v2, p0, LX/ANQ;->b:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    iget-object v2, v2, Lcom/facebook/cameracore/ui/CameraCoreFragment;->s:LX/ANZ;

    invoke-static {v1, v0, v2}, Lcom/facebook/cameracore/ui/CameraCoreFragment;->a$redex0(Lcom/facebook/cameracore/ui/CameraCoreFragment;LX/ANZ;LX/ANZ;)V

    .line 1667687
    iget-object v0, p0, LX/ANQ;->b:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    iget-object v0, v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->P:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0, v4}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 1667688
    iget-object v0, p0, LX/ANQ;->b:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    invoke-static {v0}, Lcom/facebook/cameracore/ui/CameraCoreFragment;->K(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V

    .line 1667689
    iget-object v0, p0, LX/ANQ;->b:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    iget-object v1, p0, LX/ANQ;->b:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    iget-object v1, v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->t:LX/6JF;

    invoke-static {v0, v1}, Lcom/facebook/cameracore/ui/CameraCoreFragment;->a$redex0(Lcom/facebook/cameracore/ui/CameraCoreFragment;LX/6JF;)V

    .line 1667690
    return-void
.end method
