.class public final LX/9ER;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Ve;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6Ve",
        "<",
        "LX/8q4;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/9Ea;


# direct methods
.method public constructor <init>(LX/9Ea;)V
    .locals 0

    .prologue
    .line 1457027
    iput-object p1, p0, LX/9ER;->a:LX/9Ea;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1457028
    check-cast p1, LX/8q4;

    .line 1457029
    iget-object v0, p1, LX/8pw;->a:Lcom/facebook/graphql/model/GraphQLComment;

    move-object v0, v0

    .line 1457030
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1457031
    iget-object v0, p0, LX/9ER;->a:LX/9Ea;

    iget-object v0, v0, LX/9Ea;->e:LX/03V;

    sget-object v1, LX/9Ea;->a:Ljava/lang/String;

    const-string v2, "Updated comment must have a non null feedback"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1457032
    :goto_0
    return-void

    .line 1457033
    :cond_0
    iget-object v0, p0, LX/9ER;->a:LX/9Ea;

    iget-object v0, v0, LX/9Ea;->m:LX/9EZ;

    .line 1457034
    iget-object v1, p1, LX/8pw;->a:Lcom/facebook/graphql/model/GraphQLComment;

    move-object v1, v1

    .line 1457035
    invoke-static {v0, v1}, LX/9EZ;->c(LX/9EZ;Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 1457036
    goto :goto_0
.end method
