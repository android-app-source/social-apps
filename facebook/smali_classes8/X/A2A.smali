.class public final LX/A2A;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1612935
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_8

    .line 1612936
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1612937
    :goto_0
    return v1

    .line 1612938
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_4

    .line 1612939
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1612940
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1612941
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_0

    if-eqz v8, :cond_0

    .line 1612942
    const-string v9, "day"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 1612943
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v7, v4

    move v4, v2

    goto :goto_1

    .line 1612944
    :cond_1
    const-string v9, "month"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 1612945
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v6, v3

    move v3, v2

    goto :goto_1

    .line 1612946
    :cond_2
    const-string v9, "year"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1612947
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v5, v0

    move v0, v2

    goto :goto_1

    .line 1612948
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1612949
    :cond_4
    const/4 v8, 0x3

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1612950
    if-eqz v4, :cond_5

    .line 1612951
    invoke-virtual {p1, v1, v7, v1}, LX/186;->a(III)V

    .line 1612952
    :cond_5
    if-eqz v3, :cond_6

    .line 1612953
    invoke-virtual {p1, v2, v6, v1}, LX/186;->a(III)V

    .line 1612954
    :cond_6
    if-eqz v0, :cond_7

    .line 1612955
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v5, v1}, LX/186;->a(III)V

    .line 1612956
    :cond_7
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_8
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1612957
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1612958
    invoke-virtual {p0, p1, v2, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1612959
    if-eqz v0, :cond_0

    .line 1612960
    const-string v1, "day"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1612961
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1612962
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1612963
    if-eqz v0, :cond_1

    .line 1612964
    const-string v1, "month"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1612965
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1612966
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1612967
    if-eqz v0, :cond_2

    .line 1612968
    const-string v1, "year"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1612969
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1612970
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1612971
    return-void
.end method
