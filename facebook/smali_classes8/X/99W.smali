.class public abstract LX/99W;
.super LX/6ly;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ITEM:",
        "Ljava/lang/Object;",
        ">",
        "LX/6ly",
        "<TITEM;>;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TITEM;>;"
        }
    .end annotation
.end field

.field public b:Z


# direct methods
.method public constructor <init>(LX/0ja;)V
    .locals 0

    .prologue
    .line 1447360
    invoke-direct {p0, p1}, LX/6ly;-><init>(LX/0ja;)V

    .line 1447361
    return-void
.end method


# virtual methods
.method public a(II)LX/99X;
    .locals 1

    .prologue
    .line 1447362
    if-nez p1, :cond_0

    .line 1447363
    sget-object v0, LX/99X;->FIRST:LX/99X;

    .line 1447364
    :goto_0
    return-object v0

    .line 1447365
    :cond_0
    add-int/lit8 v0, p2, -0x1

    if-ne p1, v0, :cond_1

    .line 1447366
    sget-object v0, LX/99X;->LAST:LX/99X;

    goto :goto_0

    .line 1447367
    :cond_1
    sget-object v0, LX/99X;->INNER:LX/99X;

    goto :goto_0
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TITEM;"
        }
    .end annotation

    .prologue
    .line 1447368
    iget-object v0, p0, LX/99W;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TITEM;>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 1447369
    iput-object p1, p0, LX/99W;->a:Ljava/util/List;

    .line 1447370
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, LX/99W;->b:Z

    .line 1447371
    invoke-virtual {p0}, LX/0gG;->kV_()V

    .line 1447372
    return-void

    .line 1447373
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1447374
    const v0, 0x7f0d00f8

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1447375
    iget-object v0, p0, LX/99W;->a:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/99W;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()LX/2eR;
    .locals 1

    .prologue
    .line 1447376
    invoke-virtual {p0}, LX/99W;->e()LX/2eR;

    move-result-object v0

    return-object v0
.end method

.method public abstract e()LX/2eR;
.end method
