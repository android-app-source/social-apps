.class public final LX/8zN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/metatext/TagsTextViewContainer;

.field private b:LX/8zD;

.field private c:LX/8z9;

.field private d:I

.field private e:Landroid/text/SpannableStringBuilder;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/metatext/TagsTextViewContainer;)V
    .locals 1

    .prologue
    .line 1427175
    iput-object p1, p0, LX/8zN;->a:Lcom/facebook/composer/metatext/TagsTextViewContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1427176
    const/4 v0, 0x0

    iput v0, p0, LX/8zN;->d:I

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/composer/metatext/TagsTextViewContainer;B)V
    .locals 0

    .prologue
    .line 1427177
    invoke-direct {p0, p1}, LX/8zN;-><init>(Lcom/facebook/composer/metatext/TagsTextViewContainer;)V

    return-void
.end method


# virtual methods
.method public final a(LX/8zD;LX/8z9;ILandroid/text/SpannableStringBuilder;)V
    .locals 0

    .prologue
    .line 1427178
    iput-object p1, p0, LX/8zN;->b:LX/8zD;

    .line 1427179
    iput-object p2, p0, LX/8zN;->c:LX/8z9;

    .line 1427180
    iput p3, p0, LX/8zN;->d:I

    .line 1427181
    iput-object p4, p0, LX/8zN;->e:Landroid/text/SpannableStringBuilder;

    .line 1427182
    return-void
.end method

.method public final onGlobalLayout()V
    .locals 7

    .prologue
    .line 1427183
    iget v0, p0, LX/8zN;->d:I

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8zN;->a:Lcom/facebook/composer/metatext/TagsTextViewContainer;

    iget-object v0, v0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->l:LX/8zL;

    if-nez v0, :cond_1

    .line 1427184
    :cond_0
    :goto_0
    return-void

    .line 1427185
    :cond_1
    iget-object v0, p0, LX/8zN;->a:Lcom/facebook/composer/metatext/TagsTextViewContainer;

    iget-object v0, v0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->e:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    .line 1427186
    if-eqz v0, :cond_0

    .line 1427187
    :try_start_0
    iget-object v1, p0, LX/8zN;->a:Lcom/facebook/composer/metatext/TagsTextViewContainer;

    iget-object v1, v1, Lcom/facebook/composer/metatext/TagsTextViewContainer;->e:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLineHeight()I

    move-result v1

    .line 1427188
    if-eqz v1, :cond_0

    .line 1427189
    iget-object v2, p0, LX/8zN;->a:Lcom/facebook/composer/metatext/TagsTextViewContainer;

    iget-object v2, v2, Lcom/facebook/composer/metatext/TagsTextViewContainer;->e:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    div-int v1, v2, v1

    .line 1427190
    if-lez v1, :cond_0

    .line 1427191
    iget-object v2, p0, LX/8zN;->a:Lcom/facebook/composer/metatext/TagsTextViewContainer;

    iget-object v2, v2, Lcom/facebook/composer/metatext/TagsTextViewContainer;->e:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLineCount()I

    move-result v2

    .line 1427192
    if-le v2, v1, :cond_0

    .line 1427193
    add-int/lit8 v1, v1, -0x1

    .line 1427194
    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineStart(I)I

    move-result v2

    .line 1427195
    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineVisibleEnd(I)I

    move-result v0

    .line 1427196
    iget-object v1, p0, LX/8zN;->a:Lcom/facebook/composer/metatext/TagsTextViewContainer;

    invoke-virtual {v1}, Lcom/facebook/composer/metatext/TagsTextViewContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0823cd

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1427197
    iget-object v1, p0, LX/8zN;->a:Lcom/facebook/composer/metatext/TagsTextViewContainer;

    iget-object v1, v1, Lcom/facebook/composer/metatext/TagsTextViewContainer;->e:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v4

    .line 1427198
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "... "

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v1

    .line 1427199
    iget-object v5, p0, LX/8zN;->a:Lcom/facebook/composer/metatext/TagsTextViewContainer;

    iget-object v5, v5, Lcom/facebook/composer/metatext/TagsTextViewContainer;->e:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v5

    int-to-float v5, v5

    const v6, 0x3f666666    # 0.9f

    mul-float/2addr v5, v6

    sub-float/2addr v5, v1

    .line 1427200
    const/4 v1, 0x0

    cmpg-float v1, v5, v1

    if-lez v1, :cond_0

    if-ltz v2, :cond_0

    if-ltz v0, :cond_0

    .line 1427201
    iget-object v1, p0, LX/8zN;->e:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1427202
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v1

    if-lt v0, v1, :cond_2

    .line 1427203
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    .line 1427204
    :goto_1
    invoke-virtual {v6, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    cmpl-float v0, v0, v5

    if-lez v0, :cond_3

    .line 1427205
    add-int/lit8 v0, v1, -0x1

    .line 1427206
    if-le v0, v2, :cond_0

    :cond_2
    move v1, v0

    goto :goto_1

    .line 1427207
    :cond_3
    iget-object v0, p0, LX/8zN;->e:Landroid/text/SpannableStringBuilder;

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/text/SpannableStringBuilder;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/SpannableStringBuilder;

    iput-object v0, p0, LX/8zN;->e:Landroid/text/SpannableStringBuilder;

    .line 1427208
    iget-object v0, p0, LX/8zN;->e:Landroid/text/SpannableStringBuilder;

    const-string v2, "... "

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1427209
    new-instance v0, LX/8zM;

    iget-object v2, p0, LX/8zN;->a:Lcom/facebook/composer/metatext/TagsTextViewContainer;

    iget-object v4, p0, LX/8zN;->b:LX/8zD;

    iget-object v5, p0, LX/8zN;->c:LX/8z9;

    invoke-direct {v0, v2, v4, v5}, LX/8zM;-><init>(Lcom/facebook/composer/metatext/TagsTextViewContainer;LX/8zD;LX/8z9;)V

    .line 1427210
    iget-object v2, p0, LX/8zN;->e:Landroid/text/SpannableStringBuilder;

    add-int/lit8 v4, v1, 0x4

    add-int/lit8 v1, v1, 0x4

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v1, v3

    const/16 v3, 0x21

    invoke-virtual {v2, v0, v4, v1, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1427211
    iget-object v0, p0, LX/8zN;->a:Lcom/facebook/composer/metatext/TagsTextViewContainer;

    iget-object v0, v0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->e:Landroid/widget/TextView;

    iget-object v1, p0, LX/8zN;->e:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1427212
    :catch_0
    move-exception v0

    .line 1427213
    sget-object v1, Lcom/facebook/composer/metatext/TagsTextViewContainer;->d:Ljava/lang/String;

    const-string v2, "Exception adjust caption length for TextView"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0
.end method
