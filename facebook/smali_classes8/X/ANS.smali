.class public final LX/ANS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/cameracore/ui/CameraCoreFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V
    .locals 0

    .prologue
    .line 1667697
    iput-object p1, p0, LX/ANS;->a:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x546834c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1667698
    sget-object v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->h:LX/6Ia;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->h:LX/6Ia;

    invoke-interface {v1}, LX/6Ia;->k()Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->h:LX/6Ia;

    invoke-interface {v1}, LX/6Ia;->f()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    iget-object v1, p0, LX/ANS;->a:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    iget-object v1, v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->s:LX/ANZ;

    sget-object v2, LX/ANZ;->IMAGE:LX/ANZ;

    if-ne v1, v2, :cond_3

    .line 1667699
    :cond_1
    iget-object v1, p0, LX/ANS;->a:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    .line 1667700
    iget-object v2, v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->s:LX/ANZ;

    sget-object v4, LX/ANZ;->IMAGE:LX/ANZ;

    if-eq v2, v4, :cond_2

    iget-object v2, v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->s:LX/ANZ;

    sget-object v4, LX/ANZ;->PHOTO:LX/ANZ;

    if-ne v2, v4, :cond_4

    .line 1667701
    :cond_2
    iget-object v2, v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->g:Lcom/facebook/resources/ui/FbImageButton;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/facebook/resources/ui/FbImageButton;->setEnabled(Z)V

    .line 1667702
    iget-object v2, v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->z:LX/AMH;

    sget-object v4, Lcom/facebook/cameracore/ui/CaptureType;->PHOTO:Lcom/facebook/cameracore/ui/CaptureType;

    invoke-virtual {v2, v4}, LX/AMH;->a(Lcom/facebook/cameracore/ui/CaptureType;)Ljava/io/File;

    move-result-object v2

    .line 1667703
    sget-object v4, Lcom/facebook/cameracore/ui/CameraCoreFragment;->e:LX/6Jt;

    iget-object p0, v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->f:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

    .line 1667704
    new-instance p1, LX/ANM;

    invoke-direct {p1, v1, v2}, LX/ANM;-><init>(Lcom/facebook/cameracore/ui/CameraCoreFragment;Ljava/io/File;)V

    move-object p1, p1

    .line 1667705
    invoke-virtual {v4, v2, p0, p1}, LX/6Jt;->a(Ljava/io/File;Landroid/view/View;LX/6JI;)V

    .line 1667706
    iget-object v2, v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->q:Landroid/view/View;

    iget-object v4, v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->r:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v2, v4}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1667707
    :cond_3
    :goto_0
    const v1, 0x1d09baa

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1667708
    :cond_4
    iget-object v2, v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->s:LX/ANZ;

    sget-object v4, LX/ANZ;->VIDEO:LX/ANZ;

    if-ne v2, v4, :cond_3

    .line 1667709
    const/4 p0, 0x1

    .line 1667710
    sget-object v2, Lcom/facebook/cameracore/ui/CameraCoreFragment;->e:LX/6Jt;

    invoke-virtual {v2}, LX/6Jt;->a()LX/6K9;

    move-result-object v2

    .line 1667711
    sget-object v4, LX/6K9;->PREPARED:LX/6K9;

    if-ne v2, v4, :cond_6

    .line 1667712
    invoke-static {v1}, Lcom/facebook/cameracore/ui/CameraCoreFragment;->B(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V

    .line 1667713
    :cond_5
    :goto_1
    goto :goto_0

    .line 1667714
    :cond_6
    sget-object v4, LX/6K9;->RECORDING:LX/6K9;

    if-ne v2, v4, :cond_7

    .line 1667715
    sget-object v2, Lcom/facebook/cameracore/ui/CameraCoreFragment;->e:LX/6Jt;

    invoke-virtual {v2}, LX/6Jt;->e()V

    goto :goto_1

    .line 1667716
    :cond_7
    sget-object v4, LX/6K9;->STOPPED:LX/6K9;

    if-ne v2, v4, :cond_8

    .line 1667717
    invoke-static {v1}, Lcom/facebook/cameracore/ui/CameraCoreFragment;->r(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V

    .line 1667718
    iput-boolean p0, v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->aa:Z

    goto :goto_1

    .line 1667719
    :cond_8
    sget-object v4, LX/6K9;->PREPARE_STARTED:LX/6K9;

    if-ne v2, v4, :cond_5

    .line 1667720
    iput-boolean p0, v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->aa:Z

    goto :goto_1
.end method
