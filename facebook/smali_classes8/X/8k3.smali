.class public final LX/8k3;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 1395359
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1395360
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1395361
    :goto_0
    return v1

    .line 1395362
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1395363
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1395364
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1395365
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1395366
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1395367
    const-string v4, "nodes"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1395368
    invoke-static {p0, p1}, LX/8kR;->b(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1395369
    :cond_2
    const-string v4, "page_info"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1395370
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1395371
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v5, :cond_a

    .line 1395372
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1395373
    :goto_2
    move v0, v3

    .line 1395374
    goto :goto_1

    .line 1395375
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1395376
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1395377
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1395378
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 1395379
    :cond_5
    const-string v8, "has_next_page"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 1395380
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v5, v0

    move v0, v4

    .line 1395381
    :cond_6
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_8

    .line 1395382
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1395383
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1395384
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_6

    if-eqz v7, :cond_6

    .line 1395385
    const-string v8, "end_cursor"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1395386
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_3

    .line 1395387
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 1395388
    :cond_8
    const/4 v7, 0x2

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1395389
    invoke-virtual {p1, v3, v6}, LX/186;->b(II)V

    .line 1395390
    if-eqz v0, :cond_9

    .line 1395391
    invoke-virtual {p1, v4, v5}, LX/186;->a(IZ)V

    .line 1395392
    :cond_9
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_a
    move v0, v3

    move v5, v3

    move v6, v3

    goto :goto_3
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1395393
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1395394
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1395395
    if-eqz v0, :cond_0

    .line 1395396
    const-string v1, "nodes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1395397
    invoke-static {p0, v0, p2, p3}, LX/8kR;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1395398
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1395399
    if-eqz v0, :cond_3

    .line 1395400
    const-string v1, "page_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1395401
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1395402
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1395403
    if-eqz v1, :cond_1

    .line 1395404
    const-string p1, "end_cursor"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1395405
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1395406
    :cond_1
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 1395407
    if-eqz v1, :cond_2

    .line 1395408
    const-string p1, "has_next_page"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1395409
    invoke-virtual {p2, v1}, LX/0nX;->a(Z)V

    .line 1395410
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1395411
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1395412
    return-void
.end method
