.class public final enum LX/9jG;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9jG;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9jG;

.field public static final enum ALBUM_CREATOR:LX/9jG;

.field public static final enum CHECKIN:LX/9jG;

.field public static final enum EVENT:LX/9jG;

.field public static final enum FORSALE_POST:LX/9jG;

.field public static final enum GALLERY:LX/9jG;

.field public static final enum HIDE_GEOHUBS:LX/9jG;

.field public static final enum LIGHTWEIGHT_PLACE_PICKER:LX/9jG;

.field public static final enum PHOTO:LX/9jG;

.field public static final enum PLACE_TIPS_APP:LX/9jG;

.field public static final enum PLACE_TIPS_EMPLOYEE_SETTINGS:LX/9jG;

.field public static final enum SOCIAL_SEARCH_ADD_PLACE_SEEKER:LX/9jG;

.field public static final enum SOCIAL_SEARCH_COMMENT:LX/9jG;

.field public static final enum SOCIAL_SEARCH_CONVERSION:LX/9jG;

.field public static final enum STATUS:LX/9jG;

.field public static final enum VIDEO:LX/9jG;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1529658
    new-instance v0, LX/9jG;

    const-string v1, "CHECKIN"

    invoke-direct {v0, v1, v3}, LX/9jG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9jG;->CHECKIN:LX/9jG;

    .line 1529659
    new-instance v0, LX/9jG;

    const-string v1, "EVENT"

    invoke-direct {v0, v1, v4}, LX/9jG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9jG;->EVENT:LX/9jG;

    .line 1529660
    new-instance v0, LX/9jG;

    const-string v1, "PHOTO"

    invoke-direct {v0, v1, v5}, LX/9jG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9jG;->PHOTO:LX/9jG;

    .line 1529661
    new-instance v0, LX/9jG;

    const-string v1, "STATUS"

    invoke-direct {v0, v1, v6}, LX/9jG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9jG;->STATUS:LX/9jG;

    .line 1529662
    new-instance v0, LX/9jG;

    const-string v1, "VIDEO"

    invoke-direct {v0, v1, v7}, LX/9jG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9jG;->VIDEO:LX/9jG;

    .line 1529663
    new-instance v0, LX/9jG;

    const-string v1, "ALBUM_CREATOR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/9jG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9jG;->ALBUM_CREATOR:LX/9jG;

    .line 1529664
    new-instance v0, LX/9jG;

    const-string v1, "GALLERY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/9jG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9jG;->GALLERY:LX/9jG;

    .line 1529665
    new-instance v0, LX/9jG;

    const-string v1, "HIDE_GEOHUBS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/9jG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9jG;->HIDE_GEOHUBS:LX/9jG;

    .line 1529666
    new-instance v0, LX/9jG;

    const-string v1, "LIGHTWEIGHT_PLACE_PICKER"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/9jG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9jG;->LIGHTWEIGHT_PLACE_PICKER:LX/9jG;

    .line 1529667
    new-instance v0, LX/9jG;

    const-string v1, "PLACE_TIPS_APP"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/9jG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9jG;->PLACE_TIPS_APP:LX/9jG;

    .line 1529668
    new-instance v0, LX/9jG;

    const-string v1, "PLACE_TIPS_EMPLOYEE_SETTINGS"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/9jG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9jG;->PLACE_TIPS_EMPLOYEE_SETTINGS:LX/9jG;

    .line 1529669
    new-instance v0, LX/9jG;

    const-string v1, "SOCIAL_SEARCH_COMMENT"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/9jG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9jG;->SOCIAL_SEARCH_COMMENT:LX/9jG;

    .line 1529670
    new-instance v0, LX/9jG;

    const-string v1, "SOCIAL_SEARCH_CONVERSION"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/9jG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9jG;->SOCIAL_SEARCH_CONVERSION:LX/9jG;

    .line 1529671
    new-instance v0, LX/9jG;

    const-string v1, "SOCIAL_SEARCH_ADD_PLACE_SEEKER"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/9jG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9jG;->SOCIAL_SEARCH_ADD_PLACE_SEEKER:LX/9jG;

    .line 1529672
    new-instance v0, LX/9jG;

    const-string v1, "FORSALE_POST"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/9jG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9jG;->FORSALE_POST:LX/9jG;

    .line 1529673
    const/16 v0, 0xf

    new-array v0, v0, [LX/9jG;

    sget-object v1, LX/9jG;->CHECKIN:LX/9jG;

    aput-object v1, v0, v3

    sget-object v1, LX/9jG;->EVENT:LX/9jG;

    aput-object v1, v0, v4

    sget-object v1, LX/9jG;->PHOTO:LX/9jG;

    aput-object v1, v0, v5

    sget-object v1, LX/9jG;->STATUS:LX/9jG;

    aput-object v1, v0, v6

    sget-object v1, LX/9jG;->VIDEO:LX/9jG;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/9jG;->ALBUM_CREATOR:LX/9jG;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/9jG;->GALLERY:LX/9jG;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/9jG;->HIDE_GEOHUBS:LX/9jG;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/9jG;->LIGHTWEIGHT_PLACE_PICKER:LX/9jG;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/9jG;->PLACE_TIPS_APP:LX/9jG;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/9jG;->PLACE_TIPS_EMPLOYEE_SETTINGS:LX/9jG;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/9jG;->SOCIAL_SEARCH_COMMENT:LX/9jG;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/9jG;->SOCIAL_SEARCH_CONVERSION:LX/9jG;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/9jG;->SOCIAL_SEARCH_ADD_PLACE_SEEKER:LX/9jG;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/9jG;->FORSALE_POST:LX/9jG;

    aput-object v2, v0, v1

    sput-object v0, LX/9jG;->$VALUES:[LX/9jG;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1529674
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9jG;
    .locals 1

    .prologue
    .line 1529675
    const-class v0, LX/9jG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9jG;

    return-object v0
.end method

.method public static values()[LX/9jG;
    .locals 1

    .prologue
    .line 1529676
    sget-object v0, LX/9jG;->$VALUES:[LX/9jG;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9jG;

    return-object v0
.end method


# virtual methods
.method public final isSocialSearchType()Z
    .locals 1

    .prologue
    .line 1529677
    sget-object v0, LX/9jG;->SOCIAL_SEARCH_COMMENT:LX/9jG;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/9jG;->SOCIAL_SEARCH_CONVERSION:LX/9jG;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/9jG;->SOCIAL_SEARCH_ADD_PLACE_SEEKER:LX/9jG;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toLegacyString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1529678
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "mobile_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/9jG;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
