.class public LX/8vB;
.super LX/8QL;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/8QL",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
        ">;>;"
    }
.end annotation


# instance fields
.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1416710
    sget-object v0, LX/8vA;->GROUP_TAG:LX/8vA;

    invoke-direct {p0, v0}, LX/8QL;-><init>(LX/8vA;)V

    .line 1416711
    iput-object p1, p0, LX/8vB;->e:Ljava/util/List;

    .line 1416712
    iput-object p2, p0, LX/8vB;->f:Ljava/lang/String;

    .line 1416713
    return-void
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1416714
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1416715
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/8vB;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1416716
    iget-object v0, p0, LX/8vB;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 1416717
    iget-object v3, v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->e:Lcom/facebook/user/model/Name;

    move-object v3, v3

    .line 1416718
    if-eqz v3, :cond_0

    .line 1416719
    iget-object v3, v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->e:Lcom/facebook/user/model/Name;

    move-object v0, v3

    .line 1416720
    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1416721
    :cond_0
    iget-object v0, p0, LX/8vB;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v1, v0, :cond_1

    .line 1416722
    const-string v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1416723
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1416724
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1416725
    iget-object v0, p0, LX/8vB;->e:Ljava/util/List;

    move-object v0, v0

    .line 1416726
    return-object v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 1416727
    const/4 v0, -0x1

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 1416728
    const/4 v0, -0x1

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1416729
    if-ne p1, p0, :cond_0

    .line 1416730
    const/4 v0, 0x1

    .line 1416731
    :goto_0
    return v0

    .line 1416732
    :cond_0
    instance-of v0, p1, LX/8vB;

    if-nez v0, :cond_1

    .line 1416733
    const/4 v0, 0x0

    goto :goto_0

    .line 1416734
    :cond_1
    iget-object v0, p0, LX/8vB;->e:Ljava/util/List;

    move-object v0, v0

    .line 1416735
    check-cast p1, LX/8vB;

    .line 1416736
    iget-object v1, p1, LX/8vB;->e:Ljava/util/List;

    move-object v1, v1

    .line 1416737
    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1416738
    const/4 v0, -0x1

    return v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 1416739
    const/4 v0, -0x1

    return v0
.end method

.method public final h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1416740
    iget-object v0, p0, LX/8vB;->e:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    invoke-virtual {v0}, LX/8QL;->h()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1416741
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 1416742
    iget-object v2, p0, LX/8vB;->e:Ljava/util/List;

    move-object v2, v2

    .line 1416743
    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
