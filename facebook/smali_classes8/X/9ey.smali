.class public final LX/9ey;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9bz;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V
    .locals 0

    .prologue
    .line 1520702
    iput-object p1, p0, LX/9ey;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1520703
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 1520704
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/photos/creativeediting/model/SwipeableParams;I)V
    .locals 1

    .prologue
    .line 1520705
    iget-object v0, p0, LX/9ey;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    .line 1520706
    iget p1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->aa:I

    add-int/lit8 p3, p1, 0x1

    iput p3, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->aa:I

    .line 1520707
    iget-object v0, p0, LX/9ey;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->P:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1520708
    iget-object v0, p0, LX/9ey;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->P:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9c7;

    .line 1520709
    iget-object p0, p2, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->c:LX/5jI;

    move-object p0, p0

    .line 1520710
    sget-object p1, LX/5jI;->FRAME:LX/5jI;

    if-ne p0, p1, :cond_1

    const/4 p0, 0x1

    .line 1520711
    :goto_0
    new-instance p1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    if-eqz p0, :cond_2

    sget-object p0, LX/9c4;->EDITGALLERY_FRAME_VIEWED:LX/9c4;

    invoke-virtual {p0}, LX/9c4;->toString()Ljava/lang/String;

    move-result-object p0

    :goto_1
    invoke-direct {p1, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p0, "composer"

    .line 1520712
    iput-object p0, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1520713
    move-object p0, p1

    .line 1520714
    sget-object p1, LX/9c5;->FILTER_NAME:LX/9c5;

    invoke-virtual {p1}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object p1

    .line 1520715
    iget-object p3, p2, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->b:Ljava/lang/String;

    move-object p3, p3

    .line 1520716
    invoke-virtual {p0, p1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    .line 1520717
    invoke-static {v0, p0}, LX/9c7;->a(LX/9c7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1520718
    :cond_0
    return-void

    .line 1520719
    :cond_1
    const/4 p0, 0x0

    goto :goto_0

    .line 1520720
    :cond_2
    sget-object p0, LX/9c4;->EDITGALLERY_FILTER_VIEWED:LX/9c4;

    invoke-virtual {p0}, LX/9c4;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_1
.end method

.method public final b(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 1520721
    return-void
.end method

.method public final c(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 1520722
    return-void
.end method

.method public final d(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 1520723
    return-void
.end method

.method public final e(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 1520724
    return-void
.end method
