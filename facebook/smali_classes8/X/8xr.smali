.class public final LX/8xr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/8xt;


# direct methods
.method public constructor <init>(LX/8xt;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1424811
    iput-object p1, p0, LX/8xr;->d:LX/8xt;

    iput-object p2, p0, LX/8xr;->a:Ljava/lang/String;

    iput-object p3, p0, LX/8xr;->b:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p4, p0, LX/8xr;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1424802
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1424803
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1424804
    iget-object v0, p0, LX/8xr;->a:Ljava/lang/String;

    invoke-static {p1, v0}, LX/8xt;->b(Lcom/facebook/graphql/executor/GraphQLResult;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    .line 1424805
    if-nez v0, :cond_1

    .line 1424806
    :cond_0
    :goto_0
    return-void

    .line 1424807
    :cond_1
    iget-object v1, p0, LX/8xr;->d:LX/8xt;

    iget-object v1, v1, LX/8xt;->c:LX/189;

    iget-object v2, p0, LX/8xr;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    iget-object v3, p0, LX/8xr;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, LX/189;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPage;Ljava/lang/String;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1424808
    if-eqz v0, :cond_0

    .line 1424809
    invoke-static {v0}, LX/182;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 1424810
    iget-object v1, p0, LX/8xr;->d:LX/8xt;

    iget-object v1, v1, LX/8xt;->e:LX/0bH;

    new-instance v2, LX/1Ne;

    invoke-direct {v2, v0}, LX/1Ne;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0
.end method
