.class public LX/8v3;
.super Lcom/facebook/resources/ui/FbFrameLayout;
.source ""


# instance fields
.field private final a:Landroid/graphics/Rect;

.field private b:Landroid/view/View;

.field private c:Landroid/widget/ImageView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Lcom/facebook/fbui/glyph/GlyphView;

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1416519
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/8v3;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1416520
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1416499
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/8v3;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1416500
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1416510
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1416511
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/8v3;->a:Landroid/graphics/Rect;

    .line 1416512
    const v0, 0x7f03035f

    invoke-static {p1, v0, p0}, LX/8v3;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1416513
    const v0, 0x7f0d0b19

    invoke-virtual {p0, v0}, LX/8v3;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/8v3;->b:Landroid/view/View;

    .line 1416514
    const v0, 0x7f0d0b1a

    invoke-virtual {p0, v0}, LX/8v3;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/8v3;->c:Landroid/widget/ImageView;

    .line 1416515
    const v0, 0x7f0d0b1b

    invoke-virtual {p0, v0}, LX/8v3;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/8v3;->d:Landroid/widget/TextView;

    .line 1416516
    const v0, 0x7f0d0b1c

    invoke-virtual {p0, v0}, LX/8v3;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/8v3;->e:Landroid/widget/TextView;

    .line 1416517
    const v0, 0x7f0d0b1d

    invoke-virtual {p0, v0}, LX/8v3;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/8v3;->f:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1416518
    return-void
.end method


# virtual methods
.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 1416507
    iget-object v0, p0, LX/8v3;->f:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v1, p0, LX/8v3;->a:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->getHitRect(Landroid/graphics/Rect;)V

    .line 1416508
    iget-object v0, p0, LX/8v3;->a:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    iput-boolean v0, p0, LX/8v3;->g:Z

    .line 1416509
    const/4 v0, 0x0

    return v0
.end method

.method public setBackgroundResource(I)V
    .locals 1

    .prologue
    .line 1416504
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbFrameLayout;->setBackgroundResource(I)V

    .line 1416505
    iget-object v0, p0, LX/8v3;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1416506
    return-void
.end method

.method public setSelected(Z)V
    .locals 1

    .prologue
    .line 1416501
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbFrameLayout;->setSelected(Z)V

    .line 1416502
    iget-object v0, p0, LX/8v3;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setSelected(Z)V

    .line 1416503
    return-void
.end method
