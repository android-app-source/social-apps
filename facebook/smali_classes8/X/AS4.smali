.class public final enum LX/AS4;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/AS4;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/AS4;

.field public static final enum FEED_ONLY_POST_NUX:LX/AS4;


# instance fields
.field public final controllerClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "LX/3l5;",
            ">;"
        }
    .end annotation
.end field

.field public final description:Ljava/lang/String;

.field public final interstitialId:Ljava/lang/String;

.field public final prefKey:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1673607
    new-instance v0, LX/AS4;

    const-string v1, "FEED_ONLY_POST_NUX"

    const-class v3, LX/ASE;

    const-string v4, "4155"

    sget-object v5, LX/0dp;->t:LX/0Tn;

    const-string v6, "Feed-only post NUX"

    invoke-direct/range {v0 .. v6}, LX/AS4;-><init>(Ljava/lang/String;ILjava/lang/Class;Ljava/lang/String;LX/0Tn;Ljava/lang/String;)V

    sput-object v0, LX/AS4;->FEED_ONLY_POST_NUX:LX/AS4;

    .line 1673608
    const/4 v0, 0x1

    new-array v0, v0, [LX/AS4;

    sget-object v1, LX/AS4;->FEED_ONLY_POST_NUX:LX/AS4;

    aput-object v1, v0, v2

    sput-object v0, LX/AS4;->$VALUES:[LX/AS4;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/Class;Ljava/lang/String;LX/0Tn;Ljava/lang/String;)V
    .locals 0
    .param p5    # LX/0Tn;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "LX/3l5;",
            ">;",
            "Ljava/lang/String;",
            "LX/0Tn;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1673609
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1673610
    iput-object p3, p0, LX/AS4;->controllerClass:Ljava/lang/Class;

    .line 1673611
    iput-object p4, p0, LX/AS4;->interstitialId:Ljava/lang/String;

    .line 1673612
    iput-object p5, p0, LX/AS4;->prefKey:LX/0Tn;

    .line 1673613
    iput-object p6, p0, LX/AS4;->description:Ljava/lang/String;

    .line 1673614
    return-void
.end method

.method public static forControllerClass(Ljava/lang/Class;)LX/AS4;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "LX/3l5;",
            ">;)",
            "LX/AS4;"
        }
    .end annotation

    .prologue
    .line 1673615
    invoke-static {}, LX/AS4;->values()[LX/AS4;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 1673616
    iget-object v4, v3, LX/AS4;->controllerClass:Ljava/lang/Class;

    if-ne v4, p0, :cond_0

    .line 1673617
    return-object v3

    .line 1673618
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1673619
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown controller class: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/AS4;
    .locals 1

    .prologue
    .line 1673620
    const-class v0, LX/AS4;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/AS4;

    return-object v0
.end method

.method public static values()[LX/AS4;
    .locals 1

    .prologue
    .line 1673621
    sget-object v0, LX/AS4;->$VALUES:[LX/AS4;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/AS4;

    return-object v0
.end method
