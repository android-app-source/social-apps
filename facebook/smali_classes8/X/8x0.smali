.class public LX/8x0;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/8x3;

.field private final b:LX/8x7;


# direct methods
.method public constructor <init>(LX/8x3;LX/8x7;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1423377
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1423378
    iput-object p1, p0, LX/8x0;->a:LX/8x3;

    .line 1423379
    iput-object p2, p0, LX/8x0;->b:LX/8x7;

    .line 1423380
    return-void
.end method

.method public static a(LX/0QB;)LX/8x0;
    .locals 3

    .prologue
    .line 1423381
    const-class v1, LX/8x0;

    monitor-enter v1

    .line 1423382
    :try_start_0
    sget-object v0, LX/8x0;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1423383
    sput-object v2, LX/8x0;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1423384
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1423385
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/8x0;->b(LX/0QB;)LX/8x0;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1423386
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8x0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1423387
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1423388
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/8x0;
    .locals 3

    .prologue
    .line 1423389
    new-instance v2, LX/8x0;

    invoke-static {p0}, LX/8x3;->a(LX/0QB;)LX/8x3;

    move-result-object v0

    check-cast v0, LX/8x3;

    invoke-static {p0}, LX/8x7;->a(LX/0QB;)LX/8x7;

    move-result-object v1

    check-cast v1, LX/8x7;

    invoke-direct {v2, v0, v1}, LX/8x0;-><init>(LX/8x3;LX/8x7;)V

    .line 1423390
    return-object v2
.end method

.method public static onClick(Landroid/view/View;ILX/1De;)V
    .locals 1
    .param p1    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param

    .prologue
    .line 1423391
    invoke-static {p2}, LX/8wz;->d(LX/1De;)LX/1dQ;

    move-result-object v0

    invoke-static {v0, p0, p1}, LX/8wz;->a(LX/1dQ;Landroid/view/View;I)V

    .line 1423392
    return-void
.end method


# virtual methods
.method public final a(LX/1De;II)LX/1Dg;
    .locals 3
    .param p2    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x2

    .line 1423393
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    iget-object v1, p0, LX/8x0;->a:LX/8x3;

    invoke-virtual {v1, p1}, LX/8x3;->c(LX/1De;)LX/8x1;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/8x1;->h(I)LX/8x1;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/8x1;->i(I)LX/8x1;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    invoke-interface {v1, v2}, LX/1Di;->b(I)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    iget-object v1, p0, LX/8x0;->b:LX/8x7;

    invoke-virtual {v1, p1}, LX/8x7;->c(LX/1De;)LX/8x5;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/8x5;->h(I)LX/8x5;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    invoke-interface {v1, v2}, LX/1Di;->b(I)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-static {p1, p1}, LX/8wz;->onClick(LX/1De;LX/1De;)LX/1dQ;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0
.end method
