.class public final LX/9dj;
.super Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;
.source ""


# instance fields
.field public final synthetic a:LX/9dl;


# direct methods
.method public constructor <init>(LX/9dl;)V
    .locals 0

    .prologue
    .line 1518503
    iput-object p1, p0, LX/9dj;->a:LX/9dl;

    invoke-direct {p0}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 1518504
    iget-object v1, p0, LX/9dj;->a:LX/9dl;

    invoke-virtual {v1}, LX/9dl;->getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v1

    .line 1518505
    iget-object v2, v1, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    move-object v1, v2

    .line 1518506
    if-nez v1, :cond_1

    .line 1518507
    :cond_0
    :goto_0
    return v0

    .line 1518508
    :cond_1
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v2

    float-to-double v2, v2

    .line 1518509
    iget-object v4, p0, LX/9dj;->a:LX/9dl;

    invoke-virtual {v4}, LX/9dl;->getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->c(LX/362;)D

    move-result-wide v4

    .line 1518510
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v6, v2

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    const-wide v8, 0x3f747ae147ae147bL    # 0.005

    cmpg-double v1, v6, v8

    if-ltz v1, :cond_0

    .line 1518511
    const-wide v0, 0x3fe999999999999aL    # 0.8

    const-wide v6, 0x3ff3333333333333L    # 1.2

    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->min(DD)D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    .line 1518512
    iget-object v2, p0, LX/9dj;->a:LX/9dl;

    invoke-virtual {v2}, LX/9dl;->getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v2

    mul-double/2addr v0, v4

    invoke-virtual {v2, v0, v1}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->a(D)V

    .line 1518513
    iget-object v0, p0, LX/9dj;->a:LX/9dl;

    invoke-virtual {v0}, LX/9dl;->invalidate()V

    .line 1518514
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1518515
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v1

    float-to-int v1, v1

    .line 1518516
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v2

    float-to-int v2, v2

    .line 1518517
    iget-object v3, p0, LX/9dj;->a:LX/9dl;

    invoke-virtual {v3}, LX/9dl;->getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v3

    .line 1518518
    iget-object p1, v3, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    move-object v3, p1

    .line 1518519
    if-nez v3, :cond_0

    .line 1518520
    iget-object v3, p0, LX/9dj;->a:LX/9dl;

    invoke-static {v3, v1, v2, v0}, LX/9dl;->a$redex0(LX/9dl;IIZ)Z

    .line 1518521
    :cond_0
    iget-object v1, p0, LX/9dj;->a:LX/9dl;

    invoke-virtual {v1}, LX/9dl;->getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v1

    .line 1518522
    iget-object v2, v1, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    move-object v1, v2

    .line 1518523
    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method
