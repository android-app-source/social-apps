.class public final enum LX/9g6;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9g6;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9g6;

.field public static final enum CLOSED:LX/9g6;

.field public static final enum DONE:LX/9g6;

.field public static final enum ERROR:LX/9g6;

.field public static final enum LOADING:LX/9g6;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1523034
    new-instance v0, LX/9g6;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v2}, LX/9g6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9g6;->LOADING:LX/9g6;

    .line 1523035
    new-instance v0, LX/9g6;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v3}, LX/9g6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9g6;->ERROR:LX/9g6;

    .line 1523036
    new-instance v0, LX/9g6;

    const-string v1, "DONE"

    invoke-direct {v0, v1, v4}, LX/9g6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9g6;->DONE:LX/9g6;

    .line 1523037
    new-instance v0, LX/9g6;

    const-string v1, "CLOSED"

    invoke-direct {v0, v1, v5}, LX/9g6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9g6;->CLOSED:LX/9g6;

    .line 1523038
    const/4 v0, 0x4

    new-array v0, v0, [LX/9g6;

    sget-object v1, LX/9g6;->LOADING:LX/9g6;

    aput-object v1, v0, v2

    sget-object v1, LX/9g6;->ERROR:LX/9g6;

    aput-object v1, v0, v3

    sget-object v1, LX/9g6;->DONE:LX/9g6;

    aput-object v1, v0, v4

    sget-object v1, LX/9g6;->CLOSED:LX/9g6;

    aput-object v1, v0, v5

    sput-object v0, LX/9g6;->$VALUES:[LX/9g6;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1523039
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9g6;
    .locals 1

    .prologue
    .line 1523040
    const-class v0, LX/9g6;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9g6;

    return-object v0
.end method

.method public static values()[LX/9g6;
    .locals 1

    .prologue
    .line 1523041
    sget-object v0, LX/9g6;->$VALUES:[LX/9g6;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9g6;

    return-object v0
.end method
