.class public final enum LX/95K;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/95K;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/95K;

.field public static final enum BATCH_UPLOAD_ATTEMPT_ERROR:LX/95K;

.field public static final enum BATCH_UPLOAD_FAIL:LX/95K;

.field public static final enum BATCH_UPLOAD_FINISH:LX/95K;

.field public static final enum BATCH_UPLOAD_START:LX/95K;

.field public static final enum CCU_UPLOAD_FAIL:LX/95K;

.field public static final enum CCU_UPLOAD_SUCCESSS:LX/95K;

.field public static final enum COMPUTE_DELTA_AND_UPLOAD:LX/95K;

.field public static final enum OVERALL_UPLOAD_FINISH:LX/95K;

.field public static final enum OVERALL_UPLOAD_START:LX/95K;

.field public static final enum SEND_ROOTHASH_TO_SERVER:LX/95K;

.field public static final enum SNAPSHOT_AND_IMPORT_ID_DELETED:LX/95K;

.field public static final enum START_UPLOAD_CONTACTS:LX/95K;

.field public static final enum SYNC_CHECK_SERVER_RESPONSE_NOT_RECEIVED:LX/95K;

.field public static final enum SYNC_CHECK_SERVER_RESPONSE_RECEIVED:LX/95K;

.field public static final enum TURN_OFF_CCU_AFTER_EXCEPTION:LX/95K;

.field public static final enum UPDATE_SNAPSHOT_DB_WITH_SERVER_ENTRIES:LX/95K;


# instance fields
.field private final mEventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1436742
    new-instance v0, LX/95K;

    const-string v1, "SEND_ROOTHASH_TO_SERVER"

    const-string v2, "send_roothash_to_server"

    invoke-direct {v0, v1, v4, v2}, LX/95K;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/95K;->SEND_ROOTHASH_TO_SERVER:LX/95K;

    .line 1436743
    new-instance v0, LX/95K;

    const-string v1, "SYNC_CHECK_SERVER_RESPONSE_RECEIVED"

    const-string v2, "sync_check_server_response_received"

    invoke-direct {v0, v1, v5, v2}, LX/95K;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/95K;->SYNC_CHECK_SERVER_RESPONSE_RECEIVED:LX/95K;

    .line 1436744
    new-instance v0, LX/95K;

    const-string v1, "SYNC_CHECK_SERVER_RESPONSE_NOT_RECEIVED"

    const-string v2, "sync_check_server_response_not_received"

    invoke-direct {v0, v1, v6, v2}, LX/95K;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/95K;->SYNC_CHECK_SERVER_RESPONSE_NOT_RECEIVED:LX/95K;

    .line 1436745
    new-instance v0, LX/95K;

    const-string v1, "UPDATE_SNAPSHOT_DB_WITH_SERVER_ENTRIES"

    const-string v2, "update_snapshot_db_with_server_entries"

    invoke-direct {v0, v1, v7, v2}, LX/95K;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/95K;->UPDATE_SNAPSHOT_DB_WITH_SERVER_ENTRIES:LX/95K;

    .line 1436746
    new-instance v0, LX/95K;

    const-string v1, "START_UPLOAD_CONTACTS"

    const-string v2, "start_upload_contacts"

    invoke-direct {v0, v1, v8, v2}, LX/95K;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/95K;->START_UPLOAD_CONTACTS:LX/95K;

    .line 1436747
    new-instance v0, LX/95K;

    const-string v1, "CCU_UPLOAD_SUCCESSS"

    const/4 v2, 0x5

    const-string v3, "ccu_upload_success"

    invoke-direct {v0, v1, v2, v3}, LX/95K;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/95K;->CCU_UPLOAD_SUCCESSS:LX/95K;

    .line 1436748
    new-instance v0, LX/95K;

    const-string v1, "CCU_UPLOAD_FAIL"

    const/4 v2, 0x6

    const-string v3, "ccu_upload_fail"

    invoke-direct {v0, v1, v2, v3}, LX/95K;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/95K;->CCU_UPLOAD_FAIL:LX/95K;

    .line 1436749
    new-instance v0, LX/95K;

    const-string v1, "SNAPSHOT_AND_IMPORT_ID_DELETED"

    const/4 v2, 0x7

    const-string v3, "snapshot_and_import_id_deleted"

    invoke-direct {v0, v1, v2, v3}, LX/95K;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/95K;->SNAPSHOT_AND_IMPORT_ID_DELETED:LX/95K;

    .line 1436750
    new-instance v0, LX/95K;

    const-string v1, "TURN_OFF_CCU_AFTER_EXCEPTION"

    const/16 v2, 0x8

    const-string v3, "turn_off_ccu_after_exception"

    invoke-direct {v0, v1, v2, v3}, LX/95K;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/95K;->TURN_OFF_CCU_AFTER_EXCEPTION:LX/95K;

    .line 1436751
    new-instance v0, LX/95K;

    const-string v1, "OVERALL_UPLOAD_START"

    const/16 v2, 0x9

    const-string v3, "overall_upload_start"

    invoke-direct {v0, v1, v2, v3}, LX/95K;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/95K;->OVERALL_UPLOAD_START:LX/95K;

    .line 1436752
    new-instance v0, LX/95K;

    const-string v1, "OVERALL_UPLOAD_FINISH"

    const/16 v2, 0xa

    const-string v3, "overall_upload_finish"

    invoke-direct {v0, v1, v2, v3}, LX/95K;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/95K;->OVERALL_UPLOAD_FINISH:LX/95K;

    .line 1436753
    new-instance v0, LX/95K;

    const-string v1, "COMPUTE_DELTA_AND_UPLOAD"

    const/16 v2, 0xb

    const-string v3, "compute_delta_and_upload"

    invoke-direct {v0, v1, v2, v3}, LX/95K;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/95K;->COMPUTE_DELTA_AND_UPLOAD:LX/95K;

    .line 1436754
    new-instance v0, LX/95K;

    const-string v1, "BATCH_UPLOAD_START"

    const/16 v2, 0xc

    const-string v3, "batch_upload_start"

    invoke-direct {v0, v1, v2, v3}, LX/95K;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/95K;->BATCH_UPLOAD_START:LX/95K;

    .line 1436755
    new-instance v0, LX/95K;

    const-string v1, "BATCH_UPLOAD_FINISH"

    const/16 v2, 0xd

    const-string v3, "batch_upload_finish"

    invoke-direct {v0, v1, v2, v3}, LX/95K;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/95K;->BATCH_UPLOAD_FINISH:LX/95K;

    .line 1436756
    new-instance v0, LX/95K;

    const-string v1, "BATCH_UPLOAD_ATTEMPT_ERROR"

    const/16 v2, 0xe

    const-string v3, "batch_upload_attempt_error"

    invoke-direct {v0, v1, v2, v3}, LX/95K;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/95K;->BATCH_UPLOAD_ATTEMPT_ERROR:LX/95K;

    .line 1436757
    new-instance v0, LX/95K;

    const-string v1, "BATCH_UPLOAD_FAIL"

    const/16 v2, 0xf

    const-string v3, "batch_upload_fail"

    invoke-direct {v0, v1, v2, v3}, LX/95K;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/95K;->BATCH_UPLOAD_FAIL:LX/95K;

    .line 1436758
    const/16 v0, 0x10

    new-array v0, v0, [LX/95K;

    sget-object v1, LX/95K;->SEND_ROOTHASH_TO_SERVER:LX/95K;

    aput-object v1, v0, v4

    sget-object v1, LX/95K;->SYNC_CHECK_SERVER_RESPONSE_RECEIVED:LX/95K;

    aput-object v1, v0, v5

    sget-object v1, LX/95K;->SYNC_CHECK_SERVER_RESPONSE_NOT_RECEIVED:LX/95K;

    aput-object v1, v0, v6

    sget-object v1, LX/95K;->UPDATE_SNAPSHOT_DB_WITH_SERVER_ENTRIES:LX/95K;

    aput-object v1, v0, v7

    sget-object v1, LX/95K;->START_UPLOAD_CONTACTS:LX/95K;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/95K;->CCU_UPLOAD_SUCCESSS:LX/95K;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/95K;->CCU_UPLOAD_FAIL:LX/95K;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/95K;->SNAPSHOT_AND_IMPORT_ID_DELETED:LX/95K;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/95K;->TURN_OFF_CCU_AFTER_EXCEPTION:LX/95K;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/95K;->OVERALL_UPLOAD_START:LX/95K;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/95K;->OVERALL_UPLOAD_FINISH:LX/95K;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/95K;->COMPUTE_DELTA_AND_UPLOAD:LX/95K;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/95K;->BATCH_UPLOAD_START:LX/95K;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/95K;->BATCH_UPLOAD_FINISH:LX/95K;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/95K;->BATCH_UPLOAD_ATTEMPT_ERROR:LX/95K;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/95K;->BATCH_UPLOAD_FAIL:LX/95K;

    aput-object v2, v0, v1

    sput-object v0, LX/95K;->$VALUES:[LX/95K;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1436759
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1436760
    iput-object p3, p0, LX/95K;->mEventName:Ljava/lang/String;

    .line 1436761
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/95K;
    .locals 1

    .prologue
    .line 1436762
    const-class v0, LX/95K;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/95K;

    return-object v0
.end method

.method public static values()[LX/95K;
    .locals 1

    .prologue
    .line 1436763
    sget-object v0, LX/95K;->$VALUES:[LX/95K;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/95K;

    return-object v0
.end method


# virtual methods
.method public final getEventName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1436764
    iget-object v0, p0, LX/95K;->mEventName:Ljava/lang/String;

    return-object v0
.end method
