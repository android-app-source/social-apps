.class public final LX/999;
.super Landroid/graphics/drawable/Drawable$ConstantState;
.source ""


# instance fields
.field public a:Landroid/graphics/Paint;

.field public final b:LX/99D;


# direct methods
.method public constructor <init>(ILX/1HI;Ljava/util/concurrent/Executor;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/1HI;",
            "Ljava/util/concurrent/Executor;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1447014
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable$ConstantState;-><init>()V

    .line 1447015
    new-instance v0, LX/99D;

    invoke-direct {v0, p1, p2, p3, p4}, LX/99D;-><init>(ILX/1HI;Ljava/util/concurrent/Executor;LX/0Ot;)V

    iput-object v0, p0, LX/999;->b:LX/99D;

    .line 1447016
    return-void
.end method

.method public constructor <init>(LX/999;)V
    .locals 2

    .prologue
    .line 1447009
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable$ConstantState;-><init>()V

    .line 1447010
    iget-object v0, p1, LX/999;->b:LX/99D;

    iput-object v0, p0, LX/999;->b:LX/99D;

    .line 1447011
    iget-object v0, p1, LX/999;->a:Landroid/graphics/Paint;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/graphics/Paint;

    iget-object v1, p1, LX/999;->a:Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    :goto_0
    iput-object v0, p0, LX/999;->a:Landroid/graphics/Paint;

    .line 1447012
    return-void

    .line 1447013
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 1447006
    iget-object v0, p0, LX/999;->b:LX/99D;

    iget-object v0, v0, LX/99D;->l:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1447017
    iget-object v0, p0, LX/999;->b:LX/99D;

    iget v0, v0, LX/99D;->a:I

    return v0
.end method

.method public final getChangingConfigurations()I
    .locals 1

    .prologue
    .line 1447008
    const/4 v0, 0x0

    return v0
.end method

.method public final newDrawable()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 1447007
    new-instance v0, Lcom/facebook/fbui/drawable/NetworkDrawable;

    invoke-direct {v0, p0}, Lcom/facebook/fbui/drawable/NetworkDrawable;-><init>(LX/999;)V

    return-object v0
.end method
