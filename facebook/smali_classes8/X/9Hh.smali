.class public final LX/9Hh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8qq;


# instance fields
.field public final synthetic a:LX/9FA;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLComment;

.field public final synthetic c:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public final synthetic d:Z

.field public final synthetic e:LX/9FG;

.field public final synthetic f:Lcom/facebook/graphql/model/GraphQLComment;

.field public final synthetic g:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public final synthetic h:LX/9Hj;


# direct methods
.method public constructor <init>(LX/9Hj;LX/9FA;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;ZLX/9FG;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 0

    .prologue
    .line 1461724
    iput-object p1, p0, LX/9Hh;->h:LX/9Hj;

    iput-object p2, p0, LX/9Hh;->a:LX/9FA;

    iput-object p3, p0, LX/9Hh;->b:Lcom/facebook/graphql/model/GraphQLComment;

    iput-object p4, p0, LX/9Hh;->c:Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-boolean p5, p0, LX/9Hh;->d:Z

    iput-object p6, p0, LX/9Hh;->e:LX/9FG;

    iput-object p7, p0, LX/9Hh;->f:Lcom/facebook/graphql/model/GraphQLComment;

    iput-object p8, p0, LX/9Hh;->g:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1461740
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1461738
    iget-object v0, p0, LX/9Hh;->a:LX/9FA;

    iget-object v1, p0, LX/9Hh;->b:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v0, v1}, LX/9FA;->b(Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 1461739
    return-void
.end method

.method public final a(Landroid/view/View;LX/1zt;)V
    .locals 0

    .prologue
    .line 1461737
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1461735
    iget-object v0, p0, LX/9Hh;->a:LX/9FA;

    iget-object v1, p0, LX/9Hh;->b:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v0, v1, p2, p3}, LX/9FA;->a(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;Ljava/lang/String;)V

    .line 1461736
    return-void
.end method

.method public final b(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1461733
    iget-object v0, p0, LX/9Hh;->a:LX/9FA;

    iget-object v1, p0, LX/9Hh;->b:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v2, p0, LX/9Hh;->c:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0, v1, v2}, LX/9FA;->a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1461734
    return-void
.end method

.method public final c(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1461731
    iget-object v0, p0, LX/9Hh;->a:LX/9FA;

    iget-object v1, p0, LX/9Hh;->b:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v0, v1}, LX/9FA;->c(Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 1461732
    return-void
.end method

.method public final d(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 1461727
    iget-boolean v0, p0, LX/9Hh;->d:Z

    if-eqz v0, :cond_0

    .line 1461728
    iget-object v0, p0, LX/9Hh;->e:LX/9FG;

    invoke-virtual {v0}, LX/9FG;->h()V

    .line 1461729
    :cond_0
    iget-object v0, p0, LX/9Hh;->a:LX/9FA;

    iget-object v1, p0, LX/9Hh;->b:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v2, p0, LX/9Hh;->f:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v3, p0, LX/9Hh;->g:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0, v1, v2, v3}, LX/9FA;->a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1461730
    return-void
.end method

.method public final e(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1461725
    iget-object v0, p0, LX/9Hh;->a:LX/9FA;

    iget-object v1, p0, LX/9Hh;->b:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v0, v1}, LX/9FA;->a(Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 1461726
    return-void
.end method
