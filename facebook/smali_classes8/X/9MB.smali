.class public final LX/9MB;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 1472850
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 1472851
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1472852
    :goto_0
    return v1

    .line 1472853
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1472854
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_6

    .line 1472855
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1472856
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1472857
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_1

    if-eqz v6, :cond_1

    .line 1472858
    const-string v7, "id"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1472859
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 1472860
    :cond_2
    const-string v7, "image"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1472861
    invoke-static {p0, p1}, LX/9MA;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1472862
    :cond_3
    const-string v7, "topic_name"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1472863
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 1472864
    :cond_4
    const-string v7, "topic_stories"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1472865
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1472866
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v8, :cond_c

    .line 1472867
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1472868
    :goto_2
    move v2, v6

    .line 1472869
    goto :goto_1

    .line 1472870
    :cond_5
    const-string v7, "viewer_topic_subscription_state"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1472871
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLGroupPostTopicSubscriptionState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupPostTopicSubscriptionState;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    goto :goto_1

    .line 1472872
    :cond_6
    const/4 v6, 0x5

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1472873
    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1472874
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1472875
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1472876
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1472877
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1472878
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_7
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto/16 :goto_1

    .line 1472879
    :cond_8
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_a

    .line 1472880
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1472881
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1472882
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_8

    if-eqz v9, :cond_8

    .line 1472883
    const-string v10, "count"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 1472884
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v2

    move v8, v2

    move v2, v7

    goto :goto_3

    .line 1472885
    :cond_9
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 1472886
    :cond_a
    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1472887
    if-eqz v2, :cond_b

    .line 1472888
    invoke-virtual {p1, v6, v8, v6}, LX/186;->a(III)V

    .line 1472889
    :cond_b
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto :goto_2

    :cond_c
    move v2, v6

    move v8, v6

    goto :goto_3
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v2, 0x4

    .line 1472890
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1472891
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1472892
    if-eqz v0, :cond_0

    .line 1472893
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1472894
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1472895
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1472896
    if-eqz v0, :cond_4

    .line 1472897
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1472898
    const/4 p3, 0x0

    .line 1472899
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1472900
    invoke-virtual {p0, v0, p3, p3}, LX/15i;->a(III)I

    move-result v1

    .line 1472901
    if-eqz v1, :cond_1

    .line 1472902
    const-string v3, "height"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1472903
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 1472904
    :cond_1
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1472905
    if-eqz v1, :cond_2

    .line 1472906
    const-string v3, "uri"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1472907
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1472908
    :cond_2
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1, p3}, LX/15i;->a(III)I

    move-result v1

    .line 1472909
    if-eqz v1, :cond_3

    .line 1472910
    const-string v3, "width"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1472911
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 1472912
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1472913
    :cond_4
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1472914
    if-eqz v0, :cond_5

    .line 1472915
    const-string v1, "topic_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1472916
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1472917
    :cond_5
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1472918
    if-eqz v0, :cond_7

    .line 1472919
    const-string v1, "topic_stories"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1472920
    const/4 v1, 0x0

    .line 1472921
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1472922
    invoke-virtual {p0, v0, v1, v1}, LX/15i;->a(III)I

    move-result v1

    .line 1472923
    if-eqz v1, :cond_6

    .line 1472924
    const-string v3, "count"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1472925
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 1472926
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1472927
    :cond_7
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1472928
    if-eqz v0, :cond_8

    .line 1472929
    const-string v0, "viewer_topic_subscription_state"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1472930
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1472931
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1472932
    return-void
.end method
