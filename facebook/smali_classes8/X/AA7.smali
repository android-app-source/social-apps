.class public final LX/AA7;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 1636985
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1636986
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1636987
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1636988
    invoke-static {p0, p1}, LX/AA7;->b(LX/15w;LX/186;)I

    move-result v1

    .line 1636989
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1636990
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const/4 v3, 0x6

    const-wide/16 v4, 0x0

    .line 1636930
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1636931
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1636932
    if-eqz v0, :cond_0

    .line 1636933
    const-string v1, "address"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636934
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1636935
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1636936
    if-eqz v0, :cond_1

    .line 1636937
    const-string v1, "country_code"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636938
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1636939
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1636940
    if-eqz v0, :cond_2

    .line 1636941
    const-string v1, "display_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636942
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1636943
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1636944
    if-eqz v0, :cond_3

    .line 1636945
    const-string v1, "distance_unit"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636946
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1636947
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1636948
    if-eqz v0, :cond_4

    .line 1636949
    const-string v1, "key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636950
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1636951
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1636952
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_5

    .line 1636953
    const-string v2, "latitude"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636954
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1636955
    :cond_5
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1636956
    if-eqz v0, :cond_6

    .line 1636957
    const-string v0, "location_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636958
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1636959
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1636960
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_7

    .line 1636961
    const-string v2, "longitude"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636962
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1636963
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1636964
    if-eqz v0, :cond_8

    .line 1636965
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636966
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1636967
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1636968
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_9

    .line 1636969
    const-string v2, "radius"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636970
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1636971
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1636972
    if-eqz v0, :cond_a

    .line 1636973
    const-string v1, "region_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636974
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1636975
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1636976
    if-eqz v0, :cond_b

    .line 1636977
    const-string v1, "supports_city"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636978
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1636979
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1636980
    if-eqz v0, :cond_c

    .line 1636981
    const-string v1, "supports_region"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636982
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1636983
    :cond_c
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1636984
    return-void
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1637069
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1637070
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1637071
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2}, LX/AA7;->a(LX/15i;ILX/0nX;)V

    .line 1637072
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1637073
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1637074
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 29

    .prologue
    .line 1636991
    const/16 v22, 0x0

    .line 1636992
    const/16 v21, 0x0

    .line 1636993
    const/16 v20, 0x0

    .line 1636994
    const/16 v19, 0x0

    .line 1636995
    const/16 v18, 0x0

    .line 1636996
    const-wide/16 v16, 0x0

    .line 1636997
    const/4 v13, 0x0

    .line 1636998
    const-wide/16 v14, 0x0

    .line 1636999
    const/4 v12, 0x0

    .line 1637000
    const-wide/16 v10, 0x0

    .line 1637001
    const/4 v9, 0x0

    .line 1637002
    const/4 v8, 0x0

    .line 1637003
    const/4 v7, 0x0

    .line 1637004
    const/4 v6, 0x0

    .line 1637005
    const/4 v5, 0x0

    .line 1637006
    const/4 v4, 0x0

    .line 1637007
    const/4 v3, 0x0

    .line 1637008
    const/4 v2, 0x0

    .line 1637009
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v23

    sget-object v24, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-eq v0, v1, :cond_14

    .line 1637010
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1637011
    const/4 v2, 0x0

    .line 1637012
    :goto_0
    return v2

    .line 1637013
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_e

    .line 1637014
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1637015
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1637016
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 1637017
    const-string v6, "address"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1637018
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v25, v2

    goto :goto_1

    .line 1637019
    :cond_1
    const-string v6, "country_code"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1637020
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v24, v2

    goto :goto_1

    .line 1637021
    :cond_2
    const-string v6, "display_name"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1637022
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v23, v2

    goto :goto_1

    .line 1637023
    :cond_3
    const-string v6, "distance_unit"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1637024
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v22, v2

    goto :goto_1

    .line 1637025
    :cond_4
    const-string v6, "key"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1637026
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v21, v2

    goto/16 :goto_1

    .line 1637027
    :cond_5
    const-string v6, "latitude"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1637028
    const/4 v2, 0x1

    .line 1637029
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 1637030
    :cond_6
    const-string v6, "location_type"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1637031
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 1637032
    :cond_7
    const-string v6, "longitude"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1637033
    const/4 v2, 0x1

    .line 1637034
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v11, v2

    move-wide/from16 v18, v6

    goto/16 :goto_1

    .line 1637035
    :cond_8
    const-string v6, "name"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 1637036
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 1637037
    :cond_9
    const-string v6, "radius"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1637038
    const/4 v2, 0x1

    .line 1637039
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v10, v2

    move-wide/from16 v16, v6

    goto/16 :goto_1

    .line 1637040
    :cond_a
    const-string v6, "region_key"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 1637041
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 1637042
    :cond_b
    const-string v6, "supports_city"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 1637043
    const/4 v2, 0x1

    .line 1637044
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v9, v2

    move v13, v6

    goto/16 :goto_1

    .line 1637045
    :cond_c
    const-string v6, "supports_region"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1637046
    const/4 v2, 0x1

    .line 1637047
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v8, v2

    move v12, v6

    goto/16 :goto_1

    .line 1637048
    :cond_d
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1637049
    :cond_e
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1637050
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1637051
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1637052
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1637053
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1637054
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1637055
    if-eqz v3, :cond_f

    .line 1637056
    const/4 v3, 0x5

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1637057
    :cond_f
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1637058
    if-eqz v11, :cond_10

    .line 1637059
    const/4 v3, 0x7

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v18

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1637060
    :cond_10
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1637061
    if-eqz v10, :cond_11

    .line 1637062
    const/16 v3, 0x9

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v16

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1637063
    :cond_11
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1637064
    if-eqz v9, :cond_12

    .line 1637065
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->a(IZ)V

    .line 1637066
    :cond_12
    if-eqz v8, :cond_13

    .line 1637067
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->a(IZ)V

    .line 1637068
    :cond_13
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_14
    move/from16 v23, v20

    move/from16 v24, v21

    move/from16 v25, v22

    move/from16 v20, v13

    move/from16 v21, v18

    move/from16 v22, v19

    move v13, v8

    move-wide/from16 v18, v14

    move v15, v12

    move v14, v9

    move v8, v2

    move v12, v7

    move v9, v3

    move v3, v6

    move/from16 v26, v5

    move-wide/from16 v27, v10

    move/from16 v11, v26

    move v10, v4

    move-wide/from16 v4, v16

    move-wide/from16 v16, v27

    goto/16 :goto_1
.end method
