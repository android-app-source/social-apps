.class public final LX/AAU;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1637782
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_a

    .line 1637783
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1637784
    :goto_0
    return v1

    .line 1637785
    :cond_0
    const-string v11, "max_age"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 1637786
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v5, v3

    move v3, v2

    .line 1637787
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_7

    .line 1637788
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1637789
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1637790
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 1637791
    const-string v11, "flexible_spec"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 1637792
    invoke-static {p0, p1}, LX/AAR;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 1637793
    :cond_2
    const-string v11, "genders"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1637794
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1637795
    :cond_3
    const-string v11, "geo_locations"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 1637796
    invoke-static {p0, p1}, LX/AAS;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1637797
    :cond_4
    const-string v11, "interests"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1637798
    invoke-static {p0, p1}, LX/AAT;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1637799
    :cond_5
    const-string v11, "min_age"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 1637800
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 1637801
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1637802
    :cond_7
    const/4 v10, 0x6

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1637803
    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 1637804
    invoke-virtual {p1, v2, v8}, LX/186;->b(II)V

    .line 1637805
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v7}, LX/186;->b(II)V

    .line 1637806
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v6}, LX/186;->b(II)V

    .line 1637807
    if-eqz v3, :cond_8

    .line 1637808
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v5, v1}, LX/186;->a(III)V

    .line 1637809
    :cond_8
    if-eqz v0, :cond_9

    .line 1637810
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4, v1}, LX/186;->a(III)V

    .line 1637811
    :cond_9
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_a
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1637812
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1637813
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1637814
    if-eqz v0, :cond_5

    .line 1637815
    const-string v1, "flexible_spec"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1637816
    const/4 v1, 0x0

    .line 1637817
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1637818
    invoke-virtual {p0, v0, v1, v1}, LX/15i;->a(III)I

    move-result v1

    .line 1637819
    if-eqz v1, :cond_0

    .line 1637820
    const-string v4, "count"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1637821
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 1637822
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1637823
    if-eqz v1, :cond_4

    .line 1637824
    const-string v4, "nodes"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1637825
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1637826
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v5

    if-ge v4, v5, :cond_3

    .line 1637827
    invoke-virtual {p0, v1, v4}, LX/15i;->q(II)I

    move-result v5

    .line 1637828
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1637829
    const/4 v6, 0x0

    invoke-virtual {p0, v5, v6}, LX/15i;->g(II)I

    move-result v6

    .line 1637830
    if-eqz v6, :cond_2

    .line 1637831
    const-string v0, "detailed_targeting_items"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1637832
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1637833
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p0, v6}, LX/15i;->c(I)I

    move-result v5

    if-ge v0, v5, :cond_1

    .line 1637834
    invoke-virtual {p0, v6, v0}, LX/15i;->q(II)I

    move-result v5

    invoke-static {p0, v5, p2}, LX/AA3;->a(LX/15i;ILX/0nX;)V

    .line 1637835
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1637836
    :cond_1
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1637837
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1637838
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1637839
    :cond_3
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1637840
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1637841
    :cond_5
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1637842
    if-eqz v0, :cond_6

    .line 1637843
    const-string v0, "genders"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1637844
    invoke-virtual {p0, p1, v3}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1637845
    :cond_6
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1637846
    if-eqz v0, :cond_7

    .line 1637847
    const-string v1, "geo_locations"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1637848
    invoke-static {p0, v0, p2, p3}, LX/AAS;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1637849
    :cond_7
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1637850
    if-eqz v0, :cond_8

    .line 1637851
    const-string v1, "interests"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1637852
    invoke-static {p0, v0, p2, p3}, LX/AAT;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1637853
    :cond_8
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1637854
    if-eqz v0, :cond_9

    .line 1637855
    const-string v1, "max_age"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1637856
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1637857
    :cond_9
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1637858
    if-eqz v0, :cond_a

    .line 1637859
    const-string v1, "min_age"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1637860
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1637861
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1637862
    return-void
.end method
