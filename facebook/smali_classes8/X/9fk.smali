.class public LX/9fk;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/view/View$OnClickListener;

.field public final b:LX/9ea;

.field public final c:LX/9f2;

.field private d:Landroid/widget/ImageButton;

.field public e:Z

.field private f:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

.field private g:Lcom/facebook/resources/ui/FbTextView;

.field private h:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;LX/9f2;LX/9ea;Lcom/facebook/ui/titlebar/Fb4aTitleBar;Landroid/widget/ImageButton;Lcom/facebook/resources/ui/FbTextView;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1522591
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1522592
    new-instance v0, LX/9fj;

    invoke-direct {v0, p0}, LX/9fj;-><init>(LX/9fk;)V

    iput-object v0, p0, LX/9fk;->a:Landroid/view/View$OnClickListener;

    .line 1522593
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1522594
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1522595
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1522596
    iput-object p1, p0, LX/9fk;->h:Landroid/content/Context;

    .line 1522597
    iput-object p3, p0, LX/9fk;->c:LX/9f2;

    .line 1522598
    iput-object p4, p0, LX/9fk;->b:LX/9ea;

    .line 1522599
    iget-object v0, p0, LX/9fk;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1522600
    invoke-virtual {p2, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1522601
    iput-object p5, p0, LX/9fk;->f:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 1522602
    iput-object p6, p0, LX/9fk;->d:Landroid/widget/ImageButton;

    .line 1522603
    iput-object p7, p0, LX/9fk;->g:Lcom/facebook/resources/ui/FbTextView;

    .line 1522604
    iget-object v0, p0, LX/9fk;->d:Landroid/widget/ImageButton;

    iget-object v1, p0, LX/9fk;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1522605
    invoke-virtual {p0, v2}, LX/9fk;->a(Z)V

    .line 1522606
    return-void
.end method


# virtual methods
.method public final a()LX/5Rr;
    .locals 1

    .prologue
    .line 1522617
    iget-object v0, p0, LX/9fk;->b:LX/9ea;

    invoke-interface {v0}, LX/9ea;->j()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5Rr;

    return-object v0
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 1522608
    if-eqz p1, :cond_0

    .line 1522609
    iget-object v0, p0, LX/9fk;->d:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->clearColorFilter()V

    .line 1522610
    iget-object v0, p0, LX/9fk;->g:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/9fk;->h:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1522611
    iget-object v0, p0, LX/9fk;->f:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    iget-object v1, p0, LX/9fk;->b:LX/9ea;

    invoke-interface {v1}, LX/9ea;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(Ljava/lang/String;)V

    .line 1522612
    :goto_0
    iput-boolean p1, p0, LX/9fk;->e:Z

    .line 1522613
    return-void

    .line 1522614
    :cond_0
    iget-object v0, p0, LX/9fk;->d:Landroid/widget/ImageButton;

    iget-object v1, p0, LX/9fk;->h:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a04f6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setColorFilter(I)V

    .line 1522615
    iget-object v0, p0, LX/9fk;->g:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/9fk;->h:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a04f6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1522616
    iget-object v0, p0, LX/9fk;->b:LX/9ea;

    invoke-interface {v0}, LX/9ea;->b()V

    goto :goto_0
.end method

.method public final b()LX/9ea;
    .locals 1

    .prologue
    .line 1522607
    iget-object v0, p0, LX/9fk;->b:LX/9ea;

    return-object v0
.end method
