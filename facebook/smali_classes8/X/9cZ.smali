.class public LX/9cZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Lc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3Lc",
        "<",
        "LX/9cX;",
        "LX/9cY;",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LX/0aG;

.field public b:LX/0SI;

.field public c:Ljava/util/concurrent/Executor;

.field public d:LX/3dt;

.field public e:LX/3Mb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3Mb",
            "<",
            "LX/9cX;",
            "LX/9cY;",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/3dt;LX/0aG;LX/0SI;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p4    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1516570
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1516571
    iput-object p1, p0, LX/9cZ;->d:LX/3dt;

    .line 1516572
    iput-object p2, p0, LX/9cZ;->a:LX/0aG;

    .line 1516573
    iput-object p3, p0, LX/9cZ;->b:LX/0SI;

    .line 1516574
    iput-object p4, p0, LX/9cZ;->c:Ljava/util/concurrent/Executor;

    .line 1516575
    return-void
.end method

.method public static a(LX/9cZ;LX/9cX;LX/3do;)LX/1ML;
    .locals 4

    .prologue
    .line 1516576
    new-instance v0, LX/3ea;

    .line 1516577
    iget-boolean v1, p1, LX/9cX;->a:Z

    if-eqz v1, :cond_0

    .line 1516578
    sget-object v1, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    .line 1516579
    :goto_0
    move-object v1, v1

    .line 1516580
    invoke-direct {v0, p2, v1}, LX/3ea;-><init>(LX/3do;LX/0rS;)V

    const-string v1, "COMPOSER"

    .line 1516581
    iput-object v1, v0, LX/3ea;->c:Ljava/lang/String;

    .line 1516582
    move-object v0, v0

    .line 1516583
    invoke-virtual {v0}, LX/3ea;->a()Lcom/facebook/stickers/service/FetchStickerPacksParams;

    move-result-object v0

    .line 1516584
    iget-object v1, p0, LX/9cZ;->a:LX/0aG;

    const-string v2, "fetch_sticker_packs"

    .line 1516585
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1516586
    const-string p1, "fetchStickerPacksParams"

    invoke-virtual {v3, p1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1516587
    const-string p1, "overridden_viewer_context"

    iget-object p2, p0, LX/9cZ;->b:LX/0SI;

    invoke-interface {p2}, LX/0SI;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object p2

    invoke-virtual {v3, p1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1516588
    move-object v0, v3

    .line 1516589
    const v3, 0x688c4cdd

    invoke-static {v1, v2, v0, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 1516590
    return-object v0

    :cond_0
    sget-object v1, LX/0rS;->PREFER_CACHE_IF_UP_TO_DATE:LX/0rS;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/9cX;)V
    .locals 8

    .prologue
    .line 1516591
    const/4 v0, 0x0

    .line 1516592
    const/4 v3, 0x0

    .line 1516593
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v4

    .line 1516594
    iget-object v1, p0, LX/9cZ;->d:LX/3dt;

    sget-object v2, LX/3do;->DOWNLOADED_PACKS:LX/3do;

    invoke-virtual {v1, v2}, LX/3dt;->a(LX/3do;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1516595
    iget-object v1, p0, LX/9cZ;->d:LX/3dt;

    sget-object v2, LX/3do;->DOWNLOADED_PACKS:LX/3do;

    invoke-virtual {v1, v2}, LX/3dt;->b(LX/3do;)LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v2, v3

    .line 1516596
    :goto_0
    if-ge v2, v6, :cond_1

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/stickers/model/StickerPack;

    .line 1516597
    iget-object v7, v1, Lcom/facebook/stickers/model/StickerPack;->r:Lcom/facebook/stickers/model/StickerCapabilities;

    move-object v7, v7

    .line 1516598
    iget-object v7, v7, Lcom/facebook/stickers/model/StickerCapabilities;->b:LX/03R;

    invoke-virtual {v7, v3}, LX/03R;->asBoolean(Z)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1516599
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1516600
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1516601
    :cond_1
    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    move-object v1, v1

    .line 1516602
    iget-object v2, p0, LX/9cZ;->e:LX/3Mb;

    if-nez v2, :cond_2

    .line 1516603
    :goto_1
    return-void

    .line 1516604
    :cond_2
    if-eqz v1, :cond_3

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    if-lez v2, :cond_3

    iget-boolean v2, p1, LX/9cX;->a:Z

    if-nez v2, :cond_3

    .line 1516605
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 1516606
    invoke-interface {v2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1516607
    :goto_2
    if-nez v0, :cond_4

    .line 1516608
    iget-object v0, p0, LX/9cZ;->e:LX/3Mb;

    new-instance v2, LX/9cY;

    invoke-direct {v2, v1}, LX/9cY;-><init>(LX/0Px;)V

    invoke-interface {v0, p1, v2}, LX/3Mb;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 1516609
    :cond_3
    const/4 v0, 0x1

    goto :goto_2

    .line 1516610
    :cond_4
    iget-object v0, p0, LX/9cZ;->e:LX/3Mb;

    .line 1516611
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v1

    .line 1516612
    sget-object v2, LX/3do;->DOWNLOADED_PACKS:LX/3do;

    invoke-static {p0, p1, v2}, LX/9cZ;->a(LX/9cZ;LX/9cX;LX/3do;)LX/1ML;

    move-result-object v2

    .line 1516613
    new-instance v3, LX/9cW;

    invoke-direct {v3, p0, p1, v1}, LX/9cW;-><init>(LX/9cZ;LX/9cX;Lcom/google/common/util/concurrent/SettableFuture;)V

    iget-object v4, p0, LX/9cZ;->c:Ljava/util/concurrent/Executor;

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1516614
    move-object v1, v1

    .line 1516615
    invoke-interface {v0, p1, v1}, LX/3Mb;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;)V

    goto :goto_1
.end method
