.class public final LX/98r;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:I

.field public e:Z

.field public f:Landroid/os/Bundle;

.field public g:Ljava/lang/String;

.field public h:I

.field public i:Ljava/lang/String;

.field public j:I

.field public k:I

.field public l:Ljava/lang/String;

.field public m:Z

.field public n:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1446420
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1446421
    const-string v0, "unknown"

    iput-object v0, p0, LX/98r;->g:Ljava/lang/String;

    .line 1446422
    const/4 v0, -0x1

    iput v0, p0, LX/98r;->h:I

    .line 1446423
    const-string v0, "MarketplaceSearch"

    iput-object v0, p0, LX/98r;->n:Ljava/lang/String;

    .line 1446424
    return-void
.end method


# virtual methods
.method public final a()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 1446425
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1446426
    iget-object v1, p0, LX/98r;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1446427
    const-string v1, "uri"

    iget-object v2, p0, LX/98r;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1446428
    :cond_0
    iget-object v1, p0, LX/98r;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1446429
    const-string v1, "route_name"

    iget-object v2, p0, LX/98r;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1446430
    :cond_1
    iget-object v1, p0, LX/98r;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1446431
    const-string v1, "module_name"

    iget-object v2, p0, LX/98r;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1446432
    :cond_2
    const-string v1, "title_res"

    iget v2, p0, LX/98r;->d:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1446433
    const-string v1, "show_search"

    iget-boolean v2, p0, LX/98r;->e:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1446434
    const-string v1, "init_props"

    iget-object v2, p0, LX/98r;->f:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1446435
    const-string v1, "analytics_tag"

    iget-object v2, p0, LX/98r;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1446436
    const-string v1, "requested_orientation"

    iget v2, p0, LX/98r;->h:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1446437
    const-string v1, "button_event"

    iget-object v2, p0, LX/98r;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1446438
    const-string v1, "button_icon_res"

    iget v2, p0, LX/98r;->j:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1446439
    const-string v1, "button_res"

    iget v2, p0, LX/98r;->k:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1446440
    const-string v1, "button_text"

    iget-object v2, p0, LX/98r;->l:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1446441
    const-string v1, "non_immersive"

    iget-boolean v2, p0, LX/98r;->m:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1446442
    const-string v1, "react_search_module"

    iget-object v2, p0, LX/98r;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1446443
    return-object v0
.end method
