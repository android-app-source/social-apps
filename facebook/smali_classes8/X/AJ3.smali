.class public LX/AJ3;
.super LX/1OM;
.source ""

# interfaces
.implements LX/1OO;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;",
        "LX/1OO",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/AIt;

.field public final b:LX/AJ8;

.field public final c:LX/AJ2;

.field public d:Lcom/facebook/privacy/model/SelectablePrivacyData;

.field public e:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/AJ2;LX/AJ8;LX/AIt;LX/0Or;)V
    .locals 0
    .param p1    # LX/AJ2;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/AJ8;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/AJ2;",
            "Lcom/facebook/audience/sharesheet/adapter/SharesheetRecyclerViewAdapter$AudienceStateListener;",
            "LX/AIt;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1660609
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1660610
    iput-object p1, p0, LX/AJ3;->c:LX/AJ2;

    .line 1660611
    iput-object p2, p0, LX/AJ3;->b:LX/AJ8;

    .line 1660612
    iput-object p3, p0, LX/AJ3;->a:LX/AIt;

    .line 1660613
    iget-object p1, p0, LX/AJ3;->c:LX/AJ2;

    sget-object p2, LX/AJ2;->MY_DAY:LX/AJ2;

    if-ne p1, p2, :cond_0

    invoke-interface {p4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 1660614
    invoke-interface {p4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/facebook/user/model/User;

    .line 1660615
    invoke-virtual {p1}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, LX/AJ3;->e:Ljava/lang/String;

    .line 1660616
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1660599
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1660600
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1660601
    sget-object v1, LX/AJ1;->a:[I

    iget-object v2, p0, LX/AJ3;->c:LX/AJ2;

    invoke-virtual {v2}, LX/AJ2;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1660602
    const v1, 0x7f031315

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1660603
    new-instance v0, Lcom/facebook/audience/sharesheet/adapter/MyDayViewHolder;

    invoke-direct {v0, v1}, Lcom/facebook/audience/sharesheet/adapter/MyDayViewHolder;-><init>(Landroid/view/View;)V

    :goto_0
    return-object v0

    .line 1660604
    :pswitch_0
    const v1, 0x7f031317

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1660605
    iget-object v1, p0, LX/AJ3;->a:LX/AIt;

    .line 1660606
    new-instance v3, LX/AIs;

    invoke-static {v1}, LX/0fO;->b(LX/0QB;)LX/0fO;

    move-result-object v2

    check-cast v2, LX/0fO;

    invoke-direct {v3, v2, v0}, LX/AIs;-><init>(LX/0fO;Landroid/view/View;)V

    .line 1660607
    move-object v0, v3

    .line 1660608
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(LX/1a1;I)V
    .locals 3

    .prologue
    .line 1660592
    instance-of v0, p1, LX/AIs;

    if-eqz v0, :cond_1

    .line 1660593
    check-cast p1, LX/AIs;

    .line 1660594
    iget-object v0, p0, LX/AJ3;->c:LX/AJ2;

    iget-object v1, p0, LX/AJ3;->b:LX/AJ8;

    iget-object v2, p0, LX/AJ3;->d:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-virtual {p1, v0, v1, v2}, LX/AIs;->a(LX/AJ2;LX/AJ8;Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    .line 1660595
    :cond_0
    :goto_0
    return-void

    .line 1660596
    :cond_1
    instance-of v0, p1, Lcom/facebook/audience/sharesheet/adapter/MyDayViewHolder;

    if-eqz v0, :cond_0

    .line 1660597
    check-cast p1, Lcom/facebook/audience/sharesheet/adapter/MyDayViewHolder;

    .line 1660598
    iget-object v1, p0, LX/AJ3;->c:LX/AJ2;

    iget-object v2, p0, LX/AJ3;->b:LX/AJ8;

    iget-object v0, p0, LX/AJ3;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/AJ3;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_1
    invoke-virtual {p1, v1, v2, v0}, Lcom/facebook/audience/sharesheet/adapter/MyDayViewHolder;->a(LX/AJ2;LX/AJ8;Landroid/net/Uri;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1660617
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1660591
    const/4 v0, 0x0

    return v0
.end method

.method public final ii_()I
    .locals 1

    .prologue
    .line 1660590
    const/4 v0, 0x1

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1660586
    iget-object v0, p0, LX/AJ3;->b:LX/AJ8;

    .line 1660587
    iget-object p0, v0, LX/AJ8;->a:Lcom/facebook/audience/sharesheet/app/SharesheetFragment;

    iget-object p0, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->o:LX/AIu;

    if-eqz p0, :cond_1

    iget-object p0, v0, LX/AJ8;->a:Lcom/facebook/audience/sharesheet/app/SharesheetFragment;

    iget-object p0, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->o:LX/AIu;

    invoke-virtual {p0}, LX/AIu;->e()Z

    move-result p0

    if-eqz p0, :cond_1

    const/4 p0, 0x1

    :goto_0
    move v0, p0

    .line 1660588
    move v0, v0

    .line 1660589
    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method
