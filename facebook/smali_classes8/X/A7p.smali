.class public LX/A7p;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/UniversalFeedbackType;
    .end annotation
.end field

.field public final b:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/UniversalFeedbackDelivery;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/UniversalFeedbackType;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/UniversalFeedbackDelivery;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1626749
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1626750
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1626751
    if-eqz p2, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1626752
    iput-object p1, p0, LX/A7p;->a:Ljava/lang/String;

    .line 1626753
    iput-object p2, p0, LX/A7p;->b:Ljava/lang/String;

    .line 1626754
    return-void

    :cond_0
    move v0, v2

    .line 1626755
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1626756
    goto :goto_1
.end method
