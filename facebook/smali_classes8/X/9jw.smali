.class public final LX/9jw;
.super LX/0aT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0aT",
        "<",
        "LX/9jv;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/9jw;


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/9jv;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1530589
    invoke-direct {p0, p1}, LX/0aT;-><init>(LX/0Ot;)V

    .line 1530590
    return-void
.end method

.method public static a(LX/0QB;)LX/9jw;
    .locals 4

    .prologue
    .line 1530591
    sget-object v0, LX/9jw;->a:LX/9jw;

    if-nez v0, :cond_1

    .line 1530592
    const-class v1, LX/9jw;

    monitor-enter v1

    .line 1530593
    :try_start_0
    sget-object v0, LX/9jw;->a:LX/9jw;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1530594
    if-eqz v2, :cond_0

    .line 1530595
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1530596
    new-instance v3, LX/9jw;

    const/16 p0, 0x2f35

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/9jw;-><init>(LX/0Ot;)V

    .line 1530597
    move-object v0, v3

    .line 1530598
    sput-object v0, LX/9jw;->a:LX/9jw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1530599
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1530600
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1530601
    :cond_1
    sget-object v0, LX/9jw;->a:LX/9jw;

    return-object v0

    .line 1530602
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1530603
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1530604
    check-cast p3, LX/9jv;

    .line 1530605
    iget-object v0, p3, LX/9jv;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9jS;

    .line 1530606
    const-string p0, "graphql_story"

    invoke-static {p2, p0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1530607
    const-string p1, "extra_result"

    invoke-virtual {p2, p1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, LX/7m7;->valueOf(Ljava/lang/String;)LX/7m7;

    move-result-object p1

    .line 1530608
    sget-object p3, LX/7m7;->SUCCESS:LX/7m7;

    if-ne p3, p1, :cond_0

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->X()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 1530609
    iget-object p0, v0, LX/9jS;->a:LX/9jQ;

    .line 1530610
    iget-object p1, p0, LX/9jQ;->b:LX/0TD;

    new-instance p3, LX/9jP;

    invoke-direct {p3, p0}, LX/9jP;-><init>(LX/9jQ;)V

    invoke-interface {p1, p3}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1530611
    :cond_0
    return-void
.end method
