.class public final LX/9qG;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1546622
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_a

    .line 1546623
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1546624
    :goto_0
    return v1

    .line 1546625
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_5

    .line 1546626
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1546627
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1546628
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_0

    if-eqz v10, :cond_0

    .line 1546629
    const-string v11, "bottom"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 1546630
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v5

    move v9, v5

    move v5, v2

    goto :goto_1

    .line 1546631
    :cond_1
    const-string v11, "left"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 1546632
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v4

    move v8, v4

    move v4, v2

    goto :goto_1

    .line 1546633
    :cond_2
    const-string v11, "right"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1546634
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v7, v3

    move v3, v2

    goto :goto_1

    .line 1546635
    :cond_3
    const-string v11, "top"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 1546636
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v6, v0

    move v0, v2

    goto :goto_1

    .line 1546637
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1546638
    :cond_5
    const/4 v10, 0x4

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1546639
    if-eqz v5, :cond_6

    .line 1546640
    invoke-virtual {p1, v1, v9}, LX/186;->a(IZ)V

    .line 1546641
    :cond_6
    if-eqz v4, :cond_7

    .line 1546642
    invoke-virtual {p1, v2, v8}, LX/186;->a(IZ)V

    .line 1546643
    :cond_7
    if-eqz v3, :cond_8

    .line 1546644
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v7}, LX/186;->a(IZ)V

    .line 1546645
    :cond_8
    if-eqz v0, :cond_9

    .line 1546646
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->a(IZ)V

    .line 1546647
    :cond_9
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_a
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    .line 1546648
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1546649
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1546650
    if-eqz v0, :cond_0

    .line 1546651
    const-string v1, "bottom"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1546652
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1546653
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1546654
    if-eqz v0, :cond_1

    .line 1546655
    const-string v1, "left"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1546656
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1546657
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1546658
    if-eqz v0, :cond_2

    .line 1546659
    const-string v1, "right"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1546660
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1546661
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1546662
    if-eqz v0, :cond_3

    .line 1546663
    const-string v1, "top"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1546664
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1546665
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1546666
    return-void
.end method
