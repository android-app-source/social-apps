.class public LX/9e5;
.super Landroid/widget/ImageView;
.source ""


# instance fields
.field public a:LX/9e4;

.field public b:Landroid/graphics/Matrix;

.field public c:Landroid/graphics/Matrix;

.field public d:Landroid/graphics/Matrix;

.field public e:Landroid/graphics/Matrix;

.field private f:Landroid/graphics/RectF;

.field public g:Landroid/graphics/RectF;

.field public h:Landroid/graphics/RectF;

.field public i:Landroid/graphics/RectF;

.field public j:Landroid/graphics/RectF;

.field private k:[F

.field private l:[F

.field public m:F

.field public n:F

.field private o:Z

.field public p:F

.field public q:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 1519157
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1519158
    sget-object v0, LX/9e4;->NORMAL:LX/9e4;

    iput-object v0, p0, LX/9e5;->a:LX/9e4;

    .line 1519159
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/9e5;->b:Landroid/graphics/Matrix;

    .line 1519160
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/9e5;->c:Landroid/graphics/Matrix;

    .line 1519161
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/9e5;->d:Landroid/graphics/Matrix;

    .line 1519162
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/9e5;->e:Landroid/graphics/Matrix;

    .line 1519163
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/9e5;->f:Landroid/graphics/RectF;

    .line 1519164
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/9e5;->g:Landroid/graphics/RectF;

    .line 1519165
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/9e5;->h:Landroid/graphics/RectF;

    .line 1519166
    iput-object v1, p0, LX/9e5;->i:Landroid/graphics/RectF;

    .line 1519167
    iput-object v1, p0, LX/9e5;->k:[F

    .line 1519168
    iput-object v1, p0, LX/9e5;->l:[F

    .line 1519169
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, LX/9e5;->m:F

    .line 1519170
    iput v2, p0, LX/9e5;->n:F

    .line 1519171
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/9e5;->o:Z

    .line 1519172
    const/high16 v0, 0x40800000    # 4.0f

    iput v0, p0, LX/9e5;->p:F

    .line 1519173
    iput v2, p0, LX/9e5;->q:F

    .line 1519174
    sget-object v0, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0}, LX/9e5;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 1519175
    return-void
.end method

.method public static b(LX/9e5;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/high16 v5, 0x40000000    # 2.0f

    .line 1519176
    invoke-virtual {p0}, LX/9e5;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1519177
    :goto_0
    return-void

    .line 1519178
    :cond_0
    iget-object v0, p0, LX/9e5;->b:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 1519179
    iget-object v0, p0, LX/9e5;->a:LX/9e4;

    sget-object v1, LX/9e4;->NORMAL:LX/9e4;

    if-eq v0, v1, :cond_1

    .line 1519180
    invoke-virtual {p0}, LX/9e5;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1519181
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    .line 1519182
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    .line 1519183
    iget-object v2, p0, LX/9e5;->b:Landroid/graphics/Matrix;

    neg-int v3, v1

    int-to-float v3, v3

    div-float/2addr v3, v5

    neg-int v4, v0

    int-to-float v4, v4

    div-float/2addr v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1519184
    iget-object v2, p0, LX/9e5;->b:Landroid/graphics/Matrix;

    iget-object v3, p0, LX/9e5;->a:LX/9e4;

    iget v3, v3, LX/9e4;->rotation:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 1519185
    iget-object v2, p0, LX/9e5;->a:LX/9e4;

    invoke-virtual {v2}, LX/9e4;->isRatioChanged()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1519186
    iget-object v2, p0, LX/9e5;->b:Landroid/graphics/Matrix;

    int-to-float v0, v0

    div-float/2addr v0, v5

    int-to-float v1, v1

    div-float/2addr v1, v5

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1519187
    :cond_1
    :goto_1
    iget-object v0, p0, LX/9e5;->f:Landroid/graphics/RectF;

    invoke-virtual {p0}, LX/9e5;->getRotatedDrawableWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, LX/9e5;->getRotatedDrawableHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v6, v6, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_0

    .line 1519188
    :cond_2
    iget-object v2, p0, LX/9e5;->b:Landroid/graphics/Matrix;

    int-to-float v1, v1

    div-float/2addr v1, v5

    int-to-float v0, v0

    div-float/2addr v0, v5

    invoke-virtual {v2, v1, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_1
.end method

.method public static c(LX/9e5;)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/high16 v7, 0x40000000    # 2.0f

    .line 1519189
    invoke-virtual {p0}, LX/9e5;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/9e5;->o:Z

    if-nez v0, :cond_1

    .line 1519190
    :cond_0
    :goto_0
    return-void

    .line 1519191
    :cond_1
    invoke-virtual {p0}, LX/9e5;->getRotatedDrawableHeight()I

    move-result v1

    .line 1519192
    invoke-virtual {p0}, LX/9e5;->getRotatedDrawableWidth()I

    move-result v2

    .line 1519193
    invoke-virtual {p0}, LX/9e5;->getHeight()I

    move-result v0

    .line 1519194
    invoke-virtual {p0}, LX/9e5;->getWidth()I

    move-result v3

    .line 1519195
    iget-object v4, p0, LX/9e5;->i:Landroid/graphics/RectF;

    if-nez v4, :cond_2

    .line 1519196
    iget v4, p0, LX/9e5;->m:F

    cmpl-float v4, v4, v6

    if-lez v4, :cond_3

    .line 1519197
    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v4

    int-to-float v4, v4

    iget v5, p0, LX/9e5;->m:F

    mul-float/2addr v4, v5

    .line 1519198
    int-to-float v3, v3

    iget v5, p0, LX/9e5;->n:F

    mul-float/2addr v5, v4

    sub-float/2addr v3, v5

    div-float/2addr v3, v7

    .line 1519199
    int-to-float v0, v0

    sub-float/2addr v0, v4

    div-float/2addr v0, v7

    .line 1519200
    new-instance v5, Landroid/graphics/RectF;

    iget v6, p0, LX/9e5;->n:F

    mul-float/2addr v6, v4

    add-float/2addr v6, v3

    add-float/2addr v4, v0

    invoke-direct {v5, v3, v0, v6, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v5, p0, LX/9e5;->i:Landroid/graphics/RectF;

    .line 1519201
    :cond_2
    :goto_1
    int-to-float v0, v1

    iget v3, p0, LX/9e5;->n:F

    mul-float/2addr v0, v3

    int-to-float v3, v2

    cmpl-float v0, v0, v3

    if-lez v0, :cond_5

    iget-object v0, p0, LX/9e5;->i:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    int-to-float v3, v2

    div-float/2addr v0, v3

    .line 1519202
    :goto_2
    iget-object v3, p0, LX/9e5;->c:Landroid/graphics/Matrix;

    invoke-virtual {v3}, Landroid/graphics/Matrix;->reset()V

    .line 1519203
    iget-object v3, p0, LX/9e5;->c:Landroid/graphics/Matrix;

    neg-int v2, v2

    int-to-float v2, v2

    div-float/2addr v2, v7

    neg-int v1, v1

    int-to-float v1, v1

    div-float/2addr v1, v7

    invoke-virtual {v3, v2, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1519204
    iget-object v1, p0, LX/9e5;->c:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 1519205
    iget-object v0, p0, LX/9e5;->c:Landroid/graphics/Matrix;

    iget-object v1, p0, LX/9e5;->i:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    iget-object v2, p0, LX/9e5;->i:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1519206
    iget-object v0, p0, LX/9e5;->c:Landroid/graphics/Matrix;

    iget-object v1, p0, LX/9e5;->g:Landroid/graphics/RectF;

    iget-object v2, p0, LX/9e5;->f:Landroid/graphics/RectF;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 1519207
    iget-object v0, p0, LX/9e5;->h:Landroid/graphics/RectF;

    iget-object v1, p0, LX/9e5;->g:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 1519208
    iget-object v0, p0, LX/9e5;->e:Landroid/graphics/Matrix;

    iget-object v1, p0, LX/9e5;->b:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 1519209
    iget-object v0, p0, LX/9e5;->e:Landroid/graphics/Matrix;

    iget-object v1, p0, LX/9e5;->c:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 1519210
    iget-object v0, p0, LX/9e5;->d:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 1519211
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, LX/9e5;->q:F

    .line 1519212
    iget-object v0, p0, LX/9e5;->e:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0}, LX/9e5;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 1519213
    iget-object v0, p0, LX/9e5;->h:Landroid/graphics/RectF;

    iget-object v1, p0, LX/9e5;->g:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 1519214
    const/high16 v3, 0x40000000    # 2.0f

    const/high16 v2, 0x3f800000    # 1.0f

    .line 1519215
    iget-object v0, p0, LX/9e5;->j:Landroid/graphics/RectF;

    if-nez v0, :cond_6

    .line 1519216
    :goto_3
    goto/16 :goto_0

    .line 1519217
    :cond_3
    if-le v0, v3, :cond_4

    .line 1519218
    sub-int/2addr v0, v3

    int-to-float v0, v0

    div-float/2addr v0, v7

    .line 1519219
    new-instance v4, Landroid/graphics/RectF;

    int-to-float v5, v3

    int-to-float v3, v3

    add-float/2addr v3, v0

    invoke-direct {v4, v6, v0, v5, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v4, p0, LX/9e5;->i:Landroid/graphics/RectF;

    goto/16 :goto_1

    .line 1519220
    :cond_4
    sub-int/2addr v3, v0

    int-to-float v3, v3

    div-float/2addr v3, v7

    .line 1519221
    new-instance v4, Landroid/graphics/RectF;

    int-to-float v5, v0

    add-float/2addr v5, v3

    int-to-float v0, v0

    invoke-direct {v4, v3, v6, v5, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v4, p0, LX/9e5;->i:Landroid/graphics/RectF;

    goto/16 :goto_1

    .line 1519222
    :cond_5
    iget-object v0, p0, LX/9e5;->i:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    int-to-float v3, v1

    div-float/2addr v0, v3

    goto/16 :goto_2

    .line 1519223
    :cond_6
    iget-object v0, p0, LX/9e5;->h:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget-object v1, p0, LX/9e5;->h:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_7

    iget-object v0, p0, LX/9e5;->j:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    div-float v0, v2, v0

    .line 1519224
    :goto_4
    iget-object v1, p0, LX/9e5;->i:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    iget-object v2, p0, LX/9e5;->i:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/9e5;->a(FFF)V

    .line 1519225
    iget-object v0, p0, LX/9e5;->h:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    div-float/2addr v0, v3

    iget-object v1, p0, LX/9e5;->j:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    iget-object v2, p0, LX/9e5;->h:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    .line 1519226
    iget-object v1, p0, LX/9e5;->h:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    div-float/2addr v1, v3

    iget-object v2, p0, LX/9e5;->j:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    iget-object v3, p0, LX/9e5;->h:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    .line 1519227
    invoke-virtual {p0, v0, v1}, LX/9e5;->a(FF)V

    goto :goto_3

    .line 1519228
    :cond_7
    iget-object v0, p0, LX/9e5;->j:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    div-float v0, v2, v0

    goto :goto_4
.end method


# virtual methods
.method public final a(FF)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    .line 1519229
    iget-object v1, p0, LX/9e5;->h:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    add-float/2addr v1, p1

    iget-object v2, p0, LX/9e5;->i:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_4

    .line 1519230
    iget-object v1, p0, LX/9e5;->i:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, LX/9e5;->h:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    sub-float p1, v1, v2

    .line 1519231
    :cond_0
    :goto_0
    iget-object v1, p0, LX/9e5;->h:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    add-float/2addr v1, p2

    iget-object v2, p0, LX/9e5;->i:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_5

    .line 1519232
    iget-object v1, p0, LX/9e5;->i:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    iget-object v2, p0, LX/9e5;->h:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    sub-float p2, v1, v2

    .line 1519233
    :cond_1
    :goto_1
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpg-float v1, v1, v3

    if-gez v1, :cond_2

    move p1, v0

    .line 1519234
    :cond_2
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpg-float v1, v1, v3

    if-gez v1, :cond_3

    move p2, v0

    .line 1519235
    :cond_3
    cmpl-float v1, p1, v0

    if-nez v1, :cond_6

    cmpl-float v0, p2, v0

    if-nez v0, :cond_6

    .line 1519236
    :goto_2
    return-void

    .line 1519237
    :cond_4
    iget-object v1, p0, LX/9e5;->h:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    add-float/2addr v1, p1

    iget-object v2, p0, LX/9e5;->i:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    .line 1519238
    iget-object v1, p0, LX/9e5;->i:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    iget-object v2, p0, LX/9e5;->h:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    sub-float p1, v1, v2

    goto :goto_0

    .line 1519239
    :cond_5
    iget-object v1, p0, LX/9e5;->h:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v1, p2

    iget-object v2, p0, LX/9e5;->i:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_1

    .line 1519240
    iget-object v1, p0, LX/9e5;->i:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    iget-object v2, p0, LX/9e5;->h:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    sub-float p2, v1, v2

    goto :goto_1

    .line 1519241
    :cond_6
    iget-object v0, p0, LX/9e5;->d:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1519242
    iget-object v0, p0, LX/9e5;->e:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1519243
    iget-object v0, p0, LX/9e5;->h:Landroid/graphics/RectF;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/RectF;->offset(FF)V

    .line 1519244
    iget-object v0, p0, LX/9e5;->e:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0}, LX/9e5;->setImageMatrix(Landroid/graphics/Matrix;)V

    goto :goto_2
.end method

.method public final a(FFF)V
    .locals 4

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 1519245
    iget v0, p0, LX/9e5;->q:F

    mul-float/2addr v0, p1

    iget v1, p0, LX/9e5;->p:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 1519246
    iget v0, p0, LX/9e5;->p:F

    iget v1, p0, LX/9e5;->q:F

    div-float p1, v0, v1

    .line 1519247
    :cond_0
    :goto_0
    iget v0, p0, LX/9e5;->q:F

    mul-float/2addr v0, p1

    iput v0, p0, LX/9e5;->q:F

    .line 1519248
    iget-object v0, p0, LX/9e5;->d:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1, p1, p2, p3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 1519249
    iget-object v0, p0, LX/9e5;->e:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1, p1, p2, p3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 1519250
    iget-object v0, p0, LX/9e5;->d:Landroid/graphics/Matrix;

    iget-object v1, p0, LX/9e5;->h:Landroid/graphics/RectF;

    iget-object v2, p0, LX/9e5;->g:Landroid/graphics/RectF;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 1519251
    iget-object v0, p0, LX/9e5;->h:Landroid/graphics/RectF;

    iget-object v1, p0, LX/9e5;->i:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->contains(Landroid/graphics/RectF;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1519252
    iget-object v0, p0, LX/9e5;->e:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0}, LX/9e5;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 1519253
    :goto_1
    return-void

    .line 1519254
    :cond_1
    iget v0, p0, LX/9e5;->q:F

    mul-float/2addr v0, p1

    cmpg-float v0, v0, v2

    if-gez v0, :cond_0

    .line 1519255
    iget v0, p0, LX/9e5;->q:F

    div-float p1, v2, v0

    goto :goto_0

    .line 1519256
    :cond_2
    invoke-virtual {p0, v3, v3}, LX/9e5;->a(FF)V

    goto :goto_1
.end method

.method public getMinBoundingRect()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 1519257
    iget-object v0, p0, LX/9e5;->i:Landroid/graphics/RectF;

    return-object v0
.end method

.method public getRotatedDrawableHeight()I
    .locals 1

    .prologue
    .line 1519258
    iget-object v0, p0, LX/9e5;->a:LX/9e4;

    invoke-virtual {v0}, LX/9e4;->isRatioChanged()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/9e5;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, LX/9e5;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    goto :goto_0
.end method

.method public getRotatedDrawableWidth()I
    .locals 1

    .prologue
    .line 1519259
    iget-object v0, p0, LX/9e5;->a:LX/9e4;

    invoke-virtual {v0}, LX/9e4;->isRatioChanged()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/9e5;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, LX/9e5;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    goto :goto_0
.end method

.method public final onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 1519260
    invoke-super/range {p0 .. p5}, Landroid/widget/ImageView;->onLayout(ZIIII)V

    .line 1519261
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/9e5;->o:Z

    .line 1519262
    if-eqz p1, :cond_0

    .line 1519263
    const/4 v0, 0x0

    iput-object v0, p0, LX/9e5;->i:Landroid/graphics/RectF;

    .line 1519264
    invoke-static {p0}, LX/9e5;->c(LX/9e5;)V

    .line 1519265
    :cond_0
    return-void
.end method

.method public setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 1519266
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1519267
    invoke-static {p0}, LX/9e5;->b(LX/9e5;)V

    .line 1519268
    invoke-static {p0}, LX/9e5;->c(LX/9e5;)V

    .line 1519269
    return-void
.end method
