.class public LX/8xB;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/8x9;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8xC;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1423609
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/8xB;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/8xC;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1423610
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1423611
    iput-object p1, p0, LX/8xB;->b:LX/0Ot;

    .line 1423612
    return-void
.end method

.method public static a(LX/0QB;)LX/8xB;
    .locals 4

    .prologue
    .line 1423613
    const-class v1, LX/8xB;

    monitor-enter v1

    .line 1423614
    :try_start_0
    sget-object v0, LX/8xB;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1423615
    sput-object v2, LX/8xB;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1423616
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1423617
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1423618
    new-instance v3, LX/8xB;

    const/16 p0, 0x17a6

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/8xB;-><init>(LX/0Ot;)V

    .line 1423619
    move-object v0, v3

    .line 1423620
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1423621
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8xB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1423622
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1423623
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 1423624
    check-cast p2, LX/8xA;

    .line 1423625
    iget-object v0, p0, LX/8xB;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8xC;

    iget-object v1, p2, LX/8xA;->a:Ljava/lang/String;

    const/16 p2, 0x10

    const/4 p0, 0x6

    const/4 v3, 0x4

    const/4 v8, -0x1

    const/4 v7, 0x2

    .line 1423626
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/4 v4, 0x1

    invoke-interface {v2, v4}, LX/1Dh;->C(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    iget-object v4, v0, LX/8xC;->a:LX/1vg;

    invoke-virtual {v4, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v4

    const v5, 0x7f021a47

    invoke-virtual {v4, v5}, LX/2xv;->h(I)LX/2xv;

    move-result-object v4

    const/high16 v5, 0x66000000

    invoke-virtual {v4, v5}, LX/2xv;->i(I)LX/2xv;

    move-result-object v4

    invoke-virtual {v4}, LX/1n6;->b()LX/1dc;

    move-result-object v4

    invoke-interface {v2, v4}, LX/1Dh;->b(LX/1dc;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v2

    iget-object v5, v0, LX/8xC;->a:LX/1vg;

    invoke-virtual {v5, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v5

    const v6, 0x7f0209c9

    invoke-virtual {v5, v6}, LX/2xv;->h(I)LX/2xv;

    move-result-object v5

    invoke-virtual {v5, v8}, LX/2xv;->i(I)LX/2xv;

    move-result-object v5

    invoke-virtual {v5}, LX/1n6;->b()LX/1dc;

    move-result-object v5

    invoke-virtual {v2, v5}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    invoke-interface {v2, p2}, LX/1Di;->j(I)LX/1Di;

    move-result-object v2

    invoke-interface {v2, p2}, LX/1Di;->r(I)LX/1Di;

    move-result-object v5

    if-nez v1, :cond_0

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v5, v7, v2}, LX/1Di;->h(II)LX/1Di;

    move-result-object v2

    invoke-interface {v2, v7}, LX/1Di;->b(I)LX/1Di;

    move-result-object v2

    invoke-interface {v4, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    if-nez v1, :cond_1

    const/4 v2, 0x0

    :goto_1
    invoke-interface {v4, v2}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v2

    const/4 v4, 0x7

    invoke-interface {v2, v4, v3}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, p0, p0}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 1423627
    return-object v0

    :cond_0
    move v2, v3

    goto :goto_0

    :cond_1
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v8}, LX/1ne;->m(I)LX/1ne;

    move-result-object v2

    const/high16 v5, 0x41400000    # 12.0f

    invoke-virtual {v2, v5}, LX/1ne;->g(F)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->d()LX/1X1;

    move-result-object v2

    goto :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1423628
    invoke-static {}, LX/1dS;->b()V

    .line 1423629
    const/4 v0, 0x0

    return-object v0
.end method
