.class public final LX/A97;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Z

.field public b:Lcom/facebook/graphql/enums/GraphQLBoostedComponentMessageType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1630750
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 1630751
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1630752
    iget-object v1, p0, LX/A97;->b:Lcom/facebook/graphql/enums/GraphQLBoostedComponentMessageType;

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1630753
    iget-object v3, p0, LX/A97;->c:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 1630754
    iget-object v5, p0, LX/A97;->d:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1630755
    const/4 v6, 0x4

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 1630756
    iget-boolean v6, p0, LX/A97;->a:Z

    invoke-virtual {v0, v7, v6}, LX/186;->a(IZ)V

    .line 1630757
    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1630758
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1630759
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1630760
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1630761
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1630762
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1630763
    invoke-virtual {v1, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1630764
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1630765
    new-instance v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;

    invoke-direct {v1, v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;-><init>(LX/15i;)V

    .line 1630766
    return-object v1
.end method
