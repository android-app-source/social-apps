.class public LX/A8D;
.super LX/0ht;
.source ""


# instance fields
.field public a:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1627410
    invoke-direct {p0, p1}, LX/0ht;-><init>(Landroid/content/Context;)V

    .line 1627411
    const/4 v0, -0x1

    iput v0, p0, LX/A8D;->a:I

    .line 1627412
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;ZLandroid/view/WindowManager$LayoutParams;)V
    .locals 2

    .prologue
    .line 1627406
    invoke-super {p0, p1, p2, p3}, LX/0ht;->a(Landroid/view/View;ZLandroid/view/WindowManager$LayoutParams;)V

    .line 1627407
    iget v0, p3, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1627408
    iget v0, p0, LX/A8D;->a:I

    iput v0, p3, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 1627409
    :cond_0
    return-void
.end method

.method public final d(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1627413
    iget-object v0, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    check-cast v0, Lcom/facebook/universalfeedback/ui/UniversalFeedbackPopoverViewFlipper;

    .line 1627414
    invoke-virtual {v0, p1}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackPopoverViewFlipper;->setContentViewPreservingLayout(Landroid/view/View;)V

    .line 1627415
    return-void
.end method

.method public final e(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1627403
    iget-object v0, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    check-cast v0, Lcom/facebook/universalfeedback/ui/UniversalFeedbackPopoverViewFlipper;

    .line 1627404
    invoke-virtual {v0, p1}, Lcom/facebook/universalfeedback/ui/UniversalFeedbackPopoverViewFlipper;->b(Landroid/view/View;)V

    .line 1627405
    return-void
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 1627402
    const v0, 0x7f031540

    return v0
.end method

.method public final k()Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
    .locals 2

    .prologue
    .line 1627400
    invoke-super {p0}, LX/0ht;->k()Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    move-result-object v0

    .line 1627401
    new-instance v1, LX/A8C;

    invoke-direct {v1, p0, v0}, LX/A8C;-><init>(LX/A8D;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    return-object v1
.end method
