.class public LX/8mv;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/stickers/client/IsAnimatedStickersEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1400158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1400159
    iput-object p1, p0, LX/8mv;->a:LX/0Or;

    .line 1400160
    return-void
.end method

.method public static a(Landroid/net/Uri;LX/1bZ;LX/33B;)LX/1bf;
    .locals 1
    .param p1    # LX/1bZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/33B;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1400161
    invoke-static {p0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    .line 1400162
    if-eqz p2, :cond_0

    .line 1400163
    iput-object p2, v0, LX/1bX;->j:LX/33B;

    .line 1400164
    :cond_0
    if-eqz p1, :cond_1

    .line 1400165
    iput-object p1, v0, LX/1bX;->e:LX/1bZ;

    .line 1400166
    :cond_1
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/stickers/model/Sticker;)[LX/1bf;
    .locals 2

    .prologue
    .line 1400167
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1400168
    iget-object v1, p0, Lcom/facebook/stickers/model/Sticker;->h:Landroid/net/Uri;

    if-eqz v1, :cond_0

    .line 1400169
    iget-object v1, p0, Lcom/facebook/stickers/model/Sticker;->h:Landroid/net/Uri;

    invoke-static {v1}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1400170
    :cond_0
    iget-object v1, p0, Lcom/facebook/stickers/model/Sticker;->d:Landroid/net/Uri;

    if-eqz v1, :cond_1

    .line 1400171
    iget-object v1, p0, Lcom/facebook/stickers/model/Sticker;->d:Landroid/net/Uri;

    invoke-static {v1}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1400172
    :cond_1
    iget-object v1, p0, Lcom/facebook/stickers/model/Sticker;->g:Landroid/net/Uri;

    if-eqz v1, :cond_2

    .line 1400173
    iget-object v1, p0, Lcom/facebook/stickers/model/Sticker;->g:Landroid/net/Uri;

    invoke-static {v1}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1400174
    :cond_2
    iget-object v1, p0, Lcom/facebook/stickers/model/Sticker;->c:Landroid/net/Uri;

    invoke-static {v1}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1400175
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [LX/1bf;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1bf;

    return-object v0
.end method

.method public static b(LX/0QB;)LX/8mv;
    .locals 2

    .prologue
    .line 1400176
    new-instance v0, LX/8mv;

    const/16 v1, 0x1578

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-direct {v0, v1}, LX/8mv;-><init>(LX/0Or;)V

    .line 1400177
    return-object v0
.end method

.method public static c(Lcom/facebook/stickers/model/Sticker;)LX/1bf;
    .locals 1

    .prologue
    .line 1400178
    iget-object v0, p0, Lcom/facebook/stickers/model/Sticker;->h:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 1400179
    iget-object v0, p0, Lcom/facebook/stickers/model/Sticker;->h:Landroid/net/Uri;

    invoke-static {v0}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v0

    .line 1400180
    :goto_0
    return-object v0

    .line 1400181
    :cond_0
    iget-object v0, p0, Lcom/facebook/stickers/model/Sticker;->d:Landroid/net/Uri;

    if-eqz v0, :cond_1

    .line 1400182
    iget-object v0, p0, Lcom/facebook/stickers/model/Sticker;->d:Landroid/net/Uri;

    invoke-static {v0}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v0

    goto :goto_0

    .line 1400183
    :cond_1
    iget-object v0, p0, Lcom/facebook/stickers/model/Sticker;->g:Landroid/net/Uri;

    if-eqz v0, :cond_2

    .line 1400184
    iget-object v0, p0, Lcom/facebook/stickers/model/Sticker;->g:Landroid/net/Uri;

    invoke-static {v0}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v0

    goto :goto_0

    .line 1400185
    :cond_2
    iget-object v0, p0, Lcom/facebook/stickers/model/Sticker;->c:Landroid/net/Uri;

    invoke-static {v0}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/stickers/model/Sticker;LX/8mj;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/stickers/model/Sticker;",
            "LX/8mj;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/1bf;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 1400186
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1400187
    const/4 v0, 0x0

    .line 1400188
    iget-object v2, p0, Lcom/facebook/stickers/model/Sticker;->f:Landroid/net/Uri;

    if-eqz v2, :cond_4

    .line 1400189
    iget-object v0, p0, Lcom/facebook/stickers/model/Sticker;->f:Landroid/net/Uri;

    .line 1400190
    :cond_0
    :goto_0
    if-eqz v0, :cond_3

    .line 1400191
    invoke-static {}, LX/4eB;->newBuilder()LX/4eC;

    move-result-object v2

    .line 1400192
    iget-boolean v3, p1, LX/8mj;->g:Z

    if-eqz v3, :cond_5

    .line 1400193
    invoke-virtual {v2, v4}, LX/4eC;->d(Z)LX/4eC;

    move-result-object v3

    .line 1400194
    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 1400195
    iput-object v4, v3, LX/4eC;->c:Ljava/lang/Integer;

    .line 1400196
    :cond_1
    :goto_1
    iget v3, p1, LX/8mj;->e:I

    invoke-static {v3}, Landroid/graphics/Color;->alpha(I)I

    move-result v3

    if-eqz v3, :cond_2

    .line 1400197
    iget v3, p1, LX/8mj;->e:I

    .line 1400198
    iput v3, v2, LX/4eC;->b:I

    .line 1400199
    :cond_2
    invoke-virtual {v2}, LX/4eC;->l()LX/4eB;

    move-result-object v2

    .line 1400200
    iget-object v3, p1, LX/8mj;->d:LX/33B;

    invoke-static {v0, v2, v3}, LX/8mv;->a(Landroid/net/Uri;LX/1bZ;LX/33B;)LX/1bf;

    move-result-object v0

    .line 1400201
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1400202
    :cond_3
    return-object v1

    .line 1400203
    :cond_4
    iget-object v2, p0, Lcom/facebook/stickers/model/Sticker;->e:Landroid/net/Uri;

    if-eqz v2, :cond_0

    .line 1400204
    iget-object v0, p0, Lcom/facebook/stickers/model/Sticker;->e:Landroid/net/Uri;

    goto :goto_0

    .line 1400205
    :cond_5
    iget-boolean v3, p1, LX/8mj;->h:Z

    if-eqz v3, :cond_1

    .line 1400206
    invoke-virtual {v2, v4}, LX/4eC;->e(Z)LX/4eC;

    move-result-object v3

    invoke-virtual {v3}, LX/4eC;->j()LX/4eC;

    goto :goto_1
.end method
