.class public final LX/8r8;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/ufiservices/graphql/FetchProfilesGraphQLModels$FetchProfilesQueryModel;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1408449
    const-class v1, Lcom/facebook/ufiservices/graphql/FetchProfilesGraphQLModels$FetchProfilesQueryModel;

    const v0, -0x533c8cba

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x2

    const-string v5, "FetchProfilesQuery"

    const-string v6, "0725cdf3fcf5d2dcfddf4c3b7f1f4d20"

    const-string v7, "nodes"

    const-string v8, "10155069965731729"

    const/4 v9, 0x0

    .line 1408450
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1408451
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1408452
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1408454
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1408455
    sparse-switch v0, :sswitch_data_0

    .line 1408456
    :goto_0
    return-object p1

    .line 1408457
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1408458
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1408459
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x41a91745 -> :sswitch_1
        0xa9c5322 -> :sswitch_0
        0x291d8de0 -> :sswitch_2
    .end sparse-switch
.end method

.method public final l()Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;
    .locals 2

    .prologue
    .line 1408453
    new-instance v0, Lcom/facebook/ufiservices/graphql/FetchProfilesGraphQL$FetchProfilesQueryString$1;

    const-class v1, Lcom/facebook/ufiservices/graphql/FetchProfilesGraphQLModels$FetchProfilesQueryModel;

    invoke-direct {v0, p0, v1}, Lcom/facebook/ufiservices/graphql/FetchProfilesGraphQL$FetchProfilesQueryString$1;-><init>(LX/8r8;Ljava/lang/Class;)V

    return-object v0
.end method
