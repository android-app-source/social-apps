.class public final LX/9eq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Landroid/net/Uri;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/net/Uri;

.field public final synthetic b:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 1520628
    iput-object p1, p0, LX/9eq;->b:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iput-object p2, p0, LX/9eq;->a:Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1520629
    const/4 v4, 0x0

    .line 1520630
    iget-object v0, p0, LX/9eq;->b:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->G:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/74n;

    iget-object v1, p0, LX/9eq;->b:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->k:Landroid/net/Uri;

    sget-object v2, LX/74j;->DEFAULT:LX/74j;

    invoke-virtual {v0, v1, v2}, LX/74n;->a(Landroid/net/Uri;LX/74j;)Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    .line 1520631
    if-eqz v1, :cond_0

    iget-object v0, p0, LX/9eq;->b:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->F:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/75F;

    new-instance v2, Lcom/facebook/ipc/media/MediaIdKey;

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v3

    .line 1520632
    iget-object v5, v1, Lcom/facebook/ipc/media/MediaItem;->c:Lcom/facebook/ipc/media/data/LocalMediaData;

    move-object v1, v5

    .line 1520633
    iget-wide v8, v1, Lcom/facebook/ipc/media/data/LocalMediaData;->mMediaStoreId:J

    move-wide v6, v8

    .line 1520634
    invoke-direct {v2, v3, v6, v7}, Lcom/facebook/ipc/media/MediaIdKey;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v0, v2}, LX/75F;->a(Lcom/facebook/ipc/media/MediaIdKey;)LX/0Px;

    move-result-object v0

    .line 1520635
    :goto_0
    if-eqz v0, :cond_1

    iget-object v1, p0, LX/9eq;->b:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v1

    invoke-static {v1}, LX/63w;->c(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Landroid/graphics/RectF;

    move-result-object v1

    iget-object v2, p0, LX/9eq;->b:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v3, p0, LX/9eq;->b:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v3, v3, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->k:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->a(Landroid/net/Uri;)I

    move-result v2

    invoke-static {v0, v1, v2}, LX/9ic;->a(Ljava/util/List;Landroid/graphics/RectF;I)Ljava/util/List;

    move-result-object v0

    move-object v3, v0

    .line 1520636
    :goto_1
    iget-object v0, p0, LX/9eq;->b:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->S:LX/8GV;

    const/high16 v1, 0x3f800000    # 1.0f

    iget-object v2, p0, LX/9eq;->b:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v2, v2, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-object v2, v2, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->l:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    if-eqz v3, :cond_2

    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    :goto_2
    iget-object v4, p0, LX/9eq;->a:Landroid/net/Uri;

    iget-object v5, p0, LX/9eq;->b:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v5, v5, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-boolean v5, v5, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->g:Z

    invoke-virtual/range {v0 .. v5}, LX/8GV;->a(FLcom/facebook/photos/creativeediting/model/CreativeEditingData;LX/0Px;Landroid/net/Uri;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v0, v4

    .line 1520637
    goto :goto_0

    :cond_1
    move-object v3, v4

    .line 1520638
    goto :goto_1

    :cond_2
    move-object v3, v4

    .line 1520639
    goto :goto_2
.end method
