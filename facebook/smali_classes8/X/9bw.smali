.class public final enum LX/9bw;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9bw;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9bw;

.field public static final enum MOVE:LX/9bw;

.field public static final enum RESIZE:LX/9bw;

.field public static final enum RESIZE_WITH_RATIO:LX/9bw;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1515445
    new-instance v0, LX/9bw;

    const-string v1, "MOVE"

    invoke-direct {v0, v1, v2}, LX/9bw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9bw;->MOVE:LX/9bw;

    .line 1515446
    new-instance v0, LX/9bw;

    const-string v1, "RESIZE"

    invoke-direct {v0, v1, v3}, LX/9bw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9bw;->RESIZE:LX/9bw;

    .line 1515447
    new-instance v0, LX/9bw;

    const-string v1, "RESIZE_WITH_RATIO"

    invoke-direct {v0, v1, v4}, LX/9bw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9bw;->RESIZE_WITH_RATIO:LX/9bw;

    .line 1515448
    const/4 v0, 0x3

    new-array v0, v0, [LX/9bw;

    sget-object v1, LX/9bw;->MOVE:LX/9bw;

    aput-object v1, v0, v2

    sget-object v1, LX/9bw;->RESIZE:LX/9bw;

    aput-object v1, v0, v3

    sget-object v1, LX/9bw;->RESIZE_WITH_RATIO:LX/9bw;

    aput-object v1, v0, v4

    sput-object v0, LX/9bw;->$VALUES:[LX/9bw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1515451
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9bw;
    .locals 1

    .prologue
    .line 1515450
    const-class v0, LX/9bw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9bw;

    return-object v0
.end method

.method public static values()[LX/9bw;
    .locals 1

    .prologue
    .line 1515449
    sget-object v0, LX/9bw;->$VALUES:[LX/9bw;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9bw;

    return-object v0
.end method
