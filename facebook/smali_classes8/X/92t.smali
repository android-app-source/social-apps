.class public LX/92t;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;

.field public static final c:Ljava/lang/String;

.field private static volatile g:LX/92t;


# instance fields
.field private final d:LX/0tX;

.field public final e:LX/92T;

.field public final f:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "LX/92s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1432921
    const-string v0, "60"

    sput-object v0, LX/92t;->a:Ljava/lang/String;

    .line 1432922
    const-string v0, "10"

    sput-object v0, LX/92t;->b:Ljava/lang/String;

    .line 1432923
    const-string v0, "32"

    sput-object v0, LX/92t;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0tX;LX/92T;LX/1Ck;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1432924
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1432925
    iput-object p1, p0, LX/92t;->d:LX/0tX;

    .line 1432926
    iput-object p2, p0, LX/92t;->e:LX/92T;

    .line 1432927
    iput-object p3, p0, LX/92t;->f:LX/1Ck;

    .line 1432928
    return-void
.end method

.method public static a(LX/0QB;)LX/92t;
    .locals 6

    .prologue
    .line 1432908
    sget-object v0, LX/92t;->g:LX/92t;

    if-nez v0, :cond_1

    .line 1432909
    const-class v1, LX/92t;

    monitor-enter v1

    .line 1432910
    :try_start_0
    sget-object v0, LX/92t;->g:LX/92t;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1432911
    if-eqz v2, :cond_0

    .line 1432912
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1432913
    new-instance p0, LX/92t;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/92T;->b(LX/0QB;)LX/92T;

    move-result-object v4

    check-cast v4, LX/92T;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v5

    check-cast v5, LX/1Ck;

    invoke-direct {p0, v3, v4, v5}, LX/92t;-><init>(LX/0tX;LX/92T;LX/1Ck;)V

    .line 1432914
    move-object v0, p0

    .line 1432915
    sput-object v0, LX/92t;->g:LX/92t;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1432916
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1432917
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1432918
    :cond_1
    sget-object v0, LX/92t;->g:LX/92t;

    return-object v0

    .line 1432919
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1432920
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/92t;Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;",
            ">;)",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1432851
    if-nez p1, :cond_0

    .line 1432852
    :goto_0
    return-object p1

    .line 1432853
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1432854
    check-cast v0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->K()Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;->a()LX/0Px;

    move-result-object v0

    new-instance v1, LX/92r;

    invoke-direct {v1, p0}, LX/92r;-><init>(LX/92t;)V

    invoke-static {v0, v1}, LX/0Ph;->c(Ljava/lang/Iterable;LX/0Rl;)Ljava/lang/Iterable;

    move-result-object v1

    .line 1432855
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1432856
    check-cast v0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->K()Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;

    move-result-object v0

    .line 1432857
    new-instance v2, LX/5LK;

    invoke-direct {v2}, LX/5LK;-><init>()V

    .line 1432858
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;->a()LX/0Px;

    move-result-object p0

    iput-object p0, v2, LX/5LK;->a:LX/0Px;

    .line 1432859
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object p0

    iput-object p0, v2, LX/5LK;->b:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    .line 1432860
    move-object v0, v2

    .line 1432861
    invoke-static {v1}, LX/0Px;->copyOf(Ljava/lang/Iterable;)LX/0Px;

    move-result-object v1

    .line 1432862
    iput-object v1, v0, LX/5LK;->a:LX/0Px;

    .line 1432863
    move-object v0, v0

    .line 1432864
    const/4 v7, 0x1

    const/4 v9, 0x0

    const/4 v5, 0x0

    .line 1432865
    new-instance v3, LX/186;

    const/16 v4, 0x80

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1432866
    iget-object v4, v0, LX/5LK;->a:LX/0Px;

    invoke-static {v3, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v4

    .line 1432867
    iget-object v6, v0, LX/5LK;->b:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    invoke-static {v3, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1432868
    const/4 v8, 0x2

    invoke-virtual {v3, v8}, LX/186;->c(I)V

    .line 1432869
    invoke-virtual {v3, v9, v4}, LX/186;->b(II)V

    .line 1432870
    invoke-virtual {v3, v7, v6}, LX/186;->b(II)V

    .line 1432871
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    .line 1432872
    invoke-virtual {v3, v4}, LX/186;->d(I)V

    .line 1432873
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1432874
    invoke-virtual {v4, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1432875
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1432876
    new-instance v4, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;

    invoke-direct {v4, v3}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;-><init>(LX/15i;)V

    .line 1432877
    move-object v1, v4

    .line 1432878
    invoke-static {p1}, LX/1lO;->a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/1lO;

    move-result-object v2

    new-instance v0, LX/5LJ;

    invoke-direct {v0}, LX/5LJ;-><init>()V

    .line 1432879
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1432880
    check-cast v0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;

    .line 1432881
    new-instance v3, LX/5LJ;

    invoke-direct {v3}, LX/5LJ;-><init>()V

    .line 1432882
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->s()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel$AllIconsModel;

    move-result-object v4

    iput-object v4, v3, LX/5LJ;->a:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel$AllIconsModel;

    .line 1432883
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->C()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;

    move-result-object v4

    iput-object v4, v3, LX/5LJ;->b:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;

    .line 1432884
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->D()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;

    move-result-object v4

    iput-object v4, v3, LX/5LJ;->c:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;

    .line 1432885
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->j()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, LX/5LJ;->d:Ljava/lang/String;

    .line 1432886
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->k()Z

    move-result v4

    iput-boolean v4, v3, LX/5LJ;->e:Z

    .line 1432887
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->l()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, LX/5LJ;->f:Ljava/lang/String;

    .line 1432888
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->m()I

    move-result v4

    iput v4, v3, LX/5LJ;->g:I

    .line 1432889
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->n()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, LX/5LJ;->h:Ljava/lang/String;

    .line 1432890
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->E()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v4

    iput-object v4, v3, LX/5LJ;->i:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1432891
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->F()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v4

    iput-object v4, v3, LX/5LJ;->j:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1432892
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->G()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v4

    iput-object v4, v3, LX/5LJ;->k:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1432893
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->H()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v4

    iput-object v4, v3, LX/5LJ;->l:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1432894
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->I()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v4

    iput-object v4, v3, LX/5LJ;->m:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1432895
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->J()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v4

    iput-object v4, v3, LX/5LJ;->n:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1432896
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->o()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, LX/5LJ;->o:Ljava/lang/String;

    .line 1432897
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->p()Z

    move-result v4

    iput-boolean v4, v3, LX/5LJ;->p:Z

    .line 1432898
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->q()Z

    move-result v4

    iput-boolean v4, v3, LX/5LJ;->q:Z

    .line 1432899
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->r()Z

    move-result v4

    iput-boolean v4, v3, LX/5LJ;->r:Z

    .line 1432900
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;->K()Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;

    move-result-object v4

    iput-object v4, v3, LX/5LJ;->s:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;

    .line 1432901
    move-object v0, v3

    .line 1432902
    iput-object v1, v0, LX/5LJ;->s:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;

    .line 1432903
    move-object v0, v0

    .line 1432904
    invoke-virtual {v0}, LX/5LJ;->a()Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;

    move-result-object v0

    .line 1432905
    iput-object v0, v2, LX/1lO;->k:Ljava/lang/Object;

    .line 1432906
    move-object v0, v2

    .line 1432907
    invoke-virtual {v0}, LX/1lO;->a()Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object p1

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(LX/92P;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/92P;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1432836
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1432837
    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v4

    .line 1432838
    if-nez v4, :cond_0

    .line 1432839
    sget-object v4, LX/0wB;->a:LX/0wC;

    .line 1432840
    :cond_0
    invoke-static {}, LX/5LD;->b()LX/5LC;

    move-result-object v5

    .line 1432841
    const-string v6, "legacy_activity_api_id"

    iget-object v7, p1, LX/92P;->a:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v6

    const-string v7, "taggable_object_count"

    sget-object v8, LX/92t;->b:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v6

    const-string v7, "taggable_object_image_scale"

    invoke-virtual {v6, v7, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v6

    const-string v7, "taggable_object_pp_size"

    sget-object v8, LX/92t;->a:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v6

    const-string v7, "image_scale"

    invoke-virtual {v6, v7, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v4

    const-string v6, "minutiae_image_size_large"

    sget-object v7, LX/92t;->c:Ljava/lang/String;

    invoke-virtual {v4, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v6, "place_id"

    iget-object v7, p1, LX/92P;->b:Ljava/lang/String;

    invoke-virtual {v4, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v6, "typeahead_session_id"

    iget v7, p1, LX/92P;->d:I

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v6, "request_id"

    iget-object v7, p1, LX/92P;->e:Ljava/lang/String;

    invoke-virtual {v4, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v6

    const-string v7, "taggable_object_query_string"

    iget-object v4, p1, LX/92P;->c:Ljava/lang/String;

    if-eqz v4, :cond_2

    iget-object v4, p1, LX/92P;->c:Ljava/lang/String;

    :goto_0
    invoke-virtual {v6, v7, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v6, "flow_source"

    iget-object v7, p1, LX/92P;->f:Ljava/lang/String;

    invoke-virtual {v4, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1432842
    iget-object v4, p1, LX/92P;->g:Landroid/location/Location;

    if-eqz v4, :cond_1

    .line 1432843
    const-string v4, "userLongitude"

    iget-object v6, p1, LX/92P;->g:Landroid/location/Location;

    invoke-virtual {v6}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-virtual {v5, v4, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v4

    const-string v6, "userLatitude"

    iget-object v7, p1, LX/92P;->g:Landroid/location/Location;

    invoke-virtual {v7}, Landroid/location/Location;->getLatitude()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1432844
    :cond_1
    move-object v0, v5

    .line 1432845
    invoke-static {}, LX/5LD;->b()LX/5LC;

    .line 1432846
    new-instance v1, LX/92d;

    .line 1432847
    iget-object v2, v0, LX/0gW;->e:LX/0w7;

    move-object v2, v2

    .line 1432848
    invoke-direct {v1, v0, v2}, LX/92d;-><init>(LX/0gW;LX/0w7;)V

    .line 1432849
    iget-object v2, p0, LX/92t;->d:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v3, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zT;)LX/0zO;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    new-instance v1, LX/92q;

    invoke-direct {v1, p0}, LX/92q;-><init>(LX/92t;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 1432850
    :cond_2
    const-string v4, ""

    goto :goto_0
.end method
