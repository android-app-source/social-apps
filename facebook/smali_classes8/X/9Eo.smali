.class public final LX/9Eo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLActor;

.field public final synthetic c:LX/9Ep;


# direct methods
.method public constructor <init>(LX/9Ep;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLActor;)V
    .locals 0

    .prologue
    .line 1457501
    iput-object p1, p0, LX/9Eo;->c:LX/9Ep;

    iput-object p2, p0, LX/9Eo;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p3, p0, LX/9Eo;->b:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x2152e54

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1457502
    iget-object v1, p0, LX/9Eo;->c:LX/9Ep;

    iget-object v1, v1, LX/9Ep;->c:LX/8xu;

    iget-object v2, p0, LX/9Eo;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p0, LX/9Eo;->b:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v3

    .line 1457503
    if-nez v2, :cond_1

    .line 1457504
    :cond_0
    :goto_0
    sget-object v1, LX/0ax;->bE:Ljava/lang/String;

    iget-object v2, p0, LX/9Eo;->b:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1457505
    iget-object v2, p0, LX/9Eo;->c:LX/9Ep;

    iget-object v2, v2, LX/9Ep;->a:LX/17W;

    iget-object v3, p0, LX/9Eo;->c:LX/9Ep;

    iget-object v3, v3, LX/9Ep;->b:Landroid/content/Context;

    invoke-virtual {v2, v3, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 1457506
    const v1, 0x5dbd85e2

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1457507
    :cond_1
    iget-object v5, v1, LX/8xu;->b:LX/0Zb;

    const-string v6, "social_search_people_cards_profile_click"

    const/4 p1, 0x0

    invoke-interface {v5, v6, p1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v5

    .line 1457508
    invoke-virtual {v5}, LX/0oG;->a()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1457509
    invoke-static {v1, v2, v3, v5}, LX/8xu;->a(LX/8xu;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;LX/0oG;)LX/0oG;

    move-result-object v5

    .line 1457510
    invoke-virtual {v5}, LX/0oG;->d()V

    goto :goto_0
.end method
