.class public final LX/9dm;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:LX/9dr;


# direct methods
.method public constructor <init>(LX/9dr;)V
    .locals 0

    .prologue
    .line 1518740
    iput-object p1, p0, LX/9dm;->a:LX/9dr;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1518741
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    double-to-float v0, v0

    .line 1518742
    cmpg-float v1, v0, v2

    if-gez v1, :cond_1

    iget-object v1, p0, LX/9dm;->a:LX/9dr;

    iget-boolean v1, v1, LX/9dr;->c:Z

    if-eqz v1, :cond_1

    .line 1518743
    iget-object v1, p0, LX/9dm;->a:LX/9dr;

    iget-object v1, v1, LX/9dr;->e:Landroid/widget/ImageView;

    iget-object v2, p0, LX/9dm;->a:LX/9dr;

    iget v2, v2, LX/9dr;->n:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1518744
    iget-object v1, p0, LX/9dm;->a:LX/9dr;

    const/4 v2, 0x0

    .line 1518745
    iput-boolean v2, v1, LX/9dr;->c:Z

    .line 1518746
    :cond_0
    :goto_0
    iget-object v1, p0, LX/9dm;->a:LX/9dr;

    iget-object v1, v1, LX/9dr;->e:Landroid/widget/ImageView;

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 1518747
    iget-object v1, p0, LX/9dm;->a:LX/9dr;

    iget-object v1, v1, LX/9dr;->e:Landroid/widget/ImageView;

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setScaleY(F)V

    .line 1518748
    iget-object v0, p0, LX/9dm;->a:LX/9dr;

    invoke-static {v0}, LX/9dr;->i(LX/9dr;)V

    .line 1518749
    return-void

    .line 1518750
    :cond_1
    cmpl-float v1, v0, v2

    if-ltz v1, :cond_0

    iget-object v1, p0, LX/9dm;->a:LX/9dr;

    iget-boolean v1, v1, LX/9dr;->c:Z

    if-nez v1, :cond_0

    .line 1518751
    iget-object v1, p0, LX/9dm;->a:LX/9dr;

    iget-object v1, v1, LX/9dr;->e:Landroid/widget/ImageView;

    const v2, 0x7f0215b3

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1518752
    iget-object v1, p0, LX/9dm;->a:LX/9dr;

    const/4 v2, 0x1

    .line 1518753
    iput-boolean v2, v1, LX/9dr;->c:Z

    .line 1518754
    goto :goto_0
.end method

.method public final b(LX/0wd;)V
    .locals 2

    .prologue
    .line 1518755
    const-wide/high16 v0, 0x3ff8000000000000L    # 1.5

    invoke-virtual {p1, v0, v1}, LX/0wd;->g(D)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1518756
    iget-object v0, p0, LX/9dm;->a:LX/9dr;

    sget-object v1, LX/9dq;->OPENING_COMPLETE:LX/9dq;

    .line 1518757
    iput-object v1, v0, LX/9dr;->l:LX/9dq;

    .line 1518758
    :goto_0
    return-void

    .line 1518759
    :cond_0
    iget-object v0, p0, LX/9dm;->a:LX/9dr;

    sget-object v1, LX/9dq;->DEFAULT:LX/9dq;

    .line 1518760
    iput-object v1, v0, LX/9dr;->l:LX/9dq;

    .line 1518761
    goto :goto_0
.end method
