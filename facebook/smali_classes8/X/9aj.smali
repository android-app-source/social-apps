.class public final LX/9aj;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;)V
    .locals 0

    .prologue
    .line 1513970
    iput-object p1, p0, LX/9aj;->a:Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 9

    .prologue
    .line 1513971
    iget-object v0, p0, LX/9aj;->a:Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;

    const/4 v5, 0x0

    .line 1513972
    invoke-static {v0}, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->r(Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1513973
    new-instance v7, LX/9ab;

    iget-object v1, v0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1513974
    iget-object v2, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1513975
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    iget-object v3, v0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-direct {v7, v1, v2, v3}, LX/9ab;-><init>(JLcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 1513976
    :goto_0
    iget-object v1, v0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/9ac;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, v0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->z:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v3}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->A:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v4}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    iget-object v6, v0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->o:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    if-eqz v6, :cond_0

    iget-object v5, v0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->o:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    :cond_0
    iget-object v6, v0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->q:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-object v8, v0, Lcom/facebook/photos/albumcreator/AlbumCreatorFragment;->t:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual/range {v1 .. v8}, LX/9ac;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Lcom/facebook/composer/privacy/model/ComposerPrivacyData;LX/9ab;Lcom/facebook/ipc/composer/intent/ComposerTargetData;)V

    .line 1513977
    return-void

    :cond_1
    move-object v7, v5

    goto :goto_0
.end method
