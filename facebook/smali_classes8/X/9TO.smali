.class public final LX/9TO;
.super LX/0zP;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0zP",
        "<",
        "Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$CreatePostTopicFieldModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 12

    .prologue
    .line 1495675
    const-class v1, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$CreatePostTopicFieldModel;

    const v0, 0x4daf4719    # 3.67584032E8f

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "GroupPostTopicMutation"

    const-string v6, "68903ac39aa70a6518ed82b4b2b18a0d"

    const-string v7, "group_post_topic_create"

    const-string v8, "0"

    const-string v9, "10155069966986729"

    const/4 v10, 0x0

    .line 1495676
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v11, v0

    .line 1495677
    move-object v0, p0

    invoke-direct/range {v0 .. v11}, LX/0zP;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1495678
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1495679
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1495680
    packed-switch v0, :pswitch_data_0

    .line 1495681
    :goto_0
    return-object p1

    .line 1495682
    :pswitch_0
    const-string p1, "0"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x5fb57ca
        :pswitch_0
    .end packed-switch
.end method
