.class public LX/9lP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public final d:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

.field public e:Landroid/os/ParcelUuid;


# direct methods
.method public constructor <init>(JJ)V
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 1532817
    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move-object v7, v6

    invoke-direct/range {v1 .. v7}, LX/9lP;-><init>(JJLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    .line 1532818
    return-void
.end method

.method public constructor <init>(JJLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V
    .locals 3
    .param p5    # Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1532819
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1, p5, p6}, LX/9lP;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    .line 1532820
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V
    .locals 0
    .param p3    # Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1532821
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1532822
    iput-object p1, p0, LX/9lP;->a:Ljava/lang/String;

    .line 1532823
    iput-object p2, p0, LX/9lP;->b:Ljava/lang/String;

    .line 1532824
    if-eqz p3, :cond_0

    :goto_0
    iput-object p3, p0, LX/9lP;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1532825
    if-eqz p4, :cond_1

    :goto_1
    iput-object p4, p0, LX/9lP;->d:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 1532826
    return-void

    .line 1532827
    :cond_0
    sget-object p3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    goto :goto_0

    .line 1532828
    :cond_1
    sget-object p4, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    goto :goto_1
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1532829
    iget-object v0, p0, LX/9lP;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 1532830
    iget-object v0, p0, LX/9lP;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9lP;->a:Ljava/lang/String;

    iget-object v1, p0, LX/9lP;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()LX/9lQ;
    .locals 3

    .prologue
    .line 1532831
    invoke-virtual {p0}, LX/9lP;->f()Z

    move-result v0

    .line 1532832
    iget-object v1, p0, LX/9lP;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object v1, v1

    .line 1532833
    iget-object v2, p0, LX/9lP;->d:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-object v2, v2

    .line 1532834
    invoke-static {v0, v1, v2}, LX/9lQ;->getRelationshipType(ZLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)LX/9lQ;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1532835
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "profileId"

    iget-object v2, p0, LX/9lP;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "viewerId"

    iget-object v2, p0, LX/9lP;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "friendshipStatus"

    iget-object v2, p0, LX/9lP;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "subscribeStatus"

    iget-object v2, p0, LX/9lP;->d:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
