.class public final LX/91n;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/91o;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

.field public b:Z

.field public c:Ljava/lang/String;

.field public d:LX/90B;

.field public e:LX/90D;

.field public final synthetic f:LX/91o;


# direct methods
.method public constructor <init>(LX/91o;)V
    .locals 1

    .prologue
    .line 1431385
    iput-object p1, p0, LX/91n;->f:LX/91o;

    .line 1431386
    move-object v0, p1

    .line 1431387
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1431388
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1431389
    const-string v0, "MinutiaeFeelingsComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1431390
    if-ne p0, p1, :cond_1

    .line 1431391
    :cond_0
    :goto_0
    return v0

    .line 1431392
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1431393
    goto :goto_0

    .line 1431394
    :cond_3
    check-cast p1, LX/91n;

    .line 1431395
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1431396
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1431397
    if-eq v2, v3, :cond_0

    .line 1431398
    iget-object v2, p0, LX/91n;->a:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/91n;->a:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    iget-object v3, p1, LX/91n;->a:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1431399
    goto :goto_0

    .line 1431400
    :cond_5
    iget-object v2, p1, LX/91n;->a:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    if-nez v2, :cond_4

    .line 1431401
    :cond_6
    iget-boolean v2, p0, LX/91n;->b:Z

    iget-boolean v3, p1, LX/91n;->b:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 1431402
    goto :goto_0

    .line 1431403
    :cond_7
    iget-object v2, p0, LX/91n;->c:Ljava/lang/String;

    if-eqz v2, :cond_9

    iget-object v2, p0, LX/91n;->c:Ljava/lang/String;

    iget-object v3, p1, LX/91n;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_8
    move v0, v1

    .line 1431404
    goto :goto_0

    .line 1431405
    :cond_9
    iget-object v2, p1, LX/91n;->c:Ljava/lang/String;

    if-nez v2, :cond_8

    .line 1431406
    :cond_a
    iget-object v2, p0, LX/91n;->d:LX/90B;

    if-eqz v2, :cond_c

    iget-object v2, p0, LX/91n;->d:LX/90B;

    iget-object v3, p1, LX/91n;->d:LX/90B;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    .line 1431407
    goto :goto_0

    .line 1431408
    :cond_c
    iget-object v2, p1, LX/91n;->d:LX/90B;

    if-nez v2, :cond_b

    .line 1431409
    :cond_d
    iget-object v2, p0, LX/91n;->e:LX/90D;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/91n;->e:LX/90D;

    iget-object v3, p1, LX/91n;->e:LX/90D;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1431410
    goto :goto_0

    .line 1431411
    :cond_e
    iget-object v2, p1, LX/91n;->e:LX/90D;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
