.class public final LX/8of;
.super Landroid/text/SpannableStringBuilder;
.source ""


# static fields
.field private static final a:LX/8oa;

.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public c:LX/8oi;

.field public d:LX/8oa;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1404573
    new-instance v0, LX/8ob;

    invoke-direct {v0}, LX/8ob;-><init>()V

    sput-object v0, LX/8of;->a:LX/8oa;

    .line 1404574
    const-class v0, LX/8of;

    sput-object v0, LX/8of;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1404571
    invoke-direct {p0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 1404572
    sget-object v0, LX/8of;->a:LX/8oa;

    iput-object v0, p0, LX/8of;->d:LX/8oa;

    return-void
.end method

.method public static a(LX/175;LX/3iT;)LX/8of;
    .locals 14

    .prologue
    .line 1404536
    sget-object v0, LX/8of;->a:LX/8oa;

    const/4 v8, 0x0

    .line 1404537
    new-instance v2, LX/8of;

    invoke-direct {v2}, LX/8of;-><init>()V

    .line 1404538
    iput-object v0, v2, LX/8of;->d:LX/8oa;

    .line 1404539
    if-eqz p0, :cond_3

    invoke-interface {p0}, LX/175;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1404540
    invoke-interface {p0}, LX/175;->b()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1404541
    invoke-interface {p0}, LX/175;->b()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v10

    .line 1404542
    new-instance v1, LX/8oe;

    invoke-direct {v1}, LX/8oe;-><init>()V

    invoke-static {v10, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1404543
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v11

    move v9, v8

    :goto_0
    if-ge v9, v11, :cond_2

    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1W5;

    .line 1404544
    :try_start_0
    invoke-interface {p0}, LX/175;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1}, LX/1W5;->c()I

    move-result v4

    invoke-interface {v1}, LX/1W5;->b()I

    move-result v5

    invoke-static {v3, v4, v5}, LX/1yM;->a(Ljava/lang/String;II)LX/1yN;

    move-result-object v12

    .line 1404545
    invoke-interface {v1}, LX/1W5;->a()LX/171;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, LX/1W5;->a()LX/171;

    move-result-object v3

    invoke-interface {v3}, LX/171;->e()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 1404546
    invoke-interface {p0}, LX/175;->a()Ljava/lang/String;

    move-result-object v3

    .line 1404547
    iget v4, v12, LX/1yN;->a:I

    move v4, v4

    .line 1404548
    invoke-virtual {v3, v8, v4}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/8of;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1404549
    invoke-interface {v1}, LX/1W5;->a()LX/171;

    move-result-object v3

    invoke-interface {v3}, LX/171;->e()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/8of;->a(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    .line 1404550
    if-eqz v3, :cond_0

    .line 1404551
    invoke-interface {v1}, LX/1W5;->a()LX/171;

    move-result-object v1

    invoke-interface {v1}, LX/171;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    .line 1404552
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-interface {p0}, LX/175;->a()Ljava/lang/String;

    move-result-object v1

    .line 1404553
    iget v5, v12, LX/1yN;->a:I

    move v5, v5

    .line 1404554
    iget v6, v12, LX/1yN;->a:I

    move v6, v6

    .line 1404555
    iget v13, v12, LX/1yN;->b:I

    move v13, v13

    .line 1404556
    add-int/2addr v6, v13

    invoke-virtual {v1, v5, v6}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v6, p1

    invoke-static/range {v2 .. v7}, LX/8of;->b(LX/8of;JLjava/lang/String;LX/3iT;Lcom/facebook/graphql/enums/GraphQLObjectType;)V

    .line 1404557
    :goto_1
    iget v1, v12, LX/1yN;->a:I

    move v1, v1

    .line 1404558
    iget v3, v12, LX/1yN;->b:I

    move v3, v3

    .line 1404559
    add-int/2addr v1, v3

    .line 1404560
    :goto_2
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    move v8, v1

    goto :goto_0

    .line 1404561
    :cond_0
    invoke-interface {p0}, LX/175;->a()Ljava/lang/String;

    move-result-object v1

    .line 1404562
    iget v3, v12, LX/1yN;->a:I

    move v3, v3

    .line 1404563
    iget v4, v12, LX/1yN;->a:I

    move v4, v4

    .line 1404564
    iget v5, v12, LX/1yN;->b:I

    move v5, v5

    .line 1404565
    add-int/2addr v4, v5

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/8of;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
    :try_end_0
    .catch LX/47A; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1404566
    :catch_0
    move-exception v1

    .line 1404567
    const-string v3, "MentionsSpannableStringBuilder"

    invoke-virtual {v1}, LX/47A;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    move v1, v8

    goto :goto_2

    .line 1404568
    :cond_2
    invoke-interface {p0}, LX/175;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0}, LX/175;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v1, v8, v3}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/8of;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1404569
    :cond_3
    move-object v0, v2

    .line 1404570
    return-object v0
.end method

.method public static a(Ljava/lang/CharSequence;LX/3iT;LX/8oi;)LX/8of;
    .locals 1

    .prologue
    .line 1404535
    sget-object v0, LX/8of;->a:LX/8oa;

    invoke-static {p0, p1, p2, v0}, LX/8of;->a(Ljava/lang/CharSequence;LX/3iT;LX/8oi;LX/8oa;)LX/8of;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/CharSequence;LX/3iT;LX/8oi;LX/8oa;)LX/8of;
    .locals 3

    .prologue
    .line 1404528
    new-instance v0, LX/8of;

    invoke-direct {v0}, LX/8of;-><init>()V

    .line 1404529
    iput-object p3, v0, LX/8of;->d:LX/8oa;

    .line 1404530
    new-instance v1, LX/8od;

    invoke-direct {v1, v0, p0, p1}, LX/8od;-><init>(LX/8of;Ljava/lang/CharSequence;LX/3iT;)V

    .line 1404531
    invoke-static {p0, v1}, LX/8oj;->a(Ljava/lang/CharSequence;LX/8oc;)I

    move-result v1

    .line 1404532
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    invoke-interface {p0, v1, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8of;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1404533
    iput-object p2, v0, LX/8of;->c:LX/8oi;

    .line 1404534
    return-object v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/Long;
    .locals 2

    .prologue
    .line 1404526
    :try_start_0
    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1404527
    :goto_0
    return-object v0

    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/8of;JLjava/lang/String;LX/3iT;Lcom/facebook/graphql/enums/GraphQLObjectType;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 1404575
    new-instance v1, Lcom/facebook/user/model/Name;

    invoke-direct {v1, v0, v0, p3}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, ""

    invoke-static {p5}, Lcom/facebook/tagging/model/TaggingProfile;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;)LX/7Gr;

    move-result-object v5

    move-object v0, p4

    move-wide v2, p1

    invoke-virtual/range {v0 .. v5}, LX/3iT;->a(Lcom/facebook/user/model/Name;JLjava/lang/String;LX/7Gr;)Lcom/facebook/tagging/model/TaggingProfile;

    move-result-object v0

    .line 1404576
    invoke-virtual {p0}, LX/8of;->length()I

    move-result v1

    .line 1404577
    iget-object v2, v0, Lcom/facebook/tagging/model/TaggingProfile;->a:Lcom/facebook/user/model/Name;

    move-object v2, v2

    .line 1404578
    invoke-virtual {v2}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/8of;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1404579
    new-instance v2, LX/7Gl;

    invoke-static {p0}, LX/8of;->d(LX/8of;)I

    move-result v3

    invoke-static {p0}, LX/8of;->c(LX/8of;)I

    move-result v4

    invoke-direct {v2, v3, v4, v0}, LX/7Gl;-><init>(IILcom/facebook/tagging/model/TaggingProfile;)V

    .line 1404580
    invoke-virtual {p0}, LX/8of;->length()I

    move-result v0

    const/16 v3, 0x21

    invoke-virtual {p0, v2, v1, v0, v3}, LX/8of;->setSpan(Ljava/lang/Object;III)V

    .line 1404581
    invoke-virtual {v2, p0, v1}, LX/7Gl;->a(Landroid/text/Editable;I)V

    .line 1404582
    return-void
.end method

.method public static c(LX/8of;)I
    .locals 1

    .prologue
    .line 1404525
    iget-object v0, p0, LX/8of;->d:LX/8oa;

    invoke-interface {v0}, LX/8oa;->b()I

    move-result v0

    return v0
.end method

.method public static d(LX/8of;)I
    .locals 1

    .prologue
    .line 1404524
    iget-object v0, p0, LX/8of;->d:LX/8oa;

    invoke-interface {v0}, LX/8oa;->a()I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(IILcom/facebook/tagging/model/TaggingProfile;)LX/8of;
    .locals 3

    .prologue
    .line 1404514
    invoke-virtual {p0, p1, p2}, LX/8of;->delete(II)Landroid/text/SpannableStringBuilder;

    .line 1404515
    iget-object v0, p3, Lcom/facebook/tagging/model/TaggingProfile;->a:Lcom/facebook/user/model/Name;

    move-object v0, v0

    .line 1404516
    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LX/8of;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1404517
    new-instance v0, LX/7Gl;

    invoke-static {p0}, LX/8of;->d(LX/8of;)I

    move-result v1

    invoke-static {p0}, LX/8of;->c(LX/8of;)I

    move-result v2

    invoke-direct {v0, v1, v2, p3}, LX/7Gl;-><init>(IILcom/facebook/tagging/model/TaggingProfile;)V

    .line 1404518
    iget-object v1, p3, Lcom/facebook/tagging/model/TaggingProfile;->a:Lcom/facebook/user/model/Name;

    move-object v1, v1

    .line 1404519
    invoke-virtual {v1}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v1, p1

    const/16 v2, 0x21

    invoke-virtual {p0, v0, p1, v1, v2}, LX/8of;->setSpan(Ljava/lang/Object;III)V

    .line 1404520
    invoke-virtual {v0, p0, p1}, LX/7Gl;->a(Landroid/text/Editable;I)V

    .line 1404521
    iget-object v0, p0, LX/8of;->c:LX/8oi;

    if-eqz v0, :cond_0

    .line 1404522
    iget-object v0, p0, LX/8of;->c:LX/8oi;

    invoke-interface {v0}, LX/8oi;->a()V

    .line 1404523
    :cond_0
    return-object p0
.end method

.method public final a(I)Z
    .locals 2

    .prologue
    .line 1404512
    add-int/lit8 v0, p1, 0x1

    const-class v1, LX/7Gl;

    invoke-virtual {p0, p1, v0, v1}, LX/8of;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7Gl;

    .line 1404513
    if-eqz v0, :cond_0

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic replace(IILjava/lang/CharSequence;II)Landroid/text/Editable;
    .locals 1

    .prologue
    .line 1404511
    invoke-virtual/range {p0 .. p5}, LX/8of;->replace(IILjava/lang/CharSequence;II)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public final replace(IILjava/lang/CharSequence;II)Landroid/text/SpannableStringBuilder;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1404499
    if-eq p1, p2, :cond_2

    .line 1404500
    invoke-interface {p3, p4, p5}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 1404501
    invoke-virtual {p0, p1, p2}, LX/8of;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1404502
    invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    move v1, v0

    .line 1404503
    :goto_0
    const-class v0, LX/7Gl;

    invoke-virtual {p0, p1, p2, v0}, LX/8of;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7Gl;

    .line 1404504
    invoke-super/range {p0 .. p5}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;II)Landroid/text/SpannableStringBuilder;

    .line 1404505
    array-length v3, v0

    :goto_1
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    .line 1404506
    invoke-virtual {v4, p0, v1}, LX/7Gl;->a(Landroid/text/Editable;Z)V

    .line 1404507
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1404508
    :cond_0
    iget-object v0, p0, LX/8of;->c:LX/8oi;

    if-eqz v0, :cond_1

    .line 1404509
    iget-object v0, p0, LX/8of;->c:LX/8oi;

    invoke-interface {v0}, LX/8oi;->a()V

    .line 1404510
    :cond_1
    return-object p0

    :cond_2
    move v1, v2

    goto :goto_0
.end method
