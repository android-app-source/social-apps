.class public final LX/8zr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8zq;


# instance fields
.field public final synthetic a:LX/8zs;


# direct methods
.method public constructor <init>(LX/8zs;)V
    .locals 0

    .prologue
    .line 1428192
    iput-object p1, p0, LX/8zr;->a:LX/8zs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0Px;LX/0ta;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/92e;",
            ">;",
            "LX/0ta;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1428193
    iget-object v0, p0, LX/8zr;->a:LX/8zs;

    .line 1428194
    iget-object p0, v0, LX/8zs;->b:LX/8zj;

    invoke-virtual {p0, p1}, LX/8zj;->a(LX/0Px;)V

    .line 1428195
    iput-object p1, v0, LX/8zs;->h:LX/0Px;

    .line 1428196
    iget-object p0, v0, LX/8zs;->g:LX/918;

    iget-object p2, v0, LX/8zs;->c:Ljava/lang/String;

    .line 1428197
    const-string v0, "activities_selector_verbs_loaded"

    invoke-static {v0, p2}, LX/918;->d(Ljava/lang/String;Ljava/lang/String;)LX/8yQ;

    move-result-object v0

    .line 1428198
    iget-object p1, v0, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v0, p1

    .line 1428199
    iget-object p1, p0, LX/918;->a:LX/0Zb;

    invoke-interface {p1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1428200
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1428201
    new-instance v0, LX/8zp;

    invoke-direct {v0, p0}, LX/8zp;-><init>(LX/8zr;)V

    .line 1428202
    invoke-static {p1}, LX/3d6;->forException(Ljava/lang/Throwable;)LX/1nY;

    move-result-object v1

    sget-object v2, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    if-ne v1, v2, :cond_0

    .line 1428203
    iget-object v1, p0, LX/8zr;->a:LX/8zs;

    iget-object v1, v1, LX/8zs;->d:LX/909;

    invoke-interface {v1, v0}, LX/909;->a(LX/8zc;)V

    .line 1428204
    :goto_0
    iget-object v0, p0, LX/8zr;->a:LX/8zs;

    iget-object v0, v0, LX/8zs;->g:LX/918;

    iget-object v1, p0, LX/8zr;->a:LX/8zs;

    iget-object v1, v1, LX/8zs;->c:Ljava/lang/String;

    .line 1428205
    const-string v2, "activities_selector_load_failed"

    invoke-static {v2, v1}, LX/918;->d(Ljava/lang/String;Ljava/lang/String;)LX/8yQ;

    move-result-object v2

    .line 1428206
    iget-object p0, v2, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v2, p0

    .line 1428207
    const-string p0, "error"

    invoke-virtual {v2, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1428208
    iget-object p0, v0, LX/918;->a:LX/0Zb;

    invoke-interface {p0, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1428209
    return-void

    .line 1428210
    :cond_0
    iget-object v1, p0, LX/8zr;->a:LX/8zs;

    iget-object v1, v1, LX/8zs;->d:LX/909;

    invoke-interface {v1, v0}, LX/909;->b(LX/8zc;)V

    goto :goto_0
.end method
