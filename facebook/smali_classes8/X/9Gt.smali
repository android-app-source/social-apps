.class public final LX/9Gt;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/graphql/model/GraphQLComment;

.field public final b:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public final c:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/21y;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/21y;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;",
            "LX/21y;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1460353
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1460354
    iput-object p1, p0, LX/9Gt;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1460355
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1460356
    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    iput-object v0, p0, LX/9Gt;->a:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1460357
    invoke-static {p1}, LX/5Op;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    iput-object v0, p0, LX/9Gt;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1460358
    iput-object p2, p0, LX/9Gt;->d:LX/21y;

    .line 1460359
    return-void
.end method
