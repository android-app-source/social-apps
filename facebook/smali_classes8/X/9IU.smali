.class public LX/9IU;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/9IS;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9IV;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1463128
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/9IU;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/9IV;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1463129
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1463130
    iput-object p1, p0, LX/9IU;->b:LX/0Ot;

    .line 1463131
    return-void
.end method

.method public static a(LX/0QB;)LX/9IU;
    .locals 4

    .prologue
    .line 1463132
    const-class v1, LX/9IU;

    monitor-enter v1

    .line 1463133
    :try_start_0
    sget-object v0, LX/9IU;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1463134
    sput-object v2, LX/9IU;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1463135
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1463136
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1463137
    new-instance v3, LX/9IU;

    const/16 p0, 0x1dee

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/9IU;-><init>(LX/0Ot;)V

    .line 1463138
    move-object v0, v3

    .line 1463139
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1463140
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/9IU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1463141
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1463142
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1463143
    const v0, -0x16a1c491

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 1463144
    check-cast p2, LX/9IT;

    .line 1463145
    iget-object v0, p0, LX/9IU;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9IV;

    iget-object v1, p2, LX/9IT;->a:Lcom/facebook/graphql/model/GraphQLComment;

    const/4 p2, 0x0

    .line 1463146
    iget-object v2, v0, LX/9IV;->a:LX/3AZ;

    const p0, 0x7f0105bd

    invoke-virtual {v2, p1, p0, p2}, LX/3AZ;->a(LX/1De;II)LX/3Aa;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->F()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p0

    invoke-static {p0}, LX/2yc;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/3Ab;

    move-result-object p0

    invoke-virtual {v2, p0}, LX/3Aa;->a(LX/3Ab;)LX/3Aa;

    move-result-object v2

    const p0, 0x7f0b0050

    invoke-virtual {v2, p0}, LX/3Aa;->n(I)LX/3Aa;

    move-result-object v2

    const p0, 0x7f0a0158

    invoke-virtual {v2, p0}, LX/3Aa;->j(I)LX/3Aa;

    move-result-object v2

    const/4 p0, 0x1

    invoke-virtual {v2, p0}, LX/3Aa;->m(I)LX/3Aa;

    move-result-object v2

    .line 1463147
    iget-object p0, v2, LX/3Aa;->a:LX/8yo;

    iput-boolean p2, p0, LX/8yo;->d:Z

    .line 1463148
    move-object v2, v2

    .line 1463149
    invoke-virtual {v2, p2}, LX/3Aa;->b(Z)LX/3Aa;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const p0, 0x7f0105be

    invoke-interface {v2, p0}, LX/1Di;->w(I)LX/1Di;

    move-result-object v2

    .line 1463150
    const p0, -0x16a1c491

    const/4 p2, 0x0

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 1463151
    invoke-interface {v2, p0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 1463152
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1463153
    invoke-static {}, LX/1dS;->b()V

    .line 1463154
    iget v0, p1, LX/1dQ;->b:I

    .line 1463155
    packed-switch v0, :pswitch_data_0

    .line 1463156
    :goto_0
    return-object v2

    .line 1463157
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1463158
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1463159
    check-cast v1, LX/9IT;

    .line 1463160
    iget-object v3, p0, LX/9IU;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/9IV;

    iget-object v4, v1, LX/9IT;->b:Lcom/facebook/api/ufiservices/FetchSingleCommentParams;

    iget-object p1, v1, LX/9IT;->c:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1463161
    new-instance p2, LX/89k;

    invoke-direct {p2}, LX/89k;-><init>()V

    .line 1463162
    iget-object p0, v4, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->d:Ljava/lang/String;

    move-object p0, p0

    .line 1463163
    iput-object p0, p2, LX/89k;->b:Ljava/lang/String;

    .line 1463164
    move-object p2, p2

    .line 1463165
    iget-object p0, v4, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->e:Ljava/lang/String;

    move-object p0, p0

    .line 1463166
    iput-object p0, p2, LX/89k;->c:Ljava/lang/String;

    .line 1463167
    move-object p2, p2

    .line 1463168
    iput-object p1, p2, LX/89k;->p:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1463169
    move-object p2, p2

    .line 1463170
    invoke-virtual {p2}, LX/89k;->a()Lcom/facebook/ipc/feed/PermalinkStoryIdParams;

    move-result-object p0

    .line 1463171
    iget-object p2, v3, LX/9IV;->c:LX/0Ot;

    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/0hy;

    invoke-interface {p2, p0}, LX/0hy;->a(Lcom/facebook/ipc/feed/PermalinkStoryIdParams;)Landroid/content/Intent;

    move-result-object p0

    .line 1463172
    iget-object p2, v3, LX/9IV;->b:LX/0Ot;

    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {p2, p0, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1463173
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x16a1c491
        :pswitch_0
    .end packed-switch
.end method
