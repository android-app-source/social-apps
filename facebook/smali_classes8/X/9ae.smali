.class public final enum LX/9ae;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9ae;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9ae;

.field public static final enum ALBUM_CREATED:LX/9ae;

.field public static final enum ALBUM_CREATE_FAILED:LX/9ae;

.field public static final enum ALBUM_CREATOR_CANCELLED:LX/9ae;

.field public static final enum ALBUM_CREATOR_DIALOG_SHOWN:LX/9ae;

.field public static final enum ALBUM_CREATOR_OPENED:LX/9ae;

.field public static final enum ALBUM_DESCRIPTION_TEXT_ENTERED:LX/9ae;

.field public static final enum ALBUM_LOCATION_CHANGED:LX/9ae;

.field public static final enum ALBUM_PRIVACY_SELECTOR_OPENED:LX/9ae;

.field public static final enum ALBUM_TITLE_TEXT_ENTERED:LX/9ae;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1513918
    new-instance v0, LX/9ae;

    const-string v1, "ALBUM_CREATOR_OPENED"

    const-string v2, "album_creator_opened"

    invoke-direct {v0, v1, v4, v2}, LX/9ae;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9ae;->ALBUM_CREATOR_OPENED:LX/9ae;

    .line 1513919
    new-instance v0, LX/9ae;

    const-string v1, "ALBUM_CREATOR_CANCELLED"

    const-string v2, "album_creator_cancelled"

    invoke-direct {v0, v1, v5, v2}, LX/9ae;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9ae;->ALBUM_CREATOR_CANCELLED:LX/9ae;

    .line 1513920
    new-instance v0, LX/9ae;

    const-string v1, "ALBUM_CREATED"

    const-string v2, "album_created"

    invoke-direct {v0, v1, v6, v2}, LX/9ae;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9ae;->ALBUM_CREATED:LX/9ae;

    .line 1513921
    new-instance v0, LX/9ae;

    const-string v1, "ALBUM_CREATE_FAILED"

    const-string v2, "album_create_failed"

    invoke-direct {v0, v1, v7, v2}, LX/9ae;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9ae;->ALBUM_CREATE_FAILED:LX/9ae;

    .line 1513922
    new-instance v0, LX/9ae;

    const-string v1, "ALBUM_TITLE_TEXT_ENTERED"

    const-string v2, "album_title_text_entered"

    invoke-direct {v0, v1, v8, v2}, LX/9ae;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9ae;->ALBUM_TITLE_TEXT_ENTERED:LX/9ae;

    .line 1513923
    new-instance v0, LX/9ae;

    const-string v1, "ALBUM_DESCRIPTION_TEXT_ENTERED"

    const/4 v2, 0x5

    const-string v3, "album_description_text_entered"

    invoke-direct {v0, v1, v2, v3}, LX/9ae;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9ae;->ALBUM_DESCRIPTION_TEXT_ENTERED:LX/9ae;

    .line 1513924
    new-instance v0, LX/9ae;

    const-string v1, "ALBUM_PRIVACY_SELECTOR_OPENED"

    const/4 v2, 0x6

    const-string v3, "album_privacy_selector_opened"

    invoke-direct {v0, v1, v2, v3}, LX/9ae;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9ae;->ALBUM_PRIVACY_SELECTOR_OPENED:LX/9ae;

    .line 1513925
    new-instance v0, LX/9ae;

    const-string v1, "ALBUM_LOCATION_CHANGED"

    const/4 v2, 0x7

    const-string v3, "album_location_changed"

    invoke-direct {v0, v1, v2, v3}, LX/9ae;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9ae;->ALBUM_LOCATION_CHANGED:LX/9ae;

    .line 1513926
    new-instance v0, LX/9ae;

    const-string v1, "ALBUM_CREATOR_DIALOG_SHOWN"

    const/16 v2, 0x8

    const-string v3, "album_creator_dialog_shown"

    invoke-direct {v0, v1, v2, v3}, LX/9ae;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9ae;->ALBUM_CREATOR_DIALOG_SHOWN:LX/9ae;

    .line 1513927
    const/16 v0, 0x9

    new-array v0, v0, [LX/9ae;

    sget-object v1, LX/9ae;->ALBUM_CREATOR_OPENED:LX/9ae;

    aput-object v1, v0, v4

    sget-object v1, LX/9ae;->ALBUM_CREATOR_CANCELLED:LX/9ae;

    aput-object v1, v0, v5

    sget-object v1, LX/9ae;->ALBUM_CREATED:LX/9ae;

    aput-object v1, v0, v6

    sget-object v1, LX/9ae;->ALBUM_CREATE_FAILED:LX/9ae;

    aput-object v1, v0, v7

    sget-object v1, LX/9ae;->ALBUM_TITLE_TEXT_ENTERED:LX/9ae;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/9ae;->ALBUM_DESCRIPTION_TEXT_ENTERED:LX/9ae;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/9ae;->ALBUM_PRIVACY_SELECTOR_OPENED:LX/9ae;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/9ae;->ALBUM_LOCATION_CHANGED:LX/9ae;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/9ae;->ALBUM_CREATOR_DIALOG_SHOWN:LX/9ae;

    aput-object v2, v0, v1

    sput-object v0, LX/9ae;->$VALUES:[LX/9ae;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1513915
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1513916
    iput-object p3, p0, LX/9ae;->name:Ljava/lang/String;

    .line 1513917
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9ae;
    .locals 1

    .prologue
    .line 1513928
    const-class v0, LX/9ae;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9ae;

    return-object v0
.end method

.method public static values()[LX/9ae;
    .locals 1

    .prologue
    .line 1513914
    sget-object v0, LX/9ae;->$VALUES:[LX/9ae;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9ae;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1513913
    iget-object v0, p0, LX/9ae;->name:Ljava/lang/String;

    return-object v0
.end method
