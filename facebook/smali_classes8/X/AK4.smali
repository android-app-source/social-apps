.class public LX/AK4;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/facebook/audience/snacks/model/SnackThreadMetadata;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/user/model/User;


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1662557
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1662558
    new-instance v0, LX/AK3;

    invoke-direct {v0, p0}, LX/AK3;-><init>(LX/AK4;)V

    iput-object v0, p0, LX/AK4;->a:Ljava/util/Comparator;

    .line 1662559
    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    iput-object v0, p0, LX/AK4;->b:Lcom/facebook/user/model/User;

    .line 1662560
    return-void
.end method

.method private static a(LX/AK4;)Lcom/facebook/audience/model/AudienceControlData;
    .locals 3

    .prologue
    .line 1662601
    invoke-static {}, Lcom/facebook/audience/model/AudienceControlData;->newBuilder()Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v0

    iget-object v1, p0, LX/AK4;->b:Lcom/facebook/user/model/User;

    .line 1662602
    iget-object v2, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1662603
    invoke-virtual {v0, v1}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setId(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v0

    iget-object v1, p0, LX/AK4;->b:Lcom/facebook/user/model/User;

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setName(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v0

    iget-object v1, p0, LX/AK4;->b:Lcom/facebook/user/model/User;

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setProfileUri(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v0

    iget-object v1, p0, LX/AK4;->b:Lcom/facebook/user/model/User;

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setLowResProfileUri(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/model/AudienceControlData$Builder;->a()Lcom/facebook/audience/model/AudienceControlData;

    move-result-object v0

    return-object v0
.end method

.method public static c(LX/AK4;Ljava/util/List;)Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/audience/snacks/model/SnackThreadMetadata;",
            ">;)",
            "Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1662604
    iget-object v0, p0, LX/AK4;->a:Ljava/util/Comparator;

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1662605
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;

    .line 1662606
    iget-object v2, v0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->d:Lcom/facebook/audience/model/AudienceControlData;

    move-object v3, v2

    .line 1662607
    new-instance v2, LX/AKB;

    invoke-direct {v2}, LX/AKB;-><init>()V

    move-object v4, v2

    .line 1662608
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v1

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;

    .line 1662609
    iget-boolean p0, v1, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->c:Z

    move v1, p0

    .line 1662610
    if-nez v1, :cond_2

    .line 1662611
    add-int/lit8 v1, v2, 0x1

    :goto_1
    move v2, v1

    .line 1662612
    goto :goto_0

    .line 1662613
    :cond_0
    iput v2, v4, LX/AKB;->e:I

    .line 1662614
    move-object v1, v4

    .line 1662615
    iput-object v3, v1, LX/AKB;->b:Lcom/facebook/audience/model/AudienceControlData;

    .line 1662616
    move-object v1, v1

    .line 1662617
    invoke-virtual {v3}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v2

    .line 1662618
    iput-object v2, v1, LX/AKB;->a:Ljava/lang/String;

    .line 1662619
    move-object v1, v1

    .line 1662620
    iget-object v2, v0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->f:Ljava/lang/String;

    move-object v0, v2

    .line 1662621
    iput-object v0, v1, LX/AKB;->c:Ljava/lang/String;

    .line 1662622
    move-object v0, v1

    .line 1662623
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 1662624
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;

    .line 1662625
    new-instance v9, LX/AK9;

    invoke-direct {v9}, LX/AK9;-><init>()V

    move-object v9, v9

    .line 1662626
    iget-wide v12, v6, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->i:J

    move-wide v10, v12

    .line 1662627
    iput-wide v10, v9, LX/AK9;->d:J

    .line 1662628
    move-object v9, v9

    .line 1662629
    iget-object v10, v6, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->b:Ljava/lang/String;

    move-object v10, v10

    .line 1662630
    iput-object v10, v9, LX/AK9;->a:Ljava/lang/String;

    .line 1662631
    move-object v9, v9

    .line 1662632
    iget-object v10, v6, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->j:Ljava/lang/String;

    move-object v10, v10

    .line 1662633
    iput-object v10, v9, LX/AK9;->e:Ljava/lang/String;

    .line 1662634
    move-object v9, v9

    .line 1662635
    iget-object v10, v6, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->e:Ljava/lang/String;

    move-object v10, v10

    .line 1662636
    iput-object v10, v9, LX/AK9;->b:Ljava/lang/String;

    .line 1662637
    move-object v9, v9

    .line 1662638
    iget-boolean v10, v6, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->c:Z

    move v6, v10

    .line 1662639
    iput-boolean v6, v9, LX/AK9;->c:Z

    .line 1662640
    move-object v6, v9

    .line 1662641
    new-instance v9, Lcom/facebook/audience/snacks/model/SnacksFriendThread;

    invoke-direct {v9, v6}, Lcom/facebook/audience/snacks/model/SnacksFriendThread;-><init>(LX/AK9;)V

    move-object v6, v9

    .line 1662642
    invoke-virtual {v7, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 1662643
    :cond_1
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    move-object v1, v6

    .line 1662644
    iput-object v1, v0, LX/AKB;->d:LX/0Px;

    .line 1662645
    move-object v0, v0

    .line 1662646
    new-instance v1, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;

    invoke-direct {v1, v0}, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;-><init>(LX/AKB;)V

    move-object v0, v1

    .line 1662647
    return-object v0

    :cond_2
    move v1, v2

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/0Px;)Lcom/facebook/audience/snacks/model/SnacksMyUserThread;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/snacks/model/SnackThreadMetadata;",
            ">;)",
            "Lcom/facebook/audience/snacks/model/SnacksMyUserThread;"
        }
    .end annotation

    .prologue
    .line 1662561
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1662562
    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1662563
    iget-object v0, p0, LX/AK4;->a:Ljava/util/Comparator;

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1662564
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;

    .line 1662565
    iget-object v2, v0, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->f:Ljava/lang/String;

    move-object v0, v2

    .line 1662566
    :goto_0
    invoke-static {}, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->newBuilder()LX/AKK;

    move-result-object v2

    iget-object v3, p0, LX/AK4;->b:Lcom/facebook/user/model/User;

    .line 1662567
    iget-object p1, v3, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v3, p1

    .line 1662568
    iput-object v3, v2, LX/AKK;->a:Ljava/lang/String;

    .line 1662569
    move-object v2, v2

    .line 1662570
    invoke-static {p0}, LX/AK4;->a(LX/AK4;)Lcom/facebook/audience/model/AudienceControlData;

    move-result-object v3

    .line 1662571
    iput-object v3, v2, LX/AKK;->b:Lcom/facebook/audience/model/AudienceControlData;

    .line 1662572
    move-object v2, v2

    .line 1662573
    iput-object v0, v2, LX/AKK;->c:Ljava/lang/String;

    .line 1662574
    move-object v0, v2

    .line 1662575
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 1662576
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;

    .line 1662577
    new-instance v7, LX/AKI;

    invoke-direct {v7}, LX/AKI;-><init>()V

    move-object v7, v7

    .line 1662578
    iget-wide v10, v4, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->i:J

    move-wide v8, v10

    .line 1662579
    iput-wide v8, v7, LX/AKI;->f:J

    .line 1662580
    move-object v7, v7

    .line 1662581
    iget-object v8, v4, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->b:Ljava/lang/String;

    move-object v8, v8

    .line 1662582
    iput-object v8, v7, LX/AKI;->a:Ljava/lang/String;

    .line 1662583
    move-object v7, v7

    .line 1662584
    iget-object v8, v4, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->j:Ljava/lang/String;

    move-object v8, v8

    .line 1662585
    iput-object v8, v7, LX/AKI;->g:Ljava/lang/String;

    .line 1662586
    move-object v7, v7

    .line 1662587
    iget-object v8, v4, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->e:Ljava/lang/String;

    move-object v8, v8

    .line 1662588
    iput-object v8, v7, LX/AKI;->b:Ljava/lang/String;

    .line 1662589
    move-object v7, v7

    .line 1662590
    iget v8, v4, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->h:I

    move v4, v8

    .line 1662591
    iput v4, v7, LX/AKI;->e:I

    .line 1662592
    move-object v4, v7

    .line 1662593
    new-instance v7, Lcom/facebook/audience/snacks/model/SnacksMyThread;

    invoke-direct {v7, v4}, Lcom/facebook/audience/snacks/model/SnacksMyThread;-><init>(LX/AKI;)V

    move-object v4, v7

    .line 1662594
    invoke-virtual {v5, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1662595
    :cond_0
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    move-object v1, v4

    .line 1662596
    iput-object v1, v0, LX/AKK;->d:LX/0Px;

    .line 1662597
    move-object v0, v0

    .line 1662598
    invoke-virtual {v0}, LX/AKK;->a()Lcom/facebook/audience/snacks/model/SnacksMyUserThread;

    move-result-object v0

    .line 1662599
    return-object v0

    .line 1662600
    :cond_1
    invoke-static {p0}, LX/AK4;->a(LX/AK4;)Lcom/facebook/audience/model/AudienceControlData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/model/AudienceControlData;->getLowResProfileUri()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
