.class public final LX/9z5;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 40

    .prologue
    .line 1599642
    const/16 v36, 0x0

    .line 1599643
    const/16 v35, 0x0

    .line 1599644
    const/16 v34, 0x0

    .line 1599645
    const/16 v33, 0x0

    .line 1599646
    const/16 v32, 0x0

    .line 1599647
    const/16 v31, 0x0

    .line 1599648
    const/16 v30, 0x0

    .line 1599649
    const/16 v29, 0x0

    .line 1599650
    const/16 v28, 0x0

    .line 1599651
    const/16 v27, 0x0

    .line 1599652
    const/16 v26, 0x0

    .line 1599653
    const/16 v25, 0x0

    .line 1599654
    const/16 v24, 0x0

    .line 1599655
    const/16 v23, 0x0

    .line 1599656
    const/16 v22, 0x0

    .line 1599657
    const/16 v21, 0x0

    .line 1599658
    const/16 v20, 0x0

    .line 1599659
    const/16 v19, 0x0

    .line 1599660
    const/16 v18, 0x0

    .line 1599661
    const/16 v17, 0x0

    .line 1599662
    const/16 v16, 0x0

    .line 1599663
    const/4 v15, 0x0

    .line 1599664
    const/4 v14, 0x0

    .line 1599665
    const/4 v13, 0x0

    .line 1599666
    const/4 v12, 0x0

    .line 1599667
    const/4 v11, 0x0

    .line 1599668
    const/4 v10, 0x0

    .line 1599669
    const/4 v9, 0x0

    .line 1599670
    const/4 v8, 0x0

    .line 1599671
    const/4 v7, 0x0

    .line 1599672
    const/4 v6, 0x0

    .line 1599673
    const/4 v5, 0x0

    .line 1599674
    const/4 v4, 0x0

    .line 1599675
    const/4 v3, 0x0

    .line 1599676
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v37

    sget-object v38, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v37

    move-object/from16 v1, v38

    if-eq v0, v1, :cond_1

    .line 1599677
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1599678
    const/4 v3, 0x0

    .line 1599679
    :goto_0
    return v3

    .line 1599680
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1599681
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v37

    sget-object v38, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v37

    move-object/from16 v1, v38

    if-eq v0, v1, :cond_1e

    .line 1599682
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v37

    .line 1599683
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1599684
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v38

    sget-object v39, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v38

    move-object/from16 v1, v39

    if-eq v0, v1, :cond_1

    if-eqz v37, :cond_1

    .line 1599685
    const-string v38, "__type__"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-nez v38, :cond_2

    const-string v38, "__typename"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_3

    .line 1599686
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v36

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v36

    goto :goto_1

    .line 1599687
    :cond_3
    const-string v38, "accessibility_caption"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_4

    .line 1599688
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v35

    goto :goto_1

    .line 1599689
    :cond_4
    const-string v38, "creation_story"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_5

    .line 1599690
    invoke-static/range {p0 .. p1}, LX/8Iy;->a(LX/15w;LX/186;)I

    move-result v34

    goto :goto_1

    .line 1599691
    :cond_5
    const-string v38, "external_url"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_6

    .line 1599692
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v33

    goto :goto_1

    .line 1599693
    :cond_6
    const-string v38, "feedback"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_7

    .line 1599694
    invoke-static/range {p0 .. p1}, LX/4aV;->a(LX/15w;LX/186;)I

    move-result v32

    goto/16 :goto_1

    .line 1599695
    :cond_7
    const-string v38, "focus"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_8

    .line 1599696
    invoke-static/range {p0 .. p1}, LX/4aC;->a(LX/15w;LX/186;)I

    move-result v31

    goto/16 :goto_1

    .line 1599697
    :cond_8
    const-string v38, "height"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_9

    .line 1599698
    const/4 v8, 0x1

    .line 1599699
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v30

    goto/16 :goto_1

    .line 1599700
    :cond_9
    const-string v38, "id"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_a

    .line 1599701
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v29

    goto/16 :goto_1

    .line 1599702
    :cond_a
    const-string v38, "image"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_b

    .line 1599703
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v28

    goto/16 :goto_1

    .line 1599704
    :cond_b
    const-string v38, "imageHigh"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_c

    .line 1599705
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v27

    goto/16 :goto_1

    .line 1599706
    :cond_c
    const-string v38, "imageLow"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_d

    .line 1599707
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v26

    goto/16 :goto_1

    .line 1599708
    :cond_d
    const-string v38, "imageMedium"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_e

    .line 1599709
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v25

    goto/16 :goto_1

    .line 1599710
    :cond_e
    const-string v38, "imageThumbnail"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_f

    .line 1599711
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v24

    goto/16 :goto_1

    .line 1599712
    :cond_f
    const-string v38, "is_looping"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_10

    .line 1599713
    const/4 v7, 0x1

    .line 1599714
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v23

    goto/16 :goto_1

    .line 1599715
    :cond_10
    const-string v38, "is_playable"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_11

    .line 1599716
    const/4 v6, 0x1

    .line 1599717
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v22

    goto/16 :goto_1

    .line 1599718
    :cond_11
    const-string v38, "landscape"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_12

    .line 1599719
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v21

    goto/16 :goto_1

    .line 1599720
    :cond_12
    const-string v38, "largePortraitImage"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_13

    .line 1599721
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v20

    goto/16 :goto_1

    .line 1599722
    :cond_13
    const-string v38, "largeThumbnail"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_14

    .line 1599723
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 1599724
    :cond_14
    const-string v38, "link_media"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_15

    .line 1599725
    invoke-static/range {p0 .. p1}, LX/9yy;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 1599726
    :cond_15
    const-string v38, "loop_count"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_16

    .line 1599727
    const/4 v5, 0x1

    .line 1599728
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v17

    goto/16 :goto_1

    .line 1599729
    :cond_16
    const-string v38, "narrowLandscapeImage"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_17

    .line 1599730
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v16

    goto/16 :goto_1

    .line 1599731
    :cond_17
    const-string v38, "narrowPortraitImage"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_18

    .line 1599732
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 1599733
    :cond_18
    const-string v38, "photo_encodings"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_19

    .line 1599734
    invoke-static/range {p0 .. p1}, LX/8Iz;->a(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 1599735
    :cond_19
    const-string v38, "playable_duration_in_ms"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_1a

    .line 1599736
    const/4 v4, 0x1

    .line 1599737
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v13

    goto/16 :goto_1

    .line 1599738
    :cond_1a
    const-string v38, "playable_url"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_1b

    .line 1599739
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto/16 :goto_1

    .line 1599740
    :cond_1b
    const-string v38, "portrait"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_1c

    .line 1599741
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 1599742
    :cond_1c
    const-string v38, "squareLargeImage"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_1d

    .line 1599743
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v10

    goto/16 :goto_1

    .line 1599744
    :cond_1d
    const-string v38, "width"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v37

    if-eqz v37, :cond_0

    .line 1599745
    const/4 v3, 0x1

    .line 1599746
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v9

    goto/16 :goto_1

    .line 1599747
    :cond_1e
    const/16 v37, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1599748
    const/16 v37, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v37

    move/from16 v2, v36

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1599749
    const/16 v36, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v36

    move/from16 v2, v35

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1599750
    const/16 v35, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v35

    move/from16 v2, v34

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1599751
    const/16 v34, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v34

    move/from16 v2, v33

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1599752
    const/16 v33, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v33

    move/from16 v2, v32

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1599753
    const/16 v32, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v32

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1599754
    if-eqz v8, :cond_1f

    .line 1599755
    const/4 v8, 0x6

    const/16 v31, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v0, v8, v1, v2}, LX/186;->a(III)V

    .line 1599756
    :cond_1f
    const/4 v8, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 1599757
    const/16 v8, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 1599758
    const/16 v8, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 1599759
    const/16 v8, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 1599760
    const/16 v8, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 1599761
    const/16 v8, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 1599762
    if-eqz v7, :cond_20

    .line 1599763
    const/16 v7, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 1599764
    :cond_20
    if-eqz v6, :cond_21

    .line 1599765
    const/16 v6, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 1599766
    :cond_21
    const/16 v6, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 1599767
    const/16 v6, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 1599768
    const/16 v6, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 1599769
    const/16 v6, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 1599770
    if-eqz v5, :cond_22

    .line 1599771
    const/16 v5, 0x13

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v5, v1, v6}, LX/186;->a(III)V

    .line 1599772
    :cond_22
    const/16 v5, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 1599773
    const/16 v5, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v15}, LX/186;->b(II)V

    .line 1599774
    const/16 v5, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v14}, LX/186;->b(II)V

    .line 1599775
    if-eqz v4, :cond_23

    .line 1599776
    const/16 v4, 0x17

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v13, v5}, LX/186;->a(III)V

    .line 1599777
    :cond_23
    const/16 v4, 0x18

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12}, LX/186;->b(II)V

    .line 1599778
    const/16 v4, 0x19

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->b(II)V

    .line 1599779
    const/16 v4, 0x1a

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->b(II)V

    .line 1599780
    if-eqz v3, :cond_24

    .line 1599781
    const/16 v3, 0x1b

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9, v4}, LX/186;->a(III)V

    .line 1599782
    :cond_24
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1599527
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1599528
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1599529
    if-eqz v0, :cond_0

    .line 1599530
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1599531
    invoke-static {p0, p1, v2, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1599532
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1599533
    if-eqz v0, :cond_1

    .line 1599534
    const-string v1, "accessibility_caption"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1599535
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1599536
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1599537
    if-eqz v0, :cond_2

    .line 1599538
    const-string v1, "creation_story"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1599539
    invoke-static {p0, v0, p2, p3}, LX/8Iy;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1599540
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1599541
    if-eqz v0, :cond_3

    .line 1599542
    const-string v1, "external_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1599543
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1599544
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1599545
    if-eqz v0, :cond_4

    .line 1599546
    const-string v1, "feedback"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1599547
    invoke-static {p0, v0, p2, p3}, LX/4aV;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1599548
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1599549
    if-eqz v0, :cond_5

    .line 1599550
    const-string v1, "focus"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1599551
    invoke-static {p0, v0, p2}, LX/4aC;->a(LX/15i;ILX/0nX;)V

    .line 1599552
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1599553
    if-eqz v0, :cond_6

    .line 1599554
    const-string v1, "height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1599555
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1599556
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1599557
    if-eqz v0, :cond_7

    .line 1599558
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1599559
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1599560
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1599561
    if-eqz v0, :cond_8

    .line 1599562
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1599563
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1599564
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1599565
    if-eqz v0, :cond_9

    .line 1599566
    const-string v1, "imageHigh"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1599567
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1599568
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1599569
    if-eqz v0, :cond_a

    .line 1599570
    const-string v1, "imageLow"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1599571
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1599572
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1599573
    if-eqz v0, :cond_b

    .line 1599574
    const-string v1, "imageMedium"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1599575
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1599576
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1599577
    if-eqz v0, :cond_c

    .line 1599578
    const-string v1, "imageThumbnail"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1599579
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1599580
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1599581
    if-eqz v0, :cond_d

    .line 1599582
    const-string v1, "is_looping"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1599583
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1599584
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1599585
    if-eqz v0, :cond_e

    .line 1599586
    const-string v1, "is_playable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1599587
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1599588
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1599589
    if-eqz v0, :cond_f

    .line 1599590
    const-string v1, "landscape"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1599591
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1599592
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1599593
    if-eqz v0, :cond_10

    .line 1599594
    const-string v1, "largePortraitImage"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1599595
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1599596
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1599597
    if-eqz v0, :cond_11

    .line 1599598
    const-string v1, "largeThumbnail"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1599599
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1599600
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1599601
    if-eqz v0, :cond_12

    .line 1599602
    const-string v1, "link_media"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1599603
    invoke-static {p0, v0, p2, p3}, LX/9yy;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1599604
    :cond_12
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1599605
    if-eqz v0, :cond_13

    .line 1599606
    const-string v1, "loop_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1599607
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1599608
    :cond_13
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1599609
    if-eqz v0, :cond_14

    .line 1599610
    const-string v1, "narrowLandscapeImage"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1599611
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1599612
    :cond_14
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1599613
    if-eqz v0, :cond_15

    .line 1599614
    const-string v1, "narrowPortraitImage"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1599615
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1599616
    :cond_15
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1599617
    if-eqz v0, :cond_16

    .line 1599618
    const-string v1, "photo_encodings"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1599619
    invoke-static {p0, v0, p2, p3}, LX/8Iz;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1599620
    :cond_16
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1599621
    if-eqz v0, :cond_17

    .line 1599622
    const-string v1, "playable_duration_in_ms"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1599623
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1599624
    :cond_17
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1599625
    if-eqz v0, :cond_18

    .line 1599626
    const-string v1, "playable_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1599627
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1599628
    :cond_18
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1599629
    if-eqz v0, :cond_19

    .line 1599630
    const-string v1, "portrait"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1599631
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1599632
    :cond_19
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1599633
    if-eqz v0, :cond_1a

    .line 1599634
    const-string v1, "squareLargeImage"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1599635
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1599636
    :cond_1a
    const/16 v0, 0x1b

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1599637
    if-eqz v0, :cond_1b

    .line 1599638
    const-string v1, "width"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1599639
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1599640
    :cond_1b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1599641
    return-void
.end method
