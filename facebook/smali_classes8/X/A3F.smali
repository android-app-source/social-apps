.class public final LX/A3F;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 1615912
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1615913
    invoke-virtual {p0, p1, v3, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1615914
    if-eqz v0, :cond_0

    .line 1615915
    const-string v1, "electoral_vote_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1615916
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1615917
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1615918
    if-eqz v0, :cond_1

    .line 1615919
    const-string v1, "fbid"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1615920
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1615921
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1615922
    if-eqz v0, :cond_2

    .line 1615923
    const-string v1, "is_winner"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1615924
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1615925
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1615926
    if-eqz v0, :cond_3

    .line 1615927
    const-string v1, "party"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1615928
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1615929
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1615930
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_4

    .line 1615931
    const-string v2, "unformatted_vote_percentage"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1615932
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1615933
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1615934
    if-eqz v0, :cond_5

    .line 1615935
    const-string v1, "vote_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1615936
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1615937
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1615938
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 17

    .prologue
    .line 1615939
    const/4 v12, 0x0

    .line 1615940
    const/4 v11, 0x0

    .line 1615941
    const/4 v10, 0x0

    .line 1615942
    const/4 v7, 0x0

    .line 1615943
    const-wide/16 v8, 0x0

    .line 1615944
    const/4 v6, 0x0

    .line 1615945
    const/4 v5, 0x0

    .line 1615946
    const/4 v4, 0x0

    .line 1615947
    const/4 v3, 0x0

    .line 1615948
    const/4 v2, 0x0

    .line 1615949
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v13, v14, :cond_c

    .line 1615950
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1615951
    const/4 v2, 0x0

    .line 1615952
    :goto_0
    return v2

    .line 1615953
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v14, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v14, :cond_7

    .line 1615954
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1615955
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1615956
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v14, v15, :cond_0

    if-eqz v2, :cond_0

    .line 1615957
    const-string v14, "electoral_vote_count"

    invoke-virtual {v2, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_1

    .line 1615958
    const/4 v2, 0x1

    .line 1615959
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v7

    move v13, v7

    move v7, v2

    goto :goto_1

    .line 1615960
    :cond_1
    const-string v14, "fbid"

    invoke-virtual {v2, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 1615961
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v12, v2

    goto :goto_1

    .line 1615962
    :cond_2
    const-string v14, "is_winner"

    invoke-virtual {v2, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 1615963
    const/4 v2, 0x1

    .line 1615964
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v11, v6

    move v6, v2

    goto :goto_1

    .line 1615965
    :cond_3
    const-string v14, "party"

    invoke-virtual {v2, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 1615966
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v10, v2

    goto :goto_1

    .line 1615967
    :cond_4
    const-string v14, "unformatted_vote_percentage"

    invoke-virtual {v2, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 1615968
    const/4 v2, 0x1

    .line 1615969
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 1615970
    :cond_5
    const-string v14, "vote_count"

    invoke-virtual {v2, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1615971
    const/4 v2, 0x1

    .line 1615972
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v8

    move v9, v8

    move v8, v2

    goto :goto_1

    .line 1615973
    :cond_6
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1615974
    :cond_7
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1615975
    if-eqz v7, :cond_8

    .line 1615976
    const/4 v2, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13, v7}, LX/186;->a(III)V

    .line 1615977
    :cond_8
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1615978
    if-eqz v6, :cond_9

    .line 1615979
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->a(IZ)V

    .line 1615980
    :cond_9
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1615981
    if-eqz v3, :cond_a

    .line 1615982
    const/4 v3, 0x4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1615983
    :cond_a
    if-eqz v8, :cond_b

    .line 1615984
    const/4 v2, 0x5

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9, v3}, LX/186;->a(III)V

    .line 1615985
    :cond_b
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_c
    move v13, v12

    move v12, v11

    move v11, v10

    move v10, v7

    move v7, v5

    move/from16 v16, v6

    move v6, v4

    move-wide v4, v8

    move/from16 v9, v16

    move v8, v2

    goto/16 :goto_1
.end method
