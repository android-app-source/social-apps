.class public LX/A7L;
.super Landroid/graphics/drawable/StateListDrawable;
.source ""


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<[I>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1625875
    invoke-direct {p0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 1625876
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/A7L;->b:Ljava/util/List;

    .line 1625877
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/A7L;->a:Ljava/util/List;

    .line 1625878
    return-void
.end method


# virtual methods
.method public final a([ILjava/lang/Integer;Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 1625879
    iget-object v0, p0, LX/A7L;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1625880
    iget-object v0, p0, LX/A7L;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1625881
    invoke-super {p0, p1, p3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 1625882
    return-void
.end method

.method public final addState([ILandroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 1625883
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, LX/A7L;->a([ILjava/lang/Integer;Landroid/graphics/drawable/Drawable;)V

    .line 1625884
    return-void
.end method

.method public final onStateChange([I)Z
    .locals 3

    .prologue
    .line 1625885
    invoke-super {p0, p1}, Landroid/graphics/drawable/StateListDrawable;->onStateChange([I)Z

    move-result v2

    .line 1625886
    iget-object v0, p0, LX/A7L;->a:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1625887
    invoke-virtual {p0}, LX/A7L;->clearColorFilter()V

    .line 1625888
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/A7L;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1625889
    iget-object v0, p0, LX/A7L;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    invoke-static {v0, p1}, Landroid/util/StateSet;->stateSetMatches([I[I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1625890
    iget-object v0, p0, LX/A7L;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1625891
    iget-object v0, p0, LX/A7L;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p0, v0, v1}, LX/A7L;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 1625892
    :cond_0
    return v2

    .line 1625893
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method
