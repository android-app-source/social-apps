.class public LX/9F5;
.super LX/99a;
.source ""

# interfaces
.implements LX/21l;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/99a;",
        "LX/21l",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/1Cq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cq",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Qq;LX/1Cq;)V
    .locals 0
    .param p1    # LX/1Qq;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1Cq;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Qq;",
            "LX/1Cq",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1458033
    invoke-direct {p0, p1}, LX/99a;-><init>(LX/1Qq;)V

    .line 1458034
    iput-object p2, p0, LX/9F5;->a:LX/1Cq;

    .line 1458035
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Integer;)V
    .locals 1

    .prologue
    .line 1458036
    iget-object v0, p0, LX/9F5;->a:LX/1Cq;

    .line 1458037
    iput-object p1, v0, LX/1Cq;->a:Ljava/lang/Object;

    .line 1458038
    invoke-virtual {p0}, LX/99a;->notifyDataSetChanged()V

    .line 1458039
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1458040
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, LX/9F5;->a(Ljava/lang/Integer;)V

    return-void
.end method
