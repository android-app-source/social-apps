.class public final LX/9D9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/9DG;


# direct methods
.method public constructor <init>(LX/9DG;)V
    .locals 0

    .prologue
    .line 1454780
    iput-object p1, p0, LX/9D9;->a:LX/9DG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1454781
    iget-object v0, p0, LX/9D9;->a:LX/9DG;

    iget-object v0, v0, LX/9DG;->B:LX/03V;

    sget-object v1, LX/9DG;->a:Ljava/lang/String;

    const-string v2, "Failed on GraphQLSubscription callback"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1454782
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 8
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1454783
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1454784
    if-eqz p1, :cond_1

    .line 1454785
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1454786
    if-eqz v0, :cond_1

    .line 1454787
    iget-object v1, p0, LX/9D9;->a:LX/9DG;

    .line 1454788
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1454789
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    const/4 v6, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1454790
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->N()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {v0}, LX/16z;->f(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    move-object v2, v6

    .line 1454791
    :goto_0
    move-object v0, v2

    .line 1454792
    iget-object v1, p0, LX/9D9;->a:LX/9DG;

    iget-object v1, v1, LX/9DG;->f:LX/9D1;

    invoke-virtual {v1}, LX/9D1;->d()V

    .line 1454793
    iget-object v1, p0, LX/9D9;->a:LX/9DG;

    iget-object v1, v1, LX/9DG;->y:LX/0QK;

    .line 1454794
    iget-object v2, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v2, v2

    .line 1454795
    invoke-interface {v1, v2}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1454796
    iget-object v1, p0, LX/9D9;->a:LX/9DG;

    iget-object v1, v1, LX/9DG;->f:LX/9D1;

    invoke-virtual {v1}, LX/9D1;->e()V

    .line 1454797
    if-eqz v0, :cond_1

    .line 1454798
    iget-object v1, p0, LX/9D9;->a:LX/9DG;

    iget-object v1, v1, LX/9DG;->f:LX/9D1;

    invoke-virtual {v1, v0}, LX/9D1;->a(Lcom/facebook/graphql/model/GraphQLComment;)Z

    .line 1454799
    :cond_1
    return-void

    .line 1454800
    :cond_2
    invoke-static {v0, v4}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLFeedback;I)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v5

    .line 1454801
    iget-object v2, v1, LX/9DG;->P:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1454802
    iget-object v7, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v7

    .line 1454803
    check-cast v2, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v2, v5}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result v2

    if-nez v2, :cond_3

    move v2, v3

    .line 1454804
    :goto_1
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v7

    if-eqz v7, :cond_4

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_4

    .line 1454805
    :goto_2
    if-eqz v2, :cond_5

    if-eqz v3, :cond_5

    move-object v2, v5

    goto :goto_0

    :cond_3
    move v2, v4

    .line 1454806
    goto :goto_1

    :cond_4
    move v3, v4

    .line 1454807
    goto :goto_2

    :cond_5
    move-object v2, v6

    .line 1454808
    goto :goto_0
.end method
