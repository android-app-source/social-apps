.class public final LX/ALV;
.super Landroid/view/OrientationEventListener;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/backstage/camera/CameraView;

.field public b:I


# direct methods
.method public constructor <init>(Lcom/facebook/backstage/camera/CameraView;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1664839
    iput-object p1, p0, LX/ALV;->a:Lcom/facebook/backstage/camera/CameraView;

    .line 1664840
    const/4 v0, 0x3

    invoke-direct {p0, p2, v0}, Landroid/view/OrientationEventListener;-><init>(Landroid/content/Context;I)V

    .line 1664841
    return-void
.end method


# virtual methods
.method public final onOrientationChanged(I)V
    .locals 2

    .prologue
    .line 1664842
    int-to-float v0, p1

    const/high16 v1, 0x42b40000    # 90.0f

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x5a

    rem-int/lit16 v0, v0, 0x168

    iput v0, p0, LX/ALV;->b:I

    .line 1664843
    return-void
.end method
