.class public LX/9iA;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1nG;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/8qT;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17V;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Or;LX/0Ot;LX/0Ot;LX/0Or;LX/0Ot;LX/0Zb;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1nG;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Or",
            "<",
            "LX/8qT;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17V;",
            ">;",
            "LX/0Zb;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1527337
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1527338
    iput-object p1, p0, LX/9iA;->a:LX/0Or;

    .line 1527339
    iput-object p2, p0, LX/9iA;->b:LX/0Ot;

    .line 1527340
    iput-object p3, p0, LX/9iA;->c:LX/0Ot;

    .line 1527341
    iput-object p4, p0, LX/9iA;->d:LX/0Or;

    .line 1527342
    iput-object p5, p0, LX/9iA;->e:LX/0Ot;

    .line 1527343
    iput-object p6, p0, LX/9iA;->f:LX/0Zb;

    .line 1527344
    return-void
.end method

.method public static a(LX/0QB;)LX/9iA;
    .locals 8

    .prologue
    .line 1527334
    new-instance v1, LX/9iA;

    const/16 v2, 0xbc6

    invoke-static {p0, v2}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v3, 0xb19

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x259

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x3748

    invoke-static {p0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x8b

    invoke-static {p0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v7

    check-cast v7, LX/0Zb;

    invoke-direct/range {v1 .. v7}, LX/9iA;-><init>(LX/0Or;LX/0Ot;LX/0Ot;LX/0Or;LX/0Ot;LX/0Zb;)V

    .line 1527335
    move-object v0, v1

    .line 1527336
    return-object v0
.end method

.method private static a(LX/9iA;Landroid/text/SpannableStringBuilder;LX/171;IILandroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLX/162;)V
    .locals 7
    .param p8    # Z
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1527328
    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    .line 1527329
    if-ge p3, v0, :cond_0

    if-le p4, v0, :cond_1

    .line 1527330
    :cond_0
    iget-object v0, p0, LX/9iA;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "applyActorLink entity is out of range for caption on entity "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". range "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for caption \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p6, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1527331
    :goto_0
    return-void

    .line 1527332
    :cond_1
    new-instance v0, Landroid/text/style/StyleSpan;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/16 v1, 0x21

    invoke-virtual {p1, v0, p3, p4, v1}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1527333
    new-instance v0, LX/9i6;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p6

    move-object v4, p5

    move v5, p8

    move-object/from16 v6, p9

    invoke-direct/range {v0 .. v6}, LX/9i6;-><init>(LX/9iA;LX/171;Ljava/lang/String;Landroid/content/Context;ZLX/162;)V

    const/16 v1, 0x21

    invoke-virtual {p1, v0, p3, p4, v1}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0
.end method

.method public static a(LX/5kD;)Z
    .locals 1

    .prologue
    .line 1527327
    if-eqz p0, :cond_0

    invoke-interface {p0}, LX/5kD;->ao()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$WithTagsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/5kD;->ao()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$WithTagsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$WithTagsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/5kD;->ao()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$WithTagsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$WithTagsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/graphql/model/GraphQLStory;)LX/0Px;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/model/FacebookProfile;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1527345
    if-eqz p0, :cond_2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->be()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->be()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLWithTagsConnection;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->be()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLWithTagsConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1527346
    if-nez v0, :cond_0

    .line 1527347
    const/4 v0, 0x0

    .line 1527348
    :goto_1
    return-object v0

    .line 1527349
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1527350
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->be()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLWithTagsConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->asList()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_2
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 1527351
    new-instance v5, Lcom/facebook/ipc/model/FacebookProfile;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v6, v7, v0}, Lcom/facebook/ipc/model/FacebookProfile;-><init>(JLjava/lang/String;)V

    invoke-virtual {v2, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1527352
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1527353
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static e(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1527326
    invoke-static {p0}, LX/16y;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/16y;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static e(LX/5kD;)Z
    .locals 1

    .prologue
    .line 1527325
    if-eqz p0, :cond_0

    invoke-interface {p0}, LX/5kD;->an()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/5kD;->an()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/5kD;->an()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static f(LX/5kD;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1527324
    invoke-interface {p0}, LX/5kD;->X()LX/175;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/5kD;->X()LX/175;

    move-result-object v0

    invoke-interface {v0}, LX/175;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/5kD;Landroid/content/Context;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)Landroid/text/SpannableStringBuilder;
    .locals 14
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1527307
    if-eqz p4, :cond_1

    invoke-static/range {p4 .. p4}, LX/9iA;->e(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    .line 1527308
    :goto_0
    if-nez v0, :cond_2

    .line 1527309
    const/4 v1, 0x0

    .line 1527310
    :cond_0
    return-object v1

    .line 1527311
    :cond_1
    invoke-static {p1}, LX/9iA;->f(LX/5kD;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1527312
    :cond_2
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1527313
    if-eqz p4, :cond_4

    invoke-static/range {p4 .. p4}, LX/0sa;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v0

    move-object v10, v0

    .line 1527314
    :goto_1
    invoke-static/range {p4 .. p4}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v12

    .line 1527315
    const/4 v9, 0x0

    .line 1527316
    if-eqz v12, :cond_3

    invoke-virtual {v12}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1527317
    invoke-static {v12}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v9

    .line 1527318
    :cond_3
    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v13

    .line 1527319
    const/4 v0, 0x0

    move v11, v0

    :goto_2
    if-ge v11, v13, :cond_0

    invoke-virtual {v10, v11}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1W5;

    .line 1527320
    invoke-interface {v0}, LX/1W5;->a()LX/171;

    move-result-object v2

    invoke-interface {v0}, LX/1W5;->c()I

    move-result v3

    invoke-interface {v0}, LX/1W5;->c()I

    move-result v4

    invoke-interface {v0}, LX/1W5;->b()I

    move-result v0

    add-int/2addr v4, v0

    if-eqz p4, :cond_5

    invoke-virtual/range {p4 .. p4}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v7

    :goto_3
    invoke-static {v12}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v8

    move-object v0, p0

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    invoke-static/range {v0 .. v9}, LX/9iA;->a(LX/9iA;Landroid/text/SpannableStringBuilder;LX/171;IILandroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLX/162;)V

    .line 1527321
    add-int/lit8 v0, v11, 0x1

    move v11, v0

    goto :goto_2

    .line 1527322
    :cond_4
    invoke-interface {p1}, LX/5kD;->X()LX/175;

    move-result-object v0

    invoke-interface {v0}, LX/175;->b()LX/0Px;

    move-result-object v0

    move-object v10, v0

    goto :goto_1

    .line 1527323
    :cond_5
    invoke-interface {p1}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v7

    goto :goto_3
.end method

.method public final d(LX/5kD;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5kD;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/model/FacebookProfile;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1527292
    invoke-static {p1}, LX/9iA;->a(LX/5kD;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, LX/9iA;->e(LX/5kD;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1527293
    const/4 v0, 0x0

    .line 1527294
    :goto_0
    return-object v0

    .line 1527295
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1527296
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 1527297
    invoke-static {p1}, LX/9iA;->a(LX/5kD;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1527298
    invoke-interface {p1}, LX/5kD;->ao()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$WithTagsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$WithTagsModel;->a()LX/0Px;

    move-result-object v0

    new-instance v3, LX/9i7;

    invoke-direct {v3, p0}, LX/9i7;-><init>(LX/9iA;)V

    invoke-static {v0, v3}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v0

    .line 1527299
    invoke-virtual {v1, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1527300
    invoke-interface {v2, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 1527301
    :cond_1
    invoke-static {p1}, LX/9iA;->e(LX/5kD;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1527302
    invoke-interface {p1}, LX/5kD;->an()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;->a()LX/0Px;

    move-result-object v0

    new-instance v3, LX/9i8;

    invoke-direct {v3, p0}, LX/9i8;-><init>(LX/9iA;)V

    invoke-static {v0, v3}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v0

    .line 1527303
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookProfile;

    .line 1527304
    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    iget v4, v0, Lcom/facebook/ipc/model/FacebookProfile;->a:I

    const/4 v5, 0x6

    if-eq v4, v5, :cond_2

    iget v4, v0, Lcom/facebook/ipc/model/FacebookProfile;->a:I

    const/4 v5, 0x7

    if-eq v4, v5, :cond_2

    .line 1527305
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1527306
    :cond_3
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method
