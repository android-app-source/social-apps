.class public LX/ADY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Bw;
.implements LX/0YZ;


# instance fields
.field public final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/util/SparseBooleanArray;

.field public c:LX/0Uo;

.field private d:Landroid/os/Handler;

.field private e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/03V;

.field private g:LX/0Xl;


# direct methods
.method public constructor <init>(LX/0Uo;Landroid/os/Handler;LX/0Ot;LX/03V;LX/0Xl;)V
    .locals 2
    .param p2    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/base/broadcast/BackgroundBroadcastThread;
        .end annotation
    .end param
    .param p3    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .param p5    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Uo;",
            "Landroid/os/Handler;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Xl;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x4

    .line 1644918
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1644919
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, LX/ADY;->a:Landroid/util/SparseArray;

    .line 1644920
    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0, v1}, Landroid/util/SparseBooleanArray;-><init>(I)V

    iput-object v0, p0, LX/ADY;->b:Landroid/util/SparseBooleanArray;

    .line 1644921
    iput-object p1, p0, LX/ADY;->c:LX/0Uo;

    .line 1644922
    iput-object p2, p0, LX/ADY;->d:Landroid/os/Handler;

    .line 1644923
    iput-object p3, p0, LX/ADY;->e:LX/0Ot;

    .line 1644924
    iput-object p4, p0, LX/ADY;->f:LX/03V;

    .line 1644925
    iput-object p5, p0, LX/ADY;->g:LX/0Xl;

    .line 1644926
    iget-object v0, p0, LX/ADY;->g:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    invoke-interface {v0, v1, p0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    iget-object v1, p0, LX/ADY;->d:Landroid/os/Handler;

    invoke-interface {v0, v1}, LX/0YX;->a(Landroid/os/Handler;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 1644927
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 5

    .prologue
    .line 1644913
    iget-object v0, p0, LX/ADY;->c:LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1644914
    iget-object v0, p0, LX/ADY;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/analytics/camerausage/CameraLeakListener$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/analytics/camerausage/CameraLeakListener$1;-><init>(LX/ADY;I)V

    const-wide/16 v2, 0xa

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 1644915
    :cond_0
    iget-object v0, p0, LX/ADY;->a:Landroid/util/SparseArray;

    new-instance v1, Ljava/lang/Throwable;

    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 1644916
    iget-object v0, p0, LX/ADY;->b:Landroid/util/SparseBooleanArray;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseBooleanArray;->append(IZ)V

    .line 1644917
    return-void
.end method

.method public final b(I)V
    .locals 5

    .prologue
    .line 1644908
    iget-object v0, p0, LX/ADY;->b:Landroid/util/SparseBooleanArray;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseBooleanArray;->get(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1644909
    iget-object v1, p0, LX/ADY;->f:LX/03V;

    const-string v2, "CameraLeakDetector"

    const-string v3, "Camera opened but not used."

    iget-object v0, p0, LX/ADY;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v0, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;I)V

    .line 1644910
    iget-object v0, p0, LX/ADY;->b:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseBooleanArray;->delete(I)V

    .line 1644911
    :cond_0
    iget-object v0, p0, LX/ADY;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 1644912
    return-void
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 1644906
    iget-object v0, p0, LX/ADY;->b:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseBooleanArray;->delete(I)V

    .line 1644907
    return-void
.end method

.method public final d(I)V
    .locals 0

    .prologue
    .line 1644900
    return-void
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 8

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x26

    const v2, -0x799fb982

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1644901
    iget-object v0, p0, LX/ADY;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v3

    .line 1644902
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 1644903
    iget-object v4, p0, LX/ADY;->f:LX/03V;

    const-string v5, "CameraLeakDetector"

    const-string v6, "Camera wasn\'t closed before the app backgrounded."

    iget-object v0, p0, LX/ADY;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    const/4 v7, 0x1

    invoke-virtual {v4, v5, v6, v0, v7}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;I)V

    .line 1644904
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1644905
    :cond_0
    const v0, -0x94751f8

    invoke-static {v0, v2}, LX/02F;->e(II)V

    return-void
.end method
