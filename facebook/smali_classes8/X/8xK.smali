.class public final LX/8xK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Mb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3Mb",
        "<",
        "LX/8jS;",
        "LX/8jT;",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/attachments/ui/AttachmentViewSticker;


# direct methods
.method public constructor <init>(Lcom/facebook/attachments/ui/AttachmentViewSticker;)V
    .locals 0

    .prologue
    .line 1423867
    iput-object p1, p0, LX/8xK;->a:Lcom/facebook/attachments/ui/AttachmentViewSticker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1423868
    iget-object v0, p0, LX/8xK;->a:Lcom/facebook/attachments/ui/AttachmentViewSticker;

    iget-object v0, v0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->a:LX/0Sh;

    new-instance v1, Lcom/facebook/attachments/ui/AttachmentViewSticker$4$1;

    invoke-direct {v1, p0}, Lcom/facebook/attachments/ui/AttachmentViewSticker$4$1;-><init>(LX/8xK;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 1423869
    return-void
.end method

.method private a(LX/8jS;Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1423870
    iget-object v0, p0, LX/8xK;->a:Lcom/facebook/attachments/ui/AttachmentViewSticker;

    iget-object v0, v0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->a:LX/0Sh;

    new-instance v1, Lcom/facebook/attachments/ui/AttachmentViewSticker$4$3;

    invoke-direct {v1, p0}, Lcom/facebook/attachments/ui/AttachmentViewSticker$4$3;-><init>(LX/8xK;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 1423871
    iget-object v0, p0, LX/8xK;->a:Lcom/facebook/attachments/ui/AttachmentViewSticker;

    iget-object v0, v0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->b:LX/03V;

    sget-object v1, Lcom/facebook/attachments/ui/AttachmentViewSticker;->j:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "View sticker pack failed (id = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, LX/8jS;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1423872
    return-void
.end method

.method private a(LX/8jT;)V
    .locals 2

    .prologue
    .line 1423873
    iget-object v0, p0, LX/8xK;->a:Lcom/facebook/attachments/ui/AttachmentViewSticker;

    iget-object v0, v0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->a:LX/0Sh;

    new-instance v1, Lcom/facebook/attachments/ui/AttachmentViewSticker$4$2;

    invoke-direct {v1, p0, p1}, Lcom/facebook/attachments/ui/AttachmentViewSticker$4$2;-><init>(LX/8xK;LX/8jT;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 1423874
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 0

    .prologue
    .line 1423875
    invoke-direct {p0}, LX/8xK;->a()V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1423876
    return-void
.end method

.method public final synthetic b(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1423877
    check-cast p2, LX/8jT;

    invoke-direct {p0, p2}, LX/8xK;->a(LX/8jT;)V

    return-void
.end method

.method public final synthetic c(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1423878
    check-cast p1, LX/8jS;

    check-cast p2, Ljava/lang/Throwable;

    invoke-direct {p0, p1, p2}, LX/8xK;->a(LX/8jS;Ljava/lang/Throwable;)V

    return-void
.end method
