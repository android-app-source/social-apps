.class public final LX/AA6;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v7, 0x1

    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 1636909
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 1636910
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1636911
    :goto_0
    return v1

    .line 1636912
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v12, :cond_4

    .line 1636913
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1636914
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1636915
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_0

    if-eqz v9, :cond_0

    .line 1636916
    const-string v12, "latitude"

    invoke-virtual {v9, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 1636917
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v0, v7

    goto :goto_1

    .line 1636918
    :cond_1
    const-string v12, "longitude"

    invoke-virtual {v9, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 1636919
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v10

    move v6, v7

    goto :goto_1

    .line 1636920
    :cond_2
    const-string v12, "reverse_geocode"

    invoke-virtual {v9, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1636921
    invoke-static {p0, p1}, LX/AA5;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1636922
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1636923
    :cond_4
    const/4 v9, 0x3

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1636924
    if-eqz v0, :cond_5

    move-object v0, p1

    .line 1636925
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1636926
    :cond_5
    if-eqz v6, :cond_6

    move-object v0, p1

    move v1, v7

    move-wide v2, v10

    .line 1636927
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1636928
    :cond_6
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1636929
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v6, v1

    move v0, v1

    move v8, v1

    move-wide v10, v4

    move-wide v2, v4

    goto :goto_1
.end method
