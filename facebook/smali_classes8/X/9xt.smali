.class public final LX/9xt;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 1593579
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1593580
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1593581
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1593582
    invoke-static {p0, p1}, LX/9xt;->b(LX/15w;LX/186;)I

    move-result v1

    .line 1593583
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1593584
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1593585
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1593586
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1593587
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_0

    .line 1593588
    const-string v2, "latitude"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1593589
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1593590
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1593591
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_1

    .line 1593592
    const-string v2, "longitude"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1593593
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1593594
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1593595
    if-eqz v0, :cond_2

    .line 1593596
    const-string v1, "timezone"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1593597
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1593598
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1593599
    return-void
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1593600
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1593601
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1593602
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2}, LX/9xt;->a(LX/15i;ILX/0nX;)V

    .line 1593603
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1593604
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1593605
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v7, 0x1

    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 1593606
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 1593607
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1593608
    :goto_0
    return v1

    .line 1593609
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v12, :cond_4

    .line 1593610
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1593611
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1593612
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_0

    if-eqz v9, :cond_0

    .line 1593613
    const-string v12, "latitude"

    invoke-virtual {v9, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 1593614
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v0, v7

    goto :goto_1

    .line 1593615
    :cond_1
    const-string v12, "longitude"

    invoke-virtual {v9, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 1593616
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v10

    move v6, v7

    goto :goto_1

    .line 1593617
    :cond_2
    const-string v12, "timezone"

    invoke-virtual {v9, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1593618
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 1593619
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1593620
    :cond_4
    const/4 v9, 0x3

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1593621
    if-eqz v0, :cond_5

    move-object v0, p1

    .line 1593622
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1593623
    :cond_5
    if-eqz v6, :cond_6

    move-object v0, p1

    move v1, v7

    move-wide v2, v10

    .line 1593624
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1593625
    :cond_6
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1593626
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v6, v1

    move v0, v1

    move v8, v1

    move-wide v10, v4

    move-wide v2, v4

    goto :goto_1
.end method
