.class public final LX/ANG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Ik;


# instance fields
.field public final synthetic a:Lcom/facebook/cameracore/ui/CameraCoreFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V
    .locals 0

    .prologue
    .line 1667565
    iput-object p1, p0, LX/ANG;->a:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1667566
    iget-object v0, p0, LX/ANG;->a:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    const/4 v1, 0x0

    .line 1667567
    iput-boolean v1, v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->d:Z

    .line 1667568
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1667569
    iget-object v0, p0, LX/ANG;->a:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    const/4 v1, 0x0

    .line 1667570
    iput-boolean v1, v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->d:Z

    .line 1667571
    sget-object v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->h:LX/6Ia;

    invoke-interface {v0}, LX/6Ia;->b()V

    .line 1667572
    iget-object v0, p0, LX/ANG;->a:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    invoke-static {v0, p1}, Lcom/facebook/cameracore/ui/CameraCoreFragment;->a$redex0(Lcom/facebook/cameracore/ui/CameraCoreFragment;Ljava/lang/Throwable;)V

    .line 1667573
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 1667574
    iget-object v0, p0, LX/ANG;->a:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    const/4 v1, 0x0

    .line 1667575
    iput-boolean v1, v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->d:Z

    .line 1667576
    iget-object v0, p0, LX/ANG;->a:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1667577
    iget-object v0, p0, LX/ANG;->a:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    iget-object v0, v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->J:LX/03V;

    const-string v1, "cameracore_open_camera"

    const-string v2, "Fragment is no longer added"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1667578
    :goto_0
    return-void

    .line 1667579
    :cond_0
    iget-object v0, p0, LX/ANG;->a:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    iget-object v0, v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->l:Lcom/facebook/fbui/glyph/GlyphButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setEnabled(Z)V

    .line 1667580
    iget-object v0, p0, LX/ANG;->a:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    invoke-static {v0}, Lcom/facebook/cameracore/ui/CameraCoreFragment;->n$redex0(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V

    .line 1667581
    iget-object v0, p0, LX/ANG;->a:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    iget-object v0, v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->u:LX/6JB;

    if-nez v0, :cond_1

    .line 1667582
    iget-object v0, p0, LX/ANG;->a:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Camera settings could not be created"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/facebook/cameracore/ui/CameraCoreFragment;->a$redex0(Lcom/facebook/cameracore/ui/CameraCoreFragment;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1667583
    :cond_1
    sget-object v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->e:LX/6Jt;

    sget-object v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->h:LX/6Ia;

    iget-object v2, p0, LX/ANG;->a:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    iget-object v2, v2, Lcom/facebook/cameracore/ui/CameraCoreFragment;->u:LX/6JB;

    iget-object v3, p0, LX/ANG;->a:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    iget-object v3, v3, Lcom/facebook/cameracore/ui/CameraCoreFragment;->x:LX/6JR;

    invoke-virtual {v0, v1, v2, v3}, LX/6Jt;->a(LX/6Ia;LX/6JB;LX/6JR;)V

    .line 1667584
    sget-object v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->e:LX/6Jt;

    iget-object v1, p0, LX/ANG;->a:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    iget-object v1, v1, Lcom/facebook/cameracore/ui/CameraCoreFragment;->ac:LX/6Ik;

    iget-object v2, p0, LX/ANG;->a:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    iget-object v2, v2, Lcom/facebook/cameracore/ui/CameraCoreFragment;->y:LX/6JC;

    invoke-virtual {v0, v1, v2}, LX/6Jt;->a(LX/6Ik;LX/6JC;)V

    goto :goto_0
.end method
