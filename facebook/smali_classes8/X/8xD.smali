.class public final LX/8xD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3IA;
.implements LX/3IB;


# instance fields
.field public final synthetic a:LX/8xF;

.field private b:Z


# direct methods
.method public constructor <init>(LX/8xF;)V
    .locals 1

    .prologue
    .line 1423664
    iput-object p1, p0, LX/8xD;->a:LX/8xF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1423665
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/8xD;->b:Z

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1423663
    return-void
.end method

.method public final a(FF)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1423658
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget-boolean v0, p0, LX/8xD;->b:Z

    if-nez v0, :cond_0

    .line 1423659
    iget-object v0, p0, LX/8xD;->a:LX/8xF;

    invoke-virtual {v0}, LX/8xF;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 1423660
    iput-boolean v2, p0, LX/8xD;->b:Z

    .line 1423661
    :cond_0
    iget-object v0, p0, LX/8xD;->a:LX/8xF;

    iget-object v0, v0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v0, p1, v1}, LX/2qW;->c(FF)V

    .line 1423662
    return-void
.end method

.method public final a(F)Z
    .locals 1

    .prologue
    .line 1423666
    iget-object v0, p0, LX/8xD;->a:LX/8xF;

    iget-object v0, v0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v0, p1}, LX/2qW;->a(F)V

    .line 1423667
    iget-object v0, p0, LX/8xD;->a:LX/8xF;

    invoke-virtual {v0}, LX/8wv;->p()V

    .line 1423668
    const/4 v0, 0x1

    return v0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1423652
    iget-object v0, p0, LX/8xD;->a:LX/8xF;

    iget-object v0, v0, LX/8wv;->a:LX/1xG;

    iget-object v1, p0, LX/8xD;->a:LX/8xF;

    iget-object v1, v1, LX/8wv;->r:Ljava/lang/String;

    iget-object v2, p0, LX/8xD;->a:LX/8xF;

    iget-object v2, v2, LX/8wv;->p:LX/7Dj;

    invoke-virtual {v0, v1, v2}, LX/1xG;->h(Ljava/lang/String;LX/7Dj;)V

    .line 1423653
    iget-object v0, p0, LX/8xD;->a:LX/8xF;

    iget-object v0, v0, LX/8wv;->o:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    invoke-virtual {v0}, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->h()V

    .line 1423654
    iget-object v0, p0, LX/8xD;->a:LX/8xF;

    iget-object v0, v0, LX/8wv;->o:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    const/4 v1, 0x0

    .line 1423655
    iput-boolean v1, v0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->f:Z

    .line 1423656
    iget-object v0, p0, LX/8xD;->a:LX/8xF;

    iget-object v0, v0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v0}, LX/2qW;->f()V

    .line 1423657
    return-void
.end method

.method public final b(FF)V
    .locals 2

    .prologue
    .line 1423649
    iget-object v0, p0, LX/8xD;->a:LX/8xF;

    iget-object v0, v0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LX/2qW;->d(FF)V

    .line 1423650
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/8xD;->b:Z

    .line 1423651
    return-void
.end method

.method public final c()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1423646
    iget-object v0, p0, LX/8xD;->a:LX/8xF;

    iget-object v0, v0, LX/8wv;->a:LX/1xG;

    iget-object v1, p0, LX/8xD;->a:LX/8xF;

    iget-object v1, v1, LX/8wv;->r:Ljava/lang/String;

    iget-object v2, p0, LX/8xD;->a:LX/8xF;

    iget-object v2, v2, LX/8wv;->p:LX/7Dj;

    invoke-virtual {v0, v1, v2}, LX/1xG;->g(Ljava/lang/String;LX/7Dj;)V

    .line 1423647
    iget-object v0, p0, LX/8xD;->a:LX/8xF;

    invoke-virtual {v0}, LX/8xF;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 1423648
    return v3
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1423644
    iget-object v0, p0, LX/8xD;->a:LX/8xF;

    iget-object v0, v0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v0}, LX/2qW;->g()V

    .line 1423645
    return-void
.end method
