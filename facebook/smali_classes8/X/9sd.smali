.class public final LX/9sd;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:I

.field public d:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$SquareHeaderImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Z

.field public f:Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Z

.field public h:I

.field public i:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelSubtitleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelTitleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1557260
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;
    .locals 11

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v10, 0x0

    .line 1557261
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1557262
    iget-object v1, p0, LX/9sd;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1557263
    iget-object v3, p0, LX/9sd;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1557264
    iget-object v5, p0, LX/9sd;->d:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$SquareHeaderImageModel;

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1557265
    iget-object v6, p0, LX/9sd;->f:Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1557266
    iget-object v7, p0, LX/9sd;->i:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelSubtitleModel;

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1557267
    iget-object v8, p0, LX/9sd;->j:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelTitleModel;

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1557268
    const/16 v9, 0xa

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 1557269
    invoke-virtual {v0, v10, v1}, LX/186;->b(II)V

    .line 1557270
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1557271
    const/4 v1, 0x2

    iget v3, p0, LX/9sd;->c:I

    invoke-virtual {v0, v1, v3, v10}, LX/186;->a(III)V

    .line 1557272
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1557273
    const/4 v1, 0x4

    iget-boolean v3, p0, LX/9sd;->e:Z

    invoke-virtual {v0, v1, v3}, LX/186;->a(IZ)V

    .line 1557274
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1557275
    const/4 v1, 0x6

    iget-boolean v3, p0, LX/9sd;->g:Z

    invoke-virtual {v0, v1, v3}, LX/186;->a(IZ)V

    .line 1557276
    const/4 v1, 0x7

    iget v3, p0, LX/9sd;->h:I

    invoke-virtual {v0, v1, v3, v10}, LX/186;->a(III)V

    .line 1557277
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1557278
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1557279
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1557280
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1557281
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1557282
    invoke-virtual {v1, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1557283
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1557284
    new-instance v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    invoke-direct {v1, v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;-><init>(LX/15i;)V

    .line 1557285
    return-object v1
.end method
