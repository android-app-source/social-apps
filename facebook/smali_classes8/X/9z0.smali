.class public final LX/9z0;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1598874
    const-class v1, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel;

    const v0, 0x3030084e

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "Fb4aGraphSearchPhotoQuery"

    const-string v6, "1075ada5591259dd4986de4129d4ca2a"

    const-string v7, "graph_search_query"

    const-string v8, "10155227397896729"

    const-string v9, "10155263677946729"

    .line 1598875
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1598876
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1598877
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1598885
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1598886
    sparse-switch v0, :sswitch_data_0

    .line 1598887
    :goto_0
    return-object p1

    .line 1598888
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1598889
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1598890
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1598891
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1598892
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1598893
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 1598894
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 1598895
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 1598896
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 1598897
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 1598898
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 1598899
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 1598900
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 1598901
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 1598902
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 1598903
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    .line 1598904
    :sswitch_10
    const-string p1, "16"

    goto :goto_0

    .line 1598905
    :sswitch_11
    const-string p1, "17"

    goto :goto_0

    .line 1598906
    :sswitch_12
    const-string p1, "18"

    goto :goto_0

    .line 1598907
    :sswitch_13
    const-string p1, "19"

    goto :goto_0

    .line 1598908
    :sswitch_14
    const-string p1, "20"

    goto :goto_0

    .line 1598909
    :sswitch_15
    const-string p1, "21"

    goto :goto_0

    .line 1598910
    :sswitch_16
    const-string p1, "22"

    goto :goto_0

    .line 1598911
    :sswitch_17
    const-string p1, "23"

    goto :goto_0

    .line 1598912
    :sswitch_18
    const-string p1, "24"

    goto :goto_0

    .line 1598913
    :sswitch_19
    const-string p1, "25"

    goto :goto_0

    .line 1598914
    :sswitch_1a
    const-string p1, "26"

    goto :goto_0

    .line 1598915
    :sswitch_1b
    const-string p1, "27"

    goto :goto_0

    .line 1598916
    :sswitch_1c
    const-string p1, "28"

    goto :goto_0

    .line 1598917
    :sswitch_1d
    const-string p1, "29"

    goto :goto_0

    .line 1598918
    :sswitch_1e
    const-string p1, "30"

    goto :goto_0

    .line 1598919
    :sswitch_1f
    const-string p1, "31"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x6e3ba572 -> :sswitch_1
        -0x6a24640d -> :sswitch_1b
        -0x680de62a -> :sswitch_c
        -0x6326fdb3 -> :sswitch_8
        -0x4496acc9 -> :sswitch_d
        -0x421ba035 -> :sswitch_b
        -0x41a91745 -> :sswitch_18
        -0x3c54de38 -> :sswitch_13
        -0x3be4a271 -> :sswitch_9
        -0x3b85b241 -> :sswitch_1e
        -0x36dc9999 -> :sswitch_17
        -0x30b65c8f -> :sswitch_6
        -0x2c889631 -> :sswitch_12
        -0x2a63cba2 -> :sswitch_a
        -0x1b87b280 -> :sswitch_7
        -0x1673cbea -> :sswitch_4
        -0x12efdeb3 -> :sswitch_e
        -0xa3c055b -> :sswitch_1f
        0x3677da -> :sswitch_1d
        0x101fb19 -> :sswitch_3
        0x5a7510f -> :sswitch_5
        0x66f18c8 -> :sswitch_19
        0xa1fa812 -> :sswitch_0
        0x214100e0 -> :sswitch_f
        0x2d446f86 -> :sswitch_11
        0x3052e0ff -> :sswitch_2
        0x518e5e8f -> :sswitch_16
        0x53ffd947 -> :sswitch_14
        0x553ac3c6 -> :sswitch_1a
        0x617a8767 -> :sswitch_10
        0x73a026b5 -> :sswitch_15
        0x7506f93c -> :sswitch_1c
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1598878
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 1598879
    :goto_1
    return v0

    .line 1598880
    :sswitch_0
    const-string v2, "2"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :sswitch_1
    const-string v2, "6"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "26"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_3
    const-string v2, "30"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    .line 1598881
    :pswitch_0
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1598882
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1598883
    :pswitch_2
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1598884
    :pswitch_3
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x32 -> :sswitch_0
        0x36 -> :sswitch_1
        0x644 -> :sswitch_2
        0x65d -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
