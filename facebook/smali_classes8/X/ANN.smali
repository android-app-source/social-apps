.class public final LX/ANN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6JG;


# instance fields
.field public final synthetic a:Ljava/io/File;

.field public final synthetic b:Lcom/facebook/cameracore/ui/CameraCoreFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/cameracore/ui/CameraCoreFragment;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 1667666
    iput-object p1, p0, LX/ANN;->b:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    iput-object p2, p0, LX/ANN;->a:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1667663
    iget-object v0, p0, LX/ANN;->b:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    iget-object v0, v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->p:Lcom/facebook/cameracore/ui/CounterView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/cameracore/ui/CounterView;->setVisibility(I)V

    .line 1667664
    iget-object v0, p0, LX/ANN;->b:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    iget-object v0, v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->p:Lcom/facebook/cameracore/ui/CounterView;

    invoke-virtual {v0}, Lcom/facebook/cameracore/ui/CounterView;->a()V

    .line 1667665
    return-void
.end method

.method public final a(LX/6JJ;)V
    .locals 3

    .prologue
    .line 1667657
    iget-object v0, p0, LX/ANN;->b:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    iget-object v0, v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->J:LX/03V;

    const-string v1, "cameracore_capture_video"

    invoke-virtual {v0, v1, p1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1667658
    iget-object v0, p0, LX/ANN;->b:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1667659
    iget-object v0, p0, LX/ANN;->b:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    iget-object v0, v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->J:LX/03V;

    const-string v1, "cameracore_capture_video"

    const-string v2, "Fragment is no longer added"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1667660
    :goto_0
    return-void

    .line 1667661
    :cond_0
    iget-object v0, p0, LX/ANN;->b:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    iget-object v0, v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->z:LX/AMH;

    invoke-virtual {v0}, LX/AMH;->a()V

    .line 1667662
    iget-object v0, p0, LX/ANN;->b:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    invoke-static {v0}, Lcom/facebook/cameracore/ui/CameraCoreFragment;->C(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1667651
    iget-object v0, p0, LX/ANN;->b:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1667652
    iget-object v0, p0, LX/ANN;->b:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    iget-object v0, v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->J:LX/03V;

    const-string v1, "cameracore_capture_video"

    const-string v2, "Fragment is no longer added"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1667653
    :goto_0
    return-void

    .line 1667654
    :cond_0
    iget-object v0, p0, LX/ANN;->b:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, LX/ANN;->a:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1, v4, v4}, Landroid/media/MediaScannerConnection;->scanFile(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;Landroid/media/MediaScannerConnection$OnScanCompletedListener;)V

    .line 1667655
    iget-object v0, p0, LX/ANN;->b:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    iget-object v0, v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->z:LX/AMH;

    iget-object v1, p0, LX/ANN;->a:Ljava/io/File;

    invoke-virtual {v0, v1}, LX/AMH;->a(Ljava/io/File;)V

    .line 1667656
    iget-object v0, p0, LX/ANN;->b:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    invoke-static {v0}, Lcom/facebook/cameracore/ui/CameraCoreFragment;->C(Lcom/facebook/cameracore/ui/CameraCoreFragment;)V

    goto :goto_0
.end method
