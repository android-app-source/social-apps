.class public final enum LX/9CN;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9CN;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9CN;

.field public static final enum INACTIVE:LX/9CN;

.field public static final enum REPLY_STICKY:LX/9CN;

.field public static final enum TOP_LEVEL:LX/9CN;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1453801
    new-instance v0, LX/9CN;

    const-string v1, "TOP_LEVEL"

    invoke-direct {v0, v1, v2}, LX/9CN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9CN;->TOP_LEVEL:LX/9CN;

    new-instance v0, LX/9CN;

    const-string v1, "REPLY_STICKY"

    invoke-direct {v0, v1, v3}, LX/9CN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9CN;->REPLY_STICKY:LX/9CN;

    new-instance v0, LX/9CN;

    const-string v1, "INACTIVE"

    invoke-direct {v0, v1, v4}, LX/9CN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9CN;->INACTIVE:LX/9CN;

    .line 1453802
    const/4 v0, 0x3

    new-array v0, v0, [LX/9CN;

    sget-object v1, LX/9CN;->TOP_LEVEL:LX/9CN;

    aput-object v1, v0, v2

    sget-object v1, LX/9CN;->REPLY_STICKY:LX/9CN;

    aput-object v1, v0, v3

    sget-object v1, LX/9CN;->INACTIVE:LX/9CN;

    aput-object v1, v0, v4

    sput-object v0, LX/9CN;->$VALUES:[LX/9CN;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1453803
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9CN;
    .locals 1

    .prologue
    .line 1453804
    const-class v0, LX/9CN;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9CN;

    return-object v0
.end method

.method public static values()[LX/9CN;
    .locals 1

    .prologue
    .line 1453805
    sget-object v0, LX/9CN;->$VALUES:[LX/9CN;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9CN;

    return-object v0
.end method
