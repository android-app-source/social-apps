.class public LX/9b1;
.super Landroid/widget/BaseAdapter;
.source ""

# interfaces
.implements Landroid/widget/Filterable;


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLAlbum;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/lang/String;

.field public d:LX/9b0;


# direct methods
.method public constructor <init>(LX/9b0;)V
    .locals 0
    .param p1    # LX/9b0;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1514246
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 1514247
    iput-object p1, p0, LX/9b1;->d:LX/9b0;

    .line 1514248
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLAlbumsConnection;)V
    .locals 1

    .prologue
    .line 1514198
    iput-object p1, p0, LX/9b1;->a:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    .line 1514199
    iget-object v0, p0, LX/9b1;->a:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    if-eqz v0, :cond_0

    .line 1514200
    iget-object v0, p0, LX/9b1;->a:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;->a()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/9b1;->b:LX/0Px;

    .line 1514201
    :cond_0
    const v0, 0x7cedf79

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1514202
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1514203
    iput-object p1, p0, LX/9b1;->c:Ljava/lang/String;

    .line 1514204
    const v0, 0x432681c0

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1514205
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 1514206
    iget-object v0, p0, LX/9b1;->b:LX/0Px;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/9b1;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    goto :goto_0
.end method

.method public final getFilter()Landroid/widget/Filter;
    .locals 1

    .prologue
    .line 1514207
    new-instance v0, LX/9az;

    invoke-direct {v0, p0}, LX/9az;-><init>(LX/9b1;)V

    return-object v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1514208
    iget-object v0, p0, LX/9b1;->b:LX/0Px;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/9b1;->b:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbum;

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1514209
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    .line 1514210
    invoke-virtual {p0, p1}, LX/9b1;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 1514211
    if-eqz p2, :cond_3

    .line 1514212
    check-cast p2, LX/9ay;

    .line 1514213
    :goto_0
    iget-object v1, p0, LX/9b1;->c:Ljava/lang/String;

    .line 1514214
    iget-object v2, p0, LX/9b1;->d:LX/9b0;

    sget-object p1, LX/9b0;->ENABLE_OPTIONS:LX/9b0;

    if-ne v2, p1, :cond_4

    .line 1514215
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->p()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->n()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    :goto_1
    move v2, v2

    .line 1514216
    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :goto_2
    move v2, v2

    .line 1514217
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 p3, 0x0

    .line 1514218
    iput-object v0, p2, LX/9ay;->l:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 1514219
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object p0

    if-eqz p0, :cond_6

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->X()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p0

    if-eqz p0, :cond_6

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->X()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p0

    if-nez p0, :cond_6

    .line 1514220
    invoke-virtual {p2, p3}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailPlaceholderDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1514221
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->X()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    invoke-virtual {p2, p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 1514222
    :goto_3
    invoke-virtual {p2, p3}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1514223
    sget-object p0, LX/6VF;->MEDIUM:LX/6VF;

    invoke-virtual {p2, p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 1514224
    const p0, 0x7f020201

    invoke-virtual {p2, p0}, LX/9ay;->setBackgroundResource(I)V

    .line 1514225
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p2, p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 1514226
    invoke-virtual {p2}, LX/9ay;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-static {v0, p0}, LX/9bt;->a(Lcom/facebook/graphql/model/GraphQLAlbum;Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    .line 1514227
    invoke-virtual {p2, p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 1514228
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/facebook/fbui/widget/contentview/ContentView;->getTitleText()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object p0

    const-string p1, ": "

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {p2}, Lcom/facebook/fbui/widget/contentview/ContentView;->getSubtitleText()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p2, p0}, LX/9ay;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1514229
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p2, p0}, LX/9ay;->setTag(Ljava/lang/Object;)V

    .line 1514230
    if-eqz v1, :cond_7

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object p0

    invoke-static {v1, p0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    move p0, v4

    .line 1514231
    :goto_4
    if-eqz p0, :cond_8

    .line 1514232
    const p1, 0x7f020244

    invoke-virtual {p2, p1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonResource(I)V

    .line 1514233
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonContentDescription(Ljava/lang/CharSequence;)V

    .line 1514234
    invoke-virtual {p2, p3}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1514235
    :cond_0
    :goto_5
    if-nez p0, :cond_1

    if-eqz v2, :cond_2

    :cond_1
    move v3, v4

    :cond_2
    invoke-virtual {p2, v3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setShowAuxView(Z)V

    .line 1514236
    iget-object v3, p2, LX/9ay;->n:Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v3}, LX/9ay;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1514237
    move-object v0, p2

    .line 1514238
    return-object v0

    .line 1514239
    :cond_3
    new-instance p2, LX/9ay;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p2, v1}, LX/9ay;-><init>(Landroid/content/Context;)V

    goto/16 :goto_0

    :cond_4
    const/4 v2, 0x0

    goto/16 :goto_2

    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 1514240
    :cond_6
    const p0, 0x7f020626

    invoke-virtual {p2, p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailPlaceholderResource(I)V

    .line 1514241
    invoke-virtual {p2, p3}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    goto/16 :goto_3

    :cond_7
    move p0, v3

    .line 1514242
    goto :goto_4

    .line 1514243
    :cond_8
    if-eqz v2, :cond_0

    .line 1514244
    const p1, 0x7f020d6e

    invoke-virtual {p2, p1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonResource(I)V

    .line 1514245
    iget-object p1, p2, LX/9ay;->m:Landroid/view/View$OnClickListener;

    invoke-virtual {p2, p1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_5
.end method
