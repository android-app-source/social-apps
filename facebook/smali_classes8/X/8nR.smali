.class public LX/8nR;
.super LX/8nB;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final f:Ljava/lang/Object;


# instance fields
.field private final b:LX/0tX;

.field private final c:Lcom/facebook/auth/viewercontext/ViewerContext;

.field private final d:LX/3iT;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1401015
    const-class v0, LX/8nR;

    sput-object v0, LX/8nR;->a:Ljava/lang/Class;

    .line 1401016
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/8nR;->f:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0tX;Lcom/facebook/auth/viewercontext/ViewerContext;LX/3iT;LX/0Or;)V
    .locals 0
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0tX;",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            "LX/3iT;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1401009
    invoke-direct {p0}, LX/8nB;-><init>()V

    .line 1401010
    iput-object p1, p0, LX/8nR;->b:LX/0tX;

    .line 1401011
    iput-object p2, p0, LX/8nR;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1401012
    iput-object p3, p0, LX/8nR;->d:LX/3iT;

    .line 1401013
    iput-object p4, p0, LX/8nR;->e:LX/0Or;

    .line 1401014
    return-void
.end method

.method public static a$redex0(LX/8nR;Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;)LX/7Gr;
    .locals 2

    .prologue
    .line 1401017
    iget-object v0, p0, LX/8nR;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1401018
    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1401019
    sget-object v0, LX/7Gr;->SELF:LX/7Gr;

    .line 1401020
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/tagging/model/TaggingProfile;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;)LX/7Gr;

    move-result-object v0

    goto :goto_0
.end method

.method public static a$redex0(LX/8nR;Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/tagging/model/TaggingProfile;
    .locals 9

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 1401002
    iget-object v0, p0, LX/8nR;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1401003
    iget-boolean v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v0, v1

    .line 1401004
    if-nez v0, :cond_0

    .line 1401005
    invoke-virtual {p1}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;->n()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1401006
    invoke-virtual {p1}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;->n()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    .line 1401007
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;->m()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v5, v0, LX/1vs;->b:I

    .line 1401008
    iget-object v0, p0, LX/8nR;->d:LX/3iT;

    new-instance v1, Lcom/facebook/user/model/Name;

    invoke-virtual {p1}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v2, v3}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v4, v5, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, p1}, LX/8nR;->a$redex0(LX/8nR;Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBTagSearchProfileModel;)LX/7Gr;

    move-result-object v5

    move-object v7, p2

    move-object v8, p3

    invoke-virtual/range {v0 .. v8}, LX/3iT;->a(Lcom/facebook/user/model/Name;JLjava/lang/String;LX/7Gr;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/tagging/model/TaggingProfile;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v6, v2

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;Ljava/lang/String;ZZZZZLX/8JX;)V
    .locals 9

    .prologue
    .line 1400993
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    const/4 v2, 0x3

    if-ge v1, v2, :cond_0

    .line 1400994
    const/4 v1, 0x0

    move-object/from16 v0, p8

    invoke-interface {v0, p1, v1}, LX/8JX;->a(Ljava/lang/CharSequence;Ljava/util/List;)V

    .line 1400995
    :goto_0
    return-void

    .line 1400996
    :cond_0
    invoke-static {}, LX/8oM;->a()LX/8oL;

    move-result-object v2

    .line 1400997
    const-string v1, "search_key"

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v4, "result_type"

    iget-object v1, p0, LX/8nR;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-virtual {v1}, Lcom/facebook/auth/viewercontext/ViewerContext;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "mobile_pages_manager_tagger"

    :goto_1
    invoke-virtual {v3, v4, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1400998
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    .line 1400999
    iget-object v2, p0, LX/8nR;->b:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v8

    .line 1401000
    new-instance v1, LX/8nQ;

    move-object v2, p0

    move v3, p3

    move v4, p4

    move v5, p5

    move-object/from16 v6, p8

    move-object v7, p1

    invoke-direct/range {v1 .. v7}, LX/8nQ;-><init>(LX/8nR;ZZZLX/8JX;Ljava/lang/CharSequence;)V

    invoke-static {v8, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_0

    .line 1401001
    :cond_1
    const-string v1, "mobile_android_tagger"

    goto :goto_1
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1400992
    const/4 v0, 0x1

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1400991
    const-string v0, "graphql_search"

    return-object v0
.end method
