.class public final LX/9eC;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)V
    .locals 0

    .prologue
    .line 1519327
    iput-object p1, p0, LX/9eC;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 4

    .prologue
    .line 1519328
    iget-object v0, p0, LX/9eC;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v0, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    sget-object v1, LX/9eK;->INIT:LX/9eK;

    if-ne v0, v1, :cond_0

    .line 1519329
    iget-object v0, p0, LX/9eC;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    invoke-static {v0, p1}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->d(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;Z)V

    .line 1519330
    :goto_0
    return-void

    .line 1519331
    :cond_0
    iget-object v0, p0, LX/9eC;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v0, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->z:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "PhotoAnimationDialogFragment received call to onAnimationReady in invalid state "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/9eC;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v3, v3, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    invoke-virtual {v3}, LX/9eK;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
