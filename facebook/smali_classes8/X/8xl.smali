.class public LX/8xl;
.super LX/16T;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field private static volatile d:LX/8xl;


# instance fields
.field public final b:LX/0iA;

.field public c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1424674
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SOCIAL_SEARCH_SEEKER_FEED_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/8xl;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>(LX/0iA;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1424690
    invoke-direct {p0}, LX/16T;-><init>()V

    .line 1424691
    iput-object p1, p0, LX/8xl;->b:LX/0iA;

    .line 1424692
    return-void
.end method

.method public static a(LX/0QB;)LX/8xl;
    .locals 4

    .prologue
    .line 1424677
    sget-object v0, LX/8xl;->d:LX/8xl;

    if-nez v0, :cond_1

    .line 1424678
    const-class v1, LX/8xl;

    monitor-enter v1

    .line 1424679
    :try_start_0
    sget-object v0, LX/8xl;->d:LX/8xl;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1424680
    if-eqz v2, :cond_0

    .line 1424681
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1424682
    new-instance p0, LX/8xl;

    invoke-static {v0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v3

    check-cast v3, LX/0iA;

    invoke-direct {p0, v3}, LX/8xl;-><init>(LX/0iA;)V

    .line 1424683
    move-object v0, p0

    .line 1424684
    sput-object v0, LX/8xl;->d:LX/8xl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1424685
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1424686
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1424687
    :cond_1
    sget-object v0, LX/8xl;->d:LX/8xl;

    return-object v0

    .line 1424688
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1424689
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 1

    .prologue
    .line 1424693
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1424676
    const-string v0, "4481"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1424675
    sget-object v0, LX/8xl;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
