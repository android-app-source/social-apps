.class public final LX/9bu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:LX/9bx;


# direct methods
.method public constructor <init>(LX/9bx;)V
    .locals 0

    .prologue
    .line 1515299
    iput-object p1, p0, LX/9bu;->a:LX/9bx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1515300
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    .line 1515301
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    .line 1515302
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    move v2, v1

    .line 1515303
    :cond_1
    return v2

    .line 1515304
    :pswitch_0
    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    .line 1515305
    iput-boolean v1, v0, LX/9bx;->F:Z

    .line 1515306
    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    .line 1515307
    iput v3, v0, LX/9bx;->c:F

    .line 1515308
    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    .line 1515309
    iput v4, v0, LX/9bx;->d:F

    .line 1515310
    iget-object v5, p0, LX/9bu;->a:LX/9bx;

    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    iget-object v0, v0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    sub-float v0, v3, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget-object v6, p0, LX/9bu;->a:LX/9bx;

    iget v6, v6, LX/9bx;->A:F

    cmpg-float v0, v0, v6

    if-gez v0, :cond_3

    move v0, v1

    .line 1515311
    :goto_1
    iput-boolean v0, v5, LX/9bx;->e:Z

    .line 1515312
    iget-object v5, p0, LX/9bu;->a:LX/9bx;

    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    iget-object v0, v0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->right:F

    sub-float v0, v3, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget-object v6, p0, LX/9bu;->a:LX/9bx;

    iget v6, v6, LX/9bx;->A:F

    cmpg-float v0, v0, v6

    if-gez v0, :cond_4

    move v0, v1

    .line 1515313
    :goto_2
    iput-boolean v0, v5, LX/9bx;->f:Z

    .line 1515314
    iget-object v5, p0, LX/9bu;->a:LX/9bx;

    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    iget-object v0, v0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->top:F

    sub-float v0, v4, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget-object v6, p0, LX/9bu;->a:LX/9bx;

    iget v6, v6, LX/9bx;->A:F

    cmpg-float v0, v0, v6

    if-gez v0, :cond_5

    move v0, v1

    .line 1515315
    :goto_3
    iput-boolean v0, v5, LX/9bx;->g:Z

    .line 1515316
    iget-object v5, p0, LX/9bu;->a:LX/9bx;

    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    iget-object v0, v0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    sub-float v0, v4, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget-object v6, p0, LX/9bu;->a:LX/9bx;

    iget v6, v6, LX/9bx;->A:F

    cmpg-float v0, v0, v6

    if-gez v0, :cond_6

    move v0, v1

    .line 1515317
    :goto_4
    iput-boolean v0, v5, LX/9bx;->h:Z

    .line 1515318
    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    iget-object v5, p0, LX/9bu;->a:LX/9bx;

    iget-boolean v5, v5, LX/9bx;->e:Z

    iget-object v6, p0, LX/9bu;->a:LX/9bx;

    iget-boolean v6, v6, LX/9bx;->g:Z

    and-int/2addr v5, v6

    .line 1515319
    iput-boolean v5, v0, LX/9bx;->i:Z

    .line 1515320
    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    iget-object v5, p0, LX/9bu;->a:LX/9bx;

    iget-boolean v5, v5, LX/9bx;->e:Z

    iget-object v6, p0, LX/9bu;->a:LX/9bx;

    iget-boolean v6, v6, LX/9bx;->h:Z

    and-int/2addr v5, v6

    .line 1515321
    iput-boolean v5, v0, LX/9bx;->j:Z

    .line 1515322
    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    iget-object v5, p0, LX/9bu;->a:LX/9bx;

    iget-boolean v5, v5, LX/9bx;->f:Z

    iget-object v6, p0, LX/9bu;->a:LX/9bx;

    iget-boolean v6, v6, LX/9bx;->g:Z

    and-int/2addr v5, v6

    .line 1515323
    iput-boolean v5, v0, LX/9bx;->k:Z

    .line 1515324
    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    iget-object v5, p0, LX/9bu;->a:LX/9bx;

    iget-boolean v5, v5, LX/9bx;->f:Z

    iget-object v6, p0, LX/9bu;->a:LX/9bx;

    iget-boolean v6, v6, LX/9bx;->h:Z

    and-int/2addr v5, v6

    .line 1515325
    iput-boolean v5, v0, LX/9bx;->l:Z

    .line 1515326
    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    iget-boolean v0, v0, LX/9bx;->H:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    iget-boolean v0, v0, LX/9bx;->i:Z

    if-nez v0, :cond_2

    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    iget-boolean v0, v0, LX/9bx;->j:Z

    if-nez v0, :cond_2

    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    iget-boolean v0, v0, LX/9bx;->k:Z

    if-nez v0, :cond_2

    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    iget-boolean v0, v0, LX/9bx;->l:Z

    if-eqz v0, :cond_7

    .line 1515327
    :cond_2
    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    sget-object v2, LX/9bw;->RESIZE_WITH_RATIO:LX/9bw;

    .line 1515328
    iput-object v2, v0, LX/9bx;->b:LX/9bw;

    .line 1515329
    goto/16 :goto_0

    :cond_3
    move v0, v2

    .line 1515330
    goto/16 :goto_1

    :cond_4
    move v0, v2

    .line 1515331
    goto/16 :goto_2

    :cond_5
    move v0, v2

    .line 1515332
    goto :goto_3

    :cond_6
    move v0, v2

    .line 1515333
    goto :goto_4

    .line 1515334
    :cond_7
    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    iget-boolean v0, v0, LX/9bx;->e:Z

    if-nez v0, :cond_8

    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    iget-boolean v0, v0, LX/9bx;->f:Z

    if-nez v0, :cond_8

    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    iget-boolean v0, v0, LX/9bx;->g:Z

    if-nez v0, :cond_8

    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    iget-boolean v0, v0, LX/9bx;->h:Z

    if-eqz v0, :cond_18

    .line 1515335
    :cond_8
    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    iget-object v0, v0, LX/9bx;->G:LX/9bv;

    sget-object v5, LX/9bv;->FREE_FORM:LX/9bv;

    if-ne v0, v5, :cond_9

    .line 1515336
    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    sget-object v2, LX/9bw;->RESIZE:LX/9bw;

    .line 1515337
    iput-object v2, v0, LX/9bx;->b:LX/9bw;

    .line 1515338
    goto/16 :goto_0

    .line 1515339
    :cond_9
    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    iget-boolean v0, v0, LX/9bx;->H:Z

    if-eqz v0, :cond_0

    .line 1515340
    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    iget-object v0, v0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    sub-float v0, v3, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget-object v5, p0, LX/9bu;->a:LX/9bx;

    iget-object v5, v5, LX/9bx;->t:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->right:F

    sub-float/2addr v3, v5

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpg-float v0, v0, v3

    if-gez v0, :cond_13

    move v0, v1

    .line 1515341
    :goto_5
    iget-object v3, p0, LX/9bu;->a:LX/9bx;

    iget-object v3, v3, LX/9bx;->t:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    sub-float v3, v4, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget-object v5, p0, LX/9bu;->a:LX/9bx;

    iget-object v5, v5, LX/9bx;->t:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpg-float v3, v3, v4

    if-gez v3, :cond_14

    move v3, v1

    .line 1515342
    :goto_6
    iget-object v5, p0, LX/9bu;->a:LX/9bx;

    iget-object v4, p0, LX/9bu;->a:LX/9bx;

    iget-boolean v4, v4, LX/9bx;->e:Z

    if-eqz v4, :cond_a

    if-nez v3, :cond_b

    :cond_a
    iget-object v4, p0, LX/9bu;->a:LX/9bx;

    iget-boolean v4, v4, LX/9bx;->g:Z

    if-eqz v4, :cond_15

    if-eqz v0, :cond_15

    :cond_b
    move v4, v1

    .line 1515343
    :goto_7
    iput-boolean v4, v5, LX/9bx;->i:Z

    .line 1515344
    iget-object v5, p0, LX/9bu;->a:LX/9bx;

    iget-object v4, p0, LX/9bu;->a:LX/9bx;

    iget-boolean v4, v4, LX/9bx;->f:Z

    if-eqz v4, :cond_c

    if-nez v3, :cond_d

    :cond_c
    iget-object v4, p0, LX/9bu;->a:LX/9bx;

    iget-boolean v4, v4, LX/9bx;->g:Z

    if-eqz v4, :cond_16

    if-nez v0, :cond_16

    :cond_d
    move v4, v1

    .line 1515345
    :goto_8
    iput-boolean v4, v5, LX/9bx;->k:Z

    .line 1515346
    iget-object v5, p0, LX/9bu;->a:LX/9bx;

    iget-object v4, p0, LX/9bu;->a:LX/9bx;

    iget-boolean v4, v4, LX/9bx;->e:Z

    if-eqz v4, :cond_e

    if-eqz v3, :cond_f

    :cond_e
    iget-object v4, p0, LX/9bu;->a:LX/9bx;

    iget-boolean v4, v4, LX/9bx;->h:Z

    if-eqz v4, :cond_17

    if-eqz v0, :cond_17

    :cond_f
    move v4, v1

    .line 1515347
    :goto_9
    iput-boolean v4, v5, LX/9bx;->j:Z

    .line 1515348
    iget-object v4, p0, LX/9bu;->a:LX/9bx;

    iget-object v5, p0, LX/9bu;->a:LX/9bx;

    iget-boolean v5, v5, LX/9bx;->f:Z

    if-eqz v5, :cond_10

    if-eqz v3, :cond_11

    :cond_10
    iget-object v3, p0, LX/9bu;->a:LX/9bx;

    iget-boolean v3, v3, LX/9bx;->h:Z

    if-eqz v3, :cond_12

    if-nez v0, :cond_12

    :cond_11
    move v2, v1

    .line 1515349
    :cond_12
    iput-boolean v2, v4, LX/9bx;->l:Z

    .line 1515350
    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    sget-object v2, LX/9bw;->RESIZE_WITH_RATIO:LX/9bw;

    .line 1515351
    iput-object v2, v0, LX/9bx;->b:LX/9bw;

    .line 1515352
    goto/16 :goto_0

    :cond_13
    move v0, v2

    .line 1515353
    goto :goto_5

    :cond_14
    move v3, v2

    .line 1515354
    goto :goto_6

    :cond_15
    move v4, v2

    .line 1515355
    goto :goto_7

    :cond_16
    move v4, v2

    .line 1515356
    goto :goto_8

    :cond_17
    move v4, v2

    .line 1515357
    goto :goto_9

    .line 1515358
    :cond_18
    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    iget-object v0, v0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v0, v3, v4}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1515359
    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    sget-object v2, LX/9bw;->MOVE:LX/9bw;

    .line 1515360
    iput-object v2, v0, LX/9bx;->b:LX/9bw;

    .line 1515361
    goto/16 :goto_0

    .line 1515362
    :pswitch_1
    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    .line 1515363
    iput-boolean v1, v0, LX/9bx;->F:Z

    .line 1515364
    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    iget-object v0, v0, LX/9bx;->b:LX/9bw;

    sget-object v5, LX/9bw;->RESIZE_WITH_RATIO:LX/9bw;

    if-ne v0, v5, :cond_19

    .line 1515365
    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    .line 1515366
    invoke-static {v0, v3, v4}, LX/9bx;->b$redex0(LX/9bx;FF)V

    .line 1515367
    :goto_a
    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    .line 1515368
    iput v3, v0, LX/9bx;->c:F

    .line 1515369
    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    .line 1515370
    iput v4, v0, LX/9bx;->d:F

    .line 1515371
    goto/16 :goto_0

    .line 1515372
    :cond_19
    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    iget-object v0, v0, LX/9bx;->b:LX/9bw;

    sget-object v5, LX/9bw;->RESIZE:LX/9bw;

    if-ne v0, v5, :cond_22

    .line 1515373
    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    .line 1515374
    iget v2, v0, LX/9bx;->c:F

    sub-float v2, v3, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v5, v0, LX/9bx;->B:F

    cmpl-float v2, v2, v5

    if-lez v2, :cond_1d

    .line 1515375
    iget-boolean v2, v0, LX/9bx;->e:Z

    if-eqz v2, :cond_1b

    iget v2, v0, LX/9bx;->c:F

    cmpg-float v2, v3, v2

    if-ltz v2, :cond_1a

    iget-object v2, v0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    iget v5, v0, LX/9bx;->c:F

    sub-float v5, v3, v5

    sub-float/2addr v2, v5

    iget v5, v0, LX/9bx;->C:F

    cmpl-float v2, v2, v5

    if-lez v2, :cond_1b

    .line 1515376
    :cond_1a
    iget v2, v0, LX/9bx;->o:F

    iget v5, v0, LX/9bx;->c:F

    sub-float v5, v3, v5

    add-float/2addr v2, v5

    iget-object v5, v0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    invoke-static {v2, v5}, Ljava/lang/Math;->max(FF)F

    move-result v2

    iput v2, v0, LX/9bx;->o:F

    .line 1515377
    iget v2, v0, LX/9bx;->c:F

    cmpg-float v2, v3, v2

    if-gez v2, :cond_26

    iget v2, v0, LX/9bx;->o:F

    iget-object v5, v0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v5

    iget v5, v0, LX/9bx;->q:F

    cmpg-float v2, v2, v5

    if-gez v2, :cond_26

    .line 1515378
    iget-object v5, v0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v2, v0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iput v2, v0, LX/9bx;->o:F

    .line 1515379
    :goto_b
    iput v2, v5, Landroid/graphics/RectF;->left:F

    .line 1515380
    :cond_1b
    iget-boolean v2, v0, LX/9bx;->f:Z

    if-eqz v2, :cond_1d

    iget v2, v0, LX/9bx;->c:F

    cmpl-float v2, v3, v2

    if-gtz v2, :cond_1c

    iget-object v2, v0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    iget v5, v0, LX/9bx;->c:F

    sub-float v5, v3, v5

    add-float/2addr v2, v5

    iget v5, v0, LX/9bx;->C:F

    cmpl-float v2, v2, v5

    if-lez v2, :cond_1d

    .line 1515381
    :cond_1c
    iget v2, v0, LX/9bx;->p:F

    iget v5, v0, LX/9bx;->c:F

    sub-float v5, v3, v5

    add-float/2addr v2, v5

    iget-object v5, v0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->right:F

    invoke-static {v2, v5}, Ljava/lang/Math;->min(FF)F

    move-result v2

    iput v2, v0, LX/9bx;->p:F

    .line 1515382
    iget v2, v0, LX/9bx;->c:F

    cmpl-float v2, v3, v2

    if-lez v2, :cond_28

    iget-object v2, v0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    iget v5, v0, LX/9bx;->p:F

    sub-float/2addr v2, v5

    iget v5, v0, LX/9bx;->q:F

    cmpg-float v2, v2, v5

    if-gez v2, :cond_28

    .line 1515383
    iget-object v5, v0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v2, v0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    iput v2, v0, LX/9bx;->p:F

    .line 1515384
    :goto_c
    iput v2, v5, Landroid/graphics/RectF;->right:F

    .line 1515385
    :cond_1d
    iget v2, v0, LX/9bx;->d:F

    sub-float v2, v4, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v5, v0, LX/9bx;->B:F

    cmpl-float v2, v2, v5

    if-lez v2, :cond_21

    .line 1515386
    iget-boolean v2, v0, LX/9bx;->g:Z

    if-eqz v2, :cond_1f

    iget v2, v0, LX/9bx;->d:F

    cmpg-float v2, v4, v2

    if-ltz v2, :cond_1e

    iget-object v2, v0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    iget v5, v0, LX/9bx;->d:F

    sub-float v5, v4, v5

    sub-float/2addr v2, v5

    iget v5, v0, LX/9bx;->C:F

    cmpl-float v2, v2, v5

    if-lez v2, :cond_1f

    .line 1515387
    :cond_1e
    iget v2, v0, LX/9bx;->n:F

    iget v5, v0, LX/9bx;->d:F

    sub-float v5, v4, v5

    add-float/2addr v2, v5

    iget-object v5, v0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    invoke-static {v2, v5}, Ljava/lang/Math;->max(FF)F

    move-result v2

    iput v2, v0, LX/9bx;->n:F

    .line 1515388
    iget v2, v0, LX/9bx;->d:F

    cmpg-float v2, v4, v2

    if-gez v2, :cond_2a

    iget v2, v0, LX/9bx;->n:F

    iget-object v5, v0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v5

    iget v5, v0, LX/9bx;->q:F

    cmpg-float v2, v2, v5

    if-gez v2, :cond_2a

    .line 1515389
    iget-object v5, v0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v2, v0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    iput v2, v0, LX/9bx;->n:F

    .line 1515390
    :goto_d
    iput v2, v5, Landroid/graphics/RectF;->top:F

    .line 1515391
    :cond_1f
    iget-boolean v2, v0, LX/9bx;->h:Z

    if-eqz v2, :cond_21

    iget v2, v0, LX/9bx;->d:F

    cmpl-float v2, v4, v2

    if-gtz v2, :cond_20

    iget-object v2, v0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    iget v5, v0, LX/9bx;->d:F

    sub-float v5, v4, v5

    add-float/2addr v2, v5

    iget v5, v0, LX/9bx;->C:F

    cmpl-float v2, v2, v5

    if-lez v2, :cond_21

    .line 1515392
    :cond_20
    iget v2, v0, LX/9bx;->m:F

    iget v5, v0, LX/9bx;->d:F

    sub-float v5, v4, v5

    add-float/2addr v2, v5

    iget-object v5, v0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    invoke-static {v2, v5}, Ljava/lang/Math;->min(FF)F

    move-result v2

    iput v2, v0, LX/9bx;->m:F

    .line 1515393
    iget v2, v0, LX/9bx;->d:F

    cmpl-float v2, v4, v2

    if-lez v2, :cond_2c

    iget-object v2, v0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    iget v5, v0, LX/9bx;->m:F

    sub-float/2addr v2, v5

    iget v5, v0, LX/9bx;->q:F

    cmpg-float v2, v2, v5

    if-gez v2, :cond_2c

    .line 1515394
    iget-object v5, v0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v2, v0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    iput v2, v0, LX/9bx;->m:F

    .line 1515395
    :goto_e
    iput v2, v5, Landroid/graphics/RectF;->bottom:F

    .line 1515396
    :cond_21
    iget-object v2, v0, LX/9bx;->E:Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;

    const/4 v5, 0x1

    iput-boolean v5, v2, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->f:Z

    .line 1515397
    invoke-virtual {v0}, LX/9bx;->invalidate()V

    .line 1515398
    goto/16 :goto_a

    .line 1515399
    :cond_22
    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    iget-object v0, v0, LX/9bx;->b:LX/9bw;

    sget-object v5, LX/9bw;->MOVE:LX/9bw;

    if-ne v0, v5, :cond_1

    .line 1515400
    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    .line 1515401
    iget-object v2, v0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iget v5, v0, LX/9bx;->c:F

    sub-float v5, v3, v5

    add-float/2addr v2, v5

    .line 1515402
    iget-object v5, v0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    iget v6, v0, LX/9bx;->d:F

    sub-float v6, v4, v6

    add-float/2addr v5, v6

    .line 1515403
    iget-object v6, v0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v6

    add-float/2addr v6, v2

    .line 1515404
    iget-object v7, v0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    move-result v7

    add-float/2addr v7, v5

    .line 1515405
    iget-object v8, v0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v8}, Landroid/graphics/RectF;->width()F

    move-result v8

    .line 1515406
    iget-object p1, v0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result p1

    .line 1515407
    iget-object p2, v0, LX/9bx;->r:Landroid/graphics/RectF;

    iget p2, p2, Landroid/graphics/RectF;->right:F

    cmpl-float v6, v6, p2

    if-lez v6, :cond_2e

    .line 1515408
    iget-object v2, v0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    sub-float/2addr v2, v8

    .line 1515409
    :cond_23
    :goto_f
    iget-object v6, v0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    cmpl-float v6, v7, v6

    if-lez v6, :cond_2f

    .line 1515410
    iget-object v5, v0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v5, p1

    .line 1515411
    :cond_24
    :goto_10
    iget-object v6, v0, LX/9bx;->t:Landroid/graphics/RectF;

    iput v2, v0, LX/9bx;->o:F

    iput v2, v6, Landroid/graphics/RectF;->left:F

    .line 1515412
    iget-object v6, v0, LX/9bx;->t:Landroid/graphics/RectF;

    iput v5, v0, LX/9bx;->n:F

    iput v5, v6, Landroid/graphics/RectF;->top:F

    .line 1515413
    iget-object v6, v0, LX/9bx;->t:Landroid/graphics/RectF;

    add-float/2addr v2, v8

    iput v2, v0, LX/9bx;->p:F

    iput v2, v6, Landroid/graphics/RectF;->right:F

    .line 1515414
    iget-object v2, v0, LX/9bx;->t:Landroid/graphics/RectF;

    add-float/2addr v5, p1

    iput v5, v0, LX/9bx;->m:F

    iput v5, v2, Landroid/graphics/RectF;->bottom:F

    .line 1515415
    iget-object v2, v0, LX/9bx;->E:Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;

    const/4 v5, 0x1

    iput-boolean v5, v2, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->g:Z

    .line 1515416
    invoke-virtual {v0}, LX/9bx;->invalidate()V

    .line 1515417
    goto/16 :goto_a

    .line 1515418
    :pswitch_2
    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    .line 1515419
    iput-boolean v2, v0, LX/9bx;->F:Z

    .line 1515420
    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    const/4 v3, 0x0

    .line 1515421
    iput-object v3, v0, LX/9bx;->b:LX/9bw;

    .line 1515422
    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    iget-object v3, p0, LX/9bu;->a:LX/9bx;

    iget-object v4, p0, LX/9bu;->a:LX/9bx;

    iget-object v5, p0, LX/9bu;->a:LX/9bx;

    .line 1515423
    iput-boolean v2, v5, LX/9bx;->h:Z

    move v2, v2

    .line 1515424
    iput-boolean v2, v4, LX/9bx;->g:Z

    move v2, v2

    .line 1515425
    iput-boolean v2, v3, LX/9bx;->f:Z

    move v2, v2

    .line 1515426
    iput-boolean v2, v0, LX/9bx;->e:Z

    .line 1515427
    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    iget-object v0, v0, LX/9bx;->D:LX/9eX;

    if-eqz v0, :cond_25

    .line 1515428
    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    iget-object v0, v0, LX/9bx;->E:Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;

    iget v2, v0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->h:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;->h:I

    .line 1515429
    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    iget-object v0, v0, LX/9bx;->D:LX/9eX;

    new-instance v2, Landroid/graphics/RectF;

    iget-object v3, p0, LX/9bu;->a:LX/9bx;

    iget-object v3, v3, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-direct {v2, v3}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    invoke-virtual {v0, v2, v1}, LX/9eX;->a(Landroid/graphics/RectF;Z)V

    .line 1515430
    :cond_25
    iget-object v0, p0, LX/9bu;->a:LX/9bx;

    invoke-virtual {v0}, LX/9bx;->invalidate()V

    goto/16 :goto_0

    .line 1515431
    :cond_26
    iget-object v5, v0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v2, v0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    iget v6, v0, LX/9bx;->o:F

    sub-float/2addr v2, v6

    iget-object v6, v0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    sub-float/2addr v2, v6

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v6, v0, LX/9bx;->q:F

    cmpg-float v2, v2, v6

    if-gez v2, :cond_27

    iget-object v2, v0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    iget-object v6, v0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    sub-float/2addr v2, v6

    iget-object v6, v0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    invoke-static {v2, v6}, Ljava/lang/Math;->max(FF)F

    move-result v2

    goto/16 :goto_b

    :cond_27
    iget v2, v0, LX/9bx;->o:F

    goto/16 :goto_b

    .line 1515432
    :cond_28
    iget-object v5, v0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v2, v0, LX/9bx;->p:F

    iget-object v6, v0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v6

    iget-object v6, v0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    sub-float/2addr v2, v6

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v6, v0, LX/9bx;->q:F

    cmpg-float v2, v2, v6

    if-gez v2, :cond_29

    iget-object v2, v0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iget-object v6, v0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    add-float/2addr v2, v6

    iget-object v6, v0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->right:F

    invoke-static {v2, v6}, Ljava/lang/Math;->min(FF)F

    move-result v2

    goto/16 :goto_c

    :cond_29
    iget v2, v0, LX/9bx;->p:F

    goto/16 :goto_c

    .line 1515433
    :cond_2a
    iget-object v5, v0, LX/9bx;->t:Landroid/graphics/RectF;

    iget-object v2, v0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    iget v6, v0, LX/9bx;->n:F

    sub-float/2addr v2, v6

    iget-object v6, v0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v6

    sub-float/2addr v2, v6

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v6, v0, LX/9bx;->q:F

    cmpg-float v2, v2, v6

    if-gez v2, :cond_2b

    iget-object v2, v0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    iget-object v6, v0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v6

    sub-float/2addr v2, v6

    iget-object v6, v0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    invoke-static {v2, v6}, Ljava/lang/Math;->max(FF)F

    move-result v2

    goto/16 :goto_d

    :cond_2b
    iget v2, v0, LX/9bx;->n:F

    goto/16 :goto_d

    .line 1515434
    :cond_2c
    iget-object v5, v0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v2, v0, LX/9bx;->m:F

    iget-object v6, v0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v6

    iget-object v6, v0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v6

    sub-float/2addr v2, v6

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v6, v0, LX/9bx;->q:F

    cmpg-float v2, v2, v6

    if-gez v2, :cond_2d

    iget-object v2, v0, LX/9bx;->t:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    iget-object v6, v0, LX/9bx;->t:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v6

    add-float/2addr v2, v6

    iget-object v6, v0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    invoke-static {v2, v6}, Ljava/lang/Math;->min(FF)F

    move-result v2

    goto/16 :goto_e

    :cond_2d
    iget v2, v0, LX/9bx;->m:F

    goto/16 :goto_e

    .line 1515435
    :cond_2e
    iget-object v6, v0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    cmpg-float v6, v2, v6

    if-gez v6, :cond_23

    .line 1515436
    iget-object v2, v0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    goto/16 :goto_f

    .line 1515437
    :cond_2f
    iget-object v6, v0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    cmpg-float v6, v5, v6

    if-gez v6, :cond_24

    .line 1515438
    iget-object v5, v0, LX/9bx;->r:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    goto/16 :goto_10

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
