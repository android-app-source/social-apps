.class public LX/8xR;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1424148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1424149
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;LX/5pC;)V
    .locals 8
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1424150
    const-string v0, "LoginPreferences"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1424151
    if-eqz p1, :cond_5

    .line 1424152
    const-string v1, "UserId"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1424153
    :goto_0
    if-eqz p2, :cond_6

    .line 1424154
    const-string v1, "AccessToken"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1424155
    :goto_1
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1424156
    invoke-static {p0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    move-result-object v2

    .line 1424157
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_0

    .line 1424158
    invoke-virtual {v2}, Landroid/webkit/CookieSyncManager;->sync()V

    .line 1424159
    :cond_0
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v3

    .line 1424160
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Landroid/webkit/CookieManager;->setAcceptCookie(Z)V

    .line 1424161
    invoke-virtual {v3}, Landroid/webkit/CookieManager;->removeAllCookie()V

    .line 1424162
    if-eqz p3, :cond_4

    .line 1424163
    invoke-interface {p3}, LX/5pC;->size()I

    move-result v4

    .line 1424164
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    if-ge v1, v4, :cond_4

    .line 1424165
    invoke-interface {p3, v1}, LX/5pC;->getType(I)Lcom/facebook/react/bridge/ReadableType;

    move-result-object v0

    sget-object v5, Lcom/facebook/react/bridge/ReadableType;->String:Lcom/facebook/react/bridge/ReadableType;

    if-ne v0, v5, :cond_2

    invoke-interface {p3, v1}, LX/5pC;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1424166
    :goto_3
    if-eqz v0, :cond_1

    .line 1424167
    const-string v5, "https://m.facebook.com"

    invoke-virtual {v3, v5, v0}, Landroid/webkit/CookieManager;->setCookie(Ljava/lang/String;Ljava/lang/String;)V

    .line 1424168
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1424169
    :cond_2
    invoke-interface {p3, v1}, LX/5pC;->a(I)LX/5pG;

    move-result-object v0

    .line 1424170
    const-string v5, "name"

    invoke-interface {v0, v5}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1424171
    const-string v5, "value"

    invoke-interface {v0, v5}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1424172
    const-string v5, "domain"

    invoke-interface {v0, v5}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 1424173
    const-string v5, "path"

    invoke-interface {v0, v5}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 1424174
    if-eqz v6, :cond_3

    if-eqz v7, :cond_3

    if-eqz p1, :cond_3

    if-nez p2, :cond_b

    .line 1424175
    :cond_3
    const-string v5, "React"

    const-string v6, "Ignoring FB session cookie missing required attributes"

    invoke-static {v5, v6}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1424176
    const/4 v5, 0x0

    .line 1424177
    :goto_4
    move-object v5, v5

    .line 1424178
    if-nez v5, :cond_7

    const/4 v5, 0x0

    :goto_5
    move-object v0, v5

    .line 1424179
    goto :goto_3

    .line 1424180
    :cond_4
    invoke-virtual {v2}, Landroid/webkit/CookieSyncManager;->sync()V

    .line 1424181
    return-void

    .line 1424182
    :cond_5
    const-string v1, "UserId"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 1424183
    :cond_6
    const-string v1, "AccessToken"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_1

    .line 1424184
    :cond_7
    new-instance v6, LX/8xT;

    invoke-direct {v6}, LX/8xT;-><init>()V

    .line 1424185
    iget-object v7, v5, LX/8xW;->a:Ljava/lang/String;

    move-object v7, v7

    .line 1424186
    iget-object p1, v5, LX/8xW;->b:Ljava/lang/String;

    move-object p1, p1

    .line 1424187
    invoke-virtual {v6, v7, p1}, LX/8xT;->a(Ljava/lang/String;Ljava/lang/String;)LX/8xT;

    move-result-object v6

    .line 1424188
    iget-object v7, v5, LX/8xW;->c:Ljava/lang/String;

    move-object v7, v7

    .line 1424189
    if-eqz v7, :cond_8

    .line 1424190
    const-string v7, "Expires"

    .line 1424191
    iget-object p1, v5, LX/8xW;->c:Ljava/lang/String;

    move-object p1, p1

    .line 1424192
    invoke-virtual {v6, v7, p1}, LX/8xT;->a(Ljava/lang/String;Ljava/lang/String;)LX/8xT;

    .line 1424193
    :cond_8
    const-string v7, "Domain"

    .line 1424194
    iget-object p1, v5, LX/8xW;->d:Ljava/lang/String;

    move-object p1, p1

    .line 1424195
    invoke-virtual {v6, v7, p1}, LX/8xT;->a(Ljava/lang/String;Ljava/lang/String;)LX/8xT;

    move-result-object v7

    const-string p1, "Path"

    .line 1424196
    iget-object p2, v5, LX/8xW;->f:Ljava/lang/String;

    move-object p2, p2

    .line 1424197
    invoke-virtual {v7, p1, p2}, LX/8xT;->a(Ljava/lang/String;Ljava/lang/String;)LX/8xT;

    .line 1424198
    iget-boolean v7, v5, LX/8xW;->e:Z

    move v7, v7

    .line 1424199
    if-eqz v7, :cond_a

    .line 1424200
    const-string v7, "secure"

    .line 1424201
    iget-object p1, v6, LX/8xT;->a:Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result p1

    if-lez p1, :cond_9

    .line 1424202
    iget-object p1, v6, LX/8xT;->a:Ljava/lang/StringBuilder;

    const-string p2, "; "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1424203
    :cond_9
    iget-object p1, v6, LX/8xT;->a:Ljava/lang/StringBuilder;

    invoke-virtual {p1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1424204
    :cond_a
    iget-object v7, v6, LX/8xT;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object v6, v7

    .line 1424205
    move-object v5, v6

    .line 1424206
    goto :goto_5

    .line 1424207
    :cond_b
    const-string v5, "secure"

    invoke-interface {v0, v5}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_c

    const-string v5, "secure"

    invoke-interface {v0, v5}, LX/5pG;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_c

    const/4 v5, 0x1

    .line 1424208
    :goto_6
    new-instance p0, LX/8xV;

    invoke-direct {p0}, LX/8xV;-><init>()V

    .line 1424209
    iput-object v6, p0, LX/8xV;->a:Ljava/lang/String;

    .line 1424210
    move-object v6, p0

    .line 1424211
    iput-object v7, v6, LX/8xV;->b:Ljava/lang/String;

    .line 1424212
    move-object v6, v6

    .line 1424213
    iput-object p1, v6, LX/8xV;->d:Ljava/lang/String;

    .line 1424214
    move-object v6, v6

    .line 1424215
    iput-object p2, v6, LX/8xV;->f:Ljava/lang/String;

    .line 1424216
    move-object v6, v6

    .line 1424217
    const-string v7, "expires"

    invoke-interface {v0, v7}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1424218
    iput-object v7, v6, LX/8xV;->c:Ljava/lang/String;

    .line 1424219
    move-object v6, v6

    .line 1424220
    iput-boolean v5, v6, LX/8xV;->e:Z

    .line 1424221
    move-object v5, v6

    .line 1424222
    new-instance v6, LX/8xW;

    invoke-direct {v6, v5}, LX/8xW;-><init>(LX/8xV;)V

    move-object v5, v6

    .line 1424223
    goto/16 :goto_4

    .line 1424224
    :cond_c
    const/4 v5, 0x0

    goto :goto_6
.end method

.method public static b(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1424225
    const-string v0, "LoginPreferences"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "UserId"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
