.class public final LX/9xV;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 1590107
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_b

    .line 1590108
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1590109
    :goto_0
    return v1

    .line 1590110
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1590111
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_a

    .line 1590112
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1590113
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1590114
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_1

    if-eqz v9, :cond_1

    .line 1590115
    const-string v10, "__type__"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_2

    const-string v10, "__typename"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 1590116
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v8

    goto :goto_1

    .line 1590117
    :cond_3
    const-string v10, "creation_story"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 1590118
    invoke-static {p0, p1}, LX/9q6;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1590119
    :cond_4
    const-string v10, "focus"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 1590120
    invoke-static {p0, p1}, LX/4aC;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1590121
    :cond_5
    const-string v10, "id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 1590122
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 1590123
    :cond_6
    const-string v10, "image"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 1590124
    invoke-static {p0, p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1590125
    :cond_7
    const-string v10, "imageHigh"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 1590126
    invoke-static {p0, p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1590127
    :cond_8
    const-string v10, "imageLow"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 1590128
    invoke-static {p0, p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 1590129
    :cond_9
    const-string v10, "imageMedium"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1590130
    invoke-static {p0, p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v0

    goto/16 :goto_1

    .line 1590131
    :cond_a
    const/16 v9, 0x8

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1590132
    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 1590133
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 1590134
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 1590135
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1590136
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1590137
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1590138
    const/4 v1, 0x6

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1590139
    const/4 v1, 0x7

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1590140
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_b
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1590141
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1590142
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1590143
    if-eqz v0, :cond_0

    .line 1590144
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1590145
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1590146
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1590147
    if-eqz v0, :cond_1

    .line 1590148
    const-string v1, "creation_story"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1590149
    invoke-static {p0, v0, p2, p3}, LX/9q6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1590150
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1590151
    if-eqz v0, :cond_2

    .line 1590152
    const-string v1, "focus"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1590153
    invoke-static {p0, v0, p2}, LX/4aC;->a(LX/15i;ILX/0nX;)V

    .line 1590154
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1590155
    if-eqz v0, :cond_3

    .line 1590156
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1590157
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1590158
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1590159
    if-eqz v0, :cond_4

    .line 1590160
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1590161
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1590162
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1590163
    if-eqz v0, :cond_5

    .line 1590164
    const-string v1, "imageHigh"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1590165
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1590166
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1590167
    if-eqz v0, :cond_6

    .line 1590168
    const-string v1, "imageLow"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1590169
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1590170
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1590171
    if-eqz v0, :cond_7

    .line 1590172
    const-string v1, "imageMedium"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1590173
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1590174
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1590175
    return-void
.end method
