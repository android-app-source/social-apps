.class public LX/9D4;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1454735
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/video/engine/VideoPlayerParams;
    .locals 2

    .prologue
    .line 1454710
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aS()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 1454711
    :goto_0
    invoke-static {}, Lcom/facebook/video/engine/VideoDataSource;->newBuilder()LX/2oE;

    move-result-object v1

    .line 1454712
    iput-object v0, v1, LX/2oE;->a:Landroid/net/Uri;

    .line 1454713
    move-object v0, v1

    .line 1454714
    sget-object v1, LX/097;->FROM_STREAM:LX/097;

    .line 1454715
    iput-object v1, v0, LX/2oE;->e:LX/097;

    .line 1454716
    move-object v0, v0

    .line 1454717
    invoke-virtual {v0}, LX/2oE;->h()Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v0

    .line 1454718
    invoke-static {}, Lcom/facebook/video/engine/VideoPlayerParams;->newBuilder()LX/2oH;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/2oH;->a(Lcom/facebook/video/engine/VideoDataSource;)LX/2oH;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v1

    .line 1454719
    iput-object v1, v0, LX/2oH;->b:Ljava/lang/String;

    .line 1454720
    move-object v0, v0

    .line 1454721
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ap()Z

    move-result v1

    .line 1454722
    iput-boolean v1, v0, LX/2oH;->g:Z

    .line 1454723
    move-object v0, v0

    .line 1454724
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aB()I

    move-result v1

    .line 1454725
    iput v1, v0, LX/2oH;->r:I

    .line 1454726
    move-object v0, v0

    .line 1454727
    const/4 v1, 0x0

    .line 1454728
    iput-boolean v1, v0, LX/2oH;->o:Z

    .line 1454729
    move-object v0, v0

    .line 1454730
    invoke-virtual {v0}, LX/2oH;->n()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v0

    return-object v0

    .line 1454731
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aS()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLMedia;)LX/1bf;
    .locals 1

    .prologue
    .line 1454732
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1454733
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v0

    .line 1454734
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
