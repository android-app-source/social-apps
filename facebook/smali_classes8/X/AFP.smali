.class public LX/AFP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/audience/model/ReplyThread;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1647689
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1647690
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/AFP;->a:Ljava/util/Map;

    .line 1647691
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)Lcom/facebook/audience/model/ReplyThread;
    .locals 1

    .prologue
    .line 1647692
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/AFP;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/model/ReplyThread;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/facebook/audience/model/ReplyThread;)V
    .locals 4

    .prologue
    .line 1647693
    monitor-enter p0

    .line 1647694
    :try_start_0
    iget-object v0, p1, Lcom/facebook/audience/model/ReplyThread;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1647695
    invoke-virtual {p0, v0}, LX/AFP;->a(Ljava/lang/String;)Lcom/facebook/audience/model/ReplyThread;

    move-result-object v0

    .line 1647696
    invoke-virtual {p1, v0}, Lcom/facebook/audience/model/ReplyThread;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    .line 1647697
    :goto_0
    monitor-exit p0

    return-void

    .line 1647698
    :cond_0
    if-nez v0, :cond_1

    .line 1647699
    :try_start_1
    iget-object v0, p0, LX/AFP;->a:Ljava/util/Map;

    .line 1647700
    iget-object v1, p1, Lcom/facebook/audience/model/ReplyThread;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1647701
    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1647702
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1647703
    :cond_1
    :try_start_2
    iget-object v1, p0, LX/AFP;->a:Ljava/util/Map;

    .line 1647704
    iget-object v2, p1, Lcom/facebook/audience/model/ReplyThread;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1647705
    iget-object v3, p1, Lcom/facebook/audience/model/ReplyThread;->e:LX/0Px;

    move-object v3, v3

    .line 1647706
    invoke-static {v0, v3}, LX/7h2;->a(Lcom/facebook/audience/model/ReplyThread;LX/0Px;)Lcom/facebook/audience/model/ReplyThread;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1647707
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/AFP;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1647708
    monitor-exit p0

    return-void

    .line 1647709
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
