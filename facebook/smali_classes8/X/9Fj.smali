.class public final LX/9Fj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/9Fk;

.field public final synthetic b:Lcom/facebook/feedback/ui/rows/CommentLightweightRecAddEditClickListenerPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/rows/CommentLightweightRecAddEditClickListenerPartDefinition;LX/9Fk;)V
    .locals 0

    .prologue
    .line 1459019
    iput-object p1, p0, LX/9Fj;->b:Lcom/facebook/feedback/ui/rows/CommentLightweightRecAddEditClickListenerPartDefinition;

    iput-object p2, p0, LX/9Fj;->a:LX/9Fk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, -0x6958cf40

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1459020
    iget-object v0, p0, LX/9Fj;->a:LX/9Fk;

    iget-object v0, v0, LX/9Fk;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/1WF;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v0

    .line 1459021
    iget-object v2, p0, LX/9Fj;->a:LX/9Fk;

    iget-object v2, v2, LX/9Fk;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v2}, LX/1WF;->h(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    .line 1459022
    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, LX/9Fj;->b:Lcom/facebook/feedback/ui/rows/CommentLightweightRecAddEditClickListenerPartDefinition;

    iget-object v4, v4, Lcom/facebook/feedback/ui/rows/CommentLightweightRecAddEditClickListenerPartDefinition;->c:Landroid/content/Context;

    const-class v5, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1459023
    const-string v4, "EXTRA_COMMENT_ID"

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1459024
    const-string v0, "EXTRA_FEEDBACK"

    invoke-static {v3, v0, v2}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1459025
    iget-object v0, p0, LX/9Fj;->a:LX/9Fk;

    iget-object v0, v0, LX/9Fk;->b:Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;

    if-eqz v0, :cond_0

    .line 1459026
    const-string v0, "EXTRA_PENDING_PLACE_SLOT"

    iget-object v2, p0, LX/9Fj;->a:LX/9Fk;

    iget-object v2, v2, LX/9Fk;->b:Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;

    invoke-static {v3, v0, v2}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1459027
    :cond_0
    iget-object v0, p0, LX/9Fj;->a:LX/9Fk;

    iget-object v0, v0, LX/9Fk;->c:Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;

    if-eqz v0, :cond_1

    .line 1459028
    const-string v0, "EXTRA_EXISTING_PLACE"

    iget-object v2, p0, LX/9Fj;->a:LX/9Fk;

    iget-object v2, v2, LX/9Fk;->c:Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;

    invoke-static {v3, v0, v2}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1459029
    :cond_1
    iget-object v0, p0, LX/9Fj;->b:Lcom/facebook/feedback/ui/rows/CommentLightweightRecAddEditClickListenerPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedback/ui/rows/CommentLightweightRecAddEditClickListenerPartDefinition;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/9Fj;->b:Lcom/facebook/feedback/ui/rows/CommentLightweightRecAddEditClickListenerPartDefinition;

    iget-object v2, v2, Lcom/facebook/feedback/ui/rows/CommentLightweightRecAddEditClickListenerPartDefinition;->c:Landroid/content/Context;

    invoke-interface {v0, v3, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1459030
    const v0, 0x75f98558

    invoke-static {v6, v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
