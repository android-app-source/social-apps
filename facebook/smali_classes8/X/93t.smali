.class public final LX/93t;
.super LX/93s;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0it;",
        ":",
        "LX/0iq;",
        "DerivedData::",
        "LX/5Qu;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;>",
        "LX/93s",
        "<TModelData;TDerivedData;TServices;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/93q;LX/93w;Lcom/facebook/graphql/model/GraphQLPrivacyOption;LX/0il;LX/03V;LX/1Ck;Lcom/facebook/privacy/PrivacyOperationsClient;LX/0Ot;)V
    .locals 0
    .param p1    # LX/93q;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/93w;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/graphql/model/GraphQLPrivacyOption;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/93q;",
            "LX/93w;",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;",
            "TServices;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/1Ck;",
            "Lcom/facebook/privacy/PrivacyOperationsClient;",
            "LX/0Ot",
            "<",
            "LX/2c9;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1434309
    invoke-direct/range {p0 .. p8}, LX/93s;-><init>(LX/93q;LX/93w;Lcom/facebook/graphql/model/GraphQLPrivacyOption;LX/0il;LX/03V;LX/1Ck;Lcom/facebook/privacy/PrivacyOperationsClient;LX/0Ot;)V

    .line 1434310
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;Lcom/facebook/privacy/model/PrivacyOptionsResult;)Lcom/facebook/privacy/model/SelectablePrivacyData;
    .locals 2

    .prologue
    .line 1434299
    new-instance v0, LX/8QV;

    iget-object v1, p1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-direct {v0, v1}, LX/8QV;-><init>(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    .line 1434300
    iput-object p2, v0, LX/8QV;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    .line 1434301
    move-object v0, v0

    .line 1434302
    iget-object v1, p2, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v0, v1}, LX/8QV;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QV;

    move-result-object v0

    iget-object v1, p2, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v1}, LX/2cA;->g(LX/1oS;)Z

    move-result v1

    .line 1434303
    iput-boolean v1, v0, LX/8QV;->c:Z

    .line 1434304
    move-object v0, v0

    .line 1434305
    const/4 v1, 0x1

    .line 1434306
    iput-boolean v1, v0, LX/8QV;->d:Z

    .line 1434307
    move-object v0, v0

    .line 1434308
    invoke-virtual {v0}, LX/8QV;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v0

    return-object v0
.end method
