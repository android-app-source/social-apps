.class public final LX/93a;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/composer/privacy/common/graphql/FetchEventDetailsGraphQLModels$EventDetailsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

.field public final synthetic b:LX/93c;


# direct methods
.method public constructor <init>(LX/93c;Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V
    .locals 0

    .prologue
    .line 1433893
    iput-object p1, p0, LX/93a;->b:LX/93c;

    iput-object p2, p0, LX/93a;->a:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1433894
    new-instance v0, LX/7lP;

    iget-object v1, p0, LX/93a;->a:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    invoke-direct {v0, v1}, LX/7lP;-><init>(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    const/4 v1, 0x0

    .line 1433895
    iput-boolean v1, v0, LX/7lP;->b:Z

    .line 1433896
    move-object v0, v0

    .line 1433897
    invoke-virtual {v0}, LX/7lP;->a()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    .line 1433898
    iget-object v1, p0, LX/93a;->b:LX/93c;

    invoke-virtual {v1, v0}, LX/93Q;->a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    .line 1433899
    instance-of v0, p1, LX/2Oo;

    if-eqz v0, :cond_0

    .line 1433900
    iget-object v0, p0, LX/93a;->b:LX/93c;

    .line 1433901
    iget-object v1, v0, LX/93Q;->b:LX/03V;

    move-object v0, v1

    .line 1433902
    const-string v1, "composer_event_details_fetch_error"

    const-string v2, "Failed to fetch event details for composer"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1433903
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1433904
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1433905
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1433906
    check-cast v0, Lcom/facebook/composer/privacy/common/graphql/FetchEventDetailsGraphQLModels$EventDetailsModel;

    .line 1433907
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/composer/privacy/common/graphql/FetchEventDetailsGraphQLModels$EventDetailsModel;->a()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/composer/privacy/common/graphql/FetchEventDetailsGraphQLModels$EventDetailsModel;->a()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;->d()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1433908
    :cond_0
    :goto_0
    return-void

    .line 1433909
    :cond_1
    new-instance v1, LX/7lN;

    iget-object v2, p0, LX/93a;->a:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-object v2, v2, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->a:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    invoke-direct {v1, v2}, LX/7lN;-><init>(Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;)V

    iget-object v2, p0, LX/93a;->b:LX/93c;

    invoke-virtual {v0}, Lcom/facebook/composer/privacy/common/graphql/FetchEventDetailsGraphQLModels$EventDetailsModel;->a()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v3

    invoke-static {v2, v3}, LX/93c;->a(LX/93c;LX/2rX;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v2

    .line 1433910
    iput-object v2, v1, LX/7lN;->a:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    .line 1433911
    move-object v1, v1

    .line 1433912
    iget-object v2, p0, LX/93a;->b:LX/93c;

    invoke-virtual {v0}, Lcom/facebook/composer/privacy/common/graphql/FetchEventDetailsGraphQLModels$EventDetailsModel;->a()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v3

    invoke-static {v2, v3}, LX/93c;->b$redex0(LX/93c;LX/2rX;)Ljava/lang/String;

    move-result-object v2

    .line 1433913
    iput-object v2, v1, LX/7lN;->b:Ljava/lang/String;

    .line 1433914
    move-object v1, v1

    .line 1433915
    iget-object v2, p0, LX/93a;->b:LX/93c;

    invoke-virtual {v0}, Lcom/facebook/composer/privacy/common/graphql/FetchEventDetailsGraphQLModels$EventDetailsModel;->a()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v0

    invoke-static {v2, v0}, LX/93c;->c(LX/93c;LX/2rX;)Ljava/lang/String;

    move-result-object v0

    .line 1433916
    iput-object v0, v1, LX/7lN;->c:Ljava/lang/String;

    .line 1433917
    move-object v0, v1

    .line 1433918
    invoke-virtual {v0}, LX/7lN;->a()Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    move-result-object v0

    .line 1433919
    new-instance v1, LX/7lP;

    iget-object v2, p0, LX/93a;->a:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    invoke-direct {v1, v2}, LX/7lP;-><init>(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    const/4 v2, 0x0

    .line 1433920
    iput-boolean v2, v1, LX/7lP;->b:Z

    .line 1433921
    move-object v1, v1

    .line 1433922
    invoke-virtual {v1, v0}, LX/7lP;->a(Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;)LX/7lP;

    move-result-object v0

    invoke-virtual {v0}, LX/7lP;->a()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    .line 1433923
    iget-object v1, p0, LX/93a;->b:LX/93c;

    invoke-virtual {v1, v0}, LX/93Q;->a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    goto :goto_0
.end method
