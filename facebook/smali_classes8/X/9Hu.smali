.class public LX/9Hu;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/9IQ;

.field private final b:LX/17R;

.field public final c:LX/17W;


# direct methods
.method public constructor <init>(LX/9IQ;LX/17R;LX/17W;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1462271
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1462272
    iput-object p1, p0, LX/9Hu;->a:LX/9IQ;

    .line 1462273
    iput-object p2, p0, LX/9Hu;->b:LX/17R;

    .line 1462274
    iput-object p3, p0, LX/9Hu;->c:LX/17W;

    .line 1462275
    return-void
.end method

.method public static a(LX/0QB;)LX/9Hu;
    .locals 6

    .prologue
    .line 1462276
    const-class v1, LX/9Hu;

    monitor-enter v1

    .line 1462277
    :try_start_0
    sget-object v0, LX/9Hu;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1462278
    sput-object v2, LX/9Hu;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1462279
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1462280
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1462281
    new-instance p0, LX/9Hu;

    invoke-static {v0}, LX/9IQ;->a(LX/0QB;)LX/9IQ;

    move-result-object v3

    check-cast v3, LX/9IQ;

    invoke-static {v0}, LX/17R;->a(LX/0QB;)LX/17R;

    move-result-object v4

    check-cast v4, LX/17R;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v5

    check-cast v5, LX/17W;

    invoke-direct {p0, v3, v4, v5}, LX/9Hu;-><init>(LX/9IQ;LX/17R;LX/17W;)V

    .line 1462282
    move-object v0, p0

    .line 1462283
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1462284
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/9Hu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1462285
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1462286
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
