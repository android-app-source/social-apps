.class public LX/APd;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0jI;",
        ":",
        "LX/0io;",
        ":",
        "LX/0iw;",
        ":",
        "LX/0iy;",
        ":",
        "LX/0j0;",
        ":",
        "LX/0j1;",
        ":",
        "LX/0j2;",
        ":",
        "LX/0j3;",
        ":",
        "LX/0j8;",
        ":",
        "LX/0iq;",
        ":",
        "LX/0j5;",
        ":",
        "LX/0jB;",
        ":",
        "LX/0jD;",
        ":",
        "LX/0j6;",
        ":",
        "LX/0ip;",
        ":",
        "LX/0jE;",
        "DerivedData::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesCanSubmit;",
        ":",
        "LX/5Qv;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;>",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/1RW;

.field public final c:LX/0ad;

.field public final d:LX/Hqa;

.field public final e:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field public final f:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1RW;LX/0ad;LX/Hqa;LX/0il;Landroid/view/ViewStub;)V
    .locals 1
    .param p4    # LX/Hqa;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Landroid/view/ViewStub;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/1RW;",
            "LX/0ad;",
            "Lcom/facebook/composer/compost/ComposerMoreOptionMenuController$MoreOptionMenuCallback;",
            "TServices;",
            "Landroid/view/ViewStub;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1670273
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1670274
    iput-object p1, p0, LX/APd;->a:Landroid/content/Context;

    .line 1670275
    iput-object p2, p0, LX/APd;->b:LX/1RW;

    .line 1670276
    iput-object p3, p0, LX/APd;->c:LX/0ad;

    .line 1670277
    iput-object p4, p0, LX/APd;->d:LX/Hqa;

    .line 1670278
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p5}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/APd;->e:Ljava/lang/ref/WeakReference;

    .line 1670279
    invoke-virtual {p6}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/APd;->f:Landroid/view/View;

    .line 1670280
    iget-object v0, p0, LX/APd;->f:Landroid/view/View;

    new-instance p1, LX/APa;

    invoke-direct {p1, p0}, LX/APa;-><init>(LX/APd;)V

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1670281
    return-void
.end method
