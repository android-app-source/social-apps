.class public final LX/92k;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/92n;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/92o;


# direct methods
.method public constructor <init>(LX/92o;)V
    .locals 0

    .prologue
    .line 1432711
    iput-object p1, p0, LX/92k;->a:LX/92o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 15

    .prologue
    .line 1432589
    const/4 v1, 0x0

    .line 1432590
    iget-object v0, p0, LX/92k;->a:LX/92o;

    iget-object v0, v0, LX/92o;->r:LX/92n;

    if-eqz v0, :cond_0

    .line 1432591
    iget-object v0, p0, LX/92k;->a:LX/92o;

    iget-object v0, v0, LX/92o;->r:LX/92n;

    .line 1432592
    :goto_0
    return-object v0

    .line 1432593
    :cond_0
    iget-object v0, p0, LX/92k;->a:LX/92o;

    .line 1432594
    iput v1, v0, LX/92o;->q:I

    .line 1432595
    iget-object v0, p0, LX/92k;->a:LX/92o;

    invoke-static {v0, v1}, LX/92o;->b(LX/92o;Z)LX/0w7;

    move-result-object v0

    .line 1432596
    iget-object v1, p0, LX/92k;->a:LX/92o;

    iget-object v1, v1, LX/92o;->i:LX/92u;

    invoke-virtual {v1, v0}, LX/92u;->a(LX/0w7;)Ljava/lang/String;

    move-result-object v1

    .line 1432597
    iget-object v2, p0, LX/92k;->a:LX/92o;

    iget-object v2, v2, LX/92o;->f:LX/92w;

    const/4 v6, 0x0

    .line 1432598
    iget-object v4, v2, LX/92w;->a:LX/92v;

    invoke-virtual {v4}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const-string v5, "minutiae_verb_table"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, LX/92x;->m:LX/0U1;

    .line 1432599
    iget-object v9, v8, LX/0U1;->d:Ljava/lang/String;

    move-object v8, v9

    .line 1432600
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " = ? AND "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, LX/92x;->n:LX/0U1;

    .line 1432601
    iget-object v9, v8, LX/0U1;->d:Ljava/lang/String;

    move-object v8, v9

    .line 1432602
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " = ?"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    aput-object v1, v8, v9

    const/4 v10, 0x1

    iget-object v9, v2, LX/92w;->e:LX/0Or;

    invoke-interface {v9}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/facebook/user/model/User;

    .line 1432603
    iget-object v11, v9, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v9, v11

    .line 1432604
    aput-object v9, v8, v10

    move-object v9, v6

    move-object v10, v6

    move-object v11, v6

    move-object v12, v6

    invoke-virtual/range {v4 .. v12}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 1432605
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 1432606
    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-nez v6, :cond_2

    .line 1432607
    const-string v5, "minutiae_disk_storage_get_activities_not_found"

    invoke-static {v2, v5, v1}, LX/92w;->a(LX/92w;Ljava/lang/String;Ljava/lang/String;)V

    .line 1432608
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 1432609
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v4

    .line 1432610
    :goto_1
    move-object v2, v4

    .line 1432611
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1432612
    new-instance v1, LX/92n;

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    sget-object v2, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    invoke-direct {v1, v0, v2}, LX/92n;-><init>(LX/0Px;LX/0ta;)V

    move-object v0, v1

    goto/16 :goto_0

    .line 1432613
    :cond_1
    iget-object v2, p0, LX/92k;->a:LX/92o;

    iget-object v2, v2, LX/92o;->d:LX/92N;

    invoke-virtual {v2, v0}, LX/92N;->a(LX/0w7;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    const v2, 0x6b5a16b9

    invoke-static {v0, v2}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/92n;

    .line 1432614
    iget-object v2, p0, LX/92k;->a:LX/92o;

    iget-object v2, v2, LX/92o;->f:LX/92w;

    iget-object v3, v0, LX/92n;->a:LX/0Px;

    invoke-virtual {v2, v3, v1}, LX/92w;->a(LX/0Px;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1432615
    :cond_2
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1432616
    iget-object v6, v2, LX/92w;->g:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    sget-object v8, LX/92x;->o:LX/0U1;

    .line 1432617
    iget-object v13, v8, LX/0U1;->d:Ljava/lang/String;

    move-object v13, v13

    .line 1432618
    invoke-interface {v4, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    invoke-interface {v4, v13}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v13

    move-wide v8, v13

    .line 1432619
    sub-long/2addr v6, v8

    const-wide v8, 0x757b12c00L

    cmp-long v6, v6, v8

    if-lez v6, :cond_3

    .line 1432620
    invoke-static {v2}, LX/92w;->a(LX/92w;)V

    .line 1432621
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v4

    goto :goto_1

    .line 1432622
    :cond_3
    :goto_2
    invoke-interface {v4}, Landroid/database/Cursor;->isClosed()Z

    move-result v6

    if-nez v6, :cond_5

    .line 1432623
    new-instance v6, LX/930;

    invoke-direct {v6}, LX/930;-><init>()V

    sget-object v7, LX/92x;->a:LX/0U1;

    invoke-static {v4, v7}, LX/92w;->a(Landroid/database/Cursor;LX/0U1;)Ljava/lang/String;

    move-result-object v7

    .line 1432624
    iput-object v7, v6, LX/930;->c:Ljava/lang/String;

    .line 1432625
    move-object v6, v6

    .line 1432626
    sget-object v7, LX/92x;->b:LX/0U1;

    invoke-static {v4, v7}, LX/92w;->a(Landroid/database/Cursor;LX/0U1;)Ljava/lang/String;

    move-result-object v7

    .line 1432627
    iput-object v7, v6, LX/930;->d:Ljava/lang/String;

    .line 1432628
    move-object v6, v6

    .line 1432629
    sget-object v7, LX/92x;->c:LX/0U1;

    invoke-static {v4, v7}, LX/92w;->a(Landroid/database/Cursor;LX/0U1;)Ljava/lang/String;

    move-result-object v7

    .line 1432630
    iput-object v7, v6, LX/930;->e:Ljava/lang/String;

    .line 1432631
    move-object v6, v6

    .line 1432632
    sget-object v7, LX/92x;->d:LX/0U1;

    invoke-static {v4, v7}, LX/92w;->a(Landroid/database/Cursor;LX/0U1;)Ljava/lang/String;

    move-result-object v7

    .line 1432633
    iput-object v7, v6, LX/930;->f:Ljava/lang/String;

    .line 1432634
    move-object v6, v6

    .line 1432635
    sget-object v7, LX/92x;->e:LX/0U1;

    invoke-static {v4, v7}, LX/92w;->d(Landroid/database/Cursor;LX/0U1;)Z

    move-result v7

    .line 1432636
    iput-boolean v7, v6, LX/930;->g:Z

    .line 1432637
    move-object v6, v6

    .line 1432638
    sget-object v7, LX/92x;->f:LX/0U1;

    invoke-static {v4, v7}, LX/92w;->d(Landroid/database/Cursor;LX/0U1;)Z

    move-result v7

    .line 1432639
    iput-boolean v7, v6, LX/930;->h:Z

    .line 1432640
    move-object v6, v6

    .line 1432641
    sget-object v7, LX/92x;->g:LX/0U1;

    invoke-static {v4, v7}, LX/92w;->d(Landroid/database/Cursor;LX/0U1;)Z

    move-result v7

    .line 1432642
    iput-boolean v7, v6, LX/930;->j:Z

    .line 1432643
    move-object v6, v6

    .line 1432644
    sget-object v7, LX/92x;->h:LX/0U1;

    invoke-static {v4, v7}, LX/92w;->d(Landroid/database/Cursor;LX/0U1;)Z

    move-result v7

    .line 1432645
    iput-boolean v7, v6, LX/930;->i:Z

    .line 1432646
    move-object v6, v6

    .line 1432647
    sget-object v7, LX/92x;->i:LX/0U1;

    invoke-static {v4, v7}, LX/92w;->a(Landroid/database/Cursor;LX/0U1;)Ljava/lang/String;

    move-result-object v7

    .line 1432648
    iput-object v7, v6, LX/930;->k:Ljava/lang/String;

    .line 1432649
    move-object v6, v6

    .line 1432650
    sget-object v7, LX/92x;->j:LX/0U1;

    invoke-static {v4, v7}, LX/92w;->a(Landroid/database/Cursor;LX/0U1;)Ljava/lang/String;

    move-result-object v7

    .line 1432651
    iput-object v7, v6, LX/930;->l:Ljava/lang/String;

    .line 1432652
    move-object v6, v6

    .line 1432653
    sget-object v7, LX/92x;->k:LX/0U1;

    invoke-static {v4, v7}, LX/92w;->b(Landroid/database/Cursor;LX/0U1;)I

    move-result v7

    .line 1432654
    iput v7, v6, LX/930;->b:I

    .line 1432655
    move-object v6, v6

    .line 1432656
    sget-object v7, LX/92x;->l:LX/0U1;

    invoke-static {v4, v7}, LX/92w;->b(Landroid/database/Cursor;LX/0U1;)I

    move-result v7

    .line 1432657
    iput v7, v6, LX/930;->a:I

    .line 1432658
    move-object v6, v6

    .line 1432659
    invoke-virtual {v6}, LX/930;->a()LX/931;

    move-result-object v6

    .line 1432660
    new-instance v7, LX/92e;

    .line 1432661
    new-instance v8, LX/5Lb;

    invoke-direct {v8}, LX/5Lb;-><init>()V

    iget-object v9, v6, LX/931;->a:Ljava/lang/String;

    .line 1432662
    iput-object v9, v8, LX/5Lb;->d:Ljava/lang/String;

    .line 1432663
    move-object v8, v8

    .line 1432664
    iget-object v9, v6, LX/931;->b:Ljava/lang/String;

    .line 1432665
    iput-object v9, v8, LX/5Lb;->f:Ljava/lang/String;

    .line 1432666
    move-object v8, v8

    .line 1432667
    iget-object v9, v6, LX/931;->c:Ljava/lang/String;

    .line 1432668
    iput-object v9, v8, LX/5Lb;->h:Ljava/lang/String;

    .line 1432669
    move-object v8, v8

    .line 1432670
    iget-object v9, v6, LX/931;->d:Ljava/lang/String;

    .line 1432671
    iput-object v9, v8, LX/5Lb;->o:Ljava/lang/String;

    .line 1432672
    move-object v8, v8

    .line 1432673
    iget-boolean v9, v6, LX/931;->e:Z

    .line 1432674
    iput-boolean v9, v8, LX/5Lb;->e:Z

    .line 1432675
    move-object v8, v8

    .line 1432676
    iget-boolean v9, v6, LX/931;->f:Z

    .line 1432677
    iput-boolean v9, v8, LX/5Lb;->p:Z

    .line 1432678
    move-object v8, v8

    .line 1432679
    iget-boolean v9, v6, LX/931;->g:Z

    .line 1432680
    iput-boolean v9, v8, LX/5Lb;->r:Z

    .line 1432681
    move-object v8, v8

    .line 1432682
    iget-boolean v9, v6, LX/931;->h:Z

    .line 1432683
    iput-boolean v9, v8, LX/5Lb;->q:Z

    .line 1432684
    move-object v8, v8

    .line 1432685
    new-instance v9, LX/5LY;

    invoke-direct {v9}, LX/5LY;-><init>()V

    iget-object v10, v6, LX/931;->i:Ljava/lang/String;

    .line 1432686
    iput-object v10, v9, LX/5LY;->a:Ljava/lang/String;

    .line 1432687
    move-object v9, v9

    .line 1432688
    invoke-virtual {v9}, LX/5LY;->a()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;

    move-result-object v9

    .line 1432689
    iput-object v9, v8, LX/5Lb;->b:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;

    .line 1432690
    move-object v8, v8

    .line 1432691
    new-instance v9, LX/5LZ;

    invoke-direct {v9}, LX/5LZ;-><init>()V

    iget-object v10, v6, LX/931;->j:Ljava/lang/String;

    .line 1432692
    iput-object v10, v9, LX/5LZ;->a:Ljava/lang/String;

    .line 1432693
    move-object v9, v9

    .line 1432694
    invoke-virtual {v9}, LX/5LZ;->a()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;

    move-result-object v9

    .line 1432695
    iput-object v9, v8, LX/5Lb;->c:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;

    .line 1432696
    move-object v8, v8

    .line 1432697
    new-instance v9, LX/5La;

    invoke-direct {v9}, LX/5La;-><init>()V

    iget v10, v6, LX/931;->l:I

    .line 1432698
    iput v10, v9, LX/5La;->a:I

    .line 1432699
    move-object v9, v9

    .line 1432700
    invoke-virtual {v9}, LX/5La;->a()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel$AllIconsModel;

    move-result-object v9

    .line 1432701
    iput-object v9, v8, LX/5Lb;->a:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel$AllIconsModel;

    .line 1432702
    move-object v8, v8

    .line 1432703
    invoke-virtual {v8}, LX/5Lb;->a()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    move-result-object v8

    move-object v8, v8

    .line 1432704
    invoke-direct {v7, v8}, LX/92e;-><init>(LX/5LG;)V

    move-object v6, v7

    .line 1432705
    invoke-virtual {v5, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1432706
    invoke-interface {v4}, Landroid/database/Cursor;->isLast()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1432707
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    goto/16 :goto_2

    .line 1432708
    :cond_4
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    goto/16 :goto_2

    .line 1432709
    :cond_5
    const-string v4, "minutiae_disk_storage_get_activities_found"

    invoke-static {v2, v4, v1}, LX/92w;->a(LX/92w;Ljava/lang/String;Ljava/lang/String;)V

    .line 1432710
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    invoke-static {v4}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v4

    goto/16 :goto_1
.end method
