.class public final LX/8na;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8JX;

.field public final synthetic b:Ljava/lang/CharSequence;

.field public final synthetic c:LX/8nc;


# direct methods
.method public constructor <init>(LX/8nc;LX/8JX;Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 1401203
    iput-object p1, p0, LX/8na;->c:LX/8nc;

    iput-object p2, p0, LX/8na;->a:LX/8JX;

    iput-object p3, p0, LX/8na;->b:Ljava/lang/CharSequence;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1401204
    iget-object v0, p0, LX/8na;->a:LX/8JX;

    iget-object v1, p0, LX/8na;->b:Ljava/lang/CharSequence;

    .line 1401205
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 1401206
    invoke-interface {v0, v1, v2}, LX/8JX;->a(Ljava/lang/CharSequence;Ljava/util/List;)V

    .line 1401207
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 9
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1401208
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1401209
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 1401210
    if-eqz p1, :cond_0

    .line 1401211
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1401212
    if-nez v0, :cond_1

    .line 1401213
    :cond_0
    iget-object v0, p0, LX/8na;->a:LX/8JX;

    iget-object v1, p0, LX/8na;->b:Ljava/lang/CharSequence;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/8JX;->a(Ljava/lang/CharSequence;Ljava/util/List;)V

    .line 1401214
    :goto_0
    return-void

    .line 1401215
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1401216
    check-cast v0, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel;->a()Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_3

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel;

    .line 1401217
    new-instance v5, LX/7Gq;

    invoke-direct {v5}, LX/7Gq;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 1401218
    iput-wide v6, v5, LX/7Gq;->b:J

    .line 1401219
    move-object v5, v5

    .line 1401220
    new-instance v6, Lcom/facebook/user/model/Name;

    invoke-virtual {v0}, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel;->l()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;)V

    .line 1401221
    iput-object v6, v5, LX/7Gq;->a:Lcom/facebook/user/model/Name;

    .line 1401222
    move-object v5, v5

    .line 1401223
    sget-object v6, LX/7Gr;->PRODUCT:LX/7Gr;

    .line 1401224
    iput-object v6, v5, LX/7Gq;->e:LX/7Gr;

    .line 1401225
    move-object v5, v5

    .line 1401226
    sget-object v6, LX/8nE;->PRODUCTS:LX/8nE;

    invoke-virtual {v6}, LX/8nE;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1401227
    iput-object v6, v5, LX/7Gq;->i:Ljava/lang/String;

    .line 1401228
    move-object v5, v5

    .line 1401229
    invoke-virtual {v0}, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel;->k()Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ImageModel;

    move-result-object v6

    if-nez v6, :cond_2

    const/4 v0, 0x0

    .line 1401230
    :goto_2
    iput-object v0, v5, LX/7Gq;->c:Ljava/lang/String;

    .line 1401231
    move-object v0, v5

    .line 1401232
    invoke-virtual {v0}, LX/7Gq;->l()Lcom/facebook/tagging/model/TaggingProfile;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1401233
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1401234
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel;->k()Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/tagging/graphql/protocol/FetchProductTagsQueryModels$FetchProductTagsQueryModel$TaggableProductsModel$NodesModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 1401235
    :cond_3
    iget-object v0, p0, LX/8na;->a:LX/8JX;

    iget-object v1, p0, LX/8na;->b:Ljava/lang/CharSequence;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/8JX;->a(Ljava/lang/CharSequence;Ljava/util/List;)V

    goto :goto_0
.end method
