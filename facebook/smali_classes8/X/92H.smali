.class public LX/92H;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:I

.field public final e:Z

.field public final f:I

.field public final g:Ljava/lang/String;

.field public final h:Z

.field public final i:Ljava/lang/String;

.field public final j:Ljava/lang/String;

.field public final k:Z

.field public final l:Landroid/location/Location;


# direct methods
.method public constructor <init>(LX/92G;)V
    .locals 1

    .prologue
    .line 1432162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1432163
    iget-object v0, p1, LX/92G;->g:Ljava/lang/String;

    iput-object v0, p0, LX/92H;->a:Ljava/lang/String;

    .line 1432164
    iget-object v0, p1, LX/92G;->h:Ljava/lang/String;

    iput-object v0, p0, LX/92H;->b:Ljava/lang/String;

    .line 1432165
    iget-object v0, p1, LX/92G;->i:Ljava/lang/String;

    iput-object v0, p0, LX/92H;->c:Ljava/lang/String;

    .line 1432166
    iget v0, p1, LX/92G;->j:I

    iput v0, p0, LX/92H;->d:I

    .line 1432167
    iget-boolean v0, p1, LX/92G;->k:Z

    iput-boolean v0, p0, LX/92H;->e:Z

    .line 1432168
    iget-boolean v0, p1, LX/92G;->l:Z

    iput-boolean v0, p0, LX/92H;->h:Z

    .line 1432169
    iget v0, p1, LX/92G;->a:I

    iput v0, p0, LX/92H;->f:I

    .line 1432170
    iget-object v0, p1, LX/92G;->b:Ljava/lang/String;

    iput-object v0, p0, LX/92H;->g:Ljava/lang/String;

    .line 1432171
    iget-object v0, p1, LX/92G;->c:Ljava/lang/String;

    iput-object v0, p0, LX/92H;->i:Ljava/lang/String;

    .line 1432172
    iget-object v0, p1, LX/92G;->d:Ljava/lang/String;

    iput-object v0, p0, LX/92H;->j:Ljava/lang/String;

    .line 1432173
    iget-boolean v0, p1, LX/92G;->e:Z

    iput-boolean v0, p0, LX/92H;->k:Z

    .line 1432174
    iget-object v0, p1, LX/92G;->f:Landroid/location/Location;

    iput-object v0, p0, LX/92H;->l:Landroid/location/Location;

    .line 1432175
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1432176
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "taggableActivityId"

    iget-object v2, p0, LX/92H;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "query"

    iget-object v2, p0, LX/92H;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "cursor"

    iget-object v2, p0, LX/92H;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "numItems"

    iget v2, p0, LX/92H;->d:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "allowOfflinePosting"

    iget-boolean v2, p0, LX/92H;->e:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "sessionId"

    iget v2, p0, LX/92H;->f:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "requestId"

    iget-object v2, p0, LX/92H;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "placeId"

    iget-object v2, p0, LX/92H;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "surface"

    iget-object v2, p0, LX/92H;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "isPrefetch"

    iget-boolean v2, p0, LX/92H;->k:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "location"

    iget-object v2, p0, LX/92H;->l:Landroid/location/Location;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
