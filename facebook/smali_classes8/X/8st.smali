.class public LX/8st;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8sj;


# instance fields
.field private final a:Landroid/view/View;

.field private final b:J

.field private final c:F

.field private final d:F

.field private final e:Landroid/animation/Animator$AnimatorListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;JFFLandroid/animation/Animator$AnimatorListener;)V
    .locals 0

    .prologue
    .line 1411888
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1411889
    iput-object p1, p0, LX/8st;->a:Landroid/view/View;

    .line 1411890
    iput-wide p2, p0, LX/8st;->b:J

    .line 1411891
    iput p4, p0, LX/8st;->c:F

    .line 1411892
    iput p5, p0, LX/8st;->d:F

    .line 1411893
    iput-object p6, p0, LX/8st;->e:Landroid/animation/Animator$AnimatorListener;

    .line 1411894
    return-void
.end method

.method private b(F)F
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1411895
    iget v0, p0, LX/8st;->c:F

    iget v1, p0, LX/8st;->d:F

    iget v2, p0, LX/8st;->c:F

    sub-float/2addr v1, v2

    mul-float/2addr v1, p1

    add-float/2addr v0, v1

    return v0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 1411896
    iget-wide v0, p0, LX/8st;->b:J

    return-wide v0
.end method

.method public final a(F)Landroid/animation/Animator;
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1411897
    invoke-direct {p0, p1}, LX/8st;->b(F)F

    move-result v0

    .line 1411898
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 1411899
    iget-object v2, p0, LX/8st;->a:Landroid/view/View;

    const-string v3, "scaleX"

    new-array v4, v8, [F

    aput v0, v4, v6

    iget v5, p0, LX/8st;->d:F

    aput v5, v4, v7

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v2

    iget-object v3, p0, LX/8st;->a:Landroid/view/View;

    const-string v4, "scaleY"

    new-array v5, v8, [F

    aput v0, v5, v6

    iget v0, p0, LX/8st;->d:F

    aput v0, v5, v7

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 1411900
    iget-object v0, p0, LX/8st;->e:Landroid/animation/Animator$AnimatorListener;

    if-eqz v0, :cond_0

    .line 1411901
    iget-object v0, p0, LX/8st;->e:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1411902
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr v0, p1

    iget-wide v2, p0, LX/8st;->b:J

    long-to-float v2, v2

    mul-float/2addr v0, v2

    float-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 1411903
    return-object v1
.end method
