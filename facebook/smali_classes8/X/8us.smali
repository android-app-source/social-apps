.class public final enum LX/8us;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8us;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8us;

.field public static final enum ALWAYS:LX/8us;

.field public static final enum NEVER:LX/8us;

.field public static final enum WHILE_EDITING:LX/8us;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1415811
    new-instance v0, LX/8us;

    const-string v1, "NEVER"

    invoke-direct {v0, v1, v2}, LX/8us;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8us;->NEVER:LX/8us;

    .line 1415812
    new-instance v0, LX/8us;

    const-string v1, "WHILE_EDITING"

    invoke-direct {v0, v1, v3}, LX/8us;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8us;->WHILE_EDITING:LX/8us;

    .line 1415813
    new-instance v0, LX/8us;

    const-string v1, "ALWAYS"

    invoke-direct {v0, v1, v4}, LX/8us;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8us;->ALWAYS:LX/8us;

    .line 1415814
    const/4 v0, 0x3

    new-array v0, v0, [LX/8us;

    sget-object v1, LX/8us;->NEVER:LX/8us;

    aput-object v1, v0, v2

    sget-object v1, LX/8us;->WHILE_EDITING:LX/8us;

    aput-object v1, v0, v3

    sget-object v1, LX/8us;->ALWAYS:LX/8us;

    aput-object v1, v0, v4

    sput-object v0, LX/8us;->$VALUES:[LX/8us;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1415815
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8us;
    .locals 1

    .prologue
    .line 1415816
    const-class v0, LX/8us;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8us;

    return-object v0
.end method

.method public static values()[LX/8us;
    .locals 1

    .prologue
    .line 1415817
    sget-object v0, LX/8us;->$VALUES:[LX/8us;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8us;

    return-object v0
.end method
