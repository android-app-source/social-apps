.class public final enum LX/90w;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/90w;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/90w;

.field public static final enum ACTIVITY:LX/90w;

.field public static final enum LISTENING:LX/90w;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1429795
    new-instance v0, LX/90w;

    const-string v1, "LISTENING"

    invoke-direct {v0, v1, v2}, LX/90w;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/90w;->LISTENING:LX/90w;

    .line 1429796
    new-instance v0, LX/90w;

    const-string v1, "ACTIVITY"

    invoke-direct {v0, v1, v3}, LX/90w;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/90w;->ACTIVITY:LX/90w;

    .line 1429797
    const/4 v0, 0x2

    new-array v0, v0, [LX/90w;

    sget-object v1, LX/90w;->LISTENING:LX/90w;

    aput-object v1, v0, v2

    sget-object v1, LX/90w;->ACTIVITY:LX/90w;

    aput-object v1, v0, v3

    sput-object v0, LX/90w;->$VALUES:[LX/90w;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1429792
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/90w;
    .locals 1

    .prologue
    .line 1429794
    const-class v0, LX/90w;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/90w;

    return-object v0
.end method

.method public static values()[LX/90w;
    .locals 1

    .prologue
    .line 1429793
    sget-object v0, LX/90w;->$VALUES:[LX/90w;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/90w;

    return-object v0
.end method
