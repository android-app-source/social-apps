.class public LX/AMT;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/AMS;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Z

.field public final f:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;LX/AMS;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 1666228
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1666229
    iput-object p1, p0, LX/AMT;->a:Ljava/lang/String;

    .line 1666230
    iput-object p2, p0, LX/AMT;->b:LX/AMS;

    .line 1666231
    iput-object p3, p0, LX/AMT;->c:Ljava/lang/String;

    .line 1666232
    iput-object p4, p0, LX/AMT;->d:Ljava/lang/String;

    .line 1666233
    iput-boolean p5, p0, LX/AMT;->e:Z

    .line 1666234
    iput-object p6, p0, LX/AMT;->f:Ljava/lang/String;

    .line 1666235
    return-void
.end method

.method public static a(Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$BestMaskPackageFragmentModel;)LX/AMT;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1666236
    if-nez p0, :cond_1

    .line 1666237
    :cond_0
    :goto_0
    return-object v0

    .line 1666238
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$BestMaskPackageFragmentModel;->m()Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$BestMaskPackageFragmentModel$PackagedFileModel;

    move-result-object v1

    .line 1666239
    if-eqz v1, :cond_0

    .line 1666240
    invoke-virtual {p0}, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$BestMaskPackageFragmentModel;->l()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$BestMaskPackageFragmentModel$PackagedFileModel;->j()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$BestMaskPackageFragmentModel$PackagedFileModel;->k()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$BestMaskPackageFragmentModel;->k()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1666241
    invoke-virtual {p0}, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$BestMaskPackageFragmentModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$BestMaskPackageFragmentModel$PackagedFileModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$BestMaskPackageFragmentModel$PackagedFileModel;->k()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$BestMaskPackageFragmentModel;->k()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v2, v1, v3, v4}, LX/AMT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)LX/AMT;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)LX/AMT;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1666242
    new-instance v0, LX/AMT;

    sget-object v2, LX/AMS;->SUPPORT:LX/AMS;

    const/4 v5, 0x0

    move-object v3, p0

    move-object v4, p1

    move-object v6, v1

    invoke-direct/range {v0 .. v6}, LX/AMT;-><init>(Ljava/lang/String;LX/AMS;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)LX/AMT;
    .locals 7

    .prologue
    .line 1666243
    new-instance v0, LX/AMT;

    sget-object v2, LX/AMS;->EFFECT:LX/AMS;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, LX/AMT;-><init>(Ljava/lang/String;LX/AMS;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    return-object v0
.end method

.method public static a(Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$BestMaskPackageFragmentModel;Ljava/util/Set;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$BestMaskPackageFragmentModel;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "LX/AMT;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1666244
    if-nez p0, :cond_0

    .line 1666245
    const/4 v0, 0x0

    .line 1666246
    :goto_0
    return-object v0

    .line 1666247
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1666248
    invoke-virtual {p0}, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$BestMaskPackageFragmentModel;->j()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    .line 1666249
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_2

    .line 1666250
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$BestMaskPackageFragmentModel$FaceRecognitionModelModel;

    .line 1666251
    invoke-virtual {v0}, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$BestMaskPackageFragmentModel$FaceRecognitionModelModel;->b()Ljava/lang/String;

    move-result-object v5

    .line 1666252
    invoke-virtual {v0}, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$BestMaskPackageFragmentModel$FaceRecognitionModelModel;->c()Ljava/lang/String;

    move-result-object v0

    .line 1666253
    if-eqz v5, :cond_1

    if-eqz v0, :cond_1

    invoke-interface {p1, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 1666254
    invoke-interface {p1, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1666255
    invoke-static {v5, v0}, LX/AMT;->a(Ljava/lang/String;Ljava/lang/String;)LX/AMT;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1666256
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1666257
    :cond_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method
