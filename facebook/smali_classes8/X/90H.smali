.class public final LX/90H;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8zz;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;)V
    .locals 0

    .prologue
    .line 1428585
    iput-object p1, p0, LX/90H;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1428586
    iget-object v0, p0, LX/90H;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->r:LX/90G;

    invoke-virtual {v0}, LX/90G;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1428587
    iget-object v0, p0, LX/90H;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->H:LX/918;

    iget-object v1, p0, LX/90H;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;

    iget-object v1, v1, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->p:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1428588
    iget-object v2, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v1, v2

    .line 1428589
    iget-object v2, p0, LX/90H;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;

    invoke-static {v2}, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->q(Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;)Ljava/lang/String;

    move-result-object v2

    .line 1428590
    const-string v3, "activities_selector_first_keystroke"

    invoke-static {v3, v1}, LX/918;->d(Ljava/lang/String;Ljava/lang/String;)LX/8yQ;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/8yQ;->a(Ljava/lang/String;)LX/8yQ;

    move-result-object v3

    .line 1428591
    iget-object v4, v3, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v3, v4

    .line 1428592
    iget-object v4, v0, LX/918;->a:LX/0Zb;

    invoke-interface {v4, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1428593
    :cond_0
    iget-object v0, p0, LX/90H;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->r:LX/90G;

    invoke-virtual {v0}, LX/90G;->a()V

    .line 1428594
    iget-object v0, p0, LX/90H;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->v:LX/8zh;

    invoke-virtual {v0, p1}, LX/8zh;->a(Ljava/lang/String;)V

    .line 1428595
    return-void
.end method
