.class public final LX/9JN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field public final a:LX/9JM;

.field public b:Ljava/nio/ByteBuffer;

.field public c:I

.field public d:Z

.field public e:Z


# direct methods
.method public constructor <init>(LX/9JM;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1464629
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1464630
    const/4 v0, 0x0

    iput-object v0, p0, LX/9JN;->b:Ljava/nio/ByteBuffer;

    .line 1464631
    iput v1, p0, LX/9JN;->c:I

    .line 1464632
    iput-boolean v1, p0, LX/9JN;->d:Z

    .line 1464633
    iput-boolean v1, p0, LX/9JN;->e:Z

    .line 1464634
    iput-object p1, p0, LX/9JN;->a:LX/9JM;

    .line 1464635
    return-void
.end method


# virtual methods
.method public final c()Z
    .locals 8

    .prologue
    const/4 v4, 0x3

    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1464638
    iget-boolean v2, p0, LX/9JN;->d:Z

    if-eqz v2, :cond_1

    .line 1464639
    :cond_0
    :goto_0
    return v0

    .line 1464640
    :cond_1
    iget-boolean v2, p0, LX/9JN;->e:Z

    if-nez v2, :cond_3

    .line 1464641
    const/4 v6, 0x5

    const/4 v7, 0x1

    .line 1464642
    iput-boolean v7, p0, LX/9JN;->e:Z

    .line 1464643
    invoke-static {v6}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 1464644
    sget-object v3, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 1464645
    iget-object v3, p0, LX/9JN;->a:LX/9JM;

    invoke-virtual {v3, v2}, LX/9JM;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    .line 1464646
    if-ge v3, v6, :cond_8

    .line 1464647
    iput-boolean v7, p0, LX/9JN;->d:Z

    .line 1464648
    :cond_2
    :goto_1
    iget-boolean v2, p0, LX/9JN;->d:Z

    if-nez v2, :cond_0

    .line 1464649
    :cond_3
    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 1464650
    sget-object v3, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 1464651
    iget-object v3, p0, LX/9JN;->a:LX/9JM;

    invoke-virtual {v3, v2}, LX/9JM;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    .line 1464652
    if-eq v3, v4, :cond_4

    .line 1464653
    iput-boolean v1, p0, LX/9JN;->d:Z

    .line 1464654
    iput-object v5, p0, LX/9JN;->b:Ljava/nio/ByteBuffer;

    goto :goto_0

    .line 1464655
    :cond_4
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 1464656
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->get()B

    move-result v3

    .line 1464657
    if-eq v3, v1, :cond_5

    .line 1464658
    iput-boolean v1, p0, LX/9JN;->d:Z

    .line 1464659
    iput-object v5, p0, LX/9JN;->b:Ljava/nio/ByteBuffer;

    goto :goto_0

    .line 1464660
    :cond_5
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v2

    .line 1464661
    if-nez v2, :cond_6

    .line 1464662
    iput-boolean v1, p0, LX/9JN;->d:Z

    .line 1464663
    iput-object v5, p0, LX/9JN;->b:Ljava/nio/ByteBuffer;

    goto :goto_0

    .line 1464664
    :cond_6
    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    iput-object v3, p0, LX/9JN;->b:Ljava/nio/ByteBuffer;

    .line 1464665
    iget-object v3, p0, LX/9JN;->a:LX/9JM;

    iget-object v4, p0, LX/9JN;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v4}, LX/9JM;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    .line 1464666
    if-eq v3, v2, :cond_7

    .line 1464667
    iput-boolean v1, p0, LX/9JN;->d:Z

    .line 1464668
    iput-object v5, p0, LX/9JN;->b:Ljava/nio/ByteBuffer;

    goto :goto_0

    .line 1464669
    :cond_7
    iget-object v0, p0, LX/9JN;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 1464670
    iget-object v0, p0, LX/9JN;->a:LX/9JM;

    .line 1464671
    iget-object v6, v0, LX/9JM;->b:Ljava/nio/channels/FileChannel;

    invoke-virtual {v6}, Ljava/nio/channels/FileChannel;->position()J

    move-result-wide v6

    long-to-int v6, v6

    move v0, v6

    .line 1464672
    iput v0, p0, LX/9JN;->c:I

    move v0, v1

    .line 1464673
    goto :goto_0

    .line 1464674
    :cond_8
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 1464675
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    const v6, 0x20161109

    if-eq v3, v6, :cond_9

    .line 1464676
    iput-boolean v7, p0, LX/9JN;->d:Z

    goto :goto_1

    .line 1464677
    :cond_9
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->get()B

    move-result v2

    if-eq v2, v7, :cond_2

    .line 1464678
    iput-boolean v7, p0, LX/9JN;->d:Z

    goto :goto_1
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 1464636
    iget-object v0, p0, LX/9JN;->a:LX/9JM;

    invoke-virtual {v0}, LX/9JM;->close()V

    .line 1464637
    return-void
.end method
