.class public LX/8y0;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/1Ck;


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1425024
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1425025
    iput-object p1, p0, LX/8y0;->a:LX/0tX;

    .line 1425026
    iput-object p2, p0, LX/8y0;->b:LX/1Ck;

    .line 1425027
    return-void
.end method

.method public static a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListRemoveLightweightRecMutationCallModel;
    .locals 9
    .param p1    # Lcom/facebook/graphql/model/GraphQLComment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1425030
    new-instance v0, LX/5HS;

    invoke-direct {v0}, LX/5HS;-><init>()V

    .line 1425031
    invoke-static {p1}, LX/5I9;->b(Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecMutationModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecMutationModel;->a(Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecMutationModel;)Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecMutationModel;

    move-result-object v1

    .line 1425032
    iput-object v1, v0, LX/5HS;->a:Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecMutationModel;

    .line 1425033
    move-object v1, v0

    .line 1425034
    iput-object p0, v1, LX/5HS;->b:Ljava/lang/String;

    .line 1425035
    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 1425036
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1425037
    iget-object v3, v0, LX/5HS;->a:Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecMutationModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1425038
    iget-object v5, v0, LX/5HS;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1425039
    const/4 v7, 0x2

    invoke-virtual {v2, v7}, LX/186;->c(I)V

    .line 1425040
    invoke-virtual {v2, v8, v3}, LX/186;->b(II)V

    .line 1425041
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1425042
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1425043
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1425044
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1425045
    invoke-virtual {v3, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1425046
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1425047
    new-instance v3, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListRemoveLightweightRecMutationCallModel;

    invoke-direct {v3, v2}, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListRemoveLightweightRecMutationCallModel;-><init>(LX/15i;)V

    .line 1425048
    move-object v0, v3

    .line 1425049
    return-object v0
.end method

.method public static b(LX/0QB;)LX/8y0;
    .locals 3

    .prologue
    .line 1425028
    new-instance v2, LX/8y0;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v1

    check-cast v1, LX/1Ck;

    invoke-direct {v2, v0, v1}, LX/8y0;-><init>(LX/0tX;LX/1Ck;)V

    .line 1425029
    return-object v2
.end method
