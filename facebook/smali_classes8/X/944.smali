.class public final LX/944;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/privacy/common/FixedPrivacyView;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/privacy/common/FixedPrivacyView;)V
    .locals 0

    .prologue
    .line 1434465
    iput-object p1, p0, LX/944;->a:Lcom/facebook/composer/privacy/common/FixedPrivacyView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onLongClick(Landroid/view/View;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1434459
    iget-object v2, p0, LX/944;->a:Lcom/facebook/composer/privacy/common/FixedPrivacyView;

    iget-object v2, v2, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->f:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v2

    .line 1434460
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/text/Layout;->getLineCount()I

    move-result v3

    if-lez v3, :cond_0

    invoke-virtual {v2, v1}, Landroid/text/Layout;->getEllipsisCount(I)I

    move-result v2

    if-lez v2, :cond_0

    .line 1434461
    iget-object v2, p0, LX/944;->a:Lcom/facebook/composer/privacy/common/FixedPrivacyView;

    invoke-virtual {v2}, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/944;->a:Lcom/facebook/composer/privacy/common/FixedPrivacyView;

    iget-object v3, v3, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->f:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    .line 1434462
    const/16 v3, 0x30

    invoke-virtual {v2, v3, v1, v1}, Landroid/widget/Toast;->setGravity(III)V

    .line 1434463
    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 1434464
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method
