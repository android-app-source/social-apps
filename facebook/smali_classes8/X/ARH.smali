.class public final LX/ARH;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/lang/Runnable;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/support/v4/app/DialogFragment;

.field public final synthetic b:Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;Landroid/support/v4/app/DialogFragment;)V
    .locals 0

    .prologue
    .line 1672862
    iput-object p1, p0, LX/ARH;->b:Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;

    iput-object p2, p0, LX/ARH;->a:Landroid/support/v4/app/DialogFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final dispose()V
    .locals 1

    .prologue
    .line 1672863
    invoke-super {p0}, LX/0Vd;->dispose()V

    .line 1672864
    iget-object v0, p0, LX/ARH;->a:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1672865
    iget-object v0, p0, LX/ARH;->b:Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;

    invoke-virtual {v0}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->finish()V

    .line 1672866
    return-void
.end method

.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1672867
    iget-object v0, p0, LX/ARH;->b:Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;

    iget-object v0, v0, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->u:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 1672868
    iget-object v0, p0, LX/ARH;->b:Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;

    invoke-virtual {v0}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->finish()V

    .line 1672869
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1672870
    check-cast p1, Ljava/lang/Runnable;

    .line 1672871
    if-eqz p1, :cond_0

    .line 1672872
    iget-object v0, p0, LX/ARH;->b:Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/shareintent/util/AbstractShareIntentHandler;->a(Ljava/lang/Runnable;)V

    .line 1672873
    :cond_0
    return-void
.end method
