.class public LX/APQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0ad;

.field private final b:LX/ATy;

.field private final c:LX/1RW;


# direct methods
.method public constructor <init>(LX/0ad;LX/1RW;LX/ATy;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1670178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1670179
    iput-object p1, p0, LX/APQ;->a:LX/0ad;

    .line 1670180
    iput-object p2, p0, LX/APQ;->c:LX/1RW;

    .line 1670181
    iput-object p3, p0, LX/APQ;->b:LX/ATy;

    .line 1670182
    return-void
.end method

.method private static a(LX/APQ;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1670183
    iget-object v0, p0, LX/APQ;->a:LX/0ad;

    sget-wide v2, LX/1aO;->l:J

    const-wide/16 v4, 0xa

    invoke-interface {v0, v2, v3, v4, v5}, LX/0ad;->a(JJ)J

    move-result-wide v0

    .line 1670184
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "NumChar:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/APQ;LX/0Px;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLTextWithEntities;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 1670174
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1670175
    :cond_0
    :goto_0
    return v0

    .line 1670176
    :cond_1
    iget-object v1, p0, LX/APQ;->a:LX/0ad;

    sget-wide v2, LX/1aO;->l:J

    const-wide/16 v4, 0xa

    invoke-interface {v1, v2, v3, v4, v5}, LX/0ad;->a(JJ)J

    move-result-wide v2

    .line 1670177
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v1

    int-to-long v4, v1

    cmp-long v1, v4, v2

    if-gtz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/ARN;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;LX/0Px;Lcom/facebook/graphql/model/GraphQLTextWithEntities;JLcom/facebook/ipc/composer/model/ProductItemAttachment;Lcom/facebook/ipc/composer/intent/ComposerTargetData;ZLcom/facebook/ipc/composer/intent/ComposerShareParams;Lcom/facebook/graphql/model/GraphQLAlbum;Lcom/facebook/share/model/ComposerAppAttribution;Lcom/facebook/ipc/composer/model/ComposerStickerData;Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)Z
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/ARN;",
            "Lcom/facebook/ipc/composer/intent/ComposerConfiguration;",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLTextWithEntities;",
            "J",
            "Lcom/facebook/ipc/composer/model/ProductItemAttachment;",
            "Lcom/facebook/ipc/composer/intent/ComposerTargetData;",
            "Z",
            "Lcom/facebook/ipc/composer/intent/ComposerShareParams;",
            "Lcom/facebook/graphql/model/GraphQLAlbum;",
            "Lcom/facebook/share/model/ComposerAppAttribution;",
            "Lcom/facebook/ipc/composer/model/ComposerStickerDataSpec;",
            "Lcom/facebook/ipc/composer/model/ComposerLocationInfo;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 1670147
    iget-object v0, p0, LX/APQ;->c:LX/1RW;

    invoke-virtual {v0, p1}, LX/1RW;->j(Ljava/lang/String;)V

    .line 1670148
    if-eqz p2, :cond_0

    invoke-interface {p2}, LX/ARN;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1670149
    iget-object v0, p0, LX/APQ;->c:LX/1RW;

    sget-object v1, LX/8K4;->PLUGIN_DISABLED:LX/8K4;

    iget-object v1, v1, LX/8K4;->analyticsName:Ljava/lang/String;

    invoke-virtual {p3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getComposerType()LX/2rt;

    move-result-object v2

    iget-object v2, v2, LX/2rt;->analyticsName:Ljava/lang/String;

    invoke-virtual {v0, p1, v1, v2}, LX/1RW;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1670150
    const/4 v0, 0x0

    .line 1670151
    :goto_0
    return v0

    .line 1670152
    :cond_0
    iget-object v0, p0, LX/APQ;->a:LX/0ad;

    sget-short v1, LX/1aO;->k:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1670153
    invoke-static {p0, p4, p5}, LX/APQ;->a(LX/APQ;LX/0Px;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1670154
    iget-object v0, p0, LX/APQ;->c:LX/1RW;

    invoke-static {p0}, LX/APQ;->a(LX/APQ;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1RW;->n(Ljava/lang/String;)V

    .line 1670155
    const/4 v0, 0x0

    goto :goto_0

    .line 1670156
    :cond_1
    iget-object v0, p0, LX/APQ;->c:LX/1RW;

    invoke-static {p0}, LX/APQ;->a(LX/APQ;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1RW;->o(Ljava/lang/String;)V

    .line 1670157
    :cond_2
    invoke-virtual {p3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getLaunchLoggingParams()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->getSourceSurface()LX/21D;

    move-result-object v0

    .line 1670158
    sget-object v1, LX/21D;->NEWSFEED:LX/21D;

    if-eq v0, v1, :cond_3

    sget-object v1, LX/21D;->GROUP_FEED:LX/21D;

    if-eq v0, v1, :cond_3

    sget-object v1, LX/21D;->PAGE_FEED:LX/21D;

    if-eq v0, v1, :cond_3

    sget-object v1, LX/21D;->EVENT:LX/21D;

    if-eq v0, v1, :cond_3

    sget-object v1, LX/21D;->TIMELINE:LX/21D;

    if-eq v0, v1, :cond_3

    sget-object v1, LX/21D;->COMPOST:LX/21D;

    if-eq v0, v1, :cond_3

    .line 1670159
    iget-object v1, p0, LX/APQ;->c:LX/1RW;

    invoke-virtual {v0}, LX/21D;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, LX/1RW;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1670160
    const/4 v0, 0x0

    goto :goto_0

    .line 1670161
    :cond_3
    invoke-virtual {p3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1670162
    iget-object v0, p0, LX/APQ;->c:LX/1RW;

    sget-object v1, LX/8K4;->PUBLISHED_POST:LX/8K4;

    iget-object v1, v1, LX/8K4;->analyticsName:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, LX/1RW;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1670163
    const/4 v0, 0x0

    goto :goto_0

    .line 1670164
    :cond_4
    const-wide/16 v0, 0x0

    cmp-long v0, p6, v0

    if-eqz v0, :cond_5

    .line 1670165
    iget-object v0, p0, LX/APQ;->c:LX/1RW;

    sget-object v1, LX/8K4;->MARKET_PLACE:LX/8K4;

    iget-object v1, v1, LX/8K4;->analyticsName:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, LX/1RW;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1670166
    const/4 v0, 0x0

    goto :goto_0

    .line 1670167
    :cond_5
    if-eqz p8, :cond_6

    .line 1670168
    iget-object v0, p0, LX/APQ;->c:LX/1RW;

    sget-object v1, LX/8K4;->PRODUCT_ATTACHMENT:LX/8K4;

    iget-object v1, v1, LX/8K4;->analyticsName:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, LX/1RW;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1670169
    const/4 v0, 0x0

    goto :goto_0

    .line 1670170
    :cond_6
    invoke-virtual {p3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isThrowbackPost()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1670171
    iget-object v0, p0, LX/APQ;->c:LX/1RW;

    sget-object v1, LX/8K4;->THROWBACK:LX/8K4;

    iget-object v1, v1, LX/8K4;->analyticsName:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, LX/1RW;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1670172
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1670173
    :cond_7
    iget-object v0, p0, LX/APQ;->b:LX/ATy;

    move-object v1, p1

    move-object/from16 v2, p9

    move/from16 v3, p10

    move-object/from16 v4, p11

    move-object/from16 v5, p12

    move-object/from16 v6, p13

    move-object/from16 v7, p14

    move-object/from16 v8, p15

    move-object v9, p4

    invoke-virtual/range {v0 .. v9}, LX/ATy;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerTargetData;ZLcom/facebook/ipc/composer/intent/ComposerShareParams;Lcom/facebook/graphql/model/GraphQLAlbum;Lcom/facebook/share/model/ComposerAppAttribution;Lcom/facebook/ipc/composer/model/ComposerStickerData;Lcom/facebook/ipc/composer/model/ComposerLocationInfo;LX/0Px;)Z

    move-result v0

    goto/16 :goto_0
.end method
