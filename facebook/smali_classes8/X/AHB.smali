.class public final LX/AHB;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1654866
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 1654867
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1654868
    :goto_0
    return v1

    .line 1654869
    :cond_0
    const-string v8, "unseen"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1654870
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v3, v0

    move v0, v2

    .line 1654871
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_5

    .line 1654872
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1654873
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1654874
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 1654875
    const-string v8, "first_post"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1654876
    invoke-static {p0, p1}, LX/AH8;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1654877
    :cond_2
    const-string v8, "id"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1654878
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 1654879
    :cond_3
    const-string v8, "last_posts"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1654880
    invoke-static {p0, p1}, LX/AHA;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1654881
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1654882
    :cond_5
    const/4 v7, 0x4

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1654883
    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 1654884
    invoke-virtual {p1, v2, v5}, LX/186;->b(II)V

    .line 1654885
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1654886
    if-eqz v0, :cond_6

    .line 1654887
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->a(IZ)V

    .line 1654888
    :cond_6
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1654889
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1654890
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1654891
    if-eqz v0, :cond_0

    .line 1654892
    const-string v1, "first_post"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1654893
    invoke-static {p0, v0, p2, p3}, LX/AH8;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1654894
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1654895
    if-eqz v0, :cond_1

    .line 1654896
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1654897
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1654898
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1654899
    if-eqz v0, :cond_2

    .line 1654900
    const-string v1, "last_posts"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1654901
    invoke-static {p0, v0, p2, p3}, LX/AHA;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1654902
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1654903
    if-eqz v0, :cond_3

    .line 1654904
    const-string v1, "unseen"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1654905
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1654906
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1654907
    return-void
.end method
