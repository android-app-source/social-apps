.class public LX/9Su;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/B1W;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/B1W",
        "<",
        "Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsLearningUnitPostIdsModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/api/feedtype/FeedType;

.field private final b:LX/0pn;


# direct methods
.method public constructor <init>(Lcom/facebook/api/feedtype/FeedType;LX/0pn;)V
    .locals 0
    .param p1    # Lcom/facebook/api/feedtype/FeedType;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1494791
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1494792
    iput-object p1, p0, LX/9Su;->a:Lcom/facebook/api/feedtype/FeedType;

    .line 1494793
    iput-object p2, p0, LX/9Su;->b:LX/0pn;

    .line 1494794
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/0Px;
    .locals 5

    .prologue
    .line 1494774
    check-cast p1, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsLearningUnitPostIdsModel;

    .line 1494775
    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsLearningUnitPostIdsModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsLearningUnitPostIdsModel$LearningCourseUnitStoriesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsLearningUnitPostIdsModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsLearningUnitPostIdsModel$LearningCourseUnitStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsLearningUnitPostIdsModel$LearningCourseUnitStoriesModel;->a()LX/0Px;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1494776
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1494777
    :goto_0
    return-object v0

    .line 1494778
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1494779
    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsLearningUnitPostIdsModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsLearningUnitPostIdsModel$LearningCourseUnitStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsLearningUnitPostIdsModel$LearningCourseUnitStoriesModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_2

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsLearningUnitPostIdsModel$LearningCourseUnitStoriesModel$NodesModel;

    .line 1494780
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsLearningUnitPostIdsModel$LearningCourseUnitStoriesModel$NodesModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1494781
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1494782
    :cond_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()LX/0zO;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0zO",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsLearningUnitPostIdsModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1494783
    iget-object v0, p0, LX/9Su;->a:Lcom/facebook/api/feedtype/FeedType;

    .line 1494784
    iget-object v1, v0, Lcom/facebook/api/feedtype/FeedType;->e:Ljava/lang/Object;

    move-object v0, v1

    .line 1494785
    check-cast v0, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;

    .line 1494786
    iget-object v1, v0, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->h:Ljava/lang/String;

    move-object v0, v1

    .line 1494787
    new-instance v1, LX/9Rt;

    invoke-direct {v1}, LX/9Rt;-><init>()V

    move-object v1, v1

    .line 1494788
    const-string v2, "unit_id"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "stories_to_fetch"

    iget-object v3, p0, LX/9Su;->b:LX/0pn;

    invoke-virtual {v3}, LX/0pn;->d()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1494789
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    return-object v0
.end method

.method public final b()LX/0TF;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupsFeedIdsModels$FetchGroupsLearningUnitPostIdsModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1494790
    new-instance v0, LX/B1V;

    iget-object v1, p0, LX/9Su;->a:Lcom/facebook/api/feedtype/FeedType;

    iget-object v2, p0, LX/9Su;->b:LX/0pn;

    invoke-direct {v0, p0, v1, v2}, LX/B1V;-><init>(LX/B1W;Lcom/facebook/api/feedtype/FeedType;LX/0pn;)V

    return-object v0
.end method
