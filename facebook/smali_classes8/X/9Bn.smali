.class public final LX/9Bn;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public final synthetic b:Lcom/facebook/feedback/ui/BaseFeedbackFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/BaseFeedbackFragment;Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 0

    .prologue
    .line 1452495
    iput-object p1, p0, LX/9Bn;->b:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iput-object p2, p0, LX/9Bn;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1452490
    iget-object v0, p0, LX/9Bn;->b:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    iget-object v1, p0, LX/9Bn;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v0, v1}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->e(Lcom/facebook/feedback/ui/BaseFeedbackFragment;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1452491
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1452492
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1452493
    iget-object v0, p0, LX/9Bn;->b:Lcom/facebook/feedback/ui/BaseFeedbackFragment;

    invoke-static {v0, p1}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->e(Lcom/facebook/feedback/ui/BaseFeedbackFragment;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1452494
    return-void
.end method
