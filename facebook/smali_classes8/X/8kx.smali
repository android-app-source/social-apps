.class public final LX/8kx;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/stickers/model/Sticker;

.field public final synthetic b:Lcom/facebook/stickers/keyboard/StickerKeyboardView;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/keyboard/StickerKeyboardView;Lcom/facebook/stickers/model/Sticker;)V
    .locals 0

    .prologue
    .line 1396983
    iput-object p1, p0, LX/8kx;->b:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    iput-object p2, p0, LX/8kx;->a:Lcom/facebook/stickers/model/Sticker;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1396990
    sget-object v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->c:Ljava/lang/Class;

    const-string v1, "Updating recent stickers failed"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1396991
    iget-object v0, p0, LX/8kx;->b:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    const/4 v1, 0x0

    .line 1396992
    iput-object v1, v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->t:LX/1Mv;

    .line 1396993
    iget-object v0, p0, LX/8kx;->b:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    iget-object v0, v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->l:LX/03V;

    sget-object v1, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->c:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Updating recent stickers failed"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1396994
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1396984
    iget-object v0, p0, LX/8kx;->a:Lcom/facebook/stickers/model/Sticker;

    iget-object v0, v0, Lcom/facebook/stickers/model/Sticker;->i:Lcom/facebook/stickers/model/StickerCapabilities;

    invoke-virtual {v0}, Lcom/facebook/stickers/model/StickerCapabilities;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1396985
    iget-object v0, p0, LX/8kx;->b:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    iget-object v1, p0, LX/8kx;->a:Lcom/facebook/stickers/model/Sticker;

    iget-object v1, v1, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->a$redex0(Lcom/facebook/stickers/keyboard/StickerKeyboardView;LX/0Px;)V

    .line 1396986
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Sticker added to recent list: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/8kx;->a:Lcom/facebook/stickers/model/Sticker;

    iget-object v1, v1, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1396987
    iget-object v0, p0, LX/8kx;->b:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    const/4 v1, 0x0

    .line 1396988
    iput-object v1, v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->t:LX/1Mv;

    .line 1396989
    return-void
.end method
