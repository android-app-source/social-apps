.class public LX/8ow;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field private a:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;


# direct methods
.method public constructor <init>(Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;)V
    .locals 0

    .prologue
    .line 1405027
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1405028
    iput-object p1, p0, LX/8ow;->a:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    .line 1405029
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 14

    .prologue
    const/4 v2, 0x0

    .line 1405030
    iget-object v0, p0, LX/8ow;->a:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    check-cast v0, LX/8of;

    .line 1405031
    invoke-virtual {v0}, LX/8of;->length()I

    move-result v1

    const-class v3, LX/7Gi;

    invoke-virtual {v0, v2, v1, v3}, LX/8of;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [LX/7Gi;

    .line 1405032
    array-length v3, v1

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v1, v2

    .line 1405033
    invoke-virtual {v0, v4}, LX/8of;->removeSpan(Ljava/lang/Object;)V

    .line 1405034
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1405035
    :cond_0
    const-class v1, LX/7Gl;

    const/4 v3, 0x0

    .line 1405036
    invoke-virtual {v0}, LX/8of;->length()I

    move-result v2

    invoke-virtual {v0, v3, v2, v1}, LX/8of;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [LX/7Gh;

    .line 1405037
    array-length v4, v2

    invoke-static {v4}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v4

    .line 1405038
    array-length v5, v2

    :goto_1
    if-ge v3, v5, :cond_1

    aget-object v6, v2, v3

    .line 1405039
    invoke-interface {v6, v0}, LX/7Gh;->a(Landroid/text/Editable;)I

    move-result v7

    .line 1405040
    invoke-interface {v6, v0}, LX/7Gh;->b(Landroid/text/Editable;)I

    move-result v6

    .line 1405041
    new-instance v8, LX/1yN;

    sub-int/2addr v6, v7

    invoke-direct {v8, v7, v6}, LX/1yN;-><init>(II)V

    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1405042
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1405043
    :cond_1
    move-object v1, v4

    .line 1405044
    move-object v1, v1

    .line 1405045
    iget-object v2, p0, LX/8ow;->a:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-virtual {v2}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getUserText()Ljava/lang/CharSequence;

    move-result-object v2

    .line 1405046
    new-instance v3, LX/88v;

    invoke-direct {v3, v2}, LX/88v;-><init>(Ljava/lang/CharSequence;)V

    const/16 v10, 0x1e

    const/4 v9, 0x2

    .line 1405047
    sget-object v2, LX/88v;->g:Ljava/util/regex/Pattern;

    iget-object v4, v3, LX/88v;->a:Ljava/lang/CharSequence;

    invoke-virtual {v2, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 1405048
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v4

    .line 1405049
    :cond_2
    :goto_2
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1405050
    invoke-virtual {v2, v9}, Ljava/util/regex/Matcher;->end(I)I

    move-result v5

    invoke-virtual {v2, v9}, Ljava/util/regex/Matcher;->start(I)I

    move-result v6

    sub-int/2addr v5, v6

    add-int/lit8 v5, v5, -0x1

    const/16 v6, 0x64

    if-gt v5, v6, :cond_2

    .line 1405051
    new-instance v5, LX/1yN;

    invoke-virtual {v2, v9}, Ljava/util/regex/Matcher;->start(I)I

    move-result v6

    invoke-virtual {v2, v9}, Ljava/util/regex/Matcher;->end(I)I

    move-result v7

    invoke-virtual {v2, v9}, Ljava/util/regex/Matcher;->start(I)I

    move-result v8

    sub-int/2addr v7, v8

    invoke-direct {v5, v6, v7}, LX/1yN;-><init>(II)V

    invoke-virtual {v4, v5}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1405052
    :cond_3
    iget-object v2, v3, LX/88v;->a:Ljava/lang/CharSequence;

    .line 1405053
    const/4 v5, 0x1

    .line 1405054
    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v6

    const/4 v7, 0x4

    if-lt v6, v7, :cond_6

    if-nez v5, :cond_4

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v6

    const/4 v7, 0x7

    if-lt v6, v7, :cond_6

    :cond_4
    const/16 v6, 0x2e

    invoke-static {v2, v6}, LX/88x;->a(Ljava/lang/CharSequence;C)I

    move-result v6

    if-gez v6, :cond_5

    const/16 v6, 0x3a

    invoke-static {v2, v6}, LX/88x;->a(Ljava/lang/CharSequence;C)I

    move-result v6

    if-ltz v6, :cond_6

    :cond_5
    sget-object v6, LX/88x;->b:Ljava/util/regex/Pattern;

    invoke-virtual {v6, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/regex/Matcher;->matches()Z

    move-result v6

    if-eqz v6, :cond_9

    .line 1405055
    :cond_6
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v6

    .line 1405056
    :goto_3
    move-object v5, v6

    .line 1405057
    move-object v2, v5

    .line 1405058
    invoke-static {v4, v2}, LX/88v;->a(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2, v1}, LX/88v;->a(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 1405059
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    if-le v4, v10, :cond_7

    .line 1405060
    const/4 v4, 0x0

    invoke-interface {v2, v4, v10}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    .line 1405061
    :cond_7
    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    move-object v1, v2

    .line 1405062
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1yN;

    .line 1405063
    new-instance v3, LX/7Gi;

    invoke-static {v0}, LX/8of;->d(LX/8of;)I

    move-result v4

    invoke-static {v0}, LX/8of;->c(LX/8of;)I

    move-result v5

    invoke-direct {v3, v4, v5}, LX/7Gi;-><init>(II)V

    .line 1405064
    iget v4, v1, LX/1yN;->a:I

    move v4, v4

    .line 1405065
    invoke-virtual {v1}, LX/1yN;->c()I

    move-result v5

    const/16 v6, 0x21

    invoke-virtual {v0, v3, v4, v5, v6}, LX/8of;->setSpan(Ljava/lang/Object;III)V

    .line 1405066
    goto :goto_4

    .line 1405067
    :cond_8
    return-void

    .line 1405068
    :cond_9
    sget-object v6, LX/88x;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v6, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v13

    .line 1405069
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v11

    .line 1405070
    :cond_a
    :goto_5
    invoke-virtual {v13}, Ljava/util/regex/Matcher;->find()Z

    move-result v6

    if-eqz v6, :cond_f

    .line 1405071
    invoke-virtual {v13}, Ljava/util/regex/Matcher;->start()I

    move-result v12

    .line 1405072
    invoke-virtual {v13}, Ljava/util/regex/Matcher;->end()I

    move-result p0

    .line 1405073
    if-lez v12, :cond_b

    add-int/lit8 v6, v12, -0x1

    invoke-interface {v2, v6}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v6

    const/16 v7, 0x40

    if-eq v6, v7, :cond_a

    .line 1405074
    :cond_b
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v3

    move v9, v12

    move v7, v12

    .line 1405075
    :goto_6
    if-ge v9, p0, :cond_12

    .line 1405076
    sget-object v6, LX/88x;->a:Ljava/util/Map;

    invoke-interface {v2, v9}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v8

    invoke-static {v8}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    .line 1405077
    if-eqz v6, :cond_11

    .line 1405078
    invoke-virtual {v3}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_10

    move v8, v9

    .line 1405079
    :goto_7
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-lez v7, :cond_d

    .line 1405080
    invoke-virtual {v3, v6}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 1405081
    :cond_c
    :goto_8
    add-int/lit8 v9, v9, 0x1

    move v7, v8

    goto :goto_6

    .line 1405082
    :cond_d
    invoke-virtual {v3}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_c

    invoke-virtual {v3}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    add-int/2addr v6, v7

    if-eqz v6, :cond_c

    .line 1405083
    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 1405084
    :goto_9
    invoke-virtual {v3}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_e

    .line 1405085
    new-instance v6, LX/1yN;

    sub-int v7, v8, v12

    invoke-direct {v6, v12, v7}, LX/1yN;-><init>(II)V

    invoke-interface {v11, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 1405086
    :cond_e
    new-instance v6, LX/1yN;

    sub-int v7, p0, v12

    invoke-direct {v6, v12, v7}, LX/1yN;-><init>(II)V

    invoke-interface {v11, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_5

    :cond_f
    move-object v6, v11

    .line 1405087
    goto/16 :goto_3

    :cond_10
    move v8, v7

    goto :goto_7

    :cond_11
    move v8, v7

    goto :goto_8

    :cond_12
    move v8, v7

    goto :goto_9
.end method

.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 1405088
    invoke-virtual {p0}, LX/8ow;->a()V

    .line 1405089
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1405090
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1405091
    return-void
.end method
