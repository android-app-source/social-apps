.class public final LX/ALW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:Lcom/facebook/backstage/camera/CaptureButton;


# direct methods
.method public constructor <init>(Lcom/facebook/backstage/camera/CaptureButton;)V
    .locals 0

    .prologue
    .line 1664999
    iput-object p1, p0, LX/ALW;->a:Lcom/facebook/backstage/camera/CaptureButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 1665000
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 1665001
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1665002
    :pswitch_0
    iget-object v1, p0, LX/ALW;->a:Lcom/facebook/backstage/camera/CaptureButton;

    sget-object v2, LX/ALZ;->PHOTO:LX/ALZ;

    .line 1665003
    iput-object v2, v1, Lcom/facebook/backstage/camera/CaptureButton;->i:LX/ALZ;

    .line 1665004
    iget-object v1, p0, LX/ALW;->a:Lcom/facebook/backstage/camera/CaptureButton;

    sget-object v2, LX/ALY;->NONE:LX/ALY;

    .line 1665005
    iput-object v2, v1, Lcom/facebook/backstage/camera/CaptureButton;->h:LX/ALY;

    .line 1665006
    iget-object v1, p0, LX/ALW;->a:Lcom/facebook/backstage/camera/CaptureButton;

    iget-object v1, v1, Lcom/facebook/backstage/camera/CaptureButton;->f:Landroid/os/Handler;

    iget-object v2, p0, LX/ALW;->a:Lcom/facebook/backstage/camera/CaptureButton;

    iget-object v2, v2, Lcom/facebook/backstage/camera/CaptureButton;->g:Ljava/lang/Runnable;

    invoke-static {v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1665007
    iget-object v1, p0, LX/ALW;->a:Lcom/facebook/backstage/camera/CaptureButton;

    iget-object v1, v1, Lcom/facebook/backstage/camera/CaptureButton;->f:Landroid/os/Handler;

    iget-object v2, p0, LX/ALW;->a:Lcom/facebook/backstage/camera/CaptureButton;

    iget-object v2, v2, Lcom/facebook/backstage/camera/CaptureButton;->g:Ljava/lang/Runnable;

    const-wide/16 v4, 0x7d0

    const v3, -0x6f798ecb

    invoke-static {v1, v2, v4, v5, v3}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1665008
    iget-object v1, p0, LX/ALW;->a:Lcom/facebook/backstage/camera/CaptureButton;

    invoke-static {v1}, Lcom/facebook/backstage/camera/CaptureButton;->c(Lcom/facebook/backstage/camera/CaptureButton;)V

    goto :goto_0

    .line 1665009
    :pswitch_1
    iget-object v1, p0, LX/ALW;->a:Lcom/facebook/backstage/camera/CaptureButton;

    iget-object v1, v1, Lcom/facebook/backstage/camera/CaptureButton;->f:Landroid/os/Handler;

    iget-object v2, p0, LX/ALW;->a:Lcom/facebook/backstage/camera/CaptureButton;

    iget-object v2, v2, Lcom/facebook/backstage/camera/CaptureButton;->g:Ljava/lang/Runnable;

    invoke-static {v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1665010
    iget-object v1, p0, LX/ALW;->a:Lcom/facebook/backstage/camera/CaptureButton;

    invoke-static {v1}, Lcom/facebook/backstage/camera/CaptureButton;->b(Lcom/facebook/backstage/camera/CaptureButton;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
