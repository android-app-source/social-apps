.class public LX/AOM;
.super LX/AOH;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/AOH",
        "<",
        "Ljava/lang/Integer;",
        "LX/62U",
        "<",
        "Landroid/view/View;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Px;ILX/ANk;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;I",
            "LX/ANk;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1668907
    invoke-direct {p0, p1, p2, p3, p4}, LX/AOH;-><init>(Landroid/content/Context;LX/0Px;ILX/ANk;)V

    .line 1668908
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 1668905
    iget-object v0, p0, LX/AOH;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030236

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1668906
    new-instance v1, LX/62U;

    invoke-direct {v1, v0}, LX/62U;-><init>(Landroid/view/View;)V

    return-object v1
.end method

.method public final a(LX/62U;Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1668891
    check-cast p2, Ljava/lang/Integer;

    .line 1668892
    iget-object v0, p1, LX/62U;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1668893
    instance-of v1, v0, Landroid/graphics/drawable/StateListDrawable;

    if-nez v1, :cond_0

    .line 1668894
    iget-object v0, p0, LX/AOH;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0201e1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1668895
    iget-object v1, p1, LX/62U;->l:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1668896
    :cond_0
    check-cast v0, Landroid/graphics/drawable/StateListDrawable;

    .line 1668897
    invoke-virtual {v0}, Landroid/graphics/drawable/StateListDrawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;

    .line 1668898
    invoke-virtual {v0}, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->getChildren()[Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1668899
    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v0, v2, v1

    .line 1668900
    instance-of v4, v0, Landroid/graphics/drawable/GradientDrawable;

    if-eqz v4, :cond_1

    .line 1668901
    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    .line 1668902
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 1668903
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1668904
    :cond_2
    return-void
.end method
