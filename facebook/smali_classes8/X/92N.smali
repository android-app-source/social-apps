.class public LX/92N;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0tX;

.field public final b:LX/92T;


# direct methods
.method public constructor <init>(LX/0tX;LX/92T;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1432254
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1432255
    iput-object p1, p0, LX/92N;->a:LX/0tX;

    .line 1432256
    iput-object p2, p0, LX/92N;->b:LX/92T;

    .line 1432257
    return-void
.end method


# virtual methods
.method public final a(LX/0w7;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0w7;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/92n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1432253
    iget-object v0, p0, LX/92N;->a:LX/0tX;

    invoke-static {}, LX/5L6;->a()LX/5L4;

    move-result-object v1

    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/0zO;->a(LX/0w7;)LX/0zO;

    move-result-object v1

    sget-object v2, LX/0zS;->c:LX/0zS;

    invoke-virtual {v1, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    new-instance v1, LX/92L;

    invoke-direct {v1, p0}, LX/92L;-><init>(LX/92N;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
