.class public final enum LX/A5i;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/A5i;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/A5i;

.field public static final enum ALL_FRIENDS:LX/A5i;

.field public static final enum GOOD_FRIENDS:LX/A5i;

.field public static final enum GROUP_SUGGESTIONS:LX/A5i;

.field public static final enum SUGGESTIONS:LX/A5i;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1621756
    new-instance v0, LX/A5i;

    const-string v1, "GROUP_SUGGESTIONS"

    invoke-direct {v0, v1, v2}, LX/A5i;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/A5i;->GROUP_SUGGESTIONS:LX/A5i;

    .line 1621757
    new-instance v0, LX/A5i;

    const-string v1, "SUGGESTIONS"

    invoke-direct {v0, v1, v3}, LX/A5i;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/A5i;->SUGGESTIONS:LX/A5i;

    .line 1621758
    new-instance v0, LX/A5i;

    const-string v1, "ALL_FRIENDS"

    invoke-direct {v0, v1, v4}, LX/A5i;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/A5i;->ALL_FRIENDS:LX/A5i;

    .line 1621759
    new-instance v0, LX/A5i;

    const-string v1, "GOOD_FRIENDS"

    invoke-direct {v0, v1, v5}, LX/A5i;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/A5i;->GOOD_FRIENDS:LX/A5i;

    .line 1621760
    const/4 v0, 0x4

    new-array v0, v0, [LX/A5i;

    sget-object v1, LX/A5i;->GROUP_SUGGESTIONS:LX/A5i;

    aput-object v1, v0, v2

    sget-object v1, LX/A5i;->SUGGESTIONS:LX/A5i;

    aput-object v1, v0, v3

    sget-object v1, LX/A5i;->ALL_FRIENDS:LX/A5i;

    aput-object v1, v0, v4

    sget-object v1, LX/A5i;->GOOD_FRIENDS:LX/A5i;

    aput-object v1, v0, v5

    sput-object v0, LX/A5i;->$VALUES:[LX/A5i;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1621761
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/A5i;
    .locals 1

    .prologue
    .line 1621762
    const-class v0, LX/A5i;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/A5i;

    return-object v0
.end method

.method public static values()[LX/A5i;
    .locals 1

    .prologue
    .line 1621763
    sget-object v0, LX/A5i;->$VALUES:[LX/A5i;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/A5i;

    return-object v0
.end method
