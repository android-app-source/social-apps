.class public LX/9jB;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private b:LX/0SG;

.field private c:LX/0Tn;

.field private d:LX/0Tn;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1529246
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1529247
    iput-object p1, p0, LX/9jB;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1529248
    iput-object p2, p0, LX/9jB;->b:LX/0SG;

    .line 1529249
    return-void
.end method

.method public static b(LX/0QB;)LX/9jB;
    .locals 3

    .prologue
    .line 1529258
    new-instance v2, LX/9jB;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-direct {v2, v0, v1}, LX/9jB;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;)V

    .line 1529259
    return-object v2
.end method

.method private d()I
    .locals 3

    .prologue
    .line 1529257
    iget-object v0, p0, LX/9jB;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v1, p0, LX/9jB;->c:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1529260
    iget-object v0, p0, LX/9jB;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    iget-object v1, p0, LX/9jB;->c:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1529261
    return-void
.end method

.method public final a(LX/0Tn;LX/0Tn;)V
    .locals 0

    .prologue
    .line 1529254
    iput-object p1, p0, LX/9jB;->c:LX/0Tn;

    .line 1529255
    iput-object p2, p0, LX/9jB;->d:LX/0Tn;

    .line 1529256
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 1529252
    iget-object v0, p0, LX/9jB;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    iget-object v1, p0, LX/9jB;->c:LX/0Tn;

    invoke-direct {p0}, LX/9jB;->d()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    iget-object v1, p0, LX/9jB;->d:LX/0Tn;

    iget-object v2, p0, LX/9jB;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1529253
    return-void
.end method

.method public final c()Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 1529250
    iget-object v1, p0, LX/9jB;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    iget-object v1, p0, LX/9jB;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v4, p0, LX/9jB;->d:LX/0Tn;

    const-wide/16 v6, 0x0

    invoke-interface {v1, v4, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/32 v4, 0x2932e00

    cmp-long v1, v2, v4

    if-gez v1, :cond_1

    .line 1529251
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0}, LX/9jB;->d()I

    move-result v1

    const/4 v2, 0x3

    if-ge v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
