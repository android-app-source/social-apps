.class public LX/A76;
.super LX/0Tv;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/A76;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1625478
    const-string v0, "dictionary"

    const/4 v1, 0x1

    new-instance v2, LX/A75;

    invoke-direct {v2}, LX/A75;-><init>()V

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, LX/0Tv;-><init>(Ljava/lang/String;ILX/0Px;)V

    .line 1625479
    return-void
.end method

.method public static a(LX/0QB;)LX/A76;
    .locals 3

    .prologue
    .line 1625480
    sget-object v0, LX/A76;->a:LX/A76;

    if-nez v0, :cond_1

    .line 1625481
    const-class v1, LX/A76;

    monitor-enter v1

    .line 1625482
    :try_start_0
    sget-object v0, LX/A76;->a:LX/A76;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1625483
    if-eqz v2, :cond_0

    .line 1625484
    :try_start_1
    new-instance v0, LX/A76;

    invoke-direct {v0}, LX/A76;-><init>()V

    .line 1625485
    move-object v0, v0

    .line 1625486
    sput-object v0, LX/A76;->a:LX/A76;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1625487
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1625488
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1625489
    :cond_1
    sget-object v0, LX/A76;->a:LX/A76;

    return-object v0

    .line 1625490
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1625491
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
