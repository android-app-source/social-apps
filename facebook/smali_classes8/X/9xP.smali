.class public final LX/9xP;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 32

    .prologue
    .line 1589604
    const/16 v25, 0x0

    .line 1589605
    const/16 v24, 0x0

    .line 1589606
    const/16 v23, 0x0

    .line 1589607
    const/16 v22, 0x0

    .line 1589608
    const/16 v21, 0x0

    .line 1589609
    const/16 v20, 0x0

    .line 1589610
    const/16 v19, 0x0

    .line 1589611
    const/16 v18, 0x0

    .line 1589612
    const/4 v15, 0x0

    .line 1589613
    const-wide/16 v16, 0x0

    .line 1589614
    const/4 v14, 0x0

    .line 1589615
    const/4 v13, 0x0

    .line 1589616
    const/4 v12, 0x0

    .line 1589617
    const/4 v11, 0x0

    .line 1589618
    const/4 v10, 0x0

    .line 1589619
    const/4 v9, 0x0

    .line 1589620
    const/4 v8, 0x0

    .line 1589621
    const/4 v7, 0x0

    .line 1589622
    const/4 v6, 0x0

    .line 1589623
    const/4 v5, 0x0

    .line 1589624
    const/4 v4, 0x0

    .line 1589625
    const/4 v3, 0x0

    .line 1589626
    const/4 v2, 0x0

    .line 1589627
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v26

    sget-object v27, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    if-eq v0, v1, :cond_19

    .line 1589628
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1589629
    const/4 v2, 0x0

    .line 1589630
    :goto_0
    return v2

    .line 1589631
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v27, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v27

    if-eq v2, v0, :cond_11

    .line 1589632
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1589633
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1589634
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v27

    sget-object v28, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-eq v0, v1, :cond_0

    if-eqz v2, :cond_0

    .line 1589635
    const-string v27, "broadcast_network"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_1

    .line 1589636
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v26, v2

    goto :goto_1

    .line 1589637
    :cond_1
    const-string v27, "clock"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_2

    .line 1589638
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v25, v2

    goto :goto_1

    .line 1589639
    :cond_2
    const-string v27, "first_team_fan_count"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_3

    .line 1589640
    const/4 v2, 0x1

    .line 1589641
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v11

    move/from16 v24, v11

    move v11, v2

    goto :goto_1

    .line 1589642
    :cond_3
    const-string v27, "first_team_object"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_4

    .line 1589643
    invoke-static/range {p0 .. p1}, LX/9xK;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v23, v2

    goto :goto_1

    .line 1589644
    :cond_4
    const-string v27, "first_team_primary_color"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_5

    .line 1589645
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v22, v2

    goto/16 :goto_1

    .line 1589646
    :cond_5
    const-string v27, "first_team_score"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_6

    .line 1589647
    const/4 v2, 0x1

    .line 1589648
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v7

    move/from16 v21, v7

    move v7, v2

    goto/16 :goto_1

    .line 1589649
    :cond_6
    const-string v27, "has_match_started"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_7

    .line 1589650
    const/4 v2, 0x1

    .line 1589651
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move/from16 v20, v6

    move v6, v2

    goto/16 :goto_1

    .line 1589652
    :cond_7
    const-string v27, "id"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_8

    .line 1589653
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 1589654
    :cond_8
    const-string v27, "match_page"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_9

    .line 1589655
    invoke-static/range {p0 .. p1}, LX/9xL;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 1589656
    :cond_9
    const-string v27, "scheduled_start_time"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_a

    .line 1589657
    const/4 v2, 0x1

    .line 1589658
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 1589659
    :cond_a
    const-string v27, "second_team_fan_count"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_b

    .line 1589660
    const/4 v2, 0x1

    .line 1589661
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v10

    move/from16 v17, v10

    move v10, v2

    goto/16 :goto_1

    .line 1589662
    :cond_b
    const-string v27, "second_team_object"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_c

    .line 1589663
    invoke-static/range {p0 .. p1}, LX/9xO;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v16, v2

    goto/16 :goto_1

    .line 1589664
    :cond_c
    const-string v27, "second_team_primary_color"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_d

    .line 1589665
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 1589666
    :cond_d
    const-string v27, "second_team_score"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_e

    .line 1589667
    const/4 v2, 0x1

    .line 1589668
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v9

    move v14, v9

    move v9, v2

    goto/16 :goto_1

    .line 1589669
    :cond_e
    const-string v27, "viewer_can_vote_fan_favorite"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_f

    .line 1589670
    const/4 v2, 0x1

    .line 1589671
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v8

    move v13, v8

    move v8, v2

    goto/16 :goto_1

    .line 1589672
    :cond_f
    const-string v27, "winning_team"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 1589673
    invoke-static/range {p0 .. p1}, LX/9q2;->a(LX/15w;LX/186;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 1589674
    :cond_10
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1589675
    :cond_11
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1589676
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1589677
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1589678
    if-eqz v11, :cond_12

    .line 1589679
    const/4 v2, 0x2

    const/4 v11, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1, v11}, LX/186;->a(III)V

    .line 1589680
    :cond_12
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1589681
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1589682
    if-eqz v7, :cond_13

    .line 1589683
    const/4 v2, 0x5

    const/4 v7, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1, v7}, LX/186;->a(III)V

    .line 1589684
    :cond_13
    if-eqz v6, :cond_14

    .line 1589685
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1589686
    :cond_14
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1589687
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1589688
    if-eqz v3, :cond_15

    .line 1589689
    const/16 v3, 0x9

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1589690
    :cond_15
    if-eqz v10, :cond_16

    .line 1589691
    const/16 v2, 0xa

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 1589692
    :cond_16
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1589693
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1589694
    if-eqz v9, :cond_17

    .line 1589695
    const/16 v2, 0xd

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14, v3}, LX/186;->a(III)V

    .line 1589696
    :cond_17
    if-eqz v8, :cond_18

    .line 1589697
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->a(IZ)V

    .line 1589698
    :cond_18
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1589699
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_19
    move/from16 v26, v25

    move/from16 v25, v24

    move/from16 v24, v23

    move/from16 v23, v22

    move/from16 v22, v21

    move/from16 v21, v20

    move/from16 v20, v19

    move/from16 v19, v18

    move/from16 v18, v15

    move v15, v12

    move v12, v9

    move v9, v3

    move v3, v5

    move/from16 v29, v11

    move v11, v8

    move v8, v2

    move/from16 v30, v14

    move/from16 v14, v29

    move/from16 v31, v13

    move v13, v10

    move v10, v4

    move-wide/from16 v4, v16

    move/from16 v17, v30

    move/from16 v16, v31

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 1589700
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1589701
    invoke-virtual {p0, p1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1589702
    if-eqz v0, :cond_0

    .line 1589703
    const-string v1, "broadcast_network"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589704
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1589705
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1589706
    if-eqz v0, :cond_1

    .line 1589707
    const-string v1, "clock"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589708
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1589709
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1589710
    if-eqz v0, :cond_2

    .line 1589711
    const-string v1, "first_team_fan_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589712
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1589713
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1589714
    if-eqz v0, :cond_3

    .line 1589715
    const-string v1, "first_team_object"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589716
    invoke-static {p0, v0, p2, p3}, LX/9xK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1589717
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1589718
    if-eqz v0, :cond_4

    .line 1589719
    const-string v1, "first_team_primary_color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589720
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1589721
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1589722
    if-eqz v0, :cond_5

    .line 1589723
    const-string v1, "first_team_score"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589724
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1589725
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1589726
    if-eqz v0, :cond_6

    .line 1589727
    const-string v1, "has_match_started"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589728
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1589729
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1589730
    if-eqz v0, :cond_7

    .line 1589731
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589732
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1589733
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1589734
    if-eqz v0, :cond_8

    .line 1589735
    const-string v1, "match_page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589736
    invoke-static {p0, v0, p2}, LX/9xL;->a(LX/15i;ILX/0nX;)V

    .line 1589737
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1589738
    cmp-long v2, v0, v4

    if-eqz v2, :cond_9

    .line 1589739
    const-string v2, "scheduled_start_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589740
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1589741
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1589742
    if-eqz v0, :cond_a

    .line 1589743
    const-string v1, "second_team_fan_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589744
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1589745
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1589746
    if-eqz v0, :cond_b

    .line 1589747
    const-string v1, "second_team_object"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589748
    invoke-static {p0, v0, p2, p3}, LX/9xO;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1589749
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1589750
    if-eqz v0, :cond_c

    .line 1589751
    const-string v1, "second_team_primary_color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589752
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1589753
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1589754
    if-eqz v0, :cond_d

    .line 1589755
    const-string v1, "second_team_score"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589756
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1589757
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1589758
    if-eqz v0, :cond_e

    .line 1589759
    const-string v1, "viewer_can_vote_fan_favorite"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589760
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1589761
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1589762
    if-eqz v0, :cond_f

    .line 1589763
    const-string v1, "winning_team"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589764
    invoke-static {p0, v0, p2}, LX/9q2;->a(LX/15i;ILX/0nX;)V

    .line 1589765
    :cond_f
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1589766
    return-void
.end method
