.class public final enum LX/8wL;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8wL;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8wL;

.field public static final enum AUDIENCE_MANAGEMENT:LX/8wL;

.field public static final enum BOOSTED_COMPONENT_EDIT_BUDGET:LX/8wL;

.field public static final enum BOOSTED_COMPONENT_EDIT_DURATION:LX/8wL;

.field public static final enum BOOSTED_COMPONENT_EDIT_DURATION_BUDGET:LX/8wL;

.field public static final enum BOOSTED_COMPONENT_EDIT_PACING:LX/8wL;

.field public static final enum BOOSTED_COMPONENT_EDIT_TARGETING:LX/8wL;

.field public static final enum BOOSTED_POST_ACTION_BUTTON:LX/8wL;

.field public static final enum BOOST_EVENT:LX/8wL;

.field public static final enum BOOST_EVENT_EDIT_CREATIVE:LX/8wL;

.field public static final enum BOOST_EVENT_EDIT_TARGETING:LX/8wL;

.field public static final enum BOOST_LIVE:LX/8wL;

.field public static final enum BOOST_POST:LX/8wL;

.field public static final enum BOOST_POST_EDIT_TARGETING:LX/8wL;

.field public static final enum BOOST_POST_INSIGHTS:LX/8wL;

.field public static final enum LOCAL_AWARENESS:LX/8wL;

.field public static final enum LOCAL_AWARENESS_EDIT_CREATIVE:LX/8wL;

.field public static final enum PAGE_LIKE:LX/8wL;

.field public static final enum PAGE_LIKE_EDIT_CREATIVE:LX/8wL;

.field public static final enum PAGE_LIKE_EDIT_RUNNING_CREATIVE:LX/8wL;

.field public static final enum PROMOTE_CTA:LX/8wL;

.field public static final enum PROMOTE_CTA_EDIT_CREATIVE:LX/8wL;

.field public static final enum PROMOTE_PRODUCT:LX/8wL;

.field public static final enum PROMOTE_WEBSITE:LX/8wL;

.field public static final enum PROMOTE_WEBSITE_EDIT_CREATIVE:LX/8wL;


# instance fields
.field private mBoostedComponentApp:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/BoostedComponentMobileAppID;
    .end annotation
.end field

.field private final mEntryPoint:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 1422677
    new-instance v0, LX/8wL;

    const-string v1, "BOOST_POST"

    const-string v2, "BOOSTED_POST_MOBILE"

    const-string v3, "boosted_post"

    invoke-direct {v0, v1, v6, v2, v3}, LX/8wL;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/8wL;->BOOST_POST:LX/8wL;

    .line 1422678
    new-instance v0, LX/8wL;

    const-string v1, "BOOST_LIVE"

    const-string v2, "BOOSTED_POST_MOBILE"

    const-string v3, "boosted_post"

    invoke-direct {v0, v1, v7, v2, v3}, LX/8wL;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/8wL;->BOOST_LIVE:LX/8wL;

    .line 1422679
    new-instance v0, LX/8wL;

    const-string v1, "LOCAL_AWARENESS"

    const-string v2, "BOOSTED_LOCAL_AWARENESS_MOBILE"

    invoke-direct {v0, v1, v8, v2}, LX/8wL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wL;->LOCAL_AWARENESS:LX/8wL;

    .line 1422680
    new-instance v0, LX/8wL;

    const-string v1, "PROMOTE_WEBSITE"

    const-string v2, "BOOSTED_WEBSITE_MOBILE"

    const-string v3, "promoted_website"

    invoke-direct {v0, v1, v9, v2, v3}, LX/8wL;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/8wL;->PROMOTE_WEBSITE:LX/8wL;

    .line 1422681
    new-instance v0, LX/8wL;

    const-string v1, "PROMOTE_CTA"

    const/4 v2, 0x4

    const-string v3, "BOOSTED_CCTA_MOBILE"

    invoke-direct {v0, v1, v2, v3}, LX/8wL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wL;->PROMOTE_CTA:LX/8wL;

    .line 1422682
    new-instance v0, LX/8wL;

    const-string v1, "PAGE_LIKE"

    const/4 v2, 0x5

    const-string v3, "BOOSTED_PAGELIKE_MOBILE"

    const-string v4, "promoted_page"

    invoke-direct {v0, v1, v2, v3, v4}, LX/8wL;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/8wL;->PAGE_LIKE:LX/8wL;

    .line 1422683
    new-instance v0, LX/8wL;

    const-string v1, "PROMOTE_PRODUCT"

    const/4 v2, 0x6

    const-string v3, "BOOSTED_PRODUCT_MOBILE"

    invoke-direct {v0, v1, v2, v3}, LX/8wL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wL;->PROMOTE_PRODUCT:LX/8wL;

    .line 1422684
    new-instance v0, LX/8wL;

    const-string v1, "BOOST_EVENT"

    const/4 v2, 0x7

    const-string v3, "BOOSTED_EVENT_MOBILE"

    invoke-direct {v0, v1, v2, v3}, LX/8wL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wL;->BOOST_EVENT:LX/8wL;

    .line 1422685
    new-instance v0, LX/8wL;

    const-string v1, "BOOST_POST_INSIGHTS"

    const/16 v2, 0x8

    const-string v3, "BOOSTED_POST_MOBILE"

    invoke-direct {v0, v1, v2, v3}, LX/8wL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wL;->BOOST_POST_INSIGHTS:LX/8wL;

    .line 1422686
    new-instance v0, LX/8wL;

    const-string v1, "BOOST_POST_EDIT_TARGETING"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2, v5}, LX/8wL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wL;->BOOST_POST_EDIT_TARGETING:LX/8wL;

    .line 1422687
    new-instance v0, LX/8wL;

    const-string v1, "BOOST_EVENT_EDIT_TARGETING"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2, v5}, LX/8wL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wL;->BOOST_EVENT_EDIT_TARGETING:LX/8wL;

    .line 1422688
    new-instance v0, LX/8wL;

    const-string v1, "BOOST_EVENT_EDIT_CREATIVE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2, v5}, LX/8wL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wL;->BOOST_EVENT_EDIT_CREATIVE:LX/8wL;

    .line 1422689
    new-instance v0, LX/8wL;

    const-string v1, "BOOSTED_COMPONENT_EDIT_TARGETING"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v5}, LX/8wL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wL;->BOOSTED_COMPONENT_EDIT_TARGETING:LX/8wL;

    .line 1422690
    new-instance v0, LX/8wL;

    const-string v1, "BOOSTED_COMPONENT_EDIT_DURATION"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2, v5}, LX/8wL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wL;->BOOSTED_COMPONENT_EDIT_DURATION:LX/8wL;

    .line 1422691
    new-instance v0, LX/8wL;

    const-string v1, "BOOSTED_COMPONENT_EDIT_DURATION_BUDGET"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2, v5}, LX/8wL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wL;->BOOSTED_COMPONENT_EDIT_DURATION_BUDGET:LX/8wL;

    .line 1422692
    new-instance v0, LX/8wL;

    const-string v1, "BOOSTED_COMPONENT_EDIT_BUDGET"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2, v5}, LX/8wL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wL;->BOOSTED_COMPONENT_EDIT_BUDGET:LX/8wL;

    .line 1422693
    new-instance v0, LX/8wL;

    const-string v1, "PAGE_LIKE_EDIT_CREATIVE"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2, v5}, LX/8wL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wL;->PAGE_LIKE_EDIT_CREATIVE:LX/8wL;

    .line 1422694
    new-instance v0, LX/8wL;

    const-string v1, "PROMOTE_WEBSITE_EDIT_CREATIVE"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2, v5}, LX/8wL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wL;->PROMOTE_WEBSITE_EDIT_CREATIVE:LX/8wL;

    .line 1422695
    new-instance v0, LX/8wL;

    const-string v1, "PROMOTE_CTA_EDIT_CREATIVE"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2, v5}, LX/8wL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wL;->PROMOTE_CTA_EDIT_CREATIVE:LX/8wL;

    .line 1422696
    new-instance v0, LX/8wL;

    const-string v1, "LOCAL_AWARENESS_EDIT_CREATIVE"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2, v5}, LX/8wL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wL;->LOCAL_AWARENESS_EDIT_CREATIVE:LX/8wL;

    .line 1422697
    new-instance v0, LX/8wL;

    const-string v1, "PAGE_LIKE_EDIT_RUNNING_CREATIVE"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2, v5}, LX/8wL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wL;->PAGE_LIKE_EDIT_RUNNING_CREATIVE:LX/8wL;

    .line 1422698
    new-instance v0, LX/8wL;

    const-string v1, "AUDIENCE_MANAGEMENT"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2, v5}, LX/8wL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wL;->AUDIENCE_MANAGEMENT:LX/8wL;

    .line 1422699
    new-instance v0, LX/8wL;

    const-string v1, "BOOSTED_POST_ACTION_BUTTON"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2, v5}, LX/8wL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8wL;->BOOSTED_POST_ACTION_BUTTON:LX/8wL;

    .line 1422700
    new-instance v0, LX/8wL;

    const-string v1, "BOOSTED_COMPONENT_EDIT_PACING"

    const/16 v2, 0x17

    const-string v3, "BOOSTED_POST_MOBILE"

    const-string v4, "boosted_post"

    invoke-direct {v0, v1, v2, v3, v4}, LX/8wL;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/8wL;->BOOSTED_COMPONENT_EDIT_PACING:LX/8wL;

    .line 1422701
    const/16 v0, 0x18

    new-array v0, v0, [LX/8wL;

    sget-object v1, LX/8wL;->BOOST_POST:LX/8wL;

    aput-object v1, v0, v6

    sget-object v1, LX/8wL;->BOOST_LIVE:LX/8wL;

    aput-object v1, v0, v7

    sget-object v1, LX/8wL;->LOCAL_AWARENESS:LX/8wL;

    aput-object v1, v0, v8

    sget-object v1, LX/8wL;->PROMOTE_WEBSITE:LX/8wL;

    aput-object v1, v0, v9

    const/4 v1, 0x4

    sget-object v2, LX/8wL;->PROMOTE_CTA:LX/8wL;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LX/8wL;->PAGE_LIKE:LX/8wL;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/8wL;->PROMOTE_PRODUCT:LX/8wL;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/8wL;->BOOST_EVENT:LX/8wL;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/8wL;->BOOST_POST_INSIGHTS:LX/8wL;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/8wL;->BOOST_POST_EDIT_TARGETING:LX/8wL;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/8wL;->BOOST_EVENT_EDIT_TARGETING:LX/8wL;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/8wL;->BOOST_EVENT_EDIT_CREATIVE:LX/8wL;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/8wL;->BOOSTED_COMPONENT_EDIT_TARGETING:LX/8wL;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/8wL;->BOOSTED_COMPONENT_EDIT_DURATION:LX/8wL;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/8wL;->BOOSTED_COMPONENT_EDIT_DURATION_BUDGET:LX/8wL;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/8wL;->BOOSTED_COMPONENT_EDIT_BUDGET:LX/8wL;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/8wL;->PAGE_LIKE_EDIT_CREATIVE:LX/8wL;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/8wL;->PROMOTE_WEBSITE_EDIT_CREATIVE:LX/8wL;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/8wL;->PROMOTE_CTA_EDIT_CREATIVE:LX/8wL;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/8wL;->LOCAL_AWARENESS_EDIT_CREATIVE:LX/8wL;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/8wL;->PAGE_LIKE_EDIT_RUNNING_CREATIVE:LX/8wL;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/8wL;->AUDIENCE_MANAGEMENT:LX/8wL;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/8wL;->BOOSTED_POST_ACTION_BUTTON:LX/8wL;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/8wL;->BOOSTED_COMPONENT_EDIT_PACING:LX/8wL;

    aput-object v2, v0, v1

    sput-object v0, LX/8wL;->$VALUES:[LX/8wL;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/BoostedComponentMobileAppID;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1422671
    const-string v0, "unknown"

    invoke-direct {p0, p1, p2, p3, v0}, LX/8wL;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 1422672
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/BoostedComponentMobileAppID;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1422673
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1422674
    iput-object p3, p0, LX/8wL;->mBoostedComponentApp:Ljava/lang/String;

    .line 1422675
    iput-object p4, p0, LX/8wL;->mEntryPoint:Ljava/lang/String;

    .line 1422676
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8wL;
    .locals 1

    .prologue
    .line 1422670
    const-class v0, LX/8wL;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8wL;

    return-object v0
.end method

.method public static values()[LX/8wL;
    .locals 1

    .prologue
    .line 1422669
    sget-object v0, LX/8wL;->$VALUES:[LX/8wL;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8wL;

    return-object v0
.end method


# virtual methods
.method public final getComponentAppEnum()Ljava/lang/String;
    .locals 1
    .annotation build Lcom/facebook/graphql/calls/BoostedComponentMobileAppID;
    .end annotation

    .prologue
    .line 1422668
    iget-object v0, p0, LX/8wL;->mBoostedComponentApp:Ljava/lang/String;

    return-object v0
.end method

.method public final getEntryPoint()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1422667
    iget-object v0, p0, LX/8wL;->mEntryPoint:Ljava/lang/String;

    return-object v0
.end method
