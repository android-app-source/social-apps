.class public final LX/8rj;
.super LX/2Ip;
.source ""


# instance fields
.field public final synthetic a:LX/8rk;


# direct methods
.method public constructor <init>(LX/8rk;)V
    .locals 0

    .prologue
    .line 1409078
    iput-object p1, p0, LX/8rj;->a:LX/8rk;

    invoke-direct {p0}, LX/2Ip;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 4

    .prologue
    .line 1409067
    check-cast p1, LX/2f2;

    .line 1409068
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/8rj;->a:LX/8rk;

    iget-object v0, v0, LX/8rk;->k:LX/36N;

    if-eqz v0, :cond_0

    iget-wide v0, p1, LX/2f2;->a:J

    iget-object v2, p0, LX/8rj;->a:LX/8rk;

    iget-object v2, v2, LX/8rk;->k:LX/36N;

    invoke-interface {v2}, LX/36N;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 1409069
    :cond_0
    :goto_0
    return-void

    .line 1409070
    :cond_1
    iget-object v1, p0, LX/8rj;->a:LX/8rk;

    iget-object v2, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iget-object v0, p0, LX/8rj;->a:LX/8rk;

    iget-object v0, v0, LX/8rk;->k:LX/36N;

    invoke-interface {v0}, LX/36N;->b()Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/8rj;->a:LX/8rk;

    iget-object v0, v0, LX/8rk;->k:LX/36N;

    invoke-interface {v0}, LX/36N;->b()Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;->a()I

    move-result v0

    :goto_1
    iget-object v3, p0, LX/8rj;->a:LX/8rk;

    iget-object v3, v3, LX/8rk;->k:LX/36N;

    invoke-interface {v3}, LX/36N;->k()I

    move-result v3

    invoke-static {v1, v2, v0, v3}, LX/8rk;->a$redex0(LX/8rk;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;II)V

    .line 1409071
    iget-boolean v0, p1, LX/2f2;->c:Z

    if-nez v0, :cond_0

    .line 1409072
    iget-object v0, p0, LX/8rj;->a:LX/8rk;

    iget-object v0, v0, LX/8rk;->k:LX/36N;

    invoke-interface {v0}, LX/36N;->d()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    iget-object v1, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq v0, v1, :cond_0

    .line 1409073
    iget-object v0, p0, LX/8rj;->a:LX/8rk;

    iget-object v0, v0, LX/8rk;->k:LX/36N;

    invoke-static {v0}, Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel;->a(LX/36N;)Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel;

    move-result-object v0

    invoke-static {v0}, LX/8rm;->a(Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel;)LX/8rm;

    move-result-object v0

    iget-object v1, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1409074
    iput-object v1, v0, LX/8rm;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1409075
    move-object v0, v0

    .line 1409076
    invoke-virtual {v0}, LX/8rm;->a()Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel;

    goto :goto_0

    .line 1409077
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
