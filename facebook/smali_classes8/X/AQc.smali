.class public final LX/AQc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$PeopleEntitiesTypeAheadSuggestionsQueryModel;",
        ">;",
        "LX/0Px",
        "<",
        "Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$TypeAheadSuggestionFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventRelationshipInterstitialFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventRelationshipInterstitialFragment;)V
    .locals 0

    .prologue
    .line 1671516
    iput-object p1, p0, LX/AQc;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventRelationshipInterstitialFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1671517
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1671518
    if-eqz p1, :cond_0

    .line 1671519
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1671520
    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    .line 1671521
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1671522
    :goto_1
    return-object v0

    .line 1671523
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1671524
    check-cast v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$PeopleEntitiesTypeAheadSuggestionsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$PeopleEntitiesTypeAheadSuggestionsQueryModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 1671525
    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    .line 1671526
    :cond_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1671527
    check-cast v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$PeopleEntitiesTypeAheadSuggestionsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$PeopleEntitiesTypeAheadSuggestionsQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v3, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$TypeAheadSuggestionFieldsModel;

    invoke-virtual {v1, v0, v2, v3}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_1

    .line 1671528
    :cond_4
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1671529
    goto :goto_1
.end method
