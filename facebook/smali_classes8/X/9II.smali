.class public LX/9II;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/MountSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1462812
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1462813
    return-void
.end method

.method public static a(LX/0QB;)LX/9II;
    .locals 3

    .prologue
    .line 1462814
    const-class v1, LX/9II;

    monitor-enter v1

    .line 1462815
    :try_start_0
    sget-object v0, LX/9II;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1462816
    sput-object v2, LX/9II;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1462817
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1462818
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1462819
    new-instance v0, LX/9II;

    invoke-direct {v0}, LX/9II;-><init>()V

    .line 1462820
    move-object v0, v0

    .line 1462821
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1462822
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/9II;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1462823
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1462824
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
