.class public LX/92E;
.super LX/92A;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/92E;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    .line 1432132
    const-string v0, "minutiae_verb_picker_time_to_fetch_end"

    const-string v1, "minutiae_verb_picker_time_to_fetch_end_cached"

    const-string v2, "minutiae_verb_picker_time_to_verbs_shown"

    const-string v3, "minutiae_verb_picker_time_to_verbs_shown_cached"

    const-string v4, "minutiae_verb_picker_fetch_time"

    const-string v5, "minutiae_verb_picker_fetch_time_cached"

    const-string v6, "minutiae_verb_picker_tti"

    const-string v7, "minutiae_verb_picker_tti_cached"

    invoke-static/range {v0 .. v7}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    sput-object v0, LX/92E;->a:Ljava/util/Map;

    .line 1432133
    const v0, 0x430003

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const v1, 0x430007

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x43000b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x430008

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x430004

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const v5, 0x430009

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const v6, 0x430006

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const v7, 0x43000a

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static/range {v0 .. v7}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    sput-object v0, LX/92E;->b:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/performancelogger/PerformanceLogger;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1432134
    sget-object v0, LX/92E;->a:Ljava/util/Map;

    sget-object v1, LX/92E;->b:Ljava/util/Map;

    invoke-direct {p0, p1, v0, v1}, LX/92A;-><init>(Lcom/facebook/performancelogger/PerformanceLogger;Ljava/util/Map;Ljava/util/Map;)V

    .line 1432135
    return-void
.end method

.method public static a(LX/0QB;)LX/92E;
    .locals 4

    .prologue
    .line 1432136
    sget-object v0, LX/92E;->c:LX/92E;

    if-nez v0, :cond_1

    .line 1432137
    const-class v1, LX/92E;

    monitor-enter v1

    .line 1432138
    :try_start_0
    sget-object v0, LX/92E;->c:LX/92E;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1432139
    if-eqz v2, :cond_0

    .line 1432140
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1432141
    new-instance p0, LX/92E;

    invoke-static {v0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v3

    check-cast v3, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-direct {p0, v3}, LX/92E;-><init>(Lcom/facebook/performancelogger/PerformanceLogger;)V

    .line 1432142
    move-object v0, p0

    .line 1432143
    sput-object v0, LX/92E;->c:LX/92E;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1432144
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1432145
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1432146
    :cond_1
    sget-object v0, LX/92E;->c:LX/92E;

    return-object v0

    .line 1432147
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1432148
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
