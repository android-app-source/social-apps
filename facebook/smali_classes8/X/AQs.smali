.class public LX/AQs;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z


# direct methods
.method public constructor <init>(LX/15i;I)V
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "<init>"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1671920
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1671921
    iput-boolean v2, p0, LX/AQs;->c:Z

    .line 1671922
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/AQs;->a:Ljava/lang/String;

    .line 1671923
    invoke-virtual {p1, p2, v2}, LX/15i;->g(II)I

    move-result v0

    const-class v1, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;

    invoke-virtual {p1, v0, v2, v1}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/AQs;->b:LX/0Px;

    .line 1671924
    return-void

    .line 1671925
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1671926
    goto :goto_0
.end method
