.class public LX/8zh;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/8zZ;

.field private final b:LX/92X;

.field public final c:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

.field private final d:LX/92t;

.field public final e:LX/909;

.field public final f:LX/918;

.field public final g:LX/91I;

.field public final h:LX/92D;

.field public i:LX/5LG;

.field public j:LX/0ut;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:I

.field public n:Ljava/lang/String;

.field private final o:LX/905;

.field public p:Z

.field public q:Z

.field public r:Z


# direct methods
.method public constructor <init>(LX/8zZ;Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;LX/5LG;LX/905;LX/909;ILX/92Y;LX/92t;LX/918;LX/92D;)V
    .locals 3
    .param p1    # LX/8zZ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/5LG;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/905;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/909;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1427992
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1427993
    iput-boolean v0, p0, LX/8zh;->p:Z

    .line 1427994
    iput-boolean v0, p0, LX/8zh;->q:Z

    .line 1427995
    iput-boolean v0, p0, LX/8zh;->r:Z

    .line 1427996
    iput-object p3, p0, LX/8zh;->i:LX/5LG;

    .line 1427997
    iput-object p1, p0, LX/8zh;->a:LX/8zZ;

    .line 1427998
    iput-object p2, p0, LX/8zh;->c:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1427999
    iput-object p4, p0, LX/8zh;->o:LX/905;

    .line 1428000
    iput-object p5, p0, LX/8zh;->e:LX/909;

    .line 1428001
    iput p6, p0, LX/8zh;->m:I

    .line 1428002
    iput-object p8, p0, LX/8zh;->d:LX/92t;

    .line 1428003
    iput-object p9, p0, LX/8zh;->f:LX/918;

    .line 1428004
    iput-object p10, p0, LX/8zh;->h:LX/92D;

    .line 1428005
    iget-object v0, p0, LX/8zh;->c:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1428006
    iget-object v1, v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v0, v1

    .line 1428007
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8zh;->c:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1428008
    iget-object v1, v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v0, v1

    .line 1428009
    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v0

    .line 1428010
    :goto_0
    iget-object v1, p0, LX/8zh;->c:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1428011
    iget-object v2, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->i:Ljava/lang/String;

    move-object v1, v2

    .line 1428012
    if-eqz v1, :cond_1

    iget-object v1, p0, LX/8zh;->c:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1428013
    iget-object v2, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->i:Ljava/lang/String;

    move-object v1, v2

    .line 1428014
    :goto_1
    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p7, v2, v0, v1}, LX/92Y;->a(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)LX/92X;

    move-result-object v0

    iput-object v0, p0, LX/8zh;->b:LX/92X;

    .line 1428015
    new-instance v0, LX/91I;

    .line 1428016
    iget-object v1, p2, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v1, v1

    .line 1428017
    invoke-direct {v0, v1}, LX/91I;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/8zh;->g:LX/91I;

    .line 1428018
    return-void

    .line 1428019
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1428020
    :cond_1
    const-string v1, "composer"

    goto :goto_1
.end method

.method public static b$redex0(LX/8zh;Ljava/lang/String;)V
    .locals 4
    .param p0    # LX/8zh;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1427987
    iget-object v0, p0, LX/8zh;->h:LX/92D;

    invoke-virtual {v0}, LX/92D;->e()V

    .line 1427988
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1427989
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/8zh;->r:Z

    .line 1427990
    iget-object v1, p0, LX/8zh;->b:LX/92X;

    iget-object v2, p0, LX/8zh;->i:LX/5LG;

    new-instance v3, LX/8ze;

    invoke-direct {v3, p0, v0}, LX/8ze;-><init>(LX/8zh;Ljava/lang/String;)V

    invoke-virtual {v1, v2, p1, v0, v3}, LX/92X;->a(LX/5LG;Ljava/lang/String;Ljava/lang/String;LX/0Vd;)V

    .line 1427991
    return-void
.end method

.method public static c(LX/8zh;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 1427960
    iget-object v0, p0, LX/8zh;->h:LX/92D;

    invoke-virtual {v0}, LX/92D;->e()V

    .line 1427961
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1427962
    iget-object v0, p0, LX/8zh;->d:LX/92t;

    iget-object v2, p0, LX/8zh;->c:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    iget v4, p0, LX/8zh;->m:I

    new-instance v5, LX/8zg;

    invoke-direct {v5, p0, v3}, LX/8zg;-><init>(LX/8zh;Ljava/lang/String;)V

    move-object v1, p1

    .line 1427963
    invoke-static {v2}, LX/93D;->b(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    move-result-object v6

    .line 1427964
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->bx_()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {v6}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->bx_()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;->a()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_1

    .line 1427965
    :cond_0
    :goto_0
    return-void

    .line 1427966
    :cond_1
    new-instance v7, LX/92O;

    invoke-direct {v7}, LX/92O;-><init>()V

    invoke-virtual {v6}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->bx_()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;->a()Ljava/lang/String;

    move-result-object v6

    .line 1427967
    iput-object v6, v7, LX/92O;->a:Ljava/lang/String;

    .line 1427968
    move-object v6, v7

    .line 1427969
    iget-object v7, v2, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v7, v7

    .line 1427970
    invoke-virtual {v7}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v7

    .line 1427971
    iput-object v7, v6, LX/92O;->b:Ljava/lang/String;

    .line 1427972
    move-object v6, v6

    .line 1427973
    iput-object v1, v6, LX/92O;->c:Ljava/lang/String;

    .line 1427974
    move-object v6, v6

    .line 1427975
    iput v4, v6, LX/92O;->d:I

    .line 1427976
    move-object v6, v6

    .line 1427977
    iput-object v3, v6, LX/92O;->e:Ljava/lang/String;

    .line 1427978
    move-object v6, v6

    .line 1427979
    const-string v7, "place"

    .line 1427980
    iput-object v7, v6, LX/92O;->f:Ljava/lang/String;

    .line 1427981
    move-object v6, v6

    .line 1427982
    const/4 v7, 0x0

    .line 1427983
    iput-object v7, v6, LX/92O;->g:Landroid/location/Location;

    .line 1427984
    move-object v6, v6

    .line 1427985
    invoke-virtual {v6}, LX/92O;->a()LX/92P;

    move-result-object v6

    .line 1427986
    iget-object v7, v0, LX/92t;->f:LX/1Ck;

    sget-object p0, LX/92s;->SEARCH:LX/92s;

    new-instance p1, LX/92p;

    invoke-direct {p1, v0, v6}, LX/92p;-><init>(LX/92t;LX/92P;)V

    invoke-virtual {v7, p0, p1, v5}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_0
.end method

.method public static f(LX/8zh;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1427957
    iput-boolean v1, p0, LX/8zh;->q:Z

    .line 1427958
    iget-object v0, p0, LX/8zh;->o:LX/905;

    invoke-interface {v0, v1}, LX/905;->a(Z)V

    .line 1427959
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1428021
    iput-boolean v1, p0, LX/8zh;->q:Z

    .line 1428022
    iget-object v0, p0, LX/8zh;->o:LX/905;

    invoke-interface {v0, v1}, LX/905;->a(Z)V

    .line 1428023
    iget-object v0, p0, LX/8zh;->h:LX/92D;

    .line 1428024
    const v1, 0xc60009

    const-string p0, "minutiae_objects_selector_time_to_scroll_load"

    invoke-virtual {v0, v1, p0}, LX/92A;->c(ILjava/lang/String;)V

    .line 1428025
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;)Lcom/facebook/composer/minutiae/model/MinutiaeObject;
    .locals 3

    .prologue
    .line 1427943
    iget-object v0, p0, LX/8zh;->c:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    invoke-static {v0}, LX/93D;->a(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8zh;->c:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    invoke-static {v0}, LX/93D;->b(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->e()Ljava/lang/String;

    move-result-object v0

    .line 1427944
    :goto_0
    new-instance v1, LX/2s1;

    invoke-direct {v1}, LX/2s1;-><init>()V

    .line 1427945
    iget-object v2, p0, LX/8zh;->i:LX/5LG;

    move-object v2, v2

    .line 1427946
    iput-object v2, v1, LX/2s1;->a:LX/5LG;

    .line 1427947
    move-object v1, v1

    .line 1427948
    iput-object p1, v1, LX/2s1;->b:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    .line 1427949
    move-object v1, v1

    .line 1427950
    invoke-virtual {p1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->a()Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$AssociatedPlacesInfoModel;

    move-result-object v2

    .line 1427951
    iput-object v2, v1, LX/2s1;->g:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$AssociatedPlacesInfoModel;

    .line 1427952
    move-object v1, v1

    .line 1427953
    iput-object v0, v1, LX/2s1;->d:Ljava/lang/String;

    .line 1427954
    move-object v0, v1

    .line 1427955
    invoke-virtual {v0}, LX/2s1;->a()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v0

    return-object v0

    .line 1427956
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(I)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 1427924
    iget-boolean v0, p0, LX/8zh;->p:Z

    if-eqz v0, :cond_1

    .line 1427925
    :cond_0
    :goto_0
    return-void

    .line 1427926
    :cond_1
    iget-object v0, p0, LX/8zh;->a:LX/8zZ;

    invoke-virtual {v0}, LX/8zZ;->e()I

    move-result v1

    .line 1427927
    add-int/lit8 v0, v1, -0x1

    if-ne v0, p1, :cond_2

    move v6, v7

    .line 1427928
    :goto_1
    iget-boolean v0, p0, LX/8zh;->r:Z

    if-eqz v0, :cond_3

    if-eqz v6, :cond_3

    iget-boolean v0, p0, LX/8zh;->q:Z

    if-nez v0, :cond_3

    .line 1427929
    invoke-direct {p0}, LX/8zh;->g()V

    goto :goto_0

    .line 1427930
    :cond_2
    const/4 v0, 0x0

    move v6, v0

    goto :goto_1

    .line 1427931
    :cond_3
    iget-boolean v0, p0, LX/8zh;->r:Z

    if-eqz v0, :cond_4

    if-nez v6, :cond_4

    iget-boolean v0, p0, LX/8zh;->q:Z

    if-eqz v0, :cond_4

    .line 1427932
    invoke-static {p0}, LX/8zh;->f(LX/8zh;)V

    goto :goto_0

    .line 1427933
    :cond_4
    add-int/lit8 v0, v1, -0x5

    if-lt p1, v0, :cond_0

    iget-boolean v0, p0, LX/8zh;->r:Z

    if-nez v0, :cond_0

    .line 1427934
    iget-object v0, p0, LX/8zh;->j:LX/0ut;

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/8zh;->j:LX/0ut;

    invoke-interface {v0}, LX/0ut;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/8zh;->j:LX/0ut;

    invoke-interface {v0}, LX/0ut;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 1427935
    if-eqz v0, :cond_0

    .line 1427936
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1427937
    iget-object v0, p0, LX/8zh;->b:LX/92X;

    iget-object v1, p0, LX/8zh;->i:LX/5LG;

    iget-object v2, p0, LX/8zh;->l:Ljava/lang/String;

    iget-object v4, p0, LX/8zh;->j:LX/0ut;

    invoke-interface {v4}, LX/0ut;->a()Ljava/lang/String;

    move-result-object v4

    .line 1427938
    new-instance v5, LX/8zb;

    invoke-direct {v5, p0, p1}, LX/8zb;-><init>(LX/8zh;I)V

    move-object v5, v5

    .line 1427939
    invoke-virtual/range {v0 .. v5}, LX/92X;->a(LX/5LG;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Vd;)Z

    .line 1427940
    iput-boolean v7, p0, LX/8zh;->r:Z

    .line 1427941
    if-eqz v6, :cond_0

    iget-boolean v0, p0, LX/8zh;->q:Z

    if-nez v0, :cond_0

    .line 1427942
    invoke-direct {p0}, LX/8zh;->g()V

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1427910
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1427911
    const/4 v0, 0x0

    .line 1427912
    :goto_0
    iput-object v0, p0, LX/8zh;->l:Ljava/lang/String;

    .line 1427913
    if-eqz v0, :cond_0

    iget-object v1, p0, LX/8zh;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    if-nez v0, :cond_3

    .line 1427914
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/8zh;->q:Z

    .line 1427915
    iget-object v1, p0, LX/8zh;->c:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    invoke-static {v1}, LX/93D;->a(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1427916
    invoke-static {p0, v0}, LX/8zh;->c(LX/8zh;Ljava/lang/String;)V

    .line 1427917
    :goto_1
    iget-object v1, p0, LX/8zh;->k:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/8zh;->k:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1427918
    :cond_2
    iput-object v0, p0, LX/8zh;->k:Ljava/lang/String;

    .line 1427919
    iget-object v0, p0, LX/8zh;->g:LX/91I;

    invoke-virtual {v0}, LX/91I;->a()LX/91I;

    .line 1427920
    iget-object v0, p0, LX/8zh;->g:LX/91I;

    iget-object v1, p0, LX/8zh;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/91I;->a(Ljava/lang/String;)LX/91I;

    .line 1427921
    :cond_3
    return-void

    .line 1427922
    :cond_4
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1427923
    :cond_5
    invoke-static {p0, v0}, LX/8zh;->b$redex0(LX/8zh;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1427906
    iget-object v0, p0, LX/8zh;->l:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/8zh;->l:Ljava/lang/String;

    goto :goto_0
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 1427907
    iget-object v0, p0, LX/8zh;->f:LX/918;

    iget-object v1, p0, LX/8zh;->g:LX/91I;

    .line 1427908
    iget-object v2, v0, LX/918;->a:LX/0Zb;

    invoke-virtual {v1}, LX/91I;->b()Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v2, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1427909
    return-void
.end method
