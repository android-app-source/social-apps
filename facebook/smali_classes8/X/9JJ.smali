.class public LX/9JJ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/nio/ByteBuffer;


# direct methods
.method public constructor <init>(Ljava/nio/ByteBuffer;)V
    .locals 2

    .prologue
    .line 1464557
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1464558
    iput-object p1, p0, LX/9JJ;->a:Ljava/nio/ByteBuffer;

    .line 1464559
    iget-object v0, p0, LX/9JJ;->a:Ljava/nio/ByteBuffer;

    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 1464560
    return-void
.end method


# virtual methods
.method public final a()B
    .locals 1

    .prologue
    .line 1464561
    iget-object v0, p0, LX/9JJ;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1464562
    iget-object v0, p0, LX/9JJ;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    return v0
.end method

.method public final c()S
    .locals 1

    .prologue
    .line 1464563
    iget-object v0, p0, LX/9JJ;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v0

    return v0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 1464564
    iget-object v0, p0, LX/9JJ;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v0

    return-wide v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1464565
    invoke-virtual {p0}, LX/9JJ;->g()[B

    move-result-object v1

    .line 1464566
    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    goto :goto_0
.end method

.method public final g()[B
    .locals 4

    .prologue
    .line 1464567
    iget-object v0, p0, LX/9JJ;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v2

    .line 1464568
    if-nez v2, :cond_1

    .line 1464569
    const/4 v0, 0x0

    .line 1464570
    :cond_0
    return-object v0

    .line 1464571
    :cond_1
    new-array v0, v2, [B

    .line 1464572
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 1464573
    iget-object v3, p0, LX/9JJ;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->get()B

    move-result v3

    aput-byte v3, v0, v1

    .line 1464574
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 1464575
    iget-object v0, p0, LX/9JJ;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1464576
    return-void

    .line 1464577
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
