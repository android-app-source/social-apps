.class public LX/9hQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4VT;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/4VT",
        "<",
        "Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;",
        "LX/5kO;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;)V
    .locals 1

    .prologue
    .line 1526636
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1526637
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/9hQ;->a:Ljava/lang/String;

    .line 1526638
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;

    iput-object v0, p0, LX/9hQ;->b:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;

    .line 1526639
    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1526640
    iget-object v0, p0, LX/9hQ;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/16f;LX/40U;)V
    .locals 2

    .prologue
    .line 1526641
    check-cast p1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;

    check-cast p2, LX/5kO;

    .line 1526642
    iget-object v0, p0, LX/9hQ;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1526643
    :goto_0
    return-void

    .line 1526644
    :cond_0
    iget-object v0, p0, LX/9hQ;->b:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;

    invoke-virtual {p2, v0}, LX/5kO;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;)V

    .line 1526645
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, LX/5kO;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;)V

    goto :goto_0
.end method

.method public final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1526646
    const-class v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1526647
    const-string v0, "AcceptPlaceMetadataMutatingVisitor"

    return-object v0
.end method
