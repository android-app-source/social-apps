.class public final LX/93u;
.super LX/2h1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2h1",
        "<",
        "Lcom/facebook/privacy/model/PrivacyOptionsResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

.field public final synthetic b:Z

.field public final synthetic c:LX/93s;


# direct methods
.method public constructor <init>(LX/93s;Lcom/facebook/composer/privacy/model/ComposerPrivacyData;Z)V
    .locals 0

    .prologue
    .line 1434327
    iput-object p1, p0, LX/93u;->c:LX/93s;

    iput-object p2, p0, LX/93u;->a:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iput-boolean p3, p0, LX/93u;->b:Z

    invoke-direct {p0}, LX/2h1;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 1434311
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v0

    .line 1434312
    sget-object v1, LX/1nY;->API_ERROR:LX/1nY;

    if-ne v0, v1, :cond_0

    .line 1434313
    iget-object v0, p0, LX/93u;->c:LX/93s;

    .line 1434314
    iget-object v1, v0, LX/93Q;->b:LX/03V;

    move-object v0, v1

    .line 1434315
    const-string v1, "composer_privacy_fetch_cached_error"

    const-string v2, "Privacy options fetch failure"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1434316
    :cond_0
    iget-boolean v0, p0, LX/93u;->b:Z

    if-eqz v0, :cond_1

    .line 1434317
    iget-object v0, p0, LX/93u;->c:LX/93s;

    iget-object v1, p0, LX/93u;->a:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    invoke-static {v0, v1}, LX/93s;->b(LX/93s;Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    .line 1434318
    :cond_1
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1434319
    check-cast p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;

    .line 1434320
    iget-object v0, p0, LX/93u;->c:LX/93s;

    const/4 v1, 0x1

    .line 1434321
    iput-boolean v1, v0, LX/93s;->d:Z

    .line 1434322
    iget-object v0, p0, LX/93u;->c:LX/93s;

    iget-object v1, p0, LX/93u;->a:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    invoke-static {v0, v1, p1}, LX/93s;->b(LX/93s;Lcom/facebook/composer/privacy/model/ComposerPrivacyData;Lcom/facebook/privacy/model/PrivacyOptionsResult;)V

    .line 1434323
    iget-object v0, p0, LX/93u;->c:LX/93s;

    iget-object v0, v0, LX/93s;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/93w;

    invoke-interface {v0}, LX/93w;->a()V

    .line 1434324
    iget-boolean v0, p0, LX/93u;->b:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    iget-boolean v0, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->isResultFromServer:Z

    if-nez v0, :cond_1

    .line 1434325
    :cond_0
    iget-object v0, p0, LX/93u;->c:LX/93s;

    iget-object v1, p0, LX/93u;->a:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    invoke-static {v0, v1}, LX/93s;->b(LX/93s;Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    .line 1434326
    :cond_1
    return-void
.end method
