.class public LX/92d;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0zT;


# instance fields
.field private final c:LX/0w7;

.field public final d:LX/0gW;


# direct methods
.method public constructor <init>(LX/0gW;LX/0w7;)V
    .locals 0

    .prologue
    .line 1432459
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1432460
    iput-object p1, p0, LX/92d;->d:LX/0gW;

    .line 1432461
    iput-object p2, p0, LX/92d;->c:LX/0w7;

    .line 1432462
    return-void
.end method


# virtual methods
.method public final a(LX/0w5;LX/0t2;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0w5",
            "<*>;",
            "LX/0t2;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1432463
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 1432464
    iget-object v1, p0, LX/92d;->d:LX/0gW;

    instance-of v1, v1, LX/5LB;

    if-eqz v1, :cond_0

    .line 1432465
    const-string v1, "15"

    .line 1432466
    :goto_0
    move-object v1, v1

    .line 1432467
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1432468
    iget-object v1, p0, LX/92d;->d:LX/0gW;

    instance-of v1, v1, LX/5LB;

    if-eqz v1, :cond_2

    .line 1432469
    const-string v1, "7"

    .line 1432470
    :goto_1
    move-object v1, v1

    .line 1432471
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1432472
    iget-object v1, p0, LX/92d;->d:LX/0gW;

    iget-object v2, p0, LX/92d;->c:LX/0w7;

    invoke-virtual {p2, v1, p1, v2, v0}, LX/0t2;->a(LX/0gW;LX/0w5;LX/0w7;Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1432473
    :cond_0
    iget-object v1, p0, LX/92d;->d:LX/0gW;

    instance-of v1, v1, LX/5LC;

    if-eqz v1, :cond_1

    .line 1432474
    const-string v1, "15"

    goto :goto_0

    .line 1432475
    :cond_1
    const-string v1, "typeahead_session_id"

    goto :goto_0

    .line 1432476
    :cond_2
    iget-object v1, p0, LX/92d;->d:LX/0gW;

    instance-of v1, v1, LX/5LC;

    if-eqz v1, :cond_3

    .line 1432477
    const-string v1, "8"

    goto :goto_1

    .line 1432478
    :cond_3
    const-string v1, "request_id"

    goto :goto_1
.end method

.method public final a(LX/0zO;LX/0w5;LX/0t2;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zO;",
            "LX/0w5",
            "<*>;",
            "LX/0t2;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1432479
    invoke-virtual {p0, p2, p3}, LX/92d;->a(LX/0w5;LX/0t2;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
