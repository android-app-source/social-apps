.class public LX/AEv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile d:LX/AEv;


# instance fields
.field public final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/7gw;",
            "Lcom/facebook/audience/direct/data/BasicSeenStore$UpdateListener;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/AFD;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1647155
    const-class v0, LX/AEv;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AEv;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/AFE;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1647156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1647157
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/AEv;->b:Ljava/util/Map;

    .line 1647158
    const-string v0, "snacks_inbox_disk_cache_table"

    invoke-virtual {p1, v0}, LX/AFE;->a(Ljava/lang/String;)LX/AFD;

    move-result-object v0

    iput-object v0, p0, LX/AEv;->c:LX/AFD;

    .line 1647159
    return-void
.end method

.method public static a(LX/0QB;)LX/AEv;
    .locals 4

    .prologue
    .line 1647141
    sget-object v0, LX/AEv;->d:LX/AEv;

    if-nez v0, :cond_1

    .line 1647142
    const-class v1, LX/AEv;

    monitor-enter v1

    .line 1647143
    :try_start_0
    sget-object v0, LX/AEv;->d:LX/AEv;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1647144
    if-eqz v2, :cond_0

    .line 1647145
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1647146
    new-instance p0, LX/AEv;

    const-class v3, LX/AFE;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/AFE;

    invoke-direct {p0, v3}, LX/AEv;-><init>(LX/AFE;)V

    .line 1647147
    move-object v0, p0

    .line 1647148
    sput-object v0, LX/AEv;->d:LX/AEv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1647149
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1647150
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1647151
    :cond_1
    sget-object v0, LX/AEv;->d:LX/AEv;

    return-object v0

    .line 1647152
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1647153
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(Landroid/os/Parcel;)Lcom/facebook/audience/direct/model/CachedSeenModel;
    .locals 1

    .prologue
    .line 1647154
    sget-object v0, Lcom/facebook/audience/direct/model/CachedSeenModel;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/model/CachedSeenModel;

    return-object v0
.end method
