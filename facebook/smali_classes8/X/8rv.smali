.class public LX/8rv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8rg;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/8rg",
        "<",
        "Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;",
        ">;"
    }
.end annotation


# static fields
.field private static final c:Ljava/lang/Object;


# instance fields
.field public a:LX/5Ou;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:Landroid/content/res/Resources;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1409636
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/8rv;->c:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1409634
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1409635
    return-void
.end method

.method public static a(LX/0QB;)LX/8rv;
    .locals 8

    .prologue
    .line 1409603
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 1409604
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 1409605
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 1409606
    if-nez v1, :cond_0

    .line 1409607
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1409608
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 1409609
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 1409610
    sget-object v1, LX/8rv;->c:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1409611
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 1409612
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1409613
    :cond_1
    if-nez v1, :cond_4

    .line 1409614
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 1409615
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 1409616
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 1409617
    new-instance p0, LX/8rv;

    invoke-direct {p0}, LX/8rv;-><init>()V

    .line 1409618
    invoke-static {v0}, LX/5Ou;->a(LX/0QB;)LX/5Ou;

    move-result-object v1

    check-cast v1, LX/5Ou;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v7

    check-cast v7, Landroid/content/res/Resources;

    .line 1409619
    iput-object v1, p0, LX/8rv;->a:LX/5Ou;

    iput-object v7, p0, LX/8rv;->b:Landroid/content/res/Resources;

    .line 1409620
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1409621
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 1409622
    if-nez v1, :cond_2

    .line 1409623
    sget-object v0, LX/8rv;->c:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8rv;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1409624
    :goto_1
    if-eqz v0, :cond_3

    .line 1409625
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1409626
    :goto_3
    check-cast v0, LX/8rv;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1409627
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 1409628
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1409629
    :catchall_1
    move-exception v0

    .line 1409630
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1409631
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1409632
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 1409633
    :cond_2
    :try_start_8
    sget-object v0, LX/8rv;->c:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8rv;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static a(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 1409637
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1409638
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1409639
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1409640
    return-void
.end method

.method public static a(Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1409591
    invoke-virtual {p0}, Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1409592
    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    .line 1409593
    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    .line 1409594
    const/4 v3, 0x1

    invoke-virtual {v1, p1, v2, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 1409595
    iget v2, v2, Landroid/util/TypedValue;->resourceId:I

    sget-object v3, LX/03r;->SmartButtonLite:[I

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 1409596
    const/16 v2, 0x1

    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 1409597
    const/16 v3, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    .line 1409598
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 1409599
    if-eqz v2, :cond_0

    if-nez v3, :cond_1

    .line 1409600
    :cond_0
    :goto_0
    return-void

    .line 1409601
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {p0, v1}, LX/1r0;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1409602
    invoke-virtual {p0, v0, v3}, Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;->setTextAppearance(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method private a(Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;IIII)V
    .locals 2
    .param p2    # I
        .annotation build Landroid/support/annotation/AttrRes;
        .end annotation
    .end param
    .param p3    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p4    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p5    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 1409582
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageResource(I)V

    .line 1409583
    iget-object v0, p0, LX/8rv;->b:Landroid/content/res/Resources;

    invoke-virtual {v0, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    if-eqz p4, :cond_0

    iget-object v0, p0, LX/8rv;->b:Landroid/content/res/Resources;

    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v1, v0}, Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 1409584
    iget-object v0, p0, LX/8rv;->b:Landroid/content/res/Resources;

    invoke-virtual {v0, p5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1409585
    iget-object v0, p0, LX/8rv;->a:LX/5Ou;

    invoke-virtual {v0}, LX/5Ou;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1409586
    invoke-static {p1, p2}, LX/8rv;->a(Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;I)V

    .line 1409587
    :goto_1
    iget-object v0, p0, LX/8rv;->b:Landroid/content/res/Resources;

    const/high16 v1, 0x42a00000    # 80.0f

    invoke-static {v0, v1}, LX/0tP;->a(Landroid/content/res/Resources;F)I

    move-result v0

    invoke-static {p1, v0}, LX/8rv;->a(Landroid/view/View;I)V

    .line 1409588
    return-void

    .line 1409589
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1409590
    :cond_1
    invoke-static {p0, p1}, LX/8rv;->b(LX/8rv;Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;)V

    goto :goto_1
.end method

.method public static b(LX/8rv;Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;)V
    .locals 2

    .prologue
    .line 1409577
    iget-object v0, p0, LX/8rv;->b:Landroid/content/res/Resources;

    const v1, 0x7f0a00a4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;->setTextColor(I)V

    .line 1409578
    invoke-virtual {p1}, Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 1409579
    const/high16 v0, 0x41400000    # 12.0f

    invoke-virtual {p1, v0}, Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;->setTextSize(F)V

    .line 1409580
    iget-object v0, p0, LX/8rv;->b:Landroid/content/res/Resources;

    const v1, 0x7f0219cb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {p1, v0}, LX/1r0;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1409581
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;LX/8s1;)V
    .locals 7

    .prologue
    .line 1409549
    check-cast p1, Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;

    const/4 v6, 0x0

    .line 1409550
    sget-object v0, LX/8ru;->a:[I

    invoke-virtual {p2}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1409551
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;->setVisibility(I)V

    .line 1409552
    :goto_0
    return-void

    .line 1409553
    :pswitch_0
    const v2, 0x7f0101ef

    const v3, 0x7f080f7b

    const v4, 0x7f080f7c

    const v5, 0x7f080f9c

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, LX/8rv;->a(Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;IIII)V

    .line 1409554
    :goto_1
    invoke-virtual {p1, v6}, Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;->setVisibility(I)V

    .line 1409555
    invoke-virtual {p1, v6}, Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;->setFocusable(Z)V

    goto :goto_0

    .line 1409556
    :pswitch_1
    const v2, 0x7f0101ef

    const v3, 0x7f080f7b

    const v4, 0x7f080f7c

    const v5, 0x7f080f9f

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, LX/8rv;->a(Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;IIII)V

    goto :goto_1

    .line 1409557
    :pswitch_2
    const v2, 0x7f0101e3

    const v3, 0x7f080017

    const v5, 0x7f080f9e

    move-object v0, p0

    move-object v1, p1

    move v4, v6

    invoke-direct/range {v0 .. v5}, LX/8rv;->a(Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;IIII)V

    goto :goto_1

    .line 1409558
    :pswitch_3
    invoke-static {p3}, LX/8s1;->isCommentMentionSupported(LX/8s1;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1409559
    iget-object v0, p0, LX/8rv;->a:LX/5Ou;

    invoke-virtual {v0}, LX/5Ou;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f020742

    :goto_2
    move v0, v0

    .line 1409560
    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageResource(I)V

    .line 1409561
    iget-object v1, p0, LX/8rv;->b:Landroid/content/res/Resources;

    iget-object v0, p0, LX/8rv;->a:LX/5Ou;

    invoke-virtual {v0}, LX/5Ou;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0a00d5

    :goto_3
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setGlyphColor(I)V

    .line 1409562
    iget-object v0, p0, LX/8rv;->a:LX/5Ou;

    invoke-virtual {v0}, LX/5Ou;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1409563
    iget-object v0, p0, LX/8rv;->b:Landroid/content/res/Resources;

    const v1, 0x7f080fca

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1409564
    :goto_4
    move-object v0, v0

    .line 1409565
    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 1409566
    invoke-virtual {p1, v0}, Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1409567
    iget-object v0, p0, LX/8rv;->a:LX/5Ou;

    invoke-virtual {v0}, LX/5Ou;->d()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1409568
    invoke-static {p0, p1}, LX/8rv;->b(LX/8rv;Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;)V

    .line 1409569
    :goto_5
    iget-object v0, p0, LX/8rv;->b:Landroid/content/res/Resources;

    const/high16 v1, 0x42c80000    # 100.0f

    invoke-static {v0, v1}, LX/0tP;->a(Landroid/content/res/Resources;F)I

    move-result v0

    invoke-static {p1, v0}, LX/8rv;->a(Landroid/view/View;I)V

    .line 1409570
    goto/16 :goto_1

    .line 1409571
    :cond_0
    const v2, 0x7f0101e3

    const v3, 0x7f080f76

    const v5, 0x7f080f9d

    move-object v0, p0

    move-object v1, p1

    move v4, v6

    invoke-direct/range {v0 .. v5}, LX/8rv;->a(Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;IIII)V

    goto/16 :goto_1

    .line 1409572
    :cond_1
    const v0, 0x7f0a00a6

    goto :goto_3

    .line 1409573
    :cond_2
    const v0, 0x7f0101ef

    invoke-static {p1, v0}, LX/8rv;->a(Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;I)V

    goto :goto_5

    :cond_3
    const v0, 0x7f02080a

    goto :goto_2

    .line 1409574
    :cond_4
    iget-object v0, p0, LX/8rv;->a:LX/5Ou;

    invoke-virtual {v0}, LX/5Ou;->c()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1409575
    iget-object v0, p0, LX/8rv;->b:Landroid/content/res/Resources;

    const v1, 0x7f080fcb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 1409576
    :cond_5
    iget-object v0, p0, LX/8rv;->b:Landroid/content/res/Resources;

    const v1, 0x7f080fcc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
