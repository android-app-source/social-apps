.class public final LX/AQo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/AQu;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;)V
    .locals 0

    .prologue
    .line 1671829
    iput-object p1, p0, LX/AQo;->a:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1671830
    iget-object v0, p0, LX/AQo;->a:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;

    iget-object v0, v0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1a0025

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 1671831
    iget-object v0, p0, LX/AQo;->a:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;

    iget-object v0, v0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->i:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AQo;->a:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;

    .line 1671832
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mHost:LX/0k3;

    move-object v0, v1

    .line 1671833
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AQo;->a:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1671834
    iget-object v0, p0, LX/AQo;->a:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;

    iget-object v0, v0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->i:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget-object v1, p0, LX/AQo;->a:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;

    const v2, 0x7f08003c

    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/AQo;->a:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;

    iget-object v2, v2, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->o:LX/AQr;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    .line 1671835
    :cond_0
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1671836
    check-cast p1, LX/AQu;

    .line 1671837
    iget-object v0, p0, LX/AQo;->a:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;

    iget-object v0, v0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->i:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    if-eqz v0, :cond_0

    .line 1671838
    iget-object v0, p0, LX/AQo;->a:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;

    iget-object v0, v0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->i:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 1671839
    :cond_0
    if-nez p1, :cond_1

    .line 1671840
    iget-object v0, p0, LX/AQo;->a:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;

    iget-object v0, v0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1a0025

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 1671841
    :goto_0
    return-void

    .line 1671842
    :cond_1
    iget-object v0, p0, LX/AQo;->a:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;

    const/4 v1, 0x1

    .line 1671843
    iput-boolean v1, v0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->k:Z

    .line 1671844
    iget-object v0, p0, LX/AQo;->a:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;

    iget-object v0, v0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->j:LX/AQu;

    .line 1671845
    iget-object v1, p1, LX/AQu;->a:LX/0Px;

    iput-object v1, v0, LX/AQu;->a:LX/0Px;

    .line 1671846
    iget-object v1, p1, LX/AQu;->b:LX/0Px;

    iput-object v1, v0, LX/AQu;->b:LX/0Px;

    .line 1671847
    iget-object v1, p1, LX/AQu;->c:LX/0Px;

    iput-object v1, v0, LX/AQu;->c:LX/0Px;

    .line 1671848
    iget-object v1, p1, LX/AQu;->d:Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;

    iput-object v1, v0, LX/AQu;->d:Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;

    .line 1671849
    iget-object v0, p0, LX/AQo;->a:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;

    iget-object v0, v0, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->j:LX/AQu;

    invoke-virtual {v0}, LX/AQu;->getFilter()Landroid/widget/Filter;

    move-result-object v0

    iget-object v1, p0, LX/AQo;->a:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;

    iget-object v1, v1, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->g:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    iget-object v2, p0, LX/AQo;->a:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;

    iget-object v2, v2, Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;->p:Landroid/widget/Filter$FilterListener;

    invoke-virtual {v0, v1, v2}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterListener;)V

    goto :goto_0
.end method
