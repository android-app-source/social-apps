.class public final LX/9eH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/992;
.implements LX/993;
.implements LX/994;
.implements LX/995;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)V
    .locals 0

    .prologue
    .line 1519414
    iput-object p1, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1519406
    iget-object v0, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    invoke-virtual {v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->j()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1519407
    iget-object v0, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v0, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    invoke-static {v0}, LX/9eK;->isSwiping(LX/9eK;)Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onFling invalid state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v2, v2, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1519408
    iget-object v0, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v0, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    sget-object v1, LX/9eK;->SWIPING_FRAME:LX/9eK;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    invoke-static {v0, v3, v3}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->a$redex0(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;[Landroid/graphics/drawable/Drawable;[LX/9hP;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1519409
    :cond_0
    :goto_0
    return-void

    .line 1519410
    :cond_1
    iget-object v0, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->c$redex0(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;Z)V

    .line 1519411
    iget-object v0, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v1, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v1, v1, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->E:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getAlpha()F

    move-result v1

    .line 1519412
    invoke-static {v0, v1}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->a$redex0(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;F)V

    .line 1519413
    iget-object v0, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v0, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->G:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    invoke-virtual {v0}, LX/998;->g()Z

    goto :goto_0
.end method

.method public final a(FF)V
    .locals 3

    .prologue
    .line 1519399
    iget-object v0, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    invoke-virtual {v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->j()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1519400
    iget-object v0, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v0, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    invoke-static {v0}, LX/9eK;->isSwiping(LX/9eK;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v0, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    sget-object v1, LX/9eK;->NORMAL:LX/9eK;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v0, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    sget-object v1, LX/9eK;->ANIMATE_OUT:LX/9eK;

    if-ne v0, v1, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onViewAnimating invalid state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v2, v2, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1519401
    iget-object v0, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v0, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    sget-object v1, LX/9eK;->SWIPING_IMAGE:LX/9eK;

    if-ne v0, v1, :cond_1

    .line 1519402
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 1519403
    iget-object v1, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v1, v1, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->E:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 1519404
    :cond_1
    return-void

    .line 1519405
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 6

    .prologue
    .line 1519356
    iget-object v0, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    invoke-virtual {v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->j()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1519357
    iget-object v0, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v0, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    sget-object v1, LX/9eK;->NORMAL:LX/9eK;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v0, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    invoke-static {v0}, LX/9eK;->isSwiping(LX/9eK;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onViewDismissStart invalid state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v2, v2, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1519358
    iget-object v0, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v0, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    invoke-static {v0}, LX/9eK;->isSwiping(LX/9eK;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1519359
    :goto_1
    return-void

    .line 1519360
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1519361
    :cond_2
    iget-object v0, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1519362
    iget-object v1, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    sget-object v4, LX/9eK;->NORMAL:LX/9eK;

    if-ne v1, v4, :cond_3

    move v1, v2

    :goto_2
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1519363
    new-array v1, v2, [Landroid/graphics/drawable/Drawable;

    .line 1519364
    const/4 v4, 0x0

    invoke-static {v0, v1, v4}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->a$redex0(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;[Landroid/graphics/drawable/Drawable;[LX/9hP;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 1519365
    iget-object v1, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->H:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-nez v1, :cond_4

    :goto_3
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 1519366
    iget-object v1, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->H:Landroid/widget/FrameLayout;

    iget-object v2, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->E:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1519367
    iget-object v1, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->E:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 1519368
    sget-object v1, LX/9eK;->SWIPING_FRAME:LX/9eK;

    iput-object v1, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    .line 1519369
    :goto_4
    goto :goto_1

    :cond_3
    move v1, v3

    .line 1519370
    goto :goto_2

    :cond_4
    move v2, v3

    .line 1519371
    goto :goto_3

    .line 1519372
    :cond_5
    aget-object v2, v1, v3

    new-instance v4, Landroid/graphics/Rect;

    iget-object v5, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->F:LX/9eQ;

    invoke-virtual {v5}, LX/9eQ;->getMeasuredWidth()I

    move-result v5

    iget-object p0, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->F:LX/9eQ;

    invoke-virtual {p0}, LX/9eQ;->getMeasuredHeight()I

    move-result p0

    invoke-direct {v4, v3, v3, v5, p0}, Landroid/graphics/Rect;-><init>(IIII)V

    sget-object v5, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-static {v2, v4, v5}, LX/9hP;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/Rect;Landroid/widget/ImageView$ScaleType;)LX/9hP;

    move-result-object v2

    .line 1519373
    iget-object v4, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->F:LX/9eQ;

    aget-object v1, v1, v3

    .line 1519374
    invoke-virtual {v4, v1}, LX/9eQ;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1519375
    invoke-virtual {v4, v2, v2}, LX/9eQ;->a(LX/9hP;LX/9hP;)V

    .line 1519376
    invoke-virtual {v4, v2}, LX/9eQ;->a(LX/9hP;)LX/9hP;

    .line 1519377
    iget-object v1, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->u:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0gh;

    const-string v2, "swipe"

    invoke-virtual {v1, v2}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 1519378
    iget-object v1, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->D:Landroid/widget/FrameLayout;

    iget-object v2, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->F:LX/9eQ;

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 1519379
    iget-object v1, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->G:Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;

    iget-object v2, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->F:LX/9eQ;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/fbui/draggable/widget/DismissibleFrameLayout;->addView(Landroid/view/View;I)V

    .line 1519380
    iget-object v1, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->F:LX/9eQ;

    invoke-virtual {v1, v3}, LX/9eQ;->setVisibility(I)V

    .line 1519381
    iget-object v1, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->R:LX/8Hs;

    invoke-virtual {v1}, LX/8Hs;->d()V

    .line 1519382
    sget-object v1, LX/9eK;->SWIPING_IMAGE:LX/9eK;

    iput-object v1, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    goto :goto_4
.end method

.method public final c()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1519389
    iget-object v0, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    invoke-virtual {v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->j()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1519390
    iget-object v0, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v0, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    sget-object v1, LX/9eK;->ANIMATE_OUT:LX/9eK;

    if-ne v0, v1, :cond_1

    .line 1519391
    iget-object v0, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v0, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->C:Ljava/lang/Throwable;

    if-eqz v0, :cond_0

    .line 1519392
    iget-object v0, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v0, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->z:Ljava/lang/String;

    const-string v2, "onViewDismissed unexpected. Last call:"

    iget-object v3, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v3, v3, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->C:Ljava/lang/Throwable;

    invoke-virtual {v0, v1, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1519393
    :goto_0
    return-void

    .line 1519394
    :cond_0
    iget-object v0, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v0, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->z:Ljava/lang/String;

    const-string v2, "onViewDismissed _really_ unexpected."

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1519395
    :cond_1
    iget-object v0, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v0, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    invoke-static {v0}, LX/9eK;->isSwiping(LX/9eK;)Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onViewDismissed invalid state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v2, v2, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1519396
    iget-object v0, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    invoke-static {v0, v3}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->c$redex0(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;Z)V

    .line 1519397
    iget-object v0, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v0, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->K:Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;

    iget-object v1, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v1, v1, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->v:LX/1L1;

    iget-object v2, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    invoke-static {v2}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->x(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)I

    move-result v2

    invoke-virtual {v0, v1, v3, v2}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->a(LX/1L1;ZI)V

    .line 1519398
    iget-object v0, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    goto :goto_0
.end method

.method public final d()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1519383
    iget-object v0, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    invoke-virtual {v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->j()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1519384
    iget-object v0, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v0, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    invoke-static {v0}, LX/9eK;->isSwiping(LX/9eK;)Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onViewReset invalid state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v2, v2, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1519385
    iget-object v0, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v0, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->K:Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;

    iget-object v1, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v1, v1, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->v:LX/1L1;

    iget-object v2, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    invoke-static {v2}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->x(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)I

    move-result v2

    invoke-virtual {v0, v1, v3, v2}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->a(LX/1L1;ZI)V

    .line 1519386
    iget-object v0, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v0, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->E:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 1519387
    iget-object v0, p0, LX/9eH;->a:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    invoke-static {v0, v3}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->c$redex0(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;Z)V

    .line 1519388
    return-void
.end method
