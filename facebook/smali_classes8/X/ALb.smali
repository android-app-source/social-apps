.class public LX/ALb;
.super Landroid/widget/TextView;
.source ""


# instance fields
.field public final a:LX/8YL;

.field public final b:LX/8YL;

.field public final c:LX/8YK;

.field private final d:LX/AM7;

.field public e:LX/AM3;

.field public f:LX/AM8;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1665034
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/ALb;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1665035
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1665043
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/ALb;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1665044
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8

    .prologue
    .line 1665045
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1665046
    sget-object v0, LX/AM3;->NONE:LX/AM3;

    iput-object v0, p0, LX/ALb;->e:LX/AM3;

    .line 1665047
    new-instance v0, LX/AMC;

    const-wide/high16 v2, 0x4034000000000000L    # 20.0

    const-wide/16 v4, 0x0

    invoke-direct {v0, v2, v3, v4, v5}, LX/AMC;-><init>(DD)V

    .line 1665048
    iget-wide v6, v0, LX/AMC;->a:D

    move-wide v2, v6

    .line 1665049
    iget-wide v6, v0, LX/AMC;->b:D

    move-wide v0, v6

    .line 1665050
    invoke-static {v2, v3, v0, v1}, LX/8YL;->a(DD)LX/8YL;

    move-result-object v0

    iput-object v0, p0, LX/ALb;->a:LX/8YL;

    .line 1665051
    new-instance v0, LX/AMC;

    const-wide/high16 v2, 0x4014000000000000L    # 5.0

    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    invoke-direct {v0, v2, v3, v4, v5}, LX/AMC;-><init>(DD)V

    .line 1665052
    iget-wide v6, v0, LX/AMC;->a:D

    move-wide v2, v6

    .line 1665053
    iget-wide v6, v0, LX/AMC;->b:D

    move-wide v0, v6

    .line 1665054
    invoke-static {v2, v3, v0, v1}, LX/8YL;->a(DD)LX/8YL;

    move-result-object v0

    iput-object v0, p0, LX/ALb;->b:LX/8YL;

    .line 1665055
    invoke-static {}, LX/8YM;->b()LX/8YM;

    move-result-object v0

    invoke-virtual {v0}, LX/8YH;->a()LX/8YK;

    move-result-object v0

    new-instance v1, LX/AM4;

    invoke-direct {v1, p0}, LX/AM4;-><init>(LX/ALb;)V

    invoke-virtual {v0, v1}, LX/8YK;->a(LX/7hQ;)LX/8YK;

    move-result-object v0

    iget-object v1, p0, LX/ALb;->a:LX/8YL;

    invoke-virtual {v0, v1}, LX/8YK;->a(LX/8YL;)LX/8YK;

    move-result-object v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/8YK;->a(D)LX/8YK;

    move-result-object v0

    iput-object v0, p0, LX/ALb;->c:LX/8YK;

    .line 1665056
    new-instance v0, LX/AM7;

    invoke-virtual {p0}, LX/ALb;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/AM7;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v0, p0, LX/ALb;->d:LX/AM7;

    .line 1665057
    iget-object v0, p0, LX/ALb;->d:LX/AM7;

    new-instance v1, LX/AM2;

    invoke-direct {v1, p0}, LX/AM2;-><init>(LX/ALb;)V

    .line 1665058
    iput-object v1, v0, LX/AM7;->c:LX/AM2;

    .line 1665059
    return-void
.end method


# virtual methods
.method public setEnabled(Z)V
    .locals 1

    .prologue
    .line 1665039
    if-eqz p1, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    invoke-virtual {p0, v0}, LX/ALb;->setAlpha(F)V

    .line 1665040
    invoke-super {p0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 1665041
    return-void

    .line 1665042
    :cond_0
    const/high16 v0, 0x3f000000    # 0.5f

    goto :goto_0
.end method

.method public setLongPressEnabled(Z)V
    .locals 1

    .prologue
    .line 1665036
    iget-object v0, p0, LX/ALb;->d:LX/AM7;

    .line 1665037
    iget-object p0, v0, LX/AM7;->a:Landroid/view/GestureDetector;

    invoke-virtual {p0, p1}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    .line 1665038
    return-void
.end method
