.class public final LX/95m;
.super Ljava/util/concurrent/locks/ReentrantLock;
.source ""


# instance fields
.field public final synthetic this$0:LX/95n;


# direct methods
.method public constructor <init>(LX/95n;)V
    .locals 0

    .prologue
    .line 1437555
    iput-object p1, p0, LX/95m;->this$0:LX/95n;

    invoke-direct {p0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    return-void
.end method


# virtual methods
.method public final unlock()V
    .locals 2
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    .line 1437556
    invoke-virtual {p0}, LX/95m;->getHoldCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/95m;->this$0:LX/95n;

    iget-object v0, v0, LX/95n;->t:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1437557
    :try_start_0
    iget-object v0, p0, LX/95m;->this$0:LX/95n;

    iget-object v1, v0, LX/95n;->k:LX/2k1;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1437558
    :try_start_1
    iget-object v0, p0, LX/95m;->this$0:LX/95n;

    invoke-static {v0}, LX/95n;->k(LX/95n;)V

    .line 1437559
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1437560
    invoke-super {p0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 1437561
    :goto_0
    return-void

    .line 1437562
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1437563
    :catchall_1
    move-exception v0

    invoke-super {p0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    .line 1437564
    :cond_0
    invoke-super {p0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0
.end method
