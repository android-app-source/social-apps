.class public LX/9kI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static a:J
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static i:LX/0Xm;


# instance fields
.field public final c:LX/11H;

.field public final d:LX/9kJ;

.field private final e:LX/0SG;

.field public final f:LX/0lC;

.field private final g:Landroid/content/Context;

.field private final h:Ljava/util/Locale;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1530992
    const-class v0, LX/9kI;

    sput-object v0, LX/9kI;->b:Ljava/lang/Class;

    .line 1530993
    const-wide/16 v0, 0x0

    sput-wide v0, LX/9kI;->a:J

    return-void
.end method

.method public constructor <init>(LX/11H;LX/9kJ;LX/0SG;LX/0lC;Ljava/util/Locale;Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1530994
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1530995
    iput-object p1, p0, LX/9kI;->c:LX/11H;

    .line 1530996
    iput-object p2, p0, LX/9kI;->d:LX/9kJ;

    .line 1530997
    iput-object p3, p0, LX/9kI;->e:LX/0SG;

    .line 1530998
    iput-object p4, p0, LX/9kI;->f:LX/0lC;

    .line 1530999
    iput-object p5, p0, LX/9kI;->h:Ljava/util/Locale;

    .line 1531000
    iput-object p6, p0, LX/9kI;->g:Landroid/content/Context;

    .line 1531001
    return-void
.end method

.method public static a(LX/0QB;)LX/9kI;
    .locals 10

    .prologue
    .line 1531002
    const-class v1, LX/9kI;

    monitor-enter v1

    .line 1531003
    :try_start_0
    sget-object v0, LX/9kI;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1531004
    sput-object v2, LX/9kI;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1531005
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1531006
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1531007
    new-instance v3, LX/9kI;

    invoke-static {v0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v4

    check-cast v4, LX/11H;

    .line 1531008
    new-instance v7, LX/9kJ;

    invoke-static {v0}, LX/0eD;->b(LX/0QB;)Ljava/util/Locale;

    move-result-object v5

    check-cast v5, Ljava/util/Locale;

    invoke-static {v0}, LX/0q9;->a(LX/0QB;)LX/0lp;

    move-result-object v6

    check-cast v6, LX/0lp;

    invoke-direct {v7, v5, v6}, LX/9kJ;-><init>(Ljava/util/Locale;LX/0lp;)V

    .line 1531009
    move-object v5, v7

    .line 1531010
    check-cast v5, LX/9kJ;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v7

    check-cast v7, LX/0lC;

    invoke-static {v0}, LX/0eD;->b(LX/0QB;)Ljava/util/Locale;

    move-result-object v8

    check-cast v8, Ljava/util/Locale;

    const-class v9, Landroid/content/Context;

    invoke-interface {v0, v9}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/Context;

    invoke-direct/range {v3 .. v9}, LX/9kI;-><init>(LX/11H;LX/9kJ;LX/0SG;LX/0lC;Ljava/util/Locale;Landroid/content/Context;)V

    .line 1531011
    move-object v0, v3

    .line 1531012
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1531013
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/9kI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1531014
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1531015
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(LX/9kI;)Ljava/io/File;
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1531016
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/9kI;->g:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    const-string v2, "page_topics.json"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 8

    .prologue
    .line 1531017
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 1531018
    const-string v1, "FetchPageTopics"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1531019
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "unknown type"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1531020
    :cond_0
    const-class v2, LX/9kI;

    monitor-enter v2

    .line 1531021
    :try_start_0
    const/4 v1, 0x0

    .line 1531022
    invoke-static {p0}, LX/9kI;->b(LX/9kI;)Ljava/io/File;

    move-result-object v0

    .line 1531023
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_6

    move-object v0, v1

    .line 1531024
    :goto_0
    move-object v3, v0

    .line 1531025
    iget-object v0, p0, LX/9kI;->e:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 1531026
    if-eqz v3, :cond_1

    iget-object v4, p0, LX/9kI;->h:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Lcom/facebook/places/pagetopics/FetchPageTopicsResult;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1531027
    sget-wide v4, LX/9kI;->a:J

    sub-long v4, v0, v4

    const-wide/32 v6, 0x36ee80

    cmp-long v4, v4, v6

    if-gez v4, :cond_1

    .line 1531028
    invoke-static {v3}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    monitor-exit v2

    .line 1531029
    :goto_1
    return-object v0

    .line 1531030
    :cond_1
    sput-wide v0, LX/9kI;->a:J

    .line 1531031
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Lcom/facebook/places/pagetopics/FetchPageTopicsResult;->b()Lcom/facebook/places/pagetopics/FetchPageTopicsResult$PageTopicsGetResponseSummary;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/places/pagetopics/FetchPageTopicsResult$PageTopicsGetResponseSummary;->a()J

    move-result-wide v0

    :goto_2
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1531032
    :try_start_1
    iget-object v1, p0, LX/9kI;->c:LX/11H;

    iget-object v4, p0, LX/9kI;->d:LX/9kJ;

    invoke-virtual {v1, v4, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/places/pagetopics/FetchPageTopicsResult;

    move-object v0, v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1531033
    :try_start_2
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1531034
    invoke-virtual {v0}, Lcom/facebook/places/pagetopics/FetchPageTopicsResult;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/places/pagetopics/FetchPageTopicsResult;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-nez v1, :cond_5

    .line 1531035
    :cond_2
    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1531036
    invoke-static {v3}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    monitor-exit v2

    goto :goto_1

    .line 1531037
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1531038
    :cond_3
    const-wide/16 v0, -0x1

    goto :goto_2

    .line 1531039
    :catch_0
    move-exception v0

    .line 1531040
    if-eqz v3, :cond_4

    .line 1531041
    :try_start_3
    sget-object v1, LX/9kI;->b:Ljava/lang/Class;

    const-string v4, "Exception happened while gettings page topics, using cached result"

    invoke-static {v1, v4, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1531042
    invoke-static {v3}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    monitor-exit v2

    goto :goto_1

    .line 1531043
    :cond_4
    throw v0

    .line 1531044
    :cond_5
    invoke-static {p0}, LX/9kI;->b(LX/9kI;)Ljava/io/File;

    move-result-object v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1531045
    :try_start_4
    iget-object v3, p0, LX/9kI;->f:LX/0lC;

    invoke-virtual {v3, v1, v0}, LX/0lC;->a(Ljava/io/File;Ljava/lang/Object;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1531046
    :goto_3
    :try_start_5
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    .line 1531047
    :cond_6
    :try_start_6
    iget-object v3, p0, LX/9kI;->f:LX/0lC;

    const-class v4, Lcom/facebook/places/pagetopics/FetchPageTopicsResult;

    invoke-virtual {v3, v0, v4}, LX/0lC;->a(Ljava/io/File;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/pagetopics/FetchPageTopicsResult;
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    goto/16 :goto_0

    .line 1531048
    :catch_1
    move-exception v0

    .line 1531049
    sget-object v3, LX/9kI;->b:Ljava/lang/Class;

    const-string v4, "Error reading cache"

    invoke-static {v3, v4, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 1531050
    goto/16 :goto_0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 1531051
    :catch_2
    move-exception v1

    .line 1531052
    sget-object v3, LX/9kI;->b:Ljava/lang/Class;

    const-string v4, "Error writing cache"

    invoke-static {v3, v4, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3
.end method
