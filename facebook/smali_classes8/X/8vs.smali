.class public LX/8vs;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static f:F

.field private static g:F


# instance fields
.field public a:I

.field public final b:LX/8vr;

.field public final c:LX/8vr;

.field public d:Landroid/view/animation/Interpolator;

.field private final e:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 1421441
    const/high16 v0, 0x41000000    # 8.0f

    sput v0, LX/8vs;->f:F

    .line 1421442
    sput v1, LX/8vs;->g:F

    .line 1421443
    invoke-static {v1}, LX/8vs;->b(F)F

    move-result v0

    div-float v0, v1, v0

    sput v0, LX/8vs;->g:F

    .line 1421444
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1421439
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/8vs;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    .line 1421440
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V
    .locals 1

    .prologue
    .line 1421437
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, LX/8vs;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;Z)V

    .line 1421438
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/view/animation/Interpolator;Z)V
    .locals 1

    .prologue
    .line 1421431
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1421432
    iput-object p2, p0, LX/8vs;->d:Landroid/view/animation/Interpolator;

    .line 1421433
    iput-boolean p3, p0, LX/8vs;->e:Z

    .line 1421434
    new-instance v0, LX/8vr;

    invoke-direct {v0, p1}, LX/8vr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/8vs;->b:LX/8vr;

    .line 1421435
    new-instance v0, LX/8vr;

    invoke-direct {v0, p1}, LX/8vr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/8vs;->c:LX/8vr;

    .line 1421436
    return-void
.end method

.method private static b(F)F
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 1421380
    sget v0, LX/8vs;->f:F

    mul-float/2addr v0, p0

    .line 1421381
    cmpg-float v1, v0, v4

    if-gez v1, :cond_0

    .line 1421382
    neg-float v1, v0

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->exp(D)D

    move-result-wide v2

    double-to-float v1, v2

    sub-float v1, v4, v1

    sub-float/2addr v0, v1

    .line 1421383
    :goto_0
    sget v1, LX/8vs;->g:F

    mul-float/2addr v0, v1

    .line 1421384
    return v0

    .line 1421385
    :cond_0
    sub-float v0, v4, v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    double-to-float v0, v0

    sub-float v0, v4, v0

    .line 1421386
    const v1, 0x3ebc5ab2

    const v2, 0x3f21d2a7

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(IIIIIIII)V
    .locals 11

    .prologue
    .line 1421429
    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    invoke-virtual/range {v0 .. v10}, LX/8vs;->a(IIIIIIIIII)V

    .line 1421430
    return-void
.end method

.method public final a(IIIIIIIIII)V
    .locals 6

    .prologue
    .line 1421419
    iget-boolean v0, p0, LX/8vs;->e:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/8vs;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1421420
    iget-object v0, p0, LX/8vs;->b:LX/8vr;

    iget v0, v0, LX/8vr;->e:F

    .line 1421421
    iget-object v1, p0, LX/8vs;->c:LX/8vr;

    iget v1, v1, LX/8vr;->e:F

    .line 1421422
    int-to-float v2, p3

    invoke-static {v2}, Ljava/lang/Math;->signum(F)F

    move-result v2

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v3

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    int-to-float v2, p4

    invoke-static {v2}, Ljava/lang/Math;->signum(F)F

    move-result v2

    invoke-static {v1}, Ljava/lang/Math;->signum(F)F

    move-result v3

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    .line 1421423
    int-to-float v2, p3

    add-float/2addr v0, v2

    float-to-int p3, v0

    .line 1421424
    int-to-float v0, p4

    add-float/2addr v0, v1

    float-to-int p4, v0

    move v2, p3

    .line 1421425
    :goto_0
    const/4 v0, 0x1

    iput v0, p0, LX/8vs;->a:I

    .line 1421426
    iget-object v0, p0, LX/8vs;->b:LX/8vr;

    move v1, p1

    move v3, p5

    move v4, p6

    move v5, p9

    invoke-virtual/range {v0 .. v5}, LX/8vr;->a(IIIII)V

    .line 1421427
    iget-object v0, p0, LX/8vs;->c:LX/8vr;

    move v1, p2

    move v2, p4

    move v3, p7

    move v4, p8

    move/from16 v5, p10

    invoke-virtual/range {v0 .. v5}, LX/8vr;->a(IIIII)V

    .line 1421428
    return-void

    :cond_0
    move v2, p3

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1421418
    iget-object v0, p0, LX/8vs;->b:LX/8vr;

    iget-boolean v0, v0, LX/8vr;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8vs;->c:LX/8vr;

    iget-boolean v0, v0, LX/8vr;->k:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1421417
    iget-object v0, p0, LX/8vs;->b:LX/8vr;

    iget v0, v0, LX/8vr;->b:I

    return v0
.end method

.method public final c()F
    .locals 3

    .prologue
    .line 1421414
    iget-object v0, p0, LX/8vs;->b:LX/8vr;

    iget v0, v0, LX/8vr;->e:F

    iget-object v1, p0, LX/8vs;->b:LX/8vr;

    iget v1, v1, LX/8vr;->e:F

    mul-float/2addr v0, v1

    .line 1421415
    iget-object v1, p0, LX/8vs;->c:LX/8vr;

    iget v1, v1, LX/8vr;->e:F

    iget-object v2, p0, LX/8vs;->c:LX/8vr;

    iget v2, v2, LX/8vr;->e:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    .line 1421416
    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public final d()Z
    .locals 6

    .prologue
    .line 1421390
    invoke-virtual {p0}, LX/8vs;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1421391
    const/4 v0, 0x0

    .line 1421392
    :goto_0
    return v0

    .line 1421393
    :cond_0
    iget v0, p0, LX/8vs;->a:I

    packed-switch v0, :pswitch_data_0

    .line 1421394
    :cond_1
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 1421395
    :pswitch_0
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    .line 1421396
    iget-object v2, p0, LX/8vs;->b:LX/8vr;

    iget-wide v2, v2, LX/8vr;->g:J

    sub-long/2addr v0, v2

    .line 1421397
    iget-object v2, p0, LX/8vs;->b:LX/8vr;

    iget v2, v2, LX/8vr;->h:I

    .line 1421398
    int-to-long v4, v2

    cmp-long v3, v0, v4

    if-gez v3, :cond_3

    .line 1421399
    long-to-float v0, v0

    int-to-float v1, v2

    div-float/2addr v0, v1

    .line 1421400
    iget-object v1, p0, LX/8vs;->d:Landroid/view/animation/Interpolator;

    if-nez v1, :cond_2

    .line 1421401
    invoke-static {v0}, LX/8vs;->b(F)F

    move-result v0

    .line 1421402
    :goto_2
    iget-object v1, p0, LX/8vs;->b:LX/8vr;

    invoke-virtual {v1, v0}, LX/8vr;->b(F)V

    .line 1421403
    iget-object v1, p0, LX/8vs;->c:LX/8vr;

    invoke-virtual {v1, v0}, LX/8vr;->b(F)V

    goto :goto_1

    .line 1421404
    :cond_2
    iget-object v1, p0, LX/8vs;->d:Landroid/view/animation/Interpolator;

    invoke-interface {v1, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    goto :goto_2

    .line 1421405
    :cond_3
    invoke-virtual {p0}, LX/8vs;->e()V

    goto :goto_1

    .line 1421406
    :pswitch_1
    iget-object v0, p0, LX/8vs;->b:LX/8vr;

    iget-boolean v0, v0, LX/8vr;->k:Z

    if-nez v0, :cond_4

    .line 1421407
    iget-object v0, p0, LX/8vs;->b:LX/8vr;

    invoke-virtual {v0}, LX/8vr;->c()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1421408
    iget-object v0, p0, LX/8vs;->b:LX/8vr;

    invoke-virtual {v0}, LX/8vr;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1421409
    iget-object v0, p0, LX/8vs;->b:LX/8vr;

    invoke-virtual {v0}, LX/8vr;->a()V

    .line 1421410
    :cond_4
    iget-object v0, p0, LX/8vs;->c:LX/8vr;

    iget-boolean v0, v0, LX/8vr;->k:Z

    if-nez v0, :cond_1

    .line 1421411
    iget-object v0, p0, LX/8vs;->c:LX/8vr;

    invoke-virtual {v0}, LX/8vr;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1421412
    iget-object v0, p0, LX/8vs;->c:LX/8vr;

    invoke-virtual {v0}, LX/8vr;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1421413
    iget-object v0, p0, LX/8vs;->c:LX/8vr;

    invoke-virtual {v0}, LX/8vr;->a()V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 1421387
    iget-object v0, p0, LX/8vs;->b:LX/8vr;

    invoke-virtual {v0}, LX/8vr;->a()V

    .line 1421388
    iget-object v0, p0, LX/8vs;->c:LX/8vr;

    invoke-virtual {v0}, LX/8vr;->a()V

    .line 1421389
    return-void
.end method
