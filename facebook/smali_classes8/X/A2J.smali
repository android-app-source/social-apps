.class public final LX/A2J;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1613210
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherHourlyForecastModel;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 1613211
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1613212
    iget-object v1, p0, LX/A2J;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1613213
    iget-object v2, p0, LX/A2J;->b:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1613214
    iget-object v3, p0, LX/A2J;->c:Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel;

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1613215
    const/4 v4, 0x4

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1613216
    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 1613217
    invoke-virtual {v0, v8, v2}, LX/186;->b(II)V

    .line 1613218
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1613219
    const/4 v1, 0x3

    iget-wide v2, p0, LX/A2J;->d:J

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1613220
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1613221
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1613222
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1613223
    invoke-virtual {v1, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1613224
    new-instance v0, LX/15i;

    move-object v2, v6

    move-object v3, v6

    move v4, v8

    move-object v5, v6

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1613225
    new-instance v1, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherHourlyForecastModel;

    invoke-direct {v1, v0}, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherHourlyForecastModel;-><init>(LX/15i;)V

    .line 1613226
    return-object v1
.end method
