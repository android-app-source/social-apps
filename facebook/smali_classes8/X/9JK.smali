.class public LX/9JK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/nio/ByteBuffer;


# direct methods
.method public constructor <init>(I)V
    .locals 2

    .prologue
    .line 1464597
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1464598
    invoke-static {p1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, LX/9JK;->a:Ljava/nio/ByteBuffer;

    .line 1464599
    iget-object v0, p0, LX/9JK;->a:Ljava/nio/ByteBuffer;

    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 1464600
    return-void
.end method


# virtual methods
.method public final a(B)V
    .locals 1

    .prologue
    .line 1464601
    iget-object v0, p0, LX/9JK;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 1464602
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 1464603
    iget-object v0, p0, LX/9JK;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1464604
    return-void
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 1464595
    iget-object v0, p0, LX/9JK;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1, p2}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    .line 1464596
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1464578
    if-nez p1, :cond_0

    move v0, v1

    .line 1464579
    :goto_0
    iget-object v2, p0, LX/9JK;->a:Ljava/nio/ByteBuffer;

    int-to-short v3, v0

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1464580
    :goto_1
    if-ge v1, v0, :cond_1

    .line 1464581
    iget-object v2, p0, LX/9JK;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    int-to-byte v3, v3

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 1464582
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1464583
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_0

    .line 1464584
    :cond_1
    return-void
.end method

.method public final a(S)V
    .locals 1

    .prologue
    .line 1464593
    iget-object v0, p0, LX/9JK;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1464594
    return-void
.end method

.method public final a([B)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1464585
    if-nez p1, :cond_0

    move v0, v1

    .line 1464586
    :goto_0
    iget-object v2, p0, LX/9JK;->a:Ljava/nio/ByteBuffer;

    int-to-short v0, v0

    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1464587
    if-eqz p1, :cond_1

    .line 1464588
    array-length v0, p1

    :goto_1
    if-ge v1, v0, :cond_1

    aget-byte v2, p1, v1

    .line 1464589
    iget-object v3, p0, LX/9JK;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 1464590
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1464591
    :cond_0
    array-length v0, p1

    goto :goto_0

    .line 1464592
    :cond_1
    return-void
.end method
