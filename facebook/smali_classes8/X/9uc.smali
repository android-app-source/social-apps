.class public interface abstract LX/9uc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9oG;
.implements LX/9oI;
.implements LX/9oJ;
.implements LX/9oK;
.implements LX/9oL;
.implements LX/9oM;
.implements LX/9oO;
.implements LX/9oP;
.implements LX/9oR;
.implements LX/9oS;
.implements LX/9oT;
.implements LX/9qW;
.implements LX/9u7;
.implements LX/9u8;
.implements LX/9u9;
.implements LX/9uA;
.implements LX/9uB;
.implements LX/9uC;
.implements LX/9uD;
.implements LX/9uE;
.implements LX/9uF;
.implements LX/9uG;
.implements LX/9uH;
.implements LX/9uI;
.implements LX/9uJ;
.implements LX/9uK;
.implements LX/9uL;
.implements LX/9uM;
.implements LX/9uN;
.implements LX/9uO;
.implements LX/9uP;
.implements LX/9uQ;
.implements LX/9uR;
.implements LX/9uS;
.implements LX/9uT;
.implements LX/9uU;
.implements LX/9uV;
.implements LX/9uW;
.implements LX/9uX;
.implements LX/9uY;
.implements LX/9uZ;
.implements LX/9ua;
.implements LX/9ub;


# virtual methods
.method public abstract A()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLInterfaces$ReactionUnitBadgedProfilesComponentFragment$BadgableProfiles;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract B()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageVeryResponsiveToMessagesComponentFragmentModel$BadgeIconModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract C()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract D()Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract E()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract F()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLInterfaces$ReactionUnitComponentFields$Breadcrumbs;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract G()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract H()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract I()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract J()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageCategoryBasedRecommendationsComponentFragmentModel$CategoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract K()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract L()LX/1k1;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract M()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$CityPageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract N()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract O()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract P()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract Q()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCommentComponentFragmentModel$CommentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract R()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract S()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract T()D
.end method

.method public abstract U()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract V()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract W()Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract X()Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract Y()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract Z()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract aA()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract aB()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLInterfaces$ReactionStoryAttachmentActionFragment;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract aC()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLInterfaces$ReactionUnitFriendRequestListComponentFragment$FriendingPossibilities;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract aD()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract aE()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLInterfaces$ReactionPageFriendsCityActivityComponentFragment$Friends;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract aF()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract aG()Z
.end method

.method public abstract aH()Z
.end method

.method public abstract aJ()Z
.end method

.method public abstract aK()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract aL()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract aM()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract aN()LX/5sY;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract aO()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract aP()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract aQ()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/graphql/querybuilder/common/CommonGraphQLInterfaces$DefaultTimeRangeFields;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract aR()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$IconModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract aS()LX/5sY;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract aT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$ImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract aU()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract aV()LX/5sY;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract aW()LX/5sY;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract aX()Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract aY()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "LX/5sY;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract aZ()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLInterfaces$ReactionUnitImageWithOverlayComponentFragement$ImagesWithOverlay;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract aa()LX/5sY;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract ab()LX/5sY;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract ac()I
.end method

.method public abstract ad()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLInterfaces$ReactionUnitCountsComponentFragment$Counts;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract ae()LX/5sY;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract af()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$CrisisModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract ag()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageMessageBlockComponentFragmentModel$CustomerDataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract ah()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract ai()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract aj()LX/5sY;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract ak()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract al()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract am()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract an()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "LX/174;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract ao()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLInterfaces$ReactionUnitPageOpenHoursGridComponentFragment$DetailsRows;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract ap()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract aq()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract ar()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract at()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionProfileFrameComponentFragmentModel$ExampleFrameImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract au()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract av()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLInterfaces$ReactionUnitPostPivotComponentFragment$Facepile;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract aw()Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract ax()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract ay()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract az()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeFanFavoriteComponentFragmentModel$FirstVotingPageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract b()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract bB()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract bC()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract bD()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract bE()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract bF()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPagesInsightsOverviewCardHeaderComponentFragmentModel$NameActionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract bG()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract bH()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract bI()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract bJ()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract bK()Z
.end method

.method public abstract bL()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLInterfaces$ReactionUnitOpenAlbumActionsFragment;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract bM()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract bN()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract bO()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract bP()D
.end method

.method public abstract bQ()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract bR()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract bS()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract bT()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract bU()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLInterfaces$ReactionUnitPhotosComponentFragment$Photos;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract bV()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract bW()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLInterfaces$ReactionUnitPlaceInfoBlurbWithBreadcrumbsComponentFragment$PlaceInfoBlurbBreadcrumbs;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract bX()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract bY()D
.end method

.method public abstract bZ()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract ba()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract bb()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract bc()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPagesInsightsOverviewCardHeaderComponentFragmentModel$InfoIconModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract bd()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLInterfaces$ReactionUnitPageAboutInfoGridComponent$InfoRows;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract be()Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract bf()Lcom/facebook/graphql/enums/GraphQLPageInsightsActionType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract bg()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract bh()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract bi()Z
.end method

.method public abstract bj()Z
.end method

.method public abstract bk()Z
.end method

.method public abstract bl()Z
.end method

.method public abstract bm()Z
.end method

.method public abstract bn()Z
.end method

.method public abstract bo()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionJobComponentFragmentModel$JobPhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract bp()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract bq()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract br()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract bs()D
.end method

.method public abstract bt()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$LocationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract bu()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLInterfaces$ReactionUnitPageMapComponentFragment$Locations;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract bv()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract bw()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionGeoRectangleFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract bx()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$MarkNotInCrisisLocationMessageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract by()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$MarkSafeMessageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract bz()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MatchModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract c()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract cA()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract cB()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract cC()D
.end method

.method public abstract cD()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract cE()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract cF()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeFanFavoriteComponentFragmentModel$SecondVotingPageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract cG()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLInterfaces$ReactionStoryAttachmentActionFragment;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract cH()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract cI()Lcom/facebook/graphql/enums/GraphQLSelectedActionState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract cJ()LX/9Zu;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract cK()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "LX/9Zu;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract cL()LX/9o7;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract cM()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionReviewUnitComponentFragmentModel$SpotlightStoryPreviewModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract cN()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract cO()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract cP()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract cQ()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract cR()D
.end method

.method public abstract cT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract cU()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract cV()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract cW()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract cX()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract cY()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract cZ()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract ca()Lcom/facebook/graphql/enums/GraphQLPlaceType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract cb()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "LX/9o7;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract cc()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLInterfaces$ReactionPagesInsightsMetricWithChartComponentFragment$PreviewData;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract cd()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract ce()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract cf()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionProfileFrameComponentFragmentModel$ProfilePhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract cg()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLInterfaces$ReactionUnitComponentFields$Profiles;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract ch()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract ci()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLInterfaces$ReactionSegmentedProgressBarComponentFragment$ProgressSegments;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract cj()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract ck()Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract cl()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract cm()LX/5sd;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract cn()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLInterfaces$HScrollPageCardFields;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract co()D
.end method

.method public abstract cp()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract cq()D
.end method

.method public abstract cr()D
.end method

.method public abstract cs()Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract ct()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract cu()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionFriendRequestStatefulActionListComponentFragmentModel$RequesterModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract cv()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract cw()LX/3Ab;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract cx()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract cy()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract cz()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract d()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract da()J
.end method

.method public abstract db()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract dc()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract dd()I
.end method

.method public abstract de()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract df()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLInterfaces$ReactionUnitGametimeTableComponentFragment$TypedData;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract dg()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract dh()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$UndoMessageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract di()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$UserNotInCrisisLocationMessageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract dj()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$UserSafeMessageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract dk()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract dl()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract dm()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionHeaderWithVerifiedBadgeComponentFragmentModel$VerificationActorModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract dn()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract do()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract dp()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$VideoNotificationContextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract dq()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract dr()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract ds()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract dt()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract du()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract dv()I
.end method

.method public abstract e()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract hj_()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract hk_()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract l()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLInterfaces$ReactionStoryAttachmentActionFragment;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract m()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLInterfaces$PlacePageRecommendationsSocialContextFields;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract n()LX/5sY;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract o()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract p()LX/5sY;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract q()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract r()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract s()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract t()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComposerComponentFragmentModel$AuthorModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract u()I
.end method

.method public abstract v()I
.end method

.method public abstract w()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract x()LX/174;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract y()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract z()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method
