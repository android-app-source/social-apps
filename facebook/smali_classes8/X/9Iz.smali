.class public LX/9Iz;
.super LX/6WT;
.source ""


# instance fields
.field private b:Lcom/facebook/fig/facepile/FigFacepileView;

.field private c:LX/6WU;

.field private d:I


# direct methods
.method public constructor <init>(Lcom/facebook/fig/facepile/FigFacepileView;LX/6WU;I)V
    .locals 0

    .prologue
    .line 1464130
    invoke-direct {p0, p1}, LX/6WT;-><init>(LX/6WN;)V

    .line 1464131
    iput-object p1, p0, LX/9Iz;->b:Lcom/facebook/fig/facepile/FigFacepileView;

    .line 1464132
    iput-object p2, p0, LX/9Iz;->c:LX/6WU;

    .line 1464133
    iput p3, p0, LX/9Iz;->d:I

    .line 1464134
    return-void
.end method

.method private c()Z
    .locals 1

    .prologue
    .line 1464115
    iget-object v0, p0, LX/9Iz;->c:LX/6WU;

    invoke-virtual {v0}, LX/6WU;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9Iz;->b:Lcom/facebook/fig/facepile/FigFacepileView;

    invoke-virtual {v0}, Lcom/facebook/fig/facepile/FigFacepileView;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(I)Z
    .locals 1

    .prologue
    .line 1464129
    invoke-direct {p0}, LX/9Iz;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, LX/9Iz;->d:I

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(FF)I
    .locals 3

    .prologue
    .line 1464135
    invoke-direct {p0}, LX/9Iz;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1464136
    iget-object v0, p0, LX/9Iz;->c:LX/6WU;

    .line 1464137
    iget-object v1, v0, LX/6WU;->c:Landroid/graphics/Rect;

    move-object v0, v1

    .line 1464138
    float-to-int v1, p1

    float-to-int v2, p2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1464139
    iget v0, p0, LX/9Iz;->d:I

    .line 1464140
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, LX/6WT;->a(FF)I

    move-result v0

    goto :goto_0
.end method

.method public final a(ILandroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 1464121
    invoke-direct {p0, p1}, LX/9Iz;->c(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1464122
    iget-object v0, p0, LX/9Iz;->c:LX/6WU;

    .line 1464123
    iget-object p0, v0, LX/6WU;->c:Landroid/graphics/Rect;

    move-object v0, p0

    .line 1464124
    if-eqz p2, :cond_0

    .line 1464125
    invoke-virtual {p2, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 1464126
    :goto_0
    return-object p2

    :cond_0
    move-object p2, v0

    .line 1464127
    goto :goto_0

    .line 1464128
    :cond_1
    invoke-super {p0, p1, p2}, LX/6WT;->a(ILandroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object p2

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1464117
    invoke-super {p0, p1}, LX/6WT;->a(Ljava/util/List;)V

    .line 1464118
    invoke-direct {p0}, LX/9Iz;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1464119
    iget v0, p0, LX/9Iz;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1464120
    :cond_0
    return-void
.end method

.method public final b(I)Z
    .locals 1

    .prologue
    .line 1464116
    invoke-super {p0, p1}, LX/6WT;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, LX/9Iz;->c(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
