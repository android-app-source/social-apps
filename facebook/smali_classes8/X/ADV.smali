.class public final enum LX/ADV;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ADV;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ADV;

.field public static final enum ADD_CC_STATE:LX/ADV;

.field public static final enum ADD_PAYPAL_STATE:LX/ADV;

.field public static final enum DONE_STATE:LX/ADV;

.field public static final enum PREPARE_PAYMENT_METHOD_STATE:LX/ADV;

.field public static final enum SELECT_COUNTRY_STATE:LX/ADV;

.field public static final enum SELECT_PAYMENT_METHOD_STATE:LX/ADV;

.field public static final enum START_STATE:LX/ADV;


# instance fields
.field private final mValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1644743
    new-instance v0, LX/ADV;

    const-string v1, "START_STATE"

    const-string v2, "start_state"

    invoke-direct {v0, v1, v4, v2}, LX/ADV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ADV;->START_STATE:LX/ADV;

    .line 1644744
    new-instance v0, LX/ADV;

    const-string v1, "PREPARE_PAYMENT_METHOD_STATE"

    const-string v2, "prepare_payment_method"

    invoke-direct {v0, v1, v5, v2}, LX/ADV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ADV;->PREPARE_PAYMENT_METHOD_STATE:LX/ADV;

    .line 1644745
    new-instance v0, LX/ADV;

    const-string v1, "SELECT_PAYMENT_METHOD_STATE"

    const-string v2, "select_payment_method"

    invoke-direct {v0, v1, v6, v2}, LX/ADV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ADV;->SELECT_PAYMENT_METHOD_STATE:LX/ADV;

    .line 1644746
    new-instance v0, LX/ADV;

    const-string v1, "SELECT_COUNTRY_STATE"

    const-string v2, "select_country"

    invoke-direct {v0, v1, v7, v2}, LX/ADV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ADV;->SELECT_COUNTRY_STATE:LX/ADV;

    .line 1644747
    new-instance v0, LX/ADV;

    const-string v1, "ADD_CC_STATE"

    const-string v2, "add_cc"

    invoke-direct {v0, v1, v8, v2}, LX/ADV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ADV;->ADD_CC_STATE:LX/ADV;

    .line 1644748
    new-instance v0, LX/ADV;

    const-string v1, "ADD_PAYPAL_STATE"

    const/4 v2, 0x5

    const-string v3, "add_paypal"

    invoke-direct {v0, v1, v2, v3}, LX/ADV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ADV;->ADD_PAYPAL_STATE:LX/ADV;

    .line 1644749
    new-instance v0, LX/ADV;

    const-string v1, "DONE_STATE"

    const/4 v2, 0x6

    const-string v3, "done"

    invoke-direct {v0, v1, v2, v3}, LX/ADV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ADV;->DONE_STATE:LX/ADV;

    .line 1644750
    const/4 v0, 0x7

    new-array v0, v0, [LX/ADV;

    sget-object v1, LX/ADV;->START_STATE:LX/ADV;

    aput-object v1, v0, v4

    sget-object v1, LX/ADV;->PREPARE_PAYMENT_METHOD_STATE:LX/ADV;

    aput-object v1, v0, v5

    sget-object v1, LX/ADV;->SELECT_PAYMENT_METHOD_STATE:LX/ADV;

    aput-object v1, v0, v6

    sget-object v1, LX/ADV;->SELECT_COUNTRY_STATE:LX/ADV;

    aput-object v1, v0, v7

    sget-object v1, LX/ADV;->ADD_CC_STATE:LX/ADV;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/ADV;->ADD_PAYPAL_STATE:LX/ADV;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/ADV;->DONE_STATE:LX/ADV;

    aput-object v2, v0, v1

    sput-object v0, LX/ADV;->$VALUES:[LX/ADV;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1644751
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1644752
    iput-object p3, p0, LX/ADV;->mValue:Ljava/lang/String;

    .line 1644753
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ADV;
    .locals 1

    .prologue
    .line 1644754
    const-class v0, LX/ADV;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ADV;

    return-object v0
.end method

.method public static values()[LX/ADV;
    .locals 1

    .prologue
    .line 1644755
    sget-object v0, LX/ADV;->$VALUES:[LX/ADV;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ADV;

    return-object v0
.end method


# virtual methods
.method public final getReliabilityEventName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1644756
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "payments_android_reliability_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/ADV;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1644757
    iget-object v0, p0, LX/ADV;->mValue:Ljava/lang/String;

    return-object v0
.end method
