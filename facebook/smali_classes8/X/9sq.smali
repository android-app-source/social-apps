.class public final LX/9sq;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$GroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeJobDetailActionFieldsModel$JobOpeningModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPlaysActionFieldsModel$MatchPageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:Lcom/facebook/graphql/enums/GraphQLPagePhotoSourceType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenNearbyPlacesActionFieldsModel$PlacesQueryLocationPageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public M:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public N:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public O:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public P:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public R:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public S:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$RelatedUsersModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public T:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public U:Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public V:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public W:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public X:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Y:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Z:Z

.field public a:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aa:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ab:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ac:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSendMessageAsPageFieldsModel$ThreadKeyModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ad:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ae:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public af:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Z

.field public h:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel$ActionOgObjectModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPageAlbumActionFragmentModel$AlbumModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Z

.field public m:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeePageCommerceProductsFieldsModel$CollectionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$CommentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/graphql/enums/GraphQLFundraiserSupportersConnectionType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$FriendModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewFundraiserSupportersActionFieldsModel$FundraiserModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1558584
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionCommonFragmentModel;
    .locals 58

    .prologue
    .line 1558585
    new-instance v1, LX/186;

    const/16 v2, 0x80

    invoke-direct {v1, v2}, LX/186;-><init>(I)V

    .line 1558586
    move-object/from16 v0, p0

    iget-object v2, v0, LX/9sq;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1558587
    move-object/from16 v0, p0

    iget-object v3, v0, LX/9sq;->b:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-static {v1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1558588
    move-object/from16 v0, p0

    iget-object v4, v0, LX/9sq;->c:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-static {v1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1558589
    move-object/from16 v0, p0

    iget-object v5, v0, LX/9sq;->d:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    invoke-static {v1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1558590
    move-object/from16 v0, p0

    iget-object v6, v0, LX/9sq;->e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-static {v1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1558591
    move-object/from16 v0, p0

    iget-object v7, v0, LX/9sq;->f:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-static {v1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1558592
    move-object/from16 v0, p0

    iget-object v8, v0, LX/9sq;->h:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel$ActionOgObjectModel;

    invoke-static {v1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1558593
    move-object/from16 v0, p0

    iget-object v9, v0, LX/9sq;->i:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    invoke-virtual {v1, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    .line 1558594
    move-object/from16 v0, p0

    iget-object v10, v0, LX/9sq;->j:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPageAlbumActionFragmentModel$AlbumModel;

    invoke-static {v1, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1558595
    move-object/from16 v0, p0

    iget-object v11, v0, LX/9sq;->k:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;

    invoke-static {v1, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1558596
    move-object/from16 v0, p0

    iget-object v12, v0, LX/9sq;->m:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeePageCommerceProductsFieldsModel$CollectionModel;

    invoke-static {v1, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1558597
    move-object/from16 v0, p0

    iget-object v13, v0, LX/9sq;->n:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$CommentModel;

    invoke-static {v1, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 1558598
    move-object/from16 v0, p0

    iget-object v14, v0, LX/9sq;->o:Ljava/lang/String;

    invoke-virtual {v1, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 1558599
    move-object/from16 v0, p0

    iget-object v15, v0, LX/9sq;->p:Ljava/lang/String;

    invoke-virtual {v1, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 1558600
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sq;->q:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 1558601
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sq;->r:Lcom/facebook/graphql/enums/GraphQLFundraiserSupportersConnectionType;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v17

    .line 1558602
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sq;->s:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    .line 1558603
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sq;->t:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v19

    .line 1558604
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sq;->u:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 1558605
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sq;->v:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 1558606
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sq;->w:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$FriendModel;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 1558607
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sq;->x:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v23

    .line 1558608
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sq;->y:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewFundraiserSupportersActionFieldsModel$FundraiserModel;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v24

    .line 1558609
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sq;->z:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v25

    .line 1558610
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sq;->A:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$GroupModel;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v26

    .line 1558611
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sq;->B:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-virtual {v1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v27

    .line 1558612
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sq;->C:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeJobDetailActionFieldsModel$JobOpeningModel;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v28

    .line 1558613
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sq;->D:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v29

    .line 1558614
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sq;->E:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPlaysActionFieldsModel$MatchPageModel;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v30

    .line 1558615
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sq;->F:Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-virtual {v1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v31

    .line 1558616
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sq;->G:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v32

    .line 1558617
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sq;->H:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v33

    .line 1558618
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sq;->I:Ljava/lang/String;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    invoke-virtual {v1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v34

    .line 1558619
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sq;->J:Lcom/facebook/graphql/enums/GraphQLPagePhotoSourceType;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    invoke-virtual {v1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v35

    .line 1558620
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sq;->K:Ljava/lang/String;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    invoke-virtual {v1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v36

    .line 1558621
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sq;->L:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenNearbyPlacesActionFieldsModel$PlacesQueryLocationPageModel;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v37

    .line 1558622
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sq;->M:Ljava/lang/String;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    invoke-virtual {v1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v38

    .line 1558623
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sq;->N:Ljava/lang/String;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    invoke-virtual {v1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v39

    .line 1558624
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sq;->O:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v40

    .line 1558625
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sq;->P:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v41

    .line 1558626
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sq;->Q:Ljava/lang/String;

    move-object/from16 v42, v0

    move-object/from16 v0, v42

    invoke-virtual {v1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v42

    .line 1558627
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sq;->R:Ljava/lang/String;

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    invoke-virtual {v1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v43

    .line 1558628
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sq;->S:LX/0Px;

    move-object/from16 v44, v0

    move-object/from16 v0, v44

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v44

    .line 1558629
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sq;->T:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v45

    .line 1558630
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sq;->U:Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;

    move-object/from16 v46, v0

    move-object/from16 v0, v46

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v46

    .line 1558631
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sq;->V:Ljava/lang/String;

    move-object/from16 v47, v0

    move-object/from16 v0, v47

    invoke-virtual {v1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v47

    .line 1558632
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sq;->W:Ljava/lang/String;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    invoke-virtual {v1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v48

    .line 1558633
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sq;->X:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;

    move-object/from16 v49, v0

    move-object/from16 v0, v49

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v49

    .line 1558634
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sq;->Y:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;

    move-object/from16 v50, v0

    move-object/from16 v0, v50

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v50

    .line 1558635
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sq;->aa:Ljava/lang/String;

    move-object/from16 v51, v0

    move-object/from16 v0, v51

    invoke-virtual {v1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v51

    .line 1558636
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sq;->ab:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel;

    move-object/from16 v52, v0

    move-object/from16 v0, v52

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v52

    .line 1558637
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sq;->ac:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSendMessageAsPageFieldsModel$ThreadKeyModel;

    move-object/from16 v53, v0

    move-object/from16 v0, v53

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v53

    .line 1558638
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sq;->ad:Ljava/lang/String;

    move-object/from16 v54, v0

    move-object/from16 v0, v54

    invoke-virtual {v1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v54

    .line 1558639
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sq;->ae:Ljava/lang/String;

    move-object/from16 v55, v0

    move-object/from16 v0, v55

    invoke-virtual {v1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v55

    .line 1558640
    move-object/from16 v0, p0

    iget-object v0, v0, LX/9sq;->af:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-object/from16 v56, v0

    move-object/from16 v0, v56

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v56

    .line 1558641
    const/16 v57, 0x3a

    move/from16 v0, v57

    invoke-virtual {v1, v0}, LX/186;->c(I)V

    .line 1558642
    const/16 v57, 0x0

    move/from16 v0, v57

    invoke-virtual {v1, v0, v2}, LX/186;->b(II)V

    .line 1558643
    const/4 v2, 0x1

    invoke-virtual {v1, v2, v3}, LX/186;->b(II)V

    .line 1558644
    const/4 v2, 0x2

    invoke-virtual {v1, v2, v4}, LX/186;->b(II)V

    .line 1558645
    const/4 v2, 0x3

    invoke-virtual {v1, v2, v5}, LX/186;->b(II)V

    .line 1558646
    const/4 v2, 0x4

    invoke-virtual {v1, v2, v6}, LX/186;->b(II)V

    .line 1558647
    const/4 v2, 0x5

    invoke-virtual {v1, v2, v7}, LX/186;->b(II)V

    .line 1558648
    const/4 v2, 0x6

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/9sq;->g:Z

    invoke-virtual {v1, v2, v3}, LX/186;->a(IZ)V

    .line 1558649
    const/4 v2, 0x7

    invoke-virtual {v1, v2, v8}, LX/186;->b(II)V

    .line 1558650
    const/16 v2, 0x8

    invoke-virtual {v1, v2, v9}, LX/186;->b(II)V

    .line 1558651
    const/16 v2, 0x9

    invoke-virtual {v1, v2, v10}, LX/186;->b(II)V

    .line 1558652
    const/16 v2, 0xa

    invoke-virtual {v1, v2, v11}, LX/186;->b(II)V

    .line 1558653
    const/16 v2, 0xb

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/9sq;->l:Z

    invoke-virtual {v1, v2, v3}, LX/186;->a(IZ)V

    .line 1558654
    const/16 v2, 0xc

    invoke-virtual {v1, v2, v12}, LX/186;->b(II)V

    .line 1558655
    const/16 v2, 0xd

    invoke-virtual {v1, v2, v13}, LX/186;->b(II)V

    .line 1558656
    const/16 v2, 0xe

    invoke-virtual {v1, v2, v14}, LX/186;->b(II)V

    .line 1558657
    const/16 v2, 0xf

    invoke-virtual {v1, v2, v15}, LX/186;->b(II)V

    .line 1558658
    const/16 v2, 0x10

    move/from16 v0, v16

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1558659
    const/16 v2, 0x11

    move/from16 v0, v17

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1558660
    const/16 v2, 0x12

    move/from16 v0, v18

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1558661
    const/16 v2, 0x13

    move/from16 v0, v19

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1558662
    const/16 v2, 0x14

    move/from16 v0, v20

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1558663
    const/16 v2, 0x15

    move/from16 v0, v21

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1558664
    const/16 v2, 0x16

    move/from16 v0, v22

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1558665
    const/16 v2, 0x17

    move/from16 v0, v23

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1558666
    const/16 v2, 0x18

    move/from16 v0, v24

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1558667
    const/16 v2, 0x19

    move/from16 v0, v25

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1558668
    const/16 v2, 0x1a

    move/from16 v0, v26

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1558669
    const/16 v2, 0x1b

    move/from16 v0, v27

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1558670
    const/16 v2, 0x1c

    move/from16 v0, v28

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1558671
    const/16 v2, 0x1d

    move/from16 v0, v29

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1558672
    const/16 v2, 0x1e

    move/from16 v0, v30

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1558673
    const/16 v2, 0x1f

    move/from16 v0, v31

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1558674
    const/16 v2, 0x20

    move/from16 v0, v32

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1558675
    const/16 v2, 0x21

    move/from16 v0, v33

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1558676
    const/16 v2, 0x22

    move/from16 v0, v34

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1558677
    const/16 v2, 0x23

    move/from16 v0, v35

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1558678
    const/16 v2, 0x24

    move/from16 v0, v36

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1558679
    const/16 v2, 0x25

    move/from16 v0, v37

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1558680
    const/16 v2, 0x26

    move/from16 v0, v38

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1558681
    const/16 v2, 0x27

    move/from16 v0, v39

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1558682
    const/16 v2, 0x28

    move/from16 v0, v40

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1558683
    const/16 v2, 0x29

    move/from16 v0, v41

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1558684
    const/16 v2, 0x2a

    move/from16 v0, v42

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1558685
    const/16 v2, 0x2b

    move/from16 v0, v43

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1558686
    const/16 v2, 0x2c

    move/from16 v0, v44

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1558687
    const/16 v2, 0x2d

    move/from16 v0, v45

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1558688
    const/16 v2, 0x2e

    move/from16 v0, v46

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1558689
    const/16 v2, 0x2f

    move/from16 v0, v47

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1558690
    const/16 v2, 0x30

    move/from16 v0, v48

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1558691
    const/16 v2, 0x31

    move/from16 v0, v49

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1558692
    const/16 v2, 0x32

    move/from16 v0, v50

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1558693
    const/16 v2, 0x33

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/9sq;->Z:Z

    invoke-virtual {v1, v2, v3}, LX/186;->a(IZ)V

    .line 1558694
    const/16 v2, 0x34

    move/from16 v0, v51

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1558695
    const/16 v2, 0x35

    move/from16 v0, v52

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1558696
    const/16 v2, 0x36

    move/from16 v0, v53

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1558697
    const/16 v2, 0x37

    move/from16 v0, v54

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1558698
    const/16 v2, 0x38

    move/from16 v0, v55

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1558699
    const/16 v2, 0x39

    move/from16 v0, v56

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 1558700
    invoke-virtual {v1}, LX/186;->d()I

    move-result v2

    .line 1558701
    invoke-virtual {v1, v2}, LX/186;->d(I)V

    .line 1558702
    invoke-virtual {v1}, LX/186;->e()[B

    move-result-object v1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 1558703
    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1558704
    new-instance v1, LX/15i;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1558705
    new-instance v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionCommonFragmentModel;

    invoke-direct {v2, v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionCommonFragmentModel;-><init>(LX/15i;)V

    .line 1558706
    return-object v2
.end method
