.class public LX/9EH;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""

# interfaces
.implements LX/3Wr;


# instance fields
.field public j:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/1Uf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:Lcom/facebook/intent/feed/IFeedIntentBuilder;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final n:LX/9HK;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1456834
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/9EH;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1456835
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1456836
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/9EH;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1456837
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1456839
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1456840
    invoke-direct {p0}, LX/9EH;->e()V

    .line 1456841
    new-instance v0, LX/9HK;

    invoke-direct {v0, p1}, LX/9HK;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/9EH;->n:LX/9HK;

    .line 1456842
    return-void
.end method

.method private static a(LX/9EH;LX/0ad;LX/1Uf;Lcom/facebook/intent/feed/IFeedIntentBuilder;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0

    .prologue
    .line 1456838
    iput-object p1, p0, LX/9EH;->j:LX/0ad;

    iput-object p2, p0, LX/9EH;->k:LX/1Uf;

    iput-object p3, p0, LX/9EH;->l:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iput-object p4, p0, LX/9EH;->m:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, LX/9EH;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, LX/9EH;

    invoke-static {v3}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    invoke-static {v3}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v1

    check-cast v1, LX/1Uf;

    invoke-static {v3}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v2

    check-cast v2, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {v3}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0, v0, v1, v2, v3}, LX/9EH;->a(LX/9EH;LX/0ad;LX/1Uf;Lcom/facebook/intent/feed/IFeedIntentBuilder;Lcom/facebook/content/SecureContextHelper;)V

    return-void
.end method

.method private e()V
    .locals 1

    .prologue
    .line 1456830
    const-class v0, LX/9EH;

    invoke-static {v0, p0}, LX/9EH;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1456831
    const v0, 0x7f030ddf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1456832
    invoke-direct {p0}, LX/9EH;->f()V

    .line 1456833
    return-void
.end method

.method private f()V
    .locals 9

    .prologue
    const/16 v8, 0x11

    const/4 v7, -0x1

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1456798
    invoke-virtual {p0}, LX/9EH;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 1456799
    const/high16 v0, 0x42500000    # 52.0f

    invoke-static {v6, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v2, v0

    .line 1456800
    const/high16 v0, 0x41400000    # 12.0f

    invoke-static {v6, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v3, v0

    .line 1456801
    invoke-virtual {p0}, LX/9EH;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1456802
    if-nez v0, :cond_0

    .line 1456803
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, v7, v2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 1456804
    :cond_0
    instance-of v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v4, :cond_1

    .line 1456805
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1456806
    iput v7, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 1456807
    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 1456808
    const/high16 v2, 0x41000000    # 8.0f

    invoke-static {v6, v2, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    .line 1456809
    invoke-virtual {v0, v2, v5, v2, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 1456810
    invoke-virtual {p0, v0}, LX/9EH;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1456811
    :cond_1
    iget-object v0, p0, LX/9EH;->j:LX/0ad;

    sget-short v2, LX/0wn;->ax:S

    invoke-interface {v0, v2, v5}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1456812
    const v0, 0x7f021627

    invoke-virtual {p0, v0}, LX/9EH;->setBackgroundResource(I)V

    .line 1456813
    :goto_0
    invoke-virtual {p0, v3, v5, v3, v5}, LX/9EH;->setPaddingRelative(IIII)V

    .line 1456814
    invoke-virtual {p0, v8}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setGravity(I)V

    .line 1456815
    const/high16 v0, 0x41e00000    # 28.0f

    invoke-static {v6, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    .line 1456816
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailSize(I)V

    .line 1456817
    invoke-virtual {p0, v5}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailPadding(I)V

    .line 1456818
    invoke-virtual {p0, v8}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailGravity(I)V

    .line 1456819
    const v0, 0x7f02111f

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailResource(I)V

    .line 1456820
    return-void

    .line 1456821
    :cond_2
    const v0, 0x7f021626

    invoke-virtual {p0, v0}, LX/9EH;->setBackgroundResource(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/9CP;)V
    .locals 1

    .prologue
    .line 1456794
    iget-object v0, p0, LX/9EH;->n:LX/9HK;

    invoke-virtual {v0, p1}, LX/9HK;->a(LX/9CP;)V

    .line 1456795
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1456796
    iget-object v0, p0, LX/9EH;->n:LX/9HK;

    invoke-virtual {v0}, LX/9HK;->a()V

    .line 1456797
    return-void
.end method

.method public final dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1456822
    iget-object v0, p0, LX/9EH;->n:LX/9HK;

    invoke-virtual {v0, p1}, LX/9HK;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1456827
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->draw(Landroid/graphics/Canvas;)V

    .line 1456828
    iget-object v0, p0, LX/9EH;->n:LX/9HK;

    invoke-virtual {v0, p1}, LX/9HK;->a(Landroid/graphics/Canvas;)V

    .line 1456829
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x7bd786b4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1456823
    invoke-super {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->onAttachedToWindow()V

    .line 1456824
    const/16 v1, 0x2d

    const v2, 0x3b6a1bbc

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x22c4852c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1456825
    invoke-super {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->onDetachedFromWindow()V

    .line 1456826
    const/16 v1, 0x2d

    const v2, -0x4c136cb5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
