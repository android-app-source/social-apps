.class public final enum LX/ASZ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ASZ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ASZ;

.field public static final enum HIDE:LX/ASZ;

.field public static final enum NONE:LX/ASZ;

.field public static final enum SHOW:LX/ASZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1674157
    new-instance v0, LX/ASZ;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LX/ASZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ASZ;->NONE:LX/ASZ;

    .line 1674158
    new-instance v0, LX/ASZ;

    const-string v1, "SHOW"

    invoke-direct {v0, v1, v3}, LX/ASZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ASZ;->SHOW:LX/ASZ;

    .line 1674159
    new-instance v0, LX/ASZ;

    const-string v1, "HIDE"

    invoke-direct {v0, v1, v4}, LX/ASZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ASZ;->HIDE:LX/ASZ;

    .line 1674160
    const/4 v0, 0x3

    new-array v0, v0, [LX/ASZ;

    sget-object v1, LX/ASZ;->NONE:LX/ASZ;

    aput-object v1, v0, v2

    sget-object v1, LX/ASZ;->SHOW:LX/ASZ;

    aput-object v1, v0, v3

    sget-object v1, LX/ASZ;->HIDE:LX/ASZ;

    aput-object v1, v0, v4

    sput-object v0, LX/ASZ;->$VALUES:[LX/ASZ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1674156
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ASZ;
    .locals 1

    .prologue
    .line 1674155
    const-class v0, LX/ASZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ASZ;

    return-object v0
.end method

.method public static values()[LX/ASZ;
    .locals 1

    .prologue
    .line 1674154
    sget-object v0, LX/ASZ;->$VALUES:[LX/ASZ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ASZ;

    return-object v0
.end method
