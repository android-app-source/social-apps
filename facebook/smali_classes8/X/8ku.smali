.class public final LX/8ku;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/keyboard/StickerKeyboardView;)V
    .locals 0

    .prologue
    .line 1396925
    iput-object p1, p0, LX/8ku;->a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x41d1d780

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1396926
    iget-object v1, p0, LX/8ku;->a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    iget-object v1, v1, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->m:LX/8kc;

    iget-object v2, p0, LX/8ku;->a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    iget-object v2, v2, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->Q:LX/4m4;

    .line 1396927
    iget-object v4, v1, LX/8kc;->c:LX/8kd;

    .line 1396928
    const-string v5, "sticker_keyboard"

    invoke-static {v5}, LX/8j7;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    .line 1396929
    const-string v6, "action"

    const-string p1, "sticker_store_opened"

    invoke-virtual {v5, v6, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1396930
    iget-object v6, v4, LX/8kd;->a:LX/8j7;

    invoke-virtual {v6, v5}, LX/8j7;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1396931
    new-instance v4, Landroid/content/Intent;

    iget-object v5, v1, LX/8kc;->a:Landroid/content/Context;

    const-class v6, Lcom/facebook/stickers/store/StickerStoreActivity;

    invoke-direct {v4, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1396932
    const-string v5, "stickerContext"

    invoke-virtual {v4, v5, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1396933
    sget-object v5, LX/4m4;->COMMENTS:LX/4m4;

    if-ne v2, v5, :cond_0

    .line 1396934
    iget-object v5, v1, LX/8kc;->e:LX/11i;

    sget-object v6, LX/8lJ;->c:LX/8lH;

    invoke-interface {v5, v6}, LX/11i;->a(LX/0Pq;)LX/11o;

    .line 1396935
    :cond_0
    iget-object v5, v1, LX/8kc;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v6, v1, LX/8kc;->a:Landroid/content/Context;

    invoke-interface {v5, v4, v6}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1396936
    iget-object v1, p0, LX/8ku;->a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    invoke-static {v1}, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->l(Lcom/facebook/stickers/keyboard/StickerKeyboardView;)V

    .line 1396937
    const v1, -0x53339ae4

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
