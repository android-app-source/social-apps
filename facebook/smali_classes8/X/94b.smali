.class public final enum LX/94b;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/94b;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/94b;

.field public static final enum ALBUM:LX/94b;

.field public static final enum DEFAULT:LX/94b;

.field public static final enum EVENT:LX/94b;

.field public static final enum GROUP:LX/94b;

.field public static final enum PROFILE:LX/94b;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1435544
    new-instance v0, LX/94b;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v2}, LX/94b;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/94b;->DEFAULT:LX/94b;

    .line 1435545
    new-instance v0, LX/94b;

    const-string v1, "ALBUM"

    invoke-direct {v0, v1, v3}, LX/94b;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/94b;->ALBUM:LX/94b;

    .line 1435546
    new-instance v0, LX/94b;

    const-string v1, "GROUP"

    invoke-direct {v0, v1, v4}, LX/94b;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/94b;->GROUP:LX/94b;

    .line 1435547
    new-instance v0, LX/94b;

    const-string v1, "EVENT"

    invoke-direct {v0, v1, v5}, LX/94b;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/94b;->EVENT:LX/94b;

    .line 1435548
    new-instance v0, LX/94b;

    const-string v1, "PROFILE"

    invoke-direct {v0, v1, v6}, LX/94b;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/94b;->PROFILE:LX/94b;

    .line 1435549
    const/4 v0, 0x5

    new-array v0, v0, [LX/94b;

    sget-object v1, LX/94b;->DEFAULT:LX/94b;

    aput-object v1, v0, v2

    sget-object v1, LX/94b;->ALBUM:LX/94b;

    aput-object v1, v0, v3

    sget-object v1, LX/94b;->GROUP:LX/94b;

    aput-object v1, v0, v4

    sget-object v1, LX/94b;->EVENT:LX/94b;

    aput-object v1, v0, v5

    sget-object v1, LX/94b;->PROFILE:LX/94b;

    aput-object v1, v0, v6

    sput-object v0, LX/94b;->$VALUES:[LX/94b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1435550
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/94b;
    .locals 1

    .prologue
    .line 1435551
    const-class v0, LX/94b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/94b;

    return-object v0
.end method

.method public static values()[LX/94b;
    .locals 1

    .prologue
    .line 1435552
    sget-object v0, LX/94b;->$VALUES:[LX/94b;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/94b;

    return-object v0
.end method
