.class public LX/8wa;
.super LX/4or;
.source ""

# interfaces
.implements LX/0Ya;


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1422787
    invoke-direct {p0, p1}, LX/4or;-><init>(Landroid/content/Context;)V

    .line 1422788
    const-class v0, LX/8wa;

    invoke-static {v0, p0, p1}, LX/8wa;->a(Ljava/lang/Class;LX/0Ya;Landroid/content/Context;)V

    .line 1422789
    iget-object v0, p0, LX/8wa;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0uQ;->d:LX/0Tn;

    const-wide/32 v2, 0x15180

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 1422790
    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/CharSequence;

    const-string v2, "5 minute"

    aput-object v2, v1, v4

    const-string v2, "10 minute"

    aput-object v2, v1, v5

    const-string v2, "1 hour"

    aput-object v2, v1, v6

    const-string v2, "1 day"

    aput-object v2, v1, v7

    const-string v2, "Clear Setting"

    aput-object v2, v1, v8

    .line 1422791
    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/CharSequence;

    const-string v3, "300"

    aput-object v3, v2, v4

    const-string v3, "600"

    aput-object v3, v2, v5

    const-string v3, "3600"

    aput-object v3, v2, v6

    const-string v3, "86400"

    aput-object v3, v2, v7

    const-string v3, "0"

    aput-object v3, v2, v8

    .line 1422792
    invoke-virtual {p0, v1}, LX/8wa;->setEntries([Ljava/lang/CharSequence;)V

    .line 1422793
    invoke-virtual {p0, v2}, LX/8wa;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 1422794
    invoke-virtual {p0, v0}, LX/8wa;->setDefaultValue(Ljava/lang/Object;)V

    .line 1422795
    const-class v0, LX/8wa;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/8wa;->setKey(Ljava/lang/String;)V

    .line 1422796
    invoke-virtual {p0, v4}, LX/8wa;->setPersistent(Z)V

    .line 1422797
    const-string v0, "Contacts Upload Interval"

    invoke-virtual {p0, v0}, LX/8wa;->setTitle(Ljava/lang/CharSequence;)V

    .line 1422798
    const-string v0, "How long to wait before uploading contacts again"

    invoke-virtual {p0, v0}, LX/8wa;->setSummary(Ljava/lang/CharSequence;)V

    .line 1422799
    new-instance v0, LX/8wZ;

    invoke-direct {v0, p0}, LX/8wZ;-><init>(LX/8wa;)V

    invoke-virtual {p0, v0}, LX/8wa;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 1422800
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/0Ya;Landroid/content/Context;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0Ya;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/8wa;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object p0

    check-cast p0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p0, p1, LX/8wa;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    return-void
.end method
