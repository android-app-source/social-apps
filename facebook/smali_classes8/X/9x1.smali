.class public final LX/9x1;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 1588757
    const/4 v9, 0x0

    .line 1588758
    const/4 v8, 0x0

    .line 1588759
    const/4 v7, 0x0

    .line 1588760
    const/4 v6, 0x0

    .line 1588761
    const/4 v5, 0x0

    .line 1588762
    const/4 v4, 0x0

    .line 1588763
    const/4 v3, 0x0

    .line 1588764
    const/4 v2, 0x0

    .line 1588765
    const/4 v1, 0x0

    .line 1588766
    const/4 v0, 0x0

    .line 1588767
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v10, v11, :cond_1

    .line 1588768
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1588769
    const/4 v0, 0x0

    .line 1588770
    :goto_0
    return v0

    .line 1588771
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1588772
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_9

    .line 1588773
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1588774
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1588775
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 1588776
    const-string v11, "__type__"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_2

    const-string v11, "__typename"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1588777
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v9

    goto :goto_1

    .line 1588778
    :cond_3
    const-string v11, "component_style"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 1588779
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    goto :goto_1

    .line 1588780
    :cond_4
    const-string v11, "empty_state_action"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 1588781
    invoke-static {p0, p1}, LX/9tj;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1588782
    :cond_5
    const-string v11, "has_bottom_border"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 1588783
    const/4 v2, 0x1

    .line 1588784
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v6

    goto :goto_1

    .line 1588785
    :cond_6
    const-string v11, "has_inner_borders"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 1588786
    const/4 v1, 0x1

    .line 1588787
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v5

    goto :goto_1

    .line 1588788
    :cond_7
    const-string v11, "has_top_border"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 1588789
    const/4 v0, 0x1

    .line 1588790
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v4

    goto :goto_1

    .line 1588791
    :cond_8
    const-string v11, "sub_components"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 1588792
    invoke-static {p0, p1}, LX/9y6;->b(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 1588793
    :cond_9
    const/4 v10, 0x7

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1588794
    const/4 v10, 0x0

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 1588795
    const/4 v9, 0x1

    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 1588796
    const/4 v8, 0x2

    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 1588797
    if-eqz v2, :cond_a

    .line 1588798
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v6}, LX/186;->a(IZ)V

    .line 1588799
    :cond_a
    if-eqz v1, :cond_b

    .line 1588800
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v5}, LX/186;->a(IZ)V

    .line 1588801
    :cond_b
    if-eqz v0, :cond_c

    .line 1588802
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->a(IZ)V

    .line 1588803
    :cond_c
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1588804
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1588805
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1588806
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1588807
    if-eqz v0, :cond_0

    .line 1588808
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1588809
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1588810
    :cond_0
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1588811
    if-eqz v0, :cond_1

    .line 1588812
    const-string v0, "component_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1588813
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1588814
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1588815
    if-eqz v0, :cond_2

    .line 1588816
    const-string v1, "empty_state_action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1588817
    invoke-static {p0, v0, p2, p3}, LX/9tj;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1588818
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1588819
    if-eqz v0, :cond_3

    .line 1588820
    const-string v1, "has_bottom_border"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1588821
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1588822
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1588823
    if-eqz v0, :cond_4

    .line 1588824
    const-string v1, "has_inner_borders"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1588825
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1588826
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1588827
    if-eqz v0, :cond_5

    .line 1588828
    const-string v1, "has_top_border"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1588829
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1588830
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1588831
    if-eqz v0, :cond_6

    .line 1588832
    const-string v1, "sub_components"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1588833
    invoke-static {p0, v0, p2, p3}, LX/9y6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1588834
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1588835
    return-void
.end method
