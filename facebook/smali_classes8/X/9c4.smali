.class public final enum LX/9c4;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9c4;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9c4;

.field public static final enum COMPOSER_CROP:LX/9c4;

.field public static final enum COMPOSER_FILTERS_IN_GALLERY:LX/9c4;

.field public static final enum COMPOSER_FILTER_VIEWED:LX/9c4;

.field public static final enum COMPOSER_FRAME_VIEWED:LX/9c4;

.field public static final enum COMPOSER_STICKERS_ADDED_STICKER:LX/9c4;

.field public static final enum COMPOSER_STICKERS_ENTER_FLOW:LX/9c4;

.field public static final enum COMPOSER_STICKERS_ENTER_STORE:LX/9c4;

.field public static final enum COMPOSER_STICKERS_EXIT_FLOW:LX/9c4;

.field public static final enum COMPOSER_STICKERS_REMOVED_STICKER:LX/9c4;

.field public static final enum COMPOSER_TEXT_ON_PHOTOS:LX/9c4;

.field public static final enum CREATIVECAM_CAPTURE_PHOTO:LX/9c4;

.field public static final enum CREATIVECAM_CAPTURE_PHOTO_CONFIRMED:LX/9c4;

.field public static final enum CREATIVECAM_ENTRY:LX/9c4;

.field public static final enum CREATIVECAM_EXIT_BACK:LX/9c4;

.field public static final enum CREATIVECAM_EXIT_CLOSE:LX/9c4;

.field public static final enum CREATIVECAM_FRAME_VIEWED:LX/9c4;

.field public static final enum CREATIVECAM_OPEN_SIMPLE_PICKER:LX/9c4;

.field public static final enum CREATIVECAM_RETAKE_PHOTO:LX/9c4;

.field public static final enum CREATIVECAM_SWITCH_CAMERA_FACING:LX/9c4;

.field public static final enum EDITGALLERY_FILTER_VIEWED:LX/9c4;

.field public static final enum EDITGALLERY_FRAME_VIEWED:LX/9c4;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1515819
    new-instance v0, LX/9c4;

    const-string v1, "COMPOSER_CROP"

    const-string v2, "composer_crop"

    invoke-direct {v0, v1, v4, v2}, LX/9c4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c4;->COMPOSER_CROP:LX/9c4;

    .line 1515820
    new-instance v0, LX/9c4;

    const-string v1, "COMPOSER_STICKERS_ENTER_FLOW"

    const-string v2, "composer_stickers_enter_flow"

    invoke-direct {v0, v1, v5, v2}, LX/9c4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c4;->COMPOSER_STICKERS_ENTER_FLOW:LX/9c4;

    .line 1515821
    new-instance v0, LX/9c4;

    const-string v1, "COMPOSER_STICKERS_ADDED_STICKER"

    const-string v2, "composer_stickers_added_sticker"

    invoke-direct {v0, v1, v6, v2}, LX/9c4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c4;->COMPOSER_STICKERS_ADDED_STICKER:LX/9c4;

    .line 1515822
    new-instance v0, LX/9c4;

    const-string v1, "COMPOSER_STICKERS_REMOVED_STICKER"

    const-string v2, "composer_stickers_removed_sticker"

    invoke-direct {v0, v1, v7, v2}, LX/9c4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c4;->COMPOSER_STICKERS_REMOVED_STICKER:LX/9c4;

    .line 1515823
    new-instance v0, LX/9c4;

    const-string v1, "COMPOSER_STICKERS_ENTER_STORE"

    const-string v2, "composer_stickers_enter_store"

    invoke-direct {v0, v1, v8, v2}, LX/9c4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c4;->COMPOSER_STICKERS_ENTER_STORE:LX/9c4;

    .line 1515824
    new-instance v0, LX/9c4;

    const-string v1, "COMPOSER_TEXT_ON_PHOTOS"

    const/4 v2, 0x5

    const-string v3, "composer_text_on_photos"

    invoke-direct {v0, v1, v2, v3}, LX/9c4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c4;->COMPOSER_TEXT_ON_PHOTOS:LX/9c4;

    .line 1515825
    new-instance v0, LX/9c4;

    const-string v1, "COMPOSER_FILTERS_IN_GALLERY"

    const/4 v2, 0x6

    const-string v3, "composer_filters_in_gallery"

    invoke-direct {v0, v1, v2, v3}, LX/9c4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c4;->COMPOSER_FILTERS_IN_GALLERY:LX/9c4;

    .line 1515826
    new-instance v0, LX/9c4;

    const-string v1, "COMPOSER_STICKERS_EXIT_FLOW"

    const/4 v2, 0x7

    const-string v3, "composer_stickers_exit_flow"

    invoke-direct {v0, v1, v2, v3}, LX/9c4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c4;->COMPOSER_STICKERS_EXIT_FLOW:LX/9c4;

    .line 1515827
    new-instance v0, LX/9c4;

    const-string v1, "COMPOSER_FILTER_VIEWED"

    const/16 v2, 0x8

    const-string v3, "composer_filter_viewed"

    invoke-direct {v0, v1, v2, v3}, LX/9c4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c4;->COMPOSER_FILTER_VIEWED:LX/9c4;

    .line 1515828
    new-instance v0, LX/9c4;

    const-string v1, "EDITGALLERY_FILTER_VIEWED"

    const/16 v2, 0x9

    const-string v3, "editgallery_filter_viewed"

    invoke-direct {v0, v1, v2, v3}, LX/9c4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c4;->EDITGALLERY_FILTER_VIEWED:LX/9c4;

    .line 1515829
    new-instance v0, LX/9c4;

    const-string v1, "COMPOSER_FRAME_VIEWED"

    const/16 v2, 0xa

    const-string v3, "composer_frame_viewed"

    invoke-direct {v0, v1, v2, v3}, LX/9c4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c4;->COMPOSER_FRAME_VIEWED:LX/9c4;

    .line 1515830
    new-instance v0, LX/9c4;

    const-string v1, "EDITGALLERY_FRAME_VIEWED"

    const/16 v2, 0xb

    const-string v3, "editgallery_frame_viewed"

    invoke-direct {v0, v1, v2, v3}, LX/9c4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c4;->EDITGALLERY_FRAME_VIEWED:LX/9c4;

    .line 1515831
    new-instance v0, LX/9c4;

    const-string v1, "CREATIVECAM_FRAME_VIEWED"

    const/16 v2, 0xc

    const-string v3, "creativecam_frame_viewed"

    invoke-direct {v0, v1, v2, v3}, LX/9c4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c4;->CREATIVECAM_FRAME_VIEWED:LX/9c4;

    .line 1515832
    new-instance v0, LX/9c4;

    const-string v1, "CREATIVECAM_ENTRY"

    const/16 v2, 0xd

    const-string v3, "creativecam_entry"

    invoke-direct {v0, v1, v2, v3}, LX/9c4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c4;->CREATIVECAM_ENTRY:LX/9c4;

    .line 1515833
    new-instance v0, LX/9c4;

    const-string v1, "CREATIVECAM_EXIT_BACK"

    const/16 v2, 0xe

    const-string v3, "creativecam_exit_back"

    invoke-direct {v0, v1, v2, v3}, LX/9c4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c4;->CREATIVECAM_EXIT_BACK:LX/9c4;

    .line 1515834
    new-instance v0, LX/9c4;

    const-string v1, "CREATIVECAM_EXIT_CLOSE"

    const/16 v2, 0xf

    const-string v3, "creativecam_exit_close"

    invoke-direct {v0, v1, v2, v3}, LX/9c4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c4;->CREATIVECAM_EXIT_CLOSE:LX/9c4;

    .line 1515835
    new-instance v0, LX/9c4;

    const-string v1, "CREATIVECAM_CAPTURE_PHOTO"

    const/16 v2, 0x10

    const-string v3, "creativecam_capture_photo"

    invoke-direct {v0, v1, v2, v3}, LX/9c4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c4;->CREATIVECAM_CAPTURE_PHOTO:LX/9c4;

    .line 1515836
    new-instance v0, LX/9c4;

    const-string v1, "CREATIVECAM_RETAKE_PHOTO"

    const/16 v2, 0x11

    const-string v3, "creativecam_retake_photo"

    invoke-direct {v0, v1, v2, v3}, LX/9c4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c4;->CREATIVECAM_RETAKE_PHOTO:LX/9c4;

    .line 1515837
    new-instance v0, LX/9c4;

    const-string v1, "CREATIVECAM_CAPTURE_PHOTO_CONFIRMED"

    const/16 v2, 0x12

    const-string v3, "creativecam_capture_photo_confirmed"

    invoke-direct {v0, v1, v2, v3}, LX/9c4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c4;->CREATIVECAM_CAPTURE_PHOTO_CONFIRMED:LX/9c4;

    .line 1515838
    new-instance v0, LX/9c4;

    const-string v1, "CREATIVECAM_SWITCH_CAMERA_FACING"

    const/16 v2, 0x13

    const-string v3, "creativecam_switch_camera_facing"

    invoke-direct {v0, v1, v2, v3}, LX/9c4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c4;->CREATIVECAM_SWITCH_CAMERA_FACING:LX/9c4;

    .line 1515839
    new-instance v0, LX/9c4;

    const-string v1, "CREATIVECAM_OPEN_SIMPLE_PICKER"

    const/16 v2, 0x14

    const-string v3, "creativecam_open_simple_picker"

    invoke-direct {v0, v1, v2, v3}, LX/9c4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9c4;->CREATIVECAM_OPEN_SIMPLE_PICKER:LX/9c4;

    .line 1515840
    const/16 v0, 0x15

    new-array v0, v0, [LX/9c4;

    sget-object v1, LX/9c4;->COMPOSER_CROP:LX/9c4;

    aput-object v1, v0, v4

    sget-object v1, LX/9c4;->COMPOSER_STICKERS_ENTER_FLOW:LX/9c4;

    aput-object v1, v0, v5

    sget-object v1, LX/9c4;->COMPOSER_STICKERS_ADDED_STICKER:LX/9c4;

    aput-object v1, v0, v6

    sget-object v1, LX/9c4;->COMPOSER_STICKERS_REMOVED_STICKER:LX/9c4;

    aput-object v1, v0, v7

    sget-object v1, LX/9c4;->COMPOSER_STICKERS_ENTER_STORE:LX/9c4;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/9c4;->COMPOSER_TEXT_ON_PHOTOS:LX/9c4;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/9c4;->COMPOSER_FILTERS_IN_GALLERY:LX/9c4;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/9c4;->COMPOSER_STICKERS_EXIT_FLOW:LX/9c4;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/9c4;->COMPOSER_FILTER_VIEWED:LX/9c4;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/9c4;->EDITGALLERY_FILTER_VIEWED:LX/9c4;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/9c4;->COMPOSER_FRAME_VIEWED:LX/9c4;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/9c4;->EDITGALLERY_FRAME_VIEWED:LX/9c4;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/9c4;->CREATIVECAM_FRAME_VIEWED:LX/9c4;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/9c4;->CREATIVECAM_ENTRY:LX/9c4;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/9c4;->CREATIVECAM_EXIT_BACK:LX/9c4;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/9c4;->CREATIVECAM_EXIT_CLOSE:LX/9c4;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/9c4;->CREATIVECAM_CAPTURE_PHOTO:LX/9c4;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/9c4;->CREATIVECAM_RETAKE_PHOTO:LX/9c4;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/9c4;->CREATIVECAM_CAPTURE_PHOTO_CONFIRMED:LX/9c4;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/9c4;->CREATIVECAM_SWITCH_CAMERA_FACING:LX/9c4;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/9c4;->CREATIVECAM_OPEN_SIMPLE_PICKER:LX/9c4;

    aput-object v2, v0, v1

    sput-object v0, LX/9c4;->$VALUES:[LX/9c4;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1515841
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1515842
    iput-object p3, p0, LX/9c4;->name:Ljava/lang/String;

    .line 1515843
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9c4;
    .locals 1

    .prologue
    .line 1515818
    const-class v0, LX/9c4;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9c4;

    return-object v0
.end method

.method public static values()[LX/9c4;
    .locals 1

    .prologue
    .line 1515817
    sget-object v0, LX/9c4;->$VALUES:[LX/9c4;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9c4;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1515816
    iget-object v0, p0, LX/9c4;->name:Ljava/lang/String;

    return-object v0
.end method
