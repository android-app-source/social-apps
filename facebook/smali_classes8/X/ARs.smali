.class public final enum LX/ARs;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ARs;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ARs;

.field public static final enum ATTACH_TO_ALBUM_NOT_ALLOWED:LX/ARs;

.field public static final enum FRIEND_TAGGING_NOT_ALLOWED:LX/ARs;

.field public static final enum LOCATION_TAGGING_NOT_ALLOWED:LX/ARs;

.field public static final enum MINUTIAE_NOT_ALLOWED:LX/ARs;

.field public static final enum MULTIMEDIA_NOT_ALLOWED:LX/ARs;

.field public static final enum MULTI_PHOTOS_NOT_ALLOWED:LX/ARs;

.field public static final enum POST_NOT_PUBLIC:LX/ARs;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1673331
    new-instance v0, LX/ARs;

    const-string v1, "POST_NOT_PUBLIC"

    invoke-direct {v0, v1, v3}, LX/ARs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ARs;->POST_NOT_PUBLIC:LX/ARs;

    .line 1673332
    new-instance v0, LX/ARs;

    const-string v1, "MULTI_PHOTOS_NOT_ALLOWED"

    invoke-direct {v0, v1, v4}, LX/ARs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ARs;->MULTI_PHOTOS_NOT_ALLOWED:LX/ARs;

    .line 1673333
    new-instance v0, LX/ARs;

    const-string v1, "FRIEND_TAGGING_NOT_ALLOWED"

    invoke-direct {v0, v1, v5}, LX/ARs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ARs;->FRIEND_TAGGING_NOT_ALLOWED:LX/ARs;

    .line 1673334
    new-instance v0, LX/ARs;

    const-string v1, "LOCATION_TAGGING_NOT_ALLOWED"

    invoke-direct {v0, v1, v6}, LX/ARs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ARs;->LOCATION_TAGGING_NOT_ALLOWED:LX/ARs;

    .line 1673335
    new-instance v0, LX/ARs;

    const-string v1, "MINUTIAE_NOT_ALLOWED"

    invoke-direct {v0, v1, v7}, LX/ARs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ARs;->MINUTIAE_NOT_ALLOWED:LX/ARs;

    .line 1673336
    new-instance v0, LX/ARs;

    const-string v1, "ATTACH_TO_ALBUM_NOT_ALLOWED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/ARs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ARs;->ATTACH_TO_ALBUM_NOT_ALLOWED:LX/ARs;

    .line 1673337
    new-instance v0, LX/ARs;

    const-string v1, "MULTIMEDIA_NOT_ALLOWED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/ARs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ARs;->MULTIMEDIA_NOT_ALLOWED:LX/ARs;

    .line 1673338
    const/4 v0, 0x7

    new-array v0, v0, [LX/ARs;

    sget-object v1, LX/ARs;->POST_NOT_PUBLIC:LX/ARs;

    aput-object v1, v0, v3

    sget-object v1, LX/ARs;->MULTI_PHOTOS_NOT_ALLOWED:LX/ARs;

    aput-object v1, v0, v4

    sget-object v1, LX/ARs;->FRIEND_TAGGING_NOT_ALLOWED:LX/ARs;

    aput-object v1, v0, v5

    sget-object v1, LX/ARs;->LOCATION_TAGGING_NOT_ALLOWED:LX/ARs;

    aput-object v1, v0, v6

    sget-object v1, LX/ARs;->MINUTIAE_NOT_ALLOWED:LX/ARs;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/ARs;->ATTACH_TO_ALBUM_NOT_ALLOWED:LX/ARs;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/ARs;->MULTIMEDIA_NOT_ALLOWED:LX/ARs;

    aput-object v2, v0, v1

    sput-object v0, LX/ARs;->$VALUES:[LX/ARs;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1673339
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ARs;
    .locals 1

    .prologue
    .line 1673330
    const-class v0, LX/ARs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ARs;

    return-object v0
.end method

.method public static values()[LX/ARs;
    .locals 1

    .prologue
    .line 1673329
    sget-object v0, LX/ARs;->$VALUES:[LX/ARs;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ARs;

    return-object v0
.end method
