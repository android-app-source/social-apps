.class public LX/8yN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/8yP;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1425271
    const-class v0, LX/8yN;

    sput-object v0, LX/8yN;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1425272
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1425273
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 5

    .prologue
    .line 1425274
    check-cast p1, LX/8yP;

    .line 1425275
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 1425276
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "filetype"

    .line 1425277
    iget v3, p1, LX/8yP;->b:I

    move v3, v3

    .line 1425278
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1425279
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "crash_id"

    .line 1425280
    iget-object v3, p1, LX/8yP;->c:Ljava/lang/String;

    move-object v3, v3

    .line 1425281
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1425282
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "extras"

    .line 1425283
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 1425284
    :try_start_0
    const-string v4, "app_version_code"

    iget p0, p1, LX/8yP;->d:I

    invoke-virtual {v3, v4, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1425285
    const-string v4, "brand"

    iget-object p0, p1, LX/8yP;->e:Ljava/lang/String;

    invoke-virtual {v3, v4, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1425286
    const-string v4, "memclass"

    iget p0, p1, LX/8yP;->f:I

    invoke-virtual {v3, v4, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1425287
    const-string v4, "model"

    iget-object p0, p1, LX/8yP;->g:Ljava/lang/String;

    invoke-virtual {v3, v4, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1425288
    const-string v4, "android_version"

    iget-object p0, p1, LX/8yP;->h:Ljava/lang/String;

    invoke-virtual {v3, v4, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1425289
    const-string v4, "app_version_name"

    iget-object p0, p1, LX/8yP;->i:Ljava/lang/String;

    invoke-virtual {v3, v4, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1425290
    const-string v4, "app"

    iget-object p0, p1, LX/8yP;->j:Ljava/lang/String;

    invoke-virtual {v3, v4, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1425291
    const-string v4, "process_name"

    iget-object p0, p1, LX/8yP;->k:Ljava/lang/String;

    invoke-virtual {v3, v4, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1425292
    const-string v4, "uid"

    iget-object p0, p1, LX/8yP;->l:Ljava/lang/String;

    invoke-virtual {v3, v4, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1425293
    const-string v4, "dump_cause"

    iget-object p0, p1, LX/8yP;->m:Ljava/lang/String;

    invoke-virtual {v3, v4, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1425294
    const-string v4, "background"

    iget-object p0, p1, LX/8yP;->n:Ljava/lang/String;

    invoke-virtual {v3, v4, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1425295
    const-string v4, "was_foreground"

    iget-object p0, p1, LX/8yP;->o:Ljava/lang/String;

    invoke-virtual {v3, v4, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1425296
    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 1425297
    :goto_0
    move-object v3, v3

    .line 1425298
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1425299
    iget-object v1, p1, LX/8yP;->a:Ljava/io/File;

    move-object v1, v1

    .line 1425300
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    invoke-virtual {v1}, Ljava/io/File;->length()J

    .line 1425301
    new-instance v2, LX/4ct;

    const-string v3, "application/octet-stream"

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v1, v3, v4}, LX/4ct;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V

    .line 1425302
    new-instance v1, LX/4cQ;

    const-string v3, "file"

    invoke-direct {v1, v3, v2}, LX/4cQ;-><init>(Ljava/lang/String;LX/4cO;)V

    .line 1425303
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v2

    const-string v3, "hprofUpload"

    .line 1425304
    iput-object v3, v2, LX/14O;->b:Ljava/lang/String;

    .line 1425305
    move-object v2, v2

    .line 1425306
    const-string v3, "POST"

    .line 1425307
    iput-object v3, v2, LX/14O;->c:Ljava/lang/String;

    .line 1425308
    move-object v2, v2

    .line 1425309
    const-string v3, "me/hprof"

    .line 1425310
    iput-object v3, v2, LX/14O;->d:Ljava/lang/String;

    .line 1425311
    move-object v2, v2

    .line 1425312
    iput-object v0, v2, LX/14O;->g:Ljava/util/List;

    .line 1425313
    move-object v0, v2

    .line 1425314
    sget-object v2, LX/14S;->JSON:LX/14S;

    .line 1425315
    iput-object v2, v0, LX/14O;->k:LX/14S;

    .line 1425316
    move-object v0, v0

    .line 1425317
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 1425318
    iput-object v1, v0, LX/14O;->l:Ljava/util/List;

    .line 1425319
    move-object v0, v0

    .line 1425320
    sget-object v1, LX/14R;->MULTI_PART_ENTITY:LX/14R;

    .line 1425321
    iput-object v1, v0, LX/14O;->w:LX/14R;

    .line 1425322
    move-object v0, v0

    .line 1425323
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    .line 1425324
    :catch_0
    move-exception v3

    .line 1425325
    const-string v4, "{ \'error\' : \'%s\' }"

    invoke-virtual {v3}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1425326
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1425327
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "success"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->g(LX/0lF;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
