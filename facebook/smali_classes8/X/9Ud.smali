.class public final LX/9Ud;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/graphics/drawable/Drawable;

.field public final b:Landroid/graphics/Matrix;


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;Landroid/graphics/Matrix;)V
    .locals 2

    .prologue
    .line 1498115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1498116
    iput-object p2, p0, LX/9Ud;->b:Landroid/graphics/Matrix;

    .line 1498117
    iput-object p1, p0, LX/9Ud;->a:Landroid/graphics/drawable/Drawable;

    .line 1498118
    if-eqz p1, :cond_1

    .line 1498119
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 1498120
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    if-gtz v0, :cond_1

    .line 1498121
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "KeyframesDrawable FeatureConfig Drawable must have bounds set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1498122
    :cond_1
    return-void
.end method
