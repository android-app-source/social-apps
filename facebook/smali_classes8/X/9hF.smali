.class public LX/9hF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/9hF;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1526402
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0Px;)LX/9hE;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "LX/9hE;"
        }
    .end annotation

    .prologue
    .line 1526391
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1526392
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 1526393
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 1526394
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1526395
    invoke-static {v0}, LX/1VO;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1526396
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1526397
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-static {v0}, LX/5k9;->a(Lcom/facebook/graphql/model/GraphQLMedia;)LX/5kD;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1526398
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1526399
    :cond_1
    new-instance v0, LX/9hE;

    const-class v1, LX/23W;

    new-instance v4, Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    invoke-direct {v4, v2}, Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;-><init>(LX/0Px;)V

    invoke-static {v1, v4}, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a(Ljava/lang/Class;Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;)Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    move-result-object v1

    invoke-direct {v0, v1}, LX/9hE;-><init>(Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;)V

    .line 1526400
    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9hD;->a(LX/0Px;)LX/9hD;

    .line 1526401
    return-object v0
.end method

.method public static a(LX/0Px;I)LX/9hE;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;I)",
            "LX/9hE;"
        }
    .end annotation

    .prologue
    .line 1526385
    invoke-static {p0}, LX/9hF;->a(LX/0Px;)LX/9hE;

    move-result-object v1

    .line 1526386
    if-ltz p1, :cond_0

    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 1526387
    invoke-virtual {p0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 1526388
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1526389
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/9hD;->a(Ljava/lang/String;)LX/9hD;

    .line 1526390
    :cond_0
    return-object v1
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLAlbum;)LX/9hE;
    .locals 1

    .prologue
    .line 1526383
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1526384
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/9hF;->a(Ljava/lang/String;)LX/9hE;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/9hE;
    .locals 4

    .prologue
    .line 1526283
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1526284
    new-instance v0, LX/9hE;

    const-class v1, LX/23W;

    new-instance v2, Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;-><init>(LX/0Px;)V

    invoke-static {v1, v2}, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a(Ljava/lang/Class;Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;)Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    move-result-object v1

    invoke-direct {v0, v1}, LX/9hE;-><init>(Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;)V

    .line 1526285
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-static {v1}, LX/5k9;->a(Lcom/facebook/graphql/model/GraphQLMedia;)LX/5kD;

    move-result-object v1

    .line 1526286
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/9hD;->a(LX/0Px;)LX/9hD;

    .line 1526287
    invoke-interface {v1}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9hD;->a(Ljava/lang/String;)LX/9hD;

    .line 1526288
    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStorySet;)LX/9hE;
    .locals 7

    .prologue
    .line 1526366
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1526367
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 1526368
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 1526369
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->x()Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1526370
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->x()Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;->j()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1526371
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->x()Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;->j()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_1

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1526372
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1526373
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 1526374
    if-eqz v0, :cond_0

    .line 1526375
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 1526376
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1526377
    invoke-static {v0}, LX/5k9;->a(Lcom/facebook/graphql/model/GraphQLMedia;)LX/5kD;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1526378
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1526379
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1526380
    :cond_1
    new-instance v0, LX/9hE;

    const-class v1, LX/23W;

    new-instance v4, Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;

    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    invoke-direct {v4, v3}, Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;-><init>(LX/0Px;)V

    invoke-static {v1, v4}, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a(Ljava/lang/Class;Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;)Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    move-result-object v1

    invoke-direct {v0, v1}, LX/9hE;-><init>(Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;)V

    .line 1526381
    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9hD;->a(LX/0Px;)LX/9hD;

    .line 1526382
    return-object v0
.end method

.method public static a(Ljava/lang/String;)LX/9hE;
    .locals 3

    .prologue
    .line 1526363
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1526364
    new-instance v0, LX/9hE;

    const-class v1, LX/23Y;

    new-instance v2, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;

    invoke-direct {v2, p0}, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a(Ljava/lang/Class;Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;)Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    move-result-object v1

    invoke-direct {v0, v1}, LX/9hE;-><init>(Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;)V

    .line 1526365
    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/9hE;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/AvailablePhotoCategoriesEnum;
        .end annotation
    .end param

    .prologue
    .line 1526359
    new-instance v0, LX/9hE;

    const-class v1, LX/23e;

    new-instance v2, Lcom/facebook/photos/mediafetcher/query/param/CategoryQueryParam;

    invoke-direct {v2, p0, p1, p2}, Lcom/facebook/photos/mediafetcher/query/param/CategoryQueryParam;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a(Ljava/lang/Class;Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;)Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    move-result-object v1

    invoke-direct {v0, v1}, LX/9hE;-><init>(Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;)V

    .line 1526360
    const/4 v1, 0x0

    .line 1526361
    iput-boolean v1, v0, LX/9hD;->n:Z

    .line 1526362
    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/util/List;)LX/9hE;
    .locals 3
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<+",
            "LX/1U8;",
            ">;)",
            "LX/9hE;"
        }
    .end annotation

    .prologue
    .line 1526355
    new-instance v0, LX/9hE;

    const-class v1, LX/1V6;

    new-instance v2, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;

    invoke-direct {v2, p0}, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a(Ljava/lang/Class;Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;)Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    move-result-object v1

    invoke-direct {v0, v1}, LX/9hE;-><init>(Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;)V

    .line 1526356
    if-eqz p1, :cond_0

    .line 1526357
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9hE;->b(LX/0Px;)LX/9hE;

    .line 1526358
    :cond_0
    return-object v0
.end method

.method public static a(LX/0QB;)LX/9hF;
    .locals 3

    .prologue
    .line 1526343
    sget-object v0, LX/9hF;->a:LX/9hF;

    if-nez v0, :cond_1

    .line 1526344
    const-class v1, LX/9hF;

    monitor-enter v1

    .line 1526345
    :try_start_0
    sget-object v0, LX/9hF;->a:LX/9hF;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1526346
    if-eqz v2, :cond_0

    .line 1526347
    :try_start_1
    new-instance v0, LX/9hF;

    invoke-direct {v0}, LX/9hF;-><init>()V

    .line 1526348
    move-object v0, v0

    .line 1526349
    sput-object v0, LX/9hF;->a:LX/9hF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1526350
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1526351
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1526352
    :cond_1
    sget-object v0, LX/9hF;->a:LX/9hF;

    return-object v0

    .line 1526353
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1526354
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(LX/0Px;)LX/9hE;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPhoto;",
            ">;)",
            "LX/9hE;"
        }
    .end annotation

    .prologue
    .line 1526320
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1526321
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 1526322
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 1526323
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 1526324
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->K()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1526325
    const/4 v8, 0x0

    .line 1526326
    if-nez v0, :cond_2

    .line 1526327
    :cond_0
    :goto_1
    move-object v0, v8

    .line 1526328
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1526329
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1526330
    :cond_1
    new-instance v0, LX/9hE;

    const-class v1, LX/23W;

    new-instance v4, Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    invoke-direct {v4, v2}, Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;-><init>(LX/0Px;)V

    invoke-static {v1, v4}, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a(Ljava/lang/Class;Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;)Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    move-result-object v1

    invoke-direct {v0, v1}, LX/9hE;-><init>(Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;)V

    .line 1526331
    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9hD;->a(LX/0Px;)LX/9hD;

    .line 1526332
    return-object v0

    .line 1526333
    :cond_2
    new-instance v6, LX/186;

    const/16 v7, 0x80

    invoke-direct {v6, v7}, LX/186;-><init>(I)V

    .line 1526334
    invoke-static {v6, v0}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLPhoto;)I

    move-result v7

    .line 1526335
    if-eqz v7, :cond_0

    .line 1526336
    invoke-virtual {v6, v7}, LX/186;->d(I)V

    .line 1526337
    invoke-virtual {v6}, LX/186;->e()[B

    move-result-object v6

    invoke-static {v6}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v7

    .line 1526338
    const/4 v6, 0x0

    invoke-virtual {v7, v6}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1526339
    new-instance v6, LX/15i;

    const/4 v10, 0x1

    move-object v9, v8

    move-object v11, v8

    invoke-direct/range {v6 .. v11}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1526340
    instance-of v7, v0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v7, :cond_3

    .line 1526341
    const-string v7, "PhotosMetadataConversionHelper.getMediaMetadata"

    invoke-virtual {v6, v7, v0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1526342
    :cond_3
    new-instance v8, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;

    invoke-direct {v8, v6}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;-><init>(LX/15i;)V

    goto :goto_1
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/9hE;
    .locals 3

    .prologue
    .line 1526312
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1526313
    invoke-static {p0}, LX/9hF;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/9hE;

    move-result-object v0

    .line 1526314
    const/4 v1, 0x1

    .line 1526315
    iput-boolean v1, v0, LX/9hD;->B:Z

    .line 1526316
    move-object v1, v0

    .line 1526317
    const/4 v2, 0x0

    .line 1526318
    iput-boolean v2, v1, LX/9hD;->C:Z

    .line 1526319
    return-object v0
.end method

.method public static b(Ljava/lang/String;)LX/9hE;
    .locals 3

    .prologue
    .line 1526311
    new-instance v0, LX/9hE;

    const-class v1, LX/23Z;

    invoke-static {p0}, Lcom/facebook/photos/mediafetcher/query/param/MediaTypeQueryParam;->b(Ljava/lang/String;)Lcom/facebook/photos/mediafetcher/query/param/MediaTypeQueryParam;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a(Ljava/lang/Class;Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;)Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    move-result-object v1

    invoke-direct {v0, v1}, LX/9hE;-><init>(Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;)V

    return-object v0
.end method

.method public static c(LX/0Px;)LX/9hE;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "LX/1U8;",
            ">;)",
            "LX/9hE;"
        }
    .end annotation

    .prologue
    .line 1526301
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1526302
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 1526303
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 1526304
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1U8;

    .line 1526305
    invoke-interface {v0}, LX/1U8;->d()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1526306
    invoke-static {v0}, LX/5k9;->a(LX/1U8;)LX/5kD;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1526307
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1526308
    :cond_0
    new-instance v0, LX/9hE;

    const-class v1, LX/23W;

    new-instance v4, Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    invoke-direct {v4, v2}, Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;-><init>(LX/0Px;)V

    invoke-static {v1, v4}, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a(Ljava/lang/Class;Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;)Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    move-result-object v1

    invoke-direct {v0, v1}, LX/9hE;-><init>(Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;)V

    .line 1526309
    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9hD;->a(LX/0Px;)LX/9hD;

    .line 1526310
    return-object v0
.end method

.method public static d(LX/0Px;)Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/5kD;",
            ">;)",
            "Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;"
        }
    .end annotation

    .prologue
    .line 1526295
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1526296
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 1526297
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5kD;

    .line 1526298
    invoke-interface {v0}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1526299
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1526300
    :cond_0
    const-class v0, LX/23W;

    new-instance v1, Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;-><init>(LX/0Px;)V

    invoke-static {v0, v1}, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a(Ljava/lang/Class;Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;)Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    move-result-object v0

    return-object v0
.end method

.method public static e(LX/0Px;)LX/9hE;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/5kD;",
            ">;)",
            "LX/9hE;"
        }
    .end annotation

    .prologue
    .line 1526291
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1526292
    new-instance v0, LX/9hE;

    invoke-static {p0}, LX/9hF;->d(LX/0Px;)Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    move-result-object v1

    invoke-direct {v0, v1}, LX/9hE;-><init>(Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;)V

    .line 1526293
    invoke-virtual {v0, p0}, LX/9hD;->a(LX/0Px;)LX/9hD;

    .line 1526294
    return-object v0
.end method

.method public static f(LX/0Px;)LX/9hE;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/9hE;"
        }
    .end annotation

    .prologue
    .line 1526290
    new-instance v0, LX/9hE;

    const-class v1, LX/23W;

    new-instance v2, Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;

    invoke-direct {v2, p0}, Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;-><init>(LX/0Px;)V

    invoke-static {v1, v2}, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a(Ljava/lang/Class;Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;)Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    move-result-object v1

    invoke-direct {v0, v1}, LX/9hE;-><init>(Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;)V

    return-object v0
.end method

.method public static f(Ljava/lang/String;)LX/9hE;
    .locals 3

    .prologue
    .line 1526289
    new-instance v0, LX/9hE;

    const-class v1, LX/23d;

    new-instance v2, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;

    invoke-direct {v2, p0}, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a(Ljava/lang/Class;Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;)Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    move-result-object v1

    invoke-direct {v0, v1}, LX/9hE;-><init>(Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;)V

    return-object v0
.end method
