.class public final LX/8n1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/facebook/tagging/model/TaggingProfile;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1400257
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/tagging/model/TaggingProfile;)I
    .locals 2

    .prologue
    .line 1400265
    iget-object v0, p0, Lcom/facebook/tagging/model/TaggingProfile;->h:Ljava/lang/String;

    move-object v0, v0

    .line 1400266
    invoke-static {v0}, LX/8nE;->valueOf(Ljava/lang/String;)LX/8nE;

    move-result-object v0

    .line 1400267
    sget-object v1, LX/8nE;->GROUPS:LX/8nE;

    if-ne v0, v1, :cond_0

    .line 1400268
    const/4 v0, 0x2

    .line 1400269
    :goto_0
    instance-of v1, p0, Lcom/facebook/tagging/model/TaggingProfileSectionHeader;

    if-eqz v1, :cond_2

    .line 1400270
    mul-int/lit8 v0, v0, 0x2

    .line 1400271
    :goto_1
    return v0

    .line 1400272
    :cond_0
    invoke-virtual {v0}, LX/8nE;->isGroupTagTypeaheadDataType()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1400273
    const/4 v0, 0x0

    goto :goto_0

    .line 1400274
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 1400275
    :cond_2
    mul-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 1400258
    check-cast p1, Lcom/facebook/tagging/model/TaggingProfile;

    check-cast p2, Lcom/facebook/tagging/model/TaggingProfile;

    .line 1400259
    instance-of v0, p1, Lcom/facebook/tagging/model/TagExpansionInfoHeader;

    if-eqz v0, :cond_0

    .line 1400260
    const/4 v0, -0x1

    .line 1400261
    :goto_0
    return v0

    .line 1400262
    :cond_0
    instance-of v0, p2, Lcom/facebook/tagging/model/TagExpansionInfoHeader;

    if-eqz v0, :cond_1

    .line 1400263
    const/4 v0, 0x1

    goto :goto_0

    .line 1400264
    :cond_1
    invoke-static {p1}, LX/8n1;->a(Lcom/facebook/tagging/model/TaggingProfile;)I

    move-result v0

    invoke-static {p2}, LX/8n1;->a(Lcom/facebook/tagging/model/TaggingProfile;)I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0
.end method
