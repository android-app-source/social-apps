.class public final LX/ALf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/ALg;


# direct methods
.method public constructor <init>(LX/ALg;)V
    .locals 0

    .prologue
    .line 1665186
    iput-object p1, p0, LX/ALf;->a:LX/ALg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1665187
    iget-object v0, p0, LX/ALf;->a:LX/ALg;

    iget-object v0, v0, LX/ALg;->a:Lcom/facebook/backstage/camera/PreviewView;

    iget-object v0, v0, Lcom/facebook/backstage/camera/PreviewView;->u:LX/ALj;

    if-eqz v0, :cond_0

    .line 1665188
    iget-object v0, p0, LX/ALf;->a:LX/ALg;

    iget-object v0, v0, LX/ALg;->a:Lcom/facebook/backstage/camera/PreviewView;

    iget-object v0, v0, Lcom/facebook/backstage/camera/PreviewView;->u:LX/ALj;

    iget-object v1, p0, LX/ALf;->a:LX/ALg;

    iget-object v1, v1, LX/ALg;->a:Lcom/facebook/backstage/camera/PreviewView;

    iget-object v1, v1, Lcom/facebook/backstage/camera/PreviewView;->t:LX/7gj;

    invoke-interface {v0, v1}, LX/ALj;->a(LX/7gj;)V

    .line 1665189
    :cond_0
    iget-object v0, p0, LX/ALf;->a:LX/ALg;

    iget-object v0, v0, LX/ALg;->a:Lcom/facebook/backstage/camera/PreviewView;

    iget-object v0, v0, Lcom/facebook/backstage/camera/PreviewView;->u:LX/ALj;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/ALf;->a:LX/ALg;

    iget-object v0, v0, LX/ALg;->a:Lcom/facebook/backstage/camera/PreviewView;

    iget-object v0, v0, Lcom/facebook/backstage/camera/PreviewView;->u:LX/ALj;

    invoke-interface {v0}, LX/ALj;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1665190
    iget-object v0, p0, LX/ALf;->a:LX/ALg;

    iget-object v0, v0, LX/ALg;->a:Lcom/facebook/backstage/camera/PreviewView;

    invoke-static {v0}, Lcom/facebook/backstage/camera/PreviewView;->f(Lcom/facebook/backstage/camera/PreviewView;)V

    .line 1665191
    :cond_1
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1665192
    iget-object v0, p0, LX/ALf;->a:LX/ALg;

    iget-object v0, v0, LX/ALg;->a:Lcom/facebook/backstage/camera/PreviewView;

    invoke-static {v0}, Lcom/facebook/backstage/camera/PreviewView;->f(Lcom/facebook/backstage/camera/PreviewView;)V

    .line 1665193
    sget-object v0, Lcom/facebook/backstage/camera/PreviewView;->h:Ljava/lang/String;

    const-string v1, "Failed to decode media to bitmap or video file"

    invoke-static {v0, v1}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    .line 1665194
    iget-object v1, p0, LX/ALf;->a:LX/ALg;

    iget-object v1, v1, LX/ALg;->a:Lcom/facebook/backstage/camera/PreviewView;

    iget-object v1, v1, Lcom/facebook/backstage/camera/PreviewView;->d:LX/03V;

    invoke-virtual {v1, v0}, LX/03V;->a(LX/0VG;)V

    .line 1665195
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1665196
    invoke-direct {p0}, LX/ALf;->a()V

    return-void
.end method
