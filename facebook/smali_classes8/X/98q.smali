.class public LX/98q;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/98q;


# instance fields
.field public final a:LX/0Sh;

.field public final b:LX/03V;

.field public c:LX/98n;


# direct methods
.method public constructor <init>(LX/0Sh;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1446416
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1446417
    iput-object p1, p0, LX/98q;->a:LX/0Sh;

    .line 1446418
    iput-object p2, p0, LX/98q;->b:LX/03V;

    .line 1446419
    return-void
.end method

.method public static a(LX/0QB;)LX/98q;
    .locals 5

    .prologue
    .line 1446403
    sget-object v0, LX/98q;->d:LX/98q;

    if-nez v0, :cond_1

    .line 1446404
    const-class v1, LX/98q;

    monitor-enter v1

    .line 1446405
    :try_start_0
    sget-object v0, LX/98q;->d:LX/98q;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1446406
    if-eqz v2, :cond_0

    .line 1446407
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1446408
    new-instance p0, LX/98q;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v3

    check-cast v3, LX/0Sh;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-direct {p0, v3, v4}, LX/98q;-><init>(LX/0Sh;LX/03V;)V

    .line 1446409
    move-object v0, p0

    .line 1446410
    sput-object v0, LX/98q;->d:LX/98q;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1446411
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1446412
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1446413
    :cond_1
    sget-object v0, LX/98q;->d:LX/98q;

    return-object v0

    .line 1446414
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1446415
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1446392
    iget-object v0, p0, LX/98q;->a:LX/0Sh;

    new-instance v1, Lcom/facebook/fbreact/fragment/FbReactFragmentHooks$2;

    invoke-direct {v1, p0}, Lcom/facebook/fbreact/fragment/FbReactFragmentHooks$2;-><init>(LX/98q;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 1446393
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1446398
    iget-object v0, p0, LX/98q;->c:LX/98n;

    if-eqz v0, :cond_0

    .line 1446399
    iget-object v0, p0, LX/98q;->c:LX/98n;

    .line 1446400
    iget-object p0, v0, LX/98n;->a:Lcom/facebook/fbreact/fragment/FbReactFragment;

    .line 1446401
    iput-object p1, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->j:Landroid/os/Bundle;

    .line 1446402
    :cond_0
    return-void
.end method

.method public final c()Landroid/os/Bundle;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1446394
    iget-object v0, p0, LX/98q;->c:LX/98n;

    if-eqz v0, :cond_0

    .line 1446395
    iget-object v0, p0, LX/98q;->c:LX/98n;

    .line 1446396
    iget-object p0, v0, LX/98n;->a:Lcom/facebook/fbreact/fragment/FbReactFragment;

    iget-object p0, p0, Lcom/facebook/fbreact/fragment/FbReactFragment;->j:Landroid/os/Bundle;

    move-object v0, p0

    .line 1446397
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
