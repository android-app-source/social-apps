.class public LX/9jv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/9jv;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9jS;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/9jS;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1530573
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1530574
    iput-object p1, p0, LX/9jv;->a:LX/0Ot;

    .line 1530575
    return-void
.end method

.method public static a(LX/0QB;)LX/9jv;
    .locals 4

    .prologue
    .line 1530576
    sget-object v0, LX/9jv;->b:LX/9jv;

    if-nez v0, :cond_1

    .line 1530577
    const-class v1, LX/9jv;

    monitor-enter v1

    .line 1530578
    :try_start_0
    sget-object v0, LX/9jv;->b:LX/9jv;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1530579
    if-eqz v2, :cond_0

    .line 1530580
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1530581
    new-instance v3, LX/9jv;

    const/16 p0, 0x2f2a

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/9jv;-><init>(LX/0Ot;)V

    .line 1530582
    move-object v0, v3

    .line 1530583
    sput-object v0, LX/9jv;->b:LX/9jv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1530584
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1530585
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1530586
    :cond_1
    sget-object v0, LX/9jv;->b:LX/9jv;

    return-object v0

    .line 1530587
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1530588
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
