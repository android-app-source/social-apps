.class public final LX/9ik;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8JH;


# instance fields
.field public final synthetic a:LX/9ip;


# direct methods
.method public constructor <init>(LX/9ip;)V
    .locals 0

    .prologue
    .line 1528331
    iput-object p1, p0, LX/9ik;->a:LX/9ip;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/RectF;)V
    .locals 5

    .prologue
    .line 1528332
    iget-object v0, p0, LX/9ik;->a:LX/9ip;

    iget-object v0, v0, LX/9ip;->h:LX/BIm;

    if-eqz v0, :cond_0

    .line 1528333
    iget-object v0, p0, LX/9ik;->a:LX/9ip;

    iget-object v0, v0, LX/9ip;->p:LX/9ic;

    invoke-virtual {v0, p1}, LX/9ic;->a(Landroid/graphics/RectF;)Lcom/facebook/photos/base/tagging/FaceBox;

    move-result-object v0

    .line 1528334
    if-nez v0, :cond_1

    .line 1528335
    sget-object v0, LX/9ip;->b:Ljava/lang/String;

    const-string v1, "Retrieved Box is not a valid FaceBox"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1528336
    :cond_0
    :goto_0
    return-void

    .line 1528337
    :cond_1
    iget-object v1, p0, LX/9ik;->a:LX/9ip;

    iget-object v1, v1, LX/9ip;->h:LX/BIm;

    .line 1528338
    iget-object v2, v1, LX/BIm;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-static {v2}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->q(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)LX/9ip;

    move-result-object v2

    .line 1528339
    invoke-virtual {v2, v0}, LX/9ip;->a(Lcom/facebook/photos/base/tagging/FaceBox;)Lcom/facebook/photos/base/tagging/FaceBox;

    move-result-object v3

    .line 1528340
    iget-object v4, v1, LX/BIm;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    iget-object v4, v4, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->T:LX/9iw;

    iget-object p0, v1, LX/BIm;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-static {p0}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->p(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)Lcom/facebook/photos/base/photos/LocalPhoto;

    move-result-object p0

    const/4 p1, 0x1

    .line 1528341
    iget-object v1, v2, LX/9ip;->p:LX/9ic;

    move-object v2, v1

    .line 1528342
    invoke-virtual {v4, p0, v3, p1, v2}, LX/9iw;->a(LX/74x;Lcom/facebook/photos/base/tagging/TagTarget;ZLX/9ic;)V

    .line 1528343
    goto :goto_0
.end method
