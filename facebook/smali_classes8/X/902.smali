.class public final LX/902;
.super LX/1OX;
.source ""


# instance fields
.field public final synthetic a:LX/62T;

.field public final synthetic b:Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;LX/62T;)V
    .locals 0

    .prologue
    .line 1428384
    iput-object p1, p0, LX/902;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;

    iput-object p2, p0, LX/902;->a:LX/62T;

    invoke-direct {p0}, LX/1OX;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 3

    .prologue
    .line 1428385
    invoke-super {p0, p1, p2, p3}, LX/1OX;->a(Landroid/support/v7/widget/RecyclerView;II)V

    .line 1428386
    iget-object v0, p0, LX/902;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->f:LX/91k;

    .line 1428387
    invoke-virtual {v0}, LX/5Je;->k()LX/1OR;

    move-result-object v1

    check-cast v1, LX/3wu;

    invoke-virtual {v1}, LX/1P1;->l()I

    move-result v1

    move v0, v1

    .line 1428388
    iget-object v1, p0, LX/902;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;

    iget-object v1, v1, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->h:LX/90G;

    .line 1428389
    iget v2, v1, LX/90G;->b:I

    move v1, v2

    .line 1428390
    if-eq v1, v0, :cond_1

    .line 1428391
    iget-object v1, p0, LX/902;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;

    iget-object v1, v1, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->h:LX/90G;

    invoke-virtual {v1}, LX/90G;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1428392
    iget-object v1, p0, LX/902;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;

    iget-object v1, v1, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->r:LX/91D;

    iget-object v2, p0, LX/902;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;

    invoke-static {v2}, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->l(Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;)Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    move-result-object v2

    .line 1428393
    iget-object p1, v2, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v2, p1

    .line 1428394
    const-string p1, "feeling_selector_first_scroll"

    invoke-static {p1, v2}, LX/91D;->b(Ljava/lang/String;Ljava/lang/String;)LX/8yQ;

    move-result-object p1

    .line 1428395
    iget-object p2, p1, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object p1, p2

    .line 1428396
    iget-object p2, v1, LX/91D;->a:LX/0Zb;

    invoke-interface {p2, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1428397
    :cond_0
    iget-object v1, p0, LX/902;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;

    iget-object v1, v1, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->h:LX/90G;

    .line 1428398
    iput v0, v1, LX/90G;->b:I

    .line 1428399
    :cond_1
    iget-object v0, p0, LX/902;->b:Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->c:LX/91v;

    iget-object v1, p0, LX/902;->a:LX/62T;

    invoke-virtual {v1}, LX/1P1;->n()I

    move-result v1

    invoke-virtual {v0, v1}, LX/91v;->a(I)V

    .line 1428400
    return-void
.end method
