.class public LX/9WM;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;

.field public b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/9To;


# direct methods
.method public constructor <init>(Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryInterfaces$NegativeFeedbackPromptQueryFragment$Responses;",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1501097
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1501098
    iput-object p1, p0, LX/9WM;->a:Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;

    .line 1501099
    iput-object p2, p0, LX/9WM;->b:Ljava/util/Set;

    .line 1501100
    invoke-static {p0}, LX/9WM;->h(LX/9WM;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1501101
    sget-object v0, LX/9To;->COMPLETED:LX/9To;

    iput-object v0, p0, LX/9WM;->c:LX/9To;

    .line 1501102
    :goto_0
    return-void

    .line 1501103
    :cond_0
    sget-object v0, LX/9To;->INITIAL:LX/9To;

    iput-object v0, p0, LX/9WM;->c:LX/9To;

    goto :goto_0
.end method

.method public static h(LX/9WM;)Z
    .locals 2

    .prologue
    .line 1501096
    iget-object v0, p0, LX/9WM;->b:Ljava/util/Set;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9WM;->b:Ljava/util/Set;

    iget-object v1, p0, LX/9WM;->a:Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;

    invoke-virtual {v1}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->gl_()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, LX/9WM;->a:Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;

    invoke-virtual {v0}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1501091
    invoke-static {p0}, LX/9WM;->h(LX/9WM;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1501092
    iget-object v0, p0, LX/9WM;->a:Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;

    invoke-virtual {v0}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->c()Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel$CompletedSubtitleModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9WM;->a:Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;

    invoke-virtual {v0}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->c()Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel$CompletedSubtitleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel$CompletedSubtitleModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 1501093
    :goto_0
    return-object v0

    .line 1501094
    :cond_0
    const-string v0, ""

    goto :goto_0

    .line 1501095
    :cond_1
    iget-object v0, p0, LX/9WM;->a:Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;

    invoke-virtual {v0}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->j()Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel$SubtitleModel;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/9WM;->a:Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;

    invoke-virtual {v0}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->j()Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel$SubtitleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel$SubtitleModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const-string v0, ""

    goto :goto_0
.end method

.method public final c()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;
    .locals 1

    .prologue
    .line 1501088
    iget-object v0, p0, LX/9WM;->a:Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;

    if-eqz v0, :cond_0

    .line 1501089
    iget-object v0, p0, LX/9WM;->a:Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;

    invoke-virtual {v0}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->gl_()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v0

    .line 1501090
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
