.class public LX/AOB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ANo;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/AMO;

.field public final c:LX/6Jt;

.field private d:Landroid/view/ViewGroup;

.field public final e:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public final f:Landroid/content/Context;

.field public final g:LX/ANk;

.field public h:LX/AON;

.field public i:LX/6KY;

.field public j:LX/BAC;

.field private k:Landroid/view/View;

.field public l:LX/AO4;

.field public m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/AN1;",
            ">;"
        }
    .end annotation
.end field

.field public final n:LX/BA5;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1668749
    const-class v0, LX/AOB;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AOB;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/BAD;LX/AMO;LX/6Jt;Landroid/view/ViewGroup;Lcom/facebook/widget/recyclerview/BetterRecyclerView;Landroid/content/Context;)V
    .locals 8
    .param p3    # LX/6Jt;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Landroid/view/ViewGroup;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/widget/recyclerview/BetterRecyclerView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1668750
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1668751
    new-instance v0, LX/AO5;

    invoke-direct {v0, p0}, LX/AO5;-><init>(LX/AOB;)V

    iput-object v0, p0, LX/AOB;->g:LX/ANk;

    .line 1668752
    iput-object p2, p0, LX/AOB;->b:LX/AMO;

    .line 1668753
    iput-object p3, p0, LX/AOB;->c:LX/6Jt;

    .line 1668754
    iput-object p4, p0, LX/AOB;->d:Landroid/view/ViewGroup;

    .line 1668755
    iput-object p5, p0, LX/AOB;->e:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1668756
    iput-object p6, p0, LX/AOB;->f:Landroid/content/Context;

    .line 1668757
    const/4 v2, 0x1

    .line 1668758
    const/16 v4, 0x7530

    const v5, 0x11170

    const/16 v6, 0x3a98

    const/4 v7, 0x0

    move v3, v2

    invoke-static/range {v2 .. v7}, LX/7ex;->a(ZZIIIZ)LX/7ex;

    move-result-object v2

    move-object v0, v2

    .line 1668759
    invoke-virtual {p1, v0}, LX/BAD;->a(LX/7ex;)LX/BAC;

    move-result-object v0

    iput-object v0, p0, LX/AOB;->j:LX/BAC;

    .line 1668760
    new-instance v0, LX/6KY;

    iget-object v1, p0, LX/AOB;->j:LX/BAC;

    invoke-direct {v0, v1}, LX/6KY;-><init>(LX/61B;)V

    iput-object v0, p0, LX/AOB;->i:LX/6KY;

    .line 1668761
    new-instance v0, LX/BA5;

    invoke-direct {v0}, LX/BA5;-><init>()V

    iput-object v0, p0, LX/AOB;->n:LX/BA5;

    .line 1668762
    iget-object v0, p0, LX/AOB;->n:LX/BA5;

    iget-object v1, p0, LX/AOB;->f:Landroid/content/Context;

    .line 1668763
    iput-object v1, v0, LX/BA5;->a:Landroid/content/Context;

    .line 1668764
    return-void
.end method

.method public static a$redex0(LX/AOB;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1668765
    iget-object v0, p0, LX/AOB;->b:LX/AMO;

    invoke-virtual {v0}, LX/AMO;->a()V

    .line 1668766
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1668767
    invoke-virtual {p0, v0}, LX/AOB;->a(LX/0Px;)V

    .line 1668768
    iget-object v0, p0, LX/AOB;->b:LX/AMO;

    const/4 v1, 0x1

    new-instance v2, LX/AOA;

    invoke-direct {v2, p0}, LX/AOA;-><init>(LX/AOB;)V

    const/4 v3, 0x0

    invoke-virtual {v0, v1, p1, v2, v3}, LX/AMO;->a(ZLjava/lang/String;LX/AMN;LX/AZo;)V

    .line 1668769
    return-void
.end method

.method public static e(LX/AOB;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/AN1;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1668770
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    const-string v1, "None"

    .line 1668771
    new-instance v2, LX/AN1;

    const-string v3, ""

    invoke-direct {v2, v1, v3}, LX/AN1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v2

    .line 1668772
    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    iget-object v1, p0, LX/AOB;->l:LX/AO4;

    .line 1668773
    iget-object v2, v1, LX/AO4;->c:LX/0Px;

    move-object v1, v2

    .line 1668774
    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1668775
    return-object v0
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 4

    .prologue
    .line 1668776
    iget-object v0, p0, LX/AOB;->k:Landroid/view/View;

    if-nez v0, :cond_0

    .line 1668777
    iget-object v0, p0, LX/AOB;->f:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030235

    iget-object v2, p0, LX/AOB;->d:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/AOB;->k:Landroid/view/View;

    .line 1668778
    iget-object v0, p0, LX/AOB;->k:Landroid/view/View;

    const v1, 0x7f0d088c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    .line 1668779
    new-instance v1, LX/AO6;

    invoke-direct {v1, p0}, LX/AO6;-><init>(LX/AOB;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1668780
    :cond_0
    iget-object v0, p0, LX/AOB;->k:Landroid/view/View;

    return-object v0
.end method

.method public final a(LX/0Px;)V
    .locals 2
    .param p1    # LX/0Px;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/AN1;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1668781
    iget-object v0, p0, LX/AOB;->l:LX/AO4;

    .line 1668782
    iput-object p1, v0, LX/AO4;->c:LX/0Px;

    .line 1668783
    invoke-static {p0}, LX/AOB;->e(LX/AOB;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/AOB;->m:LX/0Px;

    .line 1668784
    iget-object v0, p0, LX/AOB;->h:LX/AON;

    iget-object v1, p0, LX/AOB;->m:LX/0Px;

    invoke-virtual {v0, v1}, LX/AOH;->a(LX/0Px;)V

    .line 1668785
    return-void
.end method

.method public final a(LX/AN2;)V
    .locals 5

    .prologue
    .line 1668786
    iget-object v0, p0, LX/AOB;->n:LX/BA5;

    .line 1668787
    iget-object v1, p1, LX/AN2;->c:Ljava/lang/String;

    move-object v1, v1

    .line 1668788
    iget-object v2, p1, LX/AN2;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1668789
    iget-object v3, p1, LX/AN2;->d:Ljava/lang/String;

    move-object v3, v3

    .line 1668790
    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, LX/BA5;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)LX/BA5;

    .line 1668791
    return-void
.end method

.method public final a(Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;)V
    .locals 2

    .prologue
    .line 1668792
    iget-object v0, p0, LX/AOB;->e:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, LX/AOB;->h:LX/AON;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1668793
    return-void
.end method

.method public final b(Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;)V
    .locals 2

    .prologue
    .line 1668794
    iget-object v0, p0, LX/AOB;->e:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1668795
    return-void
.end method
