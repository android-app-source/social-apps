.class public final LX/8wI;
.super Landroid/view/View;
.source ""


# direct methods
.method private static a(II)I
    .locals 2

    .prologue
    .line 1422543
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 1422544
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 1422545
    sparse-switch v1, :sswitch_data_0

    .line 1422546
    :goto_0
    :sswitch_0
    return p0

    .line 1422547
    :sswitch_1
    invoke-static {p0, v0}, Ljava/lang/Math;->min(II)I

    move-result p0

    goto :goto_0

    :sswitch_2
    move p0, v0

    .line 1422548
    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x0 -> :sswitch_0
        0x40000000 -> :sswitch_2
    .end sparse-switch
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 1422551
    return-void
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 1422549
    invoke-virtual {p0}, LX/8wI;->getSuggestedMinimumWidth()I

    move-result v0

    invoke-static {v0, p1}, LX/8wI;->a(II)I

    move-result v0

    invoke-virtual {p0}, LX/8wI;->getSuggestedMinimumHeight()I

    move-result v1

    invoke-static {v1, p2}, LX/8wI;->a(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/8wI;->setMeasuredDimension(II)V

    .line 1422550
    return-void
.end method
