.class public LX/9i4;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1527192
    const-class v0, LX/9i4;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/9i4;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1527189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1527190
    iput-object p1, p0, LX/9i4;->b:LX/03V;

    .line 1527191
    return-void
.end method

.method public static b(LX/0QB;)LX/9i4;
    .locals 2

    .prologue
    .line 1527173
    new-instance v1, LX/9i4;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-direct {v1, v0}, LX/9i4;-><init>(LX/03V;)V

    .line 1527174
    return-object v1
.end method


# virtual methods
.method public final a(LX/0Px;)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1527175
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1527176
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1527177
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_4

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1527178
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 1527179
    invoke-static {v0}, LX/1VO;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1527180
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1527181
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1527182
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v5

    if-nez v5, :cond_2

    .line 1527183
    const-string v0, "styleList == null"

    .line 1527184
    :goto_2
    iget-object v5, p0, LX/9i4;->b:LX/03V;

    sget-object v6, LX/9i4;->a:Ljava/lang/String;

    invoke-virtual {v5, v6, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1527185
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1527186
    const-string v0, "styleList.size == 0"

    goto :goto_2

    .line 1527187
    :cond_3
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "first attachment:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v6

    invoke-virtual {v6, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " styleList.size == "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 1527188
    :cond_4
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method
