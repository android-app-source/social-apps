.class public final LX/9jO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/9jN;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/9jQ;


# direct methods
.method public constructor <init>(LX/9jQ;)V
    .locals 0

    .prologue
    .line 1529782
    iput-object p1, p0, LX/9jO;->a:LX/9jQ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1529783
    const/4 v1, 0x0

    .line 1529784
    iget-object v0, p0, LX/9jO;->a:LX/9jQ;

    iget-object v0, v0, LX/9jQ;->a:LX/9jR;

    .line 1529785
    iget-object v2, v0, LX/9jR;->d:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 1529786
    const/4 v2, 0x2

    const-class v3, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel;

    .line 1529787
    const/4 v4, 0x0

    .line 1529788
    :try_start_0
    iget-object v5, v0, LX/9jR;->c:LX/9k8;

    invoke-virtual {v5, v2}, LX/9k8;->a(I)Ljava/lang/String;

    move-result-object v5

    .line 1529789
    if-eqz v5, :cond_0

    .line 1529790
    iget-object v6, v0, LX/9jR;->b:LX/0lB;

    invoke-virtual {v6, v5, v3}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 1529791
    :cond_0
    :goto_0
    move-object v2, v4

    .line 1529792
    check-cast v2, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel;

    move-object v0, v2

    .line 1529793
    if-nez v0, :cond_1

    .line 1529794
    iget-object v0, p0, LX/9jO;->a:LX/9jQ;

    .line 1529795
    iget-object v2, v0, LX/9jQ;->d:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 1529796
    invoke-static {}, LX/5m3;->e()LX/5ly;

    move-result-object v2

    .line 1529797
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    .line 1529798
    iget-object v3, v0, LX/9jQ;->c:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    const v3, 0x34d4baf2

    invoke-static {v2, v3}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1529799
    iget-object v3, v2, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v2, v3

    .line 1529800
    check-cast v2, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel;

    move-object v2, v2

    .line 1529801
    if-eqz v2, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1529802
    iget-object v0, p0, LX/9jO;->a:LX/9jQ;

    iget-object v0, v0, LX/9jQ;->a:LX/9jR;

    invoke-virtual {v0, v2}, LX/9jR;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel;)V

    move-object v0, v2

    .line 1529803
    :cond_1
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 1529804
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v3

    .line 1529805
    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel$PlaceVisitsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel$PlaceVisitsModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    :goto_2
    if-ge v1, v5, :cond_4

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel$PlaceVisitsModel$NodesModel;

    .line 1529806
    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinHistoryMostRecentQueryModel$PlaceVisitsModel$NodesModel;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    .line 1529807
    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v6

    .line 1529808
    invoke-virtual {v3, v6}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 1529809
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1529810
    invoke-virtual {v3, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1529811
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    move v0, v1

    .line 1529812
    goto :goto_1

    .line 1529813
    :cond_4
    new-instance v0, LX/9jN;

    invoke-static {v2}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {v0, v1}, LX/9jN;-><init>(Ljava/util/List;)V

    .line 1529814
    sget-object v1, LX/9jM;->RECENT:LX/9jM;

    .line 1529815
    iput-object v1, v0, LX/9jN;->g:LX/9jM;

    .line 1529816
    return-object v0

    .line 1529817
    :catch_0
    move-exception v5

    .line 1529818
    sget-object v6, LX/9jR;->a:Ljava/lang/Class;

    const-string v7, "failed to de-serialize model"

    invoke-static {v6, v7, v5}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0
.end method
