.class public LX/A5m;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/9kE;

.field private final c:LX/0tX;

.field public final d:LX/A5k;

.field private final e:LX/8uo;

.field public final f:Landroid/os/Handler;

.field private final g:LX/03V;

.field private final h:LX/0Zb;

.field public i:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;>;"
        }
    .end annotation
.end field

.field public final j:Ljava/lang/Runnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1622325
    const-class v0, LX/A5m;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/A5m;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/9kE;LX/0tX;LX/A5k;LX/8uo;LX/03V;LX/0Zb;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1622315
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1622316
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/A5m;->f:Landroid/os/Handler;

    .line 1622317
    new-instance v0, Lcom/facebook/tagging/conversion/FriendSuggestionsRunner$2;

    invoke-direct {v0, p0}, Lcom/facebook/tagging/conversion/FriendSuggestionsRunner$2;-><init>(LX/A5m;)V

    iput-object v0, p0, LX/A5m;->j:Ljava/lang/Runnable;

    .line 1622318
    iput-object p1, p0, LX/A5m;->b:LX/9kE;

    .line 1622319
    iput-object p2, p0, LX/A5m;->c:LX/0tX;

    .line 1622320
    iput-object p3, p0, LX/A5m;->d:LX/A5k;

    .line 1622321
    iput-object p4, p0, LX/A5m;->e:LX/8uo;

    .line 1622322
    iput-object p5, p0, LX/A5m;->g:LX/03V;

    .line 1622323
    iput-object p6, p0, LX/A5m;->h:LX/0Zb;

    .line 1622324
    return-void
.end method

.method public static synthetic a(LX/A5m;LX/0Px;)LX/0Px;
    .locals 13

    .prologue
    .line 1622326
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 1622327
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v6

    const/4 v0, 0x0

    move v4, v0

    :goto_0
    if-ge v4, v6, :cond_2

    invoke-virtual {p1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithGroupsModel;

    .line 1622328
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 1622329
    invoke-virtual {v0}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithGroupsModel;->a()LX/0Px;

    move-result-object v8

    .line 1622330
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    const/4 v1, 0x0

    move v3, v1

    :goto_1
    if-ge v3, v9, :cond_1

    invoke-virtual {v8, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithGroupsModel$UsersModel;

    .line 1622331
    invoke-virtual {v1}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithGroupsModel$UsersModel;->b()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-eqz v2, :cond_0

    .line 1622332
    invoke-virtual {v1}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithGroupsModel$UsersModel;->b()LX/1vs;

    move-result-object v2

    iget-object v10, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 1622333
    const/4 v11, 0x0

    invoke-virtual {v10, v2, v11}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    .line 1622334
    :goto_2
    new-instance v10, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    new-instance v11, LX/0XI;

    invoke-direct {v11}, LX/0XI;-><init>()V

    sget-object v12, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-virtual {v1}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithGroupsModel$UsersModel;->c()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v11, v12, p0}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    move-result-object v11

    invoke-virtual {v1}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithGroupsModel$UsersModel;->d()Ljava/lang/String;

    move-result-object v1

    .line 1622335
    iput-object v1, v11, LX/0XI;->h:Ljava/lang/String;

    .line 1622336
    move-object v1, v11

    .line 1622337
    iput-object v2, v1, LX/0XI;->n:Ljava/lang/String;

    .line 1622338
    move-object v1, v1

    .line 1622339
    invoke-virtual {v1}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v1

    invoke-static {v1}, LX/8uo;->a(Lcom/facebook/user/model/User;)Lcom/facebook/user/model/User;

    move-result-object v1

    invoke-direct {v10, v1}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;-><init>(Lcom/facebook/user/model/User;)V

    invoke-virtual {v7, v10}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1622340
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    .line 1622341
    :cond_0
    const/4 v2, 0x0

    goto :goto_2

    .line 1622342
    :cond_1
    new-instance v1, LX/8vB;

    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithGroupsModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, LX/8vB;-><init>(Ljava/util/List;Ljava/lang/String;)V

    invoke-virtual {v5, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1622343
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 1622344
    :cond_2
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 1622345
    return-object v0
.end method

.method public static a(LX/0QB;)LX/A5m;
    .locals 1

    .prologue
    .line 1622296
    invoke-static {p0}, LX/A5m;->b(LX/0QB;)LX/A5m;

    move-result-object v0

    return-object v0
.end method

.method public static a$redex0(LX/A5m;Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel;)LX/0Px;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLInterfaces$FBPersonFriendTagSuggestionsQuery$;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1622297
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1622298
    invoke-virtual {p1}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel;->b()Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v4

    .line 1622299
    :goto_0
    invoke-virtual {v4}, LX/0Rc;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1622300
    invoke-virtual {v4}, LX/0Rc;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel$EdgesModel;->a()Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel$EdgesModel$NodeModel;

    move-result-object v5

    .line 1622301
    if-nez v5, :cond_0

    move v0, v1

    :goto_1
    if-eqz v0, :cond_2

    .line 1622302
    iget-object v0, p0, LX/A5m;->g:LX/03V;

    sget-object v6, LX/A5m;->a:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Invalid friendSuggestion "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v6, v5}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1622303
    :cond_0
    invoke-virtual {v5}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel$EdgesModel$NodeModel;->b()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-nez v0, :cond_1

    move v0, v1

    goto :goto_1

    :cond_1
    move v0, v2

    goto :goto_1

    .line 1622304
    :cond_2
    invoke-virtual {v5}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel$EdgesModel$NodeModel;->b()LX/1vs;

    move-result-object v0

    iget-object v6, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1622305
    new-instance v7, LX/0XI;

    invoke-direct {v7}, LX/0XI;-><init>()V

    sget-object v8, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-virtual {v5}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel$EdgesModel$NodeModel;->c()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    move-result-object v7

    invoke-virtual {v5}, Lcom/facebook/tagging/graphql/protocol/TagSearchGraphQLModels$FBPersonFriendTagSuggestionsQueryModel$SuggestedWithTagsModel$EdgesModel$NodeModel;->d()Ljava/lang/String;

    move-result-object v5

    .line 1622306
    iput-object v5, v7, LX/0XI;->h:Ljava/lang/String;

    .line 1622307
    move-object v5, v7

    .line 1622308
    invoke-virtual {v6, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 1622309
    iput-object v0, v5, LX/0XI;->n:Ljava/lang/String;

    .line 1622310
    move-object v0, v5

    .line 1622311
    invoke-virtual {v0}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v0

    .line 1622312
    new-instance v5, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    invoke-static {v0}, LX/8uo;->a(Lcom/facebook/user/model/User;)Lcom/facebook/user/model/User;

    move-result-object v0

    invoke-direct {v5, v0}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;-><init>(Lcom/facebook/user/model/User;)V

    .line 1622313
    invoke-virtual {v3, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1622314
    :cond_3
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a$redex0(LX/A5m;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1622294
    iget-object v0, p0, LX/A5m;->h:LX/0Zb;

    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "people_tag_suggestions_fetch"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "fetch_status"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1622295
    return-void
.end method

.method public static b(LX/0QB;)LX/A5m;
    .locals 7

    .prologue
    .line 1622292
    new-instance v0, LX/A5m;

    invoke-static {p0}, LX/9kE;->b(LX/0QB;)LX/9kE;

    move-result-object v1

    check-cast v1, LX/9kE;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-static {p0}, LX/A5k;->a(LX/0QB;)LX/A5k;

    move-result-object v3

    check-cast v3, LX/A5k;

    invoke-static {p0}, LX/8uo;->a(LX/0QB;)LX/8uo;

    move-result-object v4

    check-cast v4, LX/8uo;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    invoke-direct/range {v0 .. v6}, LX/A5m;-><init>(LX/9kE;LX/0tX;LX/A5k;LX/8uo;LX/03V;LX/0Zb;)V

    .line 1622293
    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1622259
    iget-object v0, p0, LX/A5m;->d:LX/A5k;

    .line 1622260
    iget-object p0, v0, LX/A5k;->c:Ljava/lang/String;

    move-object v0, p0

    .line 1622261
    return-object v0
.end method

.method public final a(LX/0TF;LX/0TF;)V
    .locals 9
    .param p1    # LX/0TF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/0TF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TF",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;>;",
            "LX/0TF",
            "<",
            "LX/0Px",
            "<",
            "LX/8vB;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1622265
    invoke-virtual {p0}, LX/A5m;->b()V

    .line 1622266
    iget-object v0, p0, LX/A5m;->d:LX/A5k;

    .line 1622267
    iget-object v5, v0, LX/A5k;->b:LX/0Px;

    if-eqz v5, :cond_3

    .line 1622268
    iget-wide v5, v0, LX/A5k;->e:J

    const-wide/32 v7, 0x927c0

    add-long/2addr v5, v7

    iget-object v7, v0, LX/A5k;->f:LX/0SG;

    invoke-interface {v7}, LX/0SG;->a()J

    move-result-wide v7

    cmp-long v5, v5, v7

    if-gez v5, :cond_3

    .line 1622269
    const/4 v5, 0x0

    .line 1622270
    :goto_0
    move-object v0, v5

    .line 1622271
    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 1622272
    invoke-interface {p2, v0}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 1622273
    :cond_0
    iget-object v0, p0, LX/A5m;->d:LX/A5k;

    .line 1622274
    iget-object v5, v0, LX/A5k;->a:LX/0Px;

    if-eqz v5, :cond_4

    .line 1622275
    iget-wide v5, v0, LX/A5k;->d:J

    const-wide/32 v7, 0x927c0

    add-long/2addr v5, v7

    iget-object v7, v0, LX/A5k;->f:LX/0SG;

    invoke-interface {v7}, LX/0SG;->a()J

    move-result-wide v7

    cmp-long v5, v5, v7

    if-gez v5, :cond_4

    .line 1622276
    const/4 v5, 0x0

    .line 1622277
    :goto_1
    move-object v0, v5

    .line 1622278
    if-eqz v0, :cond_2

    .line 1622279
    if-eqz p1, :cond_1

    .line 1622280
    invoke-interface {p1, v0}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 1622281
    :cond_1
    :goto_2
    return-void

    .line 1622282
    :cond_2
    iput-object p1, p0, LX/A5m;->i:LX/0TF;

    .line 1622283
    iget-object v0, p0, LX/A5m;->f:Landroid/os/Handler;

    iget-object v1, p0, LX/A5m;->j:Ljava/lang/Runnable;

    const-wide/16 v2, 0xfa0

    const v4, 0x64ff64f6

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1622284
    const-string v0, "start"

    invoke-static {p0, v0}, LX/A5m;->a$redex0(LX/A5m;Ljava/lang/String;)V

    .line 1622285
    iget-object v0, p0, LX/A5m;->b:LX/9kE;

    iget-object v1, p0, LX/A5m;->c:LX/0tX;

    .line 1622286
    invoke-static {}, LX/8oM;->b()LX/8oK;

    move-result-object v2

    .line 1622287
    const-string v3, "count"

    const-string v4, "10"

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v4, "profile_image_size"

    const-string v5, "80"

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1622288
    invoke-static {}, LX/8oM;->b()LX/8oK;

    move-result-object v3

    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    .line 1622289
    iget-object v4, v2, LX/0gW;->e:LX/0w7;

    move-object v2, v4

    .line 1622290
    invoke-virtual {v3, v2}, LX/0zO;->a(LX/0w7;)LX/0zO;

    move-result-object v2

    move-object v2, v2

    .line 1622291
    invoke-virtual {v1, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    invoke-static {v1}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, LX/A5l;

    invoke-direct {v2, p0, p2, p1}, LX/A5l;-><init>(LX/A5m;LX/0TF;LX/0TF;)V

    invoke-virtual {v0, v1, v2}, LX/9kE;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_2

    :cond_3
    iget-object v5, v0, LX/A5k;->b:LX/0Px;

    goto :goto_0

    :cond_4
    iget-object v5, v0, LX/A5k;->a:LX/0Px;

    goto :goto_1
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1622262
    iget-object v0, p0, LX/A5m;->f:Landroid/os/Handler;

    iget-object v1, p0, LX/A5m;->j:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1622263
    iget-object v0, p0, LX/A5m;->b:LX/9kE;

    invoke-virtual {v0}, LX/9kE;->c()V

    .line 1622264
    return-void
.end method
