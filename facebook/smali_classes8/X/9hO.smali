.class public LX/9hO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1bf;

.field public final b:LX/9hP;


# direct methods
.method public constructor <init>(LX/9hP;LX/1bf;)V
    .locals 1

    .prologue
    .line 1526557
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1526558
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9hP;

    iput-object v0, p0, LX/9hO;->b:LX/9hP;

    .line 1526559
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1bf;

    iput-object v0, p0, LX/9hO;->a:LX/1bf;

    .line 1526560
    return-void
.end method

.method public static a(Lcom/facebook/drawee/view/DraweeView;LX/1bf;)LX/9hO;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1526561
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1526562
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1526563
    :cond_0
    const/4 v0, 0x0

    .line 1526564
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, LX/9hO;

    invoke-static {p0}, LX/9hP;->a(Lcom/facebook/drawee/view/DraweeView;)LX/9hP;

    move-result-object v1

    invoke-direct {v0, v1, p1}, LX/9hO;-><init>(LX/9hP;LX/1bf;)V

    goto :goto_0
.end method
