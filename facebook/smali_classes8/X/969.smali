.class public final enum LX/969;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/969;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/969;

.field public static final enum PAGE_NAME:LX/969;

.field public static final enum PAGE_PHOTO:LX/969;

.field public static final enum PLACE_CATEGORY:LX/969;

.field public static final enum PLACE_CITY:LX/969;

.field public static final enum PLACE_COORDINATES:LX/969;

.field public static final enum PLACE_STREET_ADDRESS:LX/969;

.field public static final enum PLACE_ZIP_CODE:LX/969;


# instance fields
.field public final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1438247
    new-instance v0, LX/969;

    const-string v1, "PAGE_NAME"

    const-string v2, "PAGE_NAME"

    invoke-direct {v0, v1, v4, v2}, LX/969;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/969;->PAGE_NAME:LX/969;

    .line 1438248
    new-instance v0, LX/969;

    const-string v1, "PAGE_PHOTO"

    const-string v2, "PAGE_PHOTO"

    invoke-direct {v0, v1, v5, v2}, LX/969;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/969;->PAGE_PHOTO:LX/969;

    .line 1438249
    new-instance v0, LX/969;

    const-string v1, "PLACE_CATEGORY"

    const-string v2, "PLACE_CATEGORY"

    invoke-direct {v0, v1, v6, v2}, LX/969;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/969;->PLACE_CATEGORY:LX/969;

    .line 1438250
    new-instance v0, LX/969;

    const-string v1, "PLACE_CITY"

    const-string v2, "PLACE_CITY"

    invoke-direct {v0, v1, v7, v2}, LX/969;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/969;->PLACE_CITY:LX/969;

    .line 1438251
    new-instance v0, LX/969;

    const-string v1, "PLACE_COORDINATES"

    const-string v2, "PLACE_COORDINvATES"

    invoke-direct {v0, v1, v8, v2}, LX/969;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/969;->PLACE_COORDINATES:LX/969;

    .line 1438252
    new-instance v0, LX/969;

    const-string v1, "PLACE_STREET_ADDRESS"

    const/4 v2, 0x5

    const-string v3, "PLACE_STREET_ADDRESS"

    invoke-direct {v0, v1, v2, v3}, LX/969;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/969;->PLACE_STREET_ADDRESS:LX/969;

    .line 1438253
    new-instance v0, LX/969;

    const-string v1, "PLACE_ZIP_CODE"

    const/4 v2, 0x6

    const-string v3, "PLACE_ZIP_CODE"

    invoke-direct {v0, v1, v2, v3}, LX/969;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/969;->PLACE_ZIP_CODE:LX/969;

    .line 1438254
    const/4 v0, 0x7

    new-array v0, v0, [LX/969;

    sget-object v1, LX/969;->PAGE_NAME:LX/969;

    aput-object v1, v0, v4

    sget-object v1, LX/969;->PAGE_PHOTO:LX/969;

    aput-object v1, v0, v5

    sget-object v1, LX/969;->PLACE_CATEGORY:LX/969;

    aput-object v1, v0, v6

    sget-object v1, LX/969;->PLACE_CITY:LX/969;

    aput-object v1, v0, v7

    sget-object v1, LX/969;->PLACE_COORDINATES:LX/969;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/969;->PLACE_STREET_ADDRESS:LX/969;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/969;->PLACE_ZIP_CODE:LX/969;

    aput-object v2, v0, v1

    sput-object v0, LX/969;->$VALUES:[LX/969;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1438244
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1438245
    iput-object p3, p0, LX/969;->name:Ljava/lang/String;

    .line 1438246
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/969;
    .locals 1

    .prologue
    .line 1438255
    const-class v0, LX/969;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/969;

    return-object v0
.end method

.method public static values()[LX/969;
    .locals 1

    .prologue
    .line 1438243
    sget-object v0, LX/969;->$VALUES:[LX/969;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/969;

    return-object v0
.end method
