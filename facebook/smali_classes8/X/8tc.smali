.class public final LX/8tc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;


# instance fields
.field public final synthetic a:Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;


# direct methods
.method public constructor <init>(Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;)V
    .locals 0

    .prologue
    .line 1413563
    iput-object p1, p0, LX/8tc;->a:Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onError(Landroid/media/MediaPlayer;II)Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 1413564
    iget-object v0, p0, LX/8tc;->a:Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;

    iget-object v0, v0, Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;->e:LX/03V;

    const-string v1, "VideoTrimmingPreviewView_FAILED_TO_PREPARE_VIDEO"

    const-string v2, "MediaPlayer error (%d, %d)"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1413565
    iget-object v0, p0, LX/8tc;->a:Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;

    sget-object v1, LX/8tI;->ERROR:LX/8tI;

    .line 1413566
    invoke-virtual {v0, v1}, LX/8tJ;->a(LX/8tI;)V

    .line 1413567
    iget-object v0, p0, LX/8tc;->a:Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;

    iget-object v0, v0, Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;->f:LX/1CX;

    iget-object v1, p0, LX/8tc;->a:Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;

    invoke-virtual {v1}, Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, LX/4mm;->a(Landroid/content/res/Resources;)LX/4mn;

    move-result-object v1

    const v2, 0x7f080d60

    invoke-virtual {v1, v2}, LX/4mn;->b(I)LX/4mn;

    move-result-object v1

    invoke-virtual {v1}, LX/4mn;->l()LX/4mm;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1CX;->a(LX/4mm;)LX/2EJ;

    .line 1413568
    return v6
.end method
