.class public final LX/9QK;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1487015
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1487016
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1487017
    :goto_0
    return v1

    .line 1487018
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1487019
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1487020
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1487021
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1487022
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1487023
    const-string v4, "weather_condition"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1487024
    const/4 v3, 0x0

    .line 1487025
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v4, :cond_9

    .line 1487026
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1487027
    :goto_2
    move v2, v3

    .line 1487028
    goto :goto_1

    .line 1487029
    :cond_2
    const-string v4, "weather_url"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1487030
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 1487031
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1487032
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1487033
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1487034
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 1487035
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1487036
    :cond_6
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_8

    .line 1487037
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1487038
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1487039
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_6

    if-eqz v5, :cond_6

    .line 1487040
    const-string v6, "description"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1487041
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_3

    .line 1487042
    :cond_7
    const-string v6, "temperature"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1487043
    invoke-static {p0, p1}, LX/9QJ;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_3

    .line 1487044
    :cond_8
    const/4 v5, 0x2

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1487045
    invoke-virtual {p1, v3, v4}, LX/186;->b(II)V

    .line 1487046
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 1487047
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_9
    move v2, v3

    move v4, v3

    goto :goto_3
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 8

    .prologue
    .line 1487048
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1487049
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1487050
    if-eqz v0, :cond_4

    .line 1487051
    const-string v1, "weather_condition"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487052
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1487053
    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1487054
    if-eqz v2, :cond_0

    .line 1487055
    const-string v3, "description"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487056
    invoke-virtual {p2, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1487057
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p0, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1487058
    if-eqz v2, :cond_3

    .line 1487059
    const-string v3, "temperature"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487060
    const-wide/16 v6, 0x0

    .line 1487061
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1487062
    const/4 v4, 0x0

    invoke-virtual {p0, v2, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1487063
    if-eqz v4, :cond_1

    .line 1487064
    const-string v5, "unit"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487065
    invoke-virtual {p2, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1487066
    :cond_1
    const/4 v4, 0x1

    invoke-virtual {p0, v2, v4, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v4

    .line 1487067
    cmpl-double v6, v4, v6

    if-eqz v6, :cond_2

    .line 1487068
    const-string v6, "value"

    invoke-virtual {p2, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487069
    invoke-virtual {p2, v4, v5}, LX/0nX;->a(D)V

    .line 1487070
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1487071
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1487072
    :cond_4
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1487073
    if-eqz v0, :cond_5

    .line 1487074
    const-string v1, "weather_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487075
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1487076
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1487077
    return-void
.end method
