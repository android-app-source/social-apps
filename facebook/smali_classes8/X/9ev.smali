.class public final LX/9ev;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V
    .locals 0

    .prologue
    .line 1520678
    iput-object p1, p0, LX/9ev;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 1520679
    iget-object v0, p0, LX/9ev;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->m:LX/9el;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9ev;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget-boolean v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->k:Z

    if-eqz v0, :cond_0

    .line 1520680
    iget-object v0, p0, LX/9ev;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8GX;

    iget-object v1, p0, LX/9ev;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->k:Landroid/net/Uri;

    iget-object v2, p0, LX/9ev;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v2, v2, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget v2, v2, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->h:I

    invoke-virtual {v0, v1, v2}, LX/8GX;->a(Landroid/net/Uri;I)V

    .line 1520681
    iget-object v0, p0, LX/9ev;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->m:LX/9el;

    iget-object v1, p0, LX/9ev;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->I:Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;

    iget v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;->h:I

    invoke-interface {v0, v1}, LX/9el;->a(I)I

    .line 1520682
    :cond_0
    iget-object v0, p0, LX/9ev;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->b(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;Z)V

    .line 1520683
    return-void
.end method
