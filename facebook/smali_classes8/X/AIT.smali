.class public final LX/AIT;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 22

    .prologue
    .line 1659601
    const/16 v18, 0x0

    .line 1659602
    const/16 v17, 0x0

    .line 1659603
    const/16 v16, 0x0

    .line 1659604
    const/4 v15, 0x0

    .line 1659605
    const/4 v14, 0x0

    .line 1659606
    const/4 v13, 0x0

    .line 1659607
    const/4 v12, 0x0

    .line 1659608
    const/4 v11, 0x0

    .line 1659609
    const/4 v10, 0x0

    .line 1659610
    const/4 v9, 0x0

    .line 1659611
    const/4 v8, 0x0

    .line 1659612
    const/4 v7, 0x0

    .line 1659613
    const/4 v6, 0x0

    .line 1659614
    const/4 v5, 0x0

    .line 1659615
    const/4 v4, 0x0

    .line 1659616
    const/4 v3, 0x0

    .line 1659617
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_1

    .line 1659618
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1659619
    const/4 v3, 0x0

    .line 1659620
    :goto_0
    return v3

    .line 1659621
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1659622
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_f

    .line 1659623
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v19

    .line 1659624
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1659625
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v20

    sget-object v21, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-eq v0, v1, :cond_1

    if-eqz v19, :cond_1

    .line 1659626
    const-string v20, "__type__"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_2

    const-string v20, "__typename"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_3

    .line 1659627
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v18

    goto :goto_1

    .line 1659628
    :cond_3
    const-string v20, "hdPlayableURLString"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_4

    .line 1659629
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    goto :goto_1

    .line 1659630
    :cond_4
    const-string v20, "hdPlaylist"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_5

    .line 1659631
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    goto :goto_1

    .line 1659632
    :cond_5
    const-string v20, "id"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_6

    .line 1659633
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    goto :goto_1

    .line 1659634
    :cond_6
    const-string v20, "image"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_7

    .line 1659635
    invoke-static/range {p0 .. p1}, LX/AIQ;->a(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 1659636
    :cond_7
    const-string v20, "is_playable"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_8

    .line 1659637
    const/4 v5, 0x1

    .line 1659638
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v13

    goto/16 :goto_1

    .line 1659639
    :cond_8
    const-string v20, "lowres"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_9

    .line 1659640
    invoke-static/range {p0 .. p1}, LX/AIR;->a(LX/15w;LX/186;)I

    move-result v12

    goto/16 :goto_1

    .line 1659641
    :cond_9
    const-string v20, "message"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_a

    .line 1659642
    invoke-static/range {p0 .. p1}, LX/AIS;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 1659643
    :cond_a
    const-string v20, "playable_duration"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_b

    .line 1659644
    const/4 v4, 0x1

    .line 1659645
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v10

    goto/16 :goto_1

    .line 1659646
    :cond_b
    const-string v20, "playable_url"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_c

    .line 1659647
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto/16 :goto_1

    .line 1659648
    :cond_c
    const-string v20, "sdPlaylist"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_d

    .line 1659649
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto/16 :goto_1

    .line 1659650
    :cond_d
    const-string v20, "sphericalURLString"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_e

    .line 1659651
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto/16 :goto_1

    .line 1659652
    :cond_e
    const-string v20, "width"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_0

    .line 1659653
    const/4 v3, 0x1

    .line 1659654
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    goto/16 :goto_1

    .line 1659655
    :cond_f
    const/16 v19, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1659656
    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1659657
    const/16 v18, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1659658
    const/16 v17, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1659659
    const/16 v16, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 1659660
    const/4 v15, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v14}, LX/186;->b(II)V

    .line 1659661
    if-eqz v5, :cond_10

    .line 1659662
    const/4 v5, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v13}, LX/186;->a(IZ)V

    .line 1659663
    :cond_10
    const/4 v5, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v12}, LX/186;->b(II)V

    .line 1659664
    const/4 v5, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v11}, LX/186;->b(II)V

    .line 1659665
    if-eqz v4, :cond_11

    .line 1659666
    const/16 v4, 0x8

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10, v5}, LX/186;->a(III)V

    .line 1659667
    :cond_11
    const/16 v4, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v9}, LX/186;->b(II)V

    .line 1659668
    const/16 v4, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v8}, LX/186;->b(II)V

    .line 1659669
    const/16 v4, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v7}, LX/186;->b(II)V

    .line 1659670
    if-eqz v3, :cond_12

    .line 1659671
    const/16 v3, 0xc

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6, v4}, LX/186;->a(III)V

    .line 1659672
    :cond_12
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1659673
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1659674
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1659675
    if-eqz v0, :cond_0

    .line 1659676
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1659677
    invoke-static {p0, p1, v2, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1659678
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1659679
    if-eqz v0, :cond_1

    .line 1659680
    const-string v1, "hdPlayableURLString"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1659681
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1659682
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1659683
    if-eqz v0, :cond_2

    .line 1659684
    const-string v1, "hdPlaylist"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1659685
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1659686
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1659687
    if-eqz v0, :cond_3

    .line 1659688
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1659689
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1659690
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1659691
    if-eqz v0, :cond_4

    .line 1659692
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1659693
    invoke-static {p0, v0, p2}, LX/AIQ;->a(LX/15i;ILX/0nX;)V

    .line 1659694
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1659695
    if-eqz v0, :cond_5

    .line 1659696
    const-string v1, "is_playable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1659697
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1659698
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1659699
    if-eqz v0, :cond_6

    .line 1659700
    const-string v1, "lowres"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1659701
    invoke-static {p0, v0, p2}, LX/AIR;->a(LX/15i;ILX/0nX;)V

    .line 1659702
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1659703
    if-eqz v0, :cond_7

    .line 1659704
    const-string v1, "message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1659705
    invoke-static {p0, v0, p2}, LX/AIS;->a(LX/15i;ILX/0nX;)V

    .line 1659706
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1659707
    if-eqz v0, :cond_8

    .line 1659708
    const-string v1, "playable_duration"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1659709
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1659710
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1659711
    if-eqz v0, :cond_9

    .line 1659712
    const-string v1, "playable_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1659713
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1659714
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1659715
    if-eqz v0, :cond_a

    .line 1659716
    const-string v1, "sdPlaylist"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1659717
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1659718
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1659719
    if-eqz v0, :cond_b

    .line 1659720
    const-string v1, "sphericalURLString"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1659721
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1659722
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1659723
    if-eqz v0, :cond_c

    .line 1659724
    const-string v1, "width"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1659725
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1659726
    :cond_c
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1659727
    return-void
.end method
