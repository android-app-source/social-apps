.class public final LX/A2P;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    .line 1613560
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 1613561
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1613562
    :goto_0
    return v6

    .line 1613563
    :cond_0
    const-string v9, "value"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1613564
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v0, v1

    .line 1613565
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_3

    .line 1613566
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1613567
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1613568
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 1613569
    const-string v9, "unit"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1613570
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 1613571
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1613572
    :cond_3
    const/4 v8, 0x2

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1613573
    invoke-virtual {p1, v6, v7}, LX/186;->b(II)V

    .line 1613574
    if-eqz v0, :cond_4

    move-object v0, p1

    .line 1613575
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1613576
    :cond_4
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto :goto_0

    :cond_5
    move v0, v6

    move-wide v2, v4

    move v7, v6

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1613577
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1613578
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1613579
    if-eqz v0, :cond_0

    .line 1613580
    const-string v1, "unit"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1613581
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1613582
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1613583
    cmpl-double v2, v0, v2

    if-eqz v2, :cond_1

    .line 1613584
    const-string v2, "value"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1613585
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1613586
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1613587
    return-void
.end method
