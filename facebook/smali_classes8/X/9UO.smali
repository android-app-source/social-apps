.class public final LX/9UO;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;)V
    .locals 0

    .prologue
    .line 1497971
    iput-object p1, p0, LX/9UO;->a:Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1497968
    iget-object v0, p0, LX/9UO;->a:Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;

    iget-object v0, v0, Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;->c:LX/9UZ;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/9UZ;->a(Z)V

    .line 1497969
    iget-object v0, p0, LX/9UO;->a:Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;

    iget-object v0, v0, Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;->f:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0824a6

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1497970
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 14

    .prologue
    .line 1497952
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x0

    .line 1497953
    iget-object v0, p0, LX/9UO;->a:Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;

    iget-object v0, v0, Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;->c:LX/9UZ;

    invoke-virtual {v0, v1}, LX/9UZ;->a(Z)V

    .line 1497954
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1497955
    if-eqz p1, :cond_2

    .line 1497956
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1497957
    check-cast v0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupsQueryModel;->a()Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupsQueryModel$ActorModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupsQueryModel$ActorModel;->a()Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupsQueryModel$ActorModel$GroupsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupsQueryModel$ActorModel$GroupsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    .line 1497958
    :goto_0
    if-ge v1, v4, :cond_2

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;

    .line 1497959
    invoke-virtual {v0}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->n()Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    move-result-object v5

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;->CAN_POST_AFTER_APPROVAL:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    if-eq v5, v6, :cond_0

    invoke-virtual {v0}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->n()Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    move-result-object v5

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;->CAN_POST_WITHOUT_APPROVAL:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    if-ne v5, v6, :cond_1

    .line 1497960
    :cond_0
    const/4 v13, 0x0

    .line 1497961
    invoke-virtual {v0}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->l()LX/1vs;

    move-result-object v7

    iget-object v12, v7, LX/1vs;->a:LX/15i;

    iget v7, v7, LX/1vs;->b:I

    invoke-virtual {v12, v7, v13}, LX/15i;->g(II)I

    move-result v7

    .line 1497962
    new-instance v8, Lcom/facebook/ipc/model/FacebookProfile;

    invoke-virtual {v0}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->m()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v9

    invoke-virtual {v0}, Lcom/facebook/katana/activity/profilelist/FetchGroupGraphQLModels$GroupBasicModel;->k()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v12, v7, v13}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x3

    invoke-direct/range {v8 .. v13}, Lcom/facebook/ipc/model/FacebookProfile;-><init>(JLjava/lang/String;Ljava/lang/String;I)V

    .line 1497963
    move-object v0, v8

    .line 1497964
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1497965
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1497966
    :cond_2
    iget-object v0, p0, LX/9UO;->a:Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;

    iget-object v0, v0, Lcom/facebook/katana/activity/profilelist/GroupSelectorFragment;->b:LX/9U9;

    check-cast v0, LX/9UM;

    invoke-virtual {v0, v2}, LX/9UM;->a(Ljava/util/List;)V

    .line 1497967
    return-void
.end method
