.class public final LX/9eI;
.super LX/4mf;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)V
    .locals 2

    .prologue
    .line 1519415
    iput-object p1, p0, LX/9eI;->b:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    .line 1519416
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x1030010

    invoke-direct {p0, p1, v0, v1}, LX/4mf;-><init>(Lcom/facebook/ui/dialogs/FbDialogFragment;Landroid/content/Context;I)V

    .line 1519417
    return-void
.end method


# virtual methods
.method public final onBackPressed()V
    .locals 4

    .prologue
    .line 1519418
    iget-object v0, p0, LX/9eI;->b:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v0, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->y:LX/9eS;

    .line 1519419
    const/4 v1, 0x0

    .line 1519420
    iget-object v2, v0, LX/9eS;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v1

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/9eR;

    .line 1519421
    invoke-interface {v1}, LX/9eR;->a()Z

    move-result v1

    or-int/2addr v1, v2

    move v2, v1

    .line 1519422
    goto :goto_0

    .line 1519423
    :cond_0
    move v0, v2

    .line 1519424
    if-eqz v0, :cond_1

    .line 1519425
    :goto_1
    return-void

    .line 1519426
    :cond_1
    iget-object v0, p0, LX/9eI;->b:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v0, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    const-string v1, "tap_back_button"

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 1519427
    iget-object v0, p0, LX/9eI;->b:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    .line 1519428
    iget-object v1, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    sget-object v2, LX/9eK;->ANIMATE_OUT:LX/9eK;

    if-eq v1, v2, :cond_2

    iget-object v1, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    invoke-static {v1}, LX/9eK;->isSwiping(LX/9eK;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1519429
    :cond_2
    iget-object v1, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->w:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03V;

    sget-object v2, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->z:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string p0, "maybeDismiss: unexpected state "

    invoke-direct {v3, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object p0, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1519430
    iget-object v1, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->K:Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;

    if-eqz v1, :cond_3

    .line 1519431
    iget-object v1, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->K:Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;

    iget-object v2, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->v:LX/1L1;

    const/4 v3, 0x0

    invoke-static {v0}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->x(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)I

    move-result p0

    invoke-virtual {v1, v2, v3, p0}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->a(LX/1L1;ZI)V

    .line 1519432
    :cond_3
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 1519433
    :goto_2
    goto :goto_1

    .line 1519434
    :cond_4
    iget-object v1, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    sget-object v2, LX/9eK;->ANIMATE_IN:LX/9eK;

    if-ne v1, v2, :cond_5

    .line 1519435
    invoke-static {v0}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->r(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;)V

    .line 1519436
    :cond_5
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->a$redex0(Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;F)V

    goto :goto_2
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 1519437
    iget-object v0, p0, LX/9eI;->b:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v0, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    sget-object v1, LX/9eK;->NORMAL:LX/9eK;

    if-eq v0, v1, :cond_0

    .line 1519438
    const/4 v0, 0x0

    .line 1519439
    :goto_0
    return v0

    .line 1519440
    :cond_0
    iget-object v0, p0, LX/9eI;->b:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d1ac1

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;

    .line 1519441
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1519442
    iget-object v1, p0, LX/9eI;->b:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    invoke-virtual {v1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->h()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 1519443
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 1519444
    iget-object v0, p0, LX/9eI;->b:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    iget-object v0, v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->V:LX/9eK;

    sget-object v1, LX/9eK;->NORMAL:LX/9eK;

    if-eq v0, v1, :cond_0

    .line 1519445
    const/4 v0, 0x0

    .line 1519446
    :goto_0
    return v0

    .line 1519447
    :cond_0
    iget-object v0, p0, LX/9eI;->b:Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d1ac1

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;

    .line 1519448
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1519449
    invoke-virtual {v0, p1}, Landroid/support/v4/app/Fragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 1519450
    const/4 v0, 0x1

    goto :goto_0
.end method
