.class public LX/8hu;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/0y2;

.field private final b:LX/6aG;

.field public final c:LX/0wM;


# direct methods
.method public constructor <init>(LX/0y2;LX/6aG;LX/0wM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1390913
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1390914
    iput-object p1, p0, LX/8hu;->a:LX/0y2;

    .line 1390915
    iput-object p2, p0, LX/8hu;->b:LX/6aG;

    .line 1390916
    iput-object p3, p0, LX/8hu;->c:LX/0wM;

    .line 1390917
    return-void
.end method

.method public static a(LX/0QB;)LX/8hu;
    .locals 6

    .prologue
    .line 1390981
    const-class v1, LX/8hu;

    monitor-enter v1

    .line 1390982
    :try_start_0
    sget-object v0, LX/8hu;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1390983
    sput-object v2, LX/8hu;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1390984
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1390985
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1390986
    new-instance p0, LX/8hu;

    invoke-static {v0}, LX/0y2;->b(LX/0QB;)LX/0y2;

    move-result-object v3

    check-cast v3, LX/0y2;

    invoke-static {v0}, LX/6aG;->b(LX/0QB;)LX/6aG;

    move-result-object v4

    check-cast v4, LX/6aG;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v5

    check-cast v5, LX/0wM;

    invoke-direct {p0, v3, v4, v5}, LX/8hu;-><init>(LX/0y2;LX/6aG;LX/0wM;)V

    .line 1390987
    move-object v0, p0

    .line 1390988
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1390989
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8hu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1390990
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1390991
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
    .locals 2

    .prologue
    .line 1390976
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1390977
    :goto_0
    return-object p0

    .line 1390978
    :cond_0
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_1

    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v0

    const/16 v1, 0xa

    if-eq v0, v1, :cond_1

    .line 1390979
    const-string v0, " \u2022 "

    invoke-virtual {p0, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1390980
    :cond_1
    invoke-virtual {p0, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_0
.end method

.method public static a(LX/8hu;Landroid/content/Context;Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;)Ljava/lang/CharSequence;
    .locals 12
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1390952
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/16 v11, 0x21

    const/4 v10, 0x0

    .line 1390953
    if-nez p2, :cond_0

    .line 1390954
    :goto_0
    move-object v0, v1

    .line 1390955
    return-object v0

    .line 1390956
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1390957
    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    const v4, 0x7f0a008a

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v3, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 1390958
    invoke-virtual {p2}, Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;->b()D

    move-result-wide v5

    .line 1390959
    invoke-virtual {p2}, Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;->a()I

    move-result v4

    .line 1390960
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 1390961
    if-gtz v4, :cond_1

    const-wide/16 v7, 0x0

    cmpl-double v7, v5, v7

    if-lez v7, :cond_4

    .line 1390962
    :cond_1
    iget-object v7, p0, LX/8hu;->c:LX/0wM;

    const v8, 0x7f0209e3

    const v9, -0xbd984e

    invoke-virtual {v7, v8, v9}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 1390963
    if-eqz v7, :cond_2

    .line 1390964
    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v8

    invoke-virtual {v7, v10, v10, v1, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1390965
    new-instance v1, Landroid/text/style/ImageSpan;

    const/4 v8, 0x1

    invoke-direct {v1, v7, v8}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 1390966
    :cond_2
    const-string v7, "%.1f"

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-static {v7, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1390967
    if-eqz v1, :cond_3

    .line 1390968
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    .line 1390969
    const-string v6, "*"

    invoke-virtual {v2, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1390970
    add-int/lit8 v6, v5, 0x1

    invoke-virtual {v2, v1, v5, v6, v11}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1390971
    :cond_3
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 1390972
    invoke-virtual {v2, v3, v10, v1, v11}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1390973
    if-eqz v0, :cond_4

    .line 1390974
    const-string v1, " ("

    invoke-virtual {v2, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/text/NumberFormat;->getNumberInstance()Ljava/text/NumberFormat;

    move-result-object v3

    int-to-long v5, v4

    invoke-virtual {v3, v5, v6}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    const-string v3, ")"

    invoke-virtual {v1, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_4
    move-object v1, v2

    .line 1390975
    goto/16 :goto_0
.end method

.method public static a(Landroid/content/Context;LX/8dB;)Ljava/lang/CharSequence;
    .locals 7

    .prologue
    .line 1390932
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1390933
    invoke-interface {p1}, LX/8dB;->G()Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$PlaceOpenStatusModel;

    move-result-object v1

    .line 1390934
    invoke-interface {p1}, LX/8dB;->H()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-result-object v3

    .line 1390935
    :try_start_0
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 1390936
    if-eqz v1, :cond_1

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    if-eq v3, v4, :cond_1

    .line 1390937
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$PlaceOpenStatusModel;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, LX/8hu;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1390938
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    .line 1390939
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$PlaceOpenStatusModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    sub-int v5, v4, v1

    .line 1390940
    if-eqz v3, :cond_1

    .line 1390941
    const/4 v1, 0x0

    .line 1390942
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;->SHOW_AVAILABLE:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    if-ne v3, v6, :cond_2

    .line 1390943
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    const v3, 0x7f0a00d4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 1390944
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 1390945
    const/16 v2, 0x21

    invoke-virtual {v0, v1, v5, v4, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1390946
    :cond_1
    :goto_1
    return-object v0

    .line 1390947
    :cond_2
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;->SHOW_UNDETERMINED:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    if-ne v3, v6, :cond_3

    .line 1390948
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    const v3, 0x7f0a014a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1390949
    :catch_0
    const-string v0, ""

    goto :goto_1

    .line 1390950
    :cond_3
    :try_start_1
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;->SHOW_UNAVAILABLE:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    if-ne v3, v6, :cond_0

    .line 1390951
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    const v3, 0x7f0a00d3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/8dB;)Ljava/lang/CharSequence;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 1390918
    invoke-interface {p1}, LX/8dB;->D()Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$AddressModel;

    move-result-object v7

    .line 1390919
    if-eqz v7, :cond_0

    invoke-virtual {v7}, Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$AddressModel;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1390920
    :cond_0
    :goto_0
    return-object v6

    .line 1390921
    :cond_1
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 1390922
    iget-object v1, p0, LX/8hu;->a:LX/0y2;

    invoke-virtual {v1}, LX/0y2;->a()Lcom/facebook/location/ImmutableLocation;

    move-result-object v2

    .line 1390923
    invoke-interface {p1}, LX/8dB;->E()LX/8d5;

    move-result-object v1

    .line 1390924
    if-eqz v2, :cond_2

    if-eqz v1, :cond_2

    .line 1390925
    new-instance v3, Landroid/location/Location;

    const-string v4, "places_search"

    invoke-direct {v3, v4}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 1390926
    invoke-interface {v1}, LX/8d5;->a()D

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Landroid/location/Location;->setLatitude(D)V

    .line 1390927
    invoke-interface {v1}, LX/8d5;->b()D

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Landroid/location/Location;->setLongitude(D)V

    .line 1390928
    iget-object v1, p0, LX/8hu;->b:LX/6aG;

    invoke-static {v3}, Lcom/facebook/location/ImmutableLocation;->a(Landroid/location/Location;)Lcom/facebook/location/ImmutableLocation;

    move-result-object v3

    const-wide v4, 0x40f3a53340000000L    # 80467.203125

    invoke-virtual/range {v1 .. v6}, LX/6aG;->a(Lcom/facebook/location/ImmutableLocation;Lcom/facebook/location/ImmutableLocation;DLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1390929
    invoke-static {v0, v1}, LX/8hu;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1390930
    :cond_2
    invoke-virtual {v7}, Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$AddressModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/8hu;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-object v6, v0

    .line 1390931
    goto :goto_0
.end method
