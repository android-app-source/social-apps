.class public final LX/8jk;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackFieldsModel;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1392710
    const-class v1, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackFieldsModel;

    const v0, -0x2b8ef559

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x2

    const-string v5, "FetchStickerPacksQuery"

    const-string v6, "545a554ae4e17ada473c2b0bdf905fe6"

    const-string v7, "nodes"

    const-string v8, "10155093175021729"

    const/4 v9, 0x0

    .line 1392711
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1392712
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1392713
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1392714
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1392715
    sparse-switch v0, :sswitch_data_0

    .line 1392716
    :goto_0
    return-object p1

    .line 1392717
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1392718
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1392719
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x2cc07392 -> :sswitch_1
        0x73a026b5 -> :sswitch_0
        0x763c4507 -> :sswitch_2
    .end sparse-switch
.end method

.method public final l()Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;
    .locals 2

    .prologue
    .line 1392720
    new-instance v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQL$FetchStickerPacksQueryString$1;

    const-class v1, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackFieldsModel;

    invoke-direct {v0, p0, v1}, Lcom/facebook/stickers/graphql/FetchStickersGraphQL$FetchStickerPacksQueryString$1;-><init>(LX/8jk;Ljava/lang/Class;)V

    return-object v0
.end method
