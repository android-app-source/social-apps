.class public final LX/AIP;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1659483
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 1659484
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1659485
    :goto_0
    return v1

    .line 1659486
    :cond_0
    const-string v8, "unseen_count"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1659487
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v3, v0

    move v0, v2

    .line 1659488
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_5

    .line 1659489
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1659490
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1659491
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 1659492
    const-string v8, "id"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1659493
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 1659494
    :cond_2
    const-string v8, "other_participant"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1659495
    invoke-static {p0, p1}, LX/AIe;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1659496
    :cond_3
    const-string v8, "threads"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1659497
    invoke-static {p0, p1}, LX/AIO;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1659498
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1659499
    :cond_5
    const/4 v7, 0x4

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1659500
    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 1659501
    invoke-virtual {p1, v2, v5}, LX/186;->b(II)V

    .line 1659502
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v4}, LX/186;->b(II)V

    .line 1659503
    if-eqz v0, :cond_6

    .line 1659504
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3, v1}, LX/186;->a(III)V

    .line 1659505
    :cond_6
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1659506
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1659507
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1659508
    if-eqz v0, :cond_0

    .line 1659509
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1659510
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1659511
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1659512
    if-eqz v0, :cond_1

    .line 1659513
    const-string v1, "other_participant"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1659514
    invoke-static {p0, v0, p2, p3}, LX/AIe;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1659515
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1659516
    if-eqz v0, :cond_2

    .line 1659517
    const-string v1, "threads"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1659518
    invoke-static {p0, v0, p2, p3}, LX/AIO;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1659519
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1659520
    if-eqz v0, :cond_3

    .line 1659521
    const-string v1, "unseen_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1659522
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1659523
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1659524
    return-void
.end method
