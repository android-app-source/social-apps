.class public LX/9aQ;
.super Landroid/graphics/drawable/Drawable;
.source ""


# instance fields
.field public final a:LX/0wY;

.field public final b:LX/3RX;

.field public final c:LX/3RZ;

.field public final d:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LX/9aM;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/Deque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Deque",
            "<",
            "LX/9aL;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LX/9aL;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Landroid/graphics/Paint;

.field public h:J

.field public i:Landroid/view/GestureDetector;

.field public j:LX/9Aq;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:LX/9aL;

.field public l:Z

.field public final m:LX/0wa;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0wY;LX/3RX;LX/3RZ;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1513726
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 1513727
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/9aQ;->d:Ljava/util/Queue;

    .line 1513728
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/9aQ;->e:Ljava/util/Deque;

    .line 1513729
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/9aQ;->f:Ljava/util/Queue;

    .line 1513730
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/9aQ;->g:Landroid/graphics/Paint;

    .line 1513731
    new-instance v0, LX/9aO;

    invoke-direct {v0, p0}, LX/9aO;-><init>(LX/9aQ;)V

    iput-object v0, p0, LX/9aQ;->m:LX/0wa;

    .line 1513732
    iput-object p2, p0, LX/9aQ;->a:LX/0wY;

    .line 1513733
    iput-object p3, p0, LX/9aQ;->b:LX/3RX;

    .line 1513734
    iput-object p4, p0, LX/9aQ;->c:LX/3RZ;

    .line 1513735
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, LX/9aP;

    invoke-direct {v1, p0}, LX/9aP;-><init>(LX/9aQ;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, LX/9aQ;->i:Landroid/view/GestureDetector;

    .line 1513736
    iget-object v0, p0, LX/9aQ;->i:Landroid/view/GestureDetector;

    invoke-virtual {v0, v2}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    .line 1513737
    return-void
.end method

.method public static a$redex0(LX/9aQ;FF)LX/9aL;
    .locals 13
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1513696
    const/4 v1, 0x0

    .line 1513697
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 1513698
    invoke-virtual {p0}, LX/9aQ;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v3

    .line 1513699
    const/4 v0, 0x2

    new-array v4, v0, [F

    const/4 v0, 0x0

    aput p1, v4, v0

    const/4 v0, 0x1

    aput p2, v4, v0

    .line 1513700
    const/4 v0, 0x2

    new-array v5, v0, [F

    .line 1513701
    iget-object v0, p0, LX/9aQ;->e:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->descendingIterator()Ljava/util/Iterator;

    move-result-object v6

    .line 1513702
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1513703
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9aL;

    .line 1513704
    invoke-virtual {v0}, LX/9aL;->f()F

    move-result v7

    .line 1513705
    invoke-virtual {v0}, LX/9aL;->g()F

    move-result v8

    .line 1513706
    invoke-virtual {v2}, Landroid/graphics/Matrix;->reset()V

    .line 1513707
    neg-float v9, v7

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    neg-float v10, v8

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    invoke-virtual {v2, v9, v10}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1513708
    iget v9, v0, LX/9aL;->k:F

    move v9, v9

    .line 1513709
    iget v10, v0, LX/9aL;->k:F

    move v10, v10

    .line 1513710
    invoke-virtual {v2, v9, v10}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 1513711
    iget v9, v0, LX/9aL;->b:F

    move v9, v9

    .line 1513712
    invoke-virtual {v2, v9}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 1513713
    iget v9, v0, LX/9aL;->c:F

    move v9, v9

    .line 1513714
    div-int/lit8 v10, v3, 0x2

    int-to-float v10, v10

    add-float/2addr v9, v10

    .line 1513715
    iget v10, v0, LX/9aL;->d:F

    move v10, v10

    .line 1513716
    invoke-virtual {v2, v9, v10}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1513717
    invoke-virtual {v2, v2}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 1513718
    invoke-virtual {v2, v5, v4}, Landroid/graphics/Matrix;->mapPoints([F[F)V

    .line 1513719
    const/4 v9, 0x0

    aget v9, v5, v9

    .line 1513720
    const/4 v10, 0x1

    aget v10, v5, v10

    .line 1513721
    const/4 v11, 0x0

    cmpl-float v11, v9, v11

    if-ltz v11, :cond_0

    const/4 v11, 0x0

    cmpl-float v11, v10, v11

    if-ltz v11, :cond_0

    cmpg-float v11, v9, v7

    if-gez v11, :cond_0

    cmpg-float v11, v10, v8

    if-gez v11, :cond_0

    .line 1513722
    :goto_1
    return-object v0

    .line 1513723
    :cond_0
    if-nez v1, :cond_2

    neg-float v11, v7

    const/high16 v12, 0x40800000    # 4.0f

    div-float/2addr v11, v12

    cmpl-float v11, v9, v11

    if-ltz v11, :cond_2

    const/high16 v11, 0x40a00000    # 5.0f

    mul-float/2addr v7, v11

    const/high16 v11, 0x40800000    # 4.0f

    div-float/2addr v7, v11

    cmpg-float v7, v9, v7

    if-gez v7, :cond_2

    neg-float v7, v8

    const/high16 v9, 0x40800000    # 4.0f

    div-float/2addr v7, v9

    cmpl-float v7, v10, v7

    if-ltz v7, :cond_2

    const/high16 v7, 0x40a00000    # 5.0f

    mul-float/2addr v7, v8

    const/high16 v8, 0x40800000    # 4.0f

    div-float/2addr v7, v8

    cmpg-float v7, v10, v7

    if-gez v7, :cond_2

    :goto_2
    move-object v1, v0

    .line 1513724
    goto/16 :goto_0

    :cond_1
    move-object v0, v1

    .line 1513725
    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_2
.end method

.method public static d(LX/9aQ;)V
    .locals 13

    .prologue
    .line 1513641
    invoke-virtual {p0}, LX/9aQ;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    if-lez v0, :cond_9

    .line 1513642
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 1513643
    long-to-float v2, v0

    .line 1513644
    :goto_0
    iget-object v3, p0, LX/9aQ;->d:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    iget-wide v3, p0, LX/9aQ;->h:J

    long-to-float v3, v3

    cmpl-float v3, v2, v3

    if-ltz v3, :cond_2

    .line 1513645
    iget-object v3, p0, LX/9aQ;->d:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/9aM;

    .line 1513646
    instance-of v4, v3, LX/9aS;

    if-eqz v4, :cond_1

    move-object v4, v3

    .line 1513647
    check-cast v4, LX/9aS;

    .line 1513648
    iget-wide v5, p0, LX/9aQ;->h:J

    .line 1513649
    iget-object v8, p0, LX/9aQ;->f:Ljava/util/Queue;

    invoke-interface {v8}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/9aL;

    .line 1513650
    if-eqz v8, :cond_c

    .line 1513651
    invoke-virtual {v8, v4}, LX/9aL;->a(LX/9aS;)V

    .line 1513652
    :goto_1
    move-object v8, v8

    .line 1513653
    iput-wide v5, v8, LX/9aL;->m:J

    .line 1513654
    iget-object v9, v4, LX/9aS;->c:LX/9aT;

    invoke-interface {v9}, LX/9aT;->a()F

    move-result v9

    move v9, v9

    .line 1513655
    invoke-virtual {p0}, LX/9aQ;->getBounds()Landroid/graphics/Rect;

    move-result-object v10

    invoke-virtual {v10}, Landroid/graphics/Rect;->width()I

    move-result v10

    int-to-float v10, v10

    mul-float/2addr v9, v10

    .line 1513656
    iput v9, v8, LX/9aL;->c:F

    .line 1513657
    invoke-virtual {v8}, LX/9aL;->f()F

    move-result v9

    invoke-virtual {v8}, LX/9aL;->g()F

    move-result v10

    invoke-static {v9, v10}, Ljava/lang/Math;->max(FF)F

    move-result v9

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    .line 1513658
    iget v10, v8, LX/9aL;->k:F

    move v10, v10

    .line 1513659
    mul-float/2addr v9, v10

    .line 1513660
    iget v10, v8, LX/9aL;->e:F

    move v10, v10

    .line 1513661
    const/4 v11, 0x0

    cmpl-float v10, v10, v11

    if-lez v10, :cond_d

    .line 1513662
    neg-float v9, v9

    .line 1513663
    :goto_2
    move v9, v9

    .line 1513664
    iput v9, v8, LX/9aL;->d:F

    .line 1513665
    iget-object v9, p0, LX/9aQ;->e:Ljava/util/Deque;

    invoke-interface {v9, v8}, Ljava/util/Deque;->add(Ljava/lang/Object;)Z

    .line 1513666
    :cond_0
    :goto_3
    iget-wide v5, p0, LX/9aQ;->h:J

    invoke-interface {v3}, LX/9aM;->a()J

    move-result-wide v3

    add-long/2addr v3, v5

    iput-wide v3, p0, LX/9aQ;->h:J

    goto :goto_0

    .line 1513667
    :cond_1
    instance-of v4, v3, LX/9aN;

    if-eqz v4, :cond_0

    move-object v4, v3

    .line 1513668
    check-cast v4, LX/9aN;

    .line 1513669
    iget-object v5, p0, LX/9aQ;->b:LX/3RX;

    iget v6, v4, LX/9aN;->a:I

    iget-object v7, p0, LX/9aQ;->c:LX/3RZ;

    invoke-virtual {v7}, LX/3RZ;->a()I

    move-result v7

    iget v4, v4, LX/9aN;->b:F

    invoke-virtual {v5, v6, v7, v4}, LX/3RX;->a(IIF)LX/7Cb;

    goto :goto_3

    .line 1513670
    :cond_2
    iget-object v3, p0, LX/9aQ;->d:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-boolean v3, p0, LX/9aQ;->l:Z

    if-eqz v3, :cond_3

    .line 1513671
    const/4 v3, 0x0

    iput-boolean v3, p0, LX/9aQ;->l:Z

    .line 1513672
    :cond_3
    const/4 v7, 0x0

    .line 1513673
    invoke-virtual {p0}, LX/9aQ;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v3

    .line 1513674
    iget-object v2, p0, LX/9aQ;->e:Ljava/util/Deque;

    invoke-interface {v2}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 1513675
    :cond_4
    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1513676
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/9aL;

    .line 1513677
    iget-object v5, p0, LX/9aQ;->k:LX/9aL;

    if-eq v5, v2, :cond_5

    .line 1513678
    invoke-virtual {v2, v0, v1}, LX/9aL;->b(J)V

    .line 1513679
    :cond_5
    iget v5, v2, LX/9aL;->e:F

    move v5, v5

    .line 1513680
    cmpl-float v5, v5, v7

    if-ltz v5, :cond_6

    .line 1513681
    iget v5, v2, LX/9aL;->d:F

    iget-object v6, v2, LX/9aL;->a:LX/9aS;

    invoke-virtual {v6}, LX/9aS;->b()I

    move-result v6

    iget-object v8, v2, LX/9aL;->a:LX/9aS;

    invoke-virtual {v8}, LX/9aS;->c()I

    move-result v8

    invoke-static {v6, v8}, Ljava/lang/Math;->max(II)I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    iget v8, v2, LX/9aL;->k:F

    mul-float/2addr v6, v8

    sub-float/2addr v5, v6

    move v5, v5

    .line 1513682
    int-to-float v6, v3

    cmpl-float v5, v5, v6

    if-gez v5, :cond_7

    .line 1513683
    :cond_6
    iget v5, v2, LX/9aL;->e:F

    move v5, v5

    .line 1513684
    cmpg-float v5, v5, v7

    if-gez v5, :cond_4

    .line 1513685
    iget v5, v2, LX/9aL;->d:F

    iget-object v6, v2, LX/9aL;->a:LX/9aS;

    invoke-virtual {v6}, LX/9aS;->b()I

    move-result v6

    iget-object v8, v2, LX/9aL;->a:LX/9aS;

    invoke-virtual {v8}, LX/9aS;->c()I

    move-result v8

    invoke-static {v6, v8}, Ljava/lang/Math;->max(II)I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    iget v8, v2, LX/9aL;->k:F

    mul-float/2addr v6, v8

    add-float/2addr v5, v6

    move v5, v5

    .line 1513686
    cmpg-float v5, v5, v7

    if-gez v5, :cond_4

    .line 1513687
    :cond_7
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    .line 1513688
    iget-object v5, p0, LX/9aQ;->f:Ljava/util/Queue;

    invoke-interface {v5, v2}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1513689
    :cond_8
    invoke-virtual {p0}, LX/9aQ;->invalidateSelf()V

    .line 1513690
    :cond_9
    invoke-virtual {p0}, LX/9aQ;->a()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1513691
    iget-object v0, p0, LX/9aQ;->a:LX/0wY;

    iget-object v1, p0, LX/9aQ;->m:LX/0wa;

    invoke-interface {v0, v1}, LX/0wY;->a(LX/0wa;)V

    .line 1513692
    :goto_5
    return-void

    .line 1513693
    :cond_a
    iget-object v0, p0, LX/9aQ;->j:LX/9Aq;

    if-eqz v0, :cond_b

    .line 1513694
    iget-object v0, p0, LX/9aQ;->j:LX/9Aq;

    invoke-interface {v0}, LX/9Aq;->a()V

    .line 1513695
    :cond_b
    goto :goto_5

    :cond_c
    new-instance v8, LX/9aL;

    invoke-direct {v8, v4}, LX/9aL;-><init>(LX/9aS;)V

    goto/16 :goto_1

    :cond_d
    invoke-virtual {p0}, LX/9aQ;->getBounds()Landroid/graphics/Rect;

    move-result-object v10

    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v10

    int-to-float v10, v10

    add-float/2addr v9, v10

    goto/16 :goto_2
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1513640
    iget-object v0, p0, LX/9aQ;->e:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9aQ;->d:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    .line 1513633
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 1513634
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1513635
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1513636
    iget-object v0, p0, LX/9aQ;->e:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9aL;

    .line 1513637
    iget-object v4, p0, LX/9aQ;->g:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, v4, v2, v3}, LX/9aL;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;J)V

    goto :goto_0

    .line 1513638
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1513639
    return-void
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 1513628
    const/4 v0, -0x3

    return v0
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 1513631
    iget-object v0, p0, LX/9aQ;->g:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1513632
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 1513629
    iget-object v0, p0, LX/9aQ;->g:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 1513630
    return-void
.end method
