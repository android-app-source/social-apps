.class public final LX/9U6;
.super Landroid/content/AsyncQueryHandler;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1497501
    iput-object p1, p0, LX/9U6;->a:Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;

    .line 1497502
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 1497503
    return-void
.end method


# virtual methods
.method public final onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1497485
    iget-object v0, p0, LX/9U6;->a:Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;

    invoke-virtual {v0}, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1497486
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 1497487
    :cond_0
    return-void

    .line 1497488
    :cond_1
    iget-object v0, p0, LX/9U6;->a:Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;

    .line 1497489
    invoke-static {v0, v3, v1}, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->a$redex0(Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;IZ)V

    .line 1497490
    iget-object v0, p0, LX/9U6;->a:Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;

    invoke-virtual {v0, p3}, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->startManagingCursor(Landroid/database/Cursor;)V

    .line 1497491
    iget-object v0, p0, LX/9U6;->a:Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;

    iget-object v0, v0, Lcom/facebook/katana/activity/profilelist/ProfileListActivity;->w:LX/9U9;

    check-cast v0, LX/9UD;

    invoke-virtual {v0, p3}, LX/9UB;->a(Landroid/database/Cursor;)V

    .line 1497492
    iget-object v0, p0, LX/9U6;->a:Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;

    iget-object v0, v0, Lcom/facebook/katana/activity/profilelist/ProfileListActivity;->w:LX/9U9;

    invoke-virtual {v0}, LX/3Tf;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 1497493
    iget-object v0, p0, LX/9U6;->a:Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;

    iget-object v0, v0, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->y:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    invoke-virtual {v0}, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->a()V

    .line 1497494
    iget-object v0, p0, LX/9U6;->a:Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;

    const/4 v2, 0x2

    .line 1497495
    invoke-static {v0, v2, v3}, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->a$redex0(Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;IZ)V

    .line 1497496
    :cond_2
    :goto_0
    iget-object v0, p0, LX/9U6;->a:Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;

    iget-object v0, v0, Lcom/facebook/katana/activity/profilelist/ProfileListActivity;->w:LX/9U9;

    invoke-virtual {v0}, LX/3Tf;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1497497
    iget-object v0, p0, LX/9U6;->a:Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;

    iget-object v0, v0, Lcom/facebook/katana/activity/profilelist/ProfileListActivity;->w:LX/9U9;

    invoke-virtual {v0, v1}, LX/3Tf;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookProfile;

    .line 1497498
    if-eqz v0, :cond_3

    iget-object v2, p0, LX/9U6;->a:Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;

    iget-object v2, v2, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->q:LX/0Rf;

    iget-wide v4, v0, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1497499
    iget-object v2, p0, LX/9U6;->a:Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;

    iget-object v2, v2, Lcom/facebook/katana/activity/profilelist/FriendMultiSelectorActivity;->r:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1497500
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method
