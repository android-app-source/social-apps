.class public abstract LX/9dl;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/8GZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/widget/ImageView;

.field public c:Landroid/graphics/Rect;

.field public d:I

.field public e:Landroid/view/GestureDetector;

.field public f:Landroid/view/ScaleGestureDetector;

.field public g:LX/9e2;

.field public h:Z

.field public i:[Landroid/graphics/drawable/ColorDrawable;

.field public j:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1518615
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1518616
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/9dl;->j:Z

    .line 1518617
    invoke-direct {p0}, LX/9dl;->j()V

    .line 1518618
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1518619
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1518620
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/9dl;->j:Z

    .line 1518621
    invoke-direct {p0}, LX/9dl;->j()V

    .line 1518622
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1518623
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1518624
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/9dl;->j:Z

    .line 1518625
    invoke-direct {p0}, LX/9dl;->j()V

    .line 1518626
    return-void
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 1518627
    iget-object v1, p0, LX/9dl;->i:[Landroid/graphics/drawable/ColorDrawable;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 1518628
    if-eqz v3, :cond_0

    .line 1518629
    invoke-virtual {v3, p1}, Landroid/graphics/drawable/ColorDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 1518630
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1518631
    :cond_1
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/9dl;

    invoke-static {p0}, LX/8GZ;->a(LX/0QB;)LX/8GZ;

    move-result-object p0

    check-cast p0, LX/8GZ;

    iput-object p0, p1, LX/9dl;->a:LX/8GZ;

    return-void
.end method

.method public static a$redex0(LX/9dl;[ILandroid/view/MotionEvent;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1518632
    invoke-virtual {p0, p1}, LX/9dl;->getLocationOnScreen([I)V

    .line 1518633
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    aget v1, p1, v2

    int-to-float v1, v1

    sub-float/2addr v0, v1

    float-to-int v0, v0

    aput v0, p1, v2

    .line 1518634
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    aget v1, p1, v3

    int-to-float v1, v1

    sub-float/2addr v0, v1

    float-to-int v0, v0

    aput v0, p1, v3

    .line 1518635
    return-void
.end method

.method public static a$redex0(LX/9dl;IIZ)Z
    .locals 7

    .prologue
    .line 1518636
    invoke-virtual {p0}, LX/9dl;->getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v0

    .line 1518637
    iget-object v1, v0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    move-object v0, v1

    .line 1518638
    invoke-virtual {p0}, LX/9dl;->getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v1

    iget-object v2, p0, LX/9dl;->c:Landroid/graphics/Rect;

    .line 1518639
    iget-object v3, v1, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-static {v3}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v5

    .line 1518640
    iget-object v3, v1, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    move v4, v3

    :goto_0
    if-ltz v4, :cond_6

    .line 1518641
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/5i8;

    .line 1518642
    invoke-interface {v3}, LX/5i8;->k()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1518643
    invoke-interface {v3, v2}, LX/5i8;->a(Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v6

    invoke-virtual {v6, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1518644
    invoke-virtual {v1, v3}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b(LX/362;)V

    .line 1518645
    :goto_1
    move-object v1, v3

    .line 1518646
    if-eqz v1, :cond_3

    .line 1518647
    invoke-virtual {p0}, LX/9dl;->getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b(LX/362;)V

    .line 1518648
    :cond_0
    :goto_2
    if-eqz v0, :cond_1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    if-nez v0, :cond_4

    if-eqz v1, :cond_4

    :cond_2
    const/4 v0, 0x1

    :goto_3
    return v0

    .line 1518649
    :cond_3
    if-eqz p3, :cond_0

    .line 1518650
    invoke-virtual {p0}, LX/9dl;->getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->g()V

    goto :goto_2

    .line 1518651
    :cond_4
    const/4 v0, 0x0

    goto :goto_3

    .line 1518652
    :cond_5
    add-int/lit8 v3, v4, -0x1

    move v4, v3

    goto :goto_0

    .line 1518653
    :cond_6
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static c(LX/9dl;LX/362;)Z
    .locals 6

    .prologue
    .line 1518666
    iget-object v0, p0, LX/9dl;->c:Landroid/graphics/Rect;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1518667
    invoke-interface {p1}, LX/362;->a()Landroid/graphics/RectF;

    move-result-object v0

    .line 1518668
    new-instance v1, Landroid/graphics/Rect;

    iget v2, v0, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, LX/9dl;->c:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iget-object v3, p0, LX/9dl;->c:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    add-int/2addr v2, v3

    iget v3, v0, Landroid/graphics/RectF;->top:F

    iget-object v4, p0, LX/9dl;->c:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iget-object v4, p0, LX/9dl;->c:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    add-int/2addr v3, v4

    iget v4, v0, Landroid/graphics/RectF;->right:F

    iget-object v5, p0, LX/9dl;->c:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v4, v5

    float-to-int v4, v4

    iget-object v5, p0, LX/9dl;->c:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    add-int/2addr v4, v5

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    iget-object v5, p0, LX/9dl;->c:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v0, v5

    float-to-int v0, v0

    iget-object v5, p0, LX/9dl;->c:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    add-int/2addr v0, v5

    invoke-direct {v1, v2, v3, v4, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1518669
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 1518670
    iget-object v2, p0, LX/9dl;->b:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->getHitRect(Landroid/graphics/Rect;)V

    .line 1518671
    iget v2, v0, Landroid/graphics/Rect;->right:I

    iget v3, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    .line 1518672
    int-to-double v2, v2

    const-wide/high16 v4, 0x3fd0000000000000L    # 0.25

    mul-double/2addr v2, v4

    double-to-int v2, v2

    .line 1518673
    iget v3, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v2

    iput v3, v0, Landroid/graphics/Rect;->top:I

    .line 1518674
    iget v3, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v3, v2

    iput v3, v0, Landroid/graphics/Rect;->bottom:I

    .line 1518675
    iget v3, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v2

    iput v3, v0, Landroid/graphics/Rect;->left:I

    .line 1518676
    iget v3, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v2, v3

    iput v2, v0, Landroid/graphics/Rect;->right:I

    .line 1518677
    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z

    move-result v0

    return v0
.end method

.method private j()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1518654
    const-class v0, LX/9dl;

    invoke-static {v0, p0}, LX/9dl;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1518655
    invoke-virtual {p0}, LX/9dl;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0310f8

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1518656
    const v0, 0x7f0d2857

    invoke-virtual {p0, v0}, LX/9dl;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/9dl;->b:Landroid/widget/ImageView;

    .line 1518657
    iget-object v0, p0, LX/9dl;->b:Landroid/widget/ImageView;

    new-instance v1, LX/9df;

    invoke-direct {v1, p0}, LX/9df;-><init>(LX/9dl;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1518658
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/graphics/drawable/ColorDrawable;

    iput-object v0, p0, LX/9dl;->i:[Landroid/graphics/drawable/ColorDrawable;

    .line 1518659
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/9dl;->j:Z

    .line 1518660
    new-instance v0, LX/9e2;

    invoke-virtual {p0}, LX/9dl;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, LX/9di;

    invoke-direct {v2, p0}, LX/9di;-><init>(LX/9dl;)V

    invoke-direct {v0, v1, v2}, LX/9e2;-><init>(Landroid/content/Context;LX/9dh;)V

    iput-object v0, p0, LX/9dl;->g:LX/9e2;

    .line 1518661
    new-instance v0, Landroid/view/ScaleGestureDetector;

    invoke-virtual {p0}, LX/9dl;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, LX/9dj;

    invoke-direct {v2, p0}, LX/9dj;-><init>(LX/9dl;)V

    invoke-direct {v0, v1, v2}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, LX/9dl;->f:Landroid/view/ScaleGestureDetector;

    .line 1518662
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, LX/9dl;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, LX/9dg;

    invoke-direct {v2, p0}, LX/9dg;-><init>(LX/9dl;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, LX/9dl;->e:Landroid/view/GestureDetector;

    .line 1518663
    iget-object v0, p0, LX/9dl;->e:Landroid/view/GestureDetector;

    invoke-virtual {v0, v3}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    .line 1518664
    new-instance v0, LX/9dk;

    invoke-direct {v0, p0}, LX/9dk;-><init>(LX/9dl;)V

    invoke-virtual {p0, v0}, LX/9dl;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1518665
    return-void
.end method

.method private k()Z
    .locals 3

    .prologue
    .line 1518731
    iget-object v0, p0, LX/9dl;->c:Landroid/graphics/Rect;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1518732
    invoke-virtual {p0}, LX/9dl;->getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->a()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, LX/9dl;->getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v0

    iget-object v1, p0, LX/9dl;->c:Landroid/graphics/Rect;

    .line 1518733
    iget-object v2, v0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/5i8;

    .line 1518734
    invoke-interface {v2, v1}, LX/5i8;->a(Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1518735
    const/4 v2, 0x0

    .line 1518736
    :goto_0
    move v0, v2

    .line 1518737
    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private l()V
    .locals 1

    .prologue
    .line 1518729
    invoke-virtual {p0}, LX/9dl;->getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->c()V

    .line 1518730
    return-void
.end method

.method private m()V
    .locals 1

    .prologue
    .line 1518738
    invoke-virtual {p0}, LX/9dl;->getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->d()V

    .line 1518739
    return-void
.end method

.method public static o(LX/9dl;)V
    .locals 10

    .prologue
    .line 1518708
    invoke-virtual {p0}, LX/9dl;->getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v0

    .line 1518709
    iget-object v1, v0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    move-object v0, v1

    .line 1518710
    if-nez v0, :cond_1

    .line 1518711
    :cond_0
    :goto_0
    return-void

    .line 1518712
    :cond_1
    invoke-interface {v0}, LX/362;->a()Landroid/graphics/RectF;

    move-result-object v0

    .line 1518713
    new-instance v2, Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, LX/9dl;->c:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v1, v3

    float-to-int v1, v1

    iget-object v3, p0, LX/9dl;->c:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v3

    iget v3, v0, Landroid/graphics/RectF;->top:F

    iget-object v4, p0, LX/9dl;->c:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iget-object v4, p0, LX/9dl;->c:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    add-int/2addr v3, v4

    iget v4, v0, Landroid/graphics/RectF;->right:F

    iget-object v5, p0, LX/9dl;->c:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v4, v5

    float-to-int v4, v4

    iget-object v5, p0, LX/9dl;->c:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    add-int/2addr v4, v5

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    iget-object v5, p0, LX/9dl;->c:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v0, v5

    float-to-int v0, v0

    iget-object v5, p0, LX/9dl;->c:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    add-int/2addr v0, v5

    invoke-direct {v2, v1, v3, v4, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1518714
    iget-object v0, p0, LX/9dl;->c:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, LX/9dl;->c:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, LX/9dl;->c:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget-object v4, p0, LX/9dl;->c:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/graphics/Rect;->intersects(IIII)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1518715
    iget v0, v2, Landroid/graphics/Rect;->left:I

    .line 1518716
    iget v1, v2, Landroid/graphics/Rect;->top:I

    .line 1518717
    iget v3, v2, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, LX/9dl;->c:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    if-le v3, v4, :cond_4

    .line 1518718
    iget-object v0, p0, LX/9dl;->c:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v3

    sub-int/2addr v0, v3

    .line 1518719
    :cond_2
    :goto_1
    iget v3, v2, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, LX/9dl;->c:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    if-le v3, v4, :cond_5

    .line 1518720
    iget-object v1, p0, LX/9dl;->c:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v3

    sub-int/2addr v1, v3

    .line 1518721
    :cond_3
    :goto_2
    invoke-virtual {p0}, LX/9dl;->getAnimationController()LX/9dr;

    move-result-object v3

    iget v4, v2, Landroid/graphics/Rect;->left:I

    iget v2, v2, Landroid/graphics/Rect;->top:I

    .line 1518722
    iget-object v6, v3, LX/9dr;->j:LX/0wd;

    int-to-double v8, v4

    invoke-virtual {v6, v8, v9}, LX/0wd;->a(D)LX/0wd;

    move-result-object v6

    int-to-double v8, v0

    invoke-virtual {v6, v8, v9}, LX/0wd;->b(D)LX/0wd;

    .line 1518723
    iget-object v6, v3, LX/9dr;->k:LX/0wd;

    int-to-double v8, v2

    invoke-virtual {v6, v8, v9}, LX/0wd;->a(D)LX/0wd;

    move-result-object v6

    int-to-double v8, v1

    invoke-virtual {v6, v8, v9}, LX/0wd;->b(D)LX/0wd;

    .line 1518724
    goto/16 :goto_0

    .line 1518725
    :cond_4
    iget v3, v2, Landroid/graphics/Rect;->right:I

    iget-object v4, p0, LX/9dl;->c:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    if-ge v3, v4, :cond_2

    .line 1518726
    iget-object v0, p0, LX/9dl;->c:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    goto :goto_1

    .line 1518727
    :cond_5
    iget v3, v2, Landroid/graphics/Rect;->bottom:I

    iget-object v4, p0, LX/9dl;->c:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    if-ge v3, v4, :cond_3

    .line 1518728
    iget-object v1, p0, LX/9dl;->c:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    goto :goto_2
.end method

.method public static p(LX/9dl;)V
    .locals 8

    .prologue
    .line 1518678
    invoke-virtual {p0}, LX/9dl;->getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v0

    .line 1518679
    iget-object v1, v0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    move-object v0, v1

    .line 1518680
    if-nez v0, :cond_0

    .line 1518681
    :goto_0
    return-void

    .line 1518682
    :cond_0
    invoke-virtual {p0}, LX/9dl;->getAnimationController()LX/9dr;

    move-result-object v1

    invoke-static {p0, v0}, LX/9dl;->c(LX/9dl;LX/362;)Z

    move-result v0

    .line 1518683
    if-eqz v0, :cond_2

    iget-object v2, v1, LX/9dr;->l:LX/9dq;

    sget-object v3, LX/9dq;->DEFAULT:LX/9dq;

    if-ne v2, v3, :cond_2

    .line 1518684
    iget-object v4, v1, LX/9dr;->e:Landroid/widget/ImageView;

    const v5, 0x7f0215b4

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1518685
    iget-object v4, v1, LX/9dr;->h:LX/0wd;

    sget-object v5, LX/9dr;->a:LX/0wT;

    invoke-virtual {v4, v5}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v4

    const/4 v5, 0x1

    .line 1518686
    iput-boolean v5, v4, LX/0wd;->c:Z

    .line 1518687
    move-object v4, v4

    .line 1518688
    const-wide/high16 v6, 0x3ff8000000000000L    # 1.5

    invoke-virtual {v4, v6, v7}, LX/0wd;->b(D)LX/0wd;

    .line 1518689
    iget-object v4, v1, LX/9dr;->f:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    .line 1518690
    iget-object v5, v4, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    move-object v4, v5

    .line 1518691
    if-nez v4, :cond_3

    .line 1518692
    :goto_1
    sget-object v2, LX/9dq;->OPENING:LX/9dq;

    iput-object v2, v1, LX/9dr;->l:LX/9dq;

    .line 1518693
    :cond_1
    :goto_2
    goto :goto_0

    .line 1518694
    :cond_2
    if-nez v0, :cond_1

    iget-object v2, v1, LX/9dr;->l:LX/9dq;

    sget-object v3, LX/9dq;->OPENING_COMPLETE:LX/9dq;

    if-ne v2, v3, :cond_1

    .line 1518695
    iget-object v4, v1, LX/9dr;->e:Landroid/widget/ImageView;

    const v5, 0x7f0215b3

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1518696
    iget-object v4, v1, LX/9dr;->h:LX/0wd;

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v4, v6, v7}, LX/0wd;->b(D)LX/0wd;

    .line 1518697
    iget-object v4, v1, LX/9dr;->f:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    .line 1518698
    iget-object v5, v4, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    move-object v4, v5

    .line 1518699
    if-nez v4, :cond_4

    .line 1518700
    :goto_3
    sget-object v2, LX/9dq;->CLOSING:LX/9dq;

    iput-object v2, v1, LX/9dr;->l:LX/9dq;

    goto :goto_2

    .line 1518701
    :cond_3
    iget-object v5, v1, LX/9dr;->f:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    invoke-virtual {v5, v4}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->c(LX/362;)D

    move-result-wide v6

    iput-wide v6, v1, LX/9dr;->m:D

    .line 1518702
    iget-object v5, v1, LX/9dr;->f:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    const/16 v6, 0x64

    invoke-virtual {v5, v4, v6}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->a(LX/362;I)V

    .line 1518703
    iget-object v5, v1, LX/9dr;->i:LX/0wd;

    iget-object v6, v1, LX/9dr;->f:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    invoke-virtual {v6, v4}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->c(LX/362;)D

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, LX/0wd;->a(D)LX/0wd;

    move-result-object v5

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    invoke-virtual {v5, v6, v7}, LX/0wd;->b(D)LX/0wd;

    .line 1518704
    iget-object v5, v1, LX/9dr;->j:LX/0wd;

    iget-object v6, v1, LX/9dr;->f:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    invoke-virtual {v6, v4}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->d(LX/362;)I

    move-result v6

    int-to-double v6, v6

    invoke-virtual {v5, v6, v7}, LX/0wd;->a(D)LX/0wd;

    move-result-object v5

    iget-object v6, v1, LX/9dr;->e:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getLeft()I

    move-result v6

    int-to-double v6, v6

    invoke-virtual {v5, v6, v7}, LX/0wd;->b(D)LX/0wd;

    .line 1518705
    iget-object v5, v1, LX/9dr;->k:LX/0wd;

    iget-object v6, v1, LX/9dr;->f:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    invoke-virtual {v6, v4}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->e(LX/362;)I

    move-result v4

    int-to-double v6, v4

    invoke-virtual {v5, v6, v7}, LX/0wd;->a(D)LX/0wd;

    move-result-object v4

    iget-object v5, v1, LX/9dr;->e:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getTop()I

    move-result v5

    int-to-double v6, v5

    invoke-virtual {v4, v6, v7}, LX/0wd;->b(D)LX/0wd;

    goto :goto_1

    .line 1518706
    :cond_4
    iget-object v5, v1, LX/9dr;->f:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    const/16 v6, 0xff

    invoke-virtual {v5, v4, v6}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->a(LX/362;I)V

    .line 1518707
    iget-object v4, v1, LX/9dr;->i:LX/0wd;

    iget-wide v6, v1, LX/9dr;->m:D

    invoke-virtual {v4, v6, v7}, LX/0wd;->b(D)LX/0wd;

    goto :goto_3
.end method


# virtual methods
.method public abstract a()V
.end method

.method public final a(III)V
    .locals 3

    .prologue
    .line 1518609
    invoke-virtual {p0}, LX/9dl;->getAnimationController()LX/9dr;

    move-result-object v0

    .line 1518610
    iput p1, v0, LX/9dr;->n:I

    .line 1518611
    iget-object v1, v0, LX/9dr;->e:Landroid/widget/ImageView;

    iget v2, v0, LX/9dr;->n:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1518612
    invoke-virtual {p0}, LX/9dl;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/9dl;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1518613
    iget-object v0, p0, LX/9dl;->b:Landroid/widget/ImageView;

    invoke-virtual {p0}, LX/9dl;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1518614
    return-void
.end method

.method public abstract a(LX/362;)V
.end method

.method public abstract a(Z)V
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1518544
    iget-object v0, p0, LX/9dl;->b:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1518545
    return-void
.end method

.method public final b(LX/362;)V
    .locals 3

    .prologue
    .line 1518546
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1518547
    invoke-virtual {p0}, LX/9dl;->getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v0

    .line 1518548
    iget-object v1, v0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    if-eqz v1, :cond_0

    .line 1518549
    iget-object v1, v0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b:Ljava/util/LinkedHashMap;

    iget-object v2, v0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    invoke-virtual {v1, v2}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1518550
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    .line 1518551
    :cond_0
    invoke-virtual {p0}, LX/9dl;->getAnimationController()LX/9dr;

    move-result-object v0

    .line 1518552
    sget-object v1, LX/9dq;->DEFAULT:LX/9dq;

    iput-object v1, v0, LX/9dr;->l:LX/9dq;

    .line 1518553
    invoke-virtual {v0}, LX/9dr;->b()V

    .line 1518554
    invoke-virtual {p0, p1}, LX/9dl;->a(LX/362;)V

    .line 1518555
    return-void
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 1518556
    invoke-virtual {p0}, LX/9dl;->getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v0

    iget-object v1, p0, LX/9dl;->c:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    .line 1518557
    iget-object v0, p0, LX/9dl;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1518558
    invoke-direct {p0, p1}, LX/9dl;->a(Landroid/graphics/Canvas;)V

    .line 1518559
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 1518560
    invoke-virtual {p0}, LX/9dl;->getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v0

    iget-object v1, p0, LX/9dl;->c:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    .line 1518561
    :goto_0
    return-void

    .line 1518562
    :cond_0
    invoke-virtual {p0}, LX/9dl;->getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v0

    iget-object v1, p0, LX/9dl;->c:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    .line 1518563
    invoke-direct {p0, p1}, LX/9dl;->a(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 1518564
    iget-object v0, p0, LX/9dl;->b:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1518565
    return-void
.end method

.method public abstract getAnimationController()LX/9dr;
.end method

.method public abstract getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;
.end method

.method public getNumOfItems()I
    .locals 1

    .prologue
    .line 1518566
    invoke-direct {p0}, LX/9dl;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1518567
    const/4 v0, 0x0

    .line 1518568
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, LX/9dl;->getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v0

    .line 1518569
    iget-object p0, v0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {p0}, Ljava/util/LinkedHashMap;->size()I

    move-result p0

    move v0, p0

    .line 1518570
    goto :goto_0
.end method

.method public getOverlayMapper()LX/8GZ;
    .locals 1

    .prologue
    .line 1518571
    iget-object v0, p0, LX/9dl;->a:LX/8GZ;

    return-object v0
.end method

.method public final h()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1518572
    iget-object v0, p0, LX/9dl;->c:Landroid/graphics/Rect;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1518573
    invoke-virtual {p0}, LX/9dl;->getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v0

    iget-object v1, p0, LX/9dl;->c:Landroid/graphics/Rect;

    .line 1518574
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 1518575
    iget-object v2, v0, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/5i8;

    .line 1518576
    invoke-interface {v2, v1}, LX/5i8;->a(Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    goto :goto_0

    .line 1518577
    :cond_0
    move-object v0, v3

    .line 1518578
    iget v1, v0, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, LX/9dl;->c:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    if-ge v1, v2, :cond_1

    iget-object v1, p0, LX/9dl;->i:[Landroid/graphics/drawable/ColorDrawable;

    aget-object v1, v1, v5

    if-nez v1, :cond_1

    .line 1518579
    iget-object v1, p0, LX/9dl;->i:[Landroid/graphics/drawable/ColorDrawable;

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, LX/9dl;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0732

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    aput-object v2, v1, v5

    .line 1518580
    iget-object v1, p0, LX/9dl;->i:[Landroid/graphics/drawable/ColorDrawable;

    aget-object v1, v1, v5

    iget-object v2, p0, LX/9dl;->c:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, LX/9dl;->getHeight()I

    move-result v3

    invoke-virtual {v1, v5, v5, v2, v3}, Landroid/graphics/drawable/ColorDrawable;->setBounds(IIII)V

    .line 1518581
    :cond_1
    iget v1, v0, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, LX/9dl;->c:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    if-ge v1, v2, :cond_2

    iget-object v1, p0, LX/9dl;->i:[Landroid/graphics/drawable/ColorDrawable;

    aget-object v1, v1, v6

    if-nez v1, :cond_2

    .line 1518582
    iget-object v1, p0, LX/9dl;->i:[Landroid/graphics/drawable/ColorDrawable;

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, LX/9dl;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0732

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    aput-object v2, v1, v6

    .line 1518583
    iget-object v1, p0, LX/9dl;->i:[Landroid/graphics/drawable/ColorDrawable;

    aget-object v1, v1, v6

    iget-object v2, p0, LX/9dl;->c:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, LX/9dl;->c:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget-object v4, p0, LX/9dl;->c:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    invoke-virtual {v1, v2, v5, v3, v4}, Landroid/graphics/drawable/ColorDrawable;->setBounds(IIII)V

    .line 1518584
    :cond_2
    iget v1, v0, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, LX/9dl;->c:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    if-le v1, v2, :cond_3

    iget-object v1, p0, LX/9dl;->i:[Landroid/graphics/drawable/ColorDrawable;

    aget-object v1, v1, v7

    if-nez v1, :cond_3

    .line 1518585
    iget-object v1, p0, LX/9dl;->i:[Landroid/graphics/drawable/ColorDrawable;

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, LX/9dl;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0732

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    aput-object v2, v1, v7

    .line 1518586
    iget-object v1, p0, LX/9dl;->i:[Landroid/graphics/drawable/ColorDrawable;

    aget-object v1, v1, v7

    iget-object v2, p0, LX/9dl;->c:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    invoke-virtual {p0}, LX/9dl;->getWidth()I

    move-result v3

    invoke-virtual {p0}, LX/9dl;->getHeight()I

    move-result v4

    invoke-virtual {v1, v2, v5, v3, v4}, Landroid/graphics/drawable/ColorDrawable;->setBounds(IIII)V

    .line 1518587
    :cond_3
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v1, p0, LX/9dl;->c:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    if-le v0, v1, :cond_4

    iget-object v0, p0, LX/9dl;->i:[Landroid/graphics/drawable/ColorDrawable;

    aget-object v0, v0, v8

    if-nez v0, :cond_4

    .line 1518588
    iget-object v0, p0, LX/9dl;->i:[Landroid/graphics/drawable/ColorDrawable;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, LX/9dl;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0732

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    aput-object v1, v0, v8

    .line 1518589
    iget-object v0, p0, LX/9dl;->i:[Landroid/graphics/drawable/ColorDrawable;

    aget-object v0, v0, v8

    iget-object v1, p0, LX/9dl;->c:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, LX/9dl;->c:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    iget-object v3, p0, LX/9dl;->c:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    invoke-virtual {p0}, LX/9dl;->getHeight()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/ColorDrawable;->setBounds(IIII)V

    .line 1518590
    :cond_4
    return-void
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 1518591
    invoke-virtual {p0}, LX/9dl;->getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->g()V

    .line 1518592
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x23f4e635

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1518593
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onAttachedToWindow()V

    .line 1518594
    invoke-direct {p0}, LX/9dl;->l()V

    .line 1518595
    const/16 v1, 0x2d

    const v2, 0x9a9fb88

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x370de2c8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1518596
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 1518597
    invoke-direct {p0}, LX/9dl;->m()V

    .line 1518598
    const/16 v1, 0x2d

    const v2, -0x20540e25

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFinishTemporaryDetach()V
    .locals 0

    .prologue
    .line 1518599
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onFinishTemporaryDetach()V

    .line 1518600
    invoke-direct {p0}, LX/9dl;->l()V

    .line 1518601
    return-void
.end method

.method public final onStartTemporaryDetach()V
    .locals 0

    .prologue
    .line 1518602
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onStartTemporaryDetach()V

    .line 1518603
    invoke-direct {p0}, LX/9dl;->m()V

    .line 1518604
    return-void
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 1518605
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    .line 1518606
    if-eqz v0, :cond_0

    .line 1518607
    const/4 v0, 0x1

    .line 1518608
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, LX/9dl;->getMovableItemContainer()Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->a(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    goto :goto_0
.end method
