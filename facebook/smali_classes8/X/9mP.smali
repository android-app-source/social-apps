.class public abstract LX/9mP;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Landroid/widget/ScrollView;

.field public b:Landroid/widget/LinearLayout;

.field public c:Landroid/widget/TextView;

.field public d:Landroid/widget/TextView;

.field public e:Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;

.field private f:LX/0wM;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;LX/0wM;)V
    .locals 1

    .prologue
    .line 1535605
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1535606
    invoke-virtual {p0, p2}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1535607
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/9mP;->setOrientation(I)V

    .line 1535608
    iput-object p3, p0, LX/9mP;->e:Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;

    .line 1535609
    iput-object p4, p0, LX/9mP;->f:LX/0wM;

    .line 1535610
    const v0, 0x7f0d2826

    invoke-virtual {p0, v0}, LX/9mP;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, LX/9mP;->a:Landroid/widget/ScrollView;

    .line 1535611
    const v0, 0x7f0d2827

    invoke-virtual {p0, v0}, LX/9mP;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/9mP;->b:Landroid/widget/LinearLayout;

    .line 1535612
    const v0, 0x7f0d0cd6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/9mP;->c:Landroid/widget/TextView;

    .line 1535613
    const v0, 0x7f0d2825

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/9mP;->d:Landroid/widget/TextView;

    .line 1535614
    return-void
.end method


# virtual methods
.method public final a(Landroid/widget/TextView;II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1535633
    invoke-virtual {p0}, LX/9mP;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1982

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 1535634
    iget-object v0, p0, LX/9mP;->f:LX/0wM;

    invoke-virtual {v0, p2, p3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {p1, v0, v2, v2, v2}, LX/4lM;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1535635
    return-void
.end method

.method public final a(Landroid/widget/TextView;Ljava/lang/String;LX/0Px;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/TextView;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/rapidreporting/ui/Range;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1535620
    if-eqz p3, :cond_0

    invoke-virtual {p3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1535621
    :cond_0
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1535622
    :goto_0
    return-void

    .line 1535623
    :cond_1
    new-instance v2, Landroid/text/SpannableString;

    invoke-direct {v2, p2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1535624
    invoke-virtual {p3}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    invoke-virtual {p3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/rapidreporting/ui/Range;

    .line 1535625
    if-eqz v0, :cond_2

    .line 1535626
    iget v4, v0, Lcom/facebook/rapidreporting/ui/Range;->b:I

    move v4, v4

    .line 1535627
    iget v5, v0, Lcom/facebook/rapidreporting/ui/Range;->c:I

    move v5, v5

    .line 1535628
    iget-object v6, v0, Lcom/facebook/rapidreporting/ui/Range;->a:Ljava/lang/String;

    move-object v0, v6

    .line 1535629
    new-instance v6, LX/9mO;

    invoke-direct {v6, p0, v0}, LX/9mO;-><init>(LX/9mP;Ljava/lang/String;)V

    add-int v0, v4, v5

    const/16 v5, 0x21

    invoke-virtual {v2, v6, v4, v0, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1535630
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1535631
    :cond_3
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1535632
    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final onSizeChanged(IIII)V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2c

    const v2, 0x57c37fc6

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1535615
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/widget/CustomLinearLayout;->onSizeChanged(IIII)V

    .line 1535616
    iget-object v1, p0, LX/9mP;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v1

    if-le v1, p2, :cond_0

    .line 1535617
    iget-object v1, p0, LX/9mP;->a:Landroid/widget/ScrollView;

    const v2, 0x7f021597

    invoke-virtual {v1, v2}, Landroid/widget/ScrollView;->setBackgroundResource(I)V

    .line 1535618
    :goto_0
    const v1, -0x68fce9dd

    invoke-static {v1, v0}, LX/02F;->g(II)V

    return-void

    .line 1535619
    :cond_0
    iget-object v1, p0, LX/9mP;->a:Landroid/widget/ScrollView;

    const v2, 0x106000d

    invoke-virtual {v1, v2}, Landroid/widget/ScrollView;->setBackgroundResource(I)V

    goto :goto_0
.end method
