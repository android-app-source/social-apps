.class public LX/8kd;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/8j7;


# direct methods
.method public constructor <init>(LX/8j7;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1396604
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1396605
    iput-object p1, p0, LX/8kd;->a:LX/8j7;

    .line 1396606
    return-void
.end method

.method public static a(LX/0QB;)LX/8kd;
    .locals 1

    .prologue
    .line 1396607
    invoke-static {p0}, LX/8kd;->b(LX/0QB;)LX/8kd;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/8kd;
    .locals 2

    .prologue
    .line 1396602
    new-instance v1, LX/8kd;

    invoke-static {p0}, LX/8j7;->a(LX/0QB;)LX/8j7;

    move-result-object v0

    check-cast v0, LX/8j7;

    invoke-direct {v1, v0}, LX/8kd;-><init>(LX/8j7;)V

    .line 1396603
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 1396596
    const-string v0, "sticker_keyboard"

    invoke-static {v0}, LX/8j7;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1396597
    const-string v1, "action"

    const-string v2, "sticker_tab_selected"

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1396598
    const-string v1, "is_promoted"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1396599
    const-string v1, "sticker_pack"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1396600
    iget-object v1, p0, LX/8kd;->a:LX/8j7;

    invoke-virtual {v1, v0}, LX/8j7;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1396601
    return-void
.end method
