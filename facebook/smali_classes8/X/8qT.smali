.class public LX/8qT;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/8qT;


# instance fields
.field public final a:LX/1nJ;

.field private final b:LX/1nL;


# direct methods
.method public constructor <init>(LX/1nJ;LX/1nL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1407207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1407208
    iput-object p1, p0, LX/8qT;->a:LX/1nJ;

    .line 1407209
    iput-object p2, p0, LX/8qT;->b:LX/1nL;

    .line 1407210
    return-void
.end method

.method public static a(LX/0QB;)LX/8qT;
    .locals 5

    .prologue
    .line 1407211
    sget-object v0, LX/8qT;->c:LX/8qT;

    if-nez v0, :cond_1

    .line 1407212
    const-class v1, LX/8qT;

    monitor-enter v1

    .line 1407213
    :try_start_0
    sget-object v0, LX/8qT;->c:LX/8qT;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1407214
    if-eqz v2, :cond_0

    .line 1407215
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1407216
    new-instance p0, LX/8qT;

    invoke-static {v0}, LX/1nJ;->a(LX/0QB;)LX/1nJ;

    move-result-object v3

    check-cast v3, LX/1nJ;

    invoke-static {v0}, LX/1nL;->b(LX/0QB;)LX/1nL;

    move-result-object v4

    check-cast v4, LX/1nL;

    invoke-direct {p0, v3, v4}, LX/8qT;-><init>(LX/1nJ;LX/1nL;)V

    .line 1407217
    move-object v0, p0

    .line 1407218
    sput-object v0, LX/8qT;->c:LX/8qT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1407219
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1407220
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1407221
    :cond_1
    sget-object v0, LX/8qT;->c:LX/8qT;

    return-object v0

    .line 1407222
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1407223
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/8qT;Lcom/facebook/ufiservices/flyout/ProfileListParams;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1407224
    new-instance v0, Lcom/facebook/ufiservices/flyout/UFIProfileListFragment;

    invoke-direct {v0}, Lcom/facebook/ufiservices/flyout/UFIProfileListFragment;-><init>()V

    .line 1407225
    invoke-virtual {p1}, Lcom/facebook/ufiservices/flyout/ProfileListParams;->m()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1407226
    iget-object v1, p0, LX/8qT;->b:LX/1nL;

    invoke-virtual {v1, v0, p2}, LX/1nL;->a(LX/8qC;Landroid/content/Context;)V

    .line 1407227
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1407228
    iget-object v0, p0, LX/8qT;->a:LX/1nJ;

    .line 1407229
    iget-boolean v1, v0, LX/1nJ;->a:Z

    move v0, v1

    .line 1407230
    if-eqz v0, :cond_0

    .line 1407231
    :goto_0
    return-void

    .line 1407232
    :cond_0
    new-instance v0, LX/8qQ;

    invoke-direct {v0}, LX/8qQ;-><init>()V

    .line 1407233
    iput-object p2, v0, LX/8qQ;->c:Ljava/util/List;

    .line 1407234
    move-object v0, v0

    .line 1407235
    sget-object v1, LX/89l;->PROFILES_BY_IDS:LX/89l;

    .line 1407236
    iput-object v1, v0, LX/8qQ;->d:LX/89l;

    .line 1407237
    move-object v0, v0

    .line 1407238
    const/4 v1, 0x1

    .line 1407239
    iput-boolean v1, v0, LX/8qQ;->g:Z

    .line 1407240
    move-object v0, v0

    .line 1407241
    const/4 v1, 0x0

    .line 1407242
    iput-boolean v1, v0, LX/8qQ;->f:Z

    .line 1407243
    move-object v0, v0

    .line 1407244
    invoke-virtual {v0}, LX/8qQ;->a()Lcom/facebook/ufiservices/flyout/ProfileListParams;

    move-result-object v0

    .line 1407245
    invoke-static {p0, v0, p1}, LX/8qT;->a(LX/8qT;Lcom/facebook/ufiservices/flyout/ProfileListParams;Landroid/content/Context;)V

    goto :goto_0
.end method
