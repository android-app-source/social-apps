.class public LX/8zR;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/8zQ;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8zT;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1427403
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/8zR;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/8zT;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1427404
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1427405
    iput-object p1, p0, LX/8zR;->b:LX/0Ot;

    .line 1427406
    return-void
.end method

.method public static a(LX/0QB;)LX/8zR;
    .locals 4

    .prologue
    .line 1427407
    const-class v1, LX/8zR;

    monitor-enter v1

    .line 1427408
    :try_start_0
    sget-object v0, LX/8zR;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1427409
    sput-object v2, LX/8zR;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1427410
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1427411
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1427412
    new-instance v3, LX/8zR;

    const/16 p0, 0x1991

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/8zR;-><init>(LX/0Ot;)V

    .line 1427413
    move-object v0, v3

    .line 1427414
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1427415
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8zR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1427416
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1427417
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 1427418
    check-cast p2, LX/8zP;

    .line 1427419
    iget-object v0, p0, LX/8zR;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8zT;

    iget-object v2, p2, LX/8zP;->a:LX/8zZ;

    iget-object v3, p2, LX/8zP;->b:Ljava/lang/String;

    iget-object v4, p2, LX/8zP;->c:LX/8zS;

    iget-object v5, p2, LX/8zP;->d:Landroid/view/View$OnClickListener;

    iget-object v6, p2, LX/8zP;->e:LX/1OX;

    move-object v1, p1

    const/4 p2, 0x0

    .line 1427420
    sget-object v7, LX/8zS;->NETWORK_ERROR:LX/8zS;

    if-ne v4, v7, :cond_0

    .line 1427421
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v7

    iget-object p0, v0, LX/8zT;->a:LX/91K;

    invoke-virtual {p0, v1}, LX/91K;->c(LX/1De;)LX/91J;

    move-result-object p0

    const p1, 0x7f08003b

    invoke-virtual {p0, p1}, LX/91J;->h(I)LX/91J;

    move-result-object p0

    invoke-virtual {p0, v5}, LX/91J;->a(Landroid/view/View$OnClickListener;)LX/91J;

    move-result-object p0

    invoke-interface {v7, p0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v7

    invoke-interface {v7}, LX/1Di;->k()LX/1Dg;

    move-result-object v7

    .line 1427422
    :goto_0
    move-object v0, v7

    .line 1427423
    return-object v0

    .line 1427424
    :cond_0
    sget-object v7, LX/8zS;->GENERIC_ERROR:LX/8zS;

    if-ne v4, v7, :cond_1

    .line 1427425
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v7

    iget-object p0, v0, LX/8zT;->a:LX/91K;

    invoke-virtual {p0, v1}, LX/91K;->c(LX/1De;)LX/91J;

    move-result-object p0

    invoke-virtual {p0, v5}, LX/91J;->a(Landroid/view/View$OnClickListener;)LX/91J;

    move-result-object p0

    invoke-interface {v7, p0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v7

    invoke-interface {v7}, LX/1Di;->k()LX/1Dg;

    move-result-object v7

    goto :goto_0

    .line 1427426
    :cond_1
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v7

    const/4 p0, 0x4

    invoke-interface {v7, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v7

    invoke-static {v1}, LX/91W;->c(LX/1De;)LX/91U;

    move-result-object p0

    invoke-virtual {p0, v3}, LX/91U;->a(Ljava/lang/CharSequence;)LX/91U;

    move-result-object p0

    .line 1427427
    const p1, -0x37a597f4

    const/4 v0, 0x0

    invoke-static {v1, p1, v0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p1

    move-object p1, p1

    .line 1427428
    invoke-virtual {p0, p1}, LX/91U;->a(LX/1dQ;)LX/91U;

    move-result-object p0

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, LX/91U;->h(I)LX/91U;

    move-result-object p0

    invoke-interface {v7, p0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v7

    invoke-static {v1}, LX/5K1;->c(LX/1De;)LX/5Jz;

    move-result-object p0

    invoke-virtual {p0, v2}, LX/5Jz;->a(LX/5Je;)LX/5Jz;

    move-result-object p0

    invoke-virtual {p0, v6}, LX/5Jz;->a(LX/1OX;)LX/5Jz;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    invoke-interface {p0, p2}, LX/1Di;->a(I)LX/1Di;

    move-result-object p0

    const/high16 p1, 0x3f800000    # 1.0f

    invoke-interface {p0, p1}, LX/1Di;->a(F)LX/1Di;

    move-result-object p0

    invoke-interface {v7, p0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object p0

    sget-object v7, LX/8zS;->LOADING:LX/8zS;

    if-ne v4, v7, :cond_2

    invoke-static {v1}, LX/5Jt;->c(LX/1De;)LX/5Jr;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    const/16 p1, 0x1e

    invoke-interface {v7, p1}, LX/1Di;->r(I)LX/1Di;

    move-result-object v7

    :goto_1
    invoke-interface {p0, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v7

    invoke-interface {v7}, LX/1Di;->k()LX/1Dg;

    move-result-object v7

    goto :goto_0

    :cond_2
    const/4 v7, 0x0

    goto :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1427429
    invoke-static {}, LX/1dS;->b()V

    .line 1427430
    iget v0, p1, LX/1dQ;->b:I

    .line 1427431
    packed-switch v0, :pswitch_data_0

    .line 1427432
    :goto_0
    return-object v2

    .line 1427433
    :pswitch_0
    check-cast p2, LX/91c;

    .line 1427434
    iget-object v0, p2, LX/91c;->a:Ljava/lang/String;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1427435
    check-cast v1, LX/8zP;

    .line 1427436
    iget-object p1, p0, LX/8zR;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, LX/8zP;->f:LX/8zz;

    .line 1427437
    if-eqz p1, :cond_0

    .line 1427438
    invoke-interface {p1, v0}, LX/8zz;->a(Ljava/lang/String;)V

    .line 1427439
    :cond_0
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x37a597f4
        :pswitch_0
    .end packed-switch
.end method
