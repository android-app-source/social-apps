.class public final LX/9W3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:LX/9WC;


# direct methods
.method public constructor <init>(LX/9WC;)V
    .locals 0

    .prologue
    .line 1500508
    iput-object p1, p0, LX/9W3;->a:LX/9WC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 13

    .prologue
    .line 1500509
    iget-object v0, p0, LX/9W3;->a:LX/9WC;

    iget-object v0, v0, LX/9WC;->u:LX/9Vy;

    check-cast v0, LX/9WQ;

    .line 1500510
    if-nez v0, :cond_1

    .line 1500511
    :cond_0
    :goto_0
    return-void

    .line 1500512
    :cond_1
    iget-object v1, p0, LX/9W3;->a:LX/9WC;

    iget-object v1, v1, LX/9WC;->k:LX/9Vx;

    .line 1500513
    iget-boolean v2, v1, LX/9Vx;->g:Z

    move v1, v2

    .line 1500514
    if-eqz v1, :cond_3

    .line 1500515
    iget-object v1, p0, LX/9W3;->a:LX/9WC;

    iget-object v1, v1, LX/9WC;->k:LX/9Vx;

    .line 1500516
    iget-object v2, v1, LX/9Vx;->e:Ljava/lang/String;

    move-object v1, v2

    .line 1500517
    :goto_1
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, LX/9WQ;->getMessageBody()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1500518
    invoke-virtual {v0}, LX/9WQ;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0}, LX/2Na;->a(Landroid/content/Context;Landroid/view/View;)V

    .line 1500519
    iget-object v2, p0, LX/9W3;->a:LX/9WC;

    iget-object v3, p0, LX/9W3;->a:LX/9WC;

    iget-object v3, v3, LX/9WC;->k:LX/9Vx;

    .line 1500520
    iget-object p0, v3, LX/9Vx;->a:Ljava/lang/String;

    move-object v3, p0

    .line 1500521
    invoke-virtual {v0}, LX/9WQ;->getMessageBody()Ljava/lang/String;

    move-result-object v0

    .line 1500522
    iget-object v4, v2, LX/9WC;->g:LX/2EJ;

    const/4 v5, -0x2

    invoke-virtual {v4, v5}, LX/2EJ;->a(I)Landroid/widget/Button;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1500523
    iget-object v4, v2, LX/9WC;->f:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/8wj;

    .line 1500524
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .line 1500525
    new-instance v7, LX/8wh;

    invoke-direct {v7}, LX/8wh;-><init>()V

    .line 1500526
    iput-object v3, v7, LX/8wh;->a:Ljava/lang/String;

    .line 1500527
    move-object v7, v7

    .line 1500528
    iput-object v0, v7, LX/8wh;->b:Ljava/lang/String;

    .line 1500529
    move-object v7, v7

    .line 1500530
    iput-object v1, v7, LX/8wh;->c:Ljava/lang/String;

    .line 1500531
    move-object v7, v7

    .line 1500532
    new-instance v8, Lcom/facebook/api/negative_feedback/NegativeFeedbackMessageActionMethod$Params;

    invoke-direct {v8, v7}, Lcom/facebook/api/negative_feedback/NegativeFeedbackMessageActionMethod$Params;-><init>(LX/8wh;)V

    move-object v7, v8

    .line 1500533
    const-string v8, "negativeFeedbackMessageActionParams"

    invoke-virtual {v9, v8, v7}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1500534
    iget-object v7, v4, LX/8wj;->a:LX/0aG;

    const-string v8, "negative_feedback_message_actions"

    sget-object v10, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const/4 v11, 0x0

    const v12, -0x528e2777

    invoke-static/range {v7 .. v12}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v7

    invoke-interface {v7}, LX/1MF;->start()LX/1ML;

    move-result-object v7

    move-object v4, v7

    .line 1500535
    iget-object v5, v2, LX/9WC;->r:LX/9Ve;

    .line 1500536
    iget-object v6, v5, LX/9Ve;->a:LX/0Zb;

    const-string v7, "negativefeedback_message_action"

    const/4 v8, 0x0

    invoke-interface {v6, v7, v8}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v6

    .line 1500537
    invoke-virtual {v6}, LX/0oG;->a()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1500538
    const-string v7, "graphql_token"

    invoke-virtual {v6, v7, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1500539
    const-string v7, "message_body"

    invoke-virtual {v6, v7, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1500540
    const-string v7, "recipient_id"

    invoke-virtual {v6, v7, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1500541
    iget-object v7, v5, LX/9Ve;->b:Ljava/util/Map;

    invoke-virtual {v6, v7}, LX/0oG;->a(Ljava/util/Map;)LX/0oG;

    .line 1500542
    invoke-virtual {v6}, LX/0oG;->d()V

    .line 1500543
    :cond_2
    iget-object v5, v2, LX/9WC;->e:LX/0Sh;

    new-instance v6, LX/9WA;

    invoke-direct {v6, v2}, LX/9WA;-><init>(LX/9WC;)V

    invoke-virtual {v5, v4, v6}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1500544
    goto/16 :goto_0

    .line 1500545
    :cond_3
    iget-object v1, v0, LX/9WQ;->c:Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientAutoCompleteTextView;

    if-eqz v1, :cond_4

    .line 1500546
    iget-object v1, v0, LX/9WQ;->c:Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/facebook/negativefeedback/ui/messagecomposer/MessageRecipientAutoCompleteTextView;->getSelectedProfileId()Ljava/lang/String;

    move-result-object v1

    .line 1500547
    :goto_2
    move-object v1, v1

    .line 1500548
    goto/16 :goto_1

    :cond_4
    const-string v1, ""

    goto :goto_2
.end method
