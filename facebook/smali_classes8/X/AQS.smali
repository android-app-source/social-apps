.class public final LX/AQS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;)V
    .locals 0

    .prologue
    .line 1671403
    iput-object p1, p0, LX/AQS;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 1671404
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1671405
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3

    .prologue
    .line 1671406
    iget-object v0, p0, LX/AQS;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;

    iget-object v1, v0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->j:Landroid/view/View;

    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1671407
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1671408
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/AQS;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;

    iget-object v1, v1, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->n:LX/AQb;

    .line 1671409
    iget-object v2, v1, LX/AQb;->a:LX/3CE;

    invoke-virtual {v2, v0}, LX/18f;->f(Ljava/lang/Object;)Z

    move-result v2

    move v1, v2

    .line 1671410
    if-eqz v1, :cond_2

    .line 1671411
    :cond_0
    iget-object v1, p0, LX/AQS;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;

    iget-object v1, v1, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->m:LX/AQZ;

    iget-object v2, p0, LX/AQS;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;

    iget-object v2, v2, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->n:LX/AQb;

    invoke-virtual {v2, v0}, LX/AQb;->b(Ljava/lang/String;)LX/0Px;

    move-result-object v0

    .line 1671412
    iput-object v0, v1, LX/AQZ;->b:LX/0Px;

    .line 1671413
    iget-object v0, p0, LX/AQS;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;

    iget-object v0, v0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->m:LX/AQZ;

    const v1, -0x2730b963

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1671414
    :goto_1
    return-void

    .line 1671415
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1671416
    :cond_2
    iget-object v1, p0, LX/AQS;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;

    invoke-virtual {v1, v0}, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 1671417
    new-instance v2, LX/AQR;

    invoke-direct {v2, p0, v0}, LX/AQR;-><init>(LX/AQS;Ljava/lang/String;)V

    iget-object v0, p0, LX/AQS;->a:Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;

    iget-object v0, v0, Lcom/facebook/composer/lifeevent/interstitial/ComposerLifeEventInterstitialFragment;->d:LX/0Tf;

    invoke-static {v1, v2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_1
.end method
