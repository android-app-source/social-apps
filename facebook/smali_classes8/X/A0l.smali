.class public final LX/A0l;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 1607510
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 1607511
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1607512
    :goto_0
    return v1

    .line 1607513
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1607514
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_5

    .line 1607515
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1607516
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1607517
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 1607518
    const-string v6, "corrected_query"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1607519
    const/4 v5, 0x0

    .line 1607520
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v6, :cond_b

    .line 1607521
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1607522
    :goto_2
    move v4, v5

    .line 1607523
    goto :goto_1

    .line 1607524
    :cond_2
    const-string v6, "session_id"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1607525
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 1607526
    :cond_3
    const-string v6, "speller_confidence"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1607527
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    goto :goto_1

    .line 1607528
    :cond_4
    const-string v6, "vertical_to_log"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1607529
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 1607530
    :cond_5
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1607531
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1607532
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1607533
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1607534
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1607535
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto/16 :goto_1

    .line 1607536
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1607537
    :cond_8
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_a

    .line 1607538
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1607539
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1607540
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_8

    if-eqz v7, :cond_8

    .line 1607541
    const-string v8, "query_function"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 1607542
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_3

    .line 1607543
    :cond_9
    const-string v8, "query_title"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 1607544
    const/4 v7, 0x0

    .line 1607545
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v8, :cond_f

    .line 1607546
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1607547
    :goto_4
    move v4, v7

    .line 1607548
    goto :goto_3

    .line 1607549
    :cond_a
    const/4 v7, 0x2

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1607550
    invoke-virtual {p1, v5, v6}, LX/186;->b(II)V

    .line 1607551
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, LX/186;->b(II)V

    .line 1607552
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto/16 :goto_2

    :cond_b
    move v4, v5

    move v6, v5

    goto :goto_3

    .line 1607553
    :cond_c
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1607554
    :cond_d
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_e

    .line 1607555
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1607556
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1607557
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_d

    if-eqz v8, :cond_d

    .line 1607558
    const-string v9, "text"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 1607559
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_5

    .line 1607560
    :cond_e
    const/4 v8, 0x1

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1607561
    invoke-virtual {p1, v7, v4}, LX/186;->b(II)V

    .line 1607562
    invoke-virtual {p1}, LX/186;->d()I

    move-result v7

    goto :goto_4

    :cond_f
    move v4, v7

    goto :goto_5
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    .line 1607563
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1607564
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1607565
    if-eqz v0, :cond_3

    .line 1607566
    const-string v1, "corrected_query"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1607567
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1607568
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1607569
    if-eqz v1, :cond_0

    .line 1607570
    const-string v3, "query_function"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1607571
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1607572
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1607573
    if-eqz v1, :cond_2

    .line 1607574
    const-string v3, "query_title"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1607575
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1607576
    const/4 v3, 0x0

    invoke-virtual {p0, v1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1607577
    if-eqz v3, :cond_1

    .line 1607578
    const-string v0, "text"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1607579
    invoke-virtual {p2, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1607580
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1607581
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1607582
    :cond_3
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1607583
    if-eqz v0, :cond_4

    .line 1607584
    const-string v1, "session_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1607585
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1607586
    :cond_4
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1607587
    if-eqz v0, :cond_5

    .line 1607588
    const-string v0, "speller_confidence"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1607589
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1607590
    :cond_5
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1607591
    if-eqz v0, :cond_6

    .line 1607592
    const-string v1, "vertical_to_log"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1607593
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1607594
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1607595
    return-void
.end method
