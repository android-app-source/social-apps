.class public LX/ASh;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public b:Landroid/widget/ImageView;

.field public c:F

.field public d:F

.field public e:Lcom/facebook/gif/AnimatedImagePlayButtonView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1674375
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/ASh;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1674376
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1674377
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/ASh;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1674378
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1674379
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1674380
    const v0, 0x7f031568

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1674381
    const v0, 0x7f0d0a7d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/ASh;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1674382
    const v0, 0x7f0d0a7e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/ASh;->b:Landroid/widget/ImageView;

    .line 1674383
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, LX/ASh;->c:F

    .line 1674384
    const v0, 0x7f0d302f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/gif/AnimatedImagePlayButtonView;

    iput-object v0, p0, LX/ASh;->e:Lcom/facebook/gif/AnimatedImagePlayButtonView;

    .line 1674385
    iget-object v0, p0, LX/ASh;->e:Lcom/facebook/gif/AnimatedImagePlayButtonView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/gif/AnimatedImagePlayButtonView;->setVisibility(I)V

    .line 1674386
    new-instance v0, LX/1Uo;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 1674387
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const p2, 0x7f021af6

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1674388
    new-instance p2, Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const/16 p3, 0x3e8

    invoke-direct {p2, v1, p3}, Lcom/facebook/drawee/drawable/AutoRotateDrawable;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 1674389
    iput-object p2, v0, LX/1Uo;->l:Landroid/graphics/drawable/Drawable;

    .line 1674390
    iget-object v1, p0, LX/ASh;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 1674391
    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 3

    .prologue
    .line 1674392
    iget v0, p0, LX/ASh;->d:F

    move v1, v0

    .line 1674393
    invoke-virtual {p0}, LX/ASh;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    new-instance v2, LX/ATB;

    invoke-direct {v2, p1, p2}, LX/ATB;-><init>(II)V

    invoke-static {v1, v0, v2}, LX/ATC;->a(FLandroid/widget/FrameLayout$LayoutParams;LX/ATB;)LX/ATB;

    move-result-object v0

    .line 1674394
    iget v1, v0, LX/ATB;->a:I

    iget v0, v0, LX/ATB;->b:I

    invoke-super {p0, v1, v0}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    .line 1674395
    return-void
.end method

.method public setAspectRatio(F)V
    .locals 1

    .prologue
    .line 1674396
    iput p1, p0, LX/ASh;->d:F

    .line 1674397
    iget-object v0, p0, LX/ASh;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 1674398
    return-void
.end method

.method public setController(LX/1aZ;)V
    .locals 1

    .prologue
    .line 1674399
    iget-object v0, p0, LX/ASh;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1674400
    return-void
.end method

.method public setPlayButtonState(LX/6Wv;)V
    .locals 1

    .prologue
    .line 1674401
    iget-object v0, p0, LX/ASh;->e:Lcom/facebook/gif/AnimatedImagePlayButtonView;

    if-eqz v0, :cond_0

    .line 1674402
    iget-object v0, p0, LX/ASh;->e:Lcom/facebook/gif/AnimatedImagePlayButtonView;

    invoke-virtual {v0, p1}, Lcom/facebook/gif/AnimatedImagePlayButtonView;->setState(LX/6Wv;)V

    .line 1674403
    :cond_0
    return-void
.end method
