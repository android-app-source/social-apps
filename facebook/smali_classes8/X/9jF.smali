.class public final LX/9jF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public a:Z

.field public b:Z

.field public c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/ipc/composer/model/ComposerLocation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Z

.field public i:Z

.field public j:Z

.field public k:Z

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/composer/minutiae/model/MinutiaeObject;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/ipc/composer/model/ComposerLocation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:LX/9jG;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;"
        }
    .end annotation
.end field

.field public s:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public t:Z

.field public u:Z

.field public v:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Z

.field public y:Z

.field public z:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1529295
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1529296
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1529297
    iput-object v0, p0, LX/9jF;->r:LX/0Px;

    .line 1529298
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1529299
    iput-object v0, p0, LX/9jF;->s:LX/0Px;

    .line 1529300
    return-void
.end method

.method public constructor <init>(Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;)V
    .locals 1

    .prologue
    .line 1529301
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1529302
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1529303
    instance-of v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    if-eqz v0, :cond_0

    .line 1529304
    check-cast p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 1529305
    iget-boolean v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->a:Z

    iput-boolean v0, p0, LX/9jF;->a:Z

    .line 1529306
    iget-boolean v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->b:Z

    iput-boolean v0, p0, LX/9jF;->b:Z

    .line 1529307
    iget-object v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    iput-object v0, p0, LX/9jF;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1529308
    iget-object v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->d:Ljava/lang/String;

    iput-object v0, p0, LX/9jF;->d:Ljava/lang/String;

    .line 1529309
    iget-object v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->e:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    iput-object v0, p0, LX/9jF;->e:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1529310
    iget-object v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->f:Lcom/facebook/ipc/composer/model/ComposerLocation;

    iput-object v0, p0, LX/9jF;->f:Lcom/facebook/ipc/composer/model/ComposerLocation;

    .line 1529311
    iget-object v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->g:Ljava/lang/String;

    iput-object v0, p0, LX/9jF;->g:Ljava/lang/String;

    .line 1529312
    iget-boolean v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->h:Z

    iput-boolean v0, p0, LX/9jF;->h:Z

    .line 1529313
    iget-boolean v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->i:Z

    iput-boolean v0, p0, LX/9jF;->i:Z

    .line 1529314
    iget-boolean v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->j:Z

    iput-boolean v0, p0, LX/9jF;->j:Z

    .line 1529315
    iget-boolean v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->k:Z

    iput-boolean v0, p0, LX/9jF;->k:Z

    .line 1529316
    iget-object v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->l:Ljava/lang/String;

    iput-object v0, p0, LX/9jF;->l:Ljava/lang/String;

    .line 1529317
    iget-object v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->m:Ljava/lang/String;

    iput-object v0, p0, LX/9jF;->m:Ljava/lang/String;

    .line 1529318
    iget-object v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->n:Ljava/lang/String;

    iput-object v0, p0, LX/9jF;->n:Ljava/lang/String;

    .line 1529319
    iget-object v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->o:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    iput-object v0, p0, LX/9jF;->o:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1529320
    iget-object v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->p:Lcom/facebook/ipc/composer/model/ComposerLocation;

    iput-object v0, p0, LX/9jF;->p:Lcom/facebook/ipc/composer/model/ComposerLocation;

    .line 1529321
    iget-object v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->q:LX/9jG;

    iput-object v0, p0, LX/9jF;->q:LX/9jG;

    .line 1529322
    iget-object v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->r:LX/0Px;

    iput-object v0, p0, LX/9jF;->r:LX/0Px;

    .line 1529323
    iget-object v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->s:LX/0Px;

    iput-object v0, p0, LX/9jF;->s:LX/0Px;

    .line 1529324
    iget-boolean v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->t:Z

    iput-boolean v0, p0, LX/9jF;->t:Z

    .line 1529325
    iget-boolean v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->u:Z

    iput-boolean v0, p0, LX/9jF;->u:Z

    .line 1529326
    iget-object v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->v:Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, LX/9jF;->v:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1529327
    iget-object v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->w:Ljava/lang/String;

    iput-object v0, p0, LX/9jF;->w:Ljava/lang/String;

    .line 1529328
    iget-boolean v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->x:Z

    iput-boolean v0, p0, LX/9jF;->x:Z

    .line 1529329
    iget-boolean v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->y:Z

    iput-boolean v0, p0, LX/9jF;->y:Z

    .line 1529330
    iget-boolean v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->z:Z

    iput-boolean v0, p0, LX/9jF;->z:Z

    .line 1529331
    iget-object v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->A:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    iput-object v0, p0, LX/9jF;->A:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    .line 1529332
    iget-object v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->B:Ljava/lang/String;

    iput-object v0, p0, LX/9jF;->B:Ljava/lang/String;

    .line 1529333
    :goto_0
    return-void

    .line 1529334
    :cond_0
    iget-boolean v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->a:Z

    move v0, v0

    .line 1529335
    iput-boolean v0, p0, LX/9jF;->a:Z

    .line 1529336
    iget-boolean v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->b:Z

    move v0, v0

    .line 1529337
    iput-boolean v0, p0, LX/9jF;->b:Z

    .line 1529338
    iget-object v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v0, v0

    .line 1529339
    iput-object v0, p0, LX/9jF;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1529340
    iget-object v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1529341
    iput-object v0, p0, LX/9jF;->d:Ljava/lang/String;

    .line 1529342
    iget-object v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->e:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-object v0, v0

    .line 1529343
    iput-object v0, p0, LX/9jF;->e:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1529344
    iget-object v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->f:Lcom/facebook/ipc/composer/model/ComposerLocation;

    move-object v0, v0

    .line 1529345
    iput-object v0, p0, LX/9jF;->f:Lcom/facebook/ipc/composer/model/ComposerLocation;

    .line 1529346
    iget-object v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->g:Ljava/lang/String;

    move-object v0, v0

    .line 1529347
    iput-object v0, p0, LX/9jF;->g:Ljava/lang/String;

    .line 1529348
    iget-boolean v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->h:Z

    move v0, v0

    .line 1529349
    iput-boolean v0, p0, LX/9jF;->h:Z

    .line 1529350
    iget-boolean v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->i:Z

    move v0, v0

    .line 1529351
    iput-boolean v0, p0, LX/9jF;->i:Z

    .line 1529352
    iget-boolean v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->j:Z

    move v0, v0

    .line 1529353
    iput-boolean v0, p0, LX/9jF;->j:Z

    .line 1529354
    iget-boolean v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->k:Z

    move v0, v0

    .line 1529355
    iput-boolean v0, p0, LX/9jF;->k:Z

    .line 1529356
    iget-object v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->l:Ljava/lang/String;

    move-object v0, v0

    .line 1529357
    iput-object v0, p0, LX/9jF;->l:Ljava/lang/String;

    .line 1529358
    iget-object v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->m:Ljava/lang/String;

    move-object v0, v0

    .line 1529359
    iput-object v0, p0, LX/9jF;->m:Ljava/lang/String;

    .line 1529360
    iget-object v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->n:Ljava/lang/String;

    move-object v0, v0

    .line 1529361
    iput-object v0, p0, LX/9jF;->n:Ljava/lang/String;

    .line 1529362
    iget-object v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->o:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-object v0, v0

    .line 1529363
    iput-object v0, p0, LX/9jF;->o:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1529364
    iget-object v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->p:Lcom/facebook/ipc/composer/model/ComposerLocation;

    move-object v0, v0

    .line 1529365
    iput-object v0, p0, LX/9jF;->p:Lcom/facebook/ipc/composer/model/ComposerLocation;

    .line 1529366
    iget-object v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->q:LX/9jG;

    move-object v0, v0

    .line 1529367
    iput-object v0, p0, LX/9jF;->q:LX/9jG;

    .line 1529368
    iget-object v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->r:LX/0Px;

    move-object v0, v0

    .line 1529369
    iput-object v0, p0, LX/9jF;->r:LX/0Px;

    .line 1529370
    iget-object v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->s:LX/0Px;

    move-object v0, v0

    .line 1529371
    iput-object v0, p0, LX/9jF;->s:LX/0Px;

    .line 1529372
    iget-boolean v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->t:Z

    move v0, v0

    .line 1529373
    iput-boolean v0, p0, LX/9jF;->t:Z

    .line 1529374
    iget-boolean v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->u:Z

    move v0, v0

    .line 1529375
    iput-boolean v0, p0, LX/9jF;->u:Z

    .line 1529376
    iget-object v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->v:Lcom/facebook/graphql/model/GraphQLFeedback;

    move-object v0, v0

    .line 1529377
    iput-object v0, p0, LX/9jF;->v:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1529378
    iget-object v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->w:Ljava/lang/String;

    move-object v0, v0

    .line 1529379
    iput-object v0, p0, LX/9jF;->w:Ljava/lang/String;

    .line 1529380
    iget-boolean v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->x:Z

    move v0, v0

    .line 1529381
    iput-boolean v0, p0, LX/9jF;->x:Z

    .line 1529382
    iget-boolean v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->y:Z

    move v0, v0

    .line 1529383
    iput-boolean v0, p0, LX/9jF;->y:Z

    .line 1529384
    iget-boolean v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->z:Z

    move v0, v0

    .line 1529385
    iput-boolean v0, p0, LX/9jF;->z:Z

    .line 1529386
    iget-object v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->A:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    move-object v0, v0

    .line 1529387
    iput-object v0, p0, LX/9jF;->A:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    .line 1529388
    iget-object v0, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->B:Ljava/lang/String;

    move-object v0, v0

    .line 1529389
    iput-object v0, p0, LX/9jF;->B:Ljava/lang/String;

    goto/16 :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;
    .locals 2

    .prologue
    .line 1529390
    new-instance v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    invoke-direct {v0, p0}, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;-><init>(LX/9jF;)V

    return-object v0
.end method
