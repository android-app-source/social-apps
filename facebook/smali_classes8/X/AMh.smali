.class public final enum LX/AMh;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/AMh;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/AMh;

.field public static final enum DOWNLOAD_MSQRD_MASKS:LX/AMh;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1666357
    new-instance v0, LX/AMh;

    const-string v1, "DOWNLOAD_MSQRD_MASKS"

    invoke-direct {v0, v1, v2}, LX/AMh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AMh;->DOWNLOAD_MSQRD_MASKS:LX/AMh;

    .line 1666358
    const/4 v0, 0x1

    new-array v0, v0, [LX/AMh;

    sget-object v1, LX/AMh;->DOWNLOAD_MSQRD_MASKS:LX/AMh;

    aput-object v1, v0, v2

    sput-object v0, LX/AMh;->$VALUES:[LX/AMh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1666359
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/AMh;
    .locals 1

    .prologue
    .line 1666360
    const-class v0, LX/AMh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/AMh;

    return-object v0
.end method

.method public static values()[LX/AMh;
    .locals 1

    .prologue
    .line 1666361
    sget-object v0, LX/AMh;->$VALUES:[LX/AMh;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/AMh;

    return-object v0
.end method
