.class public abstract LX/9Uh;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/9Uf;",
            ">;"
        }
    .end annotation
.end field

.field private final b:I

.field private final c:I

.field private d:J

.field public e:Z

.field private f:I

.field public g:J

.field private h:J


# direct methods
.method public constructor <init>(LX/9Uf;II)V
    .locals 3

    .prologue
    .line 1498449
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1498450
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/9Uh;->g:J

    .line 1498451
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/9Uh;->h:J

    .line 1498452
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/9Uh;->a:Ljava/lang/ref/WeakReference;

    .line 1498453
    iput p3, p0, LX/9Uh;->b:I

    .line 1498454
    const/high16 v0, 0x447a0000    # 1000.0f

    int-to-float v1, p3

    int-to-float v2, p2

    div-float/2addr v1, v2

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, LX/9Uh;->c:I

    .line 1498455
    return-void
.end method


# virtual methods
.method public abstract a()V
.end method

.method public final a(J)V
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const-wide/16 v6, 0x0

    .line 1498456
    iget-object v0, p0, LX/9Uh;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1498457
    invoke-virtual {p0}, LX/9Uh;->b()V

    .line 1498458
    iput-wide v6, p0, LX/9Uh;->d:J

    .line 1498459
    iput-wide v6, p0, LX/9Uh;->h:J

    .line 1498460
    const/4 v0, -0x1

    iput v0, p0, LX/9Uh;->f:I

    .line 1498461
    :goto_0
    return-void

    .line 1498462
    :cond_0
    iget-wide v4, p0, LX/9Uh;->d:J

    cmp-long v0, v4, v6

    if-nez v0, :cond_1

    .line 1498463
    iput-wide p1, p0, LX/9Uh;->d:J

    .line 1498464
    :cond_1
    iget-wide v4, p0, LX/9Uh;->d:J

    sub-long v4, p1, v4

    long-to-int v0, v4

    iget v3, p0, LX/9Uh;->c:I

    div-int/2addr v0, v3

    .line 1498465
    iget v3, p0, LX/9Uh;->f:I

    if-le v0, v3, :cond_2

    move v0, v1

    .line 1498466
    :goto_1
    iget-boolean v3, p0, LX/9Uh;->e:Z

    if-eqz v3, :cond_3

    if-eqz v0, :cond_3

    .line 1498467
    iget-object v0, p0, LX/9Uh;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Uf;

    iget v1, p0, LX/9Uh;->b:I

    int-to-float v1, v1

    invoke-interface {v0, v1}, LX/9Uf;->a(F)V

    .line 1498468
    invoke-virtual {p0}, LX/9Uh;->d()V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 1498469
    goto :goto_1

    .line 1498470
    :cond_3
    iget-wide v4, p0, LX/9Uh;->d:J

    sub-long v4, p1, v4

    iget v0, p0, LX/9Uh;->c:I

    int-to-long v6, v0

    rem-long/2addr v4, v6

    .line 1498471
    iget-wide v6, p0, LX/9Uh;->h:J

    sub-long v6, p1, v6

    iget-wide v8, p0, LX/9Uh;->g:J

    cmp-long v0, v6, v8

    if-gez v0, :cond_5

    .line 1498472
    :goto_2
    if-eqz v2, :cond_4

    .line 1498473
    iget-object v0, p0, LX/9Uh;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Uf;

    long-to-float v1, v4

    iget v2, p0, LX/9Uh;->c:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iget v2, p0, LX/9Uh;->b:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    invoke-interface {v0, v1}, LX/9Uf;->a(F)V

    .line 1498474
    :cond_4
    iget-wide v0, p0, LX/9Uh;->d:J

    sub-long v0, p1, v0

    long-to-int v0, v0

    iget v1, p0, LX/9Uh;->c:I

    div-int/2addr v0, v1

    iput v0, p0, LX/9Uh;->f:I

    .line 1498475
    invoke-virtual {p0}, LX/9Uh;->a()V

    goto :goto_0

    .line 1498476
    :cond_5
    iput-wide p1, p0, LX/9Uh;->h:J

    move v2, v1

    goto :goto_2
.end method

.method public abstract b()V
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1498477
    invoke-virtual {p0}, LX/9Uh;->b()V

    .line 1498478
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/9Uh;->d:J

    .line 1498479
    const/4 v0, -0x1

    iput v0, p0, LX/9Uh;->f:I

    .line 1498480
    iget-object v0, p0, LX/9Uh;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Uf;

    invoke-interface {v0}, LX/9Uf;->d()V

    .line 1498481
    return-void
.end method
