.class public LX/8n9;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/8n8;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1400602
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1400603
    return-void
.end method


# virtual methods
.method public final a(LX/8ox;)LX/8n8;
    .locals 13

    .prologue
    .line 1400604
    new-instance v0, LX/8n8;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/8p3;->b(LX/0QB;)LX/8p3;

    move-result-object v3

    check-cast v3, LX/8p3;

    const-class v2, LX/8n3;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/8n3;

    .line 1400605
    new-instance v5, LX/8ol;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v2

    check-cast v2, LX/0Zb;

    invoke-direct {v5, v2}, LX/8ol;-><init>(LX/0Zb;)V

    .line 1400606
    move-object v5, v5

    .line 1400607
    check-cast v5, LX/8ol;

    invoke-static {p0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v6

    check-cast v6, LX/01T;

    const-class v2, LX/8nX;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/8nX;

    .line 1400608
    new-instance v9, LX/8n5;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-static {p0}, LX/8p4;->b(LX/0QB;)LX/8p4;

    move-result-object v8

    check-cast v8, LX/8p4;

    const/16 v10, 0x35da

    invoke-static {p0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    const/16 v11, 0x35d7

    invoke-static {p0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-direct {v9, v2, v8, v10, v11}, LX/8n5;-><init>(LX/0Sh;LX/8p4;LX/0Or;LX/0Or;)V

    .line 1400609
    move-object v8, v9

    .line 1400610
    check-cast v8, LX/8n5;

    const/16 v2, 0x35de

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v11

    check-cast v11, LX/0ad;

    invoke-static {p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v12

    check-cast v12, Ljava/lang/Boolean;

    move-object v2, p1

    invoke-direct/range {v0 .. v12}, LX/8n8;-><init>(Landroid/content/Context;LX/8ox;LX/8p3;LX/8n3;LX/8ol;LX/01T;LX/8nX;LX/8n5;LX/0Ot;LX/0Uh;LX/0ad;Ljava/lang/Boolean;)V

    .line 1400611
    return-object v0
.end method
