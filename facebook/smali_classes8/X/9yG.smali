.class public final LX/9yG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingTagsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/rapidreporting/ui/DialogStateData;

.field public final synthetic b:LX/0gc;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:Landroid/content/Context;

.field public final synthetic g:LX/9yI;


# direct methods
.method public constructor <init>(LX/9yI;Lcom/facebook/rapidreporting/ui/DialogStateData;LX/0gc;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1596004
    iput-object p1, p0, LX/9yG;->g:LX/9yI;

    iput-object p2, p0, LX/9yG;->a:Lcom/facebook/rapidreporting/ui/DialogStateData;

    iput-object p3, p0, LX/9yG;->b:LX/0gc;

    iput-object p4, p0, LX/9yG;->c:Ljava/lang/String;

    iput-object p5, p0, LX/9yG;->d:Ljava/lang/String;

    iput-object p6, p0, LX/9yG;->e:Ljava/lang/String;

    iput-object p7, p0, LX/9yG;->f:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1595981
    iget-object v0, p0, LX/9yG;->a:Lcom/facebook/rapidreporting/ui/DialogStateData;

    iget-object v1, p0, LX/9yG;->f:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0824b5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1595982
    iput-object v1, v0, Lcom/facebook/rapidreporting/ui/DialogStateData;->e:Ljava/lang/String;

    .line 1595983
    iget-object v0, p0, LX/9yG;->g:LX/9yI;

    iget-object v0, v0, LX/9yI;->b:LX/9lb;

    iget-object v1, p0, LX/9yG;->b:LX/0gc;

    iget-object v2, p0, LX/9yG;->a:Lcom/facebook/rapidreporting/ui/DialogStateData;

    invoke-virtual {v0, v1, v2}, LX/9lb;->a(LX/0gc;Lcom/facebook/rapidreporting/ui/DialogStateData;)V

    .line 1595984
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1595985
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1595986
    iget-object v0, p0, LX/9yG;->g:LX/9yI;

    iget-object v0, v0, LX/9yI;->g:Lcom/facebook/reportingcoordinator/ReportingCoordinatorDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1595987
    if-eqz p1, :cond_1

    .line 1595988
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1595989
    if-eqz v0, :cond_1

    .line 1595990
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1595991
    check-cast v0, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingTagsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingTagsQueryModel;->a()Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1595992
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1595993
    check-cast v0, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingTagsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingTagsQueryModel;->a()Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel;

    move-result-object v0

    .line 1595994
    invoke-virtual {v0}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1595995
    iget-object v0, p0, LX/9yG;->a:Lcom/facebook/rapidreporting/ui/DialogStateData;

    .line 1595996
    iput-object p1, v0, Lcom/facebook/rapidreporting/ui/DialogStateData;->c:Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1595997
    iget-object v0, p0, LX/9yG;->g:LX/9yI;

    iget-object v0, v0, LX/9yI;->b:LX/9lb;

    iget-object v1, p0, LX/9yG;->b:LX/0gc;

    iget-object v2, p0, LX/9yG;->a:Lcom/facebook/rapidreporting/ui/DialogStateData;

    invoke-virtual {v0, v1, v2}, LX/9lb;->a(LX/0gc;Lcom/facebook/rapidreporting/ui/DialogStateData;)V

    .line 1595998
    :goto_0
    return-void

    .line 1595999
    :cond_0
    iget-object v0, p0, LX/9yG;->b:LX/0gc;

    iget-object v1, p0, LX/9yG;->c:Ljava/lang/String;

    iget-object v2, p0, LX/9yG;->d:Ljava/lang/String;

    iget-object v3, p0, LX/9yG;->e:Ljava/lang/String;

    sget-object v4, LX/9yI;->a:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, LX/9yI;->a(LX/0gc;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1596000
    :cond_1
    iget-object v0, p0, LX/9yG;->a:Lcom/facebook/rapidreporting/ui/DialogStateData;

    iget-object v1, p0, LX/9yG;->f:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0824b4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1596001
    iput-object v1, v0, Lcom/facebook/rapidreporting/ui/DialogStateData;->e:Ljava/lang/String;

    .line 1596002
    iget-object v0, p0, LX/9yG;->g:LX/9yI;

    iget-object v0, v0, LX/9yI;->b:LX/9lb;

    iget-object v1, p0, LX/9yG;->b:LX/0gc;

    iget-object v2, p0, LX/9yG;->a:Lcom/facebook/rapidreporting/ui/DialogStateData;

    invoke-virtual {v0, v1, v2}, LX/9lb;->a(LX/0gc;Lcom/facebook/rapidreporting/ui/DialogStateData;)V

    .line 1596003
    iget-object v0, p0, LX/9yG;->g:LX/9yI;

    iget-object v0, v0, LX/9yI;->f:LX/03V;

    sget-object v1, LX/9yI;->a:Ljava/lang/String;

    const-string v2, "RapidReporting GraphQL call to fetch RapidReportingPrompt returned successfully but returned no RapidReportingPrompt"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
