.class public LX/8yb;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/components/fb/widget/FbImageBlockComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1426161
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/8yb;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/components/fb/widget/FbImageBlockComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1426162
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1426163
    iput-object p1, p0, LX/8yb;->b:LX/0Ot;

    .line 1426164
    return-void
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 30

    .prologue
    .line 1426165
    check-cast p2, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;

    .line 1426166
    move-object/from16 v0, p0

    iget-object v1, v0, LX/8yb;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/components/fb/widget/FbImageBlockComponentSpec;

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->a:Ljava/lang/CharSequence;

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->b:LX/1dQ;

    move-object/from16 v0, p2

    iget v5, v0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->c:I

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->d:Ljava/lang/CharSequence;

    move-object/from16 v0, p2

    iget v7, v0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->e:I

    move-object/from16 v0, p2

    iget v8, v0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->f:I

    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->g:LX/1dQ;

    move-object/from16 v0, p2

    iget-object v10, v0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->h:Ljava/lang/CharSequence;

    move-object/from16 v0, p2

    iget v11, v0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->i:I

    move-object/from16 v0, p2

    iget v12, v0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->j:I

    move-object/from16 v0, p2

    iget-object v13, v0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->k:LX/1dQ;

    move-object/from16 v0, p2

    iget v14, v0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->l:I

    move-object/from16 v0, p2

    iget-boolean v15, v0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->m:Z

    move-object/from16 v0, p2

    iget v0, v0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->n:I

    move/from16 v16, v0

    move-object/from16 v0, p2

    iget v0, v0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->o:I

    move/from16 v17, v0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->p:Landroid/net/Uri;

    move-object/from16 v18, v0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->q:LX/1dc;

    move-object/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->r:I

    move/from16 v20, v0

    move-object/from16 v0, p2

    iget v0, v0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->s:I

    move/from16 v21, v0

    move-object/from16 v0, p2

    iget v0, v0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->t:I

    move/from16 v22, v0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->u:LX/1dQ;

    move-object/from16 v23, v0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->x:Landroid/net/Uri;

    move-object/from16 v24, v0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->y:LX/1dc;

    move-object/from16 v25, v0

    move-object/from16 v0, p2

    iget v0, v0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->z:I

    move/from16 v26, v0

    move-object/from16 v0, p2

    iget v0, v0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->A:I

    move/from16 v27, v0

    move-object/from16 v0, p2

    iget v0, v0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->B:I

    move/from16 v28, v0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->C:LX/1dQ;

    move-object/from16 v29, v0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v29}, Lcom/facebook/components/fb/widget/FbImageBlockComponentSpec;->a(LX/1De;Ljava/lang/CharSequence;LX/1dQ;ILjava/lang/CharSequence;IILX/1dQ;Ljava/lang/CharSequence;IILX/1dQ;IZIILandroid/net/Uri;LX/1dc;IIILX/1dQ;Landroid/net/Uri;LX/1dc;IIILX/1dQ;)LX/1Dg;

    move-result-object v1

    .line 1426167
    return-object v1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1426168
    invoke-static {}, LX/1dS;->b()V

    .line 1426169
    const/4 v0, 0x0

    return-object v0
.end method
