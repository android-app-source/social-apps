.class public LX/91b;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/MountSpec;
.end annotation


# static fields
.field public static final a:I

.field public static final b:Landroid/graphics/Typeface;

.field public static final c:Landroid/text/Layout$Alignment;

.field private static final d:[Landroid/text/Layout$Alignment;

.field private static final e:[Landroid/text/TextUtils$TruncateAt;

.field private static final f:Landroid/graphics/Typeface;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1431157
    invoke-static {}, Landroid/text/Layout$Alignment;->values()[Landroid/text/Layout$Alignment;

    move-result-object v0

    sput-object v0, LX/91b;->d:[Landroid/text/Layout$Alignment;

    .line 1431158
    invoke-static {}, Landroid/text/TextUtils$TruncateAt;->values()[Landroid/text/TextUtils$TruncateAt;

    move-result-object v0

    sput-object v0, LX/91b;->e:[Landroid/text/TextUtils$TruncateAt;

    .line 1431159
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    .line 1431160
    sput-object v0, LX/91b;->f:Landroid/graphics/Typeface;

    invoke-virtual {v0}, Landroid/graphics/Typeface;->getStyle()I

    move-result v0

    sput v0, LX/91b;->a:I

    .line 1431161
    sget-object v0, LX/91b;->f:Landroid/graphics/Typeface;

    sput-object v0, LX/91b;->b:Landroid/graphics/Typeface;

    .line 1431162
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sput-object v0, LX/91b;->c:Landroid/text/Layout$Alignment;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1431155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1431156
    return-void
.end method

.method public static a(LX/1De;IILX/1no;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextUtils$TruncateAt;IIFFFIZILandroid/content/res/ColorStateList;ILandroid/content/res/ColorStateList;IIIFFILandroid/graphics/Typeface;Landroid/text/Layout$Alignment;I)V
    .locals 26
    .param p4    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p5    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p6    # Landroid/text/TextUtils$TruncateAt;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p7    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->INT:LX/32B;
        .end annotation
    .end param
    .param p8    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->INT:LX/32B;
        .end annotation
    .end param
    .param p9    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_OFFSET:LX/32B;
        .end annotation
    .end param
    .param p10    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_OFFSET:LX/32B;
        .end annotation
    .end param
    .param p11    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_OFFSET:LX/32B;
        .end annotation
    .end param
    .param p12    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p13    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->BOOL:LX/32B;
        .end annotation
    .end param
    .param p14    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p15    # Landroid/content/res/ColorStateList;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p16    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p17    # Landroid/content/res/ColorStateList;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p18    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p19    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p20    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_TEXT:LX/32B;
        .end annotation
    .end param
    .param p21    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_OFFSET:LX/32B;
        .end annotation
    .end param
    .param p22    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->FLOAT:LX/32B;
        .end annotation
    .end param
    .param p23    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p24    # Landroid/graphics/Typeface;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p25    # Landroid/text/Layout$Alignment;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param

    .prologue
    .line 1431149
    new-instance v2, Lcom/facebook/ui/search/SearchEditText;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/facebook/ui/search/SearchEditText;-><init>(Landroid/content/Context;)V

    move-object/from16 v1, p0

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move/from16 v6, p7

    move/from16 v7, p8

    move/from16 v8, p9

    move/from16 v9, p10

    move/from16 v10, p11

    move/from16 v11, p12

    move/from16 v12, p13

    move/from16 v13, p14

    move-object/from16 v14, p15

    move/from16 v15, p16

    move-object/from16 v16, p17

    move/from16 v17, p18

    move/from16 v18, p19

    move/from16 v19, p20

    move/from16 v20, p21

    move/from16 v21, p22

    move/from16 v22, p23

    move-object/from16 v23, p24

    move-object/from16 v24, p25

    move/from16 v25, p26

    .line 1431150
    invoke-static/range {v1 .. v25}, LX/91b;->a(LX/1De;Lcom/facebook/ui/search/SearchEditText;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextUtils$TruncateAt;IIFFFIZILandroid/content/res/ColorStateList;ILandroid/content/res/ColorStateList;IIIFFILandroid/graphics/Typeface;Landroid/text/Layout$Alignment;I)V

    .line 1431151
    invoke-static/range {p1 .. p1}, LX/1oC;->a(I)I

    move-result v1

    invoke-static/range {p2 .. p2}, LX/1oC;->a(I)I

    move-result v3

    invoke-virtual {v2, v1, v3}, Lcom/facebook/ui/search/SearchEditText;->measure(II)V

    .line 1431152
    invoke-virtual {v2}, Lcom/facebook/ui/search/SearchEditText;->getMeasuredWidth()I

    move-result v1

    move-object/from16 v0, p3

    iput v1, v0, LX/1no;->a:I

    .line 1431153
    invoke-virtual {v2}, Lcom/facebook/ui/search/SearchEditText;->getMeasuredHeight()I

    move-result v1

    move-object/from16 v0, p3

    iput v1, v0, LX/1no;->b:I

    .line 1431154
    return-void
.end method

.method public static a(LX/1De;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1np",
            "<",
            "Landroid/text/TextUtils$TruncateAt;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Float;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/CharSequence;",
            ">;",
            "LX/1np",
            "<",
            "Landroid/content/res/ColorStateList;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/1np",
            "<",
            "Landroid/text/Layout$Alignment;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Float;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Float;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Float;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1431107
    sget-object v1, LX/03r;->Text:[I

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, LX/1De;->a([II)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 1431108
    const/4 v1, 0x0

    invoke-virtual {v2}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v3

    :goto_0
    if-ge v1, v3, :cond_11

    .line 1431109
    invoke-virtual {v2, v1}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v4

    .line 1431110
    const/16 v5, 0x7

    if-ne v4, v5, :cond_1

    .line 1431111
    invoke-virtual {v2, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p6, v4}, LX/1np;->a(Ljava/lang/Object;)V

    .line 1431112
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1431113
    :cond_1
    const/16 v5, 0x2

    if-ne v4, v5, :cond_2

    .line 1431114
    invoke-virtual {v2, v4}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v4

    invoke-virtual {p7, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 1431115
    :cond_2
    const/16 v5, 0x0

    if-ne v4, v5, :cond_3

    .line 1431116
    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p10

    invoke-virtual {v0, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 1431117
    :cond_3
    const/16 v5, 0x5

    if-ne v4, v5, :cond_4

    .line 1431118
    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    .line 1431119
    if-lez v4, :cond_0

    .line 1431120
    sget-object v5, LX/91b;->e:[Landroid/text/TextUtils$TruncateAt;

    add-int/lit8 v4, v4, -0x1

    aget-object v4, v5, v4

    invoke-virtual {p1, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 1431121
    :cond_4
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x11

    if-lt v5, v6, :cond_5

    const/16 v5, 0x11

    if-ne v4, v5, :cond_5

    .line 1431122
    sget-object v5, LX/91b;->d:[Landroid/text/Layout$Alignment;

    const/4 v6, 0x0

    invoke-virtual {v2, v4, v6}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    aget-object v4, v5, v4

    move-object/from16 v0, p11

    invoke-virtual {v0, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 1431123
    :cond_5
    const/16 v5, 0x9

    if-ne v4, v5, :cond_6

    .line 1431124
    const/4 v5, -0x1

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p3, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 1431125
    :cond_6
    const/16 v5, 0x8

    if-ne v4, v5, :cond_7

    .line 1431126
    const/4 v5, -0x1

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p4, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 1431127
    :cond_7
    const/16 v5, 0xa

    if-ne v4, v5, :cond_8

    .line 1431128
    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {p5, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 1431129
    :cond_8
    const/16 v5, 0x4

    if-ne v4, v5, :cond_9

    .line 1431130
    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p8, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 1431131
    :cond_9
    const/16 v5, 0x3

    if-ne v4, v5, :cond_a

    .line 1431132
    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p9

    invoke-virtual {v0, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 1431133
    :cond_a
    const/16 v5, 0x1

    if-ne v4, v5, :cond_b

    .line 1431134
    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p12

    invoke-virtual {v0, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 1431135
    :cond_b
    const/16 v5, 0x10

    if-ne v4, v5, :cond_c

    .line 1431136
    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {p2, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 1431137
    :cond_c
    const/16 v5, 0xd

    if-ne v4, v5, :cond_d

    .line 1431138
    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    move-object/from16 v0, p14

    invoke-virtual {v0, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 1431139
    :cond_d
    const/16 v5, 0xe

    if-ne v4, v5, :cond_e

    .line 1431140
    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    move-object/from16 v0, p15

    invoke-virtual {v0, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 1431141
    :cond_e
    const/16 v5, 0xf

    if-ne v4, v5, :cond_f

    .line 1431142
    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    move-object/from16 v0, p13

    invoke-virtual {v0, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 1431143
    :cond_f
    const/16 v5, 0xc

    if-ne v4, v5, :cond_10

    .line 1431144
    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p16

    invoke-virtual {v0, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 1431145
    :cond_10
    const/16 v5, 0x6

    if-ne v4, v5, :cond_0

    .line 1431146
    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p17

    invoke-virtual {v0, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 1431147
    :cond_11
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 1431148
    return-void
.end method

.method public static a(LX/1De;LX/91a;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextUtils$TruncateAt;IIFFFIZILandroid/content/res/ColorStateList;ILandroid/content/res/ColorStateList;IIIFFILandroid/graphics/Typeface;Landroid/text/Layout$Alignment;I)V
    .locals 1
    .param p2    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p3    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p4    # Landroid/text/TextUtils$TruncateAt;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->INT:LX/32B;
        .end annotation
    .end param
    .param p6    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->INT:LX/32B;
        .end annotation
    .end param
    .param p7    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_OFFSET:LX/32B;
        .end annotation
    .end param
    .param p8    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_OFFSET:LX/32B;
        .end annotation
    .end param
    .param p9    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_OFFSET:LX/32B;
        .end annotation
    .end param
    .param p10    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p11    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->BOOL:LX/32B;
        .end annotation
    .end param
    .param p12    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p13    # Landroid/content/res/ColorStateList;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p14    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p15    # Landroid/content/res/ColorStateList;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p16    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p17    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p18    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_TEXT:LX/32B;
        .end annotation
    .end param
    .param p19    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_OFFSET:LX/32B;
        .end annotation
    .end param
    .param p20    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->FLOAT:LX/32B;
        .end annotation
    .end param
    .param p21    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p22    # Landroid/graphics/Typeface;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p23    # Landroid/text/Layout$Alignment;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p24    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param

    .prologue
    .line 1431069
    invoke-static {p0}, LX/91W;->d(LX/1De;)LX/1dQ;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/91a;->a(LX/1dQ;)V

    .line 1431070
    invoke-static/range {p0 .. p24}, LX/91b;->a(LX/1De;Lcom/facebook/ui/search/SearchEditText;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextUtils$TruncateAt;IIFFFIZILandroid/content/res/ColorStateList;ILandroid/content/res/ColorStateList;IIIFFILandroid/graphics/Typeface;Landroid/text/Layout$Alignment;I)V

    .line 1431071
    return-void
.end method

.method private static a(LX/1De;Lcom/facebook/ui/search/SearchEditText;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextUtils$TruncateAt;IIFFFIZILandroid/content/res/ColorStateList;ILandroid/content/res/ColorStateList;IIIFFILandroid/graphics/Typeface;Landroid/text/Layout$Alignment;I)V
    .locals 6

    .prologue
    .line 1431072
    invoke-virtual {p1, p2}, Lcom/facebook/ui/search/SearchEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1431073
    invoke-virtual {p1, p3}, Lcom/facebook/ui/search/SearchEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 1431074
    invoke-virtual {p1, p4}, Lcom/facebook/ui/search/SearchEditText;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1431075
    invoke-virtual {p1, p5}, Lcom/facebook/ui/search/SearchEditText;->setMinLines(I)V

    .line 1431076
    invoke-virtual {p1, p6}, Lcom/facebook/ui/search/SearchEditText;->setMaxLines(I)V

    .line 1431077
    move/from16 v0, p10

    invoke-virtual {p1, p7, p8, p9, v0}, Lcom/facebook/ui/search/SearchEditText;->setShadowLayer(FFFI)V

    .line 1431078
    move/from16 v0, p11

    invoke-virtual {p1, v0}, Lcom/facebook/ui/search/SearchEditText;->setSingleLine(Z)V

    .line 1431079
    move/from16 v0, p16

    invoke-virtual {p1, v0}, Lcom/facebook/ui/search/SearchEditText;->setLinkTextColor(I)V

    .line 1431080
    move/from16 v0, p17

    invoke-virtual {p1, v0}, Lcom/facebook/ui/search/SearchEditText;->setHighlightColor(I)V

    .line 1431081
    move/from16 v0, p18

    int-to-float v2, v0

    invoke-virtual {p1, v2}, Lcom/facebook/ui/search/SearchEditText;->setTextSize(F)V

    .line 1431082
    move/from16 v0, p19

    move/from16 v1, p20

    invoke-virtual {p1, v0, v1}, Lcom/facebook/ui/search/SearchEditText;->setLineSpacing(FF)V

    .line 1431083
    move-object/from16 v0, p22

    move/from16 v1, p21

    invoke-virtual {p1, v0, v1}, Lcom/facebook/ui/search/SearchEditText;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 1431084
    move/from16 v0, p24

    invoke-virtual {p1, v0}, Lcom/facebook/ui/search/SearchEditText;->setGravity(I)V

    .line 1431085
    const/4 v2, 0x6

    invoke-virtual {p1, v2}, Lcom/facebook/ui/search/SearchEditText;->setImeOptions(I)V

    .line 1431086
    invoke-virtual {p1}, Lcom/facebook/ui/search/SearchEditText;->setSingleLine()V

    .line 1431087
    new-instance v2, LX/91X;

    invoke-direct {v2, p1}, LX/91X;-><init>(Lcom/facebook/ui/search/SearchEditText;)V

    invoke-virtual {p1, v2}, Lcom/facebook/ui/search/SearchEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 1431088
    invoke-virtual {p0}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0217bc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {p1, v2, v3, v4, v5}, Lcom/facebook/ui/search/SearchEditText;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1431089
    const/16 v2, 0xf

    invoke-virtual {p1, v2}, Lcom/facebook/ui/search/SearchEditText;->setCompoundDrawablePadding(I)V

    .line 1431090
    if-eqz p13, :cond_0

    .line 1431091
    move-object/from16 v0, p13

    invoke-virtual {p1, v0}, Lcom/facebook/ui/search/SearchEditText;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 1431092
    :goto_0
    if-eqz p15, :cond_1

    .line 1431093
    move-object/from16 v0, p15

    invoke-virtual {p1, v0}, Lcom/facebook/ui/search/SearchEditText;->setHintTextColor(Landroid/content/res/ColorStateList;)V

    .line 1431094
    :goto_1
    sget-object v2, LX/91Y;->a:[I

    invoke-virtual/range {p23 .. p23}, Landroid/text/Layout$Alignment;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1431095
    :goto_2
    return-void

    .line 1431096
    :cond_0
    move/from16 v0, p12

    invoke-virtual {p1, v0}, Lcom/facebook/ui/search/SearchEditText;->setTextColor(I)V

    goto :goto_0

    .line 1431097
    :cond_1
    move/from16 v0, p14

    invoke-virtual {p1, v0}, Lcom/facebook/ui/search/SearchEditText;->setHintTextColor(I)V

    goto :goto_1

    .line 1431098
    :pswitch_0
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v2, v3, :cond_2

    .line 1431099
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, Lcom/facebook/ui/search/SearchEditText;->setTextAlignment(I)V

    goto :goto_2

    .line 1431100
    :cond_2
    or-int/lit8 v2, p24, 0x3

    invoke-virtual {p1, v2}, Lcom/facebook/ui/search/SearchEditText;->setGravity(I)V

    goto :goto_2

    .line 1431101
    :pswitch_1
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v2, v3, :cond_3

    .line 1431102
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, Lcom/facebook/ui/search/SearchEditText;->setTextAlignment(I)V

    goto :goto_2

    .line 1431103
    :cond_3
    or-int/lit8 v2, p24, 0x5

    invoke-virtual {p1, v2}, Lcom/facebook/ui/search/SearchEditText;->setGravity(I)V

    goto :goto_2

    .line 1431104
    :pswitch_2
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v2, v3, :cond_4

    .line 1431105
    const/4 v2, 0x4

    invoke-virtual {p1, v2}, Lcom/facebook/ui/search/SearchEditText;->setTextAlignment(I)V

    goto :goto_2

    .line 1431106
    :cond_4
    or-int/lit8 v2, p24, 0x1

    invoke-virtual {p1, v2}, Lcom/facebook/ui/search/SearchEditText;->setGravity(I)V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
