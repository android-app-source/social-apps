.class public final LX/8xH;
.super LX/1cC;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/attachments/ui/AttachmentViewSticker;


# direct methods
.method public constructor <init>(Lcom/facebook/attachments/ui/AttachmentViewSticker;)V
    .locals 0

    .prologue
    .line 1423839
    iput-object p1, p0, LX/8xH;->a:Lcom/facebook/attachments/ui/AttachmentViewSticker;

    invoke-direct {p0}, LX/1cC;-><init>()V

    return-void
.end method

.method private a(Landroid/graphics/drawable/Animatable;)V
    .locals 3

    .prologue
    .line 1423833
    instance-of v0, p1, Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 1423834
    check-cast p1, Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, p1}, LX/8xH;->a(Landroid/graphics/drawable/Drawable;)V

    .line 1423835
    :cond_0
    iget-object v0, p0, LX/8xH;->a:Lcom/facebook/attachments/ui/AttachmentViewSticker;

    iget-object v0, v0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->g:LX/11i;

    sget-object v1, LX/8lJ;->b:LX/8lG;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 1423836
    if-eqz v0, :cond_1

    .line 1423837
    const-string v1, "StickerToPostOptimisticComment"

    const v2, 0x523b5a8c

    invoke-static {v0, v1, v2}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1423838
    :cond_1
    return-void
.end method

.method private a(Landroid/graphics/drawable/Drawable;)V
    .locals 3

    .prologue
    .line 1423828
    iget-object v0, p0, LX/8xH;->a:Lcom/facebook/attachments/ui/AttachmentViewSticker;

    new-instance v1, LX/8mh;

    invoke-direct {v1, p1}, LX/8mh;-><init>(Landroid/graphics/drawable/Drawable;)V

    .line 1423829
    iput-object v1, v0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->p:LX/8mh;

    .line 1423830
    iget-object v0, p0, LX/8xH;->a:Lcom/facebook/attachments/ui/AttachmentViewSticker;

    invoke-static {v0}, Lcom/facebook/attachments/ui/AttachmentViewSticker;->g(Lcom/facebook/attachments/ui/AttachmentViewSticker;)V

    .line 1423831
    iget-object v0, p0, LX/8xH;->a:Lcom/facebook/attachments/ui/AttachmentViewSticker;

    iget-object v0, v0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->f:LX/0Zb;

    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "comment_sticker_viewed"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1423832
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V
    .locals 0
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/drawable/Animatable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1423827
    invoke-direct {p0, p3}, LX/8xH;->a(Landroid/graphics/drawable/Animatable;)V

    return-void
.end method
