.class public LX/8jc;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1392629
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1392630
    return-void
.end method

.method public static a(LX/4m4;)Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/graphql/calls/StickerInterfaces;
    .end annotation

    .prologue
    .line 1392631
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1392632
    sget-object v0, LX/8jb;->a:[I

    invoke-virtual {p0}, LX/4m4;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1392633
    const-string v0, "MESSAGES"

    :goto_0
    return-object v0

    .line 1392634
    :pswitch_0
    const-string v0, "MESSAGES"

    goto :goto_0

    .line 1392635
    :pswitch_1
    const-string v0, "COMMENTS"

    goto :goto_0

    .line 1392636
    :pswitch_2
    const-string v0, "COMPOSER"

    goto :goto_0

    .line 1392637
    :pswitch_3
    const-string v0, "POSTS"

    goto :goto_0

    .line 1392638
    :pswitch_4
    const-string v0, "SMS"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
