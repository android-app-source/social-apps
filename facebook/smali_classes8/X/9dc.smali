.class public final LX/9dc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;

.field private b:I


# direct methods
.method public constructor <init>(Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;)V
    .locals 1

    .prologue
    .line 1518281
    iput-object p1, p0, LX/9dc;->a:Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1518282
    iget-object v0, p0, LX/9dc;->a:Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->getLineCount()I

    move-result v0

    iput v0, p0, LX/9dc;->b:I

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 1518283
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 1518284
    iget-object v0, p0, LX/9dc;->a:Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->getLineCount()I

    move-result v0

    iput v0, p0, LX/9dc;->b:I

    .line 1518285
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 4

    .prologue
    .line 1518286
    iget-object v0, p0, LX/9dc;->a:Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    .line 1518287
    iget-object v0, p0, LX/9dc;->a:Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->getTextSize()F

    move-result v0

    .line 1518288
    iget-object v1, p0, LX/9dc;->a:Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->getLineCount()I

    move-result v1

    iget v2, p0, LX/9dc;->b:I

    if-ge v1, v2, :cond_0

    iget-object v1, p0, LX/9dc;->a:Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0c03

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    cmpg-float v1, v0, v1

    if-gez v1, :cond_0

    iget-object v1, p0, LX/9dc;->a:Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->getTop()I

    move-result v1

    if-lez v1, :cond_0

    .line 1518289
    iget-object v1, p0, LX/9dc;->a:Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;

    const/4 v2, 0x0

    const v3, 0x3f666666    # 0.9f

    div-float/2addr v0, v3

    invoke-virtual {v1, v2, v0}, Lcom/facebook/photos/creativeediting/ui/KeyboardAwareEditText;->setTextSize(IF)V

    .line 1518290
    :cond_0
    return-void
.end method
