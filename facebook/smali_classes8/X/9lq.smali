.class public final LX/9lq;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 1533921
    const/4 v10, 0x0

    .line 1533922
    const/4 v9, 0x0

    .line 1533923
    const/4 v8, 0x0

    .line 1533924
    const/4 v7, 0x0

    .line 1533925
    const/4 v6, 0x0

    .line 1533926
    const/4 v5, 0x0

    .line 1533927
    const/4 v4, 0x0

    .line 1533928
    const/4 v3, 0x0

    .line 1533929
    const/4 v2, 0x0

    .line 1533930
    const/4 v1, 0x0

    .line 1533931
    const/4 v0, 0x0

    .line 1533932
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_OBJECT:LX/15z;

    if-eq v11, v12, :cond_1

    .line 1533933
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1533934
    const/4 v0, 0x0

    .line 1533935
    :goto_0
    return v0

    .line 1533936
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1533937
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_c

    .line 1533938
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1533939
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1533940
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 1533941
    const-string v12, "__type__"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_2

    const-string v12, "__typename"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 1533942
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v10

    goto :goto_1

    .line 1533943
    :cond_3
    const-string v12, "already_completed"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 1533944
    const/4 v0, 0x1

    .line 1533945
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v9

    goto :goto_1

    .line 1533946
    :cond_4
    const-string v12, "completed_subtitle"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 1533947
    invoke-static {p0, p1}, LX/9ll;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1533948
    :cond_5
    const-string v12, "completed_title"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 1533949
    invoke-static {p0, p1}, LX/9lm;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1533950
    :cond_6
    const-string v12, "confirmation_button_label"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 1533951
    invoke-static {p0, p1}, LX/9ln;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1533952
    :cond_7
    const-string v12, "id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 1533953
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 1533954
    :cond_8
    const-string v12, "negative_feedback_action_type"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 1533955
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    goto/16 :goto_1

    .line 1533956
    :cond_9
    const-string v12, "subtitle"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 1533957
    invoke-static {p0, p1}, LX/9lo;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 1533958
    :cond_a
    const-string v12, "title"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_b

    .line 1533959
    invoke-static {p0, p1}, LX/9lp;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 1533960
    :cond_b
    const-string v12, "url"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1533961
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto/16 :goto_1

    .line 1533962
    :cond_c
    const/16 v11, 0xa

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1533963
    const/4 v11, 0x0

    invoke-virtual {p1, v11, v10}, LX/186;->b(II)V

    .line 1533964
    if-eqz v0, :cond_d

    .line 1533965
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v9}, LX/186;->a(IZ)V

    .line 1533966
    :cond_d
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1533967
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1533968
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1533969
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1533970
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1533971
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1533972
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1533973
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1533974
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x6

    const/4 v1, 0x0

    .line 1533853
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1533854
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1533855
    if-eqz v0, :cond_0

    .line 1533856
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1533857
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1533858
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1533859
    if-eqz v0, :cond_1

    .line 1533860
    const-string v1, "already_completed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1533861
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1533862
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1533863
    if-eqz v0, :cond_3

    .line 1533864
    const-string v1, "completed_subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1533865
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1533866
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1533867
    if-eqz v1, :cond_2

    .line 1533868
    const-string p3, "text"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1533869
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1533870
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1533871
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1533872
    if-eqz v0, :cond_5

    .line 1533873
    const-string v1, "completed_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1533874
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1533875
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1533876
    if-eqz v1, :cond_4

    .line 1533877
    const-string p3, "text"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1533878
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1533879
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1533880
    :cond_5
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1533881
    if-eqz v0, :cond_7

    .line 1533882
    const-string v1, "confirmation_button_label"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1533883
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1533884
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1533885
    if-eqz v1, :cond_6

    .line 1533886
    const-string p3, "text"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1533887
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1533888
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1533889
    :cond_7
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1533890
    if-eqz v0, :cond_8

    .line 1533891
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1533892
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1533893
    :cond_8
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1533894
    if-eqz v0, :cond_9

    .line 1533895
    const-string v0, "negative_feedback_action_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1533896
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1533897
    :cond_9
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1533898
    if-eqz v0, :cond_b

    .line 1533899
    const-string v1, "subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1533900
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1533901
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1533902
    if-eqz v1, :cond_a

    .line 1533903
    const-string v2, "text"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1533904
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1533905
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1533906
    :cond_b
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1533907
    if-eqz v0, :cond_d

    .line 1533908
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1533909
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1533910
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1533911
    if-eqz v1, :cond_c

    .line 1533912
    const-string v2, "text"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1533913
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1533914
    :cond_c
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1533915
    :cond_d
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1533916
    if-eqz v0, :cond_e

    .line 1533917
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1533918
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1533919
    :cond_e
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1533920
    return-void
.end method
