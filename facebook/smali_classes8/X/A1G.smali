.class public final LX/A1G;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$FetchLiveFeedStoriesGraphQLModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1610066
    const-class v1, Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$FetchLiveFeedStoriesGraphQLModel;

    const v0, 0x27cdc134

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x3

    const-string v5, "FetchLiveFeedStoriesGraphQL"

    const-string v6, "c027f78231ff477fb8cef468ddec2802"

    const-string v7, "graph_search_query"

    const-string v8, "10155232448506729"

    const/4 v9, 0x0

    .line 1610067
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1610068
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1610069
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1610070
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1610071
    sparse-switch v0, :sswitch_data_0

    .line 1610072
    :goto_0
    return-object p1

    .line 1610073
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1610074
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1610075
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1610076
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1610077
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1610078
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 1610079
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 1610080
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 1610081
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 1610082
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 1610083
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 1610084
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 1610085
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 1610086
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 1610087
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 1610088
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    .line 1610089
    :sswitch_10
    const-string p1, "16"

    goto :goto_0

    .line 1610090
    :sswitch_11
    const-string p1, "17"

    goto :goto_0

    .line 1610091
    :sswitch_12
    const-string p1, "18"

    goto :goto_0

    .line 1610092
    :sswitch_13
    const-string p1, "19"

    goto :goto_0

    .line 1610093
    :sswitch_14
    const-string p1, "20"

    goto :goto_0

    .line 1610094
    :sswitch_15
    const-string p1, "21"

    goto :goto_0

    .line 1610095
    :sswitch_16
    const-string p1, "22"

    goto :goto_0

    .line 1610096
    :sswitch_17
    const-string p1, "23"

    goto :goto_0

    .line 1610097
    :sswitch_18
    const-string p1, "24"

    goto :goto_0

    .line 1610098
    :sswitch_19
    const-string p1, "25"

    goto :goto_0

    .line 1610099
    :sswitch_1a
    const-string p1, "26"

    goto :goto_0

    .line 1610100
    :sswitch_1b
    const-string p1, "27"

    goto :goto_0

    .line 1610101
    :sswitch_1c
    const-string p1, "28"

    goto :goto_0

    .line 1610102
    :sswitch_1d
    const-string p1, "29"

    goto :goto_0

    .line 1610103
    :sswitch_1e
    const-string p1, "30"

    goto :goto_0

    .line 1610104
    :sswitch_1f
    const-string p1, "31"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x6e3ba572 -> :sswitch_3
        -0x6a24640d -> :sswitch_1a
        -0x680de62a -> :sswitch_f
        -0x6326fdb3 -> :sswitch_c
        -0x5709d77d -> :sswitch_1e
        -0x4496acc9 -> :sswitch_10
        -0x41a91745 -> :sswitch_15
        -0x3e5a1778 -> :sswitch_5
        -0x3c54de38 -> :sswitch_13
        -0x3b85b241 -> :sswitch_1d
        -0x30b65c8f -> :sswitch_9
        -0x25df1291 -> :sswitch_17
        -0x25a646c8 -> :sswitch_8
        -0x2177e47b -> :sswitch_a
        -0x1b87b280 -> :sswitch_b
        -0x1673cbea -> :sswitch_4
        -0x12efdeb3 -> :sswitch_11
        -0x587d3fa -> :sswitch_d
        0x3677da -> :sswitch_1c
        0x101fb19 -> :sswitch_2
        0x5a7510f -> :sswitch_6
        0xa1fa812 -> :sswitch_1
        0x18ce3dbb -> :sswitch_0
        0x214100e0 -> :sswitch_12
        0x2292beef -> :sswitch_19
        0x244e76e6 -> :sswitch_18
        0x26d0c0ff -> :sswitch_16
        0x43ee5105 -> :sswitch_1f
        0x5e7957c4 -> :sswitch_7
        0x63c03b07 -> :sswitch_e
        0x73a026b5 -> :sswitch_14
        0x7506f93c -> :sswitch_1b
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1610105
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 1610106
    :goto_1
    return v0

    .line 1610107
    :sswitch_0
    const-string v2, "7"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :sswitch_1
    const-string v2, "9"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "0"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_3
    const-string v2, "29"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    .line 1610108
    :pswitch_0
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1610109
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1610110
    :pswitch_2
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1610111
    :pswitch_3
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x30 -> :sswitch_2
        0x37 -> :sswitch_0
        0x39 -> :sswitch_1
        0x647 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
