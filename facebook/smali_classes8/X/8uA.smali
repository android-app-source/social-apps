.class public LX/8uA;
.super LX/8u8;
.source ""


# instance fields
.field private d:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1414553
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-direct {p0, v0}, LX/8uA;-><init>(Landroid/util/DisplayMetrics;)V

    .line 1414554
    return-void
.end method

.method private constructor <init>(Landroid/util/DisplayMetrics;)V
    .locals 2

    .prologue
    .line 1414550
    invoke-direct {p0, p1}, LX/8u8;-><init>(Landroid/util/DisplayMetrics;)V

    .line 1414551
    const/4 v0, 0x1

    const/high16 v1, 0x40400000    # 3.0f

    invoke-static {v0, v1, p1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    iput v0, p0, LX/8uA;->d:F

    .line 1414552
    return-void
.end method

.method private a(IIILandroid/graphics/Canvas;Z)Landroid/graphics/RectF;
    .locals 6

    .prologue
    .line 1414538
    invoke-virtual {p4}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 1414539
    iget v1, v0, Landroid/graphics/Rect;->left:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v1, p1

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    add-int/2addr v1, v0

    .line 1414540
    if-eqz p5, :cond_0

    int-to-float v0, p2

    iget v2, p0, LX/8uA;->d:F

    sub-float/2addr v0, v2

    .line 1414541
    :goto_0
    new-instance v2, Landroid/graphics/RectF;

    int-to-float v3, p1

    int-to-float v1, v1

    int-to-float v4, p3

    iget v5, p0, LX/8uA;->d:F

    add-float/2addr v4, v5

    invoke-direct {v2, v3, v0, v1, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object v2

    .line 1414542
    :cond_0
    int-to-float v0, p2

    goto :goto_0
.end method


# virtual methods
.method public final drawLeadingMargin(Landroid/graphics/Canvas;Landroid/graphics/Paint;IIIIILjava/lang/CharSequence;IIZLandroid/text/Layout;)V
    .locals 6

    .prologue
    .line 1414543
    invoke-virtual {p0, p2}, LX/8u8;->a(Landroid/graphics/Paint;)V

    .line 1414544
    check-cast p8, Landroid/text/Spanned;

    invoke-interface {p8, p0}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v0

    if-ne v0, p9, :cond_0

    const/4 v5, 0x1

    :goto_0
    move-object v0, p0

    move v1, p3

    move v2, p5

    move v3, p7

    move-object v4, p1

    .line 1414545
    invoke-direct/range {v0 .. v5}, LX/8uA;->a(IIILandroid/graphics/Canvas;Z)Landroid/graphics/RectF;

    move-result-object v0

    .line 1414546
    iget v1, p0, LX/8uA;->d:F

    .line 1414547
    iget-object v2, p0, LX/8u8;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v1, v2}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 1414548
    return-void

    .line 1414549
    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method
