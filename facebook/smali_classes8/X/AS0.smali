.class public abstract LX/AS0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3l6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData$ProvidesAudienceEducatorData;",
        "DerivedData::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesIsPrivacyPillSupported;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;>",
        "Ljava/lang/Object;",
        "LX/3l6;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field public final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/ViewGroup;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/ViewGroup;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0hs;

.field public e:Z


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;Landroid/view/ViewGroup;LX/0il;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Landroid/view/ViewGroup;",
            "TServices;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1673584
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1673585
    iput-object v0, p0, LX/AS0;->d:LX/0hs;

    .line 1673586
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/AS0;->e:Z

    .line 1673587
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, LX/AS0;->b:Ljava/lang/ref/WeakReference;

    .line 1673588
    if-nez p2, :cond_0

    :goto_0
    iput-object v0, p0, LX/AS0;->c:Ljava/lang/ref/WeakReference;

    .line 1673589
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/AS0;->a:Ljava/lang/ref/WeakReference;

    .line 1673590
    return-void

    .line 1673591
    :cond_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 1673543
    const/4 v0, 0x1

    return v0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 1673544
    iget-object v0, p0, LX/AS0;->d:LX/0hs;

    if-nez v0, :cond_0

    .line 1673545
    :goto_0
    return-void

    .line 1673546
    :cond_0
    iget-object v0, p0, LX/AS0;->d:LX/0hs;

    invoke-virtual {v0}, LX/0ht;->l()V

    goto :goto_0
.end method

.method public b()Ljava/lang/CharSequence;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1673547
    iget-object v0, p0, LX/AS0;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1673548
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->u()Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    move-result-object v0

    .line 1673549
    iget-object v0, v0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1673550
    iget-object v0, p0, LX/AS0;->d:LX/0hs;

    if-eqz v0, :cond_0

    .line 1673551
    iget-object v0, p0, LX/AS0;->d:LX/0hs;

    invoke-virtual {v0}, LX/0ht;->l()V

    .line 1673552
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1673553
    iget-object v0, p0, LX/AS0;->c:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_8

    iget-object v0, p0, LX/AS0;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    :goto_0
    move-object v3, v0

    .line 1673554
    if-nez v3, :cond_1

    .line 1673555
    :cond_0
    :goto_1
    return-void

    .line 1673556
    :cond_1
    iget-object v0, p0, LX/AS0;->d:LX/0hs;

    if-nez v0, :cond_2

    .line 1673557
    new-instance v4, LX/0hs;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, LX/AS0;->a()I

    move-result v5

    invoke-direct {v4, v0, v5}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 1673558
    const/4 v0, -0x1

    .line 1673559
    iput v0, v4, LX/0hs;->t:I

    .line 1673560
    iget-object v0, p0, LX/AS0;->c:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_9

    .line 1673561
    const v0, 0x7f0d2667

    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0ht;->c(Landroid/view/View;)V

    .line 1673562
    :goto_2
    move-object v0, v4

    .line 1673563
    iput-object v0, p0, LX/AS0;->d:LX/0hs;

    .line 1673564
    :cond_2
    iget-object v0, p0, LX/AS0;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1673565
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->u()Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    move-result-object v0

    .line 1673566
    iget-object v0, v0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->a:Ljava/lang/String;

    move-object v4, v0

    .line 1673567
    invoke-virtual {p0}, LX/AS0;->b()Ljava/lang/CharSequence;

    move-result-object v5

    .line 1673568
    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    .line 1673569
    :goto_3
    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    move v2, v1

    .line 1673570
    :cond_3
    if-nez v0, :cond_4

    if-eqz v2, :cond_0

    .line 1673571
    :cond_4
    iput-boolean v1, p0, LX/AS0;->e:Z

    .line 1673572
    iget-object v1, p0, LX/AS0;->d:LX/0hs;

    new-instance v6, LX/AS3;

    invoke-direct {v6, p0}, LX/AS3;-><init>(LX/AS0;)V

    .line 1673573
    iput-object v6, v1, LX/0ht;->H:LX/2dD;

    .line 1673574
    if-eqz v0, :cond_5

    .line 1673575
    iget-object v0, p0, LX/AS0;->d:LX/0hs;

    invoke-virtual {v0, v4}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 1673576
    :cond_5
    if-eqz v2, :cond_6

    .line 1673577
    iget-object v0, p0, LX/AS0;->d:LX/0hs;

    invoke-virtual {v0, v5}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 1673578
    :cond_6
    new-instance v0, Lcom/facebook/composer/tip/BaseAudienceTooltipController$2;

    invoke-direct {v0, p0}, Lcom/facebook/composer/tip/BaseAudienceTooltipController$2;-><init>(LX/AS0;)V

    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    :cond_7
    move v0, v2

    .line 1673579
    goto :goto_3

    :cond_8
    iget-object v0, p0, LX/AS0;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    goto/16 :goto_0

    .line 1673580
    :cond_9
    iget-object v0, p0, LX/AS0;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    check-cast v0, LX/0ik;

    invoke-interface {v0}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2zG;

    invoke-virtual {v0}, LX/2zG;->v()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1673581
    const v0, 0x7f0d0a85

    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0ht;->c(Landroid/view/View;)V

    goto/16 :goto_2

    .line 1673582
    :cond_a
    const v0, 0x7f0d0ace

    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0ht;->c(Landroid/view/View;)V

    goto/16 :goto_2
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 1673583
    iget-boolean v0, p0, LX/AS0;->e:Z

    return v0
.end method
