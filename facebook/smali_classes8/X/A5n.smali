.class public LX/A5n;
.super LX/622;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/622",
        "<",
        "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1622346
    invoke-direct {p0, p1}, LX/622;-><init>(Ljava/lang/String;)V

    .line 1622347
    iput-object p2, p0, LX/A5n;->a:Ljava/util/List;

    .line 1622348
    iput-object p3, p0, LX/A5n;->b:Ljava/util/List;

    .line 1622349
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/622;->a(Z)V

    .line 1622350
    return-void
.end method


# virtual methods
.method public final d()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1622351
    iget-object v0, p0, LX/A5n;->b:Ljava/util/List;

    return-object v0
.end method

.method public final e()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1622352
    iget-object v0, p0, LX/A5n;->a:Ljava/util/List;

    return-object v0
.end method
