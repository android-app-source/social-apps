.class public LX/92P;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:I

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Landroid/location/Location;


# direct methods
.method public constructor <init>(LX/92O;)V
    .locals 1

    .prologue
    .line 1432260
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1432261
    iget v0, p1, LX/92O;->d:I

    iput v0, p0, LX/92P;->d:I

    .line 1432262
    iget-object v0, p1, LX/92O;->e:Ljava/lang/String;

    iput-object v0, p0, LX/92P;->e:Ljava/lang/String;

    .line 1432263
    iget-object v0, p1, LX/92O;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/92P;->a:Ljava/lang/String;

    .line 1432264
    iget-object v0, p1, LX/92O;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/92P;->b:Ljava/lang/String;

    .line 1432265
    iget-object v0, p1, LX/92O;->c:Ljava/lang/String;

    iput-object v0, p0, LX/92P;->c:Ljava/lang/String;

    .line 1432266
    iget-object v0, p1, LX/92O;->f:Ljava/lang/String;

    iput-object v0, p0, LX/92P;->f:Ljava/lang/String;

    .line 1432267
    iget-object v0, p1, LX/92O;->g:Landroid/location/Location;

    iput-object v0, p0, LX/92P;->g:Landroid/location/Location;

    .line 1432268
    return-void
.end method
