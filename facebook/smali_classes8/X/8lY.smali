.class public final LX/8lY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Mb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3Mb",
        "<",
        "LX/8ll;",
        "LX/8lm;",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/stickers/search/StickerSearchContainer;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/search/StickerSearchContainer;)V
    .locals 0

    .prologue
    .line 1397835
    iput-object p1, p0, LX/8lY;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 0

    .prologue
    .line 1397834
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1397836
    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1397828
    check-cast p2, LX/8lm;

    .line 1397829
    iget-object v0, p2, LX/8lm;->a:LX/0Px;

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1397830
    new-instance v1, LX/8lX;

    invoke-direct {v1, p0}, LX/8lX;-><init>(LX/8lY;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1397831
    iget-object v1, p0, LX/8lY;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-object v1, v1, Lcom/facebook/stickers/search/StickerSearchContainer;->G:LX/8mu;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/8mu;->a(LX/0Px;)V

    .line 1397832
    iget-object v0, p0, LX/8lY;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    sget-object v1, LX/8lg;->TAG_SELECTION:LX/8lg;

    invoke-static {v0, v1}, Lcom/facebook/stickers/search/StickerSearchContainer;->setCurrentState(Lcom/facebook/stickers/search/StickerSearchContainer;LX/8lg;)V

    .line 1397833
    return-void
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1397825
    check-cast p2, Ljava/lang/Throwable;

    .line 1397826
    iget-object v0, p0, LX/8lY;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-object v0, v0, Lcom/facebook/stickers/search/StickerSearchContainer;->s:LX/03V;

    sget-object v1, Lcom/facebook/stickers/search/StickerSearchContainer;->b:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Featured tag loading failed"

    invoke-virtual {v0, v1, v2, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1397827
    return-void
.end method
