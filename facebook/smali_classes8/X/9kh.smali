.class public abstract LX/9kh;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/places/pagetopics/stores/PlaceCategoriesListenableStore$Listener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1532080
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1532081
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/9kh;->a:Ljava/util/Set;

    .line 1532082
    return-void
.end method

.method public static a(Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryRowModel;)Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 1532074
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 1532075
    invoke-virtual {p0}, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryRowModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, -0x4eeac7fa

    invoke-static {v1, v0, v3, v2}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 1532076
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, LX/2sN;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, LX/2sN;->b()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 1532077
    invoke-virtual {v2, v1, v3}, LX/15i;->g(II)I

    move-result v1

    invoke-virtual {v2, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1532078
    :cond_0
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_0

    .line 1532079
    :cond_1
    new-instance v0, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;

    invoke-virtual {p0}, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryRowModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryRowModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {p0}, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryRowModel;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryRowModel;->m()Z

    move-result v5

    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;-><init>(Ljava/lang/String;JLjava/lang/String;ZLX/0Px;)V

    return-object v0
.end method


# virtual methods
.method public final a(LX/9kO;)V
    .locals 1

    .prologue
    .line 1532083
    invoke-virtual {p1}, LX/9kO;->a()V

    .line 1532084
    iget-object v0, p0, LX/9kh;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1532085
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1532071
    iget-object v0, p0, LX/9kh;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9kO;

    .line 1532072
    invoke-virtual {v0}, LX/9kO;->a()V

    goto :goto_0

    .line 1532073
    :cond_0
    return-void
.end method

.method public final b(LX/9kO;)V
    .locals 1

    .prologue
    .line 1532069
    iget-object v0, p0, LX/9kh;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1532070
    return-void
.end method
