.class public LX/9gw;
.super LX/9gr;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/9gr",
        "<",
        "Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchForPhotosTakenHereModel;",
        "Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;",
        "LX/5kD;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:LX/9h9;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;Lcom/facebook/common/callercontext/CallerContext;LX/9h9;)V
    .locals 1
    .param p1    # Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1525817
    const-class v0, LX/5kD;

    invoke-direct {p0, p1, v0, p2}, LX/9gr;-><init>(Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;Ljava/lang/Class;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1525818
    iput-object p3, p0, LX/9gw;->b:LX/9h9;

    .line 1525819
    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/String;)LX/0gW;
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "LX/0gW",
            "<",
            "Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchForPhotosTakenHereModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1525820
    new-instance v0, LX/9gA;

    invoke-direct {v0}, LX/9gA;-><init>()V

    move-object v1, v0

    .line 1525821
    const-string v0, "after_cursor"

    invoke-virtual {v1, v0, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "first_count"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "node_id"

    .line 1525822
    iget-object v0, p0, LX/9g8;->a:Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;

    move-object v0, v0

    .line 1525823
    check-cast v0, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;

    iget-object v0, v0, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1525824
    iget-object v0, p0, LX/9gw;->b:LX/9h9;

    invoke-virtual {v0, v1}, LX/9h9;->a(LX/0gW;)LX/0gW;

    .line 1525825
    return-object v1
.end method

.method public final b(Lcom/facebook/graphql/executor/GraphQLResult;)LX/9fz;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchForPhotosTakenHereModel;",
            ">;)",
            "LX/9fz",
            "<",
            "LX/5kD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1525826
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 1525827
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1525828
    check-cast v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchForPhotosTakenHereModel;

    invoke-virtual {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchForPhotosTakenHereModel;->a()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchForPhotosTakenHereModel$PhotosTakenHereModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchForPhotosTakenHereModel$PhotosTakenHereModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5kD;

    .line 1525829
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/5kD;->e()LX/1Fb;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 1525830
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1525831
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1525832
    :cond_1
    new-instance v1, LX/9fz;

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    .line 1525833
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1525834
    check-cast v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchForPhotosTakenHereModel;

    invoke-virtual {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchForPhotosTakenHereModel;->a()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchForPhotosTakenHereModel$PhotosTakenHereModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchForPhotosTakenHereModel$PhotosTakenHereModel;->j()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    move-result-object v0

    invoke-direct {v1, v2, v0}, LX/9fz;-><init>(LX/0Px;Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;)V

    return-object v1
.end method
