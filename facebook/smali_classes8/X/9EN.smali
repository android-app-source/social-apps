.class public LX/9EN;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1456985
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLComment;LX/9DG;)V
    .locals 5

    .prologue
    .line 1456986
    if-nez p1, :cond_0

    .line 1456987
    :goto_0
    return-void

    .line 1456988
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    .line 1456989
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v1

    .line 1456990
    new-instance v2, LX/7Gq;

    invoke-direct {v2}, LX/7Gq;-><init>()V

    new-instance v3, Lcom/facebook/user/model/Name;

    invoke-direct {v3, v1}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;)V

    .line 1456991
    iput-object v3, v2, LX/7Gq;->a:Lcom/facebook/user/model/Name;

    .line 1456992
    move-object v1, v2

    .line 1456993
    sget-object v2, LX/7Gr;->USER:LX/7Gr;

    .line 1456994
    iput-object v2, v1, LX/7Gq;->e:LX/7Gr;

    .line 1456995
    move-object v1, v1

    .line 1456996
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1456997
    iput-wide v2, v1, LX/7Gq;->b:J

    .line 1456998
    move-object v0, v1

    .line 1456999
    invoke-virtual {v0}, LX/7Gq;->l()Lcom/facebook/tagging/model/TaggingProfile;

    move-result-object v0

    .line 1457000
    invoke-virtual {p1, v0}, LX/9DG;->b(Lcom/facebook/tagging/model/TaggingProfile;)V

    goto :goto_0
.end method
