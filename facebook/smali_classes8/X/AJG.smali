.class public final LX/AJG;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/ipc/model/FacebookProfile;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AJH;


# direct methods
.method public constructor <init>(LX/AJH;)V
    .locals 0

    .prologue
    .line 1660952
    iput-object p1, p0, LX/AJG;->a:LX/AJH;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1660953
    sget-object v0, LX/AJH;->a:Ljava/lang/String;

    const-string v1, "Failure fetching contacts"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1660954
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 11

    .prologue
    .line 1660955
    check-cast p1, Ljava/util/List;

    .line 1660956
    iget-object v0, p0, LX/AJG;->a:LX/AJH;

    iget-object v0, v0, LX/AJH;->f:LX/AIu;

    .line 1660957
    if-nez p1, :cond_0

    .line 1660958
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 1660959
    :goto_0
    move-object v1, v2

    .line 1660960
    iget-object v2, v0, LX/AIu;->b:LX/AIm;

    .line 1660961
    iput-object v1, v2, LX/AIm;->d:LX/0Px;

    .line 1660962
    invoke-virtual {v2}, LX/1OM;->notifyDataSetChanged()V

    .line 1660963
    return-void

    .line 1660964
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 1660965
    const/4 v2, 0x1

    .line 1660966
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v3, v2

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/ipc/model/FacebookProfile;

    .line 1660967
    add-int/lit8 v4, v3, 0x1

    .line 1660968
    invoke-static {}, Lcom/facebook/audience/model/AudienceControlData;->newBuilder()Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v7

    iget-wide v9, v2, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    invoke-static {v9, v10}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setId(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v7

    iget-object v8, v2, Lcom/facebook/ipc/model/FacebookProfile;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setName(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v7

    iget-object v8, v2, Lcom/facebook/ipc/model/FacebookProfile;->mImageUrl:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setProfileUri(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v7

    invoke-virtual {v7, v3}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setRanking(I)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v7

    sget-object v8, LX/7gf;->SEARCH_FRIENDS:LX/7gf;

    invoke-virtual {v7, v8}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setAudienceType(LX/7gf;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/audience/model/AudienceControlData$Builder;->a()Lcom/facebook/audience/model/AudienceControlData;

    move-result-object v7

    move-object v2, v7

    .line 1660969
    invoke-virtual {v5, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move v3, v4

    .line 1660970
    goto :goto_1

    .line 1660971
    :cond_1
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    goto :goto_0
.end method
