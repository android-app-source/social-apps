.class public final LX/9im;
.super LX/7UT;
.source ""


# instance fields
.field public final synthetic a:LX/9ip;


# direct methods
.method public constructor <init>(LX/9ip;)V
    .locals 0

    .prologue
    .line 1528351
    iput-object p1, p0, LX/9im;->a:LX/9ip;

    invoke-direct {p0}, LX/7UT;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Matrix;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1528352
    iget-object v0, p0, LX/9im;->a:LX/9ip;

    iget-boolean v0, v0, LX/9ip;->j:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/9im;->a:LX/9ip;

    invoke-virtual {v0}, LX/9ip;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1528353
    :cond_0
    iget-object v0, p0, LX/9im;->a:LX/9ip;

    iget-object v0, v0, LX/9ip;->c:LX/9j0;

    iget-object v1, p0, LX/9im;->a:LX/9ip;

    iget-boolean v1, v1, LX/9ip;->i:Z

    invoke-virtual {v0, v1}, LX/9j0;->a(Z)V

    .line 1528354
    :cond_1
    iget-object v0, p0, LX/9im;->a:LX/9ip;

    iget-boolean v0, v0, LX/9ip;->j:Z

    if-nez v0, :cond_2

    iget-object v0, p0, LX/9im;->a:LX/9ip;

    iget-object v0, v0, LX/9ip;->d:Lcom/facebook/photos/tagging/shared/FaceBoxesView;

    invoke-virtual {v0}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1528355
    :cond_2
    iget-object v0, p0, LX/9im;->a:LX/9ip;

    invoke-static {v0}, LX/9ip;->v(LX/9ip;)V

    .line 1528356
    :cond_3
    iget-object v0, p0, LX/9im;->a:LX/9ip;

    iget-boolean v0, v0, LX/9ip;->j:Z

    if-nez v0, :cond_4

    iget-object v0, p0, LX/9im;->a:LX/9ip;

    .line 1528357
    iget-object v1, v0, LX/9ip;->g:LX/8Hs;

    invoke-virtual {v1}, LX/8Hs;->a()Z

    move-result v1

    move v0, v1

    .line 1528358
    if-eqz v0, :cond_5

    .line 1528359
    :cond_4
    iget-object v0, p0, LX/9im;->a:LX/9ip;

    iget-object v0, v0, LX/9ip;->e:LX/8Jd;

    invoke-virtual {v0, p1}, LX/8Jd;->setTransformMatrix(Landroid/graphics/Matrix;)V

    .line 1528360
    :cond_5
    iget-object v0, p0, LX/9im;->a:LX/9ip;

    invoke-virtual {v0}, LX/9iQ;->getZoomableImageView()Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    move-result-object v0

    invoke-virtual {v0}, LX/7UQ;->getScale()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_8

    .line 1528361
    iget-object v0, p0, LX/9im;->a:LX/9ip;

    iget-boolean v0, v0, LX/9ip;->i:Z

    if-nez v0, :cond_7

    .line 1528362
    iget-object v0, p0, LX/9im;->a:LX/9ip;

    iget-object v0, v0, LX/9ip;->h:LX/BIm;

    if-eqz v0, :cond_6

    .line 1528363
    :cond_6
    iget-object v0, p0, LX/9im;->a:LX/9ip;

    const/4 v1, 0x1

    .line 1528364
    iput-boolean v1, v0, LX/9ip;->i:Z

    .line 1528365
    :cond_7
    :goto_0
    return-void

    .line 1528366
    :cond_8
    iget-object v0, p0, LX/9im;->a:LX/9ip;

    iget-boolean v0, v0, LX/9ip;->i:Z

    if-eqz v0, :cond_7

    .line 1528367
    iget-object v0, p0, LX/9im;->a:LX/9ip;

    iget-object v0, v0, LX/9ip;->h:LX/BIm;

    if-eqz v0, :cond_9

    .line 1528368
    iget-object v0, p0, LX/9im;->a:LX/9ip;

    iget-object v0, v0, LX/9ip;->c:LX/9j0;

    invoke-virtual {v0, v2}, LX/9j0;->a(Z)V

    .line 1528369
    :cond_9
    iget-object v0, p0, LX/9im;->a:LX/9ip;

    .line 1528370
    iput-boolean v2, v0, LX/9ip;->i:Z

    .line 1528371
    goto :goto_0
.end method

.method public final a(Landroid/graphics/PointF;Landroid/graphics/PointF;)V
    .locals 4

    .prologue
    .line 1528372
    iget-object v0, p0, LX/9im;->a:LX/9ip;

    iget-object v0, v0, LX/9ip;->d:Lcom/facebook/photos/tagging/shared/FaceBoxesView;

    invoke-virtual {v0}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/9im;->a:LX/9ip;

    iget-object v0, v0, LX/9ip;->d:Lcom/facebook/photos/tagging/shared/FaceBoxesView;

    invoke-virtual {v0}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/9im;->a:LX/9ip;

    iget-object v0, v0, LX/9ip;->d:Lcom/facebook/photos/tagging/shared/FaceBoxesView;

    invoke-virtual {v0, p1}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->a(Landroid/graphics/PointF;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1528373
    :cond_0
    :goto_0
    return-void

    .line 1528374
    :cond_1
    iget-object v0, p0, LX/9im;->a:LX/9ip;

    iget-object v0, v0, LX/9ip;->h:LX/BIm;

    if-eqz v0, :cond_0

    .line 1528375
    iget-object v0, p0, LX/9im;->a:LX/9ip;

    iget-object v0, v0, LX/9ip;->h:LX/BIm;

    .line 1528376
    iget-object v1, v0, LX/BIm;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    iget-boolean v1, v1, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->w:Z

    if-eqz v1, :cond_4

    iget-object v1, v0, LX/BIm;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    iget-boolean v1, v1, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->t:Z

    if-eqz v1, :cond_4

    .line 1528377
    iget-object v1, v0, LX/BIm;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    iget-object v1, v1, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->J:Ljava/util/List;

    if-nez v1, :cond_2

    .line 1528378
    iget-object v1, v0, LX/BIm;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-static {v1}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->l(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)V

    .line 1528379
    :cond_2
    if-eqz p2, :cond_3

    iget-object v1, v0, LX/BIm;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-static {v1}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->q(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)LX/9ip;

    move-result-object v1

    .line 1528380
    iget-boolean v2, v1, LX/9iQ;->e:Z

    move v1, v2

    .line 1528381
    if-eqz v1, :cond_3

    .line 1528382
    iget-object v1, v0, LX/BIm;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    iget-object v1, v1, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->T:LX/9iw;

    iget-object v2, v0, LX/BIm;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-static {v2}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->p(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)Lcom/facebook/photos/base/photos/LocalPhoto;

    move-result-object v2

    new-instance v3, Lcom/facebook/photos/base/tagging/TagPoint;

    iget-object p0, v0, LX/BIm;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    iget-object p0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->J:Ljava/util/List;

    invoke-direct {v3, p2, p0}, Lcom/facebook/photos/base/tagging/TagPoint;-><init>(Landroid/graphics/PointF;Ljava/util/List;)V

    const/4 p0, 0x0

    iget-object p1, v0, LX/BIm;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-static {p1}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->q(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)LX/9ip;

    move-result-object p1

    .line 1528383
    iget-object v0, p1, LX/9ip;->p:LX/9ic;

    move-object p1, v0

    .line 1528384
    invoke-virtual {v1, v2, v3, p0, p1}, LX/9iw;->a(LX/74x;Lcom/facebook/photos/base/tagging/TagTarget;ZLX/9ic;)V

    .line 1528385
    :cond_3
    :goto_1
    goto :goto_0

    .line 1528386
    :cond_4
    iget-object v1, v0, LX/BIm;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    iget-boolean v1, v1, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->r:Z

    if-nez v1, :cond_3

    .line 1528387
    iget-object v1, v0, LX/BIm;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    iget-boolean v1, v1, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->s:Z

    if-eqz v1, :cond_5

    .line 1528388
    iget-object v1, v0, LX/BIm;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-static {v1}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->r$redex0(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)V

    goto :goto_1

    .line 1528389
    :cond_5
    iget-object v1, v0, LX/BIm;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-static {v1}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->s$redex0(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)V

    goto :goto_1
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1528390
    iget-object v0, p0, LX/9im;->a:LX/9ip;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/9ip;->setTagsAndFaceboxesEnabled(Z)V

    .line 1528391
    return-void
.end method

.method public final b(Landroid/graphics/PointF;Landroid/graphics/PointF;)V
    .locals 4

    .prologue
    .line 1528392
    iget-object v0, p0, LX/9im;->a:LX/9ip;

    iget-object v0, v0, LX/9ip;->h:LX/BIm;

    if-eqz v0, :cond_0

    .line 1528393
    iget-object v0, p0, LX/9im;->a:LX/9ip;

    iget-object v0, v0, LX/9ip;->h:LX/BIm;

    .line 1528394
    if-eqz p2, :cond_0

    iget-object v1, v0, LX/BIm;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-static {v1}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->q(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)LX/9ip;

    move-result-object v1

    .line 1528395
    iget-boolean v2, v1, LX/9iQ;->e:Z

    move v1, v2

    .line 1528396
    if-eqz v1, :cond_0

    iget-object v1, v0, LX/BIm;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    iget-boolean v1, v1, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->x:Z

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/BIm;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    iget-boolean v1, v1, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->t:Z

    if-nez v1, :cond_1

    .line 1528397
    :cond_0
    :goto_0
    return-void

    .line 1528398
    :cond_1
    iget-object v1, v0, LX/BIm;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    iget-object v1, v1, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->T:LX/9iw;

    iget-object v2, v0, LX/BIm;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-static {v2}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->p(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)Lcom/facebook/photos/base/photos/LocalPhoto;

    move-result-object v2

    new-instance v3, Lcom/facebook/photos/base/tagging/TagPoint;

    iget-object p0, v0, LX/BIm;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    iget-object p0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->J:Ljava/util/List;

    invoke-direct {v3, p2, p0}, Lcom/facebook/photos/base/tagging/TagPoint;-><init>(Landroid/graphics/PointF;Ljava/util/List;)V

    const/4 p0, 0x0

    iget-object p1, v0, LX/BIm;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-static {p1}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->q(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)LX/9ip;

    move-result-object p1

    .line 1528399
    iget-object v0, p1, LX/9ip;->p:LX/9ic;

    move-object p1, v0

    .line 1528400
    invoke-virtual {v1, v2, v3, p0, p1}, LX/9iw;->a(LX/74x;Lcom/facebook/photos/base/tagging/TagTarget;ZLX/9ic;)V

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1528401
    iget-object v0, p0, LX/9im;->a:LX/9ip;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/9ip;->setTagsAndFaceboxesEnabled(Z)V

    .line 1528402
    return-void
.end method
