.class public LX/8mh;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Landroid/graphics/drawable/Drawable;

.field public b:Landroid/animation/ValueAnimator;

.field public c:Z


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;)V
    .locals 4

    .prologue
    .line 1399721
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1399722
    iput-object p1, p0, LX/8mh;->a:Landroid/graphics/drawable/Drawable;

    .line 1399723
    const/16 v0, 0x1388

    const/4 v1, 0x0

    .line 1399724
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-ge v2, v3, :cond_1

    .line 1399725
    :cond_0
    :goto_0
    move-object v0, v1

    .line 1399726
    iput-object v0, p0, LX/8mh;->b:Landroid/animation/ValueAnimator;

    .line 1399727
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/8mh;->c:Z

    .line 1399728
    return-void

    .line 1399729
    :cond_1
    instance-of v2, p1, LX/4dD;

    if-eqz v2, :cond_2

    .line 1399730
    check-cast p1, LX/4dD;

    invoke-interface {p1, v0}, LX/4dD;->a(I)Landroid/animation/ValueAnimator;

    move-result-object v1

    goto :goto_0

    .line 1399731
    :cond_2
    instance-of v2, p1, LX/4CI;

    if-eqz v2, :cond_0

    .line 1399732
    check-cast p1, LX/4CI;

    invoke-static {p1, v0}, LX/83U;->a(LX/4CI;I)Landroid/animation/ValueAnimator;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public final d()Z
    .locals 1

    .prologue
    .line 1399720
    iget-object v0, p0, LX/8mh;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    return v0
.end method
