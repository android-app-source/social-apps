.class public final LX/8mt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/stickers/model/StickerTag;

.field public final synthetic b:LX/8mu;


# direct methods
.method public constructor <init>(LX/8mu;Lcom/facebook/stickers/model/StickerTag;)V
    .locals 0

    .prologue
    .line 1400045
    iput-object p1, p0, LX/8mt;->b:LX/8mu;

    iput-object p2, p0, LX/8mt;->a:Lcom/facebook/stickers/model/StickerTag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x3e5871b6

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1400046
    iget-object v0, p0, LX/8mt;->b:LX/8mu;

    iget-object v0, v0, LX/8mu;->c:LX/8lN;

    if-eqz v0, :cond_1

    .line 1400047
    iget-object v0, p0, LX/8mt;->b:LX/8mu;

    iget-object v2, p0, LX/8mt;->a:Lcom/facebook/stickers/model/StickerTag;

    invoke-static {v0, v2}, LX/8mu;->a$redex0(LX/8mu;Lcom/facebook/stickers/model/StickerTag;)Ljava/lang/String;

    move-result-object v0

    .line 1400048
    iget-object v2, p0, LX/8mt;->b:LX/8mu;

    iget-object v2, v2, LX/8mu;->c:LX/8lN;

    iget-object v3, p0, LX/8mt;->a:Lcom/facebook/stickers/model/StickerTag;

    if-eqz v0, :cond_2

    .line 1400049
    :goto_0
    iget-object v4, v3, Lcom/facebook/stickers/model/StickerTag;->b:Ljava/lang/String;

    move-object v4, v4

    .line 1400050
    invoke-static {v4}, LX/0YN;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1400051
    iget-object p0, v2, LX/8lN;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-object p0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->K:LX/8l6;

    invoke-interface {p0, v4}, LX/8l6;->a(Ljava/lang/String;)V

    .line 1400052
    iget-object v4, v2, LX/8lN;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-object v4, v4, Lcom/facebook/stickers/search/StickerSearchContainer;->R:LX/4m4;

    sget-object p0, LX/4m4;->SMS:LX/4m4;

    if-eq v4, p0, :cond_0

    iget-object v4, v2, LX/8lN;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-boolean v4, v4, Lcom/facebook/stickers/search/StickerSearchContainer;->f:Z

    if-nez v4, :cond_1

    :cond_0
    iget-object v4, v2, LX/8lN;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-boolean v4, v4, Lcom/facebook/stickers/search/StickerSearchContainer;->P:Z

    if-eqz v4, :cond_3

    .line 1400053
    :cond_1
    :goto_1
    const v0, 0x54d28835

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 1400054
    :cond_2
    iget-object v0, p0, LX/8mt;->a:Lcom/facebook/stickers/model/StickerTag;

    .line 1400055
    iget-object v4, v0, Lcom/facebook/stickers/model/StickerTag;->b:Ljava/lang/String;

    move-object v0, v4

    .line 1400056
    goto :goto_0

    .line 1400057
    :cond_3
    iget-object v4, v2, LX/8lN;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    const/4 p0, 0x1

    .line 1400058
    iput-boolean p0, v4, Lcom/facebook/stickers/search/StickerSearchContainer;->O:Z

    .line 1400059
    iget-object v4, v2, LX/8lN;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    .line 1400060
    iget-object p0, v3, Lcom/facebook/stickers/model/StickerTag;->b:Ljava/lang/String;

    move-object p0, p0

    .line 1400061
    invoke-static {v4, p0}, Lcom/facebook/stickers/search/StickerSearchContainer;->setQuery(Lcom/facebook/stickers/search/StickerSearchContainer;Ljava/lang/String;)V

    .line 1400062
    iget-object v4, v2, LX/8lN;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-object v4, v4, Lcom/facebook/stickers/search/StickerSearchContainer;->z:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v4, v0}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1400063
    iget-object v4, v2, LX/8lN;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-object v4, v4, Lcom/facebook/stickers/search/StickerSearchContainer;->z:Lcom/facebook/resources/ui/FbEditText;

    iget-object p0, v2, LX/8lN;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-object p0, p0, Lcom/facebook/stickers/search/StickerSearchContainer;->z:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {p0}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object p0

    invoke-interface {p0}, Landroid/text/Editable;->length()I

    move-result p0

    invoke-virtual {v4, p0}, Lcom/facebook/resources/ui/FbEditText;->setSelection(I)V

    .line 1400064
    iget-object v4, v2, LX/8lN;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-object v4, v4, Lcom/facebook/stickers/search/StickerSearchContainer;->t:LX/8j9;

    .line 1400065
    iget-object p0, v3, Lcom/facebook/stickers/model/StickerTag;->a:Ljava/lang/String;

    move-object p0, p0

    .line 1400066
    const-string p1, "featured_tag_selected"

    invoke-static {v4, p1}, LX/8j9;->e(LX/8j9;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    .line 1400067
    const-string v0, "tag_id"

    invoke-virtual {p1, v0, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1400068
    iget-object v0, v4, LX/8j9;->a:LX/0Zb;

    invoke-interface {v0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1400069
    iget-object v4, v2, LX/8lN;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    .line 1400070
    iget-object p0, v3, Lcom/facebook/stickers/model/StickerTag;->a:Ljava/lang/String;

    move-object p0, p0

    .line 1400071
    iget-object p1, v4, Lcom/facebook/stickers/search/StickerSearchContainer;->l:Lcom/facebook/stickers/search/TaggedStickersLoader;

    invoke-virtual {p1}, LX/6Lb;->a()V

    .line 1400072
    sget-object p1, LX/8lg;->WAIT_FOR_TAGGED_STICKERS:LX/8lg;

    invoke-static {v4, p1}, Lcom/facebook/stickers/search/StickerSearchContainer;->setCurrentState(Lcom/facebook/stickers/search/StickerSearchContainer;LX/8lg;)V

    .line 1400073
    iget-object p1, v4, Lcom/facebook/stickers/search/StickerSearchContainer;->l:Lcom/facebook/stickers/search/TaggedStickersLoader;

    new-instance v2, LX/8lo;

    invoke-direct {v2, p0}, LX/8lo;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v2}, LX/6Lb;->a(Ljava/lang/Object;)V

    .line 1400074
    goto :goto_1
.end method
