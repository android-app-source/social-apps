.class public LX/9bt;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1515283
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLAlbum;Landroid/content/Context;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 1515284
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->v()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1515285
    const/4 v0, 0x0

    .line 1515286
    :goto_0
    return-object v0

    .line 1515287
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->v()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;->a()I

    move-result v0

    if-nez v0, :cond_1

    .line 1515288
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0811d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1515289
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->C()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1515290
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->C()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;->a()I

    move-result v0

    .line 1515291
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->v()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;->a()I

    move-result v2

    sub-int v4, v2, v0

    .line 1515292
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0085

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v2, v3, v0, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1515293
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0f0087

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v1

    invoke-virtual {v2, v5, v4, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1515294
    if-nez v0, :cond_2

    move-object v0, v2

    .line 1515295
    goto :goto_0

    .line 1515296
    :cond_2
    if-nez v4, :cond_3

    move-object v0, v3

    .line 1515297
    goto :goto_0

    .line 1515298
    :cond_3
    const-string v0, "%s, %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v3, v4, v1

    aput-object v2, v4, v8

    invoke-static {v0, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method
