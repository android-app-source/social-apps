.class public LX/9bp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<*>;",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/9bp;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1515170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/9bp;
    .locals 3

    .prologue
    .line 1515171
    sget-object v0, LX/9bp;->a:LX/9bp;

    if-nez v0, :cond_1

    .line 1515172
    const-class v1, LX/9bp;

    monitor-enter v1

    .line 1515173
    :try_start_0
    sget-object v0, LX/9bp;->a:LX/9bp;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1515174
    if-eqz v2, :cond_0

    .line 1515175
    :try_start_1
    new-instance v0, LX/9bp;

    invoke-direct {v0}, LX/9bp;-><init>()V

    .line 1515176
    move-object v0, v0

    .line 1515177
    sput-object v0, LX/9bp;->a:LX/9bp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1515178
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1515179
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1515180
    :cond_1
    sget-object v0, LX/9bp;->a:LX/9bp;

    return-object v0

    .line 1515181
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1515182
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1515183
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1515184
    if-eqz p1, :cond_0

    .line 1515185
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1515186
    if-nez v0, :cond_1

    .line 1515187
    :cond_0
    sget-object v0, LX/1nY;->API_ERROR:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 1515188
    :goto_0
    return-object v0

    .line 1515189
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1515190
    instance-of v0, v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListQueryModel;

    if-nez v0, :cond_2

    .line 1515191
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1515192
    instance-of v0, v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListCanUploadOnlyQueryModel;

    if-nez v0, :cond_2

    .line 1515193
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1515194
    instance-of v0, v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$GroupAlbumListQueryModel;

    if-nez v0, :cond_2

    .line 1515195
    sget-object v0, LX/1nY;->API_ERROR:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 1515196
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1515197
    instance-of v0, v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListQueryModel;

    if-eqz v0, :cond_3

    .line 1515198
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1515199
    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListQueryModel;

    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListQueryModel;->a()Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    move-result-object v0

    .line 1515200
    :goto_1
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 1515201
    :cond_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1515202
    instance-of v0, v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$GroupAlbumListQueryModel;

    if-eqz v0, :cond_4

    .line 1515203
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1515204
    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$GroupAlbumListQueryModel;

    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$GroupAlbumListQueryModel;->a()Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    move-result-object v0

    goto :goto_1

    .line 1515205
    :cond_4
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1515206
    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListCanUploadOnlyQueryModel;

    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$AlbumListCanUploadOnlyQueryModel;->a()Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    move-result-object v0

    goto :goto_1
.end method
