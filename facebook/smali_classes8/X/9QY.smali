.class public final LX/9QY;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 49

    .prologue
    .line 1487497
    const/16 v42, 0x0

    .line 1487498
    const-wide/16 v40, 0x0

    .line 1487499
    const/16 v39, 0x0

    .line 1487500
    const/16 v38, 0x0

    .line 1487501
    const/16 v37, 0x0

    .line 1487502
    const/16 v36, 0x0

    .line 1487503
    const/16 v35, 0x0

    .line 1487504
    const/16 v34, 0x0

    .line 1487505
    const/16 v33, 0x0

    .line 1487506
    const/16 v32, 0x0

    .line 1487507
    const/16 v31, 0x0

    .line 1487508
    const/16 v30, 0x0

    .line 1487509
    const/16 v29, 0x0

    .line 1487510
    const/16 v28, 0x0

    .line 1487511
    const/16 v27, 0x0

    .line 1487512
    const/16 v26, 0x0

    .line 1487513
    const/16 v25, 0x0

    .line 1487514
    const/16 v24, 0x0

    .line 1487515
    const/16 v23, 0x0

    .line 1487516
    const/16 v22, 0x0

    .line 1487517
    const/16 v21, 0x0

    .line 1487518
    const/16 v20, 0x0

    .line 1487519
    const/16 v19, 0x0

    .line 1487520
    const/16 v18, 0x0

    .line 1487521
    const/16 v17, 0x0

    .line 1487522
    const/16 v16, 0x0

    .line 1487523
    const/4 v15, 0x0

    .line 1487524
    const/4 v14, 0x0

    .line 1487525
    const/4 v13, 0x0

    .line 1487526
    const/4 v12, 0x0

    .line 1487527
    const/4 v11, 0x0

    .line 1487528
    const/4 v10, 0x0

    .line 1487529
    const/4 v9, 0x0

    .line 1487530
    const/4 v8, 0x0

    .line 1487531
    const/4 v7, 0x0

    .line 1487532
    const/4 v6, 0x0

    .line 1487533
    const/4 v5, 0x0

    .line 1487534
    const/4 v4, 0x0

    .line 1487535
    const/4 v3, 0x0

    .line 1487536
    const/4 v2, 0x0

    .line 1487537
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v43

    sget-object v44, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v43

    move-object/from16 v1, v44

    if-eq v0, v1, :cond_2a

    .line 1487538
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1487539
    const/4 v2, 0x0

    .line 1487540
    :goto_0
    return v2

    .line 1487541
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_24

    .line 1487542
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1487543
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1487544
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v45, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v45

    if-eq v6, v0, :cond_0

    if-eqz v2, :cond_0

    .line 1487545
    const-string v6, "archived_message"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1487546
    invoke-static/range {p0 .. p1}, LX/9QH;->a(LX/15w;LX/186;)I

    move-result v2

    move v7, v2

    goto :goto_1

    .line 1487547
    :cond_1
    const-string v6, "archived_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1487548
    const/4 v2, 0x1

    .line 1487549
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 1487550
    :cond_2
    const-string v6, "bookmark_image"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1487551
    invoke-static/range {p0 .. p1}, LX/9QI;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v44, v2

    goto :goto_1

    .line 1487552
    :cond_3
    const-string v6, "community_category"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1487553
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v43, v2

    goto :goto_1

    .line 1487554
    :cond_4
    const-string v6, "community_location"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1487555
    invoke-static/range {p0 .. p1}, LX/9QK;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v42, v2

    goto :goto_1

    .line 1487556
    :cond_5
    const-string v6, "cover_photo"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1487557
    invoke-static/range {p0 .. p1}, LX/9QM;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v41, v2

    goto :goto_1

    .line 1487558
    :cond_6
    const-string v6, "description"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1487559
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v40, v2

    goto/16 :goto_1

    .line 1487560
    :cond_7
    const-string v6, "email_domains"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1487561
    invoke-static/range {p0 .. p1}, LX/9Pl;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v39, v2

    goto/16 :goto_1

    .line 1487562
    :cond_8
    const-string v6, "forumFields"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 1487563
    invoke-static/range {p0 .. p1}, LX/9QF;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v38, v2

    goto/16 :goto_1

    .line 1487564
    :cond_9
    const-string v6, "full_name"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1487565
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v37, v2

    goto/16 :goto_1

    .line 1487566
    :cond_a
    const-string v6, "group_events"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 1487567
    invoke-static/range {p0 .. p1}, LX/9QO;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v36, v2

    goto/16 :goto_1

    .line 1487568
    :cond_b
    const-string v6, "group_member_profiles"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 1487569
    invoke-static/range {p0 .. p1}, LX/9QQ;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v35, v2

    goto/16 :goto_1

    .line 1487570
    :cond_c
    const-string v6, "group_members"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 1487571
    invoke-static/range {p0 .. p1}, LX/9QR;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v34, v2

    goto/16 :goto_1

    .line 1487572
    :cond_d
    const-string v6, "group_members_viewer_friend_count"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 1487573
    const/4 v2, 0x1

    .line 1487574
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v11, v2

    move/from16 v33, v6

    goto/16 :goto_1

    .line 1487575
    :cond_e
    const-string v6, "group_owner_authored_stories"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 1487576
    invoke-static/range {p0 .. p1}, LX/9QS;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v32, v2

    goto/16 :goto_1

    .line 1487577
    :cond_f
    const-string v6, "group_pending_members"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 1487578
    invoke-static/range {p0 .. p1}, LX/9QT;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v31, v2

    goto/16 :goto_1

    .line 1487579
    :cond_10
    const-string v6, "group_pending_stories"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_11

    .line 1487580
    invoke-static/range {p0 .. p1}, LX/9QU;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v30, v2

    goto/16 :goto_1

    .line 1487581
    :cond_11
    const-string v6, "group_reported_stories"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_12

    .line 1487582
    invoke-static/range {p0 .. p1}, LX/9QV;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v29, v2

    goto/16 :goto_1

    .line 1487583
    :cond_12
    const-string v6, "group_topic_tags"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_13

    .line 1487584
    invoke-static/range {p0 .. p1}, LX/9QW;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v28, v2

    goto/16 :goto_1

    .line 1487585
    :cond_13
    const-string v6, "has_viewer_favorited"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_14

    .line 1487586
    const/4 v2, 0x1

    .line 1487587
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v10, v2

    move/from16 v27, v6

    goto/16 :goto_1

    .line 1487588
    :cond_14
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_15

    .line 1487589
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v26, v2

    goto/16 :goto_1

    .line 1487590
    :cond_15
    const-string v6, "is_multi_company_group"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_16

    .line 1487591
    const/4 v2, 0x1

    .line 1487592
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v9, v2

    move/from16 v25, v6

    goto/16 :goto_1

    .line 1487593
    :cond_16
    const-string v6, "name"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_17

    .line 1487594
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v24, v2

    goto/16 :goto_1

    .line 1487595
    :cond_17
    const-string v6, "parent_group"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_18

    .line 1487596
    invoke-static/range {p0 .. p1}, LX/9Pm;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v23, v2

    goto/16 :goto_1

    .line 1487597
    :cond_18
    const-string v6, "url"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_19

    .line 1487598
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v22, v2

    goto/16 :goto_1

    .line 1487599
    :cond_19
    const-string v6, "viewer_content_safety_restrictions"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1a

    .line 1487600
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v21, v2

    goto/16 :goto_1

    .line 1487601
    :cond_1a
    const-string v6, "viewer_email_in_domain"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1b

    .line 1487602
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 1487603
    :cond_1b
    const-string v6, "viewer_has_seen_multi_company_nux"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1c

    .line 1487604
    const/4 v2, 0x1

    .line 1487605
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v8, v2

    move/from16 v19, v6

    goto/16 :goto_1

    .line 1487606
    :cond_1c
    const-string v6, "viewer_join_state"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1d

    .line 1487607
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 1487608
    :cond_1d
    const-string v6, "viewer_leave_scenario"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1e

    .line 1487609
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 1487610
    :cond_1e
    const-string v6, "viewer_pending_auth_state"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1f

    .line 1487611
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLGroupPendingState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupPendingState;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v16, v2

    goto/16 :goto_1

    .line 1487612
    :cond_1f
    const-string v6, "viewer_subscription_level"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_20

    .line 1487613
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 1487614
    :cond_20
    const-string v6, "visibility"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_21

    .line 1487615
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 1487616
    :cond_21
    const-string v6, "visibility_sentence"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_22

    .line 1487617
    invoke-static/range {p0 .. p1}, LX/9QX;->a(LX/15w;LX/186;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 1487618
    :cond_22
    const-string v6, "work_communities"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_23

    .line 1487619
    invoke-static/range {p0 .. p1}, LX/9Qr;->a(LX/15w;LX/186;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 1487620
    :cond_23
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1487621
    :cond_24
    const/16 v2, 0x23

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1487622
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1487623
    if-eqz v3, :cond_25

    .line 1487624
    const/4 v3, 0x1

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1487625
    :cond_25
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1487626
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1487627
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1487628
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1487629
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1487630
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1487631
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1487632
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1487633
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1487634
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1487635
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1487636
    if-eqz v11, :cond_26

    .line 1487637
    const/16 v2, 0xd

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 1487638
    :cond_26
    const/16 v2, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1487639
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1487640
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1487641
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1487642
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1487643
    if-eqz v10, :cond_27

    .line 1487644
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1487645
    :cond_27
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1487646
    if-eqz v9, :cond_28

    .line 1487647
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1487648
    :cond_28
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1487649
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1487650
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1487651
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1487652
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1487653
    if-eqz v8, :cond_29

    .line 1487654
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1487655
    :cond_29
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1487656
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1487657
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1487658
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1487659
    const/16 v2, 0x20

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1487660
    const/16 v2, 0x21

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1487661
    const/16 v2, 0x22

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1487662
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_2a
    move/from16 v43, v38

    move/from16 v44, v39

    move/from16 v38, v33

    move/from16 v39, v34

    move/from16 v33, v28

    move/from16 v34, v29

    move/from16 v28, v23

    move/from16 v29, v24

    move/from16 v23, v18

    move/from16 v24, v19

    move/from16 v18, v13

    move/from16 v19, v14

    move v13, v8

    move v14, v9

    move v8, v2

    move v9, v3

    move v3, v6

    move/from16 v46, v11

    move v11, v5

    move/from16 v47, v20

    move/from16 v20, v15

    move v15, v10

    move v10, v4

    move-wide/from16 v4, v40

    move/from16 v40, v35

    move/from16 v41, v36

    move/from16 v35, v30

    move/from16 v36, v31

    move/from16 v31, v26

    move/from16 v30, v25

    move/from16 v26, v21

    move/from16 v25, v47

    move/from16 v21, v16

    move/from16 v16, v46

    move/from16 v48, v22

    move/from16 v22, v17

    move/from16 v17, v12

    move v12, v7

    move/from16 v7, v42

    move/from16 v42, v37

    move/from16 v37, v32

    move/from16 v32, v27

    move/from16 v27, v48

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 9

    .prologue
    const/16 v8, 0x1d

    const/16 v7, 0x1c

    const/16 v6, 0x19

    const/4 v5, 0x3

    const/4 v4, 0x0

    .line 1487663
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1487664
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 1487665
    if-eqz v0, :cond_0

    .line 1487666
    const-string v1, "archived_message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487667
    invoke-static {p0, v0, p2}, LX/9QH;->a(LX/15i;ILX/0nX;)V

    .line 1487668
    :cond_0
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1487669
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_1

    .line 1487670
    const-string v2, "archived_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487671
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1487672
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1487673
    if-eqz v0, :cond_3

    .line 1487674
    const-string v1, "bookmark_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487675
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1487676
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1487677
    if-eqz v1, :cond_2

    .line 1487678
    const-string v2, "uri"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487679
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1487680
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1487681
    :cond_3
    invoke-virtual {p0, p1, v5}, LX/15i;->g(II)I

    move-result v0

    .line 1487682
    if-eqz v0, :cond_4

    .line 1487683
    const-string v0, "community_category"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487684
    invoke-virtual {p0, p1, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1487685
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1487686
    if-eqz v0, :cond_5

    .line 1487687
    const-string v1, "community_location"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487688
    invoke-static {p0, v0, p2, p3}, LX/9QK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1487689
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1487690
    if-eqz v0, :cond_7

    .line 1487691
    const-string v1, "cover_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487692
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1487693
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1487694
    if-eqz v1, :cond_6

    .line 1487695
    const-string v2, "photo"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487696
    invoke-static {p0, v1, p2, p3}, LX/9QL;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1487697
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1487698
    :cond_7
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1487699
    if-eqz v0, :cond_8

    .line 1487700
    const-string v1, "description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487701
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1487702
    :cond_8
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1487703
    if-eqz v0, :cond_9

    .line 1487704
    const-string v1, "email_domains"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487705
    invoke-static {p0, v0, p2, p3}, LX/9Pl;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1487706
    :cond_9
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1487707
    if-eqz v0, :cond_a

    .line 1487708
    const-string v1, "forumFields"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487709
    invoke-static {p0, v0, p2, p3}, LX/9QF;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1487710
    :cond_a
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1487711
    if-eqz v0, :cond_b

    .line 1487712
    const-string v1, "full_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487713
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1487714
    :cond_b
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1487715
    if-eqz v0, :cond_c

    .line 1487716
    const-string v1, "group_events"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487717
    invoke-static {p0, v0, p2, p3}, LX/9QO;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1487718
    :cond_c
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1487719
    if-eqz v0, :cond_d

    .line 1487720
    const-string v1, "group_member_profiles"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487721
    invoke-static {p0, v0, p2, p3}, LX/9QQ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1487722
    :cond_d
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1487723
    if-eqz v0, :cond_f

    .line 1487724
    const-string v1, "group_members"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487725
    const/4 v1, 0x0

    .line 1487726
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1487727
    invoke-virtual {p0, v0, v1, v1}, LX/15i;->a(III)I

    move-result v1

    .line 1487728
    if-eqz v1, :cond_e

    .line 1487729
    const-string v2, "count"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487730
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 1487731
    :cond_e
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1487732
    :cond_f
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0, v4}, LX/15i;->a(III)I

    move-result v0

    .line 1487733
    if-eqz v0, :cond_10

    .line 1487734
    const-string v1, "group_members_viewer_friend_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487735
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1487736
    :cond_10
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1487737
    if-eqz v0, :cond_11

    .line 1487738
    const-string v1, "group_owner_authored_stories"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487739
    invoke-static {p0, v0, p2}, LX/9QS;->a(LX/15i;ILX/0nX;)V

    .line 1487740
    :cond_11
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1487741
    if-eqz v0, :cond_13

    .line 1487742
    const-string v1, "group_pending_members"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487743
    const/4 v1, 0x0

    .line 1487744
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1487745
    invoke-virtual {p0, v0, v1, v1}, LX/15i;->a(III)I

    move-result v1

    .line 1487746
    if-eqz v1, :cond_12

    .line 1487747
    const-string v2, "count"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487748
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 1487749
    :cond_12
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1487750
    :cond_13
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1487751
    if-eqz v0, :cond_15

    .line 1487752
    const-string v1, "group_pending_stories"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487753
    const/4 v1, 0x0

    .line 1487754
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1487755
    invoke-virtual {p0, v0, v1, v1}, LX/15i;->a(III)I

    move-result v1

    .line 1487756
    if-eqz v1, :cond_14

    .line 1487757
    const-string v2, "count"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487758
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 1487759
    :cond_14
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1487760
    :cond_15
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1487761
    if-eqz v0, :cond_17

    .line 1487762
    const-string v1, "group_reported_stories"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487763
    const/4 v1, 0x0

    .line 1487764
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1487765
    invoke-virtual {p0, v0, v1, v1}, LX/15i;->a(III)I

    move-result v1

    .line 1487766
    if-eqz v1, :cond_16

    .line 1487767
    const-string v2, "count"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487768
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 1487769
    :cond_16
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1487770
    :cond_17
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1487771
    if-eqz v0, :cond_19

    .line 1487772
    const-string v1, "group_topic_tags"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487773
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1487774
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_18

    .line 1487775
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2}, LX/9QW;->a(LX/15i;ILX/0nX;)V

    .line 1487776
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1487777
    :cond_18
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1487778
    :cond_19
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1487779
    if-eqz v0, :cond_1a

    .line 1487780
    const-string v1, "has_viewer_favorited"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487781
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1487782
    :cond_1a
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1487783
    if-eqz v0, :cond_1b

    .line 1487784
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487785
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1487786
    :cond_1b
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1487787
    if-eqz v0, :cond_1c

    .line 1487788
    const-string v1, "is_multi_company_group"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487789
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1487790
    :cond_1c
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1487791
    if-eqz v0, :cond_1d

    .line 1487792
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487793
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1487794
    :cond_1d
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1487795
    if-eqz v0, :cond_1e

    .line 1487796
    const-string v1, "parent_group"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487797
    invoke-static {p0, v0, p2, p3}, LX/9Pm;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1487798
    :cond_1e
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1487799
    if-eqz v0, :cond_1f

    .line 1487800
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487801
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1487802
    :cond_1f
    invoke-virtual {p0, p1, v6}, LX/15i;->g(II)I

    move-result v0

    .line 1487803
    if-eqz v0, :cond_20

    .line 1487804
    const-string v0, "viewer_content_safety_restrictions"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487805
    invoke-virtual {p0, p1, v6}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1487806
    :cond_20
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1487807
    if-eqz v0, :cond_21

    .line 1487808
    const-string v1, "viewer_email_in_domain"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487809
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1487810
    :cond_21
    const/16 v0, 0x1b

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1487811
    if-eqz v0, :cond_22

    .line 1487812
    const-string v1, "viewer_has_seen_multi_company_nux"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487813
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1487814
    :cond_22
    invoke-virtual {p0, p1, v7}, LX/15i;->g(II)I

    move-result v0

    .line 1487815
    if-eqz v0, :cond_23

    .line 1487816
    const-string v0, "viewer_join_state"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487817
    invoke-virtual {p0, p1, v7}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1487818
    :cond_23
    invoke-virtual {p0, p1, v8}, LX/15i;->g(II)I

    move-result v0

    .line 1487819
    if-eqz v0, :cond_24

    .line 1487820
    const-string v0, "viewer_leave_scenario"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487821
    invoke-virtual {p0, p1, v8}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1487822
    :cond_24
    const/16 v0, 0x1e

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1487823
    if-eqz v0, :cond_25

    .line 1487824
    const-string v0, "viewer_pending_auth_state"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487825
    const/16 v0, 0x1e

    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1487826
    :cond_25
    const/16 v0, 0x1f

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1487827
    if-eqz v0, :cond_26

    .line 1487828
    const-string v0, "viewer_subscription_level"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487829
    const/16 v0, 0x1f

    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1487830
    :cond_26
    const/16 v0, 0x20

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1487831
    if-eqz v0, :cond_27

    .line 1487832
    const-string v0, "visibility"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487833
    const/16 v0, 0x20

    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1487834
    :cond_27
    const/16 v0, 0x21

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1487835
    if-eqz v0, :cond_28

    .line 1487836
    const-string v1, "visibility_sentence"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487837
    invoke-static {p0, v0, p2, p3}, LX/9QX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1487838
    :cond_28
    const/16 v0, 0x22

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1487839
    if-eqz v0, :cond_29

    .line 1487840
    const-string v1, "work_communities"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1487841
    invoke-static {p0, v0, p2, p3}, LX/9Qr;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1487842
    :cond_29
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1487843
    return-void
.end method
