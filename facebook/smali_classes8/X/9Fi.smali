.class public final enum LX/9Fi;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9Fi;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9Fi;

.field public static final enum THREADED:LX/9Fi;

.field public static final enum TOP_LEVEL:LX/9Fi;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1458999
    new-instance v0, LX/9Fi;

    const-string v1, "TOP_LEVEL"

    invoke-direct {v0, v1, v2}, LX/9Fi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9Fi;->TOP_LEVEL:LX/9Fi;

    .line 1459000
    new-instance v0, LX/9Fi;

    const-string v1, "THREADED"

    invoke-direct {v0, v1, v3}, LX/9Fi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9Fi;->THREADED:LX/9Fi;

    .line 1459001
    const/4 v0, 0x2

    new-array v0, v0, [LX/9Fi;

    sget-object v1, LX/9Fi;->TOP_LEVEL:LX/9Fi;

    aput-object v1, v0, v2

    sget-object v1, LX/9Fi;->THREADED:LX/9Fi;

    aput-object v1, v0, v3

    sput-object v0, LX/9Fi;->$VALUES:[LX/9Fi;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1459002
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getCommentLevel(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/9Fi;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;)",
            "LX/9Fi;"
        }
    .end annotation

    .prologue
    .line 1459003
    invoke-static {p0}, LX/5Op;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, LX/9Fi;->THREADED:LX/9Fi;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/9Fi;->TOP_LEVEL:LX/9Fi;

    goto :goto_0
.end method

.method public static getCommentLevelFromAttachment(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/9Fi;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "LX/9Fi;"
        }
    .end annotation

    .prologue
    .line 1459004
    invoke-static {p0}, LX/1WF;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    .line 1459005
    const/4 v2, 0x0

    .line 1459006
    iget-object v1, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v1

    .line 1459007
    move-object v4, v1

    move-object v1, v2

    move-object v2, v4

    .line 1459008
    :goto_0
    if-eqz v2, :cond_1

    .line 1459009
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1459010
    instance-of v3, v3, Lcom/facebook/graphql/model/GraphQLComment;

    if-eqz v3, :cond_0

    .line 1459011
    iget-object v1, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1459012
    check-cast v1, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1459013
    :cond_0
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v2, v3

    .line 1459014
    goto :goto_0

    .line 1459015
    :cond_1
    move-object v1, v1

    .line 1459016
    if-eq v0, v1, :cond_2

    sget-object v0, LX/9Fi;->THREADED:LX/9Fi;

    :goto_1
    return-object v0

    :cond_2
    sget-object v0, LX/9Fi;->TOP_LEVEL:LX/9Fi;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/9Fi;
    .locals 1

    .prologue
    .line 1459017
    const-class v0, LX/9Fi;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9Fi;

    return-object v0
.end method

.method public static values()[LX/9Fi;
    .locals 1

    .prologue
    .line 1459018
    sget-object v0, LX/9Fi;->$VALUES:[LX/9Fi;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9Fi;

    return-object v0
.end method
