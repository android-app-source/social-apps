.class public LX/93k;
.super LX/93Q;
.source ""


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/93q;LX/03V;LX/1Ck;Landroid/content/res/Resources;Ljava/lang/String;)V
    .locals 0
    .param p1    # LX/93q;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1434154
    invoke-direct {p0, p1, p2, p3}, LX/93Q;-><init>(LX/93q;LX/03V;LX/1Ck;)V

    .line 1434155
    iput-object p4, p0, LX/93k;->a:Landroid/content/res/Resources;

    .line 1434156
    iput-object p5, p0, LX/93k;->b:Ljava/lang/String;

    .line 1434157
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1434158
    invoke-super {p0}, LX/93Q;->a()V

    .line 1434159
    new-instance v0, LX/2rZ;

    invoke-direct {v0}, LX/2rZ;-><init>()V

    iget-object v1, p0, LX/93k;->a:Landroid/content/res/Resources;

    const v2, 0x7f08131d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1434160
    iput-object v1, v0, LX/2rZ;->d:Ljava/lang/String;

    .line 1434161
    move-object v0, v0

    .line 1434162
    invoke-virtual {v0}, LX/2rZ;->a()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    .line 1434163
    new-instance v0, LX/7lN;

    invoke-direct {v0}, LX/7lN;-><init>()V

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->EVERYONE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    .line 1434164
    iput-object v1, v0, LX/7lN;->a:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    .line 1434165
    move-object v0, v0

    .line 1434166
    iget-object v1, p0, LX/93k;->a:Landroid/content/res/Resources;

    const v2, 0x7f08131d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1434167
    iput-object v1, v0, LX/7lN;->b:Ljava/lang/String;

    .line 1434168
    move-object v0, v0

    .line 1434169
    iget-object v1, p0, LX/93k;->a:Landroid/content/res/Resources;

    const v2, 0x7f081328

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1434170
    iput-object v1, v0, LX/7lN;->c:Ljava/lang/String;

    .line 1434171
    move-object v0, v0

    .line 1434172
    invoke-virtual {v0}, LX/7lN;->a()Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    move-result-object v0

    .line 1434173
    new-instance v1, LX/7lP;

    invoke-direct {v1}, LX/7lP;-><init>()V

    const/4 v2, 0x1

    .line 1434174
    iput-boolean v2, v1, LX/7lP;->a:Z

    .line 1434175
    move-object v1, v1

    .line 1434176
    invoke-virtual {v1, v0}, LX/7lP;->a(Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;)LX/7lP;

    move-result-object v0

    invoke-virtual {v0}, LX/7lP;->a()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    .line 1434177
    invoke-virtual {p0, v0}, LX/93Q;->a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    .line 1434178
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1434179
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "marketplace:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/93k;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
