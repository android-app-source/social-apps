.class public LX/93N;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1433635
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1433636
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/93N;->a:Z

    return-void
.end method


# virtual methods
.method public final a(LX/93H;Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;LX/5LG;ZLcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;)V
    .locals 7
    .param p5    # Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1433637
    const/4 v1, 0x0

    .line 1433638
    invoke-virtual {p1}, LX/93H;->d()V

    .line 1433639
    if-eqz p4, :cond_0

    .line 1433640
    const v0, 0x7f020305

    invoke-virtual {p1, v0}, LX/93H;->setBackgroundResource(I)V

    .line 1433641
    :goto_0
    invoke-virtual {p2}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->c()Ljava/lang/String;

    move-result-object v5

    .line 1433642
    if-eqz p4, :cond_1

    invoke-virtual {p2}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->e()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->e()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->d()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1433643
    invoke-virtual {p2}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->e()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->d()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v0

    move-object v0, v1

    .line 1433644
    :goto_1
    iget-object v6, p1, LX/93H;->j:Landroid/widget/TextView;

    invoke-static {v6, v5}, LX/93H;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1433645
    move-object v5, p1

    .line 1433646
    iget-object v6, v5, LX/93H;->k:LX/0am;

    invoke-virtual {v6}, LX/0am;->isPresent()Z

    move-result v6

    invoke-static {v6}, LX/0PB;->checkArgument(Z)V

    .line 1433647
    iget-object v6, v5, LX/93H;->k:LX/0am;

    invoke-virtual {v6}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    invoke-static {v6, v0}, LX/93H;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1433648
    move-object v0, v5

    .line 1433649
    iput-object v3, v0, LX/93H;->u:Landroid/net/Uri;

    .line 1433650
    iget-object v5, v0, LX/93H;->m:Landroid/graphics/drawable/Drawable;

    iget v6, v0, LX/93H;->p:I

    invoke-static {v0, v3, v5, v6}, LX/93H;->a(LX/93H;Landroid/net/Uri;Landroid/graphics/drawable/Drawable;I)V

    .line 1433651
    move-object v0, v0

    .line 1433652
    iget-object v5, v0, LX/93H;->n:Landroid/graphics/drawable/Drawable;

    iget v6, v0, LX/93H;->q:I

    invoke-static {v0, v4, v5, v6}, LX/93H;->a(LX/93H;Landroid/net/Uri;Landroid/graphics/drawable/Drawable;I)V

    .line 1433653
    move-object v0, v0

    .line 1433654
    iget-boolean v4, p0, LX/93N;->a:Z

    if-eqz v4, :cond_3

    if-eqz v3, :cond_3

    .line 1433655
    :goto_2
    iget-object v2, v0, LX/93H;->u:Landroid/net/Uri;

    if-nez v2, :cond_7

    .line 1433656
    iget-object v2, v0, LX/93H;->n:Landroid/graphics/drawable/Drawable;

    iget v3, v0, LX/93H;->q:I

    invoke-static {v0, v1, v2, v3}, LX/93H;->a(LX/93H;Landroid/net/Uri;Landroid/graphics/drawable/Drawable;I)V

    .line 1433657
    :goto_3
    move-object v0, v0

    .line 1433658
    invoke-virtual {p2}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->e()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->c()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/93H;->a(Z)LX/93H;

    .line 1433659
    new-instance v0, LX/93M;

    invoke-direct {v0, p0, p5, p2}, LX/93M;-><init>(LX/93N;Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;)V

    invoke-virtual {p1, v0}, LX/93H;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1433660
    return-void

    .line 1433661
    :cond_0
    const v0, 0x7f020309

    invoke-virtual {p1, v0}, LX/93H;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 1433662
    :cond_1
    invoke-interface {p3}, LX/5LG;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->d()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1433663
    invoke-virtual {p2}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->d()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v0

    move-object v0, v1

    goto :goto_1

    .line 1433664
    :cond_2
    invoke-virtual {p2}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->ah_()Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$SubtextModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 1433665
    invoke-virtual {p2}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->ah_()Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$SubtextModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$SubtextModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 1433666
    :goto_4
    invoke-virtual {p2}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->e()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {p2}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->e()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->d()LX/1Fb;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 1433667
    invoke-virtual {p2}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->e()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->d()LX/1Fb;

    move-result-object v2

    invoke-interface {v2}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1433668
    :goto_5
    invoke-virtual {p2}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->d()LX/1Fb;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 1433669
    invoke-virtual {p2}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->d()LX/1Fb;

    move-result-object v3

    invoke-interface {v3}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    move-object v4, v1

    move-object v6, v2

    move-object v2, v3

    move-object v3, v6

    goto/16 :goto_1

    :cond_3
    move-object v1, v2

    .line 1433670
    goto/16 :goto_2

    :cond_4
    move-object v3, v2

    move-object v4, v1

    move-object v2, v1

    goto/16 :goto_1

    :cond_5
    move-object v2, v1

    goto :goto_5

    :cond_6
    move-object v0, v1

    goto :goto_4

    .line 1433671
    :cond_7
    iget-object v2, v0, LX/93H;->n:Landroid/graphics/drawable/Drawable;

    iget v3, v0, LX/93H;->q:I

    .line 1433672
    if-nez v1, :cond_8

    .line 1433673
    :goto_6
    goto/16 :goto_3

    .line 1433674
    :cond_8
    iget v4, v0, LX/93H;->p:I

    sub-int/2addr v4, v3

    div-int/lit8 v4, v4, 0x2

    .line 1433675
    iget-object v5, v0, LX/93H;->l:Lcom/facebook/drawee/view/DraweeView;

    invoke-virtual {v5, v4, v4, v4, v4}, Lcom/facebook/drawee/view/DraweeView;->setPadding(IIII)V

    .line 1433676
    new-instance v4, LX/1Uo;

    invoke-virtual {v0}, LX/93H;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-direct {v4, v5}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 1433677
    iput-object v2, v4, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 1433678
    move-object v4, v4

    .line 1433679
    invoke-virtual {v4}, LX/1Uo;->u()LX/1af;

    move-result-object v4

    .line 1433680
    iget-object v5, v0, LX/93H;->l:Lcom/facebook/drawee/view/DraweeView;

    invoke-virtual {v5, v4}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 1433681
    iget-object v4, v0, LX/93H;->o:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1Ad;

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->getCallerContext()Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v4

    invoke-virtual {v4, v1}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v4

    invoke-virtual {v4}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v4

    .line 1433682
    iget-object v5, v0, LX/93H;->l:Lcom/facebook/drawee/view/DraweeView;

    invoke-virtual {v5, v4}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1433683
    iget-object v4, v0, LX/93H;->l:Lcom/facebook/drawee/view/DraweeView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/facebook/drawee/view/DraweeView;->setVisibility(I)V

    goto :goto_6
.end method
