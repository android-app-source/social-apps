.class public interface abstract LX/97f;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/97e;


# annotations
.annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
    from = "SuggestEditsField"
    processor = "com.facebook.dracula.transformer.Transformer"
.end annotation


# virtual methods
.method public abstract j()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCrowdsourcedField"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract k()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOptions"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method
