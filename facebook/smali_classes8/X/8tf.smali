.class public final LX/8tf;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/widget/popover/PopoverFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/popover/PopoverFragment;)V
    .locals 0

    .prologue
    .line 1413733
    iput-object p1, p0, LX/8tf;->a:Lcom/facebook/widget/popover/PopoverFragment;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 6

    .prologue
    .line 1413734
    iget-object v0, p0, LX/8tf;->a:Lcom/facebook/widget/popover/PopoverFragment;

    invoke-virtual {v0}, Lcom/facebook/widget/popover/PopoverFragment;->q()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1413735
    :cond_0
    :goto_0
    return-void

    .line 1413736
    :cond_1
    iget-object v0, p0, LX/8tf;->a:Lcom/facebook/widget/popover/PopoverFragment;

    iget-object v0, v0, Lcom/facebook/widget/popover/PopoverFragment;->r:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1413737
    iget-object v0, p0, LX/8tf;->a:Lcom/facebook/widget/popover/PopoverFragment;

    invoke-virtual {v0}, Lcom/facebook/widget/popover/PopoverFragment;->u()LX/31M;

    move-result-object v0

    invoke-virtual {v0}, LX/31M;->isYAxis()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/8tf;->a:Lcom/facebook/widget/popover/PopoverFragment;

    iget-object v0, v0, Lcom/facebook/widget/popover/PopoverFragment;->n:LX/8tn;

    invoke-virtual {v0}, LX/8tn;->getHeight()I

    move-result v0

    .line 1413738
    :goto_1
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v2

    .line 1413739
    const-wide v4, 0x3fee666660000000L    # 0.949999988079071

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    int-to-double v0, v0

    div-double v0, v2, v0

    const-wide v2, 0x3fa9999a00000000L    # 0.050000011920928955

    mul-double/2addr v0, v2

    add-double/2addr v0, v4

    double-to-float v0, v0

    .line 1413740
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 1413741
    iget-object v1, p0, LX/8tf;->a:Lcom/facebook/widget/popover/PopoverFragment;

    iget-object v1, v1, Lcom/facebook/widget/popover/PopoverFragment;->r:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setScaleX(F)V

    .line 1413742
    iget-object v1, p0, LX/8tf;->a:Lcom/facebook/widget/popover/PopoverFragment;

    iget-object v1, v1, Lcom/facebook/widget/popover/PopoverFragment;->r:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setScaleY(F)V

    goto :goto_0

    .line 1413743
    :cond_2
    iget-object v0, p0, LX/8tf;->a:Lcom/facebook/widget/popover/PopoverFragment;

    iget-object v0, v0, Lcom/facebook/widget/popover/PopoverFragment;->n:LX/8tn;

    invoke-virtual {v0}, LX/8tn;->getWidth()I

    move-result v0

    goto :goto_1
.end method
