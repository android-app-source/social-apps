.class public LX/9lb;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/1Ck;

.field public final c:LX/0TD;

.field public final d:LX/03V;

.field public final e:LX/0SI;

.field public final f:LX/0tX;

.field public final g:LX/9lc;

.field public h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/8wj;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/A7t;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1532972
    const-class v0, LX/9lb;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/9lb;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/1Ck;LX/0TD;LX/03V;LX/0SI;LX/0tX;LX/0Or;LX/9lc;LX/A7t;)V
    .locals 0
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ck;",
            "LX/0TD;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0SI;",
            "LX/0tX;",
            "LX/0Or",
            "<",
            "LX/8wj;",
            ">;",
            "LX/9lc;",
            "LX/A7t;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1533014
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1533015
    iput-object p1, p0, LX/9lb;->b:LX/1Ck;

    .line 1533016
    iput-object p2, p0, LX/9lb;->c:LX/0TD;

    .line 1533017
    iput-object p3, p0, LX/9lb;->d:LX/03V;

    .line 1533018
    iput-object p7, p0, LX/9lb;->g:LX/9lc;

    .line 1533019
    iput-object p4, p0, LX/9lb;->e:LX/0SI;

    .line 1533020
    iput-object p6, p0, LX/9lb;->h:LX/0Or;

    .line 1533021
    iput-object p5, p0, LX/9lb;->f:LX/0tX;

    .line 1533022
    iput-object p8, p0, LX/9lb;->i:LX/A7t;

    .line 1533023
    return-void
.end method

.method public static a(LX/9lb;Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;Z)V
    .locals 5

    .prologue
    .line 1533024
    if-eqz p1, :cond_1

    .line 1533025
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v0, v0

    .line 1533026
    iget-object v1, p1, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->q:Lcom/facebook/rapidreporting/ui/DialogStateData;

    move-object v1, v1

    .line 1533027
    invoke-virtual {v1}, Lcom/facebook/rapidreporting/ui/DialogStateData;->l()Ljava/lang/String;

    move-result-object v1

    .line 1533028
    iget-object v2, p1, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->q:Lcom/facebook/rapidreporting/ui/DialogStateData;

    move-object v2, v2

    .line 1533029
    iget-object v3, v2, Lcom/facebook/rapidreporting/ui/DialogStateData;->a:Lcom/facebook/rapidreporting/ui/DialogConfig;

    .line 1533030
    iget-object v2, v3, Lcom/facebook/rapidreporting/ui/DialogConfig;->c:LX/9lZ;

    move-object v3, v2

    .line 1533031
    move-object v2, v3

    .line 1533032
    iget-object v3, p1, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->q:Lcom/facebook/rapidreporting/ui/DialogStateData;

    move-object v3, v3

    .line 1533033
    iget-object v4, v3, Lcom/facebook/rapidreporting/ui/DialogStateData;->c:Lcom/facebook/graphql/executor/GraphQLResult;

    if-nez v4, :cond_2

    .line 1533034
    const/4 v4, 0x0

    .line 1533035
    :goto_0
    move v3, v4

    .line 1533036
    invoke-virtual {p1}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1533037
    if-eqz v3, :cond_0

    .line 1533038
    new-instance v3, LX/A7p;

    const-string v4, "RR_FEEDBACK"

    const-string p1, "FB4A_RR_DIALOG"

    invoke-direct {v3, v4, p1}, LX/A7p;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1533039
    iput-object v1, v3, LX/A7p;->c:Ljava/lang/String;

    .line 1533040
    move-object v3, v3

    .line 1533041
    iget-object v4, p0, LX/9lb;->i:LX/A7t;

    invoke-virtual {v4, v3, v0}, LX/A7t;->a(LX/A7p;LX/0gc;)V

    .line 1533042
    :cond_0
    if-eqz v2, :cond_1

    .line 1533043
    if-eqz p2, :cond_1

    .line 1533044
    invoke-interface {v2}, LX/9lZ;->a()V

    .line 1533045
    :cond_1
    return-void

    :cond_2
    iget-object v4, v3, Lcom/facebook/rapidreporting/ui/DialogStateData;->c:Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1533046
    iget-object v3, v4, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v4, v3

    .line 1533047
    check-cast v4, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingTagsQueryModel;

    invoke-virtual {v4}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingTagsQueryModel;->a()Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel;->m()Z

    move-result v4

    goto :goto_0
.end method

.method public static a(Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;Lcom/facebook/rapidreporting/ui/DialogStateData;LX/9mN;LX/0gc;)V
    .locals 2
    .param p0    # Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1533052
    if-eqz p0, :cond_0

    .line 1533053
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1533054
    :cond_0
    iput-object p2, p1, Lcom/facebook/rapidreporting/ui/DialogStateData;->b:LX/9mN;

    .line 1533055
    invoke-static {}, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->a()Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;

    move-result-object v0

    .line 1533056
    iput-object p1, v0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->q:Lcom/facebook/rapidreporting/ui/DialogStateData;

    .line 1533057
    move-object v0, v0

    .line 1533058
    sget-object v1, LX/9lb;->a:Ljava/lang/String;

    invoke-virtual {v0, p3, v1}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1533059
    return-void
.end method

.method public static a$redex0(LX/9lb;Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;LX/9lY;)V
    .locals 1

    .prologue
    .line 1533048
    iget-object v0, p0, LX/9lb;->b:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 1533049
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->c(Z)V

    .line 1533050
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p2}, LX/9lb;->b(Landroid/content/Context;LX/9lY;)V

    .line 1533051
    return-void
.end method

.method public static a$redex0(LX/9lb;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1533000
    new-instance v0, LX/9ld;

    invoke-direct {v0}, LX/9ld;-><init>()V

    move-object v0, v0

    .line 1533001
    new-instance v1, LX/4JT;

    invoke-direct {v1}, LX/4JT;-><init>()V

    .line 1533002
    const-string v2, "rapid_reporting_prompt_node_token"

    invoke-virtual {v1, v2, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1533003
    move-object v1, v1

    .line 1533004
    iget-object v2, p0, LX/9lb;->e:LX/0SI;

    invoke-interface {v2}, LX/0SI;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v2

    .line 1533005
    iget-object p1, v2, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v2, p1

    .line 1533006
    const-string p1, "actor_id"

    invoke-virtual {v1, p1, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1533007
    move-object v1, v1

    .line 1533008
    const-string v2, "input"

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1533009
    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 1533010
    iget-object v1, p0, LX/9lb;->f:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1533011
    return-void
.end method

.method public static b(LX/0QB;)LX/9lb;
    .locals 9

    .prologue
    .line 1533012
    new-instance v0, LX/9lb;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v1

    check-cast v1, LX/1Ck;

    invoke-static {p0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v2

    check-cast v2, LX/0TD;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {p0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v4

    check-cast v4, LX/0SI;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    const/16 v6, 0x1769

    invoke-static {p0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {p0}, LX/9lc;->b(LX/0QB;)LX/9lc;

    move-result-object v7

    check-cast v7, LX/9lc;

    invoke-static {p0}, LX/A7t;->b(LX/0QB;)LX/A7t;

    move-result-object v8

    check-cast v8, LX/A7t;

    invoke-direct/range {v0 .. v8}, LX/9lb;-><init>(LX/1Ck;LX/0TD;LX/03V;LX/0SI;LX/0tX;LX/0Or;LX/9lc;LX/A7t;)V

    .line 1533013
    return-object v0
.end method

.method public static b(Landroid/content/Context;LX/9lY;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1532996
    sget-object v0, LX/9lY;->NETWORK_ERROR:LX/9lY;

    if-ne p1, v0, :cond_0

    .line 1532997
    const v0, 0x7f0824b5

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1532998
    :goto_0
    return-void

    .line 1532999
    :cond_0
    const v0, 0x7f0824b4

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public static e(LX/9lb;Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;)V
    .locals 3

    .prologue
    .line 1532990
    iget-object v0, p0, LX/9lb;->b:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1532991
    iget-object v0, p1, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->q:Lcom/facebook/rapidreporting/ui/DialogStateData;

    move-object v0, v0

    .line 1532992
    sget-object v1, LX/9mN;->THANK_YOU:LX/9mN;

    .line 1532993
    iget-object v2, p1, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v2, v2

    .line 1532994
    invoke-static {p1, v0, v1, v2}, LX/9lb;->a(Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;Lcom/facebook/rapidreporting/ui/DialogStateData;LX/9mN;LX/0gc;)V

    .line 1532995
    :cond_0
    return-void
.end method

.method public static g(LX/9lb;Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;)V
    .locals 1

    .prologue
    .line 1532987
    iget-object v0, p0, LX/9lb;->b:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 1532988
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, LX/9lb;->a(LX/9lb;Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;Z)V

    .line 1532989
    return-void
.end method


# virtual methods
.method public final a(LX/0gc;Lcom/facebook/rapidreporting/ui/DialogConfig;)V
    .locals 3

    .prologue
    .line 1532982
    iget-object v0, p0, LX/9lb;->g:LX/9lc;

    .line 1532983
    iget-object v1, p2, Lcom/facebook/rapidreporting/ui/DialogConfig;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1532984
    invoke-virtual {v0, v1}, LX/9lc;->a(Ljava/lang/String;)V

    .line 1532985
    const/4 v0, 0x0

    new-instance v1, Lcom/facebook/rapidreporting/ui/DialogStateData;

    invoke-direct {v1, p2}, Lcom/facebook/rapidreporting/ui/DialogStateData;-><init>(Lcom/facebook/rapidreporting/ui/DialogConfig;)V

    sget-object v2, LX/9mN;->FEEDBACK:LX/9mN;

    invoke-static {v0, v1, v2, p1}, LX/9lb;->a(Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;Lcom/facebook/rapidreporting/ui/DialogStateData;LX/9mN;LX/0gc;)V

    .line 1532986
    return-void
.end method

.method public final a(LX/0gc;Lcom/facebook/rapidreporting/ui/DialogStateData;)V
    .locals 2

    .prologue
    .line 1532973
    iget-object v0, p0, LX/9lb;->g:LX/9lc;

    invoke-virtual {p2}, Lcom/facebook/rapidreporting/ui/DialogStateData;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9lc;->a(Ljava/lang/String;)V

    .line 1532974
    iget-object v0, p2, Lcom/facebook/rapidreporting/ui/DialogStateData;->e:Ljava/lang/String;

    move-object v0, v0

    .line 1532975
    if-nez v0, :cond_0

    .line 1532976
    invoke-virtual {p2}, Lcom/facebook/rapidreporting/ui/DialogStateData;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/9lb;->a$redex0(LX/9lb;Ljava/lang/String;)V

    .line 1532977
    :cond_0
    invoke-static {}, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->a()Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;

    move-result-object v0

    .line 1532978
    iput-object p2, v0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->q:Lcom/facebook/rapidreporting/ui/DialogStateData;

    .line 1532979
    move-object v0, v0

    .line 1532980
    sget-object v1, LX/9lb;->a:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1532981
    return-void
.end method
