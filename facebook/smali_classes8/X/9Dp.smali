.class public LX/9Dp;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1456233
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1456234
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1456235
    iput-object v0, p0, LX/9Dp;->a:LX/0Ot;

    .line 1456236
    return-void
.end method

.method public static a(LX/0QB;)LX/9Dp;
    .locals 4

    .prologue
    .line 1456237
    const-class v1, LX/9Dp;

    monitor-enter v1

    .line 1456238
    :try_start_0
    sget-object v0, LX/9Dp;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1456239
    sput-object v2, LX/9Dp;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1456240
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1456241
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1456242
    new-instance v3, LX/9Dp;

    invoke-direct {v3}, LX/9Dp;-><init>()V

    .line 1456243
    const/16 p0, 0x17d

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 1456244
    iput-object p0, v3, LX/9Dp;->a:LX/0Ot;

    .line 1456245
    move-object v0, v3

    .line 1456246
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1456247
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/9Dp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1456248
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1456249
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static d(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLPage;
    .locals 1

    .prologue
    .line 1456250
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->R()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1456251
    invoke-static {p1}, LX/9Dp;->d(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    .line 1456252
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/9Dp;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v0

    .line 1456253
    iget-object p0, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, p0

    .line 1456254
    goto :goto_0
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLFeedback;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1456255
    invoke-static {p1}, LX/9Dp;->d(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    .line 1456256
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/9Dp;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final c(Lcom/facebook/graphql/model/GraphQLFeedback;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1456257
    invoke-static {p1}, LX/9Dp;->d(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    .line 1456258
    if-nez v0, :cond_0

    .line 1456259
    iget-object v0, p0, LX/9Dp;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1456260
    :goto_0
    return-object v0

    .line 1456261
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 1456262
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
