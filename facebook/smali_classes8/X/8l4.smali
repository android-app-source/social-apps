.class public LX/8l4;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "Lcom/facebook/stickers/keyboard/StickerPackPageView;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1397374
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1397375
    return-void
.end method


# virtual methods
.method public final a(LX/4m4;LX/7kc;)Lcom/facebook/stickers/keyboard/StickerPackPageView;
    .locals 9

    .prologue
    .line 1397376
    new-instance v0, Lcom/facebook/stickers/keyboard/StickerPackPageView;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, Lcom/facebook/stickers/client/StickerDownloadManager;->a(LX/0QB;)Lcom/facebook/stickers/client/StickerDownloadManager;

    move-result-object v2

    check-cast v2, Lcom/facebook/stickers/client/StickerDownloadManager;

    invoke-static {p0}, LX/8jY;->b(LX/0QB;)LX/8jY;

    move-result-object v3

    check-cast v3, LX/8jY;

    invoke-static {p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v4

    check-cast v4, LX/0Xl;

    const-class v5, LX/8mq;

    invoke-interface {p0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/8mq;

    const-class v6, LX/8ms;

    invoke-interface {p0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/8ms;

    move-object v7, p1

    move-object v8, p2

    invoke-direct/range {v0 .. v8}, Lcom/facebook/stickers/keyboard/StickerPackPageView;-><init>(Landroid/content/Context;Lcom/facebook/stickers/client/StickerDownloadManager;LX/8jY;LX/0Xl;LX/8mq;LX/8ms;LX/4m4;LX/7kc;)V

    .line 1397377
    return-object v0
.end method
