.class public LX/90x;
.super LX/90i;
.source ""


# static fields
.field public static final a:Ljava/lang/Object;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Landroid/view/LayoutInflater;

.field private final d:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;

.field private final e:LX/93P;

.field private f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/92e;",
            ">;"
        }
    .end annotation
.end field

.field public g:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1429838
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/90x;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/LayoutInflater;Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;)V
    .locals 1

    .prologue
    .line 1429832
    invoke-direct {p0}, LX/90i;-><init>()V

    .line 1429833
    iput-object p1, p0, LX/90x;->b:Landroid/content/Context;

    .line 1429834
    iput-object p3, p0, LX/90x;->d:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;

    .line 1429835
    iput-object p2, p0, LX/90x;->c:Landroid/view/LayoutInflater;

    .line 1429836
    new-instance v0, LX/93P;

    invoke-direct {v0}, LX/93P;-><init>()V

    iput-object v0, p0, LX/90x;->e:LX/93P;

    .line 1429837
    return-void
.end method

.method private a(I)Z
    .locals 1

    .prologue
    .line 1429831
    iget-boolean v0, p0, LX/90x;->g:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1429827
    sget-object v0, LX/90v;->a:[I

    invoke-static {}, LX/90w;->values()[LX/90w;

    move-result-object v1

    aget-object v1, v1, p1

    invoke-virtual {v1}, LX/90w;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1429828
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown viewType while creating in "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v2, LX/90x;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1429829
    :pswitch_0
    iget-object v0, p0, LX/90x;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f030338

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1429830
    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, LX/90x;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f03033f

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 3

    .prologue
    .line 1429816
    sget-object v0, LX/90v;->a:[I

    invoke-static {}, LX/90w;->values()[LX/90w;

    move-result-object v1

    aget-object v1, v1, p4

    invoke-virtual {v1}, LX/90w;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1429817
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown viewType while binding in "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v2, LX/90x;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1429818
    :pswitch_0
    iget-object v0, p0, LX/90x;->e:LX/93P;

    check-cast p3, Lcom/facebook/composer/minutiae/view/MinutiaeVerbRowView;

    check-cast p2, LX/92e;

    iget-object v1, p0, LX/90x;->d:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;

    .line 1429819
    iget-object v2, p2, LX/92e;->a:LX/5LG;

    move-object v2, v2

    .line 1429820
    invoke-interface {v2}, LX/5LG;->n()Ljava/lang/String;

    move-result-object v2

    .line 1429821
    invoke-virtual {p3, v2}, Lcom/facebook/composer/minutiae/view/MinutiaeVerbRowView;->setText(Ljava/lang/CharSequence;)V

    .line 1429822
    move-object v2, p3

    .line 1429823
    iget-object p0, p2, LX/92e;->a:LX/5LG;

    move-object p0, p0

    .line 1429824
    invoke-interface {p0}, LX/5LG;->A()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;->a()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    invoke-virtual {v2, p0}, Lcom/facebook/composer/minutiae/view/MinutiaeVerbRowView;->a(Landroid/net/Uri;)Lcom/facebook/composer/minutiae/view/MinutiaeVerbRowView;

    .line 1429825
    new-instance v2, LX/93O;

    invoke-direct {v2, v0, v1, p2}, LX/93O;-><init>(LX/93P;Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableVerbFragment;LX/92e;)V

    invoke-virtual {p3, v2}, Lcom/facebook/composer/minutiae/view/MinutiaeVerbRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1429826
    :pswitch_1
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/92e;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1429813
    iput-object p1, p0, LX/90x;->f:LX/0Px;

    .line 1429814
    const v0, 0x2fac5917

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1429815
    return-void
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 1429808
    iget-boolean v0, p0, LX/90x;->g:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 1429809
    :goto_0
    iget-object v1, p0, LX/90x;->f:LX/0Px;

    if-eqz v1, :cond_0

    .line 1429810
    iget-object v1, p0, LX/90x;->f:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    add-int/2addr v0, v1

    .line 1429811
    :cond_0
    return v0

    .line 1429812
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1429803
    invoke-direct {p0, p1}, LX/90x;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1429804
    sget-object v0, LX/90x;->a:Ljava/lang/Object;

    .line 1429805
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/90x;->f:LX/0Px;

    .line 1429806
    iget-boolean v1, p0, LX/90x;->g:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 1429807
    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1429798
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1429800
    invoke-direct {p0, p1}, LX/90x;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1429801
    sget-object v0, LX/90w;->LISTENING:LX/90w;

    invoke-virtual {v0}, LX/90w;->ordinal()I

    move-result v0

    .line 1429802
    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/90w;->ACTIVITY:LX/90w;

    invoke-virtual {v0}, LX/90w;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 1429799
    invoke-static {}, LX/90w;->values()[LX/90w;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
