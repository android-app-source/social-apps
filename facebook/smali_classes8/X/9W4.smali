.class public final LX/9W4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:LX/9WC;


# direct methods
.method public constructor <init>(LX/9WC;)V
    .locals 0

    .prologue
    .line 1500549
    iput-object p1, p0, LX/9W4;->a:LX/9WC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1500550
    instance-of v0, p2, Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    if-nez v0, :cond_0

    .line 1500551
    :goto_0
    return-void

    .line 1500552
    :cond_0
    check-cast p2, Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setChecked(Z)V

    .line 1500553
    iget-object v0, p0, LX/9W4;->a:LX/9WC;

    const/4 v1, 0x0

    .line 1500554
    iput-boolean v1, v0, LX/9WC;->l:Z

    .line 1500555
    check-cast p1, Landroid/widget/ListView;

    invoke-virtual {p1}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    sub-int v0, p3, v0

    .line 1500556
    iget-object v1, p0, LX/9W4;->a:LX/9WC;

    iget-object v2, p0, LX/9W4;->a:LX/9WC;

    iget-object v2, v2, LX/9WC;->i:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Vx;

    .line 1500557
    iput-object v0, v1, LX/9WC;->k:LX/9Vx;

    .line 1500558
    iget-object v0, p0, LX/9W4;->a:LX/9WC;

    iget-object v0, v0, LX/9WC;->r:LX/9Ve;

    iget-object v1, p0, LX/9W4;->a:LX/9WC;

    iget-object v1, v1, LX/9WC;->k:LX/9Vx;

    .line 1500559
    iget-object v2, v1, LX/9Vx;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1500560
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "negativefeedback_answer_question"

    invoke-direct {v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1500561
    invoke-static {v0, v2, v1}, LX/9Ve;->a(LX/9Ve;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 1500562
    iget-object v0, p0, LX/9W4;->a:LX/9WC;

    new-instance v1, LX/9Vx;

    iget-object v2, p0, LX/9W4;->a:LX/9WC;

    iget-object v2, v2, LX/9WC;->k:LX/9Vx;

    invoke-direct {v1, v2}, LX/9Vx;-><init>(LX/9Vx;)V

    invoke-static {v0, v1}, LX/9WC;->a$redex0(LX/9WC;LX/9Vx;)V

    goto :goto_0
.end method
