.class public LX/8tH;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/8tH;


# instance fields
.field private final a:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1412757
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1412758
    iput-object p1, p0, LX/8tH;->a:Landroid/content/res/Resources;

    .line 1412759
    return-void
.end method

.method public static a(LX/0QB;)LX/8tH;
    .locals 4

    .prologue
    .line 1412760
    sget-object v0, LX/8tH;->b:LX/8tH;

    if-nez v0, :cond_1

    .line 1412761
    const-class v1, LX/8tH;

    monitor-enter v1

    .line 1412762
    :try_start_0
    sget-object v0, LX/8tH;->b:LX/8tH;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1412763
    if-eqz v2, :cond_0

    .line 1412764
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1412765
    new-instance p0, LX/8tH;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-direct {p0, v3}, LX/8tH;-><init>(Landroid/content/res/Resources;)V

    .line 1412766
    move-object v0, p0

    .line 1412767
    sput-object v0, LX/8tH;->b:LX/8tH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1412768
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1412769
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1412770
    :cond_1
    sget-object v0, LX/8tH;->b:LX/8tH;

    return-object v0

    .line 1412771
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1412772
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(I)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1412773
    int-to-double v0, p1

    const-wide v2, 0x40c47ae147ae147bL    # 10485.76

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    .line 1412774
    const-string v0, "%01.2f"

    new-array v1, v5, [Ljava/lang/Object;

    int-to-float v2, p1

    const/high16 v3, 0x49800000    # 1048576.0f

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1412775
    iget-object v1, p0, LX/8tH;->a:Landroid/content/res/Resources;

    const v2, 0x7f082380

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1412776
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(J)Ljava/lang/String;
    .locals 11

    .prologue
    const-wide/32 v6, 0xea60

    const/4 v10, 0x2

    const-wide/32 v4, 0x36ee80

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1412777
    div-long v0, p1, v4

    long-to-int v0, v0

    .line 1412778
    rem-long v2, p1, v4

    div-long/2addr v2, v6

    long-to-int v1, v2

    .line 1412779
    rem-long v2, p1, v4

    rem-long/2addr v2, v6

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    long-to-int v2, v2

    .line 1412780
    if-lez v0, :cond_0

    iget-object v3, p0, LX/8tH;->a:Landroid/content/res/Resources;

    const v4, 0x7f08237e

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const-string v6, "%d"

    new-array v7, v9, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v7, v8

    invoke-static {v6, v7}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v8

    const-string v0, "%02d"

    new-array v6, v9, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v8

    invoke-static {v0, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v9

    const-string v0, "%02d"

    new-array v1, v9, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v8

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v10

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/8tH;->a:Landroid/content/res/Resources;

    const v3, 0x7f08237f

    new-array v4, v10, [Ljava/lang/Object;

    const-string v5, "%d"

    new-array v6, v9, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v8

    invoke-static {v5, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v8

    const-string v1, "%02d"

    new-array v5, v9, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v8

    invoke-static {v1, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v9

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
