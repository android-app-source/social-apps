.class public final LX/8uj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/8QL;

.field public b:Landroid/text/TextPaint;

.field public c:Landroid/content/res/Resources;

.field private d:Ljava/lang/Integer;

.field public e:Landroid/graphics/drawable/Drawable;

.field private f:Ljava/lang/Integer;

.field private g:Ljava/lang/Boolean;

.field public h:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1415617
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1415618
    return-void
.end method


# virtual methods
.method public final a(I)LX/8uj;
    .locals 1

    .prologue
    .line 1415619
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/8uj;->d:Ljava/lang/Integer;

    .line 1415620
    return-object p0
.end method

.method public final a(Z)LX/8uj;
    .locals 1

    .prologue
    .line 1415621
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/8uj;->g:Ljava/lang/Boolean;

    .line 1415622
    return-object p0
.end method

.method public final a(Landroid/content/Context;)LX/8ul;
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 1415623
    iget-object v0, p0, LX/8uj;->a:LX/8QL;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1415624
    iget-object v0, p0, LX/8uj;->b:Landroid/text/TextPaint;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1415625
    iget-object v0, p0, LX/8uj;->c:Landroid/content/res/Resources;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1415626
    iget-object v0, p0, LX/8uj;->d:Ljava/lang/Integer;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1415627
    iget-object v0, p0, LX/8uj;->a:LX/8QL;

    invoke-virtual {v0}, LX/8QL;->d()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, LX/8uj;->c:Landroid/content/res/Resources;

    const v1, 0x7f0b01b1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v9

    .line 1415628
    :goto_0
    iget-object v0, p0, LX/8uj;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/8uj;->f:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 1415629
    :goto_1
    iget-object v0, p0, LX/8uj;->c:Landroid/content/res/Resources;

    const v1, 0x7f0b01b0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v8

    .line 1415630
    iget-object v0, p0, LX/8uj;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/8uj;->g:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    .line 1415631
    :goto_2
    new-instance v0, LX/8ul;

    iget-object v1, p0, LX/8uj;->a:LX/8QL;

    iget-object v2, p0, LX/8uj;->b:Landroid/text/TextPaint;

    iget-object v3, p0, LX/8uj;->c:Landroid/content/res/Resources;

    iget-object v4, p0, LX/8uj;->e:Landroid/graphics/drawable/Drawable;

    iget-object v5, p0, LX/8uj;->h:Ljava/lang/Integer;

    iget-object v6, p0, LX/8uj;->d:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-direct/range {v0 .. v11}, LX/8ul;-><init>(LX/8QL;Landroid/text/TextPaint;Landroid/content/res/Resources;Landroid/graphics/drawable/Drawable;Ljava/lang/Integer;IIIIZB)V

    .line 1415632
    invoke-virtual {v0, p1}, LX/8uk;->a(Landroid/content/Context;)V

    .line 1415633
    return-object v0

    :cond_0
    move v9, v11

    .line 1415634
    goto :goto_0

    .line 1415635
    :cond_1
    iget-object v0, p0, LX/8uj;->c:Landroid/content/res/Resources;

    const v1, 0x7f0b01af

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v7

    goto :goto_1

    :cond_2
    move v10, v11

    .line 1415636
    goto :goto_2
.end method

.method public final b(I)LX/8uj;
    .locals 1

    .prologue
    .line 1415637
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/8uj;->f:Ljava/lang/Integer;

    .line 1415638
    return-object p0
.end method
