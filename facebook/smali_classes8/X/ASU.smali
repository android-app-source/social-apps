.class public LX/ASU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3l6;


# instance fields
.field public final a:Landroid/content/res/Resources;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3lZ;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/33A;

.field public final d:Lcom/facebook/privacy/PrivacyOperationsClient;

.field private final e:LX/0SG;

.field private f:Landroid/content/Context;

.field public final g:LX/339;

.field public final h:LX/0Uh;

.field public final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/HqU;

.field public k:Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;

.field public l:Z

.field public m:LX/Hrb;

.field private n:LX/2EJ;

.field private final o:Landroid/content/DialogInterface$OnClickListener;

.field private final p:Landroid/content/DialogInterface$OnClickListener;

.field private final q:Landroid/content/DialogInterface$OnCancelListener;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0Ot;LX/33A;Lcom/facebook/privacy/PrivacyOperationsClient;LX/0SG;Landroid/content/Context;LX/339;LX/0Or;LX/0Uh;LX/HqU;LX/Hrb;)V
    .locals 1
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/privacy/gating/IsDefaultPostPrivacyEnabled;
        .end annotation
    .end param
    .param p10    # LX/HqU;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p11    # LX/Hrb;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "LX/0Ot",
            "<",
            "LX/3lZ;",
            ">;",
            "LX/33A;",
            "Lcom/facebook/privacy/PrivacyOperationsClient;",
            "LX/0SG;",
            "Landroid/content/Context;",
            "LX/339;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/composer/tip/StickyGuardrailInterstitialController$DataProvider;",
            "Lcom/facebook/composer/tip/StickyGuardrailInterstitialController$StickyGuardrailCallback;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1674003
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1674004
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/ASU;->l:Z

    .line 1674005
    new-instance v0, LX/ASQ;

    invoke-direct {v0, p0}, LX/ASQ;-><init>(LX/ASU;)V

    iput-object v0, p0, LX/ASU;->o:Landroid/content/DialogInterface$OnClickListener;

    .line 1674006
    new-instance v0, LX/ASR;

    invoke-direct {v0, p0}, LX/ASR;-><init>(LX/ASU;)V

    iput-object v0, p0, LX/ASU;->p:Landroid/content/DialogInterface$OnClickListener;

    .line 1674007
    new-instance v0, LX/ASS;

    invoke-direct {v0, p0}, LX/ASS;-><init>(LX/ASU;)V

    iput-object v0, p0, LX/ASU;->q:Landroid/content/DialogInterface$OnCancelListener;

    .line 1674008
    iput-object p1, p0, LX/ASU;->a:Landroid/content/res/Resources;

    .line 1674009
    iput-object p2, p0, LX/ASU;->b:LX/0Ot;

    .line 1674010
    iput-object p3, p0, LX/ASU;->c:LX/33A;

    .line 1674011
    iput-object p4, p0, LX/ASU;->d:Lcom/facebook/privacy/PrivacyOperationsClient;

    .line 1674012
    iput-object p5, p0, LX/ASU;->e:LX/0SG;

    .line 1674013
    iput-object p6, p0, LX/ASU;->f:Landroid/content/Context;

    .line 1674014
    iput-object p7, p0, LX/ASU;->g:LX/339;

    .line 1674015
    iput-object p8, p0, LX/ASU;->i:LX/0Or;

    .line 1674016
    iput-object p9, p0, LX/ASU;->h:LX/0Uh;

    .line 1674017
    iput-object p10, p0, LX/ASU;->j:LX/HqU;

    .line 1674018
    iput-object p11, p0, LX/ASU;->m:LX/Hrb;

    .line 1674019
    return-void
.end method

.method public static a$redex0(LX/ASU;LX/5no;)V
    .locals 14

    .prologue
    const/4 v1, 0x0

    .line 1673995
    iget-object v0, p0, LX/ASU;->d:Lcom/facebook/privacy/PrivacyOperationsClient;

    iget-object v2, p0, LX/ASU;->e:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, p0, LX/ASU;->k:Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;

    invoke-virtual {v3}, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;->d()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v4, p0, LX/ASU;->k:Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;

    invoke-virtual {v4}, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;->b()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, LX/ASU;->k:Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;

    invoke-virtual {v4}, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;->b()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->c()Ljava/lang/String;

    move-result-object v4

    :goto_0
    iget-object v5, p0, LX/ASU;->k:Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;

    invoke-virtual {v5}, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;->c()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v5

    if-eqz v5, :cond_1

    iget-object v1, p0, LX/ASU;->k:Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;

    invoke-virtual {v1}, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;->c()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->c()Ljava/lang/String;

    move-result-object v5

    :goto_1
    move-object v1, p1

    .line 1673996
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1673997
    new-instance v12, Landroid/os/Bundle;

    invoke-direct {v12}, Landroid/os/Bundle;-><init>()V

    .line 1673998
    const-string v13, "params"

    new-instance v6, Lcom/facebook/privacy/protocol/ReportStickyGuardrailActionParams;

    move-object v7, v1

    move-object v8, v2

    move-object v9, v3

    move-object v10, v4

    move-object v11, v5

    invoke-direct/range {v6 .. v11}, Lcom/facebook/privacy/protocol/ReportStickyGuardrailActionParams;-><init>(LX/5no;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v12, v13, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1673999
    iget-object v6, v0, Lcom/facebook/privacy/PrivacyOperationsClient;->c:LX/0aG;

    const-string v7, "report_sticky_guardrail_action"

    sget-object v9, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    sget-object v10, Lcom/facebook/privacy/PrivacyOperationsClient;->a:Lcom/facebook/common/callercontext/CallerContext;

    const v11, 0x1f7115f1

    move-object v8, v12

    invoke-static/range {v6 .. v11}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v6

    .line 1674000
    invoke-static {v0, v6}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(Lcom/facebook/privacy/PrivacyOperationsClient;LX/1MF;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1674001
    return-void

    :cond_0
    move-object v4, v1

    .line 1674002
    goto :goto_0

    :cond_1
    move-object v5, v1

    goto :goto_1
.end method

.method public static b(LX/ASU;)Z
    .locals 2

    .prologue
    .line 1673991
    iget-object v0, p0, LX/ASU;->k:Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ASU;->k:Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;

    .line 1673992
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;->b()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;->b()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    invoke-static {v0}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->ONLY_ME:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 1673993
    :cond_0
    iget-object v0, p0, LX/ASU;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3lZ;

    invoke-virtual {v0}, LX/3lZ;->a()Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;

    move-result-object v0

    goto :goto_0

    .line 1673994
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static m(LX/ASU;)V
    .locals 1

    .prologue
    .line 1673989
    iget-object v0, p0, LX/ASU;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3lZ;

    invoke-virtual {v0}, LX/3lZ;->a()Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;

    move-result-object v0

    iput-object v0, p0, LX/ASU;->k:Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;

    .line 1673990
    return-void
.end method

.method public static n(LX/ASU;)V
    .locals 3

    .prologue
    .line 1673983
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/ASU;->l:Z

    .line 1673984
    iget-object v0, p0, LX/ASU;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3lZ;

    invoke-virtual {v0}, LX/3lZ;->b()V

    .line 1673985
    invoke-static {p0}, LX/ASU;->m(LX/ASU;)V

    .line 1673986
    iget-object v0, p0, LX/ASU;->m:LX/Hrb;

    .line 1673987
    iget-object v1, v0, LX/Hrb;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v1, v1, Lcom/facebook/composer/activity/ComposerFragment;->k:LX/ASX;

    sget-object v2, LX/ASW;->INTERSTITIAL_NUX:LX/ASW;

    const/4 p0, 0x1

    invoke-virtual {v1, v2, p0}, LX/ASX;->a(LX/ASW;Z)Z

    .line 1673988
    return-void
.end method


# virtual methods
.method public final a(LX/5L2;)LX/ASZ;
    .locals 5

    .prologue
    .line 1673942
    sget-object v0, LX/AST;->a:[I

    invoke-virtual {p1}, LX/5L2;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1673943
    :cond_0
    sget-object v0, LX/ASZ;->NONE:LX/ASZ;

    :goto_0
    return-object v0

    .line 1673944
    :pswitch_0
    const/4 v0, 0x0

    .line 1673945
    invoke-virtual {p0}, LX/ASU;->f()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, LX/ASU;->g:LX/339;

    .line 1673946
    iget-boolean v2, v1, LX/339;->g:Z

    move v1, v2

    .line 1673947
    if-nez v1, :cond_3

    iget-object v1, p0, LX/ASU;->h:LX/0Uh;

    sget v2, LX/7l1;->g:I

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v2, 0x0

    .line 1673948
    iget-object v1, p0, LX/ASU;->i:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03R;

    invoke-virtual {v1, v2}, LX/03R;->asBoolean(Z)Z

    move-result v1

    if-nez v1, :cond_5

    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 1673949
    if-eqz v1, :cond_3

    .line 1673950
    iget-object v1, p0, LX/ASU;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3lZ;

    invoke-virtual {v1}, LX/3lZ;->a()Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;

    move-result-object v1

    .line 1673951
    invoke-virtual {v1}, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;->mSuggestedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-nez v2, :cond_6

    .line 1673952
    :cond_1
    const/4 v1, 0x0

    .line 1673953
    :goto_2
    move v1, v1

    .line 1673954
    if-eqz v1, :cond_3

    const/4 v1, 0x0

    .line 1673955
    iget-object v2, p0, LX/ASU;->j:LX/HqU;

    .line 1673956
    iget-object v3, v2, LX/HqU;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v3, v3, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v3}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getComposerType()LX/2rt;

    move-result-object v3

    move-object v2, v3

    .line 1673957
    sget-object v3, LX/2rt;->STATUS:LX/2rt;

    if-eq v2, v3, :cond_a

    sget-object v3, LX/2rt;->SHARE:LX/2rt;

    if-ne v2, v3, :cond_2

    invoke-static {p0}, LX/ASU;->b(LX/ASU;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 1673958
    :cond_2
    :goto_3
    move v1, v1

    .line 1673959
    if-eqz v1, :cond_3

    .line 1673960
    iget-object v1, p0, LX/ASU;->c:LX/33A;

    invoke-virtual {v1}, LX/33A;->a()Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;

    move-result-object v1

    .line 1673961
    if-eqz v1, :cond_b

    iget-boolean v1, v1, Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;->mEligible:Z

    if-eqz v1, :cond_b

    const/4 v1, 0x1

    :goto_4
    move v1, v1

    .line 1673962
    if-nez v1, :cond_3

    const/4 v0, 0x1

    .line 1673963
    :cond_3
    if-eqz v0, :cond_4

    .line 1673964
    invoke-static {p0}, LX/ASU;->m(LX/ASU;)V

    .line 1673965
    :cond_4
    move v0, v0

    .line 1673966
    if-eqz v0, :cond_0

    .line 1673967
    sget-object v0, LX/ASZ;->SHOW:LX/ASZ;

    goto/16 :goto_0

    :cond_5
    move v1, v2

    goto :goto_1

    :cond_6
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1673968
    iget-object v4, p0, LX/ASU;->j:LX/HqU;

    invoke-virtual {v4}, LX/HqU;->b()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v4

    if-eqz v4, :cond_9

    iget-object v4, p0, LX/ASU;->j:LX/HqU;

    invoke-virtual {v4}, LX/HqU;->b()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v4, :cond_9

    iget-object v4, p0, LX/ASU;->j:LX/HqU;

    invoke-virtual {v4}, LX/HqU;->b()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-object v4, v4, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    if-eqz v4, :cond_9

    iget-object v4, p0, LX/ASU;->j:LX/HqU;

    invoke-virtual {v4}, LX/HqU;->b()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1673969
    iget-object p1, v4, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v4, p1

    .line 1673970
    if-eqz v4, :cond_9

    iget-object v4, p0, LX/ASU;->j:LX/HqU;

    invoke-virtual {v4}, LX/HqU;->b()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-object v4, v4, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-boolean v4, v4, Lcom/facebook/privacy/model/PrivacyOptionsResult;->isResultFromServer:Z

    if-nez v4, :cond_7

    iget-object v4, p0, LX/ASU;->j:LX/HqU;

    .line 1673971
    iget-object p1, v4, LX/HqU;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object p1, p1, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-interface {p1}, LX/5Qu;->c()Z

    move-result p1

    move v4, p1

    .line 1673972
    if-eqz v4, :cond_9

    .line 1673973
    :cond_7
    iget-object v4, p0, LX/ASU;->j:LX/HqU;

    invoke-virtual {v4}, LX/HqU;->b()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1673974
    iget-object p1, v4, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v4, p1

    .line 1673975
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;->b()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->c()Ljava/lang/String;

    move-result-object p1

    invoke-static {v4, p1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/CharSequence;

    invoke-virtual {v1}, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;->b()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->d()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v4, v3

    invoke-virtual {v1}, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;->c()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->d()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v4, v2

    invoke-static {v4}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 1673976
    :goto_5
    move v1, v2

    .line 1673977
    goto/16 :goto_2

    .line 1673978
    :cond_8
    iget-object v2, p0, LX/ASU;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3lZ;

    invoke-virtual {v2}, LX/3lZ;->b()V

    .line 1673979
    invoke-static {p0}, LX/ASU;->m(LX/ASU;)V

    :cond_9
    move v2, v3

    .line 1673980
    goto :goto_5

    :cond_a
    iget-object v2, p0, LX/ASU;->j:LX/HqU;

    .line 1673981
    iget-object v3, v2, LX/HqU;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v3, v3, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v3}, LX/0it;->d()Z

    move-result v3

    move v2, v3

    .line 1673982
    if-nez v2, :cond_2

    const/4 v1, 0x1

    goto/16 :goto_3

    :cond_b
    const/4 v1, 0x0

    goto/16 :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 1673917
    invoke-virtual {p0}, LX/ASU;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1673918
    iget-object v0, p0, LX/ASU;->n:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->hide()V

    .line 1673919
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1673936
    iget-object v0, p0, LX/ASU;->n:LX/2EJ;

    if-eqz v0, :cond_0

    .line 1673937
    iget-object v0, p0, LX/ASU;->n:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->dismiss()V

    .line 1673938
    :cond_0
    iput-object v1, p0, LX/ASU;->f:Landroid/content/Context;

    .line 1673939
    iput-object v1, p0, LX/ASU;->j:LX/HqU;

    .line 1673940
    iput-object v1, p0, LX/ASU;->m:LX/Hrb;

    .line 1673941
    return-void
.end method

.method public final e()V
    .locals 7

    .prologue
    .line 1673923
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/ASU;->l:Z

    .line 1673924
    new-instance v0, LX/0ju;

    iget-object v1, p0, LX/ASU;->f:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 1673925
    invoke-static {p0}, LX/ASU;->b(LX/ASU;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1673926
    iget-object v1, p0, LX/ASU;->a:Landroid/content/res/Resources;

    const v2, 0x7f08147f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1673927
    :goto_0
    move-object v1, v1

    .line 1673928
    invoke-virtual {v0, v1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    const/16 v6, 0x21

    const/4 v5, 0x1

    .line 1673929
    invoke-static {p0}, LX/ASU;->b(LX/ASU;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1673930
    new-instance v1, LX/47x;

    iget-object v2, p0, LX/ASU;->a:Landroid/content/res/Resources;

    invoke-direct {v1, v2}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    const v2, 0x7f081480

    invoke-virtual {v1, v2}, LX/47x;->a(I)LX/47x;

    move-result-object v1

    const-string v2, "\n\n"

    invoke-virtual {v1, v2}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    move-result-object v1

    const v2, 0x7f081482

    invoke-virtual {v1, v2}, LX/47x;->a(I)LX/47x;

    move-result-object v1

    const-string v2, "%1$s"

    iget-object v3, p0, LX/ASU;->k:Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;

    invoke-virtual {v3}, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;->b()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->d()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Landroid/text/style/StyleSpan;

    invoke-direct {v4, v5}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v1, v2, v3, v4, v6}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    move-result-object v1

    const-string v2, "%2$s"

    iget-object v3, p0, LX/ASU;->k:Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;

    invoke-virtual {v3}, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;->c()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->d()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Landroid/text/style/StyleSpan;

    invoke-direct {v4, v5}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v1, v2, v3, v4, v6}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    move-result-object v1

    invoke-virtual {v1}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v1

    .line 1673931
    :goto_1
    move-object v1, v1

    .line 1673932
    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    iget-object v1, p0, LX/ASU;->a:Landroid/content/res/Resources;

    const v2, 0x7f081484

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/ASU;->p:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    iget-object v1, p0, LX/ASU;->a:Landroid/content/res/Resources;

    const v2, 0x7f081483

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/ASU;->o:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    iget-object v1, p0, LX/ASU;->q:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v1}, LX/0ju;->a(Landroid/content/DialogInterface$OnCancelListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    iput-object v0, p0, LX/ASU;->n:LX/2EJ;

    .line 1673933
    iget-object v0, p0, LX/ASU;->n:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 1673934
    sget-object v0, LX/5no;->EXPOSED:LX/5no;

    invoke-static {p0, v0}, LX/ASU;->a$redex0(LX/ASU;LX/5no;)V

    .line 1673935
    return-void

    :cond_0
    iget-object v1, p0, LX/ASU;->a:Landroid/content/res/Resources;

    const v2, 0x7f08147e

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, LX/ASU;->k:Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;

    invoke-virtual {v5}, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;->c()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->d()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    :cond_1
    new-instance v1, LX/47x;

    iget-object v2, p0, LX/ASU;->a:Landroid/content/res/Resources;

    invoke-direct {v1, v2}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    const v2, 0x7f081480

    invoke-virtual {v1, v2}, LX/47x;->a(I)LX/47x;

    move-result-object v1

    const-string v2, "\n\n"

    invoke-virtual {v1, v2}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    move-result-object v1

    const v2, 0x7f081481

    invoke-virtual {v1, v2}, LX/47x;->a(I)LX/47x;

    move-result-object v1

    const-string v2, "%1$s"

    iget-object v3, p0, LX/ASU;->k:Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;

    invoke-virtual {v3}, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;->c()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->d()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Landroid/text/style/StyleSpan;

    invoke-direct {v4, v5}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v1, v2, v3, v4, v6}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    move-result-object v1

    const-string v2, "%2$s"

    iget-object v3, p0, LX/ASU;->k:Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;

    invoke-virtual {v3}, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;->b()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->d()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Landroid/text/style/StyleSpan;

    invoke-direct {v4, v5}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v1, v2, v3, v4, v6}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    move-result-object v1

    invoke-virtual {v1}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v1

    goto/16 :goto_1
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 1673920
    iget-object v0, p0, LX/ASU;->n:LX/2EJ;

    if-nez v0, :cond_0

    .line 1673921
    const/4 v0, 0x0

    .line 1673922
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/ASU;->n:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->isShowing()Z

    move-result v0

    goto :goto_0
.end method
