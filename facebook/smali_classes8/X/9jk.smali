.class public final LX/9jk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Rl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Rl",
        "<",
        "LX/9jm;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/9jo;

.field public final synthetic b:LX/9jn;


# direct methods
.method public constructor <init>(LX/9jn;LX/9jo;)V
    .locals 0

    .prologue
    .line 1530379
    iput-object p1, p0, LX/9jk;->b:LX/9jn;

    iput-object p2, p0, LX/9jk;->a:LX/9jo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Z
    .locals 12

    .prologue
    .line 1530380
    check-cast p1, LX/9jm;

    .line 1530381
    iget-object v0, p0, LX/9jk;->a:LX/9jo;

    iget-object v1, p1, LX/9jm;->a:LX/9jo;

    const-wide v2, 0x4092c00000000000L    # 1200.0

    const/4 v4, 0x0

    .line 1530382
    iget-object v5, v0, LX/9jo;->a:Ljava/lang/String;

    move-object v5, v5

    .line 1530383
    iget-object v6, v1, LX/9jo;->a:Ljava/lang/String;

    move-object v6, v6

    .line 1530384
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 1530385
    :cond_0
    :goto_0
    move v0, v4

    .line 1530386
    return v0

    .line 1530387
    :cond_1
    iget-object v5, v0, LX/9jo;->e:Ljava/lang/String;

    move-object v5, v5

    .line 1530388
    iget-object v6, v1, LX/9jo;->e:Ljava/lang/String;

    move-object v6, v6

    .line 1530389
    invoke-static {v5, v6}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1530390
    iget-object v5, v0, LX/9jo;->f:Ljava/lang/String;

    move-object v5, v5

    .line 1530391
    iget-object v6, v1, LX/9jo;->f:Ljava/lang/String;

    move-object v6, v6

    .line 1530392
    invoke-static {v5, v6}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1530393
    iget-object v4, v0, LX/9jo;->b:Landroid/location/Location;

    move-object v4, v4

    .line 1530394
    iget-object v5, v1, LX/9jo;->b:Landroid/location/Location;

    move-object v5, v5

    .line 1530395
    if-nez v4, :cond_3

    if-nez v5, :cond_3

    .line 1530396
    const-wide/16 v10, 0x0

    .line 1530397
    :goto_1
    move-wide v8, v10

    .line 1530398
    cmpg-double v8, v8, v2

    if-gtz v8, :cond_2

    const/4 v8, 0x1

    :goto_2
    move v4, v8

    .line 1530399
    goto :goto_0

    :cond_2
    const/4 v8, 0x0

    goto :goto_2

    .line 1530400
    :cond_3
    if-eqz v4, :cond_4

    if-nez v5, :cond_5

    .line 1530401
    :cond_4
    const-wide/high16 v10, 0x7ff0000000000000L    # Double.POSITIVE_INFINITY

    goto :goto_1

    .line 1530402
    :cond_5
    invoke-virtual {v4, v5}, Landroid/location/Location;->distanceTo(Landroid/location/Location;)F

    move-result v10

    float-to-double v10, v10

    goto :goto_1
.end method
