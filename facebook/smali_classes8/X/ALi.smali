.class public final LX/ALi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8CF;


# instance fields
.field public final synthetic a:Lcom/facebook/backstage/camera/PreviewView;


# direct methods
.method public constructor <init>(Lcom/facebook/backstage/camera/PreviewView;)V
    .locals 0

    .prologue
    .line 1665229
    iput-object p1, p0, LX/ALi;->a:Lcom/facebook/backstage/camera/PreviewView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 1665225
    iget-object v0, p0, LX/ALi;->a:Lcom/facebook/backstage/camera/PreviewView;

    iget-object v0, v0, Lcom/facebook/backstage/camera/PreviewView;->o:Lcom/facebook/messaging/doodle/ColourIndicator;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/doodle/ColourIndicator;->setColour(I)V

    .line 1665226
    iget-object v0, p0, LX/ALi;->a:Lcom/facebook/backstage/camera/PreviewView;

    iget-object v0, v0, Lcom/facebook/backstage/camera/PreviewView;->o:Lcom/facebook/messaging/doodle/ColourIndicator;

    invoke-virtual {v0}, Lcom/facebook/messaging/doodle/ColourIndicator;->a()V

    .line 1665227
    iget-object v0, p0, LX/ALi;->a:Lcom/facebook/backstage/camera/PreviewView;

    iget-object v0, v0, Lcom/facebook/backstage/camera/PreviewView;->m:Lcom/facebook/backstage/ui/ScalingToggleableEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/backstage/ui/ScalingToggleableEditText;->setTextColor(I)V

    .line 1665228
    return-void
.end method

.method public final a(IFFF)V
    .locals 1

    .prologue
    .line 1665222
    iget-object v0, p0, LX/ALi;->a:Lcom/facebook/backstage/camera/PreviewView;

    iget-object v0, v0, Lcom/facebook/backstage/camera/PreviewView;->o:Lcom/facebook/messaging/doodle/ColourIndicator;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/facebook/messaging/doodle/ColourIndicator;->a(IFFF)V

    .line 1665223
    iget-object v0, p0, LX/ALi;->a:Lcom/facebook/backstage/camera/PreviewView;

    iget-object v0, v0, Lcom/facebook/backstage/camera/PreviewView;->m:Lcom/facebook/backstage/ui/ScalingToggleableEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/backstage/ui/ScalingToggleableEditText;->setTextColor(I)V

    .line 1665224
    return-void
.end method
