.class public final LX/9dS;
.super Landroid/animation/AnimatorListenerAdapter;
.source ""


# instance fields
.field public final synthetic a:LX/9dK;


# direct methods
.method public constructor <init>(LX/9dK;)V
    .locals 0

    .prologue
    .line 1518143
    iput-object p1, p0, LX/9dS;->a:LX/9dK;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 8

    .prologue
    .line 1518144
    iget-object v0, p0, LX/9dS;->a:LX/9dK;

    iget-object v0, v0, LX/9dK;->i:LX/9cz;

    invoke-virtual {v0}, LX/9cz;->a()V

    .line 1518145
    iget-object v0, p0, LX/9dS;->a:LX/9dK;

    .line 1518146
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    invoke-static {v0, v4, v5}, LX/9dK;->a(LX/9dK;FF)Landroid/animation/Animator;

    move-result-object v4

    const-wide/16 v6, 0xc8

    invoke-virtual {v4, v6, v7}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    move-result-object v4

    .line 1518147
    new-instance v5, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v4, v5}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1518148
    move-object v0, v4

    .line 1518149
    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 1518150
    iget-object v0, p0, LX/9dS;->a:LX/9dK;

    iget-object v0, v0, LX/9dK;->h:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 1518151
    iget-object v0, p0, LX/9dS;->a:LX/9dK;

    const-wide/16 v2, 0x1f4

    .line 1518152
    iget-object v1, v0, LX/9dK;->b:Landroid/view/ViewGroup;

    new-instance v4, Lcom/facebook/photos/creativeediting/swipeable/composer/nux/SwipeableAnimatingNuxViewController$6;

    invoke-direct {v4, v0}, Lcom/facebook/photos/creativeediting/swipeable/composer/nux/SwipeableAnimatingNuxViewController$6;-><init>(LX/9dK;)V

    invoke-virtual {v1, v4, v2, v3}, Landroid/view/ViewGroup;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1518153
    :cond_0
    return-void
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 1518154
    iget-object v0, p0, LX/9dS;->a:LX/9dK;

    iget-object v0, v0, LX/9dK;->i:LX/9cz;

    iget-object v1, p0, LX/9dS;->a:LX/9dK;

    invoke-virtual {v1}, LX/9dK;->f()F

    move-result v1

    .line 1518155
    iget-object p0, v0, LX/9cz;->a:LX/9d5;

    iget-object p0, p0, LX/9d5;->L:LX/9dI;

    .line 1518156
    invoke-static {p0, v1}, LX/9dI;->c$redex0(LX/9dI;F)V

    .line 1518157
    sget-object v0, LX/5jJ;->SWIPING:LX/5jJ;

    invoke-static {p0, v0}, LX/9dI;->a$redex0(LX/9dI;LX/5jJ;)V

    .line 1518158
    return-void
.end method
