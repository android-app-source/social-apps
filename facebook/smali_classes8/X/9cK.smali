.class public LX/9cK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9cG;


# static fields
.field private static final b:Landroid/graphics/RectF;


# instance fields
.field public final a:LX/9cL;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 1516479
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v1, v1, v2, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    sput-object v0, LX/9cK;->b:Landroid/graphics/RectF;

    return-void
.end method

.method public constructor <init>(LX/9cL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1516476
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1516477
    iput-object p1, p0, LX/9cK;->a:LX/9cL;

    .line 1516478
    return-void
.end method

.method public static a(LX/0QB;)LX/9cK;
    .locals 1

    .prologue
    .line 1516475
    invoke-static {p0}, LX/9cK;->b(LX/0QB;)LX/9cK;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/9cK;
    .locals 4

    .prologue
    .line 1516470
    new-instance v1, LX/9cK;

    .line 1516471
    new-instance v3, LX/9cL;

    invoke-static {p0}, LX/8GZ;->a(LX/0QB;)LX/8GZ;

    move-result-object v0

    check-cast v0, LX/8GZ;

    const-class v2, LX/9cH;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/9cH;

    invoke-direct {v3, v0, v2}, LX/9cL;-><init>(LX/8GZ;LX/9cH;)V

    .line 1516472
    move-object v0, v3

    .line 1516473
    check-cast v0, LX/9cL;

    invoke-direct {v1, v0}, LX/9cK;-><init>(LX/9cL;)V

    .line 1516474
    return-object v1
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1516468
    iget-object v0, p0, LX/9cK;->a:LX/9cL;

    invoke-virtual {v0}, LX/9cL;->a()V

    .line 1516469
    return-void
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 1516461
    iget-object v0, p0, LX/9cK;->a:LX/9cL;

    .line 1516462
    iget-object v1, v0, LX/9cL;->b:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    .line 1516463
    iget-object p0, v1, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->k:LX/5i8;

    move-object v1, p0

    .line 1516464
    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1516465
    iget-object v1, v0, LX/9cL;->b:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    iget-object p0, v0, LX/9cL;->h:Landroid/graphics/Rect;

    invoke-virtual {v1, p1, p0}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    .line 1516466
    return-void

    .line 1516467
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final varargs a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;IIILandroid/view/View;Z[LX/9cJ;)V
    .locals 13

    .prologue
    .line 1516426
    invoke-static/range {p7 .. p7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1516427
    if-eqz p6, :cond_0

    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v1

    invoke-static {v1}, LX/63w;->c(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Landroid/graphics/RectF;

    move-result-object v7

    .line 1516428
    :goto_0
    new-instance v8, LX/0Pz;

    invoke-direct {v8}, LX/0Pz;-><init>()V

    .line 1516429
    const/4 v5, 0x0

    .line 1516430
    const/4 v4, 0x0

    .line 1516431
    const/4 v2, 0x0

    .line 1516432
    const/4 v3, 0x0

    .line 1516433
    move-object/from16 v0, p7

    array-length v9, v0

    const/4 v1, 0x0

    move v6, v1

    :goto_1
    if-ge v6, v9, :cond_1

    aget-object v1, p7, v6

    .line 1516434
    sget-object v10, LX/9cI;->a:[I

    invoke-virtual {v1}, LX/9cJ;->ordinal()I

    move-result v1

    aget v1, v10, v1

    packed-switch v1, :pswitch_data_0

    move v1, v3

    move v3, v4

    move v4, v5

    .line 1516435
    :goto_2
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    move v5, v4

    move v4, v3

    move v3, v1

    goto :goto_1

    .line 1516436
    :cond_0
    sget-object v7, LX/9cK;->b:Landroid/graphics/RectF;

    goto :goto_0

    .line 1516437
    :pswitch_0
    const/4 v1, 0x1

    move v12, v3

    move v3, v4

    move v4, v1

    move v1, v12

    .line 1516438
    goto :goto_2

    .line 1516439
    :pswitch_1
    const/4 v1, 0x1

    move v4, v5

    move v12, v1

    move v1, v3

    move v3, v12

    .line 1516440
    goto :goto_2

    .line 1516441
    :pswitch_2
    const/4 v1, 0x1

    move v2, v1

    move v1, v3

    move v3, v4

    move v4, v5

    .line 1516442
    goto :goto_2

    .line 1516443
    :pswitch_3
    const/4 v1, 0x1

    move v3, v4

    move v4, v5

    goto :goto_2

    .line 1516444
    :cond_1
    invoke-static {p1}, LX/5iB;->b(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    .line 1516445
    const/4 v1, 0x0

    move v6, v1

    :goto_3
    if-ge v6, v10, :cond_5

    invoke-virtual {v9, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/362;

    .line 1516446
    instance-of v11, v1, Lcom/facebook/photos/creativeediting/model/StickerParams;

    if-eqz v11, :cond_3

    if-eqz v5, :cond_3

    .line 1516447
    check-cast v1, LX/5i8;

    invoke-virtual {v8, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1516448
    :cond_2
    :goto_4
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_3

    .line 1516449
    :cond_3
    instance-of v11, v1, Lcom/facebook/photos/creativeediting/model/TextParams;

    if-eqz v11, :cond_4

    if-eqz v4, :cond_4

    .line 1516450
    check-cast v1, LX/5i8;

    invoke-virtual {v8, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_4

    .line 1516451
    :cond_4
    instance-of v11, v1, Lcom/facebook/photos/creativeediting/model/DoodleParams;

    if-eqz v11, :cond_2

    if-eqz v2, :cond_2

    .line 1516452
    check-cast v1, LX/5i8;

    invoke-virtual {v8, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_4

    .line 1516453
    :cond_5
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 1516454
    const/4 v1, 0x0

    .line 1516455
    if-eqz v3, :cond_9

    .line 1516456
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFrameOverlayItems()LX/0Px;

    move-result-object v3

    .line 1516457
    :goto_5
    if-eqz v2, :cond_6

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_7

    :cond_6
    if-eqz v3, :cond_8

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_8

    .line 1516458
    :cond_7
    iget-object v1, p0, LX/9cK;->a:LX/9cL;

    move v4, p2

    move/from16 v5, p3

    move/from16 v6, p4

    move-object/from16 v8, p5

    invoke-virtual/range {v1 .. v8}, LX/9cL;->a(LX/0Px;LX/0Px;IIILandroid/graphics/RectF;Landroid/view/View;)V

    .line 1516459
    iget-object v1, p0, LX/9cK;->a:LX/9cL;

    invoke-virtual {v1}, LX/9cL;->a()V

    .line 1516460
    :cond_8
    return-void

    :cond_9
    move-object v3, v1

    goto :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 1516415
    iget-object v0, p0, LX/9cK;->a:LX/9cL;

    .line 1516416
    iget-object p0, v0, LX/9cL;->b:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    invoke-virtual {p0, p1}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->a(Landroid/graphics/drawable/Drawable;)Z

    move-result p0

    move v0, p0

    .line 1516417
    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1516424
    iget-object v0, p0, LX/9cK;->a:LX/9cL;

    invoke-virtual {v0}, LX/9cL;->b()V

    .line 1516425
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 1516422
    invoke-virtual {p0}, LX/9cK;->b()V

    .line 1516423
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1516418
    iget-object v0, p0, LX/9cK;->a:LX/9cL;

    .line 1516419
    invoke-virtual {v0}, LX/9cL;->b()V

    .line 1516420
    iget-object p0, v0, LX/9cL;->b:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->j()V

    .line 1516421
    return-void
.end method
