.class public final LX/AHs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageThreadSeenMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/AHu;


# direct methods
.method public constructor <init>(LX/AHu;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1656656
    iput-object p1, p0, LX/AHs;->b:LX/AHu;

    iput-object p2, p0, LX/AHs;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1656657
    sget-object v0, LX/AHu;->a:Ljava/lang/String;

    const-string v1, "markThreadSeenV1 Failed to mark thread %s as seen."

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/AHs;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1656658
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1656659
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1656660
    if-nez p1, :cond_0

    .line 1656661
    :goto_0
    return-void

    .line 1656662
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1656663
    check-cast v0, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageThreadSeenMutationModel;

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageThreadSeenMutationModel;->a()Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageThreadSeenMutationModel$ThreadModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBBackstageMutationFragmentsModels$FBBackstageThreadSeenMutationModel$ThreadModel;->j()Ljava/lang/String;

    goto :goto_0
.end method
