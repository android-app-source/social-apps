.class public LX/92S;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Jw;


# instance fields
.field private final a:LX/92o;

.field public final b:LX/03V;

.field public final c:LX/919;


# direct methods
.method public constructor <init>(LX/92o;LX/03V;LX/919;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1432298
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1432299
    iput-object p1, p0, LX/92S;->a:LX/92o;

    .line 1432300
    iput-object p2, p0, LX/92S;->b:LX/03V;

    .line 1432301
    iput-object p3, p0, LX/92S;->c:LX/919;

    .line 1432302
    return-void
.end method


# virtual methods
.method public final a(LX/2Jy;)Z
    .locals 3

    .prologue
    .line 1432294
    invoke-virtual {p1}, LX/2Jy;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1432295
    iget-object v0, p0, LX/92S;->a:LX/92o;

    new-instance v1, LX/92R;

    invoke-direct {v1, p0}, LX/92R;-><init>(LX/92S;)V

    .line 1432296
    iget-object v2, v0, LX/92o;->d:LX/92N;

    const/4 p0, 0x0

    invoke-static {v0, p0}, LX/92o;->b(LX/92o;Z)LX/0w7;

    move-result-object p0

    invoke-virtual {v2, p0}, LX/92N;->a(LX/0w7;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance p0, LX/92l;

    invoke-direct {p0, v0, v1}, LX/92l;-><init>(LX/92o;LX/0Vd;)V

    iget-object p1, v0, LX/92o;->g:Ljava/util/concurrent/Executor;

    invoke-static {v2, p0, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1432297
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
