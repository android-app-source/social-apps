.class public LX/9g3;
.super LX/9g2;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<GRAPHQ",
        "L_RESULT_TYPE:Ljava/lang/Object;",
        "PAGE_RESU",
        "LT_TYPE:Ljava/lang/Object;",
        ">",
        "LX/9g2",
        "<",
        "LX/9gr;",
        "TPAGE_RESU",
        "LT_TYPE;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/9gr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/9gr",
            "<TGRAPHQ",
            "L_RESULT_TYPE;",
            "+",
            "Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;",
            "TPAGE_RESU",
            "LT_TYPE;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Sh;

.field private final d:Ljava/util/concurrent/ExecutorService;

.field private final e:LX/1My;

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/9fz",
            "<TPAGE_RESU",
            "LT_TYPE;",
            ">;>;"
        }
    .end annotation
.end field

.field private g:LX/9g1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/9g3",
            "<TGRAPHQ",
            "L_RESULT_TYPE;",
            "TPAGE_RESU",
            "LT_TYPE;",
            ">.NewPageFetchingCallback;"
        }
    .end annotation
.end field

.field private h:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TGRAPHQ",
            "L_RESULT_TYPE;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1522961
    const-class v0, LX/9g3;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/9g3;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/9gr;LX/0Sh;Ljava/util/concurrent/ExecutorService;LX/1My;LX/03V;)V
    .locals 1
    .param p1    # LX/9gr;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1522962
    invoke-direct {p0, p1, p2, p5}, LX/9g2;-><init>(LX/9g8;LX/0Sh;LX/03V;)V

    .line 1522963
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/9g3;->f:Ljava/util/List;

    .line 1522964
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/9g3;->i:LX/0am;

    .line 1522965
    iput-object p1, p0, LX/9g3;->b:LX/9gr;

    .line 1522966
    iput-object p2, p0, LX/9g3;->c:LX/0Sh;

    .line 1522967
    iput-object p3, p0, LX/9g3;->d:Ljava/util/concurrent/ExecutorService;

    .line 1522968
    iput-object p4, p0, LX/9g3;->e:LX/1My;

    .line 1522969
    return-void
.end method

.method public static a$redex0(LX/9g3;ILcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TGRAPHQ",
            "L_RESULT_TYPE;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1522970
    iget-object v0, p0, LX/9g3;->c:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1522971
    iget-object v0, p0, LX/9g3;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 1522972
    iget-object v0, p0, LX/9g3;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1522973
    :cond_0
    iget-object v0, p0, LX/9g3;->f:Ljava/util/List;

    iget-object v1, p0, LX/9g3;->b:LX/9gr;

    invoke-virtual {v1, p2}, LX/9gr;->b(Lcom/facebook/graphql/executor/GraphQLResult;)LX/9fz;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1522974
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v1

    .line 1522975
    iget-object v0, p0, LX/9g3;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9fz;

    .line 1522976
    iget-object v0, v0, LX/9fz;->a:LX/0Px;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 1522977
    :cond_1
    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    move-object v2, v0

    .line 1522978
    iget-object v0, p0, LX/9g3;->i:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, LX/9g2;->d()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1522979
    :cond_2
    invoke-virtual {p0, v2}, LX/9g2;->a(LX/0Px;)V

    .line 1522980
    :goto_1
    return-void

    .line 1522981
    :cond_3
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_5

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 1522982
    check-cast v0, LX/16i;

    invoke-interface {v0}, LX/16i;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, LX/9g3;->i:LX/0am;

    invoke-virtual {v4}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v4

    invoke-static {v0, v4}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1522983
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/9g3;->i:LX/0am;

    .line 1522984
    invoke-virtual {p0, v2}, LX/9g2;->a(LX/0Px;)V

    goto :goto_1

    .line 1522985
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1522986
    :cond_5
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    const/16 v1, 0x80

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v1, p0, LX/9g3;->i:LX/0am;

    invoke-virtual {p0, v0, v1}, LX/9g2;->a(ILX/0am;)V

    goto :goto_1
.end method

.method private f()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1522987
    iget-object v0, p0, LX/9g3;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1522988
    const/4 v0, 0x0

    .line 1522989
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/9g3;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    invoke-static {v0}, LX/0RZ;->f(Ljava/util/Iterator;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9fz;

    iget-object v0, v0, LX/9fz;->b:Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    goto :goto_0
.end method

.method public static h(LX/9g3;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1522990
    iget-object v0, p0, LX/9g3;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 1522991
    iget-object v0, p0, LX/9g3;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1522992
    iput-object v2, p0, LX/9g3;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1522993
    :cond_0
    iget-object v0, p0, LX/9g3;->g:LX/9g1;

    if-eqz v0, :cond_1

    .line 1522994
    iget-object v0, p0, LX/9g3;->g:LX/9g1;

    invoke-virtual {v0}, LX/0Vd;->dispose()V

    .line 1522995
    iput-object v2, p0, LX/9g3;->g:LX/9g1;

    .line 1522996
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(ILX/0am;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1522997
    iget-object v0, p0, LX/9g3;->c:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1522998
    iget-object v0, p0, LX/9g2;->e:LX/9g6;

    move-object v0, v0

    .line 1522999
    sget-object v3, LX/9g6;->CLOSED:LX/9g6;

    if-eq v0, v3, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Can not fetchMore() on closed media fetcher"

    invoke-static {v0, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1523000
    invoke-virtual {p0}, LX/9g2;->d()Z

    move-result v0

    const-string v3, "Requesting more media than we can provide"

    invoke-static {v0, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1523001
    iget-object v0, p0, LX/9g2;->e:LX/9g6;

    move-object v0, v0

    .line 1523002
    sget-object v3, LX/9g6;->LOADING:LX/9g6;

    if-ne v0, v3, :cond_1

    .line 1523003
    sget-object v0, LX/9g3;->a:Ljava/lang/String;

    const-string v1, "fetchMore() called while loading"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1523004
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 1523005
    goto :goto_0

    .line 1523006
    :cond_1
    iget-object v0, p0, LX/9g3;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    if-nez v0, :cond_3

    move v0, v1

    :goto_2
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1523007
    iget-object v0, p0, LX/9g3;->g:LX/9g1;

    if-nez v0, :cond_2

    move v2, v1

    :cond_2
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 1523008
    sget-object v0, LX/9g6;->LOADING:LX/9g6;

    invoke-virtual {p0, v0}, LX/9g2;->a(LX/9g6;)V

    .line 1523009
    iput-object p2, p0, LX/9g3;->i:LX/0am;

    .line 1523010
    invoke-direct {p0}, LX/9g3;->f()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    move-result-object v0

    .line 1523011
    if-nez v0, :cond_4

    const/4 v0, 0x0

    .line 1523012
    :goto_3
    iget-object v2, p0, LX/9g3;->b:LX/9gr;

    invoke-virtual {v2, p1, v0}, LX/9gr;->b(ILjava/lang/String;)LX/0zO;

    move-result-object v0

    sget-object v2, LX/0zS;->a:LX/0zS;

    invoke-virtual {v0, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    .line 1523013
    iput-boolean v1, v0, LX/0zO;->p:Z

    .line 1523014
    move-object v0, v0

    .line 1523015
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    const-wide/16 v2, 0xb4

    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v0

    .line 1523016
    iget-object v1, p0, LX/9g3;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    .line 1523017
    new-instance v2, LX/9g0;

    invoke-direct {v2, p0, v1}, LX/9g0;-><init>(LX/9g3;I)V

    .line 1523018
    iget-object v3, p0, LX/9g3;->e:LX/1My;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, LX/9g3;->b:LX/9gr;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v2, v4}, LX/1My;->a(LX/0zO;LX/0TF;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/9g3;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1523019
    new-instance v0, LX/9g1;

    invoke-direct {v0, p0, v1}, LX/9g1;-><init>(LX/9g3;I)V

    iput-object v0, p0, LX/9g3;->g:LX/9g1;

    .line 1523020
    iget-object v0, p0, LX/9g3;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    iget-object v1, p0, LX/9g3;->g:LX/9g1;

    iget-object v2, p0, LX/9g3;->d:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_1

    :cond_3
    move v0, v2

    .line 1523021
    goto :goto_2

    .line 1523022
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1523023
    invoke-super {p0}, LX/9g2;->c()V

    .line 1523024
    invoke-static {p0}, LX/9g3;->h(LX/9g3;)V

    .line 1523025
    iget-object v0, p0, LX/9g3;->e:LX/1My;

    invoke-virtual {v0}, LX/1My;->a()V

    .line 1523026
    return-void
.end method

.method public final d()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1523027
    iget-object v1, p0, LX/9g2;->e:LX/9g6;

    move-object v1, v1

    .line 1523028
    sget-object v2, LX/9g6;->CLOSED:LX/9g6;

    if-ne v1, v2, :cond_1

    .line 1523029
    :cond_0
    :goto_0
    return v0

    .line 1523030
    :cond_1
    invoke-direct {p0}, LX/9g3;->f()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    move-result-object v1

    .line 1523031
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method
