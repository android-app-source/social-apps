.class public final LX/9Vv;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1500398
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 1500399
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1500400
    :goto_0
    return v1

    .line 1500401
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_5

    .line 1500402
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1500403
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1500404
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_0

    if-eqz v6, :cond_0

    .line 1500405
    const-string v7, "is_uf_eligible"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1500406
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v5, v0

    move v0, v2

    goto :goto_1

    .line 1500407
    :cond_1
    const-string v7, "responses"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1500408
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1500409
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->START_ARRAY:LX/15z;

    if-ne v6, v7, :cond_2

    .line 1500410
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_ARRAY:LX/15z;

    if-eq v6, v7, :cond_2

    .line 1500411
    invoke-static {p0, p1}, LX/9Vt;->b(LX/15w;LX/186;)I

    move-result v6

    .line 1500412
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1500413
    :cond_2
    invoke-static {v4, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v4

    move v4, v4

    .line 1500414
    goto :goto_1

    .line 1500415
    :cond_3
    const-string v7, "title"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1500416
    invoke-static {p0, p1}, LX/9Vu;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1500417
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1500418
    :cond_5
    const/4 v6, 0x3

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1500419
    if-eqz v0, :cond_6

    .line 1500420
    invoke-virtual {p1, v1, v5}, LX/186;->a(IZ)V

    .line 1500421
    :cond_6
    invoke-virtual {p1, v2, v4}, LX/186;->b(II)V

    .line 1500422
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1500423
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1500424
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1500425
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1500426
    if-eqz v0, :cond_0

    .line 1500427
    const-string v1, "is_uf_eligible"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1500428
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1500429
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1500430
    if-eqz v0, :cond_2

    .line 1500431
    const-string v1, "responses"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1500432
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1500433
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 1500434
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/9Vt;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1500435
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1500436
    :cond_1
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1500437
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1500438
    if-eqz v0, :cond_3

    .line 1500439
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1500440
    invoke-static {p0, v0, p2}, LX/9Vu;->a(LX/15i;ILX/0nX;)V

    .line 1500441
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1500442
    return-void
.end method
