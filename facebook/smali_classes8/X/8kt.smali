.class public final LX/8kt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/stickers/model/Sticker;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/keyboard/StickerKeyboardView;)V
    .locals 0

    .prologue
    .line 1396910
    iput-object p1, p0, LX/8kt;->a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1396911
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1396912
    check-cast p1, Ljava/util/List;

    .line 1396913
    if-nez p1, :cond_0

    .line 1396914
    :goto_0
    return-void

    .line 1396915
    :cond_0
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 1396916
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, LX/8kt;->a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    iget-object v0, v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->A:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1396917
    iget-object v0, p0, LX/8kt;->a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    iget-object v0, v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->A:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/Sticker;

    .line 1396918
    iget-object v0, v0, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1396919
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1396920
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/Sticker;

    .line 1396921
    if-eqz v0, :cond_2

    .line 1396922
    iget-object v1, v0, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1396923
    iget-object v1, p0, LX/8kt;->a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    iget-object v4, v1, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->A:Ljava/util/List;

    iget-object v1, v0, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v4, v1, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 1396924
    :cond_3
    iget-object v0, p0, LX/8kt;->a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    invoke-static {v0}, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->n(Lcom/facebook/stickers/keyboard/StickerKeyboardView;)V

    goto :goto_0
.end method
