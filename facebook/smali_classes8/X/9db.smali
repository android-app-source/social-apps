.class public LX/9db;
.super LX/4or;
.source ""


# instance fields
.field public a:LX/8G7;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1518267
    invoke-direct {p0, p1}, LX/4or;-><init>(Landroid/content/Context;)V

    .line 1518268
    const-class v0, LX/9db;

    invoke-static {v0, p0}, LX/9db;->a(Ljava/lang/Class;Landroid/preference/Preference;)V

    .line 1518269
    const/4 p1, 0x1

    const/4 v2, 0x0

    .line 1518270
    new-array v0, p1, [Ljava/lang/String;

    const-string v1, "Reset Composer Swipeable Frame NUX"

    aput-object v1, v0, v2

    .line 1518271
    const-string v1, "Composer Swipeable Frame Nux Settings"

    invoke-virtual {p0, v1}, LX/9db;->setTitle(Ljava/lang/CharSequence;)V

    .line 1518272
    const-string v1, "Configure swipeable frame nux in composer (on device only)."

    invoke-virtual {p0, v1}, LX/9db;->setSummary(Ljava/lang/CharSequence;)V

    .line 1518273
    invoke-virtual {p0, v0}, LX/9db;->setEntries([Ljava/lang/CharSequence;)V

    .line 1518274
    new-array v0, p1, [Ljava/lang/String;

    const-string v1, "composer_swipeable_frame_nux"

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, LX/9db;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 1518275
    const-string v0, "Ok"

    invoke-virtual {p0, v0}, LX/9db;->setPositiveButtonText(Ljava/lang/CharSequence;)V

    .line 1518276
    const-string v0, "Cancel"

    invoke-virtual {p0, v0}, LX/9db;->setNegativeButtonText(Ljava/lang/CharSequence;)V

    .line 1518277
    const-string v0, "SwipeableFrameNuxPreference"

    invoke-virtual {p0, v0}, LX/9db;->setKey(Ljava/lang/String;)V

    .line 1518278
    new-instance v0, LX/9da;

    invoke-direct {v0, p0}, LX/9da;-><init>(LX/9db;)V

    .line 1518279
    invoke-virtual {p0, v0}, LX/9db;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 1518280
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/preference/Preference;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/preference/Preference;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/9db;

    invoke-static {p0}, LX/8G7;->b(LX/0QB;)LX/8G7;

    move-result-object p0

    check-cast p0, LX/8G7;

    iput-object p0, p1, LX/9db;->a:LX/8G7;

    return-void
.end method
