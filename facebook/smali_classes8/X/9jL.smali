.class public final enum LX/9jL;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9jL;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9jL;

.field public static final enum AddHome:LX/9jL;

.field public static final enum AddPlace:LX/9jL;

.field public static final enum SelectAtTagRow:LX/9jL;

.field public static final enum SelectAtTagRowHeader:LX/9jL;

.field public static final enum SocialSearchAddPlaceSeekerHeader:LX/9jL;

.field public static final enum TextOnlyRow:LX/9jL;

.field public static final enum Undefined:LX/9jL;

.field public static final enum UseAsText:LX/9jL;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1529748
    new-instance v0, LX/9jL;

    const-string v1, "AddHome"

    invoke-direct {v0, v1, v3}, LX/9jL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9jL;->AddHome:LX/9jL;

    .line 1529749
    new-instance v0, LX/9jL;

    const-string v1, "AddPlace"

    invoke-direct {v0, v1, v4}, LX/9jL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9jL;->AddPlace:LX/9jL;

    .line 1529750
    new-instance v0, LX/9jL;

    const-string v1, "SelectAtTagRow"

    invoke-direct {v0, v1, v5}, LX/9jL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9jL;->SelectAtTagRow:LX/9jL;

    .line 1529751
    new-instance v0, LX/9jL;

    const-string v1, "SelectAtTagRowHeader"

    invoke-direct {v0, v1, v6}, LX/9jL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9jL;->SelectAtTagRowHeader:LX/9jL;

    .line 1529752
    new-instance v0, LX/9jL;

    const-string v1, "UseAsText"

    invoke-direct {v0, v1, v7}, LX/9jL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9jL;->UseAsText:LX/9jL;

    .line 1529753
    new-instance v0, LX/9jL;

    const-string v1, "TextOnlyRow"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/9jL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9jL;->TextOnlyRow:LX/9jL;

    .line 1529754
    new-instance v0, LX/9jL;

    const-string v1, "SocialSearchAddPlaceSeekerHeader"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/9jL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9jL;->SocialSearchAddPlaceSeekerHeader:LX/9jL;

    .line 1529755
    new-instance v0, LX/9jL;

    const-string v1, "Undefined"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/9jL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9jL;->Undefined:LX/9jL;

    .line 1529756
    const/16 v0, 0x8

    new-array v0, v0, [LX/9jL;

    sget-object v1, LX/9jL;->AddHome:LX/9jL;

    aput-object v1, v0, v3

    sget-object v1, LX/9jL;->AddPlace:LX/9jL;

    aput-object v1, v0, v4

    sget-object v1, LX/9jL;->SelectAtTagRow:LX/9jL;

    aput-object v1, v0, v5

    sget-object v1, LX/9jL;->SelectAtTagRowHeader:LX/9jL;

    aput-object v1, v0, v6

    sget-object v1, LX/9jL;->UseAsText:LX/9jL;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/9jL;->TextOnlyRow:LX/9jL;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/9jL;->SocialSearchAddPlaceSeekerHeader:LX/9jL;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/9jL;->Undefined:LX/9jL;

    aput-object v2, v0, v1

    sput-object v0, LX/9jL;->$VALUES:[LX/9jL;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1529757
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9jL;
    .locals 1

    .prologue
    .line 1529747
    const-class v0, LX/9jL;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9jL;

    return-object v0
.end method

.method public static values()[LX/9jL;
    .locals 1

    .prologue
    .line 1529746
    sget-object v0, LX/9jL;->$VALUES:[LX/9jL;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9jL;

    return-object v0
.end method
