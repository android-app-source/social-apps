.class public final LX/A1W;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 16

    .prologue
    .line 1611086
    const/4 v12, 0x0

    .line 1611087
    const/4 v11, 0x0

    .line 1611088
    const/4 v10, 0x0

    .line 1611089
    const/4 v9, 0x0

    .line 1611090
    const/4 v8, 0x0

    .line 1611091
    const/4 v7, 0x0

    .line 1611092
    const/4 v6, 0x0

    .line 1611093
    const/4 v5, 0x0

    .line 1611094
    const/4 v4, 0x0

    .line 1611095
    const/4 v3, 0x0

    .line 1611096
    const/4 v2, 0x0

    .line 1611097
    const/4 v1, 0x0

    .line 1611098
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v13, v14, :cond_1

    .line 1611099
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1611100
    const/4 v1, 0x0

    .line 1611101
    :goto_0
    return v1

    .line 1611102
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1611103
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->END_OBJECT:LX/15z;

    if-eq v13, v14, :cond_a

    .line 1611104
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v13

    .line 1611105
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1611106
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v14, v15, :cond_1

    if-eqz v13, :cond_1

    .line 1611107
    const-string v14, "bolded_title"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 1611108
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto :goto_1

    .line 1611109
    :cond_2
    const-string v14, "display_style"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 1611110
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto :goto_1

    .line 1611111
    :cond_3
    const-string v14, "fetch_size"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 1611112
    const/4 v3, 0x1

    .line 1611113
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v10

    goto :goto_1

    .line 1611114
    :cond_4
    const-string v14, "module_size"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 1611115
    const/4 v2, 0x1

    .line 1611116
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v9

    goto :goto_1

    .line 1611117
    :cond_5
    const-string v14, "source"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 1611118
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 1611119
    :cond_6
    const-string v14, "suggestion_type"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 1611120
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 1611121
    :cond_7
    const-string v14, "suggestions"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 1611122
    invoke-static/range {p0 .. p1}, LX/A1V;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 1611123
    :cond_8
    const-string v14, "title"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 1611124
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto/16 :goto_1

    .line 1611125
    :cond_9
    const-string v14, "use_for_badge_count"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 1611126
    const/4 v1, 0x1

    .line 1611127
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v4

    goto/16 :goto_1

    .line 1611128
    :cond_a
    const/16 v13, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 1611129
    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 1611130
    const/4 v12, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 1611131
    if-eqz v3, :cond_b

    .line 1611132
    const/4 v3, 0x2

    const/4 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10, v11}, LX/186;->a(III)V

    .line 1611133
    :cond_b
    if-eqz v2, :cond_c

    .line 1611134
    const/4 v2, 0x3

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9, v3}, LX/186;->a(III)V

    .line 1611135
    :cond_c
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1611136
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1611137
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1611138
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 1611139
    if-eqz v1, :cond_d

    .line 1611140
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v4}, LX/186;->a(IZ)V

    .line 1611141
    :cond_d
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1611142
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1611143
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1611144
    if-eqz v0, :cond_0

    .line 1611145
    const-string v1, "bolded_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1611146
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1611147
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1611148
    if-eqz v0, :cond_1

    .line 1611149
    const-string v1, "display_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1611150
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1611151
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1611152
    if-eqz v0, :cond_2

    .line 1611153
    const-string v1, "fetch_size"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1611154
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1611155
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1611156
    if-eqz v0, :cond_3

    .line 1611157
    const-string v1, "module_size"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1611158
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1611159
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1611160
    if-eqz v0, :cond_4

    .line 1611161
    const-string v1, "source"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1611162
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1611163
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1611164
    if-eqz v0, :cond_5

    .line 1611165
    const-string v1, "suggestion_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1611166
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1611167
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1611168
    if-eqz v0, :cond_7

    .line 1611169
    const-string v1, "suggestions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1611170
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1611171
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_6

    .line 1611172
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/A1V;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1611173
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1611174
    :cond_6
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1611175
    :cond_7
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1611176
    if-eqz v0, :cond_8

    .line 1611177
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1611178
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1611179
    :cond_8
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1611180
    if-eqz v0, :cond_9

    .line 1611181
    const-string v1, "use_for_badge_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1611182
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1611183
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1611184
    return-void
.end method
