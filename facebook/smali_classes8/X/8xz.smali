.class public LX/8xz;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0tX;

.field private final b:LX/0sa;

.field public final c:LX/1Ck;


# direct methods
.method public constructor <init>(LX/0tX;LX/0sa;LX/1Ck;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1424993
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1424994
    iput-object p1, p0, LX/8xz;->a:LX/0tX;

    .line 1424995
    iput-object p2, p0, LX/8xz;->b:LX/0sa;

    .line 1424996
    iput-object p3, p0, LX/8xz;->c:LX/1Ck;

    .line 1424997
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPage;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$DeletePlaceRecommendationFromCommentMutationModel;
    .locals 10
    .param p1    # Lcom/facebook/graphql/model/GraphQLComment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1424998
    new-instance v0, LX/5IU;

    invoke-direct {v0}, LX/5IU;-><init>()V

    .line 1424999
    invoke-static {p0}, LX/5I9;->a(Lcom/facebook/graphql/model/GraphQLPage;)Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;->a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;)Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 1425000
    iput-object v1, v0, LX/5IU;->a:LX/0Px;

    .line 1425001
    if-eqz p1, :cond_0

    .line 1425002
    invoke-static {p1}, LX/5I9;->a(Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;->a(Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;)Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;

    move-result-object v1

    .line 1425003
    iput-object v1, v0, LX/5IU;->b:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;

    .line 1425004
    :cond_0
    if-eqz p2, :cond_1

    .line 1425005
    invoke-static {p2}, LX/5I9;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel;->a(Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel;)Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel;

    move-result-object v1

    .line 1425006
    iput-object v1, v0, LX/5IU;->c:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel;

    .line 1425007
    :cond_1
    const/4 v6, 0x1

    const/4 v9, 0x0

    const/4 v4, 0x0

    .line 1425008
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1425009
    iget-object v3, v0, LX/5IU;->a:LX/0Px;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v3

    .line 1425010
    iget-object v5, v0, LX/5IU;->b:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;

    invoke-static {v2, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1425011
    iget-object v7, v0, LX/5IU;->c:Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel;

    invoke-static {v2, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1425012
    const/4 v8, 0x3

    invoke-virtual {v2, v8}, LX/186;->c(I)V

    .line 1425013
    invoke-virtual {v2, v9, v3}, LX/186;->b(II)V

    .line 1425014
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1425015
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 1425016
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1425017
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1425018
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1425019
    invoke-virtual {v3, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1425020
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1425021
    new-instance v3, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$DeletePlaceRecommendationFromCommentMutationModel;

    invoke-direct {v3, v2}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$DeletePlaceRecommendationFromCommentMutationModel;-><init>(LX/15i;)V

    .line 1425022
    move-object v0, v3

    .line 1425023
    return-object v0
.end method
