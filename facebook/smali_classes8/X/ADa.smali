.class public final LX/ADa;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Z

.field public final synthetic c:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic d:Lcom/facebook/graphql/model/GraphQLActor;

.field public final synthetic e:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public final synthetic f:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;


# direct methods
.method public constructor <init>(Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;Ljava/lang/String;ZLcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLActor;Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 0

    .prologue
    .line 1644930
    iput-object p1, p0, LX/ADa;->f:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    iput-object p2, p0, LX/ADa;->a:Ljava/lang/String;

    iput-boolean p3, p0, LX/ADa;->b:Z

    iput-object p4, p0, LX/ADa;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p5, p0, LX/ADa;->d:Lcom/facebook/graphql/model/GraphQLActor;

    iput-object p6, p0, LX/ADa;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    .line 1644931
    iget-object v0, p0, LX/ADa;->f:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    iget-object v0, v0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->g:LX/03V;

    const-string v1, "toggle_like_fail"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1644932
    instance-of v0, p1, Lcom/facebook/fbservice/service/ServiceException;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lcom/facebook/fbservice/service/ServiceException;

    .line 1644933
    iget-object v1, v0, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v1, v1

    .line 1644934
    sget-object v3, LX/1nY;->API_ERROR:LX/1nY;

    if-eq v1, v3, :cond_0

    .line 1644935
    iget-object v1, v0, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v1, v1

    .line 1644936
    sget-object v3, LX/1nY;->HTTP_400_AUTHENTICATION:LX/1nY;

    if-eq v1, v3, :cond_0

    .line 1644937
    iget-object v1, v0, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v1, v1

    .line 1644938
    sget-object v3, LX/1nY;->HTTP_400_OTHER:LX/1nY;

    if-eq v1, v3, :cond_0

    .line 1644939
    iget-object v1, v0, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v1, v1

    .line 1644940
    sget-object v3, LX/1nY;->HTTP_500_CLASS:LX/1nY;

    if-eq v1, v3, :cond_0

    .line 1644941
    iget-object v1, v0, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v1, v1

    .line 1644942
    sget-object v3, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    if-ne v1, v3, :cond_4

    :cond_0
    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 1644943
    if-nez v0, :cond_1

    .line 1644944
    iget-object v0, p0, LX/ADa;->f:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    iget-object v1, p0, LX/ADa;->a:Ljava/lang/String;

    iget-boolean v2, p0, LX/ADa;->b:Z

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    sget-object v4, LX/AkV;->FAILURE:LX/AkV;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->a$redex0(Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;Ljava/lang/String;ZLjava/lang/String;LX/AkV;)V

    .line 1644945
    :goto_1
    return-void

    .line 1644946
    :cond_1
    iget-object v0, p0, LX/ADa;->f:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    iget-object v0, v0, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/189;

    iget-object v3, p0, LX/ADa;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v4, p0, LX/ADa;->d:Lcom/facebook/graphql/model/GraphQLActor;

    iget-object v1, p0, LX/ADa;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->s_()Z

    move-result v1

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    invoke-virtual {v0, v3, v4, v1}, LX/189;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLActor;Z)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1644947
    invoke-static {v0}, LX/182;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 1644948
    if-nez v0, :cond_3

    .line 1644949
    iget-object v1, p0, LX/ADa;->f:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    iget-object v1, v1, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->g:LX/03V;

    const-string v3, "FeedUnitCacheMutator"

    const-string v4, "Feedbackable should either be a FeedUnit or it\'s root should be a FeedUnit"

    invoke-virtual {v1, v3, v4}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1644950
    :goto_3
    iget-object v1, p0, LX/ADa;->f:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    iget-object v1, v1, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->h:LX/0Zb;

    .line 1644951
    const-string v3, "newsfeed_story_like_fail"

    iget-object v4, p0, LX/ADa;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/ADa;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLFeedback;->s_()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v5

    const-string v6, "native_newsfeed"

    invoke-static {v3, v4, v5, v6}, LX/17V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v1, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1644952
    invoke-static {p1}, Lcom/facebook/fbservice/service/ServiceException;->forException(Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/ServiceException;

    move-result-object v1

    .line 1644953
    iget-object v3, p0, LX/ADa;->f:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    iget-object v3, v3, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->i:LX/1CW;

    invoke-virtual {v3, v1, v2, v2}, LX/1CW;->a(Lcom/facebook/fbservice/service/ServiceException;ZZ)Ljava/lang/String;

    move-result-object v1

    .line 1644954
    iget-object v2, p0, LX/ADa;->f:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    iget-object v3, p0, LX/ADa;->a:Ljava/lang/String;

    iget-boolean v4, p0, LX/ADa;->b:Z

    sget-object v5, LX/AkV;->FAILURE:LX/AkV;

    invoke-static {v2, v3, v4, v1, v5}, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->a$redex0(Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;Ljava/lang/String;ZLjava/lang/String;LX/AkV;)V

    .line 1644955
    iget-object v1, p0, LX/ADa;->f:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    iget-object v1, v1, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->b:LX/0jU;

    sget-object v2, LX/69s;->LIKE:LX/69s;

    invoke-virtual {v1, v0, v2, p1}, LX/0jU;->a(Lcom/facebook/graphql/model/FeedUnit;LX/69s;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1644956
    :cond_2
    const/4 v1, 0x0

    goto :goto_2

    .line 1644957
    :cond_3
    iget-object v1, p0, LX/ADa;->f:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    iget-object v1, v1, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->b:LX/0jU;

    invoke-virtual {v1, v0}, LX/0jU;->b(Lcom/facebook/graphql/model/FeedUnit;)V

    goto :goto_3

    :cond_4
    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1644958
    iget-object v0, p0, LX/ADa;->f:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    iget-object v1, p0, LX/ADa;->a:Ljava/lang/String;

    iget-boolean v2, p0, LX/ADa;->b:Z

    const/4 v3, 0x0

    sget-object v4, LX/AkV;->SUCCESS:LX/AkV;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->a$redex0(Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;Ljava/lang/String;ZLjava/lang/String;LX/AkV;)V

    .line 1644959
    return-void
.end method
