.class public LX/9kw;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/facebook/widget/CustomLinearLayout;"
    }
.end annotation


# instance fields
.field public a:Landroid/widget/EditText;

.field public b:Landroid/widget/ImageButton;

.field public c:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<+",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcom/facebook/widget/listview/EmptyListViewItem;

.field public e:LX/9km;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/9km",
            "<TT;>;"
        }
    .end annotation
.end field

.field public f:Lcom/facebook/widget/listview/BetterListView;

.field public g:Landroid/widget/LinearLayout;

.field public h:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<+",
            "Lcom/facebook/places/pickers/PlaceContentPickerView$OnContentClickListener",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field public i:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<+",
            "Landroid/text/TextWatcher;",
            ">;"
        }
    .end annotation
.end field

.field public j:Ljava/lang/CharSequence;

.field public k:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1532274
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1532275
    const/4 v2, 0x0

    .line 1532276
    const v0, 0x7f030f64

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1532277
    const v0, 0x7f0d0a6b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, LX/9kw;->a:Landroid/widget/EditText;

    .line 1532278
    const v0, 0x7f0d094d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, LX/9kw;->b:Landroid/widget/ImageButton;

    .line 1532279
    const v0, 0x7f0d2523

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, LX/9kw;->f:Lcom/facebook/widget/listview/BetterListView;

    .line 1532280
    invoke-virtual {p0}, LX/9kw;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030f65

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/EmptyListViewItem;

    iput-object v0, p0, LX/9kw;->d:Lcom/facebook/widget/listview/EmptyListViewItem;

    .line 1532281
    invoke-static {p0}, LX/9kw;->e(LX/9kw;)Landroid/widget/LinearLayout;

    move-result-object v0

    iput-object v0, p0, LX/9kw;->g:Landroid/widget/LinearLayout;

    .line 1532282
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/9kw;->c:LX/0am;

    .line 1532283
    new-instance v0, LX/9km;

    invoke-direct {v0}, LX/9km;-><init>()V

    iput-object v0, p0, LX/9kw;->e:LX/9km;

    .line 1532284
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/9kw;->h:LX/0am;

    .line 1532285
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/9kw;->i:LX/0am;

    .line 1532286
    const-string v0, ""

    iput-object v0, p0, LX/9kw;->j:Ljava/lang/CharSequence;

    .line 1532287
    iput-boolean v2, p0, LX/9kw;->k:Z

    .line 1532288
    invoke-static {p0}, LX/9kw;->f(LX/9kw;)V

    .line 1532289
    const/4 p1, 0x0

    const/4 v2, 0x0

    .line 1532290
    iget-object v0, p0, LX/9kw;->f:Lcom/facebook/widget/listview/BetterListView;

    new-instance v1, LX/9kt;

    invoke-direct {v1, p0}, LX/9kt;-><init>(LX/9kw;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1532291
    iget-object v0, p0, LX/9kw;->f:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, LX/9kw;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, p1, v2}, Lcom/facebook/widget/listview/BetterListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 1532292
    invoke-static {p0}, LX/9kw;->e(LX/9kw;)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 1532293
    iget-object v1, p0, LX/9kw;->d:Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1532294
    iget-object v1, p0, LX/9kw;->f:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1, v0, p1, v2}, Lcom/facebook/widget/listview/BetterListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 1532295
    iget-object v0, p0, LX/9kw;->f:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, LX/9kw;->e:LX/9km;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1532296
    iget-object v0, p0, LX/9kw;->b:Landroid/widget/ImageButton;

    new-instance v1, LX/9ku;

    invoke-direct {v1, p0}, LX/9ku;-><init>(LX/9kw;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1532297
    iget-object v0, p0, LX/9kw;->a:Landroid/widget/EditText;

    new-instance v1, LX/9kv;

    invoke-direct {v1, p0}, LX/9kv;-><init>(LX/9kw;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1532298
    return-void
.end method

.method public static e(LX/9kw;)Landroid/widget/LinearLayout;
    .locals 2

    .prologue
    .line 1532317
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, LX/9kw;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1532318
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1532319
    return-object v0
.end method

.method public static f(LX/9kw;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1532310
    iget-boolean v0, p0, LX/9kw;->k:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/9kw;->j:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    .line 1532311
    :goto_0
    iget-object v4, p0, LX/9kw;->d:Lcom/facebook/widget/listview/EmptyListViewItem;

    if-eqz v0, :cond_2

    move v3, v1

    :goto_1
    invoke-virtual {v4, v3}, Lcom/facebook/widget/listview/EmptyListViewItem;->setVisibility(I)V

    .line 1532312
    iget-object v3, p0, LX/9kw;->f:Lcom/facebook/widget/listview/BetterListView;

    if-nez v0, :cond_3

    :goto_2
    invoke-virtual {v3, v2}, Lcom/facebook/widget/listview/BetterListView;->setFooterDividersEnabled(Z)V

    .line 1532313
    return-void

    :cond_1
    move v0, v1

    .line 1532314
    goto :goto_0

    .line 1532315
    :cond_2
    const/16 v3, 0x8

    goto :goto_1

    :cond_3
    move v2, v1

    .line 1532316
    goto :goto_2
.end method


# virtual methods
.method public final a(Z)V
    .locals 1

    .prologue
    .line 1532320
    iget-object v0, p0, LX/9kw;->d:Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/EmptyListViewItem;->a(Z)V

    .line 1532321
    iput-boolean p1, p0, LX/9kw;->k:Z

    .line 1532322
    invoke-static {p0}, LX/9kw;->f(LX/9kw;)V

    .line 1532323
    return-void
.end method

.method public setFooterMessage(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1532306
    iget-object v0, p0, LX/9kw;->d:Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/EmptyListViewItem;->setMessage(Ljava/lang/CharSequence;)V

    .line 1532307
    iput-object p1, p0, LX/9kw;->j:Ljava/lang/CharSequence;

    .line 1532308
    invoke-static {p0}, LX/9kw;->f(LX/9kw;)V

    .line 1532309
    return-void
.end method

.method public setHeaderVisibility(I)V
    .locals 1

    .prologue
    .line 1532303
    iget-object v0, p0, LX/9kw;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1532304
    iget-object v0, p0, LX/9kw;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 1532305
    :cond_0
    return-void
.end method

.method public setRows(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/9ks",
            "<TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 1532299
    iget-object v0, p0, LX/9kw;->e:LX/9km;

    .line 1532300
    iput-object p1, v0, LX/9km;->a:LX/0Px;

    .line 1532301
    const p0, 0x5e25bdb7

    invoke-static {v0, p0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1532302
    return-void
.end method
