.class public LX/AS5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3l5;


# instance fields
.field private final a:LX/0wM;

.field public b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:LX/0hs;


# direct methods
.method public constructor <init>(LX/0wM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1673652
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1673653
    iput-object p1, p0, LX/AS5;->a:LX/0wM;

    .line 1673654
    return-void
.end method

.method private g()Z
    .locals 1

    .prologue
    .line 1673655
    iget-object v0, p0, LX/AS5;->b:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 1673658
    const-wide/32 v0, 0x5265c00

    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 1

    .prologue
    .line 1673656
    invoke-direct {p0}, LX/AS5;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final a(LX/5L2;)LX/ASZ;
    .locals 1

    .prologue
    .line 1673657
    sget-object v0, LX/ASZ;->NONE:LX/ASZ;

    return-object v0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 1673650
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 1673651
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 1673645
    invoke-direct {p0}, LX/AS5;->g()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1673646
    invoke-virtual {p0}, LX/AS5;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1673647
    iget-object v0, p0, LX/AS5;->d:LX/0hs;

    invoke-virtual {v0}, LX/0ht;->l()V

    .line 1673648
    const/4 v0, 0x0

    iput-object v0, p0, LX/AS5;->d:LX/0hs;

    .line 1673649
    :cond_0
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1673644
    const-string v0, "4544"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1673643
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->COMPOSER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final d()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1673639
    iput-object v0, p0, LX/AS5;->b:Landroid/view/View;

    .line 1673640
    iput-object v0, p0, LX/AS5;->c:Landroid/view/View;

    .line 1673641
    iput-object v0, p0, LX/AS5;->d:LX/0hs;

    .line 1673642
    return-void
.end method

.method public final e()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 1673625
    invoke-direct {p0}, LX/AS5;->g()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1673626
    invoke-virtual {p0}, LX/AS5;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1673627
    :goto_0
    return-void

    .line 1673628
    :cond_0
    iget-object v0, p0, LX/AS5;->c:Landroid/view/View;

    if-nez v0, :cond_1

    .line 1673629
    iget-object v0, p0, LX/AS5;->b:Landroid/view/View;

    const v1, 0x7f0d0a81

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/AS5;->c:Landroid/view/View;

    .line 1673630
    :cond_1
    new-instance v0, LX/0hs;

    iget-object v1, p0, LX/AS5;->c:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, LX/0hs;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, LX/AS5;->d:LX/0hs;

    .line 1673631
    iget-object v0, p0, LX/AS5;->d:LX/0hs;

    iget-object v1, p0, LX/AS5;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, LX/0ht;->c(Landroid/view/View;)V

    .line 1673632
    iget-object v0, p0, LX/AS5;->d:LX/0hs;

    iget-object v1, p0, LX/AS5;->a:LX/0wM;

    const v2, 0x7f0208ef

    invoke-virtual {v1, v2, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0hs;->b(Landroid/graphics/drawable/Drawable;)V

    .line 1673633
    iget-object v0, p0, LX/AS5;->d:LX/0hs;

    const v1, 0x7f0814b7

    invoke-virtual {v0, v1}, LX/0hs;->a(I)V

    .line 1673634
    iget-object v0, p0, LX/AS5;->d:LX/0hs;

    const v1, 0x7f0814b8

    invoke-virtual {v0, v1}, LX/0hs;->b(I)V

    .line 1673635
    iget-object v0, p0, LX/AS5;->d:LX/0hs;

    sget-object v1, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v0, v1}, LX/0ht;->a(LX/3AV;)V

    .line 1673636
    iget-object v0, p0, LX/AS5;->d:LX/0hs;

    .line 1673637
    iput v3, v0, LX/0hs;->t:I

    .line 1673638
    iget-object v0, p0, LX/AS5;->d:LX/0hs;

    invoke-virtual {v0}, LX/0ht;->d()V

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 1673622
    iget-object v0, p0, LX/AS5;->d:LX/0hs;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AS5;->d:LX/0hs;

    .line 1673623
    iget-boolean p0, v0, LX/0ht;->r:Z

    move v0, p0

    .line 1673624
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
