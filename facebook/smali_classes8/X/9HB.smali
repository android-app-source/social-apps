.class public LX/9HB;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/view/View;

.field private final b:LX/9CG;

.field public c:Landroid/view/View$OnTouchListener;


# direct methods
.method public constructor <init>(Landroid/view/View;LX/9CG;)V
    .locals 0
    .param p1    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1460924
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1460925
    iput-object p1, p0, LX/9HB;->a:Landroid/view/View;

    .line 1460926
    iput-object p2, p0, LX/9HB;->b:LX/9CG;

    .line 1460927
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1460928
    iget-object v0, p0, LX/9HB;->a:Landroid/view/View;

    iget-object v1, p0, LX/9HB;->b:LX/9CG;

    iget-object v2, p0, LX/9HB;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/9CG;->a(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1460929
    return-void
.end method

.method public final a(Landroid/animation/ValueAnimator;)V
    .locals 1

    .prologue
    .line 1460930
    iget-object v0, p0, LX/9HB;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {v0, p1}, LX/9CG;->a(Landroid/graphics/drawable/Drawable;Landroid/animation/ValueAnimator;)V

    .line 1460931
    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 1460932
    iget-object v0, p0, LX/9HB;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1460933
    iget-object v0, p0, LX/9HB;->c:Landroid/view/View$OnTouchListener;

    if-eqz v0, :cond_0

    .line 1460934
    iget-object v0, p0, LX/9HB;->c:Landroid/view/View$OnTouchListener;

    iget-object v1, p0, LX/9HB;->a:Landroid/view/View;

    invoke-interface {v0, v1, p1}, Landroid/view/View$OnTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 1460935
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
