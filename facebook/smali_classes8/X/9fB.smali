.class public LX/9fB;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "Lcom/facebook/photos/editgallery/EditGalleryFragmentController;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1521297
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1521298
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;LX/9el;Landroid/net/Uri;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;LX/9fh;)Lcom/facebook/photos/editgallery/EditGalleryFragmentController;
    .locals 30

    .prologue
    .line 1521299
    new-instance v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    const/16 v2, 0x2e31

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const-class v2, LX/9d6;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/9d6;

    invoke-static/range {p0 .. p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v11

    check-cast v11, LX/0hB;

    const/16 v2, 0x2e3d

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v2, 0x12b1

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-static/range {p0 .. p0}, LX/8GV;->a(LX/0QB;)LX/8GV;

    move-result-object v14

    check-cast v14, LX/8GV;

    const-class v2, LX/9ej;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v15

    check-cast v15, LX/9ej;

    const-class v2, LX/9fZ;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v16

    check-cast v16, LX/9fZ;

    const-class v2, LX/9fV;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v17

    check-cast v17, LX/9fV;

    const-class v2, LX/9ed;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v18

    check-cast v18, LX/9ed;

    const-class v2, LX/9fJ;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v19

    check-cast v19, LX/9fJ;

    const-class v2, LX/9fg;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v20

    check-cast v20, LX/9fg;

    invoke-static/range {p0 .. p0}, LX/9c7;->a(LX/0QB;)LX/9c7;

    move-result-object v21

    check-cast v21, LX/9c7;

    invoke-static/range {p0 .. p0}, LX/1Ad;->a(LX/0QB;)LX/1Ad;

    move-result-object v22

    check-cast v22, LX/1Ad;

    const-class v2, LX/9fQ;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v23

    check-cast v23, LX/9fQ;

    invoke-static/range {p0 .. p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v24

    check-cast v24, Ljava/lang/Boolean;

    const/16 v2, 0x160c

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v25

    invoke-static/range {p0 .. p0}, LX/1e4;->a(LX/0QB;)LX/1e4;

    move-result-object v26

    check-cast v26, LX/1e4;

    const/16 v2, 0x2ec8

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v27

    const/16 v2, 0x2e13

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v28

    const/16 v2, 0xac0

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v29

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v1 .. v29}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;-><init>(Lcom/facebook/photos/editgallery/EditGalleryDialogFragment;LX/9el;Landroid/net/Uri;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/facebook/photos/editgallery/EditGalleryFragmentController$State;LX/9fh;LX/0Or;LX/9d6;LX/0hB;LX/0Ot;LX/0Ot;LX/8GV;LX/9ej;LX/9fZ;LX/9fV;LX/9ed;LX/9fJ;LX/9fg;LX/9c7;LX/1Ad;LX/9fQ;Ljava/lang/Boolean;LX/0Or;LX/1e4;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 1521300
    return-object v1
.end method
