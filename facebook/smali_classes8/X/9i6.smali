.class public final LX/9i6;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:LX/171;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Landroid/content/Context;

.field public final synthetic d:Z

.field public final synthetic e:LX/162;

.field public final synthetic f:LX/9iA;


# direct methods
.method public constructor <init>(LX/9iA;LX/171;Ljava/lang/String;Landroid/content/Context;ZLX/162;)V
    .locals 0

    .prologue
    .line 1527239
    iput-object p1, p0, LX/9i6;->f:LX/9iA;

    iput-object p2, p0, LX/9i6;->a:LX/171;

    iput-object p3, p0, LX/9i6;->b:Ljava/lang/String;

    iput-object p4, p0, LX/9i6;->c:Landroid/content/Context;

    iput-boolean p5, p0, LX/9i6;->d:Z

    iput-object p6, p0, LX/9i6;->e:LX/162;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    .line 1527218
    iget-object v0, p0, LX/9i6;->f:LX/9iA;

    iget-object v1, p0, LX/9i6;->a:LX/171;

    const/4 v2, 0x0

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1527219
    invoke-interface {v1}, LX/171;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    const v4, -0x7333ac54

    if-ne v3, v4, :cond_4

    .line 1527220
    invoke-interface {v1}, LX/171;->w_()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1527221
    :cond_0
    :goto_0
    move-object v1, v2

    .line 1527222
    if-nez v1, :cond_2

    .line 1527223
    iget-object v0, p0, LX/9i6;->f:LX/9iA;

    iget-object v0, v0, LX/9iA;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    iget-object v1, p0, LX/9i6;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "null url from entity with id "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/9i6;->a:LX/171;

    invoke-interface {v3}, LX/171;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1527224
    :cond_1
    :goto_1
    return-void

    .line 1527225
    :cond_2
    iget-object v0, p0, LX/9i6;->f:LX/9iA;

    iget-object v0, v0, LX/9iA;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iget-object v2, p0, LX/9i6;->c:Landroid/content/Context;

    invoke-interface {v0, v2, v1}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 1527226
    iget-object v0, p0, LX/9i6;->f:LX/9iA;

    iget-object v0, v0, LX/9iA;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17V;

    iget-boolean v2, p0, LX/9i6;->d:Z

    iget-object v3, p0, LX/9i6;->e:LX/162;

    const-string v4, "photo_gallery"

    invoke-virtual {v0, v1, v2, v3, v4}, LX/17V;->a(Ljava/lang/String;ZLX/0lF;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1527227
    if-eqz v0, :cond_1

    .line 1527228
    invoke-static {p1}, LX/1vZ;->a(Landroid/view/View;)LX/1vY;

    move-result-object v1

    .line 1527229
    invoke-static {v0, v1}, LX/1vZ;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/1vY;)V

    .line 1527230
    iget-object v1, p0, LX/9i6;->f:LX/9iA;

    iget-object v1, v1, LX/9iA;->f:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    goto :goto_1

    .line 1527231
    :cond_3
    iget-object v2, v0, LX/9iA;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1nG;

    invoke-interface {v1}, LX/171;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-interface {v1}, LX/171;->w_()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-interface {v1}, LX/171;->v_()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-interface {v1}, LX/171;->e()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v3, v4}, LX/1nG;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 1527232
    :cond_4
    invoke-interface {v1}, LX/171;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    const v4, 0x1eaef984

    if-ne v3, v4, :cond_5

    .line 1527233
    invoke-interface {v1}, LX/171;->j()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1527234
    iget-object v2, v0, LX/9iA;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1nG;

    invoke-interface {v1}, LX/171;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-interface {v1}, LX/171;->e()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1}, LX/171;->j()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, LX/1nG;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 1527235
    :cond_5
    invoke-interface {v1}, LX/171;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    const v3, 0x5fcedbf5

    if-ne v2, v3, :cond_6

    .line 1527236
    iget-object v2, v0, LX/9iA;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-interface {v1}, LX/171;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    new-array v3, v8, [Ljava/lang/Object;

    invoke-interface {v1}, LX/171;->e()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-interface {v1}, LX/171;->j()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v2, v3}, LX/1nG;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 1527237
    :cond_6
    iget-object v2, v0, LX/9iA;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-interface {v1}, LX/171;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    new-array v3, v7, [Ljava/lang/Object;

    invoke-interface {v1}, LX/171;->e()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, LX/1nG;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 0

    .prologue
    .line 1527238
    return-void
.end method
