.class public LX/8ip;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:I


# instance fields
.field private b:Landroid/view/View;

.field public c:Landroid/view/GestureDetector;

.field public d:Z

.field public e:LX/8ir;

.field public f:LX/8ik;

.field public g:LX/8iq;

.field public h:Z

.field public i:Z

.field public j:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1392015
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v0

    sput v0, LX/8ip;->a:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1392006
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1392007
    iput-object p2, p0, LX/8ip;->b:Landroid/view/View;

    .line 1392008
    iget-object v0, p0, LX/8ip;->b:Landroid/view/View;

    new-instance v1, LX/8io;

    invoke-direct {v1, p0}, LX/8io;-><init>(LX/8ip;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1392009
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, LX/8in;

    invoke-direct {v1, p0}, LX/8in;-><init>(LX/8ip;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, LX/8ip;->c:Landroid/view/GestureDetector;

    .line 1392010
    iget-object v0, p0, LX/8ip;->c:Landroid/view/GestureDetector;

    invoke-virtual {v0, v2}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    .line 1392011
    iget-object v0, p0, LX/8ip;->c:Landroid/view/GestureDetector;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/GestureDetector;->setOnDoubleTapListener(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    .line 1392012
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8ip;->d:Z

    .line 1392013
    iput-boolean v2, p0, LX/8ip;->j:Z

    .line 1392014
    return-void
.end method

.method public static a$redex0(LX/8ip;Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1392002
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 1392003
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 1392004
    iget-object v2, p0, LX/8ip;->b:Landroid/view/View;

    .line 1392005
    cmpl-float v3, v0, v4

    if-ltz v3, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    cmpg-float v0, v0, v3

    if-gtz v0, :cond_0

    cmpl-float v0, v1, v4

    if-ltz v0, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    cmpg-float v0, v1, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/8ip;)V
    .locals 1

    .prologue
    .line 1391999
    iget-object v0, p0, LX/8ip;->g:LX/8iq;

    if-eqz v0, :cond_0

    .line 1392000
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8ip;->i:Z

    .line 1392001
    :cond_0
    return-void
.end method
