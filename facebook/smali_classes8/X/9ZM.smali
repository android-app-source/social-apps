.class public final LX/9ZM;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 40

    .prologue
    .line 1509949
    const/16 v36, 0x0

    .line 1509950
    const/16 v35, 0x0

    .line 1509951
    const/16 v34, 0x0

    .line 1509952
    const/16 v33, 0x0

    .line 1509953
    const/16 v32, 0x0

    .line 1509954
    const/16 v31, 0x0

    .line 1509955
    const/16 v30, 0x0

    .line 1509956
    const/16 v29, 0x0

    .line 1509957
    const/16 v28, 0x0

    .line 1509958
    const/16 v27, 0x0

    .line 1509959
    const/16 v26, 0x0

    .line 1509960
    const/16 v25, 0x0

    .line 1509961
    const/16 v24, 0x0

    .line 1509962
    const/16 v23, 0x0

    .line 1509963
    const/16 v22, 0x0

    .line 1509964
    const/16 v21, 0x0

    .line 1509965
    const/16 v20, 0x0

    .line 1509966
    const/16 v19, 0x0

    .line 1509967
    const/16 v18, 0x0

    .line 1509968
    const/16 v17, 0x0

    .line 1509969
    const/16 v16, 0x0

    .line 1509970
    const/4 v15, 0x0

    .line 1509971
    const/4 v14, 0x0

    .line 1509972
    const/4 v13, 0x0

    .line 1509973
    const/4 v12, 0x0

    .line 1509974
    const/4 v11, 0x0

    .line 1509975
    const/4 v10, 0x0

    .line 1509976
    const/4 v9, 0x0

    .line 1509977
    const/4 v8, 0x0

    .line 1509978
    const/4 v7, 0x0

    .line 1509979
    const/4 v6, 0x0

    .line 1509980
    const/4 v5, 0x0

    .line 1509981
    const/4 v4, 0x0

    .line 1509982
    const/4 v3, 0x0

    .line 1509983
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v37

    sget-object v38, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v37

    move-object/from16 v1, v38

    if-eq v0, v1, :cond_1

    .line 1509984
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1509985
    const/4 v3, 0x0

    .line 1509986
    :goto_0
    return v3

    .line 1509987
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1509988
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v37

    sget-object v38, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v37

    move-object/from16 v1, v38

    if-eq v0, v1, :cond_19

    .line 1509989
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v37

    .line 1509990
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1509991
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v38

    sget-object v39, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v38

    move-object/from16 v1, v39

    if-eq v0, v1, :cond_1

    if-eqz v37, :cond_1

    .line 1509992
    const-string v38, "attribution"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_2

    .line 1509993
    invoke-static/range {p0 .. p1}, LX/9ZI;->a(LX/15w;LX/186;)I

    move-result v36

    goto :goto_1

    .line 1509994
    :cond_2
    const-string v38, "category_names"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_3

    .line 1509995
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v35

    goto :goto_1

    .line 1509996
    :cond_3
    const-string v38, "cover_photo"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_4

    .line 1509997
    invoke-static/range {p0 .. p1}, LX/9ZU;->a(LX/15w;LX/186;)I

    move-result v34

    goto :goto_1

    .line 1509998
    :cond_4
    const-string v38, "does_viewer_like"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_5

    .line 1509999
    const/4 v12, 0x1

    .line 1510000
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v33

    goto :goto_1

    .line 1510001
    :cond_5
    const-string v38, "expressed_as_place"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_6

    .line 1510002
    const/4 v11, 0x1

    .line 1510003
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v32

    goto :goto_1

    .line 1510004
    :cond_6
    const-string v38, "is_eligible_for_page_verification"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_7

    .line 1510005
    const/4 v10, 0x1

    .line 1510006
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v31

    goto :goto_1

    .line 1510007
    :cond_7
    const-string v38, "is_opted_in_sponsor_tags"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_8

    .line 1510008
    const/4 v9, 0x1

    .line 1510009
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v30

    goto :goto_1

    .line 1510010
    :cond_8
    const-string v38, "is_owned"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_9

    .line 1510011
    const/4 v8, 0x1

    .line 1510012
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v29

    goto/16 :goto_1

    .line 1510013
    :cond_9
    const-string v38, "is_published"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_a

    .line 1510014
    const/4 v7, 0x1

    .line 1510015
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v28

    goto/16 :goto_1

    .line 1510016
    :cond_a
    const-string v38, "is_service_page"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_b

    .line 1510017
    const/4 v6, 0x1

    .line 1510018
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v27

    goto/16 :goto_1

    .line 1510019
    :cond_b
    const-string v38, "is_verified"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_c

    .line 1510020
    const/4 v5, 0x1

    .line 1510021
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v26

    goto/16 :goto_1

    .line 1510022
    :cond_c
    const-string v38, "name"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_d

    .line 1510023
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v25

    goto/16 :goto_1

    .line 1510024
    :cond_d
    const-string v38, "place_type"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_e

    .line 1510025
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/facebook/graphql/enums/GraphQLPlaceType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v24

    goto/16 :goto_1

    .line 1510026
    :cond_e
    const-string v38, "profilePictureAsCover"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_f

    .line 1510027
    invoke-static/range {p0 .. p1}, LX/9ZV;->a(LX/15w;LX/186;)I

    move-result v23

    goto/16 :goto_1

    .line 1510028
    :cond_f
    const-string v38, "profile_photo"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_10

    .line 1510029
    invoke-static/range {p0 .. p1}, LX/9ZO;->a(LX/15w;LX/186;)I

    move-result v22

    goto/16 :goto_1

    .line 1510030
    :cond_10
    const-string v38, "profile_picture"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_11

    .line 1510031
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v21

    goto/16 :goto_1

    .line 1510032
    :cond_11
    const-string v38, "profile_picture_is_silhouette"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_12

    .line 1510033
    const/4 v4, 0x1

    .line 1510034
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v20

    goto/16 :goto_1

    .line 1510035
    :cond_12
    const-string v38, "redirection_info"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_13

    .line 1510036
    invoke-static/range {p0 .. p1}, LX/9ZK;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 1510037
    :cond_13
    const-string v38, "should_show_username"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_14

    .line 1510038
    const/4 v3, 0x1

    .line 1510039
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v18

    goto/16 :goto_1

    .line 1510040
    :cond_14
    const-string v38, "super_category_type"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_15

    .line 1510041
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v17

    goto/16 :goto_1

    .line 1510042
    :cond_15
    const-string v38, "timeline_pinned_unit"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_16

    .line 1510043
    invoke-static/range {p0 .. p1}, LX/9ZL;->a(LX/15w;LX/186;)I

    move-result v16

    goto/16 :goto_1

    .line 1510044
    :cond_16
    const-string v38, "username"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_17

    .line 1510045
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    goto/16 :goto_1

    .line 1510046
    :cond_17
    const-string v38, "verification_status"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_18

    .line 1510047
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v14

    goto/16 :goto_1

    .line 1510048
    :cond_18
    const-string v38, "viewer_profile_permissions"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v37

    if-eqz v37, :cond_0

    .line 1510049
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v13

    goto/16 :goto_1

    .line 1510050
    :cond_19
    const/16 v37, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1510051
    const/16 v37, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v37

    move/from16 v2, v36

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1510052
    const/16 v36, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v36

    move/from16 v2, v35

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1510053
    const/16 v35, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v35

    move/from16 v2, v34

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1510054
    if-eqz v12, :cond_1a

    .line 1510055
    const/4 v12, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v12, v1}, LX/186;->a(IZ)V

    .line 1510056
    :cond_1a
    if-eqz v11, :cond_1b

    .line 1510057
    const/4 v11, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v11, v1}, LX/186;->a(IZ)V

    .line 1510058
    :cond_1b
    if-eqz v10, :cond_1c

    .line 1510059
    const/4 v10, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v10, v1}, LX/186;->a(IZ)V

    .line 1510060
    :cond_1c
    if-eqz v9, :cond_1d

    .line 1510061
    const/4 v9, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v9, v1}, LX/186;->a(IZ)V

    .line 1510062
    :cond_1d
    if-eqz v8, :cond_1e

    .line 1510063
    const/4 v8, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 1510064
    :cond_1e
    if-eqz v7, :cond_1f

    .line 1510065
    const/16 v7, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 1510066
    :cond_1f
    if-eqz v6, :cond_20

    .line 1510067
    const/16 v6, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 1510068
    :cond_20
    if-eqz v5, :cond_21

    .line 1510069
    const/16 v5, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 1510070
    :cond_21
    const/16 v5, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 1510071
    const/16 v5, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 1510072
    const/16 v5, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 1510073
    const/16 v5, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 1510074
    const/16 v5, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 1510075
    if-eqz v4, :cond_22

    .line 1510076
    const/16 v4, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 1510077
    :cond_22
    const/16 v4, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1510078
    if-eqz v3, :cond_23

    .line 1510079
    const/16 v3, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v3, v1}, LX/186;->a(IZ)V

    .line 1510080
    :cond_23
    const/16 v3, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1510081
    const/16 v3, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1510082
    const/16 v3, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->b(II)V

    .line 1510083
    const/16 v3, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->b(II)V

    .line 1510084
    const/16 v3, 0x17

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 1510085
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method
