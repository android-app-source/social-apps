.class public abstract LX/8vi;
.super Landroid/view/ViewGroup;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Landroid/widget/Adapter;",
        ">",
        "Landroid/view/ViewGroup;"
    }
.end annotation


# instance fields
.field public V:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "scrolling"
    .end annotation
.end field

.field public W:I

.field private a:I

.field public aa:I

.field public ab:J

.field public ac:J

.field public ad:Z

.field public ae:I

.field public af:Z

.field public ag:LX/8vm;

.field public ah:LX/9cc;

.field public ai:LX/8vl;

.field public aj:Z

.field public ak:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "list"
    .end annotation
.end field

.field public al:J

.field public am:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "list"
    .end annotation
.end field

.field public an:J

.field public ao:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "list"
    .end annotation
.end field

.field public ap:I

.field public aq:Landroid/view/accessibility/AccessibilityManager;

.field public ar:I

.field public as:J

.field public at:Z

.field private b:Landroid/view/View;

.field private c:Z

.field private d:Z

.field public e:Lit/sephiroth/android/library/widget/AdapterView$SelectionNotifier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/8vi",
            "<TT;>.SelectionNotifier;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v1, -0x1

    const-wide/high16 v2, -0x8000000000000000L

    const/4 v0, 0x0

    .line 1417828
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 1417829
    iput v0, p0, LX/8vi;->V:I

    .line 1417830
    iput-wide v2, p0, LX/8vi;->ab:J

    .line 1417831
    iput-boolean v0, p0, LX/8vi;->ad:Z

    .line 1417832
    iput-boolean v0, p0, LX/8vi;->af:Z

    .line 1417833
    iput v1, p0, LX/8vi;->ak:I

    .line 1417834
    iput-wide v2, p0, LX/8vi;->al:J

    .line 1417835
    iput v1, p0, LX/8vi;->am:I

    .line 1417836
    iput-wide v2, p0, LX/8vi;->an:J

    .line 1417837
    iput v1, p0, LX/8vi;->ar:I

    .line 1417838
    iput-wide v2, p0, LX/8vi;->as:J

    .line 1417839
    iput-boolean v0, p0, LX/8vi;->at:Z

    .line 1417840
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v1, -0x1

    const-wide/high16 v2, -0x8000000000000000L

    const/4 v0, 0x0

    .line 1417841
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1417842
    iput v0, p0, LX/8vi;->V:I

    .line 1417843
    iput-wide v2, p0, LX/8vi;->ab:J

    .line 1417844
    iput-boolean v0, p0, LX/8vi;->ad:Z

    .line 1417845
    iput-boolean v0, p0, LX/8vi;->af:Z

    .line 1417846
    iput v1, p0, LX/8vi;->ak:I

    .line 1417847
    iput-wide v2, p0, LX/8vi;->al:J

    .line 1417848
    iput v1, p0, LX/8vi;->am:I

    .line 1417849
    iput-wide v2, p0, LX/8vi;->an:J

    .line 1417850
    iput v1, p0, LX/8vi;->ar:I

    .line 1417851
    iput-wide v2, p0, LX/8vi;->as:J

    .line 1417852
    iput-boolean v0, p0, LX/8vi;->at:Z

    .line 1417853
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v1, -0x1

    const-wide/high16 v2, -0x8000000000000000L

    const/4 v0, 0x0

    .line 1417854
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1417855
    iput v0, p0, LX/8vi;->V:I

    .line 1417856
    iput-wide v2, p0, LX/8vi;->ab:J

    .line 1417857
    iput-boolean v0, p0, LX/8vi;->ad:Z

    .line 1417858
    iput-boolean v0, p0, LX/8vi;->af:Z

    .line 1417859
    iput v1, p0, LX/8vi;->ak:I

    .line 1417860
    iput-wide v2, p0, LX/8vi;->al:J

    .line 1417861
    iput v1, p0, LX/8vi;->am:I

    .line 1417862
    iput-wide v2, p0, LX/8vi;->an:J

    .line 1417863
    iput v1, p0, LX/8vi;->ar:I

    .line 1417864
    iput-wide v2, p0, LX/8vi;->as:J

    .line 1417865
    iput-boolean v0, p0, LX/8vi;->at:Z

    .line 1417866
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 1417867
    invoke-virtual {p0}, LX/8vi;->getImportantForAccessibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1417868
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/8vi;->setImportantForAccessibility(I)V

    .line 1417869
    :cond_0
    invoke-virtual {p0}, LX/8vi;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1417870
    invoke-virtual {p0}, LX/8vi;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, LX/8vi;->aq:Landroid/view/accessibility/AccessibilityManager;

    .line 1417871
    :cond_1
    return-void
.end method

.method public static synthetic a(LX/8vi;)Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 1417872
    invoke-virtual {p0}, LX/8vi;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(LX/8vi;Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 1417873
    invoke-virtual {p0, p1}, LX/8vi;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void
.end method

.method private a(Z)V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WrongCall"
        }
    .end annotation

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1417805
    goto :goto_0

    .line 1417806
    :goto_0
    if-eqz p1, :cond_2

    .line 1417807
    iget-object v0, p0, LX/8vi;->b:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 1417808
    iget-object v0, p0, LX/8vi;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1417809
    invoke-virtual {p0, v2}, LX/8vi;->setVisibility(I)V

    .line 1417810
    :goto_1
    iget-boolean v0, p0, LX/8vi;->aj:Z

    if-eqz v0, :cond_0

    .line 1417811
    invoke-virtual {p0}, LX/8vi;->getLeft()I

    move-result v2

    invoke-virtual {p0}, LX/8vi;->getTop()I

    move-result v3

    invoke-virtual {p0}, LX/8vi;->getRight()I

    move-result v4

    invoke-virtual {p0}, LX/8vi;->getBottom()I

    move-result v5

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/8vi;->onLayout(ZIIII)V

    .line 1417812
    :cond_0
    :goto_2
    return-void

    .line 1417813
    :cond_1
    invoke-virtual {p0, v1}, LX/8vi;->setVisibility(I)V

    goto :goto_1

    .line 1417814
    :cond_2
    iget-object v0, p0, LX/8vi;->b:Landroid/view/View;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/8vi;->b:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1417815
    :cond_3
    invoke-virtual {p0, v1}, LX/8vi;->setVisibility(I)V

    goto :goto_2
.end method

.method public static c$redex0(LX/8vi;)V
    .locals 2

    .prologue
    .line 1417874
    iget-object v0, p0, LX/8vi;->ag:LX/8vm;

    if-nez v0, :cond_1

    .line 1417875
    :cond_0
    :goto_0
    return-void

    .line 1417876
    :cond_1
    iget v0, p0, LX/8vi;->ak:I

    move v0, v0

    .line 1417877
    if-ltz v0, :cond_0

    .line 1417878
    invoke-virtual {p0}, LX/8vi;->getSelectedView()Landroid/view/View;

    .line 1417879
    invoke-virtual {p0}, LX/8vi;->getAdapter()Landroid/widget/Adapter;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/widget/Adapter;->getItemId(I)J

    goto :goto_0
.end method

.method public static d(LX/8vi;)V
    .locals 1

    .prologue
    .line 1417880
    iget-object v0, p0, LX/8vi;->aq:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1417881
    :cond_0
    :goto_0
    return-void

    .line 1417882
    :cond_1
    iget v0, p0, LX/8vi;->ak:I

    move v0, v0

    .line 1417883
    if-ltz v0, :cond_0

    .line 1417884
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, LX/8vi;->sendAccessibilityEvent(I)V

    goto :goto_0
.end method

.method private e()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1417885
    invoke-virtual {p0}, LX/8vi;->getAdapter()Landroid/widget/Adapter;

    move-result-object v1

    .line 1417886
    if-eqz v1, :cond_1

    .line 1417887
    invoke-interface {v1}, Landroid/widget/Adapter;->getCount()I

    move-result v1

    .line 1417888
    if-lez v1, :cond_1

    .line 1417889
    iget v2, p0, LX/8vi;->V:I

    move v2, v2

    .line 1417890
    if-gtz v2, :cond_0

    invoke-virtual {p0}, LX/8vi;->getLastVisiblePosition()I

    move-result v2

    add-int/lit8 v1, v1, -0x1

    if-ge v2, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 1417891
    :cond_1
    return v0
.end method


# virtual methods
.method public a(IZ)I
    .locals 0

    .prologue
    .line 1417892
    return p1
.end method

.method public final a(Landroid/view/View;)I
    .locals 4

    .prologue
    const/4 v1, -0x1

    .line 1417893
    :goto_0
    :try_start_0
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-nez v2, :cond_0

    move-object p1, v0

    .line 1417894
    goto :goto_0

    .line 1417895
    :catch_0
    move v0, v1

    .line 1417896
    :goto_1
    return v0

    .line 1417897
    :cond_0
    invoke-virtual {p0}, LX/8vi;->getChildCount()I

    move-result v2

    .line 1417898
    const/4 v0, 0x0

    :goto_2
    if-ge v0, v2, :cond_2

    .line 1417899
    invoke-virtual {p0, v0}, LX/8vi;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1417900
    iget v1, p0, LX/8vi;->V:I

    add-int/2addr v0, v1

    goto :goto_1

    .line 1417901
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    move v0, v1

    .line 1417902
    goto :goto_1
.end method

.method public a(Landroid/view/View;IJ)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1417903
    iget-object v2, p0, LX/8vi;->ah:LX/9cc;

    if-eqz v2, :cond_1

    .line 1417904
    invoke-virtual {p0, v1}, LX/8vi;->playSoundEffect(I)V

    .line 1417905
    if-eqz p1, :cond_0

    .line 1417906
    invoke-virtual {p1, v0}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 1417907
    :cond_0
    iget-object v1, p0, LX/8vi;->ah:LX/9cc;

    .line 1417908
    iget-object v2, v1, LX/9cc;->a:LX/9cg;

    iget-object v2, v2, LX/9cg;->g:Landroid/support/v4/view/ViewPager;

    const/4 p0, 0x1

    invoke-virtual {v2, p2, p0}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 1417909
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final addView(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1417910
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "addView(View) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final addView(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 1417912
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "addView(View, int) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    .prologue
    .line 1417911
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "addView(View, int, LayoutParams) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    .prologue
    .line 1417927
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "addView(View, LayoutParams) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public canAnimate()Z
    .locals 1

    .prologue
    .line 1417926
    invoke-super {p0}, Landroid/view/ViewGroup;->canAnimate()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, LX/8vi;->ao:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(I)J
    .locals 2

    .prologue
    .line 1417924
    invoke-virtual {p0}, LX/8vi;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    .line 1417925
    if-eqz v0, :cond_0

    if-gez p1, :cond_1

    :cond_0
    const-wide/high16 v0, -0x8000000000000000L

    :goto_0
    return-wide v0

    :cond_1
    invoke-interface {v0, p1}, Landroid/widget/Adapter;->getItemId(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public final dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 2

    .prologue
    .line 1417920
    invoke-virtual {p0}, LX/8vi;->getSelectedView()Landroid/view/View;

    move-result-object v0

    .line 1417921
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1417922
    const/4 v0, 0x1

    .line 1417923
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final dispatchRestoreInstanceState(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1417918
    invoke-virtual {p0, p1}, LX/8vi;->dispatchThawSelfOnly(Landroid/util/SparseArray;)V

    .line 1417919
    return-void
.end method

.method public final dispatchSaveInstanceState(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1417916
    invoke-virtual {p0, p1}, LX/8vi;->dispatchFreezeSelfOnly(Landroid/util/SparseArray;)V

    .line 1417917
    return-void
.end method

.method public abstract getAdapter()Landroid/widget/Adapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public getEmptyView()Landroid/view/View;
    .locals 1

    .prologue
    .line 1417915
    iget-object v0, p0, LX/8vi;->b:Landroid/view/View;

    return-object v0
.end method

.method public getLastVisiblePosition()I
    .locals 2

    .prologue
    .line 1417914
    iget v0, p0, LX/8vi;->V:I

    invoke-virtual {p0}, LX/8vi;->getChildCount()I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public final getOnItemClickListener()LX/9cc;
    .locals 1

    .prologue
    .line 1417913
    iget-object v0, p0, LX/8vi;->ah:LX/9cc;

    return-object v0
.end method

.method public final getOnItemLongClickListener()LX/8vl;
    .locals 1

    .prologue
    .line 1417826
    iget-object v0, p0, LX/8vi;->ai:LX/8vl;

    return-object v0
.end method

.method public final getOnItemSelectedListener()LX/8vm;
    .locals 1

    .prologue
    .line 1417827
    iget-object v0, p0, LX/8vi;->ag:LX/8vm;

    return-object v0
.end method

.method public getSelectedItem()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1417627
    invoke-virtual {p0}, LX/8vi;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    .line 1417628
    iget v1, p0, LX/8vi;->ak:I

    move v1, v1

    .line 1417629
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v2

    if-lez v2, :cond_0

    if-ltz v1, :cond_0

    .line 1417630
    invoke-interface {v0, v1}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 1417631
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract getSelectedView()Landroid/view/View;
.end method

.method public k()V
    .locals 8

    .prologue
    const-wide/high16 v6, -0x8000000000000000L

    const/4 v5, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1417632
    iget v4, p0, LX/8vi;->ao:I

    .line 1417633
    if-lez v4, :cond_6

    .line 1417634
    iget-boolean v0, p0, LX/8vi;->ad:Z

    if-eqz v0, :cond_5

    .line 1417635
    iput-boolean v1, p0, LX/8vi;->ad:Z

    .line 1417636
    invoke-virtual {p0}, LX/8vi;->n()I

    move-result v0

    .line 1417637
    if-ltz v0, :cond_5

    .line 1417638
    invoke-virtual {p0, v0, v2}, LX/8vi;->a(IZ)I

    move-result v3

    .line 1417639
    if-ne v3, v0, :cond_5

    .line 1417640
    invoke-virtual {p0, v0}, LX/8vi;->setNextSelectedPositionInt(I)V

    move v3, v2

    .line 1417641
    :goto_0
    if-nez v3, :cond_3

    .line 1417642
    iget v0, p0, LX/8vi;->ak:I

    move v0, v0

    .line 1417643
    if-lt v0, v4, :cond_0

    .line 1417644
    add-int/lit8 v0, v4, -0x1

    .line 1417645
    :cond_0
    if-gez v0, :cond_1

    move v0, v1

    .line 1417646
    :cond_1
    invoke-virtual {p0, v0, v2}, LX/8vi;->a(IZ)I

    move-result v4

    .line 1417647
    if-gez v4, :cond_4

    .line 1417648
    invoke-virtual {p0, v0, v1}, LX/8vi;->a(IZ)I

    move-result v0

    .line 1417649
    :goto_1
    if-ltz v0, :cond_3

    .line 1417650
    invoke-virtual {p0, v0}, LX/8vi;->setNextSelectedPositionInt(I)V

    .line 1417651
    invoke-virtual {p0}, LX/8vi;->m()V

    move v0, v2

    .line 1417652
    :goto_2
    if-nez v0, :cond_2

    .line 1417653
    iput v5, p0, LX/8vi;->am:I

    .line 1417654
    iput-wide v6, p0, LX/8vi;->an:J

    .line 1417655
    iput v5, p0, LX/8vi;->ak:I

    .line 1417656
    iput-wide v6, p0, LX/8vi;->al:J

    .line 1417657
    iput-boolean v1, p0, LX/8vi;->ad:Z

    .line 1417658
    invoke-virtual {p0}, LX/8vi;->m()V

    .line 1417659
    :cond_2
    return-void

    :cond_3
    move v0, v3

    goto :goto_2

    :cond_4
    move v0, v4

    goto :goto_1

    :cond_5
    move v3, v1

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_2
.end method

.method public final l()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1417660
    invoke-virtual {p0}, LX/8vi;->getAdapter()Landroid/widget/Adapter;

    move-result-object v4

    .line 1417661
    if-eqz v4, :cond_0

    invoke-interface {v4}, Landroid/widget/Adapter;->getCount()I

    move-result v0

    if-nez v0, :cond_5

    :cond_0
    move v0, v1

    .line 1417662
    :goto_0
    if-eqz v0, :cond_1

    .line 1417663
    goto :goto_4

    :cond_1
    move v3, v1

    .line 1417664
    :goto_1
    if-eqz v3, :cond_6

    iget-boolean v0, p0, LX/8vi;->d:Z

    if-eqz v0, :cond_6

    move v0, v1

    :goto_2
    invoke-super {p0, v0}, Landroid/view/ViewGroup;->setFocusableInTouchMode(Z)V

    .line 1417665
    if-eqz v3, :cond_7

    iget-boolean v0, p0, LX/8vi;->c:Z

    if-eqz v0, :cond_7

    move v0, v1

    :goto_3
    invoke-super {p0, v0}, Landroid/view/ViewGroup;->setFocusable(Z)V

    .line 1417666
    iget-object v0, p0, LX/8vi;->b:Landroid/view/View;

    if-eqz v0, :cond_4

    .line 1417667
    if-eqz v4, :cond_2

    invoke-interface {v4}, Landroid/widget/Adapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v2, v1

    :cond_3
    invoke-direct {p0, v2}, LX/8vi;->a(Z)V

    .line 1417668
    :cond_4
    return-void

    :cond_5
    move v0, v2

    .line 1417669
    goto :goto_0

    :goto_4
    move v3, v2

    .line 1417670
    goto :goto_1

    :cond_6
    move v0, v2

    .line 1417671
    goto :goto_2

    :cond_7
    move v0, v2

    .line 1417672
    goto :goto_3
.end method

.method public final m()V
    .locals 4

    .prologue
    .line 1417673
    iget v0, p0, LX/8vi;->am:I

    iget v1, p0, LX/8vi;->ar:I

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, LX/8vi;->an:J

    iget-wide v2, p0, LX/8vi;->as:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_5

    .line 1417674
    :cond_0
    iget-object v0, p0, LX/8vi;->ag:LX/8vm;

    if-nez v0, :cond_1

    iget-object v0, p0, LX/8vi;->aq:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1417675
    :cond_1
    iget-boolean v0, p0, LX/8vi;->af:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, LX/8vi;->at:Z

    if-eqz v0, :cond_6

    .line 1417676
    :cond_2
    iget-object v0, p0, LX/8vi;->e:Lit/sephiroth/android/library/widget/AdapterView$SelectionNotifier;

    if-nez v0, :cond_3

    .line 1417677
    new-instance v0, Lit/sephiroth/android/library/widget/AdapterView$SelectionNotifier;

    invoke-direct {v0, p0}, Lit/sephiroth/android/library/widget/AdapterView$SelectionNotifier;-><init>(LX/8vi;)V

    iput-object v0, p0, LX/8vi;->e:Lit/sephiroth/android/library/widget/AdapterView$SelectionNotifier;

    .line 1417678
    :cond_3
    iget-object v0, p0, LX/8vi;->e:Lit/sephiroth/android/library/widget/AdapterView$SelectionNotifier;

    invoke-virtual {p0, v0}, LX/8vi;->post(Ljava/lang/Runnable;)Z

    .line 1417679
    :cond_4
    :goto_0
    iget v0, p0, LX/8vi;->am:I

    iput v0, p0, LX/8vi;->ar:I

    .line 1417680
    iget-wide v0, p0, LX/8vi;->an:J

    iput-wide v0, p0, LX/8vi;->as:J

    .line 1417681
    :cond_5
    return-void

    .line 1417682
    :cond_6
    invoke-static {p0}, LX/8vi;->c$redex0(LX/8vi;)V

    .line 1417683
    invoke-static {p0}, LX/8vi;->d(LX/8vi;)V

    goto :goto_0
.end method

.method public final n()I
    .locals 12

    .prologue
    .line 1417684
    iget v6, p0, LX/8vi;->ao:I

    .line 1417685
    if-nez v6, :cond_0

    .line 1417686
    const/4 v3, -0x1

    .line 1417687
    :goto_0
    return v3

    .line 1417688
    :cond_0
    iget-wide v8, p0, LX/8vi;->ab:J

    .line 1417689
    iget v0, p0, LX/8vi;->aa:I

    .line 1417690
    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v1, v8, v2

    if-nez v1, :cond_1

    .line 1417691
    const/4 v3, -0x1

    goto :goto_0

    .line 1417692
    :cond_1
    const/4 v1, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1417693
    add-int/lit8 v1, v6, -0x1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1417694
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x64

    add-long v10, v2, v4

    .line 1417695
    const/4 v0, 0x0

    .line 1417696
    invoke-virtual {p0}, LX/8vi;->getAdapter()Landroid/widget/Adapter;

    move-result-object v7

    .line 1417697
    if-nez v7, :cond_b

    .line 1417698
    const/4 v3, -0x1

    goto :goto_0

    .line 1417699
    :cond_2
    add-int/lit8 v4, v6, -0x1

    if-ne v1, v4, :cond_6

    const/4 v4, 0x1

    move v5, v4

    .line 1417700
    :goto_1
    if-nez v2, :cond_7

    const/4 v4, 0x1

    .line 1417701
    :goto_2
    if-eqz v5, :cond_3

    if-nez v4, :cond_a

    .line 1417702
    :cond_3
    if-nez v4, :cond_4

    if-eqz v0, :cond_8

    if-nez v5, :cond_8

    .line 1417703
    :cond_4
    add-int/lit8 v1, v1, 0x1

    .line 1417704
    const/4 v0, 0x0

    move v3, v1

    .line 1417705
    :cond_5
    :goto_3
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    cmp-long v4, v4, v10

    if-gtz v4, :cond_a

    .line 1417706
    invoke-interface {v7, v3}, Landroid/widget/Adapter;->getItemId(I)J

    move-result-wide v4

    .line 1417707
    cmp-long v4, v4, v8

    if-nez v4, :cond_2

    goto :goto_0

    .line 1417708
    :cond_6
    const/4 v4, 0x0

    move v5, v4

    goto :goto_1

    .line 1417709
    :cond_7
    const/4 v4, 0x0

    goto :goto_2

    .line 1417710
    :cond_8
    if-nez v5, :cond_9

    if-nez v0, :cond_5

    if-nez v4, :cond_5

    .line 1417711
    :cond_9
    add-int/lit8 v2, v2, -0x1

    .line 1417712
    const/4 v0, 0x1

    move v3, v2

    goto :goto_3

    .line 1417713
    :cond_a
    const/4 v3, -0x1

    goto :goto_0

    :cond_b
    move v2, v1

    move v3, v1

    goto :goto_3
.end method

.method public final o()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1417714
    invoke-virtual {p0}, LX/8vi;->getChildCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 1417715
    iput-boolean v5, p0, LX/8vi;->ad:Z

    .line 1417716
    iget v0, p0, LX/8vi;->a:I

    int-to-long v0, v0

    iput-wide v0, p0, LX/8vi;->ac:J

    .line 1417717
    iget v0, p0, LX/8vi;->am:I

    if-ltz v0, :cond_2

    .line 1417718
    iget v0, p0, LX/8vi;->am:I

    iget v1, p0, LX/8vi;->V:I

    sub-int/2addr v0, v1

    invoke-virtual {p0, v0}, LX/8vi;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1417719
    iget-wide v2, p0, LX/8vi;->al:J

    iput-wide v2, p0, LX/8vi;->ab:J

    .line 1417720
    iget v1, p0, LX/8vi;->ak:I

    iput v1, p0, LX/8vi;->aa:I

    .line 1417721
    if-eqz v0, :cond_0

    .line 1417722
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    iput v0, p0, LX/8vi;->W:I

    .line 1417723
    :cond_0
    iput v4, p0, LX/8vi;->ae:I

    .line 1417724
    :cond_1
    :goto_0
    return-void

    .line 1417725
    :cond_2
    invoke-virtual {p0, v4}, LX/8vi;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1417726
    invoke-virtual {p0}, LX/8vi;->getAdapter()Landroid/widget/Adapter;

    move-result-object v1

    .line 1417727
    iget v2, p0, LX/8vi;->V:I

    if-ltz v2, :cond_4

    iget v2, p0, LX/8vi;->V:I

    invoke-interface {v1}, Landroid/widget/Adapter;->getCount()I

    move-result v3

    if-ge v2, v3, :cond_4

    .line 1417728
    iget v2, p0, LX/8vi;->V:I

    invoke-interface {v1, v2}, Landroid/widget/Adapter;->getItemId(I)J

    move-result-wide v2

    iput-wide v2, p0, LX/8vi;->ab:J

    .line 1417729
    :goto_1
    iget v1, p0, LX/8vi;->V:I

    iput v1, p0, LX/8vi;->aa:I

    .line 1417730
    if-eqz v0, :cond_3

    .line 1417731
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    iput v0, p0, LX/8vi;->W:I

    .line 1417732
    :cond_3
    iput v5, p0, LX/8vi;->ae:I

    goto :goto_0

    .line 1417733
    :cond_4
    const-wide/16 v2, -0x1

    iput-wide v2, p0, LX/8vi;->ab:J

    goto :goto_1
.end method

.method public onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x7b9193b3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1417734
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 1417735
    iget-object v1, p0, LX/8vi;->e:Lit/sephiroth/android/library/widget/AdapterView$SelectionNotifier;

    invoke-virtual {p0, v1}, LX/8vi;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1417736
    const/16 v1, 0x2d

    const v2, -0x21e427a5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 1417737
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1417738
    const-class v0, LX/8vi;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 1417739
    invoke-direct {p0}, LX/8vi;->e()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setScrollable(Z)V

    .line 1417740
    invoke-virtual {p0}, LX/8vi;->getSelectedView()Landroid/view/View;

    move-result-object v0

    .line 1417741
    if-eqz v0, :cond_0

    .line 1417742
    invoke-virtual {v0}, Landroid/view/View;->isEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setEnabled(Z)V

    .line 1417743
    :cond_0
    iget v0, p0, LX/8vi;->ak:I

    move v0, v0

    .line 1417744
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setCurrentItemIndex(I)V

    .line 1417745
    iget v0, p0, LX/8vi;->V:I

    move v0, v0

    .line 1417746
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setFromIndex(I)V

    .line 1417747
    invoke-virtual {p0}, LX/8vi;->getLastVisiblePosition()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setToIndex(I)V

    .line 1417748
    iget v0, p0, LX/8vi;->ao:I

    move v0, v0

    .line 1417749
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setItemCount(I)V

    .line 1417750
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 1417751
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 1417752
    const-class v0, LX/8vi;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 1417753
    invoke-direct {p0}, LX/8vi;->e()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setScrollable(Z)V

    .line 1417754
    invoke-virtual {p0}, LX/8vi;->getSelectedView()Landroid/view/View;

    move-result-object v0

    .line 1417755
    if-eqz v0, :cond_0

    .line 1417756
    invoke-virtual {v0}, Landroid/view/View;->isEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setEnabled(Z)V

    .line 1417757
    :cond_0
    return-void
.end method

.method public onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 1417758
    invoke-virtual {p0}, LX/8vi;->getWidth()I

    move-result v0

    iput v0, p0, LX/8vi;->a:I

    .line 1417759
    return-void
.end method

.method public final onRequestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 1417760
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onRequestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1417761
    invoke-static {}, Landroid/view/accessibility/AccessibilityEvent;->obtain()Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    .line 1417762
    invoke-virtual {p0, v0}, LX/8vi;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1417763
    invoke-virtual {p1, v0}, Landroid/view/View;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    .line 1417764
    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->appendRecord(Landroid/view/accessibility/AccessibilityRecord;)V

    .line 1417765
    const/4 v0, 0x1

    .line 1417766
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final removeAllViews()V
    .locals 2

    .prologue
    .line 1417767
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "removeAllViews() is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final removeView(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1417768
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "removeView(View) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final removeViewAt(I)V
    .locals 2

    .prologue
    .line 1417769
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "removeViewAt(int) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public abstract setAdapter(Landroid/widget/Adapter;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method

.method public setEmptyView(Landroid/view/View;)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 1417770
    iput-object p1, p0, LX/8vi;->b:Landroid/view/View;

    .line 1417771
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_0

    .line 1417772
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getImportantForAccessibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 1417773
    invoke-virtual {p1, v0}, Landroid/view/View;->setImportantForAccessibility(I)V

    .line 1417774
    :cond_0
    invoke-virtual {p0}, LX/8vi;->getAdapter()Landroid/widget/Adapter;

    move-result-object v1

    .line 1417775
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/widget/Adapter;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1417776
    :cond_1
    :goto_0
    invoke-direct {p0, v0}, LX/8vi;->a(Z)V

    .line 1417777
    return-void

    .line 1417778
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setFocusable(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1417779
    invoke-virtual {p0}, LX/8vi;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    .line 1417780
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    move v0, v2

    .line 1417781
    :goto_0
    iput-boolean p1, p0, LX/8vi;->c:Z

    .line 1417782
    if-nez p1, :cond_1

    .line 1417783
    iput-boolean v1, p0, LX/8vi;->d:Z

    .line 1417784
    :cond_1
    if-eqz p1, :cond_4

    if-eqz v0, :cond_2

    .line 1417785
    goto :goto_2

    :cond_2
    :goto_1
    invoke-super {p0, v2}, Landroid/view/ViewGroup;->setFocusable(Z)V

    .line 1417786
    return-void

    :cond_3
    move v0, v1

    .line 1417787
    goto :goto_0

    :cond_4
    :goto_2
    move v2, v1

    .line 1417788
    goto :goto_1
.end method

.method public setFocusableInTouchMode(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1417789
    invoke-virtual {p0}, LX/8vi;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    .line 1417790
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    move v0, v2

    .line 1417791
    :goto_0
    iput-boolean p1, p0, LX/8vi;->d:Z

    .line 1417792
    if-eqz p1, :cond_1

    .line 1417793
    iput-boolean v2, p0, LX/8vi;->c:Z

    .line 1417794
    :cond_1
    if-eqz p1, :cond_4

    if-eqz v0, :cond_2

    .line 1417795
    goto :goto_2

    :cond_2
    :goto_1
    invoke-super {p0, v2}, Landroid/view/ViewGroup;->setFocusableInTouchMode(Z)V

    .line 1417796
    return-void

    :cond_3
    move v0, v1

    .line 1417797
    goto :goto_0

    :cond_4
    :goto_2
    move v2, v1

    .line 1417798
    goto :goto_1
.end method

.method public setNextSelectedPositionInt(I)V
    .locals 2

    .prologue
    .line 1417799
    iput p1, p0, LX/8vi;->ak:I

    .line 1417800
    invoke-virtual {p0, p1}, LX/8vi;->d(I)J

    move-result-wide v0

    iput-wide v0, p0, LX/8vi;->al:J

    .line 1417801
    iget-boolean v0, p0, LX/8vi;->ad:Z

    if-eqz v0, :cond_0

    iget v0, p0, LX/8vi;->ae:I

    if-nez v0, :cond_0

    if-ltz p1, :cond_0

    .line 1417802
    iput p1, p0, LX/8vi;->aa:I

    .line 1417803
    iget-wide v0, p0, LX/8vi;->al:J

    iput-wide v0, p0, LX/8vi;->ab:J

    .line 1417804
    :cond_0
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 1417816
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Don\'t call setOnClickListener for an AdapterView. You probably want setOnItemClickListener instead"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setOnItemLongClickListener(LX/8vl;)V
    .locals 1

    .prologue
    .line 1417817
    invoke-virtual {p0}, LX/8vi;->isLongClickable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1417818
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/8vi;->setLongClickable(Z)V

    .line 1417819
    :cond_0
    iput-object p1, p0, LX/8vi;->ai:LX/8vl;

    .line 1417820
    return-void
.end method

.method public setOnItemSelectedListener(LX/8vm;)V
    .locals 0

    .prologue
    .line 1417821
    iput-object p1, p0, LX/8vi;->ag:LX/8vm;

    .line 1417822
    return-void
.end method

.method public setSelectedPositionInt(I)V
    .locals 2

    .prologue
    .line 1417823
    iput p1, p0, LX/8vi;->am:I

    .line 1417824
    invoke-virtual {p0, p1}, LX/8vi;->d(I)J

    move-result-wide v0

    iput-wide v0, p0, LX/8vi;->an:J

    .line 1417825
    return-void
.end method

.method public abstract setSelection(I)V
.end method
