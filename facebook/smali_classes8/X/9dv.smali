.class public final LX/9dv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8CF;


# instance fields
.field public final synthetic a:LX/9dx;


# direct methods
.method public constructor <init>(LX/9dx;)V
    .locals 0

    .prologue
    .line 1518861
    iput-object p1, p0, LX/9dv;->a:LX/9dx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 1518855
    iget-object v0, p0, LX/9dv;->a:LX/9dx;

    iget-object v0, v0, LX/9dx;->b:Lcom/facebook/messaging/doodle/ColourIndicator;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/doodle/ColourIndicator;->setColour(I)V

    .line 1518856
    iget-object v0, p0, LX/9dv;->a:LX/9dx;

    iget-object v0, v0, LX/9dx;->b:Lcom/facebook/messaging/doodle/ColourIndicator;

    invoke-virtual {v0}, Lcom/facebook/messaging/doodle/ColourIndicator;->a()V

    .line 1518857
    iget-object v0, p0, LX/9dv;->a:LX/9dx;

    iget-object v0, v0, LX/9dx;->a:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawingview/DrawingView;->setColour(I)V

    .line 1518858
    iget-object v0, p0, LX/9dv;->a:LX/9dx;

    const/4 v1, 0x1

    .line 1518859
    iput-boolean v1, v0, LX/9dx;->f:Z

    .line 1518860
    return-void
.end method

.method public final a(IFFF)V
    .locals 2

    .prologue
    .line 1518851
    iget-object v0, p0, LX/9dv;->a:LX/9dx;

    iget-object v0, v0, LX/9dx;->b:Lcom/facebook/messaging/doodle/ColourIndicator;

    iget-object v1, p0, LX/9dv;->a:LX/9dx;

    iget v1, v1, LX/9dx;->h:I

    int-to-float v1, v1

    sub-float v1, p2, v1

    invoke-virtual {v0, p1, v1, p3, p4}, Lcom/facebook/messaging/doodle/ColourIndicator;->a(IFFF)V

    .line 1518852
    iget-object v0, p0, LX/9dv;->a:LX/9dx;

    iget-object v0, v0, LX/9dx;->a:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v0, p4}, Lcom/facebook/drawingview/DrawingView;->setStrokeWidth(F)V

    .line 1518853
    iget-object v0, p0, LX/9dv;->a:LX/9dx;

    iget-object v0, v0, LX/9dx;->a:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawingview/DrawingView;->setColour(I)V

    .line 1518854
    return-void
.end method
