.class public final LX/AMK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Ljava/util/Map;

.field public final synthetic b:Ljava/util/concurrent/atomic/AtomicLong;

.field public final synthetic c:Ljava/util/concurrent/atomic/AtomicLong;

.field public final synthetic d:LX/AZo;

.field public final synthetic e:LX/AMO;


# direct methods
.method public constructor <init>(LX/AMO;Ljava/util/Map;Ljava/util/concurrent/atomic/AtomicLong;Ljava/util/concurrent/atomic/AtomicLong;LX/AZo;)V
    .locals 0

    .prologue
    .line 1666075
    iput-object p1, p0, LX/AMK;->e:LX/AMO;

    iput-object p2, p0, LX/AMK;->a:Ljava/util/Map;

    iput-object p3, p0, LX/AMK;->b:Ljava/util/concurrent/atomic/AtomicLong;

    iput-object p4, p0, LX/AMK;->c:Ljava/util/concurrent/atomic/AtomicLong;

    iput-object p5, p0, LX/AMK;->d:LX/AZo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/AMT;J)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1666076
    iget-object v0, p0, LX/AMK;->a:Ljava/util/Map;

    .line 1666077
    iget-object v1, p1, LX/AMT;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1666078
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1666079
    iget-object v0, p0, LX/AMK;->b:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    long-to-double v0, v0

    .line 1666080
    iget-object v2, p0, LX/AMK;->c:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2, p2, p3}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    move-result-wide v2

    .line 1666081
    cmp-long v4, v2, v4

    if-eqz v4, :cond_0

    .line 1666082
    iget-object v4, p0, LX/AMK;->d:LX/AZo;

    long-to-double v2, v2

    div-double/2addr v0, v2

    invoke-virtual {v4, v0, v1}, LX/AZo;->a(D)V

    .line 1666083
    :cond_0
    return-void
.end method
