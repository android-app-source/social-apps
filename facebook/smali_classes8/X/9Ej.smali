.class public final LX/9Ej;
.super LX/451;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/451",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/auth/viewercontext/ViewerContext;

.field public final synthetic b:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic c:Lcom/facebook/feedback/ui/SingletonFeedbackController;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/SingletonFeedbackController;Lcom/facebook/auth/viewercontext/ViewerContext;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0

    .prologue
    .line 1457325
    iput-object p1, p0, LX/9Ej;->c:Lcom/facebook/feedback/ui/SingletonFeedbackController;

    iput-object p2, p0, LX/9Ej;->a:Lcom/facebook/auth/viewercontext/ViewerContext;

    iput-object p3, p0, LX/9Ej;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, LX/451;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1457326
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1457327
    if-eqz p1, :cond_0

    .line 1457328
    iget-object v0, p0, LX/9Ej;->a:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1457329
    iget-object v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->b:Ljava/lang/String;

    move-object v0, v1

    .line 1457330
    invoke-static {p1, v0}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;)V

    .line 1457331
    :cond_0
    iget-object v0, p0, LX/9Ej;->b:Lcom/google/common/util/concurrent/SettableFuture;

    const v1, -0x14c9b52

    invoke-static {v0, p1, v1}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 1457332
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1457333
    iget-object v0, p0, LX/9Ej;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 1457334
    return-void
.end method
