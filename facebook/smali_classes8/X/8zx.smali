.class public final enum LX/8zx;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8zx;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8zx;

.field public static final enum GENERIC_ERROR:LX/8zx;

.field public static final enum NETWORK_ERROR:LX/8zx;

.field public static final enum NORMAL:LX/8zx;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1428340
    new-instance v0, LX/8zx;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v2}, LX/8zx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8zx;->NORMAL:LX/8zx;

    .line 1428341
    new-instance v0, LX/8zx;

    const-string v1, "NETWORK_ERROR"

    invoke-direct {v0, v1, v3}, LX/8zx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8zx;->NETWORK_ERROR:LX/8zx;

    .line 1428342
    new-instance v0, LX/8zx;

    const-string v1, "GENERIC_ERROR"

    invoke-direct {v0, v1, v4}, LX/8zx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8zx;->GENERIC_ERROR:LX/8zx;

    .line 1428343
    const/4 v0, 0x3

    new-array v0, v0, [LX/8zx;

    sget-object v1, LX/8zx;->NORMAL:LX/8zx;

    aput-object v1, v0, v2

    sget-object v1, LX/8zx;->NETWORK_ERROR:LX/8zx;

    aput-object v1, v0, v3

    sget-object v1, LX/8zx;->GENERIC_ERROR:LX/8zx;

    aput-object v1, v0, v4

    sput-object v0, LX/8zx;->$VALUES:[LX/8zx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1428344
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8zx;
    .locals 1

    .prologue
    .line 1428345
    const-class v0, LX/8zx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8zx;

    return-object v0
.end method

.method public static values()[LX/8zx;
    .locals 1

    .prologue
    .line 1428346
    sget-object v0, LX/8zx;->$VALUES:[LX/8zx;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8zx;

    return-object v0
.end method
