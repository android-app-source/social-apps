.class public final LX/904;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/903;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;)V
    .locals 0

    .prologue
    .line 1428401
    iput-object p1, p0, LX/904;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 1428402
    iget-object v0, p0, LX/904;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;

    invoke-static {v0}, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->l(Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;)Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->a(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)LX/937;

    move-result-object v0

    .line 1428403
    iput-object v5, v0, LX/937;->k:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1428404
    move-object v0, v0

    .line 1428405
    invoke-virtual {v0}, LX/937;->a()Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    move-result-object v0

    .line 1428406
    iget-object v1, p0, LX/904;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;

    .line 1428407
    iget-object v2, v1, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->a:LX/90T;

    invoke-interface {v2, v0}, LX/90T;->a(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)V

    .line 1428408
    iget-object v1, p0, LX/904;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;

    invoke-virtual {v1}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, -0x1

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    const-string v4, "minutiae_object"

    .line 1428409
    iget-object v6, v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->j:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-object v0, v6

    .line 1428410
    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1428411
    iget-object v0, p0, LX/904;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;

    sget-object v1, LX/91g;->LOADED:LX/91g;

    invoke-static {v0, v1, v5}, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->a$redex0(Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;LX/91g;Landroid/view/View$OnClickListener;)V

    .line 1428412
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1428413
    iget-object v0, p0, LX/904;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;

    invoke-static {v0}, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->l(Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;)Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    move-result-object v0

    .line 1428414
    iget-object v1, p0, LX/904;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;

    .line 1428415
    iget-object p0, v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->j:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-object v0, p0

    .line 1428416
    invoke-static {v1, v0}, Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;->a$redex0(Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;Lcom/facebook/composer/minutiae/model/MinutiaeObject;)V

    .line 1428417
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1428418
    iget-object v0, p0, LX/904;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeFeelingsFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 1428419
    return-void
.end method
