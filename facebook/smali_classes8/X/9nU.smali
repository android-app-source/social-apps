.class public LX/9nU;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1537354
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(FLcom/facebook/csslayout/YogaMeasureMode;)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1537355
    sget-object v0, Lcom/facebook/csslayout/YogaMeasureMode;->EXACTLY:Lcom/facebook/csslayout/YogaMeasureMode;

    if-ne p1, v0, :cond_0

    .line 1537356
    float-to-int v0, p0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 1537357
    :goto_0
    return v0

    .line 1537358
    :cond_0
    sget-object v0, Lcom/facebook/csslayout/YogaMeasureMode;->AT_MOST:Lcom/facebook/csslayout/YogaMeasureMode;

    if-ne p1, v0, :cond_1

    .line 1537359
    float-to-int v0, p0

    const/high16 v1, -0x80000000

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_0

    .line 1537360
    :cond_1
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_0
.end method
