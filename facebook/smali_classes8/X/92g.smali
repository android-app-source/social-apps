.class public final LX/92g;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8zq;


# instance fields
.field public final synthetic a:LX/8zq;

.field public final synthetic b:LX/92o;


# direct methods
.method public constructor <init>(LX/92o;LX/8zq;)V
    .locals 0

    .prologue
    .line 1432517
    iput-object p1, p0, LX/92g;->b:LX/92o;

    iput-object p2, p0, LX/92g;->a:LX/8zq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0Px;LX/0ta;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/92e;",
            ">;",
            "LX/0ta;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1432518
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1432519
    iget-object v0, p0, LX/92g;->a:LX/8zq;

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "No verbs returned from server"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, LX/8zq;->a(Ljava/lang/Throwable;)V

    .line 1432520
    :goto_0
    return-void

    .line 1432521
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1432522
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/92e;

    .line 1432523
    if-eqz v0, :cond_1

    .line 1432524
    iget-object v4, v0, LX/92e;->a:LX/5LG;

    move-object v4, v4

    .line 1432525
    if-eqz v4, :cond_1

    .line 1432526
    iget-object v4, v0, LX/92e;->a:LX/5LG;

    move-object v4, v4

    .line 1432527
    invoke-interface {v4}, LX/5LG;->l()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    const-string v4, "383634835006146"

    .line 1432528
    iget-object v5, v0, LX/92e;->a:LX/5LG;

    move-object v5, v5

    .line 1432529
    invoke-interface {v5}, LX/5LG;->l()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1432530
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1432531
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1432532
    :cond_2
    iget-object v0, p0, LX/92g;->a:LX/8zq;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1, p2}, LX/8zq;->a(LX/0Px;LX/0ta;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1432533
    iget-object v0, p0, LX/92g;->a:LX/8zq;

    invoke-interface {v0, p1}, LX/8zq;->a(Ljava/lang/Throwable;)V

    .line 1432534
    return-void
.end method
