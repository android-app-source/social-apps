.class public final LX/AHL;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 24

    .prologue
    .line 1655232
    const/16 v18, 0x0

    .line 1655233
    const-wide/16 v16, 0x0

    .line 1655234
    const/4 v15, 0x0

    .line 1655235
    const/4 v14, 0x0

    .line 1655236
    const/4 v13, 0x0

    .line 1655237
    const/4 v12, 0x0

    .line 1655238
    const/4 v11, 0x0

    .line 1655239
    const/4 v10, 0x0

    .line 1655240
    const/4 v9, 0x0

    .line 1655241
    const/4 v8, 0x0

    .line 1655242
    const/4 v7, 0x0

    .line 1655243
    const/4 v6, 0x0

    .line 1655244
    const/4 v5, 0x0

    .line 1655245
    const/4 v4, 0x0

    .line 1655246
    const/4 v3, 0x0

    .line 1655247
    const/4 v2, 0x0

    .line 1655248
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_13

    .line 1655249
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1655250
    const/4 v2, 0x0

    .line 1655251
    :goto_0
    return v2

    .line 1655252
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_d

    .line 1655253
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1655254
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1655255
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v21, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v21

    if-eq v6, v0, :cond_0

    if-eqz v2, :cond_0

    .line 1655256
    const-string v6, "__type__"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    const-string v6, "__typename"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1655257
    :cond_1
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v2

    move v7, v2

    goto :goto_1

    .line 1655258
    :cond_2
    const-string v6, "created_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1655259
    const/4 v2, 0x1

    .line 1655260
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 1655261
    :cond_3
    const-string v6, "height"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1655262
    const/4 v2, 0x1

    .line 1655263
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v11, v2

    move/from16 v20, v6

    goto :goto_1

    .line 1655264
    :cond_4
    const-string v6, "image"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1655265
    invoke-static/range {p0 .. p1}, LX/AHH;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v19, v2

    goto :goto_1

    .line 1655266
    :cond_5
    const-string v6, "lowres"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1655267
    invoke-static/range {p0 .. p1}, LX/AHI;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v18, v2

    goto :goto_1

    .line 1655268
    :cond_6
    const-string v6, "message"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1655269
    invoke-static/range {p0 .. p1}, LX/AHJ;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 1655270
    :cond_7
    const-string v6, "owner"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1655271
    invoke-static/range {p0 .. p1}, LX/AHK;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v16, v2

    goto/16 :goto_1

    .line 1655272
    :cond_8
    const-string v6, "playable_duration"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 1655273
    const/4 v2, 0x1

    .line 1655274
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v10, v2

    move v15, v6

    goto/16 :goto_1

    .line 1655275
    :cond_9
    const-string v6, "playable_url"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1655276
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 1655277
    :cond_a
    const-string v6, "video_full_size"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 1655278
    const/4 v2, 0x1

    .line 1655279
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v9, v2

    move v13, v6

    goto/16 :goto_1

    .line 1655280
    :cond_b
    const-string v6, "width"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1655281
    const/4 v2, 0x1

    .line 1655282
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v8, v2

    move v12, v6

    goto/16 :goto_1

    .line 1655283
    :cond_c
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1655284
    :cond_d
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1655285
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1655286
    if-eqz v3, :cond_e

    .line 1655287
    const/4 v3, 0x1

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1655288
    :cond_e
    if-eqz v11, :cond_f

    .line 1655289
    const/4 v2, 0x2

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 1655290
    :cond_f
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1655291
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1655292
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1655293
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1655294
    if-eqz v10, :cond_10

    .line 1655295
    const/4 v2, 0x7

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15, v3}, LX/186;->a(III)V

    .line 1655296
    :cond_10
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1655297
    if-eqz v9, :cond_11

    .line 1655298
    const/16 v2, 0x9

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13, v3}, LX/186;->a(III)V

    .line 1655299
    :cond_11
    if-eqz v8, :cond_12

    .line 1655300
    const/16 v2, 0xa

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12, v3}, LX/186;->a(III)V

    .line 1655301
    :cond_12
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_13
    move/from16 v19, v14

    move/from16 v20, v15

    move v14, v9

    move v15, v10

    move v10, v4

    move v9, v3

    move v3, v6

    move/from16 v22, v8

    move v8, v2

    move/from16 v23, v5

    move-wide/from16 v4, v16

    move/from16 v16, v11

    move/from16 v17, v12

    move v12, v7

    move/from16 v11, v23

    move/from16 v7, v18

    move/from16 v18, v13

    move/from16 v13, v22

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 1655302
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1655303
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1655304
    if-eqz v0, :cond_0

    .line 1655305
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1655306
    invoke-static {p0, p1, v3, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1655307
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1655308
    cmp-long v2, v0, v4

    if-eqz v2, :cond_1

    .line 1655309
    const-string v2, "created_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1655310
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1655311
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1655312
    if-eqz v0, :cond_2

    .line 1655313
    const-string v1, "height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1655314
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1655315
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1655316
    if-eqz v0, :cond_6

    .line 1655317
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1655318
    const/4 v4, 0x0

    .line 1655319
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1655320
    invoke-virtual {p0, v0, v4, v4}, LX/15i;->a(III)I

    move-result v1

    .line 1655321
    if-eqz v1, :cond_3

    .line 1655322
    const-string v2, "height"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1655323
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 1655324
    :cond_3
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1655325
    if-eqz v1, :cond_4

    .line 1655326
    const-string v2, "uri"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1655327
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1655328
    :cond_4
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1, v4}, LX/15i;->a(III)I

    move-result v1

    .line 1655329
    if-eqz v1, :cond_5

    .line 1655330
    const-string v2, "width"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1655331
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 1655332
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1655333
    :cond_6
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1655334
    if-eqz v0, :cond_a

    .line 1655335
    const-string v1, "lowres"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1655336
    const/4 v4, 0x0

    .line 1655337
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1655338
    invoke-virtual {p0, v0, v4, v4}, LX/15i;->a(III)I

    move-result v1

    .line 1655339
    if-eqz v1, :cond_7

    .line 1655340
    const-string v2, "height"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1655341
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 1655342
    :cond_7
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1655343
    if-eqz v1, :cond_8

    .line 1655344
    const-string v2, "uri"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1655345
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1655346
    :cond_8
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1, v4}, LX/15i;->a(III)I

    move-result v1

    .line 1655347
    if-eqz v1, :cond_9

    .line 1655348
    const-string v2, "width"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1655349
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 1655350
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1655351
    :cond_a
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1655352
    if-eqz v0, :cond_b

    .line 1655353
    const-string v1, "message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1655354
    invoke-static {p0, v0, p2}, LX/AHJ;->a(LX/15i;ILX/0nX;)V

    .line 1655355
    :cond_b
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1655356
    if-eqz v0, :cond_c

    .line 1655357
    const-string v1, "owner"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1655358
    invoke-static {p0, v0, p2, p3}, LX/AHK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1655359
    :cond_c
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1655360
    if-eqz v0, :cond_d

    .line 1655361
    const-string v1, "playable_duration"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1655362
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1655363
    :cond_d
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1655364
    if-eqz v0, :cond_e

    .line 1655365
    const-string v1, "playable_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1655366
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1655367
    :cond_e
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1655368
    if-eqz v0, :cond_f

    .line 1655369
    const-string v1, "video_full_size"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1655370
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1655371
    :cond_f
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1655372
    if-eqz v0, :cond_10

    .line 1655373
    const-string v1, "width"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1655374
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1655375
    :cond_10
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1655376
    return-void
.end method
