.class public LX/8x8;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Ljava/lang/Boolean;)V
    .locals 0
    .param p1    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1423550
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1423551
    iput-object p1, p0, LX/8x8;->a:Ljava/lang/Boolean;

    .line 1423552
    return-void
.end method

.method public static a(Ljava/lang/Boolean;I)I
    .locals 3

    .prologue
    .line 1423553
    packed-switch p1, :pswitch_data_0

    .line 1423554
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown badge type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1423555
    :pswitch_0
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f08110e

    .line 1423556
    :goto_0
    return v0

    .line 1423557
    :cond_0
    const v0, 0x7f08110d

    goto :goto_0

    .line 1423558
    :pswitch_1
    const v0, 0x7f08110f

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(LX/0QB;)LX/8x8;
    .locals 3

    .prologue
    .line 1423559
    const-class v1, LX/8x8;

    monitor-enter v1

    .line 1423560
    :try_start_0
    sget-object v0, LX/8x8;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1423561
    sput-object v2, LX/8x8;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1423562
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1423563
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/8x8;->b(LX/0QB;)LX/8x8;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1423564
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8x8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1423565
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1423566
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/8x8;
    .locals 2

    .prologue
    .line 1423567
    new-instance v1, LX/8x8;

    invoke-static {p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-direct {v1, v0}, LX/8x8;-><init>(Ljava/lang/Boolean;)V

    .line 1423568
    return-object v1
.end method


# virtual methods
.method public final a(LX/1De;I)LX/1Dg;
    .locals 3
    .param p2    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param

    .prologue
    .line 1423569
    iget-object v0, p0, LX/8x8;->a:Ljava/lang/Boolean;

    invoke-static {v0, p2}, LX/8x8;->a(Ljava/lang/Boolean;I)I

    move-result v0

    .line 1423570
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1ne;->h(I)LX/1ne;

    move-result-object v1

    sget-object v2, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    invoke-virtual {v1, v2}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v1

    const v2, 0x7f0a0048

    invoke-virtual {v1, v2}, LX/1ne;->n(I)LX/1ne;

    move-result-object v1

    const v2, 0x7f0b0050

    invoke-virtual {v1, v2}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    invoke-interface {v1, v0}, LX/1Di;->A(I)LX/1Di;

    move-result-object v0

    const v1, 0x7f020ad6

    invoke-interface {v0, v1}, LX/1Di;->x(I)LX/1Di;

    move-result-object v0

    const/4 v1, 0x7

    const/4 v2, 0x6

    invoke-interface {v0, v1, v2}, LX/1Di;->h(II)LX/1Di;

    move-result-object v0

    const/4 v1, 0x4

    const/16 v2, 0xf

    invoke-interface {v0, v1, v2}, LX/1Di;->h(II)LX/1Di;

    move-result-object v0

    const/4 v1, 0x5

    const/16 v2, 0x8

    invoke-interface {v0, v1, v2}, LX/1Di;->h(II)LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0
.end method
