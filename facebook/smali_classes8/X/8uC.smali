.class public LX/8uC;
.super LX/8u8;
.source ""


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 1414565
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/8uC;-><init>(ILandroid/util/DisplayMetrics;)V

    .line 1414566
    return-void
.end method

.method public constructor <init>(ILandroid/util/DisplayMetrics;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 1414567
    invoke-direct {p0, p2}, LX/8u8;-><init>(Landroid/util/DisplayMetrics;)V

    .line 1414568
    if-nez p1, :cond_0

    .line 1414569
    const/4 v0, 0x0

    iput v0, p0, LX/8uC;->a:F

    .line 1414570
    :goto_0
    return-void

    .line 1414571
    :cond_0
    const/high16 v0, 0x41b00000    # 22.0f

    invoke-static {v3, v0, p2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    .line 1414572
    int-to-float v1, p1

    const/high16 v2, 0x41980000    # 19.0f

    invoke-static {v3, v2, p2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, LX/8uC;->a:F

    goto :goto_0
.end method


# virtual methods
.method public drawLeadingMargin(Landroid/graphics/Canvas;Landroid/graphics/Paint;IIIIILjava/lang/CharSequence;IIZLandroid/text/Layout;)V
    .locals 0

    .prologue
    .line 1414573
    return-void
.end method
