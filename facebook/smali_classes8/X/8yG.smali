.class public LX/8yG;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/8yG;


# instance fields
.field public final a:LX/0if;

.field public b:Z


# direct methods
.method public constructor <init>(LX/0if;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1425179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1425180
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/8yG;->b:Z

    .line 1425181
    iput-object p1, p0, LX/8yG;->a:LX/0if;

    .line 1425182
    return-void
.end method

.method public static a(LX/0QB;)LX/8yG;
    .locals 4

    .prologue
    .line 1425183
    sget-object v0, LX/8yG;->c:LX/8yG;

    if-nez v0, :cond_1

    .line 1425184
    const-class v1, LX/8yG;

    monitor-enter v1

    .line 1425185
    :try_start_0
    sget-object v0, LX/8yG;->c:LX/8yG;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1425186
    if-eqz v2, :cond_0

    .line 1425187
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1425188
    new-instance p0, LX/8yG;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v3

    check-cast v3, LX/0if;

    invoke-direct {p0, v3}, LX/8yG;-><init>(LX/0if;)V

    .line 1425189
    move-object v0, p0

    .line 1425190
    sput-object v0, LX/8yG;->c:LX/8yG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1425191
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1425192
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1425193
    :cond_1
    sget-object v0, LX/8yG;->c:LX/8yG;

    return-object v0

    .line 1425194
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1425195
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final d()V
    .locals 3

    .prologue
    .line 1425196
    iget-object v0, p0, LX/8yG;->a:LX/0if;

    sget-object v1, LX/0ig;->aA:LX/0ih;

    const-string v2, "friends_fetch_empty"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1425197
    return-void
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 1425198
    iget-object v0, p0, LX/8yG;->a:LX/0if;

    sget-object v1, LX/0ig;->aA:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->c(LX/0ih;)V

    .line 1425199
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/8yG;->b:Z

    .line 1425200
    return-void
.end method
