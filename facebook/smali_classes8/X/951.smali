.class public final LX/951;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/contacts/upload/ContinuousContactUploadClient;


# direct methods
.method public constructor <init>(Lcom/facebook/contacts/upload/ContinuousContactUploadClient;)V
    .locals 0

    .prologue
    .line 1436327
    iput-object p1, p0, LX/951;->a:Lcom/facebook/contacts/upload/ContinuousContactUploadClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1436328
    iget-object v0, p0, LX/951;->a:Lcom/facebook/contacts/upload/ContinuousContactUploadClient;

    iget-object v0, v0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->e:LX/2UP;

    .line 1436329
    iget-object v2, v0, LX/2UP;->a:LX/0Zb;

    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "contacts_upload_failed"

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1436330
    const-string v4, "contacts_upload"

    move-object v4, v4

    .line 1436331
    iput-object v4, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1436332
    move-object v3, v3

    .line 1436333
    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1436334
    iget-object v0, p0, LX/951;->a:Lcom/facebook/contacts/upload/ContinuousContactUploadClient;

    iget-object v0, v0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->e:LX/2UP;

    sget-object v2, LX/95K;->CCU_UPLOAD_FAIL:LX/95K;

    invoke-virtual {v0, v2}, LX/2UP;->a(LX/95K;)V

    .line 1436335
    instance-of v0, p1, Lcom/facebook/fbservice/service/ServiceException;

    if-eqz v0, :cond_0

    .line 1436336
    check-cast p1, Lcom/facebook/fbservice/service/ServiceException;

    .line 1436337
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1436338
    iget-object v2, v0, Lcom/facebook/fbservice/service/OperationResult;->errorCode:LX/1nY;

    move-object v0, v2

    .line 1436339
    sget-object v2, LX/1nY;->API_ERROR:LX/1nY;

    if-ne v0, v2, :cond_0

    .line 1436340
    iget-object v0, p0, LX/951;->a:Lcom/facebook/contacts/upload/ContinuousContactUploadClient;

    iget-object v0, v0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->f:LX/2UQ;

    invoke-virtual {v0}, LX/2UQ;->a()V

    .line 1436341
    iget-object v0, p0, LX/951;->a:Lcom/facebook/contacts/upload/ContinuousContactUploadClient;

    iget-object v0, v0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->e:LX/2UP;

    sget-object v2, LX/95K;->SNAPSHOT_AND_IMPORT_ID_DELETED:LX/95K;

    invoke-virtual {v0, v2}, LX/2UP;->a(LX/95K;)V

    .line 1436342
    const/4 v2, 0x1

    .line 1436343
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1436344
    invoke-virtual {v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/http/protocol/ApiErrorResult;

    .line 1436345
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v0

    const/16 v3, 0x1716

    if-ne v0, v3, :cond_1

    move v0, v1

    .line 1436346
    :goto_0
    if-eqz v0, :cond_0

    .line 1436347
    iget-object v0, p0, LX/951;->a:Lcom/facebook/contacts/upload/ContinuousContactUploadClient;

    iget-object v0, v0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->g:LX/30I;

    invoke-virtual {v0, v1}, LX/30I;->a(Z)V

    .line 1436348
    iget-object v0, p0, LX/951;->a:Lcom/facebook/contacts/upload/ContinuousContactUploadClient;

    iget-object v0, v0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->e:LX/2UP;

    sget-object v1, LX/95K;->TURN_OFF_CCU_AFTER_EXCEPTION:LX/95K;

    invoke-virtual {v0, v1}, LX/2UP;->a(LX/95K;)V

    .line 1436349
    :cond_0
    iget-object v0, p0, LX/951;->a:Lcom/facebook/contacts/upload/ContinuousContactUploadClient;

    iget-object v0, v0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->e:LX/2UP;

    invoke-virtual {v0}, LX/2UP;->b()V

    .line 1436350
    return-void

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1436351
    iget-object v0, p0, LX/951;->a:Lcom/facebook/contacts/upload/ContinuousContactUploadClient;

    invoke-static {v0}, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->a$redex0(Lcom/facebook/contacts/upload/ContinuousContactUploadClient;)V

    .line 1436352
    iget-object v0, p0, LX/951;->a:Lcom/facebook/contacts/upload/ContinuousContactUploadClient;

    iget-object v0, v0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->e:LX/2UP;

    .line 1436353
    iget-object v1, v0, LX/2UP;->a:LX/0Zb;

    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "contacts_upload_succeeded"

    invoke-direct {v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1436354
    const-string p1, "contacts_upload"

    move-object p1, p1

    .line 1436355
    iput-object p1, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1436356
    move-object v2, v2

    .line 1436357
    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1436358
    iget-object v0, p0, LX/951;->a:Lcom/facebook/contacts/upload/ContinuousContactUploadClient;

    iget-object v0, v0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->e:LX/2UP;

    sget-object v1, LX/95K;->CCU_UPLOAD_SUCCESSS:LX/95K;

    invoke-virtual {v0, v1}, LX/2UP;->a(LX/95K;)V

    .line 1436359
    iget-object v0, p0, LX/951;->a:Lcom/facebook/contacts/upload/ContinuousContactUploadClient;

    iget-object v0, v0, Lcom/facebook/contacts/upload/ContinuousContactUploadClient;->e:LX/2UP;

    invoke-virtual {v0}, LX/2UP;->b()V

    .line 1436360
    return-void
.end method
