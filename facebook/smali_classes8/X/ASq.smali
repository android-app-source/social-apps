.class public final LX/ASq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;)V
    .locals 0

    .prologue
    .line 1674537
    iput-object p1, p0, LX/ASq;->a:Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x1c7222a5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1674538
    iget-object v0, p0, LX/ASq;->a:Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->j:Lcom/facebook/composer/attachments/ComposerAttachment;

    if-eqz v0, :cond_0

    .line 1674539
    iget-object v0, p0, LX/ASq;->a:Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1674540
    iget-object v2, p0, LX/ASq;->a:Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;

    iget-object v2, v2, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->g:LX/3l1;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0j0;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    .line 1674541
    iput-object v0, v2, LX/3l1;->b:Ljava/lang/String;

    .line 1674542
    iget-object v0, p0, LX/ASq;->a:Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->g:LX/3l1;

    .line 1674543
    sget-object v2, LX/BOR;->SLIDESHOW_REMOVED:LX/BOR;

    invoke-static {v2}, LX/3l1;->a(LX/BOR;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-static {v0, v2}, LX/3l1;->a(LX/3l1;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1674544
    iget-object v0, p0, LX/ASq;->a:Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->d:LX/ATL;

    iget-object v2, p0, LX/ASq;->a:Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;

    iget-object v2, v2, Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;->j:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0, v2}, LX/ATL;->c(Lcom/facebook/composer/attachments/ComposerAttachment;)V

    .line 1674545
    :cond_0
    const v0, 0x2663c9ba

    invoke-static {v3, v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
