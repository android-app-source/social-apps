.class public final LX/9PQ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 17

    .prologue
    .line 1484859
    const/4 v11, 0x0

    .line 1484860
    const/4 v10, 0x0

    .line 1484861
    const/4 v7, 0x0

    .line 1484862
    const-wide/16 v8, 0x0

    .line 1484863
    const/4 v6, 0x0

    .line 1484864
    const/4 v5, 0x0

    .line 1484865
    const/4 v4, 0x0

    .line 1484866
    const/4 v3, 0x0

    .line 1484867
    const/4 v2, 0x0

    .line 1484868
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->START_OBJECT:LX/15z;

    if-eq v12, v13, :cond_b

    .line 1484869
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1484870
    const/4 v2, 0x0

    .line 1484871
    :goto_0
    return v2

    .line 1484872
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v13, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v13, :cond_9

    .line 1484873
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1484874
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1484875
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v13, v14, :cond_0

    if-eqz v3, :cond_0

    .line 1484876
    const-string v13, "actors"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 1484877
    invoke-static/range {p0 .. p1}, LX/9PJ;->a(LX/15w;LX/186;)I

    move-result v3

    move v12, v3

    goto :goto_1

    .line 1484878
    :cond_1
    const-string v13, "attachments"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 1484879
    invoke-static/range {p0 .. p1}, LX/9PN;->a(LX/15w;LX/186;)I

    move-result v3

    move v7, v3

    goto :goto_1

    .line 1484880
    :cond_2
    const-string v13, "cache_id"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 1484881
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v6, v3

    goto :goto_1

    .line 1484882
    :cond_3
    const-string v13, "creation_time"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 1484883
    const/4 v2, 0x1

    .line 1484884
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    goto :goto_1

    .line 1484885
    :cond_4
    const-string v13, "feedback"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 1484886
    invoke-static/range {p0 .. p1}, LX/5Av;->a(LX/15w;LX/186;)I

    move-result v3

    move v11, v3

    goto :goto_1

    .line 1484887
    :cond_5
    const-string v13, "id"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 1484888
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v10, v3

    goto :goto_1

    .line 1484889
    :cond_6
    const-string v13, "message"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_7

    .line 1484890
    invoke-static/range {p0 .. p1}, LX/9PO;->a(LX/15w;LX/186;)I

    move-result v3

    move v9, v3

    goto/16 :goto_1

    .line 1484891
    :cond_7
    const-string v13, "to"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 1484892
    invoke-static/range {p0 .. p1}, LX/9PP;->a(LX/15w;LX/186;)I

    move-result v3

    move v8, v3

    goto/16 :goto_1

    .line 1484893
    :cond_8
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1484894
    :cond_9
    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1484895
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 1484896
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 1484897
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 1484898
    if-eqz v2, :cond_a

    .line 1484899
    const/4 v3, 0x3

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1484900
    :cond_a
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1484901
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1484902
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1484903
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1484904
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_b
    move v12, v11

    move v11, v6

    move v6, v7

    move v7, v10

    move v10, v5

    move-wide v15, v8

    move v9, v4

    move v8, v3

    move-wide v4, v15

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 1484905
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1484906
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1484907
    if-eqz v0, :cond_1

    .line 1484908
    const-string v1, "actors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1484909
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1484910
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 1484911
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v4

    invoke-static {p0, v4, p2, p3}, LX/9PJ;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1484912
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1484913
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1484914
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1484915
    if-eqz v0, :cond_3

    .line 1484916
    const-string v1, "attachments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1484917
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1484918
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v4

    if-ge v1, v4, :cond_2

    .line 1484919
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v4

    invoke-static {p0, v4, p2, p3}, LX/9PN;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1484920
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1484921
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1484922
    :cond_3
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1484923
    if-eqz v0, :cond_4

    .line 1484924
    const-string v1, "cache_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1484925
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1484926
    :cond_4
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1484927
    cmp-long v2, v0, v2

    if-eqz v2, :cond_5

    .line 1484928
    const-string v2, "creation_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1484929
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1484930
    :cond_5
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1484931
    if-eqz v0, :cond_6

    .line 1484932
    const-string v1, "feedback"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1484933
    invoke-static {p0, v0, p2, p3}, LX/5Av;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1484934
    :cond_6
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1484935
    if-eqz v0, :cond_7

    .line 1484936
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1484937
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1484938
    :cond_7
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1484939
    if-eqz v0, :cond_8

    .line 1484940
    const-string v1, "message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1484941
    invoke-static {p0, v0, p2}, LX/9PO;->a(LX/15i;ILX/0nX;)V

    .line 1484942
    :cond_8
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1484943
    if-eqz v0, :cond_9

    .line 1484944
    const-string v1, "to"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1484945
    invoke-static {p0, v0, p2}, LX/9PP;->a(LX/15i;ILX/0nX;)V

    .line 1484946
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1484947
    return-void
.end method
