.class public LX/9dx;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/drawingview/DrawingView;

.field public b:Lcom/facebook/messaging/doodle/ColourIndicator;

.field public c:Lcom/facebook/messaging/doodle/ColourPicker;

.field private d:Landroid/animation/ValueAnimator;

.field public e:Z

.field public f:Z

.field public g:LX/9eh;

.field public h:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1518866
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1518867
    invoke-virtual {p0}, LX/9dx;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1518868
    const v1, 0x7f030f3d

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1518869
    const v0, 0x7f0d0803

    invoke-static {v1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawingview/DrawingView;

    iput-object v0, p0, LX/9dx;->a:Lcom/facebook/drawingview/DrawingView;

    .line 1518870
    const v0, 0x7f0d0803

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawingview/DrawingView;

    iput-object v0, p0, LX/9dx;->a:Lcom/facebook/drawingview/DrawingView;

    .line 1518871
    iget-object v0, p0, LX/9dx;->a:Lcom/facebook/drawingview/DrawingView;

    new-instance p1, LX/9dt;

    invoke-direct {p1, p0}, LX/9dt;-><init>(LX/9dx;)V

    .line 1518872
    iput-object p1, v0, Lcom/facebook/drawingview/DrawingView;->d:LX/5Ne;

    .line 1518873
    const v0, 0x7f0d24e6

    invoke-static {v1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/doodle/ColourIndicator;

    iput-object v0, p0, LX/9dx;->b:Lcom/facebook/messaging/doodle/ColourIndicator;

    .line 1518874
    const v0, 0x7f0d24e7

    invoke-static {v1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/doodle/ColourPicker;

    iput-object v0, p0, LX/9dx;->c:Lcom/facebook/messaging/doodle/ColourPicker;

    .line 1518875
    iget-object v0, p0, LX/9dx;->c:Lcom/facebook/messaging/doodle/ColourPicker;

    .line 1518876
    new-instance v1, LX/9dv;

    invoke-direct {v1, p0}, LX/9dv;-><init>(LX/9dx;)V

    move-object v1, v1

    .line 1518877
    iput-object v1, v0, Lcom/facebook/messaging/doodle/ColourPicker;->c:LX/8CF;

    .line 1518878
    new-instance v0, LX/9du;

    invoke-direct {v0, p0}, LX/9du;-><init>(LX/9dx;)V

    invoke-virtual {p0, v0}, LX/9dx;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1518879
    invoke-virtual {p0}, LX/9dx;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0c0f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/9dx;->h:I

    .line 1518880
    return-void
.end method

.method public static a$redex0(LX/9dx;ILandroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1518881
    if-nez p1, :cond_2

    const/high16 v0, 0x3f800000    # 1.0f

    .line 1518882
    :goto_0
    iget-object v3, p0, LX/9dx;->d:Landroid/animation/ValueAnimator;

    if-eqz v3, :cond_0

    .line 1518883
    iget-object v3, p0, LX/9dx;->d:Landroid/animation/ValueAnimator;

    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->cancel()V

    .line 1518884
    :cond_0
    const/4 v3, 0x2

    new-array v3, v3, [F

    invoke-virtual {p2}, Landroid/view/View;->getAlpha()F

    move-result v4

    aput v4, v3, v2

    aput v0, v3, v1

    invoke-static {v3}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, LX/9dx;->d:Landroid/animation/ValueAnimator;

    .line 1518885
    iget-object v0, p0, LX/9dx;->d:Landroid/animation/ValueAnimator;

    new-instance v3, LX/9dw;

    invoke-direct {v3, p0, p2}, LX/9dw;-><init>(LX/9dx;Landroid/view/View;)V

    invoke-virtual {v0, v3}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1518886
    if-ne p1, v1, :cond_1

    .line 1518887
    iget-object v0, p0, LX/9dx;->d:Landroid/animation/ValueAnimator;

    const-wide/16 v4, 0xc8

    invoke-virtual {v0, v4, v5}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 1518888
    :cond_1
    if-nez p1, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {p2, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 1518889
    iget-object v0, p0, LX/9dx;->d:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 1518890
    return-void

    .line 1518891
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    move v0, v2

    .line 1518892
    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1518893
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/9dx;->e:Z

    .line 1518894
    return-void
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 1518895
    iget-object v0, p0, LX/9dx;->a:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v0}, Lcom/facebook/drawingview/DrawingView;->getHistorySize()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 1518896
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/9dx;->setVisibility(I)V

    .line 1518897
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/9dx;->setEnabled(Z)V

    .line 1518898
    return-void
.end method
