.class public LX/AGr;
.super LX/AGp;
.source ""


# instance fields
.field public final c:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel$EdgesModel;

.field public final d:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel$EdgesModel;Lcom/facebook/audience/model/AudienceControlData;Lcom/facebook/audience/model/AudienceControlData;)V
    .locals 2

    .prologue
    .line 1653267
    invoke-direct {p0, p2, p3}, LX/AGp;-><init>(Lcom/facebook/audience/model/AudienceControlData;Lcom/facebook/audience/model/AudienceControlData;)V

    .line 1653268
    iput-object p1, p0, LX/AGr;->c:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel$EdgesModel;

    .line 1653269
    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    .line 1653270
    iget-object v0, p0, LX/AGr;->c:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;->n()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$SeenDirectUsersModel;

    move-result-object v0

    .line 1653271
    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$SeenDirectUsersModel;->a()LX/0Px;

    move-result-object p2

    invoke-virtual {p2}, LX/0Px;->size()I

    move-result p3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, p3, :cond_0

    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$SeenDirectUsersModel$EdgesModel;

    .line 1653272
    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$SeenDirectUsersModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1653273
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1653274
    :cond_0
    move-object v0, p1

    .line 1653275
    iput-object v0, p0, LX/AGr;->d:Ljava/util/HashSet;

    .line 1653276
    return-void
.end method

.method private a(Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;)Z
    .locals 2

    .prologue
    .line 1653266
    invoke-virtual {p1}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;->m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/AGp;->b:Lcom/facebook/audience/model/AudienceControlData;

    invoke-virtual {v1}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;
    .locals 1

    .prologue
    .line 1653265
    iget-object v0, p0, LX/AGr;->c:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;->l()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1653244
    iget-object v0, p0, LX/AGr;->c:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;->m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/facebook/audience/model/AudienceControlData;
    .locals 1

    .prologue
    .line 1653264
    iget-object v0, p0, LX/AGp;->a:Lcom/facebook/audience/model/AudienceControlData;

    return-object v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 1653263
    iget-object v0, p0, LX/AGr;->d:Ljava/util/HashSet;

    iget-object v1, p0, LX/AGp;->b:Lcom/facebook/audience/model/AudienceControlData;

    invoke-virtual {v1}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final e()LX/7gz;
    .locals 3

    .prologue
    .line 1653254
    invoke-direct {p0}, LX/AGr;->m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    move-result-object v0

    .line 1653255
    invoke-direct {p0, v0}, LX/AGr;->a(Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;)Z

    move-result v0

    .line 1653256
    iget-object v1, p0, LX/AGr;->d:Ljava/util/HashSet;

    iget-object v2, p0, LX/AGp;->a:Lcom/facebook/audience/model/AudienceControlData;

    invoke-virtual {v2}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    move v1, v1

    .line 1653257
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 1653258
    sget-object v0, LX/7gz;->SEEN:LX/7gz;

    .line 1653259
    :goto_0
    return-object v0

    .line 1653260
    :cond_0
    if-eqz v0, :cond_1

    if-nez v1, :cond_1

    .line 1653261
    sget-object v0, LX/7gz;->SENT:LX/7gz;

    goto :goto_0

    .line 1653262
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1653277
    iget-object v0, p0, LX/AGr;->c:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;->m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;->l()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel$MessageMediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel$MessageMediaModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 1653278
    if-nez v0, :cond_0

    .line 1653279
    const/4 v0, 0x0

    .line 1653280
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public final g()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1653250
    iget-object v0, p0, LX/AGr;->c:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;->m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;->l()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel$MessageMediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel$MessageMediaModel;->k()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel$MessageMediaModel$ImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel$MessageMediaModel$ImageModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 1653251
    if-nez v0, :cond_0

    .line 1653252
    const/4 v0, 0x0

    .line 1653253
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public final h()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1653246
    iget-object v0, p0, LX/AGr;->c:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;->m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;->l()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel$MessageMediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel$MessageMediaModel;->l()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel$MessageMediaModel$LowresModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel$MessageMediaModel$LowresModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 1653247
    if-nez v0, :cond_0

    .line 1653248
    const/4 v0, 0x0

    .line 1653249
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public final i()J
    .locals 4

    .prologue
    .line 1653245
    iget-object v0, p0, LX/AGr;->c:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;->l()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;->o()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public final j()LX/7gy;
    .locals 2

    .prologue
    .line 1653234
    iget-object v0, p0, LX/AGr;->c:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;->l()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;->k()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/AGr;->c:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel$EdgesModel;

    invoke-virtual {v1}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;->m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 1653235
    invoke-direct {p0}, LX/AGr;->m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    move-result-object v1

    invoke-direct {p0, v1}, LX/AGr;->a(Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;)Z

    move-result v1

    .line 1653236
    if-eqz v0, :cond_1

    .line 1653237
    if-eqz v1, :cond_0

    .line 1653238
    sget-object v0, LX/7gy;->OUTGOING_STORY:LX/7gy;

    .line 1653239
    :goto_0
    return-object v0

    .line 1653240
    :cond_0
    sget-object v0, LX/7gy;->INCOMING_STORY:LX/7gy;

    goto :goto_0

    .line 1653241
    :cond_1
    if-eqz v1, :cond_2

    .line 1653242
    sget-object v0, LX/7gy;->OUTOING_REPLY:LX/7gy;

    goto :goto_0

    .line 1653243
    :cond_2
    sget-object v0, LX/7gy;->INCOMING_REPLY:LX/7gy;

    goto :goto_0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1653233
    iget-object v0, p0, LX/AGr;->c:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 1653230
    iget-object v0, p0, LX/AGr;->c:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;->m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AGr;->c:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;->l()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AGr;->c:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;->m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;->l()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel$MessageMediaModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1653231
    :cond_0
    const/4 v0, 0x0

    .line 1653232
    :goto_0
    return v0

    :cond_1
    invoke-super {p0}, LX/AGp;->l()Z

    move-result v0

    goto :goto_0
.end method
