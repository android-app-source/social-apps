.class public LX/9mW;
.super LX/9mP;
.source ""


# instance fields
.field public e:Lcom/facebook/rapidreporting/ui/DialogStateData;

.field public f:Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;

.field public final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/9mI;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcom/facebook/resources/ui/FbTextView;

.field public i:Landroid/view/View;

.field public final j:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/rapidreporting/ui/DialogStateData;Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;LX/0wM;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1535833
    const v0, 0x7f0310ed

    invoke-direct {p0, p1, v0, p4, p5}, LX/9mP;-><init>(Landroid/content/Context;ILcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;LX/0wM;)V

    .line 1535834
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/9mW;->g:Ljava/util/List;

    .line 1535835
    new-instance v0, LX/9mV;

    invoke-direct {v0, p0}, LX/9mV;-><init>(LX/9mW;)V

    iput-object v0, p0, LX/9mW;->j:Landroid/view/View$OnClickListener;

    .line 1535836
    iput-object p2, p0, LX/9mW;->e:Lcom/facebook/rapidreporting/ui/DialogStateData;

    .line 1535837
    iput-object p3, p0, LX/9mW;->f:Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;

    .line 1535838
    iget-object v0, p0, LX/9mP;->c:Landroid/widget/TextView;

    .line 1535839
    iget-object v1, p2, Lcom/facebook/rapidreporting/ui/DialogStateData;->d:Lcom/facebook/graphql/executor/GraphQLResult;

    if-nez v1, :cond_4

    .line 1535840
    const/4 v1, 0x0

    .line 1535841
    :goto_0
    move-object v1, v1

    .line 1535842
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1535843
    iget-object v0, p0, LX/9mP;->c:Landroid/widget/TextView;

    const v1, 0x7f0207db

    const v2, -0xa76f01

    invoke-virtual {p0, v0, v1, v2}, LX/9mP;->a(Landroid/widget/TextView;II)V

    .line 1535844
    invoke-virtual {p2}, Lcom/facebook/rapidreporting/ui/DialogStateData;->y()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p2}, Lcom/facebook/rapidreporting/ui/DialogStateData;->y()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 1535845
    const v0, 0x7f0d2834

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1535846
    const v0, 0x7f0d2835

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/9mW;->h:Lcom/facebook/resources/ui/FbTextView;

    .line 1535847
    iget-object v0, p0, LX/9mW;->h:Lcom/facebook/resources/ui/FbTextView;

    .line 1535848
    iget-object v1, p2, Lcom/facebook/rapidreporting/ui/DialogStateData;->d:Lcom/facebook/graphql/executor/GraphQLResult;

    if-nez v1, :cond_5

    .line 1535849
    const/4 v1, 0x0

    .line 1535850
    :goto_1
    move-object v1, v1

    .line 1535851
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1535852
    iget-object v0, p0, LX/9mW;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1535853
    invoke-virtual {p2}, Lcom/facebook/rapidreporting/ui/DialogStateData;->y()LX/0Px;

    move-result-object v0

    const/4 p3, 0x3

    const/4 v3, 0x0

    .line 1535854
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p1

    move v2, v3

    .line 1535855
    :goto_2
    if-ge v2, p1, :cond_2

    .line 1535856
    new-instance p2, LX/9mI;

    invoke-virtual {p0}, LX/9mW;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p2, v1}, LX/9mI;-><init>(Landroid/content/Context;)V

    .line 1535857
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/rapidreporting/ui/GuidedActionItem;

    invoke-virtual {p2, p0, v1}, LX/9mI;->a(LX/9mW;Lcom/facebook/rapidreporting/ui/GuidedActionItem;)V

    .line 1535858
    iget-object v1, p0, LX/9mW;->g:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1535859
    if-lt v2, p3, :cond_0

    iget-object v1, p0, LX/9mW;->e:Lcom/facebook/rapidreporting/ui/DialogStateData;

    .line 1535860
    iget-boolean p4, v1, Lcom/facebook/rapidreporting/ui/DialogStateData;->m:Z

    move v1, p4

    .line 1535861
    if-eqz v1, :cond_1

    .line 1535862
    :cond_0
    invoke-static {p0, p2}, LX/9mW;->b(LX/9mW;Landroid/view/View;)V

    .line 1535863
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    .line 1535864
    :cond_2
    if-le p1, p3, :cond_3

    iget-object v1, p0, LX/9mW;->e:Lcom/facebook/rapidreporting/ui/DialogStateData;

    .line 1535865
    iget-boolean v2, v1, Lcom/facebook/rapidreporting/ui/DialogStateData;->m:Z

    move v1, v2

    .line 1535866
    if-nez v1, :cond_3

    .line 1535867
    const v1, 0x7f0d02a5

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, LX/9mW;->i:Landroid/view/View;

    .line 1535868
    iget-object v1, p0, LX/9mW;->i:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1535869
    iget-object v1, p0, LX/9mW;->i:Landroid/view/View;

    iget-object v2, p0, LX/9mW;->j:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1535870
    :cond_3
    return-void

    :cond_4
    iget-object v1, p2, Lcom/facebook/rapidreporting/ui/DialogStateData;->d:Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1535871
    iget-object v2, v1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v2

    .line 1535872
    check-cast v1, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel;

    invoke-virtual {v1}, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel;->a()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const/4 p1, 0x4

    const-class p3, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$ConfirmationPromptModel$TitleModel;

    invoke-virtual {v2, v1, p1, p3}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$ConfirmationPromptModel$TitleModel;

    invoke-virtual {v1}, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$ConfirmationPromptModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    :cond_5
    iget-object v1, p2, Lcom/facebook/rapidreporting/ui/DialogStateData;->d:Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1535873
    iget-object v2, v1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v2

    .line 1535874
    check-cast v1, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel;

    invoke-virtual {v1}, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel;->a()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const/4 p1, 0x1

    invoke-virtual {v2, v1, p1}, LX/15i;->g(II)I

    move-result v1

    const/4 p1, 0x0

    invoke-virtual {v2, v1, p1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1
.end method

.method public static b(LX/9mW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1535875
    iget-object v0, p0, LX/9mP;->b:Landroid/widget/LinearLayout;

    iget-object v1, p0, LX/9mP;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, p1, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 1535876
    return-void
.end method


# virtual methods
.method public final onAttachedToWindow()V
    .locals 9

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2c

    const v1, -0x399005c7

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1535877
    invoke-super {p0}, LX/9mP;->onAttachedToWindow()V

    .line 1535878
    iget-object v1, p0, LX/9mP;->d:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1535879
    iget-object v1, p0, LX/9mP;->d:Landroid/widget/TextView;

    iget-object v2, p0, LX/9mW;->e:Lcom/facebook/rapidreporting/ui/DialogStateData;

    .line 1535880
    iget-object v3, v2, Lcom/facebook/rapidreporting/ui/DialogStateData;->d:Lcom/facebook/graphql/executor/GraphQLResult;

    if-nez v3, :cond_0

    .line 1535881
    const/4 v3, 0x0

    .line 1535882
    :goto_0
    move-object v2, v3

    .line 1535883
    iget-object v3, p0, LX/9mW;->e:Lcom/facebook/rapidreporting/ui/DialogStateData;

    .line 1535884
    iget-object v5, v3, Lcom/facebook/rapidreporting/ui/DialogStateData;->d:Lcom/facebook/graphql/executor/GraphQLResult;

    if-nez v5, :cond_1

    .line 1535885
    const/4 v5, 0x0

    .line 1535886
    :goto_1
    move-object v3, v5

    .line 1535887
    invoke-virtual {p0, v1, v2, v3}, LX/9mP;->a(Landroid/widget/TextView;Ljava/lang/String;LX/0Px;)V

    .line 1535888
    const/16 v1, 0x2d

    const v2, -0x96a5476

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_0
    iget-object v3, v2, Lcom/facebook/rapidreporting/ui/DialogStateData;->d:Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1535889
    iget-object v5, v3, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v3, v5

    .line 1535890
    check-cast v3, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel;

    invoke-virtual {v3}, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel;->a()LX/1vs;

    move-result-object v3

    iget-object v5, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const/4 v6, 0x3

    const-class v7, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$ConfirmationPromptModel$SubtitleModel;

    invoke-virtual {v5, v3, v6, v7}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v3

    check-cast v3, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$ConfirmationPromptModel$SubtitleModel;

    invoke-virtual {v3}, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$ConfirmationPromptModel$SubtitleModel;->j()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_1
    iget-object v5, v3, Lcom/facebook/rapidreporting/ui/DialogStateData;->d:Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1535891
    iget-object v6, v5, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v5, v6

    .line 1535892
    check-cast v5, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel;

    invoke-virtual {v5}, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel;->a()LX/1vs;

    move-result-object v5

    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    const/4 v7, 0x3

    const-class v8, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$ConfirmationPromptModel$SubtitleModel;

    invoke-virtual {v6, v5, v7, v8}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v5

    check-cast v5, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$ConfirmationPromptModel$SubtitleModel;

    invoke-virtual {v5}, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$ConfirmationPromptModel$SubtitleModel;->a()LX/0Px;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/rapidreporting/ui/Range;->a(Ljava/util/List;)LX/0Px;

    move-result-object v5

    goto :goto_1
.end method
