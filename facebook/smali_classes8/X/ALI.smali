.class public final LX/ALI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5f5;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/5f5",
        "<",
        "LX/5fl;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/ALM;


# direct methods
.method public constructor <init>(LX/ALM;)V
    .locals 0

    .prologue
    .line 1664743
    iput-object p1, p0, LX/ALI;->a:LX/ALM;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    .line 1664741
    iget-object v0, p0, LX/ALI;->a:LX/ALM;

    iget-object v0, v0, LX/ALM;->a:Lcom/facebook/backstage/camera/CameraView;

    iget-object v0, v0, Lcom/facebook/backstage/camera/CameraView;->u:Landroid/os/Handler;

    iget-object v1, p0, LX/ALI;->a:LX/ALM;

    iget-object v1, v1, LX/ALM;->a:Lcom/facebook/backstage/camera/CameraView;

    iget-object v1, v1, Lcom/facebook/backstage/camera/CameraView;->v:Ljava/lang/Runnable;

    const-wide/16 v2, 0x190

    const v4, 0x7a5e53ec

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1664742
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Exception;)V
    .locals 3

    .prologue
    .line 1664735
    iget-object v0, p0, LX/ALI;->a:LX/ALM;

    iget-object v0, v0, LX/ALM;->a:Lcom/facebook/backstage/camera/CameraView;

    invoke-virtual {v0}, Lcom/facebook/backstage/camera/CameraView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082841

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    .line 1664736
    sget-object v0, Lcom/facebook/backstage/camera/CameraView;->f:Ljava/lang/String;

    const-string v1, "Failed to start recording a video"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1664737
    iget-object v0, p0, LX/ALI;->a:LX/ALM;

    iget-object v0, v0, LX/ALM;->a:Lcom/facebook/backstage/camera/CameraView;

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1664738
    invoke-static {v0, v1, v2}, Lcom/facebook/backstage/camera/CameraView;->a$redex0(Lcom/facebook/backstage/camera/CameraView;ZZ)V

    .line 1664739
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1664740
    invoke-direct {p0}, LX/ALI;->a()V

    return-void
.end method
