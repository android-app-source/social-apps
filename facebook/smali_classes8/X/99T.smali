.class public final LX/99T;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1PY;


# instance fields
.field public a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/widget/listview/BetterListView;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1447345
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/1PU;)V
    .locals 2

    .prologue
    .line 1447346
    iget-object v0, p0, LX/99T;->a:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    .line 1447347
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "List view must be set beforeregisterScrollListener is called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1447348
    :cond_0
    iget-object v0, p0, LX/99T;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    .line 1447349
    if-nez v0, :cond_1

    .line 1447350
    :goto_0
    return-void

    .line 1447351
    :cond_1
    new-instance v1, LX/99S;

    invoke-direct {v1, p0, p1}, LX/99S;-><init>(LX/99T;LX/1PU;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->a(Landroid/widget/AbsListView$OnScrollListener;)V

    goto :goto_0
.end method
