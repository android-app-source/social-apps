.class public LX/935;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/res/Resources;

.field public final b:LX/0ad;

.field public c:LX/90V;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0ad;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1433230
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1433231
    iput-object p1, p0, LX/935;->a:Landroid/content/res/Resources;

    .line 1433232
    const/4 v0, 0x0

    iput-object v0, p0, LX/935;->c:LX/90V;

    .line 1433233
    iput-object p2, p0, LX/935;->b:LX/0ad;

    .line 1433234
    return-void
.end method


# virtual methods
.method public final a(LX/0h5;Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)V
    .locals 3

    .prologue
    .line 1433235
    iget-object v0, p2, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->n:LX/93E;

    move-object v0, v0

    .line 1433236
    if-nez v0, :cond_0

    .line 1433237
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1433238
    :cond_0
    sget-object v1, LX/934;->a:[I

    invoke-virtual {v0}, LX/93E;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1433239
    :goto_0
    return-void

    .line 1433240
    :pswitch_0
    iget-object v0, p0, LX/935;->b:LX/0ad;

    sget-char v1, LX/8zO;->e:C

    const v2, 0x7f08141a

    iget-object p2, p0, LX/935;->a:Landroid/content/res/Resources;

    invoke-interface {v0, v1, v2, p2}, LX/0ad;->a(CILandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 1433241
    invoke-interface {p1, v0}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 1433242
    check-cast p1, Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1433243
    goto :goto_0

    .line 1433244
    :pswitch_1
    iget-object v0, p2, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->h:Ljava/lang/String;

    move-object v0, v0

    .line 1433245
    if-eqz v0, :cond_2

    .line 1433246
    iget-object v0, p2, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->h:Ljava/lang/String;

    move-object v0, v0

    .line 1433247
    invoke-interface {p1, v0}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 1433248
    :cond_1
    :goto_1
    check-cast p1, Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1433249
    goto :goto_0

    .line 1433250
    :pswitch_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1433251
    :cond_2
    iget-object v0, p2, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->m:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    move-object v0, v0

    .line 1433252
    if-eqz v0, :cond_3

    .line 1433253
    iget-object v0, p2, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->m:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    move-object v0, v0

    .line 1433254
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;->n()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, LX/0h5;->setTitle(Ljava/lang/String;)V

    goto :goto_1

    .line 1433255
    :cond_3
    invoke-static {p2}, LX/93D;->a(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1433256
    invoke-static {p2}, LX/93D;->b(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->bx_()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 1433257
    iget-object v0, p0, LX/935;->a:Landroid/content/res/Resources;

    const v1, 0x7f0819d7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1433258
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    const/4 p2, 0x1

    .line 1433259
    iput p2, v1, LX/108;->a:I

    .line 1433260
    move-object v1, v1

    .line 1433261
    iput-object v0, v1, LX/108;->g:Ljava/lang/String;

    .line 1433262
    move-object v1, v1

    .line 1433263
    const/4 p2, -0x2

    .line 1433264
    iput p2, v1, LX/108;->h:I

    .line 1433265
    move-object v1, v1

    .line 1433266
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    .line 1433267
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-interface {p1, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 1433268
    new-instance v1, LX/933;

    invoke-direct {v1, p0}, LX/933;-><init>(LX/935;)V

    invoke-interface {p1, v1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 1433269
    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
