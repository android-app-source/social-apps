.class public final LX/9f9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9f8;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V
    .locals 0

    .prologue
    .line 1520824
    iput-object p1, p0, LX/9f9;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/5i7;)V
    .locals 2
    .param p1    # LX/5i7;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1520825
    iget-object v0, p0, LX/9f9;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9f8;

    .line 1520826
    invoke-interface {v0, p1}, LX/9f8;->a(LX/5i7;)V

    goto :goto_0

    .line 1520827
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;LX/5i7;)V
    .locals 2
    .param p2    # LX/5i7;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1520828
    iget-object v0, p0, LX/9f9;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9f8;

    .line 1520829
    invoke-interface {v0, p1, p2}, LX/9f8;->a(Ljava/lang/String;LX/5i7;)V

    goto :goto_0

    .line 1520830
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;LX/5i7;)V
    .locals 2
    .param p2    # LX/5i7;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1520831
    iget-object v0, p0, LX/9f9;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9f8;

    .line 1520832
    invoke-interface {v0, p1, p2}, LX/9f8;->b(Ljava/lang/String;LX/5i7;)V

    goto :goto_0

    .line 1520833
    :cond_0
    return-void
.end method

.method public final c(Ljava/lang/String;LX/5i7;)V
    .locals 2
    .param p2    # LX/5i7;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1520834
    iget-object v0, p0, LX/9f9;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9f8;

    .line 1520835
    invoke-interface {v0, p1, p2}, LX/9f8;->c(Ljava/lang/String;LX/5i7;)V

    goto :goto_0

    .line 1520836
    :cond_0
    return-void
.end method
