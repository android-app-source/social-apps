.class public LX/90j;
.super LX/90i;
.source ""


# static fields
.field public static final a:Ljava/lang/Object;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field


# instance fields
.field public b:LX/5LG;

.field private final c:Landroid/content/Context;

.field private final d:Landroid/view/LayoutInflater;

.field private final e:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

.field public final f:LX/93N;

.field public final g:LX/92C;

.field public final h:LX/5Lv;

.field public final i:LX/92X;

.field public final j:LX/03V;

.field public final k:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectAdapter$ItemBindedCallback;",
            ">;"
        }
    .end annotation
.end field

.field public l:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

.field public m:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1429374
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/90j;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/92X;LX/03V;LX/92C;Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;)V
    .locals 1

    .prologue
    .line 1429363
    invoke-direct {p0}, LX/90i;-><init>()V

    .line 1429364
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/90j;->k:Ljava/util/Map;

    .line 1429365
    iput-object p1, p0, LX/90j;->c:Landroid/content/Context;

    .line 1429366
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, LX/90j;->d:Landroid/view/LayoutInflater;

    .line 1429367
    iput-object p5, p0, LX/90j;->e:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    .line 1429368
    iput-object p2, p0, LX/90j;->i:LX/92X;

    .line 1429369
    iput-object p3, p0, LX/90j;->j:LX/03V;

    .line 1429370
    iput-object p4, p0, LX/90j;->g:LX/92C;

    .line 1429371
    new-instance v0, LX/5Lv;

    invoke-direct {v0}, LX/5Lv;-><init>()V

    iput-object v0, p0, LX/90j;->h:LX/5Lv;

    .line 1429372
    new-instance v0, LX/93N;

    invoke-direct {v0}, LX/93N;-><init>()V

    iput-object v0, p0, LX/90j;->f:LX/93N;

    .line 1429373
    return-void
.end method

.method public static b(LX/90j;I)Z
    .locals 1

    .prologue
    .line 1429362
    iget-object v0, p0, LX/90j;->l:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/90j;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(LX/90j;I)Z
    .locals 1

    .prologue
    .line 1429361
    iget-object v0, p0, LX/90j;->l:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/90j;->h:LX/5Lv;

    invoke-virtual {v0}, LX/5Lv;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/90j;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1429360
    iget-object v0, p0, LX/90j;->h:LX/5Lv;

    invoke-virtual {v0}, LX/5Lv;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/90j;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final a(Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;)I
    .locals 1

    .prologue
    .line 1429357
    invoke-virtual {p0, p1}, LX/90j;->b(Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/90j;->b()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/90j;->h:LX/5Lv;

    .line 1429358
    iget-object p0, v0, LX/5Lv;->a:Ljava/util/List;

    invoke-interface {p0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p0

    move v0, p0

    .line 1429359
    goto :goto_0
.end method

.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1429352
    invoke-static {}, LX/90g;->values()[LX/90g;

    move-result-object v0

    aget-object v0, v0, p1

    .line 1429353
    sget-object v1, LX/90f;->a:[I

    invoke-virtual {v0}, LX/90g;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1429354
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown view type,"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1429355
    :pswitch_0
    new-instance v0, LX/93L;

    iget-object v1, p0, LX/90j;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/93L;-><init>(Landroid/content/Context;)V

    .line 1429356
    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, LX/90j;->d:Landroid/view/LayoutInflater;

    const v1, 0x7f030337

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 6

    .prologue
    .line 1429340
    invoke-static {}, LX/90g;->values()[LX/90g;

    move-result-object v0

    aget-object v1, v0, p4

    .line 1429341
    iget-object v0, p0, LX/90j;->k:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, LX/90j;->k:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1429342
    iget-object v0, p0, LX/90j;->k:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/90d;

    .line 1429343
    iget-object v2, v0, LX/90d;->a:LX/90e;

    iget-object v2, v2, LX/90e;->b:LX/90j;

    iget-object v2, v2, LX/90j;->g:LX/92C;

    invoke-virtual {v2}, LX/92C;->c()V

    .line 1429344
    iget-object v2, v0, LX/90d;->a:LX/90e;

    iget-object v2, v2, LX/90e;->b:LX/90j;

    iget-object v2, v2, LX/90j;->g:LX/92C;

    invoke-virtual {v2}, LX/92C;->d()V

    .line 1429345
    iget-object v0, p0, LX/90j;->k:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1429346
    :cond_0
    sget-object v0, LX/90f;->a:[I

    invoke-virtual {v1}, LX/90g;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1429347
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown view type,"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1429348
    :pswitch_0
    iget-object v0, p0, LX/90j;->f:LX/93N;

    move-object v1, p3

    check-cast v1, LX/93H;

    move-object v2, p2

    check-cast v2, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    iget-object v3, p0, LX/90j;->b:LX/5LG;

    const/4 v4, 0x0

    iget-object v5, p0, LX/90j;->e:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    invoke-virtual/range {v0 .. v5}, LX/93N;->a(LX/93H;Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;LX/5LG;ZLcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;)V

    .line 1429349
    :goto_0
    return-void

    .line 1429350
    :pswitch_1
    iget-object v0, p0, LX/90j;->f:LX/93N;

    move-object v1, p3

    check-cast v1, LX/93H;

    move-object v2, p2

    check-cast v2, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    iget-object v3, p0, LX/90j;->b:LX/5LG;

    const/4 v4, 0x1

    iget-object v5, p0, LX/90j;->e:Lcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;

    invoke-virtual/range {v0 .. v5}, LX/93N;->a(LX/93H;Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;LX/5LG;ZLcom/facebook/composer/minutiae/activity/MinutiaeTaggableObjectFragment;)V

    goto :goto_0

    .line 1429351
    :pswitch_2
    invoke-virtual {p3}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, LX/90c;

    invoke-direct {v1, p0, p3}, LX/90c;-><init>(LX/90j;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1429339
    iget-object v0, p0, LX/90j;->h:LX/5Lv;

    invoke-virtual {v0}, LX/5Lv;->b()I

    move-result v0

    return v0
.end method

.method public final b(Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;)Z
    .locals 1
    .param p1    # Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1429307
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/90j;->l:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1429336
    iget-object v0, p0, LX/90j;->h:LX/5Lv;

    .line 1429337
    iget-object p0, v0, LX/5Lv;->d:Ljava/lang/String;

    move-object v0, p0

    .line 1429338
    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1429329
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 1429330
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, LX/90j;->h:LX/5Lv;

    invoke-virtual {v2}, LX/5Lv;->b()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 1429331
    iget-object v2, p0, LX/90j;->h:LX/5Lv;

    invoke-virtual {v2, v0}, LX/5Lv;->a(I)Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->e()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1429332
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1429333
    :cond_0
    iget-object v0, p0, LX/90j;->l:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    if-eqz v0, :cond_1

    .line 1429334
    const-string v0, "0"

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1429335
    :cond_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 1429323
    iget-object v0, p0, LX/90j;->h:LX/5Lv;

    invoke-virtual {v0}, LX/5Lv;->b()I

    move-result v0

    .line 1429324
    iget-object v1, p0, LX/90j;->l:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    if-eqz v1, :cond_1

    .line 1429325
    add-int/lit8 v0, v0, 0x1

    .line 1429326
    :cond_0
    :goto_0
    return v0

    .line 1429327
    :cond_1
    iget-object v1, p0, LX/90j;->h:LX/5Lv;

    invoke-virtual {v1}, LX/5Lv;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1429328
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1429317
    invoke-static {p0, p1}, LX/90j;->b(LX/90j;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1429318
    iget-object v0, p0, LX/90j;->l:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    .line 1429319
    :goto_0
    return-object v0

    .line 1429320
    :cond_0
    invoke-static {p0, p1}, LX/90j;->c(LX/90j;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1429321
    sget-object v0, LX/90j;->a:Ljava/lang/Object;

    goto :goto_0

    .line 1429322
    :cond_1
    iget-object v0, p0, LX/90j;->h:LX/5Lv;

    invoke-virtual {v0, p1}, LX/5Lv;->a(I)Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    move-result-object v0

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1429316
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1429309
    invoke-static {p0, p1}, LX/90j;->b(LX/90j;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1429310
    sget-object v0, LX/90g;->FREE_FORM_VIEW:LX/90g;

    .line 1429311
    :goto_0
    move-object v0, v0

    .line 1429312
    invoke-virtual {v0}, LX/90g;->ordinal()I

    move-result v0

    return v0

    .line 1429313
    :cond_0
    invoke-static {p0, p1}, LX/90j;->c(LX/90j;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1429314
    sget-object v0, LX/90g;->LOADING_INDICATOR:LX/90g;

    goto :goto_0

    .line 1429315
    :cond_1
    sget-object v0, LX/90g;->ITEM_VIEW:LX/90g;

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 1429308
    invoke-static {}, LX/90g;->values()[LX/90g;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
