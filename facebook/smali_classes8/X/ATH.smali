.class public final LX/ATH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4oV;


# instance fields
.field public final synthetic a:LX/ATO;


# direct methods
.method public constructor <init>(LX/ATO;)V
    .locals 0

    .prologue
    .line 1675396
    iput-object p1, p0, LX/ATH;->a:LX/ATO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(III)V
    .locals 5

    .prologue
    .line 1675397
    iget-object v0, p0, LX/ATH;->a:LX/ATO;

    iget v0, v0, LX/ATO;->U:I

    if-nez v0, :cond_0

    .line 1675398
    iget-object v0, p0, LX/ATH;->a:LX/ATO;

    iget-object v1, p0, LX/ATH;->a:LX/ATO;

    iget-object v1, v1, LX/ATO;->w:Lcom/facebook/widget/ScrollingAwareScrollView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/widget/ScrollingAwareScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    iget-object v2, p0, LX/ATH;->a:LX/ATO;

    iget-object v2, v2, LX/ATO;->w:Lcom/facebook/widget/ScrollingAwareScrollView;

    invoke-virtual {v2}, Lcom/facebook/widget/ScrollingAwareScrollView;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v1, v2

    .line 1675399
    iput v1, v0, LX/ATO;->U:I

    .line 1675400
    :cond_0
    iget-object v0, p0, LX/ATH;->a:LX/ATO;

    iget v0, v0, LX/ATO;->U:I

    if-eqz v0, :cond_1

    .line 1675401
    int-to-double v0, p2

    iget-object v2, p0, LX/ATH;->a:LX/ATO;

    iget v2, v2, LX/ATO;->U:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    mul-double/2addr v0, v2

    .line 1675402
    iget-object v2, p0, LX/ATH;->a:LX/ATO;

    iget-wide v2, v2, LX/ATO;->T:D

    cmpl-double v2, v0, v2

    if-lez v2, :cond_1

    .line 1675403
    iget-object v2, p0, LX/ATH;->a:LX/ATO;

    .line 1675404
    iput-wide v0, v2, LX/ATO;->T:D

    .line 1675405
    :cond_1
    iget-object v0, p0, LX/ATH;->a:LX/ATO;

    invoke-static {v0}, LX/ATO;->t(LX/ATO;)V

    .line 1675406
    return-void
.end method
