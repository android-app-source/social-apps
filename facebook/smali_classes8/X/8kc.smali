.class public LX/8kc;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/facebook/content/SecureContextHelper;

.field public final c:LX/8kd;

.field private final d:LX/0So;

.field public final e:LX/11i;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/8kd;LX/0So;LX/11i;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1396586
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1396587
    iput-object p1, p0, LX/8kc;->a:Landroid/content/Context;

    .line 1396588
    iput-object p2, p0, LX/8kc;->b:Lcom/facebook/content/SecureContextHelper;

    .line 1396589
    iput-object p3, p0, LX/8kc;->c:LX/8kd;

    .line 1396590
    iput-object p4, p0, LX/8kc;->d:LX/0So;

    .line 1396591
    iput-object p5, p0, LX/8kc;->e:LX/11i;

    .line 1396592
    return-void
.end method

.method public static a(LX/0QB;)LX/8kc;
    .locals 7

    .prologue
    .line 1396593
    new-instance v1, LX/8kc;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/8kd;->b(LX/0QB;)LX/8kd;

    move-result-object v4

    check-cast v4, LX/8kd;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v5

    check-cast v5, LX/0So;

    invoke-static {p0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v6

    check-cast v6, LX/11i;

    invoke-direct/range {v1 .. v6}, LX/8kc;-><init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/8kd;LX/0So;LX/11i;)V

    .line 1396594
    move-object v0, v1

    .line 1396595
    return-object v0
.end method
