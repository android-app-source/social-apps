.class public LX/ARA;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/composer/system/model/ComposerModelImpl;

.field public final b:LX/AR3;

.field public final c:LX/AR1;

.field public final d:I

.field public final e:LX/8LV;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/system/model/ComposerModelImpl;LX/AR3;LX/AR1;Ljava/lang/Integer;LX/8LV;)V
    .locals 1
    .param p1    # Lcom/facebook/composer/system/model/ComposerModelImpl;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/AR3;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/AR1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1672580
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1672581
    iput-object p1, p0, LX/ARA;->a:Lcom/facebook/composer/system/model/ComposerModelImpl;

    .line 1672582
    iput-object p3, p0, LX/ARA;->c:LX/AR1;

    .line 1672583
    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/ARA;->d:I

    .line 1672584
    iput-object p2, p0, LX/ARA;->b:LX/AR3;

    .line 1672585
    iput-object p5, p0, LX/ARA;->e:LX/8LV;

    .line 1672586
    return-void
.end method


# virtual methods
.method public final a()Landroid/content/Intent;
    .locals 12

    .prologue
    .line 1672587
    iget-object v3, p0, LX/ARA;->b:LX/AR3;

    invoke-virtual {v3}, LX/AR3;->a()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1672588
    :cond_0
    :goto_0
    iget-object v3, p0, LX/ARA;->a:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v3}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v3

    invoke-static {v3}, LX/7kq;->s(LX/0Px;)LX/0Px;

    move-result-object v3

    .line 1672589
    iget-object v4, p0, LX/ARA;->a:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v4}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v4

    invoke-static {v4}, LX/93r;->b(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v7

    .line 1672590
    const-string v4, "Review must have selectable privacy data"

    invoke-static {v7, v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1672591
    new-instance v4, LX/7li;

    iget-object v5, p0, LX/ARA;->a:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v5}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v5

    iget-wide v5, v5, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    iget v8, p0, LX/ARA;->d:I

    iget-object v9, p0, LX/ARA;->a:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v9}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getOgMechanism()Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, LX/ARA;->a:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v10}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getOgSurface()Ljava/lang/String;

    move-result-object v10

    invoke-direct/range {v4 .. v10}, LX/7li;-><init>(JLcom/facebook/graphql/model/GraphQLPrivacyOption;ILjava/lang/String;Ljava/lang/String;)V

    iget-object v5, p0, LX/ARA;->a:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v5}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v5

    .line 1672592
    iput-object v5, v4, LX/7li;->a:Ljava/lang/String;

    .line 1672593
    move-object v4, v4

    .line 1672594
    iget-object v5, p0, LX/ARA;->a:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v5}, LX/0j2;->getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    invoke-static {v5}, LX/8oj;->a(LX/175;)Ljava/lang/String;

    move-result-object v5

    .line 1672595
    iput-object v5, v4, LX/7li;->c:Ljava/lang/String;

    .line 1672596
    move-object v4, v4

    .line 1672597
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_3

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/ipc/media/MediaItem;

    .line 1672598
    :goto_1
    iput-object v3, v4, LX/7li;->h:Lcom/facebook/ipc/media/MediaItem;

    .line 1672599
    move-object v3, v4

    .line 1672600
    invoke-virtual {v3}, LX/7li;->a()Lcom/facebook/composer/protocol/PostReviewParams;

    move-result-object v3

    move-object v0, v3

    .line 1672601
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1672602
    const-string v2, "publishReviewParams"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1672603
    const-string v0, "extra_feed_unit_cache_id"

    iget-object v2, p0, LX/ARA;->a:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getCacheId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1672604
    iget-object v0, p0, LX/ARA;->c:LX/AR1;

    invoke-virtual {v0}, LX/AR1;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1672605
    const-string v0, "extra_optimistic_feed_story"

    iget-object v2, p0, LX/ARA;->c:LX/AR1;

    invoke-virtual {v2}, LX/AR1;->b()LX/9A3;

    move-result-object v2

    invoke-virtual {v2}, LX/9A3;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-static {v1, v0, v2}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1672606
    :cond_1
    return-object v1

    .line 1672607
    :cond_2
    iget-object v3, p0, LX/ARA;->a:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v3}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v3

    invoke-static {v3}, LX/93r;->b(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v7

    .line 1672608
    const-string v3, "Review must have selectable privacy data"

    invoke-static {v7, v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1672609
    iget-object v3, p0, LX/ARA;->e:LX/8LV;

    iget-object v4, p0, LX/ARA;->a:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v4}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v4

    invoke-static {v4}, LX/7kq;->s(LX/0Px;)LX/0Px;

    move-result-object v11

    new-instance v4, LX/7li;

    iget-object v5, p0, LX/ARA;->a:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v5}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v5

    iget-wide v5, v5, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    iget v8, p0, LX/ARA;->d:I

    iget-object v9, p0, LX/ARA;->a:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v9}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getOgMechanism()Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, LX/ARA;->a:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v10}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getOgSurface()Ljava/lang/String;

    move-result-object v10

    invoke-direct/range {v4 .. v10}, LX/7li;-><init>(JLcom/facebook/graphql/model/GraphQLPrivacyOption;ILjava/lang/String;Ljava/lang/String;)V

    iget-object v5, p0, LX/ARA;->a:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v5}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v5

    .line 1672610
    iput-object v5, v4, LX/7li;->a:Ljava/lang/String;

    .line 1672611
    move-object v4, v4

    .line 1672612
    iget-object v5, p0, LX/ARA;->b:LX/AR3;

    .line 1672613
    iget-object v6, v5, LX/AR3;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-object v5, v6

    .line 1672614
    invoke-static {v5}, LX/8oj;->a(LX/175;)Ljava/lang/String;

    move-result-object v5

    .line 1672615
    iput-object v5, v4, LX/7li;->c:Ljava/lang/String;

    .line 1672616
    move-object v4, v4

    .line 1672617
    invoke-virtual {v4}, LX/7li;->a()Lcom/facebook/composer/protocol/PostReviewParams;

    move-result-object v4

    iget-object v5, p0, LX/ARA;->a:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v5}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v11, v4, v5}, LX/8LV;->a(LX/0Px;Lcom/facebook/composer/protocol/PostReviewParams;Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v3

    .line 1672618
    iget-object v4, p0, LX/ARA;->b:LX/AR3;

    invoke-virtual {v4, v3}, LX/AR3;->a(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1672619
    iget-object v3, p0, LX/ARA;->c:LX/AR1;

    invoke-virtual {v3}, LX/AR1;->a()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1672620
    iget-object v3, p0, LX/ARA;->b:LX/AR3;

    invoke-virtual {v3}, LX/AR3;->e()V

    goto/16 :goto_0

    :cond_3
    const/4 v3, 0x0

    goto/16 :goto_1
.end method
