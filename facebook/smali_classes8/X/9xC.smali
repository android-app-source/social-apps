.class public final LX/9xC;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 1589158
    const/4 v8, 0x0

    .line 1589159
    const/4 v7, 0x0

    .line 1589160
    const/4 v6, 0x0

    .line 1589161
    const-wide/16 v4, 0x0

    .line 1589162
    const/4 v3, 0x0

    .line 1589163
    const/4 v2, 0x0

    .line 1589164
    const/4 v1, 0x0

    .line 1589165
    const/4 v0, 0x0

    .line 1589166
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v9, v10, :cond_b

    .line 1589167
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1589168
    const/4 v0, 0x0

    .line 1589169
    :goto_0
    return v0

    .line 1589170
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v10, :cond_9

    .line 1589171
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    .line 1589172
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1589173
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_0

    if-eqz v1, :cond_0

    .line 1589174
    const-string v10, "attachments"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 1589175
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1589176
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->START_ARRAY:LX/15z;

    if-ne v9, v10, :cond_1

    .line 1589177
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_ARRAY:LX/15z;

    if-eq v9, v10, :cond_1

    .line 1589178
    invoke-static {p0, p1}, LX/9x5;->b(LX/15w;LX/186;)I

    move-result v9

    .line 1589179
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1589180
    :cond_1
    invoke-static {v1, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 1589181
    move v9, v1

    goto :goto_1

    .line 1589182
    :cond_2
    const-string v10, "author"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 1589183
    invoke-static {p0, p1}, LX/9x7;->a(LX/15w;LX/186;)I

    move-result v1

    move v5, v1

    goto :goto_1

    .line 1589184
    :cond_3
    const-string v10, "body"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 1589185
    invoke-static {p0, p1}, LX/5Q6;->a(LX/15w;LX/186;)I

    move-result v1

    move v4, v1

    goto :goto_1

    .line 1589186
    :cond_4
    const-string v10, "created_time"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 1589187
    const/4 v0, 0x1

    .line 1589188
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    goto :goto_1

    .line 1589189
    :cond_5
    const-string v10, "edit_history"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 1589190
    invoke-static {p0, p1}, LX/9x8;->a(LX/15w;LX/186;)I

    move-result v1

    move v8, v1

    goto :goto_1

    .line 1589191
    :cond_6
    const-string v10, "feedback"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 1589192
    invoke-static {p0, p1}, LX/9xA;->a(LX/15w;LX/186;)I

    move-result v1

    move v7, v1

    goto/16 :goto_1

    .line 1589193
    :cond_7
    const-string v10, "parent_feedback"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1589194
    invoke-static {p0, p1}, LX/9xB;->a(LX/15w;LX/186;)I

    move-result v1

    move v6, v1

    goto/16 :goto_1

    .line 1589195
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1589196
    :cond_9
    const/4 v1, 0x7

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1589197
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 1589198
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1589199
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1589200
    if-eqz v0, :cond_a

    .line 1589201
    const/4 v1, 0x3

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1589202
    :cond_a
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1589203
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1589204
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1589205
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_b
    move v9, v8

    move v8, v3

    move v12, v2

    move-wide v2, v4

    move v4, v6

    move v5, v7

    move v7, v12

    move v6, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 1589206
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1589207
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1589208
    if-eqz v0, :cond_1

    .line 1589209
    const-string v1, "attachments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589210
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1589211
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 1589212
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v4

    invoke-static {p0, v4, p2, p3}, LX/9x5;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1589213
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1589214
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1589215
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1589216
    if-eqz v0, :cond_2

    .line 1589217
    const-string v1, "author"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589218
    invoke-static {p0, v0, p2, p3}, LX/9x7;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1589219
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1589220
    if-eqz v0, :cond_3

    .line 1589221
    const-string v1, "body"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589222
    invoke-static {p0, v0, p2, p3}, LX/5Q6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1589223
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1589224
    cmp-long v2, v0, v2

    if-eqz v2, :cond_4

    .line 1589225
    const-string v2, "created_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589226
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1589227
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1589228
    if-eqz v0, :cond_5

    .line 1589229
    const-string v1, "edit_history"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589230
    invoke-static {p0, v0, p2}, LX/9x8;->a(LX/15i;ILX/0nX;)V

    .line 1589231
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1589232
    if-eqz v0, :cond_6

    .line 1589233
    const-string v1, "feedback"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589234
    invoke-static {p0, v0, p2, p3}, LX/9xA;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1589235
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1589236
    if-eqz v0, :cond_7

    .line 1589237
    const-string v1, "parent_feedback"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1589238
    invoke-static {p0, v0, p2}, LX/9xB;->a(LX/15i;ILX/0nX;)V

    .line 1589239
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1589240
    return-void
.end method
