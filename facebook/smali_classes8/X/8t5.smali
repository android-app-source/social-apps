.class public final LX/8t5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field private a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/user/tiles/UserTileDrawableController;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/user/tiles/UserTileDrawableController;)V
    .locals 1

    .prologue
    .line 1412142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1412143
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/8t5;->a:Ljava/lang/ref/WeakReference;

    .line 1412144
    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, 0x612a6465

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1412135
    iget-object v0, p0, LX/8t5;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileDrawableController;

    .line 1412136
    if-eqz v0, :cond_0

    .line 1412137
    iget-object v2, v0, Lcom/facebook/user/tiles/UserTileDrawableController;->t:LX/8t9;

    if-eqz v2, :cond_0

    const-string v2, "updated_users"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object p0, v0, Lcom/facebook/user/tiles/UserTileDrawableController;->t:LX/8t9;

    .line 1412138
    iget-object p2, p0, LX/8t9;->c:Lcom/facebook/user/model/UserKey;

    move-object p0, p2

    .line 1412139
    invoke-virtual {v2, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1412140
    invoke-static {v0}, Lcom/facebook/user/tiles/UserTileDrawableController;->e(Lcom/facebook/user/tiles/UserTileDrawableController;)V

    .line 1412141
    :cond_0
    const/16 v0, 0x27

    const v2, 0x15887e61

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
