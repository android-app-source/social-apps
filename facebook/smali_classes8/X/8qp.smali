.class public LX/8qp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8qo;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/facebook/content/SecureContextHelper;

.field public final c:LX/0Zb;

.field public final d:LX/1nG;

.field private final e:Landroid/view/View$OnClickListener;

.field public final f:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/facebook/content/SecureContextHelper;LX/0Zb;LX/1nG;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1407969
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1407970
    iput-object p1, p0, LX/8qp;->a:Landroid/content/Context;

    .line 1407971
    iput-object p2, p0, LX/8qp;->b:Lcom/facebook/content/SecureContextHelper;

    .line 1407972
    iput-object p3, p0, LX/8qp;->c:LX/0Zb;

    .line 1407973
    iput-object p4, p0, LX/8qp;->d:LX/1nG;

    .line 1407974
    new-instance v0, LX/8qm;

    invoke-direct {v0, p0}, LX/8qm;-><init>(LX/8qp;)V

    iput-object v0, p0, LX/8qp;->f:Landroid/view/View$OnClickListener;

    .line 1407975
    new-instance v0, LX/8qn;

    invoke-direct {v0, p0}, LX/8qn;-><init>(LX/8qp;)V

    iput-object v0, p0, LX/8qp;->e:Landroid/view/View$OnClickListener;

    .line 1407976
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/1y5;Lcom/facebook/analytics/logger/HoneyClientEvent;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 1407977
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1407978
    iget-object v0, p0, LX/8qp;->d:LX/1nG;

    invoke-interface {p2}, LX/1y5;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-interface {p2}, LX/1y5;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1nG;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 1407979
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1407980
    :goto_0
    return-void

    .line 1407981
    :cond_0
    iget-object v1, p0, LX/8qp;->c:LX/0Zb;

    invoke-interface {v1, p3}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1407982
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1407983
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    .line 1407984
    if-nez p4, :cond_1

    .line 1407985
    new-instance p4, Landroid/os/Bundle;

    invoke-direct {p4}, Landroid/os/Bundle;-><init>()V

    .line 1407986
    :cond_1
    invoke-interface {p2}, LX/1y5;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    invoke-interface {p2}, LX/1y5;->e()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p2}, LX/1y5;->m()LX/3Bf;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {p2}, LX/1y5;->m()LX/3Bf;

    move-result-object v0

    invoke-interface {v0}, LX/3Bf;->b()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-interface {p2}, LX/1y5;->v_()Ljava/lang/String;

    move-result-object v4

    invoke-static {p4, v2, v3, v0, v4}, LX/5ve;->a(Landroid/os/Bundle;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1407987
    invoke-virtual {v1, p4}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1407988
    iget-object v0, p0, LX/8qp;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0

    .line 1407989
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Landroid/view/View;Lcom/facebook/graphql/model/GraphQLProfile;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 2

    .prologue
    .line 1407990
    const/4 v0, 0x0

    .line 1407991
    invoke-static {p2}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLProfile;)LX/1y5;

    move-result-object v1

    .line 1407992
    if-eqz p1, :cond_0

    if-eqz v1, :cond_0

    .line 1407993
    const p2, 0x7f0d006a

    invoke-virtual {p1, p2, p3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1407994
    const p2, 0x7f0d0068

    invoke-virtual {p1, p2, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1407995
    const p2, 0x7f0d0069

    invoke-virtual {p1, p2, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1407996
    iget-object p2, p0, LX/8qp;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1407997
    :goto_0
    return-void

    .line 1407998
    :cond_0
    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method
