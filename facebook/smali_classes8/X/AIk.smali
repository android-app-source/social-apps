.class public LX/AIk;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/concurrent/Executor;

.field public final c:LX/0tX;

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1660260
    const-class v0, LX/1Fm;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AIk;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;LX/0tX;LX/0Or;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            "LX/0tX;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1660261
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1660262
    iput-object p1, p0, LX/AIk;->b:Ljava/util/concurrent/Executor;

    .line 1660263
    iput-object p2, p0, LX/AIk;->c:LX/0tX;

    .line 1660264
    iput-object p3, p0, LX/AIk;->d:LX/0Or;

    .line 1660265
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;DDLX/Efx;)V
    .locals 7

    .prologue
    .line 1660266
    move-object v0, p0

    move-object v1, p1

    move-wide v2, p3

    move-wide v4, p5

    .line 1660267
    new-instance v6, LX/AHx;

    invoke-direct {v6}, LX/AHx;-><init>()V

    move-object p1, v6

    .line 1660268
    new-instance p2, LX/4E7;

    invoke-direct {p2}, LX/4E7;-><init>()V

    iget-object v6, v0, LX/AIk;->d:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 1660269
    const-string p3, "actor_id"

    invoke-virtual {p2, p3, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1660270
    move-object v6, p2

    .line 1660271
    iget-object p2, p1, LX/0gW;->h:Ljava/lang/String;

    move-object p2, p2

    .line 1660272
    const-string p3, "client_mutation_id"

    invoke-virtual {v6, p3, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1660273
    move-object v6, v6

    .line 1660274
    const-string p2, "thread_id"

    invoke-virtual {v6, p2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1660275
    move-object v6, v6

    .line 1660276
    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p2

    .line 1660277
    const-string p3, "x_position"

    invoke-virtual {v6, p3, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1660278
    move-object v6, v6

    .line 1660279
    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p2

    .line 1660280
    const-string p3, "y_position"

    invoke-virtual {v6, p3, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1660281
    move-object v6, v6

    .line 1660282
    const-string p2, "1"

    invoke-virtual {p1, p2, v6}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1660283
    invoke-static {p1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v6

    .line 1660284
    iget-object p1, v0, LX/AIk;->c:LX/0tX;

    invoke-virtual {p1, v6}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    move-object v0, v6

    .line 1660285
    new-instance v1, LX/AIj;

    invoke-direct {v1, p0, p7}, LX/AIj;-><init>(LX/AIk;LX/Efx;)V

    iget-object v2, p0, LX/AIk;->b:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1660286
    return-void
.end method
