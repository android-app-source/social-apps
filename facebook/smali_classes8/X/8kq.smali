.class public final LX/8kq;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/keyboard/StickerKeyboardView;)V
    .locals 0

    .prologue
    .line 1396832
    iput-object p1, p0, LX/8kq;->a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/stickers/model/Sticker;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1396833
    iget-object v0, p0, LX/8kq;->a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    .line 1396834
    iget-object v1, v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->f:LX/8ky;

    if-eqz v1, :cond_3

    .line 1396835
    iget-object v1, v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->n:LX/8kd;

    .line 1396836
    const-string v2, "sticker_keyboard"

    invoke-static {v2}, LX/8j7;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 1396837
    const-string v3, "action"

    const-string p0, "sticker_selected"

    invoke-virtual {v2, v3, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1396838
    const-string v3, "sticker"

    iget-object p0, p1, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1396839
    const-string v3, "sticker_pack"

    invoke-virtual {v2, v3, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1396840
    iget-object v3, v1, LX/8kd;->a:LX/8j7;

    invoke-virtual {v3, v2}, LX/8j7;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1396841
    const/16 v3, 0x10

    .line 1396842
    iget-object v1, v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->A:Ljava/util/List;

    iget-object v2, p1, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    .line 1396843
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p0

    if-eqz p0, :cond_4

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/facebook/stickers/model/Sticker;

    .line 1396844
    iget-object p0, p0, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 1396845
    const/4 p0, 0x1

    .line 1396846
    :goto_0
    move v1, p0

    .line 1396847
    if-nez v1, :cond_1

    .line 1396848
    iget-object v1, v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->A:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1396849
    iget-object v1, v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->A:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v3, :cond_1

    .line 1396850
    iget-object v1, v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->A:Ljava/util/List;

    iget-object v2, v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->A:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {v1, v3, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 1396851
    :cond_1
    invoke-static {v0}, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->n(Lcom/facebook/stickers/keyboard/StickerKeyboardView;)V

    .line 1396852
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1396853
    const-string v2, "sticker"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1396854
    iget-object v2, v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->o:LX/0aG;

    const-string v3, "update_recent_stickers"

    const p0, -0x19717304

    invoke-static {v2, v3, v1, p0}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v1

    invoke-interface {v1}, LX/1MF;->start()LX/1ML;

    move-result-object v1

    .line 1396855
    new-instance v2, LX/8kx;

    invoke-direct {v2, v0, p1}, LX/8kx;-><init>(Lcom/facebook/stickers/keyboard/StickerKeyboardView;Lcom/facebook/stickers/model/Sticker;)V

    .line 1396856
    invoke-static {v1, v2}, LX/1Mv;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)LX/1Mv;

    move-result-object v3

    iput-object v3, v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->t:LX/1Mv;

    .line 1396857
    iget-object v3, v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->i:LX/0TD;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1396858
    iget-object v1, v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->e:LX/8jL;

    sget-object v2, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 1396859
    iget-object v3, v1, LX/8jL;->b:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p1, Lcom/facebook/stickers/model/Sticker;->e:Landroid/net/Uri;

    if-eqz v3, :cond_5

    iget-object v3, p1, Lcom/facebook/stickers/model/Sticker;->f:Landroid/net/Uri;

    if-nez v3, :cond_5

    .line 1396860
    iget-object v3, p1, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    sget-object p0, LX/3e1;->ANIMATED:LX/3e1;

    iget-object p2, p1, Lcom/facebook/stickers/model/Sticker;->e:Landroid/net/Uri;

    invoke-static {v1, v3, p0, p2, v2}, LX/8jL;->a(LX/8jL;Ljava/lang/String;LX/3e1;Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1396861
    :cond_2
    :goto_1
    iget-object v1, v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->f:LX/8ky;

    invoke-interface {v1, p1}, LX/8ky;->a(Lcom/facebook/stickers/model/Sticker;)V

    .line 1396862
    :cond_3
    return-void

    :cond_4
    const/4 p0, 0x0

    goto :goto_0

    .line 1396863
    :cond_5
    iget-object v3, p1, Lcom/facebook/stickers/model/Sticker;->f:Landroid/net/Uri;

    if-nez v3, :cond_2

    iget-object v3, p1, Lcom/facebook/stickers/model/Sticker;->d:Landroid/net/Uri;

    if-nez v3, :cond_2

    .line 1396864
    iget-object v3, p1, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    sget-object p0, LX/3e1;->STATIC:LX/3e1;

    iget-object p2, p1, Lcom/facebook/stickers/model/Sticker;->c:Landroid/net/Uri;

    invoke-static {v1, v3, p0, p2, v2}, LX/8jL;->a(LX/8jL;Ljava/lang/String;LX/3e1;Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1396865
    iget-object v0, p0, LX/8kq;->a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    iget-object v0, v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->f:LX/8ky;

    if-eqz v0, :cond_0

    .line 1396866
    :cond_0
    return-void
.end method

.method public final b(Lcom/facebook/stickers/model/StickerPack;)V
    .locals 1

    .prologue
    .line 1396867
    iget-object v0, p0, LX/8kq;->a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    iget-object v0, v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->L:Lcom/facebook/stickers/client/StickerDownloadManager;

    invoke-virtual {v0, p1}, Lcom/facebook/stickers/client/StickerDownloadManager;->a(Lcom/facebook/stickers/model/StickerPack;)V

    .line 1396868
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1396869
    iget-object v0, p0, LX/8kq;->a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    iget-object v0, v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->f:LX/8ky;

    if-eqz v0, :cond_0

    .line 1396870
    :cond_0
    return-void
.end method
