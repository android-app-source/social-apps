.class public final LX/9LF;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 24

    .prologue
    .line 1470487
    const/16 v20, 0x0

    .line 1470488
    const/16 v19, 0x0

    .line 1470489
    const/16 v18, 0x0

    .line 1470490
    const/16 v17, 0x0

    .line 1470491
    const/16 v16, 0x0

    .line 1470492
    const/4 v15, 0x0

    .line 1470493
    const/4 v14, 0x0

    .line 1470494
    const/4 v13, 0x0

    .line 1470495
    const/4 v12, 0x0

    .line 1470496
    const/4 v11, 0x0

    .line 1470497
    const/4 v10, 0x0

    .line 1470498
    const/4 v9, 0x0

    .line 1470499
    const/4 v8, 0x0

    .line 1470500
    const/4 v7, 0x0

    .line 1470501
    const/4 v6, 0x0

    .line 1470502
    const/4 v5, 0x0

    .line 1470503
    const/4 v4, 0x0

    .line 1470504
    const/4 v3, 0x0

    .line 1470505
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v21

    sget-object v22, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-eq v0, v1, :cond_1

    .line 1470506
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1470507
    const/4 v3, 0x0

    .line 1470508
    :goto_0
    return v3

    .line 1470509
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1470510
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v21

    sget-object v22, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-eq v0, v1, :cond_f

    .line 1470511
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v21

    .line 1470512
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1470513
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v22

    sget-object v23, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    if-eq v0, v1, :cond_1

    if-eqz v21, :cond_1

    .line 1470514
    const-string v22, "client_section_header"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_2

    .line 1470515
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v20

    goto :goto_1

    .line 1470516
    :cond_2
    const-string v22, "cross_post_suggestions"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_3

    .line 1470517
    invoke-static/range {p0 .. p1}, LX/9L9;->a(LX/15w;LX/186;)I

    move-result v19

    goto :goto_1

    .line 1470518
    :cond_3
    const-string v22, "currencies"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_4

    .line 1470519
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v18

    goto :goto_1

    .line 1470520
    :cond_4
    const-string v22, "for_sale_categories"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_5

    .line 1470521
    invoke-static/range {p0 .. p1}, LX/9LC;->a(LX/15w;LX/186;)I

    move-result v17

    goto :goto_1

    .line 1470522
    :cond_5
    const-string v22, "id"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_6

    .line 1470523
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    goto :goto_1

    .line 1470524
    :cond_6
    const-string v22, "is_category_optional"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_7

    .line 1470525
    const/4 v6, 0x1

    .line 1470526
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v15

    goto :goto_1

    .line 1470527
    :cond_7
    const-string v22, "is_post_intercept_enabled"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_8

    .line 1470528
    const/4 v5, 0x1

    .line 1470529
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v14

    goto/16 :goto_1

    .line 1470530
    :cond_8
    const-string v22, "location_picker_setting"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_9

    .line 1470531
    invoke-static/range {p0 .. p1}, LX/9LD;->a(LX/15w;LX/186;)I

    move-result v13

    goto/16 :goto_1

    .line 1470532
    :cond_9
    const-string v22, "marketplace_cross_post_setting"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_a

    .line 1470533
    invoke-static/range {p0 .. p1}, LX/9LE;->a(LX/15w;LX/186;)I

    move-result v12

    goto/16 :goto_1

    .line 1470534
    :cond_a
    const-string v22, "post_intercept_words"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_b

    .line 1470535
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 1470536
    :cond_b
    const-string v22, "post_intercept_words_after_number"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_c

    .line 1470537
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v10

    goto/16 :goto_1

    .line 1470538
    :cond_c
    const-string v22, "prefill_category_id"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_d

    .line 1470539
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto/16 :goto_1

    .line 1470540
    :cond_d
    const-string v22, "should_show_autos_options_in_composer"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_e

    .line 1470541
    const/4 v4, 0x1

    .line 1470542
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v8

    goto/16 :goto_1

    .line 1470543
    :cond_e
    const-string v22, "user_group_commerce_post_to_marketplace_state"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_0

    .line 1470544
    const/4 v3, 0x1

    .line 1470545
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    goto/16 :goto_1

    .line 1470546
    :cond_f
    const/16 v21, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1470547
    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1470548
    const/16 v20, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1470549
    const/16 v19, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1470550
    const/16 v18, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1470551
    const/16 v17, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1470552
    if-eqz v6, :cond_10

    .line 1470553
    const/4 v6, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v15}, LX/186;->a(IZ)V

    .line 1470554
    :cond_10
    if-eqz v5, :cond_11

    .line 1470555
    const/4 v5, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v14}, LX/186;->a(IZ)V

    .line 1470556
    :cond_11
    const/4 v5, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v13}, LX/186;->b(II)V

    .line 1470557
    const/16 v5, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v12}, LX/186;->b(II)V

    .line 1470558
    const/16 v5, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v11}, LX/186;->b(II)V

    .line 1470559
    const/16 v5, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v10}, LX/186;->b(II)V

    .line 1470560
    const/16 v5, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v9}, LX/186;->b(II)V

    .line 1470561
    if-eqz v4, :cond_12

    .line 1470562
    const/16 v4, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v8}, LX/186;->a(IZ)V

    .line 1470563
    :cond_12
    if-eqz v3, :cond_13

    .line 1470564
    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->a(IZ)V

    .line 1470565
    :cond_13
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const/16 v4, 0xa

    const/16 v3, 0x9

    const/4 v2, 0x2

    .line 1470428
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1470429
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1470430
    if-eqz v0, :cond_0

    .line 1470431
    const-string v1, "client_section_header"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1470432
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1470433
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1470434
    if-eqz v0, :cond_1

    .line 1470435
    const-string v1, "cross_post_suggestions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1470436
    invoke-static {p0, v0, p2, p3}, LX/9L9;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1470437
    :cond_1
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1470438
    if-eqz v0, :cond_2

    .line 1470439
    const-string v0, "currencies"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1470440
    invoke-virtual {p0, p1, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1470441
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1470442
    if-eqz v0, :cond_3

    .line 1470443
    const-string v1, "for_sale_categories"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1470444
    invoke-static {p0, v0, p2, p3}, LX/9LC;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1470445
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1470446
    if-eqz v0, :cond_4

    .line 1470447
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1470448
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1470449
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1470450
    if-eqz v0, :cond_5

    .line 1470451
    const-string v1, "is_category_optional"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1470452
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1470453
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1470454
    if-eqz v0, :cond_6

    .line 1470455
    const-string v1, "is_post_intercept_enabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1470456
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1470457
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1470458
    if-eqz v0, :cond_7

    .line 1470459
    const-string v1, "location_picker_setting"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1470460
    invoke-static {p0, v0, p2}, LX/9LD;->a(LX/15i;ILX/0nX;)V

    .line 1470461
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1470462
    if-eqz v0, :cond_8

    .line 1470463
    const-string v1, "marketplace_cross_post_setting"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1470464
    invoke-static {p0, v0, p2}, LX/9LE;->a(LX/15i;ILX/0nX;)V

    .line 1470465
    :cond_8
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1470466
    if-eqz v0, :cond_9

    .line 1470467
    const-string v0, "post_intercept_words"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1470468
    invoke-virtual {p0, p1, v3}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1470469
    :cond_9
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 1470470
    if-eqz v0, :cond_a

    .line 1470471
    const-string v0, "post_intercept_words_after_number"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1470472
    invoke-virtual {p0, p1, v4}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1470473
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1470474
    if-eqz v0, :cond_b

    .line 1470475
    const-string v1, "prefill_category_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1470476
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1470477
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1470478
    if-eqz v0, :cond_c

    .line 1470479
    const-string v1, "should_show_autos_options_in_composer"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1470480
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1470481
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1470482
    if-eqz v0, :cond_d

    .line 1470483
    const-string v1, "user_group_commerce_post_to_marketplace_state"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1470484
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1470485
    :cond_d
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1470486
    return-void
.end method
