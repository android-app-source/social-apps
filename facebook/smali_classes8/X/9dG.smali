.class public final LX/9dG;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source ""


# instance fields
.field public final synthetic a:LX/9dI;


# direct methods
.method public constructor <init>(LX/9dI;)V
    .locals 0

    .prologue
    .line 1517842
    iput-object p1, p0, LX/9dG;->a:LX/9dI;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 1517843
    iget-object v0, p0, LX/9dG;->a:LX/9dI;

    iget-object v1, p0, LX/9dG;->a:LX/9dI;

    iget-object v1, v1, LX/9dI;->y:Landroid/view/View$OnClickListener;

    invoke-static {v0, v1}, LX/9dI;->c(LX/9dI;Landroid/view/View$OnClickListener;)Z

    move-result v0

    return v0
.end method

.method public final onDown(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1517844
    iget-object v0, p0, LX/9dG;->a:LX/9dI;

    invoke-static {v0, v2}, LX/9dI;->c(LX/9dI;Z)V

    .line 1517845
    iget-object v0, p0, LX/9dG;->a:LX/9dI;

    iget-boolean v0, v0, LX/9dI;->t:Z

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 1517846
    :goto_0
    iget-object v1, p0, LX/9dG;->a:LX/9dI;

    iget-object v1, v1, LX/9dI;->q:LX/5jK;

    invoke-virtual {v1}, LX/5jK;->g()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1517847
    iget-object v1, p0, LX/9dG;->a:LX/9dI;

    iget-object v1, v1, LX/9dI;->p:LX/8Ge;

    .line 1517848
    const/4 p1, 0x1

    iput-boolean p1, v1, LX/8Ge;->j:Z

    .line 1517849
    iget-object v1, p0, LX/9dG;->a:LX/9dI;

    .line 1517850
    iput-boolean v2, v1, LX/9dI;->c:Z

    .line 1517851
    iget-object v1, p0, LX/9dG;->a:LX/9dI;

    .line 1517852
    iput v0, v1, LX/9dI;->d:F

    .line 1517853
    iget-object v1, p0, LX/9dG;->a:LX/9dI;

    .line 1517854
    iput v0, v1, LX/9dI;->e:F

    .line 1517855
    :goto_1
    iget-object v0, p0, LX/9dG;->a:LX/9dI;

    iget-object v0, v0, LX/9dI;->v:Lcom/facebook/widget/ScrollingAwareScrollView;

    if-eqz v0, :cond_0

    .line 1517856
    iget-object v0, p0, LX/9dG;->a:LX/9dI;

    iget-object v0, v0, LX/9dI;->v:Lcom/facebook/widget/ScrollingAwareScrollView;

    iget-object v1, p0, LX/9dG;->a:LX/9dI;

    iget-object v1, v1, LX/9dI;->l:LX/4oU;

    .line 1517857
    iput-object v1, v0, Lcom/facebook/widget/ScrollingAwareScrollView;->e:LX/4oU;

    .line 1517858
    :cond_0
    return v2

    .line 1517859
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    goto :goto_0

    .line 1517860
    :cond_2
    iget-object v1, p0, LX/9dG;->a:LX/9dI;

    invoke-static {v1, v0}, LX/9dI;->c$redex0(LX/9dI;F)V

    .line 1517861
    iget-object v1, p0, LX/9dG;->a:LX/9dI;

    iget-object v1, v1, LX/9dI;->q:LX/5jK;

    .line 1517862
    iput v0, v1, LX/5jK;->c:F

    .line 1517863
    goto :goto_1
.end method

.method public final onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 1517864
    iget-object v0, p0, LX/9dG;->a:LX/9dI;

    iget-boolean v0, v0, LX/9dI;->t:Z

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 1517865
    :goto_0
    iget-object v1, p0, LX/9dG;->a:LX/9dI;

    iget-boolean v1, v1, LX/9dI;->t:Z

    if-eqz v1, :cond_3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 1517866
    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    .line 1517867
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    sub-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    .line 1517868
    iget-object v6, p0, LX/9dG;->a:LX/9dI;

    iget-boolean v6, v6, LX/9dI;->t:Z

    if-eqz v6, :cond_0

    cmpg-float v6, v4, v5

    if-ltz v6, :cond_1

    :cond_0
    iget-object v6, p0, LX/9dG;->a:LX/9dI;

    iget-boolean v6, v6, LX/9dI;->t:Z

    if-nez v6, :cond_4

    cmpg-float v4, v5, v4

    if-gez v4, :cond_4

    :cond_1
    move v0, v2

    .line 1517869
    :goto_2
    return v0

    .line 1517870
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    goto :goto_0

    .line 1517871
    :cond_3
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    goto :goto_1

    .line 1517872
    :cond_4
    iget-object v4, p0, LX/9dG;->a:LX/9dI;

    iget-object v4, v4, LX/9dI;->r:LX/9d1;

    invoke-interface {v4}, LX/9d1;->b()Z

    move-result v4

    if-nez v4, :cond_5

    move v0, v2

    .line 1517873
    goto :goto_2

    .line 1517874
    :cond_5
    iget-object v2, p0, LX/9dG;->a:LX/9dI;

    invoke-static {v2, v3}, LX/9dI;->c(LX/9dI;Z)V

    .line 1517875
    iget-object v2, p0, LX/9dG;->a:LX/9dI;

    iget-object v2, v2, LX/9dI;->r:LX/9d1;

    invoke-interface {v2}, LX/9d1;->a()Z

    move-result v2

    if-nez v2, :cond_6

    .line 1517876
    iget-object v2, p0, LX/9dG;->a:LX/9dI;

    new-instance v4, LX/3rL;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-direct {v4, v0, v1}, LX/3rL;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1517877
    iput-object v4, v2, LX/9dI;->f:LX/3rL;

    .line 1517878
    move v0, v3

    .line 1517879
    goto :goto_2

    .line 1517880
    :cond_6
    iget-object v0, p0, LX/9dG;->a:LX/9dI;

    iget-object v0, v0, LX/9dI;->q:LX/5jK;

    invoke-virtual {v0}, LX/5jK;->h()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, LX/9dG;->a:LX/9dI;

    iget-object v0, v0, LX/9dI;->q:LX/5jK;

    invoke-virtual {v0}, LX/5jK;->g()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1517881
    :cond_7
    iget-object v0, p0, LX/9dG;->a:LX/9dI;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2, v3, v3}, LX/9dI;->a$redex0(LX/9dI;FFZZ)V

    :cond_8
    move v0, v3

    .line 1517882
    goto :goto_2
.end method

.method public final onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1517883
    iget-object v0, p0, LX/9dG;->a:LX/9dI;

    iget-object v0, v0, LX/9dI;->r:LX/9d1;

    invoke-interface {v0}, LX/9d1;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9dG;->a:LX/9dI;

    iget-boolean v0, v0, LX/9dI;->B:Z

    if-nez v0, :cond_1

    :cond_0
    move v0, v3

    .line 1517884
    :goto_0
    return v0

    .line 1517885
    :cond_1
    iget-object v0, p0, LX/9dG;->a:LX/9dI;

    iget-object v0, v0, LX/9dI;->q:LX/5jK;

    invoke-virtual {v0}, LX/5jK;->a()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget-object v1, p0, LX/9dG;->a:LX/9dI;

    iget-object v1, v1, LX/9dI;->b:Landroid/view/ViewConfiguration;

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    .line 1517886
    iget-object v0, p0, LX/9dG;->a:LX/9dI;

    invoke-static {v0, v2}, LX/9dI;->c(LX/9dI;Z)V

    .line 1517887
    :cond_2
    iget-object v0, p0, LX/9dG;->a:LX/9dI;

    iget-boolean v0, v0, LX/9dI;->t:Z

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 1517888
    :goto_1
    iget-object v1, p0, LX/9dG;->a:LX/9dI;

    iget-boolean v1, v1, LX/9dI;->t:Z

    if-eqz v1, :cond_5

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 1517889
    :goto_2
    iget-object v4, p0, LX/9dG;->a:LX/9dI;

    iget-boolean v4, v4, LX/9dI;->t:Z

    if-eqz v4, :cond_6

    .line 1517890
    :goto_3
    iget-object v4, p0, LX/9dG;->a:LX/9dI;

    iget-boolean v4, v4, LX/9dI;->c:Z

    if-eqz v4, :cond_7

    .line 1517891
    iget-object v3, p0, LX/9dG;->a:LX/9dI;

    .line 1517892
    iput v1, v3, LX/9dI;->e:F

    .line 1517893
    iget-object v3, p0, LX/9dG;->a:LX/9dI;

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v4

    invoke-static {v3, v4}, LX/9dI;->d$redex0(LX/9dI;F)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1517894
    iget-object v3, p0, LX/9dG;->a:LX/9dI;

    new-instance v4, LX/3rL;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-direct {v4, v0, v1}, LX/3rL;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1517895
    iput-object v4, v3, LX/9dI;->f:LX/3rL;

    .line 1517896
    :cond_3
    move v0, v2

    .line 1517897
    goto :goto_0

    .line 1517898
    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    goto :goto_1

    .line 1517899
    :cond_5
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    goto :goto_2

    :cond_6
    move p3, p4

    .line 1517900
    goto :goto_3

    .line 1517901
    :cond_7
    iget-object v0, p0, LX/9dG;->a:LX/9dI;

    iget-object v0, v0, LX/9dI;->q:LX/5jK;

    invoke-virtual {v0}, LX/5jK;->c()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1517902
    iget-object v0, p0, LX/9dG;->a:LX/9dI;

    sget-object v2, LX/5jJ;->SWIPING:LX/5jJ;

    invoke-static {v0, v2}, LX/9dI;->a$redex0(LX/9dI;LX/5jJ;)V

    .line 1517903
    :cond_8
    iget-object v0, p0, LX/9dG;->a:LX/9dI;

    iget-object v0, v0, LX/9dI;->q:LX/5jK;

    invoke-virtual {v0}, LX/5jK;->h()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1517904
    iget-object v0, p0, LX/9dG;->a:LX/9dI;

    invoke-static {v0, v1, p3, v3, v3}, LX/9dI;->a$redex0(LX/9dI;FFZZ)V

    :cond_9
    move v0, v3

    .line 1517905
    goto/16 :goto_0
.end method

.method public final onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 1517906
    iget-object v0, p0, LX/9dG;->a:LX/9dI;

    iget-object v1, p0, LX/9dG;->a:LX/9dI;

    iget-object v1, v1, LX/9dI;->w:Landroid/view/View$OnClickListener;

    invoke-static {v0, v1}, LX/9dI;->c(LX/9dI;Landroid/view/View$OnClickListener;)Z

    move-result v0

    return v0
.end method
