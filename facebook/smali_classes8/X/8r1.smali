.class public LX/8r1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/graphics/drawable/Drawable;

.field public b:Landroid/graphics/drawable/Drawable;

.field public c:I

.field public d:I

.field public e:I

.field public f:LX/8qr;

.field public g:LX/1zt;

.field public h:Z

.field public i:Z

.field public j:Z

.field public k:Z

.field public l:Z

.field public m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/text/Spannable;",
            ">;"
        }
    .end annotation
.end field

.field private final n:LX/3iM;

.field public final o:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/11S;",
            ">;"
        }
    .end annotation
.end field

.field public final p:Z


# direct methods
.method public constructor <init>(LX/8qr;LX/3iM;LX/0Or;ZZZZZ)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8qr;",
            "LX/3iM;",
            "LX/0Or",
            "<",
            "LX/11S;",
            ">;ZZZZZ)V"
        }
    .end annotation

    .prologue
    .line 1408257
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1408258
    sget-object v0, LX/1zt;->c:LX/1zt;

    iput-object v0, p0, LX/8r1;->g:LX/1zt;

    .line 1408259
    iput-object p1, p0, LX/8r1;->f:LX/8qr;

    .line 1408260
    iput-boolean p4, p0, LX/8r1;->h:Z

    .line 1408261
    iput-boolean p5, p0, LX/8r1;->i:Z

    .line 1408262
    iput-boolean p6, p0, LX/8r1;->p:Z

    .line 1408263
    iput-boolean p7, p0, LX/8r1;->k:Z

    .line 1408264
    iput-boolean p8, p0, LX/8r1;->l:Z

    .line 1408265
    if-eqz p7, :cond_0

    .line 1408266
    new-instance v0, LX/0wM;

    invoke-virtual {p1}, LX/8qr;->b()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0wM;-><init>(Landroid/content/res/Resources;)V

    .line 1408267
    const v1, 0x7f0208fc

    invoke-virtual {p1}, LX/8qr;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00a2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, LX/8r1;->a:Landroid/graphics/drawable/Drawable;

    .line 1408268
    const v1, 0x7f0208fc

    invoke-virtual {p1}, LX/8qr;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a008a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/8r1;->b:Landroid/graphics/drawable/Drawable;

    .line 1408269
    :goto_0
    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    .line 1408270
    if-eqz p7, :cond_1

    const v0, 0x7f0105a5

    move v1, v0

    .line 1408271
    :goto_1
    if-eqz p7, :cond_2

    const v0, 0x7f0a00a8

    .line 1408272
    :goto_2
    iget-object v3, p1, LX/8qr;->a:Landroid/content/Context;

    move-object v3, v3

    .line 1408273
    invoke-virtual {v3}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v1, v2, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 1408274
    invoke-virtual {p1}, LX/8qr;->b()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, v2, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, LX/8r1;->c:I

    .line 1408275
    invoke-virtual {p1}, LX/8qr;->b()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, LX/8r1;->d:I

    .line 1408276
    invoke-virtual {p1}, LX/8qr;->b()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a008a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, LX/8r1;->e:I

    .line 1408277
    iput-object p2, p0, LX/8r1;->n:LX/3iM;

    .line 1408278
    iput-object p3, p0, LX/8r1;->o:LX/0Or;

    .line 1408279
    return-void

    .line 1408280
    :cond_0
    invoke-virtual {p1}, LX/8qr;->b()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0219cc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/8r1;->b:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, LX/8r1;->a:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 1408281
    :cond_1
    const v0, 0x7f0105a4

    move v1, v0

    goto :goto_1

    .line 1408282
    :cond_2
    const v0, 0x7f0a042e

    goto :goto_2
.end method

.method private a(I)Landroid/text/Spannable;
    .locals 5

    .prologue
    .line 1408252
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, " "

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v0, p0, LX/8r1;->p:Z

    if-eqz v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1408253
    new-instance v1, Landroid/text/SpannableString;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, LX/8r1;->f:LX/8qr;

    invoke-virtual {v3}, LX/8qr;->b()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080fc7

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1408254
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v0, p1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v2, 0x0

    invoke-interface {v1}, Landroid/text/Spannable;->length()I

    move-result v3

    const/16 v4, 0x21

    invoke-interface {v1, v0, v2, v3, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 1408255
    return-object v1

    .line 1408256
    :cond_0
    const-string v0, "  "

    goto :goto_0
.end method

.method public static d(LX/8r1;)Landroid/text/Spannable;
    .locals 1

    .prologue
    .line 1408217
    iget v0, p0, LX/8r1;->c:I

    invoke-direct {p0, v0}, LX/8r1;->a(I)Landroid/text/Spannable;

    move-result-object v0

    return-object v0
.end method

.method public static d(LX/8r1;Lcom/facebook/graphql/model/GraphQLComment;)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/text/Spannable;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v10, 0x21

    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 1408230
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->s_()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-static {v0}, LX/16z;->n(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-static {v0}, LX/16z;->o(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v0

    if-nez v0, :cond_1

    .line 1408231
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1408232
    :goto_0
    return-object v0

    .line 1408233
    :cond_1
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1408234
    invoke-virtual {p0}, LX/8r1;->a()Landroid/text/Spannable;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1408235
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->s_()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-static {v0}, LX/16z;->n(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v0

    if-nez v0, :cond_2

    move v0, v1

    .line 1408236
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "   "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/text/NumberFormat;->getInstance()Ljava/text/NumberFormat;

    move-result-object v3

    int-to-long v8, v0

    invoke-virtual {v3, v8, v9}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1408237
    new-instance v7, Landroid/text/SpannableString;

    invoke-virtual {p0, v2}, LX/8r1;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v7, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1408238
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->s_()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/8r1;->b:Landroid/graphics/drawable/Drawable;

    .line 1408239
    :goto_2
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    invoke-virtual {v2, v4, v4, v3, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1408240
    iget-boolean v3, p0, LX/8r1;->p:Z

    if-eqz v3, :cond_4

    const/4 v3, 0x2

    .line 1408241
    :goto_3
    iget-boolean v5, p0, LX/8r1;->k:Z

    if-eqz v5, :cond_5

    move v5, v4

    .line 1408242
    :goto_4
    new-instance v8, LX/34T;

    invoke-direct {v8, v2, v5}, LX/34T;-><init>(Landroid/graphics/drawable/Drawable;I)V

    add-int/lit8 v2, v3, 0x1

    invoke-interface {v7, v8, v3, v2, v10}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 1408243
    new-instance v2, LX/8qv;

    iget-object v3, p0, LX/8r1;->f:LX/8qr;

    .line 1408244
    iget-object v5, v3, LX/8qr;->a:Landroid/content/Context;

    move-object v3, v5

    .line 1408245
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f0f0060

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v1, v4

    invoke-virtual {v3, v5, v0, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, p0, v0, p1}, LX/8qv;-><init>(LX/8r1;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLComment;)V

    invoke-interface {v7}, Landroid/text/Spannable;->length()I

    move-result v0

    invoke-interface {v7, v2, v4, v0, v10}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 1408246
    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v0, v6

    .line 1408247
    goto/16 :goto_0

    .line 1408248
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-static {v0}, LX/16z;->o(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v0

    goto :goto_1

    .line 1408249
    :cond_3
    iget-object v2, p0, LX/8r1;->a:Landroid/graphics/drawable/Drawable;

    goto :goto_2

    :cond_4
    move v3, v4

    .line 1408250
    goto :goto_3

    :cond_5
    move v5, v1

    .line 1408251
    goto :goto_4
.end method

.method public static g(LX/8r1;Lcom/facebook/graphql/model/GraphQLComment;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/text/Spannable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1408283
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1408284
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1408285
    :goto_0
    return-object v0

    .line 1408286
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1408287
    invoke-virtual {p0}, LX/8r1;->a()Landroid/text/Spannable;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1408288
    iget-object v1, p0, LX/8r1;->f:LX/8qr;

    .line 1408289
    iget-object v2, v1, LX/8qr;->a:Landroid/content/Context;

    move-object v1, v2

    .line 1408290
    const v2, 0x7f080fe9

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1408291
    new-instance v2, Landroid/text/SpannableString;

    invoke-virtual {p0, v1}, LX/8r1;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1408292
    new-instance v1, LX/8qz;

    invoke-direct {v1, p0}, LX/8qz;-><init>(LX/8r1;)V

    const/4 v3, 0x0

    invoke-interface {v2}, Landroid/text/Spannable;->length()I

    move-result v4

    const/16 v5, 0x21

    invoke-interface {v2, v1, v3, v4, v5}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 1408293
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static h(LX/8r1;Lcom/facebook/graphql/model/GraphQLComment;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/text/Spannable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1408219
    iget-boolean v0, p0, LX/8r1;->i:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1408220
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1408221
    :goto_0
    return-object v0

    .line 1408222
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1408223
    invoke-virtual {p0}, LX/8r1;->a()Landroid/text/Spannable;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1408224
    iget-object v1, p0, LX/8r1;->f:LX/8qr;

    .line 1408225
    iget-object v2, v1, LX/8qr;->a:Landroid/content/Context;

    move-object v1, v2

    .line 1408226
    const v2, 0x7f080fd9

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1408227
    new-instance v2, Landroid/text/SpannableString;

    invoke-virtual {p0, v1}, LX/8r1;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1408228
    new-instance v1, LX/8r0;

    invoke-direct {v1, p0}, LX/8r0;-><init>(LX/8r1;)V

    const/4 v3, 0x0

    invoke-interface {v2}, Landroid/text/Spannable;->length()I

    move-result v4

    const/16 v5, 0x21

    invoke-interface {v2, v1, v3, v4, v5}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 1408229
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/text/Spannable;
    .locals 1

    .prologue
    .line 1408218
    iget-boolean v0, p0, LX/8r1;->j:Z

    if-eqz v0, :cond_0

    iget v0, p0, LX/8r1;->c:I

    :goto_0
    invoke-direct {p0, v0}, LX/8r1;->a(I)Landroid/text/Spannable;

    move-result-object v0

    return-object v0

    :cond_0
    iget v0, p0, LX/8r1;->d:I

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1408214
    iget-boolean v0, p0, LX/8r1;->p:Z

    if-eqz v0, :cond_0

    .line 1408215
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "  "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1408216
    :cond_0
    return-object p1
.end method

.method public a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/text/Spannable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1408095
    iget-object v0, p0, LX/8r1;->f:LX/8qr;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1408096
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/8r1;->m:Ljava/util/List;

    .line 1408097
    iget-object v0, p0, LX/8r1;->n:LX/3iM;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->H()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3iM;->c(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v0

    .line 1408098
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->POSTING:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-ne v0, v1, :cond_0

    .line 1408099
    iget-object v0, p0, LX/8r1;->m:Ljava/util/List;

    .line 1408100
    new-instance v1, Landroid/text/SpannableString;

    iget-object v2, p0, LX/8r1;->f:LX/8qr;

    invoke-virtual {v2}, LX/8qr;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0810f0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1408101
    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    iget v3, p0, LX/8r1;->c:I

    invoke-direct {v2, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v3, 0x0

    invoke-interface {v1}, Landroid/text/Spannable;->length()I

    move-result p1

    const/16 p2, 0x21

    invoke-interface {v1, v2, v3, p1, p2}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 1408102
    move-object v1, v1

    .line 1408103
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1408104
    iget-object v0, p0, LX/8r1;->m:Ljava/util/List;

    .line 1408105
    :goto_0
    return-object v0

    .line 1408106
    :cond_0
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->FAILED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-ne v0, v1, :cond_1

    .line 1408107
    iget-object v0, p0, LX/8r1;->m:Ljava/util/List;

    .line 1408108
    new-instance v1, Landroid/text/SpannableString;

    iget-object v2, p0, LX/8r1;->f:LX/8qr;

    invoke-virtual {v2}, LX/8qr;->b()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0810f1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1408109
    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    iget v3, p0, LX/8r1;->c:I

    invoke-direct {v2, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v3, 0x0

    invoke-interface {v1}, Landroid/text/Spannable;->length()I

    move-result p1

    const/16 p2, 0x21

    invoke-interface {v1, v2, v3, p1, p2}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 1408110
    move-object v1, v1

    .line 1408111
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1408112
    iget-object v0, p0, LX/8r1;->m:Ljava/util/List;

    goto :goto_0

    .line 1408113
    :cond_1
    iget-object v2, p0, LX/8r1;->m:Ljava/util/List;

    .line 1408114
    new-instance v5, Landroid/text/SpannableString;

    iget-object v4, p0, LX/8r1;->o:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/11S;

    sget-object v6, LX/1lB;->STREAM_RELATIVE_STYLE:LX/1lB;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->x()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    invoke-interface {v4, v6, v8, v9}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1408115
    new-instance v4, Landroid/text/style/ForegroundColorSpan;

    iget v6, p0, LX/8r1;->c:I

    invoke-direct {v4, v6}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v6, 0x0

    invoke-interface {v5}, Landroid/text/Spannable;->length()I

    move-result v7

    const/16 v8, 0x21

    invoke-interface {v5, v4, v6, v7, v8}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 1408116
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1408117
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1408118
    iget-boolean v5, p0, LX/8r1;->p:Z

    if-eqz v5, :cond_2

    .line 1408119
    new-instance v5, Landroid/text/SpannableString;

    const-string v6, "  "

    invoke-direct {v5, v6}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1408120
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1408121
    :cond_2
    move-object v3, v4

    .line 1408122
    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1408123
    iget-object v2, p0, LX/8r1;->m:Ljava/util/List;

    .line 1408124
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->y()Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    move-result-object v3

    if-eqz v3, :cond_a

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->y()Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;->a()I

    move-result v3

    if-lez v3, :cond_a

    const/4 v3, 0x1

    :goto_1
    move v3, v3

    .line 1408125
    if-nez v3, :cond_9

    .line 1408126
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1408127
    :goto_2
    move-object v3, v3

    .line 1408128
    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1408129
    iget-boolean v2, p0, LX/8r1;->h:Z

    if-eqz v2, :cond_6

    .line 1408130
    iget-object v2, p0, LX/8r1;->m:Ljava/util/List;

    const/4 v6, 0x0

    .line 1408131
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    .line 1408132
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->o()Z

    move-result v3

    if-nez v3, :cond_b

    .line 1408133
    :cond_3
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    .line 1408134
    :goto_3
    move-object v3, v3

    .line 1408135
    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1408136
    iget-object v2, p0, LX/8r1;->m:Ljava/util/List;

    invoke-static {p0, p2}, LX/8r1;->g(LX/8r1;Lcom/facebook/graphql/model/GraphQLComment;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1408137
    iget-object v2, p0, LX/8r1;->m:Ljava/util/List;

    invoke-static {p0, p1}, LX/8r1;->h(LX/8r1;Lcom/facebook/graphql/model/GraphQLComment;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1408138
    iget-object v2, p0, LX/8r1;->m:Ljava/util/List;

    const/16 v11, 0x21

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 1408139
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    .line 1408140
    if-eqz v3, :cond_4

    invoke-static {v3}, LX/16z;->q(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-static {v3}, LX/16z;->p(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v4

    if-nez v4, :cond_e

    .line 1408141
    :cond_4
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    .line 1408142
    :goto_4
    move-object v3, v3

    .line 1408143
    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1408144
    :cond_5
    :goto_5
    iget-object v0, p0, LX/8r1;->m:Ljava/util/List;

    goto/16 :goto_0

    .line 1408145
    :cond_6
    iget-object v2, p0, LX/8r1;->m:Ljava/util/List;

    .line 1408146
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    if-eqz v3, :cond_7

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->r_()Z

    move-result v3

    if-nez v3, :cond_12

    .line 1408147
    :cond_7
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1408148
    :goto_6
    move-object v3, v3

    .line 1408149
    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1408150
    iget-boolean v2, p0, LX/8r1;->l:Z

    if-nez v2, :cond_8

    .line 1408151
    iget-object v2, p0, LX/8r1;->m:Ljava/util/List;

    invoke-static {p0, p1}, LX/8r1;->d(LX/8r1;Lcom/facebook/graphql/model/GraphQLComment;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1408152
    :cond_8
    iget-object v2, p0, LX/8r1;->m:Ljava/util/List;

    invoke-static {p0, p2}, LX/8r1;->g(LX/8r1;Lcom/facebook/graphql/model/GraphQLComment;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1408153
    iget-object v2, p0, LX/8r1;->m:Ljava/util/List;

    invoke-static {p0, p1}, LX/8r1;->h(LX/8r1;Lcom/facebook/graphql/model/GraphQLComment;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1408154
    iget-boolean v2, p0, LX/8r1;->l:Z

    if-eqz v2, :cond_5

    .line 1408155
    iget-object v2, p0, LX/8r1;->m:Ljava/util/List;

    invoke-static {p0, p1}, LX/8r1;->d(LX/8r1;Lcom/facebook/graphql/model/GraphQLComment;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_5

    .line 1408156
    :cond_9
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1408157
    invoke-static {p0}, LX/8r1;->d(LX/8r1;)Landroid/text/Spannable;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1408158
    iget-object v4, p0, LX/8r1;->f:LX/8qr;

    .line 1408159
    iget-object v5, v4, LX/8qr;->a:Landroid/content/Context;

    move-object v4, v5

    .line 1408160
    const v5, 0x7f0810fe

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1408161
    new-instance v5, Landroid/text/SpannableString;

    invoke-virtual {p0, v4}, LX/8r1;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1408162
    new-instance v4, LX/8qt;

    invoke-direct {v4, p0}, LX/8qt;-><init>(LX/8r1;)V

    const/4 v6, 0x0

    invoke-interface {v5}, Landroid/text/Spannable;->length()I

    move-result v7

    const/16 v8, 0x21

    invoke-interface {v5, v4, v6, v7, v8}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 1408163
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    :cond_a
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 1408164
    :cond_b
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1408165
    invoke-static {p0}, LX/8r1;->d(LX/8r1;)Landroid/text/Spannable;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1408166
    iget-object v3, p0, LX/8r1;->g:LX/1zt;

    sget-object v5, LX/1zt;->c:LX/1zt;

    if-eq v3, v5, :cond_c

    const/4 v3, 0x1

    move v5, v3

    .line 1408167
    :goto_7
    if-eqz v5, :cond_d

    iget-object v3, p0, LX/8r1;->g:LX/1zt;

    .line 1408168
    iget-object v7, v3, LX/1zt;->f:Ljava/lang/String;

    move-object v3, v7

    .line 1408169
    :goto_8
    new-instance v7, Landroid/text/SpannableString;

    invoke-virtual {p0, v3}, LX/8r1;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v7, v3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1408170
    new-instance v3, LX/8qx;

    invoke-direct {v3, p0, v5}, LX/8qx;-><init>(LX/8r1;Z)V

    invoke-interface {v7}, Landroid/text/Spannable;->length()I

    move-result v5

    const/16 v8, 0x21

    invoke-interface {v7, v3, v6, v5, v8}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 1408171
    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v3, v4

    .line 1408172
    goto/16 :goto_3

    :cond_c
    move v5, v6

    .line 1408173
    goto :goto_7

    .line 1408174
    :cond_d
    iget-object v3, p0, LX/8r1;->f:LX/8qr;

    .line 1408175
    iget-object v7, v3, LX/8qr;->a:Landroid/content/Context;

    move-object v3, v7

    .line 1408176
    const v7, 0x7f080fc8

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_8

    .line 1408177
    :cond_e
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1408178
    invoke-virtual {p0}, LX/8r1;->a()Landroid/text/Spannable;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1408179
    invoke-static {v3}, LX/16z;->p(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v5

    .line 1408180
    iget-object v6, p0, LX/8r1;->f:LX/8qr;

    .line 1408181
    iget-object v7, v6, LX/8qr;->d:Ljava/lang/String;

    move-object v6, v7

    .line 1408182
    iget-object v7, p0, LX/8r1;->f:LX/8qr;

    .line 1408183
    iget-object v8, v7, LX/8qr;->c:Landroid/graphics/drawable/Drawable;

    move-object v7, v8

    .line 1408184
    if-nez v7, :cond_11

    .line 1408185
    invoke-static {v3}, LX/16z;->p(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v7

    invoke-static {v3}, LX/16z;->o(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v3

    if-ne v7, v3, :cond_10

    const v3, 0x7f0f0060

    .line 1408186
    :goto_9
    iget-object v7, p0, LX/8r1;->f:LX/8qr;

    .line 1408187
    iget-object v8, v7, LX/8qr;->a:Landroid/content/Context;

    move-object v7, v8

    .line 1408188
    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    new-array v8, v10, [Ljava/lang/Object;

    aput-object v6, v8, v9

    invoke-virtual {v7, v3, v5, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1408189
    :goto_a
    new-instance v5, Landroid/text/SpannableString;

    invoke-virtual {p0, v3}, LX/8r1;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1408190
    iget-object v6, p0, LX/8r1;->f:LX/8qr;

    .line 1408191
    iget-object v7, v6, LX/8qr;->c:Landroid/graphics/drawable/Drawable;

    move-object v6, v7

    .line 1408192
    if-eqz v6, :cond_f

    .line 1408193
    iget-object v6, p0, LX/8r1;->f:LX/8qr;

    .line 1408194
    iget-object v7, v6, LX/8qr;->c:Landroid/graphics/drawable/Drawable;

    move-object v6, v7

    .line 1408195
    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v7

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v8

    invoke-virtual {v6, v9, v9, v7, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1408196
    new-instance v7, Landroid/text/style/ImageSpan;

    invoke-direct {v7, v6}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;)V

    invoke-interface {v5, v7, v9, v10, v11}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 1408197
    :cond_f
    new-instance v6, LX/8qy;

    invoke-direct {v6, p0, v3}, LX/8qy;-><init>(LX/8r1;Ljava/lang/String;)V

    invoke-interface {v5}, Landroid/text/Spannable;->length()I

    move-result v3

    invoke-interface {v5, v6, v9, v3, v11}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 1408198
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v3, v4

    .line 1408199
    goto/16 :goto_4

    .line 1408200
    :cond_10
    const v3, 0x7f0f0061

    goto :goto_9

    .line 1408201
    :cond_11
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "  "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_a

    .line 1408202
    :cond_12
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1408203
    invoke-static {p0}, LX/8r1;->d(LX/8r1;)Landroid/text/Spannable;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1408204
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->s_()Z

    move-result v3

    if-eqz v3, :cond_13

    iget-object v3, p0, LX/8r1;->f:LX/8qr;

    .line 1408205
    iget-object v5, v3, LX/8qr;->a:Landroid/content/Context;

    move-object v3, v5

    .line 1408206
    const v5, 0x7f080fc9

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1408207
    :goto_b
    new-instance v5, Landroid/text/SpannableString;

    invoke-virtual {p0, v3}, LX/8r1;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v5, v3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1408208
    new-instance v3, LX/8qu;

    invoke-direct {v3, p0}, LX/8qu;-><init>(LX/8r1;)V

    const/4 v6, 0x0

    invoke-interface {v5}, Landroid/text/Spannable;->length()I

    move-result v7

    const/16 v8, 0x21

    invoke-interface {v5, v3, v6, v7, v8}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 1408209
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v3, v4

    .line 1408210
    goto/16 :goto_6

    .line 1408211
    :cond_13
    iget-object v3, p0, LX/8r1;->f:LX/8qr;

    .line 1408212
    iget-object v5, v3, LX/8qr;->a:Landroid/content/Context;

    move-object v3, v5

    .line 1408213
    const v5, 0x7f080fc8

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_b
.end method
