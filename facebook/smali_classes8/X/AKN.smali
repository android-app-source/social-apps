.class public LX/AKN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/AKN;


# instance fields
.field public a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/AJx;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1663110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1663111
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/AKN;->a:Ljava/util/Set;

    .line 1663112
    return-void
.end method

.method public static a(LX/0QB;)LX/AKN;
    .locals 3

    .prologue
    .line 1663113
    sget-object v0, LX/AKN;->c:LX/AKN;

    if-nez v0, :cond_1

    .line 1663114
    const-class v1, LX/AKN;

    monitor-enter v1

    .line 1663115
    :try_start_0
    sget-object v0, LX/AKN;->c:LX/AKN;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1663116
    if-eqz v2, :cond_0

    .line 1663117
    :try_start_1
    new-instance v0, LX/AKN;

    invoke-direct {v0}, LX/AKN;-><init>()V

    .line 1663118
    move-object v0, v0

    .line 1663119
    sput-object v0, LX/AKN;->c:LX/AKN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1663120
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1663121
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1663122
    :cond_1
    sget-object v0, LX/AKN;->c:LX/AKN;

    return-object v0

    .line 1663123
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1663124
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
