.class public final LX/9UG;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;)V
    .locals 0

    .prologue
    .line 1497751
    iput-object p1, p0, LX/9UG;->a:Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1497752
    const-class v0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorActivity;

    const-string v1, "failed to get list of suggested friends"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1497753
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1497754
    check-cast p1, LX/0Px;

    .line 1497755
    iget-object v0, p0, LX/9UG;->a:Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;

    iget-object v0, v0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->c:LX/0ad;

    sget v1, LX/1EB;->M:I

    const/4 v2, 0x5

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v1

    .line 1497756
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1497757
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 1497758
    invoke-virtual {p1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1497759
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1497760
    :cond_0
    iget-object v0, p0, LX/9UG;->a:Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;

    iget-object v0, v0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->n:LX/9U9;

    check-cast v0, LX/9UD;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1497761
    iput-object v1, v0, LX/9UD;->i:LX/0Px;

    .line 1497762
    iget-object v2, v0, LX/9UC;->i:LX/0Px;

    iput-object v2, v0, LX/9UD;->j:LX/0Px;

    .line 1497763
    iget-object v2, v0, LX/9UA;->d:Landroid/database/Cursor;

    invoke-virtual {v0, v2}, LX/9UB;->a(Landroid/database/Cursor;)V

    .line 1497764
    iget-object v0, p0, LX/9UG;->a:Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;

    iget-object v0, v0, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->n:LX/9U9;

    check-cast v0, LX/9UD;

    iget-object v0, v0, LX/9UC;->h:Landroid/widget/Filter;

    iget-object v1, p0, LX/9UG;->a:Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;

    iget-object v1, v1, Lcom/facebook/katana/activity/profilelist/FriendSingleSelectorFragment;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    .line 1497765
    return-void
.end method
