.class public LX/8tz;
.super LX/8tv;
.source ""


# instance fields
.field private final a:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/facebook/widget/text/html/FbHtml$TagHandler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1414329
    invoke-direct {p0}, LX/8tv;-><init>()V

    .line 1414330
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, LX/8tz;->a:Ljava/util/Vector;

    return-void
.end method


# virtual methods
.method public final a(LX/8tv;)LX/8tz;
    .locals 1

    .prologue
    .line 1414331
    iget-object v0, p0, LX/8tz;->a:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 1414332
    return-object p0
.end method

.method public final a(Ljava/lang/String;Landroid/text/Editable;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1414333
    iget-object v0, p0, LX/8tz;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_1

    iget-object v0, p0, LX/8tz;->a:Ljava/util/Vector;

    invoke-virtual {v0, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8tv;

    .line 1414334
    invoke-virtual {v0, p1, p2}, LX/8tv;->a(Ljava/lang/String;Landroid/text/Editable;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1414335
    const/4 v0, 0x1

    .line 1414336
    :goto_1
    return v0

    .line 1414337
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1414338
    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Lorg/xml/sax/Attributes;Landroid/text/Editable;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1414339
    iget-object v0, p0, LX/8tz;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_1

    iget-object v0, p0, LX/8tz;->a:Ljava/util/Vector;

    invoke-virtual {v0, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8tv;

    .line 1414340
    invoke-virtual {v0, p1, p2, p3}, LX/8tv;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;Landroid/text/Editable;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1414341
    const/4 v0, 0x1

    .line 1414342
    :goto_1
    return v0

    .line 1414343
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1414344
    goto :goto_1
.end method

.method public final a([CIILandroid/text/Editable;)Z
    .locals 3

    .prologue
    .line 1414345
    iget-object v0, p0, LX/8tz;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, LX/8tz;->a:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8tv;

    .line 1414346
    invoke-virtual {v0, p1, p2, p3, p4}, LX/8tv;->a([CIILandroid/text/Editable;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1414347
    const/4 v0, 0x1

    .line 1414348
    :goto_1
    return v0

    .line 1414349
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1414350
    :cond_1
    invoke-super {p0, p1, p2, p3, p4}, LX/8tv;->a([CIILandroid/text/Editable;)Z

    move-result v0

    goto :goto_1
.end method
