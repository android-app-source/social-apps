.class public final enum LX/9XA;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/9X2;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9XA;",
        ">;",
        "LX/9X2;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9XA;

.field public static final enum EVENT_CHECKIN_ERROR:LX/9XA;

.field public static final enum EVENT_CITY_HUB_PYML_MODULE_LIKE_ERROR:LX/9XA;

.field public static final enum EVENT_CITY_HUB_PYML_MODULE_RENDER_ERROR:LX/9XA;

.field public static final enum EVENT_CITY_HUB_SOCIAL_MODULE_FETCH_PAGES_ERROR:LX/9XA;

.field public static final enum EVENT_CITY_HUB_SOCIAL_MODULE_LIKE_ERROR:LX/9XA;

.field public static final enum EVENT_CITY_HUB_SOCIAL_MODULE_RENDER_ERROR:LX/9XA;

.field public static final enum EVENT_FB_EVENT_STATUS_FAILURE:LX/9XA;

.field public static final enum EVENT_LIKE_ERROR:LX/9XA;

.field public static final enum EVENT_PAGE_ADD_TO_FAVORITES_ERROR:LX/9XA;

.field public static final enum EVENT_PAGE_DETAILS_LOAD_ERROR:LX/9XA;

.field public static final enum EVENT_PAGE_EDIT_REVIEW_PRIVACY_ERROR:LX/9XA;

.field public static final enum EVENT_PAGE_FOLLOW_ERROR:LX/9XA;

.field public static final enum EVENT_PAGE_FOLLOW_REGULAR_ERROR:LX/9XA;

.field public static final enum EVENT_PAGE_FOLLOW_SEE_FIRST_ERROR:LX/9XA;

.field public static final enum EVENT_PAGE_INFO_LOAD_ERROR:LX/9XA;

.field public static final enum EVENT_PAGE_RATING_ERROR:LX/9XA;

.field public static final enum EVENT_PAGE_RECOMMENDATION_ERROR:LX/9XA;

.field public static final enum EVENT_PAGE_REMOVE_FROM_FAVORITES_ERROR:LX/9XA;

.field public static final enum EVENT_PAGE_SAVE_ERROR:LX/9XA;

.field public static final enum EVENT_PAGE_UNFOLLOW_ERROR:LX/9XA;

.field public static final enum EVENT_PAGE_UNSAVE_ERROR:LX/9XA;

.field public static final enum EVENT_PLACE_DELETE_REVIEW_FAILED:LX/9XA;

.field public static final enum EVENT_PLACE_EDIT_REVIEW_ERROR:LX/9XA;

.field public static final enum EVENT_PLACE_REPORT_ERROR:LX/9XA;

.field public static final enum EVENT_PLACE_SAVE_ERROR:LX/9XA;

.field public static final enum EVENT_PLACE_UNSAVE_ERROR:LX/9XA;

.field public static final enum EVENT_RECOMMENDATION_LIKE_FAIL:LX/9XA;

.field public static final enum EVENT_RECOMMENDATION_UNLIKE_FAIL:LX/9XA;

.field public static final enum EVENT_SECTION_LOAD_ERROR:LX/9XA;

.field public static final enum EVENT_SHARE_PAGE_ERROR:LX/9XA;

.field public static final enum EVENT_UNLIKE_ERROR:LX/9XA;


# instance fields
.field private mEventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1502388
    new-instance v0, LX/9XA;

    const-string v1, "EVENT_PAGE_DETAILS_LOAD_ERROR"

    const-string v2, "page_load_error"

    invoke-direct {v0, v1, v4, v2}, LX/9XA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XA;->EVENT_PAGE_DETAILS_LOAD_ERROR:LX/9XA;

    .line 1502389
    new-instance v0, LX/9XA;

    const-string v1, "EVENT_PAGE_INFO_LOAD_ERROR"

    const-string v2, "info_page_load_error"

    invoke-direct {v0, v1, v5, v2}, LX/9XA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XA;->EVENT_PAGE_INFO_LOAD_ERROR:LX/9XA;

    .line 1502390
    new-instance v0, LX/9XA;

    const-string v1, "EVENT_PAGE_RATING_ERROR"

    const-string v2, "page_rating_error"

    invoke-direct {v0, v1, v6, v2}, LX/9XA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XA;->EVENT_PAGE_RATING_ERROR:LX/9XA;

    .line 1502391
    new-instance v0, LX/9XA;

    const-string v1, "EVENT_PLACE_REPORT_ERROR"

    const-string v2, "place_report_error"

    invoke-direct {v0, v1, v7, v2}, LX/9XA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XA;->EVENT_PLACE_REPORT_ERROR:LX/9XA;

    .line 1502392
    new-instance v0, LX/9XA;

    const-string v1, "EVENT_PAGE_RECOMMENDATION_ERROR"

    const-string v2, "page_recommendation_error"

    invoke-direct {v0, v1, v8, v2}, LX/9XA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XA;->EVENT_PAGE_RECOMMENDATION_ERROR:LX/9XA;

    .line 1502393
    new-instance v0, LX/9XA;

    const-string v1, "EVENT_PAGE_EDIT_REVIEW_PRIVACY_ERROR"

    const/4 v2, 0x5

    const-string v3, "page_edit_review_privacy_error"

    invoke-direct {v0, v1, v2, v3}, LX/9XA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XA;->EVENT_PAGE_EDIT_REVIEW_PRIVACY_ERROR:LX/9XA;

    .line 1502394
    new-instance v0, LX/9XA;

    const-string v1, "EVENT_PLACE_EDIT_REVIEW_ERROR"

    const/4 v2, 0x6

    const-string v3, "place_edit_review_error"

    invoke-direct {v0, v1, v2, v3}, LX/9XA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XA;->EVENT_PLACE_EDIT_REVIEW_ERROR:LX/9XA;

    .line 1502395
    new-instance v0, LX/9XA;

    const-string v1, "EVENT_PLACE_DELETE_REVIEW_FAILED"

    const/4 v2, 0x7

    const-string v3, "place_delete_review_failed"

    invoke-direct {v0, v1, v2, v3}, LX/9XA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XA;->EVENT_PLACE_DELETE_REVIEW_FAILED:LX/9XA;

    .line 1502396
    new-instance v0, LX/9XA;

    const-string v1, "EVENT_SHARE_PAGE_ERROR"

    const/16 v2, 0x8

    const-string v3, "page_share_error"

    invoke-direct {v0, v1, v2, v3}, LX/9XA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XA;->EVENT_SHARE_PAGE_ERROR:LX/9XA;

    .line 1502397
    new-instance v0, LX/9XA;

    const-string v1, "EVENT_CHECKIN_ERROR"

    const/16 v2, 0x9

    const-string v3, "page_checkin_error"

    invoke-direct {v0, v1, v2, v3}, LX/9XA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XA;->EVENT_CHECKIN_ERROR:LX/9XA;

    .line 1502398
    new-instance v0, LX/9XA;

    const-string v1, "EVENT_LIKE_ERROR"

    const/16 v2, 0xa

    const-string v3, "page_like_error"

    invoke-direct {v0, v1, v2, v3}, LX/9XA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XA;->EVENT_LIKE_ERROR:LX/9XA;

    .line 1502399
    new-instance v0, LX/9XA;

    const-string v1, "EVENT_UNLIKE_ERROR"

    const/16 v2, 0xb

    const-string v3, "page_unlike_error"

    invoke-direct {v0, v1, v2, v3}, LX/9XA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XA;->EVENT_UNLIKE_ERROR:LX/9XA;

    .line 1502400
    new-instance v0, LX/9XA;

    const-string v1, "EVENT_FB_EVENT_STATUS_FAILURE"

    const/16 v2, 0xc

    const-string v3, "page_event_status_update_failure"

    invoke-direct {v0, v1, v2, v3}, LX/9XA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XA;->EVENT_FB_EVENT_STATUS_FAILURE:LX/9XA;

    .line 1502401
    new-instance v0, LX/9XA;

    const-string v1, "EVENT_RECOMMENDATION_UNLIKE_FAIL"

    const/16 v2, 0xd

    const-string v3, "recommendation_unlike_fail"

    invoke-direct {v0, v1, v2, v3}, LX/9XA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XA;->EVENT_RECOMMENDATION_UNLIKE_FAIL:LX/9XA;

    .line 1502402
    new-instance v0, LX/9XA;

    const-string v1, "EVENT_RECOMMENDATION_LIKE_FAIL"

    const/16 v2, 0xe

    const-string v3, "recommendation_like_fail"

    invoke-direct {v0, v1, v2, v3}, LX/9XA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XA;->EVENT_RECOMMENDATION_LIKE_FAIL:LX/9XA;

    .line 1502403
    new-instance v0, LX/9XA;

    const-string v1, "EVENT_SECTION_LOAD_ERROR"

    const/16 v2, 0xf

    const-string v3, "section_load_error"

    invoke-direct {v0, v1, v2, v3}, LX/9XA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XA;->EVENT_SECTION_LOAD_ERROR:LX/9XA;

    .line 1502404
    new-instance v0, LX/9XA;

    const-string v1, "EVENT_PLACE_SAVE_ERROR"

    const/16 v2, 0x10

    const-string v3, "place_save_error"

    invoke-direct {v0, v1, v2, v3}, LX/9XA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XA;->EVENT_PLACE_SAVE_ERROR:LX/9XA;

    .line 1502405
    new-instance v0, LX/9XA;

    const-string v1, "EVENT_PLACE_UNSAVE_ERROR"

    const/16 v2, 0x11

    const-string v3, "place_unsave_error"

    invoke-direct {v0, v1, v2, v3}, LX/9XA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XA;->EVENT_PLACE_UNSAVE_ERROR:LX/9XA;

    .line 1502406
    new-instance v0, LX/9XA;

    const-string v1, "EVENT_PAGE_SAVE_ERROR"

    const/16 v2, 0x12

    const-string v3, "page_save_error"

    invoke-direct {v0, v1, v2, v3}, LX/9XA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XA;->EVENT_PAGE_SAVE_ERROR:LX/9XA;

    .line 1502407
    new-instance v0, LX/9XA;

    const-string v1, "EVENT_PAGE_UNSAVE_ERROR"

    const/16 v2, 0x13

    const-string v3, "page_unsave_error"

    invoke-direct {v0, v1, v2, v3}, LX/9XA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XA;->EVENT_PAGE_UNSAVE_ERROR:LX/9XA;

    .line 1502408
    new-instance v0, LX/9XA;

    const-string v1, "EVENT_PAGE_FOLLOW_ERROR"

    const/16 v2, 0x14

    const-string v3, "page_follow_error"

    invoke-direct {v0, v1, v2, v3}, LX/9XA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XA;->EVENT_PAGE_FOLLOW_ERROR:LX/9XA;

    .line 1502409
    new-instance v0, LX/9XA;

    const-string v1, "EVENT_PAGE_UNFOLLOW_ERROR"

    const/16 v2, 0x15

    const-string v3, "page_unfollow_error"

    invoke-direct {v0, v1, v2, v3}, LX/9XA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XA;->EVENT_PAGE_UNFOLLOW_ERROR:LX/9XA;

    .line 1502410
    new-instance v0, LX/9XA;

    const-string v1, "EVENT_PAGE_FOLLOW_SEE_FIRST_ERROR"

    const/16 v2, 0x16

    const-string v3, "page_follow_see_first_error"

    invoke-direct {v0, v1, v2, v3}, LX/9XA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XA;->EVENT_PAGE_FOLLOW_SEE_FIRST_ERROR:LX/9XA;

    .line 1502411
    new-instance v0, LX/9XA;

    const-string v1, "EVENT_PAGE_FOLLOW_REGULAR_ERROR"

    const/16 v2, 0x17

    const-string v3, "page_follow_regular_error"

    invoke-direct {v0, v1, v2, v3}, LX/9XA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XA;->EVENT_PAGE_FOLLOW_REGULAR_ERROR:LX/9XA;

    .line 1502412
    new-instance v0, LX/9XA;

    const-string v1, "EVENT_PAGE_ADD_TO_FAVORITES_ERROR"

    const/16 v2, 0x18

    const-string v3, "page_add_to_favorites_error"

    invoke-direct {v0, v1, v2, v3}, LX/9XA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XA;->EVENT_PAGE_ADD_TO_FAVORITES_ERROR:LX/9XA;

    .line 1502413
    new-instance v0, LX/9XA;

    const-string v1, "EVENT_PAGE_REMOVE_FROM_FAVORITES_ERROR"

    const/16 v2, 0x19

    const-string v3, "page_remove_from_favorites_error"

    invoke-direct {v0, v1, v2, v3}, LX/9XA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XA;->EVENT_PAGE_REMOVE_FROM_FAVORITES_ERROR:LX/9XA;

    .line 1502414
    new-instance v0, LX/9XA;

    const-string v1, "EVENT_CITY_HUB_SOCIAL_MODULE_LIKE_ERROR"

    const/16 v2, 0x1a

    const-string v3, "city_hub_social_module_like_error"

    invoke-direct {v0, v1, v2, v3}, LX/9XA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XA;->EVENT_CITY_HUB_SOCIAL_MODULE_LIKE_ERROR:LX/9XA;

    .line 1502415
    new-instance v0, LX/9XA;

    const-string v1, "EVENT_CITY_HUB_SOCIAL_MODULE_FETCH_PAGES_ERROR"

    const/16 v2, 0x1b

    const-string v3, "city_hub_social_module_fetch_pages_error"

    invoke-direct {v0, v1, v2, v3}, LX/9XA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XA;->EVENT_CITY_HUB_SOCIAL_MODULE_FETCH_PAGES_ERROR:LX/9XA;

    .line 1502416
    new-instance v0, LX/9XA;

    const-string v1, "EVENT_CITY_HUB_SOCIAL_MODULE_RENDER_ERROR"

    const/16 v2, 0x1c

    const-string v3, "city_hub_social_module_render_error"

    invoke-direct {v0, v1, v2, v3}, LX/9XA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XA;->EVENT_CITY_HUB_SOCIAL_MODULE_RENDER_ERROR:LX/9XA;

    .line 1502417
    new-instance v0, LX/9XA;

    const-string v1, "EVENT_CITY_HUB_PYML_MODULE_LIKE_ERROR"

    const/16 v2, 0x1d

    const-string v3, "city_hub_pyml_module_like_error"

    invoke-direct {v0, v1, v2, v3}, LX/9XA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XA;->EVENT_CITY_HUB_PYML_MODULE_LIKE_ERROR:LX/9XA;

    .line 1502418
    new-instance v0, LX/9XA;

    const-string v1, "EVENT_CITY_HUB_PYML_MODULE_RENDER_ERROR"

    const/16 v2, 0x1e

    const-string v3, "city_hub_pyml_module_render_error"

    invoke-direct {v0, v1, v2, v3}, LX/9XA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9XA;->EVENT_CITY_HUB_PYML_MODULE_RENDER_ERROR:LX/9XA;

    .line 1502419
    const/16 v0, 0x1f

    new-array v0, v0, [LX/9XA;

    sget-object v1, LX/9XA;->EVENT_PAGE_DETAILS_LOAD_ERROR:LX/9XA;

    aput-object v1, v0, v4

    sget-object v1, LX/9XA;->EVENT_PAGE_INFO_LOAD_ERROR:LX/9XA;

    aput-object v1, v0, v5

    sget-object v1, LX/9XA;->EVENT_PAGE_RATING_ERROR:LX/9XA;

    aput-object v1, v0, v6

    sget-object v1, LX/9XA;->EVENT_PLACE_REPORT_ERROR:LX/9XA;

    aput-object v1, v0, v7

    sget-object v1, LX/9XA;->EVENT_PAGE_RECOMMENDATION_ERROR:LX/9XA;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/9XA;->EVENT_PAGE_EDIT_REVIEW_PRIVACY_ERROR:LX/9XA;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/9XA;->EVENT_PLACE_EDIT_REVIEW_ERROR:LX/9XA;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/9XA;->EVENT_PLACE_DELETE_REVIEW_FAILED:LX/9XA;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/9XA;->EVENT_SHARE_PAGE_ERROR:LX/9XA;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/9XA;->EVENT_CHECKIN_ERROR:LX/9XA;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/9XA;->EVENT_LIKE_ERROR:LX/9XA;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/9XA;->EVENT_UNLIKE_ERROR:LX/9XA;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/9XA;->EVENT_FB_EVENT_STATUS_FAILURE:LX/9XA;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/9XA;->EVENT_RECOMMENDATION_UNLIKE_FAIL:LX/9XA;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/9XA;->EVENT_RECOMMENDATION_LIKE_FAIL:LX/9XA;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/9XA;->EVENT_SECTION_LOAD_ERROR:LX/9XA;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/9XA;->EVENT_PLACE_SAVE_ERROR:LX/9XA;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/9XA;->EVENT_PLACE_UNSAVE_ERROR:LX/9XA;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/9XA;->EVENT_PAGE_SAVE_ERROR:LX/9XA;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/9XA;->EVENT_PAGE_UNSAVE_ERROR:LX/9XA;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/9XA;->EVENT_PAGE_FOLLOW_ERROR:LX/9XA;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/9XA;->EVENT_PAGE_UNFOLLOW_ERROR:LX/9XA;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/9XA;->EVENT_PAGE_FOLLOW_SEE_FIRST_ERROR:LX/9XA;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/9XA;->EVENT_PAGE_FOLLOW_REGULAR_ERROR:LX/9XA;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/9XA;->EVENT_PAGE_ADD_TO_FAVORITES_ERROR:LX/9XA;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/9XA;->EVENT_PAGE_REMOVE_FROM_FAVORITES_ERROR:LX/9XA;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/9XA;->EVENT_CITY_HUB_SOCIAL_MODULE_LIKE_ERROR:LX/9XA;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/9XA;->EVENT_CITY_HUB_SOCIAL_MODULE_FETCH_PAGES_ERROR:LX/9XA;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/9XA;->EVENT_CITY_HUB_SOCIAL_MODULE_RENDER_ERROR:LX/9XA;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/9XA;->EVENT_CITY_HUB_PYML_MODULE_LIKE_ERROR:LX/9XA;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, LX/9XA;->EVENT_CITY_HUB_PYML_MODULE_RENDER_ERROR:LX/9XA;

    aput-object v2, v0, v1

    sput-object v0, LX/9XA;->$VALUES:[LX/9XA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1502385
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1502386
    iput-object p3, p0, LX/9XA;->mEventName:Ljava/lang/String;

    .line 1502387
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9XA;
    .locals 1

    .prologue
    .line 1502420
    const-class v0, LX/9XA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9XA;

    return-object v0
.end method

.method public static values()[LX/9XA;
    .locals 1

    .prologue
    .line 1502384
    sget-object v0, LX/9XA;->$VALUES:[LX/9XA;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9XA;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1502382
    iget-object v0, p0, LX/9XA;->mEventName:Ljava/lang/String;

    return-object v0
.end method

.method public final getType()LX/9XC;
    .locals 1

    .prologue
    .line 1502383
    sget-object v0, LX/9XC;->NETWORK_FAILURE:LX/9XC;

    return-object v0
.end method
