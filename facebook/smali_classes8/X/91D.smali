.class public LX/91D;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/91D;


# instance fields
.field public final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1430226
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1430227
    iput-object p1, p0, LX/91D;->a:LX/0Zb;

    .line 1430228
    return-void
.end method

.method public static a(LX/0QB;)LX/91D;
    .locals 4

    .prologue
    .line 1430229
    sget-object v0, LX/91D;->b:LX/91D;

    if-nez v0, :cond_1

    .line 1430230
    const-class v1, LX/91D;

    monitor-enter v1

    .line 1430231
    :try_start_0
    sget-object v0, LX/91D;->b:LX/91D;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1430232
    if-eqz v2, :cond_0

    .line 1430233
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1430234
    new-instance p0, LX/91D;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/91D;-><init>(LX/0Zb;)V

    .line 1430235
    move-object v0, p0

    .line 1430236
    sput-object v0, LX/91D;->b:LX/91D;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1430237
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1430238
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1430239
    :cond_1
    sget-object v0, LX/91D;->b:LX/91D;

    return-object v0

    .line 1430240
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1430241
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)LX/8yQ;
    .locals 2

    .prologue
    .line 1430242
    new-instance v0, LX/8yQ;

    const-string v1, "feeling_selector"

    invoke-direct {v0, p0, p1, v1}, LX/8yQ;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1430243
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1430244
    const-string v0, "feeling_selector_load_failed"

    invoke-static {v0, p1}, LX/91D;->b(Ljava/lang/String;Ljava/lang/String;)LX/8yQ;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/8yQ;->a(Ljava/lang/String;)LX/8yQ;

    move-result-object v0

    .line 1430245
    iget-object v1, v0, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v0, v1

    .line 1430246
    const-string v1, "error"

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1430247
    iget-object v1, p0, LX/91D;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1430248
    return-void
.end method
