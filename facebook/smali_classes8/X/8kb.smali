.class public LX/8kb;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public final a:Lcom/facebook/stickers/keyboard/StickerPackInfoView;

.field private final b:Lcom/facebook/resources/ui/FbButton;

.field private final c:Lcom/facebook/resources/ui/FbButton;

.field public d:LX/8l8;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1396578
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1396579
    const v0, 0x7f030d93

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1396580
    const v0, 0x7f0d215a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/keyboard/StickerPackInfoView;

    iput-object v0, p0, LX/8kb;->a:Lcom/facebook/stickers/keyboard/StickerPackInfoView;

    .line 1396581
    const v0, 0x7f0d1e70

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/8kb;->b:Lcom/facebook/resources/ui/FbButton;

    .line 1396582
    iget-object v0, p0, LX/8kb;->b:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/8kZ;

    invoke-direct {v1, p0}, LX/8kZ;-><init>(LX/8kb;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1396583
    const v0, 0x7f0d09a9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/8kb;->c:Lcom/facebook/resources/ui/FbButton;

    .line 1396584
    iget-object v0, p0, LX/8kb;->c:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/8ka;

    invoke-direct {v1, p0}, LX/8ka;-><init>(LX/8kb;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1396585
    return-void
.end method
