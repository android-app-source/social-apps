.class public final LX/9vn;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel$PageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetRedirectionLinkGraphQLModel$RedirectionInfoModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1573933
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;
    .locals 14

    .prologue
    .line 1573934
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1573935
    iget-object v1, p0, LX/9vn;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1573936
    iget-object v2, p0, LX/9vn;->b:LX/0Px;

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/util/List;)I

    move-result v2

    .line 1573937
    iget-object v3, p0, LX/9vn;->c:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1573938
    iget-object v4, p0, LX/9vn;->d:Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1573939
    iget-object v5, p0, LX/9vn;->e:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    invoke-virtual {v0, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 1573940
    iget-object v6, p0, LX/9vn;->f:Ljava/lang/String;

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1573941
    iget-object v7, p0, LX/9vn;->j:Ljava/lang/String;

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1573942
    iget-object v8, p0, LX/9vn;->k:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel$PageModel;

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1573943
    iget-object v9, p0, LX/9vn;->l:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyActorsLinksGraphQLModel$RangesModel$EntityModel$ProfilePictureModel;

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1573944
    iget-object v10, p0, LX/9vn;->m:LX/0Px;

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v10

    .line 1573945
    iget-object v11, p0, LX/9vn;->n:Ljava/lang/String;

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 1573946
    iget-object v12, p0, LX/9vn;->o:Ljava/lang/String;

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 1573947
    const/16 v13, 0xf

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 1573948
    const/4 v13, 0x0

    invoke-virtual {v0, v13, v1}, LX/186;->b(II)V

    .line 1573949
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1573950
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1573951
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1573952
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1573953
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1573954
    const/4 v1, 0x6

    iget-boolean v2, p0, LX/9vn;->g:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 1573955
    const/4 v1, 0x7

    iget-boolean v2, p0, LX/9vn;->h:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 1573956
    const/16 v1, 0x8

    iget-boolean v2, p0, LX/9vn;->i:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 1573957
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1573958
    const/16 v1, 0xa

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1573959
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 1573960
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 1573961
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v11}, LX/186;->b(II)V

    .line 1573962
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v12}, LX/186;->b(II)V

    .line 1573963
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1573964
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1573965
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1573966
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1573967
    new-instance v0, LX/15i;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1573968
    new-instance v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;

    invoke-direct {v1, v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel$RangesModel$EntityModel;-><init>(LX/15i;)V

    .line 1573969
    return-object v1
.end method
