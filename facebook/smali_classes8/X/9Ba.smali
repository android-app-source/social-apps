.class public final LX/9Ba;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:LX/9Be;


# direct methods
.method public constructor <init>(LX/9Be;)V
    .locals 0

    .prologue
    .line 1451845
    iput-object p1, p0, LX/9Ba;->a:LX/9Be;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 8

    .prologue
    .line 1451846
    iget-object v0, p0, LX/9Ba;->a:LX/9Be;

    iget v0, v0, LX/9BY;->v:I

    iget-object v1, p0, LX/9Ba;->a:LX/9Be;

    iget v1, v1, LX/9BY;->w:I

    iget-object v2, p0, LX/9Ba;->a:LX/9Be;

    iget v2, v2, LX/9BY;->v:I

    sub-int/2addr v1, v2

    int-to-double v2, v1

    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-int v1, v2

    add-int/2addr v0, v1

    .line 1451847
    iget-object v1, p0, LX/9Ba;->a:LX/9Be;

    iget v1, v1, LX/9BY;->y:I

    iget-object v2, p0, LX/9Ba;->a:LX/9Be;

    iget v2, v2, LX/9BY;->x:I

    iget-object v3, p0, LX/9Ba;->a:LX/9Be;

    iget v3, v3, LX/9BY;->y:I

    sub-int/2addr v2, v3

    int-to-double v2, v2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v6

    sub-double/2addr v4, v6

    mul-double/2addr v2, v4

    double-to-int v2, v2

    add-int/2addr v2, v1

    .line 1451848
    iget-object v1, p0, LX/9Ba;->a:LX/9Be;

    invoke-virtual {v1}, LX/9Be;->getWidth()I

    move-result v1

    sub-int v0, v1, v0

    div-int/lit8 v3, v0, 0x2

    .line 1451849
    iget-object v0, p0, LX/9Ba;->a:LX/9Be;

    .line 1451850
    iget-object v1, v0, LX/9BY;->E:Landroid/graphics/drawable/Drawable;

    move-object v4, v1

    .line 1451851
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 1451852
    iget v1, v0, Landroid/graphics/Rect;->top:I

    .line 1451853
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 1451854
    iget-object v5, p0, LX/9Ba;->a:LX/9Be;

    invoke-virtual {v5}, LX/9BY;->a()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1451855
    sub-int v1, v0, v2

    .line 1451856
    :goto_0
    iget-object v2, p0, LX/9Ba;->a:LX/9Be;

    invoke-virtual {v2}, LX/9Be;->getWidth()I

    move-result v2

    sub-int/2addr v2, v3

    invoke-virtual {v4, v3, v1, v2, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1451857
    iget-object v0, p0, LX/9Ba;->a:LX/9Be;

    invoke-virtual {v0}, LX/9Be;->invalidate()V

    .line 1451858
    return-void

    .line 1451859
    :cond_0
    add-int v0, v1, v2

    goto :goto_0
.end method
