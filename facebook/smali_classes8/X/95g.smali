.class public final LX/95g;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/95f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<EdgeType:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/95f",
        "<TEdgeType;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<TEdgeType;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<TEdgeType;>;)V"
        }
    .end annotation

    .prologue
    .line 1437169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1437170
    iput-object p1, p0, LX/95g;->a:Ljava/util/ArrayList;

    .line 1437171
    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/Object;)Z
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITEdgeType;)Z"
        }
    .end annotation

    .prologue
    .line 1437172
    iget-object v0, p0, LX/95g;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    .line 1437173
    iget-object v0, p0, LX/95g;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1437174
    const/4 v0, 0x1

    .line 1437175
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
