.class public final LX/9Tz;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v1, 0x0

    .line 1497250
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_9

    .line 1497251
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1497252
    :goto_0
    return v1

    .line 1497253
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1497254
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_8

    .line 1497255
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1497256
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1497257
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 1497258
    const-string v9, "cover_photo"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 1497259
    const/4 v8, 0x0

    .line 1497260
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v9, :cond_d

    .line 1497261
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1497262
    :goto_2
    move v7, v8

    .line 1497263
    goto :goto_1

    .line 1497264
    :cond_2
    const-string v9, "full_name"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1497265
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 1497266
    :cond_3
    const-string v9, "group_icon"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 1497267
    const/4 v8, 0x0

    .line 1497268
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v9, :cond_19

    .line 1497269
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1497270
    :goto_3
    move v5, v8

    .line 1497271
    goto :goto_1

    .line 1497272
    :cond_4
    const-string v9, "id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1497273
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 1497274
    :cond_5
    const-string v9, "parent_group"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 1497275
    invoke-static {p0, p1}, LX/9Ty;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1497276
    :cond_6
    const-string v9, "viewer_post_status"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 1497277
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    goto/16 :goto_1

    .line 1497278
    :cond_7
    const-string v9, "visibility"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1497279
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    goto/16 :goto_1

    .line 1497280
    :cond_8
    const/4 v8, 0x7

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1497281
    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 1497282
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 1497283
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1497284
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1497285
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1497286
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1497287
    const/4 v1, 0x6

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1497288
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_9
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    goto/16 :goto_1

    .line 1497289
    :cond_a
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1497290
    :cond_b
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_c

    .line 1497291
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1497292
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1497293
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_b

    if-eqz v9, :cond_b

    .line 1497294
    const-string v10, "photo"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_a

    .line 1497295
    const/4 v9, 0x0

    .line 1497296
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v10, :cond_11

    .line 1497297
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1497298
    :goto_5
    move v7, v9

    .line 1497299
    goto :goto_4

    .line 1497300
    :cond_c
    const/4 v9, 0x1

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1497301
    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 1497302
    invoke-virtual {p1}, LX/186;->d()I

    move-result v8

    goto/16 :goto_2

    :cond_d
    move v7, v8

    goto :goto_4

    .line 1497303
    :cond_e
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1497304
    :cond_f
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_10

    .line 1497305
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1497306
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1497307
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_f

    if-eqz v10, :cond_f

    .line 1497308
    const-string v11, "image"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_e

    .line 1497309
    const/4 v10, 0x0

    .line 1497310
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v11, :cond_15

    .line 1497311
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1497312
    :goto_7
    move v7, v10

    .line 1497313
    goto :goto_6

    .line 1497314
    :cond_10
    const/4 v10, 0x1

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1497315
    invoke-virtual {p1, v9, v7}, LX/186;->b(II)V

    .line 1497316
    invoke-virtual {p1}, LX/186;->d()I

    move-result v9

    goto :goto_5

    :cond_11
    move v7, v9

    goto :goto_6

    .line 1497317
    :cond_12
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1497318
    :cond_13
    :goto_8
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_14

    .line 1497319
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1497320
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1497321
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_13

    if-eqz v11, :cond_13

    .line 1497322
    const-string v12, "uri"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_12

    .line 1497323
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_8

    .line 1497324
    :cond_14
    const/4 v11, 0x1

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1497325
    invoke-virtual {p1, v10, v7}, LX/186;->b(II)V

    .line 1497326
    invoke-virtual {p1}, LX/186;->d()I

    move-result v10

    goto :goto_7

    :cond_15
    move v7, v10

    goto :goto_8

    .line 1497327
    :cond_16
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1497328
    :cond_17
    :goto_9
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_18

    .line 1497329
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1497330
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1497331
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_17

    if-eqz v9, :cond_17

    .line 1497332
    const-string v10, "dark_icon"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_16

    .line 1497333
    const/4 v9, 0x0

    .line 1497334
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v10, :cond_1d

    .line 1497335
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1497336
    :goto_a
    move v5, v9

    .line 1497337
    goto :goto_9

    .line 1497338
    :cond_18
    const/4 v9, 0x1

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1497339
    invoke-virtual {p1, v8, v5}, LX/186;->b(II)V

    .line 1497340
    invoke-virtual {p1}, LX/186;->d()I

    move-result v8

    goto/16 :goto_3

    :cond_19
    move v5, v8

    goto :goto_9

    .line 1497341
    :cond_1a
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1497342
    :cond_1b
    :goto_b
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_1c

    .line 1497343
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1497344
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1497345
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1b

    if-eqz v10, :cond_1b

    .line 1497346
    const-string v11, "uri"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1a

    .line 1497347
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_b

    .line 1497348
    :cond_1c
    const/4 v10, 0x1

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1497349
    invoke-virtual {p1, v9, v5}, LX/186;->b(II)V

    .line 1497350
    invoke-virtual {p1}, LX/186;->d()I

    move-result v9

    goto :goto_a

    :cond_1d
    move v5, v9

    goto :goto_b
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const/4 v3, 0x6

    const/4 v2, 0x5

    .line 1497351
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1497352
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1497353
    if-eqz v0, :cond_3

    .line 1497354
    const-string v1, "cover_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1497355
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1497356
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1497357
    if-eqz v1, :cond_2

    .line 1497358
    const-string v4, "photo"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1497359
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1497360
    const/4 v4, 0x0

    invoke-virtual {p0, v1, v4}, LX/15i;->g(II)I

    move-result v4

    .line 1497361
    if-eqz v4, :cond_1

    .line 1497362
    const-string v0, "image"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1497363
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1497364
    const/4 v0, 0x0

    invoke-virtual {p0, v4, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1497365
    if-eqz v0, :cond_0

    .line 1497366
    const-string v1, "uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1497367
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1497368
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1497369
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1497370
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1497371
    :cond_3
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1497372
    if-eqz v0, :cond_4

    .line 1497373
    const-string v1, "full_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1497374
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1497375
    :cond_4
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1497376
    if-eqz v0, :cond_7

    .line 1497377
    const-string v1, "group_icon"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1497378
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1497379
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1497380
    if-eqz v1, :cond_6

    .line 1497381
    const-string v4, "dark_icon"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1497382
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1497383
    const/4 v4, 0x0

    invoke-virtual {p0, v1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1497384
    if-eqz v4, :cond_5

    .line 1497385
    const-string v0, "uri"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1497386
    invoke-virtual {p2, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1497387
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1497388
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1497389
    :cond_7
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1497390
    if-eqz v0, :cond_8

    .line 1497391
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1497392
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1497393
    :cond_8
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1497394
    if-eqz v0, :cond_9

    .line 1497395
    const-string v1, "parent_group"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1497396
    invoke-static {p0, v0, p2}, LX/9Ty;->a(LX/15i;ILX/0nX;)V

    .line 1497397
    :cond_9
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1497398
    if-eqz v0, :cond_a

    .line 1497399
    const-string v0, "viewer_post_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1497400
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1497401
    :cond_a
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1497402
    if-eqz v0, :cond_b

    .line 1497403
    const-string v0, "visibility"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1497404
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1497405
    :cond_b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1497406
    return-void
.end method
