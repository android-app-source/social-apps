.class public LX/ASE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3l5;


# instance fields
.field public final a:LX/3kp;

.field public b:Landroid/content/Context;

.field public c:Landroid/view/View;

.field private d:LX/0hs;


# direct methods
.method public constructor <init>(LX/3kp;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1673756
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1673757
    iput-object p1, p0, LX/ASE;->a:LX/3kp;

    .line 1673758
    return-void
.end method

.method private g()Z
    .locals 1

    .prologue
    .line 1673797
    iget-object v0, p0, LX/ASE;->b:Landroid/content/Context;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 1673796
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 1

    .prologue
    .line 1673790
    invoke-direct {p0}, LX/ASE;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1673791
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    .line 1673792
    :goto_0
    return-object v0

    .line 1673793
    :cond_0
    iget-object v0, p0, LX/ASE;->a:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1673794
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    goto :goto_0

    .line 1673795
    :cond_1
    iget-object v0, p0, LX/ASE;->c:Landroid/view/View;

    if-eqz v0, :cond_2

    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    goto :goto_0

    :cond_2
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final a(LX/5L2;)LX/ASZ;
    .locals 1

    .prologue
    .line 1673789
    sget-object v0, LX/ASZ;->NONE:LX/ASZ;

    return-object v0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 1673788
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 1673798
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 1673779
    invoke-direct {p0}, LX/ASE;->g()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1673780
    iget-object v0, p0, LX/ASE;->d:LX/0hs;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ASE;->d:LX/0hs;

    .line 1673781
    iget-boolean v1, v0, LX/0ht;->r:Z

    move v0, v1

    .line 1673782
    if-eqz v0, :cond_0

    .line 1673783
    iget-object v0, p0, LX/ASE;->d:LX/0hs;

    invoke-virtual {v0}, LX/0ht;->l()V

    .line 1673784
    const/4 v0, 0x0

    iput-object v0, p0, LX/ASE;->d:LX/0hs;

    .line 1673785
    :cond_0
    if-eqz p1, :cond_1

    .line 1673786
    iget-object v0, p0, LX/ASE;->a:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->a()V

    .line 1673787
    :cond_1
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1673778
    const-string v0, "4155"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1673777
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->COMPOSER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final d()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1673773
    iput-object v0, p0, LX/ASE;->c:Landroid/view/View;

    .line 1673774
    iput-object v0, p0, LX/ASE;->b:Landroid/content/Context;

    .line 1673775
    iput-object v0, p0, LX/ASE;->d:LX/0hs;

    .line 1673776
    return-void
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 1673764
    invoke-direct {p0}, LX/ASE;->g()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1673765
    new-instance v0, LX/0hs;

    iget-object v1, p0, LX/ASE;->b:Landroid/content/Context;

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, LX/0hs;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, LX/ASE;->d:LX/0hs;

    .line 1673766
    iget-object v0, p0, LX/ASE;->d:LX/0hs;

    const v1, 0x7f0814ab

    invoke-virtual {v0, v1}, LX/0hs;->b(I)V

    .line 1673767
    iget-object v0, p0, LX/ASE;->d:LX/0hs;

    sget-object v1, LX/3AV;->ABOVE:LX/3AV;

    invoke-virtual {v0, v1}, LX/0ht;->a(LX/3AV;)V

    .line 1673768
    iget-object v0, p0, LX/ASE;->d:LX/0hs;

    const/4 v1, -0x1

    .line 1673769
    iput v1, v0, LX/0hs;->t:I

    .line 1673770
    iget-object v0, p0, LX/ASE;->d:LX/0hs;

    new-instance v1, LX/ASD;

    invoke-direct {v1, p0}, LX/ASD;-><init>(LX/ASE;)V

    invoke-virtual {v0, v1}, LX/0hs;->a(LX/5Od;)V

    .line 1673771
    iget-object v0, p0, LX/ASE;->d:LX/0hs;

    iget-object v1, p0, LX/ASE;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, LX/0ht;->f(Landroid/view/View;)V

    .line 1673772
    return-void
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 1673759
    iget-object v0, p0, LX/ASE;->d:LX/0hs;

    if-nez v0, :cond_0

    .line 1673760
    const/4 v0, 0x0

    .line 1673761
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/ASE;->d:LX/0hs;

    .line 1673762
    iget-boolean p0, v0, LX/0ht;->r:Z

    move v0, p0

    .line 1673763
    goto :goto_0
.end method
