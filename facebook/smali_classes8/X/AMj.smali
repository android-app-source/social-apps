.class public final LX/AMj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AMe;


# instance fields
.field public final synthetic a:LX/AMk;


# direct methods
.method public constructor <init>(LX/AMk;)V
    .locals 0

    .prologue
    .line 1666399
    iput-object p1, p0, LX/AMj;->a:LX/AMk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 9

    .prologue
    .line 1666370
    iget-object v0, p0, LX/AMj;->a:LX/AMk;

    iget-object v0, v0, LX/AMk;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AMK;

    .line 1666371
    iget-object v2, p0, LX/AMj;->a:LX/AMk;

    iget-object v2, v2, LX/AMk;->b:LX/AMT;

    .line 1666372
    iget-object v3, v0, LX/AMK;->a:Ljava/util/Map;

    .line 1666373
    iget-object v4, v2, LX/AMT;->d:Ljava/lang/String;

    move-object v4, v4

    .line 1666374
    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    sub-long v3, p1, v3

    .line 1666375
    iget-object v5, v0, LX/AMK;->a:Ljava/util/Map;

    .line 1666376
    iget-object v6, v2, LX/AMT;->d:Ljava/lang/String;

    move-object v6, v6

    .line 1666377
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1666378
    iget-object v5, v0, LX/AMK;->b:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v5, v3, v4}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    move-result-wide v3

    long-to-double v3, v3

    .line 1666379
    iget-object v5, v0, LX/AMK;->c:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v5

    .line 1666380
    const-wide/16 v7, 0x0

    cmp-long v7, v5, v7

    if-eqz v7, :cond_0

    .line 1666381
    iget-object v7, v0, LX/AMK;->d:LX/AZo;

    long-to-double v5, v5

    div-double/2addr v3, v5

    invoke-virtual {v7, v3, v4}, LX/AZo;->a(D)V

    .line 1666382
    :cond_0
    goto :goto_0

    .line 1666383
    :cond_1
    return-void
.end method

.method public final a(Ljava/io/File;)V
    .locals 6

    .prologue
    .line 1666387
    iget-object v0, p0, LX/AMj;->a:LX/AMk;

    iget-object v0, v0, LX/AMk;->b:LX/AMT;

    .line 1666388
    iget-boolean v1, v0, LX/AMT;->e:Z

    move v0, v1

    .line 1666389
    if-nez v0, :cond_0

    .line 1666390
    iget-object v0, p0, LX/AMj;->a:LX/AMk;

    invoke-static {v0, p1}, LX/AMk;->a$redex0(LX/AMk;Ljava/io/File;)V

    .line 1666391
    iget-object v0, p0, LX/AMj;->a:LX/AMk;

    iget-object v0, v0, LX/AMk;->a:Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;

    iget-object v1, p0, LX/AMj;->a:LX/AMk;

    iget-object v1, v1, LX/AMk;->b:LX/AMT;

    invoke-static {v0, v1}, Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;->a$redex0(Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;LX/AMT;)V

    .line 1666392
    :goto_0
    return-void

    .line 1666393
    :cond_0
    iget-object v0, p0, LX/AMj;->a:LX/AMk;

    iget-object v0, v0, LX/AMk;->a:Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;

    iget-object v0, v0, Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;->d:LX/AMf;

    iget-object v1, p0, LX/AMj;->a:LX/AMk;

    iget-object v1, v1, LX/AMk;->b:LX/AMT;

    .line 1666394
    iget-object v2, v1, LX/AMT;->f:Ljava/lang/String;

    move-object v1, v2

    .line 1666395
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/AMi;

    invoke-direct {v3, p0}, LX/AMi;-><init>(LX/AMj;)V

    .line 1666396
    iget-object v4, v0, LX/AMf;->a:Lcom/facebook/compactdisk/DiskCache;

    new-instance v5, LX/AMb;

    invoke-direct {v5, v0, v2}, LX/AMb;-><init>(LX/AMf;Ljava/lang/String;)V

    invoke-virtual {v4, v1, v5}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->storeManual(Ljava/lang/String;Lcom/facebook/compactdisk/ManualWrite;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 1666397
    iget-object v5, v0, LX/AMf;->b:LX/1Ck;

    sget-object p0, LX/AMd;->UNZIP_TO_CACHE:LX/AMd;

    new-instance p1, LX/AMc;

    invoke-direct {p1, v0, v1, v3}, LX/AMc;-><init>(LX/AMf;Ljava/lang/String;LX/AMe;)V

    invoke-virtual {v5, p0, v4, p1}, LX/1Ck;->c(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1666398
    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1666384
    iget-object v0, p0, LX/AMj;->a:LX/AMk;

    invoke-static {v0, p1}, LX/AMk;->a$redex0(LX/AMk;Ljava/lang/Throwable;)V

    .line 1666385
    iget-object v0, p0, LX/AMj;->a:LX/AMk;

    iget-object v0, v0, LX/AMk;->a:Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;

    iget-object v1, p0, LX/AMj;->a:LX/AMk;

    iget-object v1, v1, LX/AMk;->b:LX/AMT;

    invoke-static {v0, v1}, Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;->a$redex0(Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;LX/AMT;)V

    .line 1666386
    return-void
.end method
