.class public final LX/9Ei;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/9DF;

.field public final synthetic b:Lcom/facebook/feedback/ui/SingleLineCommentComposerView;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/SingleLineCommentComposerView;LX/9DF;)V
    .locals 0

    .prologue
    .line 1457318
    iput-object p1, p0, LX/9Ei;->b:Lcom/facebook/feedback/ui/SingleLineCommentComposerView;

    iput-object p2, p0, LX/9Ei;->a:LX/9DF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x39787795

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1457319
    iget-object v2, p0, LX/9Ei;->a:LX/9DF;

    iget-object v0, p0, LX/9Ei;->b:Lcom/facebook/feedback/ui/SingleLineCommentComposerView;

    iget-boolean v0, v0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->ab:Z

    if-eqz v0, :cond_0

    sget-object v0, LX/9CO;->REPLY_COMPOSER:LX/9CO;

    :goto_0
    iget-object v3, p0, LX/9Ei;->b:Lcom/facebook/feedback/ui/SingleLineCommentComposerView;

    iget-object v3, v3, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v3}, Lcom/facebook/feedback/ui/CommentComposerHelper;->d(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v3

    invoke-virtual {v2, v0, v3}, LX/9DF;->a(LX/9CO;Z)V

    .line 1457320
    const v0, 0x58d86f2d

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 1457321
    :cond_0
    sget-object v0, LX/9CO;->COMMENT_COMPOSER:LX/9CO;

    goto :goto_0
.end method
