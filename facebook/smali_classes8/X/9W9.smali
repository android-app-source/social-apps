.class public final LX/9W9;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;

.field public final synthetic b:Landroid/widget/ListView;

.field public final synthetic c:LX/9WM;

.field public final synthetic d:LX/9WC;


# direct methods
.method public constructor <init>(LX/9WC;Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;Landroid/widget/ListView;LX/9WM;)V
    .locals 0

    .prologue
    .line 1500630
    iput-object p1, p0, LX/9W9;->d:LX/9WC;

    iput-object p2, p0, LX/9W9;->a:Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;

    iput-object p3, p0, LX/9W9;->b:Landroid/widget/ListView;

    iput-object p4, p0, LX/9W9;->c:LX/9WM;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 1500631
    iget-object v0, p0, LX/9W9;->d:LX/9WC;

    iget-object v0, v0, LX/9WC;->d:LX/03V;

    sget-object v1, LX/9WC;->b:Ljava/lang/String;

    const-string v2, "NFX action failed"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1500632
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1500633
    const/4 v2, 0x1

    .line 1500634
    iget-object v0, p0, LX/9W9;->d:LX/9WC;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 1500635
    iput-object v1, v0, LX/9WC;->B:Ljava/lang/Boolean;

    .line 1500636
    sget-object v0, LX/9Tp;->d:LX/0Rf;

    iget-object v1, p0, LX/9W9;->a:Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;

    invoke-virtual {v1}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->gl_()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1500637
    iget-object v0, p0, LX/9W9;->d:LX/9WC;

    invoke-static {v0}, LX/9WC;->i(LX/9WC;)V

    .line 1500638
    :cond_0
    iget-object v0, p0, LX/9W9;->b:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 1500639
    iget-object v0, p0, LX/9W9;->d:LX/9WC;

    iget-object v0, v0, LX/9WC;->q:Ljava/util/Set;

    iget-object v1, p0, LX/9W9;->a:Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;

    invoke-virtual {v1}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->gl_()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1500640
    iget-object v0, p0, LX/9W9;->c:LX/9WM;

    iget-object v1, p0, LX/9W9;->d:LX/9WC;

    iget-object v1, v1, LX/9WC;->q:Ljava/util/Set;

    .line 1500641
    iput-object v1, v0, LX/9WM;->b:Ljava/util/Set;

    .line 1500642
    iget-object v0, p0, LX/9W9;->c:LX/9WM;

    sget-object v1, LX/9To;->COMPLETED:LX/9To;

    .line 1500643
    iput-object v1, v0, LX/9WM;->c:LX/9To;

    .line 1500644
    iget-object v0, p0, LX/9W9;->d:LX/9WC;

    iget-object v0, v0, LX/9WC;->u:LX/9Vy;

    instance-of v0, v0, LX/9WP;

    if-eqz v0, :cond_1

    .line 1500645
    iget-object v0, p0, LX/9W9;->d:LX/9WC;

    iget-object v0, v0, LX/9WC;->u:LX/9Vy;

    check-cast v0, LX/9WP;

    .line 1500646
    iget-object v1, v0, LX/9WP;->d:LX/9WL;

    move-object v0, v1

    .line 1500647
    const v1, -0xe17d9fe

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1500648
    :cond_1
    return-void
.end method
