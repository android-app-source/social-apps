.class public final LX/8mK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/facebook/stickers/model/StickerPack;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/stickers/store/StickerStoreFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/store/StickerStoreFragment;)V
    .locals 0

    .prologue
    .line 1398763
    iput-object p1, p0, LX/8mK;->a:Lcom/facebook/stickers/store/StickerStoreFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/stickers/model/StickerPack;Lcom/facebook/stickers/model/StickerPack;)I
    .locals 2

    .prologue
    .line 1398764
    if-eqz p0, :cond_0

    .line 1398765
    iget-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1398766
    if-nez v0, :cond_2

    :cond_0
    const-string v0, ""

    move-object v1, v0

    .line 1398767
    :goto_0
    if-eqz p1, :cond_1

    .line 1398768
    iget-object v0, p1, Lcom/facebook/stickers/model/StickerPack;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1398769
    if-nez v0, :cond_3

    :cond_1
    const-string v0, ""

    .line 1398770
    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0

    .line 1398771
    :cond_2
    iget-object v0, p0, Lcom/facebook/stickers/model/StickerPack;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1398772
    move-object v1, v0

    goto :goto_0

    .line 1398773
    :cond_3
    iget-object v0, p1, Lcom/facebook/stickers/model/StickerPack;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1398774
    goto :goto_1
.end method


# virtual methods
.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1398775
    check-cast p1, Lcom/facebook/stickers/model/StickerPack;

    check-cast p2, Lcom/facebook/stickers/model/StickerPack;

    invoke-static {p1, p2}, LX/8mK;->a(Lcom/facebook/stickers/model/StickerPack;Lcom/facebook/stickers/model/StickerPack;)I

    move-result v0

    return v0
.end method
