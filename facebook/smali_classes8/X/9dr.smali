.class public LX/9dr;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0wT;

.field public static final b:LX/0wT;


# instance fields
.field public c:Z

.field public d:LX/0wW;

.field public e:Landroid/widget/ImageView;

.field public final f:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

.field private final g:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/9dl;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0wd;

.field public i:LX/0wd;

.field public j:LX/0wd;

.field public k:LX/0wd;

.field public l:LX/9dq;

.field public m:D

.field public n:I


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1518825
    const-wide/high16 v0, 0x405e000000000000L    # 120.0

    const-wide/high16 v2, 0x4028000000000000L    # 12.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, LX/9dr;->a:LX/0wT;

    .line 1518826
    const-wide v0, 0x4046800000000000L    # 45.0

    const-wide/high16 v2, 0x4014000000000000L    # 5.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, LX/9dr;->b:LX/0wT;

    return-void
.end method

.method public constructor <init>(Landroid/widget/ImageView;Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;LX/9dl;LX/0wW;)V
    .locals 5
    .param p1    # Landroid/widget/ImageView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/9dl;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1518800
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1518801
    const v0, 0x7f0215ae

    iput v0, p0, LX/9dr;->n:I

    .line 1518802
    iput-object p4, p0, LX/9dr;->d:LX/0wW;

    .line 1518803
    iput-object p1, p0, LX/9dr;->e:Landroid/widget/ImageView;

    .line 1518804
    iput-object p2, p0, LX/9dr;->f:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    .line 1518805
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/9dr;->g:Ljava/lang/ref/WeakReference;

    .line 1518806
    iget-object v1, p0, LX/9dr;->d:LX/0wW;

    invoke-virtual {v1}, LX/0wW;->a()LX/0wd;

    move-result-object v1

    sget-object v2, LX/9dr;->b:LX/0wT;

    invoke-virtual {v1, v2}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v1

    const/4 v2, 0x1

    .line 1518807
    iput-boolean v2, v1, LX/0wd;->c:Z

    .line 1518808
    move-object v1, v1

    .line 1518809
    const-wide/high16 v3, -0x4010000000000000L    # -1.0

    invoke-virtual {v1, v3, v4}, LX/0wd;->a(D)LX/0wd;

    move-result-object v1

    new-instance v2, LX/9dm;

    invoke-direct {v2, p0}, LX/9dm;-><init>(LX/9dr;)V

    invoke-virtual {v1, v2}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v1

    iput-object v1, p0, LX/9dr;->h:LX/0wd;

    .line 1518810
    iget-object v1, p0, LX/9dr;->d:LX/0wW;

    invoke-virtual {v1}, LX/0wW;->a()LX/0wd;

    move-result-object v1

    sget-object v2, LX/9dr;->a:LX/0wT;

    invoke-virtual {v1, v2}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v1

    const/4 v2, 0x1

    .line 1518811
    iput-boolean v2, v1, LX/0wd;->c:Z

    .line 1518812
    move-object v1, v1

    .line 1518813
    const-wide/high16 v3, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v1, v3, v4}, LX/0wd;->a(D)LX/0wd;

    move-result-object v1

    new-instance v2, LX/9dn;

    invoke-direct {v2, p0}, LX/9dn;-><init>(LX/9dr;)V

    invoke-virtual {v1, v2}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v1

    iput-object v1, p0, LX/9dr;->i:LX/0wd;

    .line 1518814
    sget-object v1, LX/9dq;->DEFAULT:LX/9dq;

    iput-object v1, p0, LX/9dr;->l:LX/9dq;

    .line 1518815
    const/4 v3, 0x1

    .line 1518816
    iget-object v1, p0, LX/9dr;->d:LX/0wW;

    invoke-virtual {v1}, LX/0wW;->a()LX/0wd;

    move-result-object v1

    sget-object v2, LX/9dr;->a:LX/0wT;

    invoke-virtual {v1, v2}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v1

    .line 1518817
    iput-boolean v3, v1, LX/0wd;->c:Z

    .line 1518818
    move-object v1, v1

    .line 1518819
    new-instance v2, LX/9do;

    invoke-direct {v2, p0}, LX/9do;-><init>(LX/9dr;)V

    invoke-virtual {v1, v2}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v1

    iput-object v1, p0, LX/9dr;->j:LX/0wd;

    .line 1518820
    iget-object v1, p0, LX/9dr;->d:LX/0wW;

    invoke-virtual {v1}, LX/0wW;->a()LX/0wd;

    move-result-object v1

    sget-object v2, LX/9dr;->a:LX/0wT;

    invoke-virtual {v1, v2}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v1

    .line 1518821
    iput-boolean v3, v1, LX/0wd;->c:Z

    .line 1518822
    move-object v1, v1

    .line 1518823
    new-instance v2, LX/9dp;

    invoke-direct {v2, p0}, LX/9dp;-><init>(LX/9dr;)V

    invoke-virtual {v1, v2}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v1

    iput-object v1, p0, LX/9dr;->k:LX/0wd;

    .line 1518824
    return-void
.end method

.method public static i(LX/9dr;)V
    .locals 1

    .prologue
    .line 1518827
    iget-object v0, p0, LX/9dr;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1518828
    iget-object v0, p0, LX/9dr;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9dl;

    invoke-virtual {v0}, LX/9dl;->invalidate()V

    .line 1518829
    :cond_0
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 4

    .prologue
    .line 1518794
    iget-object v0, p0, LX/9dr;->f:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1518795
    :goto_0
    return-void

    .line 1518796
    :cond_0
    iget-object v0, p0, LX/9dr;->h:LX/0wd;

    sget-object v1, LX/9dr;->b:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    const/4 v1, 0x0

    .line 1518797
    iput-boolean v1, v0, LX/0wd;->c:Z

    .line 1518798
    move-object v0, v0

    .line 1518799
    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    goto :goto_0
.end method
