.class public final LX/8iY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/sounds/configurator/AudioConfigurator;


# direct methods
.method public constructor <init>(Lcom/facebook/sounds/configurator/AudioConfigurator;)V
    .locals 0

    .prologue
    .line 1391563
    iput-object p1, p0, LX/8iY;->a:Lcom/facebook/sounds/configurator/AudioConfigurator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/sounds/configurator/AudioConfigurator;B)V
    .locals 0

    .prologue
    .line 1391564
    invoke-direct {p0, p1}, LX/8iY;-><init>(Lcom/facebook/sounds/configurator/AudioConfigurator;)V

    return-void
.end method


# virtual methods
.method public final onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 10

    .prologue
    const-wide/16 v2, 0x0

    .line 1391565
    int-to-double v0, p2

    const-wide v4, 0x40c3880000000000L    # 10000.0

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    move-wide v6, v2

    invoke-static/range {v0 .. v9}, LX/8YN;->a(DDDDD)D

    move-result-wide v2

    .line 1391566
    iget-object v0, p0, LX/8iY;->a:Lcom/facebook/sounds/configurator/AudioConfigurator;

    iget-object v0, v0, Lcom/facebook/sounds/configurator/AudioConfigurator;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8iQ;

    double-to-float v1, v2

    .line 1391567
    iput v1, v0, LX/8iQ;->a:F

    .line 1391568
    iget-object v0, p0, LX/8iY;->a:Lcom/facebook/sounds/configurator/AudioConfigurator;

    invoke-static {v0}, Lcom/facebook/sounds/configurator/AudioConfigurator;->e(Lcom/facebook/sounds/configurator/AudioConfigurator;)V

    .line 1391569
    return-void
.end method

.method public final onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    .prologue
    .line 1391570
    return-void
.end method

.method public final onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2

    .prologue
    .line 1391571
    iget-object v0, p0, LX/8iY;->a:Lcom/facebook/sounds/configurator/AudioConfigurator;

    .line 1391572
    invoke-static {v0}, Lcom/facebook/sounds/configurator/AudioConfigurator;->c$redex0(Lcom/facebook/sounds/configurator/AudioConfigurator;)V

    .line 1391573
    return-void
.end method
