.class public final LX/9Lf;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 1471541
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_8

    .line 1471542
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1471543
    :goto_0
    return v1

    .line 1471544
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1471545
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_7

    .line 1471546
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1471547
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1471548
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 1471549
    const-string v8, "group_commerce_item_title"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1471550
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 1471551
    :cond_2
    const-string v8, "id"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1471552
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 1471553
    :cond_3
    const-string v8, "item_price"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1471554
    const/4 v7, 0x0

    .line 1471555
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v8, :cond_c

    .line 1471556
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1471557
    :goto_2
    move v4, v7

    .line 1471558
    goto :goto_1

    .line 1471559
    :cond_4
    const-string v8, "origin_group"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1471560
    invoke-static {p0, p1}, LX/9La;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1471561
    :cond_5
    const-string v8, "primary_photo"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 1471562
    const/4 v7, 0x0

    .line 1471563
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v8, :cond_10

    .line 1471564
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1471565
    :goto_3
    move v2, v7

    .line 1471566
    goto :goto_1

    .line 1471567
    :cond_6
    const-string v8, "sync_object"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1471568
    invoke-static {p0, p1}, LX/9Le;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1471569
    :cond_7
    const/4 v7, 0x6

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1471570
    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 1471571
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1471572
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1471573
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1471574
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1471575
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1471576
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_8
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto/16 :goto_1

    .line 1471577
    :cond_9
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1471578
    :cond_a
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_b

    .line 1471579
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1471580
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1471581
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_a

    if-eqz v8, :cond_a

    .line 1471582
    const-string v9, "formatted"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 1471583
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_4

    .line 1471584
    :cond_b
    const/4 v8, 0x1

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1471585
    invoke-virtual {p1, v7, v4}, LX/186;->b(II)V

    .line 1471586
    invoke-virtual {p1}, LX/186;->d()I

    move-result v7

    goto/16 :goto_2

    :cond_c
    move v4, v7

    goto :goto_4

    .line 1471587
    :cond_d
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1471588
    :cond_e
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_f

    .line 1471589
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1471590
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1471591
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_e

    if-eqz v8, :cond_e

    .line 1471592
    const-string v9, "image"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_d

    .line 1471593
    const/4 v8, 0x0

    .line 1471594
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v9, :cond_14

    .line 1471595
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1471596
    :goto_6
    move v2, v8

    .line 1471597
    goto :goto_5

    .line 1471598
    :cond_f
    const/4 v8, 0x1

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1471599
    invoke-virtual {p1, v7, v2}, LX/186;->b(II)V

    .line 1471600
    invoke-virtual {p1}, LX/186;->d()I

    move-result v7

    goto/16 :goto_3

    :cond_10
    move v2, v7

    goto :goto_5

    .line 1471601
    :cond_11
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1471602
    :cond_12
    :goto_7
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_13

    .line 1471603
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1471604
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1471605
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_12

    if-eqz v9, :cond_12

    .line 1471606
    const-string v10, "uri"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_11

    .line 1471607
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_7

    .line 1471608
    :cond_13
    const/4 v9, 0x1

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1471609
    invoke-virtual {p1, v8, v2}, LX/186;->b(II)V

    .line 1471610
    invoke-virtual {p1}, LX/186;->d()I

    move-result v8

    goto :goto_6

    :cond_14
    move v2, v8

    goto :goto_7
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1471611
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1471612
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1471613
    if-eqz v0, :cond_0

    .line 1471614
    const-string v1, "group_commerce_item_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1471615
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1471616
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1471617
    if-eqz v0, :cond_1

    .line 1471618
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1471619
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1471620
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1471621
    if-eqz v0, :cond_3

    .line 1471622
    const-string v1, "item_price"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1471623
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1471624
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1471625
    if-eqz v1, :cond_2

    .line 1471626
    const-string v2, "formatted"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1471627
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1471628
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1471629
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1471630
    if-eqz v0, :cond_4

    .line 1471631
    const-string v1, "origin_group"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1471632
    invoke-static {p0, v0, p2}, LX/9La;->a(LX/15i;ILX/0nX;)V

    .line 1471633
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1471634
    if-eqz v0, :cond_7

    .line 1471635
    const-string v1, "primary_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1471636
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1471637
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1471638
    if-eqz v1, :cond_6

    .line 1471639
    const-string v2, "image"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1471640
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1471641
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1471642
    if-eqz v2, :cond_5

    .line 1471643
    const-string v0, "uri"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1471644
    invoke-virtual {p2, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1471645
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1471646
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1471647
    :cond_7
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1471648
    if-eqz v0, :cond_8

    .line 1471649
    const-string v1, "sync_object"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1471650
    invoke-static {p0, v0, p2, p3}, LX/9Le;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1471651
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1471652
    return-void
.end method
