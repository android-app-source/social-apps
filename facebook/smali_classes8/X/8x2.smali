.class public final LX/8x2;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/8x3;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public final synthetic c:LX/8x3;


# direct methods
.method public constructor <init>(LX/8x3;)V
    .locals 1

    .prologue
    .line 1423421
    iput-object p1, p0, LX/8x2;->c:LX/8x3;

    .line 1423422
    move-object v0, p1

    .line 1423423
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1423424
    const/4 v0, 0x0

    iput v0, p0, LX/8x2;->b:I

    .line 1423425
    return-void
.end method

.method public synthetic constructor <init>(LX/8x3;B)V
    .locals 0

    .prologue
    .line 1423426
    invoke-direct {p0, p1}, LX/8x2;-><init>(LX/8x3;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1423427
    const-string v0, "PostPostBadgeIconComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1423428
    if-ne p0, p1, :cond_1

    .line 1423429
    :cond_0
    :goto_0
    return v0

    .line 1423430
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1423431
    goto :goto_0

    .line 1423432
    :cond_3
    check-cast p1, LX/8x2;

    .line 1423433
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1423434
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1423435
    if-eq v2, v3, :cond_0

    .line 1423436
    iget v2, p0, LX/8x2;->a:I

    iget v3, p1, LX/8x2;->a:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1423437
    goto :goto_0

    .line 1423438
    :cond_4
    iget v2, p0, LX/8x2;->b:I

    iget v3, p1, LX/8x2;->b:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1423439
    goto :goto_0
.end method
