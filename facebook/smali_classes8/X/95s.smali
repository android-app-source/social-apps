.class public final LX/95s;
.super LX/2h1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2h1",
        "<",
        "Lcom/facebook/graphql/model/GraphQLComment;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1L9;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLComment;

.field public final synthetic c:LX/3iQ;


# direct methods
.method public constructor <init>(LX/3iQ;LX/1L9;Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 0

    .prologue
    .line 1438009
    iput-object p1, p0, LX/95s;->c:LX/3iQ;

    iput-object p2, p0, LX/95s;->a:LX/1L9;

    iput-object p3, p0, LX/95s;->b:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-direct {p0}, LX/2h1;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 2

    .prologue
    .line 1438017
    iget-object v0, p0, LX/95s;->a:LX/1L9;

    if-eqz v0, :cond_0

    .line 1438018
    iget-object v0, p0, LX/95s;->a:LX/1L9;

    iget-object v1, p0, LX/95s;->b:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-interface {v0, v1, p1}, LX/1L9;->a(Ljava/lang/Object;Lcom/facebook/fbservice/service/ServiceException;)V

    .line 1438019
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1438010
    check-cast p1, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1438011
    if-nez p1, :cond_1

    .line 1438012
    iget-object v0, p0, LX/95s;->a:LX/1L9;

    if-eqz v0, :cond_0

    .line 1438013
    iget-object v0, p0, LX/95s;->a:LX/1L9;

    iget-object v1, p0, LX/95s;->b:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-interface {v0, v1}, LX/1L9;->c(Ljava/lang/Object;)V

    .line 1438014
    :cond_0
    :goto_0
    return-void

    .line 1438015
    :cond_1
    iget-object v0, p0, LX/95s;->a:LX/1L9;

    if-eqz v0, :cond_0

    .line 1438016
    iget-object v0, p0, LX/95s;->a:LX/1L9;

    invoke-interface {v0, p1}, LX/1L9;->b(Ljava/lang/Object;)V

    goto :goto_0
.end method
