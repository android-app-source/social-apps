.class public final LX/8kK;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 17

    .prologue
    .line 1395908
    const/4 v13, 0x0

    .line 1395909
    const/4 v12, 0x0

    .line 1395910
    const/4 v11, 0x0

    .line 1395911
    const/4 v10, 0x0

    .line 1395912
    const/4 v9, 0x0

    .line 1395913
    const/4 v8, 0x0

    .line 1395914
    const/4 v7, 0x0

    .line 1395915
    const/4 v6, 0x0

    .line 1395916
    const/4 v5, 0x0

    .line 1395917
    const/4 v4, 0x0

    .line 1395918
    const/4 v3, 0x0

    .line 1395919
    const/4 v2, 0x0

    .line 1395920
    const/4 v1, 0x0

    .line 1395921
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->START_OBJECT:LX/15z;

    if-eq v14, v15, :cond_1

    .line 1395922
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1395923
    const/4 v1, 0x0

    .line 1395924
    :goto_0
    return v1

    .line 1395925
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1395926
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->END_OBJECT:LX/15z;

    if-eq v14, v15, :cond_8

    .line 1395927
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v14

    .line 1395928
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1395929
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v15

    sget-object v16, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_1

    if-eqz v14, :cond_1

    .line 1395930
    const-string v15, "id"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 1395931
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    goto :goto_1

    .line 1395932
    :cond_2
    const-string v15, "is_comments_capable"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 1395933
    const/4 v6, 0x1

    .line 1395934
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    goto :goto_1

    .line 1395935
    :cond_3
    const-string v15, "is_composer_capable"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 1395936
    const/4 v5, 0x1

    .line 1395937
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    goto :goto_1

    .line 1395938
    :cond_4
    const-string v15, "is_messenger_capable"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 1395939
    const/4 v4, 0x1

    .line 1395940
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    goto :goto_1

    .line 1395941
    :cond_5
    const-string v15, "is_montage_capable"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 1395942
    const/4 v3, 0x1

    .line 1395943
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    goto :goto_1

    .line 1395944
    :cond_6
    const-string v15, "is_posts_capable"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 1395945
    const/4 v2, 0x1

    .line 1395946
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v8

    goto :goto_1

    .line 1395947
    :cond_7
    const-string v15, "is_sms_capable"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 1395948
    const/4 v1, 0x1

    .line 1395949
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    goto/16 :goto_1

    .line 1395950
    :cond_8
    const/4 v14, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->c(I)V

    .line 1395951
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 1395952
    if-eqz v6, :cond_9

    .line 1395953
    const/4 v6, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v12}, LX/186;->a(IZ)V

    .line 1395954
    :cond_9
    if-eqz v5, :cond_a

    .line 1395955
    const/4 v5, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v11}, LX/186;->a(IZ)V

    .line 1395956
    :cond_a
    if-eqz v4, :cond_b

    .line 1395957
    const/4 v4, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->a(IZ)V

    .line 1395958
    :cond_b
    if-eqz v3, :cond_c

    .line 1395959
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->a(IZ)V

    .line 1395960
    :cond_c
    if-eqz v2, :cond_d

    .line 1395961
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->a(IZ)V

    .line 1395962
    :cond_d
    if-eqz v1, :cond_e

    .line 1395963
    const/4 v1, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v7}, LX/186;->a(IZ)V

    .line 1395964
    :cond_e
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    .line 1395965
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1395966
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1395967
    if-eqz v0, :cond_0

    .line 1395968
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1395969
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1395970
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1395971
    if-eqz v0, :cond_1

    .line 1395972
    const-string v1, "is_comments_capable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1395973
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1395974
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1395975
    if-eqz v0, :cond_2

    .line 1395976
    const-string v1, "is_composer_capable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1395977
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1395978
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1395979
    if-eqz v0, :cond_3

    .line 1395980
    const-string v1, "is_messenger_capable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1395981
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1395982
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1395983
    if-eqz v0, :cond_4

    .line 1395984
    const-string v1, "is_montage_capable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1395985
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1395986
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1395987
    if-eqz v0, :cond_5

    .line 1395988
    const-string v1, "is_posts_capable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1395989
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1395990
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1395991
    if-eqz v0, :cond_6

    .line 1395992
    const-string v1, "is_sms_capable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1395993
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1395994
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1395995
    return-void
.end method
