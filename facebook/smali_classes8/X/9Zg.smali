.class public final LX/9Zg;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 1511512
    const-wide/16 v10, 0x0

    .line 1511513
    const/4 v8, 0x0

    .line 1511514
    const/4 v7, 0x0

    .line 1511515
    const/4 v6, 0x0

    .line 1511516
    const/4 v5, 0x0

    .line 1511517
    const/4 v4, 0x0

    .line 1511518
    const/4 v3, 0x0

    .line 1511519
    const/4 v2, 0x0

    .line 1511520
    const/4 v1, 0x0

    .line 1511521
    const/4 v0, 0x0

    .line 1511522
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v12, LX/15z;->START_OBJECT:LX/15z;

    if-eq v9, v12, :cond_c

    .line 1511523
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1511524
    const/4 v0, 0x0

    .line 1511525
    :goto_0
    return v0

    .line 1511526
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v0, v4, :cond_8

    .line 1511527
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    .line 1511528
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1511529
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_0

    if-eqz v0, :cond_0

    .line 1511530
    const-string v4, "admin_time"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1511531
    const/4 v0, 0x1

    .line 1511532
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v1, v0

    goto :goto_1

    .line 1511533
    :cond_1
    const-string v4, "biography"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1511534
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    move v13, v0

    goto :goto_1

    .line 1511535
    :cond_2
    const-string v4, "id"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1511536
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    move v12, v0

    goto :goto_1

    .line 1511537
    :cond_3
    const-string v4, "job_role"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1511538
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    move v11, v0

    goto :goto_1

    .line 1511539
    :cond_4
    const-string v4, "num_of_friends"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1511540
    const/4 v0, 0x1

    .line 1511541
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v7, v0

    move v10, v4

    goto :goto_1

    .line 1511542
    :cond_5
    const-string v4, "num_of_mutual_friends"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1511543
    const/4 v0, 0x1

    .line 1511544
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v6, v0

    move v9, v4

    goto :goto_1

    .line 1511545
    :cond_6
    const-string v4, "user"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1511546
    invoke-static {p0, p1}, LX/9Zf;->a(LX/15w;LX/186;)I

    move-result v0

    move v8, v0

    goto/16 :goto_1

    .line 1511547
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1511548
    :cond_8
    const/4 v0, 0x7

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1511549
    if-eqz v1, :cond_9

    .line 1511550
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1511551
    :cond_9
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v13}, LX/186;->b(II)V

    .line 1511552
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v12}, LX/186;->b(II)V

    .line 1511553
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 1511554
    if-eqz v7, :cond_a

    .line 1511555
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v10, v1}, LX/186;->a(III)V

    .line 1511556
    :cond_a
    if-eqz v6, :cond_b

    .line 1511557
    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v9, v1}, LX/186;->a(III)V

    .line 1511558
    :cond_b
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1511559
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_c
    move v9, v4

    move v12, v7

    move v13, v8

    move v8, v3

    move v7, v1

    move v1, v2

    move-wide v2, v10

    move v10, v5

    move v11, v6

    move v6, v0

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 1511560
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1511561
    invoke-virtual {p0, p1, v3, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1511562
    cmp-long v2, v0, v4

    if-eqz v2, :cond_0

    .line 1511563
    const-string v2, "admin_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1511564
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1511565
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1511566
    if-eqz v0, :cond_1

    .line 1511567
    const-string v1, "biography"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1511568
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1511569
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1511570
    if-eqz v0, :cond_2

    .line 1511571
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1511572
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1511573
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1511574
    if-eqz v0, :cond_3

    .line 1511575
    const-string v1, "job_role"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1511576
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1511577
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1511578
    if-eqz v0, :cond_4

    .line 1511579
    const-string v1, "num_of_friends"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1511580
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1511581
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1511582
    if-eqz v0, :cond_5

    .line 1511583
    const-string v1, "num_of_mutual_friends"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1511584
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1511585
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1511586
    if-eqz v0, :cond_6

    .line 1511587
    const-string v1, "user"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1511588
    invoke-static {p0, v0, p2, p3}, LX/9Zf;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1511589
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1511590
    return-void
.end method
