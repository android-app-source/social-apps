.class public final enum LX/9jC;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9jC;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9jC;

.field public static final enum Checkin:LX/9jC;

.field public static final enum Other:LX/9jC;

.field public static final enum Photo:LX/9jC;

.field public static final enum Status:LX/9jC;


# instance fields
.field public final mName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1529262
    new-instance v0, LX/9jC;

    const-string v1, "Status"

    const-string v2, "status"

    invoke-direct {v0, v1, v3, v2}, LX/9jC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9jC;->Status:LX/9jC;

    .line 1529263
    new-instance v0, LX/9jC;

    const-string v1, "Photo"

    const-string v2, "photo"

    invoke-direct {v0, v1, v4, v2}, LX/9jC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9jC;->Photo:LX/9jC;

    .line 1529264
    new-instance v0, LX/9jC;

    const-string v1, "Checkin"

    const-string v2, "checkin"

    invoke-direct {v0, v1, v5, v2}, LX/9jC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9jC;->Checkin:LX/9jC;

    .line 1529265
    new-instance v0, LX/9jC;

    const-string v1, "Other"

    const-string v2, "other"

    invoke-direct {v0, v1, v6, v2}, LX/9jC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/9jC;->Other:LX/9jC;

    .line 1529266
    const/4 v0, 0x4

    new-array v0, v0, [LX/9jC;

    sget-object v1, LX/9jC;->Status:LX/9jC;

    aput-object v1, v0, v3

    sget-object v1, LX/9jC;->Photo:LX/9jC;

    aput-object v1, v0, v4

    sget-object v1, LX/9jC;->Checkin:LX/9jC;

    aput-object v1, v0, v5

    sget-object v1, LX/9jC;->Other:LX/9jC;

    aput-object v1, v0, v6

    sput-object v0, LX/9jC;->$VALUES:[LX/9jC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1529267
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1529268
    iput-object p3, p0, LX/9jC;->mName:Ljava/lang/String;

    .line 1529269
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9jC;
    .locals 1

    .prologue
    .line 1529270
    const-class v0, LX/9jC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9jC;

    return-object v0
.end method

.method public static values()[LX/9jC;
    .locals 1

    .prologue
    .line 1529271
    sget-object v0, LX/9jC;->$VALUES:[LX/9jC;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9jC;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1529272
    iget-object v0, p0, LX/9jC;->mName:Ljava/lang/String;

    return-object v0
.end method
