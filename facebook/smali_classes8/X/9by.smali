.class public final LX/9by;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0xi;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/creativeediting/RotatingFrameLayout;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/creativeediting/RotatingFrameLayout;)V
    .locals 0

    .prologue
    .line 1515680
    iput-object p1, p0, LX/9by;->a:Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 8

    .prologue
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    .line 1515663
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_1

    move-wide v0, v2

    .line 1515664
    :goto_0
    iget-object v4, p0, LX/9by;->a:Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    iget-wide v4, v4, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->d:D

    iget-object v6, p0, LX/9by;->a:Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    iget-wide v6, v6, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->c:D

    sub-double/2addr v4, v6

    mul-double/2addr v0, v4

    iget-object v4, p0, LX/9by;->a:Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    iget-wide v4, v4, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->c:D

    add-double/2addr v0, v4

    double-to-float v0, v0

    .line 1515665
    iget-object v1, p0, LX/9by;->a:Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    invoke-virtual {v1, v0}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->setRotation(F)V

    .line 1515666
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    .line 1515667
    invoke-virtual {p1}, LX/0wd;->j()LX/0wd;

    .line 1515668
    :cond_0
    return-void

    .line 1515669
    :cond_1
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    goto :goto_0
.end method

.method public final b(LX/0wd;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 1515672
    iget-object v0, p0, LX/9by;->a:Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    iget-object v1, p0, LX/9by;->a:Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    iget-wide v2, v1, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->d:D

    double-to-float v1, v2

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->setRotation(F)V

    .line 1515673
    iget-object v0, p0, LX/9by;->a:Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    const-wide v2, -0x3f89800000000000L    # -360.0

    invoke-static {v0, v2, v3}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->a(Lcom/facebook/photos/creativeediting/RotatingFrameLayout;D)D

    .line 1515674
    iget-object v0, p0, LX/9by;->a:Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    iget-object v1, p0, LX/9by;->a:Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    iget-wide v2, v1, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->d:D

    .line 1515675
    iput-wide v2, v0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->c:D

    .line 1515676
    sget-object v0, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1515677
    iget-object v0, p0, LX/9by;->a:Lcom/facebook/photos/creativeediting/RotatingFrameLayout;

    .line 1515678
    invoke-static {v0, v4}, Lcom/facebook/photos/creativeediting/RotatingFrameLayout;->a$redex0(Lcom/facebook/photos/creativeediting/RotatingFrameLayout;Z)V

    .line 1515679
    return-void
.end method

.method public final c(LX/0wd;)V
    .locals 0

    .prologue
    .line 1515671
    return-void
.end method

.method public final d(LX/0wd;)V
    .locals 0

    .prologue
    .line 1515670
    return-void
.end method
