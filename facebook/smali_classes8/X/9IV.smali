.class public LX/9IV;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public a:LX/3AZ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0hy;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1463174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1463175
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1463176
    iput-object v0, p0, LX/9IV;->b:LX/0Ot;

    .line 1463177
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1463178
    iput-object v0, p0, LX/9IV;->c:LX/0Ot;

    .line 1463179
    return-void
.end method

.method public static a(LX/0QB;)LX/9IV;
    .locals 6

    .prologue
    .line 1463180
    const-class v1, LX/9IV;

    monitor-enter v1

    .line 1463181
    :try_start_0
    sget-object v0, LX/9IV;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1463182
    sput-object v2, LX/9IV;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1463183
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1463184
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1463185
    new-instance v4, LX/9IV;

    invoke-direct {v4}, LX/9IV;-><init>()V

    .line 1463186
    invoke-static {v0}, LX/3AZ;->a(LX/0QB;)LX/3AZ;

    move-result-object v3

    check-cast v3, LX/3AZ;

    const/16 v5, 0x455

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0xbe1

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 1463187
    iput-object v3, v4, LX/9IV;->a:LX/3AZ;

    iput-object v5, v4, LX/9IV;->b:LX/0Ot;

    iput-object p0, v4, LX/9IV;->c:LX/0Ot;

    .line 1463188
    move-object v0, v4

    .line 1463189
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1463190
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/9IV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1463191
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1463192
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
