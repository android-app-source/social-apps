.class public LX/9jQ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/9jQ;


# instance fields
.field public final a:LX/9jR;

.field public final b:LX/0TD;

.field public final c:LX/0tX;

.field public final d:LX/0Sh;


# direct methods
.method public constructor <init>(LX/9jR;LX/0TD;LX/0tX;LX/0Sh;)V
    .locals 0
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1529847
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1529848
    iput-object p1, p0, LX/9jQ;->a:LX/9jR;

    .line 1529849
    iput-object p2, p0, LX/9jQ;->b:LX/0TD;

    .line 1529850
    iput-object p3, p0, LX/9jQ;->c:LX/0tX;

    .line 1529851
    iput-object p4, p0, LX/9jQ;->d:LX/0Sh;

    .line 1529852
    return-void
.end method

.method public static a(LX/0QB;)LX/9jQ;
    .locals 7

    .prologue
    .line 1529828
    sget-object v0, LX/9jQ;->e:LX/9jQ;

    if-nez v0, :cond_1

    .line 1529829
    const-class v1, LX/9jQ;

    monitor-enter v1

    .line 1529830
    :try_start_0
    sget-object v0, LX/9jQ;->e:LX/9jQ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1529831
    if-eqz v2, :cond_0

    .line 1529832
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1529833
    new-instance p0, LX/9jQ;

    .line 1529834
    new-instance v6, LX/9jR;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v3

    check-cast v3, LX/0lB;

    .line 1529835
    new-instance v5, LX/9k8;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-direct {v5, v4}, LX/9k8;-><init>(Landroid/content/Context;)V

    .line 1529836
    move-object v4, v5

    .line 1529837
    check-cast v4, LX/9k8;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v5

    check-cast v5, LX/0Sh;

    invoke-direct {v6, v3, v4, v5}, LX/9jR;-><init>(LX/0lB;LX/9k8;LX/0Sh;)V

    .line 1529838
    move-object v3, v6

    .line 1529839
    check-cast v3, LX/9jR;

    invoke-static {v0}, LX/0tb;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, LX/0TD;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v6

    check-cast v6, LX/0Sh;

    invoke-direct {p0, v3, v4, v5, v6}, LX/9jQ;-><init>(LX/9jR;LX/0TD;LX/0tX;LX/0Sh;)V

    .line 1529840
    move-object v0, p0

    .line 1529841
    sput-object v0, LX/9jQ;->e:LX/9jQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1529842
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1529843
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1529844
    :cond_1
    sget-object v0, LX/9jQ;->e:LX/9jQ;

    return-object v0

    .line 1529845
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1529846
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
