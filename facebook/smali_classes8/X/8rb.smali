.class public final LX/8rb;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/44w",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/graphql/model/GraphQLActor;",
        ">;",
        "Lcom/facebook/graphql/model/GraphQLPageInfo;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/ufiservices/ui/ProfileListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/ufiservices/ui/ProfileListFragment;)V
    .locals 0

    .prologue
    .line 1408977
    iput-object p1, p0, LX/8rb;->a:Lcom/facebook/ufiservices/ui/ProfileListFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1408978
    iget-object v0, p0, LX/8rb;->a:Lcom/facebook/ufiservices/ui/ProfileListFragment;

    const/4 v1, 0x0

    .line 1408979
    iput-boolean v1, v0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->r:Z

    .line 1408980
    iget-object v0, p0, LX/8rb;->a:Lcom/facebook/ufiservices/ui/ProfileListFragment;

    const/4 v3, 0x1

    .line 1408981
    invoke-static {p1}, Lcom/facebook/fbservice/service/ServiceException;->forException(Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/ServiceException;

    move-result-object v1

    .line 1408982
    iget-object v2, v0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->c:LX/1CW;

    invoke-virtual {v2, v1, v3, v3}, LX/1CW;->a(Lcom/facebook/fbservice/service/ServiceException;ZZ)Ljava/lang/String;

    move-result-object v2

    .line 1408983
    invoke-static {v1}, LX/1CW;->b(Lcom/facebook/fbservice/service/ServiceException;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1408984
    iget-object v1, v0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->b:LX/1CX;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3}, LX/4mm;->a(Landroid/content/res/Resources;)LX/4mn;

    move-result-object v3

    sget-object p0, LX/8iJ;->SENTRY_PROFILE_LIST_BLOCK:LX/8iJ;

    invoke-virtual {p0}, LX/8iJ;->getTitleId()I

    move-result p0

    invoke-virtual {v3, p0}, LX/4mn;->a(I)LX/4mn;

    move-result-object v3

    .line 1408985
    iput-object v2, v3, LX/4mn;->c:Ljava/lang/String;

    .line 1408986
    move-object v3, v3

    .line 1408987
    const p0, 0x7f080054

    invoke-virtual {v3, p0}, LX/4mn;->c(I)LX/4mn;

    move-result-object v3

    sget-object p0, LX/8iK;->a:Landroid/net/Uri;

    .line 1408988
    iput-object p0, v3, LX/4mn;->e:Landroid/net/Uri;

    .line 1408989
    move-object v3, v3

    .line 1408990
    invoke-virtual {v3}, LX/4mn;->l()LX/4mm;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/1CX;->a(LX/4mm;)LX/2EJ;

    .line 1408991
    :cond_0
    iget-object v1, v0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->k:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->m:LX/55h;

    invoke-virtual {v1}, LX/55h;->c()I

    move-result v1

    if-nez v1, :cond_1

    .line 1408992
    iget-object v1, v0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->k:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget-object v3, v0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->n:LX/8re;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    .line 1408993
    :cond_1
    const-string v1, "feedbackId: %s"

    iget-object v2, v0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->s:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1408994
    iget-object v2, v0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->d:LX/03V;

    const-string v3, "FetchLikersFailed"

    invoke-virtual {v2, v3, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1408995
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 1408996
    check-cast p1, LX/44w;

    const/4 v1, 0x0

    .line 1408997
    iget-object v0, p0, LX/8rb;->a:Lcom/facebook/ufiservices/ui/ProfileListFragment;

    .line 1408998
    iput-boolean v1, v0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->r:Z

    .line 1408999
    iget-object v0, p0, LX/8rb;->a:Lcom/facebook/ufiservices/ui/ProfileListFragment;

    iget-object v0, v0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->k:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 1409000
    iget-object v0, p0, LX/8rb;->a:Lcom/facebook/ufiservices/ui/ProfileListFragment;

    const/4 v2, 0x0

    .line 1409001
    if-eqz p1, :cond_0

    iget-object v3, p1, LX/44w;->a:Ljava/lang/Object;

    if-nez v3, :cond_1

    .line 1409002
    :cond_0
    :goto_0
    iget-object v0, p0, LX/8rb;->a:Lcom/facebook/ufiservices/ui/ProfileListFragment;

    iget-object v0, v0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->j:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setVisibility(I)V

    .line 1409003
    return-void

    .line 1409004
    :cond_1
    iget-object v3, v0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->j:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v3}, Lcom/facebook/widget/listview/BetterListView;->getFirstVisiblePosition()I

    move-result v4

    .line 1409005
    iget-object v3, v0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->j:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v3, v2}, Lcom/facebook/widget/listview/BetterListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1409006
    if-nez v3, :cond_2

    move v3, v2

    .line 1409007
    :goto_1
    iget-object v2, p1, LX/44w;->b:Ljava/lang/Object;

    check-cast v2, Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-static {v2}, LX/55i;->a(Lcom/facebook/graphql/model/GraphQLPageInfo;)LX/1vs;

    move-result-object v2

    iget-object v5, v2, LX/1vs;->a:LX/15i;

    iget v6, v2, LX/1vs;->b:I

    .line 1409008
    iget-object v7, v0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->m:LX/55h;

    iget-object v2, p1, LX/44w;->a:Ljava/lang/Object;

    check-cast v2, Ljava/util/List;

    invoke-virtual {v0, v2}, Lcom/facebook/ufiservices/ui/ProfileListFragment;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v7, v2, v5, v6}, LX/55h;->a(Ljava/lang/Iterable;LX/15i;I)V

    .line 1409009
    iget-object v2, v0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->l:Landroid/widget/BaseAdapter;

    const v5, 0x7e01d51e

    invoke-static {v2, v5}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1409010
    iget-object v2, v0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->j:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v2, v4, v3}, Lcom/facebook/widget/listview/BetterListView;->setSelectionFromTop(II)V

    goto :goto_0

    .line 1409011
    :cond_2
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v2

    move v3, v2

    goto :goto_1
.end method
