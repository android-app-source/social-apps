.class public LX/8ol;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1404691
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1404692
    const-string v0, ""

    iput-object v0, p0, LX/8ol;->b:Ljava/lang/String;

    .line 1404693
    iput-object p1, p0, LX/8ol;->a:LX/0Zb;

    .line 1404694
    return-void
.end method

.method public static a(LX/8ol;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 3

    .prologue
    .line 1404695
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "click"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "mentions"

    .line 1404696
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1404697
    move-object v0, v0

    .line 1404698
    const-string v1, "action"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "session_id"

    iget-object v2, p0, LX/8ol;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    return-object v0
.end method
