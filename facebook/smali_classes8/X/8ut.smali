.class public final enum LX/8ut;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8ut;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8ut;

.field public static final enum NORMAL:LX/8ut;

.field public static final enum NO_DROPDOWN:LX/8ut;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1415818
    new-instance v0, LX/8ut;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v2}, LX/8ut;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8ut;->NORMAL:LX/8ut;

    new-instance v0, LX/8ut;

    const-string v1, "NO_DROPDOWN"

    invoke-direct {v0, v1, v3}, LX/8ut;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8ut;->NO_DROPDOWN:LX/8ut;

    .line 1415819
    const/4 v0, 0x2

    new-array v0, v0, [LX/8ut;

    sget-object v1, LX/8ut;->NORMAL:LX/8ut;

    aput-object v1, v0, v2

    sget-object v1, LX/8ut;->NO_DROPDOWN:LX/8ut;

    aput-object v1, v0, v3

    sput-object v0, LX/8ut;->$VALUES:[LX/8ut;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1415820
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8ut;
    .locals 1

    .prologue
    .line 1415821
    const-class v0, LX/8ut;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8ut;

    return-object v0
.end method

.method public static values()[LX/8ut;
    .locals 1

    .prologue
    .line 1415822
    sget-object v0, LX/8ut;->$VALUES:[LX/8ut;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8ut;

    return-object v0
.end method
