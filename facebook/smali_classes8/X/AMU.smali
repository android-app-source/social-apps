.class public final LX/AMU;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$FetchBestMasksQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AMJ;

.field public final synthetic b:LX/AMW;


# direct methods
.method public constructor <init>(LX/AMW;LX/AMJ;)V
    .locals 0

    .prologue
    .line 1666258
    iput-object p1, p0, LX/AMU;->b:LX/AMW;

    iput-object p2, p0, LX/AMU;->a:LX/AMJ;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1666281
    iget-object v0, p0, LX/AMU;->a:LX/AMJ;

    .line 1666282
    iget-object p0, v0, LX/AMJ;->a:LX/AMN;

    invoke-interface {p0, p1}, LX/AMN;->a(Ljava/lang/Throwable;)V

    .line 1666283
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 1666259
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v6, 0x0

    .line 1666260
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1666261
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1666262
    check-cast v0, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$FetchBestMasksQueryModel;

    invoke-virtual {v0}, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$FetchBestMasksQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1666263
    if-nez v0, :cond_0

    .line 1666264
    iget-object v0, p0, LX/AMU;->a:LX/AMJ;

    .line 1666265
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 1666266
    invoke-virtual {v0, v1}, LX/AMJ;->a(LX/0Px;)V

    .line 1666267
    :goto_0
    return-void

    .line 1666268
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1666269
    :cond_0
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 1666270
    const v4, 0x34aae13e

    invoke-static {v2, v0, v6, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v2

    :cond_1
    :goto_2
    invoke-interface {v2}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1666271
    const-class v5, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$FetchBestMasksQueryModel$MaskEffectsModel$EdgesModel$NodeModel;

    invoke-virtual {v4, v0, v6, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$FetchBestMasksQueryModel$MaskEffectsModel$EdgesModel$NodeModel;

    .line 1666272
    if-eqz v0, :cond_1

    .line 1666273
    invoke-virtual {v0}, Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$FetchBestMasksQueryModel$MaskEffectsModel$EdgesModel$NodeModel;->j()Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$BestMaskPackageFragmentModel;

    move-result-object v0

    .line 1666274
    invoke-static {v0}, LX/AMT;->a(Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$BestMaskPackageFragmentModel;)LX/AMT;

    move-result-object v4

    .line 1666275
    if-eqz v4, :cond_1

    .line 1666276
    invoke-virtual {v1, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1666277
    invoke-static {v0, v3}, LX/AMT;->a(Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$BestMaskPackageFragmentModel;Ljava/util/Set;)Ljava/util/List;

    move-result-object v0

    .line 1666278
    invoke-virtual {v1, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    goto :goto_2

    .line 1666279
    :cond_2
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_1

    .line 1666280
    :cond_3
    iget-object v0, p0, LX/AMU;->a:LX/AMJ;

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/AMJ;->a(LX/0Px;)V

    goto :goto_0
.end method
