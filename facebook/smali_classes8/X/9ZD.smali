.class public final LX/9ZD;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1509513
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_8

    .line 1509514
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1509515
    :goto_0
    return v1

    .line 1509516
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_4

    .line 1509517
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1509518
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1509519
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_0

    if-eqz v8, :cond_0

    .line 1509520
    const-string v9, "can_viewer_promote"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 1509521
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v4

    move v7, v4

    move v4, v2

    goto :goto_1

    .line 1509522
    :cond_1
    const-string v9, "does_viewer_pin"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 1509523
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v6, v3

    move v3, v2

    goto :goto_1

    .line 1509524
    :cond_2
    const-string v9, "is_likely_to_advertise"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1509525
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v5, v0

    move v0, v2

    goto :goto_1

    .line 1509526
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1509527
    :cond_4
    const/4 v8, 0x3

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1509528
    if-eqz v4, :cond_5

    .line 1509529
    invoke-virtual {p1, v1, v7}, LX/186;->a(IZ)V

    .line 1509530
    :cond_5
    if-eqz v3, :cond_6

    .line 1509531
    invoke-virtual {p1, v2, v6}, LX/186;->a(IZ)V

    .line 1509532
    :cond_6
    if-eqz v0, :cond_7

    .line 1509533
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v5}, LX/186;->a(IZ)V

    .line 1509534
    :cond_7
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_8
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    .line 1509535
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1509536
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1509537
    if-eqz v0, :cond_0

    .line 1509538
    const-string v1, "can_viewer_promote"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1509539
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1509540
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1509541
    if-eqz v0, :cond_1

    .line 1509542
    const-string v1, "does_viewer_pin"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1509543
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1509544
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1509545
    if-eqz v0, :cond_2

    .line 1509546
    const-string v1, "is_likely_to_advertise"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1509547
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1509548
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1509549
    return-void
.end method
