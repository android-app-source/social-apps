.class public final LX/9f0;
.super LX/9cs;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V
    .locals 0

    .prologue
    .line 1520748
    iput-object p1, p0, LX/9f0;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    invoke-direct {p0}, LX/9cs;-><init>()V

    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 1520726
    sget-object v0, LX/9ek;->SHOW_ORIGINAL_URI:LX/9ek;

    iget-object v1, p0, LX/9f0;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->N:LX/9eb;

    invoke-interface {v1}, LX/9eb;->k()LX/9ek;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9ek;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1520727
    iget-object v0, p0, LX/9f0;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-object v1, p0, LX/9f0;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    invoke-static {v1}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->w(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)Landroid/graphics/Rect;

    move-result-object v1

    .line 1520728
    iput-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->ab:Landroid/graphics/Rect;

    .line 1520729
    :goto_0
    return-void

    .line 1520730
    :cond_0
    iget-object v0, p0, LX/9f0;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    .line 1520731
    invoke-static {v0}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->w(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)Landroid/graphics/Rect;

    move-result-object v1

    .line 1520732
    iget-object p0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->ab:Landroid/graphics/Rect;

    invoke-static {v1, p0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    if-eqz p1, :cond_2

    .line 1520733
    :cond_1
    iput-object v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->ab:Landroid/graphics/Rect;

    .line 1520734
    invoke-static {v0, v1}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->a(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;Landroid/graphics/Rect;)V

    .line 1520735
    :cond_2
    invoke-static {v0}, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->x(Lcom/facebook/photos/editgallery/EditGalleryFragmentController;)V

    .line 1520736
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/5iG;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1520743
    invoke-super {p0, p1}, LX/9cs;->a(LX/5iG;)V

    .line 1520744
    iget-object v0, p0, LX/9f0;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    .line 1520745
    iput-boolean v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->ac:Z

    .line 1520746
    invoke-direct {p0, v1}, LX/9f0;->a(Z)V

    .line 1520747
    return-void
.end method

.method public final b(LX/5iG;LX/5iG;LX/5iG;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1520737
    invoke-super {p0, p1, p2, p3}, LX/9cs;->b(LX/5iG;LX/5iG;LX/5iG;)V

    .line 1520738
    iget-object v0, p0, LX/9f0;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    iget-boolean v0, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->ac:Z

    if-nez v0, :cond_0

    .line 1520739
    invoke-direct {p0, v1}, LX/9f0;->a(Z)V

    .line 1520740
    :cond_0
    iget-object v0, p0, LX/9f0;->a:Lcom/facebook/photos/editgallery/EditGalleryFragmentController;

    .line 1520741
    iput-boolean v1, v0, Lcom/facebook/photos/editgallery/EditGalleryFragmentController;->ac:Z

    .line 1520742
    return-void
.end method
