.class public LX/A6h;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;


# instance fields
.field private final c:LX/0Uh;

.field public final d:LX/0ad;

.field private final e:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1624652
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "transliteration/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 1624653
    sput-object v0, LX/A6h;->a:LX/0Tn;

    const-string v1, "enabled"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/A6h;->b:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/0Uh;LX/0ad;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1624654
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1624655
    iput-object p1, p0, LX/A6h;->c:LX/0Uh;

    .line 1624656
    iput-object p2, p0, LX/A6h;->d:LX/0ad;

    .line 1624657
    iput-object p3, p0, LX/A6h;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1624658
    return-void
.end method

.method public static a(LX/0QB;)LX/A6h;
    .locals 1

    .prologue
    .line 1624659
    invoke-static {p0}, LX/A6h;->b(LX/0QB;)LX/A6h;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/A6h;
    .locals 4

    .prologue
    .line 1624660
    new-instance v3, LX/A6h;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v3, v0, v1, v2}, LX/A6h;-><init>(LX/0Uh;LX/0ad;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 1624661
    return-object v3
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 1624662
    iget-object v0, p0, LX/A6h;->c:LX/0Uh;

    const/16 v1, 0x44c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 1624663
    iget-object v0, p0, LX/A6h;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1624664
    iget-object v0, p0, LX/A6h;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/A6h;->b:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->b(LX/0Tn;)LX/03R;

    move-result-object v0

    .line 1624665
    invoke-virtual {v0}, LX/03R;->isSet()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1624666
    invoke-virtual {v0}, LX/03R;->asBoolean()Z

    move-result v0

    .line 1624667
    :goto_0
    return v0

    .line 1624668
    :cond_0
    iget-object v0, p0, LX/A6h;->d:LX/0ad;

    sget-short v1, LX/A6O;->a:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 1624669
    move v0, v0

    .line 1624670
    goto :goto_0
.end method
