.class public LX/8uR;
.super Landroid/graphics/drawable/shapes/RectShape;
.source ""

# interfaces
.implements LX/7U6;


# instance fields
.field private a:F

.field private b:F

.field private c:Landroid/graphics/Path;

.field private d:Z

.field private e:Landroid/graphics/RectF;


# direct methods
.method public constructor <init>(FF)V
    .locals 1

    .prologue
    .line 1414772
    invoke-direct {p0}, Landroid/graphics/drawable/shapes/RectShape;-><init>()V

    .line 1414773
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, LX/8uR;->c:Landroid/graphics/Path;

    .line 1414774
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/8uR;->e:Landroid/graphics/RectF;

    .line 1414775
    invoke-virtual {p0, p1, p2}, LX/8uR;->a(FF)Z

    .line 1414776
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/RectF;)V
    .locals 4

    .prologue
    .line 1414777
    iget-boolean v0, p0, LX/8uR;->d:Z

    if-eqz v0, :cond_0

    .line 1414778
    iget-object v0, p0, LX/8uR;->c:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 1414779
    iget-object v0, p0, LX/8uR;->c:Landroid/graphics/Path;

    invoke-virtual {p0}, LX/8uR;->rect()Landroid/graphics/RectF;

    move-result-object v1

    iget v2, p0, LX/8uR;->a:F

    iget v3, p0, LX/8uR;->b:F

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 1414780
    iget-object v0, p0, LX/8uR;->c:Landroid/graphics/Path;

    iget-object v1, p0, LX/8uR;->e:Landroid/graphics/RectF;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 1414781
    :cond_0
    iget-object v0, p0, LX/8uR;->e:Landroid/graphics/RectF;

    invoke-virtual {p1, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 1414782
    return-void
.end method

.method public final a(FF)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1414783
    iget v1, p0, LX/8uR;->a:F

    cmpl-float v1, v1, p1

    if-nez v1, :cond_0

    iget v1, p0, LX/8uR;->b:F

    cmpl-float v1, v1, p2

    if-nez v1, :cond_0

    .line 1414784
    const/4 v0, 0x0

    .line 1414785
    :goto_0
    return v0

    .line 1414786
    :cond_0
    iput p1, p0, LX/8uR;->a:F

    .line 1414787
    iput p2, p0, LX/8uR;->b:F

    .line 1414788
    iput-boolean v0, p0, LX/8uR;->d:Z

    goto :goto_0
.end method

.method public final draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 6

    .prologue
    .line 1414789
    invoke-virtual {p0}, LX/8uR;->rect()Landroid/graphics/RectF;

    move-result-object v1

    iget v2, p0, LX/8uR;->a:F

    iget v3, p0, LX/8uR;->b:F

    const/4 v4, 0x1

    move-object v0, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 1414790
    return-void
.end method

.method public final onResize(FF)V
    .locals 1

    .prologue
    .line 1414791
    invoke-super {p0, p1, p2}, Landroid/graphics/drawable/shapes/RectShape;->onResize(FF)V

    .line 1414792
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8uR;->d:Z

    .line 1414793
    return-void
.end method
