.class public LX/9WP;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/9Vy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/widget/CustomLinearLayout;",
        "LX/9Vy",
        "<",
        "Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryInterfaces$NegativeFeedbackPromptQueryFragment;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Landroid/widget/ListView;

.field private b:Lcom/facebook/resources/ui/FbTextView;

.field public c:Landroid/widget/FrameLayout;

.field public d:LX/9WL;

.field public e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1501136
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1501137
    invoke-static {p0}, LX/9W0;->a(Lcom/facebook/widget/CustomLinearLayout;)V

    .line 1501138
    const v0, 0x7f030bd8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1501139
    const v0, 0x7f0d1d7f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, LX/9WP;->a:Landroid/widget/ListView;

    .line 1501140
    const v0, 0x7f0d1d7e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, LX/9WP;->c:Landroid/widget/FrameLayout;

    .line 1501141
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/9WP;->e:Ljava/util/Set;

    .line 1501142
    iget-object v0, p0, LX/9WP;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object p1, p0, LX/9WP;->a:Landroid/widget/ListView;

    invoke-static {p1, p0}, LX/9W0;->a(Landroid/widget/ListView;Lcom/facebook/widget/CustomLinearLayout;)Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1501143
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1501144
    invoke-virtual {p1}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1501145
    invoke-virtual {p0}, LX/9WP;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030bd5

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1501146
    :goto_0
    iget-object v1, p0, LX/9WP;->a:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 1501147
    new-instance v1, LX/9WL;

    invoke-virtual {p0}, LX/9WP;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0d1d79

    invoke-direct {v1, v3, v4}, LX/9WL;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, LX/9WP;->d:LX/9WL;

    .line 1501148
    iget-object v1, p0, LX/9WP;->a:Landroid/widget/ListView;

    iget-object v3, p0, LX/9WP;->d:LX/9WL;

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1501149
    const v1, 0x7f0d1d78

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/9WP;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 1501150
    invoke-virtual {p1}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;->c()Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$TitleModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1501151
    iget-object v0, p0, LX/9WP;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p1}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;->c()Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$TitleModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1501152
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v2

    .line 1501153
    :goto_1
    if-ge v1, v4, :cond_3

    .line 1501154
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;

    .line 1501155
    sget-object v5, LX/9Tp;->b:LX/0Rf;

    invoke-virtual {v0}, Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;->gl_()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1501156
    iget-object v5, p0, LX/9WP;->d:LX/9WL;

    new-instance v6, LX/9WM;

    iget-object v7, p0, LX/9WP;->e:Ljava/util/Set;

    invoke-direct {v6, v0, v7}, LX/9WM;-><init>(Lcom/facebook/negativefeedback/protocol/NegativeFeedbackQueryModels$NegativeFeedbackPromptQueryFragmentModel$ResponsesModel;Ljava/util/Set;)V

    invoke-virtual {v5, v6}, LX/9WL;->add(Ljava/lang/Object;)V

    .line 1501157
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1501158
    :cond_2
    invoke-virtual {p0}, LX/9WP;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030bd9

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    goto :goto_0

    .line 1501159
    :cond_3
    invoke-virtual {p0, v2}, LX/9WP;->setProgressBarVisibility(Z)V

    .line 1501160
    return-void
.end method

.method public setProgressBarVisibility(Z)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 1501161
    if-eqz p1, :cond_0

    .line 1501162
    iget-object v0, p0, LX/9WP;->c:Landroid/widget/FrameLayout;

    iget-object v1, p0, LX/9WP;->a:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getHeight()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setMinimumHeight(I)V

    .line 1501163
    iget-object v0, p0, LX/9WP;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1501164
    iget-object v0, p0, LX/9WP;->a:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setVisibility(I)V

    .line 1501165
    :goto_0
    return-void

    .line 1501166
    :cond_0
    iget-object v0, p0, LX/9WP;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1501167
    iget-object v0, p0, LX/9WP;->a:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_0
.end method
