.class public final LX/97L;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1441476
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 1441477
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1441478
    :goto_0
    return v1

    .line 1441479
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1441480
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_5

    .line 1441481
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1441482
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1441483
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 1441484
    const-string v6, "page"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1441485
    invoke-static {p0, p1}, LX/97J;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1441486
    :cond_2
    const-string v6, "place_profile_picture"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1441487
    invoke-static {p0, p1}, LX/97K;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1441488
    :cond_3
    const-string v6, "place_subtitle"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1441489
    invoke-static {p0, p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1441490
    :cond_4
    const-string v6, "place_title"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1441491
    invoke-static {p0, p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1441492
    :cond_5
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1441493
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1441494
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1441495
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1441496
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1441497
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1441498
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1441499
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1441500
    if-eqz v0, :cond_0

    .line 1441501
    const-string v1, "page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1441502
    invoke-static {p0, v0, p2}, LX/97J;->a(LX/15i;ILX/0nX;)V

    .line 1441503
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1441504
    if-eqz v0, :cond_1

    .line 1441505
    const-string v1, "place_profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1441506
    invoke-static {p0, v0, p2}, LX/97K;->a(LX/15i;ILX/0nX;)V

    .line 1441507
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1441508
    if-eqz v0, :cond_2

    .line 1441509
    const-string v1, "place_subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1441510
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 1441511
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1441512
    if-eqz v0, :cond_3

    .line 1441513
    const-string v1, "place_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1441514
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 1441515
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1441516
    return-void
.end method
