.class public final enum LX/9dq;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/9dq;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/9dq;

.field public static final enum CLOSING:LX/9dq;

.field public static final enum DEFAULT:LX/9dq;

.field public static final enum OPENING:LX/9dq;

.field public static final enum OPENING_COMPLETE:LX/9dq;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1518786
    new-instance v0, LX/9dq;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v2}, LX/9dq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9dq;->DEFAULT:LX/9dq;

    .line 1518787
    new-instance v0, LX/9dq;

    const-string v1, "OPENING"

    invoke-direct {v0, v1, v3}, LX/9dq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9dq;->OPENING:LX/9dq;

    .line 1518788
    new-instance v0, LX/9dq;

    const-string v1, "OPENING_COMPLETE"

    invoke-direct {v0, v1, v4}, LX/9dq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9dq;->OPENING_COMPLETE:LX/9dq;

    .line 1518789
    new-instance v0, LX/9dq;

    const-string v1, "CLOSING"

    invoke-direct {v0, v1, v5}, LX/9dq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/9dq;->CLOSING:LX/9dq;

    .line 1518790
    const/4 v0, 0x4

    new-array v0, v0, [LX/9dq;

    sget-object v1, LX/9dq;->DEFAULT:LX/9dq;

    aput-object v1, v0, v2

    sget-object v1, LX/9dq;->OPENING:LX/9dq;

    aput-object v1, v0, v3

    sget-object v1, LX/9dq;->OPENING_COMPLETE:LX/9dq;

    aput-object v1, v0, v4

    sget-object v1, LX/9dq;->CLOSING:LX/9dq;

    aput-object v1, v0, v5

    sput-object v0, LX/9dq;->$VALUES:[LX/9dq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1518791
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/9dq;
    .locals 1

    .prologue
    .line 1518792
    const-class v0, LX/9dq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/9dq;

    return-object v0
.end method

.method public static values()[LX/9dq;
    .locals 1

    .prologue
    .line 1518793
    sget-object v0, LX/9dq;->$VALUES:[LX/9dq;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/9dq;

    return-object v0
.end method
