.class public LX/9nH;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1536975
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)LX/5pH;
    .locals 1
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1536974
    const-string v0, "Invalid key"

    invoke-static {p0, v0}, LX/9nH;->a(Ljava/lang/String;Ljava/lang/String;)LX/5pH;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)LX/5pH;
    .locals 2
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1536967
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v0

    .line 1536968
    const-string v1, "message"

    invoke-interface {v0, v1, p1}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1536969
    if-eqz p0, :cond_0

    .line 1536970
    const-string v1, "key"

    invoke-interface {v0, v1, p0}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1536971
    :cond_0
    return-object v0
.end method

.method public static b(Ljava/lang/String;)LX/5pH;
    .locals 1
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1536973
    const-string v0, "Invalid Value"

    invoke-static {p0, v0}, LX/9nH;->a(Ljava/lang/String;Ljava/lang/String;)LX/5pH;

    move-result-object v0

    return-object v0
.end method

.method public static c(Ljava/lang/String;)LX/5pH;
    .locals 1
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1536972
    const-string v0, "Database Error"

    invoke-static {p0, v0}, LX/9nH;->a(Ljava/lang/String;Ljava/lang/String;)LX/5pH;

    move-result-object v0

    return-object v0
.end method
