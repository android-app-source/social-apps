.class public final LX/95w;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:LX/1L9;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public final synthetic c:LX/3id;


# direct methods
.method public constructor <init>(LX/3id;LX/1L9;Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 0

    .prologue
    .line 1438052
    iput-object p1, p0, LX/95w;->c:LX/3id;

    iput-object p2, p0, LX/95w;->a:LX/1L9;

    iput-object p3, p0, LX/95w;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 2

    .prologue
    .line 1438044
    iget-object v0, p0, LX/95w;->a:LX/1L9;

    if-eqz v0, :cond_0

    .line 1438045
    iget-object v0, p0, LX/95w;->a:LX/1L9;

    iget-object v1, p0, LX/95w;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-interface {v0, v1, p1}, LX/1L9;->a(Ljava/lang/Object;Lcom/facebook/fbservice/service/ServiceException;)V

    .line 1438046
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1438047
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 1438048
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1438049
    iget-object v1, p0, LX/95w;->a:LX/1L9;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 1438050
    iget-object v1, p0, LX/95w;->a:LX/1L9;

    invoke-interface {v1, v0}, LX/1L9;->b(Ljava/lang/Object;)V

    .line 1438051
    :cond_0
    return-void
.end method
