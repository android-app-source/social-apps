.class public LX/8tB;
.super LX/3Tf;
.source ""

# interfaces
.implements LX/3LJ;


# instance fields
.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/621",
            "<+",
            "LX/8QL;",
            ">;>;"
        }
    .end annotation
.end field

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/621",
            "<+",
            "LX/8QL;",
            ">;>;"
        }
    .end annotation
.end field

.field public final e:Landroid/view/inputmethod/InputMethodManager;

.field private f:LX/8tD;

.field private g:LX/8RE;

.field private h:LX/8vM;

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<+",
            "LX/8QL;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/inputmethod/InputMethodManager;LX/8vM;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1412507
    invoke-direct {p0}, LX/3Tf;-><init>()V

    .line 1412508
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/8tB;->c:Ljava/util/List;

    .line 1412509
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/8tB;->d:Ljava/util/List;

    .line 1412510
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/8tB;->i:Ljava/util/List;

    .line 1412511
    iput-object p1, p0, LX/8tB;->e:Landroid/view/inputmethod/InputMethodManager;

    .line 1412512
    iput-object p2, p0, LX/8tB;->h:LX/8vM;

    .line 1412513
    return-void
.end method

.method private a(LX/8vI;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1412599
    sget-object v0, LX/8vH;->b:[I

    invoke-virtual {p1}, LX/8vI;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1412600
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Could not find child view."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1412601
    :pswitch_0
    iget-object v0, p0, LX/8tB;->g:LX/8RE;

    invoke-interface {v0, p2}, LX/8RE;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1412602
    :goto_0
    return-object v0

    .line 1412603
    :pswitch_1
    iget-object v0, p0, LX/8tB;->g:LX/8RE;

    invoke-interface {v0, p2}, LX/8RE;->c(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 1412604
    :pswitch_2
    iget-object v0, p0, LX/8tB;->g:LX/8RE;

    invoke-interface {v0, p2}, LX/8RE;->b(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 1412605
    :pswitch_3
    iget-object v0, p0, LX/8tB;->g:LX/8RE;

    invoke-interface {v0, p2}, LX/8RE;->e(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private a(Landroid/view/View;LX/8vI;II)V
    .locals 3

    .prologue
    .line 1412588
    sget-object v0, LX/8vI;->ITEM:LX/8vI;

    if-ne p2, v0, :cond_1

    .line 1412589
    invoke-virtual {p0, p3, p4}, LX/8tB;->a(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QL;

    .line 1412590
    if-eqz v0, :cond_0

    .line 1412591
    iget-object v1, p0, LX/8tB;->g:LX/8RE;

    iget-object v2, p0, LX/8tB;->i:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    invoke-interface {v1, p1, v0, v2}, LX/8RE;->a(Landroid/view/View;LX/8QL;Z)V

    .line 1412592
    :cond_0
    :goto_0
    return-void

    .line 1412593
    :cond_1
    sget-object v0, LX/8vI;->VIEW_MORE:LX/8vI;

    if-ne p2, v0, :cond_2

    .line 1412594
    iget-object v0, p0, LX/8tB;->g:LX/8RE;

    new-instance v1, LX/8vF;

    invoke-direct {v1, p0, p3, p1}, LX/8vF;-><init>(LX/8tB;ILandroid/view/View;)V

    invoke-interface {v0, p1, v1}, LX/8RE;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 1412595
    :cond_2
    sget-object v0, LX/8vI;->SUBTITLED_ITEM:LX/8vI;

    if-ne p2, v0, :cond_0

    .line 1412596
    invoke-virtual {p0, p3, p4}, LX/8tB;->a(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QL;

    .line 1412597
    if-eqz v0, :cond_0

    .line 1412598
    iget-object v1, p0, LX/8tB;->g:LX/8RE;

    iget-object v2, p0, LX/8tB;->i:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    invoke-interface {v1, p1, v0, v2}, LX/8RE;->b(Landroid/view/View;LX/8QL;Z)V

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/8tB;
    .locals 3

    .prologue
    .line 1412586
    new-instance v2, LX/8tB;

    invoke-static {p0}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    const-class v1, LX/8vM;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/8vM;

    invoke-direct {v2, v0, v1}, LX/8tB;-><init>(Landroid/view/inputmethod/InputMethodManager;LX/8vM;)V

    .line 1412587
    return-object v2
.end method


# virtual methods
.method public final a(I)I
    .locals 1

    .prologue
    .line 1412585
    sget-object v0, LX/8vI;->HEADER:LX/8vI;

    invoke-virtual {v0}, LX/8vI;->ordinal()I

    move-result v0

    return v0
.end method

.method public a()LX/333;
    .locals 1

    .prologue
    .line 1412584
    iget-object v0, p0, LX/8tB;->f:LX/8tD;

    return-object v0
.end method

.method public a(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1412576
    invoke-virtual {p0, p1, p2}, LX/8tB;->c(II)I

    move-result v0

    .line 1412577
    invoke-static {}, LX/8vI;->values()[LX/8vI;

    move-result-object v1

    aget-object v1, v1, v0

    .line 1412578
    if-nez p4, :cond_0

    .line 1412579
    invoke-direct {p0, v1, p5}, LX/8tB;->a(LX/8vI;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1412580
    :goto_0
    :try_start_0
    invoke-direct {p0, v0, v1, p1, p2}, LX/8tB;->a(Landroid/view/View;LX/8vI;II)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1412581
    :goto_1
    return-object v0

    .line 1412582
    :catch_0
    invoke-direct {p0, v1, p5}, LX/8tB;->a(LX/8vI;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1412583
    invoke-direct {p0, v0, v1, p1, p2}, LX/8tB;->a(Landroid/view/View;LX/8vI;II)V

    goto :goto_1

    :cond_0
    move-object v0, p4

    goto :goto_0
.end method

.method public a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1412571
    if-nez p2, :cond_0

    .line 1412572
    iget-object v0, p0, LX/8tB;->g:LX/8RE;

    invoke-interface {v0, p3}, LX/8RE;->d(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 1412573
    :cond_0
    invoke-virtual {p0, p1}, LX/8tB;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/621;

    .line 1412574
    iget-object v1, p0, LX/8tB;->g:LX/8RE;

    invoke-interface {v1, p2, v0}, LX/8RE;->a(Landroid/view/View;LX/621;)V

    .line 1412575
    return-object p2
.end method

.method public final a(II)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1412568
    iget-object v0, p0, LX/8tB;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/621;

    invoke-interface {v0}, LX/621;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p2, v0, :cond_0

    .line 1412569
    iget-object v0, p0, LX/8tB;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/621;

    invoke-interface {v0}, LX/621;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 1412570
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(ILX/621;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/621",
            "<+",
            "LX/8QL;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1412564
    iget-object v0, p0, LX/8tB;->c:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1412565
    iget-object v0, p0, LX/8tB;->c:Ljava/util/List;

    iput-object v0, p0, LX/8tB;->d:Ljava/util/List;

    .line 1412566
    const v0, 0xdbdf0ca

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1412567
    return-void
.end method

.method public final a(LX/8RK;)V
    .locals 3

    .prologue
    .line 1412561
    const/4 v0, 0x1

    .line 1412562
    new-instance v1, LX/8RF;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, LX/8RF;-><init>(Z)V

    invoke-virtual {p0, p1, v1, v0}, LX/8tB;->a(LX/8RK;LX/8RE;Z)V

    .line 1412563
    return-void
.end method

.method public a(LX/8RK;LX/8RE;)V
    .locals 1

    .prologue
    .line 1412559
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, LX/8tB;->a(LX/8RK;LX/8RE;Z)V

    .line 1412560
    return-void
.end method

.method public final a(LX/8RK;LX/8RE;Z)V
    .locals 3

    .prologue
    .line 1412553
    iput-object p2, p0, LX/8tB;->g:LX/8RE;

    .line 1412554
    iget-object v0, p0, LX/8tB;->h:LX/8vM;

    invoke-virtual {p0}, LX/8tB;->e()LX/8vE;

    move-result-object v1

    .line 1412555
    new-instance p2, LX/8tD;

    invoke-static {v0}, LX/0Zr;->a(LX/0QB;)LX/0Zr;

    move-result-object v2

    check-cast v2, LX/0Zr;

    invoke-direct {p2, v2, p1, v1, p3}, LX/8tD;-><init>(LX/0Zr;LX/8RK;LX/8vE;Z)V

    .line 1412556
    move-object v0, p2

    .line 1412557
    iput-object v0, p0, LX/8tB;->f:LX/8tD;

    .line 1412558
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "LX/621",
            "<+",
            "LX/8QL;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1412550
    invoke-static {p1}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/8tB;->d:Ljava/util/List;

    iput-object v0, p0, LX/8tB;->c:Ljava/util/List;

    .line 1412551
    const v0, -0x4d06b54d

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1412552
    return-void
.end method

.method public final areAllItemsEnabled()Z
    .locals 2

    .prologue
    .line 1412546
    sget-object v0, LX/8vH;->a:[I

    iget-object v1, p0, LX/8tB;->g:LX/8RE;

    invoke-interface {v1}, LX/8RE;->a()LX/03R;

    move-result-object v1

    invoke-virtual {v1}, LX/03R;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1412547
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1412548
    :pswitch_0
    invoke-super {p0}, LX/3Tf;->areAllItemsEnabled()Z

    move-result v0

    goto :goto_0

    .line 1412549
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1412545
    iget-object v0, p0, LX/8tB;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1412544
    iget-object v0, p0, LX/8tB;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final b(II)Z
    .locals 3

    .prologue
    .line 1412538
    invoke-virtual {p0, p1}, LX/8tB;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/621;

    .line 1412539
    invoke-interface {v0}, LX/621;->b()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge p2, v1, :cond_0

    .line 1412540
    invoke-virtual {p0, p1, p2}, LX/8tB;->a(II)Ljava/lang/Object;

    move-result-object v1

    .line 1412541
    instance-of v2, v1, LX/8QL;

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 1412542
    check-cast v0, LX/8QL;

    invoke-virtual {v0}, LX/8QK;->a()Z

    move-result v0

    .line 1412543
    :goto_0
    return v0

    :cond_0
    invoke-interface {v0}, LX/621;->f()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 1412537
    iget-object v0, p0, LX/8tB;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final c(I)I
    .locals 2

    .prologue
    .line 1412533
    iget-object v0, p0, LX/8tB;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/621;

    .line 1412534
    invoke-interface {v0}, LX/621;->c()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/621;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1412535
    :cond_0
    invoke-interface {v0}, LX/621;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 1412536
    :goto_0
    return v0

    :cond_1
    invoke-interface {v0}, LX/621;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public final c(II)I
    .locals 3

    .prologue
    .line 1412523
    invoke-virtual {p0, p1}, LX/8tB;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/621;

    .line 1412524
    invoke-virtual {p0, p1, p2}, LX/8tB;->a(II)Ljava/lang/Object;

    move-result-object v1

    .line 1412525
    invoke-interface {v0}, LX/621;->f()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, LX/621;->b()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1412526
    sget-object v0, LX/8vI;->LOADING_SECTION:LX/8vI;

    invoke-virtual {v0}, LX/8vI;->ordinal()I

    move-result v0

    .line 1412527
    :goto_0
    return v0

    .line 1412528
    :cond_0
    invoke-interface {v0}, LX/621;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, LX/621;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne p2, v0, :cond_1

    .line 1412529
    sget-object v0, LX/8vI;->VIEW_MORE:LX/8vI;

    invoke-virtual {v0}, LX/8vI;->ordinal()I

    move-result v0

    goto :goto_0

    .line 1412530
    :cond_1
    instance-of v0, v1, LX/8QL;

    if-eqz v0, :cond_3

    move-object v0, v1

    check-cast v0, LX/8QL;

    invoke-virtual {v0}, LX/8QL;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    check-cast v1, LX/8QL;

    invoke-virtual {v1}, LX/8QL;->l()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1412531
    :cond_2
    sget-object v0, LX/8vI;->SUBTITLED_ITEM:LX/8vI;

    invoke-virtual {v0}, LX/8vI;->ordinal()I

    move-result v0

    goto :goto_0

    .line 1412532
    :cond_3
    sget-object v0, LX/8vI;->ITEM:LX/8vI;

    invoke-virtual {v0}, LX/8vI;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final e()LX/8vE;
    .locals 1

    .prologue
    .line 1412522
    new-instance v0, LX/8vE;

    invoke-direct {v0, p0}, LX/8vE;-><init>(LX/8tB;)V

    return-object v0
.end method

.method public final f()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1412517
    move v1, v0

    .line 1412518
    :goto_0
    invoke-virtual {p0}, LX/8tB;->c()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 1412519
    invoke-virtual {p0, v0}, LX/8tB;->c(I)I

    move-result v2

    add-int/2addr v1, v2

    .line 1412520
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1412521
    :cond_0
    return v1
.end method

.method public final g(I)LX/621;
    .locals 1

    .prologue
    .line 1412516
    iget-object v0, p0, LX/8tB;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/621;

    return-object v0
.end method

.method public final getFilter()Landroid/widget/Filter;
    .locals 1

    .prologue
    .line 1412515
    new-instance v0, LX/8vG;

    invoke-direct {v0, p0}, LX/8vG;-><init>(LX/8tB;)V

    return-object v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 1412514
    invoke-static {}, LX/8vI;->values()[LX/8vI;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
