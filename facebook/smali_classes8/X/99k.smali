.class public final LX/99k;
.super LX/2eF;
.source ""


# instance fields
.field private final b:F

.field private final c:Z

.field private final d:Z

.field private e:I

.field private f:I


# direct methods
.method public constructor <init>(FZZ)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1447624
    invoke-direct {p0}, LX/2eF;-><init>()V

    .line 1447625
    iput p1, p0, LX/99k;->b:F

    .line 1447626
    iput-boolean p2, p0, LX/99k;->c:Z

    .line 1447627
    iput-boolean p3, p0, LX/99k;->d:Z

    .line 1447628
    iput v0, p0, LX/99k;->e:I

    .line 1447629
    iput v0, p0, LX/99k;->f:I

    .line 1447630
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1447596
    iget v0, p0, LX/99k;->f:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1447597
    const/high16 v0, 0x41000000    # 8.0f

    invoke-static {p1, v0}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/99k;->f:I

    .line 1447598
    :cond_0
    return-void
.end method

.method private b(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1447599
    iget v0, p0, LX/99k;->e:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1447600
    iget v0, p0, LX/99k;->b:F

    invoke-static {p1, v0}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, LX/99k;->e:I

    .line 1447601
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 1447602
    iget v0, p0, LX/99k;->e:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1447603
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "getPageWidthPixelsForRecyclerView() must be called after applyToRecyclerView()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1447604
    :cond_0
    iget v0, p0, LX/99k;->e:I

    return v0
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    .prologue
    .line 1447605
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, LX/99k;->b(Landroid/content/Context;)V

    .line 1447606
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, LX/99k;->a(Landroid/content/Context;)V

    .line 1447607
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1447608
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, LX/99k;->b(Landroid/content/Context;)V

    .line 1447609
    iget-boolean v0, p0, LX/99k;->d:Z

    if-eqz v0, :cond_1

    .line 1447610
    iget v0, p0, LX/99k;->e:I

    invoke-static {p1, v0}, LX/2eF;->a(Landroid/view/View;I)V

    .line 1447611
    :cond_0
    :goto_0
    return-void

    .line 1447612
    :cond_1
    instance-of v0, p1, LX/2fC;

    if-eqz v0, :cond_0

    .line 1447613
    check-cast p1, LX/2fC;

    iget v0, p0, LX/99k;->e:I

    invoke-interface {p1, v0}, LX/2fC;->setWidth(I)V

    goto :goto_0
.end method

.method public final b(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1447614
    instance-of v0, p1, Landroid/widget/FrameLayout;

    if-nez v0, :cond_0

    .line 1447615
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This style requires an extra wrapping FrameLayout to work"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1447616
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, LX/99k;->b(Landroid/content/Context;)V

    .line 1447617
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, LX/99k;->a(Landroid/content/Context;)V

    move-object v0, p1

    .line 1447618
    check-cast v0, Landroid/widget/FrameLayout;

    .line 1447619
    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1447620
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1447621
    instance-of v0, p1, LX/2fC;

    if-eqz v0, :cond_1

    .line 1447622
    check-cast p1, LX/2fC;

    iget v0, p0, LX/99k;->e:I

    const/4 v1, 0x1

    iget-boolean v2, p0, LX/99k;->c:Z

    invoke-interface {p1, v0, v1, v2}, LX/2fC;->a(IZZ)V

    .line 1447623
    :cond_1
    return-void
.end method
