.class public final LX/9Em;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:[LX/9Bw;

.field public final synthetic b:Lcom/facebook/ufiservices/flyout/FeedbackParams;

.field public final synthetic c:Lcom/facebook/feedback/ui/SingletonFeedbackController;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/SingletonFeedbackController;[LX/9Bw;Lcom/facebook/ufiservices/flyout/FeedbackParams;)V
    .locals 0

    .prologue
    .line 1457361
    iput-object p1, p0, LX/9Em;->c:Lcom/facebook/feedback/ui/SingletonFeedbackController;

    iput-object p2, p0, LX/9Em;->a:[LX/9Bw;

    iput-object p3, p0, LX/9Em;->b:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 1457362
    invoke-static {p1}, Lcom/facebook/fbservice/service/ServiceException;->forException(Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/ServiceException;

    move-result-object v1

    .line 1457363
    iget-object v2, p0, LX/9Em;->a:[LX/9Bw;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 1457364
    iget-object v5, p0, LX/9Em;->b:Lcom/facebook/ufiservices/flyout/FeedbackParams;

    invoke-interface {v4, v1, v5}, LX/9Bw;->a(Lcom/facebook/fbservice/service/ServiceException;Lcom/facebook/ufiservices/flyout/FeedbackParams;)V

    .line 1457365
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1457366
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1457367
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1457368
    if-nez p1, :cond_1

    .line 1457369
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Fetched feedback was non-existent"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/9Em;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    .line 1457370
    :cond_0
    return-void

    .line 1457371
    :cond_1
    iget-object v1, p0, LX/9Em;->a:[LX/9Bw;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 1457372
    invoke-interface {v3, p1}, LX/9Bw;->b(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1457373
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
