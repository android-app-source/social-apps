.class public LX/AEd;
.super LX/2y5;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1PW;",
        ">",
        "LX/2y5",
        "<TE;>;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static l:LX/0Xm;


# instance fields
.field public final b:LX/0bH;

.field public final c:I

.field public final d:Landroid/content/res/ColorStateList;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:LX/03V;

.field public final h:I

.field private final i:LX/1Nt;

.field private final j:LX/2yS;

.field private final k:LX/2yT;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1646889
    const-class v0, LX/AEd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AEd;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0bH;Landroid/content/res/Resources;LX/03V;LX/2yS;LX/2yT;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1646890
    invoke-direct {p0}, LX/2y5;-><init>()V

    .line 1646891
    iput-object p1, p0, LX/AEd;->b:LX/0bH;

    .line 1646892
    const v0, 0x7f020abd

    iput v0, p0, LX/AEd;->c:I

    .line 1646893
    const v0, 0x7f0a0a29

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, LX/AEd;->d:Landroid/content/res/ColorStateList;

    .line 1646894
    const v0, 0x7f08103f

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/AEd;->e:Ljava/lang/String;

    .line 1646895
    const v0, 0x7f081040

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/AEd;->f:Ljava/lang/String;

    .line 1646896
    iput-object p3, p0, LX/AEd;->g:LX/03V;

    .line 1646897
    const v0, 0x7f0b00f0

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/AEd;->h:I

    .line 1646898
    iput-object p4, p0, LX/AEd;->j:LX/2yS;

    .line 1646899
    iput-object p5, p0, LX/AEd;->k:LX/2yT;

    .line 1646900
    new-instance v0, Lcom/facebook/attachments/angora/actionbutton/LikePageActionButton$LikePageActionButtonPartDefinition;

    invoke-direct {v0, p0}, Lcom/facebook/attachments/angora/actionbutton/LikePageActionButton$LikePageActionButtonPartDefinition;-><init>(LX/AEd;)V

    iput-object v0, p0, LX/AEd;->i:LX/1Nt;

    .line 1646901
    return-void
.end method

.method public static a(LX/0QB;)LX/AEd;
    .locals 9

    .prologue
    .line 1646878
    const-class v1, LX/AEd;

    monitor-enter v1

    .line 1646879
    :try_start_0
    sget-object v0, LX/AEd;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1646880
    sput-object v2, LX/AEd;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1646881
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1646882
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1646883
    new-instance v3, LX/AEd;

    invoke-static {v0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v4

    check-cast v4, LX/0bH;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {v0}, LX/2yS;->a(LX/0QB;)LX/2yS;

    move-result-object v7

    check-cast v7, LX/2yS;

    const-class v8, LX/2yT;

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/2yT;

    invoke-direct/range {v3 .. v8}, LX/AEd;-><init>(LX/0bH;Landroid/content/res/Resources;LX/03V;LX/2yS;LX/2yT;)V

    .line 1646884
    move-object v0, v3

    .line 1646885
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1646886
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/AEd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1646887
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1646888
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/AEd;Lcom/facebook/feed/rows/core/props/FeedProps;)Landroid/view/View$OnClickListener;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation

    .prologue
    .line 1646877
    new-instance v0, LX/AEb;

    invoke-direct {v0, p0, p1}, LX/AEb;-><init>(LX/AEd;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    return-object v0
.end method


# virtual methods
.method public final a()LX/1Nt;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Landroid/view/View;",
            ":",
            "LX/35p;",
            ">()",
            "LX/1Nt",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;*TE;TV;>;"
        }
    .end annotation

    .prologue
    .line 1646855
    iget-object v0, p0, LX/AEd;->i:LX/1Nt;

    return-object v0
.end method

.method public final a(LX/1De;LX/1PW;Lcom/facebook/feed/rows/core/props/FeedProps;Z)LX/AE0;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "TE;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;Z)",
            "LX/AE0;"
        }
    .end annotation

    .prologue
    .line 1646856
    iget-object v0, p3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1646857
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1646858
    invoke-static {v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 1646859
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1646860
    const/4 v0, 0x0

    .line 1646861
    :goto_0
    return-object v0

    .line 1646862
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->w()Z

    move-result v1

    .line 1646863
    if-eqz v1, :cond_1

    const v0, 0x7f0a00d2

    .line 1646864
    :goto_1
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 1646865
    iget-object v2, p0, LX/AEd;->k:LX/2yT;

    const/4 v3, 0x3

    iget-object v4, p0, LX/AEd;->j:LX/2yS;

    invoke-virtual {v2, p4, v3, v4}, LX/2yT;->a(ZILX/2yS;)LX/AE0;

    move-result-object v2

    iget v3, p0, LX/AEd;->c:I

    invoke-virtual {v2, v3}, LX/AE0;->a(I)LX/AE0;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/AE0;->d(I)LX/AE0;

    move-result-object v0

    const/4 v2, 0x1

    .line 1646866
    iput-boolean v2, v0, LX/AE0;->l:Z

    .line 1646867
    move-object v0, v0

    .line 1646868
    const v2, 0x7f020a8d

    invoke-virtual {v0, v2}, LX/AE0;->b(I)LX/AE0;

    move-result-object v0

    invoke-static {p0, p3}, LX/AEd;->a$redex0(LX/AEd;Lcom/facebook/feed/rows/core/props/FeedProps;)Landroid/view/View$OnClickListener;

    move-result-object v2

    .line 1646869
    iput-object v2, v0, LX/AE0;->o:Landroid/view/View$OnClickListener;

    .line 1646870
    move-object v0, v0

    .line 1646871
    const v2, 0x7f0b00f0

    invoke-virtual {v0, v2}, LX/AE0;->e(I)LX/AE0;

    move-result-object v2

    if-eqz v1, :cond_2

    iget-object v0, p0, LX/AEd;->e:Ljava/lang/String;

    .line 1646872
    :goto_2
    iput-object v0, v2, LX/AE0;->g:Ljava/lang/CharSequence;

    .line 1646873
    move-object v0, v2

    .line 1646874
    goto :goto_0

    .line 1646875
    :cond_1
    const v0, 0x7f0a00e7

    goto :goto_1

    .line 1646876
    :cond_2
    iget-object v0, p0, LX/AEd;->f:Ljava/lang/String;

    goto :goto_2
.end method
