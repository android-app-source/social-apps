.class public final LX/A5d;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;)V
    .locals 0

    .prologue
    .line 1621637
    iput-object p1, p0, LX/A5d;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1621638
    iget-object v0, p0, LX/A5d;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    iget-object v0, v0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->P:LX/8tC;

    invoke-virtual {v0, p3}, LX/3Tf;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QL;

    .line 1621639
    if-nez v0, :cond_0

    .line 1621640
    iget-object v0, p0, LX/A5d;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    iget-object v0, v0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->w:LX/03V;

    const-string v1, "FriendSuggestionsAndSelectorFragment: null token"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Returned by getItem("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "), row id("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1621641
    :goto_0
    return-void

    .line 1621642
    :cond_0
    instance-of v1, v0, LX/8vB;

    if-eqz v1, :cond_4

    .line 1621643
    iget-object v2, p0, LX/A5d;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    move-object v1, v0

    check-cast v1, LX/8vB;

    .line 1621644
    const/4 v3, 0x1

    .line 1621645
    iget-object p1, v1, LX/8vB;->e:Ljava/util/List;

    move-object p1, p1

    .line 1621646
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    move p1, v3

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 1621647
    invoke-static {v2, v3}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->d(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;LX/8QL;)I

    move-result p4

    if-nez p4, :cond_1

    .line 1621648
    const/4 p1, 0x0

    .line 1621649
    iget-object p4, v2, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->Q:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-static {v2, v3, p4}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->a$redex0(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;LX/8QL;Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)V

    :cond_1
    move v3, p1

    move p1, v3

    .line 1621650
    goto :goto_1

    .line 1621651
    :cond_2
    if-eqz p1, :cond_3

    .line 1621652
    iget-object v3, v1, LX/8vB;->e:Ljava/util/List;

    move-object v3, v3

    .line 1621653
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 1621654
    iget-object p2, v2, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->Q:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-static {v2, v3, p2}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->a$redex0(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;LX/8QL;Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)V

    goto :goto_2

    .line 1621655
    :cond_3
    iget-object v1, p0, LX/A5d;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    iget-object v1, v1, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->C:LX/A5T;

    check-cast v0, LX/8vB;

    add-int/lit8 v2, p3, -0x1

    iget-object v3, p0, LX/A5d;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    iget-object v3, v3, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->W:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    .line 1621656
    iget-object p1, v3, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->k:Ljava/lang/String;

    move-object v3, p1

    .line 1621657
    iget-object p1, v1, LX/A5T;->a:LX/0Zb;

    const-string p2, "group_tag_suggestion_clicked"

    .line 1621658
    iget-object p3, v0, LX/8vB;->f:Ljava/lang/String;

    move-object p3, p3

    .line 1621659
    invoke-static {p2, p3, v3}, LX/A5T;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p2

    const-string p3, "index_clicked"

    invoke-virtual {p2, p3, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p2

    invoke-interface {p1, p2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1621660
    :goto_3
    iget-object v0, p0, LX/A5d;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    invoke-static {v0}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->n(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;)V

    goto :goto_0

    .line 1621661
    :cond_4
    iget-object v1, p0, LX/A5d;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    iget-object v2, p0, LX/A5d;->a:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    iget-object v2, v2, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->Q:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-static {v1, v0, v2}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->a$redex0(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;LX/8QL;Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)V

    goto :goto_3
.end method
