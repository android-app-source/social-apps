.class public final LX/AJM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/93X;


# instance fields
.field public final synthetic a:LX/AJQ;


# direct methods
.method public constructor <init>(LX/AJQ;)V
    .locals 0

    .prologue
    .line 1661056
    iput-object p1, p0, LX/AJM;->a:LX/AJQ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/privacy/model/SelectablePrivacyData;)V
    .locals 3

    .prologue
    .line 1661057
    iget-object v0, p0, LX/AJM;->a:LX/AJQ;

    invoke-virtual {v0, p1}, LX/AJQ;->a(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    .line 1661058
    iget-object v0, p0, LX/AJM;->a:LX/AJQ;

    iget-object v0, v0, LX/AJQ;->b:LX/AJA;

    iget-object v1, p0, LX/AJM;->a:LX/AJQ;

    iget-object v1, v1, LX/AJQ;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-object v1, v1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-virtual {v0, v1}, LX/AJA;->a(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    .line 1661059
    iget-object v0, p0, LX/AJM;->a:LX/AJQ;

    iget-object v0, v0, LX/AJQ;->f:LX/7gT;

    .line 1661060
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1661061
    invoke-static {v0, v1}, LX/7gT;->a(LX/7gT;Landroid/os/Bundle;)V

    .line 1661062
    sget-object v2, LX/7gS;->PREVIOUS_PRIVACY:LX/7gS;

    invoke-virtual {v2}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object p1, v0, LX/7gT;->i:Ljava/lang/String;

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1661063
    sget-object v2, LX/7gS;->CURRENT_PRIVACY:LX/7gS;

    invoke-virtual {v2}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object p1, v0, LX/7gT;->j:Ljava/lang/String;

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1661064
    sget-object v2, LX/7gR;->MODIFY_NEWS_FEED_PRIVACY:LX/7gR;

    invoke-static {v0, v2, v1}, LX/7gT;->a(LX/7gT;LX/7gR;Landroid/os/Bundle;)V

    .line 1661065
    iget-object v0, p0, LX/AJM;->a:LX/AJQ;

    iget-object v0, v0, LX/AJQ;->g:LX/AJK;

    .line 1661066
    iget-boolean v1, v0, LX/AJK;->a:Z

    if-nez v1, :cond_0

    .line 1661067
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/AJK;->a:Z

    .line 1661068
    :cond_0
    return-void
.end method
