.class public final LX/A6a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:I

.field public final synthetic e:LX/A6e;


# direct methods
.method public constructor <init>(LX/A6e;ILjava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 1624523
    iput-object p1, p0, LX/A6a;->e:LX/A6e;

    iput p2, p0, LX/A6a;->a:I

    iput-object p3, p0, LX/A6a;->b:Ljava/lang/String;

    iput-object p4, p0, LX/A6a;->c:Ljava/lang/String;

    iput p5, p0, LX/A6a;->d:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 13

    .prologue
    .line 1624524
    iget-object v0, p0, LX/A6a;->e:LX/A6e;

    iget v1, p0, LX/A6a;->a:I

    iget-object v2, p0, LX/A6a;->b:Ljava/lang/String;

    iget-object v3, p0, LX/A6a;->c:Ljava/lang/String;

    iget v4, p0, LX/A6a;->d:I

    const/4 v12, 0x1

    .line 1624525
    iget-object v5, v0, LX/A6e;->k:LX/A77;

    invoke-virtual {v5}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    .line 1624526
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 1624527
    sget-object v5, LX/A74;->b:LX/0U1;

    .line 1624528
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 1624529
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v8, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1624530
    sget-object v5, LX/A74;->c:LX/0U1;

    .line 1624531
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 1624532
    invoke-virtual {v8, v5, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1624533
    sget-object v5, LX/A74;->d:LX/0U1;

    .line 1624534
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 1624535
    invoke-virtual {v8, v5, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1624536
    const-wide/16 v5, 0x0

    .line 1624537
    if-ne v4, v12, :cond_1

    .line 1624538
    const-string v5, "dictionary_table"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, LX/A74;->b:LX/0U1;

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, "=? AND "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v9, LX/A74;->c:LX/0U1;

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, "=?"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    aput-object v2, v9, v12

    invoke-virtual {v7, v5, v8, v6, v9}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    int-to-long v5, v5

    .line 1624539
    :cond_0
    :goto_0
    move-wide v0, v5

    .line 1624540
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0

    .line 1624541
    :cond_1
    if-nez v4, :cond_0

    .line 1624542
    const-string v5, "dictionary_table"

    const/4 v6, 0x0

    const v9, 0x36cbb5c

    invoke-static {v9}, LX/03h;->a(I)V

    invoke-virtual {v7, v5, v6, v8}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v5

    const v7, -0x12a34433

    invoke-static {v7}, LX/03h;->a(I)V

    goto :goto_0
.end method
