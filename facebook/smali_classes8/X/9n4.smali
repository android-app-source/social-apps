.class public final LX/9n4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/Choreographer$FrameCallback;


# instance fields
.field public final synthetic a:LX/9n7;


# direct methods
.method public constructor <init>(LX/9n7;)V
    .locals 0

    .prologue
    .line 1536654
    iput-object p1, p0, LX/9n4;->a:LX/9n7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(LX/9n7;B)V
    .locals 0

    .prologue
    .line 1536655
    invoke-direct {p0, p1}, LX/9n4;-><init>(LX/9n7;)V

    return-void
.end method


# virtual methods
.method public final doFrame(J)V
    .locals 3

    .prologue
    .line 1536656
    iget-object v0, p0, LX/9n4;->a:LX/9n7;

    iget-object v0, v0, LX/9n7;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9n4;->a:LX/9n7;

    iget-object v0, v0, LX/9n7;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1536657
    :goto_0
    return-void

    .line 1536658
    :cond_0
    iget-object v0, p0, LX/9n4;->a:LX/9n7;

    iget-object v0, v0, LX/9n7;->j:Lcom/facebook/react/modules/core/Timing$IdleCallbackRunnable;

    if-eqz v0, :cond_1

    .line 1536659
    iget-object v0, p0, LX/9n4;->a:LX/9n7;

    iget-object v0, v0, LX/9n7;->j:Lcom/facebook/react/modules/core/Timing$IdleCallbackRunnable;

    invoke-virtual {v0}, Lcom/facebook/react/modules/core/Timing$IdleCallbackRunnable;->a()V

    .line 1536660
    :cond_1
    iget-object v0, p0, LX/9n4;->a:LX/9n7;

    new-instance v1, Lcom/facebook/react/modules/core/Timing$IdleCallbackRunnable;

    iget-object v2, p0, LX/9n4;->a:LX/9n7;

    invoke-direct {v1, v2, p1, p2}, Lcom/facebook/react/modules/core/Timing$IdleCallbackRunnable;-><init>(LX/9n7;J)V

    .line 1536661
    iput-object v1, v0, LX/9n7;->j:Lcom/facebook/react/modules/core/Timing$IdleCallbackRunnable;

    .line 1536662
    iget-object v0, p0, LX/9n4;->a:LX/9n7;

    invoke-static {v0}, LX/9n7;->i(LX/9n7;)LX/5pY;

    move-result-object v0

    iget-object v1, p0, LX/9n4;->a:LX/9n7;

    iget-object v1, v1, LX/9n7;->j:Lcom/facebook/react/modules/core/Timing$IdleCallbackRunnable;

    invoke-virtual {v0, v1}, LX/5pX;->c(Ljava/lang/Runnable;)V

    .line 1536663
    iget-object v0, p0, LX/9n4;->a:LX/9n7;

    iget-object v0, v0, LX/9n7;->k:LX/5r6;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5r6;

    sget-object v1, LX/5r4;->IDLE_EVENT:LX/5r4;

    invoke-virtual {v0, v1, p0}, LX/5r6;->a(LX/5r4;Landroid/view/Choreographer$FrameCallback;)V

    goto :goto_0
.end method
