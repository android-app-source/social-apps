.class public final LX/9cW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/9cX;

.field public final synthetic b:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic c:LX/9cZ;


# direct methods
.method public constructor <init>(LX/9cZ;LX/9cX;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0

    .prologue
    .line 1516563
    iput-object p1, p0, LX/9cW;->c:LX/9cZ;

    iput-object p2, p0, LX/9cW;->a:LX/9cX;

    iput-object p3, p0, LX/9cW;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1516545
    iget-object v0, p0, LX/9cW;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 1516546
    iget-object v0, p0, LX/9cW;->c:LX/9cZ;

    iget-object v0, v0, LX/9cZ;->e:LX/3Mb;

    iget-object v1, p0, LX/9cW;->a:LX/9cX;

    invoke-interface {v0, v1, p1}, LX/3Mb;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1516547
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 1516548
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    const/4 v3, 0x0

    .line 1516549
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/service/FetchStickerPacksResult;

    .line 1516550
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v4

    .line 1516551
    iget-object v1, v0, Lcom/facebook/stickers/service/FetchStickerPacksResult;->b:LX/0am;

    move-object v1, v1

    .line 1516552
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1516553
    iget-object v1, v0, Lcom/facebook/stickers/service/FetchStickerPacksResult;->b:LX/0am;

    move-object v0, v1

    .line 1516554
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v5

    move v2, v3

    :goto_0
    if-ge v2, v5, :cond_1

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/stickers/model/StickerPack;

    .line 1516555
    iget-object v6, v1, Lcom/facebook/stickers/model/StickerPack;->r:Lcom/facebook/stickers/model/StickerCapabilities;

    move-object v6, v6

    .line 1516556
    iget-object v6, v6, Lcom/facebook/stickers/model/StickerCapabilities;->b:LX/03R;

    invoke-virtual {v6, v3}, LX/03R;->asBoolean(Z)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1516557
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1516558
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1516559
    :cond_1
    new-instance v0, LX/9cY;

    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/9cY;-><init>(LX/0Px;)V

    .line 1516560
    iget-object v1, p0, LX/9cW;->c:LX/9cZ;

    iget-object v1, v1, LX/9cZ;->e:LX/3Mb;

    iget-object v2, p0, LX/9cW;->a:LX/9cX;

    invoke-interface {v1, v2, v0}, LX/3Mb;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1516561
    iget-object v1, p0, LX/9cW;->b:Lcom/google/common/util/concurrent/SettableFuture;

    const v2, -0x621f995e

    invoke-static {v1, v0, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 1516562
    return-void
.end method
