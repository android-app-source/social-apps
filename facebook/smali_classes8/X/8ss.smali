.class public LX/8ss;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8sj;


# instance fields
.field private final a:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

.field private final b:J

.field private final c:F

.field private final d:F

.field private final e:Landroid/animation/Animator$AnimatorListener;


# direct methods
.method public constructor <init>(Landroid/animation/ValueAnimator$AnimatorUpdateListener;JFFLandroid/animation/Animator$AnimatorListener;)V
    .locals 0

    .prologue
    .line 1411872
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1411873
    iput-object p1, p0, LX/8ss;->a:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    .line 1411874
    iput-wide p2, p0, LX/8ss;->b:J

    .line 1411875
    iput p4, p0, LX/8ss;->c:F

    .line 1411876
    iput p5, p0, LX/8ss;->d:F

    .line 1411877
    iput-object p6, p0, LX/8ss;->e:Landroid/animation/Animator$AnimatorListener;

    .line 1411878
    return-void
.end method

.method private b(F)F
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1411886
    iget v0, p0, LX/8ss;->c:F

    iget v1, p0, LX/8ss;->d:F

    iget v2, p0, LX/8ss;->c:F

    sub-float/2addr v1, v2

    mul-float/2addr v1, p1

    add-float/2addr v0, v1

    return v0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 1411887
    iget-wide v0, p0, LX/8ss;->b:J

    return-wide v0
.end method

.method public final a(F)Landroid/animation/Animator;
    .locals 4

    .prologue
    .line 1411879
    invoke-direct {p0, p1}, LX/8ss;->b(F)F

    move-result v0

    .line 1411880
    const/4 v1, 0x2

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput v0, v1, v2

    const/4 v0, 0x1

    iget v2, p0, LX/8ss;->d:F

    aput v2, v1, v0

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 1411881
    iget-object v1, p0, LX/8ss;->a:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1411882
    iget-object v1, p0, LX/8ss;->e:Landroid/animation/Animator$AnimatorListener;

    if-eqz v1, :cond_0

    .line 1411883
    iget-object v1, p0, LX/8ss;->e:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1411884
    :cond_0
    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, p1

    iget-wide v2, p0, LX/8ss;->b:J

    long-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1411885
    return-object v0
.end method
