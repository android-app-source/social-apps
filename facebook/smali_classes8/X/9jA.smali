.class public LX/9jA;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Tn;

.field private final b:LX/0Tn;

.field public c:LX/9jB;

.field public d:LX/1e2;

.field private e:Lcom/facebook/common/perftest/PerfTestConfig;


# direct methods
.method public constructor <init>(LX/9jB;LX/1e2;Lcom/facebook/common/perftest/PerfTestConfig;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1529236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1529237
    sget-object v0, LX/9jz;->a:LX/0Tn;

    const-string v1, "place_to_people_time_of_last_skip"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/9jA;->a:LX/0Tn;

    .line 1529238
    sget-object v0, LX/9jz;->a:LX/0Tn;

    const-string v1, "place_to_people_consecutive_num_times_skipped"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/9jA;->b:LX/0Tn;

    .line 1529239
    iput-object p1, p0, LX/9jA;->c:LX/9jB;

    .line 1529240
    iput-object p2, p0, LX/9jA;->d:LX/1e2;

    .line 1529241
    iput-object p3, p0, LX/9jA;->e:Lcom/facebook/common/perftest/PerfTestConfig;

    .line 1529242
    iget-object v0, p0, LX/9jA;->c:LX/9jB;

    iget-object v1, p0, LX/9jA;->b:LX/0Tn;

    iget-object v2, p0, LX/9jA;->a:LX/0Tn;

    invoke-virtual {v0, v1, v2}, LX/9jB;->a(LX/0Tn;LX/0Tn;)V

    .line 1529243
    return-void
.end method

.method public static b(LX/0QB;)LX/9jA;
    .locals 4

    .prologue
    .line 1529244
    new-instance v3, LX/9jA;

    invoke-static {p0}, LX/9jB;->b(LX/0QB;)LX/9jB;

    move-result-object v0

    check-cast v0, LX/9jB;

    invoke-static {p0}, LX/1e2;->b(LX/0QB;)LX/1e2;

    move-result-object v1

    check-cast v1, LX/1e2;

    invoke-static {p0}, Lcom/facebook/common/perftest/PerfTestConfig;->a(LX/0QB;)Lcom/facebook/common/perftest/PerfTestConfig;

    move-result-object v2

    check-cast v2, Lcom/facebook/common/perftest/PerfTestConfig;

    invoke-direct {v3, v0, v1, v2}, LX/9jA;-><init>(LX/9jB;LX/1e2;Lcom/facebook/common/perftest/PerfTestConfig;)V

    .line 1529245
    return-object v3
.end method
