.class public LX/8uF;
.super LX/8uC;
.source ""


# instance fields
.field private final d:F

.field private final e:F

.field private f:Landroid/graphics/Paint;

.field private g:I


# direct methods
.method public constructor <init>(III)V
    .locals 1
    .param p3    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 1414604
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, LX/8uF;-><init>(IIILandroid/util/DisplayMetrics;)V

    .line 1414605
    return-void
.end method

.method private constructor <init>(IIILandroid/util/DisplayMetrics;)V
    .locals 2
    .param p3    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x2

    .line 1414606
    add-int/lit8 v0, p1, 0x1

    invoke-direct {p0, v0, p4}, LX/8uC;-><init>(ILandroid/util/DisplayMetrics;)V

    .line 1414607
    iput p2, p0, LX/8uF;->g:I

    .line 1414608
    iput p3, p0, LX/8uF;->b:I

    .line 1414609
    const/high16 v0, 0x40400000    # 3.0f

    invoke-static {v1, v0, p4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    iput v0, p0, LX/8uF;->d:F

    .line 1414610
    const/high16 v0, 0x41600000    # 14.0f

    invoke-static {v1, v0, p4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    iput v0, p0, LX/8uF;->e:F

    .line 1414611
    return-void
.end method

.method private b(Landroid/graphics/Paint;)V
    .locals 2

    .prologue
    .line 1414612
    iget-object v0, p0, LX/8uF;->f:Landroid/graphics/Paint;

    if-nez v0, :cond_0

    .line 1414613
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, p1}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v0, p0, LX/8uF;->f:Landroid/graphics/Paint;

    .line 1414614
    iget-object v0, p0, LX/8uF;->f:Landroid/graphics/Paint;

    iget v1, p0, LX/8u8;->b:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1414615
    iget-object v0, p0, LX/8uF;->f:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1414616
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)LX/8u8;
    .locals 1

    .prologue
    .line 1414617
    iget-object v0, p0, LX/8uF;->f:Landroid/graphics/Paint;

    if-eqz v0, :cond_0

    .line 1414618
    iget-object v0, p0, LX/8uF;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1414619
    :cond_0
    invoke-super {p0, p1}, LX/8uC;->a(I)LX/8u8;

    move-result-object v0

    return-object v0
.end method

.method public final drawLeadingMargin(Landroid/graphics/Canvas;Landroid/graphics/Paint;IIIIILjava/lang/CharSequence;IIZLandroid/text/Layout;)V
    .locals 6

    .prologue
    .line 1414620
    check-cast p8, Landroid/text/Spanned;

    invoke-interface {p8, p0}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v0

    if-eq v0, p9, :cond_0

    .line 1414621
    :goto_0
    return-void

    .line 1414622
    :cond_0
    invoke-virtual {p0, p2}, LX/8u8;->a(Landroid/graphics/Paint;)V

    .line 1414623
    invoke-direct {p0, p2}, LX/8uF;->b(Landroid/graphics/Paint;)V

    .line 1414624
    int-to-float v0, p3

    int-to-float v1, p4

    iget v2, p0, LX/8u8;->a:F

    iget v3, p0, LX/8uF;->e:F

    sub-float/2addr v2, v3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    .line 1414625
    iget v1, p0, LX/8uF;->g:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1414626
    :pswitch_0
    add-int v1, p5, p7

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    iget v2, p0, LX/8uF;->d:F

    iget-object v3, p0, LX/8u8;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 1414627
    :pswitch_1
    add-int v1, p5, p7

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    iget v2, p0, LX/8uF;->d:F

    iget-object v3, p0, LX/8uF;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 1414628
    :pswitch_2
    add-int v1, p5, p7

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float v4, v1, v2

    .line 1414629
    iget v1, p0, LX/8uF;->d:F

    sub-float v1, v0, v1

    iget v2, p0, LX/8uF;->d:F

    sub-float v2, v4, v2

    iget v3, p0, LX/8uF;->d:F

    add-float/2addr v3, v0

    iget v0, p0, LX/8uF;->d:F

    add-float/2addr v4, v0

    iget-object v5, p0, LX/8u8;->c:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
