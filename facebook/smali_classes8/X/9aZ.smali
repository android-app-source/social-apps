.class public final LX/9aZ;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Landroid/support/v4/app/DialogFragment;

.field public final synthetic b:Landroid/app/Activity;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/9ac;


# direct methods
.method public constructor <init>(LX/9ac;Landroid/support/v4/app/DialogFragment;Landroid/app/Activity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1513842
    iput-object p1, p0, LX/9aZ;->d:LX/9ac;

    iput-object p2, p0, LX/9aZ;->a:Landroid/support/v4/app/DialogFragment;

    iput-object p3, p0, LX/9aZ;->b:Landroid/app/Activity;

    iput-object p4, p0, LX/9aZ;->c:Ljava/lang/String;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 1513843
    iget-object v0, p0, LX/9aZ;->a:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 1513844
    iget-object v0, p0, LX/9aZ;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, LX/9aZ;->b:Landroid/app/Activity;

    const v2, 0x7f08244e

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1513845
    iget-object v0, p0, LX/9aZ;->d:LX/9ac;

    iget-object v0, v0, LX/9ac;->a:LX/9af;

    invoke-virtual {p1}, Lcom/facebook/fbservice/service/ServiceException;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 1513846
    sget-object v2, LX/9ae;->ALBUM_CREATE_FAILED:LX/9ae;

    invoke-static {v2}, LX/9af;->a(LX/9ae;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string p0, "message"

    invoke-virtual {v2, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-static {v0, v2}, LX/9af;->a(LX/9af;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1513847
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1513848
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 1513849
    iget-object v0, p0, LX/9aZ;->a:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 1513850
    iget-object v0, p0, LX/9aZ;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, LX/9aZ;->b:Landroid/app/Activity;

    const v2, 0x7f08244f

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1513851
    iget-object v0, p0, LX/9aZ;->d:LX/9ac;

    iget-object v0, v0, LX/9ac;->a:LX/9af;

    iget-object v1, p0, LX/9aZ;->c:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    .line 1513852
    sget-object v2, LX/9ae;->ALBUM_CREATED:LX/9ae;

    invoke-static {v2}, LX/9af;->a(LX/9ae;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "is_untitled_album"

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-static {v0, v2}, LX/9af;->a(LX/9af;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1513853
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "result"

    invoke-static {v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 1513854
    iget-object v1, p0, LX/9aZ;->d:LX/9ac;

    iget-object v1, v1, LX/9ac;->f:LX/9bY;

    new-instance v2, LX/9ba;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, LX/9ba;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 1513855
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1513856
    const-string v2, "extra_album"

    invoke-static {v1, v2, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1513857
    iget-object v0, p0, LX/9aZ;->b:Landroid/app/Activity;

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1513858
    iget-object v0, p0, LX/9aZ;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 1513859
    return-void
.end method
