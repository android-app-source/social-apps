.class public final LX/A5W;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0P1;

.field public final synthetic b:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;LX/0P1;)V
    .locals 0

    .prologue
    .line 1621560
    iput-object p1, p0, LX/A5W;->b:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    iput-object p2, p0, LX/A5W;->a:LX/0P1;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1621561
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 12

    .prologue
    .line 1621562
    check-cast p1, LX/0Px;

    .line 1621563
    iget-object v0, p0, LX/A5W;->b:Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;

    iget-object v1, p0, LX/A5W;->a:LX/0P1;

    const/4 v4, 0x0

    .line 1621564
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 1621565
    iget-object v2, v0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->W:Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    .line 1621566
    iget-object v3, v2, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->m:LX/0Px;

    move-object v5, v3

    .line 1621567
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v7

    .line 1621568
    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1621569
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v8

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v8, :cond_0

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 1621570
    invoke-virtual {v7, v2}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1621571
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 1621572
    :cond_0
    invoke-virtual {v7}, LX/0cA;->b()LX/0Rf;

    move-result-object v2

    move-object v7, v2

    .line 1621573
    invoke-virtual {v7}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v3, v4

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 1621574
    invoke-virtual {v1, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v6, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1621575
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    .line 1621576
    goto :goto_1

    .line 1621577
    :cond_1
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v8

    move v5, v4

    :goto_2
    if-ge v5, v8, :cond_2

    invoke-virtual {p1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 1621578
    iget-object v9, v2, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->g:Lcom/facebook/user/model/UserKey;

    move-object v9, v9

    .line 1621579
    invoke-virtual {v9}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    .line 1621580
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v7, v9}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 1621581
    invoke-virtual {v6, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1621582
    add-int/lit8 v2, v3, 0x1

    .line 1621583
    :goto_3
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    move v3, v2

    goto :goto_2

    .line 1621584
    :cond_2
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 1621585
    const/16 v5, 0xa

    invoke-static {v3, v5}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 1621586
    new-instance v5, LX/A5n;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f08139c

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v4, v3}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v3

    invoke-direct {v5, v6, v2, v3}, LX/A5n;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V

    .line 1621587
    invoke-virtual {v5, v4}, LX/622;->a(Z)V

    .line 1621588
    iget-object v2, v0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->P:LX/8tC;

    sget-object v3, LX/A5i;->SUGGESTIONS:LX/A5i;

    invoke-virtual {v3}, LX/A5i;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, v5}, LX/8tB;->a(ILX/621;)V

    .line 1621589
    iget-object v2, v0, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorFragment;->P:LX/8tC;

    const v3, -0x6f58f13a

    invoke-static {v2, v3}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1621590
    return-void

    :cond_3
    move v2, v3

    goto :goto_3
.end method
