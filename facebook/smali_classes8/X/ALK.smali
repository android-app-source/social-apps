.class public final LX/ALK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5f5;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/5f5",
        "<",
        "Landroid/hardware/Camera$Size;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/ALM;


# direct methods
.method public constructor <init>(LX/ALM;)V
    .locals 0

    .prologue
    .line 1664757
    iput-object p1, p0, LX/ALK;->a:LX/ALM;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1664758
    iget-object v0, p0, LX/ALK;->a:LX/ALM;

    iget-object v0, v0, LX/ALM;->a:Lcom/facebook/backstage/camera/CameraView;

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1664759
    invoke-static {v0, v1, v2}, Lcom/facebook/backstage/camera/CameraView;->a$redex0(Lcom/facebook/backstage/camera/CameraView;ZZ)V

    .line 1664760
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 1664761
    iget-object v0, p0, LX/ALK;->a:LX/ALM;

    iget-object v0, v0, LX/ALM;->a:Lcom/facebook/backstage/camera/CameraView;

    invoke-virtual {v0}, Lcom/facebook/backstage/camera/CameraView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082843

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    .line 1664762
    sget-object v0, Lcom/facebook/backstage/camera/CameraView;->f:Ljava/lang/String;

    const-string v1, "Failed to restart the preview"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1664763
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1664764
    invoke-direct {p0}, LX/ALK;->a()V

    return-void
.end method
