.class public final LX/8tS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Executor;


# instance fields
.field private final a:Ljava/util/concurrent/Executor;

.field private volatile b:Z


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;)V
    .locals 0

    .prologue
    .line 1413007
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1413008
    iput-object p1, p0, LX/8tS;->a:Ljava/util/concurrent/Executor;

    .line 1413009
    return-void
.end method

.method public synthetic constructor <init>(Ljava/util/concurrent/Executor;B)V
    .locals 0

    .prologue
    .line 1413010
    invoke-direct {p0, p1}, LX/8tS;-><init>(Ljava/util/concurrent/Executor;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1413011
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8tS;->b:Z

    .line 1413012
    return-void
.end method

.method public final execute(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 1413013
    iget-boolean v0, p0, LX/8tS;->b:Z

    if-eqz v0, :cond_0

    .line 1413014
    :goto_0
    return-void

    .line 1413015
    :cond_0
    iget-object v0, p0, LX/8tS;->a:Ljava/util/concurrent/Executor;

    const v1, -0x3742a21d

    invoke-static {v0, p1, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method
