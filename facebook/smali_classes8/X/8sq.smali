.class public LX/8sq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8sj;


# instance fields
.field public final a:Landroid/view/View;

.field private final b:I

.field private final c:I

.field private final d:Landroid/animation/Animator$AnimatorListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final e:J


# direct methods
.method public constructor <init>(Landroid/view/View;JIILandroid/animation/Animator$AnimatorListener;)V
    .locals 0

    .prologue
    .line 1411840
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1411841
    iput-object p1, p0, LX/8sq;->a:Landroid/view/View;

    .line 1411842
    iput p4, p0, LX/8sq;->b:I

    .line 1411843
    iput p5, p0, LX/8sq;->c:I

    .line 1411844
    iput-wide p2, p0, LX/8sq;->e:J

    .line 1411845
    iput-object p6, p0, LX/8sq;->d:Landroid/animation/Animator$AnimatorListener;

    .line 1411846
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 1411847
    iget-wide v0, p0, LX/8sq;->e:J

    return-wide v0
.end method

.method public final a(F)Landroid/animation/Animator;
    .locals 4

    .prologue
    .line 1411848
    iget v0, p0, LX/8sq;->b:I

    int-to-float v0, v0

    iget v1, p0, LX/8sq;->c:I

    iget v2, p0, LX/8sq;->b:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    mul-float/2addr v1, p1

    add-float/2addr v0, v1

    float-to-int v0, v0

    .line 1411849
    const/4 v1, 0x2

    new-array v1, v1, [I

    const/4 v2, 0x0

    aput v0, v1, v2

    const/4 v2, 0x1

    iget v3, p0, LX/8sq;->c:I

    aput v3, v1, v2

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v1

    .line 1411850
    new-instance v2, LX/8so;

    invoke-direct {v2, p0}, LX/8so;-><init>(LX/8sq;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1411851
    iget-object v2, p0, LX/8sq;->d:Landroid/animation/Animator$AnimatorListener;

    if-eqz v2, :cond_0

    .line 1411852
    iget-object v2, p0, LX/8sq;->d:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1411853
    :cond_0
    new-instance v2, LX/8sp;

    invoke-direct {v2, p0, v0}, LX/8sp;-><init>(LX/8sq;I)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1411854
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr v0, p1

    iget-wide v2, p0, LX/8sq;->e:J

    long-to-float v2, v2

    mul-float/2addr v0, v2

    float-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1411855
    return-object v1
.end method
