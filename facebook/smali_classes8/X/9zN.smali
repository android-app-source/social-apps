.class public final LX/9zN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel$AboutInformationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel$MetadataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLNode;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1601213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;
    .locals 14

    .prologue
    const/4 v4, 0x1

    const/4 v13, 0x0

    const/4 v2, 0x0

    .line 1601214
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1601215
    iget-object v1, p0, LX/9zN;->a:Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel$AboutInformationModel;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1601216
    iget-object v3, p0, LX/9zN;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1601217
    iget-object v5, p0, LX/9zN;->c:Ljava/lang/String;

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1601218
    iget-object v6, p0, LX/9zN;->d:Ljava/lang/String;

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1601219
    iget-object v7, p0, LX/9zN;->e:Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel$MetadataModel;

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1601220
    iget-object v8, p0, LX/9zN;->f:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1601221
    iget-object v9, p0, LX/9zN;->g:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1601222
    iget-object v10, p0, LX/9zN;->h:Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1601223
    iget-object v11, p0, LX/9zN;->i:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-virtual {v0, v11}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v11

    .line 1601224
    const/16 v12, 0x9

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 1601225
    invoke-virtual {v0, v13, v1}, LX/186;->b(II)V

    .line 1601226
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1601227
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1601228
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1601229
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1601230
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1601231
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 1601232
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 1601233
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v11}, LX/186;->b(II)V

    .line 1601234
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1601235
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1601236
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1601237
    invoke-virtual {v1, v13}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1601238
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1601239
    new-instance v1, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;

    invoke-direct {v1, v0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;-><init>(LX/15i;)V

    .line 1601240
    iget-object v0, p0, LX/9zN;->b:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    .line 1601241
    invoke-virtual {v1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->b()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    iget-object v2, p0, LX/9zN;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->L_()LX/0x2;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/graphql/model/GraphQLStory;->a(LX/0x2;)V

    .line 1601242
    :cond_0
    iget-object v0, p0, LX/9zN;->g:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_1

    .line 1601243
    invoke-virtual {v1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->fM_()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    iget-object v2, p0, LX/9zN;->g:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->L_()LX/0x2;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/graphql/model/GraphQLStory;->a(LX/0x2;)V

    .line 1601244
    :cond_1
    return-object v1
.end method
