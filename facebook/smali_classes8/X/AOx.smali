.class public final LX/AOx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AOy;


# direct methods
.method public constructor <init>(LX/AOy;)V
    .locals 0

    .prologue
    .line 1669845
    iput-object p1, p0, LX/AOx;->a:LX/AOy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1669846
    iget-object v0, p0, LX/AOx;->a:LX/AOy;

    iget-object v0, v0, LX/AOy;->a:Lcom/facebook/clashmanagement/debug/ClashManagementSettingsActivity;

    const-string v1, "Eligible Clash Unit Fetched"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1669847
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1669848
    iget-object v0, p0, LX/AOx;->a:LX/AOy;

    iget-object v0, v0, LX/AOy;->a:Lcom/facebook/clashmanagement/debug/ClashManagementSettingsActivity;

    const-string v1, "Fail to fetch Eligible Clash Unit"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1669849
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1669850
    invoke-direct {p0}, LX/AOx;->a()V

    return-void
.end method
