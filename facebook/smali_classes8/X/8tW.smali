.class public final enum LX/8tW;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8tW;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8tW;

.field public static final enum LEFT_TRIM:LX/8tW;

.field public static final enum RIGHT_TRIM:LX/8tW;

.field public static final enum SCRUBBER:LX/8tW;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1413293
    new-instance v0, LX/8tW;

    const-string v1, "LEFT_TRIM"

    invoke-direct {v0, v1, v2}, LX/8tW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8tW;->LEFT_TRIM:LX/8tW;

    .line 1413294
    new-instance v0, LX/8tW;

    const-string v1, "RIGHT_TRIM"

    invoke-direct {v0, v1, v3}, LX/8tW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8tW;->RIGHT_TRIM:LX/8tW;

    .line 1413295
    new-instance v0, LX/8tW;

    const-string v1, "SCRUBBER"

    invoke-direct {v0, v1, v4}, LX/8tW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8tW;->SCRUBBER:LX/8tW;

    .line 1413296
    const/4 v0, 0x3

    new-array v0, v0, [LX/8tW;

    sget-object v1, LX/8tW;->LEFT_TRIM:LX/8tW;

    aput-object v1, v0, v2

    sget-object v1, LX/8tW;->RIGHT_TRIM:LX/8tW;

    aput-object v1, v0, v3

    sget-object v1, LX/8tW;->SCRUBBER:LX/8tW;

    aput-object v1, v0, v4

    sput-object v0, LX/8tW;->$VALUES:[LX/8tW;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1413297
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8tW;
    .locals 1

    .prologue
    .line 1413298
    const-class v0, LX/8tW;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8tW;

    return-object v0
.end method

.method public static values()[LX/8tW;
    .locals 1

    .prologue
    .line 1413299
    sget-object v0, LX/8tW;->$VALUES:[LX/8tW;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8tW;

    return-object v0
.end method
