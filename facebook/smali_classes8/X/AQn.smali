.class public final LX/AQn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$FBLifeEventSuggestionsQueryModel;",
        ">;",
        "LX/AQu;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;)V
    .locals 0

    .prologue
    .line 1671798
    iput-object p1, p0, LX/AQn;->a:Lcom/facebook/composer/lifeevent/type/ComposerLifeEventTypeListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1671799
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x1

    .line 1671800
    if-eqz p1, :cond_0

    .line 1671801
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1671802
    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    .line 1671803
    const/4 v0, 0x0

    .line 1671804
    :goto_1
    return-object v0

    .line 1671805
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1671806
    check-cast v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$FBLifeEventSuggestionsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$FBLifeEventSuggestionsQueryModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 1671807
    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 1671808
    :cond_3
    new-instance v1, LX/AQu;

    invoke-direct {v1}, LX/AQu;-><init>()V

    .line 1671809
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1671810
    check-cast v0, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$FBLifeEventSuggestionsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$FBLifeEventSuggestionsQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v6, 0x0

    .line 1671811
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 1671812
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 1671813
    const/4 v3, 0x2

    const v4, -0x3ec3e34a

    invoke-static {v2, v0, v3, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-static {v3}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v3

    :goto_2
    invoke-virtual {v3}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :cond_4
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    .line 1671814
    new-instance v5, LX/AQs;

    invoke-direct {v5, v4, v3}, LX/AQs;-><init>(LX/15i;I)V

    invoke-virtual {v7, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1671815
    invoke-virtual {v4, v3, v6}, LX/15i;->g(II)I

    move-result v3

    const-class v5, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;

    invoke-virtual {v4, v3, v6, v5}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    move-object v4, v3

    :goto_3
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result p0

    move v5, v6

    :goto_4
    if-ge v5, p0, :cond_4

    invoke-virtual {v4, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;

    .line 1671816
    invoke-virtual {v3}, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;->a()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v8, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1671817
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_4

    .line 1671818
    :cond_5
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v3

    goto :goto_2

    .line 1671819
    :cond_6
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 1671820
    move-object v4, v3

    goto :goto_3

    .line 1671821
    :cond_7
    invoke-interface {v8}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    iput-object v3, v1, LX/AQu;->a:LX/0Px;

    .line 1671822
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    iput-object v3, v1, LX/AQu;->c:LX/0Px;

    .line 1671823
    const/4 v3, 0x1

    const-class v4, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;

    invoke-virtual {v2, v0, v3, v4}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v3

    if-eqz v3, :cond_8

    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    :goto_5
    iput-object v3, v1, LX/AQu;->b:LX/0Px;

    .line 1671824
    const-class v3, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;

    invoke-virtual {v2, v0, v6, v3}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;

    iput-object v3, v1, LX/AQu;->d:Lcom/facebook/composer/lifeevent/protocol/FetchLifeEventComposerDataGraphQLModels$LifeEventFieldsModel;

    .line 1671825
    move-object v0, v1

    .line 1671826
    goto/16 :goto_1

    .line 1671827
    :cond_8
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 1671828
    goto :goto_5
.end method
