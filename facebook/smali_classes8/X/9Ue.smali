.class public final LX/9Ue;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:Z


# instance fields
.field public final synthetic b:LX/9Ug;

.field public final c:LX/3K1;

.field public final d:LX/9Ua;

.field public final e:LX/9Ua;

.field public final f:LX/9Uq;

.field public final g:LX/9Uo;

.field public final h:Landroid/graphics/Matrix;

.field public final i:Landroid/graphics/Matrix;

.field public final j:[F

.field public k:Z

.field public l:[Landroid/graphics/Shader;

.field public m:Landroid/graphics/Shader;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1498143
    const-class v0, LX/9Ug;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, LX/9Ue;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(LX/9Ug;LX/3K1;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1498123
    iput-object p1, p0, LX/9Ue;->b:LX/9Ug;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1498124
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, LX/9Ue;->j:[F

    .line 1498125
    iput-object p2, p0, LX/9Ue;->c:LX/3K1;

    .line 1498126
    invoke-static {p0}, LX/9Ue;->m(LX/9Ue;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1498127
    iput-object v1, p0, LX/9Ue;->d:LX/9Ua;

    .line 1498128
    iput-object v1, p0, LX/9Ue;->f:LX/9Uq;

    .line 1498129
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/9Ue;->h:Landroid/graphics/Matrix;

    .line 1498130
    :goto_0
    new-instance v0, LX/9Uo;

    invoke-direct {v0}, LX/9Uo;-><init>()V

    iput-object v0, p0, LX/9Ue;->g:LX/9Uo;

    .line 1498131
    iget-object v0, p0, LX/9Ue;->c:LX/3K1;

    .line 1498132
    iget-object p1, v0, LX/3K1;->n:LX/3K1;

    move-object v0, p1

    .line 1498133
    if-eqz v0, :cond_1

    .line 1498134
    new-instance v0, LX/9Ua;

    invoke-direct {v0}, LX/9Ua;-><init>()V

    iput-object v0, p0, LX/9Ue;->e:LX/9Ua;

    .line 1498135
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/9Ue;->i:Landroid/graphics/Matrix;

    .line 1498136
    :goto_1
    sget-boolean v0, LX/9Ue;->a:Z

    if-nez v0, :cond_2

    iget-object v0, p0, LX/9Ue;->h:Landroid/graphics/Matrix;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1498137
    :cond_0
    new-instance v0, LX/9Ua;

    invoke-direct {v0}, LX/9Ua;-><init>()V

    iput-object v0, p0, LX/9Ue;->d:LX/9Ua;

    .line 1498138
    new-instance v0, LX/9Uq;

    invoke-direct {v0}, LX/9Uq;-><init>()V

    iput-object v0, p0, LX/9Ue;->f:LX/9Uq;

    .line 1498139
    iget-object v0, p1, LX/9Ug;->f:Landroid/graphics/Matrix;

    iput-object v0, p0, LX/9Ue;->h:Landroid/graphics/Matrix;

    goto :goto_0

    .line 1498140
    :cond_1
    iput-object v1, p0, LX/9Ue;->e:LX/9Ua;

    .line 1498141
    iput-object v1, p0, LX/9Ue;->i:Landroid/graphics/Matrix;

    goto :goto_1

    .line 1498142
    :cond_2
    return-void
.end method

.method public static a(LX/9Ue;LX/3K1;)V
    .locals 14

    .prologue
    const/4 v1, 0x0

    .line 1498167
    iget-object v0, p0, LX/9Ue;->l:[Landroid/graphics/Shader;

    if-eqz v0, :cond_1

    .line 1498168
    :cond_0
    return-void

    .line 1498169
    :cond_1
    iget-object v0, p0, LX/9Ue;->b:LX/9Ug;

    iget-object v0, v0, LX/9Ug;->a:LX/3K3;

    .line 1498170
    iget v2, v0, LX/3K3;->a:I

    move v0, v2

    .line 1498171
    iget-object v2, p0, LX/9Ue;->b:LX/9Ug;

    iget-object v2, v2, LX/9Ug;->a:LX/3K3;

    .line 1498172
    iget v3, v2, LX/3K3;->b:I

    move v9, v3

    .line 1498173
    const/high16 v2, 0x41f00000    # 30.0f

    int-to-float v3, v9

    mul-float/2addr v2, v3

    int-to-float v0, v0

    div-float v0, v2, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v10

    .line 1498174
    add-int/lit8 v0, v10, 0x1

    new-array v0, v0, [Landroid/graphics/LinearGradient;

    iput-object v0, p0, LX/9Ue;->l:[Landroid/graphics/Shader;

    .line 1498175
    new-instance v11, LX/9Um;

    invoke-direct {v11}, LX/9Um;-><init>()V

    .line 1498176
    iget-object v0, p1, LX/3K1;->p:LX/3kN;

    move-object v0, v0

    .line 1498177
    iget-object v2, v0, LX/3kN;->a:LX/3kK;

    move-object v12, v2

    .line 1498178
    const/4 v0, 0x0

    move v8, v0

    :goto_0
    if-ge v8, v10, :cond_0

    .line 1498179
    int-to-float v0, v8

    int-to-float v2, v10

    div-float/2addr v0, v2

    int-to-float v2, v9

    mul-float/2addr v0, v2

    .line 1498180
    iget-object v2, v12, LX/3kK;->a:LX/3kM;

    move-object v2, v2

    .line 1498181
    invoke-virtual {v2, v0, v11}, LX/3Jj;->a(FLjava/lang/Object;)V

    .line 1498182
    iget-object v2, v12, LX/3kK;->b:LX/3kM;

    move-object v2, v2

    .line 1498183
    invoke-virtual {v2, v0, v11}, LX/3Jj;->a(FLjava/lang/Object;)V

    .line 1498184
    iget-object v13, p0, LX/9Ue;->l:[Landroid/graphics/Shader;

    new-instance v0, Landroid/graphics/LinearGradient;

    iget-object v2, p0, LX/9Ue;->b:LX/9Ug;

    iget-object v2, v2, LX/9Ug;->a:LX/3K3;

    .line 1498185
    iget-object v3, v2, LX/3K3;->e:[F

    move-object v2, v3

    .line 1498186
    const/4 v3, 0x1

    aget v4, v2, v3

    .line 1498187
    iget v2, v11, LX/9Um;->a:I

    move v5, v2

    .line 1498188
    iget v2, v11, LX/9Um;->b:I

    move v6, v2

    .line 1498189
    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move v2, v1

    move v3, v1

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    aput-object v0, v13, v8

    .line 1498190
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_0
.end method

.method public static m(LX/9Ue;)Z
    .locals 1

    .prologue
    .line 1498165
    invoke-virtual {p0}, LX/9Ue;->k()LX/9Ud;

    move-result-object v0

    .line 1498166
    if-eqz v0, :cond_0

    iget-object v0, v0, LX/9Ud;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final d()F
    .locals 1

    .prologue
    .line 1498162
    iget-object v0, p0, LX/9Ue;->f:LX/9Uq;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9Ue;->f:LX/9Uq;

    .line 1498163
    iget p0, v0, LX/9Uq;->a:F

    invoke-static {p0}, Ljava/lang/Math;->abs(F)F

    move-result p0

    move v0, p0

    .line 1498164
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()I
    .locals 3

    .prologue
    .line 1498157
    const/high16 v0, 0x437f0000    # 255.0f

    .line 1498158
    iget-object v1, p0, LX/9Ue;->g:LX/9Uo;

    .line 1498159
    iget v2, v1, LX/9Uo;->a:F

    move v1, v2

    .line 1498160
    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v1, v2

    move v1, v1

    .line 1498161
    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 1498154
    iget-object v0, p0, LX/9Ue;->c:LX/3K1;

    .line 1498155
    iget p0, v0, LX/3K1;->f:I

    move v0, p0

    .line 1498156
    return v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 1498151
    iget-object v0, p0, LX/9Ue;->c:LX/3K1;

    .line 1498152
    iget p0, v0, LX/3K1;->e:I

    move v0, p0

    .line 1498153
    return v0
.end method

.method public final i()Landroid/graphics/Paint$Cap;
    .locals 1

    .prologue
    .line 1498148
    iget-object v0, p0, LX/9Ue;->c:LX/3K1;

    .line 1498149
    iget-object p0, v0, LX/3K1;->m:Landroid/graphics/Paint$Cap;

    move-object v0, p0

    .line 1498150
    return-object v0
.end method

.method public final k()LX/9Ud;
    .locals 2

    .prologue
    .line 1498144
    iget-object v0, p0, LX/9Ue;->b:LX/9Ug;

    iget-object v0, v0, LX/9Ug;->n:Ljava/util/Map;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 1498145
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/9Ue;->b:LX/9Ug;

    iget-object v0, v0, LX/9Ug;->n:Ljava/util/Map;

    iget-object v1, p0, LX/9Ue;->c:LX/3K1;

    .line 1498146
    iget-object p0, v1, LX/3K1;->q:Ljava/lang/String;

    move-object v1, p0

    .line 1498147
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Ud;

    goto :goto_0
.end method
