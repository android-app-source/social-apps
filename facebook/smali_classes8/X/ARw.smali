.class public LX/ARw;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/ARv;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1673515
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1673516
    return-void
.end method


# virtual methods
.method public final a(LX/Hqt;Ljava/lang/Object;)LX/ARv;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData:",
            "Ljava/lang/Object;",
            ":",
            "LX/0io;",
            ":",
            "LX/0j0;",
            ":",
            "LX/0j1;",
            ":",
            "LX/0j3;",
            ":",
            "LX/0j8;",
            ":",
            "LX/0j4;",
            ":",
            "LX/0j5;",
            ":",
            "LX/0jB;",
            ":",
            "LX/0jD;",
            ":",
            "LX/0j6;",
            ":",
            "LX/0ip;",
            ":",
            "LX/0jF;",
            "DerivedData:",
            "Ljava/lang/Object;",
            ":",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesIsTargetMenuSupported;",
            ":",
            "LX/5RE;",
            "PluginData:",
            "Ljava/lang/Object;",
            ":",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginGetters$ProvidesPluginAllowsAttachingToAlbumsGetter;",
            ":",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginGetters$ProvidesPluginAllowsCheckinGetter;",
            ":",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginGetters$ProvidesPluginAllowsMinutiaeGetter;",
            ":",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginGetters$ProvidesPluginAllowsPhotoGetter;",
            ":",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginGetters$ProvidesPluginAllowsTaggingPeopleGetter;",
            ":",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginGetters$ProvidesPluginTitleGetter;",
            "Services:",
            "Ljava/lang/Object;",
            ":",
            "LX/0il",
            "<TModelData;>;:",
            "LX/0ik",
            "<TDerivedData;>;:",
            "LX/0in",
            "<TPluginData;>;>(",
            "Lcom/facebook/composer/targetselection/ComposerTargetSelectorController$TargetSelectorClient;",
            "TServices;)",
            "LX/ARv",
            "<TModelData;TDerivedData;TPluginData;TServices;>;"
        }
    .end annotation

    .prologue
    .line 1673517
    new-instance v0, LX/ARv;

    invoke-static/range {p0 .. p0}, LX/ARx;->a(LX/0QB;)LX/ARx;

    move-result-object v1

    check-cast v1, LX/ARx;

    invoke-static/range {p0 .. p0}, LX/ARy;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v2

    invoke-static/range {p0 .. p0}, LX/BEH;->a(LX/0QB;)LX/BEH;

    move-result-object v3

    check-cast v3, LX/7mI;

    invoke-static/range {p0 .. p0}, LX/APD;->a(LX/0QB;)LX/APD;

    move-result-object v4

    check-cast v4, LX/APD;

    invoke-static/range {p0 .. p0}, LX/31w;->a(LX/0QB;)LX/31w;

    move-result-object v5

    check-cast v5, LX/31w;

    invoke-static/range {p0 .. p0}, LX/APF;->a(LX/0QB;)LX/APF;

    move-result-object v6

    check-cast v6, LX/APF;

    invoke-static/range {p0 .. p0}, LX/2sV;->a(LX/0QB;)LX/2sV;

    move-result-object v7

    check-cast v7, LX/2sV;

    invoke-static/range {p0 .. p0}, LX/APL;->a(LX/0QB;)LX/APL;

    move-result-object v8

    check-cast v8, LX/APL;

    invoke-static/range {p0 .. p0}, LX/AP6;->a(LX/0QB;)LX/AP6;

    move-result-object v9

    check-cast v9, LX/AP6;

    invoke-static/range {p0 .. p0}, LX/APz;->a(LX/0QB;)LX/APz;

    move-result-object v10

    check-cast v10, LX/APz;

    invoke-static/range {p0 .. p0}, LX/0gd;->a(LX/0QB;)LX/0gd;

    move-result-object v11

    check-cast v11, LX/0gd;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v12

    check-cast v12, LX/03V;

    invoke-static/range {p0 .. p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v13

    check-cast v13, Landroid/content/res/Resources;

    move-object/from16 v15, p2

    check-cast v15, LX/0il;

    move-object/from16 v14, p1

    invoke-direct/range {v0 .. v15}, LX/ARv;-><init>(LX/ARx;Ljava/util/Set;LX/7mI;LX/APD;LX/31w;LX/APF;LX/2sV;LX/APL;LX/AP6;LX/APz;LX/0gd;LX/03V;Landroid/content/res/Resources;LX/Hqt;LX/0il;)V

    .line 1673518
    return-object v0
.end method
