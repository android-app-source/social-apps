.class public final LX/AMX;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AMP;

.field public final synthetic b:LX/AMZ;


# direct methods
.method public constructor <init>(LX/AMZ;LX/AMP;)V
    .locals 0

    .prologue
    .line 1666293
    iput-object p1, p0, LX/AMX;->b:LX/AMZ;

    iput-object p2, p0, LX/AMX;->a:LX/AMP;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1666294
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 1666295
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1666296
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1666297
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1666298
    check-cast v0, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel;

    invoke-virtual {v0}, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel;->a()Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel;

    move-result-object v0

    .line 1666299
    if-nez v0, :cond_1

    .line 1666300
    iget-object v0, p0, LX/AMX;->a:LX/AMP;

    .line 1666301
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 1666302
    invoke-virtual {v0, v1}, LX/AMP;->a(LX/0Px;)V

    .line 1666303
    :cond_0
    return-void

    .line 1666304
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel$EdgesModel;

    .line 1666305
    invoke-virtual {v0}, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel$EdgesModel;->a()Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel$EdgesModel$NodeModel;

    move-result-object v0

    .line 1666306
    if-eqz v0, :cond_2

    .line 1666307
    invoke-virtual {v0}, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".zip"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel$EdgesModel$NodeModel;->l()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {v0}, Lcom/facebook/cameracore/assets/graphql/FetchBestShaderFilterQueryModels$FetchBestShaderFilterQueryModel$ShaderFiltersModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v6, v7, v8, v0}, LX/AMT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)LX/AMT;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1666308
    iget-object v0, p0, LX/AMX;->a:LX/AMP;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/AMP;->a(LX/0Px;)V

    .line 1666309
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method
