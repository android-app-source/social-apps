.class public final LX/945;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/composer/privacy/common/FixedPrivacyView;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/privacy/common/FixedPrivacyView;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1434466
    iput-object p1, p0, LX/945;->b:Lcom/facebook/composer/privacy/common/FixedPrivacyView;

    iput-object p2, p0, LX/945;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x2b05804

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1434467
    iget-object v1, p0, LX/945;->a:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1434468
    const v1, 0x3fb0009f

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1434469
    :goto_0
    return-void

    .line 1434470
    :cond_0
    iget-object v1, p0, LX/945;->b:Lcom/facebook/composer/privacy/common/FixedPrivacyView;

    iget-object v1, v1, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->g:LX/0hs;

    if-nez v1, :cond_1

    .line 1434471
    iget-object v1, p0, LX/945;->b:Lcom/facebook/composer/privacy/common/FixedPrivacyView;

    new-instance v2, LX/0hs;

    iget-object v3, p0, LX/945;->b:Lcom/facebook/composer/privacy/common/FixedPrivacyView;

    invoke-virtual {v3}, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/0hs;-><init>(Landroid/content/Context;)V

    .line 1434472
    iput-object v2, v1, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->g:LX/0hs;

    .line 1434473
    iget-object v1, p0, LX/945;->b:Lcom/facebook/composer/privacy/common/FixedPrivacyView;

    iget-object v1, v1, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->g:LX/0hs;

    const/4 v2, -0x1

    .line 1434474
    iput v2, v1, LX/0hs;->t:I

    .line 1434475
    iget-object v1, p0, LX/945;->b:Lcom/facebook/composer/privacy/common/FixedPrivacyView;

    iget-object v1, v1, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->g:LX/0hs;

    iget-object v2, p0, LX/945;->b:Lcom/facebook/composer/privacy/common/FixedPrivacyView;

    invoke-virtual {v1, v2}, LX/0ht;->c(Landroid/view/View;)V

    .line 1434476
    :cond_1
    iget-object v1, p0, LX/945;->b:Lcom/facebook/composer/privacy/common/FixedPrivacyView;

    iget-object v1, v1, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->g:LX/0hs;

    iget-object v2, p0, LX/945;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 1434477
    iget-object v1, p0, LX/945;->b:Lcom/facebook/composer/privacy/common/FixedPrivacyView;

    iget-object v1, v1, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->g:LX/0hs;

    invoke-virtual {v1}, LX/0ht;->d()V

    .line 1434478
    const v1, 0x756ef511

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
