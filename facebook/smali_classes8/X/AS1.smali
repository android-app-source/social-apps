.class public LX/AS1;
.super LX/AS0;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData$ProvidesAudienceEducatorData;",
        "DerivedData::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesIsPrivacyPillSupported;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;>",
        "LX/AS0",
        "<TModelData;TDerivedData;TServices;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;Landroid/view/ViewGroup;LX/0il;)V
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/view/ViewGroup;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Landroid/view/ViewGroup;",
            "TServices;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1673592
    invoke-direct {p0, p1, p2, p3}, LX/AS0;-><init>(Landroid/view/ViewGroup;Landroid/view/ViewGroup;LX/0il;)V

    .line 1673593
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1673594
    const/4 v0, 0x1

    return v0
.end method

.method public final a(LX/5L2;)LX/ASZ;
    .locals 2

    .prologue
    .line 1673595
    sget-object v0, LX/ARz;->a:[I

    invoke-virtual {p1}, LX/5L2;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1673596
    sget-object v0, LX/ASZ;->NONE:LX/ASZ;

    :goto_0
    return-object v0

    .line 1673597
    :pswitch_0
    sget-object v0, LX/ASZ;->SHOW:LX/ASZ;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
