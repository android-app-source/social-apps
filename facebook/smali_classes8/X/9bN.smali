.class public final LX/9bN;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/base/fragment/FbFragment;

.field public final synthetic b:Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;Lcom/facebook/base/fragment/FbFragment;)V
    .locals 0

    .prologue
    .line 1514858
    iput-object p1, p0, LX/9bN;->b:Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    iput-object p2, p0, LX/9bN;->a:Lcom/facebook/base/fragment/FbFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1514859
    sget-object v0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->c:Ljava/lang/Class;

    const-string v1, "Failed to fetch FacebookPhoto by fbid"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1514860
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1514861
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 1514862
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/method/FetchPhotosMetadataResult;

    .line 1514863
    if-eqz v0, :cond_0

    .line 1514864
    iget-object v1, v0, Lcom/facebook/photos/data/method/FetchPhotosMetadataResult;->a:Ljava/util/List;

    move-object v1, v1

    .line 1514865
    if-eqz v1, :cond_0

    .line 1514866
    iget-object v1, v0, Lcom/facebook/photos/data/method/FetchPhotosMetadataResult;->a:Ljava/util/List;

    move-object v1, v1

    .line 1514867
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1514868
    :cond_0
    :goto_0
    return-void

    .line 1514869
    :cond_1
    iget-object v1, v0, Lcom/facebook/photos/data/method/FetchPhotosMetadataResult;->a:Ljava/util/List;

    move-object v0, v1

    .line 1514870
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 1514871
    iget-object v2, p0, LX/9bN;->a:Lcom/facebook/base/fragment/FbFragment;

    .line 1514872
    invoke-static {v0, v2}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a(Lcom/facebook/graphql/model/GraphQLPhoto;Lcom/facebook/base/fragment/FbFragment;)V

    .line 1514873
    goto :goto_0
.end method
