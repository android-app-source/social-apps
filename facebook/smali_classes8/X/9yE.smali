.class public final LX/9yE;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 23

    .prologue
    .line 1595829
    const/16 v19, 0x0

    .line 1595830
    const/16 v18, 0x0

    .line 1595831
    const/16 v17, 0x0

    .line 1595832
    const/16 v16, 0x0

    .line 1595833
    const/4 v15, 0x0

    .line 1595834
    const/4 v14, 0x0

    .line 1595835
    const/4 v13, 0x0

    .line 1595836
    const/4 v12, 0x0

    .line 1595837
    const/4 v11, 0x0

    .line 1595838
    const/4 v10, 0x0

    .line 1595839
    const/4 v9, 0x0

    .line 1595840
    const/4 v8, 0x0

    .line 1595841
    const/4 v7, 0x0

    .line 1595842
    const/4 v6, 0x0

    .line 1595843
    const/4 v5, 0x0

    .line 1595844
    const/4 v4, 0x0

    .line 1595845
    const/4 v3, 0x0

    .line 1595846
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v20

    sget-object v21, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-eq v0, v1, :cond_1

    .line 1595847
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1595848
    const/4 v3, 0x0

    .line 1595849
    :goto_0
    return v3

    .line 1595850
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1595851
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v20

    sget-object v21, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-eq v0, v1, :cond_13

    .line 1595852
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v20

    .line 1595853
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1595854
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v21

    sget-object v22, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-eq v0, v1, :cond_1

    if-eqz v20, :cond_1

    .line 1595855
    const-string v21, "__type__"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_2

    const-string v21, "__typename"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_3

    .line 1595856
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v19

    goto :goto_1

    .line 1595857
    :cond_3
    const-string v21, "action"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_4

    .line 1595858
    invoke-static/range {p0 .. p1}, LX/9tj;->a(LX/15w;LX/186;)I

    move-result v18

    goto :goto_1

    .line 1595859
    :cond_4
    const-string v21, "component_logical_path"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_5

    .line 1595860
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    goto :goto_1

    .line 1595861
    :cond_5
    const-string v21, "component_style"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_6

    .line 1595862
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v16

    goto :goto_1

    .line 1595863
    :cond_6
    const-string v21, "component_tracking_data"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_7

    .line 1595864
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    goto/16 :goto_1

    .line 1595865
    :cond_7
    const-string v21, "core_attribute_background_color"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_8

    .line 1595866
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    goto/16 :goto_1

    .line 1595867
    :cond_8
    const-string v21, "core_attribute_border"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_9

    .line 1595868
    invoke-static/range {p0 .. p1}, LX/9qG;->a(LX/15w;LX/186;)I

    move-result v13

    goto/16 :goto_1

    .line 1595869
    :cond_9
    const-string v21, "core_attribute_glyph_alignment"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_a

    .line 1595870
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v12

    goto/16 :goto_1

    .line 1595871
    :cond_a
    const-string v21, "core_attribute_margin"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_b

    .line 1595872
    invoke-static/range {p0 .. p1}, LX/9qH;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 1595873
    :cond_b
    const-string v21, "core_attribute_padding"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_c

    .line 1595874
    invoke-static/range {p0 .. p1}, LX/9qI;->a(LX/15w;LX/186;)I

    move-result v10

    goto/16 :goto_1

    .line 1595875
    :cond_c
    const-string v21, "core_glyph"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_d

    .line 1595876
    invoke-static/range {p0 .. p1}, LX/5tK;->a(LX/15w;LX/186;)I

    move-result v9

    goto/16 :goto_1

    .line 1595877
    :cond_d
    const-string v21, "primary_message"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_e

    .line 1595878
    invoke-static/range {p0 .. p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v8

    goto/16 :goto_1

    .line 1595879
    :cond_e
    const-string v21, "primary_spec"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_f

    .line 1595880
    invoke-static/range {p0 .. p1}, LX/9qJ;->a(LX/15w;LX/186;)I

    move-result v7

    goto/16 :goto_1

    .line 1595881
    :cond_f
    const-string v21, "secondary_message"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_10

    .line 1595882
    invoke-static/range {p0 .. p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 1595883
    :cond_10
    const-string v21, "secondary_spec"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_11

    .line 1595884
    invoke-static/range {p0 .. p1}, LX/9qJ;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 1595885
    :cond_11
    const-string v21, "tertiary_message"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_12

    .line 1595886
    invoke-static/range {p0 .. p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 1595887
    :cond_12
    const-string v21, "tertiary_spec"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_0

    .line 1595888
    invoke-static/range {p0 .. p1}, LX/9qJ;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 1595889
    :cond_13
    const/16 v20, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1595890
    const/16 v20, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1595891
    const/16 v19, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1595892
    const/16 v18, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1595893
    const/16 v17, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1595894
    const/16 v16, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 1595895
    const/4 v15, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v14}, LX/186;->b(II)V

    .line 1595896
    const/4 v14, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 1595897
    const/4 v13, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 1595898
    const/16 v12, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 1595899
    const/16 v11, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 1595900
    const/16 v10, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 1595901
    const/16 v9, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 1595902
    const/16 v8, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v7}, LX/186;->b(II)V

    .line 1595903
    const/16 v7, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v6}, LX/186;->b(II)V

    .line 1595904
    const/16 v6, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v5}, LX/186;->b(II)V

    .line 1595905
    const/16 v5, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 1595906
    const/16 v4, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1595907
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v3, 0x7

    const/4 v2, 0x3

    const/4 v1, 0x0

    .line 1595908
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1595909
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1595910
    if-eqz v0, :cond_0

    .line 1595911
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1595912
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1595913
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1595914
    if-eqz v0, :cond_1

    .line 1595915
    const-string v1, "action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1595916
    invoke-static {p0, v0, p2, p3}, LX/9tj;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1595917
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1595918
    if-eqz v0, :cond_2

    .line 1595919
    const-string v1, "component_logical_path"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1595920
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1595921
    :cond_2
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1595922
    if-eqz v0, :cond_3

    .line 1595923
    const-string v0, "component_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1595924
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1595925
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1595926
    if-eqz v0, :cond_4

    .line 1595927
    const-string v1, "component_tracking_data"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1595928
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1595929
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1595930
    if-eqz v0, :cond_5

    .line 1595931
    const-string v1, "core_attribute_background_color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1595932
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1595933
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1595934
    if-eqz v0, :cond_6

    .line 1595935
    const-string v1, "core_attribute_border"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1595936
    invoke-static {p0, v0, p2}, LX/9qG;->a(LX/15i;ILX/0nX;)V

    .line 1595937
    :cond_6
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1595938
    if-eqz v0, :cond_7

    .line 1595939
    const-string v0, "core_attribute_glyph_alignment"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1595940
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1595941
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1595942
    if-eqz v0, :cond_8

    .line 1595943
    const-string v1, "core_attribute_margin"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1595944
    invoke-static {p0, v0, p2}, LX/9qH;->a(LX/15i;ILX/0nX;)V

    .line 1595945
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1595946
    if-eqz v0, :cond_9

    .line 1595947
    const-string v1, "core_attribute_padding"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1595948
    invoke-static {p0, v0, p2}, LX/9qI;->a(LX/15i;ILX/0nX;)V

    .line 1595949
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1595950
    if-eqz v0, :cond_a

    .line 1595951
    const-string v1, "core_glyph"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1595952
    invoke-static {p0, v0, p2}, LX/5tK;->a(LX/15i;ILX/0nX;)V

    .line 1595953
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1595954
    if-eqz v0, :cond_b

    .line 1595955
    const-string v1, "primary_message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1595956
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 1595957
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1595958
    if-eqz v0, :cond_c

    .line 1595959
    const-string v1, "primary_spec"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1595960
    invoke-static {p0, v0, p2}, LX/9qJ;->a(LX/15i;ILX/0nX;)V

    .line 1595961
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1595962
    if-eqz v0, :cond_d

    .line 1595963
    const-string v1, "secondary_message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1595964
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 1595965
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1595966
    if-eqz v0, :cond_e

    .line 1595967
    const-string v1, "secondary_spec"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1595968
    invoke-static {p0, v0, p2}, LX/9qJ;->a(LX/15i;ILX/0nX;)V

    .line 1595969
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1595970
    if-eqz v0, :cond_f

    .line 1595971
    const-string v1, "tertiary_message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1595972
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 1595973
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1595974
    if-eqz v0, :cond_10

    .line 1595975
    const-string v1, "tertiary_spec"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1595976
    invoke-static {p0, v0, p2}, LX/9qJ;->a(LX/15i;ILX/0nX;)V

    .line 1595977
    :cond_10
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1595978
    return-void
.end method
