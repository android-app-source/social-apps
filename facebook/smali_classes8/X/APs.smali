.class public final LX/APs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0iK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0it;",
        ":",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesPrivacyOverride;",
        ":",
        "LX/0j1;",
        ":",
        "LX/0j3;",
        ":",
        "LX/0j4;",
        ":",
        "LX/0iq;",
        ":",
        "LX/0j6;",
        "DerivedData::",
        "LX/5Qu;",
        "PluginData::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginGetters$ProvidesPluginPrivacyDelegateGetter;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;:",
        "LX/0in",
        "<TPluginData;>;>",
        "Ljava/lang/Object;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;"
    }
.end annotation


# instance fields
.field public a:LX/93Q;

.field private b:Ljava/lang/String;

.field private c:LX/93t;

.field private final d:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field private final e:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/composer/controller/ComposerPrivacyController$ComposerPrivacyCallback;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/93S;

.field private final g:LX/93n;

.field private final h:LX/93y;

.field private final i:LX/93d;

.field private final j:LX/93j;

.field private final k:LX/941;

.field private final l:LX/93p;

.field private final m:LX/93l;

.field private final n:LX/93Z;

.field private final o:LX/93f;

.field private final p:Landroid/content/res/Resources;

.field private final q:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(LX/0il;LX/HqS;LX/93S;LX/93n;LX/93y;LX/93d;LX/93j;LX/941;LX/93p;LX/93l;LX/93Z;LX/93f;Landroid/content/res/Resources;Ljava/lang/Boolean;)V
    .locals 1
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/HqS;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p14    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "Lcom/facebook/composer/controller/ComposerPrivacyController$ComposerPrivacyCallback;",
            "LX/93S;",
            "LX/93n;",
            "LX/93y;",
            "LX/93d;",
            "LX/93j;",
            "LX/941;",
            "LX/93p;",
            "LX/93l;",
            "LX/93Z;",
            "LX/93f;",
            "Landroid/content/res/Resources;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1670491
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1670492
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/APs;->d:Ljava/lang/ref/WeakReference;

    .line 1670493
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/APs;->e:Ljava/lang/ref/WeakReference;

    .line 1670494
    iput-object p3, p0, LX/APs;->f:LX/93S;

    .line 1670495
    iput-object p4, p0, LX/APs;->g:LX/93n;

    .line 1670496
    iput-object p5, p0, LX/APs;->h:LX/93y;

    .line 1670497
    iput-object p6, p0, LX/APs;->i:LX/93d;

    .line 1670498
    iput-object p7, p0, LX/APs;->j:LX/93j;

    .line 1670499
    iput-object p8, p0, LX/APs;->k:LX/941;

    .line 1670500
    iput-object p9, p0, LX/APs;->l:LX/93p;

    .line 1670501
    iput-object p10, p0, LX/APs;->m:LX/93l;

    .line 1670502
    iput-object p11, p0, LX/APs;->n:LX/93Z;

    .line 1670503
    iput-object p12, p0, LX/APs;->o:LX/93f;

    .line 1670504
    iput-object p13, p0, LX/APs;->p:Landroid/content/res/Resources;

    .line 1670505
    iput-object p14, p0, LX/APs;->q:Ljava/lang/Boolean;

    .line 1670506
    invoke-direct {p0}, LX/APs;->e()V

    .line 1670507
    return-void
.end method

.method private e()V
    .locals 4

    .prologue
    .line 1670543
    invoke-direct {p0}, LX/APs;->f()LX/93Q;

    move-result-object v1

    .line 1670544
    iget-object v0, p0, LX/APs;->a:LX/93Q;

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    .line 1670545
    :goto_0
    if-eqz v0, :cond_0

    iget-object v2, p0, LX/APs;->b:Ljava/lang/String;

    invoke-virtual {v1}, LX/93Q;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1670546
    :cond_0
    if-eqz v0, :cond_1

    .line 1670547
    iget-object v2, p0, LX/APs;->a:LX/93Q;

    invoke-virtual {v2}, LX/93Q;->e()V

    .line 1670548
    :cond_1
    iput-object v1, p0, LX/APs;->a:LX/93Q;

    .line 1670549
    iget-object v1, p0, LX/APs;->a:LX/93Q;

    invoke-virtual {v1}, LX/93Q;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/APs;->b:Ljava/lang/String;

    .line 1670550
    if-eqz v0, :cond_2

    .line 1670551
    iget-object v0, p0, LX/APs;->a:LX/93Q;

    invoke-virtual {v0}, LX/93Q;->a()V

    .line 1670552
    :cond_2
    return-void

    .line 1670553
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()LX/93Q;
    .locals 14

    .prologue
    .line 1670511
    iget-object v0, p0, LX/APs;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1670512
    iget-object v1, p0, LX/APs;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HqS;

    move-object v2, v0

    .line 1670513
    check-cast v2, LX/0in;

    invoke-interface {v2}, LX/0in;->d()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/AQ9;

    .line 1670514
    iget-object v3, v2, LX/AQ9;->y:LX/Aje;

    move-object v2, v3

    .line 1670515
    if-eqz v2, :cond_0

    .line 1670516
    check-cast v0, LX/0in;

    invoke-interface {v0}, LX/0in;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AQ9;

    .line 1670517
    iget-object v2, v0, LX/AQ9;->y:LX/Aje;

    move-object v0, v2

    .line 1670518
    invoke-interface {v0, v1, v1}, LX/Aje;->a(LX/93q;LX/93w;)LX/93Q;

    move-result-object v0

    .line 1670519
    :goto_0
    return-object v0

    .line 1670520
    :cond_0
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0it;

    check-cast v2, LX/0j3;

    invoke-interface {v2}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v3

    .line 1670521
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0it;

    check-cast v2, LX/0j6;

    invoke-interface {v2}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v4

    .line 1670522
    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1670523
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0it;

    check-cast v2, LX/0j1;

    invoke-interface {v2}, LX/0j1;->getTargetAlbum()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, v4, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v5, LX/2rw;->GROUP:LX/2rw;

    if-eq v2, v5, :cond_1

    .line 1670524
    iget-object v2, p0, LX/APs;->f:LX/93S;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0it;

    check-cast v0, LX/0j1;

    invoke-interface {v0}, LX/0j1;->getTargetAlbum()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, LX/93S;->a(LX/93q;Lcom/facebook/graphql/model/GraphQLAlbum;)LX/93R;

    move-result-object v0

    goto :goto_0

    .line 1670525
    :cond_1
    iget-object v2, v4, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v5, LX/2rw;->UNDIRECTED:LX/2rw;

    if-ne v2, v5, :cond_3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEditPrivacyEnabled()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/APs;->q:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1670526
    :cond_2
    invoke-virtual {p0}, LX/APs;->a()LX/93t;

    move-result-object v0

    goto :goto_0

    .line 1670527
    :cond_3
    sget-object v2, LX/APr;->a:[I

    iget-object v3, v4, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    invoke-virtual {v3}, LX/2rw;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1670528
    iget-object v0, p0, LX/APs;->n:LX/93Z;

    invoke-virtual {v0, v1}, LX/93Z;->a(LX/93q;)LX/93Y;

    move-result-object v0

    goto :goto_0

    .line 1670529
    :pswitch_0
    iget-object v0, p0, LX/APs;->i:LX/93d;

    iget-wide v2, v4, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v4, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    iget-object v4, v4, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetPrivacy:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/93d;->a(LX/93q;Ljava/lang/Long;Ljava/lang/String;LX/2rX;)LX/93c;

    move-result-object v0

    goto/16 :goto_0

    .line 1670530
    :pswitch_1
    iget-object v0, p0, LX/APs;->j:LX/93j;

    iget-wide v2, v4, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/93j;->a(LX/93q;Ljava/lang/Long;)LX/93i;

    move-result-object v0

    goto/16 :goto_0

    .line 1670531
    :pswitch_2
    iget-object v0, p0, LX/APs;->k:LX/941;

    iget-wide v2, v4, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v4, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    iget-object v4, v4, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetPrivacy:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/941;->a(LX/93q;Ljava/lang/Long;Ljava/lang/String;LX/2rX;)LX/940;

    move-result-object v0

    goto/16 :goto_0

    .line 1670532
    :pswitch_3
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0it;

    check-cast v0, LX/0j4;

    invoke-interface {v0}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v0

    invoke-static {v0}, LX/2rm;->a(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1670533
    iget-object v0, p0, LX/APs;->g:LX/93n;

    iget-object v2, p0, LX/APs;->p:Landroid/content/res/Resources;

    const v3, 0x7f0812d2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/APs;->p:Landroid/content/res/Resources;

    const v5, 0x7f081446

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v4, v4, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    aput-object v4, v6, v7

    invoke-virtual {v3, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/93n;->a(LX/93q;Ljava/lang/String;Ljava/lang/String;)LX/93m;

    move-result-object v0

    goto/16 :goto_0

    .line 1670534
    :cond_4
    iget-object v0, p0, LX/APs;->l:LX/93p;

    iget-object v2, v4, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/93p;->a(LX/93q;Ljava/lang/String;)LX/93o;

    move-result-object v0

    goto/16 :goto_0

    .line 1670535
    :pswitch_4
    iget-object v0, p0, LX/APs;->m:LX/93l;

    iget-object v2, v4, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    .line 1670536
    new-instance v8, LX/93k;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v10

    check-cast v10, LX/03V;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v11

    check-cast v11, LX/1Ck;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v12

    check-cast v12, Landroid/content/res/Resources;

    move-object v9, v1

    move-object v13, v2

    invoke-direct/range {v8 .. v13}, LX/93k;-><init>(LX/93q;LX/03V;LX/1Ck;Landroid/content/res/Resources;Ljava/lang/String;)V

    .line 1670537
    move-object v0, v8

    .line 1670538
    goto/16 :goto_0

    .line 1670539
    :pswitch_5
    iget-object v0, p0, LX/APs;->o:LX/93f;

    iget-object v2, v4, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetPrivacy:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    iget-object v3, v4, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    .line 1670540
    new-instance v8, LX/93e;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v10

    check-cast v10, LX/03V;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v11

    check-cast v11, LX/1Ck;

    move-object v9, v1

    move-object v12, v2

    move-object v13, v3

    invoke-direct/range {v8 .. v13}, LX/93e;-><init>(LX/93q;LX/03V;LX/1Ck;LX/2rX;Ljava/lang/String;)V

    .line 1670541
    move-object v0, v8

    .line 1670542
    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public final a()LX/93t;
    .locals 4

    .prologue
    .line 1670554
    iget-object v0, p0, LX/APs;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1670555
    iget-object v1, p0, LX/APs;->c:LX/93t;

    if-eqz v1, :cond_0

    .line 1670556
    iget-object v1, p0, LX/APs;->c:LX/93t;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0it;

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getPrivacyOverride()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/93s;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)V

    .line 1670557
    iget-object v0, p0, LX/APs;->c:LX/93t;

    .line 1670558
    :goto_0
    return-object v0

    .line 1670559
    :cond_0
    iget-object v1, p0, LX/APs;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HqS;

    .line 1670560
    iget-object v3, p0, LX/APs;->h:LX/93y;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0it;

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v2}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getPrivacyOverride()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v2

    invoke-virtual {v3, v1, v1, v2, v0}, LX/93y;->a(LX/93q;LX/93w;Lcom/facebook/graphql/model/GraphQLPrivacyOption;Ljava/lang/Object;)LX/93t;

    move-result-object v0

    iput-object v0, p0, LX/APs;->c:LX/93t;

    goto :goto_0
.end method

.method public final a(LX/5L2;)V
    .locals 0

    .prologue
    .line 1670510
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1670508
    invoke-direct {p0}, LX/APs;->e()V

    .line 1670509
    return-void
.end method
