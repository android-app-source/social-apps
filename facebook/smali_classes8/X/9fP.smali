.class public LX/9fP;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Landroid/graphics/RectF;


# instance fields
.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/8Hx;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 1521853
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v1, v1, v2, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    sput-object v0, LX/9fP;->a:Landroid/graphics/RectF;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;LX/0Or;)V
    .locals 0
    .param p1    # Ljava/util/List;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/RectF;",
            ">;",
            "LX/0Or",
            "<",
            "LX/8Hx;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1521854
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1521855
    iput-object p1, p0, LX/9fP;->b:Ljava/util/List;

    .line 1521856
    iput-object p2, p0, LX/9fP;->c:LX/0Or;

    .line 1521857
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;LX/9ek;I)LX/8Hx;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1521858
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFilterName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFilterName()Ljava/lang/String;

    move-result-object v0

    sget-object v1, LX/5iL;->PassThrough:LX/5iL;

    invoke-virtual {v1}, LX/5iL;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/9fP;->b:Ljava/util/List;

    if-nez v0, :cond_1

    .line 1521859
    :cond_0
    const/4 v0, 0x0

    .line 1521860
    :goto_0
    return-object v0

    .line 1521861
    :cond_1
    sget-object v0, LX/9ek;->SHOW_EDITED_URI:LX/9ek;

    if-ne p2, v0, :cond_2

    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v0

    if-nez v0, :cond_3

    :cond_2
    if-eqz p3, :cond_8

    .line 1521862
    :cond_3
    iget-object v0, p0, LX/9fP;->b:Ljava/util/List;

    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v1

    invoke-static {v1}, LX/63w;->c(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Landroid/graphics/RectF;

    move-result-object v1

    const/high16 p2, 0x3f000000    # 0.5f

    .line 1521863
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1521864
    new-instance v4, Landroid/graphics/Matrix;

    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    .line 1521865
    if-eqz v1, :cond_4

    .line 1521866
    sget-object v2, LX/9fP;->a:Landroid/graphics/RectF;

    sget-object v5, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v4, v1, v2, v5}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 1521867
    :cond_4
    int-to-float v2, p3

    invoke-virtual {v4, v2, p2, p2}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 1521868
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_5
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/RectF;

    .line 1521869
    new-instance p2, Landroid/graphics/RectF;

    invoke-direct {p2, v2}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 1521870
    if-eqz v1, :cond_6

    invoke-virtual {v1, p2}, Landroid/graphics/RectF;->contains(Landroid/graphics/RectF;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1521871
    :cond_6
    invoke-virtual {v4, p2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 1521872
    invoke-interface {v3, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1521873
    :cond_7
    const/4 v2, 0x0

    new-array v2, v2, [Landroid/graphics/RectF;

    invoke-interface {v3, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Landroid/graphics/RectF;

    move-object v0, v2

    .line 1521874
    move-object v1, v0

    .line 1521875
    :goto_2
    iget-object v0, p0, LX/9fP;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Hx;

    .line 1521876
    invoke-virtual {v0, v1}, LX/8Hx;->a([Landroid/graphics/RectF;)V

    .line 1521877
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFilterName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8Hx;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1521878
    :cond_8
    iget-object v0, p0, LX/9fP;->b:Ljava/util/List;

    const/4 v1, 0x0

    new-array v1, v1, [Landroid/graphics/RectF;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/graphics/RectF;

    move-object v1, v0

    goto :goto_2
.end method
