.class public final LX/8zb;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/8zh;


# direct methods
.method public constructor <init>(LX/8zh;I)V
    .locals 0

    .prologue
    .line 1427692
    iput-object p1, p0, LX/8zb;->b:LX/8zh;

    iput p2, p0, LX/8zb;->a:I

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1427693
    new-instance v0, Lcom/facebook/composer/minutiae/activities/MinutiaeObjectsController$1$1;

    invoke-direct {v0, p0}, Lcom/facebook/composer/minutiae/activities/MinutiaeObjectsController$1$1;-><init>(LX/8zb;)V

    .line 1427694
    iget-object v1, p0, LX/8zb;->b:LX/8zh;

    const/4 v2, 0x0

    .line 1427695
    iput-boolean v2, v1, LX/8zh;->r:Z

    .line 1427696
    iget-object v1, p0, LX/8zb;->b:LX/8zh;

    const/4 v2, 0x1

    .line 1427697
    iput-boolean v2, v1, LX/8zh;->p:Z

    .line 1427698
    iget-object v1, p0, LX/8zb;->b:LX/8zh;

    invoke-static {v1}, LX/8zh;->f(LX/8zh;)V

    .line 1427699
    iget-object v1, p0, LX/8zb;->b:LX/8zh;

    iget-object v1, v1, LX/8zh;->a:LX/8zZ;

    .line 1427700
    iget-boolean v2, v1, LX/8zZ;->h:Z

    if-eqz v2, :cond_0

    .line 1427701
    :goto_0
    iget-object v0, p0, LX/8zb;->b:LX/8zh;

    iget-object v0, v0, LX/8zh;->f:LX/918;

    iget-object v1, p0, LX/8zb;->b:LX/8zh;

    iget-object v1, v1, LX/8zh;->c:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1427702
    iget-object v2, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v1, v2

    .line 1427703
    iget-object v2, p0, LX/8zb;->b:LX/8zh;

    iget-object v2, v2, LX/8zh;->i:LX/5LG;

    invoke-interface {v2}, LX/5LG;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/918;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1427704
    return-void

    .line 1427705
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, v1, LX/8zZ;->h:Z

    .line 1427706
    iput-object v0, v1, LX/8zZ;->i:Ljava/lang/Runnable;

    .line 1427707
    iget-object v2, v1, LX/8zZ;->e:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    invoke-virtual {v1, v2}, LX/3mY;->u_(I)V

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1427708
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v4, 0x0

    .line 1427709
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1427710
    check-cast v0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;

    .line 1427711
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;->o()Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;

    move-result-object v0

    .line 1427712
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1427713
    :cond_0
    invoke-virtual {p0, v4}, LX/8zb;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    .line 1427714
    :goto_0
    return-void

    .line 1427715
    :cond_1
    iget-object v1, p0, LX/8zb;->b:LX/8zh;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;->b()LX/0ut;

    move-result-object v0

    .line 1427716
    iput-object v0, v1, LX/8zh;->j:LX/0ut;

    .line 1427717
    iget-object p0, v1, LX/8zh;->a:LX/8zZ;

    iget-object p1, v1, LX/8zh;->i:LX/5LG;

    .line 1427718
    iput-object p1, p0, LX/8zZ;->f:LX/5LG;

    .line 1427719
    iget-object p0, v1, LX/8zh;->a:LX/8zZ;

    .line 1427720
    invoke-virtual {p0}, LX/8zZ;->e()I

    move-result p1

    .line 1427721
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1427722
    iget-object v0, p0, LX/8zZ;->e:LX/0Px;

    move-object v0, v0

    .line 1427723
    invoke-virtual {v3, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v3

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    iput-object v3, p0, LX/8zZ;->e:LX/0Px;

    .line 1427724
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    invoke-virtual {p0, p1, v3}, LX/3mY;->a(II)V

    .line 1427725
    const/4 p0, 0x0

    iput-boolean p0, v1, LX/8zh;->r:Z

    .line 1427726
    iget-boolean p0, v1, LX/8zh;->q:Z

    if-eqz p0, :cond_2

    .line 1427727
    invoke-static {v1}, LX/8zh;->f(LX/8zh;)V

    .line 1427728
    :cond_2
    iput-object v4, v1, LX/8zh;->n:Ljava/lang/String;

    .line 1427729
    iget-object p0, v1, LX/8zh;->h:LX/92D;

    .line 1427730
    const p1, 0xc60009

    const-string v3, "minutiae_objects_selector_time_to_scroll_load"

    invoke-virtual {p0, p1, v3}, LX/92A;->b(ILjava/lang/String;)V

    .line 1427731
    goto :goto_0
.end method
