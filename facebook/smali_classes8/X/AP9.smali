.class public LX/AP9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1E5;


# direct methods
.method public constructor <init>(LX/1E5;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1670025
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1670026
    iput-object p1, p0, LX/AP9;->a:LX/1E5;

    .line 1670027
    return-void
.end method


# virtual methods
.method public final a(LX/2rw;ZZZLX/5Rn;LX/ARN;)Z
    .locals 3
    .param p5    # LX/5Rn;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 1670028
    if-eqz p6, :cond_1

    invoke-interface {p6}, LX/ARN;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1670029
    :cond_0
    :goto_0
    return v0

    :cond_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v1, v2, :cond_0

    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v1

    if-lez v1, :cond_0

    if-nez p3, :cond_0

    if-nez p4, :cond_0

    sget-object v1, LX/5Rn;->NORMAL:LX/5Rn;

    if-ne p5, v1, :cond_0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1670030
    sget-object p3, LX/AP8;->a:[I

    invoke-virtual {p1}, LX/2rw;->ordinal()I

    move-result p4

    aget p3, p3, p4

    packed-switch p3, :pswitch_data_0

    move v1, v2

    .line 1670031
    :cond_2
    :goto_1
    move v1, v1

    .line 1670032
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 1670033
    :pswitch_0
    iget-object v1, p0, LX/AP9;->a:LX/1E5;

    invoke-virtual {v1}, LX/1E5;->a()Z

    move-result v1

    goto :goto_1

    .line 1670034
    :pswitch_1
    if-nez p2, :cond_3

    iget-object p3, p0, LX/AP9;->a:LX/1E5;

    invoke-virtual {p3}, LX/1E5;->a()Z

    move-result p3

    if-nez p3, :cond_2

    :cond_3
    move v1, v2

    goto :goto_1

    .line 1670035
    :pswitch_2
    if-eqz p2, :cond_4

    iget-object p3, p0, LX/AP9;->a:LX/1E5;

    invoke-virtual {p3}, LX/1E5;->b()Z

    move-result p3

    if-nez p3, :cond_2

    :cond_4
    move v1, v2

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
