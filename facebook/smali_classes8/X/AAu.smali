.class public final LX/AAu;
.super LX/0zP;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0zP",
        "<",
        "Lcom/facebook/adinterfaces/protocol/BoostedComponentDeleteMutationModels$BoostedComponentDeleteMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 12

    .prologue
    .line 1639330
    const-class v1, Lcom/facebook/adinterfaces/protocol/BoostedComponentDeleteMutationModels$BoostedComponentDeleteMutationModel;

    const v0, 0x63f14a0b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x3

    const-string v5, "BoostedComponentDeleteMutation"

    const-string v6, "c2ad766bf9dc430478d9b6e40a30b994"

    const-string v7, "boosted_component_delete"

    const-string v8, "44"

    const-string v9, "10155261854871729"

    const/4 v10, 0x0

    const-string v0, "short_term_cache_key_pyml"

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v11

    move-object v0, p0

    invoke-direct/range {v0 .. v11}, LX/0zP;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1639331
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1639235
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1639236
    sparse-switch v0, :sswitch_data_0

    .line 1639237
    :goto_0
    return-object p1

    .line 1639238
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1639239
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1639240
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1639241
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1639242
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1639243
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 1639244
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 1639245
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 1639246
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 1639247
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 1639248
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 1639249
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 1639250
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 1639251
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 1639252
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 1639253
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    .line 1639254
    :sswitch_10
    const-string p1, "16"

    goto :goto_0

    .line 1639255
    :sswitch_11
    const-string p1, "17"

    goto :goto_0

    .line 1639256
    :sswitch_12
    const-string p1, "18"

    goto :goto_0

    .line 1639257
    :sswitch_13
    const-string p1, "19"

    goto :goto_0

    .line 1639258
    :sswitch_14
    const-string p1, "20"

    goto :goto_0

    .line 1639259
    :sswitch_15
    const-string p1, "21"

    goto :goto_0

    .line 1639260
    :sswitch_16
    const-string p1, "22"

    goto :goto_0

    .line 1639261
    :sswitch_17
    const-string p1, "23"

    goto :goto_0

    .line 1639262
    :sswitch_18
    const-string p1, "24"

    goto :goto_0

    .line 1639263
    :sswitch_19
    const-string p1, "25"

    goto :goto_0

    .line 1639264
    :sswitch_1a
    const-string p1, "26"

    goto :goto_0

    .line 1639265
    :sswitch_1b
    const-string p1, "27"

    goto :goto_0

    .line 1639266
    :sswitch_1c
    const-string p1, "28"

    goto :goto_0

    .line 1639267
    :sswitch_1d
    const-string p1, "29"

    goto :goto_0

    .line 1639268
    :sswitch_1e
    const-string p1, "30"

    goto :goto_0

    .line 1639269
    :sswitch_1f
    const-string p1, "31"

    goto :goto_0

    .line 1639270
    :sswitch_20
    const-string p1, "32"

    goto :goto_0

    .line 1639271
    :sswitch_21
    const-string p1, "33"

    goto :goto_0

    .line 1639272
    :sswitch_22
    const-string p1, "34"

    goto :goto_0

    .line 1639273
    :sswitch_23
    const-string p1, "35"

    goto :goto_0

    .line 1639274
    :sswitch_24
    const-string p1, "36"

    goto :goto_0

    .line 1639275
    :sswitch_25
    const-string p1, "37"

    goto :goto_0

    .line 1639276
    :sswitch_26
    const-string p1, "38"

    goto :goto_0

    .line 1639277
    :sswitch_27
    const-string p1, "39"

    goto :goto_0

    .line 1639278
    :sswitch_28
    const-string p1, "40"

    goto :goto_0

    .line 1639279
    :sswitch_29
    const-string p1, "41"

    goto :goto_0

    .line 1639280
    :sswitch_2a
    const-string p1, "42"

    goto/16 :goto_0

    .line 1639281
    :sswitch_2b
    const-string p1, "43"

    goto/16 :goto_0

    .line 1639282
    :sswitch_2c
    const-string p1, "44"

    goto/16 :goto_0

    .line 1639283
    :sswitch_2d
    const-string p1, "45"

    goto/16 :goto_0

    .line 1639284
    :sswitch_2e
    const-string p1, "46"

    goto/16 :goto_0

    .line 1639285
    :sswitch_2f
    const-string p1, "47"

    goto/16 :goto_0

    .line 1639286
    :sswitch_30
    const-string p1, "48"

    goto/16 :goto_0

    .line 1639287
    :sswitch_31
    const-string p1, "49"

    goto/16 :goto_0

    .line 1639288
    :sswitch_32
    const-string p1, "50"

    goto/16 :goto_0

    .line 1639289
    :sswitch_33
    const-string p1, "51"

    goto/16 :goto_0

    .line 1639290
    :sswitch_34
    const-string p1, "52"

    goto/16 :goto_0

    .line 1639291
    :sswitch_35
    const-string p1, "53"

    goto/16 :goto_0

    .line 1639292
    :sswitch_36
    const-string p1, "54"

    goto/16 :goto_0

    .line 1639293
    :sswitch_37
    const-string p1, "55"

    goto/16 :goto_0

    .line 1639294
    :sswitch_38
    const-string p1, "56"

    goto/16 :goto_0

    .line 1639295
    :sswitch_39
    const-string p1, "57"

    goto/16 :goto_0

    .line 1639296
    :sswitch_3a
    const-string p1, "58"

    goto/16 :goto_0

    .line 1639297
    :sswitch_3b
    const-string p1, "59"

    goto/16 :goto_0

    .line 1639298
    :sswitch_3c
    const-string p1, "60"

    goto/16 :goto_0

    .line 1639299
    :sswitch_3d
    const-string p1, "61"

    goto/16 :goto_0

    .line 1639300
    :sswitch_3e
    const-string p1, "62"

    goto/16 :goto_0

    .line 1639301
    :sswitch_3f
    const-string p1, "63"

    goto/16 :goto_0

    .line 1639302
    :sswitch_40
    const-string p1, "64"

    goto/16 :goto_0

    .line 1639303
    :sswitch_41
    const-string p1, "65"

    goto/16 :goto_0

    .line 1639304
    :sswitch_42
    const-string p1, "66"

    goto/16 :goto_0

    .line 1639305
    :sswitch_43
    const-string p1, "67"

    goto/16 :goto_0

    .line 1639306
    :sswitch_44
    const-string p1, "68"

    goto/16 :goto_0

    .line 1639307
    :sswitch_45
    const-string p1, "69"

    goto/16 :goto_0

    .line 1639308
    :sswitch_46
    const-string p1, "70"

    goto/16 :goto_0

    .line 1639309
    :sswitch_47
    const-string p1, "71"

    goto/16 :goto_0

    .line 1639310
    :sswitch_48
    const-string p1, "72"

    goto/16 :goto_0

    .line 1639311
    :sswitch_49
    const-string p1, "73"

    goto/16 :goto_0

    .line 1639312
    :sswitch_4a
    const-string p1, "74"

    goto/16 :goto_0

    .line 1639313
    :sswitch_4b
    const-string p1, "75"

    goto/16 :goto_0

    .line 1639314
    :sswitch_4c
    const-string p1, "76"

    goto/16 :goto_0

    .line 1639315
    :sswitch_4d
    const-string p1, "77"

    goto/16 :goto_0

    .line 1639316
    :sswitch_4e
    const-string p1, "78"

    goto/16 :goto_0

    .line 1639317
    :sswitch_4f
    const-string p1, "79"

    goto/16 :goto_0

    .line 1639318
    :sswitch_50
    const-string p1, "80"

    goto/16 :goto_0

    .line 1639319
    :sswitch_51
    const-string p1, "81"

    goto/16 :goto_0

    .line 1639320
    :sswitch_52
    const-string p1, "82"

    goto/16 :goto_0

    .line 1639321
    :sswitch_53
    const-string p1, "83"

    goto/16 :goto_0

    .line 1639322
    :sswitch_54
    const-string p1, "84"

    goto/16 :goto_0

    .line 1639323
    :sswitch_55
    const-string p1, "85"

    goto/16 :goto_0

    .line 1639324
    :sswitch_56
    const-string p1, "86"

    goto/16 :goto_0

    .line 1639325
    :sswitch_57
    const-string p1, "87"

    goto/16 :goto_0

    .line 1639326
    :sswitch_58
    const-string p1, "88"

    goto/16 :goto_0

    .line 1639327
    :sswitch_59
    const-string p1, "89"

    goto/16 :goto_0

    .line 1639328
    :sswitch_5a
    const-string p1, "90"

    goto/16 :goto_0

    .line 1639329
    :sswitch_5b
    const-string p1, "91"

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x7f566515 -> :sswitch_2d
        -0x7e998586 -> :sswitch_f
        -0x7b752021 -> :sswitch_3
        -0x7531a756 -> :sswitch_20
        -0x7354b836 -> :sswitch_59
        -0x6e3ba572 -> :sswitch_13
        -0x6c1bed17 -> :sswitch_54
        -0x6a24640d -> :sswitch_43
        -0x6a02a4f4 -> :sswitch_30
        -0x69f19a9a -> :sswitch_1
        -0x680de62a -> :sswitch_26
        -0x6326fdb3 -> :sswitch_23
        -0x626f1062 -> :sswitch_11
        -0x5e743804 -> :sswitch_c
        -0x57984ae8 -> :sswitch_3b
        -0x5709d77d -> :sswitch_48
        -0x55ff6f9b -> :sswitch_2
        -0x5349037c -> :sswitch_2e
        -0x51484e72 -> :sswitch_19
        -0x513764de -> :sswitch_44
        -0x50cab1c8 -> :sswitch_8
        -0x4f1f32b4 -> :sswitch_4d
        -0x4eea3afb -> :sswitch_b
        -0x4aeac1fa -> :sswitch_4e
        -0x4ae70342 -> :sswitch_9
        -0x48fcb87a -> :sswitch_5a
        -0x46beb116 -> :sswitch_1f
        -0x4496acc9 -> :sswitch_27
        -0x41a91745 -> :sswitch_39
        -0x41143822 -> :sswitch_18
        -0x3c54de38 -> :sswitch_2b
        -0x3b85b241 -> :sswitch_47
        -0x39e54905 -> :sswitch_37
        -0x30b65c8f -> :sswitch_1c
        -0x2fab0379 -> :sswitch_46
        -0x2f1c601a -> :sswitch_21
        -0x25a646c8 -> :sswitch_1b
        -0x2511c384 -> :sswitch_2a
        -0x24e1906f -> :sswitch_4
        -0x2177e47b -> :sswitch_1d
        -0x201d08e7 -> :sswitch_35
        -0x1d6ce0bf -> :sswitch_33
        -0x1b87b280 -> :sswitch_22
        -0x17e48248 -> :sswitch_5
        -0x15db59af -> :sswitch_56
        -0x14283bca -> :sswitch_34
        -0x12efdeb3 -> :sswitch_28
        -0x8ca6426 -> :sswitch_7
        -0x6fe61e8 -> :sswitch_4f
        -0x587d3fa -> :sswitch_24
        -0x3e446ed -> :sswitch_1e
        -0x12603b3 -> :sswitch_41
        0x180aba4 -> :sswitch_17
        0x4cb0e44 -> :sswitch_57
        0x5fb57ca -> :sswitch_2c
        0xa1fa812 -> :sswitch_12
        0xc168ff8 -> :sswitch_a
        0xe50e2a0 -> :sswitch_51
        0x11850e88 -> :sswitch_3d
        0x15888c51 -> :sswitch_50
        0x18ce3dbb -> :sswitch_e
        0x214100e0 -> :sswitch_29
        0x2292beef -> :sswitch_42
        0x244e76e6 -> :sswitch_40
        0x26d0c0ff -> :sswitch_3c
        0x27208b4a -> :sswitch_3e
        0x291d8de0 -> :sswitch_3a
        0x2f8b060e -> :sswitch_4a
        0x3052e0ff -> :sswitch_15
        0x326dc744 -> :sswitch_38
        0x32e05a5f -> :sswitch_5b
        0x34e16755 -> :sswitch_0
        0x410878b1 -> :sswitch_36
        0x420eb51c -> :sswitch_14
        0x43ee5105 -> :sswitch_49
        0x44431ea4 -> :sswitch_10
        0x54ace343 -> :sswitch_3f
        0x54df6484 -> :sswitch_d
        0x5aa53d79 -> :sswitch_32
        0x5e7957c4 -> :sswitch_1a
        0x5eacdfdf -> :sswitch_53
        0x5f424068 -> :sswitch_16
        0x5f559782 -> :sswitch_4c
        0x63c03b07 -> :sswitch_25
        0x670b906a -> :sswitch_52
        0x6771e9f5 -> :sswitch_2f
        0x70f836af -> :sswitch_55
        0x732c0b14 -> :sswitch_4b
        0x73a026b5 -> :sswitch_31
        0x7506f93c -> :sswitch_45
        0x7c6b80b3 -> :sswitch_6
        0x7cf1f032 -> :sswitch_58
    .end sparse-switch
.end method
