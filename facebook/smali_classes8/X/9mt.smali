.class public final LX/9mt;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final b:Ljava/lang/Exception;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 1536251
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1536252
    iput-object p1, p0, LX/9mt;->b:Ljava/lang/Exception;

    .line 1536253
    const/4 v0, 0x0

    iput-object v0, p0, LX/9mt;->a:Ljava/lang/Object;

    .line 1536254
    return-void
.end method

.method private constructor <init>(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 1536247
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1536248
    const/4 v0, 0x0

    iput-object v0, p0, LX/9mt;->b:Ljava/lang/Exception;

    .line 1536249
    iput-object p1, p0, LX/9mt;->a:Ljava/lang/Object;

    .line 1536250
    return-void
.end method

.method public static a(Ljava/lang/Exception;)LX/9mt;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Exception;",
            ")",
            "LX/9mt",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1536246
    new-instance v0, LX/9mt;

    invoke-direct {v0, p0}, LX/9mt;-><init>(Ljava/lang/Exception;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Object;)LX/9mt;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:TT;>(TU;)",
            "LX/9mt",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1536245
    new-instance v0, LX/9mt;

    invoke-direct {v0, p0}, LX/9mt;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 1536241
    iget-object v0, p0, LX/9mt;->b:Ljava/lang/Exception;

    if-eqz v0, :cond_0

    .line 1536242
    iget-object v0, p0, LX/9mt;->b:Ljava/lang/Exception;

    throw v0

    .line 1536243
    :cond_0
    iget-object v0, p0, LX/9mt;->a:Ljava/lang/Object;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1536244
    iget-object v0, p0, LX/9mt;->a:Ljava/lang/Object;

    return-object v0
.end method
