.class public LX/8xm;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/0So;

.field public final c:LX/0hB;

.field public final d:LX/0iA;

.field public final e:Landroid/graphics/Rect;

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/view/View;",
            "Lcom/facebook/checkin/socialsearch/nux/SocialSearchFeedStoryNuxManager$FeedStoryNuxRunnable;",
            ">;"
        }
    .end annotation
.end field

.field public g:J

.field public h:I

.field public i:I

.field public j:I


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0So;LX/0hB;LX/0iA;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1424732
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1424733
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, LX/8xm;->e:Landroid/graphics/Rect;

    .line 1424734
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/8xm;->f:Ljava/util/Map;

    .line 1424735
    iput-object p1, p0, LX/8xm;->a:Landroid/content/Context;

    .line 1424736
    iput-object p2, p0, LX/8xm;->b:LX/0So;

    .line 1424737
    iput-object p3, p0, LX/8xm;->c:LX/0hB;

    .line 1424738
    iput-object p4, p0, LX/8xm;->d:LX/0iA;

    .line 1424739
    return-void
.end method

.method public static a$redex0(LX/8xm;J)V
    .locals 1

    .prologue
    .line 1424740
    iput-wide p1, p0, LX/8xm;->g:J

    .line 1424741
    iget-object v0, p0, LX/8xm;->e:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iput v0, p0, LX/8xm;->h:I

    .line 1424742
    iget-object v0, p0, LX/8xm;->e:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iput v0, p0, LX/8xm;->i:I

    .line 1424743
    return-void
.end method

.method public static b(LX/8xm;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1424744
    iget-object v0, p0, LX/8xm;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/nux/SocialSearchFeedStoryNuxManager$FeedStoryNuxRunnable;

    .line 1424745
    if-eqz v0, :cond_0

    .line 1424746
    const/4 p0, 0x1

    iput-boolean p0, v0, Lcom/facebook/checkin/socialsearch/nux/SocialSearchFeedStoryNuxManager$FeedStoryNuxRunnable;->d:Z

    .line 1424747
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;I)V
    .locals 4
    .param p2    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 1424748
    if-nez p1, :cond_0

    .line 1424749
    :goto_0
    return-void

    .line 1424750
    :cond_0
    invoke-static {p0, p1}, LX/8xm;->b(LX/8xm;Landroid/view/View;)V

    .line 1424751
    iget-object v0, p0, LX/8xm;->e:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1424752
    iget-object v0, p0, LX/8xm;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    invoke-static {p0, v0, v1}, LX/8xm;->a$redex0(LX/8xm;J)V

    .line 1424753
    new-instance v0, Lcom/facebook/checkin/socialsearch/nux/SocialSearchFeedStoryNuxManager$FeedStoryNuxRunnable;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/checkin/socialsearch/nux/SocialSearchFeedStoryNuxManager$FeedStoryNuxRunnable;-><init>(LX/8xm;Landroid/view/View;I)V

    .line 1424754
    iget-object v1, p0, LX/8xm;->f:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1424755
    const-wide/16 v2, 0x1f4

    invoke-virtual {p1, v0, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
