.class public LX/940;
.super LX/93Q;
.source ""


# instance fields
.field private final a:J

.field private final b:LX/0tX;

.field public final c:Ljava/lang/String;

.field public final d:LX/2rX;

.field public final e:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(LX/93q;LX/03V;LX/1Ck;Ljava/lang/Long;Ljava/lang/String;LX/2rX;LX/0tX;Landroid/content/res/Resources;)V
    .locals 2
    .param p1    # LX/93q;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Long;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/2rX;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1434386
    invoke-direct {p0, p1, p2, p3}, LX/93Q;-><init>(LX/93q;LX/03V;LX/1Ck;)V

    .line 1434387
    iput-object p7, p0, LX/940;->b:LX/0tX;

    .line 1434388
    invoke-virtual {p4}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, LX/940;->a:J

    .line 1434389
    iput-object p5, p0, LX/940;->c:Ljava/lang/String;

    .line 1434390
    iput-object p6, p0, LX/940;->d:LX/2rX;

    .line 1434391
    iput-object p8, p0, LX/940;->e:Landroid/content/res/Resources;

    .line 1434392
    return-void
.end method

.method public static a$redex0(LX/940;LX/2rX;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1434393
    if-eqz p1, :cond_2

    invoke-interface {p1}, LX/2rX;->c()LX/1Fd;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1434394
    const-string v0, "friends_of_friends"

    invoke-interface {p1}, LX/2rX;->c()LX/1Fd;

    move-result-object v1

    invoke-interface {v1}, LX/1Fd;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1434395
    iget-object v0, p0, LX/940;->e:Landroid/content/res/Resources;

    const v1, 0x7f081332

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1434396
    :goto_0
    return-object v0

    .line 1434397
    :cond_0
    const-string v0, "everyone"

    invoke-interface {p1}, LX/2rX;->c()LX/1Fd;

    move-result-object v1

    invoke-interface {v1}, LX/1Fd;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1434398
    iget-object v0, p0, LX/940;->e:Landroid/content/res/Resources;

    const v1, 0x7f08131d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1434399
    :cond_1
    const-string v0, "friends"

    invoke-interface {p1}, LX/2rX;->c()LX/1Fd;

    move-result-object v1

    invoke-interface {v1}, LX/1Fd;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1434400
    iget-object v0, p0, LX/940;->e:Landroid/content/res/Resources;

    const v1, 0x7f081331

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1434401
    :cond_2
    iget-object v0, p0, LX/940;->e:Landroid/content/res/Resources;

    const v1, 0x7f0812dc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(LX/2rX;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;
    .locals 2

    .prologue
    .line 1434402
    if-eqz p0, :cond_1

    invoke-interface {p0}, LX/2rX;->c()LX/1Fd;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1434403
    const-string v0, "friends_of_friends"

    invoke-interface {p0}, LX/2rX;->c()LX/1Fd;

    move-result-object v1

    invoke-interface {v1}, LX/1Fd;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1434404
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->GROUP:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    .line 1434405
    :goto_0
    return-object v0

    .line 1434406
    :cond_0
    const-string v0, "everyone"

    invoke-interface {p0}, LX/2rX;->c()LX/1Fd;

    move-result-object v1

    invoke-interface {v1}, LX/1Fd;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1434407
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->EVERYONE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    goto :goto_0

    .line 1434408
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->CUSTOM:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    .line 1434409
    invoke-super {p0}, LX/93Q;->a()V

    .line 1434410
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1434411
    new-instance v0, LX/7lN;

    invoke-direct {v0}, LX/7lN;-><init>()V

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->CUSTOM:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    .line 1434412
    iput-object v3, v0, LX/7lN;->a:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    .line 1434413
    move-object v0, v0

    .line 1434414
    iget-object v3, p0, LX/940;->e:Landroid/content/res/Resources;

    const v4, 0x7f0812dc

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1434415
    iput-object v3, v0, LX/7lN;->b:Ljava/lang/String;

    .line 1434416
    move-object v0, v0

    .line 1434417
    iget-object v3, p0, LX/940;->e:Landroid/content/res/Resources;

    const v4, 0x7f08131f

    new-array v5, v1, [Ljava/lang/Object;

    iget-object v6, p0, LX/940;->c:Ljava/lang/String;

    aput-object v6, v5, v2

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1434418
    iput-object v3, v0, LX/7lN;->c:Ljava/lang/String;

    .line 1434419
    move-object v0, v0

    .line 1434420
    invoke-virtual {v0}, LX/7lN;->a()Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    move-result-object v0

    .line 1434421
    iget-object v3, p0, LX/940;->d:LX/2rX;

    if-eqz v3, :cond_0

    .line 1434422
    new-instance v3, LX/7lN;

    invoke-direct {v3, v0}, LX/7lN;-><init>(Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;)V

    iget-object v0, p0, LX/940;->d:LX/2rX;

    invoke-static {v0}, LX/940;->b(LX/2rX;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v0

    .line 1434423
    iput-object v0, v3, LX/7lN;->a:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    .line 1434424
    move-object v0, v3

    .line 1434425
    iget-object v3, p0, LX/940;->d:LX/2rX;

    iget-object v4, p0, LX/940;->c:Ljava/lang/String;

    invoke-static {p0, v3, v4}, LX/940;->a$redex0(LX/940;LX/2rX;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1434426
    iput-object v3, v0, LX/7lN;->b:Ljava/lang/String;

    .line 1434427
    move-object v0, v0

    .line 1434428
    invoke-virtual {v0}, LX/7lN;->a()Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    move-result-object v0

    .line 1434429
    :cond_0
    new-instance v3, LX/7lP;

    invoke-direct {v3}, LX/7lP;-><init>()V

    .line 1434430
    iput-boolean v1, v3, LX/7lP;->a:Z

    .line 1434431
    move-object v3, v3

    .line 1434432
    iget-object v4, p0, LX/940;->d:LX/2rX;

    if-nez v4, :cond_2

    .line 1434433
    :goto_0
    iput-boolean v1, v3, LX/7lP;->b:Z

    .line 1434434
    move-object v1, v3

    .line 1434435
    invoke-virtual {v1, v0}, LX/7lP;->a(Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;)LX/7lP;

    move-result-object v0

    invoke-virtual {v0}, LX/7lP;->a()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    move-object v0, v0

    .line 1434436
    invoke-virtual {p0, v0}, LX/93Q;->a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    .line 1434437
    iget-boolean v1, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->d:Z

    if-nez v1, :cond_1

    .line 1434438
    :goto_1
    return-void

    .line 1434439
    :cond_1
    new-instance v1, LX/94M;

    invoke-direct {v1}, LX/94M;-><init>()V

    move-object v1, v1

    .line 1434440
    const-string v2, "profile_id"

    iget-wide v4, p0, LX/940;->a:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1434441
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    .line 1434442
    iget-object v2, p0, LX/93Q;->c:LX/1Ck;

    move-object v2, v2

    .line 1434443
    const-string v3, "fetch_profile_data"

    iget-object v4, p0, LX/940;->b:LX/0tX;

    invoke-virtual {v4, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    new-instance v4, LX/93z;

    invoke-direct {v4, p0, v0}, LX/93z;-><init>(LX/940;Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    invoke-virtual {v2, v3, v1, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1434444
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "timeline:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, LX/940;->a:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
