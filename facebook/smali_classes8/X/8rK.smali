.class public final LX/8rK;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1408735
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLEntity;)LX/1yA;
    .locals 15

    .prologue
    const/4 v2, 0x0

    .line 1408736
    if-nez p0, :cond_1

    .line 1408737
    :cond_0
    :goto_0
    return-object v2

    .line 1408738
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1408739
    const/4 v11, 0x1

    const/4 v4, 0x0

    .line 1408740
    if-nez p0, :cond_3

    .line 1408741
    :goto_1
    move v1, v4

    .line 1408742
    if-eqz v1, :cond_0

    .line 1408743
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1408744
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1408745
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1408746
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1408747
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 1408748
    const-string v1, "EntitySpanConverter.getGetEntityFbLinkGraphQL"

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1408749
    :cond_2
    new-instance v2, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetEntityFbLinkGraphQLModel;

    invoke-direct {v2, v0}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetEntityFbLinkGraphQLModel;-><init>(LX/15i;)V

    goto :goto_0

    .line 1408750
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v5

    .line 1408751
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1408752
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->v_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1408753
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->r()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    const/4 v3, 0x0

    .line 1408754
    if-nez v1, :cond_6

    .line 1408755
    :goto_2
    move v8, v3

    .line 1408756
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->t()LX/0Px;

    move-result-object v9

    .line 1408757
    if-eqz v9, :cond_5

    .line 1408758
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v1

    new-array v10, v1, [I

    move v3, v4

    .line 1408759
    :goto_3
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v1

    if-ge v3, v1, :cond_4

    .line 1408760
    invoke-virtual {v9, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLRedirectionInfo;

    const/4 v12, 0x0

    .line 1408761
    if-nez v1, :cond_7

    .line 1408762
    :goto_4
    move v1, v12

    .line 1408763
    aput v1, v10, v3

    .line 1408764
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_3

    .line 1408765
    :cond_4
    invoke-virtual {v0, v10, v11}, LX/186;->a([IZ)I

    move-result v1

    .line 1408766
    :goto_5
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->w_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1408767
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->j()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1408768
    const/4 v10, 0x7

    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 1408769
    invoke-virtual {v0, v4, v5}, LX/186;->b(II)V

    .line 1408770
    invoke-virtual {v0, v11, v6}, LX/186;->b(II)V

    .line 1408771
    const/4 v4, 0x2

    invoke-virtual {v0, v4, v7}, LX/186;->b(II)V

    .line 1408772
    const/4 v4, 0x3

    invoke-virtual {v0, v4, v8}, LX/186;->b(II)V

    .line 1408773
    const/4 v4, 0x4

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1408774
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1408775
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 1408776
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    .line 1408777
    invoke-virtual {v0, v4}, LX/186;->d(I)V

    goto/16 :goto_1

    :cond_5
    move v1, v4

    goto :goto_5

    .line 1408778
    :cond_6
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1408779
    const/4 v9, 0x1

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 1408780
    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 1408781
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    .line 1408782
    invoke-virtual {v0, v3}, LX/186;->d(I)V

    goto :goto_2

    .line 1408783
    :cond_7
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLRedirectionInfo;->a()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 1408784
    const/4 v14, 0x1

    invoke-virtual {v0, v14}, LX/186;->c(I)V

    .line 1408785
    invoke-virtual {v0, v12, v13}, LX/186;->b(II)V

    .line 1408786
    invoke-virtual {v0}, LX/186;->d()I

    move-result v12

    .line 1408787
    invoke-virtual {v0, v12}, LX/186;->d(I)V

    goto :goto_4
.end method
