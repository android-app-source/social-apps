.class public final LX/8jN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/stickers/model/StickerPack;

.field public final synthetic b:Lcom/facebook/stickers/client/StickerDownloadManager;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/client/StickerDownloadManager;Lcom/facebook/stickers/model/StickerPack;)V
    .locals 0

    .prologue
    .line 1392366
    iput-object p1, p0, LX/8jN;->b:Lcom/facebook/stickers/client/StickerDownloadManager;

    iput-object p2, p0, LX/8jN;->a:Lcom/facebook/stickers/model/StickerPack;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8

    .prologue
    .line 1392349
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    const/4 v3, 0x0

    .line 1392350
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/service/FetchStickerPacksResult;

    .line 1392351
    iget-object v1, v0, Lcom/facebook/stickers/service/FetchStickerPacksResult;->b:LX/0am;

    move-object v0, v1

    .line 1392352
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 1392353
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1392354
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v5

    move v2, v3

    :goto_0
    if-ge v2, v5, :cond_1

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/stickers/model/StickerPack;

    .line 1392355
    iget-object v6, v1, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v6, v6

    .line 1392356
    iget-object v7, p0, LX/8jN;->a:Lcom/facebook/stickers/model/StickerPack;

    .line 1392357
    iget-object p1, v7, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v7, p1

    .line 1392358
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1392359
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1392360
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1392361
    :cond_1
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1392362
    const-string v0, "stickerPacks"

    invoke-virtual {v2, v0, v4}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1392363
    const-string v0, "deletedStickerPacks"

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/facebook/stickers/model/StickerPack;

    iget-object v4, p0, LX/8jN;->a:Lcom/facebook/stickers/model/StickerPack;

    aput-object v4, v1, v3

    invoke-static {v1}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1392364
    iget-object v0, p0, LX/8jN;->b:Lcom/facebook/stickers/client/StickerDownloadManager;

    iget-object v0, v0, Lcom/facebook/stickers/client/StickerDownloadManager;->c:LX/0aG;

    const-string v1, "set_downloaded_sticker_packs"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    sget-object v4, Lcom/facebook/stickers/client/StickerDownloadManager;->a:Lcom/facebook/common/callercontext/CallerContext;

    const v5, 0x2dff509a

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 1392365
    return-object v0
.end method
