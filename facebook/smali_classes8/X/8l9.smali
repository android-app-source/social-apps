.class public final LX/8l9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/stickers/model/StickerPack;

.field public final synthetic b:Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;Lcom/facebook/stickers/model/StickerPack;)V
    .locals 0

    .prologue
    .line 1397395
    iput-object p1, p0, LX/8l9;->b:Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;

    iput-object p2, p0, LX/8l9;->a:Lcom/facebook/stickers/model/StickerPack;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x14dbb297

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1397396
    iget-object v1, p0, LX/8l9;->b:Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;

    iget-object v1, v1, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->o:LX/8kq;

    if-eqz v1, :cond_1

    .line 1397397
    iget-object v1, p0, LX/8l9;->b:Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;

    iget-object v1, v1, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->o:LX/8kq;

    iget-object v2, p0, LX/8l9;->a:Lcom/facebook/stickers/model/StickerPack;

    .line 1397398
    iget-object v4, v1, LX/8kq;->a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    iget-object v4, v4, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->m:LX/8kc;

    const/4 v5, 0x0

    iget-object v6, v1, LX/8kq;->a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    iget-object v6, v6, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->Q:LX/4m4;

    .line 1397399
    iget-object v7, v4, LX/8kc;->c:LX/8kd;

    .line 1397400
    const-string v8, "sticker_keyboard"

    invoke-static {v8}, LX/8j7;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    .line 1397401
    const-string p0, "action"

    const-string p1, "sticker_store_pack_opened"

    invoke-virtual {v8, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1397402
    const-string p0, "sticker_pack"

    .line 1397403
    iget-object p1, v2, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object p1, p1

    .line 1397404
    invoke-virtual {v8, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1397405
    const-string p0, "promoted_download"

    invoke-virtual {v8, p0, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1397406
    iget-object p0, v7, LX/8kd;->a:LX/8j7;

    invoke-virtual {p0, v8}, LX/8j7;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1397407
    new-instance v7, Landroid/content/Intent;

    iget-object v8, v4, LX/8kc;->a:Landroid/content/Context;

    const-class p0, Lcom/facebook/stickers/store/StickerStoreActivity;

    invoke-direct {v7, v8, p0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1397408
    const-string v8, "stickerPack"

    invoke-virtual {v7, v8, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1397409
    const-string v8, "startDownload"

    invoke-virtual {v7, v8, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1397410
    const-string v8, "stickerContext"

    invoke-virtual {v7, v8, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1397411
    sget-object v8, LX/4m4;->COMMENTS:LX/4m4;

    if-ne v6, v8, :cond_0

    .line 1397412
    iget-object v8, v4, LX/8kc;->e:LX/11i;

    sget-object p0, LX/8lJ;->d:LX/8lI;

    invoke-interface {v8, p0}, LX/11i;->a(LX/0Pq;)LX/11o;

    .line 1397413
    :cond_0
    iget-object v8, v4, LX/8kc;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object p0, v4, LX/8kc;->a:Landroid/content/Context;

    invoke-interface {v8, v7, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1397414
    iget-object v4, v1, LX/8kq;->a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    invoke-static {v4}, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->l(Lcom/facebook/stickers/keyboard/StickerKeyboardView;)V

    .line 1397415
    :cond_1
    const v1, 0x1f12b81d

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
