.class public final LX/8yo;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/3AZ;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/3Ab;

.field public b:LX/HJI;

.field public c:Landroid/text/TextUtils$TruncateAt;

.field public d:Z

.field public e:Landroid/text/Layout$Alignment;

.field public f:I

.field public g:I

.field public h:Z

.field public i:F

.field public j:I

.field public k:I

.field public l:I

.field public m:I

.field public n:I

.field public o:I

.field public p:Landroid/graphics/Typeface;

.field public q:Landroid/graphics/Typeface;

.field public r:I

.field public s:I

.field public t:I

.field public u:I

.field public final synthetic v:LX/3AZ;


# direct methods
.method public constructor <init>(LX/3AZ;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1426501
    iput-object p1, p0, LX/8yo;->v:LX/3AZ;

    .line 1426502
    move-object v0, p1

    .line 1426503
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1426504
    iput-boolean v1, p0, LX/8yo;->d:Z

    .line 1426505
    sget-object v0, LX/8yt;->e:Landroid/text/Layout$Alignment;

    iput-object v0, p0, LX/8yo;->e:Landroid/text/Layout$Alignment;

    .line 1426506
    const/high16 v0, -0x80000000

    iput v0, p0, LX/8yo;->f:I

    .line 1426507
    const v0, 0x7fffffff

    iput v0, p0, LX/8yo;->g:I

    .line 1426508
    iput-boolean v1, p0, LX/8yo;->h:Z

    .line 1426509
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, LX/8yo;->i:F

    .line 1426510
    const/high16 v0, -0x1000000

    iput v0, p0, LX/8yo;->j:I

    .line 1426511
    sget v0, LX/8yt;->a:I

    iput v0, p0, LX/8yo;->k:I

    .line 1426512
    sget v0, LX/8yt;->b:I

    iput v0, p0, LX/8yo;->l:I

    .line 1426513
    iput v1, p0, LX/8yo;->m:I

    .line 1426514
    const/16 v0, 0xd

    iput v0, p0, LX/8yo;->n:I

    .line 1426515
    const/4 v0, 0x0

    iput v0, p0, LX/8yo;->o:I

    .line 1426516
    sget-object v0, LX/8yt;->c:Landroid/graphics/Typeface;

    iput-object v0, p0, LX/8yo;->p:Landroid/graphics/Typeface;

    .line 1426517
    sget-object v0, LX/8yt;->d:Landroid/graphics/Typeface;

    iput-object v0, p0, LX/8yo;->q:Landroid/graphics/Typeface;

    .line 1426518
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1426519
    const-string v0, "LinkableTextWithEntitiesComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1426520
    if-ne p0, p1, :cond_1

    .line 1426521
    :cond_0
    :goto_0
    return v0

    .line 1426522
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1426523
    goto :goto_0

    .line 1426524
    :cond_3
    check-cast p1, LX/8yo;

    .line 1426525
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1426526
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1426527
    if-eq v2, v3, :cond_0

    .line 1426528
    iget-object v2, p0, LX/8yo;->a:LX/3Ab;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/8yo;->a:LX/3Ab;

    iget-object v3, p1, LX/8yo;->a:LX/3Ab;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1426529
    goto :goto_0

    .line 1426530
    :cond_5
    iget-object v2, p1, LX/8yo;->a:LX/3Ab;

    if-nez v2, :cond_4

    .line 1426531
    :cond_6
    iget-object v2, p0, LX/8yo;->b:LX/HJI;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/8yo;->b:LX/HJI;

    iget-object v3, p1, LX/8yo;->b:LX/HJI;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1426532
    goto :goto_0

    .line 1426533
    :cond_8
    iget-object v2, p1, LX/8yo;->b:LX/HJI;

    if-nez v2, :cond_7

    .line 1426534
    :cond_9
    iget-object v2, p0, LX/8yo;->c:Landroid/text/TextUtils$TruncateAt;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/8yo;->c:Landroid/text/TextUtils$TruncateAt;

    iget-object v3, p1, LX/8yo;->c:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, Landroid/text/TextUtils$TruncateAt;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 1426535
    goto :goto_0

    .line 1426536
    :cond_b
    iget-object v2, p1, LX/8yo;->c:Landroid/text/TextUtils$TruncateAt;

    if-nez v2, :cond_a

    .line 1426537
    :cond_c
    iget-boolean v2, p0, LX/8yo;->d:Z

    iget-boolean v3, p1, LX/8yo;->d:Z

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 1426538
    goto :goto_0

    .line 1426539
    :cond_d
    iget-object v2, p0, LX/8yo;->e:Landroid/text/Layout$Alignment;

    if-eqz v2, :cond_f

    iget-object v2, p0, LX/8yo;->e:Landroid/text/Layout$Alignment;

    iget-object v3, p1, LX/8yo;->e:Landroid/text/Layout$Alignment;

    invoke-virtual {v2, v3}, Landroid/text/Layout$Alignment;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    :cond_e
    move v0, v1

    .line 1426540
    goto :goto_0

    .line 1426541
    :cond_f
    iget-object v2, p1, LX/8yo;->e:Landroid/text/Layout$Alignment;

    if-nez v2, :cond_e

    .line 1426542
    :cond_10
    iget v2, p0, LX/8yo;->f:I

    iget v3, p1, LX/8yo;->f:I

    if-eq v2, v3, :cond_11

    move v0, v1

    .line 1426543
    goto :goto_0

    .line 1426544
    :cond_11
    iget v2, p0, LX/8yo;->g:I

    iget v3, p1, LX/8yo;->g:I

    if-eq v2, v3, :cond_12

    move v0, v1

    .line 1426545
    goto :goto_0

    .line 1426546
    :cond_12
    iget-boolean v2, p0, LX/8yo;->h:Z

    iget-boolean v3, p1, LX/8yo;->h:Z

    if-eq v2, v3, :cond_13

    move v0, v1

    .line 1426547
    goto/16 :goto_0

    .line 1426548
    :cond_13
    iget v2, p0, LX/8yo;->i:F

    iget v3, p1, LX/8yo;->i:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_14

    move v0, v1

    .line 1426549
    goto/16 :goto_0

    .line 1426550
    :cond_14
    iget v2, p0, LX/8yo;->j:I

    iget v3, p1, LX/8yo;->j:I

    if-eq v2, v3, :cond_15

    move v0, v1

    .line 1426551
    goto/16 :goto_0

    .line 1426552
    :cond_15
    iget v2, p0, LX/8yo;->k:I

    iget v3, p1, LX/8yo;->k:I

    if-eq v2, v3, :cond_16

    move v0, v1

    .line 1426553
    goto/16 :goto_0

    .line 1426554
    :cond_16
    iget v2, p0, LX/8yo;->l:I

    iget v3, p1, LX/8yo;->l:I

    if-eq v2, v3, :cond_17

    move v0, v1

    .line 1426555
    goto/16 :goto_0

    .line 1426556
    :cond_17
    iget v2, p0, LX/8yo;->m:I

    iget v3, p1, LX/8yo;->m:I

    if-eq v2, v3, :cond_18

    move v0, v1

    .line 1426557
    goto/16 :goto_0

    .line 1426558
    :cond_18
    iget v2, p0, LX/8yo;->n:I

    iget v3, p1, LX/8yo;->n:I

    if-eq v2, v3, :cond_19

    move v0, v1

    .line 1426559
    goto/16 :goto_0

    .line 1426560
    :cond_19
    iget v2, p0, LX/8yo;->o:I

    iget v3, p1, LX/8yo;->o:I

    if-eq v2, v3, :cond_1a

    move v0, v1

    .line 1426561
    goto/16 :goto_0

    .line 1426562
    :cond_1a
    iget-object v2, p0, LX/8yo;->p:Landroid/graphics/Typeface;

    if-eqz v2, :cond_1c

    iget-object v2, p0, LX/8yo;->p:Landroid/graphics/Typeface;

    iget-object v3, p1, LX/8yo;->p:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/graphics/Typeface;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1d

    :cond_1b
    move v0, v1

    .line 1426563
    goto/16 :goto_0

    .line 1426564
    :cond_1c
    iget-object v2, p1, LX/8yo;->p:Landroid/graphics/Typeface;

    if-nez v2, :cond_1b

    .line 1426565
    :cond_1d
    iget-object v2, p0, LX/8yo;->q:Landroid/graphics/Typeface;

    if-eqz v2, :cond_1f

    iget-object v2, p0, LX/8yo;->q:Landroid/graphics/Typeface;

    iget-object v3, p1, LX/8yo;->q:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/graphics/Typeface;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_20

    :cond_1e
    move v0, v1

    .line 1426566
    goto/16 :goto_0

    .line 1426567
    :cond_1f
    iget-object v2, p1, LX/8yo;->q:Landroid/graphics/Typeface;

    if-nez v2, :cond_1e

    .line 1426568
    :cond_20
    iget v2, p0, LX/8yo;->r:I

    iget v3, p1, LX/8yo;->r:I

    if-eq v2, v3, :cond_21

    move v0, v1

    .line 1426569
    goto/16 :goto_0

    .line 1426570
    :cond_21
    iget v2, p0, LX/8yo;->s:I

    iget v3, p1, LX/8yo;->s:I

    if-eq v2, v3, :cond_22

    move v0, v1

    .line 1426571
    goto/16 :goto_0

    .line 1426572
    :cond_22
    iget v2, p0, LX/8yo;->t:I

    iget v3, p1, LX/8yo;->t:I

    if-eq v2, v3, :cond_23

    move v0, v1

    .line 1426573
    goto/16 :goto_0

    .line 1426574
    :cond_23
    iget v2, p0, LX/8yo;->u:I

    iget v3, p1, LX/8yo;->u:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1426575
    goto/16 :goto_0
.end method
