.class public final LX/AJV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AJX;


# direct methods
.method public constructor <init>(LX/AJX;)V
    .locals 0

    .prologue
    .line 1661201
    iput-object p1, p0, LX/AJV;->a:LX/AJX;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1661210
    sget-object v0, LX/AJX;->a:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1661211
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1661202
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1661203
    if-nez p1, :cond_0

    .line 1661204
    :goto_0
    return-void

    .line 1661205
    :cond_0
    iget-object v1, p0, LX/AJV;->a:LX/AJX;

    .line 1661206
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1661207
    check-cast v0, Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel;

    invoke-static {v0}, LX/AJU;->a(Lcom/facebook/audience/sharesheet/protocol/FetchRankedAudienceQueryModels$FetchRankedAudienceQueryModel;)LX/0Px;

    move-result-object v0

    .line 1661208
    iput-object v0, v1, LX/AJX;->f:LX/0Px;

    .line 1661209
    goto :goto_0
.end method
