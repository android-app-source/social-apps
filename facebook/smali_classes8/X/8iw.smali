.class public LX/8iw;
.super LX/3RX;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/8iw;


# instance fields
.field private final a:Landroid/media/AudioManager;

.field public b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/7Cc;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8iQ;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private e:LX/3RZ;


# direct methods
.method public constructor <init>(Landroid/media/AudioManager;LX/0Or;LX/0Or;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/3RZ;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/media/AudioManager;",
            "LX/0Or",
            "<",
            "LX/7Cb;",
            ">;",
            "LX/0Or",
            "<",
            "LX/7Cc;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8iQ;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/3RZ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1392070
    invoke-direct {p0, p2, p3, p6}, LX/3RX;-><init>(LX/0Or;LX/0Or;LX/3RZ;)V

    .line 1392071
    iput-object p1, p0, LX/8iw;->a:Landroid/media/AudioManager;

    .line 1392072
    iput-object p3, p0, LX/8iw;->b:LX/0Or;

    .line 1392073
    iput-object p4, p0, LX/8iw;->c:LX/0Ot;

    .line 1392074
    iput-object p5, p0, LX/8iw;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1392075
    iput-object p6, p0, LX/8iw;->e:LX/3RZ;

    .line 1392076
    return-void
.end method

.method public static b(LX/0QB;)LX/8iw;
    .locals 10

    .prologue
    .line 1392051
    sget-object v0, LX/8iw;->f:LX/8iw;

    if-nez v0, :cond_1

    .line 1392052
    const-class v1, LX/8iw;

    monitor-enter v1

    .line 1392053
    :try_start_0
    sget-object v0, LX/8iw;->f:LX/8iw;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1392054
    if-eqz v2, :cond_0

    .line 1392055
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1392056
    new-instance v3, LX/8iw;

    invoke-static {v0}, LX/19T;->b(LX/0QB;)Landroid/media/AudioManager;

    move-result-object v4

    check-cast v4, Landroid/media/AudioManager;

    const/16 v5, 0x3563

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x3564

    invoke-static {v0, v6}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x3566

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v8

    check-cast v8, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/3RZ;->b(LX/0QB;)LX/3RZ;

    move-result-object v9

    check-cast v9, LX/3RZ;

    invoke-direct/range {v3 .. v9}, LX/8iw;-><init>(Landroid/media/AudioManager;LX/0Or;LX/0Or;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/3RZ;)V

    .line 1392057
    move-object v0, v3

    .line 1392058
    sput-object v0, LX/8iw;->f:LX/8iw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1392059
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1392060
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1392061
    :cond_1
    sget-object v0, LX/8iw;->f:LX/8iw;

    return-object v0

    .line 1392062
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1392063
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1392065
    invoke-virtual {p0}, LX/3RX;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1392066
    iget-object v0, p0, LX/8iw;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Cc;

    invoke-virtual {v0, p1}, LX/7Cc;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 1392067
    iget-object v0, p0, LX/8iw;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8iQ;

    invoke-virtual {v0, v1}, LX/8iQ;->a(I)F

    move-result v0

    move v0, v0

    .line 1392068
    invoke-virtual {p0, p1, v0}, LX/3RX;->a(Ljava/lang/String;F)LX/7Cb;

    .line 1392069
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1392064
    iget-object v1, p0, LX/8iw;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/1rO;->b:LX/0Tn;

    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/8iw;->a:Landroid/media/AudioManager;

    iget-object v2, p0, LX/8iw;->e:LX/3RZ;

    invoke-virtual {v2}, LX/3RZ;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    if-lez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
