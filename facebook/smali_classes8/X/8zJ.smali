.class public LX/8zJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8z3;


# instance fields
.field private final a:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1427149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1427150
    iput-object p1, p0, LX/8zJ;->a:Landroid/content/res/Resources;

    .line 1427151
    return-void
.end method

.method private a(Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;Ljava/util/Map;Ljava/util/Map;)Ljava/lang/String;
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLInterfaces$MinutiaePreviewTemplate;",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;",
            "Ljava/lang/CharSequence;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLInterfaces$MinutiaePreviewTemplate$TemplateTokens;",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1427137
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1427138
    const/4 v0, 0x0

    .line 1427139
    invoke-virtual {p1}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;->b()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    .line 1427140
    new-instance v2, LX/8zH;

    invoke-direct {v2, p0}, LX/8zH;-><init>(LX/8zJ;)V

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1427141
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel$TemplateTokensModel;

    .line 1427142
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel$TemplateTokensModel;->b()Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 1427143
    if-eqz v1, :cond_1

    .line 1427144
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel$TemplateTokensModel;->a()I

    move-result v5

    add-int/2addr v5, v2

    invoke-virtual {v3, v5, v1}, Ljava/lang/StringBuilder;->insert(ILjava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 1427145
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel$TemplateTokensModel;->a()I

    move-result v5

    add-int/2addr v5, v2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {p3, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1427146
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    add-int/2addr v0, v2

    :goto_1
    move v2, v0

    .line 1427147
    goto :goto_0

    .line 1427148
    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method private a(LX/8z5;LX/8z7;Landroid/text/SpannableStringBuilder;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;)V
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8z5;",
            "LX/8z7;",
            "Landroid/text/SpannableStringBuilder;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLInterfaces$MinutiaePreviewTemplate$TemplateTokens;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;",
            "Ljava/lang/CharSequence;",
            ">;",
            "Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLInterfaces$MinutiaePreviewTemplate;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1427113
    invoke-virtual/range {p3 .. p3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->length()I

    move-result v3

    sub-int v18, v2, v3

    .line 1427114
    move-object/from16 v0, p2

    iget-object v2, v0, LX/8z7;->a:Landroid/text/style/CharacterStyle;

    invoke-virtual/range {p3 .. p3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    const/16 v4, 0x21

    move-object/from16 v0, p3

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1427115
    invoke-virtual/range {p7 .. p7}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;->b()LX/0Px;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, LX/0Px;->size()I

    move-result v20

    const/4 v2, 0x0

    move/from16 v17, v2

    :goto_0
    move/from16 v0, v17

    move/from16 v1, v20

    if-ge v0, v1, :cond_4

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel$TemplateTokensModel;

    .line 1427116
    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1427117
    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int v3, v3, v18

    add-int/lit8 v7, v3, 0x3

    .line 1427118
    invoke-virtual {v2}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel$TemplateTokensModel;->b()Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    move-result-object v3

    move-object/from16 v0, p6

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v3

    add-int v8, v7, v3

    .line 1427119
    invoke-virtual {v2}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel$TemplateTokensModel;->b()Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->PEOPLE:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    if-eq v3, v4, :cond_1

    invoke-virtual {v2}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel$TemplateTokensModel;->b()Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->PERSON:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    if-eq v3, v4, :cond_1

    .line 1427120
    invoke-virtual {v2}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel$TemplateTokensModel;->b()Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    move-result-object v4

    move-object/from16 v0, p1

    iget-object v5, v0, LX/8z5;->h:LX/8z9;

    move-object/from16 v0, p2

    iget-object v6, v0, LX/8z7;->b:Landroid/text/style/CharacterStyle;

    const/4 v9, -0x1

    const/4 v10, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p3

    invoke-static/range {v2 .. v10}, LX/8zJ;->a(LX/8zJ;Landroid/text/SpannableStringBuilder;Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;LX/8z9;Landroid/text/style/CharacterStyle;IIIZ)V

    .line 1427121
    :cond_0
    :goto_1
    add-int/lit8 v2, v17, 0x1

    move/from16 v17, v2

    goto :goto_0

    .line 1427122
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8zJ;->a:Landroid/content/res/Resources;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/8z5;->b(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v21

    .line 1427123
    move-object/from16 v0, p1

    iget-object v2, v0, LX/8z5;->d:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1427124
    const/4 v15, 0x0

    :goto_2
    move-object/from16 v0, p1

    iget-object v2, v0, LX/8z5;->d:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v15, v2, :cond_2

    .line 1427125
    move-object/from16 v0, p1

    iget-object v2, v0, LX/8z5;->d:LX/0Px;

    invoke-virtual {v2, v15}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1427126
    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    add-int v13, v7, v3

    .line 1427127
    sget-object v10, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->PERSON:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    move-object/from16 v0, p1

    iget-object v11, v0, LX/8z5;->h:LX/8z9;

    move-object/from16 v0, p2

    iget-object v12, v0, LX/8z7;->b:Landroid/text/style/CharacterStyle;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int v14, v13, v2

    const/16 v16, 0x0

    move-object/from16 v8, p0

    move-object/from16 v9, p3

    invoke-static/range {v8 .. v16}, LX/8zJ;->a(LX/8zJ;Landroid/text/SpannableStringBuilder;Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;LX/8z9;Landroid/text/style/CharacterStyle;IIIZ)V

    .line 1427128
    add-int/lit8 v15, v15, 0x1

    goto :goto_2

    .line 1427129
    :cond_2
    move-object/from16 v0, p1

    iget-object v2, v0, LX/8z5;->i:Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    if-eqz v2, :cond_3

    .line 1427130
    move-object/from16 v0, p1

    iget-object v2, v0, LX/8z5;->i:Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    invoke-virtual {v2}, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->b()Ljava/lang/String;

    move-result-object v2

    .line 1427131
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->PERSON:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    move-object/from16 v0, p1

    iget-object v5, v0, LX/8z5;->h:LX/8z9;

    move-object/from16 v0, p2

    iget-object v6, v0, LX/8z7;->b:Landroid/text/style/CharacterStyle;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int v8, v7, v2

    const/4 v9, -0x1

    const/4 v10, 0x1

    move-object/from16 v2, p0

    move-object/from16 v3, p3

    invoke-static/range {v2 .. v10}, LX/8zJ;->a(LX/8zJ;Landroid/text/SpannableStringBuilder;Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;LX/8z9;Landroid/text/style/CharacterStyle;IIIZ)V

    .line 1427132
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8zJ;->a:Landroid/content/res/Resources;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/8z5;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v2

    .line 1427133
    if-eqz v2, :cond_0

    .line 1427134
    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    add-int/2addr v7, v3

    .line 1427135
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->PEOPLE:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    move-object/from16 v0, p1

    iget-object v5, v0, LX/8z5;->h:LX/8z9;

    move-object/from16 v0, p2

    iget-object v6, v0, LX/8z7;->b:Landroid/text/style/CharacterStyle;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int v8, v7, v2

    const/4 v9, -0x1

    const/4 v10, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p3

    invoke-static/range {v2 .. v10}, LX/8zJ;->a(LX/8zJ;Landroid/text/SpannableStringBuilder;Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;LX/8z9;Landroid/text/style/CharacterStyle;IIIZ)V

    goto/16 :goto_1

    .line 1427136
    :cond_4
    return-void
.end method

.method private static a(LX/8zJ;Landroid/text/SpannableStringBuilder;Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;LX/8z9;Landroid/text/style/CharacterStyle;IIIZ)V
    .locals 7

    .prologue
    const/16 v6, 0x21

    .line 1427109
    invoke-static {p4}, Landroid/text/style/CharacterStyle;->wrap(Landroid/text/style/CharacterStyle;)Landroid/text/style/CharacterStyle;

    move-result-object v0

    invoke-virtual {p1, v0, p5, p6, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1427110
    if-eqz p3, :cond_0

    .line 1427111
    new-instance v0, LX/8zG;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move v4, p7

    move v5, p8

    invoke-direct/range {v0 .. v5}, LX/8zG;-><init>(LX/8zJ;Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;LX/8z9;IZ)V

    invoke-virtual {p1, v0, p5, p6, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1427112
    :cond_0
    return-void
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;LX/8z9;IZ)V
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1427039
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1427040
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1427041
    sget-object v0, LX/8zI;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1427042
    :goto_0
    return-void

    .line 1427043
    :pswitch_0
    invoke-interface {p1}, LX/8z9;->a()V

    goto :goto_0

    .line 1427044
    :pswitch_1
    if-eqz p3, :cond_0

    .line 1427045
    invoke-interface {p1}, LX/8z9;->e()V

    goto :goto_0

    .line 1427046
    :cond_0
    invoke-interface {p1, p2}, LX/8z9;->a(I)V

    goto :goto_0

    .line 1427047
    :pswitch_2
    invoke-interface {p1}, LX/8z9;->b()V

    goto :goto_0

    .line 1427048
    :pswitch_3
    invoke-interface {p1}, LX/8z9;->c()V

    goto :goto_0

    .line 1427049
    :pswitch_4
    invoke-interface {p1}, LX/8z9;->d()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static b(LX/0QB;)LX/8zJ;
    .locals 2

    .prologue
    .line 1427107
    new-instance v1, LX/8zJ;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-direct {v1, v0}, LX/8zJ;-><init>(Landroid/content/res/Resources;)V

    .line 1427108
    return-object v1
.end method

.method private b(LX/8z5;)Ljava/util/Map;
    .locals 7
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8z5;",
            ")",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 1427089
    iget-object v0, p1, LX/8z5;->a:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1427090
    iget-object v1, p0, LX/8zJ;->a:Landroid/content/res/Resources;

    invoke-virtual {p1, v1}, LX/8z5;->b(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    .line 1427091
    iget-object v2, p1, LX/8z5;->b:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1427092
    iget-object v3, v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1427093
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v3

    .line 1427094
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->OBJECT:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    iget-object v5, v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {v5}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->c()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1427095
    if-eqz v2, :cond_0

    .line 1427096
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->PLACE:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    invoke-virtual {v2}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1427097
    :cond_0
    invoke-virtual {p1}, LX/8z5;->a()I

    move-result v2

    if-le v2, v6, :cond_2

    .line 1427098
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->PEOPLE:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    invoke-interface {v3, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1427099
    :goto_0
    iget-object v1, v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {v1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v0, v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-boolean v0, p1, LX/8z5;->g:Z

    if-eqz v0, :cond_1

    .line 1427100
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, LX/3rK;->a(Ljava/util/Locale;)I

    move-result v0

    .line 1427101
    if-ne v6, v0, :cond_3

    .line 1427102
    const-string v0, " \u200c"

    .line 1427103
    :goto_1
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->ICON:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1427104
    :cond_1
    return-object v3

    .line 1427105
    :cond_2
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->PERSON:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    invoke-interface {v3, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1427106
    :cond_3
    const-string v0, "\u200c "

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/8z5;)Landroid/text/SpannableStringBuilder;
    .locals 2

    .prologue
    .line 1427088
    new-instance v0, LX/8z8;

    iget-object v1, p0, LX/8zJ;->a:Landroid/content/res/Resources;

    invoke-direct {v0, v1}, LX/8z8;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v0}, LX/8z8;->a()LX/8z7;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LX/8zJ;->a(LX/8z5;LX/8z7;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/8z5;LX/8z7;)Landroid/text/SpannableStringBuilder;
    .locals 8

    .prologue
    .line 1427050
    iget-object v0, p1, LX/8z5;->a:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1427051
    iget-object v2, p1, LX/8z5;->b:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1427052
    iget-object v1, p0, LX/8zJ;->a:Landroid/content/res/Resources;

    invoke-virtual {p1, v1}, LX/8z5;->b(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v4

    .line 1427053
    invoke-virtual {p1}, LX/8z5;->a()I

    move-result v5

    .line 1427054
    iget-object v1, p1, LX/8z5;->i:Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    if-eqz v1, :cond_1

    iget-object v1, p1, LX/8z5;->i:Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    .line 1427055
    iget-object v3, v1, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->b:Ljava/lang/String;

    move-object v1, v3

    .line 1427056
    :goto_0
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 1427057
    const/4 v6, 0x1

    .line 1427058
    if-nez v2, :cond_3

    if-nez v4, :cond_3

    if-nez v1, :cond_3

    .line 1427059
    iget-object v6, v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->verb:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    invoke-virtual {v6}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;->F()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v6

    .line 1427060
    :goto_1
    move-object v7, v6

    .line 1427061
    invoke-direct {p0, p1}, LX/8zJ;->b(LX/8z5;)Ljava/util/Map;

    move-result-object v6

    .line 1427062
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v5

    .line 1427063
    invoke-direct {p0, v7, v6, v5}, LX/8zJ;->a(Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;Ljava/util/Map;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    .line 1427064
    iget-boolean v1, p1, LX/8z5;->f:Z

    if-eqz v1, :cond_2

    .line 1427065
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " \u2014 "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1427066
    invoke-virtual {v3, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    .line 1427067
    invoke-direct/range {v0 .. v7}, LX/8zJ;->a(LX/8z5;LX/8z7;Landroid/text/SpannableStringBuilder;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;)V

    .line 1427068
    :goto_2
    iget-boolean v0, p1, LX/8z5;->g:Z

    if-eqz v0, :cond_0

    .line 1427069
    iget-object v0, p1, LX/8z5;->a:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1427070
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;->ICON:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    invoke-interface {v6, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1427071
    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x200c

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 1427072
    if-gez v1, :cond_9

    .line 1427073
    :cond_0
    :goto_3
    return-object v3

    .line 1427074
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 1427075
    :cond_2
    invoke-virtual {v3, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_2

    .line 1427076
    :cond_3
    if-eqz v2, :cond_5

    if-eqz v4, :cond_5

    .line 1427077
    if-le v5, v6, :cond_4

    .line 1427078
    iget-object v6, v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->verb:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    invoke-virtual {v6}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;->H()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v6

    goto :goto_1

    .line 1427079
    :cond_4
    iget-object v6, v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->verb:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    invoke-virtual {v6}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;->J()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v6

    goto :goto_1

    .line 1427080
    :cond_5
    if-eqz v2, :cond_6

    if-eqz v1, :cond_6

    .line 1427081
    iget-object v6, v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->verb:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    invoke-virtual {v6}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;->J()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v6

    goto :goto_1

    .line 1427082
    :cond_6
    if-eqz v2, :cond_7

    .line 1427083
    iget-object v6, v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->verb:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    invoke-virtual {v6}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;->E()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v6

    goto :goto_1

    .line 1427084
    :cond_7
    if-le v5, v6, :cond_8

    .line 1427085
    iget-object v6, v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->verb:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    invoke-virtual {v6}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;->G()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v6

    goto :goto_1

    .line 1427086
    :cond_8
    iget-object v6, v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->verb:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    invoke-virtual {v6}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;->I()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v6

    goto/16 :goto_1

    .line 1427087
    :cond_9
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a()Landroid/net/Uri;

    move-result-object v2

    add-int/lit8 v4, v1, 0x1

    const/16 v5, 0x21

    invoke-virtual {v3, v2, v1, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_3
.end method
