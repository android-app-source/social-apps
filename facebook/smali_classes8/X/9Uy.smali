.class public LX/9Uy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public final c:Landroid/os/Handler;

.field public d:I

.field public e:LX/9Ut;

.field public f:Z

.field public g:J


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/9Ut;)V
    .locals 1

    .prologue
    .line 1498812
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1498813
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, LX/9Uy;->c:Landroid/os/Handler;

    .line 1498814
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/9Uy;->f:Z

    .line 1498815
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    .line 1498816
    mul-int/2addr v0, v0

    iput v0, p0, LX/9Uy;->d:I

    .line 1498817
    iput-object p2, p0, LX/9Uy;->e:LX/9Ut;

    .line 1498818
    return-void
.end method

.method public static final a()J
    .locals 2

    .prologue
    .line 1498819
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1498820
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 1498821
    :goto_0
    return v2

    .line 1498822
    :pswitch_0
    iput-boolean v2, p0, LX/9Uy;->f:Z

    .line 1498823
    invoke-static {}, LX/9Uy;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/9Uy;->g:J

    .line 1498824
    iget-object v0, p0, LX/9Uy;->e:LX/9Ut;

    invoke-interface {v0}, LX/9Ut;->a()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
