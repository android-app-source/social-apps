.class public LX/9h4;
.super LX/9gr;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/9gr",
        "<",
        "Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromMediaSetTokenModel;",
        "Lcom/facebook/photos/mediafetcher/query/param/MediaTypeQueryParam;",
        "LX/5kD;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:LX/9h9;

.field private final c:LX/03V;

.field private final d:Ljava/lang/String;

.field private final e:LX/0sX;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/mediafetcher/query/param/MediaTypeQueryParam;Lcom/facebook/common/callercontext/CallerContext;LX/9h9;LX/03V;LX/0sX;)V
    .locals 1
    .param p1    # Lcom/facebook/photos/mediafetcher/query/param/MediaTypeQueryParam;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1526042
    const-class v0, LX/5kD;

    invoke-direct {p0, p1, v0, p2}, LX/9gr;-><init>(Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;Ljava/lang/Class;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1526043
    iput-object p3, p0, LX/9h4;->b:LX/9h9;

    .line 1526044
    iput-object p4, p0, LX/9h4;->c:LX/03V;

    .line 1526045
    iget-object v0, p1, Lcom/facebook/photos/mediafetcher/query/param/MediaTypeQueryParam;->a:Ljava/lang/String;

    iput-object v0, p0, LX/9h4;->d:Ljava/lang/String;

    .line 1526046
    iput-object p5, p0, LX/9h4;->e:LX/0sX;

    .line 1526047
    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/String;)LX/0gW;
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "LX/0gW",
            "<",
            "Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromMediaSetTokenModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1526007
    new-instance v0, LX/9gF;

    invoke-direct {v0}, LX/9gF;-><init>()V

    move-object v1, v0

    .line 1526008
    const-string v0, "after_cursor"

    invoke-virtual {v1, v0, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "first_count"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "node_id"

    .line 1526009
    iget-object v0, p0, LX/9g8;->a:Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;

    move-object v0, v0

    .line 1526010
    check-cast v0, Lcom/facebook/photos/mediafetcher/query/param/MediaTypeQueryParam;

    iget-object v0, v0, Lcom/facebook/photos/mediafetcher/query/param/MediaTypeQueryParam;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "query_media_type"

    .line 1526011
    iget-object v0, p0, LX/9g8;->a:Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;

    move-object v0, v0

    .line 1526012
    check-cast v0, Lcom/facebook/photos/mediafetcher/query/param/MediaTypeQueryParam;

    iget-object v0, v0, Lcom/facebook/photos/mediafetcher/query/param/MediaTypeQueryParam;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "automatic_photo_captioning_enabled"

    iget-object v3, p0, LX/9h4;->e:LX/0sX;

    invoke-virtual {v3}, LX/0sX;->a()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1526013
    iget-object v0, p0, LX/9h4;->b:LX/9h9;

    invoke-virtual {v0, v1}, LX/9h9;->a(LX/0gW;)LX/0gW;

    .line 1526014
    return-object v1
.end method

.method public final b(Lcom/facebook/graphql/executor/GraphQLResult;)LX/9fz;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromMediaSetTokenModel;",
            ">;)",
            "LX/9fz",
            "<",
            "LX/5kD;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1526015
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v5

    .line 1526016
    if-nez p1, :cond_1

    move v4, v2

    .line 1526017
    :goto_0
    if-nez v4, :cond_2

    .line 1526018
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1526019
    if-nez v0, :cond_2

    move v3, v2

    .line 1526020
    :goto_1
    if-nez v3, :cond_3

    .line 1526021
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1526022
    check-cast v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromMediaSetTokenModel;

    invoke-virtual {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromMediaSetTokenModel;->a()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromMediaSetTokenModel$MediaModel;

    move-result-object v0

    if-nez v0, :cond_3

    move v0, v2

    .line 1526023
    :goto_2
    if-nez v4, :cond_0

    if-nez v3, :cond_0

    if-eqz v0, :cond_4

    .line 1526024
    :cond_0
    iget-object v2, p0, LX/9h4;->c:LX/03V;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "MediaGallery:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "null in response, set id: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, LX/9h4;->d:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", resultIsNull: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ", getResultIsNull: "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mediaIsNull: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, LX/0VG;->b(Ljava/lang/String;Ljava/lang/String;)LX/0VG;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/03V;->a(LX/0VG;)V

    .line 1526025
    new-instance v0, LX/9fz;

    invoke-static {v5}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    new-instance v3, LX/9gW;

    invoke-direct {v3}, LX/9gW;-><init>()V

    .line 1526026
    iput-boolean v1, v3, LX/9gW;->c:Z

    .line 1526027
    move-object v1, v3

    .line 1526028
    invoke-virtual {v1}, LX/9gW;->a()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    move-result-object v1

    invoke-direct {v0, v2, v1}, LX/9fz;-><init>(LX/0Px;Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;)V

    .line 1526029
    :goto_3
    return-object v0

    :cond_1
    move v4, v1

    .line 1526030
    goto/16 :goto_0

    :cond_2
    move v3, v1

    .line 1526031
    goto :goto_1

    :cond_3
    move v0, v1

    .line 1526032
    goto :goto_2

    .line 1526033
    :cond_4
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1526034
    check-cast v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromMediaSetTokenModel;

    invoke-virtual {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromMediaSetTokenModel;->a()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromMediaSetTokenModel$MediaModel;

    move-result-object v0

    .line 1526035
    invoke-virtual {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromMediaSetTokenModel$MediaModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    :goto_4
    if-ge v1, v3, :cond_6

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5kD;

    .line 1526036
    if-eqz v0, :cond_5

    invoke-interface {v0}, LX/5kD;->e()LX/1Fb;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 1526037
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1526038
    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 1526039
    :cond_6
    new-instance v1, LX/9fz;

    invoke-static {v5}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    .line 1526040
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1526041
    check-cast v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromMediaSetTokenModel;

    invoke-virtual {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromMediaSetTokenModel;->a()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromMediaSetTokenModel$MediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromMediaSetTokenModel$MediaModel;->j()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    move-result-object v0

    invoke-direct {v1, v2, v0}, LX/9fz;-><init>(LX/0Px;Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;)V

    move-object v0, v1

    goto :goto_3
.end method
