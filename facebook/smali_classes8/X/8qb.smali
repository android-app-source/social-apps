.class public final LX/8qb;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1407471
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLActor;)LX/36L;
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 1407472
    if-nez p0, :cond_1

    .line 1407473
    :cond_0
    :goto_0
    return-object v2

    .line 1407474
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1407475
    const/4 v1, 0x0

    .line 1407476
    if-nez p0, :cond_3

    .line 1407477
    :goto_1
    move v1, v1

    .line 1407478
    if-eqz v1, :cond_0

    .line 1407479
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1407480
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1407481
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1407482
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1407483
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 1407484
    const-string v1, "UFIProfileListConverter.getUFIProfileListFragmentGraphQL"

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1407485
    :cond_2
    new-instance v2, Lcom/facebook/ufiservices/flyout/UFIProfileListFragmentGraphQLModels$UFIProfileListFragmentGraphQLModel;

    invoke-direct {v2, v0}, Lcom/facebook/ufiservices/flyout/UFIProfileListFragmentGraphQLModels$UFIProfileListFragmentGraphQLModel;-><init>(LX/15i;)V

    goto :goto_0

    .line 1407486
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    .line 1407487
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->E()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    .line 1407488
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1407489
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aa()Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    move-result-object v6

    const/4 v7, 0x0

    .line 1407490
    if-nez v6, :cond_4

    .line 1407491
    :goto_2
    move v6, v7

    .line 1407492
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1407493
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v8

    const/4 v9, 0x0

    .line 1407494
    if-nez v8, :cond_5

    .line 1407495
    :goto_3
    move v8, v9

    .line 1407496
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ba()Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    move-result-object v9

    const/4 v10, 0x0

    .line 1407497
    if-nez v9, :cond_6

    .line 1407498
    :goto_4
    move v9, v10

    .line 1407499
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->az()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1407500
    const/16 v11, 0x9

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 1407501
    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1407502
    const/4 v3, 0x1

    invoke-virtual {v0, v3, v4}, LX/186;->b(II)V

    .line 1407503
    const/4 v3, 0x2

    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 1407504
    const/4 v3, 0x3

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 1407505
    const/4 v3, 0x4

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 1407506
    const/4 v3, 0x5

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 1407507
    const/4 v3, 0x6

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ay()I

    move-result v4

    invoke-virtual {v0, v3, v4, v1}, LX/186;->a(III)V

    .line 1407508
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 1407509
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 1407510
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1407511
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    goto/16 :goto_1

    .line 1407512
    :cond_4
    const/4 v8, 0x1

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 1407513
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;->a()I

    move-result v8

    invoke-virtual {v0, v7, v8, v7}, LX/186;->a(III)V

    .line 1407514
    invoke-virtual {v0}, LX/186;->d()I

    move-result v7

    .line 1407515
    invoke-virtual {v0, v7}, LX/186;->d(I)V

    goto :goto_2

    .line 1407516
    :cond_5
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1407517
    const/4 v11, 0x3

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 1407518
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v11

    invoke-virtual {v0, v9, v11, v9}, LX/186;->a(III)V

    .line 1407519
    const/4 v11, 0x1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 1407520
    const/4 v10, 0x2

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v11

    invoke-virtual {v0, v10, v11, v9}, LX/186;->a(III)V

    .line 1407521
    invoke-virtual {v0}, LX/186;->d()I

    move-result v9

    .line 1407522
    invoke-virtual {v0, v9}, LX/186;->d(I)V

    goto :goto_3

    .line 1407523
    :cond_6
    const/4 v11, 0x1

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 1407524
    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;->j()I

    move-result v11

    invoke-virtual {v0, v10, v11, v10}, LX/186;->a(III)V

    .line 1407525
    invoke-virtual {v0}, LX/186;->d()I

    move-result v10

    .line 1407526
    invoke-virtual {v0, v10}, LX/186;->d(I)V

    goto/16 :goto_4
.end method
