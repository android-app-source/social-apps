.class public final LX/AGF;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 1651604
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 1651605
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1651606
    :goto_0
    return v1

    .line 1651607
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1651608
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_6

    .line 1651609
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1651610
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1651611
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_1

    if-eqz v6, :cond_1

    .line 1651612
    const-string v7, "backstage"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1651613
    invoke-static {p0, p1}, LX/AGE;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1651614
    :cond_2
    const-string v7, "id"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1651615
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 1651616
    :cond_3
    const-string v7, "lowres_profile"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1651617
    const/4 v6, 0x0

    .line 1651618
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v7, :cond_b

    .line 1651619
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1651620
    :goto_2
    move v3, v6

    .line 1651621
    goto :goto_1

    .line 1651622
    :cond_4
    const-string v7, "name"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1651623
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 1651624
    :cond_5
    const-string v7, "profile_picture"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1651625
    const/4 v6, 0x0

    .line 1651626
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v7, :cond_f

    .line 1651627
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1651628
    :goto_3
    move v0, v6

    .line 1651629
    goto :goto_1

    .line 1651630
    :cond_6
    const/4 v6, 0x5

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1651631
    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1651632
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1651633
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1651634
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1651635
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1651636
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_7
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto/16 :goto_1

    .line 1651637
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1651638
    :cond_9
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_a

    .line 1651639
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1651640
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1651641
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_9

    if-eqz v7, :cond_9

    .line 1651642
    const-string v8, "uri"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 1651643
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_4

    .line 1651644
    :cond_a
    const/4 v7, 0x1

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1651645
    invoke-virtual {p1, v6, v3}, LX/186;->b(II)V

    .line 1651646
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto/16 :goto_2

    :cond_b
    move v3, v6

    goto :goto_4

    .line 1651647
    :cond_c
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1651648
    :cond_d
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_e

    .line 1651649
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1651650
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1651651
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_d

    if-eqz v7, :cond_d

    .line 1651652
    const-string v8, "uri"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 1651653
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_5

    .line 1651654
    :cond_e
    const/4 v7, 0x1

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1651655
    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 1651656
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto/16 :goto_3

    :cond_f
    move v0, v6

    goto :goto_5
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1651657
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1651658
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1651659
    if-eqz v0, :cond_0

    .line 1651660
    const-string v1, "backstage"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1651661
    invoke-static {p0, v0, p2, p3}, LX/AGE;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1651662
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1651663
    if-eqz v0, :cond_1

    .line 1651664
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1651665
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1651666
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1651667
    if-eqz v0, :cond_3

    .line 1651668
    const-string v1, "lowres_profile"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1651669
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1651670
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1651671
    if-eqz v1, :cond_2

    .line 1651672
    const-string p3, "uri"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1651673
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1651674
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1651675
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1651676
    if-eqz v0, :cond_4

    .line 1651677
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1651678
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1651679
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1651680
    if-eqz v0, :cond_6

    .line 1651681
    const-string v1, "profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1651682
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1651683
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1651684
    if-eqz v1, :cond_5

    .line 1651685
    const-string p1, "uri"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1651686
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1651687
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1651688
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1651689
    return-void
.end method
