.class public LX/99w;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1B1;

.field public final b:LX/0bH;

.field public c:Z


# direct methods
.method public constructor <init>(LX/1B1;LX/0bH;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1447938
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1447939
    iput-object p1, p0, LX/99w;->a:LX/1B1;

    .line 1447940
    iput-object p2, p0, LX/99w;->b:LX/0bH;

    .line 1447941
    return-void
.end method

.method public static a(LX/0QB;)LX/99w;
    .locals 1

    .prologue
    .line 1447942
    invoke-static {p0}, LX/99w;->b(LX/0QB;)LX/99w;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/99w;
    .locals 3

    .prologue
    .line 1447943
    new-instance v2, LX/99w;

    invoke-static {p0}, LX/1B1;->a(LX/0QB;)LX/1B1;

    move-result-object v0

    check-cast v0, LX/1B1;

    invoke-static {p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v1

    check-cast v1, LX/0bH;

    invoke-direct {v2, v0, v1}, LX/99w;-><init>(LX/1B1;LX/0bH;)V

    .line 1447944
    return-object v2
.end method

.method public static d(LX/99w;)V
    .locals 2

    .prologue
    .line 1447945
    iget-object v0, p0, LX/99w;->a:LX/1B1;

    iget-object v1, p0, LX/99w;->b:LX/0bH;

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b4;)V

    .line 1447946
    return-void
.end method


# virtual methods
.method public final varargs a([LX/0b2;)V
    .locals 1

    .prologue
    .line 1447947
    iget-boolean v0, p0, LX/99w;->c:Z

    if-nez v0, :cond_0

    .line 1447948
    iget-object v0, p0, LX/99w;->a:LX/1B1;

    invoke-virtual {v0, p1}, LX/1B1;->a([LX/0b2;)V

    .line 1447949
    invoke-static {p0}, LX/99w;->d(LX/99w;)V

    .line 1447950
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/99w;->c:Z

    .line 1447951
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1447952
    iget-boolean v0, p0, LX/99w;->c:Z

    return v0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1447953
    invoke-static {p0}, LX/99w;->d(LX/99w;)V

    .line 1447954
    return-void
.end method
