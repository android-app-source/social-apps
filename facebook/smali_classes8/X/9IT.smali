.class public final LX/9IT;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/9IU;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLComment;

.field public b:Lcom/facebook/api/ufiservices/FetchSingleCommentParams;

.field public c:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

.field public final synthetic d:LX/9IU;


# direct methods
.method public constructor <init>(LX/9IU;)V
    .locals 1

    .prologue
    .line 1463106
    iput-object p1, p0, LX/9IT;->d:LX/9IU;

    .line 1463107
    move-object v0, p1

    .line 1463108
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1463109
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1463110
    const-string v0, "EscapeHatchComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1463111
    if-ne p0, p1, :cond_1

    .line 1463112
    :cond_0
    :goto_0
    return v0

    .line 1463113
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1463114
    goto :goto_0

    .line 1463115
    :cond_3
    check-cast p1, LX/9IT;

    .line 1463116
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1463117
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1463118
    if-eq v2, v3, :cond_0

    .line 1463119
    iget-object v2, p0, LX/9IT;->a:Lcom/facebook/graphql/model/GraphQLComment;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/9IT;->a:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v3, p1, LX/9IT;->a:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/model/GraphQLComment;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1463120
    goto :goto_0

    .line 1463121
    :cond_5
    iget-object v2, p1, LX/9IT;->a:Lcom/facebook/graphql/model/GraphQLComment;

    if-nez v2, :cond_4

    .line 1463122
    :cond_6
    iget-object v2, p0, LX/9IT;->b:Lcom/facebook/api/ufiservices/FetchSingleCommentParams;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/9IT;->b:Lcom/facebook/api/ufiservices/FetchSingleCommentParams;

    iget-object v3, p1, LX/9IT;->b:Lcom/facebook/api/ufiservices/FetchSingleCommentParams;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1463123
    goto :goto_0

    .line 1463124
    :cond_8
    iget-object v2, p1, LX/9IT;->b:Lcom/facebook/api/ufiservices/FetchSingleCommentParams;

    if-nez v2, :cond_7

    .line 1463125
    :cond_9
    iget-object v2, p0, LX/9IT;->c:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/9IT;->c:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    iget-object v3, p1, LX/9IT;->c:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-virtual {v2, v3}, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1463126
    goto :goto_0

    .line 1463127
    :cond_a
    iget-object v2, p1, LX/9IT;->c:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
