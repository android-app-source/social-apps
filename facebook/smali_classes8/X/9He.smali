.class public final LX/9He;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/9Hg;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/9Hf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/9Hg",
            "<TE;>.CommentComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/9Hg;

.field private c:[Ljava/lang/String;

.field private d:I

.field public e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/9Hg;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 1461629
    iput-object p1, p0, LX/9He;->b:LX/9Hg;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1461630
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "commentProps"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "stylingResolver"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "feedbackLoggingParams"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "environment"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/9He;->c:[Ljava/lang/String;

    .line 1461631
    iput v3, p0, LX/9He;->d:I

    .line 1461632
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/9He;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/9He;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/9He;LX/1De;IILX/9Hf;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/9Hg",
            "<TE;>.CommentComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 1461625
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1461626
    iput-object p4, p0, LX/9He;->a:LX/9Hf;

    .line 1461627
    iget-object v0, p0, LX/9He;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1461628
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1461621
    invoke-super {p0}, LX/1X5;->a()V

    .line 1461622
    const/4 v0, 0x0

    iput-object v0, p0, LX/9He;->a:LX/9Hf;

    .line 1461623
    iget-object v0, p0, LX/9He;->b:LX/9Hg;

    iget-object v0, v0, LX/9Hg;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1461624
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/9Hg;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1461611
    iget-object v1, p0, LX/9He;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/9He;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/9He;->d:I

    if-ge v1, v2, :cond_2

    .line 1461612
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1461613
    :goto_0
    iget v2, p0, LX/9He;->d:I

    if-ge v0, v2, :cond_1

    .line 1461614
    iget-object v2, p0, LX/9He;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1461615
    iget-object v2, p0, LX/9He;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1461616
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1461617
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1461618
    :cond_2
    iget-object v0, p0, LX/9He;->a:LX/9Hf;

    .line 1461619
    invoke-virtual {p0}, LX/9He;->a()V

    .line 1461620
    return-object v0
.end method
