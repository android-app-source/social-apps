.class public LX/9FD;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/high16 v6, 0x40c00000    # 6.0f

    const/high16 v5, 0x41400000    # 12.0f

    const/4 v4, 0x1

    .line 1458309
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1458310
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 1458311
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    .line 1458312
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 1458313
    const v0, 0x7f0105b7

    invoke-virtual {v2, v0, v1, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    :goto_0
    iput v0, p0, LX/9FD;->a:I

    .line 1458314
    const v0, 0x7f0105b8

    invoke-virtual {v2, v0, v1, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    :goto_1
    iput v0, p0, LX/9FD;->b:I

    .line 1458315
    const v0, 0x7f0105b9

    invoke-virtual {v2, v0, v1, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    :goto_2
    iput v0, p0, LX/9FD;->c:I

    .line 1458316
    const v0, 0x7f0105ba

    invoke-virtual {v2, v0, v1, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    iget v0, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    :goto_3
    iput v0, p0, LX/9FD;->d:I

    .line 1458317
    const v0, 0x7f0105ad

    invoke-virtual {v2, v0, v1, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v0

    if-eqz v0, :cond_4

    iget v0, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    :goto_4
    iput v0, p0, LX/9FD;->e:I

    .line 1458318
    :try_start_0
    const v0, 0x7f0b08d0

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/9FD;->f:I
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1458319
    :goto_5
    :try_start_1
    const v0, 0x7f0b08bd

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/9FD;->g:I
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1458320
    :goto_6
    return-void

    .line 1458321
    :cond_0
    invoke-static {v3, v6}, LX/0tP;->a(Landroid/content/res/Resources;F)I

    move-result v0

    goto :goto_0

    .line 1458322
    :cond_1
    invoke-static {v3, v6}, LX/0tP;->a(Landroid/content/res/Resources;F)I

    move-result v0

    goto :goto_1

    .line 1458323
    :cond_2
    invoke-static {v3, v5}, LX/0tP;->a(Landroid/content/res/Resources;F)I

    move-result v0

    goto :goto_2

    .line 1458324
    :cond_3
    invoke-static {v3, v5}, LX/0tP;->a(Landroid/content/res/Resources;F)I

    move-result v0

    goto :goto_3

    .line 1458325
    :cond_4
    const/high16 v0, 0x42200000    # 40.0f

    invoke-static {v3, v0}, LX/0tP;->a(Landroid/content/res/Resources;F)I

    move-result v0

    goto :goto_4

    .line 1458326
    :catch_0
    const/high16 v0, 0x41c00000    # 24.0f

    invoke-static {v3, v0}, LX/0tP;->a(Landroid/content/res/Resources;F)I

    move-result v0

    iput v0, p0, LX/9FD;->f:I

    goto :goto_5

    .line 1458327
    :catch_1
    invoke-static {v3, v5}, LX/0tP;->a(Landroid/content/res/Resources;F)I

    move-result v0

    iput v0, p0, LX/9FD;->g:I

    goto :goto_6
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1458328
    iget v0, p0, LX/9FD;->a:I

    return v0
.end method

.method public final a(LX/9HA;)I
    .locals 2

    .prologue
    .line 1458329
    sget-object v0, LX/9FC;->a:[I

    invoke-virtual {p1}, LX/9HA;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1458330
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1458331
    :pswitch_0
    iget v0, p0, LX/9FD;->c:I

    move v0, v0

    .line 1458332
    goto :goto_0

    .line 1458333
    :pswitch_1
    iget v0, p0, LX/9FD;->c:I

    move v0, v0

    .line 1458334
    iget v1, p0, LX/9FD;->e:I

    move v1, v1

    .line 1458335
    add-int/2addr v0, v1

    .line 1458336
    iget v1, p0, LX/9FD;->g:I

    move v1, v1

    .line 1458337
    add-int/2addr v0, v1

    goto :goto_0

    .line 1458338
    :pswitch_2
    iget v0, p0, LX/9FD;->c:I

    move v0, v0

    .line 1458339
    iget v1, p0, LX/9FD;->e:I

    move v1, v1

    .line 1458340
    add-int/2addr v0, v1

    .line 1458341
    iget v1, p0, LX/9FD;->g:I

    move v1, v1

    .line 1458342
    add-int/2addr v0, v1

    .line 1458343
    iget v1, p0, LX/9FD;->f:I

    move v1, v1

    .line 1458344
    add-int/2addr v0, v1

    .line 1458345
    iget v1, p0, LX/9FD;->g:I

    move v1, v1

    .line 1458346
    add-int/2addr v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1458347
    iget v0, p0, LX/9FD;->b:I

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 1458348
    iget v0, p0, LX/9FD;->d:I

    return v0
.end method
