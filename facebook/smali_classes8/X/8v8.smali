.class public LX/8v8;
.super LX/8QK;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/8QK",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Lcom/facebook/user/model/User;

.field private e:Z


# virtual methods
.method public final a(Z)V
    .locals 1

    .prologue
    .line 1416667
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/8QK;->b:LX/8Re;

    if-eqz v0, :cond_0

    .line 1416668
    :goto_0
    return-void

    .line 1416669
    :cond_0
    invoke-super {p0, p1}, LX/8QK;->a(Z)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1416670
    iget-boolean v0, p0, LX/8v8;->e:Z

    if-eqz v0, :cond_0

    invoke-super {p0}, LX/8QK;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1416671
    iget-object v0, p0, LX/8v8;->a:Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1416672
    if-ne p1, p0, :cond_0

    .line 1416673
    const/4 v0, 0x1

    .line 1416674
    :goto_0
    return v0

    .line 1416675
    :cond_0
    instance-of v0, p1, LX/8v8;

    if-nez v0, :cond_1

    .line 1416676
    const/4 v0, 0x0

    goto :goto_0

    .line 1416677
    :cond_1
    iget-object v0, p0, LX/8v8;->a:Lcom/facebook/user/model/User;

    .line 1416678
    iget-object v1, v0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v0, v1

    .line 1416679
    check-cast p1, LX/8v8;

    iget-object v1, p1, LX/8v8;->a:Lcom/facebook/user/model/User;

    .line 1416680
    iget-object p0, v1, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v1, p0

    .line 1416681
    invoke-virtual {v0, v1}, Lcom/facebook/user/model/UserKey;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1416682
    iget-object v0, p0, LX/8v8;->a:Lcom/facebook/user/model/User;

    .line 1416683
    iget-object p0, v0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v0, p0

    .line 1416684
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->hashCode()I

    move-result v0

    return v0
.end method
