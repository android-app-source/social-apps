.class public LX/9ip;
.super LX/9iQ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/9iQ",
        "<",
        "LX/74x;",
        ">;"
    }
.end annotation


# static fields
.field public static final b:Ljava/lang/String;


# instance fields
.field public A:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

.field public a:LX/9cK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/9j0;

.field public d:Lcom/facebook/photos/tagging/shared/FaceBoxesView;

.field public e:LX/8Jd;

.field public f:LX/8Hs;

.field public g:LX/8Hs;

.field public h:LX/BIm;

.field public i:Z

.field public j:Z

.field public k:Z

.field public l:Lcom/facebook/performancelogger/PerformanceLogger;

.field public m:LX/0Yj;

.field public n:LX/4mV;

.field public o:Z

.field public p:LX/9ic;

.field public q:Z

.field public r:Z

.field public s:Landroid/graphics/RectF;

.field public t:LX/8GZ;

.field public u:LX/75Q;

.field public v:LX/75F;

.field public w:LX/4n9;

.field public x:LX/33B;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Landroid/net/Uri;

.field private z:Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1528520
    const-class v0, LX/9ip;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/9ip;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 14

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 1528521
    invoke-direct {p0, p1}, LX/9iQ;-><init>(Landroid/content/Context;)V

    .line 1528522
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v1, v1, v2, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, LX/9ip;->s:Landroid/graphics/RectF;

    .line 1528523
    const/4 v0, 0x0

    iput-object v0, p0, LX/9ip;->y:Landroid/net/Uri;

    .line 1528524
    const/4 v7, 0x0

    const/4 v6, -0x1

    .line 1528525
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    move-object v5, p0

    check-cast v5, LX/9ip;

    invoke-static {v3}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v8

    check-cast v8, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-static {v3}, LX/4mV;->a(LX/0QB;)LX/4mV;

    move-result-object v9

    check-cast v9, LX/4mV;

    invoke-static {v3}, LX/9ic;->a(LX/0QB;)LX/9ic;

    move-result-object v10

    check-cast v10, LX/9ic;

    invoke-static {v3}, LX/8GZ;->a(LX/0QB;)LX/8GZ;

    move-result-object v11

    check-cast v11, LX/8GZ;

    invoke-static {v3}, LX/75Q;->a(LX/0QB;)LX/75Q;

    move-result-object v12

    check-cast v12, LX/75Q;

    invoke-static {v3}, LX/75F;->a(LX/0QB;)LX/75F;

    move-result-object v13

    check-cast v13, LX/75F;

    iput-object v8, v5, LX/9ip;->l:Lcom/facebook/performancelogger/PerformanceLogger;

    iput-object v9, v5, LX/9ip;->n:LX/4mV;

    iput-object v10, v5, LX/9ip;->p:LX/9ic;

    iput-object v11, v5, LX/9ip;->t:LX/8GZ;

    iput-object v12, v5, LX/9ip;->u:LX/75Q;

    iput-object v13, v5, LX/9ip;->v:LX/75F;

    invoke-static {v3}, LX/9cK;->b(LX/0QB;)LX/9cK;

    move-result-object v8

    check-cast v8, LX/9cK;

    iput-object v8, v5, LX/9ip;->a:LX/9cK;

    .line 1528526
    iget-object v3, p0, LX/9iQ;->c:Lcom/facebook/widget/images/UrlImage;

    sget-object v4, LX/7UL;->PROGRESS_BAR_INDETERMINATE_WITH_PLACEHOLDER:LX/7UL;

    invoke-virtual {v3, v4}, Lcom/facebook/widget/images/UrlImage;->setProgressBarMode(LX/7UL;)V

    .line 1528527
    iput-boolean v7, p0, LX/9ip;->o:Z

    .line 1528528
    new-instance v3, Lcom/facebook/photos/tagging/shared/FaceBoxesView;

    invoke-virtual {p0}, LX/9ip;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, LX/9ip;->d:Lcom/facebook/photos/tagging/shared/FaceBoxesView;

    .line 1528529
    iget-object v3, p0, LX/9ip;->d:Lcom/facebook/photos/tagging/shared/FaceBoxesView;

    new-instance v4, LX/9ik;

    invoke-direct {v4, p0}, LX/9ik;-><init>(LX/9ip;)V

    .line 1528530
    iput-object v4, v3, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->i:LX/8JH;

    .line 1528531
    iget-object v3, p0, LX/9ip;->d:Lcom/facebook/photos/tagging/shared/FaceBoxesView;

    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v4, v6, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v3, v4}, LX/9ip;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1528532
    new-instance v3, LX/9j0;

    invoke-virtual {p0}, LX/9ip;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {p0}, LX/9iQ;->getZoomableImageView()Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    move-result-object v5

    invoke-direct {v3, v4, v5}, LX/9j0;-><init>(Landroid/content/Context;Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;)V

    iput-object v3, p0, LX/9ip;->c:LX/9j0;

    .line 1528533
    iget-object v3, p0, LX/9ip;->c:LX/9j0;

    new-instance v4, LX/9il;

    invoke-direct {v4, p0}, LX/9il;-><init>(LX/9ip;)V

    .line 1528534
    iput-object v4, v3, LX/9j0;->g:LX/9il;

    .line 1528535
    iget-object v3, p0, LX/9ip;->c:LX/9j0;

    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v4, v6, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v3, v4}, LX/9ip;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1528536
    new-instance v3, LX/8Jd;

    invoke-virtual {p0}, LX/9ip;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, LX/8Jd;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, LX/9ip;->e:LX/8Jd;

    .line 1528537
    iget-object v3, p0, LX/9ip;->e:LX/8Jd;

    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v4, v6, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v3, v4}, LX/9ip;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1528538
    invoke-virtual {p0}, LX/9iQ;->getZoomableImageView()Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    move-result-object v3

    .line 1528539
    new-instance v4, LX/9in;

    invoke-direct {v4, p0}, LX/9in;-><init>(LX/9ip;)V

    move-object v4, v4

    .line 1528540
    iput-object v4, v3, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->Q:LX/7UX;

    .line 1528541
    invoke-virtual {p0}, LX/9iQ;->getZoomableImageView()Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    move-result-object v3

    new-instance v4, LX/9im;

    invoke-direct {v4, p0}, LX/9im;-><init>(LX/9ip;)V

    invoke-virtual {v3, v4}, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->a(LX/7US;)V

    .line 1528542
    new-instance v3, LX/8Hs;

    iget-object v4, p0, LX/9ip;->c:LX/9j0;

    const-wide/16 v5, 0x96

    iget-object v8, p0, LX/9ip;->n:LX/4mV;

    invoke-direct/range {v3 .. v8}, LX/8Hs;-><init>(Landroid/view/View;JZLX/4mV;)V

    iput-object v3, p0, LX/9ip;->f:LX/8Hs;

    .line 1528543
    new-instance v3, LX/8Hs;

    iget-object v4, p0, LX/9ip;->e:LX/8Jd;

    const-wide/16 v5, 0x12c

    iget-object v8, p0, LX/9ip;->n:LX/4mV;

    invoke-direct/range {v3 .. v8}, LX/8Hs;-><init>(Landroid/view/View;JZLX/4mV;)V

    iput-object v3, p0, LX/9ip;->g:LX/8Hs;

    .line 1528544
    invoke-static {p0, v7}, LX/9ip;->c(LX/9ip;Z)V

    .line 1528545
    invoke-virtual {p0}, LX/9ip;->o()V

    .line 1528546
    invoke-virtual {p0, v7}, LX/9ip;->b(Z)V

    .line 1528547
    return-void
.end method

.method public static c(LX/9ip;Z)V
    .locals 1

    .prologue
    .line 1528548
    iget-object v0, p0, LX/9ip;->f:LX/8Hs;

    invoke-virtual {v0, p1}, LX/8Hs;->b(Z)V

    .line 1528549
    return-void
.end method

.method private u()V
    .locals 2

    .prologue
    .line 1528550
    iget-object v0, p0, LX/9ip;->c:LX/9j0;

    iget-boolean v1, p0, LX/9ip;->i:Z

    .line 1528551
    new-instance p0, Lcom/facebook/photos/tagging/ui/TagsView$3;

    invoke-direct {p0, v0, v1}, Lcom/facebook/photos/tagging/ui/TagsView$3;-><init>(LX/9j0;Z)V

    invoke-static {v0, p0}, LX/8He;->c(Landroid/view/View;Ljava/lang/Runnable;)Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 1528552
    return-void
.end method

.method public static v(LX/9ip;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 1528615
    invoke-virtual {p0}, LX/9iQ;->getZoomableImageView()Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    move-result-object v0

    .line 1528616
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->getImageMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1528617
    :cond_0
    :goto_0
    return-void

    .line 1528618
    :cond_1
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 1528619
    invoke-virtual {v0}, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->getLeft()I

    move-result v2

    int-to-float v2, v2

    iput v2, v1, Landroid/graphics/RectF;->left:F

    .line 1528620
    invoke-virtual {v0}, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->getTop()I

    move-result v2

    int-to-float v2, v2

    iput v2, v1, Landroid/graphics/RectF;->top:F

    .line 1528621
    invoke-virtual {v0}, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->getTop()I

    move-result v2

    invoke-virtual {v0}, LX/7UQ;->getPhotoHeight()I

    move-result v3

    add-int/2addr v2, v3

    int-to-float v2, v2

    iput v2, v1, Landroid/graphics/RectF;->bottom:F

    .line 1528622
    invoke-virtual {v0}, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->getLeft()I

    move-result v2

    invoke-virtual {v0}, LX/7UQ;->getPhotoWidth()I

    move-result v3

    add-int/2addr v2, v3

    int-to-float v2, v2

    iput v2, v1, Landroid/graphics/RectF;->right:F

    .line 1528623
    invoke-virtual {v0}, LX/7UQ;->getPhotoDisplayMatrix()Landroid/graphics/Matrix;

    move-result-object v2

    .line 1528624
    invoke-virtual {v2, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 1528625
    new-instance v2, Landroid/graphics/RectF;

    invoke-virtual {v0}, LX/7UQ;->getPhotoWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v0}, LX/7UQ;->getPhotoHeight()I

    move-result v0

    int-to-float v0, v0

    invoke-direct {v2, v4, v4, v3, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1528626
    iget-object v0, p0, LX/9ip;->d:Lcom/facebook/photos/tagging/shared/FaceBoxesView;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v4

    float-to-int v4, v4

    .line 1528627
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1528628
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1528629
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 1528630
    sget-object p0, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v5, v2, v1, p0}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 1528631
    move-object v1, v5

    .line 1528632
    invoke-virtual {v0, v3, v4, v1}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->a(IILandroid/graphics/Matrix;)V

    goto :goto_0
.end method

.method public static x(LX/9ip;)V
    .locals 10

    .prologue
    const/4 v6, 0x1

    .line 1528553
    invoke-virtual {p0}, LX/9iQ;->getZoomableImageView()Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    move-result-object v1

    .line 1528554
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->getImageMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, LX/7UQ;->getPhotoHeight()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->getMeasuredWidth()I

    move-result v0

    if-nez v0, :cond_4

    .line 1528555
    :cond_0
    const/4 v0, 0x0

    .line 1528556
    :goto_0
    move-object v3, v0

    .line 1528557
    iget-object v0, p0, LX/9ip;->A:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    if-eqz v0, :cond_1

    if-nez v3, :cond_2

    .line 1528558
    :cond_1
    :goto_1
    return-void

    .line 1528559
    :cond_2
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v2

    float-to-int v2, v2

    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1528560
    iget v1, v3, Landroid/graphics/RectF;->top:F

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 1528561
    iget v1, v3, Landroid/graphics/RectF;->left:F

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 1528562
    iget-object v1, p0, LX/9ip;->z:Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;

    invoke-virtual {v1, v0}, Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1528563
    const/16 v0, 0x3e9

    invoke-virtual {p0, v0}, LX/9ip;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_3

    .line 1528564
    iget-object v0, p0, LX/9ip;->z:Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;

    invoke-virtual {p0, v0}, LX/9ip;->addView(Landroid/view/View;)V

    .line 1528565
    :cond_3
    iget-object v0, p0, LX/9ip;->a:LX/9cK;

    .line 1528566
    invoke-virtual {v0}, LX/9cK;->b()V

    .line 1528567
    iget-object v0, p0, LX/9ip;->a:LX/9cK;

    iget-object v1, p0, LX/9ip;->A:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    float-to-int v3, v3

    .line 1528568
    iget-object v4, p0, LX/9iQ;->b:LX/74w;

    move-object v4, v4

    .line 1528569
    check-cast v4, Lcom/facebook/photos/base/photos/LocalPhoto;

    .line 1528570
    iget v5, v4, Lcom/facebook/photos/base/photos/LocalPhoto;->e:I

    move v4, v5

    .line 1528571
    iget-object v5, p0, LX/9ip;->z:Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;

    const/4 v7, 0x4

    new-array v7, v7, [LX/9cJ;

    const/4 v8, 0x0

    sget-object v9, LX/9cJ;->STICKERS:LX/9cJ;

    aput-object v9, v7, v8

    sget-object v8, LX/9cJ;->TEXTS:LX/9cJ;

    aput-object v8, v7, v6

    const/4 v8, 0x2

    sget-object v9, LX/9cJ;->DOODLE:LX/9cJ;

    aput-object v9, v7, v8

    const/4 v8, 0x3

    sget-object v9, LX/9cJ;->FRAME:LX/9cJ;

    aput-object v9, v7, v8

    invoke-virtual/range {v0 .. v7}, LX/9cK;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;IIILandroid/view/View;Z[LX/9cJ;)V

    .line 1528572
    iget-object v0, p0, LX/9ip;->z:Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;

    iget-object v1, p0, LX/9ip;->a:LX/9cK;

    .line 1528573
    iput-object v1, v0, Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;->a:LX/9cG;

    .line 1528574
    goto :goto_1

    .line 1528575
    :cond_4
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 1528576
    invoke-virtual {v1}, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->getTop()I

    move-result v2

    int-to-float v2, v2

    iput v2, v0, Landroid/graphics/RectF;->top:F

    .line 1528577
    invoke-virtual {v1}, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->getLeft()I

    move-result v2

    int-to-float v2, v2

    iput v2, v0, Landroid/graphics/RectF;->left:F

    .line 1528578
    invoke-virtual {v1}, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->getTop()I

    move-result v2

    invoke-virtual {v1}, LX/7UQ;->getPhotoHeight()I

    move-result v3

    add-int/2addr v2, v3

    int-to-float v2, v2

    iput v2, v0, Landroid/graphics/RectF;->bottom:F

    .line 1528579
    invoke-virtual {v1}, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->getLeft()I

    move-result v2

    invoke-virtual {v1}, LX/7UQ;->getPhotoWidth()I

    move-result v3

    add-int/2addr v2, v3

    int-to-float v2, v2

    iput v2, v0, Landroid/graphics/RectF;->right:F

    .line 1528580
    invoke-virtual {v1}, LX/7UQ;->getPhotoDisplayMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    .line 1528581
    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/base/tagging/FaceBox;)Lcom/facebook/photos/base/tagging/FaceBox;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1528582
    iget-object v0, p0, LX/9ip;->p:LX/9ic;

    invoke-virtual {v0, p1}, LX/9ic;->a(Lcom/facebook/photos/base/tagging/FaceBox;)Lcom/facebook/photos/base/tagging/FaceBox;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/74w;)V
    .locals 0
    .param p1    # LX/74w;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1528583
    check-cast p1, LX/74x;

    invoke-virtual {p0, p1}, LX/9ip;->a(LX/74x;)V

    return-void
.end method

.method public final a(LX/74x;)V
    .locals 8
    .param p1    # LX/74x;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 1528584
    invoke-super {p0, p1}, LX/9iQ;->a(LX/74w;)V

    .line 1528585
    const/4 v1, 0x0

    .line 1528586
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 1528587
    if-eqz p1, :cond_0

    .line 1528588
    iget-object v0, p0, LX/9ip;->s:Landroid/graphics/RectF;

    .line 1528589
    instance-of v1, p1, Lcom/facebook/photos/base/photos/LocalPhoto;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 1528590
    check-cast v0, Lcom/facebook/photos/base/photos/LocalPhoto;

    .line 1528591
    iget v1, v0, Lcom/facebook/photos/base/photos/LocalPhoto;->e:I

    move v1, v1

    .line 1528592
    iget-object v0, p0, LX/9ip;->s:Landroid/graphics/RectF;

    .line 1528593
    invoke-static {v1}, LX/8Ga;->a(I)I

    move-result v3

    .line 1528594
    invoke-static {v0, v3}, LX/8Ga;->a(Landroid/graphics/RectF;I)Landroid/graphics/RectF;

    move-result-object v3

    move-object v0, v3

    .line 1528595
    :goto_0
    iget-object v3, p0, LX/9ip;->p:LX/9ic;

    iget-object v4, p0, LX/9ip;->s:Landroid/graphics/RectF;

    iget-object v5, p0, LX/9ip;->v:LX/75F;

    invoke-virtual {v5, p1}, LX/75F;->a(LX/74x;)Ljava/util/List;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v1}, LX/9ic;->a(Landroid/graphics/RectF;Ljava/util/List;I)V

    .line 1528596
    iget-object v3, p0, LX/9ip;->t:LX/8GZ;

    iget-object v4, p0, LX/9ip;->s:Landroid/graphics/RectF;

    invoke-virtual {v3, v4, v1}, LX/8GZ;->a(Landroid/graphics/RectF;I)V

    .line 1528597
    iget-object v3, p0, LX/9ip;->v:LX/75F;

    invoke-virtual {v3, p1}, LX/75F;->a(LX/74x;)Ljava/util/List;

    move-result-object v3

    invoke-static {v3, v0, v1}, LX/9ic;->a(Ljava/util/List;Landroid/graphics/RectF;I)Ljava/util/List;

    move-result-object v1

    .line 1528598
    iget-object v0, p0, LX/9ip;->t:LX/8GZ;

    iget-object v3, p0, LX/9ip;->u:LX/75Q;

    invoke-virtual {v3, p1}, LX/75Q;->a(LX/74x;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/8GZ;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 1528599
    iget-object v3, p0, LX/9ip;->l:Lcom/facebook/performancelogger/PerformanceLogger;

    if-eqz v3, :cond_0

    .line 1528600
    new-instance v3, LX/0Yj;

    const v4, 0x14000f

    const-string v5, "FaceBoxesTimeToDisplay"

    invoke-direct {v3, v4, v5}, LX/0Yj;-><init>(ILjava/lang/String;)V

    iput-object v3, p0, LX/9ip;->m:LX/0Yj;

    .line 1528601
    iget-object v3, p0, LX/9ip;->m:LX/0Yj;

    .line 1528602
    iget-wide v6, p1, LX/74w;->a:J

    move-wide v4, v6

    .line 1528603
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    .line 1528604
    iput-object v4, v3, LX/0Yj;->e:Ljava/lang/String;

    .line 1528605
    iget-object v3, p0, LX/9ip;->m:LX/0Yj;

    const-wide v4, 0x3fc999999999999aL    # 0.2

    invoke-virtual {v3, v4, v5}, LX/0Yj;->a(D)LX/0Yj;

    .line 1528606
    :cond_0
    iget-object v3, p0, LX/9ip;->c:LX/9j0;

    iget-boolean v4, p0, LX/9ip;->k:Z

    invoke-virtual {v3, v0, v4}, LX/9j0;->a(Ljava/util/List;Z)V

    .line 1528607
    iget-object v0, p0, LX/9ip;->c:LX/9j0;

    invoke-virtual {v0, v1}, LX/9j0;->setFaceBoxRects(Ljava/util/List;)V

    .line 1528608
    invoke-static {p0, v2}, LX/9ip;->c(LX/9ip;Z)V

    .line 1528609
    invoke-virtual {p0, v2}, LX/9ip;->b(Z)V

    .line 1528610
    new-instance v0, Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;

    invoke-virtual {p0}, LX/9ip;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/9ip;->z:Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;

    .line 1528611
    iget-object v0, p0, LX/9ip;->z:Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;

    const/16 v1, 0x3e9

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;->setId(I)V

    .line 1528612
    const/4 v0, 0x1

    .line 1528613
    iput-boolean v0, p0, LX/9ip;->k:Z

    .line 1528614
    return-void

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 1528509
    iget-object v0, p0, LX/9iQ;->c:Lcom/facebook/widget/images/UrlImage;

    move-object v0, v0

    .line 1528510
    if-eqz v0, :cond_0

    .line 1528511
    iget-object v0, p0, LX/9iQ;->c:Lcom/facebook/widget/images/UrlImage;

    move-object v0, v0

    .line 1528512
    invoke-virtual {v0}, Lcom/facebook/widget/images/UrlImage;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1528513
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/9ip;->r:Z

    .line 1528514
    :goto_0
    return-void

    .line 1528515
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/9ip;->r:Z

    .line 1528516
    invoke-direct {p0}, LX/9ip;->u()V

    .line 1528517
    iget-object v0, p0, LX/9ip;->f:LX/8Hs;

    invoke-virtual {v0, p1}, LX/8Hs;->a(Z)V

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 1528518
    iget-object v0, p0, LX/9ip;->g:LX/8Hs;

    invoke-virtual {v0, p1}, LX/8Hs;->b(Z)V

    .line 1528519
    return-void
.end method

.method public final h()V
    .locals 6

    .prologue
    .line 1528485
    iget-object v1, p0, LX/9ip;->u:LX/75Q;

    .line 1528486
    iget-object v0, p0, LX/9iQ;->b:LX/74w;

    move-object v0, v0

    .line 1528487
    check-cast v0, LX/74x;

    invoke-virtual {v1, v0}, LX/75Q;->a(LX/74x;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1528488
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, LX/9ip;->u:LX/75Q;

    .line 1528489
    iget-object v0, p0, LX/9iQ;->b:LX/74w;

    move-object v0, v0

    .line 1528490
    check-cast v0, LX/74x;

    invoke-virtual {v2, v0}, LX/75Q;->a(LX/74x;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1528491
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/tagging/Tag;

    .line 1528492
    iget-object v2, p0, LX/9ip;->t:LX/8GZ;

    .line 1528493
    iget-object v3, v2, LX/8GZ;->c:Landroid/graphics/RectF;

    .line 1528494
    iget-object v4, v0, Lcom/facebook/photos/base/tagging/Tag;->a:Lcom/facebook/photos/base/tagging/TagTarget;

    move-object v4, v4

    .line 1528495
    invoke-interface {v4}, Lcom/facebook/photos/base/tagging/TagTarget;->e()Landroid/graphics/PointF;

    move-result-object v4

    iget v4, v4, Landroid/graphics/PointF;->x:F

    .line 1528496
    iget-object v5, v0, Lcom/facebook/photos/base/tagging/Tag;->a:Lcom/facebook/photos/base/tagging/TagTarget;

    move-object v5, v5

    .line 1528497
    invoke-interface {v5}, Lcom/facebook/photos/base/tagging/TagTarget;->e()Landroid/graphics/PointF;

    move-result-object v5

    iget v5, v5, Landroid/graphics/PointF;->y:F

    invoke-virtual {v3, v4, v5}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1528498
    const/4 v3, 0x1

    .line 1528499
    :goto_1
    move v2, v3

    .line 1528500
    if-nez v2, :cond_0

    .line 1528501
    iget-object v2, p0, LX/9ip;->h:LX/BIm;

    invoke-virtual {v2, v0}, LX/BIm;->a(Lcom/facebook/photos/base/tagging/Tag;)V

    goto :goto_0

    .line 1528502
    :cond_1
    iget-object v1, p0, LX/9ip;->t:LX/8GZ;

    iget-object v2, p0, LX/9ip;->u:LX/75Q;

    .line 1528503
    iget-object v0, p0, LX/9iQ;->b:LX/74w;

    move-object v0, v0

    .line 1528504
    check-cast v0, LX/74x;

    invoke-virtual {v2, v0}, LX/75Q;->a(LX/74x;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/8GZ;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 1528505
    iget-object v1, p0, LX/9ip;->c:LX/9j0;

    iget-boolean v2, p0, LX/9ip;->k:Z

    invoke-virtual {v1, v0, v2}, LX/9j0;->a(Ljava/util/List;Z)V

    .line 1528506
    invoke-virtual {p0}, LX/9ip;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1528507
    invoke-direct {p0}, LX/9ip;->u()V

    .line 1528508
    :cond_2
    return-void

    :cond_3
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 1528484
    iget-object v0, p0, LX/9ip;->f:LX/8Hs;

    invoke-virtual {v0}, LX/8Hs;->a()Z

    move-result v0

    return v0
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 1528482
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/9ip;->a(Z)V

    .line 1528483
    return-void
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 1528480
    iget-object v0, p0, LX/9ip;->c:LX/9j0;

    invoke-virtual {v0}, LX/9j0;->a()V

    .line 1528481
    return-void
.end method

.method public final m()V
    .locals 3

    .prologue
    .line 1528462
    iget-object v0, p0, LX/9ip;->d:Lcom/facebook/photos/tagging/shared/FaceBoxesView;

    invoke-virtual {v0}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1528463
    invoke-virtual {p0}, LX/9ip;->n()V

    .line 1528464
    :cond_0
    const/4 v0, 0x0

    .line 1528465
    iget-object v1, p0, LX/9iQ;->b:LX/74w;

    move-object v1, v1

    .line 1528466
    instance-of v1, v1, Lcom/facebook/photos/base/photos/LocalPhoto;

    if-eqz v1, :cond_3

    .line 1528467
    iget-object v0, p0, LX/9iQ;->b:LX/74w;

    move-object v0, v0

    .line 1528468
    check-cast v0, Lcom/facebook/photos/base/photos/LocalPhoto;

    .line 1528469
    iget v1, v0, Lcom/facebook/photos/base/photos/LocalPhoto;->e:I

    move v0, v1

    .line 1528470
    move v1, v0

    .line 1528471
    :goto_0
    iget-object v2, p0, LX/9ip;->v:LX/75F;

    .line 1528472
    iget-object v0, p0, LX/9iQ;->b:LX/74w;

    move-object v0, v0

    .line 1528473
    check-cast v0, LX/74x;

    invoke-virtual {v2, v0}, LX/75F;->a(LX/74x;)Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, LX/9ip;->s:Landroid/graphics/RectF;

    invoke-static {v0, v2, v1}, LX/9ic;->a(Ljava/util/List;Landroid/graphics/RectF;I)Ljava/util/List;

    move-result-object v0

    .line 1528474
    iget-object v1, p0, LX/9ip;->c:LX/9j0;

    invoke-virtual {v1, v0}, LX/9j0;->setFaceBoxRects(Ljava/util/List;)V

    .line 1528475
    invoke-virtual {p0}, LX/9ip;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1528476
    invoke-direct {p0}, LX/9ip;->u()V

    .line 1528477
    :cond_1
    iget-object v0, p0, LX/9ip;->l:Lcom/facebook/performancelogger/PerformanceLogger;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/9ip;->m:LX/0Yj;

    if-eqz v0, :cond_2

    .line 1528478
    iget-object v0, p0, LX/9ip;->l:Lcom/facebook/performancelogger/PerformanceLogger;

    iget-object v1, p0, LX/9ip;->m:LX/0Yj;

    invoke-interface {v0, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->b(LX/0Yj;)V

    .line 1528479
    :cond_2
    return-void

    :cond_3
    move v1, v0

    goto :goto_0
.end method

.method public final n()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1528448
    iget-object v0, p0, LX/9iQ;->c:Lcom/facebook/widget/images/UrlImage;

    move-object v0, v0

    .line 1528449
    if-eqz v0, :cond_0

    .line 1528450
    iget-object v0, p0, LX/9iQ;->c:Lcom/facebook/widget/images/UrlImage;

    move-object v0, v0

    .line 1528451
    invoke-virtual {v0}, Lcom/facebook/widget/images/UrlImage;->d()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1528452
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/9ip;->q:Z

    .line 1528453
    :cond_1
    :goto_0
    return-void

    .line 1528454
    :cond_2
    iput-boolean v1, p0, LX/9ip;->q:Z

    .line 1528455
    iget-object v0, p0, LX/9ip;->d:Lcom/facebook/photos/tagging/shared/FaceBoxesView;

    invoke-virtual {v0, v1}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->setVisibility(I)V

    .line 1528456
    iget-object v1, p0, LX/9ip;->d:Lcom/facebook/photos/tagging/shared/FaceBoxesView;

    iget-object v2, p0, LX/9ip;->p:LX/9ic;

    iget-object v3, p0, LX/9ip;->v:LX/75F;

    .line 1528457
    iget-object v0, p0, LX/9iQ;->b:LX/74w;

    move-object v0, v0

    .line 1528458
    check-cast v0, LX/74x;

    invoke-virtual {v3, v0}, LX/75F;->a(LX/74x;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/9ic;->a(Ljava/util/List;)Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->setFaceBoxes(Ljava/util/Collection;)V

    .line 1528459
    invoke-static {p0}, LX/9ip;->v(LX/9ip;)V

    .line 1528460
    iget-boolean v0, p0, LX/9ip;->o:Z

    if-eqz v0, :cond_1

    .line 1528461
    iget-object v0, p0, LX/9ip;->d:Lcom/facebook/photos/tagging/shared/FaceBoxesView;

    invoke-virtual {v0}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->a()V

    goto :goto_0
.end method

.method public final o()V
    .locals 2

    .prologue
    .line 1528446
    iget-object v0, p0, LX/9ip;->d:Lcom/facebook/photos/tagging/shared/FaceBoxesView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->setVisibility(I)V

    .line 1528447
    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x7858f73

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1528442
    invoke-super {p0}, LX/9iQ;->onDetachedFromWindow()V

    .line 1528443
    iget-object v1, p0, LX/9ip;->l:Lcom/facebook/performancelogger/PerformanceLogger;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/9ip;->m:LX/0Yj;

    if-eqz v1, :cond_0

    .line 1528444
    iget-object v1, p0, LX/9ip;->l:Lcom/facebook/performancelogger/PerformanceLogger;

    iget-object v2, p0, LX/9ip;->m:LX/0Yj;

    invoke-interface {v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->f(LX/0Yj;)V

    .line 1528445
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x58bc1d2d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setFetchParams(LX/4n9;)V
    .locals 3

    .prologue
    .line 1528419
    iget-object v0, p0, LX/9ip;->y:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/9ip;->y:Landroid/net/Uri;

    invoke-static {v0}, LX/4n9;->a(Landroid/net/Uri;)LX/4n8;

    move-result-object v0

    .line 1528420
    iget-object v1, p1, LX/4n9;->b:LX/4n3;

    move-object v1, v1

    .line 1528421
    iput-object v1, v0, LX/4n8;->c:LX/4n3;

    .line 1528422
    iget-object v1, p1, LX/4n9;->d:LX/4n2;

    move-object v1, v1

    .line 1528423
    iput-object v1, v0, LX/4n8;->d:LX/4n2;

    .line 1528424
    iget-object v1, p1, LX/4n9;->f:LX/4n5;

    move-object v1, v1

    .line 1528425
    iput-object v1, v0, LX/4n8;->e:LX/4n5;

    .line 1528426
    iget-boolean v1, p1, LX/4n9;->g:Z

    move v1, v1

    .line 1528427
    iput-boolean v1, v0, LX/4n8;->f:Z

    .line 1528428
    iget-boolean v1, p1, LX/4n9;->h:Z

    move v1, v1

    .line 1528429
    iput-boolean v1, v0, LX/4n8;->g:Z

    .line 1528430
    iget-boolean v1, p1, LX/4n9;->i:Z

    move v1, v1

    .line 1528431
    iput-boolean v1, v0, LX/4n8;->h:Z

    .line 1528432
    iget-boolean v1, p1, LX/4n9;->j:Z

    move v1, v1

    .line 1528433
    iput-boolean v1, v0, LX/4n8;->i:Z

    .line 1528434
    move-object v0, v0

    .line 1528435
    invoke-virtual {v0}, LX/4n8;->a()LX/4n9;

    move-result-object p1

    :cond_0
    iput-object p1, p0, LX/9ip;->w:LX/4n9;

    .line 1528436
    iget-object v0, p0, LX/9ip;->x:LX/33B;

    if-eqz v0, :cond_1

    .line 1528437
    iget-object v0, p0, LX/9iQ;->c:Lcom/facebook/widget/images/UrlImage;

    move-object v0, v0

    .line 1528438
    iget-object v1, p0, LX/9ip;->w:LX/4n9;

    iget-object v2, p0, LX/9ip;->x:LX/33B;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/images/UrlImage;->a(LX/4n9;LX/33B;)V

    .line 1528439
    :goto_0
    return-void

    .line 1528440
    :cond_1
    iget-object v0, p0, LX/9iQ;->c:Lcom/facebook/widget/images/UrlImage;

    move-object v0, v0

    .line 1528441
    iget-object v1, p0, LX/9ip;->w:LX/4n9;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/images/UrlImage;->setImageParams(LX/4n9;)V

    goto :goto_0
.end method

.method public setTagsAndFaceboxesEnabled(Z)V
    .locals 1

    .prologue
    .line 1528416
    iget-object v0, p0, LX/9ip;->c:LX/9j0;

    invoke-virtual {v0, p1}, LX/9j0;->setEnabled(Z)V

    .line 1528417
    iget-object v0, p0, LX/9ip;->d:Lcom/facebook/photos/tagging/shared/FaceBoxesView;

    invoke-virtual {v0, p1}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->setEnabled(Z)V

    .line 1528418
    return-void
.end method
