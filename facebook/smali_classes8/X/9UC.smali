.class public LX/9UC;
.super LX/9UB;
.source ""


# instance fields
.field private final c:LX/2Rd;

.field public final h:Landroid/widget/Filter;

.field public i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/0ad;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;LX/2Rd;LX/0ad;)V
    .locals 1
    .param p2    # Landroid/database/Cursor;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1497684
    invoke-direct {p0, p1, p2}, LX/9UB;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    .line 1497685
    iput-object p3, p0, LX/9UC;->c:LX/2Rd;

    .line 1497686
    iput-object p4, p0, LX/9UC;->k:LX/0ad;

    .line 1497687
    new-instance v0, LX/9UV;

    invoke-direct {v0, p0, p1}, LX/9UV;-><init>(LX/9UC;Landroid/content/Context;)V

    iput-object v0, p0, LX/9UC;->h:Landroid/widget/Filter;

    .line 1497688
    return-void
.end method


# virtual methods
.method public final a(II)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1497689
    iget-object v0, p0, LX/9UC;->j:LX/0Px;

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    iget-object v0, p0, LX/9UC;->k:LX/0ad;

    sget-short v1, LX/1EB;->L:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1497690
    iget-object v3, p0, LX/9UC;->j:LX/0Px;

    invoke-virtual {v3, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 1497691
    new-instance v4, Lcom/facebook/ipc/model/FacebookProfile;

    invoke-virtual {v3}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->p()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-virtual {v3}, LX/8QK;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3}, LX/8QL;->h()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-direct/range {v4 .. v9}, Lcom/facebook/ipc/model/FacebookProfile;-><init>(JLjava/lang/String;Ljava/lang/String;I)V

    move-object v0, v4

    .line 1497692
    :goto_0
    return-object v0

    .line 1497693
    :cond_0
    iget-object v3, p0, LX/9UA;->d:Landroid/database/Cursor;

    invoke-interface {v3, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1497694
    new-instance v4, Lcom/facebook/ipc/model/FacebookProfile;

    iget-object v3, p0, LX/9UA;->d:Landroid/database/Cursor;

    iget-object v5, p0, LX/9UA;->d:Landroid/database/Cursor;

    const-string v6, "user_id"

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    iget-object v3, p0, LX/9UA;->d:Landroid/database/Cursor;

    iget-object v7, p0, LX/9UA;->d:Landroid/database/Cursor;

    const-string v8, "display_name"

    invoke-interface {v7, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v3, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    iget-object v3, p0, LX/9UA;->d:Landroid/database/Cursor;

    iget-object v8, p0, LX/9UA;->d:Landroid/database/Cursor;

    const-string v9, "user_image_url"

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v3, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-direct/range {v4 .. v9}, Lcom/facebook/ipc/model/FacebookProfile;-><init>(JLjava/lang/String;Ljava/lang/String;I)V

    move-object v0, v4

    .line 1497695
    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 1497696
    check-cast p1, Ljava/lang/String;

    return-object p1
.end method

.method public final a(Landroid/database/Cursor;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1497697
    iput-object p1, p0, LX/9UC;->d:Landroid/database/Cursor;

    .line 1497698
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/9UC;->e:Ljava/util/List;

    .line 1497699
    iget-object v0, p0, LX/9UB;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1497700
    iget-object v1, p0, LX/9UC;->j:LX/0Px;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/9UC;->k:LX/0ad;

    sget-short v2, LX/1EB;->L:S

    invoke-interface {v1, v2, v5}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1497701
    iget-object v1, p0, LX/9UA;->e:Ljava/util/List;

    new-instance v2, LX/9US;

    const v3, 0x7f08149d

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/9UC;->j:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    invoke-direct {v2, v3, v5, v4}, LX/9US;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1497702
    :cond_0
    if-nez p1, :cond_1

    .line 1497703
    :goto_0
    return-void

    .line 1497704
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1497705
    iget-object v1, p0, LX/9UA;->e:Ljava/util/List;

    new-instance v2, LX/9US;

    const v3, 0x7f08149c

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-direct {v2, v0, v5, v3}, LX/9US;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1497706
    const v0, 0x226b2ae6

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0
.end method

.method public final b(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1497707
    iget-object v0, p0, LX/9UC;->c:LX/2Rd;

    const/4 p0, 0x0

    .line 1497708
    const-string v1, "sort_name"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1497709
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-gtz v2, :cond_0

    .line 1497710
    const-string v1, "display_name"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1497711
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, p0, v2}, Ljava/lang/String;->offsetByCodePoints(II)I

    move-result v2

    .line 1497712
    invoke-virtual {v1, p0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 1497713
    invoke-virtual {v0, v1}, LX/2Rd;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
