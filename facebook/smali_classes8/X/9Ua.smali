.class public LX/9Ua;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/graphics/Path;

.field public final b:[F


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1498085
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-direct {p0, v0, v1}, LX/9Ua;-><init>(Landroid/graphics/Path;[F)V

    .line 1498086
    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method private constructor <init>(Landroid/graphics/Path;[F)V
    .locals 0

    .prologue
    .line 1498087
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1498088
    iput-object p1, p0, LX/9Ua;->a:Landroid/graphics/Path;

    .line 1498089
    iput-object p2, p0, LX/9Ua;->b:[F

    .line 1498090
    return-void
.end method

.method public static e(LX/9Ua;FF)V
    .locals 2

    .prologue
    .line 1498091
    iget-object v0, p0, LX/9Ua;->b:[F

    const/4 v1, 0x0

    aput p1, v0, v1

    .line 1498092
    iget-object v0, p0, LX/9Ua;->b:[F

    const/4 v1, 0x1

    aput p2, v0, v1

    .line 1498093
    return-void
.end method

.method public static f(LX/9Ua;FF)V
    .locals 3

    .prologue
    .line 1498094
    iget-object v0, p0, LX/9Ua;->b:[F

    const/4 v1, 0x0

    aget v2, v0, v1

    add-float/2addr v2, p1

    aput v2, v0, v1

    .line 1498095
    iget-object v0, p0, LX/9Ua;->b:[F

    const/4 v1, 0x1

    aget v2, v0, v1

    add-float/2addr v2, p2

    aput v2, v0, v1

    .line 1498096
    return-void
.end method


# virtual methods
.method public final a(FFFFFF)V
    .locals 7

    .prologue
    .line 1498097
    iget-object v0, p0, LX/9Ua;->a:Landroid/graphics/Path;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 1498098
    invoke-static {p0, p5, p6}, LX/9Ua;->e(LX/9Ua;FF)V

    .line 1498099
    return-void
.end method

.method public final a(Landroid/graphics/Matrix;)V
    .locals 1

    .prologue
    .line 1498100
    iget-object v0, p0, LX/9Ua;->a:Landroid/graphics/Path;

    invoke-virtual {v0, p1}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 1498101
    iget-object v0, p0, LX/9Ua;->b:[F

    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 1498102
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1498103
    iget-object v0, p0, LX/9Ua;->a:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 1498104
    invoke-static {p0, v1, v1}, LX/9Ua;->e(LX/9Ua;FF)V

    .line 1498105
    return-void
.end method

.method public final b(FFFFFF)V
    .locals 7

    .prologue
    .line 1498106
    iget-object v0, p0, LX/9Ua;->a:Landroid/graphics/Path;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->rCubicTo(FFFFFF)V

    .line 1498107
    invoke-static {p0, p5, p6}, LX/9Ua;->f(LX/9Ua;FF)V

    .line 1498108
    return-void
.end method
