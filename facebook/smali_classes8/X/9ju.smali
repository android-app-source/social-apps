.class public LX/9ju;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/9j7;

.field public final c:Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;

.field public final d:LX/9j5;

.field public final e:LX/9jQ;

.field public final f:LX/9kB;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/9kB",
            "<",
            "LX/9jt;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0ad;

.field public final h:Landroid/os/Handler;

.field private final i:Lcom/facebook/common/perftest/PerfTestConfig;

.field public j:LX/90X;

.field public k:Ljava/lang/Runnable;

.field public l:Ljava/lang/Runnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1530524
    const-class v0, LX/9ju;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/9ju;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/9j7;LX/9j5;LX/9jQ;LX/9kB;Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;LX/0ad;Lcom/facebook/common/perftest/PerfTestConfig;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1530514
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1530515
    const-class v0, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;

    invoke-static {v0, p5}, LX/9jy;->a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;

    iput-object v0, p0, LX/9ju;->c:Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;

    .line 1530516
    const-class v0, LX/9j7;

    invoke-static {v0, p1}, LX/9jy;->a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9j7;

    iput-object v0, p0, LX/9ju;->b:LX/9j7;

    .line 1530517
    const-class v0, LX/9j5;

    invoke-static {v0, p2}, LX/9jy;->a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9j5;

    iput-object v0, p0, LX/9ju;->d:LX/9j5;

    .line 1530518
    const-class v0, LX/9jQ;

    invoke-static {v0, p3}, LX/9jy;->a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9jQ;

    iput-object v0, p0, LX/9ju;->e:LX/9jQ;

    .line 1530519
    const-class v0, LX/9kB;

    invoke-static {v0, p4}, LX/9jy;->a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9kB;

    iput-object v0, p0, LX/9ju;->f:LX/9kB;

    .line 1530520
    const-class v0, LX/0ad;

    invoke-static {v0, p6}, LX/9jy;->a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    iput-object v0, p0, LX/9ju;->g:LX/0ad;

    .line 1530521
    const-class v0, Landroid/os/Handler;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-static {v0, v1}, LX/9jy;->a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, LX/9ju;->h:Landroid/os/Handler;

    .line 1530522
    iput-object p7, p0, LX/9ju;->i:Lcom/facebook/common/perftest/PerfTestConfig;

    .line 1530523
    return-void
.end method

.method public static a(LX/0QB;)LX/9ju;
    .locals 1

    .prologue
    .line 1530567
    invoke-static {p0}, LX/9ju;->b(LX/0QB;)LX/9ju;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/9ju;
    .locals 8

    .prologue
    .line 1530565
    new-instance v0, LX/9ju;

    invoke-static {p0}, LX/9j7;->a(LX/0QB;)LX/9j7;

    move-result-object v1

    check-cast v1, LX/9j7;

    invoke-static {p0}, LX/9j5;->a(LX/0QB;)LX/9j5;

    move-result-object v2

    check-cast v2, LX/9j5;

    invoke-static {p0}, LX/9jQ;->a(LX/0QB;)LX/9jQ;

    move-result-object v3

    check-cast v3, LX/9jQ;

    invoke-static {p0}, LX/9kB;->c(LX/0QB;)LX/9kB;

    move-result-object v4

    check-cast v4, LX/9kB;

    invoke-static {p0}, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;->b(LX/0QB;)Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;

    move-result-object v5

    check-cast v5, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-static {p0}, Lcom/facebook/common/perftest/PerfTestConfig;->a(LX/0QB;)Lcom/facebook/common/perftest/PerfTestConfig;

    move-result-object v7

    check-cast v7, Lcom/facebook/common/perftest/PerfTestConfig;

    invoke-direct/range {v0 .. v7}, LX/9ju;-><init>(LX/9j7;LX/9j5;LX/9jQ;LX/9kB;Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;LX/0ad;Lcom/facebook/common/perftest/PerfTestConfig;)V

    .line 1530566
    return-object v0
.end method

.method public static f(LX/9ju;)V
    .locals 2

    .prologue
    .line 1530568
    iget-object v0, p0, LX/9ju;->k:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 1530569
    iget-object v0, p0, LX/9ju;->h:Landroid/os/Handler;

    iget-object v1, p0, LX/9ju;->k:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1530570
    const/4 v0, 0x0

    iput-object v0, p0, LX/9ju;->k:Ljava/lang/Runnable;

    .line 1530571
    :cond_0
    iget-object v0, p0, LX/9ju;->j:LX/90X;

    invoke-interface {v0}, LX/90X;->a()V

    .line 1530572
    return-void
.end method

.method public static g(LX/9ju;)V
    .locals 4

    .prologue
    .line 1530562
    iget-object v0, p0, LX/9ju;->f:LX/9kB;

    sget-object v1, LX/9jt;->MOST_RECENT:LX/9jt;

    new-instance v2, LX/9jr;

    invoke-direct {v2, p0}, LX/9jr;-><init>(LX/9ju;)V

    new-instance v3, LX/9js;

    invoke-direct {v3, p0}, LX/9js;-><init>(LX/9ju;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1530563
    iget-object v0, p0, LX/9ju;->j:LX/90X;

    invoke-interface {v0}, LX/90X;->a()V

    .line 1530564
    return-void
.end method

.method public static i(LX/9ju;)V
    .locals 1

    .prologue
    .line 1530559
    iget-object v0, p0, LX/9ju;->f:LX/9kB;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 1530560
    iget-object v0, p0, LX/9ju;->c:Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;

    invoke-virtual {v0}, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;->b()V

    .line 1530561
    return-void
.end method


# virtual methods
.method public final a(LX/9jo;Z)V
    .locals 12

    .prologue
    .line 1530525
    iget-object v0, p1, LX/9jo;->c:LX/9jG;

    move-object v0, v0

    .line 1530526
    invoke-virtual {v0}, LX/9jG;->isSocialSearchType()Z

    move-result v0

    .line 1530527
    iget-object v1, p1, LX/9jo;->b:Landroid/location/Location;

    move-object v1, v1

    .line 1530528
    if-nez v1, :cond_0

    .line 1530529
    iget-object v1, p1, LX/9jo;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1530530
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    .line 1530531
    iget-object v0, p0, LX/9ju;->c:Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;

    invoke-virtual {v0}, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;->b()V

    .line 1530532
    invoke-static {p0}, LX/9ju;->f(LX/9ju;)V

    .line 1530533
    invoke-static {p0}, LX/9ju;->g(LX/9ju;)V

    .line 1530534
    :goto_0
    return-void

    .line 1530535
    :cond_0
    invoke-static {p0}, LX/9ju;->i(LX/9ju;)V

    .line 1530536
    if-nez p2, :cond_1

    .line 1530537
    iget-object v1, p0, LX/9ju;->b:LX/9j7;

    .line 1530538
    iget-object v2, p1, LX/9jo;->c:LX/9jG;

    move-object v2, v2

    .line 1530539
    const v6, 0x150018

    .line 1530540
    iget-object v3, v1, LX/9j7;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v4, 0x150017

    const/4 v5, 0x2

    invoke-interface {v3, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 1530541
    iget-object v3, v1, LX/9j7;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v3, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 1530542
    invoke-static {v1, v6, v2}, LX/9j7;->a(LX/9j7;ILX/9jG;)V

    .line 1530543
    :cond_1
    if-nez p2, :cond_2

    if-nez v0, :cond_2

    .line 1530544
    new-instance v0, Lcom/facebook/places/checkin/protocol/PlacePickerFetcher$1;

    invoke-direct {v0, p0}, Lcom/facebook/places/checkin/protocol/PlacePickerFetcher$1;-><init>(LX/9ju;)V

    iput-object v0, p0, LX/9ju;->l:Ljava/lang/Runnable;

    .line 1530545
    iget-object v0, p0, LX/9ju;->h:Landroid/os/Handler;

    iget-object v1, p0, LX/9ju;->l:Ljava/lang/Runnable;

    .line 1530546
    iget-object v2, p1, LX/9jo;->c:LX/9jG;

    move-object v2, v2

    .line 1530547
    const-wide/16 v7, 0x0

    .line 1530548
    sget-wide v10, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->l:J

    move-wide v5, v10

    .line 1530549
    cmp-long v9, v5, v7

    if-eqz v9, :cond_3

    .line 1530550
    :goto_1
    move-wide v2, v5

    .line 1530551
    const v4, -0x5175eb76

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1530552
    :cond_2
    iget-object v0, p0, LX/9ju;->c:Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;

    .line 1530553
    iget-object v1, p1, LX/9jo;->c:LX/9jG;

    move-object v1, v1

    .line 1530554
    new-instance v2, LX/9jq;

    invoke-direct {v2, p0, v1}, LX/9jq;-><init>(LX/9ju;LX/9jG;)V

    move-object v1, v2

    .line 1530555
    invoke-virtual {v0, p1, v1}, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;->a(LX/9jo;LX/0TF;)V

    .line 1530556
    iget-object v0, p0, LX/9ju;->j:LX/90X;

    invoke-interface {v0}, LX/90X;->a()V

    goto :goto_0

    .line 1530557
    :cond_3
    sget-object v5, LX/9jG;->STATUS:LX/9jG;

    if-eq v2, v5, :cond_4

    sget-object v5, LX/9jG;->PHOTO:LX/9jG;

    if-eq v2, v5, :cond_4

    sget-object v5, LX/9jG;->VIDEO:LX/9jG;

    if-ne v2, v5, :cond_6

    :cond_4
    const/4 v5, 0x1

    :goto_2
    move v5, v5

    .line 1530558
    if-eqz v5, :cond_5

    move-wide v5, v7

    goto :goto_1

    :cond_5
    const-wide/16 v5, 0x1388

    goto :goto_1

    :cond_6
    const/4 v5, 0x0

    goto :goto_2
.end method
