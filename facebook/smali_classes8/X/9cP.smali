.class public LX/9cP;
.super LX/0b4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b4",
        "<",
        "LX/9cQ;",
        "LX/9cO;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/9cP;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1516530
    invoke-direct {p0}, LX/0b4;-><init>()V

    .line 1516531
    return-void
.end method

.method public static a(LX/0QB;)LX/9cP;
    .locals 3

    .prologue
    .line 1516518
    sget-object v0, LX/9cP;->a:LX/9cP;

    if-nez v0, :cond_1

    .line 1516519
    const-class v1, LX/9cP;

    monitor-enter v1

    .line 1516520
    :try_start_0
    sget-object v0, LX/9cP;->a:LX/9cP;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1516521
    if-eqz v2, :cond_0

    .line 1516522
    :try_start_1
    new-instance v0, LX/9cP;

    invoke-direct {v0}, LX/9cP;-><init>()V

    .line 1516523
    move-object v0, v0

    .line 1516524
    sput-object v0, LX/9cP;->a:LX/9cP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1516525
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1516526
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1516527
    :cond_1
    sget-object v0, LX/9cP;->a:LX/9cP;

    return-object v0

    .line 1516528
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1516529
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
