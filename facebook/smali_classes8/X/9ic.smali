.class public LX/9ic;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Landroid/graphics/RectF;


# instance fields
.field public b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/photos/base/tagging/FaceBox;",
            "Lcom/facebook/photos/base/tagging/FaceBox;",
            ">;"
        }
    .end annotation
.end field

.field public c:Landroid/graphics/RectF;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 1528144
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v1, v1, v2, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    sput-object v0, LX/9ic;->a:Landroid/graphics/RectF;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1528105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1528106
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/9ic;->b:Ljava/util/Map;

    .line 1528107
    return-void
.end method

.method public static a(LX/0QB;)LX/9ic;
    .locals 1

    .prologue
    .line 1528141
    new-instance v0, LX/9ic;

    invoke-direct {v0}, LX/9ic;-><init>()V

    .line 1528142
    move-object v0, v0

    .line 1528143
    return-object v0
.end method

.method public static a(Ljava/util/List;Landroid/graphics/RectF;I)Ljava/util/List;
    .locals 5
    .param p0    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Landroid/graphics/RectF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/base/tagging/FaceBox;",
            ">;",
            "Landroid/graphics/RectF;",
            "I)",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation

    .prologue
    const/high16 v4, 0x3f000000    # 0.5f

    .line 1528127
    if-nez p0, :cond_0

    .line 1528128
    const/4 v0, 0x0

    .line 1528129
    :goto_0
    return-object v0

    .line 1528130
    :cond_0
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 1528131
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 1528132
    if-eqz p1, :cond_1

    .line 1528133
    sget-object v0, LX/9ic;->a:Landroid/graphics/RectF;

    sget-object v3, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v2, p1, v0, v3}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 1528134
    :cond_1
    int-to-float v0, p2

    invoke-virtual {v2, v0, v4, v4}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 1528135
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1528136
    new-instance v4, Landroid/graphics/RectF;

    invoke-virtual {v0}, Lcom/facebook/photos/base/tagging/FaceBox;->d()Landroid/graphics/RectF;

    move-result-object v0

    invoke-direct {v4, v0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 1528137
    if-eqz p1, :cond_3

    invoke-virtual {p1, v4}, Landroid/graphics/RectF;->contains(Landroid/graphics/RectF;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1528138
    :cond_3
    invoke-virtual {v2, v4}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 1528139
    invoke-virtual {v1, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1528140
    :cond_4
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/photos/base/tagging/FaceBox;Lcom/facebook/photos/base/tagging/FaceBox;)Z
    .locals 2

    .prologue
    .line 1528122
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1528123
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1528124
    invoke-virtual {p0}, Lcom/facebook/photos/base/tagging/FaceBox;->d()Landroid/graphics/RectF;

    move-result-object v0

    iget v0, v0, Landroid/graphics/RectF;->left:F

    invoke-virtual {p1}, Lcom/facebook/photos/base/tagging/FaceBox;->d()Landroid/graphics/RectF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/RectF;->left:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/photos/base/tagging/FaceBox;->d()Landroid/graphics/RectF;

    move-result-object v0

    iget v0, v0, Landroid/graphics/RectF;->right:F

    invoke-virtual {p1}, Lcom/facebook/photos/base/tagging/FaceBox;->d()Landroid/graphics/RectF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/RectF;->right:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/photos/base/tagging/FaceBox;->d()Landroid/graphics/RectF;

    move-result-object v0

    iget v0, v0, Landroid/graphics/RectF;->top:F

    invoke-virtual {p1}, Lcom/facebook/photos/base/tagging/FaceBox;->d()Landroid/graphics/RectF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/RectF;->top:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/photos/base/tagging/FaceBox;->d()Landroid/graphics/RectF;

    move-result-object v0

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {p1}, Lcom/facebook/photos/base/tagging/FaceBox;->d()Landroid/graphics/RectF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 1528125
    const/4 v0, 0x1

    .line 1528126
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Ljava/util/List;)V
    .locals 9
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/base/tagging/FaceBox;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/high16 v4, 0x3f000000    # 0.5f

    .line 1528145
    iget-object v0, p0, LX/9ic;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1528146
    if-nez p1, :cond_1

    .line 1528147
    :cond_0
    return-void

    .line 1528148
    :cond_1
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 1528149
    iget-object v0, p0, LX/9ic;->c:Landroid/graphics/RectF;

    if-eqz v0, :cond_2

    .line 1528150
    iget-object v0, p0, LX/9ic;->c:Landroid/graphics/RectF;

    sget-object v2, LX/9ic;->a:Landroid/graphics/RectF;

    sget-object v3, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v1, v0, v2, v3}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 1528151
    :cond_2
    iget v0, p0, LX/9ic;->d:I

    int-to-float v0, v0

    invoke-virtual {v1, v0, v4, v4}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 1528152
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1528153
    iget-boolean v3, v0, Lcom/facebook/photos/base/tagging/FaceBox;->f:Z

    move v3, v3

    .line 1528154
    if-nez v3, :cond_3

    .line 1528155
    new-instance v3, Landroid/graphics/RectF;

    invoke-virtual {v0}, Lcom/facebook/photos/base/tagging/FaceBox;->d()Landroid/graphics/RectF;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 1528156
    iget-object v4, p0, LX/9ic;->c:Landroid/graphics/RectF;

    if-nez v4, :cond_4

    .line 1528157
    iget-object v3, p0, LX/9ic;->b:Ljava/util/Map;

    invoke-interface {v3, v0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1528158
    :cond_4
    const/4 p1, 0x0

    .line 1528159
    new-instance v4, Landroid/graphics/RectF;

    invoke-virtual {v0}, Lcom/facebook/photos/base/tagging/FaceBox;->d()Landroid/graphics/RectF;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 1528160
    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v5

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v6

    mul-float/2addr v5, v6

    .line 1528161
    iget-object v6, p0, LX/9ic;->c:Landroid/graphics/RectF;

    if-eqz v6, :cond_6

    .line 1528162
    iget-object v6, p0, LX/9ic;->c:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->right:F

    iget v7, v4, Landroid/graphics/RectF;->right:F

    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v6

    iget-object v7, p0, LX/9ic;->c:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->left:F

    iget v8, v4, Landroid/graphics/RectF;->left:F

    invoke-static {v7, v8}, Ljava/lang/Math;->max(FF)F

    move-result v7

    sub-float/2addr v6, v7

    invoke-static {p1, v6}, Ljava/lang/Math;->max(FF)F

    move-result v6

    .line 1528163
    iget-object v7, p0, LX/9ic;->c:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->bottom:F

    iget v8, v4, Landroid/graphics/RectF;->bottom:F

    invoke-static {v7, v8}, Ljava/lang/Math;->min(FF)F

    move-result v7

    iget-object v8, p0, LX/9ic;->c:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->top:F

    iget v4, v4, Landroid/graphics/RectF;->top:F

    invoke-static {v8, v4}, Ljava/lang/Math;->max(FF)F

    move-result v4

    sub-float v4, v7, v4

    invoke-static {p1, v4}, Ljava/lang/Math;->max(FF)F

    move-result v4

    .line 1528164
    mul-float/2addr v4, v6

    .line 1528165
    :goto_1
    iget-object v6, p0, LX/9ic;->c:Landroid/graphics/RectF;

    invoke-virtual {v0}, Lcom/facebook/photos/base/tagging/FaceBox;->b()Landroid/graphics/PointF;

    move-result-object v7

    iget v7, v7, Landroid/graphics/PointF;->x:F

    invoke-virtual {v0}, Lcom/facebook/photos/base/tagging/FaceBox;->b()Landroid/graphics/PointF;

    move-result-object v8

    iget v8, v8, Landroid/graphics/PointF;->y:F

    invoke-virtual {v6, v7, v8}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v6

    if-eqz v6, :cond_5

    const/high16 v6, 0x3f000000    # 0.5f

    mul-float/2addr v5, v6

    cmpl-float v4, v4, v5

    if-ltz v4, :cond_5

    const/4 v4, 0x1

    :goto_2
    move v4, v4

    .line 1528166
    if-eqz v4, :cond_3

    .line 1528167
    invoke-virtual {v1, v3}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 1528168
    iget-object v4, p0, LX/9ic;->b:Ljava/util/Map;

    new-instance v5, Lcom/facebook/photos/base/tagging/FaceBox;

    invoke-virtual {v0}, Lcom/facebook/photos/base/tagging/FaceBox;->n()Ljava/util/List;

    move-result-object v6

    .line 1528169
    iget-boolean v7, v0, Lcom/facebook/photos/base/tagging/FaceBox;->f:Z

    move v7, v7

    .line 1528170
    const/4 v8, 0x0

    invoke-direct {v5, v3, v6, v7, v8}, Lcom/facebook/photos/base/tagging/FaceBox;-><init>(Landroid/graphics/RectF;Ljava/util/List;ZZ)V

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :cond_5
    const/4 v4, 0x0

    goto :goto_2

    :cond_6
    move v4, v5

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/graphics/RectF;)Lcom/facebook/photos/base/tagging/FaceBox;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1528117
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1528118
    iget-object v0, p0, LX/9ic;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1528119
    invoke-virtual {v0}, Lcom/facebook/photos/base/tagging/FaceBox;->d()Landroid/graphics/RectF;

    move-result-object v2

    if-ne v2, p1, :cond_0

    .line 1528120
    iget-object v1, p0, LX/9ic;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1528121
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/photos/base/tagging/FaceBox;)Lcom/facebook/photos/base/tagging/FaceBox;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1528113
    iget-object v0, p0, LX/9ic;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1528114
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    if-eq v1, p1, :cond_1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/base/tagging/FaceBox;

    invoke-static {v1, p1}, LX/9ic;->a(Lcom/facebook/photos/base/tagging/FaceBox;Lcom/facebook/photos/base/tagging/FaceBox;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1528115
    :cond_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1528116
    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)Ljava/util/Collection;
    .locals 3
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/base/tagging/FaceBox;",
            ">;)",
            "Ljava/util/Collection",
            "<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1528108
    invoke-direct {p0, p1}, LX/9ic;->c(Ljava/util/List;)V

    .line 1528109
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 1528110
    iget-object v0, p0, LX/9ic;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1528111
    invoke-virtual {v0}, Lcom/facebook/photos/base/tagging/FaceBox;->d()Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1528112
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/graphics/RectF;Ljava/util/List;I)V
    .locals 1
    .param p1    # Landroid/graphics/RectF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/RectF;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/base/tagging/FaceBox;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 1528099
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1528100
    sget-object v0, LX/9ic;->a:Landroid/graphics/RectF;

    invoke-virtual {v0, p1}, Landroid/graphics/RectF;->contains(Landroid/graphics/RectF;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1528101
    iput-object p1, p0, LX/9ic;->c:Landroid/graphics/RectF;

    .line 1528102
    iput p3, p0, LX/9ic;->d:I

    .line 1528103
    invoke-direct {p0, p2}, LX/9ic;->c(Ljava/util/List;)V

    .line 1528104
    return-void
.end method
