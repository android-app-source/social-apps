.class public final LX/AA4;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const-wide/16 v4, 0x0

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 1636828
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_8

    .line 1636829
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1636830
    :goto_0
    return v1

    .line 1636831
    :cond_0
    const-string v12, "start_timestamp"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 1636832
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v0, v6

    .line 1636833
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_6

    .line 1636834
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1636835
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1636836
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 1636837
    const-string v12, "event_buy_ticket_display_url"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 1636838
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 1636839
    :cond_2
    const-string v12, "event_buy_ticket_url"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 1636840
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 1636841
    :cond_3
    const-string v12, "id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 1636842
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 1636843
    :cond_4
    const-string v12, "location"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 1636844
    invoke-static {p0, p1}, LX/AA6;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1636845
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1636846
    :cond_6
    const/4 v11, 0x5

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1636847
    invoke-virtual {p1, v1, v10}, LX/186;->b(II)V

    .line 1636848
    invoke-virtual {p1, v6, v9}, LX/186;->b(II)V

    .line 1636849
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 1636850
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 1636851
    if-eqz v0, :cond_7

    .line 1636852
    const/4 v1, 0x4

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1636853
    :cond_7
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_8
    move v0, v1

    move-wide v2, v4

    move v7, v1

    move v8, v1

    move v9, v1

    move v10, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 10

    .prologue
    const-wide/16 v2, 0x0

    .line 1636854
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1636855
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1636856
    if-eqz v0, :cond_0

    .line 1636857
    const-string v1, "event_buy_ticket_display_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636858
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1636859
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1636860
    if-eqz v0, :cond_1

    .line 1636861
    const-string v1, "event_buy_ticket_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636862
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1636863
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1636864
    if-eqz v0, :cond_2

    .line 1636865
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636866
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1636867
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1636868
    if-eqz v0, :cond_7

    .line 1636869
    const-string v1, "location"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636870
    const-wide/16 v8, 0x0

    .line 1636871
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1636872
    const/4 v4, 0x0

    invoke-virtual {p0, v0, v4, v8, v9}, LX/15i;->a(IID)D

    move-result-wide v4

    .line 1636873
    cmpl-double v6, v4, v8

    if-eqz v6, :cond_3

    .line 1636874
    const-string v6, "latitude"

    invoke-virtual {p2, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636875
    invoke-virtual {p2, v4, v5}, LX/0nX;->a(D)V

    .line 1636876
    :cond_3
    const/4 v4, 0x1

    invoke-virtual {p0, v0, v4, v8, v9}, LX/15i;->a(IID)D

    move-result-wide v4

    .line 1636877
    cmpl-double v6, v4, v8

    if-eqz v6, :cond_4

    .line 1636878
    const-string v6, "longitude"

    invoke-virtual {p2, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636879
    invoke-virtual {p2, v4, v5}, LX/0nX;->a(D)V

    .line 1636880
    :cond_4
    const/4 v4, 0x2

    invoke-virtual {p0, v0, v4}, LX/15i;->g(II)I

    move-result v4

    .line 1636881
    if-eqz v4, :cond_6

    .line 1636882
    const-string v5, "reverse_geocode"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636883
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1636884
    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v5

    .line 1636885
    if-eqz v5, :cond_5

    .line 1636886
    const-string v6, "address"

    invoke-virtual {p2, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636887
    invoke-virtual {p2, v5}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1636888
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1636889
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1636890
    :cond_7
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1636891
    cmp-long v2, v0, v2

    if-eqz v2, :cond_8

    .line 1636892
    const-string v2, "start_timestamp"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636893
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1636894
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1636895
    return-void
.end method
