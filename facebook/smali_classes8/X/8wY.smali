.class public LX/8wY;
.super LX/4or;
.source ""

# interfaces
.implements LX/0Ya;


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1422768
    invoke-direct {p0, p1}, LX/4or;-><init>(Landroid/content/Context;)V

    .line 1422769
    const-class v0, LX/8wY;

    invoke-static {v0, p0, p1}, LX/8wY;->a(Ljava/lang/Class;LX/0Ya;Landroid/content/Context;)V

    .line 1422770
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/8wY;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0uQ;->c:LX/0Tn;

    const-wide/32 v4, 0x493e0

    invoke-interface {v1, v2, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1422771
    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/CharSequence;

    const-string v2, "1 second"

    aput-object v2, v1, v6

    const-string v2, "2 seconds"

    aput-object v2, v1, v7

    const-string v2, "5 seconds"

    aput-object v2, v1, v8

    const-string v2, "10 seconds"

    aput-object v2, v1, v9

    const-string v2, "300 seconds"

    aput-object v2, v1, v10

    .line 1422772
    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/CharSequence;

    const-string v3, "1"

    aput-object v3, v2, v6

    const-string v3, "2"

    aput-object v3, v2, v7

    const-string v3, "5"

    aput-object v3, v2, v8

    const-string v3, "10"

    aput-object v3, v2, v9

    const-string v3, "300"

    aput-object v3, v2, v10

    .line 1422773
    invoke-virtual {p0, v1}, LX/8wY;->setEntries([Ljava/lang/CharSequence;)V

    .line 1422774
    invoke-virtual {p0, v2}, LX/8wY;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 1422775
    invoke-virtual {p0, v0}, LX/8wY;->setDefaultValue(Ljava/lang/Object;)V

    .line 1422776
    const-class v0, LX/8wY;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/8wY;->setKey(Ljava/lang/String;)V

    .line 1422777
    const-string v0, "Batch Log Interval"

    invoke-virtual {p0, v0}, LX/8wY;->setTitle(Ljava/lang/CharSequence;)V

    .line 1422778
    const-string v0, "how long to batch logs before send them back"

    invoke-virtual {p0, v0}, LX/8wY;->setSummary(Ljava/lang/CharSequence;)V

    .line 1422779
    new-instance v0, LX/8wX;

    invoke-direct {v0, p0}, LX/8wX;-><init>(LX/8wY;)V

    invoke-virtual {p0, v0}, LX/8wY;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 1422780
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/0Ya;Landroid/content/Context;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0Ya;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/8wY;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object p0

    check-cast p0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p0, p1, LX/8wY;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    return-void
.end method
