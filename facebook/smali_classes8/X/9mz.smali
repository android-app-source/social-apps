.class public LX/9mz;
.super LX/5pb;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "DeviceEventManager"
.end annotation


# instance fields
.field private final a:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(LX/5pY;LX/98l;)V
    .locals 1

    .prologue
    .line 1536566
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 1536567
    new-instance v0, Lcom/facebook/react/modules/core/DeviceEventManagerModule$1;

    invoke-direct {v0, p0, p2}, Lcom/facebook/react/modules/core/DeviceEventManagerModule$1;-><init>(LX/9mz;LX/98l;)V

    iput-object v0, p0, LX/9mz;->a:Ljava/lang/Runnable;

    .line 1536568
    return-void
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1536569
    const-string v0, "DeviceEventManager"

    return-object v0
.end method

.method public final h()V
    .locals 3

    .prologue
    .line 1536570
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 1536571
    const-class v1, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;

    invoke-virtual {v0, v1}, LX/5pX;->a(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;

    const-string v1, "hardwareBackPress"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;->emit(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1536572
    return-void
.end method

.method public invokeDefaultBackPressHandler()V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1536573
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 1536574
    iget-object v1, p0, LX/9mz;->a:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, LX/5pX;->a(Ljava/lang/Runnable;)V

    .line 1536575
    return-void
.end method
