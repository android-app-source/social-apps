.class public final LX/9ps;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 1545568
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1545569
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1545570
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1545571
    invoke-static {p0, p1}, LX/9ps;->b(LX/15w;LX/186;)I

    move-result v1

    .line 1545572
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1545573
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1545574
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1545575
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1545576
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/9ps;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1545577
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1545578
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1545579
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1545580
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_9

    .line 1545581
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1545582
    :goto_0
    return v1

    .line 1545583
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_6

    .line 1545584
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1545585
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1545586
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_0

    if-eqz v8, :cond_0

    .line 1545587
    const-string v9, "is_request"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 1545588
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v7, v3

    move v3, v2

    goto :goto_1

    .line 1545589
    :cond_1
    const-string v9, "is_seen"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 1545590
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v6, v0

    move v0, v2

    goto :goto_1

    .line 1545591
    :cond_2
    const-string v9, "requester"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1545592
    invoke-static {p0, p1}, LX/9pq;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1545593
    :cond_3
    const-string v9, "suggesters"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1545594
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1545595
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->START_ARRAY:LX/15z;

    if-ne v8, v9, :cond_4

    .line 1545596
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_ARRAY:LX/15z;

    if-eq v8, v9, :cond_4

    .line 1545597
    invoke-static {p0, p1}, LX/9pr;->b(LX/15w;LX/186;)I

    move-result v8

    .line 1545598
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1545599
    :cond_4
    invoke-static {v4, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v4

    move v4, v4

    .line 1545600
    goto :goto_1

    .line 1545601
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1545602
    :cond_6
    const/4 v8, 0x4

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1545603
    if-eqz v3, :cond_7

    .line 1545604
    invoke-virtual {p1, v1, v7}, LX/186;->a(IZ)V

    .line 1545605
    :cond_7
    if-eqz v0, :cond_8

    .line 1545606
    invoke-virtual {p1, v2, v6}, LX/186;->a(IZ)V

    .line 1545607
    :cond_8
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1545608
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1545609
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_9
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1545610
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1545611
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1545612
    if-eqz v0, :cond_0

    .line 1545613
    const-string v1, "is_request"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1545614
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1545615
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1545616
    if-eqz v0, :cond_1

    .line 1545617
    const-string v1, "is_seen"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1545618
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1545619
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1545620
    if-eqz v0, :cond_2

    .line 1545621
    const-string v1, "requester"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1545622
    invoke-static {p0, v0, p2, p3}, LX/9pq;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1545623
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1545624
    if-eqz v0, :cond_4

    .line 1545625
    const-string v1, "suggesters"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1545626
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1545627
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result p1

    if-ge v1, p1, :cond_3

    .line 1545628
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result p1

    invoke-static {p0, p1, p2}, LX/9pr;->a(LX/15i;ILX/0nX;)V

    .line 1545629
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1545630
    :cond_3
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1545631
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1545632
    return-void
.end method
