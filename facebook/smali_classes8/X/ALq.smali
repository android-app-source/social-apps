.class public LX/ALq;
.super Landroid/widget/FrameLayout;
.source ""


# static fields
.field public static final d:Ljava/lang/String;


# instance fields
.field public a:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1En;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final e:Landroid/view/View$OnClickListener;

.field private final f:Landroid/view/View$OnClickListener;

.field private final g:LX/8CF;

.field public final h:Landroid/view/View;

.field private final i:Landroid/view/View;

.field private final j:Lcom/facebook/resources/ui/FbTextView;

.field public final k:Lcom/facebook/messaging/doodle/ColourIndicator;

.field private final l:Lcom/facebook/messaging/doodle/ColourPicker;

.field public final m:LX/AM1;

.field public n:LX/7gj;

.field public o:LX/ALp;

.field public p:Lcom/google/common/util/concurrent/ListenableFuture;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1665437
    const-class v0, LX/ALq;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/ALq;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1665438
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/ALq;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1665439
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1665414
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/ALq;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1665415
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1665416
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1665417
    new-instance v0, LX/ALl;

    invoke-direct {v0, p0}, LX/ALl;-><init>(LX/ALq;)V

    iput-object v0, p0, LX/ALq;->e:Landroid/view/View$OnClickListener;

    .line 1665418
    new-instance v0, LX/ALm;

    invoke-direct {v0, p0}, LX/ALm;-><init>(LX/ALq;)V

    iput-object v0, p0, LX/ALq;->f:Landroid/view/View$OnClickListener;

    .line 1665419
    new-instance v0, LX/ALn;

    invoke-direct {v0, p0}, LX/ALn;-><init>(LX/ALq;)V

    iput-object v0, p0, LX/ALq;->g:LX/8CF;

    .line 1665420
    const-class v0, LX/ALq;

    invoke-static {v0, p0}, LX/ALq;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1665421
    const v0, 0x7f031491

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1665422
    const v0, 0x7f0d119d

    invoke-virtual {p0, v0}, LX/ALq;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/ALq;->h:Landroid/view/View;

    .line 1665423
    const v0, 0x7f0d2ec6

    invoke-virtual {p0, v0}, LX/ALq;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/ALq;->i:Landroid/view/View;

    .line 1665424
    const v0, 0x7f0d2ec5

    invoke-virtual {p0, v0}, LX/ALq;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/ALq;->j:Lcom/facebook/resources/ui/FbTextView;

    .line 1665425
    const v0, 0x7f0d2509

    invoke-virtual {p0, v0}, LX/ALq;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/doodle/ColourIndicator;

    iput-object v0, p0, LX/ALq;->k:Lcom/facebook/messaging/doodle/ColourIndicator;

    .line 1665426
    const v0, 0x7f0d250a

    invoke-virtual {p0, v0}, LX/ALq;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/doodle/ColourPicker;

    iput-object v0, p0, LX/ALq;->l:Lcom/facebook/messaging/doodle/ColourPicker;

    .line 1665427
    iget-object v0, p0, LX/ALq;->l:Lcom/facebook/messaging/doodle/ColourPicker;

    iget-object v1, p0, LX/ALq;->g:LX/8CF;

    .line 1665428
    iput-object v1, v0, Lcom/facebook/messaging/doodle/ColourPicker;->c:LX/8CF;

    .line 1665429
    const v0, 0x7f0d06be

    invoke-virtual {p0, v0}, LX/ALq;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/backstage/ui/ScalingToggleableEditText;

    iput-object v0, p0, LX/ALq;->m:LX/AM1;

    .line 1665430
    iget-object v0, p0, LX/ALq;->j:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/text/DateFormat;->getTimeInstance(I)Ljava/text/DateFormat;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1665431
    iget-object v0, p0, LX/ALq;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0}, LX/ALq;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 1665432
    iget-object v0, p0, LX/ALq;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0}, LX/ALq;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 1665433
    iget-object v0, p0, LX/ALq;->m:LX/AM1;

    iget-object v1, p0, LX/ALq;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, LX/AM1;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1665434
    iget-object v0, p0, LX/ALq;->h:Landroid/view/View;

    iget-object v1, p0, LX/ALq;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1665435
    iget-object v0, p0, LX/ALq;->i:Landroid/view/View;

    new-instance v1, LX/ALo;

    invoke-direct {v1, p0}, LX/ALo;-><init>(LX/ALq;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1665436
    return-void
.end method

.method private static a(LX/ALq;LX/0TD;LX/1En;LX/03V;)V
    .locals 0

    .prologue
    .line 1665413
    iput-object p1, p0, LX/ALq;->a:LX/0TD;

    iput-object p2, p0, LX/ALq;->b:LX/1En;

    iput-object p3, p0, LX/ALq;->c:LX/03V;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, LX/ALq;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, LX/ALq;

    invoke-static {v2}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v0

    check-cast v0, LX/0TD;

    invoke-static {v2}, LX/1En;->a(LX/0QB;)LX/1En;

    move-result-object v1

    check-cast v1, LX/1En;

    invoke-static {v2}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {p0, v0, v1, v2}, LX/ALq;->a(LX/ALq;LX/0TD;LX/1En;LX/03V;)V

    return-void
.end method

.method public static b(LX/ALq;)V
    .locals 2

    .prologue
    .line 1665411
    iget-object v0, p0, LX/ALq;->b:LX/1En;

    iget-object v1, p0, LX/ALq;->n:LX/7gj;

    invoke-virtual {v0, v1}, LX/1En;->a(LX/7gj;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/ALq;->p:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1665412
    return-void
.end method


# virtual methods
.method public setListener(LX/ALp;)V
    .locals 0

    .prologue
    .line 1665409
    iput-object p1, p0, LX/ALq;->o:LX/ALp;

    .line 1665410
    return-void
.end method
