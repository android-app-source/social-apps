.class public final LX/8jQ;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/stickers/model/StickerPack;

.field public final synthetic b:Lcom/facebook/stickers/client/StickerDownloadManager;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/client/StickerDownloadManager;Lcom/facebook/stickers/model/StickerPack;)V
    .locals 0

    .prologue
    .line 1392405
    iput-object p1, p0, LX/8jQ;->b:Lcom/facebook/stickers/client/StickerDownloadManager;

    iput-object p2, p0, LX/8jQ;->a:Lcom/facebook/stickers/model/StickerPack;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final dispose()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 1392406
    invoke-super {p0}, LX/0Vd;->dispose()V

    .line 1392407
    sget-object v0, Lcom/facebook/stickers/client/StickerDownloadManager;->b:Ljava/lang/Class;

    const-string v1, "Image download for pack %s cancelled."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, LX/8jQ;->a:Lcom/facebook/stickers/model/StickerPack;

    .line 1392408
    iget-object v5, v3, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v3, v5

    .line 1392409
    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1392410
    iget-object v0, p0, LX/8jQ;->b:Lcom/facebook/stickers/client/StickerDownloadManager;

    iget-object v1, p0, LX/8jQ;->a:Lcom/facebook/stickers/model/StickerPack;

    invoke-static {v0, v4, v1}, Lcom/facebook/stickers/client/StickerDownloadManager;->a$redex0(Lcom/facebook/stickers/client/StickerDownloadManager;ZLcom/facebook/stickers/model/StickerPack;)V

    .line 1392411
    return-void
.end method

.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 1392412
    sget-object v0, Lcom/facebook/stickers/client/StickerDownloadManager;->b:Ljava/lang/Class;

    const-string v1, "Unable to download sticker pack %s"

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/8jQ;->a:Lcom/facebook/stickers/model/StickerPack;

    .line 1392413
    iget-object v6, v4, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v4, v6

    .line 1392414
    aput-object v4, v2, v3

    invoke-static {v0, p1, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1392415
    iget-object v0, p0, LX/8jQ;->b:Lcom/facebook/stickers/client/StickerDownloadManager;

    iget-object v1, p0, LX/8jQ;->a:Lcom/facebook/stickers/model/StickerPack;

    invoke-static {v0, v5, v1}, Lcom/facebook/stickers/client/StickerDownloadManager;->a$redex0(Lcom/facebook/stickers/client/StickerDownloadManager;ZLcom/facebook/stickers/model/StickerPack;)V

    .line 1392416
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1392417
    iget-object v0, p0, LX/8jQ;->b:Lcom/facebook/stickers/client/StickerDownloadManager;

    const/4 v1, 0x1

    iget-object v2, p0, LX/8jQ;->a:Lcom/facebook/stickers/model/StickerPack;

    invoke-static {v0, v1, v2}, Lcom/facebook/stickers/client/StickerDownloadManager;->a$redex0(Lcom/facebook/stickers/client/StickerDownloadManager;ZLcom/facebook/stickers/model/StickerPack;)V

    .line 1392418
    return-void
.end method
