.class public final LX/9cv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9cu;


# instance fields
.field public final synthetic a:LX/9d5;


# direct methods
.method public constructor <init>(LX/9d5;)V
    .locals 0

    .prologue
    .line 1516893
    iput-object p1, p0, LX/9cv;->a:LX/9d5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1516894
    iget-object v0, p0, LX/9cv;->a:LX/9d5;

    invoke-virtual {v0}, LX/9d5;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1516895
    iget-object v0, p0, LX/9cv;->a:LX/9d5;

    iget-object v0, v0, LX/9d5;->m:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->a(Z)V

    .line 1516896
    :cond_0
    return-void
.end method

.method public final a(LX/5jK;LX/5jJ;)V
    .locals 4

    .prologue
    .line 1516897
    iget-object v0, p0, LX/9cv;->a:LX/9d5;

    iget-boolean v0, v0, LX/9d5;->w:Z

    if-nez v0, :cond_1

    .line 1516898
    :cond_0
    :goto_0
    return-void

    .line 1516899
    :cond_1
    sget-object v0, LX/9d4;->a:[I

    invoke-virtual {p2}, LX/5jJ;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1516900
    :pswitch_0
    iget-object v0, p0, LX/9cv;->a:LX/9d5;

    iget-object v0, v0, LX/9d5;->K:Lcom/facebook/widget/ScrollingAwareScrollView;

    if-eqz v0, :cond_2

    .line 1516901
    iget-object v0, p0, LX/9cv;->a:LX/9d5;

    iget-object v0, v0, LX/9d5;->K:Lcom/facebook/widget/ScrollingAwareScrollView;

    const/4 v1, 0x1

    .line 1516902
    iput-boolean v1, v0, Lcom/facebook/widget/ScrollingAwareScrollView;->a:Z

    .line 1516903
    :cond_2
    iget-object v0, p0, LX/9cv;->a:LX/9d5;

    iget-boolean v0, v0, LX/9d5;->y:Z

    if-nez v0, :cond_3

    .line 1516904
    sget-object v0, LX/9d5;->a:Ljava/lang/String;

    const-string v1, "Invisible, no need to update the swiping state"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1516905
    :cond_3
    invoke-virtual {p1}, LX/5jK;->h()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p1}, LX/5jK;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1516906
    :cond_4
    invoke-virtual {p1}, LX/5jK;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1516907
    iget-object v0, p0, LX/9cv;->a:LX/9d5;

    const/4 v2, 0x1

    .line 1516908
    iget-object v1, v0, LX/9d5;->D:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    iget-object p0, v0, LX/9d5;->F:LX/0Px;

    iget-object p1, v0, LX/9d5;->D:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1516909
    iget-object p2, p1, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->b:Ljava/lang/String;

    move-object p1, p2

    .line 1516910
    invoke-static {p0, p1}, LX/8GN;->a(LX/0Px;Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    move v1, v2

    .line 1516911
    :goto_1
    if-eqz v1, :cond_c

    iget-object v1, v0, LX/9d5;->F:LX/0Px;

    iget-object p0, v0, LX/9d5;->E:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    invoke-static {v1, p0}, LX/8GN;->b(LX/0Px;Lcom/facebook/photos/creativeediting/model/SwipeableParams;)Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v1

    :goto_2
    iput-object v1, v0, LX/9d5;->C:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1516912
    iget-object v1, v0, LX/9d5;->E:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    iput-object v1, v0, LX/9d5;->D:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1516913
    iget-object v1, v0, LX/9d5;->F:LX/0Px;

    iget-object p0, v0, LX/9d5;->D:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    invoke-static {v1, p0}, LX/8GN;->a(LX/0Px;Lcom/facebook/photos/creativeediting/model/SwipeableParams;)Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v1

    iput-object v1, v0, LX/9d5;->E:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1516914
    iget-object v1, v0, LX/9d5;->A:LX/5iG;

    iput-object v1, v0, LX/9d5;->z:LX/5iG;

    .line 1516915
    iget-object v1, v0, LX/9d5;->B:LX/5iG;

    iput-object v1, v0, LX/9d5;->A:LX/5iG;

    .line 1516916
    iget-object v1, v0, LX/9d5;->B:LX/5iG;

    const/4 p0, 0x2

    invoke-static {v0, v1, p0}, LX/9d5;->b$redex0(LX/9d5;LX/5iG;I)V

    .line 1516917
    const/4 v1, 0x0

    iput-object v1, v0, LX/9d5;->B:LX/5iG;

    .line 1516918
    iget-object v1, v0, LX/9d5;->s:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->e()V

    .line 1516919
    iget-object v1, v0, LX/9d5;->i:LX/9bz;

    iget-object p0, v0, LX/9d5;->r:Ljava/lang/String;

    iget-object p1, v0, LX/9d5;->D:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    invoke-interface {v1, p0, p1, v2}, LX/9bz;->a(Ljava/lang/String;Lcom/facebook/photos/creativeediting/model/SwipeableParams;I)V

    .line 1516920
    iput-boolean v2, v0, LX/9d5;->N:Z

    .line 1516921
    iput v2, v0, LX/9d5;->O:I

    .line 1516922
    iget-object v1, v0, LX/9d5;->m:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    .line 1516923
    iget-object v2, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->q:LX/5iG;

    iput-object v2, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->p:LX/5iG;

    .line 1516924
    iget-object v2, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->r:LX/5iG;

    iput-object v2, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->q:LX/5iG;

    .line 1516925
    const/4 v2, 0x0

    iput-object v2, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->r:LX/5iG;

    .line 1516926
    iget-object v2, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->s:LX/1aX;

    .line 1516927
    iget-object p0, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->t:LX/1aX;

    iput-object p0, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->s:LX/1aX;

    .line 1516928
    iget-object p0, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->u:LX/1aX;

    iput-object p0, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->t:LX/1aX;

    .line 1516929
    iput-object v2, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->u:LX/1aX;

    .line 1516930
    iget-object v2, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->v:LX/1cC;

    .line 1516931
    iget-object p0, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->w:LX/1cC;

    iput-object p0, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->v:LX/1cC;

    .line 1516932
    iget-object p0, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->x:LX/1cC;

    iput-object p0, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->w:LX/1cC;

    .line 1516933
    iput-object v2, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->x:LX/1cC;

    .line 1516934
    iget-object v2, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->m:Landroid/net/Uri;

    if-eqz v2, :cond_e

    .line 1516935
    iget-object p0, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->f:LX/9dC;

    sget-object v2, LX/5jI;->FILTER:LX/5jI;

    iget-object p1, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->y:LX/9cy;

    invoke-virtual {p1}, LX/9cy;->c()Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object p1

    .line 1516936
    iget-object p2, p1, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->c:LX/5jI;

    move-object p1, p2

    .line 1516937
    invoke-virtual {v2, p1}, LX/5jI;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    iget-object v2, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->y:LX/9cy;

    invoke-virtual {v2}, LX/9cy;->c()Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v2

    .line 1516938
    iget-object p1, v2, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->b:Ljava/lang/String;

    move-object v2, p1

    .line 1516939
    :goto_3
    iget-object p1, p0, LX/9dC;->c:LX/9dD;

    .line 1516940
    iget-object p2, p0, LX/9dC;->d:LX/9dD;

    iput-object p2, p0, LX/9dC;->c:LX/9dD;

    .line 1516941
    iget-object p2, p0, LX/9dC;->e:LX/9dD;

    iput-object p2, p0, LX/9dC;->d:LX/9dD;

    .line 1516942
    iput-object p1, p0, LX/9dC;->e:LX/9dD;

    .line 1516943
    const/4 p1, 0x0

    iput-boolean p1, p0, LX/9dC;->h:Z

    .line 1516944
    iget-object p1, p0, LX/9dC;->e:LX/9dD;

    invoke-virtual {p1, v2}, LX/9dD;->a(Ljava/lang/String;)V

    .line 1516945
    :goto_4
    goto/16 :goto_0

    .line 1516946
    :cond_5
    invoke-virtual {p1}, LX/5jK;->e()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1516947
    iget-object v0, p0, LX/9cv;->a:LX/9d5;

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1516948
    iget-object v1, v0, LX/9d5;->D:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    iget-object p0, v0, LX/9d5;->F:LX/0Px;

    iget-object p1, v0, LX/9d5;->D:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1516949
    iget-object p2, p1, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->b:Ljava/lang/String;

    move-object p1, p2

    .line 1516950
    invoke-static {p0, p1}, LX/8GN;->a(LX/0Px;Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    move v1, v2

    .line 1516951
    :goto_5
    if-eqz v1, :cond_10

    iget-object v1, v0, LX/9d5;->F:LX/0Px;

    iget-object p0, v0, LX/9d5;->C:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    invoke-static {v1, p0}, LX/8GN;->a(LX/0Px;Lcom/facebook/photos/creativeediting/model/SwipeableParams;)Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v1

    :goto_6
    iput-object v1, v0, LX/9d5;->E:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1516952
    iget-object v1, v0, LX/9d5;->C:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    iput-object v1, v0, LX/9d5;->D:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1516953
    iget-object v1, v0, LX/9d5;->F:LX/0Px;

    iget-object p0, v0, LX/9d5;->D:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    invoke-static {v1, p0}, LX/8GN;->b(LX/0Px;Lcom/facebook/photos/creativeediting/model/SwipeableParams;)Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v1

    iput-object v1, v0, LX/9d5;->C:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1516954
    iget-object v1, v0, LX/9d5;->A:LX/5iG;

    iput-object v1, v0, LX/9d5;->B:LX/5iG;

    .line 1516955
    iget-object v1, v0, LX/9d5;->z:LX/5iG;

    iput-object v1, v0, LX/9d5;->A:LX/5iG;

    .line 1516956
    iget-object v1, v0, LX/9d5;->z:LX/5iG;

    invoke-static {v0, v1, v3}, LX/9d5;->b$redex0(LX/9d5;LX/5iG;I)V

    .line 1516957
    const/4 v1, 0x0

    iput-object v1, v0, LX/9d5;->z:LX/5iG;

    .line 1516958
    iget-object v1, v0, LX/9d5;->s:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->e()V

    .line 1516959
    iget-object v1, v0, LX/9d5;->i:LX/9bz;

    iget-object p0, v0, LX/9d5;->r:Ljava/lang/String;

    iget-object p1, v0, LX/9d5;->D:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    invoke-interface {v1, p0, p1, v3}, LX/9bz;->a(Ljava/lang/String;Lcom/facebook/photos/creativeediting/model/SwipeableParams;I)V

    .line 1516960
    iput-boolean v2, v0, LX/9d5;->N:Z

    .line 1516961
    iput v3, v0, LX/9d5;->O:I

    .line 1516962
    iget-object v1, v0, LX/9d5;->m:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    .line 1516963
    iget-object v2, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->q:LX/5iG;

    iput-object v2, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->r:LX/5iG;

    .line 1516964
    iget-object v2, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->p:LX/5iG;

    iput-object v2, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->q:LX/5iG;

    .line 1516965
    const/4 v2, 0x0

    iput-object v2, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->p:LX/5iG;

    .line 1516966
    iget-object v2, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->u:LX/1aX;

    .line 1516967
    iget-object v3, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->t:LX/1aX;

    iput-object v3, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->u:LX/1aX;

    .line 1516968
    iget-object v3, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->s:LX/1aX;

    iput-object v3, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->t:LX/1aX;

    .line 1516969
    iput-object v2, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->s:LX/1aX;

    .line 1516970
    iget-object v2, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->x:LX/1cC;

    .line 1516971
    iget-object v3, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->w:LX/1cC;

    iput-object v3, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->x:LX/1cC;

    .line 1516972
    iget-object v3, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->v:LX/1cC;

    iput-object v3, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->w:LX/1cC;

    .line 1516973
    iput-object v2, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->v:LX/1cC;

    .line 1516974
    iget-object v2, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->m:Landroid/net/Uri;

    if-eqz v2, :cond_12

    .line 1516975
    iget-object v3, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->f:LX/9dC;

    sget-object v2, LX/5jI;->FILTER:LX/5jI;

    iget-object p0, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->y:LX/9cy;

    invoke-virtual {p0}, LX/9cy;->a()Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object p0

    .line 1516976
    iget-object p1, p0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->c:LX/5jI;

    move-object p0, p1

    .line 1516977
    invoke-virtual {v2, p0}, LX/5jI;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    iget-object v2, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->y:LX/9cy;

    invoke-virtual {v2}, LX/9cy;->a()Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v2

    .line 1516978
    iget-object p0, v2, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->b:Ljava/lang/String;

    move-object v2, p0

    .line 1516979
    :goto_7
    iget-object p0, v3, LX/9dC;->e:LX/9dD;

    .line 1516980
    iget-object p1, v3, LX/9dC;->d:LX/9dD;

    iput-object p1, v3, LX/9dC;->e:LX/9dD;

    .line 1516981
    iget-object p1, v3, LX/9dC;->c:LX/9dD;

    iput-object p1, v3, LX/9dC;->d:LX/9dD;

    .line 1516982
    iput-object p0, v3, LX/9dC;->c:LX/9dD;

    .line 1516983
    const/4 p0, 0x0

    iput-boolean p0, v3, LX/9dC;->f:Z

    .line 1516984
    iget-object p0, v3, LX/9dC;->c:LX/9dD;

    invoke-virtual {p0, v2}, LX/9dD;->a(Ljava/lang/String;)V

    .line 1516985
    :goto_8
    goto/16 :goto_0

    .line 1516986
    :cond_6
    invoke-virtual {p1}, LX/5jK;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1516987
    iget-object v0, p0, LX/9cv;->a:LX/9d5;

    iget-object v0, v0, LX/9d5;->s:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->a()V

    .line 1516988
    iget-object v0, p0, LX/9cv;->a:LX/9d5;

    invoke-virtual {v0}, LX/9d5;->e()V

    .line 1516989
    iget-object v0, p0, LX/9cv;->a:LX/9d5;

    iget-object v1, p0, LX/9cv;->a:LX/9d5;

    iget-object v1, v1, LX/9d5;->D:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1516990
    iput-object v1, v0, LX/9d5;->G:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1516991
    iget-object v0, p0, LX/9cv;->a:LX/9d5;

    iget-object v0, v0, LX/9d5;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8GH;

    .line 1516992
    iget-object v2, p0, LX/9cv;->a:LX/9d5;

    iget-object v2, v2, LX/9d5;->D:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    invoke-interface {v0, v2}, LX/8GH;->c(Lcom/facebook/photos/creativeediting/model/SwipeableParams;)V

    goto :goto_9

    .line 1516993
    :pswitch_1
    iget-object v0, p0, LX/9cv;->a:LX/9d5;

    iget-object v0, v0, LX/9d5;->K:Lcom/facebook/widget/ScrollingAwareScrollView;

    if-eqz v0, :cond_7

    .line 1516994
    iget-object v0, p0, LX/9cv;->a:LX/9d5;

    iget-object v0, v0, LX/9d5;->K:Lcom/facebook/widget/ScrollingAwareScrollView;

    const/4 v1, 0x0

    .line 1516995
    iput-boolean v1, v0, Lcom/facebook/widget/ScrollingAwareScrollView;->a:Z

    .line 1516996
    :cond_7
    iget-object v0, p0, LX/9cv;->a:LX/9d5;

    iget-object v0, v0, LX/9d5;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8GH;

    .line 1516997
    iget-object v2, p0, LX/9cv;->a:LX/9d5;

    iget-object v2, v2, LX/9d5;->G:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    invoke-interface {v0, v2}, LX/8GH;->a(Lcom/facebook/photos/creativeediting/model/SwipeableParams;)V

    goto :goto_a

    .line 1516998
    :pswitch_2
    iget-object v0, p0, LX/9cv;->a:LX/9d5;

    iget-object v0, v0, LX/9d5;->L:LX/9dI;

    .line 1516999
    invoke-virtual {v0}, LX/9dI;->f()F

    move-result v2

    .line 1517000
    iget-boolean v1, v0, LX/9dI;->t:Z

    if-eqz v1, :cond_13

    iget-object v1, v0, LX/9dI;->u:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    int-to-float v1, v1

    .line 1517001
    :goto_b
    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v1, v3

    .line 1517002
    iget-boolean v3, v0, LX/9dI;->A:Z

    if-nez v3, :cond_9

    iget-object v3, v0, LX/9dI;->q:LX/5jK;

    invoke-virtual {v3}, LX/5jK;->e()Z

    move-result v3

    if-eqz v3, :cond_8

    cmpl-float v3, v2, v1

    if-gtz v3, :cond_9

    :cond_8
    iget-object v3, v0, LX/9dI;->q:LX/5jK;

    invoke-virtual {v3}, LX/5jK;->d()Z

    move-result v3

    if-eqz v3, :cond_14

    cmpg-float v1, v2, v1

    if-gez v1, :cond_14

    :cond_9
    const/4 v1, 0x1

    :goto_c
    move v0, v1

    .line 1517003
    if-eqz v0, :cond_0

    .line 1517004
    iget-object v1, p0, LX/9cv;->a:LX/9d5;

    invoke-virtual {p1}, LX/5jK;->d()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, LX/9cv;->a:LX/9d5;

    iget-object v0, v0, LX/9d5;->E:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1517005
    :goto_d
    iput-object v0, v1, LX/9d5;->G:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1517006
    iget-object v0, p0, LX/9cv;->a:LX/9d5;

    iget-object v0, v0, LX/9d5;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8GH;

    .line 1517007
    iget-object v2, p0, LX/9cv;->a:LX/9d5;

    iget-object v2, v2, LX/9d5;->G:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    invoke-interface {v0, v2}, LX/8GH;->b(Lcom/facebook/photos/creativeediting/model/SwipeableParams;)V

    goto :goto_e

    .line 1517008
    :cond_a
    iget-object v0, p0, LX/9cv;->a:LX/9d5;

    iget-object v0, v0, LX/9d5;->C:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    goto :goto_d

    .line 1517009
    :cond_b
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 1517010
    :cond_c
    iget-object v1, v0, LX/9d5;->D:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    goto/16 :goto_2

    .line 1517011
    :cond_d
    sget-object v2, LX/5iL;->PassThrough:LX/5iL;

    invoke-virtual {v2}, LX/5iL;->name()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    .line 1517012
    :cond_e
    iget-object v2, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->u:LX/1aX;

    iget-object p0, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->y:LX/9cy;

    invoke-virtual {p0}, LX/9cy;->c()Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object p0

    iget-object p1, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->r:LX/5iG;

    invoke-static {v1, v2, p0, p1}, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->a$redex0(Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;LX/1aX;Lcom/facebook/photos/creativeediting/model/SwipeableParams;LX/5iG;)LX/5iG;

    move-result-object v2

    iput-object v2, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->r:LX/5iG;

    .line 1517013
    invoke-static {v1}, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->g(Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;)V

    goto/16 :goto_4

    :cond_f
    move v1, v3

    .line 1517014
    goto/16 :goto_5

    .line 1517015
    :cond_10
    iget-object v1, v0, LX/9d5;->D:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    goto/16 :goto_6

    .line 1517016
    :cond_11
    sget-object v2, LX/5iL;->PassThrough:LX/5iL;

    invoke-virtual {v2}, LX/5iL;->name()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_7

    .line 1517017
    :cond_12
    iget-object v2, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->s:LX/1aX;

    iget-object v3, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->y:LX/9cy;

    invoke-virtual {v3}, LX/9cy;->a()Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v3

    iget-object p0, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->p:LX/5iG;

    invoke-static {v1, v2, v3, p0}, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->a$redex0(Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;LX/1aX;Lcom/facebook/photos/creativeediting/model/SwipeableParams;LX/5iG;)LX/5iG;

    move-result-object v2

    iput-object v2, v1, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->p:LX/5iG;

    .line 1517018
    invoke-static {v1}, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->g(Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;)V

    goto/16 :goto_8

    .line 1517019
    :cond_13
    iget-object v1, v0, LX/9dI;->u:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    int-to-float v1, v1

    goto/16 :goto_b

    .line 1517020
    :cond_14
    const/4 v1, 0x0

    goto/16 :goto_c

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1517021
    iget-object v0, p0, LX/9cv;->a:LX/9d5;

    invoke-static {v0}, LX/9d5;->t(LX/9d5;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1517022
    iget-object v0, p0, LX/9cv;->a:LX/9d5;

    iget-object v0, v0, LX/9d5;->L:LX/9dI;

    invoke-virtual {v0}, LX/9dI;->g()V

    .line 1517023
    :cond_0
    return-void
.end method
