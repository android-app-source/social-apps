.class public LX/95N;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/95M;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1436780
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1436781
    return-void
.end method

.method public static a(LX/0QB;)LX/95N;
    .locals 1

    .prologue
    .line 1436782
    new-instance v0, LX/95N;

    invoke-direct {v0}, LX/95N;-><init>()V

    .line 1436783
    move-object v0, v0

    .line 1436784
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1436785
    check-cast p1, LX/95M;

    .line 1436786
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 1436787
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "enabled"

    iget-boolean v2, p1, LX/95M;->a:Z

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1436788
    new-instance v0, LX/14N;

    const-string v1, "setGlobalKillSwitchForContactsUpload"

    const-string v2, "POST"

    const-string v3, "me/contactsmessengersync"

    sget-object v5, LX/14S;->STRING:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1436789
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1436790
    const/4 v0, 0x0

    return-object v0
.end method
