.class public final LX/AFZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/audience/direct/protocol/DirectInboxBadgeQueryModels$DirectInboxBadgeQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0gL;


# direct methods
.method public constructor <init>(LX/0gL;)V
    .locals 0

    .prologue
    .line 1647871
    iput-object p1, p0, LX/AFZ;->a:LX/0gL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1647872
    sget-object v0, LX/0gL;->a:Ljava/lang/String;

    const-string v1, "Failed to query inbox badge count"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1647873
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1647874
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1647875
    if-nez p1, :cond_1

    .line 1647876
    :cond_0
    :goto_0
    return-void

    .line 1647877
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1647878
    check-cast v0, Lcom/facebook/audience/direct/protocol/DirectInboxBadgeQueryModels$DirectInboxBadgeQueryModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/DirectInboxBadgeQueryModels$DirectInboxBadgeQueryModel;->a()I

    move-result v0

    .line 1647879
    iget-object v1, p0, LX/AFZ;->a:LX/0gL;

    iget-object v1, v1, LX/0gL;->f:LX/AEw;

    if-eqz v1, :cond_0

    .line 1647880
    iget-object v1, p0, LX/AFZ;->a:LX/0gL;

    iget-object v1, v1, LX/0gL;->f:LX/AEw;

    invoke-virtual {v1, v0}, LX/AEw;->a(I)V

    goto :goto_0
.end method
