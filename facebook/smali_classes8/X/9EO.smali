.class public final LX/9EO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Ve;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6Ve",
        "<",
        "LX/8q0;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/9Ea;


# direct methods
.method public constructor <init>(LX/9Ea;)V
    .locals 0

    .prologue
    .line 1457001
    iput-object p1, p0, LX/9EO;->a:LX/9Ea;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1457002
    check-cast p1, LX/8q0;

    .line 1457003
    iget-object v0, p1, LX/8pw;->a:Lcom/facebook/graphql/model/GraphQLComment;

    move-object v0, v0

    .line 1457004
    iget-object v1, p0, LX/9EO;->a:LX/9Ea;

    iget-object v1, v1, LX/9Ea;->u:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v1, v0}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result v1

    .line 1457005
    iget-object v2, p0, LX/9EO;->a:LX/9Ea;

    iget-object v2, v2, LX/9Ea;->m:LX/9EZ;

    .line 1457006
    iget-object v3, v2, LX/9EZ;->a:LX/9Ea;

    iget-object v3, v3, LX/9Ea;->i:LX/20j;

    iget-object v4, v2, LX/9EZ;->a:LX/9Ea;

    iget-object v4, v4, LX/9Ea;->u:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1457007
    sget-object v5, LX/6PR;->ADD_OR_EDIT:LX/6PR;

    const/4 p1, 0x1

    invoke-static {v3, v4, v0, v5, p1}, LX/20j;->a(LX/20j;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;LX/6PR;Z)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v5

    move-object v3, v5

    .line 1457008
    iget-object v4, v2, LX/9EZ;->a:LX/9Ea;

    iget-object v4, v4, LX/9Ea;->b:LX/0QK;

    invoke-interface {v4, v3}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1457009
    iget-object v2, p0, LX/9EO;->a:LX/9Ea;

    iget-object v2, v2, LX/9Ea;->f:LX/9Do;

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/9EO;->a:LX/9Ea;

    iget-object v2, v2, LX/9Ea;->c:LX/9CC;

    if-eqz v2, :cond_0

    if-nez v1, :cond_0

    .line 1457010
    iget-object v1, p0, LX/9EO;->a:LX/9Ea;

    iget-object v1, v1, LX/9Ea;->f:LX/9Do;

    invoke-virtual {v1, v0}, LX/9Do;->a(Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 1457011
    iget-object v1, p0, LX/9EO;->a:LX/9Ea;

    iget-object v1, v1, LX/9Ea;->c:LX/9CC;

    invoke-virtual {v1, v0}, LX/9CC;->a(Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 1457012
    iget-object v0, p0, LX/9EO;->a:LX/9Ea;

    iget-object v0, v0, LX/9Ea;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3RX;

    const-string v1, "live_comment"

    invoke-virtual {v0, v1}, LX/3RX;->a(Ljava/lang/String;)V

    .line 1457013
    :cond_0
    return-void
.end method
