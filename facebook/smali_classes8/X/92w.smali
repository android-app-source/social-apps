.class public LX/92w;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/92w;


# instance fields
.field public final a:LX/92v;

.field private final b:LX/0Zb;

.field private final c:Ljava/util/concurrent/ExecutorService;

.field private final d:LX/0TD;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/03V;

.field public final g:LX/0SG;


# direct methods
.method public constructor <init>(LX/92v;LX/0Zb;Ljava/util/concurrent/ExecutorService;LX/0TD;LX/0Or;LX/03V;LX/0SG;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .param p4    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/92v;",
            "LX/0Zb;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0TD;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1433113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1433114
    iput-object p1, p0, LX/92w;->a:LX/92v;

    .line 1433115
    iput-object p2, p0, LX/92w;->b:LX/0Zb;

    .line 1433116
    iput-object p3, p0, LX/92w;->c:Ljava/util/concurrent/ExecutorService;

    .line 1433117
    iput-object p4, p0, LX/92w;->d:LX/0TD;

    .line 1433118
    iput-object p5, p0, LX/92w;->e:LX/0Or;

    .line 1433119
    iput-object p6, p0, LX/92w;->f:LX/03V;

    .line 1433120
    iput-object p7, p0, LX/92w;->g:LX/0SG;

    .line 1433121
    return-void
.end method

.method public static a(LX/0QB;)LX/92w;
    .locals 11

    .prologue
    .line 1433100
    sget-object v0, LX/92w;->h:LX/92w;

    if-nez v0, :cond_1

    .line 1433101
    const-class v1, LX/92w;

    monitor-enter v1

    .line 1433102
    :try_start_0
    sget-object v0, LX/92w;->h:LX/92w;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1433103
    if-eqz v2, :cond_0

    .line 1433104
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1433105
    new-instance v3, LX/92w;

    invoke-static {v0}, LX/92v;->a(LX/0QB;)LX/92v;

    move-result-object v4

    check-cast v4, LX/92v;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    invoke-static {v0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0tb;->a(LX/0QB;)LX/0TD;

    move-result-object v7

    check-cast v7, LX/0TD;

    const/16 v8, 0x12cb

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v10

    check-cast v10, LX/0SG;

    invoke-direct/range {v3 .. v10}, LX/92w;-><init>(LX/92v;LX/0Zb;Ljava/util/concurrent/ExecutorService;LX/0TD;LX/0Or;LX/03V;LX/0SG;)V

    .line 1433106
    move-object v0, v3

    .line 1433107
    sput-object v0, LX/92w;->h:LX/92w;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1433108
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1433109
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1433110
    :cond_1
    sget-object v0, LX/92w;->h:LX/92w;

    return-object v0

    .line 1433111
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1433112
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Landroid/database/Cursor;LX/0U1;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1433098
    iget-object v0, p1, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1433099
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/92w;)V
    .locals 7

    .prologue
    .line 1433086
    iget-object v0, p0, LX/92w;->a:LX/92v;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const v1, 0x686985ac

    invoke-static {v0, v1}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 1433087
    :try_start_0
    iget-object v0, p0, LX/92w;->a:LX/92v;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "minutiae_verb_table"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LX/92x;->n:LX/0U1;

    .line 1433088
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1433089
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " = ?"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v0, p0, LX/92w;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 1433090
    iget-object v6, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v6

    .line 1433091
    aput-object v0, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1433092
    iget-object v0, p0, LX/92w;->a:LX/92v;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1433093
    iget-object v0, p0, LX/92w;->a:LX/92v;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const v1, 0x1c6e0587

    invoke-static {v0, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 1433094
    :goto_0
    return-void

    .line 1433095
    :catch_0
    move-exception v0

    .line 1433096
    :try_start_1
    iget-object v1, p0, LX/92w;->f:LX/03V;

    const-string v2, "MinutiaeDiskStorage.deleteVerbsForUser"

    const-string v3, "Exception thrown writing minutiae data to storage"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1433097
    iget-object v0, p0, LX/92w;->a:LX/92v;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const v1, 0x99be0c5

    invoke-static {v0, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/92w;->a:LX/92v;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const v2, 0x2d538406

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public static a(LX/92w;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1433080
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1433081
    iget-object v0, p0, LX/92w;->b:LX/0Zb;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 1433082
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1433083
    const-string v1, "CACHE_KEY"

    invoke-virtual {v0, v1, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1433084
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 1433085
    :cond_0
    return-void
.end method

.method public static b(Landroid/database/Cursor;LX/0U1;)I
    .locals 1

    .prologue
    .line 1433078
    iget-object v0, p1, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1433079
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public static d(Landroid/database/Cursor;LX/0U1;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1432954
    invoke-static {p0, p1}, LX/92w;->b(Landroid/database/Cursor;LX/0U1;)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0Px;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/92e;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1433076
    iget-object v0, p0, LX/92w;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/composer/minutiae/protocol/db/MinutiaeDiskStorage$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/composer/minutiae/protocol/db/MinutiaeDiskStorage$2;-><init>(LX/92w;LX/0Px;Ljava/lang/String;)V

    const v2, 0x39b313cc

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1433077
    return-void
.end method

.method public final b(LX/0Px;Ljava/lang/String;)Z
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/92e;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1432955
    iget-object v0, p0, LX/92w;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1432956
    :cond_0
    :goto_0
    return v1

    .line 1432957
    :cond_1
    invoke-static {p0}, LX/92w;->a(LX/92w;)V

    .line 1432958
    iget-object v0, p0, LX/92w;->a:LX/92v;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const v2, 0x72f6d254

    invoke-static {v0, v2}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 1432959
    iget-object v0, p0, LX/92w;->a:LX/92v;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v2, "SELECT COUNT(*) FROM minutiae_verb_table"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1432960
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1432961
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 1432962
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1432963
    iget-object v0, p0, LX/92w;->a:LX/92v;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 1432964
    iget-object v0, p0, LX/92w;->a:LX/92v;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const v3, 0x6a03cd68

    invoke-static {v0, v3}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 1432965
    move v0, v2

    .line 1432966
    const/16 v2, 0x3c

    if-gt v0, v2, :cond_0

    .line 1432967
    iget-object v0, p0, LX/92w;->a:LX/92v;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const v2, -0x6e4f032a

    invoke-static {v0, v2}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    move v2, v1

    .line 1432968
    :goto_1
    :try_start_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 1432969
    new-instance v3, Landroid/content/ContentValues;

    sget v0, LX/92y;->a:I

    invoke-direct {v3, v0}, Landroid/content/ContentValues;-><init>(I)V

    .line 1432970
    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/92e;

    iget-object v4, p0, LX/92w;->f:LX/03V;

    .line 1432971
    iget-object v5, v0, LX/92e;->a:LX/5LG;

    move-object v5, v5

    .line 1432972
    new-instance v6, LX/930;

    invoke-direct {v6}, LX/930;-><init>()V

    invoke-interface {v5}, LX/5LG;->j()Ljava/lang/String;

    move-result-object v7

    .line 1432973
    iput-object v7, v6, LX/930;->c:Ljava/lang/String;

    .line 1432974
    move-object v6, v6

    .line 1432975
    invoke-interface {v5}, LX/5LG;->l()Ljava/lang/String;

    move-result-object v7

    .line 1432976
    iput-object v7, v6, LX/930;->d:Ljava/lang/String;

    .line 1432977
    move-object v6, v6

    .line 1432978
    iput v2, v6, LX/930;->a:I

    .line 1432979
    move-object v6, v6

    .line 1432980
    invoke-interface {v5}, LX/5LG;->n()Ljava/lang/String;

    move-result-object v7

    .line 1432981
    iput-object v7, v6, LX/930;->e:Ljava/lang/String;

    .line 1432982
    move-object v6, v6

    .line 1432983
    invoke-interface {v5}, LX/5LG;->o()Ljava/lang/String;

    move-result-object v7

    .line 1432984
    iput-object v7, v6, LX/930;->f:Ljava/lang/String;

    .line 1432985
    move-object v6, v6

    .line 1432986
    invoke-interface {v5}, LX/5LG;->k()Z

    move-result v7

    .line 1432987
    iput-boolean v7, v6, LX/930;->g:Z

    .line 1432988
    move-object v6, v6

    .line 1432989
    invoke-interface {v5}, LX/5LG;->p()Z

    move-result v7

    .line 1432990
    iput-boolean v7, v6, LX/930;->h:Z

    .line 1432991
    move-object v6, v6

    .line 1432992
    invoke-interface {v5}, LX/5LG;->r()Z

    move-result v7

    .line 1432993
    iput-boolean v7, v6, LX/930;->i:Z

    .line 1432994
    move-object v6, v6

    .line 1432995
    invoke-interface {v5}, LX/5LG;->q()Z

    move-result v7

    .line 1432996
    iput-boolean v7, v6, LX/930;->j:Z

    .line 1432997
    move-object v6, v6

    .line 1432998
    invoke-interface {v5}, LX/5LG;->A()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;->a()Ljava/lang/String;

    move-result-object v7

    .line 1432999
    iput-object v7, v6, LX/930;->k:Ljava/lang/String;

    .line 1433000
    move-object v6, v6

    .line 1433001
    invoke-interface {v5}, LX/5LG;->B()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel$AllIconsModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel$AllIconsModel;->a()I

    move-result v7

    .line 1433002
    iput v7, v6, LX/930;->b:I

    .line 1433003
    move-object v6, v6

    .line 1433004
    invoke-interface {v5}, LX/5LG;->z()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;

    move-result-object v7

    if-eqz v7, :cond_4

    .line 1433005
    invoke-interface {v5}, LX/5LG;->z()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;->a()Ljava/lang/String;

    move-result-object v7

    .line 1433006
    iput-object v7, v6, LX/930;->l:Ljava/lang/String;

    .line 1433007
    :goto_2
    move-object v6, v6

    .line 1433008
    invoke-virtual {v6}, LX/930;->a()LX/931;

    move-result-object v6

    move-object v5, v6

    .line 1433009
    move-object v0, v5

    .line 1433010
    sget-object v4, LX/92x;->a:LX/0U1;

    .line 1433011
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 1433012
    iget-object v5, v0, LX/931;->a:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1433013
    sget-object v4, LX/92x;->b:LX/0U1;

    .line 1433014
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 1433015
    iget-object v5, v0, LX/931;->b:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1433016
    sget-object v4, LX/92x;->c:LX/0U1;

    .line 1433017
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 1433018
    iget-object v5, v0, LX/931;->c:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1433019
    sget-object v4, LX/92x;->d:LX/0U1;

    .line 1433020
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 1433021
    iget-object v5, v0, LX/931;->d:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1433022
    sget-object v4, LX/92x;->e:LX/0U1;

    .line 1433023
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 1433024
    iget-boolean v5, v0, LX/931;->e:Z

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1433025
    sget-object v4, LX/92x;->f:LX/0U1;

    .line 1433026
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 1433027
    iget-boolean v5, v0, LX/931;->f:Z

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1433028
    sget-object v4, LX/92x;->g:LX/0U1;

    .line 1433029
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 1433030
    iget-boolean v5, v0, LX/931;->h:Z

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1433031
    sget-object v4, LX/92x;->h:LX/0U1;

    .line 1433032
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 1433033
    iget-boolean v5, v0, LX/931;->g:Z

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1433034
    sget-object v4, LX/92x;->i:LX/0U1;

    .line 1433035
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 1433036
    iget-object v5, v0, LX/931;->i:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1433037
    sget-object v4, LX/92x;->j:LX/0U1;

    .line 1433038
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 1433039
    iget-object v5, v0, LX/931;->j:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1433040
    sget-object v4, LX/92x;->k:LX/0U1;

    .line 1433041
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 1433042
    iget v5, v0, LX/931;->l:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1433043
    sget-object v4, LX/92x;->l:LX/0U1;

    .line 1433044
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 1433045
    iget v0, v0, LX/931;->k:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1433046
    sget-object v0, LX/92x;->m:LX/0U1;

    .line 1433047
    iget-object v4, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v4

    .line 1433048
    invoke-virtual {v3, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1433049
    sget-object v0, LX/92x;->n:LX/0U1;

    .line 1433050
    iget-object v4, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v4

    .line 1433051
    iget-object v0, p0, LX/92w;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 1433052
    iget-object v5, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v5

    .line 1433053
    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1433054
    sget-object v0, LX/92x;->o:LX/0U1;

    .line 1433055
    iget-object v4, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v4

    .line 1433056
    iget-object v4, p0, LX/92w;->g:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1433057
    iget-object v0, p0, LX/92w;->a:LX/92v;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v4, "minutiae_verb_table"

    const/4 v5, 0x0

    const v6, 0x27d04ae6

    invoke-static {v6}, LX/03h;->a(I)V

    invoke-virtual {v0, v4, v5, v3}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    const v0, -0x163ac6f3

    invoke-static {v0}, LX/03h;->a(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1433058
    const-wide/16 v6, -0x1

    cmp-long v0, v4, v6

    if-nez v0, :cond_2

    .line 1433059
    const-string v0, "minutiae_disk_storage_write_activities_failed"

    invoke-static {p0, v0, p2}, LX/92w;->a(LX/92w;Ljava/lang/String;Ljava/lang/String;)V

    .line 1433060
    iget-object v0, p0, LX/92w;->a:LX/92v;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const v2, -0x1ffcd686

    invoke-static {v0, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    goto/16 :goto_0

    .line 1433061
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_1

    .line 1433062
    :cond_3
    :try_start_1
    iget-object v0, p0, LX/92w;->a:LX/92v;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1433063
    const/4 v0, 0x1

    .line 1433064
    const-string v1, "minutiae_disk_storage_write_activities_succeeded"

    invoke-static {p0, v1, p2}, LX/92w;->a(LX/92w;Ljava/lang/String;Ljava/lang/String;)V

    .line 1433065
    iget-object v1, p0, LX/92w;->a:LX/92v;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const v2, 0x179f92a6

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    :goto_3
    move v1, v0

    .line 1433066
    goto/16 :goto_0

    .line 1433067
    :catch_0
    move-exception v0

    .line 1433068
    :try_start_2
    iget-object v2, p0, LX/92w;->f:LX/03V;

    const-string v3, "MinutiaeDiskStorage.writeVerbs"

    const-string v4, "Exception thrown writing minutiae data to storage"

    invoke-virtual {v2, v3, v4, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1433069
    const-string v0, "minutiae_disk_storage_write_activities_failed"

    invoke-static {p0, v0, p2}, LX/92w;->a(LX/92w;Ljava/lang/String;Ljava/lang/String;)V

    .line 1433070
    iget-object v0, p0, LX/92w;->a:LX/92v;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const v2, -0x74a7051    # -2.946E34f

    invoke-static {v0, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    move v0, v1

    .line 1433071
    goto :goto_3

    .line 1433072
    :catchall_0
    move-exception v0

    .line 1433073
    const-string v1, "minutiae_disk_storage_write_activities_failed"

    invoke-static {p0, v1, p2}, LX/92w;->a(LX/92w;Ljava/lang/String;Ljava/lang/String;)V

    .line 1433074
    iget-object v1, p0, LX/92w;->a:LX/92v;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const v2, 0x2a0a00b8

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0

    .line 1433075
    :cond_4
    const-string v7, "minutiae_without_large_image"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v0, "The minutiae with id: "

    invoke-direct {v8, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v5}, LX/5LG;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v0, " and legacy id: "

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v5}, LX/5LG;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v0, " does not have a large image"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2
.end method
