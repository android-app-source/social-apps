.class public LX/A6V;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/transliteration/TransliterationFragment;

.field public b:Lcom/facebook/resources/ui/FbTextView;

.field public c:Lcom/facebook/resources/ui/FbTextView;

.field public d:Lcom/facebook/resources/ui/FbTextView;

.field public e:Lcom/facebook/resources/ui/FbTextView;

.field public f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/transliteration/TransliterationFragment;)V
    .locals 0

    .prologue
    .line 1624340
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1624341
    iput-object p1, p0, LX/A6V;->a:Lcom/facebook/transliteration/TransliterationFragment;

    .line 1624342
    return-void
.end method

.method public static a$redex0(LX/A6V;IZ)V
    .locals 3

    .prologue
    .line 1624343
    iget-object v0, p0, LX/A6V;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    .line 1624344
    iget-object v0, p0, LX/A6V;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1624345
    iget-object v1, p0, LX/A6V;->a:Lcom/facebook/transliteration/TransliterationFragment;

    add-int/lit8 v2, p1, -0x1

    invoke-virtual {v1, v0, v2, p2}, Lcom/facebook/transliteration/TransliterationFragment;->a(Ljava/lang/String;IZ)V

    .line 1624346
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1624334
    iget-object v0, p0, LX/A6V;->b:Lcom/facebook/resources/ui/FbTextView;

    const-string v1, " "

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1624335
    iget-object v0, p0, LX/A6V;->c:Lcom/facebook/resources/ui/FbTextView;

    const-string v1, " "

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1624336
    iget-object v0, p0, LX/A6V;->d:Lcom/facebook/resources/ui/FbTextView;

    const-string v1, " "

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1624337
    iget-object v0, p0, LX/A6V;->e:Lcom/facebook/resources/ui/FbTextView;

    const-string v1, " "

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1624338
    iget-object v0, p0, LX/A6V;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1624339
    return-void
.end method
