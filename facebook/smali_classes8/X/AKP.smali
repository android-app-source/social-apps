.class public LX/AKP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/audience/model/AudienceControlData;

.field public final b:Lcom/facebook/audience/model/AudienceControlData;

.field public c:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;

.field public d:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;Lcom/facebook/audience/model/AudienceControlData;Lcom/facebook/audience/model/AudienceControlData;)V
    .locals 2

    .prologue
    .line 1663220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1663221
    iput-object p1, p0, LX/AKP;->c:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;

    .line 1663222
    iput-object p2, p0, LX/AKP;->a:Lcom/facebook/audience/model/AudienceControlData;

    .line 1663223
    iput-object p3, p0, LX/AKP;->b:Lcom/facebook/audience/model/AudienceControlData;

    .line 1663224
    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    .line 1663225
    iget-object v0, p0, LX/AKP;->c:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;->n()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$SeenDirectUsersModel;

    move-result-object v0

    .line 1663226
    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$SeenDirectUsersModel;->a()LX/0Px;

    move-result-object p2

    invoke-virtual {p2}, LX/0Px;->size()I

    move-result p3

    .line 1663227
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, p3, :cond_0

    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$SeenDirectUsersModel$EdgesModel;

    .line 1663228
    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$SeenDirectUsersModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1663229
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1663230
    :cond_0
    move-object v0, p1

    .line 1663231
    iput-object v0, p0, LX/AKP;->d:Ljava/util/HashSet;

    .line 1663232
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/audience/snacks/model/SnackThreadMetadata;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1663233
    iget-object v0, p0, LX/AKP;->c:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;->m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;->l()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel$MessageMediaModel;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1663234
    if-nez v0, :cond_0

    .line 1663235
    const/4 v0, 0x0

    .line 1663236
    :goto_1
    return-object v0

    :cond_0
    invoke-static {}, Lcom/facebook/audience/snacks/model/SnackThreadMetadata;->newBuilder()LX/AK6;

    move-result-object v0

    .line 1663237
    iget-object v1, p0, LX/AKP;->c:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;

    invoke-virtual {v1}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;->j()Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 1663238
    iput-object v1, v0, LX/AK6;->a:Ljava/lang/String;

    .line 1663239
    move-object v0, v0

    .line 1663240
    iget-object v1, p0, LX/AKP;->d:Ljava/util/HashSet;

    iget-object v2, p0, LX/AKP;->a:Lcom/facebook/audience/model/AudienceControlData;

    invoke-virtual {v2}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, LX/AKP;->c:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;

    invoke-virtual {v1}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;->k()Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_1
    const/4 v1, 0x1

    :goto_2
    move v1, v1

    .line 1663241
    iput-boolean v1, v0, LX/AK6;->b:Z

    .line 1663242
    move-object v0, v0

    .line 1663243
    iget-object v1, p0, LX/AKP;->b:Lcom/facebook/audience/model/AudienceControlData;

    move-object v1, v1

    .line 1663244
    iput-object v1, v0, LX/AK6;->c:Lcom/facebook/audience/model/AudienceControlData;

    .line 1663245
    move-object v0, v0

    .line 1663246
    iget-object v1, p0, LX/AKP;->c:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;

    invoke-virtual {v1}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;->m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;->l()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel$MessageMediaModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel$MessageMediaModel;->k()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel$MessageMediaModel$ImageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel$MessageMediaModel$ImageModel;->j()Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 1663247
    iput-object v1, v0, LX/AK6;->d:Ljava/lang/String;

    .line 1663248
    move-object v0, v0

    .line 1663249
    iget-object v1, p0, LX/AKP;->c:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;

    invoke-virtual {v1}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;->m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;->l()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel$MessageMediaModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel$MessageMediaModel;->l()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel$MessageMediaModel$LowresModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel$MessageMediaModel$LowresModel;->a()Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 1663250
    iput-object v1, v0, LX/AK6;->e:Ljava/lang/String;

    .line 1663251
    move-object v0, v0

    .line 1663252
    iget-object v1, p0, LX/AKP;->c:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;

    invoke-virtual {v1}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;->n()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$SeenDirectUsersModel;

    move-result-object v1

    .line 1663253
    iget-object v2, p0, LX/AKP;->b:Lcom/facebook/audience/model/AudienceControlData;

    invoke-virtual {v2}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/AKP;->a:Lcom/facebook/audience/model/AudienceControlData;

    invoke-virtual {v3}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    move v2, v2

    .line 1663254
    if-eqz v2, :cond_4

    .line 1663255
    const/4 v2, 0x0

    invoke-virtual {v1}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$SeenDirectUsersModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1663256
    :goto_3
    move v1, v1

    .line 1663257
    iput v1, v0, LX/AK6;->g:I

    .line 1663258
    move-object v0, v0

    .line 1663259
    iget-object v4, p0, LX/AKP;->c:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;

    invoke-virtual {v4}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;->m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;->o()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    move-wide v2, v4

    .line 1663260
    iput-wide v2, v0, LX/AK6;->h:J

    .line 1663261
    move-object v0, v0

    .line 1663262
    iget-object v1, p0, LX/AKP;->c:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;

    invoke-virtual {v1}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;->m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;->l()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel$MessageMediaModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel$MessageMediaModel;->m()Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 1663263
    iput-object v1, v0, LX/AK6;->i:Ljava/lang/String;

    .line 1663264
    move-object v0, v0

    .line 1663265
    iget-object v1, p0, LX/AKP;->c:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;

    invoke-virtual {v1}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel;->m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;->k()Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 1663266
    iput-object v1, v0, LX/AK6;->f:Ljava/lang/String;

    .line 1663267
    move-object v0, v0

    .line 1663268
    invoke-virtual {v0}, LX/AK6;->a()Lcom/facebook/audience/snacks/model/SnackThreadMetadata;

    move-result-object v0

    goto/16 :goto_1

    :cond_2
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_3
    const/4 v1, 0x0

    goto/16 :goto_2

    :cond_4
    invoke-virtual {v1}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$SeenDirectUsersModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    goto :goto_3
.end method
