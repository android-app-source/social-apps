.class public LX/9EK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field public final a:Lcom/facebook/content/SecureContextHelper;

.field public final b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field public final c:LX/1Uf;

.field public final d:LX/14w;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0ad;

.field public final g:LX/1zf;

.field public final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/1Uf;LX/0Ot;LX/14w;LX/0ad;LX/1zf;LX/0Or;)V
    .locals 0
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/content/SecureContextHelper;",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            "LX/1Uf;",
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;",
            "LX/14w;",
            "LX/0ad;",
            "LX/1zf;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1456867
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1456868
    iput-object p1, p0, LX/9EK;->a:Lcom/facebook/content/SecureContextHelper;

    .line 1456869
    iput-object p2, p0, LX/9EK;->b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 1456870
    iput-object p3, p0, LX/9EK;->c:LX/1Uf;

    .line 1456871
    iput-object p5, p0, LX/9EK;->d:LX/14w;

    .line 1456872
    iput-object p4, p0, LX/9EK;->e:LX/0Ot;

    .line 1456873
    iput-object p6, p0, LX/9EK;->f:LX/0ad;

    .line 1456874
    iput-object p7, p0, LX/9EK;->g:LX/1zf;

    .line 1456875
    iput-object p8, p0, LX/9EK;->h:LX/0Or;

    .line 1456876
    return-void
.end method

.method public static a(LX/0QB;)LX/9EK;
    .locals 12

    .prologue
    .line 1456901
    const-class v1, LX/9EK;

    monitor-enter v1

    .line 1456902
    :try_start_0
    sget-object v0, LX/9EK;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1456903
    sput-object v2, LX/9EK;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1456904
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1456905
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1456906
    new-instance v3, LX/9EK;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v5

    check-cast v5, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {v0}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v6

    check-cast v6, LX/1Uf;

    const/16 v7, 0x97

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v8

    check-cast v8, LX/14w;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-static {v0}, LX/1zf;->a(LX/0QB;)LX/1zf;

    move-result-object v10

    check-cast v10, LX/1zf;

    const/16 v11, 0x15e8

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-direct/range {v3 .. v11}, LX/9EK;-><init>(Lcom/facebook/content/SecureContextHelper;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/1Uf;LX/0Ot;LX/14w;LX/0ad;LX/1zf;LX/0Or;)V

    .line 1456907
    move-object v0, v3

    .line 1456908
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1456909
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/9EK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1456910
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1456911
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1456897
    invoke-static {p0}, LX/182;->o(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1456898
    :goto_0
    return-object p0

    .line 1456899
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1456900
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1456877
    invoke-static {p1}, LX/9EK;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, p0, LX/9EK;->d:LX/14w;

    .line 1456878
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1456879
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->e()LX/0Px;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, LX/14w;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1456880
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1456881
    if-eqz v0, :cond_0

    .line 1456882
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1456883
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->G()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1456884
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1456885
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->G()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->j()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1456886
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1456887
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->G()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/9EK;->f:LX/0ad;

    sget-short v2, LX/227;->d:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1456888
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1456889
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v3, 0x1

    const/4 p1, 0x0

    .line 1456890
    iget-object v2, p0, LX/9EK;->f:LX/0ad;

    sget-short p2, LX/227;->c:S

    invoke-interface {v2, p2, p1}, LX/0ad;->a(SZ)Z

    move-result v2

    .line 1456891
    if-eqz v2, :cond_1

    move v2, v3

    .line 1456892
    :goto_0
    move v0, v2

    .line 1456893
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    move v0, v1

    goto :goto_1

    .line 1456894
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-lez v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v2

    iget-object p2, p0, LX/9EK;->h:LX/0Or;

    invoke-interface {p2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p2

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v2, v3

    .line 1456895
    goto :goto_0

    :cond_2
    move v2, p1

    .line 1456896
    goto :goto_0
.end method
