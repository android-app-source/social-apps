.class public LX/8xF;
.super LX/8wv;
.source ""

# interfaces
.implements LX/0hY;
.implements LX/1a7;


# instance fields
.field public A:Z

.field public B:Z

.field public C:Z

.field public D:LX/8xD;

.field public E:LX/3IC;

.field public F:Landroid/view/GestureDetector;

.field public y:Lcom/facebook/common/callercontext/CallerContext;

.field private z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1423732
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/8xF;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1423733
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1423730
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/8xF;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1423731
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1423721
    invoke-direct {p0, p1, p2, p3}, LX/8wv;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1423722
    iput-boolean v1, p0, LX/8xF;->z:Z

    .line 1423723
    iput-boolean v1, p0, LX/8xF;->A:Z

    .line 1423724
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8xF;->B:Z

    .line 1423725
    iput-boolean v1, p0, LX/8xF;->C:Z

    .line 1423726
    new-instance v0, LX/8xD;

    invoke-direct {v0, p0}, LX/8xD;-><init>(LX/8xF;)V

    iput-object v0, p0, LX/8xF;->D:LX/8xD;

    .line 1423727
    new-instance v0, LX/3IC;

    invoke-virtual {p0}, LX/8xF;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object p1, p0, LX/8xF;->D:LX/8xD;

    iget-object p2, p0, LX/8xF;->D:LX/8xD;

    invoke-direct {v0, v1, p1, p2}, LX/3IC;-><init>(Landroid/content/Context;LX/3IA;LX/3IB;)V

    iput-object v0, p0, LX/8xF;->E:LX/3IC;

    .line 1423728
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, LX/8xF;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance p1, LX/8xE;

    invoke-direct {p1, p0}, LX/8xE;-><init>(LX/8xF;)V

    invoke-direct {v0, v1, p1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, LX/8xF;->F:Landroid/view/GestureDetector;

    .line 1423729
    return-void
.end method


# virtual methods
.method public final a(LX/31M;II)Z
    .locals 1

    .prologue
    .line 1423720
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Lcom/facebook/spherical/photo/model/SphericalPhotoParams;Lcom/facebook/common/callercontext/CallerContext;Ljava/lang/String;LX/7Dj;)V
    .locals 1

    .prologue
    .line 1423709
    iget-object v0, p0, LX/8wv;->o:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    invoke-virtual {v0}, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->f()V

    .line 1423710
    iget-object v0, p0, LX/8wv;->q:LX/1ca;

    if-eqz v0, :cond_0

    .line 1423711
    iget-object v0, p0, LX/8wv;->q:LX/1ca;

    invoke-interface {v0}, LX/1ca;->g()Z

    .line 1423712
    const/4 v0, 0x0

    iput-object v0, p0, LX/8xF;->q:LX/1ca;

    .line 1423713
    :cond_0
    invoke-virtual {p0, p1, p2, p3, p4}, LX/8wv;->a(Lcom/facebook/spherical/photo/model/SphericalPhotoParams;Lcom/facebook/common/callercontext/CallerContext;Ljava/lang/String;LX/7Dj;)V

    .line 1423714
    iget-boolean v0, p0, LX/8xF;->B:Z

    if-nez v0, :cond_1

    .line 1423715
    iget-object v0, p0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    const/4 p1, 0x0

    invoke-virtual {p0, v0, p1}, LX/8xF;->addView(Landroid/view/View;I)V

    .line 1423716
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8xF;->B:Z

    .line 1423717
    :cond_1
    invoke-virtual {p0}, LX/8wv;->k()V

    .line 1423718
    invoke-virtual {p0}, LX/8wv;->h()V

    .line 1423719
    return-void
.end method

.method public final cr_()Z
    .locals 1

    .prologue
    .line 1423708
    const/4 v0, 0x1

    return v0
.end method

.method public final f()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1423734
    iget-boolean v0, p0, LX/8wv;->t:Z

    if-eqz v0, :cond_0

    .line 1423735
    :goto_0
    return-void

    .line 1423736
    :cond_0
    iput-boolean v2, p0, LX/8xF;->w:Z

    .line 1423737
    iget-object v0, p0, LX/8wv;->k:Lcom/facebook/drawee/view/GenericDraweeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/GenericDraweeView;->setVisibility(I)V

    .line 1423738
    iget-object v0, p0, LX/8wv;->m:Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;

    if-eqz v0, :cond_1

    .line 1423739
    iget-object v0, p0, LX/8wv;->m:Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;

    invoke-virtual {v0}, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->b()V

    .line 1423740
    :cond_1
    iget-object v0, p0, LX/8wv;->o:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    invoke-virtual {v0}, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->g()V

    .line 1423741
    iput-boolean v2, p0, LX/8xF;->s:Z

    .line 1423742
    iget-boolean v0, p0, LX/8xF;->C:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, LX/8xF;->z:Z

    if-eqz v0, :cond_3

    .line 1423743
    :cond_2
    iget-object v0, p0, LX/8wv;->o:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    invoke-virtual {v0}, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->i()V

    .line 1423744
    :cond_3
    iget-boolean v0, p0, LX/8xF;->C:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, LX/8xF;->z:Z

    if-eqz v0, :cond_5

    :cond_4
    iget-boolean v0, p0, LX/8xF;->A:Z

    if-eqz v0, :cond_6

    .line 1423745
    :cond_5
    invoke-virtual {p0}, LX/8wv;->m()V

    .line 1423746
    :cond_6
    invoke-virtual {p0}, LX/8wv;->g()V

    .line 1423747
    invoke-virtual {p0}, LX/8wv;->p()V

    goto :goto_0
.end method

.method public getShouldShowPhoneAnimationInFullScreen()Z
    .locals 1

    .prologue
    .line 1423705
    iget-object v0, p0, LX/8wv;->o:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    .line 1423706
    iget-boolean p0, v0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->f:Z

    move v0, p0

    .line 1423707
    return v0
.end method

.method public final onMeasure(II)V
    .locals 0

    .prologue
    .line 1423672
    invoke-super {p0, p1, p1}, LX/8wv;->onMeasure(II)V

    .line 1423673
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const v0, -0x656c6d8

    invoke-static {v3, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1423696
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 1423697
    iget-object v0, p0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v0, v2}, LX/2qW;->a(Z)V

    .line 1423698
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-eq v0, v2, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v2, 0x3

    if-ne v0, v2, :cond_2

    .line 1423699
    :cond_1
    iget-object v0, p0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/2qW;->a(Z)V

    .line 1423700
    :cond_2
    iget-object v0, p0, LX/8xF;->F:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1423701
    iget-boolean v0, p0, LX/8wv;->s:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/8wv;->m:Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;

    if-eqz v0, :cond_3

    .line 1423702
    iget-object v0, p0, LX/8wv;->m:Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;

    invoke-virtual {v0}, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->d()V

    .line 1423703
    :cond_3
    iget-object v0, p0, LX/8xF;->E:LX/3IC;

    invoke-virtual {v0, p1}, LX/3IC;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, -0x37bb4a5a

    invoke-static {v3, v3, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1423704
    :goto_0
    return v0

    :cond_4
    invoke-super {p0, p1}, LX/8wv;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, 0x169bae79

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public final q()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1423678
    iput-object v2, p0, LX/8xF;->y:Lcom/facebook/common/callercontext/CallerContext;

    .line 1423679
    iget-boolean v0, p0, LX/8xF;->B:Z

    if-eqz v0, :cond_0

    .line 1423680
    invoke-virtual {p0}, LX/8wv;->n()V

    .line 1423681
    iget-object v0, p0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {p0, v0}, LX/8xF;->removeView(Landroid/view/View;)V

    .line 1423682
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/8xF;->B:Z

    .line 1423683
    :cond_0
    invoke-virtual {p0, v2}, LX/8xF;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1423684
    iput-boolean v1, p0, LX/8xF;->w:Z

    .line 1423685
    iput-boolean v1, p0, LX/8xF;->s:Z

    .line 1423686
    iget-object v0, p0, LX/8wv;->k:Lcom/facebook/drawee/view/GenericDraweeView;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/GenericDraweeView;->setVisibility(I)V

    .line 1423687
    iget-object v0, p0, LX/8wv;->o:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    .line 1423688
    iput-boolean v1, v0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->h:Z

    .line 1423689
    iget-object v0, p0, LX/8wv;->l:Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;

    invoke-virtual {v0}, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->a()V

    .line 1423690
    iget-object v0, p0, LX/8wv;->m:Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;

    if-eqz v0, :cond_1

    .line 1423691
    iget-object v0, p0, LX/8wv;->m:Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;

    invoke-virtual {v0}, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->c()V

    .line 1423692
    :cond_1
    iget-object v0, p0, LX/8wv;->q:LX/1ca;

    if-eqz v0, :cond_2

    .line 1423693
    iget-object v0, p0, LX/8wv;->q:LX/1ca;

    invoke-interface {v0}, LX/1ca;->g()Z

    .line 1423694
    iput-object v2, p0, LX/8xF;->q:LX/1ca;

    .line 1423695
    :cond_2
    return-void
.end method

.method public setIsViewInFullScreenMode(Z)V
    .locals 0

    .prologue
    .line 1423676
    iput-boolean p1, p0, LX/8xF;->z:Z

    .line 1423677
    return-void
.end method

.method public setShouldShowPhoneAnimationInFullScreen(Z)V
    .locals 1

    .prologue
    .line 1423674
    iget-object v0, p0, LX/8wv;->o:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    invoke-virtual {v0, p1}, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->setShouldShowPhoneAnimationInFullScreen(Z)V

    .line 1423675
    return-void
.end method
