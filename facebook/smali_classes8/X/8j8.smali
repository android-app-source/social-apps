.class public final enum LX/8j8;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8j8;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8j8;

.field public static final enum CANCELLED:LX/8j8;

.field public static final enum COMPLETED:LX/8j8;

.field public static final enum FAILED:LX/8j8;

.field public static final enum STARTED:LX/8j8;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1392191
    new-instance v0, LX/8j8;

    const-string v1, "STARTED"

    invoke-direct {v0, v1, v2}, LX/8j8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8j8;->STARTED:LX/8j8;

    .line 1392192
    new-instance v0, LX/8j8;

    const-string v1, "COMPLETED"

    invoke-direct {v0, v1, v3}, LX/8j8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8j8;->COMPLETED:LX/8j8;

    .line 1392193
    new-instance v0, LX/8j8;

    const-string v1, "CANCELLED"

    invoke-direct {v0, v1, v4}, LX/8j8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8j8;->CANCELLED:LX/8j8;

    .line 1392194
    new-instance v0, LX/8j8;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v5}, LX/8j8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8j8;->FAILED:LX/8j8;

    .line 1392195
    const/4 v0, 0x4

    new-array v0, v0, [LX/8j8;

    sget-object v1, LX/8j8;->STARTED:LX/8j8;

    aput-object v1, v0, v2

    sget-object v1, LX/8j8;->COMPLETED:LX/8j8;

    aput-object v1, v0, v3

    sget-object v1, LX/8j8;->CANCELLED:LX/8j8;

    aput-object v1, v0, v4

    sget-object v1, LX/8j8;->FAILED:LX/8j8;

    aput-object v1, v0, v5

    sput-object v0, LX/8j8;->$VALUES:[LX/8j8;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1392196
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8j8;
    .locals 1

    .prologue
    .line 1392197
    const-class v0, LX/8j8;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8j8;

    return-object v0
.end method

.method public static values()[LX/8j8;
    .locals 1

    .prologue
    .line 1392198
    sget-object v0, LX/8j8;->$VALUES:[LX/8j8;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8j8;

    return-object v0
.end method
