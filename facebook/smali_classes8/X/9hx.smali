.class public LX/9hx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4VT;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/4VT",
        "<",
        "Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;",
        "LX/5kO;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1527090
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1527091
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/9hx;->a:Ljava/lang/String;

    .line 1527092
    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1527093
    iget-object v0, p0, LX/9hx;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/16f;LX/40U;)V
    .locals 2

    .prologue
    .line 1527094
    check-cast p1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;

    check-cast p2, LX/5kO;

    .line 1527095
    iget-object v0, p0, LX/9hx;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1527096
    :goto_0
    return-void

    .line 1527097
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, LX/5kO;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;)V

    goto :goto_0
.end method

.method public final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1527098
    const-class v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1527099
    const-string v0, "RejectPlaceMetadataMutatingVisitor"

    return-object v0
.end method
