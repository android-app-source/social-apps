.class public final LX/92f;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8zq;


# instance fields
.field public final synthetic a:LX/91s;

.field public final synthetic b:LX/92o;


# direct methods
.method public constructor <init>(LX/92o;LX/91s;)V
    .locals 0

    .prologue
    .line 1432488
    iput-object p1, p0, LX/92f;->b:LX/92o;

    iput-object p2, p0, LX/92f;->a:LX/91s;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0Px;LX/0ta;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/92e;",
            ">;",
            "LX/0ta;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1432489
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1432490
    iget-object v0, p0, LX/92f;->a:LX/91s;

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "No verbs returned from server"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/91s;->a(Ljava/lang/Throwable;)V

    .line 1432491
    :goto_0
    return-void

    .line 1432492
    :cond_0
    const/4 v1, 0x0

    .line 1432493
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_3

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/92e;

    .line 1432494
    if-eqz v0, :cond_2

    .line 1432495
    iget-object v4, v0, LX/92e;->a:LX/5LG;

    move-object v4, v4

    .line 1432496
    if-eqz v4, :cond_2

    .line 1432497
    iget-object v4, v0, LX/92e;->a:LX/5LG;

    move-object v4, v4

    .line 1432498
    invoke-interface {v4}, LX/5LG;->l()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    const-string v4, "383634835006146"

    .line 1432499
    iget-object v5, v0, LX/92e;->a:LX/5LG;

    move-object v5, v5

    .line 1432500
    invoke-interface {v5}, LX/5LG;->l()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1432501
    :goto_2
    if-nez v0, :cond_1

    .line 1432502
    iget-object v1, p0, LX/92f;->a:LX/91s;

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Feelings verb not returned by verb fetch"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/91s;->a(Ljava/lang/Throwable;)V

    .line 1432503
    :cond_1
    iget-object v1, p0, LX/92f;->a:LX/91s;

    .line 1432504
    iget-object v2, v1, LX/91s;->a:LX/91v;

    .line 1432505
    iget-object v3, v0, LX/92e;->a:LX/5LG;

    move-object v3, v3

    .line 1432506
    iput-object v3, v2, LX/91v;->g:LX/5LG;

    .line 1432507
    iget-object v2, v1, LX/91s;->a:LX/91v;

    iget-object v2, v2, LX/91v;->f:LX/91D;

    iget-object v3, v1, LX/91s;->a:LX/91v;

    iget-object v3, v3, LX/91v;->d:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1432508
    iget-object v0, v3, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v3, v0

    .line 1432509
    const-string v4, "feeling_selector_feeling_verb_loaded"

    invoke-static {v4, v3}, LX/91D;->b(Ljava/lang/String;Ljava/lang/String;)LX/8yQ;

    move-result-object v4

    .line 1432510
    iget-object v0, v4, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v4, v0

    .line 1432511
    iget-object v0, v2, LX/91D;->a:LX/0Zb;

    invoke-interface {v0, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1432512
    iget-object v2, v1, LX/91s;->a:LX/91v;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/91v;->a(Ljava/lang/String;)V

    .line 1432513
    goto :goto_0

    .line 1432514
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1432515
    iget-object v0, p0, LX/92f;->a:LX/91s;

    invoke-virtual {v0, p1}, LX/91s;->a(Ljava/lang/Throwable;)V

    .line 1432516
    return-void
.end method
