.class public LX/95c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/95b;


# instance fields
.field private final a:LX/5Mb;

.field private final b:Ljava/lang/String;

.field private final c:LX/2js;

.field private final d:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/5Mb;Ljava/lang/String;LX/2js;LX/0QK;)V
    .locals 1
    .param p4    # LX/0QK;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5Mb",
            "<*>;",
            "Ljava/lang/String;",
            "LX/2js;",
            "LX/0QK",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1437121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1437122
    const/4 v0, 0x0

    iput-object v0, p0, LX/95c;->e:Ljava/util/ArrayList;

    .line 1437123
    iput-object p1, p0, LX/95c;->a:LX/5Mb;

    .line 1437124
    iput-object p2, p0, LX/95c;->b:Ljava/lang/String;

    .line 1437125
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2js;

    iput-object v0, p0, LX/95c;->c:LX/2js;

    .line 1437126
    iput-object p4, p0, LX/95c;->d:LX/0QK;

    .line 1437127
    return-void
.end method

.method private g(I)Lcom/facebook/flatbuffers/MutableFlattenable;
    .locals 1

    .prologue
    .line 1437118
    iget-object v0, p0, LX/95c;->a:LX/5Mb;

    .line 1437119
    iget-object p0, v0, LX/5Mb;->b:LX/0Px;

    move-object v0, p0

    .line 1437120
    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/MutableFlattenable;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1437115
    iget-object v0, p0, LX/95c;->a:LX/5Mb;

    .line 1437116
    iget-object p0, v0, LX/5Mb;->b:LX/0Px;

    move-object v0, p0

    .line 1437117
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final a(I)I
    .locals 1

    .prologue
    .line 1437114
    invoke-direct {p0, p1}, LX/95c;->g(I)Lcom/facebook/flatbuffers/MutableFlattenable;

    move-result-object v0

    invoke-interface {v0}, Lcom/facebook/flatbuffers/MutableFlattenable;->o_()I

    move-result v0

    return v0
.end method

.method public final b(I)LX/0w5;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/0w5",
            "<+",
            "Lcom/facebook/flatbuffers/MutableFlattenable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1437095
    invoke-direct {p0, p1}, LX/95c;->g(I)Lcom/facebook/flatbuffers/MutableFlattenable;

    move-result-object v0

    invoke-static {v0}, LX/0w5;->a(Lcom/facebook/flatbuffers/Flattenable;)LX/0w5;

    move-result-object v0

    return-object v0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1437106
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/95c;->e:Ljava/util/ArrayList;

    .line 1437107
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, LX/95c;->a()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1437108
    invoke-direct {p0, v0}, LX/95c;->g(I)Lcom/facebook/flatbuffers/MutableFlattenable;

    move-result-object v1

    .line 1437109
    iget-object v2, p0, LX/95c;->c:LX/2js;

    invoke-interface {v2, v1}, LX/2js;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    .line 1437110
    invoke-interface {v1}, Ljava/util/Set;->size()I

    .line 1437111
    iget-object v2, p0, LX/95c;->e:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1437112
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1437113
    :cond_0
    return-void
.end method

.method public final c(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1437105
    iget-object v0, p0, LX/95c;->b:Ljava/lang/String;

    invoke-static {v0, p1}, LX/2nU;->b(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d(I)Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1437102
    iget-object v0, p0, LX/95c;->e:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 1437103
    invoke-virtual {p0}, LX/95c;->b()V

    .line 1437104
    :cond_0
    iget-object v0, p0, LX/95c;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method public final e(I)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1437097
    iget-object v0, p0, LX/95c;->d:LX/0QK;

    if-eqz v0, :cond_0

    .line 1437098
    invoke-direct {p0, p1}, LX/95c;->g(I)Lcom/facebook/flatbuffers/MutableFlattenable;

    move-result-object v0

    .line 1437099
    iget-object v1, p0, LX/95c;->d:LX/0QK;

    invoke-interface {v1, v0}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1437100
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1437101
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f(I)I
    .locals 1

    .prologue
    .line 1437096
    const/4 v0, 0x0

    return v0
.end method
