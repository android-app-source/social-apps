.class public LX/8xy;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0tX;

.field private final b:LX/1Ck;

.field public final c:LX/20j;

.field public final d:LX/1K9;


# direct methods
.method public constructor <init>(LX/0tX;LX/20j;LX/1K9;LX/1Ck;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1424987
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1424988
    iput-object p1, p0, LX/8xy;->a:LX/0tX;

    .line 1424989
    iput-object p2, p0, LX/8xy;->c:LX/20j;

    .line 1424990
    iput-object p4, p0, LX/8xy;->b:LX/1Ck;

    .line 1424991
    iput-object p3, p0, LX/8xy;->d:LX/1K9;

    .line 1424992
    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/5HI;
    .locals 3
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1424971
    new-instance v0, LX/4IH;

    invoke-direct {v0}, LX/4IH;-><init>()V

    .line 1424972
    const-string v1, "comment_id"

    invoke-virtual {v0, v1, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1424973
    move-object v0, v0

    .line 1424974
    const-string v1, "slot_id"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1424975
    move-object v0, v0

    .line 1424976
    const-string v1, "name"

    invoke-virtual {v0, v1, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1424977
    move-object v0, v0

    .line 1424978
    invoke-static {p3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1424979
    const-string v1, "address"

    invoke-virtual {v0, v1, p3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1424980
    :cond_0
    invoke-static {p4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1424981
    const-string v1, "phone_number"

    invoke-virtual {v0, v1, p4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1424982
    :cond_1
    invoke-static {p5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1424983
    const-string v1, "website_uri"

    invoke-virtual {v0, v1, p5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1424984
    :cond_2
    new-instance v1, LX/5HI;

    invoke-direct {v1}, LX/5HI;-><init>()V

    move-object v1, v1

    .line 1424985
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1424986
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0TF;)V
    .locals 9
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsInterfaces$PlaceListLightweightCreateFields;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1424967
    invoke-static/range {p2 .. p7}, LX/8xy;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/5HI;

    move-result-object v0

    .line 1424968
    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 1424969
    iget-object v6, p0, LX/8xy;->b:LX/1Ck;

    const-string v7, "place_list_lightweight_rec_create"

    iget-object v1, p0, LX/8xy;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    new-instance v0, LX/8xx;

    move-object v1, p0

    move-object/from16 v2, p8

    move-object v3, p3

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LX/8xx;-><init>(LX/8xy;LX/0TF;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;)V

    invoke-virtual {v6, v7, v8, v0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1424970
    return-void
.end method
