.class public final LX/9en;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

.field public final synthetic b:Lcom/facebook/photos/editgallery/EditGalleryActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/editgallery/EditGalleryActivity;Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;)V
    .locals 0

    .prologue
    .line 1520373
    iput-object p1, p0, LX/9en;->b:Lcom/facebook/photos/editgallery/EditGalleryActivity;

    iput-object p2, p0, LX/9en;->a:Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Landroid/net/Uri;)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 1520375
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1520376
    iget-object v0, p0, LX/9en;->b:Lcom/facebook/photos/editgallery/EditGalleryActivity;

    const v1, 0x7f0d0d1b

    invoke-virtual {v0, v1}, Lcom/facebook/photos/editgallery/EditGalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    .line 1520377
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1520378
    iget-object v0, p0, LX/9en;->b:Lcom/facebook/photos/editgallery/EditGalleryActivity;

    .line 1520379
    iput-object p1, v0, Lcom/facebook/photos/editgallery/EditGalleryActivity;->s:Landroid/net/Uri;

    .line 1520380
    iget-object v0, p0, LX/9en;->b:Lcom/facebook/photos/editgallery/EditGalleryActivity;

    invoke-virtual {v0}, Lcom/facebook/photos/editgallery/EditGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1941

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    .line 1520381
    iget-object v1, p0, LX/9en;->b:Lcom/facebook/photos/editgallery/EditGalleryActivity;

    invoke-static {v1, v0}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v2

    .line 1520382
    iget-object v0, p0, LX/9en;->b:Lcom/facebook/photos/editgallery/EditGalleryActivity;

    iget-object v1, p0, LX/9en;->b:Lcom/facebook/photos/editgallery/EditGalleryActivity;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryActivity;->s:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/facebook/photos/editgallery/EditGalleryActivity;->a(Lcom/facebook/photos/editgallery/EditGalleryActivity;Landroid/net/Uri;)F

    move-result v0

    .line 1520383
    int-to-float v1, v2

    div-float v0, v1, v0

    float-to-int v3, v0

    .line 1520384
    iget-object v0, p0, LX/9en;->b:Lcom/facebook/photos/editgallery/EditGalleryActivity;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryActivity;->r:LX/9fD;

    iget-object v1, p0, LX/9en;->b:Lcom/facebook/photos/editgallery/EditGalleryActivity;

    iget-object v1, v1, Lcom/facebook/photos/editgallery/EditGalleryActivity;->s:Landroid/net/Uri;

    iget-object v4, p0, LX/9en;->a:Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    iget-object v5, p0, LX/9en;->b:Lcom/facebook/photos/editgallery/EditGalleryActivity;

    iget-object v5, v5, Lcom/facebook/photos/editgallery/EditGalleryActivity;->q:LX/9el;

    move-object v7, v6

    invoke-virtual/range {v0 .. v7}, LX/9fD;->a(Landroid/net/Uri;IILcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;LX/9el;Ljava/util/List;LX/9fh;)V

    .line 1520385
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1520386
    iget-object v0, p0, LX/9en;->b:Lcom/facebook/photos/editgallery/EditGalleryActivity;

    iget-object v0, v0, Lcom/facebook/photos/editgallery/EditGalleryActivity;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Failed to save photo to file"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1520387
    iget-object v0, p0, LX/9en;->b:Lcom/facebook/photos/editgallery/EditGalleryActivity;

    invoke-virtual {v0}, Lcom/facebook/photos/editgallery/EditGalleryActivity;->finish()V

    .line 1520388
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1520374
    check-cast p1, Landroid/net/Uri;

    invoke-direct {p0, p1}, LX/9en;->a(Landroid/net/Uri;)V

    return-void
.end method
