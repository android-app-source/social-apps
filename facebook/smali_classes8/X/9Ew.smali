.class public final LX/9Ew;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feedback/ui/SpeechCommentDialog;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/ui/SpeechCommentDialog;)V
    .locals 0

    .prologue
    .line 1457654
    iput-object p1, p0, LX/9Ew;->a:Lcom/facebook/feedback/ui/SpeechCommentDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1457655
    iget-object v2, p0, LX/9Ew;->a:Lcom/facebook/feedback/ui/SpeechCommentDialog;

    iget-object v2, v2, Lcom/facebook/feedback/ui/SpeechCommentDialog;->v:LX/9F0;

    if-nez v2, :cond_0

    .line 1457656
    :goto_0
    return v0

    .line 1457657
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    move v0, v1

    .line 1457658
    goto :goto_0

    .line 1457659
    :pswitch_1
    iget-object v0, p0, LX/9Ew;->a:Lcom/facebook/feedback/ui/SpeechCommentDialog;

    iget-object v0, v0, Lcom/facebook/feedback/ui/SpeechCommentDialog;->v:LX/9F0;

    .line 1457660
    iget-boolean v2, v0, LX/9F0;->g:Z

    if-eqz v2, :cond_1

    .line 1457661
    invoke-static {v0}, LX/9F0;->g(LX/9F0;)V

    .line 1457662
    invoke-static {v0}, LX/9F0;->f(LX/9F0;)V

    .line 1457663
    :goto_1
    move v0, v1

    .line 1457664
    goto :goto_0

    .line 1457665
    :cond_1
    iget-object v2, v0, LX/9F0;->k:LX/0i4;

    iget-object p0, v0, LX/9F0;->f:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v2, p0}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v2

    .line 1457666
    sget-object p0, LX/9F0;->b:[Ljava/lang/String;

    new-instance p1, LX/9Ez;

    invoke-direct {p1, v0}, LX/9Ez;-><init>(LX/9F0;)V

    invoke-virtual {v2, p0, p1}, LX/0i5;->a([Ljava/lang/String;LX/6Zx;)V

    .line 1457667
    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
