.class public LX/9kE;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/util/List;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/9kD",
            "<",
            "LX/0TF;",
            ">;>;"
        }
    .end annotation
.end field

.field public c:Landroid/os/Handler;

.field private final d:Ljava/util/concurrent/ExecutorService;

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Ljava/util/concurrent/Future;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1530980
    const-class v0, LX/9kE;

    sput-object v0, LX/9kE;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/ExecutorService;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1530974
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1530975
    const/4 v0, 0x0

    iput-object v0, p0, LX/9kE;->e:Ljava/util/List;

    .line 1530976
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/9kE;->a:Ljava/util/List;

    .line 1530977
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, LX/9kE;->f:Ljava/util/WeakHashMap;

    .line 1530978
    iput-object p1, p0, LX/9kE;->d:Ljava/util/concurrent/ExecutorService;

    .line 1530979
    return-void
.end method

.method private a(LX/0TF;)LX/9kD;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TF;",
            ")",
            "LX/9kD",
            "<",
            "LX/0TF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1530971
    new-instance v0, LX/9kD;

    invoke-direct {v0, p1}, LX/9kD;-><init>(Ljava/lang/Object;)V

    .line 1530972
    iget-object v1, p0, LX/9kE;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1530973
    return-object v0
.end method

.method public static a(LX/0QB;)LX/9kE;
    .locals 1

    .prologue
    .line 1530970
    invoke-static {p0}, LX/9kE;->b(LX/0QB;)LX/9kE;

    move-result-object v0

    return-object v0
.end method

.method public static a$redex0(LX/9kE;LX/9kD;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/9kD",
            "<",
            "LX/0TF;",
            ">;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1530968
    iget-object v0, p0, LX/9kE;->c:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/places/future/SimpleExecutor$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/places/future/SimpleExecutor$3;-><init>(LX/9kE;LX/9kD;Ljava/lang/Object;)V

    const v2, -0xaa438be

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1530969
    return-void
.end method

.method public static a$redex0(LX/9kE;LX/9kD;Ljava/lang/Throwable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/9kD",
            "<",
            "LX/0TF;",
            ">;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1530965
    sget-object v0, LX/9kE;->b:Ljava/lang/Class;

    const-string v1, "Exception in background task"

    invoke-static {v0, v1, p2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1530966
    iget-object v0, p0, LX/9kE;->c:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/places/future/SimpleExecutor$4;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/places/future/SimpleExecutor$4;-><init>(LX/9kE;LX/9kD;Ljava/lang/Throwable;)V

    const v2, 0x64c332d1

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1530967
    return-void
.end method

.method public static b(LX/0QB;)LX/9kE;
    .locals 2

    .prologue
    .line 1530981
    new-instance v1, LX/9kE;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    invoke-direct {v1, v0}, LX/9kE;-><init>(Ljava/util/concurrent/ExecutorService;)V

    .line 1530982
    return-object v1
.end method

.method private d()V
    .locals 1

    .prologue
    .line 1530962
    iget-object v0, p0, LX/9kE;->c:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 1530963
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/9kE;->c:Landroid/os/Handler;

    .line 1530964
    :cond_0
    return-void
.end method

.method public static e(LX/9kE;)V
    .locals 2

    .prologue
    .line 1530958
    iget-object v0, p0, LX/9kE;->c:Landroid/os/Handler;

    if-nez v0, :cond_1

    .line 1530959
    :cond_0
    return-void

    .line 1530960
    :cond_1
    iget-object v0, p0, LX/9kE;->c:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1530961
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SimpleExecutor is not thread-safe and should be called from a single Looper thread"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1530934
    invoke-static {p0}, LX/9kE;->e(LX/9kE;)V

    .line 1530935
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/9kE;->e:Ljava/util/List;

    .line 1530936
    return-void
.end method

.method public final a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;",
            "LX/0TF",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1530952
    invoke-direct {p0}, LX/9kE;->d()V

    .line 1530953
    invoke-static {p0}, LX/9kE;->e(LX/9kE;)V

    .line 1530954
    invoke-direct {p0, p2}, LX/9kE;->a(LX/0TF;)LX/9kD;

    move-result-object v0

    .line 1530955
    new-instance v1, LX/9kC;

    invoke-direct {v1, p0, v0}, LX/9kC;-><init>(LX/9kE;LX/9kD;)V

    invoke-static {p1, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1530956
    iget-object v0, p0, LX/9kE;->f:Ljava/util/WeakHashMap;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1530957
    return-void
.end method

.method public final a(Ljava/util/concurrent/Callable;LX/0TF;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TT;>;",
            "LX/0TF",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1530946
    invoke-direct {p0}, LX/9kE;->d()V

    .line 1530947
    invoke-static {p0}, LX/9kE;->e(LX/9kE;)V

    .line 1530948
    invoke-direct {p0, p2}, LX/9kE;->a(LX/0TF;)LX/9kD;

    move-result-object v0

    .line 1530949
    iget-object v1, p0, LX/9kE;->d:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/places/future/SimpleExecutor$1;

    invoke-direct {v2, p0, p1, v0}, Lcom/facebook/places/future/SimpleExecutor$1;-><init>(LX/9kE;Ljava/util/concurrent/Callable;LX/9kD;)V

    const v0, 0x425a6c22

    invoke-static {v1, v2, v0}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    move-result-object v0

    .line 1530950
    iget-object v1, p0, LX/9kE;->f:Ljava/util/WeakHashMap;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1530951
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1530937
    invoke-static {p0}, LX/9kE;->e(LX/9kE;)V

    .line 1530938
    iget-object v0, p0, LX/9kE;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9kD;

    .line 1530939
    iput-object v2, v0, LX/9kD;->a:Ljava/lang/Object;

    .line 1530940
    goto :goto_0

    .line 1530941
    :cond_0
    iget-object v0, p0, LX/9kE;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1530942
    iput-object v2, p0, LX/9kE;->e:Ljava/util/List;

    .line 1530943
    iget-object v0, p0, LX/9kE;->f:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1530944
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Future;

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Ljava/util/concurrent/Future;->cancel(Z)Z

    goto :goto_1

    .line 1530945
    :cond_1
    return-void
.end method
