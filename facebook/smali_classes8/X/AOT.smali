.class public LX/AOT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ANo;


# instance fields
.field public final a:LX/6Jt;

.field public final b:Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;

.field private final c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field private final d:Landroid/content/Context;

.field private e:LX/AOR;

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "LX/BVJ;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/6Jt;Lcom/facebook/widget/recyclerview/BetterRecyclerView;Landroid/content/Context;Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;)V
    .locals 0
    .param p1    # LX/6Jt;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/widget/recyclerview/BetterRecyclerView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1668944
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1668945
    iput-object p1, p0, LX/AOT;->a:LX/6Jt;

    .line 1668946
    iput-object p4, p0, LX/AOT;->b:Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;

    .line 1668947
    iput-object p2, p0, LX/AOT;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1668948
    iput-object p3, p0, LX/AOT;->d:Landroid/content/Context;

    .line 1668949
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 1

    .prologue
    .line 1668950
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;)V
    .locals 14

    .prologue
    const/4 v2, 0x0

    .line 1668951
    iget-object v0, p0, LX/AOT;->e:LX/AOR;

    if-eqz v0, :cond_0

    .line 1668952
    iget-object v0, p0, LX/AOT;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, LX/AOT;->e:LX/AOR;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1668953
    :goto_0
    return-void

    .line 1668954
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;->getHeight()I

    move-result v1

    .line 1668955
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1668956
    new-instance v4, Landroid/util/Pair;

    const-string v5, "None"

    new-instance v6, LX/BVJ;

    new-instance v7, LX/BV8;

    invoke-direct {v7}, LX/BV8;-><init>()V

    invoke-virtual {v7}, LX/BV8;->a()LX/BVA;

    move-result-object v7

    invoke-direct {v6, v7}, LX/BVJ;-><init>(LX/BVA;)V

    invoke-direct {v4, v5, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668957
    const v11, 0x3f866666    # 1.05f

    const/4 v10, 0x0

    .line 1668958
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 1668959
    const-string v5, "{\"fallinggrid\":{},\"facekillah\":{}}"

    .line 1668960
    new-instance v6, LX/BV9;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "res:///"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v8, 0x7f020dc5

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    new-instance v8, LX/BVG;

    invoke-direct {v8}, LX/BVG;-><init>()V

    invoke-virtual {v8, v10, v10}, LX/BVG;->g(FF)LX/BVG;

    move-result-object v8

    invoke-virtual {v8, v10, v10}, LX/BVG;->h(FF)LX/BVG;

    move-result-object v8

    const/4 v9, -0x1

    invoke-virtual {v8, v9}, LX/BVG;->d(I)LX/BVG;

    move-result-object v8

    invoke-virtual {v8, v11, v11}, LX/BVG;->f(FF)LX/BVG;

    move-result-object v8

    const/16 v9, 0x46

    invoke-virtual {v8, v9}, LX/BVG;->a(I)LX/BVG;

    move-result-object v8

    const/high16 v9, 0x428c0000    # 70.0f

    invoke-virtual {v8, v9}, LX/BVG;->a(F)LX/BVG;

    move-result-object v8

    const/16 v9, 0x1388

    invoke-virtual {v8, v9}, LX/BVG;->b(I)LX/BVG;

    move-result-object v8

    .line 1668961
    iput v10, v8, LX/BVG;->L:F

    .line 1668962
    move-object v8, v8

    .line 1668963
    new-instance v9, LX/BVB;

    invoke-direct {v9}, LX/BVB;-><init>()V

    invoke-virtual {v9, v10, v10}, LX/BVB;->a(FF)LX/BVB;

    move-result-object v9

    invoke-virtual {v9, v10, v10}, LX/BVB;->b(FF)LX/BVB;

    move-result-object v9

    invoke-direct {v6, v7, v8, v9, v5}, LX/BV9;-><init>(Landroid/net/Uri;LX/BVG;LX/BVB;Ljava/lang/String;)V

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668964
    new-instance v5, Landroid/util/Pair;

    const-string v6, "FaceInvaders"

    new-instance v7, LX/BVJ;

    new-instance v8, LX/BV8;

    invoke-direct {v8}, LX/BV8;-><init>()V

    .line 1668965
    iput-object v4, v8, LX/BV8;->b:Ljava/util/List;

    .line 1668966
    move-object v4, v8

    .line 1668967
    invoke-virtual {v4}, LX/BV8;->a()LX/BVA;

    move-result-object v4

    invoke-direct {v7, v4}, LX/BVJ;-><init>(LX/BVA;)V

    invoke-direct {v5, v6, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v4, v5

    .line 1668968
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668969
    const/high16 p1, 0x40c00000    # 6.0f

    const/high16 v13, 0x40000000    # 2.0f

    const/high16 v12, -0x40800000    # -1.0f

    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v11, 0x0

    .line 1668970
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 1668971
    mul-int/lit8 v5, v0, 0x20

    div-int/2addr v5, v1

    .line 1668972
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "{\"grid\":{\"cols\":"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", \"rows\":32"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", \"overscan\":0"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "},\"optflowdebug\":{}"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "}"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1668973
    new-instance v7, LX/BV9;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "res:///"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v9, 0x7f0200b2

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    new-instance v9, LX/BVG;

    invoke-direct {v9}, LX/BVG;-><init>()V

    invoke-virtual {v9, v11, v11, v10, v10}, LX/BVG;->a(FFFF)LX/BVG;

    move-result-object v9

    invoke-virtual {v9, v11, v11, v10, v10}, LX/BVG;->b(FFFF)LX/BVG;

    move-result-object v9

    invoke-virtual {v9, v12, v12}, LX/BVG;->c(FF)LX/BVG;

    move-result-object v9

    invoke-virtual {v9, v10, v10}, LX/BVG;->d(FF)LX/BVG;

    move-result-object v9

    invoke-virtual {v9, v11, v11}, LX/BVG;->g(FF)LX/BVG;

    move-result-object v9

    invoke-virtual {v9, v11, v11}, LX/BVG;->h(FF)LX/BVG;

    move-result-object v9

    const/4 v10, -0x1

    invoke-virtual {v9, v10}, LX/BVG;->d(I)LX/BVG;

    move-result-object v9

    invoke-virtual {v9, v13, v13}, LX/BVG;->f(FF)LX/BVG;

    move-result-object v9

    invoke-virtual {v9, p1, p1}, LX/BVG;->i(FF)LX/BVG;

    move-result-object v9

    add-int/lit8 v5, v5, 0x0

    mul-int/lit8 v5, v5, 0x20

    invoke-virtual {v9, v5}, LX/BVG;->a(I)LX/BVG;

    move-result-object v5

    invoke-virtual {v5, v11, v11}, LX/BVG;->e(FF)LX/BVG;

    move-result-object v5

    invoke-virtual {v5, v11}, LX/BVG;->a(F)LX/BVG;

    move-result-object v5

    const/16 v9, 0xbb8

    invoke-virtual {v5, v9}, LX/BVG;->b(I)LX/BVG;

    move-result-object v5

    const/high16 v9, 0x42a00000    # 80.0f

    .line 1668974
    iput v9, v5, LX/BVG;->L:F

    .line 1668975
    move-object v5, v5

    .line 1668976
    new-instance v9, LX/BVB;

    invoke-direct {v9}, LX/BVB;-><init>()V

    invoke-virtual {v9, v11, v11}, LX/BVB;->a(FF)LX/BVB;

    move-result-object v9

    invoke-virtual {v9, v11, v11}, LX/BVB;->b(FF)LX/BVB;

    move-result-object v9

    invoke-direct {v7, v8, v5, v9, v6}, LX/BV9;-><init>(Landroid/net/Uri;LX/BVG;LX/BVB;Ljava/lang/String;)V

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668977
    new-instance v5, Landroid/util/Pair;

    const-string v6, "OptFlow"

    new-instance v7, LX/BVJ;

    new-instance v8, LX/BV8;

    invoke-direct {v8}, LX/BV8;-><init>()V

    const/4 v9, 0x0

    .line 1668978
    iput-object v9, v8, LX/BV8;->a:Ljava/lang/String;

    .line 1668979
    move-object v8, v8

    .line 1668980
    iput-object v4, v8, LX/BV8;->b:Ljava/util/List;

    .line 1668981
    move-object v4, v8

    .line 1668982
    const/4 v8, 0x0

    .line 1668983
    iput-object v8, v4, LX/BV8;->c:LX/BVN;

    .line 1668984
    move-object v4, v4

    .line 1668985
    invoke-virtual {v4}, LX/BV8;->a()LX/BVA;

    move-result-object v4

    invoke-direct {v7, v4}, LX/BVJ;-><init>(LX/BVA;)V

    invoke-direct {v5, v6, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v4, v5

    .line 1668986
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668987
    const/high16 v12, 0x3f800000    # 1.0f

    const/4 v11, 0x0

    .line 1668988
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 1668989
    mul-int/lit8 v5, v0, 0x30

    div-int/2addr v5, v1

    .line 1668990
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "{\"grid\":{\"cols\":"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", \"rows\":48"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", \"overscan\":0"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "},\"facehighlighter\":{}"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ",\"rainbow\":{}}"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1668991
    new-instance v7, LX/BV9;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "res:///"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v9, 0x7f020166

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    new-instance v9, LX/BVG;

    invoke-direct {v9}, LX/BVG;-><init>()V

    invoke-virtual {v9, v11, v11}, LX/BVG;->g(FF)LX/BVG;

    move-result-object v9

    invoke-virtual {v9, v11, v11}, LX/BVG;->h(FF)LX/BVG;

    move-result-object v9

    const/4 v10, -0x1

    invoke-virtual {v9, v10}, LX/BVG;->d(I)LX/BVG;

    move-result-object v9

    invoke-virtual {v9, v12, v12}, LX/BVG;->f(FF)LX/BVG;

    move-result-object v9

    add-int/lit8 v10, v5, 0x0

    mul-int/lit8 v10, v10, 0x30

    invoke-virtual {v9, v10}, LX/BVG;->a(I)LX/BVG;

    move-result-object v9

    invoke-virtual {v9, v11}, LX/BVG;->a(F)LX/BVG;

    move-result-object v9

    add-int/lit8 v5, v5, 0x0

    mul-int/lit8 v5, v5, 0x30

    invoke-virtual {v9, v5}, LX/BVG;->b(I)LX/BVG;

    move-result-object v5

    .line 1668992
    iput v11, v5, LX/BVG;->L:F

    .line 1668993
    move-object v5, v5

    .line 1668994
    new-instance v9, LX/BVB;

    invoke-direct {v9}, LX/BVB;-><init>()V

    invoke-virtual {v9, v11, v11}, LX/BVB;->a(FF)LX/BVB;

    move-result-object v9

    invoke-virtual {v9, v11, v11}, LX/BVB;->b(FF)LX/BVB;

    move-result-object v9

    invoke-direct {v7, v8, v5, v9, v6}, LX/BV9;-><init>(Landroid/net/Uri;LX/BVG;LX/BVB;Ljava/lang/String;)V

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668995
    new-instance v5, Landroid/util/Pair;

    const-string v6, "Disco"

    new-instance v7, LX/BVJ;

    new-instance v8, LX/BV8;

    invoke-direct {v8}, LX/BV8;-><init>()V

    .line 1668996
    iput-object v4, v8, LX/BV8;->b:Ljava/util/List;

    .line 1668997
    move-object v4, v8

    .line 1668998
    invoke-virtual {v4}, LX/BV8;->a()LX/BVA;

    move-result-object v4

    invoke-direct {v7, v4}, LX/BVJ;-><init>(LX/BVA;)V

    invoke-direct {v5, v6, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v4, v5

    .line 1668999
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1669000
    const v10, -0x4059999a    # -1.3f

    const/high16 v12, 0x3f800000    # 1.0f

    const/4 v11, 0x0

    .line 1669001
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 1669002
    const-string v5, "{\"faceavoidah\":{\"strength\": -0.025 }}"

    .line 1669003
    new-instance v6, LX/BV9;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "res:///"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v8, 0x7f020411

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    new-instance v8, LX/BVG;

    invoke-direct {v8}, LX/BVG;-><init>()V

    const v9, 0x3e4ccccd    # 0.2f

    invoke-virtual {v8, v11, v11, v12, v9}, LX/BVG;->a(FFFF)LX/BVG;

    move-result-object v8

    invoke-virtual {v8, v11, v11, v12, v12}, LX/BVG;->b(FFFF)LX/BVG;

    move-result-object v8

    const/high16 v9, -0x40800000    # -1.0f

    invoke-virtual {v8, v9, v10}, LX/BVG;->c(FF)LX/BVG;

    move-result-object v8

    invoke-virtual {v8, v12, v10}, LX/BVG;->d(FF)LX/BVG;

    move-result-object v8

    invoke-virtual {v8, v11, v11}, LX/BVG;->g(FF)LX/BVG;

    move-result-object v8

    invoke-virtual {v8, v11, v11}, LX/BVG;->h(FF)LX/BVG;

    move-result-object v8

    const/16 v9, 0xfa0

    invoke-virtual {v8, v9}, LX/BVG;->c(I)LX/BVG;

    move-result-object v8

    const/16 v9, 0x1b58

    invoke-virtual {v8, v9}, LX/BVG;->d(I)LX/BVG;

    move-result-object v8

    const v9, 0x3dcccccd    # 0.1f

    const v10, 0x3ecccccd    # 0.4f

    invoke-virtual {v8, v9, v10}, LX/BVG;->f(FF)LX/BVG;

    move-result-object v8

    const/high16 v9, 0x40000000    # 2.0f

    const/high16 v10, 0x40e00000    # 7.0f

    invoke-virtual {v8, v9, v10}, LX/BVG;->i(FF)LX/BVG;

    move-result-object v8

    const/high16 v9, 0x43480000    # 200.0f

    invoke-virtual {v8, v9}, LX/BVG;->a(F)LX/BVG;

    move-result-object v8

    const/16 v9, 0x12c

    invoke-virtual {v8, v9}, LX/BVG;->a(I)LX/BVG;

    move-result-object v8

    const v9, 0x3fa66666    # 1.3f

    invoke-virtual {v8, v11, v9}, LX/BVG;->e(FF)LX/BVG;

    move-result-object v8

    const/16 v9, 0xbb8

    invoke-virtual {v8, v9}, LX/BVG;->b(I)LX/BVG;

    move-result-object v8

    .line 1669004
    iput v11, v8, LX/BVG;->L:F

    .line 1669005
    move-object v8, v8

    .line 1669006
    new-instance v9, LX/BVB;

    invoke-direct {v9}, LX/BVB;-><init>()V

    invoke-virtual {v9, v12, v12}, LX/BVB;->a(FF)LX/BVB;

    move-result-object v9

    invoke-virtual {v9, v11, v11}, LX/BVB;->b(FF)LX/BVB;

    move-result-object v9

    invoke-direct {v6, v7, v8, v9, v5}, LX/BV9;-><init>(Landroid/net/Uri;LX/BVG;LX/BVB;Ljava/lang/String;)V

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1669007
    new-instance v5, Landroid/util/Pair;

    const-string v6, "Dust"

    new-instance v7, LX/BVJ;

    new-instance v8, LX/BV8;

    invoke-direct {v8}, LX/BV8;-><init>()V

    .line 1669008
    iput-object v4, v8, LX/BV8;->b:Ljava/util/List;

    .line 1669009
    move-object v4, v8

    .line 1669010
    invoke-virtual {v4}, LX/BV8;->a()LX/BVA;

    move-result-object v4

    invoke-direct {v7, v4}, LX/BVJ;-><init>(LX/BVA;)V

    invoke-direct {v5, v6, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v4, v5

    .line 1669011
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1669012
    const/4 p1, 0x0

    const/16 v13, 0x1388

    const v12, 0x3e4ccccd    # 0.2f

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v10, 0x0

    .line 1669013
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 1669014
    const-string v5, "{\"faceavoidah\":{\"strength\": -0.025 }}"

    .line 1669015
    new-instance v6, LX/BV9;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "res:///"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v8, 0x7f021859

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    new-instance v8, LX/BVG;

    invoke-direct {v8}, LX/BVG;-><init>()V

    invoke-virtual {v8, v10, v10}, LX/BVG;->e(FF)LX/BVG;

    move-result-object v8

    invoke-virtual {v8, v10, v10}, LX/BVG;->g(FF)LX/BVG;

    move-result-object v8

    invoke-virtual {v8, v10, v10}, LX/BVG;->h(FF)LX/BVG;

    move-result-object v8

    const/4 v9, -0x1

    invoke-virtual {v8, v9}, LX/BVG;->d(I)LX/BVG;

    move-result-object v8

    invoke-virtual {v8, v10}, LX/BVG;->a(F)LX/BVG;

    move-result-object v8

    invoke-virtual {v8, v13}, LX/BVG;->b(I)LX/BVG;

    move-result-object v8

    invoke-virtual {v8, v13}, LX/BVG;->a(I)LX/BVG;

    move-result-object v8

    invoke-virtual {v8, v12, v12}, LX/BVG;->f(FF)LX/BVG;

    move-result-object v8

    invoke-virtual {v8, v11, v11}, LX/BVG;->i(FF)LX/BVG;

    move-result-object v8

    const/high16 v9, 0x41700000    # 15.0f

    .line 1669016
    iput v9, v8, LX/BVG;->L:F

    .line 1669017
    move-object v8, v8

    .line 1669018
    new-instance v9, LX/BVB;

    invoke-direct {v9}, LX/BVB;-><init>()V

    invoke-virtual {v9, v11, v11}, LX/BVB;->a(FF)LX/BVB;

    move-result-object v9

    invoke-virtual {v9, v10, v10}, LX/BVB;->b(FF)LX/BVB;

    move-result-object v9

    invoke-direct {v6, v7, v8, v9, v5}, LX/BV9;-><init>(Landroid/net/Uri;LX/BVG;LX/BVB;Ljava/lang/String;)V

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1669019
    new-instance v5, Landroid/util/Pair;

    const-string v6, "Random"

    new-instance v7, LX/BVJ;

    new-instance v8, LX/BV8;

    invoke-direct {v8}, LX/BV8;-><init>()V

    .line 1669020
    iput-object p1, v8, LX/BV8;->a:Ljava/lang/String;

    .line 1669021
    move-object v8, v8

    .line 1669022
    iput-object v4, v8, LX/BV8;->b:Ljava/util/List;

    .line 1669023
    move-object v4, v8

    .line 1669024
    iput-object p1, v4, LX/BV8;->c:LX/BVN;

    .line 1669025
    move-object v4, v4

    .line 1669026
    invoke-virtual {v4}, LX/BV8;->a()LX/BVA;

    move-result-object v4

    invoke-direct {v7, v4}, LX/BVJ;-><init>(LX/BVA;)V

    invoke-direct {v5, v6, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v4, v5

    .line 1669027
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1669028
    const/4 p1, 0x0

    const/high16 v13, 0x40000000    # 2.0f

    const v12, -0x4059999a    # -1.3f

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v10, 0x0

    .line 1669029
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 1669030
    const-string v5, "{}"

    .line 1669031
    new-instance v6, LX/BV9;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "res:///"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v8, 0x7f02018d

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    new-instance v8, LX/BVG;

    invoke-direct {v8}, LX/BVG;-><init>()V

    invoke-virtual {v8, v10, v10, v11, v11}, LX/BVG;->a(FFFF)LX/BVG;

    move-result-object v8

    invoke-virtual {v8, v10, v10, v11, v11}, LX/BVG;->b(FFFF)LX/BVG;

    move-result-object v8

    const/high16 v9, -0x40800000    # -1.0f

    invoke-virtual {v8, v9, v12}, LX/BVG;->c(FF)LX/BVG;

    move-result-object v8

    invoke-virtual {v8, v11, v12}, LX/BVG;->d(FF)LX/BVG;

    move-result-object v8

    invoke-virtual {v8, v10, v10}, LX/BVG;->g(FF)LX/BVG;

    move-result-object v8

    invoke-virtual {v8, v10, v10}, LX/BVG;->h(FF)LX/BVG;

    move-result-object v8

    const/16 v9, 0x1b58

    invoke-virtual {v8, v9}, LX/BVG;->c(I)LX/BVG;

    move-result-object v8

    const/16 v9, 0x3a98

    invoke-virtual {v8, v9}, LX/BVG;->d(I)LX/BVG;

    move-result-object v8

    invoke-virtual {v8, v11, v11}, LX/BVG;->f(FF)LX/BVG;

    move-result-object v8

    const/high16 v9, 0x40400000    # 3.0f

    invoke-virtual {v8, v13, v9}, LX/BVG;->i(FF)LX/BVG;

    move-result-object v8

    invoke-virtual {v8, v13}, LX/BVG;->a(F)LX/BVG;

    move-result-object v8

    const/4 v9, 0x5

    invoke-virtual {v8, v9}, LX/BVG;->a(I)LX/BVG;

    move-result-object v8

    const/high16 v9, 0x3f000000    # 0.5f

    invoke-virtual {v8, v10, v9}, LX/BVG;->e(FF)LX/BVG;

    move-result-object v8

    const/16 v9, 0x28

    invoke-virtual {v8, v9}, LX/BVG;->b(I)LX/BVG;

    move-result-object v8

    .line 1669032
    iput v10, v8, LX/BVG;->L:F

    .line 1669033
    move-object v8, v8

    .line 1669034
    new-instance v9, LX/BVB;

    invoke-direct {v9}, LX/BVB;-><init>()V

    invoke-virtual {v9, v11, v11}, LX/BVB;->a(FF)LX/BVB;

    move-result-object v9

    invoke-virtual {v9, v10, v10}, LX/BVB;->b(FF)LX/BVB;

    move-result-object v9

    invoke-direct {v6, v7, v8, v9, v5}, LX/BV9;-><init>(Landroid/net/Uri;LX/BVG;LX/BVB;Ljava/lang/String;)V

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1669035
    new-instance v5, Landroid/util/Pair;

    const-string v6, "Bubbles"

    new-instance v7, LX/BVJ;

    new-instance v8, LX/BV8;

    invoke-direct {v8}, LX/BV8;-><init>()V

    .line 1669036
    iput-object p1, v8, LX/BV8;->a:Ljava/lang/String;

    .line 1669037
    move-object v8, v8

    .line 1669038
    iput-object v4, v8, LX/BV8;->b:Ljava/util/List;

    .line 1669039
    move-object v4, v8

    .line 1669040
    iput-object p1, v4, LX/BV8;->c:LX/BVN;

    .line 1669041
    move-object v4, v4

    .line 1669042
    invoke-virtual {v4}, LX/BV8;->a()LX/BVA;

    move-result-object v4

    invoke-direct {v7, v4}, LX/BVJ;-><init>(LX/BVA;)V

    invoke-direct {v5, v6, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v4, v5

    .line 1669043
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1669044
    invoke-static {}, LX/BVO;->e()Landroid/util/Pair;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1669045
    move-object v0, v3

    .line 1669046
    iput-object v0, p0, LX/AOT;->f:Ljava/util/List;

    .line 1669047
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    move v1, v2

    .line 1669048
    :goto_1
    iget-object v0, p0, LX/AOT;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1669049
    iget-object v0, p0, LX/AOT;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1669050
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1669051
    :cond_1
    new-instance v0, LX/AOR;

    iget-object v1, p0, LX/AOT;->d:Landroid/content/Context;

    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    new-instance v4, LX/AOS;

    invoke-direct {v4, p0}, LX/AOS;-><init>(LX/AOT;)V

    invoke-direct {v0, v1, v3, v2, v4}, LX/AOR;-><init>(Landroid/content/Context;LX/0Px;ILX/ANk;)V

    iput-object v0, p0, LX/AOT;->e:LX/AOR;

    .line 1669052
    iget-object v0, p0, LX/AOT;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, LX/AOT;->e:LX/AOR;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    goto/16 :goto_0
.end method

.method public final b(Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;)V
    .locals 2

    .prologue
    .line 1669053
    iget-object v0, p0, LX/AOT;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1669054
    return-void
.end method
