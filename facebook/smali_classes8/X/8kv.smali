.class public final LX/8kv;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/stickers/model/StickerPack;

.field public final synthetic b:Lcom/facebook/stickers/keyboard/StickerKeyboardView;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/keyboard/StickerKeyboardView;Lcom/facebook/stickers/model/StickerPack;)V
    .locals 0

    .prologue
    .line 1396938
    iput-object p1, p0, LX/8kv;->b:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    iput-object p2, p0, LX/8kv;->a:Lcom/facebook/stickers/model/StickerPack;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 1396939
    sget-object v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->c:Ljava/lang/Class;

    const-string v1, "Unable to close sticker pack %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/8kv;->a:Lcom/facebook/stickers/model/StickerPack;

    .line 1396940
    iget-object v5, v4, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v4, v5

    .line 1396941
    aput-object v4, v2, v3

    invoke-static {v0, p1, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1396942
    iget-object v0, p0, LX/8kv;->b:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    const/4 v1, 0x0

    .line 1396943
    iput-object v1, v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->s:LX/1Mv;

    .line 1396944
    iget-object v0, p0, LX/8kv;->b:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    iget-object v0, v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->l:LX/03V;

    sget-object v1, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->c:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Marking sticker pack as closed failed"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1396945
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1396946
    iget-object v0, p0, LX/8kv;->b:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    const/4 v1, 0x0

    .line 1396947
    iput-object v1, v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->s:LX/1Mv;

    .line 1396948
    return-void
.end method
