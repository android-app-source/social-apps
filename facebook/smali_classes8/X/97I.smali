.class public final LX/97I;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 19

    .prologue
    .line 1441292
    const/4 v15, 0x0

    .line 1441293
    const/4 v14, 0x0

    .line 1441294
    const/4 v13, 0x0

    .line 1441295
    const/4 v12, 0x0

    .line 1441296
    const/4 v11, 0x0

    .line 1441297
    const/4 v10, 0x0

    .line 1441298
    const/4 v9, 0x0

    .line 1441299
    const/4 v8, 0x0

    .line 1441300
    const/4 v7, 0x0

    .line 1441301
    const/4 v6, 0x0

    .line 1441302
    const/4 v5, 0x0

    .line 1441303
    const/4 v4, 0x0

    .line 1441304
    const/4 v3, 0x0

    .line 1441305
    const/4 v2, 0x0

    .line 1441306
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_1

    .line 1441307
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1441308
    const/4 v2, 0x0

    .line 1441309
    :goto_0
    return v2

    .line 1441310
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1441311
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_e

    .line 1441312
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v16

    .line 1441313
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1441314
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v17

    sget-object v18, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_1

    if-eqz v16, :cond_1

    .line 1441315
    const-string v17, "id"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 1441316
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    goto :goto_1

    .line 1441317
    :cond_2
    const-string v17, "place_info"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 1441318
    invoke-static/range {p0 .. p1}, LX/97L;->a(LX/15w;LX/186;)I

    move-result v14

    goto :goto_1

    .line 1441319
    :cond_3
    const-string v17, "place_question_answers"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 1441320
    invoke-static/range {p0 .. p1}, LX/97C;->a(LX/15w;LX/186;)I

    move-result v13

    goto :goto_1

    .line 1441321
    :cond_4
    const-string v17, "place_question_checkbox_text"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 1441322
    invoke-static/range {p0 .. p1}, LX/4ap;->a(LX/15w;LX/186;)I

    move-result v12

    goto :goto_1

    .line 1441323
    :cond_5
    const-string v17, "place_question_details"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 1441324
    invoke-static/range {p0 .. p1}, LX/4ap;->a(LX/15w;LX/186;)I

    move-result v11

    goto :goto_1

    .line 1441325
    :cond_6
    const-string v17, "place_question_image"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 1441326
    invoke-static/range {p0 .. p1}, LX/97D;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 1441327
    :cond_7
    const-string v17, "place_question_orientation"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 1441328
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    goto/16 :goto_1

    .line 1441329
    :cond_8
    const-string v17, "place_question_place"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_9

    .line 1441330
    invoke-static/range {p0 .. p1}, LX/97H;->a(LX/15w;LX/186;)I

    move-result v8

    goto/16 :goto_1

    .line 1441331
    :cond_9
    const-string v17, "place_question_street_points"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_a

    .line 1441332
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto/16 :goto_1

    .line 1441333
    :cond_a
    const-string v17, "place_question_subtitle"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_b

    .line 1441334
    invoke-static/range {p0 .. p1}, LX/4ap;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 1441335
    :cond_b
    const-string v17, "place_question_timeout_ms"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_c

    .line 1441336
    const/4 v2, 0x1

    .line 1441337
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v5

    goto/16 :goto_1

    .line 1441338
    :cond_c
    const-string v17, "place_question_title"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_d

    .line 1441339
    invoke-static/range {p0 .. p1}, LX/4ap;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 1441340
    :cond_d
    const-string v17, "place_question_type"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_0

    .line 1441341
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    goto/16 :goto_1

    .line 1441342
    :cond_e
    const/16 v16, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1441343
    const/16 v16, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 1441344
    const/4 v15, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v14}, LX/186;->b(II)V

    .line 1441345
    const/4 v14, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 1441346
    const/4 v13, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 1441347
    const/4 v12, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 1441348
    const/4 v11, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 1441349
    const/4 v10, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 1441350
    const/4 v9, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 1441351
    const/16 v8, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v7}, LX/186;->b(II)V

    .line 1441352
    const/16 v7, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v6}, LX/186;->b(II)V

    .line 1441353
    if-eqz v2, :cond_f

    .line 1441354
    const/16 v2, 0xa

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5, v6}, LX/186;->a(III)V

    .line 1441355
    :cond_f
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1441356
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1441357
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1441423
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1441424
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1441425
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/97I;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1441426
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1441427
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1441428
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 1441417
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1441418
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1441419
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1441420
    invoke-static {p0, p1}, LX/97I;->a(LX/15w;LX/186;)I

    move-result v1

    .line 1441421
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1441422
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const/16 v4, 0xc

    const/4 v3, 0x6

    const/4 v2, 0x0

    .line 1441358
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1441359
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1441360
    if-eqz v0, :cond_0

    .line 1441361
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1441362
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1441363
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1441364
    if-eqz v0, :cond_1

    .line 1441365
    const-string v1, "place_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1441366
    invoke-static {p0, v0, p2, p3}, LX/97L;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1441367
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1441368
    if-eqz v0, :cond_3

    .line 1441369
    const-string v1, "place_question_answers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1441370
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1441371
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v5

    if-ge v1, v5, :cond_2

    .line 1441372
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v5

    invoke-static {p0, v5, p2, p3}, LX/97C;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1441373
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1441374
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1441375
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1441376
    if-eqz v0, :cond_4

    .line 1441377
    const-string v1, "place_question_checkbox_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1441378
    invoke-static {p0, v0, p2, p3}, LX/4ap;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1441379
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1441380
    if-eqz v0, :cond_5

    .line 1441381
    const-string v1, "place_question_details"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1441382
    invoke-static {p0, v0, p2, p3}, LX/4ap;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1441383
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1441384
    if-eqz v0, :cond_6

    .line 1441385
    const-string v1, "place_question_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1441386
    invoke-static {p0, v0, p2}, LX/97D;->a(LX/15i;ILX/0nX;)V

    .line 1441387
    :cond_6
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1441388
    if-eqz v0, :cond_7

    .line 1441389
    const-string v0, "place_question_orientation"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1441390
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1441391
    :cond_7
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1441392
    if-eqz v0, :cond_8

    .line 1441393
    const-string v1, "place_question_place"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1441394
    invoke-static {p0, v0, p2, p3}, LX/97H;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1441395
    :cond_8
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1441396
    if-eqz v0, :cond_9

    .line 1441397
    const-string v1, "place_question_street_points"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1441398
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1441399
    :cond_9
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1441400
    if-eqz v0, :cond_a

    .line 1441401
    const-string v1, "place_question_subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1441402
    invoke-static {p0, v0, p2, p3}, LX/4ap;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1441403
    :cond_a
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1441404
    if-eqz v0, :cond_b

    .line 1441405
    const-string v1, "place_question_timeout_ms"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1441406
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1441407
    :cond_b
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1441408
    if-eqz v0, :cond_c

    .line 1441409
    const-string v1, "place_question_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1441410
    invoke-static {p0, v0, p2, p3}, LX/4ap;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1441411
    :cond_c
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 1441412
    if-eqz v0, :cond_d

    .line 1441413
    const-string v0, "place_question_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1441414
    invoke-virtual {p0, p1, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1441415
    :cond_d
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1441416
    return-void
.end method
