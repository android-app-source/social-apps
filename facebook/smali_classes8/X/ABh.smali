.class public final LX/ABh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$FetchPageTargetingTypeaheadInterestQueryModel;",
        ">;",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/ABi;


# direct methods
.method public constructor <init>(LX/ABi;)V
    .locals 0

    .prologue
    .line 1641323
    iput-object p1, p0, LX/ABh;->a:LX/ABi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1641324
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1641325
    if-eqz p1, :cond_0

    .line 1641326
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1641327
    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_5

    .line 1641328
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1641329
    :goto_2
    return-object v0

    .line 1641330
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1641331
    check-cast v0, Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$FetchPageTargetingTypeaheadInterestQueryModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$FetchPageTargetingTypeaheadInterestQueryModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 1641332
    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    .line 1641333
    :cond_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1641334
    check-cast v0, Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$FetchPageTargetingTypeaheadInterestQueryModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$FetchPageTargetingTypeaheadInterestQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1641335
    invoke-virtual {v3, v0, v2}, LX/15i;->g(II)I

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_1

    .line 1641336
    :cond_5
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1641337
    check-cast v0, Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$FetchPageTargetingTypeaheadInterestQueryModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/FetchPageTargetingTypeaheadGraphQLModels$FetchPageTargetingTypeaheadInterestQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v0

    const-class v3, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;

    invoke-virtual {v1, v0, v2, v3}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_2

    .line 1641338
    :cond_6
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1641339
    goto :goto_2
.end method
