.class public final LX/AE2;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/2yS;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/AE1;

.field private b:[Ljava/lang/String;

.field private c:I

.field private d:Ljava/util/BitSet;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1645958
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1645959
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "clickListener"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/AE2;->b:[Ljava/lang/String;

    .line 1645960
    iput v3, p0, LX/AE2;->c:I

    .line 1645961
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/AE2;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/AE2;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/AE2;LX/1De;IILX/AE1;)V
    .locals 1

    .prologue
    .line 1645954
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1645955
    iput-object p4, p0, LX/AE2;->a:LX/AE1;

    .line 1645956
    iget-object v0, p0, LX/AE2;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1645957
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View$OnClickListener;)LX/AE2;
    .locals 2

    .prologue
    .line 1645951
    iget-object v0, p0, LX/AE2;->a:LX/AE1;

    iput-object p1, v0, LX/AE1;->k:Landroid/view/View$OnClickListener;

    .line 1645952
    iget-object v0, p0, LX/AE2;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1645953
    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;)LX/AE2;
    .locals 1

    .prologue
    .line 1645949
    iget-object v0, p0, LX/AE2;->a:LX/AE1;

    iput-object p1, v0, LX/AE1;->a:Ljava/lang/CharSequence;

    .line 1645950
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1645945
    invoke-super {p0}, LX/1X5;->a()V

    .line 1645946
    const/4 v0, 0x0

    iput-object v0, p0, LX/AE2;->a:LX/AE1;

    .line 1645947
    sget-object v0, LX/2yS;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1645948
    return-void
.end method

.method public final c(F)LX/AE2;
    .locals 2
    .param p1    # F
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param

    .prologue
    .line 1645943
    iget-object v0, p0, LX/AE2;->a:LX/AE1;

    invoke-virtual {p0, p1}, LX/1Dp;->a(F)I

    move-result v1

    iput v1, v0, LX/AE1;->i:I

    .line 1645944
    return-object p0
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/2yS;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1645933
    iget-object v1, p0, LX/AE2;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/AE2;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/AE2;->c:I

    if-ge v1, v2, :cond_2

    .line 1645934
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1645935
    :goto_0
    iget v2, p0, LX/AE2;->c:I

    if-ge v0, v2, :cond_1

    .line 1645936
    iget-object v2, p0, LX/AE2;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1645937
    iget-object v2, p0, LX/AE2;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1645938
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1645939
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1645940
    :cond_2
    iget-object v0, p0, LX/AE2;->a:LX/AE1;

    .line 1645941
    invoke-virtual {p0}, LX/AE2;->a()V

    .line 1645942
    return-object v0
.end method

.method public final h(I)LX/AE2;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 1645931
    iget-object v0, p0, LX/AE2;->a:LX/AE1;

    iput p1, v0, LX/AE1;->c:I

    .line 1645932
    return-object p0
.end method

.method public final j(I)LX/AE2;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 1645929
    iget-object v0, p0, LX/AE2;->a:LX/AE1;

    iput p1, v0, LX/AE1;->e:I

    .line 1645930
    return-object p0
.end method
