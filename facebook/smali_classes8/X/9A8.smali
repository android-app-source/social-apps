.class public LX/9A8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/99L;


# direct methods
.method public constructor <init>(LX/99L;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1448867
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1448868
    iput-object p1, p0, LX/9A8;->a:LX/99L;

    .line 1448869
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)LX/99K;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMedia;",
            ">;)",
            "LX/99K;"
        }
    .end annotation

    .prologue
    .line 1448870
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1448871
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1448872
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 1448873
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->bB()I

    move-result v4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->S()I

    move-result v0

    .line 1448874
    new-instance v5, LX/9A7;

    invoke-direct {v5, p0, v4, v0}, LX/9A7;-><init>(LX/9A8;II)V

    move-object v0, v5

    .line 1448875
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1448876
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1448877
    :cond_0
    iget-object v0, p0, LX/9A8;->a:LX/99L;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1448878
    const/4 v6, 0x0

    .line 1448879
    const/4 v4, 0x0

    .line 1448880
    iget-object v2, v0, LX/99L;->a:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 1448881
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v5

    if-gt v3, v5, :cond_1

    .line 1448882
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result p0

    const/4 v2, 0x0

    move v7, v2

    :goto_1
    if-ge v7, p0, :cond_1

    invoke-virtual {v3, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/99K;

    .line 1448883
    sget-object v5, LX/99G;->USER:LX/99G;

    .line 1448884
    iget-object p1, v2, LX/99K;->a:Ljava/util/EnumSet;

    invoke-virtual {p1, v5}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result p1

    move v5, p1

    .line 1448885
    if-eqz v5, :cond_4

    .line 1448886
    invoke-static {v2, v1}, LX/99L;->a(LX/99K;LX/0Px;)F

    move-result v5

    .line 1448887
    if-eqz v6, :cond_2

    cmpl-float p1, v5, v4

    if-lez p1, :cond_4

    :cond_2
    move-object v4, v2

    move v2, v5

    .line 1448888
    :goto_2
    add-int/lit8 v5, v7, 0x1

    move v7, v5

    move-object v6, v4

    move v4, v2

    goto :goto_1

    .line 1448889
    :cond_3
    move-object v0, v6

    .line 1448890
    return-object v0

    :cond_4
    move v2, v4

    move-object v4, v6

    goto :goto_2
.end method
