.class public final LX/91u;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/91v;


# direct methods
.method public constructor <init>(LX/91v;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1431544
    iput-object p1, p0, LX/91u;->c:LX/91v;

    iput-object p2, p0, LX/91u;->a:Ljava/lang/String;

    iput-object p3, p0, LX/91u;->b:Ljava/lang/String;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1431619
    iget-object v0, p0, LX/91u;->c:LX/91v;

    const/4 v1, 0x0

    .line 1431620
    iput-boolean v1, v0, LX/91v;->o:Z

    .line 1431621
    new-instance v0, LX/91t;

    invoke-direct {v0, p0}, LX/91t;-><init>(LX/91u;)V

    .line 1431622
    invoke-static {p1}, LX/3d6;->forException(Ljava/lang/Throwable;)LX/1nY;

    move-result-object v1

    sget-object v2, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    if-ne v1, v2, :cond_0

    .line 1431623
    iget-object v1, p0, LX/91u;->c:LX/91v;

    iget-object v1, v1, LX/91v;->l:LX/909;

    invoke-interface {v1, v0}, LX/909;->a(LX/8zc;)V

    .line 1431624
    :goto_0
    iget-object v0, p0, LX/91u;->c:LX/91v;

    iget-object v0, v0, LX/91v;->f:LX/91D;

    iget-object v1, p0, LX/91u;->c:LX/91v;

    iget-object v1, v1, LX/91v;->d:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1431625
    iget-object v2, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v1, v2

    .line 1431626
    iget-object v2, p0, LX/91u;->c:LX/91v;

    .line 1431627
    iget-object v3, v2, LX/91v;->g:LX/5LG;

    move-object v2, v3

    .line 1431628
    invoke-interface {v2}, LX/5LG;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, LX/91D;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1431629
    iget-object v0, p0, LX/91u;->c:LX/91v;

    iget-object v0, v0, LX/91v;->e:LX/92B;

    .line 1431630
    const v1, 0xc50003

    const-string v2, "minutiae_feelings_selector_time_to_fetch_end"

    invoke-virtual {v0, v1, v2}, LX/92A;->a(ILjava/lang/String;)V

    .line 1431631
    const v1, 0xc50004

    const-string v2, "minutiae_feelings_selector_time_to_results_shown"

    invoke-virtual {v0, v1, v2}, LX/92A;->a(ILjava/lang/String;)V

    .line 1431632
    const v1, 0xc50005

    const-string v2, "minutiae_feelings_selector_fetch_time"

    invoke-virtual {v0, v1, v2}, LX/92A;->a(ILjava/lang/String;)V

    .line 1431633
    return-void

    .line 1431634
    :cond_0
    iget-object v1, p0, LX/91u;->c:LX/91v;

    iget-object v1, v1, LX/91v;->l:LX/909;

    invoke-interface {v1, v0}, LX/909;->b(LX/8zc;)V

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 1431545
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1431546
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1431547
    check-cast v0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;

    .line 1431548
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;->o()Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1431549
    :goto_0
    return-void

    .line 1431550
    :cond_0
    iget-object v1, p0, LX/91u;->c:LX/91v;

    invoke-static {v1, v0}, LX/91v;->a$redex0(LX/91v;Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;)V

    .line 1431551
    iget-object v1, p0, LX/91u;->c:LX/91v;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;->o()Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;->o()Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v0

    iget-object v4, p0, LX/91u;->a:Ljava/lang/String;

    .line 1431552
    const/4 v6, 0x0

    .line 1431553
    iput-object v4, v1, LX/91v;->q:Ljava/lang/String;

    .line 1431554
    iput-object v0, v1, LX/91v;->h:LX/0ut;

    .line 1431555
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v8

    move v7, v6

    :goto_1
    if-ge v7, v8, :cond_6

    invoke-virtual {v2, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    .line 1431556
    invoke-virtual {v5}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->e()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v5}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->e()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->v_()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v5}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->e()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->v_()Ljava/lang/String;

    move-result-object v5

    iget-object v3, v1, LX/91v;->j:Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1431557
    const/4 v5, 0x1

    .line 1431558
    :goto_2
    iget-object v7, v1, LX/91v;->j:Ljava/lang/String;

    invoke-static {v7}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_4

    if-nez v5, :cond_4

    iget-object v5, v1, LX/91v;->g:LX/5LG;

    invoke-interface {v5}, LX/5LG;->q()Z

    move-result v5

    if-eqz v5, :cond_4

    iget-object v5, v1, LX/91v;->g:LX/5LG;

    invoke-interface {v5}, LX/5LG;->z()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;

    move-result-object v5

    if-eqz v5, :cond_4

    iget-object v5, v1, LX/91v;->g:LX/5LG;

    invoke-interface {v5}, LX/5LG;->A()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;

    move-result-object v5

    if-eqz v5, :cond_4

    .line 1431559
    new-instance v5, LX/5LL;

    invoke-direct {v5}, LX/5LL;-><init>()V

    iget-object v7, v1, LX/91v;->j:Ljava/lang/String;

    .line 1431560
    iput-object v7, v5, LX/5LL;->d:Ljava/lang/String;

    .line 1431561
    move-object v5, v5

    .line 1431562
    iput-boolean v6, v5, LX/5LL;->h:Z

    .line 1431563
    move-object v5, v5

    .line 1431564
    new-instance v7, LX/4aM;

    invoke-direct {v7}, LX/4aM;-><init>()V

    iget-object v8, v1, LX/91v;->g:LX/5LG;

    invoke-interface {v8}, LX/5LG;->z()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;->a()Ljava/lang/String;

    move-result-object v8

    .line 1431565
    iput-object v8, v7, LX/4aM;->b:Ljava/lang/String;

    .line 1431566
    move-object v7, v7

    .line 1431567
    invoke-virtual {v7}, LX/4aM;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v7

    .line 1431568
    iput-object v7, v5, LX/5LL;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1431569
    move-object v5, v5

    .line 1431570
    new-instance v7, LX/5Lc;

    invoke-direct {v7}, LX/5Lc;-><init>()V

    iget-object v8, v1, LX/91v;->j:Ljava/lang/String;

    .line 1431571
    iput-object v8, v7, LX/5Lc;->d:Ljava/lang/String;

    .line 1431572
    move-object v7, v7

    .line 1431573
    iput-boolean v6, v7, LX/5Lc;->c:Z

    .line 1431574
    move-object v7, v7

    .line 1431575
    new-instance v8, LX/4aM;

    invoke-direct {v8}, LX/4aM;-><init>()V

    iget-object v3, v1, LX/91v;->g:LX/5LG;

    invoke-interface {v3}, LX/5LG;->A()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 1431576
    iput-object v3, v8, LX/4aM;->b:Ljava/lang/String;

    .line 1431577
    move-object v8, v8

    .line 1431578
    invoke-virtual {v8}, LX/4aM;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v8

    .line 1431579
    iput-object v8, v7, LX/5Lc;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1431580
    move-object v7, v7

    .line 1431581
    invoke-virtual {v7}, LX/5Lc;->a()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v7

    .line 1431582
    iput-object v7, v5, LX/5LL;->g:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    .line 1431583
    move-object v5, v5

    .line 1431584
    invoke-virtual {v5}, LX/5LL;->a()Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    move-result-object v5

    .line 1431585
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    invoke-virtual {v7, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v5

    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    .line 1431586
    iget-object v7, v1, LX/91v;->a:LX/91k;

    .line 1431587
    iput-object v5, v7, LX/91k;->a:LX/0Px;

    .line 1431588
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v2

    const/4 v8, 0x0

    move v3, v8

    :goto_3
    if-ge v3, v2, :cond_1

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    .line 1431589
    iget-object v0, v7, LX/91k;->b:Ljava/util/Map;

    invoke-interface {v0, v8, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1431590
    add-int/lit8 v8, v3, 0x1

    move v3, v8

    goto :goto_3

    .line 1431591
    :cond_1
    iput v6, v7, LX/91k;->f:I

    .line 1431592
    invoke-virtual {v7}, LX/3mY;->bE_()V

    .line 1431593
    :goto_4
    iput-boolean v6, v1, LX/91v;->o:Z

    .line 1431594
    iget-boolean v5, v1, LX/91v;->n:Z

    if-eqz v5, :cond_2

    .line 1431595
    invoke-static {v1}, LX/91v;->g(LX/91v;)V

    .line 1431596
    :cond_2
    iget-object v5, v1, LX/91v;->f:LX/91D;

    iget-object v6, v1, LX/91v;->d:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1431597
    iget-object v7, v6, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v6, v7

    .line 1431598
    iget-object v7, v1, LX/91v;->g:LX/5LG;

    move-object v7, v7

    .line 1431599
    invoke-interface {v7}, LX/5LG;->l()Ljava/lang/String;

    move-result-object v7

    iget-object v8, v1, LX/91v;->c:LX/92X;

    .line 1431600
    iget v3, v8, LX/92X;->f:I

    move v8, v3

    .line 1431601
    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    .line 1431602
    const-string v3, "feeling_selector_initial_results_loaded"

    invoke-static {v3, v6}, LX/91D;->b(Ljava/lang/String;Ljava/lang/String;)LX/8yQ;

    move-result-object v3

    invoke-virtual {v3, v7}, LX/8yQ;->a(Ljava/lang/String;)LX/8yQ;

    move-result-object v3

    invoke-virtual {v3, v8}, LX/8yQ;->e(Ljava/lang/String;)LX/8yQ;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/8yQ;->f(Ljava/lang/String;)LX/8yQ;

    move-result-object v3

    .line 1431603
    iget-object v1, v3, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v3, v1

    .line 1431604
    iget-object v1, v5, LX/91D;->a:LX/0Zb;

    invoke-interface {v1, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1431605
    iget-object v0, p0, LX/91u;->c:LX/91v;

    iget-object v0, v0, LX/91v;->e:LX/92B;

    iget-object v1, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    .line 1431606
    const v2, 0xc50003

    const-string v3, "minutiae_feelings_selector_time_to_fetch_end"

    invoke-virtual {v0, v2, v3, v1}, LX/92A;->a(ILjava/lang/String;LX/0ta;)V

    .line 1431607
    const v2, 0xc50005

    const-string v3, "minutiae_feelings_selector_fetch_time"

    invoke-virtual {v0, v2, v3, v1}, LX/92A;->a(ILjava/lang/String;LX/0ta;)V

    .line 1431608
    goto/16 :goto_0

    .line 1431609
    :cond_3
    add-int/lit8 v5, v7, 0x1

    move v7, v5

    goto/16 :goto_1

    .line 1431610
    :cond_4
    iget-object v5, v1, LX/91v;->a:LX/91k;

    .line 1431611
    iget-object v7, v5, LX/91k;->b:Ljava/util/Map;

    invoke-interface {v7}, Ljava/util/Map;->clear()V

    .line 1431612
    iput-object v2, v5, LX/91k;->a:LX/0Px;

    .line 1431613
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v7, 0x0

    move v8, v7

    :goto_5
    if-ge v8, v3, :cond_5

    invoke-virtual {v2, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    .line 1431614
    iget-object v0, v5, LX/91k;->b:Ljava/util/Map;

    invoke-interface {v0, v7, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1431615
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto :goto_5

    .line 1431616
    :cond_5
    const/4 v7, -0x1

    iput v7, v5, LX/91k;->f:I

    .line 1431617
    invoke-virtual {v5}, LX/3mY;->bE_()V

    .line 1431618
    goto :goto_4

    :cond_6
    move v5, v6

    goto/16 :goto_2
.end method
