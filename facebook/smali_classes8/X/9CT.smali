.class public final LX/9CT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1L9;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1L9",
        "<",
        "Lcom/facebook/graphql/model/GraphQLComment;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public final synthetic b:LX/9Cd;


# direct methods
.method public constructor <init>(LX/9Cd;Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 0

    .prologue
    .line 1454130
    iput-object p1, p0, LX/9CT;->b:LX/9Cd;

    iput-object p2, p0, LX/9CT;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1454131
    check-cast p1, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1454132
    iget-object v0, p0, LX/9CT;->b:LX/9Cd;

    iget-object v0, v0, LX/9Cd;->b:LX/1K9;

    new-instance v1, LX/8q4;

    iget-object v2, p0, LX/9CT;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p1, v2}, LX/8q4;-><init>(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/1K9;->a(LX/1KJ;)V

    .line 1454133
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->s_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1454134
    iget-object v0, p0, LX/9CT;->b:LX/9Cd;

    iget-object v0, v0, LX/9Cd;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3RX;

    const-string v1, "like_comment"

    invoke-virtual {v0, v1}, LX/3RX;->a(Ljava/lang/String;)V

    .line 1454135
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Object;Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 1454136
    check-cast p1, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1454137
    iget-object v0, p0, LX/9CT;->b:LX/9Cd;

    iget-object v0, v0, LX/9Cd;->b:LX/1K9;

    new-instance v1, LX/8q4;

    iget-object v2, p0, LX/9CT;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p1, v2}, LX/8q4;-><init>(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/1K9;->a(LX/1KJ;)V

    .line 1454138
    iget-object v0, p0, LX/9CT;->b:LX/9Cd;

    iget-object v0, v0, LX/9Cd;->d:LX/3iL;

    invoke-virtual {v0, p2}, LX/3iL;->a(Lcom/facebook/fbservice/service/ServiceException;)V

    .line 1454139
    return-void
.end method

.method public final bridge synthetic b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1454140
    return-void
.end method

.method public final bridge synthetic c(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1454141
    return-void
.end method
