.class public final LX/8lS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/stickers/search/StickerSearchContainer;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/search/StickerSearchContainer;)V
    .locals 0

    .prologue
    .line 1397798
    iput-object p1, p0, LX/8lS;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 1397797
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1397796
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 1397793
    iget-object v0, p0, LX/8lS;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    iget-boolean v0, v0, Lcom/facebook/stickers/search/StickerSearchContainer;->U:Z

    if-nez v0, :cond_0

    .line 1397794
    iget-object v0, p0, LX/8lS;->a:Lcom/facebook/stickers/search/StickerSearchContainer;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/stickers/search/StickerSearchContainer;->setQuery(Lcom/facebook/stickers/search/StickerSearchContainer;Ljava/lang/String;)V

    .line 1397795
    :cond_0
    return-void
.end method
