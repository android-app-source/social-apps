.class public final LX/AKo;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source ""


# instance fields
.field public final synthetic a:LX/AKu;


# direct methods
.method public constructor <init>(LX/AKu;)V
    .locals 0

    .prologue
    .line 1664315
    iput-object p1, p0, LX/AKo;->a:LX/AKu;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 2

    .prologue
    .line 1664316
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x437a0000    # 250.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 1664317
    const/4 v0, 0x0

    .line 1664318
    :goto_0
    return v0

    .line 1664319
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    sub-float/2addr v0, v1

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x43480000    # 200.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 1664320
    iget-object v0, p0, LX/AKo;->a:LX/AKu;

    invoke-static {v0}, LX/AKu;->b$redex0(LX/AKu;)V

    .line 1664321
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 1664322
    :cond_1
    iget-object v0, p0, LX/AKo;->a:LX/AKu;

    invoke-static {v0}, LX/AKu;->d$redex0(LX/AKu;)V

    goto :goto_1
.end method
