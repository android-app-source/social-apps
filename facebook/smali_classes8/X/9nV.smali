.class public LX/9nV;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Landroid/util/TypedValue;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1537361
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    sput-object v0, LX/9nV;->a:Landroid/util/TypedValue;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1537362
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;LX/5pG;)Landroid/graphics/drawable/Drawable;
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/16 v4, 0x15

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1537363
    const-string v0, "type"

    invoke-interface {p1, v0}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1537364
    const-string v1, "ThemeAttrAndroid"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1537365
    const-string v0, "attribute"

    invoke-interface {p1, v0}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1537366
    invoke-static {v0}, LX/5pd;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1537367
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "attr"

    const-string v3, "android"

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 1537368
    if-nez v1, :cond_0

    .line 1537369
    new-instance v1, LX/5pA;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Attribute "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " couldn\'t be found in the resource list"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1537370
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    sget-object v3, LX/9nV;->a:Landroid/util/TypedValue;

    invoke-virtual {v2, v1, v3, v6}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1537371
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 1537372
    if-lt v0, v4, :cond_1

    .line 1537373
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget-object v1, LX/9nV;->a:Landroid/util/TypedValue;

    iget v1, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getDrawable(ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1537374
    :goto_0
    return-object v0

    .line 1537375
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget-object v1, LX/9nV;->a:Landroid/util/TypedValue;

    iget v1, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 1537376
    :cond_2
    new-instance v1, LX/5pA;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Attribute "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " couldn\'t be resolved into a drawable"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1537377
    :cond_3
    const-string v1, "RippleAndroid"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1537378
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v4, :cond_4

    .line 1537379
    new-instance v0, LX/5pA;

    const-string v1, "Ripple drawable is not available on android API <21"

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1537380
    :cond_4
    const-string v0, "color"

    invoke-interface {p1, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "color"

    invoke-interface {p1, v0}, LX/5pG;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 1537381
    const-string v0, "color"

    invoke-interface {p1, v0}, LX/5pG;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 1537382
    :goto_1
    const-string v1, "borderless"

    invoke-interface {p1, v1}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "borderless"

    invoke-interface {p1, v1}, LX/5pG;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "borderless"

    invoke-interface {p1, v1}, LX/5pG;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 1537383
    :cond_5
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const/4 v3, -0x1

    invoke-direct {v1, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 1537384
    :goto_2
    new-instance v3, Landroid/content/res/ColorStateList;

    new-array v4, v6, [[I

    new-array v5, v7, [I

    aput-object v5, v4, v7

    new-array v5, v6, [I

    aput v0, v5, v7

    invoke-direct {v3, v4, v5}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    .line 1537385
    new-instance v0, Landroid/graphics/drawable/RippleDrawable;

    invoke-direct {v0, v3, v2, v1}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 1537386
    :cond_6
    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const v1, 0x101042c

    sget-object v3, LX/9nV;->a:Landroid/util/TypedValue;

    invoke-virtual {v0, v1, v3, v6}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1537387
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget-object v1, LX/9nV;->a:Landroid/util/TypedValue;

    iget v1, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_1

    .line 1537388
    :cond_7
    new-instance v0, LX/5pA;

    const-string v1, "Attribute colorControlHighlight couldn\'t be resolved into a drawable"

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1537389
    :cond_8
    new-instance v1, LX/5pA;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid type for android drawable: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_9
    move-object v1, v2

    goto :goto_2
.end method
