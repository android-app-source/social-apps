.class public final LX/AMa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/compactdisk/WriteCallback;


# instance fields
.field public final synthetic a:Ljava/io/InputStream;

.field public final synthetic b:LX/AMe;

.field public final synthetic c:LX/AMf;


# direct methods
.method public constructor <init>(LX/AMf;Ljava/io/InputStream;LX/AMe;)V
    .locals 0

    .prologue
    .line 1666319
    iput-object p1, p0, LX/AMa;->c:LX/AMf;

    iput-object p2, p0, LX/AMa;->a:Ljava/io/InputStream;

    iput-object p3, p0, LX/AMa;->b:LX/AMe;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final write(Ljava/io/OutputStream;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1666320
    const/16 v0, 0x1000

    new-array v2, v0, [B

    move v0, v1

    .line 1666321
    :goto_0
    iget-object v3, p0, LX/AMa;->a:Ljava/io/InputStream;

    invoke-virtual {v3, v2}, Ljava/io/InputStream;->read([B)I

    move-result v3

    .line 1666322
    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 1666323
    invoke-virtual {p1, v2, v1, v3}, Ljava/io/OutputStream;->write([BII)V

    .line 1666324
    add-int/2addr v0, v3

    .line 1666325
    iget-object v3, p0, LX/AMa;->b:LX/AMe;

    int-to-long v4, v0

    invoke-interface {v3, v4, v5}, LX/AMe;->a(J)V

    goto :goto_0

    .line 1666326
    :cond_0
    return-void
.end method
