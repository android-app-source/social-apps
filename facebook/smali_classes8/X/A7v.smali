.class public LX/A7v;
.super LX/398;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;

.field private static volatile d:LX/A7v;


# instance fields
.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1626850
    const-string v0, "universal_feedback/?"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/A7v;->a:Ljava/lang/String;

    .line 1626851
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/A7v;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "feedback_id=%s"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/A7v;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Or;)V
    .locals 3
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/universalfeedback/annotations/IsInDebugUriGatekeeper;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1626843
    invoke-direct {p0}, LX/398;-><init>()V

    .line 1626844
    iput-object p1, p0, LX/A7v;->c:LX/0Or;

    .line 1626845
    sget-object v0, LX/A7v;->b:Ljava/lang/String;

    const-string v1, "args_feedback_id"

    const-string v2, "UNKNOWN"

    .line 1626846
    const-string p1, "{%s %s}"

    invoke-static {p1, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    move-object v1, p1

    .line 1626847
    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1626848
    const-class v1, Lcom/facebook/universalfeedback/debug/DebugUniversalFeedbackActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 1626849
    return-void
.end method

.method public static a(LX/0QB;)LX/A7v;
    .locals 4

    .prologue
    .line 1626830
    sget-object v0, LX/A7v;->d:LX/A7v;

    if-nez v0, :cond_1

    .line 1626831
    const-class v1, LX/A7v;

    monitor-enter v1

    .line 1626832
    :try_start_0
    sget-object v0, LX/A7v;->d:LX/A7v;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1626833
    if-eqz v2, :cond_0

    .line 1626834
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1626835
    new-instance v3, LX/A7v;

    const/16 p0, 0x158e

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, p0}, LX/A7v;-><init>(LX/0Or;)V

    .line 1626836
    move-object v0, v3

    .line 1626837
    sput-object v0, LX/A7v;->d:LX/A7v;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1626838
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1626839
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1626840
    :cond_1
    sget-object v0, LX/A7v;->d:LX/A7v;

    return-object v0

    .line 1626841
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1626842
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1626829
    iget-object v0, p0, LX/A7v;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
