.class public final LX/8r5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/model/GraphQLComment;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/3iU;


# direct methods
.method public constructor <init>(LX/3iU;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1408432
    iput-object p1, p0, LX/8r5;->b:LX/3iU;

    iput-object p2, p0, LX/8r5;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1408433
    iget-object v0, p0, LX/8r5;->b:LX/3iU;

    iget-object v0, v0, LX/3iU;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x39001d

    iget-object v2, p0, LX/8r5;->a:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    const/4 v3, 0x3

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 1408434
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1408435
    check-cast p1, Lcom/facebook/graphql/model/GraphQLComment;

    const v3, 0x39001d

    .line 1408436
    if-nez p1, :cond_0

    .line 1408437
    iget-object v0, p0, LX/8r5;->b:LX/3iU;

    iget-object v0, v0, LX/3iU;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v1, p0, LX/8r5;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/16 v2, 0xa0

    invoke-interface {v0, v3, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 1408438
    :goto_0
    return-void

    .line 1408439
    :cond_0
    iget-object v0, p0, LX/8r5;->b:LX/3iU;

    iget-object v0, v0, LX/3iU;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v1, p0, LX/8r5;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v2, 0x2

    invoke-interface {v0, v3, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    goto :goto_0
.end method
