.class public final LX/8lQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Mb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3Mb",
        "<",
        "LX/8jW;",
        "LX/8jX;",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/ArrayList;

.field public final synthetic b:LX/8lZ;

.field public final synthetic c:Lcom/facebook/stickers/search/StickerSearchContainer;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/search/StickerSearchContainer;Ljava/util/ArrayList;LX/8lZ;)V
    .locals 0

    .prologue
    .line 1397775
    iput-object p1, p0, LX/8lQ;->c:Lcom/facebook/stickers/search/StickerSearchContainer;

    iput-object p2, p0, LX/8lQ;->a:Ljava/util/ArrayList;

    iput-object p3, p0, LX/8lQ;->b:LX/8lZ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 0

    .prologue
    .line 1397776
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1397777
    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1397778
    check-cast p2, LX/8jX;

    .line 1397779
    iget-object v1, p2, LX/8jX;->a:Ljava/util/List;

    iget-object v2, p0, LX/8lQ;->a:Ljava/util/ArrayList;

    .line 1397780
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v4

    .line 1397781
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/stickers/model/Sticker;

    .line 1397782
    iget-object p2, v3, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-virtual {v4, p2, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_0

    .line 1397783
    :cond_0
    invoke-virtual {v4}, LX/0P2;->b()LX/0P1;

    move-result-object v3

    move-object v4, v3

    .line 1397784
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object p1

    .line 1397785
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/stickers/model/Sticker;

    .line 1397786
    iget-object v0, v3, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-interface {v4, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v3, v3, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/stickers/model/Sticker;

    :cond_1
    invoke-virtual {p1, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1397787
    :cond_2
    invoke-virtual {p1}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    move-object v3, v3

    .line 1397788
    move-object v0, v3

    .line 1397789
    iget-object v1, p0, LX/8lQ;->b:LX/8lZ;

    invoke-interface {v1, v0}, LX/8lZ;->a(LX/0Px;)V

    .line 1397790
    return-void
.end method

.method public final bridge synthetic c(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1397791
    return-void
.end method
