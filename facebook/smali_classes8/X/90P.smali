.class public final LX/90P;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/90B;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;)V
    .locals 0

    .prologue
    .line 1428638
    iput-object p1, p0, LX/90P;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;B)V
    .locals 0

    .prologue
    .line 1428639
    invoke-direct {p0, p1}, LX/90P;-><init>(Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;)V
    .locals 14

    .prologue
    .line 1428640
    iget-object v0, p0, LX/90P;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->u:LX/8zZ;

    .line 1428641
    iget-object v1, v0, LX/8zZ;->e:LX/0Px;

    invoke-virtual {v1, p1}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v1

    move v7, v1

    .line 1428642
    iget-object v0, p0, LX/90P;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->H:LX/918;

    iget-object v1, p0, LX/90P;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;

    iget-object v1, v1, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->p:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1428643
    iget-object v2, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->k:Ljava/lang/String;

    move-object v1, v2

    .line 1428644
    iget-object v2, p0, LX/90P;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;

    invoke-static {v2}, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->q(Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->e()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->e()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/90P;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;

    iget-object v4, v4, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->u:LX/8zZ;

    invoke-virtual {v4}, LX/8zZ;->l()I

    move-result v4

    iget-object v5, p0, LX/90P;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;

    iget-object v5, v5, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->u:LX/8zZ;

    invoke-virtual {v5}, LX/8zZ;->n()LX/0Px;

    move-result-object v5

    iget-object v6, p0, LX/90P;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;

    iget-object v6, v6, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->r:LX/90G;

    .line 1428645
    iget v8, v6, LX/90G;->a:I

    move v6, v8

    .line 1428646
    iget-object v8, p0, LX/90P;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;

    iget-object v8, v8, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->v:LX/8zh;

    invoke-virtual {v8}, LX/8zh;->b()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, LX/90P;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;

    iget v9, v9, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->B:I

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, LX/90P;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;

    iget-object v10, v10, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->u:LX/8zZ;

    .line 1428647
    iget-object v11, v10, LX/8zZ;->j:LX/5Lv;

    invoke-virtual {v11, p1}, LX/5Lv;->b(Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;)Ljava/lang/String;

    move-result-object v11

    move-object v10, v11

    .line 1428648
    invoke-virtual {p1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->ag_()Ljava/lang/String;

    move-result-object v11

    .line 1428649
    const-string v12, "activities_selector_object_selected"

    invoke-static {v12, v1}, LX/918;->d(Ljava/lang/String;Ljava/lang/String;)LX/8yQ;

    move-result-object v12

    invoke-virtual {v12, v2}, LX/8yQ;->a(Ljava/lang/String;)LX/8yQ;

    move-result-object v12

    invoke-virtual {v12, v3}, LX/8yQ;->b(Ljava/lang/String;)LX/8yQ;

    move-result-object v12

    invoke-virtual {v12, v4}, LX/8yQ;->a(I)LX/8yQ;

    move-result-object v12

    invoke-virtual {v12, v5}, LX/8yQ;->a(LX/0Px;)LX/8yQ;

    move-result-object v12

    invoke-virtual {v12, v6}, LX/8yQ;->b(I)LX/8yQ;

    move-result-object v12

    invoke-virtual {v12, v7}, LX/8yQ;->c(I)LX/8yQ;

    move-result-object v12

    invoke-virtual {v12, v8}, LX/8yQ;->d(Ljava/lang/String;)LX/8yQ;

    move-result-object v12

    invoke-virtual {v12, v9}, LX/8yQ;->e(Ljava/lang/String;)LX/8yQ;

    move-result-object v12

    invoke-virtual {v12, v10}, LX/8yQ;->f(Ljava/lang/String;)LX/8yQ;

    move-result-object v12

    invoke-virtual {v12, v11}, LX/8yQ;->g(Ljava/lang/String;)LX/8yQ;

    move-result-object v12

    .line 1428650
    iget-object v13, v12, LX/8yQ;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v12, v13

    .line 1428651
    iget-object v13, v0, LX/918;->a:LX/0Zb;

    invoke-interface {v13, v12}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1428652
    iget-object v0, p0, LX/90P;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->v:LX/8zh;

    .line 1428653
    iget-object v1, v0, LX/8zh;->g:LX/91I;

    invoke-virtual {v1, p1, v7}, LX/91I;->a(Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;I)LX/91I;

    .line 1428654
    invoke-virtual {v0}, LX/8zh;->d()V

    .line 1428655
    iget-object v0, p0, LX/90P;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->v:LX/8zh;

    invoke-virtual {v0, p1}, LX/8zh;->a(Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;)Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v0

    .line 1428656
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1428657
    const-string v2, "minutiae_object"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1428658
    iget-object v0, p0, LX/90P;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;

    iget-object v0, v0, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->p:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1428659
    iget-object v2, v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v0, v2

    .line 1428660
    if-eqz v0, :cond_0

    .line 1428661
    const-string v2, "extra_place"

    invoke-static {v1, v2, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1428662
    :cond_0
    iget-object v0, p0, LX/90P;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->setResult(ILandroid/content/Intent;)V

    .line 1428663
    iget-object v0, p0, LX/90P;->a:Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/activity/MinutiaeObjectSelectorActivity;->finish()V

    .line 1428664
    return-void
.end method
