.class public final LX/ASj;
.super LX/1cC;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;)V
    .locals 0

    .prologue
    .line 1674408
    iput-object p1, p0, LX/ASj;->a:Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;

    invoke-direct {p0}, LX/1cC;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V
    .locals 3
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/drawable/Animatable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1674409
    check-cast p2, LX/1ln;

    .line 1674410
    iget-object v0, p0, LX/ASj;->a:Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;->k:LX/ASh;

    sget-object v1, LX/6Wv;->DONE_LOADING:LX/6Wv;

    invoke-virtual {v0, v1}, LX/ASh;->setPlayButtonState(LX/6Wv;)V

    .line 1674411
    if-nez p2, :cond_1

    .line 1674412
    :cond_0
    :goto_0
    return-void

    .line 1674413
    :cond_1
    if-eqz p3, :cond_2

    .line 1674414
    invoke-interface {p3}, Landroid/graphics/drawable/Animatable;->start()V

    .line 1674415
    :cond_2
    invoke-virtual {p2}, LX/1ln;->h()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1674416
    iget-object v0, p0, LX/ASj;->a:Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;->k:LX/ASh;

    invoke-virtual {p2}, LX/1ln;->g()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x3f800000    # 1.0f

    mul-float/2addr v1, v2

    invoke-virtual {p2}, LX/1ln;->h()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-virtual {v0, v1}, LX/ASh;->setAspectRatio(F)V

    goto :goto_0
.end method
