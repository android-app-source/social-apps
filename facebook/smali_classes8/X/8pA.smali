.class public final LX/8pA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1E2;


# direct methods
.method public constructor <init>(LX/1E2;)V
    .locals 0

    .prologue
    .line 1405526
    iput-object p1, p0, LX/8pA;->a:LX/1E2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1405527
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1405528
    check-cast p1, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 1405529
    iget-object v0, p0, LX/8pA;->a:LX/1E2;

    invoke-virtual {v0}, LX/1E2;->b()V

    .line 1405530
    iget-object v0, p0, LX/8pA;->a:LX/1E2;

    iget-object v0, v0, LX/1E2;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1405531
    :try_start_0
    iget-object v0, p0, LX/8pA;->a:LX/1E2;

    iget-object v0, v0, LX/1E2;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 1405532
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1405533
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1EI;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;->b()Ljava/lang/String;

    :cond_1
    invoke-interface {v0}, LX/1EI;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1405534
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/8pA;->a:LX/1E2;

    iget-object v1, v1, LX/1E2;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_2
    iget-object v0, p0, LX/8pA;->a:LX/1E2;

    iget-object v0, v0, LX/1E2;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1405535
    return-void
.end method
