.class public LX/8zy;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/91P;

.field public final b:LX/91K;


# direct methods
.method public constructor <init>(LX/91P;LX/91K;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1428347
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1428348
    iput-object p1, p0, LX/8zy;->a:LX/91P;

    .line 1428349
    iput-object p2, p0, LX/8zy;->b:LX/91K;

    .line 1428350
    return-void
.end method

.method public static a(LX/0QB;)LX/8zy;
    .locals 5

    .prologue
    .line 1428351
    const-class v1, LX/8zy;

    monitor-enter v1

    .line 1428352
    :try_start_0
    sget-object v0, LX/8zy;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1428353
    sput-object v2, LX/8zy;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1428354
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1428355
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1428356
    new-instance p0, LX/8zy;

    invoke-static {v0}, LX/91P;->a(LX/0QB;)LX/91P;

    move-result-object v3

    check-cast v3, LX/91P;

    invoke-static {v0}, LX/91K;->a(LX/0QB;)LX/91K;

    move-result-object v4

    check-cast v4, LX/91K;

    invoke-direct {p0, v3, v4}, LX/8zy;-><init>(LX/91P;LX/91K;)V

    .line 1428357
    move-object v0, p0

    .line 1428358
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1428359
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8zy;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1428360
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1428361
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
