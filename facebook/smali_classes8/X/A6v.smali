.class public LX/A6v;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "LX/A6v;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field private c:D


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Double;)V
    .locals 2

    .prologue
    .line 1625151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1625152
    iput-object p1, p0, LX/A6v;->a:Ljava/lang/String;

    .line 1625153
    iput-object p2, p0, LX/A6v;->b:Ljava/lang/String;

    .line 1625154
    invoke-virtual {p3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    iput-wide v0, p0, LX/A6v;->c:D

    .line 1625155
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Double;
    .locals 2

    .prologue
    .line 1625156
    iget-wide v0, p0, LX/A6v;->c:D

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method public final a(D)V
    .locals 5

    .prologue
    .line 1625157
    iget-wide v0, p0, LX/A6v;->c:D

    .line 1625158
    cmpl-double v2, v0, p1

    if-lez v2, :cond_0

    .line 1625159
    sub-double v2, p1, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->exp(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->log1p(D)D

    move-result-wide v2

    add-double/2addr v0, v2

    iput-wide v0, p0, LX/A6v;->c:D

    .line 1625160
    :goto_0
    return-void

    .line 1625161
    :cond_0
    sub-double/2addr v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->log1p(D)D

    move-result-wide v0

    add-double/2addr v0, p1

    iput-wide v0, p0, LX/A6v;->c:D

    goto :goto_0
.end method

.method public final compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 1625162
    check-cast p1, LX/A6v;

    .line 1625163
    invoke-virtual {p0}, LX/A6v;->a()Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {p1}, LX/A6v;->a()Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Double;->compareTo(Ljava/lang/Double;)I

    move-result v0

    mul-int/lit8 v0, v0, -0x1

    return v0
.end method
