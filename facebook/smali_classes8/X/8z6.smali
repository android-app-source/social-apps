.class public final LX/8z6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

.field public b:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

.field public c:Ljava/lang/String;

.field private d:LX/0Pz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Pz",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public e:I

.field public f:Z

.field public g:Z

.field public h:LX/8z9;

.field public i:Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1426942
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1426943
    iput-boolean v0, p0, LX/8z6;->f:Z

    .line 1426944
    iput-boolean v0, p0, LX/8z6;->g:Z

    return-void
.end method


# virtual methods
.method public final a()LX/8z5;
    .locals 11

    .prologue
    .line 1426939
    iget-object v0, p0, LX/8z6;->d:LX/0Pz;

    if-nez v0, :cond_0

    .line 1426940
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    iput-object v0, p0, LX/8z6;->d:LX/0Pz;

    .line 1426941
    :cond_0
    new-instance v0, LX/8z5;

    iget-object v1, p0, LX/8z6;->a:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    iget-object v2, p0, LX/8z6;->b:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    iget-object v3, p0, LX/8z6;->c:Ljava/lang/String;

    iget-object v4, p0, LX/8z6;->d:LX/0Pz;

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    iget v5, p0, LX/8z6;->e:I

    iget-boolean v6, p0, LX/8z6;->f:Z

    iget-boolean v7, p0, LX/8z6;->g:Z

    iget-object v8, p0, LX/8z6;->h:LX/8z9;

    iget-object v9, p0, LX/8z6;->i:Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    const/4 v10, 0x0

    invoke-direct/range {v0 .. v10}, LX/8z5;-><init>(Lcom/facebook/composer/minutiae/model/MinutiaeObject;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Ljava/lang/String;LX/0Px;IZZLX/8z9;Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;B)V

    return-object v0
.end method

.method public final a(I)LX/8z6;
    .locals 0

    .prologue
    .line 1426937
    iput p1, p0, LX/8z6;->e:I

    .line 1426938
    return-object p0
.end method

.method public final a(LX/0Px;)LX/8z6;
    .locals 1
    .param p1    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/8z6;"
        }
    .end annotation

    .prologue
    .line 1426933
    if-eqz p1, :cond_0

    .line 1426934
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    iput-object v0, p0, LX/8z6;->d:LX/0Pz;

    .line 1426935
    iget-object v0, p0, LX/8z6;->d:LX/0Pz;

    invoke-virtual {v0, p1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1426936
    :cond_0
    return-object p0
.end method

.method public final a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)LX/8z6;
    .locals 0

    .prologue
    .line 1426945
    iput-object p1, p0, LX/8z6;->a:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1426946
    return-object p0
.end method

.method public final a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)LX/8z6;
    .locals 0

    .prologue
    .line 1426931
    iput-object p1, p0, LX/8z6;->b:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1426932
    return-object p0
.end method

.method public final a(Z)LX/8z6;
    .locals 0

    .prologue
    .line 1426923
    iput-boolean p1, p0, LX/8z6;->f:Z

    .line 1426924
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/8z6;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1426927
    if-eqz p1, :cond_0

    .line 1426928
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    iput-object v0, p0, LX/8z6;->d:LX/0Pz;

    .line 1426929
    iget-object v0, p0, LX/8z6;->d:LX/0Pz;

    invoke-virtual {v0, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1426930
    :cond_0
    return-object p0
.end method

.method public final b(Z)LX/8z6;
    .locals 0

    .prologue
    .line 1426925
    iput-boolean p1, p0, LX/8z6;->g:Z

    .line 1426926
    return-object p0
.end method
