.class public LX/APt;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/APs;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1670561
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1670562
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/HqS;)LX/APs;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData:",
            "Ljava/lang/Object;",
            ":",
            "LX/0it;",
            ":",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesPrivacyOverride;",
            ":",
            "LX/0j1;",
            ":",
            "LX/0j3;",
            ":",
            "LX/0j4;",
            ":",
            "LX/0iq;",
            ":",
            "LX/0j6;",
            "DerivedData::",
            "LX/5Qu;",
            "PluginData::",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginGetters$ProvidesPluginPrivacyDelegateGetter;",
            "Services:",
            "Ljava/lang/Object;",
            ":",
            "LX/0il",
            "<TModelData;>;:",
            "LX/0ik",
            "<TDerivedData;>;:",
            "LX/0in",
            "<TPluginData;>;>(TServices;",
            "Lcom/facebook/composer/controller/ComposerPrivacyController$ComposerPrivacyCallback;",
            ")",
            "LX/APs",
            "<TModelData;TDerivedData;TPluginData;TServices;>;"
        }
    .end annotation

    .prologue
    .line 1670563
    new-instance v0, LX/APs;

    move-object/from16 v1, p1

    check-cast v1, LX/0il;

    const-class v2, LX/93S;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/93S;

    const-class v2, LX/93n;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/93n;

    const-class v2, LX/93y;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/93y;

    const-class v2, LX/93d;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/93d;

    const-class v2, LX/93j;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/93j;

    const-class v2, LX/941;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/941;

    const-class v2, LX/93p;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/93p;

    const-class v2, LX/93l;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/93l;

    const-class v2, LX/93Z;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/93Z;

    const-class v2, LX/93f;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/93f;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v13

    check-cast v13, Landroid/content/res/Resources;

    invoke-static {p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v14

    check-cast v14, Ljava/lang/Boolean;

    move-object/from16 v2, p2

    invoke-direct/range {v0 .. v14}, LX/APs;-><init>(LX/0il;LX/HqS;LX/93S;LX/93n;LX/93y;LX/93d;LX/93j;LX/941;LX/93p;LX/93l;LX/93Z;LX/93f;Landroid/content/res/Resources;Ljava/lang/Boolean;)V

    .line 1670564
    return-object v0
.end method
