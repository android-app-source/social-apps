.class public LX/8vT;
.super LX/8vS;
.source ""


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1417087
    invoke-direct {p0, p1}, LX/8vS;-><init>(Landroid/view/View;)V

    .line 1417088
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 2

    .prologue
    .line 1417084
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setScrollX: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1417085
    iget-object v0, p0, LX/8vS;->a:Landroid/view/View;

    iget-object v1, p0, LX/8vS;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getScrollY()I

    move-result v1

    invoke-virtual {v0, p1, v1}, Landroid/view/View;->scrollTo(II)V

    .line 1417086
    return-void
.end method

.method public a(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 1417081
    iget-object v0, p0, LX/8vS;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 1417082
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 1417083
    const/4 v0, 0x0

    return v0
.end method
