.class public LX/8zW;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/8zU;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8zX;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1427597
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/8zW;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/8zX;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1427520
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1427521
    iput-object p1, p0, LX/8zW;->b:LX/0Ot;

    .line 1427522
    return-void
.end method

.method public static a(LX/0QB;)LX/8zW;
    .locals 4

    .prologue
    .line 1427586
    const-class v1, LX/8zW;

    monitor-enter v1

    .line 1427587
    :try_start_0
    sget-object v0, LX/8zW;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1427588
    sput-object v2, LX/8zW;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1427589
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1427590
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1427591
    new-instance v3, LX/8zW;

    const/16 p0, 0x1993

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/8zW;-><init>(LX/0Ot;)V

    .line 1427592
    move-object v0, v3

    .line 1427593
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1427594
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8zW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1427595
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1427596
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;)LX/1dQ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            "Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLInterfaces$TaggableObjectEdge;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1427585
    const v0, -0x4e4b4a3c

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 1427544
    check-cast p2, LX/8zV;

    .line 1427545
    iget-object v0, p0, LX/8zW;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8zX;

    iget-object v1, p2, LX/8zV;->a:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    iget-object v2, p2, LX/8zV;->b:LX/5LG;

    iget-boolean v3, p2, LX/8zV;->c:Z

    const/4 p2, 0x2

    const/4 p0, 0x0

    .line 1427546
    invoke-virtual {v1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->c()Ljava/lang/String;

    move-result-object v5

    .line 1427547
    invoke-virtual {v1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->e()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->e()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->c()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1427548
    new-instance v4, Landroid/text/SpannableStringBuilder;

    invoke-direct {v4, v5}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1427549
    const-string v5, " "

    invoke-virtual {v4, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1427550
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0050

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    .line 1427551
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b1899

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    .line 1427552
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f021a25

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 1427553
    if-eqz v7, :cond_0

    .line 1427554
    add-int v8, v6, v5

    invoke-virtual {v7, v6, p0, v8, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1427555
    :cond_0
    new-instance v5, LX/34T;

    invoke-direct {v5, v7, p2}, LX/34T;-><init>(Landroid/graphics/drawable/Drawable;I)V

    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    const/16 v8, 0x21

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1427556
    :goto_0
    invoke-static {v0, v2, v1, v3, p1}, LX/8zX;->a(LX/8zX;LX/5LG;Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;ZLX/1De;)V

    .line 1427557
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, p2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v6

    iget-object v7, v0, LX/8zX;->a:LX/91M;

    invoke-virtual {v7, p1}, LX/91M;->c(LX/1De;)LX/91L;

    move-result-object v7

    iget-object v8, v0, LX/8zX;->e:Landroid/net/Uri;

    invoke-virtual {v7, v8}, LX/91L;->a(Landroid/net/Uri;)LX/91L;

    move-result-object v7

    iget v8, v0, LX/8zX;->b:F

    float-to-int v8, v8

    .line 1427558
    iget-object p2, v7, LX/91L;->a:Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;

    iput v8, p2, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->c:I

    .line 1427559
    move-object v7, v7

    .line 1427560
    iget v8, v0, LX/8zX;->b:F

    float-to-int v8, v8

    .line 1427561
    iget-object p2, v7, LX/91L;->a:Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;

    iput v8, p2, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->d:I

    .line 1427562
    move-object v7, v7

    .line 1427563
    iget v8, v0, LX/8zX;->d:F

    float-to-int v8, v8

    .line 1427564
    iget-object p2, v7, LX/91L;->a:Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;

    iput v8, p2, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->e:I

    .line 1427565
    move-object v7, v7

    .line 1427566
    iget-object v8, v0, LX/8zX;->f:Landroid/net/Uri;

    .line 1427567
    iget-object p2, v7, LX/91L;->a:Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;

    iput-object v8, p2, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->o:Landroid/net/Uri;

    .line 1427568
    move-object v7, v7

    .line 1427569
    iget v8, v0, LX/8zX;->c:F

    float-to-int v8, v8

    .line 1427570
    iget-object p2, v7, LX/91L;->a:Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;

    iput v8, p2, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->p:I

    .line 1427571
    move-object v7, v7

    .line 1427572
    iget v8, v0, LX/8zX;->c:F

    float-to-int v8, v8

    .line 1427573
    iget-object p2, v7, LX/91L;->a:Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;

    iput v8, p2, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->q:I

    .line 1427574
    move-object v7, v7

    .line 1427575
    iget v8, v0, LX/8zX;->d:F

    float-to-int v8, v8

    .line 1427576
    iget-object p2, v7, LX/91L;->a:Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;

    iput v8, p2, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->r:I

    .line 1427577
    move-object v7, v7

    .line 1427578
    invoke-virtual {v7, v4}, LX/91L;->a(Ljava/lang/CharSequence;)LX/91L;

    move-result-object v4

    iget-object v7, v0, LX/8zX;->g:Ljava/lang/String;

    .line 1427579
    iget-object v8, v4, LX/91L;->a:Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;

    iput-object v7, v8, Lcom/facebook/composer/minutiae/common/MinutiaeImageBlockComponent$MinutiaeImageBlockComponentImpl;->g:Ljava/lang/CharSequence;

    .line 1427580
    move-object v4, v4

    .line 1427581
    invoke-virtual {v4, p0}, LX/91L;->m(I)LX/91L;

    move-result-object v4

    invoke-interface {v6, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    .line 1427582
    const v6, -0x4e4b4a3c

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v1, v7, v8

    invoke-static {p1, v6, v7}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v6

    move-object v6, v6

    .line 1427583
    invoke-interface {v4, v6}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v4

    const v6, 0x7f020307

    invoke-interface {v4, v6}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    const/16 v5, 0x8

    const v6, 0x7f0b1898

    invoke-interface {v4, v5, v6}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0a0720

    invoke-interface {v4, v5}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 1427584
    return-object v0

    :cond_1
    move-object v4, v5

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 1427531
    invoke-static {}, LX/1dS;->b()V

    .line 1427532
    iget v0, p1, LX/1dQ;->b:I

    .line 1427533
    packed-switch v0, :pswitch_data_0

    .line 1427534
    :goto_0
    return-object v3

    .line 1427535
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1427536
    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    iget-object v2, p1, LX/1dQ;->a:LX/1X1;

    .line 1427537
    check-cast v2, LX/8zV;

    .line 1427538
    iget-object v4, p0, LX/8zW;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    iget-boolean v4, v2, LX/8zV;->c:Z

    iget-object p1, v2, LX/8zV;->d:LX/90B;

    iget-object p2, v2, LX/8zV;->e:LX/90D;

    .line 1427539
    if-eqz p2, :cond_1

    if-eqz v4, :cond_1

    .line 1427540
    invoke-interface {p2, v0}, LX/90D;->a(Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;)V

    .line 1427541
    :cond_0
    :goto_1
    goto :goto_0

    .line 1427542
    :cond_1
    if-eqz p1, :cond_0

    .line 1427543
    invoke-interface {p1, v0}, LX/90B;->a(Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch -0x4e4b4a3c
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/8zU;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1427523
    new-instance v1, LX/8zV;

    invoke-direct {v1, p0}, LX/8zV;-><init>(LX/8zW;)V

    .line 1427524
    sget-object v2, LX/8zW;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8zU;

    .line 1427525
    if-nez v2, :cond_0

    .line 1427526
    new-instance v2, LX/8zU;

    invoke-direct {v2}, LX/8zU;-><init>()V

    .line 1427527
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/8zU;->a$redex0(LX/8zU;LX/1De;IILX/8zV;)V

    .line 1427528
    move-object v1, v2

    .line 1427529
    move-object v0, v1

    .line 1427530
    return-object v0
.end method
