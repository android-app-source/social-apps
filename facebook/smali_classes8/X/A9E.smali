.class public final LX/A9E;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:D

.field public g:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:D

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:D

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Z

.field public m:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1632090
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;
    .locals 15

    .prologue
    const/4 v14, 0x1

    const/4 v13, 0x0

    const/4 v12, 0x0

    const-wide/16 v4, 0x0

    .line 1632091
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1632092
    iget-object v1, p0, LX/A9E;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1632093
    iget-object v2, p0, LX/A9E;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1632094
    iget-object v3, p0, LX/A9E;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1632095
    iget-object v6, p0, LX/A9E;->d:Ljava/lang/String;

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1632096
    iget-object v7, p0, LX/A9E;->e:Ljava/lang/String;

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1632097
    iget-object v8, p0, LX/A9E;->g:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    invoke-virtual {v0, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 1632098
    iget-object v9, p0, LX/A9E;->i:Ljava/lang/String;

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1632099
    iget-object v10, p0, LX/A9E;->k:Ljava/lang/String;

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1632100
    const/16 v11, 0xd

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 1632101
    invoke-virtual {v0, v13, v1}, LX/186;->b(II)V

    .line 1632102
    invoke-virtual {v0, v14, v2}, LX/186;->b(II)V

    .line 1632103
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1632104
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1632105
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1632106
    const/4 v1, 0x5

    iget-wide v2, p0, LX/A9E;->f:D

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1632107
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1632108
    const/4 v1, 0x7

    iget-wide v2, p0, LX/A9E;->h:D

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1632109
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 1632110
    const/16 v1, 0x9

    iget-wide v2, p0, LX/A9E;->j:D

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1632111
    const/16 v1, 0xa

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 1632112
    const/16 v1, 0xb

    iget-boolean v2, p0, LX/A9E;->l:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 1632113
    const/16 v1, 0xc

    iget-boolean v2, p0, LX/A9E;->m:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 1632114
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1632115
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1632116
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1632117
    invoke-virtual {v1, v13}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1632118
    new-instance v0, LX/15i;

    move-object v2, v12

    move-object v3, v12

    move v4, v14

    move-object v5, v12

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1632119
    new-instance v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    invoke-direct {v1, v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;-><init>(LX/15i;)V

    .line 1632120
    return-object v1
.end method
