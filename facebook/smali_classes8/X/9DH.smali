.class public LX/9DH;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/9DG;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1455333
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1455334
    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/Fragment;Landroid/content/Context;LX/9Bj;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;ZZZLX/9Br;ZLX/0QK;)LX/9DG;
    .locals 41
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/Fragment;",
            "Landroid/content/Context;",
            "LX/9Bj;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;",
            "ZZZ",
            "LX/9Br;",
            "Z",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            "Ljava/lang/Void;",
            ">;)",
            "LX/9DG;"
        }
    .end annotation

    .prologue
    .line 1455335
    new-instance v1, LX/9DG;

    const-class v2, LX/9Cz;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/9Cz;

    invoke-static/range {p0 .. p0}, LX/9FO;->a(LX/0QB;)LX/9FO;

    move-result-object v14

    check-cast v14, LX/9FO;

    const-class v2, LX/9Eb;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v15

    check-cast v15, LX/9Eb;

    const-class v2, LX/9EC;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v16

    check-cast v16, LX/9EC;

    invoke-static/range {p0 .. p0}, LX/9Da;->a(LX/0QB;)LX/9Da;

    move-result-object v17

    check-cast v17, LX/9Da;

    invoke-static/range {p0 .. p0}, LX/9Do;->a(LX/0QB;)LX/9Do;

    move-result-object v18

    check-cast v18, LX/9Do;

    const/16 v2, 0x1d9a

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v19

    invoke-static/range {p0 .. p0}, LX/9Du;->a(LX/0QB;)LX/9Du;

    move-result-object v20

    check-cast v20, LX/9Du;

    invoke-static/range {p0 .. p0}, LX/1Db;->a(LX/0QB;)LX/1Db;

    move-result-object v21

    check-cast v21, LX/1Db;

    invoke-static/range {p0 .. p0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v22

    check-cast v22, LX/0iA;

    invoke-static/range {p0 .. p0}, LX/2cm;->a(LX/0QB;)LX/2cm;

    move-result-object v23

    check-cast v23, LX/2cm;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v24

    check-cast v24, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/20j;->a(LX/0QB;)LX/20j;

    move-result-object v25

    check-cast v25, LX/20j;

    invoke-static/range {p0 .. p0}, LX/3iP;->a(LX/0QB;)LX/3iP;

    move-result-object v26

    check-cast v26, LX/3iP;

    invoke-static/range {p0 .. p0}, LX/9D7;->a(LX/0QB;)LX/9D7;

    move-result-object v27

    check-cast v27, LX/9D7;

    invoke-static/range {p0 .. p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v28

    check-cast v28, Lcom/facebook/content/SecureContextHelper;

    const-class v2, LX/9CQ;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v29

    check-cast v29, LX/9CQ;

    invoke-static/range {p0 .. p0}, LX/0tF;->a(LX/0QB;)LX/0tF;

    move-result-object v30

    check-cast v30, LX/0tF;

    invoke-static/range {p0 .. p0}, LX/9Im;->a(LX/0QB;)LX/9Im;

    move-result-object v31

    check-cast v31, LX/9Im;

    invoke-static/range {p0 .. p0}, LX/9CD;->a(LX/0QB;)LX/9CD;

    move-result-object v32

    check-cast v32, LX/9CD;

    const-class v2, LX/9FB;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v33

    check-cast v33, LX/9FB;

    invoke-static/range {p0 .. p0}, LX/1My;->a(LX/0QB;)LX/1My;

    move-result-object v34

    check-cast v34, LX/1My;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v35

    check-cast v35, LX/03V;

    const/16 v2, 0x3567

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v36

    const-class v2, LX/9F8;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v37

    check-cast v37, LX/9F8;

    const-class v2, LX/9EM;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v38

    check-cast v38, LX/9EM;

    const/16 v2, 0x2a

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v39

    const/16 v2, 0xdf4

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v40

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move-object/from16 v10, p9

    move/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v1 .. v40}, LX/9DG;-><init>(Landroid/support/v4/app/Fragment;Landroid/content/Context;LX/9Bj;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;ZZZLX/9Br;ZLX/0QK;LX/9Cz;LX/9FO;LX/9Eb;LX/9EC;LX/9Da;LX/9Do;LX/0Ot;LX/9Du;LX/1Db;LX/0iA;LX/2cm;LX/0ad;LX/20j;LX/3iP;LX/9D7;Lcom/facebook/content/SecureContextHelper;LX/9CQ;LX/0tF;LX/9Im;LX/9CD;LX/9FB;LX/1My;LX/03V;LX/0Ot;LX/9F8;LX/9EM;LX/0Ot;LX/0Ot;)V

    .line 1455336
    return-object v1
.end method
