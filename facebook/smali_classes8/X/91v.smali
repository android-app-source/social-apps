.class public LX/91v;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/91k;

.field private final b:LX/92o;

.field public final c:LX/92X;

.field public final d:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

.field public final e:LX/92B;

.field public f:LX/91D;

.field public g:LX/5LG;

.field public h:LX/0ut;

.field private i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field private final k:LX/905;

.field public final l:LX/909;

.field public m:Z

.field public n:Z

.field public o:Z

.field private final p:LX/0Vd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vd",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public q:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/91k;Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;LX/905;LX/909;ILX/92o;LX/92Y;LX/91D;LX/92B;)V
    .locals 3
    .param p1    # LX/91k;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/905;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/909;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1431635
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1431636
    iput-boolean v0, p0, LX/91v;->m:Z

    .line 1431637
    iput-boolean v0, p0, LX/91v;->n:Z

    .line 1431638
    iput-boolean v0, p0, LX/91v;->o:Z

    .line 1431639
    new-instance v0, LX/91q;

    invoke-direct {v0, p0}, LX/91q;-><init>(LX/91v;)V

    iput-object v0, p0, LX/91v;->p:LX/0Vd;

    .line 1431640
    iput-object p1, p0, LX/91v;->a:LX/91k;

    .line 1431641
    iput-object p6, p0, LX/91v;->b:LX/92o;

    .line 1431642
    iput-object p2, p0, LX/91v;->d:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1431643
    iput-object p3, p0, LX/91v;->k:LX/905;

    .line 1431644
    iput-object p4, p0, LX/91v;->l:LX/909;

    .line 1431645
    iget-object v0, p0, LX/91v;->d:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1431646
    iget-object v1, v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v0, v1

    .line 1431647
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/91v;->d:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1431648
    iget-object v1, v0, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v0, v1

    .line 1431649
    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v0

    .line 1431650
    :goto_0
    iget-object v1, p0, LX/91v;->d:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1431651
    iget-object v2, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->i:Ljava/lang/String;

    move-object v1, v2

    .line 1431652
    if-eqz v1, :cond_1

    iget-object v1, p0, LX/91v;->d:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    .line 1431653
    iget-object v2, v1, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->i:Ljava/lang/String;

    move-object v1, v2

    .line 1431654
    :goto_1
    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p7, v2, v0, v1}, LX/92Y;->a(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)LX/92X;

    move-result-object v0

    iput-object v0, p0, LX/91v;->c:LX/92X;

    .line 1431655
    iput-object p8, p0, LX/91v;->f:LX/91D;

    .line 1431656
    iput-object p9, p0, LX/91v;->e:LX/92B;

    .line 1431657
    return-void

    .line 1431658
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1431659
    :cond_1
    const-string v1, "composer"

    goto :goto_1
.end method

.method public static a$redex0(LX/91v;Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;)V
    .locals 2

    .prologue
    .line 1431709
    iget-object v0, p0, LX/91v;->g:LX/5LG;

    invoke-interface {v0}, LX/5LG;->v()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1431710
    iget-object v0, p0, LX/91v;->g:LX/5LG;

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    invoke-static {v0}, LX/5Lb;->a(Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;)LX/5Lb;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;->a()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v1

    .line 1431711
    iput-object v1, v0, LX/5Lb;->i:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1431712
    move-object v0, v0

    .line 1431713
    invoke-virtual {p1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;->j()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v1

    .line 1431714
    iput-object v1, v0, LX/5Lb;->j:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1431715
    move-object v0, v0

    .line 1431716
    invoke-virtual {p1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;->k()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v1

    .line 1431717
    iput-object v1, v0, LX/5Lb;->k:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1431718
    move-object v0, v0

    .line 1431719
    invoke-virtual {p1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;->l()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v1

    .line 1431720
    iput-object v1, v0, LX/5Lb;->l:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1431721
    move-object v0, v0

    .line 1431722
    invoke-virtual {p1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;->m()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v1

    .line 1431723
    iput-object v1, v0, LX/5Lb;->m:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1431724
    move-object v0, v0

    .line 1431725
    invoke-virtual {p1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableObjectsQueryModel;->n()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v1

    .line 1431726
    iput-object v1, v0, LX/5Lb;->n:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    .line 1431727
    move-object v0, v0

    .line 1431728
    invoke-virtual {v0}, LX/5Lb;->a()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel;

    move-result-object v0

    iput-object v0, p0, LX/91v;->g:LX/5LG;

    .line 1431729
    :cond_0
    return-void
.end method

.method public static b$redex0(LX/91v;Ljava/lang/String;)V
    .locals 4
    .param p0    # LX/91v;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1431734
    iget-object v0, p0, LX/91v;->e:LX/92B;

    .line 1431735
    const v1, 0xc50002

    const-string v2, "minutiae_feelings_selector_time_to_fetch_start"

    invoke-virtual {v0, v1, v2}, LX/92A;->b(ILjava/lang/String;)V

    .line 1431736
    const v1, 0xc50005

    const-string v2, "minutiae_feelings_selector_fetch_time"

    invoke-virtual {v0, v1, v2}, LX/92A;->c(ILjava/lang/String;)V

    .line 1431737
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1431738
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/91v;->o:Z

    .line 1431739
    iget-object v1, p0, LX/91v;->c:LX/92X;

    iget-object v2, p0, LX/91v;->g:LX/5LG;

    new-instance v3, LX/91u;

    invoke-direct {v3, p0, v0, p1}, LX/91u;-><init>(LX/91v;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2, p1, v0, v3}, LX/92X;->a(LX/5LG;Ljava/lang/String;Ljava/lang/String;LX/0Vd;)V

    .line 1431740
    return-void
.end method

.method public static c(LX/91v;Ljava/lang/String;)V
    .locals 6
    .param p0    # LX/91v;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1431730
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/91v;->o:Z

    .line 1431731
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1431732
    iget-object v0, p0, LX/91v;->c:LX/92X;

    iget-object v1, p0, LX/91v;->g:LX/5LG;

    iget-object v2, p0, LX/91v;->h:LX/0ut;

    invoke-interface {v2}, LX/0ut;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/91v;->p:LX/0Vd;

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, LX/92X;->a(LX/5LG;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Vd;)Z

    .line 1431733
    return-void
.end method

.method public static g(LX/91v;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1431701
    iput-boolean v1, p0, LX/91v;->n:Z

    .line 1431702
    iget-object v0, p0, LX/91v;->k:LX/905;

    invoke-interface {v0, v1}, LX/905;->a(Z)V

    .line 1431703
    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1431704
    iput-boolean v1, p0, LX/91v;->n:Z

    .line 1431705
    iget-object v0, p0, LX/91v;->k:LX/905;

    invoke-interface {v0, v1}, LX/905;->a(Z)V

    .line 1431706
    iget-object v0, p0, LX/91v;->e:LX/92B;

    .line 1431707
    const v1, 0xc5000b

    const-string p0, "minutiae_feelings_selector_time_to_scroll_load"

    invoke-virtual {v0, v1, p0}, LX/92A;->c(ILjava/lang/String;)V

    .line 1431708
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;)Lcom/facebook/composer/minutiae/model/MinutiaeObject;
    .locals 3

    .prologue
    .line 1431687
    iget-object v0, p0, LX/91v;->d:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    invoke-static {v0}, LX/93D;->a(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/91v;->d:Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    invoke-static {v0}, LX/93D;->b(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->e()Ljava/lang/String;

    move-result-object v0

    .line 1431688
    :goto_0
    new-instance v1, LX/2s1;

    invoke-direct {v1}, LX/2s1;-><init>()V

    .line 1431689
    iget-object v2, p0, LX/91v;->g:LX/5LG;

    move-object v2, v2

    .line 1431690
    iput-object v2, v1, LX/2s1;->a:LX/5LG;

    .line 1431691
    move-object v1, v1

    .line 1431692
    iput-object p1, v1, LX/2s1;->b:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    .line 1431693
    move-object v1, v1

    .line 1431694
    invoke-virtual {p1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->a()Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$AssociatedPlacesInfoModel;

    move-result-object v2

    .line 1431695
    iput-object v2, v1, LX/2s1;->g:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$AssociatedPlacesInfoModel;

    .line 1431696
    move-object v1, v1

    .line 1431697
    iput-object v0, v1, LX/2s1;->d:Ljava/lang/String;

    .line 1431698
    move-object v0, v1

    .line 1431699
    invoke-virtual {v0}, LX/2s1;->a()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v0

    return-object v0

    .line 1431700
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 1431684
    iget-object v0, p0, LX/91v;->b:LX/92o;

    new-instance v1, LX/91s;

    invoke-direct {v1, p0}, LX/91s;-><init>(LX/91v;)V

    .line 1431685
    invoke-static {v0, v1}, LX/92o;->b(LX/92o;LX/91s;)LX/8zq;

    move-result-object p0

    invoke-virtual {v0, p0}, LX/92o;->a(LX/8zq;)V

    .line 1431686
    return-void
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 1431669
    iget-object v0, p0, LX/91v;->a:LX/91k;

    invoke-virtual {v0}, LX/91k;->e()I

    move-result v1

    .line 1431670
    add-int/lit8 v0, v1, -0x1

    if-ne v0, p1, :cond_1

    const/4 v0, 0x1

    .line 1431671
    :goto_0
    iget-boolean v2, p0, LX/91v;->m:Z

    if-eqz v2, :cond_2

    .line 1431672
    :cond_0
    :goto_1
    return-void

    .line 1431673
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1431674
    :cond_2
    iget-boolean v2, p0, LX/91v;->o:Z

    if-eqz v2, :cond_3

    if-eqz v0, :cond_3

    iget-boolean v2, p0, LX/91v;->n:Z

    if-nez v2, :cond_3

    .line 1431675
    invoke-direct {p0}, LX/91v;->h()V

    goto :goto_1

    .line 1431676
    :cond_3
    iget-boolean v2, p0, LX/91v;->o:Z

    if-eqz v2, :cond_4

    if-nez v0, :cond_4

    iget-boolean v2, p0, LX/91v;->n:Z

    if-eqz v2, :cond_4

    .line 1431677
    invoke-static {p0}, LX/91v;->g(LX/91v;)V

    goto :goto_1

    .line 1431678
    :cond_4
    add-int/lit8 v1, v1, -0x5

    if-lt p1, v1, :cond_0

    iget-boolean v1, p0, LX/91v;->o:Z

    if-nez v1, :cond_0

    .line 1431679
    iget-object v1, p0, LX/91v;->h:LX/0ut;

    if-eqz v1, :cond_5

    iget-object v1, p0, LX/91v;->h:LX/0ut;

    invoke-interface {v1}, LX/0ut;->b()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, LX/91v;->h:LX/0ut;

    invoke-interface {v1}, LX/0ut;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    const/4 v1, 0x1

    :goto_2
    move v1, v1

    .line 1431680
    if-eqz v1, :cond_0

    .line 1431681
    iget-object v1, p0, LX/91v;->j:Ljava/lang/String;

    invoke-static {p0, v1}, LX/91v;->c(LX/91v;Ljava/lang/String;)V

    .line 1431682
    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/91v;->n:Z

    if-nez v0, :cond_0

    .line 1431683
    invoke-direct {p0}, LX/91v;->h()V

    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1431661
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1431662
    const/4 v0, 0x0

    .line 1431663
    :goto_0
    iput-object v0, p0, LX/91v;->j:Ljava/lang/String;

    .line 1431664
    if-eqz v0, :cond_0

    iget-object v1, p0, LX/91v;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    if-nez v0, :cond_2

    .line 1431665
    :cond_1
    invoke-static {p0, v0}, LX/91v;->b$redex0(LX/91v;Ljava/lang/String;)V

    .line 1431666
    iput-object v0, p0, LX/91v;->i:Ljava/lang/String;

    .line 1431667
    :cond_2
    return-void

    .line 1431668
    :cond_3
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1431660
    iget-object v0, p0, LX/91v;->j:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/91v;->j:Ljava/lang/String;

    goto :goto_0
.end method
