.class public LX/9jj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/net/wifi/WifiManager;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2GD;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0lB;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0Ot;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Landroid/net/wifi/WifiManager;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2GD;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0lB;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1530341
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1530342
    iput-object p1, p0, LX/9jj;->a:LX/0Or;

    .line 1530343
    iput-object p2, p0, LX/9jj;->b:LX/0Ot;

    .line 1530344
    iput-object p3, p0, LX/9jj;->c:LX/0Or;

    .line 1530345
    return-void
.end method

.method public static b(LX/0QB;)LX/9jj;
    .locals 4

    .prologue
    .line 1530346
    new-instance v0, LX/9jj;

    const/16 v1, 0x26

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v2, 0x13aa

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x2ba

    invoke-static {p0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LX/9jj;-><init>(LX/0Or;LX/0Ot;LX/0Or;)V

    .line 1530347
    return-object v0
.end method


# virtual methods
.method public final a(LX/4DI;)Z
    .locals 14

    .prologue
    .line 1530348
    new-instance v2, Lcom/facebook/places/checkin/protocol/CheckinSearchQueryLocationExtraData;

    new-instance v3, Lcom/facebook/places/checkin/protocol/CheckinSearchQueryLocationExtraData$CheckinSearchQueryLocationExtraDataWifi;

    const/4 v7, 0x0

    .line 1530349
    iget-object v6, p0, LX/9jj;->a:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/wifi/WifiManager;

    .line 1530350
    invoke-virtual {v6}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v8

    if-nez v8, :cond_2

    move-object v6, v7

    .line 1530351
    :goto_0
    move-object v4, v6

    .line 1530352
    iget-object v6, p0, LX/9jj;->b:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/2GD;

    const-wide/32 v8, 0xea60

    invoke-virtual {v6, v8, v9}, LX/2GD;->a(J)Ljava/util/List;

    move-result-object v6

    .line 1530353
    if-eqz v6, :cond_0

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 1530354
    :cond_0
    const/4 v6, 0x0

    .line 1530355
    :goto_1
    move-object v5, v6

    .line 1530356
    invoke-direct {v3, v4, v5}, Lcom/facebook/places/checkin/protocol/CheckinSearchQueryLocationExtraData$CheckinSearchQueryLocationExtraDataWifi;-><init>(Lcom/facebook/places/checkin/protocol/CheckinSearchQueryLocationExtraData$CheckinSearchQueryLocationExtraDataWifiObject;Ljava/util/List;)V

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/facebook/places/checkin/protocol/CheckinSearchQueryLocationExtraData;-><init>(Lcom/facebook/places/checkin/protocol/CheckinSearchQueryLocationExtraData$CheckinSearchQueryLocationExtraDataWifi;Ljava/lang/String;)V

    move-object v1, v2

    .line 1530357
    if-nez v1, :cond_1

    .line 1530358
    const/4 v0, 0x0

    .line 1530359
    :goto_2
    return v0

    .line 1530360
    :cond_1
    :try_start_0
    iget-object v0, p0, LX/9jj;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lB;

    invoke-virtual {v0, v1}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1530361
    const-string v1, "location_extra_data"

    invoke-virtual {p1, v1, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    .line 1530362
    const/4 v0, 0x1

    goto :goto_2

    .line 1530363
    :catch_0
    move-exception v0

    .line 1530364
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1530365
    :cond_2
    invoke-virtual {v6}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v10

    .line 1530366
    if-nez v10, :cond_3

    move-object v6, v7

    .line 1530367
    goto :goto_0

    .line 1530368
    :cond_3
    new-instance v6, Lcom/facebook/places/checkin/protocol/CheckinSearchQueryLocationExtraData$CheckinSearchQueryLocationExtraDataWifiObject;

    invoke-virtual {v10}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v10}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v8

    .line 1530369
    if-nez v8, :cond_4

    .line 1530370
    const/4 v9, 0x0

    .line 1530371
    :goto_3
    move-object v8, v9

    .line 1530372
    invoke-virtual {v10}, Landroid/net/wifi/WifiInfo;->getRssi()I

    move-result v9

    .line 1530373
    sget v11, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v12, 0x15

    if-ge v11, v12, :cond_5

    .line 1530374
    const/4 v11, 0x0

    .line 1530375
    :goto_4
    move v10, v11

    .line 1530376
    const-wide/16 v12, 0x0

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-direct/range {v6 .. v11}, Lcom/facebook/places/checkin/protocol/CheckinSearchQueryLocationExtraData$CheckinSearchQueryLocationExtraDataWifiObject;-><init>(Ljava/lang/String;Ljava/lang/String;IILjava/lang/Long;)V

    goto :goto_0

    :cond_4
    const-string v9, "\""

    const-string v11, ""

    invoke-virtual {v8, v9, v11}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    goto :goto_3

    :cond_5
    invoke-virtual {v10}, Landroid/net/wifi/WifiInfo;->getFrequency()I

    move-result v11

    goto :goto_4

    .line 1530377
    :cond_6
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    .line 1530378
    new-instance v7, LX/9ji;

    invoke-direct {v7, p0, v8, v9}, LX/9ji;-><init>(LX/9jj;J)V

    invoke-static {v6, v7}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v6

    goto :goto_1
.end method
