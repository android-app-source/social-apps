.class public LX/9Ia;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/9Hp;

.field public final b:LX/0tF;

.field public final c:LX/0ad;


# direct methods
.method public constructor <init>(LX/9Hp;LX/0tF;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1463330
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1463331
    iput-object p1, p0, LX/9Ia;->a:LX/9Hp;

    .line 1463332
    iput-object p2, p0, LX/9Ia;->b:LX/0tF;

    .line 1463333
    iput-object p3, p0, LX/9Ia;->c:LX/0ad;

    .line 1463334
    return-void
.end method

.method public static a(LX/0QB;)LX/9Ia;
    .locals 6

    .prologue
    .line 1463308
    const-class v1, LX/9Ia;

    monitor-enter v1

    .line 1463309
    :try_start_0
    sget-object v0, LX/9Ia;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1463310
    sput-object v2, LX/9Ia;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1463311
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1463312
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1463313
    new-instance p0, LX/9Ia;

    invoke-static {v0}, LX/9Hp;->a(LX/0QB;)LX/9Hp;

    move-result-object v3

    check-cast v3, LX/9Hp;

    invoke-static {v0}, LX/0tF;->a(LX/0QB;)LX/0tF;

    move-result-object v4

    check-cast v4, LX/0tF;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-direct {p0, v3, v4, v5}, LX/9Ia;-><init>(LX/9Hp;LX/0tF;LX/0ad;)V

    .line 1463314
    move-object v0, p0

    .line 1463315
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1463316
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/9Ia;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1463317
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1463318
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLFeedback;LX/5Gs;LX/9Fi;LX/9FA;LX/9Gn;)V
    .locals 3

    .prologue
    .line 1463319
    iget-boolean v0, p4, LX/9Gn;->a:Z

    move v0, v0

    .line 1463320
    if-eqz v0, :cond_0

    .line 1463321
    :goto_0
    return-void

    .line 1463322
    :cond_0
    new-instance v1, LX/9Ga;

    invoke-direct {v1}, LX/9Ga;-><init>()V

    .line 1463323
    const/4 v0, 0x0

    iput-boolean v0, v1, LX/9Ga;->a:Z

    .line 1463324
    invoke-static {p0, p1}, LX/9Ii;->a(Lcom/facebook/graphql/model/GraphQLFeedback;LX/5Gs;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1463325
    new-instance v0, LX/9GZ;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, LX/9GZ;-><init>(Ljava/lang/String;)V

    invoke-interface {p3, v0, v1}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    .line 1463326
    :goto_1
    invoke-interface {p3}, LX/1Pq;->iN_()V

    goto :goto_0

    .line 1463327
    :cond_1
    const/4 v0, 0x1

    .line 1463328
    iput-boolean v0, p4, LX/9Gn;->a:Z

    .line 1463329
    sget-object v0, LX/9Fi;->TOP_LEVEL:LX/9Fi;

    invoke-virtual {v0, p2}, LX/9Fi;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0x19

    :goto_2
    new-instance v2, LX/9IZ;

    invoke-direct {v2, p3, p0, v1, p4}, LX/9IZ;-><init>(LX/9FA;Lcom/facebook/graphql/model/GraphQLFeedback;LX/9Ga;LX/9Gn;)V

    invoke-virtual {p3, p0, p1, v0, v2}, LX/9FA;->a(Lcom/facebook/graphql/model/GraphQLFeedback;LX/5Gs;ILX/0TF;)V

    goto :goto_1

    :cond_2
    const/16 v0, 0xa

    goto :goto_2
.end method
