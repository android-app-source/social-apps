.class public LX/ASC;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/ASB;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1673749
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1673750
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;Landroid/view/ViewGroup;Ljava/lang/Object;LX/HqU;LX/Hrb;LX/0Px;)LX/ASB;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData:",
            "Ljava/lang/Object;",
            ":",
            "LX/0io;",
            ":",
            "Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData$ProvidesAudienceEducatorData;",
            ":",
            "LX/0j0;",
            ":",
            "LX/0j1;",
            ":",
            "LX/0j3;",
            ":",
            "LX/0j4;",
            ":",
            "LX/0j6;",
            ":",
            "LX/0ip;",
            ":",
            "LX/0jF;",
            "DerivedData:",
            "Ljava/lang/Object;",
            ":",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesIsMinutiaeSupported;",
            ":",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesIsPrivacyPillSupported;",
            "Services:",
            "Ljava/lang/Object;",
            ":",
            "LX/0il",
            "<TModelData;>;:",
            "LX/0ik",
            "<TDerivedData;>;>(",
            "Landroid/view/ViewGroup;",
            "Landroid/view/ViewGroup;",
            "TServices;",
            "Lcom/facebook/composer/tip/StickyGuardrailInterstitialController$DataProvider;",
            "Lcom/facebook/composer/tip/StickyGuardrailInterstitialController$StickyGuardrailCallback;",
            "LX/0Px",
            "<",
            "LX/3l4;",
            ">;)",
            "LX/ASB",
            "<TModelData;TDerivedData;TServices;>;"
        }
    .end annotation

    .prologue
    .line 1673751
    new-instance v0, LX/ASB;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v2

    check-cast v2, LX/0iA;

    const-class v3, LX/AS2;

    invoke-interface {p0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/AS2;

    const-class v4, LX/ASP;

    invoke-interface {p0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/ASP;

    const-class v5, LX/ASH;

    invoke-interface {p0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/ASH;

    const-class v6, LX/ASV;

    invoke-interface {p0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/ASV;

    const-class v7, LX/AS8;

    invoke-interface {p0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/AS8;

    move-object/from16 v10, p3

    check-cast v10, LX/0il;

    move-object v8, p1

    move-object/from16 v9, p2

    move-object/from16 v11, p4

    move-object/from16 v12, p5

    move-object/from16 v13, p6

    invoke-direct/range {v0 .. v13}, LX/ASB;-><init>(Landroid/content/Context;LX/0iA;LX/AS2;LX/ASP;LX/ASH;LX/ASV;LX/AS8;Landroid/view/ViewGroup;Landroid/view/ViewGroup;LX/0il;LX/HqU;LX/Hrb;LX/0Px;)V

    .line 1673752
    return-object v0
.end method
