.class public final LX/9IP;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/9IQ;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public b:Z

.field public c:LX/2pa;

.field public d:LX/2oV;

.field public final synthetic e:LX/9IQ;


# direct methods
.method public constructor <init>(LX/9IQ;)V
    .locals 1

    .prologue
    .line 1462962
    iput-object p1, p0, LX/9IP;->e:LX/9IQ;

    .line 1462963
    move-object v0, p1

    .line 1462964
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1462965
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1462966
    const-string v0, "CommentVideoComponent"

    return-object v0
.end method

.method public final a(LX/1X1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<",
            "LX/9IQ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1462967
    check-cast p1, LX/9IP;

    .line 1462968
    iget-object v0, p1, LX/9IP;->c:LX/2pa;

    iput-object v0, p0, LX/9IP;->c:LX/2pa;

    .line 1462969
    iget-object v0, p1, LX/9IP;->d:LX/2oV;

    iput-object v0, p0, LX/9IP;->d:LX/2oV;

    .line 1462970
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1462971
    if-ne p0, p1, :cond_1

    .line 1462972
    :cond_0
    :goto_0
    return v0

    .line 1462973
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1462974
    goto :goto_0

    .line 1462975
    :cond_3
    check-cast p1, LX/9IP;

    .line 1462976
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1462977
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1462978
    if-eq v2, v3, :cond_0

    .line 1462979
    iget-object v2, p0, LX/9IP;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/9IP;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget-object v3, p1, LX/9IP;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1462980
    goto :goto_0

    .line 1462981
    :cond_5
    iget-object v2, p1, LX/9IP;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-nez v2, :cond_4

    .line 1462982
    :cond_6
    iget-boolean v2, p0, LX/9IP;->b:Z

    iget-boolean v3, p1, LX/9IP;->b:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1462983
    goto :goto_0
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 1462984
    const/4 v1, 0x0

    .line 1462985
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/9IP;

    .line 1462986
    iput-object v1, v0, LX/9IP;->c:LX/2pa;

    .line 1462987
    iput-object v1, v0, LX/9IP;->d:LX/2oV;

    .line 1462988
    return-object v0
.end method
