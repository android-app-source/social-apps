.class public final LX/8yU;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/8yV;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1425779
    invoke-direct {p0}, LX/1X5;-><init>()V

    return-void
.end method

.method public static a$redex0(LX/8yU;LX/1De;IILcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;)V
    .locals 0

    .prologue
    .line 1425780
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1425781
    iput-object p4, p0, LX/8yU;->a:Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;

    .line 1425782
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/common/callercontext/CallerContext;)LX/8yU;
    .locals 1

    .prologue
    .line 1425764
    iget-object v0, p0, LX/8yU;->a:Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;

    iput-object p1, v0, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 1425765
    return-object p0
.end method

.method public final a(Ljava/util/List;)LX/8yU;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;)",
            "LX/8yU;"
        }
    .end annotation

    .prologue
    .line 1425777
    iget-object v0, p0, LX/8yU;->a:Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;

    iput-object p1, v0, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->g:Ljava/util/List;

    .line 1425778
    return-object p0
.end method

.method public final a(Z)LX/8yU;
    .locals 1

    .prologue
    .line 1425775
    iget-object v0, p0, LX/8yU;->a:Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;

    iput-boolean p1, v0, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->f:Z

    .line 1425776
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1425771
    invoke-super {p0}, LX/1X5;->a()V

    .line 1425772
    const/4 v0, 0x0

    iput-object v0, p0, LX/8yU;->a:Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;

    .line 1425773
    sget-object v0, LX/8yV;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1425774
    return-void
.end method

.method public final c(F)LX/8yU;
    .locals 2
    .param p1    # F
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param

    .prologue
    .line 1425769
    iget-object v0, p0, LX/8yU;->a:Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;

    invoke-virtual {p0, p1}, LX/1Dp;->a(F)I

    move-result v1

    iput v1, v0, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->d:I

    .line 1425770
    return-object p0
.end method

.method public final d()LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/8yV;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1425766
    iget-object v0, p0, LX/8yU;->a:Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;

    .line 1425767
    invoke-virtual {p0}, LX/8yU;->a()V

    .line 1425768
    return-object v0
.end method

.method public final d(F)LX/8yU;
    .locals 2
    .param p1    # F
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param

    .prologue
    .line 1425783
    iget-object v0, p0, LX/8yU;->a:Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;

    invoke-virtual {p0, p1}, LX/1Dp;->a(F)I

    move-result v1

    iput v1, v0, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->k:I

    .line 1425784
    return-object p0
.end method

.method public final h(I)LX/8yU;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 1425739
    iget-object v0, p0, LX/8yU;->a:Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;

    invoke-virtual {p0, p1}, LX/1Dp;->e(I)I

    move-result v1

    iput v1, v0, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->b:I

    .line 1425740
    return-object p0
.end method

.method public final i(I)LX/8yU;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param

    .prologue
    .line 1425741
    iget-object v0, p0, LX/8yU;->a:Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;

    invoke-virtual {p0, p1}, LX/1Dp;->d(I)I

    move-result v1

    iput v1, v0, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->c:I

    .line 1425742
    return-object p0
.end method

.method public final k(I)LX/8yU;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 1425743
    iget-object v0, p0, LX/8yU;->a:Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;

    invoke-virtual {p0, p1}, LX/1Dp;->e(I)I

    move-result v1

    iput v1, v0, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->d:I

    .line 1425744
    return-object p0
.end method

.method public final l(I)LX/8yU;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 1425745
    iget-object v0, p0, LX/8yU;->a:Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;

    invoke-virtual {p0, p1}, LX/1Dp;->f(I)I

    move-result v1

    iput v1, v0, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->e:I

    .line 1425746
    return-object p0
.end method

.method public final m(I)LX/8yU;
    .locals 1

    .prologue
    .line 1425747
    iget-object v0, p0, LX/8yU;->a:Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;

    iput p1, v0, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->h:I

    .line 1425748
    return-object p0
.end method

.method public final n(I)LX/8yU;
    .locals 4
    .param p1    # I
        .annotation build Landroid/support/annotation/IntegerRes;
        .end annotation
    .end param

    .prologue
    .line 1425749
    iget-object v0, p0, LX/8yU;->a:Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;

    .line 1425750
    if-eqz p1, :cond_1

    .line 1425751
    iget-object v1, p0, LX/1Dp;->d:LX/1Ds;

    invoke-virtual {v1, p1}, LX/1Ds;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 1425752
    if-eqz v1, :cond_0

    .line 1425753
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 1425754
    :goto_0
    move v1, v1

    .line 1425755
    iput v1, v0, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->h:I

    .line 1425756
    return-object p0

    .line 1425757
    :cond_0
    iget-object v1, p0, LX/1Dp;->b:Landroid/content/res/Resources;

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    .line 1425758
    iget-object v2, p0, LX/1Dp;->d:LX/1Ds;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, p1, v3}, LX/1Ds;->a(ILjava/lang/Object;)V

    goto :goto_0

    .line 1425759
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final q(I)LX/8yU;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param

    .prologue
    .line 1425760
    iget-object v0, p0, LX/8yU;->a:Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;

    iput p1, v0, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->k:I

    .line 1425761
    return-object p0
.end method

.method public final r(I)LX/8yU;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 1425762
    iget-object v0, p0, LX/8yU;->a:Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;

    invoke-virtual {p0, p1}, LX/1Dp;->e(I)I

    move-result v1

    iput v1, v0, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->k:I

    .line 1425763
    return-object p0
.end method
