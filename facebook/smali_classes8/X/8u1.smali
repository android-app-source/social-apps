.class public LX/8u1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/Html$TagHandler;


# instance fields
.field private final a:LX/8u0;

.field private b:Z


# direct methods
.method public constructor <init>(LX/8u0;)V
    .locals 1

    .prologue
    .line 1414379
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1414380
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/8u1;->b:Z

    .line 1414381
    iput-object p1, p0, LX/8u1;->a:LX/8u0;

    .line 1414382
    return-void
.end method


# virtual methods
.method public final handleTag(ZLjava/lang/String;Landroid/text/Editable;Lorg/xml/sax/XMLReader;)V
    .locals 2

    .prologue
    .line 1414383
    iget-boolean v0, p0, LX/8u1;->b:Z

    if-nez v0, :cond_0

    .line 1414384
    iget-object v0, p0, LX/8u1;->a:LX/8u0;

    .line 1414385
    iput-object p3, v0, LX/8u0;->b:Landroid/text/Editable;

    .line 1414386
    move-object v0, v0

    .line 1414387
    invoke-interface {p4}, Lorg/xml/sax/XMLReader;->getContentHandler()Lorg/xml/sax/ContentHandler;

    move-result-object v1

    .line 1414388
    iput-object v1, v0, LX/8u0;->a:Lorg/xml/sax/ContentHandler;

    .line 1414389
    iget-object v0, p0, LX/8u1;->a:LX/8u0;

    invoke-interface {p4, v0}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 1414390
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8u1;->b:Z

    .line 1414391
    :cond_0
    return-void
.end method
