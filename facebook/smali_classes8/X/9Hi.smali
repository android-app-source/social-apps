.class public final LX/9Hi;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/9FA;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public final synthetic c:Landroid/content/Context;


# direct methods
.method public constructor <init>(LX/9FA;Lcom/facebook/graphql/model/GraphQLFeedback;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1461741
    iput-object p1, p0, LX/9Hi;->a:LX/9FA;

    iput-object p2, p0, LX/9Hi;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object p3, p0, LX/9Hi;->c:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 2

    .prologue
    .line 1461742
    iget-object v0, p0, LX/9Hi;->a:LX/9FA;

    iget-object v1, p0, LX/9Hi;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0, p1, v1}, LX/9FA;->d(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1461743
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 13

    .prologue
    .line 1461744
    iget-object v0, p0, LX/9Hi;->a:LX/9FA;

    .line 1461745
    iget-object v1, v0, LX/9FA;->e:LX/9Cd;

    iget-object v2, v0, LX/9FA;->a:Landroid/content/Context;

    .line 1461746
    new-instance v6, Landroid/app/ProgressDialog;

    invoke-direct {v6, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 1461747
    const v3, 0x7f081231

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 1461748
    invoke-virtual {v6}, Landroid/app/ProgressDialog;->show()V

    .line 1461749
    iget-object v3, v1, LX/9Cd;->f:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v7

    .line 1461750
    iget-object v3, v1, LX/9Cd;->e:LX/0hx;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, LX/0hx;->a(Z)V

    .line 1461751
    iget-object v3, v1, LX/9Cd;->h:LX/1Ck;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ban_user_comment_author_"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    new-instance v12, LX/9Cc;

    invoke-direct {v12, v1, p1, p2}, LX/9Cc;-><init>(LX/9Cd;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLComment;)V

    new-instance v4, LX/9CS;

    move-object v5, v1

    move-object v9, v2

    move-object v10, p2

    invoke-direct/range {v4 .. v10}, LX/9CS;-><init>(LX/9Cd;Landroid/app/ProgressDialog;JLandroid/content/Context;Lcom/facebook/graphql/model/GraphQLComment;)V

    invoke-virtual {v3, v11, v12, v4}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1461752
    return-void
.end method
