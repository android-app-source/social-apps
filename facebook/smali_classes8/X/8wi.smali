.class public LX/8wi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/api/negative_feedback/NegativeFeedbackMessageActionMethod$Params;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1422882
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1422883
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 7

    .prologue
    .line 1422884
    check-cast p1, Lcom/facebook/api/negative_feedback/NegativeFeedbackMessageActionMethod$Params;

    .line 1422885
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v5

    .line 1422886
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "action_node_token"

    iget-object v2, p1, Lcom/facebook/api/negative_feedback/NegativeFeedbackMessageActionMethod$Params;->a:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1422887
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "message_body"

    iget-object v2, p1, Lcom/facebook/api/negative_feedback/NegativeFeedbackMessageActionMethod$Params;->b:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1422888
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "recipient_id"

    iget-object v2, p1, Lcom/facebook/api/negative_feedback/NegativeFeedbackMessageActionMethod$Params;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1422889
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "json"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1422890
    new-instance v0, LX/14N;

    const-string v1, "negativeFeedbackMessageActions"

    const-string v2, "POST"

    const-string v3, "negative_feedback_message_actions"

    sget-object v4, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    sget-object v6, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v6}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1422891
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1422892
    const/4 v0, 0x0

    return-object v0
.end method
