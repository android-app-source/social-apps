.class public final LX/ANM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6JI;


# instance fields
.field public final synthetic a:Ljava/io/File;

.field public final synthetic b:Lcom/facebook/cameracore/ui/CameraCoreFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/cameracore/ui/CameraCoreFragment;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 1667635
    iput-object p1, p0, LX/ANM;->b:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    iput-object p2, p0, LX/ANM;->a:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1667636
    return-void
.end method

.method public final a(LX/6JJ;)V
    .locals 3

    .prologue
    .line 1667637
    iget-object v0, p0, LX/ANM;->b:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    iget-object v0, v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->J:LX/03V;

    const-string v1, "cameracore_capture_photo"

    invoke-virtual {v0, v1, p1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1667638
    iget-object v0, p0, LX/ANM;->b:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1667639
    iget-object v0, p0, LX/ANM;->b:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    iget-object v0, v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->J:LX/03V;

    const-string v1, "cameracore_capture_photo"

    const-string v2, "Fragment is no longer added"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1667640
    :goto_0
    return-void

    .line 1667641
    :cond_0
    iget-object v0, p0, LX/ANM;->b:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    iget-object v0, v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->z:LX/AMH;

    invoke-virtual {v0}, LX/AMH;->a()V

    .line 1667642
    iget-object v0, p0, LX/ANM;->b:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    iget-object v0, v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->g:Lcom/facebook/resources/ui/FbImageButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbImageButton;->setEnabled(Z)V

    goto :goto_0
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 1667643
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 1667644
    return-void
.end method

.method public final b()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1667645
    iget-object v0, p0, LX/ANM;->b:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1667646
    iget-object v0, p0, LX/ANM;->b:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    iget-object v0, v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->J:LX/03V;

    const-string v1, "cameracore_capture_photo"

    const-string v2, "Fragment is no longer added"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1667647
    :goto_0
    return-void

    .line 1667648
    :cond_0
    iget-object v0, p0, LX/ANM;->b:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, LX/ANM;->a:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1, v5, v5}, Landroid/media/MediaScannerConnection;->scanFile(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;Landroid/media/MediaScannerConnection$OnScanCompletedListener;)V

    .line 1667649
    iget-object v0, p0, LX/ANM;->b:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    iget-object v0, v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->z:LX/AMH;

    iget-object v1, p0, LX/ANM;->a:Ljava/io/File;

    invoke-virtual {v0, v1}, LX/AMH;->a(Ljava/io/File;)V

    .line 1667650
    iget-object v0, p0, LX/ANM;->b:Lcom/facebook/cameracore/ui/CameraCoreFragment;

    iget-object v0, v0, Lcom/facebook/cameracore/ui/CameraCoreFragment;->g:Lcom/facebook/resources/ui/FbImageButton;

    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/FbImageButton;->setEnabled(Z)V

    goto :goto_0
.end method
