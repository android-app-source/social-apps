.class public LX/8mu;
.super Landroid/widget/BaseAdapter;
.source ""


# instance fields
.field public a:Landroid/content/Context;

.field private b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/stickers/model/StickerTag;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/8lN;

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final e:LX/03V;


# direct methods
.method public constructor <init>(Landroid/content/Context;ZLX/03V;)V
    .locals 1

    .prologue
    .line 1400075
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 1400076
    iput-object p1, p0, LX/8mu;->a:Landroid/content/Context;

    .line 1400077
    if-eqz p2, :cond_0

    .line 1400078
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1400079
    const-string p1, "669501923132281"

    const p2, 0x7f0809d7

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1400080
    const-string p1, "856119944401477"

    const p2, 0x7f0809d8

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1400081
    const-string p1, "557948687649549"

    const p2, 0x7f0809d9

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1400082
    const-string p1, "726891670711210"

    const p2, 0x7f0809da

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1400083
    const-string p1, "245800698877618"

    const p2, 0x7f0809db

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1400084
    const-string p1, "678353922211749"

    const p2, 0x7f0809dc

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1400085
    const-string p1, "847909358556595"

    const p2, 0x7f0809dd

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1400086
    const-string p1, "818703164807551"

    const p2, 0x7f0809de

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1400087
    const-string p1, "665788383491047"

    const p2, 0x7f0809df

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1400088
    const-string p1, "749563621732178"

    const p2, 0x7f0809e0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1400089
    move-object v0, v0

    .line 1400090
    :goto_0
    iput-object v0, p0, LX/8mu;->d:Ljava/util/Map;

    .line 1400091
    iput-object p3, p0, LX/8mu;->e:LX/03V;

    .line 1400092
    return-void

    .line 1400093
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/8mu;Lcom/facebook/stickers/model/StickerTag;)Ljava/lang/String;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1400094
    iget-object v1, p0, LX/8mu;->d:Ljava/util/Map;

    if-nez v1, :cond_0

    .line 1400095
    :goto_0
    return-object v0

    .line 1400096
    :cond_0
    iget-object v1, p0, LX/8mu;->d:Ljava/util/Map;

    .line 1400097
    iget-object v2, p1, Lcom/facebook/stickers/model/StickerTag;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1400098
    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1400099
    iget-object v0, p0, LX/8mu;->d:Ljava/util/Map;

    .line 1400100
    iget-object v1, p1, Lcom/facebook/stickers/model/StickerTag;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1400101
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1400102
    iget-object v1, p0, LX/8mu;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1400103
    :cond_1
    iget-object v1, p0, LX/8mu;->e:LX/03V;

    const-class v2, LX/8mu;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unexpected sticker tag:  "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1400104
    iget-object v4, p1, Lcom/facebook/stickers/model/StickerTag;->b:Ljava/lang/String;

    move-object v4, v4

    .line 1400105
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/stickers/model/StickerTag;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1400106
    iput-object p1, p0, LX/8mu;->b:LX/0Px;

    .line 1400107
    const v0, -0x183bdc48

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1400108
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 1400109
    iget-object v0, p0, LX/8mu;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1400110
    iget-object v0, p0, LX/8mu;->b:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1400111
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    .line 1400112
    invoke-virtual {p0, p1}, LX/8mu;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 1400113
    if-nez p2, :cond_1

    .line 1400114
    new-instance v2, Lcom/facebook/stickers/ui/StickerTagItemView;

    iget-object v1, p0, LX/8mu;->a:Landroid/content/Context;

    invoke-direct {v2, v1}, Lcom/facebook/stickers/ui/StickerTagItemView;-><init>(Landroid/content/Context;)V

    .line 1400115
    :goto_0
    check-cast v0, Lcom/facebook/stickers/model/StickerTag;

    move-object v1, v2

    .line 1400116
    check-cast v1, Lcom/facebook/stickers/ui/StickerTagItemView;

    .line 1400117
    invoke-static {p0, v0}, LX/8mu;->a$redex0(LX/8mu;Lcom/facebook/stickers/model/StickerTag;)Ljava/lang/String;

    move-result-object v3

    .line 1400118
    if-eqz v3, :cond_0

    .line 1400119
    :goto_1
    iget-object v4, v0, Lcom/facebook/stickers/model/StickerTag;->c:Ljava/lang/String;

    move-object v4, v4

    .line 1400120
    invoke-virtual {v1}, Lcom/facebook/stickers/ui/StickerTagItemView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    check-cast p1, Landroid/graphics/drawable/GradientDrawable;

    .line 1400121
    new-instance p3, Ljava/lang/StringBuilder;

    const-string p2, "#"

    invoke-direct {p3, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p3

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-static {p3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p3

    invoke-virtual {p1, p3}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 1400122
    iget-object v4, v1, Lcom/facebook/stickers/ui/StickerTagItemView;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {v3}, LX/0YN;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1400123
    iget-object v4, v0, Lcom/facebook/stickers/model/StickerTag;->f:Ljava/lang/String;

    move-object v4, v4

    .line 1400124
    const-string p1, ""

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 1400125
    iget-object v4, v0, Lcom/facebook/stickers/model/StickerTag;->f:Ljava/lang/String;

    move-object v4, v4

    .line 1400126
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 1400127
    invoke-virtual {v1}, Lcom/facebook/stickers/ui/StickerTagItemView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p3, 0x7f0b07ce

    invoke-virtual {p1, p3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    .line 1400128
    invoke-static {v4}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v4

    new-instance p3, LX/1o9;

    invoke-direct {p3, p1, p1}, LX/1o9;-><init>(II)V

    .line 1400129
    iput-object p3, v4, LX/1bX;->c:LX/1o9;

    .line 1400130
    move-object v4, v4

    .line 1400131
    invoke-virtual {v4}, LX/1bX;->n()LX/1bf;

    move-result-object p1

    .line 1400132
    iget-object p3, v1, Lcom/facebook/stickers/ui/StickerTagItemView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v4, v1, Lcom/facebook/stickers/ui/StickerTagItemView;->a:LX/1Ad;

    sget-object p2, Lcom/facebook/stickers/ui/StickerTagItemView;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, p2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v4

    const/4 p2, 0x1

    invoke-virtual {v4, p2}, LX/1Ae;->c(Z)LX/1Ae;

    move-result-object v4

    check-cast v4, LX/1Ad;

    invoke-virtual {v4, p1}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v4

    check-cast v4, LX/1Ad;

    invoke-virtual {v4}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v4

    invoke-virtual {p3, v4}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1400133
    :goto_2
    new-instance v1, LX/8mt;

    invoke-direct {v1, p0, v0}, LX/8mt;-><init>(LX/8mu;Lcom/facebook/stickers/model/StickerTag;)V

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1400134
    move-object v0, v2

    .line 1400135
    return-object v0

    .line 1400136
    :cond_0
    iget-object v3, v0, Lcom/facebook/stickers/model/StickerTag;->b:Ljava/lang/String;

    move-object v3, v3

    .line 1400137
    goto/16 :goto_1

    :cond_1
    move-object v2, p2

    goto/16 :goto_0

    .line 1400138
    :cond_2
    iget-object v4, v1, Lcom/facebook/stickers/ui/StickerTagItemView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 p1, 0x0

    sget-object p3, Lcom/facebook/stickers/ui/StickerTagItemView;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, p1, p3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_2
.end method
