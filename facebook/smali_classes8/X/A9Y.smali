.class public final LX/A9Y;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 1635023
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_3

    .line 1635024
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1635025
    :goto_0
    return v1

    .line 1635026
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1635027
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_2

    .line 1635028
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1635029
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1635030
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 1635031
    const-string v3, "default_spec"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1635032
    const/4 v2, 0x0

    .line 1635033
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 1635034
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1635035
    :goto_2
    move v0, v2

    .line 1635036
    goto :goto_1

    .line 1635037
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1635038
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1635039
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    .line 1635040
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1635041
    :cond_5
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_6

    .line 1635042
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1635043
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1635044
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_5

    if-eqz v3, :cond_5

    .line 1635045
    const-string v4, "available_audiences"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1635046
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1635047
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v5, :cond_e

    .line 1635048
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1635049
    :goto_4
    move v0, v3

    .line 1635050
    goto :goto_3

    .line 1635051
    :cond_6
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1635052
    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1635053
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_2

    :cond_7
    move v0, v2

    goto :goto_3

    .line 1635054
    :cond_8
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_c

    .line 1635055
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1635056
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1635057
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_8

    if-eqz v7, :cond_8

    .line 1635058
    const-string v8, "count"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 1635059
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v6, v0

    move v0, v4

    goto :goto_5

    .line 1635060
    :cond_9
    const-string v8, "edges"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 1635061
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1635062
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->START_ARRAY:LX/15z;

    if-ne v7, v8, :cond_a

    .line 1635063
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_ARRAY:LX/15z;

    if-eq v7, v8, :cond_a

    .line 1635064
    const/4 v8, 0x0

    .line 1635065
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v9, :cond_12

    .line 1635066
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1635067
    :goto_7
    move v7, v8

    .line 1635068
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 1635069
    :cond_a
    invoke-static {v5, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v5

    move v5, v5

    .line 1635070
    goto :goto_5

    .line 1635071
    :cond_b
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_5

    .line 1635072
    :cond_c
    const/4 v7, 0x2

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1635073
    if-eqz v0, :cond_d

    .line 1635074
    invoke-virtual {p1, v3, v6, v3}, LX/186;->a(III)V

    .line 1635075
    :cond_d
    invoke-virtual {p1, v4, v5}, LX/186;->b(II)V

    .line 1635076
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_4

    :cond_e
    move v0, v3

    move v5, v3

    move v6, v3

    goto :goto_5

    .line 1635077
    :cond_f
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1635078
    :cond_10
    :goto_8
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_11

    .line 1635079
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1635080
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1635081
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_10

    if-eqz v9, :cond_10

    .line 1635082
    const-string v10, "node"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_f

    .line 1635083
    invoke-static {p0, p1}, LX/A9c;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_8

    .line 1635084
    :cond_11
    const/4 v9, 0x1

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1635085
    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 1635086
    invoke-virtual {p1}, LX/186;->d()I

    move-result v8

    goto :goto_7

    :cond_12
    move v7, v8

    goto :goto_8
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1635017
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1635018
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1635019
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/A9Y;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1635020
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1635021
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1635022
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 1634979
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1634980
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1634981
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1634982
    invoke-static {p0, p1}, LX/A9Y;->a(LX/15w;LX/186;)I

    move-result v1

    .line 1634983
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1634984
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1634985
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1634986
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1634987
    if-eqz v0, :cond_5

    .line 1634988
    const-string v1, "default_spec"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634989
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1634990
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1634991
    if-eqz v1, :cond_4

    .line 1634992
    const-string v2, "available_audiences"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634993
    const/4 v2, 0x0

    .line 1634994
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1634995
    invoke-virtual {p0, v1, v2, v2}, LX/15i;->a(III)I

    move-result v2

    .line 1634996
    if-eqz v2, :cond_0

    .line 1634997
    const-string v3, "count"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1634998
    invoke-virtual {p2, v2}, LX/0nX;->b(I)V

    .line 1634999
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1635000
    if-eqz v2, :cond_3

    .line 1635001
    const-string v3, "edges"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635002
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1635003
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {p0, v2}, LX/15i;->c(I)I

    move-result p1

    if-ge v3, p1, :cond_2

    .line 1635004
    invoke-virtual {p0, v2, v3}, LX/15i;->q(II)I

    move-result p1

    .line 1635005
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1635006
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1635007
    if-eqz v0, :cond_1

    .line 1635008
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1635009
    invoke-static {p0, v0, p2, p3}, LX/A9c;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1635010
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1635011
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1635012
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1635013
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1635014
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1635015
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1635016
    return-void
.end method
