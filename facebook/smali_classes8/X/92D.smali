.class public LX/92D;
.super LX/92A;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/92D;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    .line 1432100
    const-string v0, "minutiae_objects_selector_time_to_fetch_end"

    const-string v1, "minutiae_objects_selector_time_to_fetch_end_cached"

    const-string v2, "minutiae_objects_selector_time_to_results_shown"

    const-string v3, "minutiae_objects_selector_time_to_results_shown_cached"

    const-string v4, "minutiae_objects_selector_fetch_time"

    const-string v5, "minutiae_objects_selector_fetch_time_cached"

    invoke-static/range {v0 .. v5}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    sput-object v0, LX/92D;->a:Ljava/util/Map;

    .line 1432101
    const v0, 0xc60003

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const v1, 0xc60006

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0xc60004

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0xc60007

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0xc60005

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const v5, 0xc60008

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static/range {v0 .. v5}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    sput-object v0, LX/92D;->b:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/performancelogger/PerformanceLogger;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1432102
    sget-object v0, LX/92D;->a:Ljava/util/Map;

    sget-object v1, LX/92D;->b:Ljava/util/Map;

    invoke-direct {p0, p1, v0, v1}, LX/92A;-><init>(Lcom/facebook/performancelogger/PerformanceLogger;Ljava/util/Map;Ljava/util/Map;)V

    .line 1432103
    return-void
.end method

.method public static a(LX/0QB;)LX/92D;
    .locals 4

    .prologue
    .line 1432104
    sget-object v0, LX/92D;->c:LX/92D;

    if-nez v0, :cond_1

    .line 1432105
    const-class v1, LX/92D;

    monitor-enter v1

    .line 1432106
    :try_start_0
    sget-object v0, LX/92D;->c:LX/92D;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1432107
    if-eqz v2, :cond_0

    .line 1432108
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1432109
    new-instance p0, LX/92D;

    invoke-static {v0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v3

    check-cast v3, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-direct {p0, v3}, LX/92D;-><init>(Lcom/facebook/performancelogger/PerformanceLogger;)V

    .line 1432110
    move-object v0, p0

    .line 1432111
    sput-object v0, LX/92D;->c:LX/92D;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1432112
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1432113
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1432114
    :cond_1
    sget-object v0, LX/92D;->c:LX/92D;

    return-object v0

    .line 1432115
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1432116
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1432117
    const v0, 0xc60001

    const-string v1, "minutiae_objects_selector_time_to_init"

    invoke-virtual {p0, v0, v1}, LX/92A;->c(ILjava/lang/String;)V

    .line 1432118
    const v0, 0xc60002

    const-string v1, "minutiae_objects_selector_time_to_fetch_start"

    invoke-virtual {p0, v0, v1}, LX/92A;->c(ILjava/lang/String;)V

    .line 1432119
    const v0, 0xc60003

    const-string v1, "minutiae_objects_selector_time_to_fetch_end"

    invoke-virtual {p0, v0, v1}, LX/92A;->c(ILjava/lang/String;)V

    .line 1432120
    const v0, 0xc60004

    const-string v1, "minutiae_objects_selector_time_to_results_shown"

    invoke-virtual {p0, v0, v1}, LX/92A;->c(ILjava/lang/String;)V

    .line 1432121
    return-void
.end method

.method public final a(LX/0ta;)V
    .locals 2

    .prologue
    .line 1432122
    const v0, 0xc60003

    const-string v1, "minutiae_objects_selector_time_to_fetch_end"

    invoke-virtual {p0, v0, v1, p1}, LX/92A;->a(ILjava/lang/String;LX/0ta;)V

    .line 1432123
    const v0, 0xc60005

    const-string v1, "minutiae_objects_selector_fetch_time"

    invoke-virtual {p0, v0, v1, p1}, LX/92A;->a(ILjava/lang/String;LX/0ta;)V

    .line 1432124
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 1432125
    const v0, 0xc60002

    const-string v1, "minutiae_objects_selector_time_to_fetch_start"

    invoke-virtual {p0, v0, v1}, LX/92A;->b(ILjava/lang/String;)V

    .line 1432126
    const v0, 0xc60005

    const-string v1, "minutiae_objects_selector_fetch_time"

    invoke-virtual {p0, v0, v1}, LX/92A;->c(ILjava/lang/String;)V

    .line 1432127
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 1432128
    const v0, 0xc60003

    const-string v1, "minutiae_objects_selector_time_to_fetch_end"

    invoke-virtual {p0, v0, v1}, LX/92A;->a(ILjava/lang/String;)V

    .line 1432129
    const v0, 0xc60004

    const-string v1, "minutiae_objects_selector_time_to_results_shown"

    invoke-virtual {p0, v0, v1}, LX/92A;->a(ILjava/lang/String;)V

    .line 1432130
    const v0, 0xc60005

    const-string v1, "minutiae_objects_selector_fetch_time"

    invoke-virtual {p0, v0, v1}, LX/92A;->a(ILjava/lang/String;)V

    .line 1432131
    return-void
.end method
