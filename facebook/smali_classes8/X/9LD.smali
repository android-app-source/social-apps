.class public final LX/9LD;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1470226
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_8

    .line 1470227
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1470228
    :goto_0
    return v1

    .line 1470229
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_4

    .line 1470230
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1470231
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1470232
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_0

    if-eqz v8, :cond_0

    .line 1470233
    const-string v9, "is_compulsory"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 1470234
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v4

    move v7, v4

    move v4, v2

    goto :goto_1

    .line 1470235
    :cond_1
    const-string v9, "use_neighborhood_datasource"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 1470236
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v6, v3

    move v3, v2

    goto :goto_1

    .line 1470237
    :cond_2
    const-string v9, "use_zip_code"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1470238
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v5, v0

    move v0, v2

    goto :goto_1

    .line 1470239
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1470240
    :cond_4
    const/4 v8, 0x3

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1470241
    if-eqz v4, :cond_5

    .line 1470242
    invoke-virtual {p1, v1, v7}, LX/186;->a(IZ)V

    .line 1470243
    :cond_5
    if-eqz v3, :cond_6

    .line 1470244
    invoke-virtual {p1, v2, v6}, LX/186;->a(IZ)V

    .line 1470245
    :cond_6
    if-eqz v0, :cond_7

    .line 1470246
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v5}, LX/186;->a(IZ)V

    .line 1470247
    :cond_7
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_8
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    .line 1470248
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1470249
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1470250
    if-eqz v0, :cond_0

    .line 1470251
    const-string v1, "is_compulsory"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1470252
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1470253
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1470254
    if-eqz v0, :cond_1

    .line 1470255
    const-string v1, "use_neighborhood_datasource"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1470256
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1470257
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1470258
    if-eqz v0, :cond_2

    .line 1470259
    const-string v1, "use_zip_code"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1470260
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1470261
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1470262
    return-void
.end method
