.class public LX/9iE;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1527542
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1527543
    iput-object p1, p0, LX/9iE;->a:LX/0Zb;

    .line 1527544
    return-void
.end method

.method public static a(LX/0QB;)LX/9iE;
    .locals 2

    .prologue
    .line 1527545
    new-instance v1, LX/9iE;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/9iE;-><init>(LX/0Zb;)V

    .line 1527546
    move-object v0, v1

    .line 1527547
    return-object v0
.end method

.method public static a(LX/9iE;Ljava/lang/String;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;LX/5kD;)V
    .locals 3

    .prologue
    .line 1527548
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "session_id"

    invoke-virtual {p2}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "place_id"

    invoke-virtual {p2}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;->a()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "photo_id"

    invoke-interface {p3}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "photo_owner_id"

    invoke-interface {p3}, LX/5kD;->Z()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1527549
    iget-object v1, p0, LX/9iE;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1527550
    return-void
.end method
