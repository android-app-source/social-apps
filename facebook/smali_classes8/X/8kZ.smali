.class public final LX/8kZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/8kb;


# direct methods
.method public constructor <init>(LX/8kb;)V
    .locals 0

    .prologue
    .line 1396527
    iput-object p1, p0, LX/8kZ;->a:LX/8kb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x1399e573

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1396528
    iget-object v1, p0, LX/8kZ;->a:LX/8kb;

    iget-object v1, v1, LX/8kb;->d:LX/8l8;

    if-eqz v1, :cond_0

    .line 1396529
    iget-object v1, p0, LX/8kZ;->a:LX/8kb;

    iget-object v1, v1, LX/8kb;->d:LX/8l8;

    .line 1396530
    iget-object v3, v1, LX/8l8;->b:Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;

    iget-object v3, v3, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->g:LX/8j6;

    iget-object v4, v1, LX/8l8;->a:LX/8km;

    iget-object v4, v4, LX/8km;->a:Lcom/facebook/stickers/model/StickerPack;

    .line 1396531
    iget-object p0, v4, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v4, p0

    .line 1396532
    const-string p0, "download_button_pressed"

    invoke-static {v3, p0}, LX/8j6;->d(LX/8j6;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    .line 1396533
    const-string p1, "pack_id"

    invoke-virtual {p0, p1, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1396534
    iget-object p1, v3, LX/8j6;->a:LX/0Zb;

    invoke-interface {p1, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1396535
    iget-object v3, v1, LX/8l8;->b:Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;

    iget-object v3, v3, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->o:LX/8kq;

    if-eqz v3, :cond_0

    .line 1396536
    iget-object v3, v1, LX/8l8;->b:Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;

    iget-object v3, v3, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->o:LX/8kq;

    iget-object v4, v1, LX/8l8;->a:LX/8km;

    iget-object v4, v4, LX/8km;->a:Lcom/facebook/stickers/model/StickerPack;

    invoke-virtual {v3, v4}, LX/8kq;->b(Lcom/facebook/stickers/model/StickerPack;)V

    .line 1396537
    :cond_0
    const v1, -0x4b248ca9

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
