.class public final LX/A9t;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 43

    .prologue
    .line 1636282
    const/16 v36, 0x0

    .line 1636283
    const/16 v35, 0x0

    .line 1636284
    const/16 v34, 0x0

    .line 1636285
    const/16 v33, 0x0

    .line 1636286
    const/16 v32, 0x0

    .line 1636287
    const/16 v31, 0x0

    .line 1636288
    const/16 v30, 0x0

    .line 1636289
    const/16 v29, 0x0

    .line 1636290
    const/16 v28, 0x0

    .line 1636291
    const/16 v27, 0x0

    .line 1636292
    const/16 v26, 0x0

    .line 1636293
    const/16 v25, 0x0

    .line 1636294
    const/16 v24, 0x0

    .line 1636295
    const/16 v23, 0x0

    .line 1636296
    const/16 v22, 0x0

    .line 1636297
    const/16 v21, 0x0

    .line 1636298
    const/16 v20, 0x0

    .line 1636299
    const/16 v19, 0x0

    .line 1636300
    const/16 v18, 0x0

    .line 1636301
    const/16 v17, 0x0

    .line 1636302
    const/16 v16, 0x0

    .line 1636303
    const/4 v11, 0x0

    .line 1636304
    const-wide/16 v14, 0x0

    .line 1636305
    const-wide/16 v12, 0x0

    .line 1636306
    const/4 v10, 0x0

    .line 1636307
    const/4 v9, 0x0

    .line 1636308
    const/4 v8, 0x0

    .line 1636309
    const/4 v7, 0x0

    .line 1636310
    const/4 v6, 0x0

    .line 1636311
    const/4 v5, 0x0

    .line 1636312
    const/4 v4, 0x0

    .line 1636313
    const/4 v3, 0x0

    .line 1636314
    const/4 v2, 0x0

    .line 1636315
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v37

    sget-object v38, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v37

    move-object/from16 v1, v38

    if-eq v0, v1, :cond_23

    .line 1636316
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1636317
    const/4 v2, 0x0

    .line 1636318
    :goto_0
    return v2

    .line 1636319
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v38, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v38

    if-eq v2, v0, :cond_1c

    .line 1636320
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1636321
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1636322
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v38

    sget-object v39, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v38

    move-object/from16 v1, v39

    if-eq v0, v1, :cond_0

    if-eqz v2, :cond_0

    .line 1636323
    const-string v38, "ad_account"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_1

    .line 1636324
    invoke-static/range {p0 .. p1}, LX/A9Q;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v37, v2

    goto :goto_1

    .line 1636325
    :cond_1
    const-string v38, "audience"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_2

    .line 1636326
    invoke-static/range {p0 .. p1}, LX/A9c;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v36, v2

    goto :goto_1

    .line 1636327
    :cond_2
    const-string v38, "aymt_lwi_channel"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_3

    .line 1636328
    invoke-static/range {p0 .. p1}, LX/A9h;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v35, v2

    goto :goto_1

    .line 1636329
    :cond_3
    const-string v38, "bid_amount"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_4

    .line 1636330
    const/4 v2, 0x1

    .line 1636331
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v10

    move/from16 v34, v10

    move v10, v2

    goto :goto_1

    .line 1636332
    :cond_4
    const-string v38, "boosting_status"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_5

    .line 1636333
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v33, v2

    goto :goto_1

    .line 1636334
    :cond_5
    const-string v38, "budget"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_6

    .line 1636335
    invoke-static/range {p0 .. p1}, LX/AA2;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v32, v2

    goto/16 :goto_1

    .line 1636336
    :cond_6
    const-string v38, "budget_type"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_7

    .line 1636337
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v31, v2

    goto/16 :goto_1

    .line 1636338
    :cond_7
    const-string v38, "campaign_group"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_8

    .line 1636339
    invoke-static/range {p0 .. p1}, LX/AA0;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v30, v2

    goto/16 :goto_1

    .line 1636340
    :cond_8
    const-string v38, "creative"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_9

    .line 1636341
    invoke-static/range {p0 .. p1}, LX/A9g;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v29, v2

    goto/16 :goto_1

    .line 1636342
    :cond_9
    const-string v38, "default_spec"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_a

    .line 1636343
    invoke-static/range {p0 .. p1}, LX/A9p;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v28, v2

    goto/16 :goto_1

    .line 1636344
    :cond_a
    const-string v38, "eligible_objectives"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_b

    .line 1636345
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v27, v2

    goto/16 :goto_1

    .line 1636346
    :cond_b
    const-string v38, "feed_unit_preview"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_c

    .line 1636347
    invoke-static/range {p0 .. p1}, LX/2ao;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v26, v2

    goto/16 :goto_1

    .line 1636348
    :cond_c
    const-string v38, "insights"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_d

    .line 1636349
    invoke-static/range {p0 .. p1}, LX/A9r;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v25, v2

    goto/16 :goto_1

    .line 1636350
    :cond_d
    const-string v38, "instant_workflow_ctas"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_e

    .line 1636351
    invoke-static/range {p0 .. p1}, LX/A9Z;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v24, v2

    goto/16 :goto_1

    .line 1636352
    :cond_e
    const-string v38, "is_boosted_slideshow"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_f

    .line 1636353
    const/4 v2, 0x1

    .line 1636354
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    move/from16 v23, v9

    move v9, v2

    goto/16 :goto_1

    .line 1636355
    :cond_f
    const-string v38, "is_eligible_for_action_buttons"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_10

    .line 1636356
    const/4 v2, 0x1

    .line 1636357
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move/from16 v22, v7

    move v7, v2

    goto/16 :goto_1

    .line 1636358
    :cond_10
    const-string v38, "is_pacing_editable"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_11

    .line 1636359
    const/4 v2, 0x1

    .line 1636360
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move/from16 v21, v6

    move v6, v2

    goto/16 :goto_1

    .line 1636361
    :cond_11
    const-string v38, "objective"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_12

    .line 1636362
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 1636363
    :cond_12
    const-string v38, "pacing_type"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_13

    .line 1636364
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 1636365
    :cond_13
    const-string v38, "post_attachment_type"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_14

    .line 1636366
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 1636367
    :cond_14
    const-string v38, "rejection_reason"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_15

    .line 1636368
    invoke-static/range {p0 .. p1}, LX/4ap;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 1636369
    :cond_15
    const-string v38, "spent"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_16

    .line 1636370
    invoke-static/range {p0 .. p1}, LX/AA2;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v16, v2

    goto/16 :goto_1

    .line 1636371
    :cond_16
    const-string v38, "start_time"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_17

    .line 1636372
    const/4 v2, 0x1

    .line 1636373
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 1636374
    :cond_17
    const-string v38, "stop_time"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_18

    .line 1636375
    const/4 v2, 0x1

    .line 1636376
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v14

    move v8, v2

    goto/16 :goto_1

    .line 1636377
    :cond_18
    const-string v38, "target_spec"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_19

    .line 1636378
    invoke-static/range {p0 .. p1}, LX/AAU;->a(LX/15w;LX/186;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 1636379
    :cond_19
    const-string v38, "targeting_description"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_1a

    .line 1636380
    invoke-static/range {p0 .. p1}, LX/A9s;->a(LX/15w;LX/186;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 1636381
    :cond_1a
    const-string v38, "targeting_types"

    move-object/from16 v0, v38

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 1636382
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 1636383
    :cond_1b
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1636384
    :cond_1c
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1636385
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1636386
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1636387
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1636388
    if-eqz v10, :cond_1d

    .line 1636389
    const/4 v2, 0x3

    const/4 v10, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1, v10}, LX/186;->a(III)V

    .line 1636390
    :cond_1d
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1636391
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1636392
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1636393
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1636394
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1636395
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1636396
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1636397
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1636398
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1636399
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1636400
    if-eqz v9, :cond_1e

    .line 1636401
    const/16 v2, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1636402
    :cond_1e
    if-eqz v7, :cond_1f

    .line 1636403
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1636404
    :cond_1f
    if-eqz v6, :cond_20

    .line 1636405
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1636406
    :cond_20
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1636407
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1636408
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1636409
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1636410
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1636411
    if-eqz v3, :cond_21

    .line 1636412
    const/16 v3, 0x16

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1636413
    :cond_21
    if-eqz v8, :cond_22

    .line 1636414
    const/16 v3, 0x17

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v14

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1636415
    :cond_22
    const/16 v2, 0x18

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1636416
    const/16 v2, 0x19

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1636417
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1636418
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_23
    move/from16 v37, v36

    move/from16 v36, v35

    move/from16 v35, v34

    move/from16 v34, v33

    move/from16 v33, v32

    move/from16 v32, v31

    move/from16 v31, v30

    move/from16 v30, v29

    move/from16 v29, v28

    move/from16 v28, v27

    move/from16 v27, v26

    move/from16 v26, v25

    move/from16 v25, v24

    move/from16 v24, v23

    move/from16 v23, v22

    move/from16 v22, v21

    move/from16 v21, v20

    move/from16 v20, v19

    move/from16 v19, v18

    move/from16 v18, v17

    move/from16 v17, v16

    move/from16 v16, v11

    move v11, v8

    move v8, v2

    move/from16 v40, v10

    move v10, v7

    move v7, v5

    move-wide/from16 v41, v14

    move-wide v14, v12

    move/from16 v13, v40

    move v12, v9

    move v9, v6

    move v6, v4

    move-wide/from16 v4, v41

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1636419
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1636420
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1636421
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/A9t;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1636422
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1636423
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1636424
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 1636276
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1636277
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1636278
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1636279
    invoke-static {p0, p1}, LX/A9t;->a(LX/15w;LX/186;)I

    move-result v1

    .line 1636280
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1636281
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 8

    .prologue
    const/16 v7, 0xa

    const/4 v6, 0x6

    const/4 v3, 0x4

    const/4 v2, 0x0

    const-wide/16 v4, 0x0

    .line 1636161
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1636162
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1636163
    if-eqz v0, :cond_0

    .line 1636164
    const-string v1, "ad_account"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636165
    invoke-static {p0, v0, p2, p3}, LX/A9Q;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1636166
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1636167
    if-eqz v0, :cond_1

    .line 1636168
    const-string v1, "audience"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636169
    invoke-static {p0, v0, p2, p3}, LX/A9c;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1636170
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1636171
    if-eqz v0, :cond_2

    .line 1636172
    const-string v1, "aymt_lwi_channel"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636173
    invoke-static {p0, v0, p2, p3}, LX/A9h;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1636174
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1636175
    if-eqz v0, :cond_3

    .line 1636176
    const-string v1, "bid_amount"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636177
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1636178
    :cond_3
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1636179
    if-eqz v0, :cond_4

    .line 1636180
    const-string v0, "boosting_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636181
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1636182
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1636183
    if-eqz v0, :cond_5

    .line 1636184
    const-string v1, "budget"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636185
    invoke-static {p0, v0, p2}, LX/AA2;->a(LX/15i;ILX/0nX;)V

    .line 1636186
    :cond_5
    invoke-virtual {p0, p1, v6}, LX/15i;->g(II)I

    move-result v0

    .line 1636187
    if-eqz v0, :cond_6

    .line 1636188
    const-string v0, "budget_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636189
    invoke-virtual {p0, p1, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1636190
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1636191
    if-eqz v0, :cond_7

    .line 1636192
    const-string v1, "campaign_group"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636193
    invoke-static {p0, v0, p2, p3}, LX/AA0;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1636194
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1636195
    if-eqz v0, :cond_8

    .line 1636196
    const-string v1, "creative"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636197
    invoke-static {p0, v0, p2, p3}, LX/A9g;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1636198
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1636199
    if-eqz v0, :cond_9

    .line 1636200
    const-string v1, "default_spec"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636201
    invoke-static {p0, v0, p2, p3}, LX/A9p;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1636202
    :cond_9
    invoke-virtual {p0, p1, v7}, LX/15i;->g(II)I

    move-result v0

    .line 1636203
    if-eqz v0, :cond_a

    .line 1636204
    const-string v0, "eligible_objectives"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636205
    invoke-virtual {p0, p1, v7}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1636206
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1636207
    if-eqz v0, :cond_b

    .line 1636208
    const-string v1, "feed_unit_preview"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636209
    invoke-static {p0, v0, p2, p3}, LX/2ao;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1636210
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1636211
    if-eqz v0, :cond_c

    .line 1636212
    const-string v1, "insights"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636213
    invoke-static {p0, v0, p2, p3}, LX/A9r;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1636214
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1636215
    if-eqz v0, :cond_e

    .line 1636216
    const-string v1, "instant_workflow_ctas"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636217
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1636218
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_d

    .line 1636219
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2}, LX/A9Z;->a(LX/15i;ILX/0nX;)V

    .line 1636220
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1636221
    :cond_d
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1636222
    :cond_e
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1636223
    if-eqz v0, :cond_f

    .line 1636224
    const-string v1, "is_boosted_slideshow"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636225
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1636226
    :cond_f
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1636227
    if-eqz v0, :cond_10

    .line 1636228
    const-string v1, "is_eligible_for_action_buttons"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636229
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1636230
    :cond_10
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1636231
    if-eqz v0, :cond_11

    .line 1636232
    const-string v1, "is_pacing_editable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636233
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1636234
    :cond_11
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1636235
    if-eqz v0, :cond_12

    .line 1636236
    const-string v0, "objective"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636237
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1636238
    :cond_12
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1636239
    if-eqz v0, :cond_13

    .line 1636240
    const-string v0, "pacing_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636241
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1636242
    :cond_13
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1636243
    if-eqz v0, :cond_14

    .line 1636244
    const-string v0, "post_attachment_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636245
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1636246
    :cond_14
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1636247
    if-eqz v0, :cond_15

    .line 1636248
    const-string v1, "rejection_reason"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636249
    invoke-static {p0, v0, p2, p3}, LX/4ap;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1636250
    :cond_15
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1636251
    if-eqz v0, :cond_16

    .line 1636252
    const-string v1, "spent"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636253
    invoke-static {p0, v0, p2}, LX/AA2;->a(LX/15i;ILX/0nX;)V

    .line 1636254
    :cond_16
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1636255
    cmp-long v2, v0, v4

    if-eqz v2, :cond_17

    .line 1636256
    const-string v2, "start_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636257
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1636258
    :cond_17
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1636259
    cmp-long v2, v0, v4

    if-eqz v2, :cond_18

    .line 1636260
    const-string v2, "stop_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636261
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1636262
    :cond_18
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1636263
    if-eqz v0, :cond_19

    .line 1636264
    const-string v1, "target_spec"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636265
    invoke-static {p0, v0, p2, p3}, LX/AAU;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1636266
    :cond_19
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1636267
    if-eqz v0, :cond_1a

    .line 1636268
    const-string v1, "targeting_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636269
    invoke-static {p0, v0, p2, p3}, LX/A9s;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1636270
    :cond_1a
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1636271
    if-eqz v0, :cond_1b

    .line 1636272
    const-string v0, "targeting_types"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1636273
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1636274
    :cond_1b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1636275
    return-void
.end method
