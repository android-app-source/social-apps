.class public final LX/8kz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Mb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3Mb",
        "<",
        "LX/8jW;",
        "LX/8jX;",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/stickers/keyboard/StickerPackPageView;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/keyboard/StickerPackPageView;)V
    .locals 0

    .prologue
    .line 1397270
    iput-object p1, p0, LX/8kz;->a:Lcom/facebook/stickers/keyboard/StickerPackPageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 3

    .prologue
    .line 1397271
    iget-object v0, p0, LX/8kz;->a:Lcom/facebook/stickers/keyboard/StickerPackPageView;

    iget-object v0, v0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->i:Landroid/widget/GridView;

    iget-object v1, p0, LX/8kz;->a:Lcom/facebook/stickers/keyboard/StickerPackPageView;

    const v2, 0x7f0d1327

    invoke-virtual {v1, v2}, Lcom/facebook/stickers/keyboard/StickerPackPageView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setEmptyView(Landroid/view/View;)V

    .line 1397272
    iget-object v0, p0, LX/8kz;->a:Lcom/facebook/stickers/keyboard/StickerPackPageView;

    iget-object v0, v0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->p:LX/8l5;

    if-eqz v0, :cond_1

    .line 1397273
    iget-object v0, p0, LX/8kz;->a:Lcom/facebook/stickers/keyboard/StickerPackPageView;

    iget-object v0, v0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->p:LX/8l5;

    iget-object v1, p0, LX/8kz;->a:Lcom/facebook/stickers/keyboard/StickerPackPageView;

    iget-object v1, v1, Lcom/facebook/stickers/keyboard/StickerPackPageView;->q:Ljava/lang/String;

    .line 1397274
    iget-object v2, v0, LX/8l5;->a:Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;

    iget-object v2, v2, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->o:LX/8kq;

    if-eqz v2, :cond_1

    iget-object v2, v0, LX/8l5;->a:Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;

    iget-object v2, v2, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->x:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1397275
    iget-object v2, v0, LX/8l5;->a:Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;

    iget-object v2, v2, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->o:LX/8kq;

    .line 1397276
    iget-object v1, v2, LX/8kq;->a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    iget-object v1, v1, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->f:LX/8ky;

    if-eqz v1, :cond_0

    .line 1397277
    :cond_0
    iget-object v2, v0, LX/8l5;->a:Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;

    iget-object v2, v2, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->d:LX/0Sg;

    invoke-virtual {v2, p2}, LX/0Sg;->a(Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 1397278
    :cond_1
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1397279
    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1397280
    check-cast p2, LX/8jX;

    .line 1397281
    iget-object v0, p0, LX/8kz;->a:Lcom/facebook/stickers/keyboard/StickerPackPageView;

    iget-object v0, v0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->m:Lcom/facebook/stickers/ui/StickerGridViewAdapter;

    iget-object v1, p2, LX/8jX;->a:Ljava/util/List;

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/stickers/ui/StickerGridViewAdapter;->a(LX/0Px;)V

    .line 1397282
    iget-object v0, p0, LX/8kz;->a:Lcom/facebook/stickers/keyboard/StickerPackPageView;

    invoke-static {v0}, Lcom/facebook/stickers/keyboard/StickerPackPageView;->a(Lcom/facebook/stickers/keyboard/StickerPackPageView;)V

    .line 1397283
    iget-object v0, p0, LX/8kz;->a:Lcom/facebook/stickers/keyboard/StickerPackPageView;

    iget-object v0, v0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->p:LX/8l5;

    if-eqz v0, :cond_0

    .line 1397284
    iget-object v0, p0, LX/8kz;->a:Lcom/facebook/stickers/keyboard/StickerPackPageView;

    iget-object v0, v0, Lcom/facebook/stickers/keyboard/StickerPackPageView;->p:LX/8l5;

    iget-object v1, p0, LX/8kz;->a:Lcom/facebook/stickers/keyboard/StickerPackPageView;

    iget-object v1, v1, Lcom/facebook/stickers/keyboard/StickerPackPageView;->q:Ljava/lang/String;

    .line 1397285
    iget-object p0, v0, LX/8l5;->a:Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;

    iget-object p0, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->o:LX/8kq;

    if-eqz p0, :cond_0

    iget-object p0, v0, LX/8l5;->a:Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;

    iget-object p0, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->x:Ljava/lang/String;

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 1397286
    iget-object p0, v0, LX/8l5;->a:Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;

    iget-object p0, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->o:LX/8kq;

    invoke-virtual {p0}, LX/8kq;->c()V

    .line 1397287
    iget-object p0, v0, LX/8l5;->a:Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;

    const/4 p1, 0x0

    .line 1397288
    iput-object p1, p0, Lcom/facebook/stickers/keyboard/StickerTabbedPagerAdapter;->x:Ljava/lang/String;

    .line 1397289
    :cond_0
    return-void
.end method

.method public final bridge synthetic c(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1397290
    return-void
.end method
