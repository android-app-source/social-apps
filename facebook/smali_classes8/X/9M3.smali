.class public LX/9M3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0sZ;


# direct methods
.method public constructor <init>(LX/0sZ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1472367
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1472368
    iput-object p1, p0, LX/9M3;->a:LX/0sZ;

    .line 1472369
    return-void
.end method

.method public static b(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1472370
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1472371
    invoke-virtual {p0}, Lcom/facebook/graphql/executor/GraphQLResult;->d()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1472372
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1472373
    :cond_0
    new-instance v0, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1472374
    iget-object v2, p0, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v2, v2

    .line 1472375
    iget-wide v6, p0, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v4, v6

    .line 1472376
    invoke-direct {v0, v1, v2, v4, v5}, Lcom/facebook/graphql/executor/GraphQLResult;-><init>(Ljava/lang/Object;LX/0ta;J)V

    return-object v0
.end method
