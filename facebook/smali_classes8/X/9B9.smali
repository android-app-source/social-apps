.class public LX/9B9;
.super LX/1Cv;
.source ""


# instance fields
.field private final a:LX/55h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/55h",
            "<",
            "LX/9Al;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/model/GraphQLProfileDiscoveryBucket;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:Ljava/lang/String;

.field private final d:Z

.field private final e:Z

.field private final f:LX/8s1;

.field private final g:Lcom/facebook/ipc/feed/ViewPermalinkParams;

.field private final h:LX/82m;

.field public final i:LX/17W;

.field public j:Landroid/view/View$OnClickListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/55h;Ljava/lang/String;ZZLX/8s1;Lcom/facebook/ipc/feed/ViewPermalinkParams;LX/82m;LX/17W;)V
    .locals 0
    .param p6    # Lcom/facebook/ipc/feed/ViewPermalinkParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # LX/82m;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/55h",
            "<",
            "LX/9Al;",
            ">;",
            "Ljava/lang/String;",
            "ZZ",
            "LX/8s1;",
            "Lcom/facebook/ipc/feed/ViewPermalinkParams;",
            "LX/82m;",
            "LX/17W;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1451383
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 1451384
    iput-object p1, p0, LX/9B9;->a:LX/55h;

    .line 1451385
    iput-object p2, p0, LX/9B9;->c:Ljava/lang/String;

    .line 1451386
    iput-boolean p3, p0, LX/9B9;->d:Z

    .line 1451387
    iput-boolean p4, p0, LX/9B9;->e:Z

    .line 1451388
    iput-object p5, p0, LX/9B9;->f:LX/8s1;

    .line 1451389
    iput-object p6, p0, LX/9B9;->g:Lcom/facebook/ipc/feed/ViewPermalinkParams;

    .line 1451390
    iput-object p7, p0, LX/9B9;->h:LX/82m;

    .line 1451391
    iput-object p8, p0, LX/9B9;->i:LX/17W;

    .line 1451392
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1451375
    packed-switch p1, :pswitch_data_0

    .line 1451376
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected itemViewType: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1451377
    :pswitch_0
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031188

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedback/reactions/ui/ProfileDiscoveryEntryPointRowView;

    .line 1451378
    iget-object v1, p0, LX/9B9;->j:Landroid/view/View$OnClickListener;

    if-nez v1, :cond_0

    .line 1451379
    new-instance v1, LX/9B8;

    invoke-direct {v1, p0}, LX/9B8;-><init>(LX/9B9;)V

    iput-object v1, p0, LX/9B9;->j:Landroid/view/View$OnClickListener;

    .line 1451380
    :cond_0
    iget-object v1, p0, LX/9B9;->j:Landroid/view/View$OnClickListener;

    move-object v1, v1

    .line 1451381
    invoke-virtual {v0, v1}, Lcom/facebook/feedback/reactions/ui/ProfileDiscoveryEntryPointRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1451382
    :goto_0
    return-object v0

    :pswitch_1
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03118c

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 8

    .prologue
    .line 1451370
    packed-switch p4, :pswitch_data_0

    .line 1451371
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected itemViewType: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    move-object v1, p2

    .line 1451372
    check-cast v1, LX/9Al;

    move-object v0, p3

    .line 1451373
    check-cast v0, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;

    iget-object v2, p0, LX/9B9;->c:Ljava/lang/String;

    iget-boolean v3, p0, LX/9B9;->d:Z

    iget-boolean v4, p0, LX/9B9;->e:Z

    iget-object v5, p0, LX/9B9;->f:LX/8s1;

    iget-object v6, p0, LX/9B9;->g:Lcom/facebook/ipc/feed/ViewPermalinkParams;

    iget-object v7, p0, LX/9B9;->h:LX/82m;

    invoke-virtual/range {v0 .. v7}, Lcom/facebook/feedback/reactions/ui/ReactorsRowView;->a(LX/9Al;Ljava/lang/String;ZZLX/8s1;Lcom/facebook/ipc/feed/ViewPermalinkParams;LX/82m;)V

    .line 1451374
    :pswitch_1
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 1451369
    iget-object v0, p0, LX/9B9;->a:LX/55h;

    invoke-virtual {v0}, LX/55h;->c()I

    move-result v1

    iget-object v0, p0, LX/9B9;->b:Lcom/facebook/graphql/model/GraphQLProfileDiscoveryBucket;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1451364
    iget-object v0, p0, LX/9B9;->b:Lcom/facebook/graphql/model/GraphQLProfileDiscoveryBucket;

    if-eqz v0, :cond_1

    .line 1451365
    if-nez p1, :cond_0

    iget-object v0, p0, LX/9B9;->b:Lcom/facebook/graphql/model/GraphQLProfileDiscoveryBucket;

    .line 1451366
    :goto_0
    return-object v0

    .line 1451367
    :cond_0
    iget-object v0, p0, LX/9B9;->a:LX/55h;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, LX/55h;->a(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 1451368
    :cond_1
    iget-object v0, p0, LX/9B9;->a:LX/55h;

    invoke-virtual {v0, p1}, LX/55h;->a(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1451363
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1451361
    if-nez p1, :cond_0

    iget-object v0, p0, LX/9B9;->b:Lcom/facebook/graphql/model/GraphQLProfileDiscoveryBucket;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 1451362
    const/4 v0, 0x2

    return v0
.end method
