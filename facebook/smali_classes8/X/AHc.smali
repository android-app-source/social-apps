.class public final enum LX/AHc;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/AHc;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/AHc;

.field public static final enum POST:LX/AHc;

.field public static final enum SUBSCRIPTION:LX/AHc;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1655761
    new-instance v0, LX/AHc;

    const-string v1, "POST"

    invoke-direct {v0, v1, v2}, LX/AHc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AHc;->POST:LX/AHc;

    .line 1655762
    new-instance v0, LX/AHc;

    const-string v1, "SUBSCRIPTION"

    invoke-direct {v0, v1, v3}, LX/AHc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AHc;->SUBSCRIPTION:LX/AHc;

    .line 1655763
    const/4 v0, 0x2

    new-array v0, v0, [LX/AHc;

    sget-object v1, LX/AHc;->POST:LX/AHc;

    aput-object v1, v0, v2

    sget-object v1, LX/AHc;->SUBSCRIPTION:LX/AHc;

    aput-object v1, v0, v3

    sput-object v0, LX/AHc;->$VALUES:[LX/AHc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1655764
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/AHc;
    .locals 1

    .prologue
    .line 1655760
    const-class v0, LX/AHc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/AHc;

    return-object v0
.end method

.method public static values()[LX/AHc;
    .locals 1

    .prologue
    .line 1655759
    sget-object v0, LX/AHc;->$VALUES:[LX/AHc;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/AHc;

    return-object v0
.end method
