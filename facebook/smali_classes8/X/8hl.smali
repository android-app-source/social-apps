.class public final LX/8hl;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1390679
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_5

    .line 1390680
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1390681
    :goto_0
    return v1

    .line 1390682
    :cond_0
    const-string v6, "selected"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1390683
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v3, v0

    move v0, v2

    .line 1390684
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_3

    .line 1390685
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1390686
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1390687
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 1390688
    const-string v6, "node"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1390689
    invoke-static {p0, p1}, LX/8hp;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1390690
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1390691
    :cond_3
    const/4 v5, 0x2

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1390692
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1390693
    if-eqz v0, :cond_4

    .line 1390694
    invoke-virtual {p1, v2, v3}, LX/186;->a(IZ)V

    .line 1390695
    :cond_4
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1390696
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1390697
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1390698
    if-eqz v0, :cond_0

    .line 1390699
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1390700
    invoke-static {p0, v0, p2, p3}, LX/8hp;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1390701
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1390702
    if-eqz v0, :cond_1

    .line 1390703
    const-string v1, "selected"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1390704
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1390705
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1390706
    return-void
.end method
