.class public final LX/8xA;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/8xB;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public final synthetic b:LX/8xB;


# direct methods
.method public constructor <init>(LX/8xB;)V
    .locals 1

    .prologue
    .line 1423605
    iput-object p1, p0, LX/8xA;->b:LX/8xB;

    .line 1423606
    move-object v0, p1

    .line 1423607
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1423608
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1423593
    const-string v0, "ProductPhotoOverlayComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1423594
    if-ne p0, p1, :cond_1

    .line 1423595
    :cond_0
    :goto_0
    return v0

    .line 1423596
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1423597
    goto :goto_0

    .line 1423598
    :cond_3
    check-cast p1, LX/8xA;

    .line 1423599
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1423600
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1423601
    if-eq v2, v3, :cond_0

    .line 1423602
    iget-object v2, p0, LX/8xA;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/8xA;->a:Ljava/lang/String;

    iget-object v3, p1, LX/8xA;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1423603
    goto :goto_0

    .line 1423604
    :cond_4
    iget-object v2, p1, LX/8xA;->a:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
