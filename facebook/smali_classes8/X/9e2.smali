.class public LX/9e2;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private A:I

.field private B:J

.field private C:I

.field private final a:Landroid/content/Context;

.field private final b:LX/9dh;

.field public c:F

.field public d:F

.field private e:F

.field private f:F

.field private g:F

.field private h:F

.field private i:F

.field private j:F

.field private k:F

.field private l:F

.field private m:F

.field private n:F

.field private o:F

.field private p:F

.field private q:F

.field private r:J

.field private s:J

.field private t:Z

.field private u:I

.field private v:I

.field private w:I

.field private x:F

.field private y:F

.field private z:F


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/9dh;)V
    .locals 2

    .prologue
    .line 1519097
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1519098
    iput-object p1, p0, LX/9e2;->a:Landroid/content/Context;

    .line 1519099
    iput-object p2, p0, LX/9e2;->b:LX/9dh;

    .line 1519100
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/9e2;->u:I

    .line 1519101
    const/4 v0, 0x3

    iput v0, p0, LX/9e2;->v:I

    .line 1519102
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1519103
    const v1, 0x7f0b0365

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, LX/9e2;->C:I

    .line 1519104
    const v1, 0x7f0b0366

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/9e2;->w:I

    .line 1519105
    return-void
.end method

.method private static a(FFFF)F
    .locals 6

    .prologue
    .line 1519095
    float-to-double v0, p0

    float-to-double v2, p1

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    float-to-double v2, p2

    float-to-double v4, p3

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    sub-double/2addr v0, v2

    .line 1519096
    const-wide v2, 0x4066800000000000L    # 180.0

    mul-double/2addr v0, v2

    const-wide v2, 0x400921fb54442d18L    # Math.PI

    div-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method

.method private b(Landroid/view/MotionEvent;)V
    .locals 14

    .prologue
    const/4 v1, 0x1

    const/high16 v13, 0x40000000    # 2.0f

    const/4 v2, 0x0

    .line 1519056
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 1519057
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v10

    .line 1519058
    iget-wide v6, p0, LX/9e2;->B:J

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x80

    cmp-long v0, v4, v6

    if-ltz v0, :cond_6

    move v0, v1

    .line 1519059
    :goto_0
    const/4 v3, 0x0

    move v8, v2

    move v9, v2

    move v4, v0

    .line 1519060
    :goto_1
    if-ge v8, v10, :cond_b

    .line 1519061
    iget v0, p0, LX/9e2;->z:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    .line 1519062
    :goto_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v11

    .line 1519063
    add-int/lit8 v12, v11, 0x1

    move v6, v2

    move v5, v3

    .line 1519064
    :goto_3
    if-ge v6, v12, :cond_a

    .line 1519065
    if-ge v6, v11, :cond_8

    .line 1519066
    invoke-virtual {p1, v8, v6}, Landroid/view/MotionEvent;->getHistoricalTouchMajor(II)F

    move-result v3

    .line 1519067
    :goto_4
    iget v7, p0, LX/9e2;->C:I

    int-to-float v7, v7

    cmpg-float v7, v3, v7

    if-gez v7, :cond_0

    iget v3, p0, LX/9e2;->C:I

    int-to-float v3, v3

    .line 1519068
    :cond_0
    add-float v7, v5, v3

    .line 1519069
    iget v5, p0, LX/9e2;->x:F

    invoke-static {v5}, Ljava/lang/Float;->isNaN(F)Z

    move-result v5

    if-nez v5, :cond_1

    iget v5, p0, LX/9e2;->x:F

    cmpl-float v5, v3, v5

    if-lez v5, :cond_2

    .line 1519070
    :cond_1
    iput v3, p0, LX/9e2;->x:F

    .line 1519071
    :cond_2
    iget v5, p0, LX/9e2;->y:F

    invoke-static {v5}, Ljava/lang/Float;->isNaN(F)Z

    move-result v5

    if-nez v5, :cond_3

    iget v5, p0, LX/9e2;->y:F

    cmpg-float v5, v3, v5

    if-gez v5, :cond_4

    .line 1519072
    :cond_3
    iput v3, p0, LX/9e2;->y:F

    .line 1519073
    :cond_4
    if-eqz v0, :cond_d

    .line 1519074
    iget v5, p0, LX/9e2;->z:F

    sub-float/2addr v3, v5

    invoke-static {v3}, Ljava/lang/Math;->signum(F)F

    move-result v3

    float-to-int v3, v3

    .line 1519075
    iget v5, p0, LX/9e2;->A:I

    if-ne v3, v5, :cond_5

    if-nez v3, :cond_d

    iget v5, p0, LX/9e2;->A:I

    if-nez v5, :cond_d

    .line 1519076
    :cond_5
    iput v3, p0, LX/9e2;->A:I

    .line 1519077
    if-ge v6, v11, :cond_9

    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->getHistoricalEventTime(I)J

    move-result-wide v4

    .line 1519078
    :goto_5
    iput-wide v4, p0, LX/9e2;->B:J

    move v3, v2

    .line 1519079
    :goto_6
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    move v5, v7

    move v4, v3

    goto :goto_3

    :cond_6
    move v0, v2

    .line 1519080
    goto :goto_0

    :cond_7
    move v0, v2

    .line 1519081
    goto :goto_2

    .line 1519082
    :cond_8
    invoke-virtual {p1, v8}, Landroid/view/MotionEvent;->getTouchMajor(I)F

    move-result v3

    goto :goto_4

    .line 1519083
    :cond_9
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    goto :goto_5

    .line 1519084
    :cond_a
    add-int v3, v9, v12

    .line 1519085
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    move v9, v3

    move v3, v5

    goto/16 :goto_1

    .line 1519086
    :cond_b
    int-to-float v0, v9

    div-float v0, v3, v0

    .line 1519087
    if-eqz v4, :cond_c

    .line 1519088
    iget v1, p0, LX/9e2;->x:F

    iget v3, p0, LX/9e2;->y:F

    add-float/2addr v1, v3

    add-float/2addr v0, v1

    const/high16 v1, 0x40400000    # 3.0f

    div-float/2addr v0, v1

    .line 1519089
    iget v1, p0, LX/9e2;->x:F

    add-float/2addr v1, v0

    div-float/2addr v1, v13

    iput v1, p0, LX/9e2;->x:F

    .line 1519090
    iget v1, p0, LX/9e2;->y:F

    add-float/2addr v1, v0

    div-float/2addr v1, v13

    iput v1, p0, LX/9e2;->y:F

    .line 1519091
    iput v0, p0, LX/9e2;->z:F

    .line 1519092
    iput v2, p0, LX/9e2;->A:I

    .line 1519093
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v0

    iput-wide v0, p0, LX/9e2;->B:J

    .line 1519094
    :cond_c
    return-void

    :cond_d
    move v3, v4

    goto :goto_6
.end method

.method private d()V
    .locals 2

    .prologue
    const/high16 v0, 0x7fc00000    # NaNf

    .line 1518963
    iput v0, p0, LX/9e2;->x:F

    .line 1518964
    iput v0, p0, LX/9e2;->y:F

    .line 1518965
    iput v0, p0, LX/9e2;->z:F

    .line 1518966
    const/4 v0, 0x0

    iput v0, p0, LX/9e2;->A:I

    .line 1518967
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/9e2;->B:J

    .line 1518968
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MotionEvent;)Z
    .locals 13

    .prologue
    .line 1518970
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v0

    iput-wide v0, p0, LX/9e2;->r:J

    .line 1518971
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v7

    .line 1518972
    const/4 v0, 0x1

    if-eq v7, v0, :cond_0

    const/4 v0, 0x3

    if-ne v7, v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    .line 1518973
    :goto_0
    if-eqz v7, :cond_1

    if-eqz v0, :cond_4

    .line 1518974
    :cond_1
    iget-boolean v1, p0, LX/9e2;->t:Z

    if-eqz v1, :cond_2

    .line 1518975
    iget-object v1, p0, LX/9e2;->b:LX/9dh;

    invoke-virtual {v1}, LX/9dh;->a()V

    .line 1518976
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/9e2;->t:Z

    .line 1518977
    const/4 v1, 0x0

    iput v1, p0, LX/9e2;->g:F

    .line 1518978
    :cond_2
    if-eqz v0, :cond_4

    .line 1518979
    invoke-direct {p0}, LX/9e2;->d()V

    .line 1518980
    const/4 v0, 0x1

    .line 1518981
    :goto_1
    return v0

    .line 1518982
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 1518983
    :cond_4
    if-eqz v7, :cond_5

    const/4 v0, 0x6

    if-eq v7, v0, :cond_5

    const/4 v0, 0x5

    if-ne v7, v0, :cond_7

    :cond_5
    const/4 v0, 0x1

    move v6, v0

    .line 1518984
    :goto_2
    const/4 v0, 0x6

    if-ne v7, v0, :cond_8

    const/4 v0, 0x1

    move v1, v0

    .line 1518985
    :goto_3
    if-eqz v1, :cond_9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    .line 1518986
    :goto_4
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1518987
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    .line 1518988
    if-eqz v1, :cond_a

    add-int/lit8 v1, v2, -0x1

    .line 1518989
    :goto_5
    const/4 v3, 0x0

    move v12, v3

    move v3, v4

    move v4, v5

    move v5, v12

    :goto_6
    if-ge v5, v2, :cond_b

    .line 1518990
    if-eq v0, v5, :cond_6

    .line 1518991
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v8

    add-float/2addr v4, v8

    .line 1518992
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v8

    add-float/2addr v3, v8

    .line 1518993
    :cond_6
    add-int/lit8 v5, v5, 0x1

    goto :goto_6

    .line 1518994
    :cond_7
    const/4 v0, 0x0

    move v6, v0

    goto :goto_2

    .line 1518995
    :cond_8
    const/4 v0, 0x0

    move v1, v0

    goto :goto_3

    .line 1518996
    :cond_9
    const/4 v0, -0x1

    goto :goto_4

    :cond_a
    move v1, v2

    .line 1518997
    goto :goto_5

    .line 1518998
    :cond_b
    int-to-float v5, v1

    div-float v8, v4, v5

    .line 1518999
    int-to-float v4, v1

    div-float v9, v3, v4

    .line 1519000
    invoke-direct {p0, p1}, LX/9e2;->b(Landroid/view/MotionEvent;)V

    .line 1519001
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1519002
    const/4 v3, 0x0

    move v12, v3

    move v3, v4

    move v4, v5

    move v5, v12

    :goto_7
    if-ge v5, v2, :cond_d

    .line 1519003
    if-eq v0, v5, :cond_c

    .line 1519004
    iget v10, p0, LX/9e2;->z:F

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    .line 1519005
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v11

    sub-float/2addr v11, v8

    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    move-result v11

    add-float/2addr v11, v10

    add-float/2addr v4, v11

    .line 1519006
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v11

    sub-float/2addr v11, v9

    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    move-result v11

    add-float/2addr v10, v11

    add-float/2addr v3, v10

    .line 1519007
    :cond_c
    add-int/lit8 v5, v5, 0x1

    goto :goto_7

    .line 1519008
    :cond_d
    int-to-float v0, v1

    div-float/2addr v4, v0

    .line 1519009
    int-to-float v0, v1

    div-float/2addr v3, v0

    .line 1519010
    const/4 v0, 0x1

    if-le v2, v0, :cond_15

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    sub-float/2addr v0, v1

    move v1, v0

    .line 1519011
    :goto_8
    const/4 v0, 0x1

    if-le v2, v0, :cond_16

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    sub-float/2addr v0, v2

    .line 1519012
    :goto_9
    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v2, v4

    .line 1519013
    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v3, v4

    .line 1519014
    mul-float v4, v2, v2

    mul-float v5, v3, v3

    add-float/2addr v4, v5

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v4, v4

    .line 1519015
    iget-boolean v5, p0, LX/9e2;->t:Z

    .line 1519016
    iput v8, p0, LX/9e2;->c:F

    .line 1519017
    iput v9, p0, LX/9e2;->d:F

    .line 1519018
    iget-boolean v8, p0, LX/9e2;->t:Z

    if-eqz v8, :cond_f

    iget v8, p0, LX/9e2;->w:I

    int-to-float v8, v8

    cmpg-float v8, v4, v8

    if-ltz v8, :cond_e

    if-eqz v6, :cond_f

    .line 1519019
    :cond_e
    iget-object v8, p0, LX/9e2;->b:LX/9dh;

    invoke-virtual {v8}, LX/9dh;->a()V

    .line 1519020
    const/4 v8, 0x0

    iput-boolean v8, p0, LX/9e2;->t:Z

    .line 1519021
    iput v4, p0, LX/9e2;->g:F

    .line 1519022
    :cond_f
    if-eqz v6, :cond_10

    .line 1519023
    iput v2, p0, LX/9e2;->h:F

    iput v2, p0, LX/9e2;->l:F

    .line 1519024
    iput v3, p0, LX/9e2;->i:F

    iput v3, p0, LX/9e2;->m:F

    .line 1519025
    iput v1, p0, LX/9e2;->j:F

    iput v1, p0, LX/9e2;->n:F

    iput v1, p0, LX/9e2;->p:F

    .line 1519026
    iput v0, p0, LX/9e2;->k:F

    iput v0, p0, LX/9e2;->o:F

    iput v0, p0, LX/9e2;->q:F

    .line 1519027
    iput v4, p0, LX/9e2;->e:F

    iput v4, p0, LX/9e2;->f:F

    iput v4, p0, LX/9e2;->g:F

    .line 1519028
    :cond_10
    iget v6, p0, LX/9e2;->w:I

    .line 1519029
    iget-boolean v8, p0, LX/9e2;->t:Z

    if-nez v8, :cond_12

    int-to-float v6, v6

    cmpl-float v6, v4, v6

    if-ltz v6, :cond_12

    if-nez v5, :cond_11

    iget v5, p0, LX/9e2;->q:F

    iget v6, p0, LX/9e2;->p:F

    invoke-static {v5, v6, v0, v1}, LX/9e2;->a(FFFF)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    iget v6, p0, LX/9e2;->v:I

    int-to-float v6, v6

    cmpl-float v5, v5, v6

    if-lez v5, :cond_12

    .line 1519030
    :cond_11
    iput v2, p0, LX/9e2;->h:F

    iput v2, p0, LX/9e2;->l:F

    .line 1519031
    iput v3, p0, LX/9e2;->i:F

    iput v3, p0, LX/9e2;->m:F

    .line 1519032
    iput v1, p0, LX/9e2;->j:F

    iput v1, p0, LX/9e2;->n:F

    .line 1519033
    iput v0, p0, LX/9e2;->k:F

    iput v0, p0, LX/9e2;->o:F

    .line 1519034
    iput v4, p0, LX/9e2;->e:F

    iput v4, p0, LX/9e2;->f:F

    .line 1519035
    iget-wide v8, p0, LX/9e2;->r:J

    iput-wide v8, p0, LX/9e2;->s:J

    .line 1519036
    iget-object v5, p0, LX/9e2;->b:LX/9dh;

    invoke-virtual {v5, p0}, LX/9dh;->a(LX/9e2;)Z

    move-result v5

    iput-boolean v5, p0, LX/9e2;->t:Z

    .line 1519037
    :cond_12
    const/4 v5, 0x2

    if-ne v7, v5, :cond_14

    .line 1519038
    iput v2, p0, LX/9e2;->h:F

    .line 1519039
    iput v3, p0, LX/9e2;->i:F

    .line 1519040
    iput v4, p0, LX/9e2;->e:F

    .line 1519041
    iput v1, p0, LX/9e2;->j:F

    .line 1519042
    iput v0, p0, LX/9e2;->k:F

    .line 1519043
    const/4 v0, 0x1

    .line 1519044
    iget-boolean v1, p0, LX/9e2;->t:Z

    if-eqz v1, :cond_13

    .line 1519045
    iget-object v0, p0, LX/9e2;->b:LX/9dh;

    invoke-virtual {v0, p0}, LX/9dh;->b(LX/9e2;)Z

    move-result v0

    .line 1519046
    :cond_13
    if-eqz v0, :cond_14

    .line 1519047
    iget v0, p0, LX/9e2;->h:F

    iput v0, p0, LX/9e2;->l:F

    .line 1519048
    iget v0, p0, LX/9e2;->i:F

    iput v0, p0, LX/9e2;->m:F

    .line 1519049
    iget v0, p0, LX/9e2;->j:F

    iput v0, p0, LX/9e2;->n:F

    .line 1519050
    iget v0, p0, LX/9e2;->k:F

    iput v0, p0, LX/9e2;->o:F

    .line 1519051
    iget v0, p0, LX/9e2;->e:F

    iput v0, p0, LX/9e2;->f:F

    .line 1519052
    iget-wide v0, p0, LX/9e2;->r:J

    iput-wide v0, p0, LX/9e2;->s:J

    .line 1519053
    :cond_14
    const/4 v0, 0x1

    goto/16 :goto_1

    .line 1519054
    :cond_15
    const/4 v0, 0x0

    move v1, v0

    goto/16 :goto_8

    .line 1519055
    :cond_16
    const/4 v0, 0x0

    goto/16 :goto_9
.end method

.method public final c()F
    .locals 4

    .prologue
    .line 1518969
    iget v0, p0, LX/9e2;->o:F

    iget v1, p0, LX/9e2;->n:F

    iget v2, p0, LX/9e2;->k:F

    iget v3, p0, LX/9e2;->j:F

    invoke-static {v0, v1, v2, v3}, LX/9e2;->a(FFFF)F

    move-result v0

    return v0
.end method
