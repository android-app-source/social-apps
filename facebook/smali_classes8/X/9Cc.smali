.class public final LX/9Cc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLComment;

.field public final synthetic c:LX/9Cd;


# direct methods
.method public constructor <init>(LX/9Cd;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 0

    .prologue
    .line 1454202
    iput-object p1, p0, LX/9Cc;->c:LX/9Cd;

    iput-object p2, p0, LX/9Cc;->a:Ljava/lang/String;

    iput-object p3, p0, LX/9Cc;->b:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1454203
    iget-object v0, p0, LX/9Cc;->c:LX/9Cd;

    iget-object v0, v0, LX/9Cd;->q:LX/2dj;

    iget-object v1, p0, LX/9Cc;->a:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v1, p0, LX/9Cc;->b:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v0, v2, v3, v4, v5}, LX/2dj;->a(JJ)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
