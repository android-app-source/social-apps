.class public LX/A7E;
.super Landroid/widget/BaseAdapter;
.source ""


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/A7C;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "LX/A7C;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1625692
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 1625693
    iput-object p2, p0, LX/A7E;->a:Ljava/util/List;

    .line 1625694
    iput-object p1, p0, LX/A7E;->b:Landroid/content/Context;

    .line 1625695
    return-void
.end method

.method private a(I)LX/A7C;
    .locals 1

    .prologue
    .line 1625696
    iget-object v0, p0, LX/A7E;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/A7C;

    return-object v0
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    .prologue
    .line 1625697
    iget-object v0, p0, LX/A7E;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1625698
    invoke-direct {p0, p1}, LX/A7E;->a(I)LX/A7C;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1625699
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1625700
    if-nez p2, :cond_0

    .line 1625701
    iget-object v0, p0, LX/A7E;->b:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 1625702
    const v1, 0x7f030278

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 1625703
    new-instance v1, LX/A7D;

    invoke-direct {v1}, LX/A7D;-><init>()V

    .line 1625704
    iput p1, v1, LX/A7D;->c:I

    .line 1625705
    const v0, 0x7f0d091a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, v1, LX/A7D;->a:Lcom/facebook/resources/ui/FbTextView;

    .line 1625706
    const v0, 0x7f0d091b

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, v1, LX/A7D;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 1625707
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v0, v1

    .line 1625708
    :goto_0
    invoke-direct {p0, p1}, LX/A7E;->a(I)LX/A7C;

    move-result-object v1

    .line 1625709
    iget-object v2, v0, LX/A7D;->a:Lcom/facebook/resources/ui/FbTextView;

    iget-object v3, v1, LX/A7C;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1625710
    iget-object v0, v0, LX/A7D;->b:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, v1, LX/A7C;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1625711
    return-object p2

    .line 1625712
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/A7D;

    goto :goto_0
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 1625713
    const/4 v0, 0x1

    return v0
.end method
