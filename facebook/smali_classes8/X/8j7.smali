.class public LX/8j7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/8j7;


# instance fields
.field private final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1392170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1392171
    iput-object p1, p0, LX/8j7;->a:LX/0Zb;

    .line 1392172
    return-void
.end method

.method public static a(LX/0QB;)LX/8j7;
    .locals 4

    .prologue
    .line 1392173
    sget-object v0, LX/8j7;->b:LX/8j7;

    if-nez v0, :cond_1

    .line 1392174
    const-class v1, LX/8j7;

    monitor-enter v1

    .line 1392175
    :try_start_0
    sget-object v0, LX/8j7;->b:LX/8j7;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1392176
    if-eqz v2, :cond_0

    .line 1392177
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1392178
    new-instance p0, LX/8j7;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/8j7;-><init>(LX/0Zb;)V

    .line 1392179
    move-object v0, p0

    .line 1392180
    sput-object v0, LX/8j7;->b:LX/8j7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1392181
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1392182
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1392183
    :cond_1
    sget-object v0, LX/8j7;->b:LX/8j7;

    return-object v0

    .line 1392184
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1392185
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 1392186
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "click"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1392187
    iput-object p0, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->d:Ljava/lang/String;

    .line 1392188
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 1

    .prologue
    .line 1392189
    iget-object v0, p0, LX/8j7;->a:LX/0Zb;

    invoke-interface {v0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1392190
    return-void
.end method
