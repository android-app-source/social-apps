.class public LX/8nZ;
.super LX/8nB;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/3iT;

.field public final c:Landroid/content/res/Resources;

.field public final d:LX/0tX;

.field public final e:Ljava/util/concurrent/ExecutorService;

.field private final f:Ljava/util/concurrent/ScheduledExecutorService;

.field private final g:LX/0So;

.field private h:J

.field private i:Ljava/util/concurrent/ScheduledFuture;


# direct methods
.method public constructor <init>(LX/3iT;Landroid/content/res/Resources;LX/0tX;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ScheduledExecutorService;LX/0So;Ljava/lang/String;)V
    .locals 0
    .param p4    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .param p5    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1401184
    invoke-direct {p0}, LX/8nB;-><init>()V

    .line 1401185
    iput-object p7, p0, LX/8nZ;->a:Ljava/lang/String;

    .line 1401186
    iput-object p1, p0, LX/8nZ;->b:LX/3iT;

    .line 1401187
    iput-object p2, p0, LX/8nZ;->c:Landroid/content/res/Resources;

    .line 1401188
    iput-object p3, p0, LX/8nZ;->d:LX/0tX;

    .line 1401189
    iput-object p4, p0, LX/8nZ;->e:Ljava/util/concurrent/ExecutorService;

    .line 1401190
    iput-object p5, p0, LX/8nZ;->f:Ljava/util/concurrent/ScheduledExecutorService;

    .line 1401191
    iput-object p6, p0, LX/8nZ;->g:LX/0So;

    .line 1401192
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;Ljava/lang/String;ZZZZZLX/8JX;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0xc8

    .line 1401193
    if-nez p1, :cond_0

    .line 1401194
    :goto_0
    return-void

    .line 1401195
    :cond_0
    iget-object v0, p0, LX/8nZ;->g:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iget-wide v2, p0, LX/8nZ;->h:J

    sub-long/2addr v0, v2

    cmp-long v0, v0, v4

    if-gez v0, :cond_1

    iget-object v0, p0, LX/8nZ;->i:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_1

    .line 1401196
    iget-object v0, p0, LX/8nZ;->i:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 1401197
    :cond_1
    iget-object v0, p0, LX/8nZ;->g:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/8nZ;->h:J

    .line 1401198
    iget-object v0, p0, LX/8nZ;->f:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/tagging/graphql/data/GroupsTaggingTypeaheadDataSource$1;

    invoke-direct {v1, p0, p1, p2, p8}, Lcom/facebook/tagging/graphql/data/GroupsTaggingTypeaheadDataSource$1;-><init>(LX/8nZ;Ljava/lang/CharSequence;Ljava/lang/String;LX/8JX;)V

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v4, v5, v2}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/8nZ;->i:Ljava/util/concurrent/ScheduledFuture;

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1401199
    const/4 v0, 0x1

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1401200
    const-string v0, "groups_fetcher"

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1401201
    sget-object v0, LX/8nE;->GROUPS:LX/8nE;

    invoke-virtual {v0}, LX/8nE;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1401202
    iget-object v0, p0, LX/8nZ;->a:Ljava/lang/String;

    return-object v0
.end method
