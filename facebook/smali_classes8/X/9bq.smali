.class public LX/9bq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0rq;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9bp;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tE;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0ad;

.field private final g:LX/0sa;

.field private final h:LX/0tG;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0ad;LX/0sa;LX/0tG;)V
    .locals 0
    .param p1    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0rq;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/9bp;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0tE;",
            ">;",
            "LX/0ad;",
            "LX/0sa;",
            "LX/0tG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1515207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1515208
    iput-object p1, p0, LX/9bq;->a:LX/0Ot;

    .line 1515209
    iput-object p2, p0, LX/9bq;->b:LX/0Ot;

    .line 1515210
    iput-object p3, p0, LX/9bq;->c:LX/0Ot;

    .line 1515211
    iput-object p4, p0, LX/9bq;->d:LX/0Ot;

    .line 1515212
    iput-object p5, p0, LX/9bq;->e:LX/0Ot;

    .line 1515213
    iput-object p6, p0, LX/9bq;->f:LX/0ad;

    .line 1515214
    iput-object p7, p0, LX/9bq;->g:LX/0sa;

    .line 1515215
    iput-object p8, p0, LX/9bq;->h:LX/0tG;

    .line 1515216
    return-void
.end method

.method private a(LX/0gW;Ljava/lang/String;I)LX/0gW;
    .locals 6

    .prologue
    .line 1515271
    const/4 v4, 0x0

    const/16 v5, 0x64

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, LX/9bq;->a(LX/0gW;Ljava/lang/String;ILjava/lang/String;I)LX/0gW;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/0gW;Ljava/lang/String;ILjava/lang/String;I)LX/0gW;
    .locals 4
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 1515257
    const-string v0, "node_id"

    invoke-virtual {p1, v0, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1515258
    const-string v0, "first"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1515259
    if-eqz p4, :cond_0

    .line 1515260
    const-string v0, "after"

    invoke-virtual {p1, v0, p4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1515261
    :cond_0
    const-string v0, "image_width"

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1515262
    const-string v0, "image_height"

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1515263
    const-string v1, "media_type"

    iget-object v0, p0, LX/9bq;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rq;

    invoke-virtual {v0}, LX/0rq;->a()LX/0wF;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 1515264
    const-string v0, "enable_album_feedback"

    iget-object v1, p0, LX/9bq;->f:LX/0ad;

    sget-short v2, LX/5lr;->b:S

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 1515265
    iget-object v0, p0, LX/9bq;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tE;

    invoke-virtual {v0, p1, v3}, LX/0tE;->a(LX/0gW;Z)V

    .line 1515266
    const-string v0, "profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1515267
    const-string v0, "max_comments"

    const/16 v1, 0x19

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1515268
    const-string v0, "fetch_reshare_counts"

    iget-object v1, p0, LX/9bq;->f:LX/0ad;

    sget-short v2, LX/0wn;->aF:S

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 1515269
    iget-object v0, p0, LX/9bq;->h:LX/0tG;

    invoke-virtual {v0, p1}, LX/0tG;->a(LX/0gW;)V

    .line 1515270
    return-object p1
.end method

.method public static a(LX/0QB;)LX/9bq;
    .locals 12

    .prologue
    .line 1515246
    const-class v1, LX/9bq;

    monitor-enter v1

    .line 1515247
    :try_start_0
    sget-object v0, LX/9bq;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1515248
    sput-object v2, LX/9bq;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1515249
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1515250
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1515251
    new-instance v3, LX/9bq;

    const/16 v4, 0x140f

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xafd

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xb24

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2e02

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x124

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-static {v0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v10

    check-cast v10, LX/0sa;

    invoke-static {v0}, LX/0tG;->a(LX/0QB;)LX/0tG;

    move-result-object v11

    check-cast v11, LX/0tG;

    invoke-direct/range {v3 .. v11}, LX/9bq;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0ad;LX/0sa;LX/0tG;)V

    .line 1515252
    move-object v0, v3

    .line 1515253
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1515254
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/9bq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1515255
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1515256
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Ljava/lang/String;)Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1515272
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "FetchPhotoAlbums"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;I)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1515235
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1515236
    new-instance v0, LX/5g2;

    invoke-direct {v0}, LX/5g2;-><init>()V

    .line 1515237
    invoke-direct {p0, v0, p1, p2}, LX/9bq;->a(LX/0gW;Ljava/lang/String;I)LX/0gW;

    move-result-object v0

    check-cast v0, LX/5g2;

    .line 1515238
    iget-object v1, p0, LX/9bq;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0tX;

    .line 1515239
    new-instance v2, LX/5g2;

    invoke-direct {v2}, LX/5g2;-><init>()V

    move-object v2, v2

    .line 1515240
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    sget-object v3, LX/0zS;->d:LX/0zS;

    invoke-virtual {v2, v3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v2

    .line 1515241
    iget-object v3, v0, LX/0gW;->e:LX/0w7;

    move-object v0, v3

    .line 1515242
    invoke-virtual {v2, v0}, LX/0zO;->a(LX/0w7;)LX/0zO;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    .line 1515243
    iget-object v0, p0, LX/9bq;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0QK;

    iget-object v1, p0, LX/9bq;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    invoke-static {v2, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1515244
    return-object v0

    .line 1515245
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;ILX/0zS;Ljava/lang/String;I)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "LX/0zS;",
            "Ljava/lang/String;",
            "I)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1515234
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v6}, LX/9bq;->a(Ljava/lang/String;ILX/0zS;Ljava/lang/String;ILjava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;ILX/0zS;Ljava/lang/String;ILjava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/util/Set;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "LX/0zS;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1515224
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1515225
    new-instance v1, LX/5g3;

    invoke-direct {v1}, LX/5g3;-><init>()V

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p4

    move v5, p5

    .line 1515226
    invoke-direct/range {v0 .. v5}, LX/9bq;->a(LX/0gW;Ljava/lang/String;ILjava/lang/String;I)LX/0gW;

    move-result-object v0

    check-cast v0, LX/5g3;

    .line 1515227
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    .line 1515228
    if-eqz p6, :cond_0

    .line 1515229
    iput-object p6, v1, LX/0zO;->d:Ljava/util/Set;

    .line 1515230
    :cond_0
    iget-object v0, p0, LX/9bq;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    .line 1515231
    iget-object v0, p0, LX/9bq;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0QK;

    iget-object v1, p0, LX/9bq;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    invoke-static {v2, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1515232
    return-object v0

    .line 1515233
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;I)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1515217
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1515218
    new-instance v0, LX/5g5;

    invoke-direct {v0}, LX/5g5;-><init>()V

    .line 1515219
    invoke-direct {p0, v0, p1, p2}, LX/9bq;->a(LX/0gW;Ljava/lang/String;I)LX/0gW;

    move-result-object v0

    check-cast v0, LX/5g5;

    .line 1515220
    iget-object v1, p0, LX/9bq;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v2, LX/0zS;->d:LX/0zS;

    invoke-virtual {v0, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    .line 1515221
    iget-object v0, p0, LX/9bq;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0QK;

    iget-object v1, p0, LX/9bq;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    invoke-static {v2, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1515222
    return-object v0

    .line 1515223
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
